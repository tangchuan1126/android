package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oso.com.vvmescreen.R;
import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;

public class Goable {

	public static final String from_product_list_to_create = "from_product_list_to_create";
	public static final String from_product_create = "from_product_create" ;
	public static String Machine  = "";
	public static final String FROM = "from"; 
	
	public static String LoginAccount = "";
	public static String Password = "";
	public static boolean hasSd = false ;
	public static String Version =  "" ;

	public static long adid = 0L;
	public static long adgid = 0L;
	public static int CLIENT_ADGID = 100026;//客户部门ID
	

	
	
	public static int adminUsersPerRow = 3;
	
	public static String file_base_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/";
	
	public static String file_image_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/";	//拍照原始的地址
	
	public static String file_image_path_up = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/up/";	//拍照上传图片的地址

	
	public static String file_image_path_thumbnail = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/thumbnail/";	//拍照图片生成缩略图的地址
	
	//android has sd .file dirs 
	public static String file_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/files/";
	
	
	public static int STORAGE_TYPE_SELECT = 0;
	public static int STORAGE_TYPE_SELF = 1;
	public static int STORAGE_TYPE_TRANSPORT = 2;
	public static int STORAGE_TYPE_SUPPLIER = 3;
	public static int SOTRAGE_TYPE_THIRD = 4;
	public static int STORAGE_TYPE_CLIENT = 5;
	public static int STORAGE_TYPE_REPAIR = 6;

	
	
	public static String DeliveyGotoTab = "DeliveyGotoTab"; 
	
	public static Map<Integer, String> getStorageTypeKey(Resources res){
		Map<Integer, String> map=new HashMap<Integer, String>();
		 map.put(STORAGE_TYPE_SELECT, "请选择.....");
		map.put(STORAGE_TYPE_SELF, res.getString(R.string.storage_type_self));
		map.put(STORAGE_TYPE_TRANSPORT, res.getString(R.string.storage_type_transport));
		map.put(STORAGE_TYPE_SUPPLIER, res.getString(R.string.storage_type_supplier));
		map.put(SOTRAGE_TYPE_THIRD,  res.getString(R.string.sotrage_type_third));
		map.put(STORAGE_TYPE_CLIENT, res.getString(R.string.storage_type_client));
		map.put(STORAGE_TYPE_REPAIR, res.getString(R.string.storage_type_repair));

		return map;
	}
	  public static List<String> getStorageTypeName(Resources res){
			List<String> typeName=new ArrayList<String>();
			Map<Integer, String> typeKey=getStorageTypeKey(res);
			typeName.add(typeKey.get(STORAGE_TYPE_SELECT));
			typeName.add(typeKey.get(STORAGE_TYPE_SELF));
			typeName.add(typeKey.get(STORAGE_TYPE_TRANSPORT));
			typeName.add(typeKey.get(STORAGE_TYPE_SUPPLIER));
			typeName.add(typeKey.get(SOTRAGE_TYPE_THIRD));
			typeName.add(typeKey.get(STORAGE_TYPE_CLIENT));
			typeName.add(typeKey.get(STORAGE_TYPE_REPAIR));
			return typeName;
		}
	  public static int PC_CODE_TYPE_PCID = 0;
	  public static int PC_CODE_TYPE_MAIN = 1;
	  public static int PC_CODE_TYPE_UPC = 2;
	  public static int PC_CODE_TYPE_AMAZON = 3;
	  public static int PC_CODE_TYPE_OLD = 4;

	public static Map<Integer, String> getPcodeTypeKey(){
		Map<Integer, String> map=new HashMap<Integer, String>();
		map.put(0, "ID");
		map.put(1, "Main");
		map.put(2, "UPC");
		map.put(3, "Amazon");
		map.put(4, "Old");
		return map;
	}
	
	public static Map<String, Integer> getPcodeTypeValue(){
		Map<String, Integer> map=new HashMap<String, Integer>();
		map.put("ID" ,0);
		map.put("Main" ,1);
		map.put("UPC" ,2);
		map.put("Amazon" ,3);
		map.put("Old" ,4);
		return map;
	}
	
	public static List<String> getPcodeTypeName(){
		List<String> typeName=new ArrayList<String>();
		Map<Integer, String> typeKey=getPcodeTypeKey();
		typeName.add(typeKey.get(1));
		typeName.add(typeKey.get(2));
		typeName.add(typeKey.get(3));
		typeName.add(typeKey.get(4));
		return typeName;
	}
	public static int getKeyByValue(Map map,String value){
		Set ks=map.keySet(); 
		Iterator it=ks.iterator(); 
		while(it.hasNext()){ 
		int key=(Integer) it.next(); 
		Object getValue=map.get(key); 
		if(getValue.equals(value)){
			return key;
			}
		}
		return 0;
	}
	public static String getCodeTypeValueByKey(Map<String,String> map,int key){
		String value = (String)map.get(key);
		return value;
	}

	public static void initGoable(Context context){
		if(Goable.LoginAccount != null && Goable.LoginAccount.length() > 0 && !StringUtil.isNullOfStr(Goable.Version)) {
			return ;
		}
		initGoable();
		Goable.Version = Utility.getVersionName(context);
	}
	private static void initGoable(){
		if(Goable.LoginAccount != null && Goable.LoginAccount.trim().length() > 0) { return ;}
		Goable.LoginAccount = "admin";
		Goable.Password = "admin";
		Goable.Machine = "admin";
		Goable.hasSd = Utility.isSDExits() ;
	}
}
