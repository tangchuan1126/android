package util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class HttpUrlPath {

//	public static String basePath = "http://192.198.208.38/Sync10/";//假象数据 可删,美国
	public static String basePath = "http://sync.logisticsteam.com/Sync10/";//张睿
	
	/**
	 * 大屏幕action
	 */
	public static String CheckInWareHouseBigScreenAction = "action/checkin/CheckInWareHouseBigScreenAction.action";
	
	public static String getCheckInWareHouseBigScreenAction = "action/checkin/CheckInWareHouseBigScreenAction.action";
	public static String MicroServices = "_receive/android/scheduleScreen";//微服务
 	public static String LoadApkVersion = "action/android/LoadAndroidScreenApkAction.action";
//	public static String LoadApkVersion = "/action/warehouse.action";
 	
 	public static void setSP(String url ,Context context){
 		SharedPreferences sp = context.getSharedPreferences("httpurls", Context.MODE_PRIVATE);
 		Editor editor = sp.edit(); // 选中了缓存帐号密码
		editor.putString("url", url);
		editor.commit();
 	}
 	
 	public static String getSP(Context context){
 		SharedPreferences sp = context.getSharedPreferences("httpurls", Context.MODE_PRIVATE);
 		return sp.getString("url", basePath);
 	}
}
