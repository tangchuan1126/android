package util;

import java.io.Serializable;
import java.util.List;

import com.android.activity.common.ScreenModeTypeBean;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
/**
 * @ClassName: TheInitialScreenData 
 * @Description: 参数设定适配屏幕的基础数据
 * @author gcy
 * @date 2014-11-22 下午4:38:43
 */
public class TheInitialScreenData implements Serializable  {
	

	private static final long serialVersionUID = -2916773620327564701L;

	private int pageSize;//大屏幕请求的数据总数
	private int pageNo;//大屏幕的的页码
	private int everyItem;//通用21行行高px
	private int widthPixels;//屏幕的宽px
	private int heightPixels;//屏幕的高px
	private int heightWucha;//误差 由于所求尺寸均为int类型 浮点后的数据被省略掉了 
	private int padding;//padding的尺寸 px
	private int widthWucha;//误差 由于所求尺寸均为int类型 浮点后的数据被省略掉了  
	private int textSize;//字体大小

	private int navigationHeight;
	
	
	private int smallScreenEveryItem;//小屏幕的行高
	private int smallScreenTitleHeight;//小屏幕的Title的行高
	private int smallScreenNavigationHeight;//小屏幕的导航的行高
	private int smallScreenTextSize;//小屏幕的字体大小
	private int smallWucha;//小屏幕的误差
	
	private int largeScreenEveryItem;
	private int largeScreenTitleHeight;
	private int largeScreenNavigationHeight;
	private int largeScreenTextSize;
	
	private int DATAS_ALL_SIZE;
	
	/*********************************************************/
	
	public static TheInitialScreenData getScreenMode(Activity activity){
		DisplayMetrics  dm = ScreenMonitorTools.getDisplayMetrics(activity);
		TheInitialScreenData s = getScreenMode(activity, dm.heightPixels, dm.widthPixels);
		return s;
	}

	/**
	 * 可以计算出屏幕的一些通用数据
	 * @param activity
	 * @param heightPixels
	 * @param widthPixels
	 * @return
	 */	
	public static TheInitialScreenData getScreenMode(Activity activity,int heightPixels,int widthPixels){
		TheInitialScreenData s = new TheInitialScreenData();//经多次测试 算出屏幕顶栏导航条高度的适配栏目数为22 以两屏为基准
		int screen_size = 22;
		s.setPageSize(screen_size);
		s.setHeightPixels(heightPixels);
		s.setWidthPixels(widthPixels);
		s.setPadding(ScreenMonitorTools.getPadding(activity));
		s.setEveryItem(ScreenMonitorTools.getEveryItem(s.getHeightPixels() - 4*s.getPadding())); //meiyixiang
		s.setTextSize(DisplayUtil.px2sp(activity,s.getEveryItem()/2));
		s.setHeightWucha(ScreenMonitorTools.getHeightWucha(s.getHeightPixels() - 4*s.getPadding()));
		s.setWidthWucha(ScreenMonitorTools.getWidthWucha(s.getWidthPixels()));
		int navigationDefult = s.getEveryItem()+s.getHeightWucha()/3+s.getHeightWucha()%3;
		s.setNavigationHeight(navigationDefult);
	
		return s;
	}
	
	/**
	 * @Description:初始化屏幕基础数据
	 * @param @param activity
	 * @param @param s
	 * @param @param DATAS_ALL_SIZE 
	 * @param @param longitudinalNum 用于计算padding值
	 * @param @return
	 */
	public static TheInitialScreenData getScreenModess(Activity activity,TheInitialScreenData s,int DATAS_ALL_SIZE,int longitudinalNum){		
		double biaozhun = 0.8;//缩放屏幕小标题以及小导航的比率
		
		//---------------------------------------------------------------------------------
//		int longitudinalNum = screenNum/2 + screenNum%2;
		
		int height = s.getHeightPixels()-s.getNavigationHeight()-s.getPadding()*longitudinalNum*2;
		int everyItes = height/DATAS_ALL_SIZE;
		int sixwucha = height%DATAS_ALL_SIZE;
		int sixsmall = (int)(everyItes*biaozhun);
		int haveH = (everyItes-sixsmall)*longitudinalNum*2+everyItes*(DATAS_ALL_SIZE-longitudinalNum*2)+sixwucha;
		everyItes = haveH/(DATAS_ALL_SIZE-longitudinalNum*2);
		sixwucha = haveH%(DATAS_ALL_SIZE-longitudinalNum*2);
		int sixNavigation = sixsmall+sixwucha/longitudinalNum;
		s.setSmallScreenEveryItem(everyItes);
		s.setSmallScreenNavigationHeight(sixNavigation);
		s.setSmallScreenTitleHeight(sixsmall);
		
		int standardSize = 31;
		if(DATAS_ALL_SIZE<standardSize){
			int flag_everyItes = height/standardSize;
			int flag_sixwucha = height%standardSize;
			int flag_sixsmall = (int)(flag_everyItes*biaozhun);
			int flag_haveH = (flag_everyItes-flag_sixsmall)*longitudinalNum*2+flag_everyItes*(standardSize-longitudinalNum*2)+flag_sixwucha;
			flag_everyItes = flag_haveH/(standardSize-longitudinalNum*2);
			s.setSmallScreenTextSize(DisplayUtil.px2sp(activity,flag_everyItes/2));
		}else{
			s.setSmallScreenTextSize(DisplayUtil.px2sp(activity,everyItes/2));
		}
		s.setSmallWucha(everyItes%longitudinalNum);
		
		
		//---------------------------------------------------------------------------------
		
		
		int lheight = s.getHeightPixels()-s.getNavigationHeight()-s.getPadding()*2;
		int leveryItes = lheight/DATAS_ALL_SIZE;
		int lsixwucha = lheight%DATAS_ALL_SIZE;
		int lsixsmall = (int)(leveryItes*biaozhun);
		int lhaveH = (leveryItes-lsixsmall)*2+leveryItes*(DATAS_ALL_SIZE-2)+lsixwucha;
		leveryItes = lhaveH/(DATAS_ALL_SIZE-2);
		lsixwucha = lhaveH%(DATAS_ALL_SIZE-2);
		int lsixNavigation = lsixsmall+lsixwucha/2+lsixwucha%2;
		lsixsmall = lsixsmall+lsixwucha/2;
		s.setLargeScreenEveryItem(leveryItes);
		s.setLargeScreenTitleHeight(lsixsmall);
		s.setLargeScreenNavigationHeight(lsixNavigation);
		s.setLargeTextSize(DisplayUtil.px2sp(activity,leveryItes/2));
		
		s.setDATAS_ALL_SIZE(DATAS_ALL_SIZE);
		return s;
	}
	
	public static int fixLongitudinalNum(List<ScreenModeTypeBean> list){	
		int line = 0;
		if(!Utility.isNullForList(list)){
			for(int i=0;i<list.size();i++){
				
				if(line<list.get(i).getLine()){
					line = list.get(i).getLine();
				}
			}
		}
		return line;
	}
	
	public static Intent setIntent(TheInitialScreenData screenModeBean){
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable("screenMode", screenModeBean);
		intent.putExtras(bundle);
		return intent;
	}
	
	/*********************************************************/
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getEveryItem() {
		return everyItem;
	}
	public void setEveryItem(int everyItem) {
		this.everyItem = everyItem;
	}
	public int getWidthPixels() {
		return widthPixels;
	}
	public void setWidthPixels(int widthPixels) {
		this.widthPixels = widthPixels;
	}
	public int getHeightPixels() {
		return heightPixels;
	}
	public void setHeightPixels(int heightPixels) {
		this.heightPixels = heightPixels;
	}
	public int getHeightWucha() {
		return heightWucha;
	}
	public void setHeightWucha(int heightWucha) {
		this.heightWucha = heightWucha;
	}
	public int getPadding() {
		return padding;
	}
	public void setPadding(int padding) {
		this.padding = padding;
	}
	public int getWidthWucha() {
		return widthWucha;
	}
	public void setWidthWucha(int widthWucha) {
		this.widthWucha = widthWucha;
	}
	public int getTextSize() {
		return textSize;
	}
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	public int getLargeScreenEveryItem() {
		return largeScreenEveryItem;
	}

	public void setLargeScreenEveryItem(int largeEveryItem) {
		this.largeScreenEveryItem = largeEveryItem;
	}

	public int getLargeScreenTitleHeight() {
		return largeScreenTitleHeight;
	}

	public void setLargeScreenTitleHeight(int largeTitleHeight) {
		this.largeScreenTitleHeight = largeTitleHeight;
	}

	public int getLargeScreenNavigationHeight() {
		return largeScreenNavigationHeight;
	}

	public void setLargeScreenNavigationHeight(int largeNavigationHeight) {
		this.largeScreenNavigationHeight = largeNavigationHeight;
	}

	public int getNavigationHeight() {
		return navigationHeight;
	}

	public void setNavigationHeight(int navigationHeight) {
		this.navigationHeight = navigationHeight;
	}

	public int getSmallScreenEveryItem() {
		return smallScreenEveryItem;
	}

	public void setSmallScreenEveryItem(int sixScreenEveryItem) {
		this.smallScreenEveryItem = sixScreenEveryItem;
	}

	public int getSmallScreenTitleHeight() {
		return smallScreenTitleHeight;
	}

	public void setSmallScreenTitleHeight(int sixScreenTitleHeight) {
		this.smallScreenTitleHeight = sixScreenTitleHeight;
	}

	public int getSmallScreenNavigationHeight() {
		return smallScreenNavigationHeight;
	}

	public void setSmallScreenNavigationHeight(int sixScreenNavigationHeight) {
		this.smallScreenNavigationHeight = sixScreenNavigationHeight;
	}

	public int getSmallScreenTextSize() {
		return smallScreenTextSize;
	}

	public void setSmallScreenTextSize(int sixScreenTextSize) {
		this.smallScreenTextSize = sixScreenTextSize;
	}

	public int getSmallWucha() {
		return smallWucha;
	}

	public void setSmallWucha(int sixWucha) {
		this.smallWucha = sixWucha;
	}

	public int getLargeTextSize() {
		return largeScreenTextSize;
	}

	public void setLargeTextSize(int largeTextSize) {
		this.largeScreenTextSize = largeTextSize;
	}

	public int getDATAS_ALL_SIZE() {
		return DATAS_ALL_SIZE;
	}

	public void setDATAS_ALL_SIZE(int dATAS_ALL_SIZE) {
		DATAS_ALL_SIZE = dATAS_ALL_SIZE;
	}
	
	
}
