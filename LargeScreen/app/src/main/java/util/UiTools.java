package util;

public class UiTools {
	
	public static final boolean DEBUG = true;//是否启用debug模式,如:server中显示内网15等
	public static final boolean DEBUG_BySelf = false;	//发布时需置false
	
	public static long TASK_GOINGTOWAREHOUSE_TIME = 0;
	public static long TASK_REMAINJOBS_TIME = 0;
	public static long TASK_WORKINGONWAREHOUSE_TIME = 0;

	public static boolean isFastClickForTask(int flag) {
		long time = System.currentTimeMillis();
		long timeD;
		if(flag==ScreenMonitorTools.TASK_GOINGTOWAREHOUSE_FLAG){
			timeD = time - TASK_GOINGTOWAREHOUSE_TIME;
		}else if(flag==ScreenMonitorTools.TASK_REMAINJOBS_FLAG){
			timeD = time - TASK_REMAINJOBS_TIME;
		}else{
			timeD = time - TASK_WORKINGONWAREHOUSE_TIME;
		}
		if (timeD > ScreenMonitorTools.SCREEN_EXPIRATION_TIME) {
			if(flag==ScreenMonitorTools.TASK_GOINGTOWAREHOUSE_FLAG){
				TASK_GOINGTOWAREHOUSE_TIME = time;
			}else if(flag==ScreenMonitorTools.TASK_REMAINJOBS_FLAG){
				TASK_REMAINJOBS_TIME = time;
			}else{
				TASK_WORKINGONWAREHOUSE_TIME = time;
			}
			return true;
		}
		return false;
	}
	
	
	public static long SPOT_TIME = 0;
	public static long DOOR_TIME = 0;
	public static long DOCKCLOSE_TIME = 0;
	public static long PARKING_TIME = 0;
	
	public static boolean isFastClickForFacility(int flag) {
		long time = System.currentTimeMillis();
		long timeD;
		if(flag==ScreenMonitorTools.FACILITY_SPOT_FLAG){
			timeD = time - SPOT_TIME;
		}else if(flag==ScreenMonitorTools.FACILITY_DOOR_FLAG){
			timeD = time - DOOR_TIME;
		}else if(flag==ScreenMonitorTools.FACILITY_DOCK_FLAG){
			timeD = time - DOCKCLOSE_TIME;
		}else{
			timeD = time - PARKING_TIME;
		}
		if (timeD > ScreenMonitorTools.SCREEN_EXPIRATION_TIME) {
			if(flag==ScreenMonitorTools.FACILITY_SPOT_FLAG){
				SPOT_TIME = time;
			}else if(flag==ScreenMonitorTools.FACILITY_DOOR_FLAG){
				DOOR_TIME = time;
			}else if(flag==ScreenMonitorTools.FACILITY_DOCK_FLAG){
				DOCKCLOSE_TIME = time;
			}else{
				PARKING_TIME = time;
			}
			return true;
		}
		return false;
	}
	
	public static long WINDOW_TIME = 0;
	public static long WAITING_TIME = 0;
	public static long FORGET_TIME = 0;
	public static long FORGETCHECKOUT_TIME = 0;
	
	public static boolean isFastClickForWindow(int flag) {
		long time = System.currentTimeMillis();
		long timeD;
		if(flag==ScreenMonitorTools.WINDOW_W_FLAG){
			timeD = time - WINDOW_TIME;
		}else if(flag==ScreenMonitorTools.DATA4_FLAG){
			timeD = time - WAITING_TIME;
		}else if(flag==ScreenMonitorTools.DATA2_FLAG){
			timeD = time - FORGET_TIME;
		}else{
			timeD = time - FORGETCHECKOUT_TIME;
		}
		if (timeD > ScreenMonitorTools.SCREEN_EXPIRATION_TIME) {
			if(flag==ScreenMonitorTools.WINDOW_W_FLAG){
				WINDOW_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA4_FLAG){
				WAITING_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA2_FLAG){
				FORGET_TIME = time;
			}else{
				FORGETCHECKOUT_TIME = time;
			}
			return true;
		}
		return false;
	}
	
	public static long PRINTER_TIME = 0;
	public static long PERSONNEL_TIME = 0;
	
	public static boolean isFastClickForResource(int flag) {
		long time = System.currentTimeMillis();
		long timeD;
		if(flag==ScreenMonitorTools.RESOURCE_PRINTER_FLAG){
			timeD = time - PRINTER_TIME;
		}else{
			timeD = time - PERSONNEL_TIME;
		}
		if (timeD > ScreenMonitorTools.SCREEN_EXPIRATION_TIME) {
			if(flag==ScreenMonitorTools.RESOURCE_PRINTER_FLAG){
				PRINTER_TIME = time;
			}else{
				PERSONNEL_TIME = time;
			}
			return true;
		}
		return false;
	}
	
	
	
	public static long DATA5_TIME = 0;
	public static long DATA4_TIME = 0;
	public static long DATA2_TIME = 0;
	public static long DATA6_TIME = 0;
	public static long DATA3_TIME = 0;
	public static long DATA1_TIME = 0;
	
	public static boolean isFastClickForData(int flag) {
		long time = System.currentTimeMillis();
		long timeD;
		if(flag==ScreenMonitorTools.DATA5_FLAG){
			timeD = time - DATA5_TIME;
		}else if(flag==ScreenMonitorTools.DATA4_FLAG){
			timeD = time - DATA4_TIME;
		}else if(flag==ScreenMonitorTools.DATA2_FLAG){
			timeD = time - DATA2_TIME;
		}else if(flag==ScreenMonitorTools.DATA6_FLAG){
			timeD = time - DATA6_TIME;
		}else if(flag==ScreenMonitorTools.DATA3_FLAG){
			timeD = time - DATA3_TIME;
		}else{
			timeD = time - DATA1_TIME;
		}
		if (timeD > ScreenMonitorTools.SCREEN_EXPIRATION_TIME) {
			if(flag==ScreenMonitorTools.DATA5_FLAG){
				DATA5_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA4_FLAG){
				DATA4_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA2_FLAG){
				DATA2_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA6_FLAG){
				DATA6_TIME = time;
			}else if(flag==ScreenMonitorTools.DATA3_FLAG){
				DATA3_TIME = time;
			}else{
				DATA1_TIME = time;
			}
			return true;
		}
		return false;
	}
}
