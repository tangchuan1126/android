package util;

import java.lang.reflect.Field;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utility {

	//-------------ac操作------------------------
	
	// 弹至某activity,如:主页
	public static void popTo(Activity context, Class<?> main) {
		Intent intent = new Intent(context, main);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	//----------文本处理-------------------------------
	
	public static int getMaxLen_ofString(float textSize, String[] strs) {
		return getMaxLen_ofString(textSize, strs, null);
	}
	
	/**
	 * 获取-最长str的长度
	 * 
	 * @param textSize,文字尺寸(px)
	 * @param strs
	 * @param addition,附加字符
	 * @return
	 */
	public static int getMaxLen_ofString(float textSize, String[] strs,String addition) {
		if (strs == null)
			return 0;
		
		if(addition==null)
			addition="";
		
		Paint pt = new Paint();
		pt.setTextSize(textSize);
		float maxLen = 0;
		for (int i = 0; i < strs.length; i++) {
			float tmpLen = (float) pt.measureText(strs[i]+addition);
			if (tmpLen > maxLen)
				maxLen = tmpLen;
		}
		return (int)(maxLen+5f);
	}
	
	public static float getTextLen(float textSize,String str){
		Paint pt = new Paint();
		pt.setTextSize(textSize);
		return (float) pt.measureText(str);
	}
	
	
	//-------------------------------------------

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()������������Ŀ
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // ��������View �Ŀ��
			totalHeight += listItem.getMeasuredHeight(); // ͳ������������ܸ߶�
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static boolean isConnectNet(Context context) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected()) {
					if (info.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		} catch (Exception e) {

		}
		return false;
	}

	public static boolean isSDExits() {
		boolean sdExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
		return sdExist;
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, View edit) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, EditText edit) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		// edit.setCursorVisible(false);//失去光标
		edit.requestFocus();
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * @Description:获取版本号
	 * @param @param context
	 * @param @return
	 */
	public static String getVersionName(Context context) {
		String version = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			android.content.pm.PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			version = packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}

	public static int getStatusBarHeight(Context context) {
		Class<?> c = null;
		Object obj = null;
		Field field = null;
		int x = 0, statusBarHeight = 0;
		try {
			c = Class.forName("com.android.internal.R$dimen");
			obj = c.newInstance();
			field = c.getField("status_bar_height");
			x = Integer.parseInt(field.get(obj).toString());
			statusBarHeight = context.getResources().getDimensionPixelSize(x);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return statusBarHeight;
	}
	/**
	 * @Description:判断List是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 * @param <E>
	 */
	public static <E> boolean isNullForList(List<E> list){
		boolean flag = true;
		if(list!=null&&list.size()>0){
			flag = false;
		}
		return flag;
	}
	
	//获取登陆后的adid
	public static String getSharedPreferencesUrlValue(Activity context,String key) {
		String adid = "";
		if(!StringUtil.isNullOfStr(key)){
			SharedPreferences sharedPreferences = context.getSharedPreferences("loginSave", Context.MODE_PRIVATE);
			adid = ("&"+key+"="+sharedPreferences.getString(key, ""));
		}
		return adid;
	}
	
}
