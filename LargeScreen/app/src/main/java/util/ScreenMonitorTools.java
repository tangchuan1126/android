package util;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.android.activity.platform.model.ScreenModeBean;
/**
 * @ClassName: ScreenMonitorTools 
 * @Description: 大屏幕工具类  包括屏幕轮换时间 以及通用尺寸
 * @author gcy
 * @date 2014-9-24 下午3:47:22
 */
public class ScreenMonitorTools {
	

	public static final int SCREEN_EXPIRATION_TIME = 15*1000;//循环线程监视线程失效的过期时间
	
	//-------------------------------------------
	//
	//----------TaskMonitorActivity
	//
	//-------------------------------------------
	
//	goingToWareHouse
//	remainJobs
//	workingOnWarehouse
	
	/*************************屏幕循环请求的时间*************************/
	public static final int TASK_GOINGTOWAREHOUSE_RUN_TIME = 9*1000;
	public static final int TASK_REMAINJOBS_RUN_TIME = 9*1000;
	public static final int TASK_WORKINGONWAREHOUSE_RUN_TIME = 10*1000;
	
	/*************************屏幕的标识******************************/
	public static final int TASK_GOINGTOWAREHOUSE_FLAG = 1;
	public static final int TASK_REMAINJOBS_FLAG = 2;
	public static final int TASK_WORKINGONWAREHOUSE_FLAG = 3;
	
	/*************************每个屏幕请求的条数*************************/
	public static final int TASK_GOINGTOWAREHOUSE_SIZE = 10;
	public static final int TASK_REMAINJOBS_SIZE = 10;
	public static final int TASK_WORKINGONWAREHOUSE_SIZE = 10;
	
	public static final int TASK_TITLENUM = 1+1+1;//标题栏  + 第一个屏幕的标题栏 + 第二个屏幕的标题栏
	public static final int TASK_BIG_SCREEN_SIZE_NUM = (TASK_GOINGTOWAREHOUSE_SIZE+1)+TASK_WORKINGONWAREHOUSE_SIZE+1;//每个屏幕所请求的条数(含第一个大屏幕的页脚 为1条数据) + 标题栏
	
	public static final int itemNum = TASK_GOINGTOWAREHOUSE_SIZE+1+TASK_WORKINGONWAREHOUSE_SIZE+1+TASK_TITLENUM;//因为第一个屏幕与第二个屏幕为一行 所以只计算第一个与第三个的
	
	
	

	//-------------------------------------------
	//
	//----------FacilityMonitor
	//
	//-------------------------------------------
		
	/*************************屏幕循环请求的时间*************************/
	public static final int FACILITY_SPOT_RUN_TIME = 9*1000;
	public static final int FACILITY_DOOR_RUN_TIME =9*1000;	
	public static final int FACILITY_DOCKCLOSE_RUN_TIME = 10*1000;
	public static final int FACILITY_PARKING_TIME = 10*1000;
	/*************************屏幕的标识******************************/
	public static final int FACILITY_SPOT_FLAG = 1;
	public static final int FACILITY_DOOR_FLAG = 2;
	public static final int FACILITY_DOCK_FLAG = 3;
	public static final int FACILITY_PARKING_FLAG = 4;
	
	public static final int FACILITY_DOOR_TASK_RUN_TIME = 2500; //门的task轮换时间
	public static final int FACILITY_DOOR_TASK_RUN_NUM = FACILITY_DOOR_RUN_TIME/FACILITY_DOOR_TASK_RUN_TIME;

	
	public static final int FACILITY_SIZE = TASK_BIG_SCREEN_SIZE_NUM;//全屏幕所请求的条数 + 标题栏 
	public static final int FACILITY_SPOT_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int FACILITY_DOOR_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int FACILITY_DOCK_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int FACILITY_PARKING_SIZE = 10;
	
	
	
	//-------------------------------------------
	//
	//----------WindowMonitor
	//
	//-------------------------------------------
		
	/*************************屏幕循环请求的时间*************************/
	public static final int WINDOW_W_RUN_TIME = 9*1000;
	public static final int WINDOW_WAITING_TIME = 9*1000;
	public static final int WINDOW_FORGET_RUN_TIME = 10*1000;
	public static final int WINDOW_FORGETCHECKOUT_TIME = 10*1000;
	/*************************屏幕的标识******************************/
	public static final int WINDOW_W_FLAG = 1;
	public static final int WINDOW_WAITING_FLAG = 2;
	public static final int WINDOW_FORGET_FLAG = 3;
	public static final int WINDOW_FORGETCHECKOUT_FLAG = 4;
		
	public static final int WINDOW_SIZE = TASK_BIG_SCREEN_SIZE_NUM;//全屏幕所请求的条数 + 标题栏 
	public static final int WINDOW_W_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int WINDOW_WAITING_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int WINDOW_FORGET_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int WINDOW_FORGETCHECKOUT_SIZE = 10;
	

	//-------------------------------------------
	//
	//----------ResourceMonitor
	//
	//-------------------------------------------
		
	/*************************屏幕循环请求的时间*************************/
	public static final int RESOURCE_PRINTER_RUN_TIME = 9*1000;
	public static final int RESOURCE_PERSONAL_RUN_TIME = 9*1000;
	/*************************屏幕的标识******************************/
	public static final int RESOURCE_PRINTER_FLAG = 1;
	public static final int RESOURCE_PERSONAL_FLAG = 2;
	
	/*************************每个屏幕请求的条数*************************/
	public static final int RESOURCE_SIZE = TASK_BIG_SCREEN_SIZE_NUM;//全屏幕所请求的条数 + 标题栏 
	public static final int RESOURCE_PRINTER_SIZE = 22;//每个屏幕所请求的条数 + 标题栏 
	public static final int RESOURCE_PERSONNEL_SIZE = 0;//每个屏幕所请求的条数 + 标题栏 
	
	
	
	//-------------------------------------------
	//
	//----------WindowMonitor
	//
	//-------------------------------------------
			
	/*************************屏幕循环请求的时间*************************/
	public static final int DATA5_RUN_TIME = 9*1000;
	public static final int DATA4_TIME = 9*1000;
	public static final int DATA2_RUN_TIME = 10*1000;
	public static final int DATA6_RUN_TIME = 10*1000;
	public static final int DATA3_RUN_TIME = 10*1000;
	public static final int DATA1_RUN_TIME = 10*1000;
	/*************************屏幕的标识******************************/
	public static final int DATA5_FLAG = 1;
	public static final int DATA4_FLAG = 2;
	public static final int DATA2_FLAG = 3;
	public static final int DATA6_FLAG = 4;
	public static final int DATA3_FLAG = 4;
	public static final int DATA1_FLAG = 4;
		
	public static final int DATAS_SIZE = TASK_BIG_SCREEN_SIZE_NUM;//全屏幕所请求的条数 + 标题栏 
	public static final int DATA5_SIZE = 5;//每个屏幕所请求的条数 + 标题栏 
	public static final int DATA4_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int DATA2_SIZE = 10;//每个屏幕所请求的条数 + 标题栏 
	public static final int DATA6_SIZE = 5;
	public static final int DATA3_SIZE = 10;
	public static final int DATA1_SIZE = 10;
	
	public static final int DATAS_ALL_SIZE = DATA1_SIZE+2+DATA3_SIZE+2+DATA5_SIZE+2;//全屏幕所请求的条数 + 标题栏 
		
	
	public static DisplayMetrics getDisplayMetrics(Activity activity){
		DisplayMetrics  dm = new DisplayMetrics();    
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm;
	}
//	/**
//	 * @Description:获取屏幕的宽
//	 * @param @param activity
//	 * @param @return
//	 */
//	public static int getWidth(Activity activity){
//		return getDisplayMetrics(activity).widthPixels;
//	}
//	/**
//	 * @Description:获取屏幕的高
//	 * @param @param activity
//	 * @param @return
//	 */
//	public static int getHeight(Activity activity){
//		return getDisplayMetrics(activity).heightPixels;
//	}
	/**
	 * @Description:获取每一行的高度
	 * @param @param activity
	 * @param @return
	 */
	public static int getEveryItem(int screenHeight){
		return screenHeight/itemNum;
	}
	/**
	 * @Description:获取每一行高度精算后的误差尺寸
	 * @param @param activity
	 * @param @return
	 */
	public static int getHeightWucha(int screenHeight){
		return screenHeight%itemNum;
	}
	/**
	 * @Description:获取padding值
	 * @param @param activity
	 * @param @return
	 */
	public static int getPadding(Activity activity){
		return activity.getResources().getDimensionPixelSize(R.dimen.actionbar_vertival_padding);
	}
	
	/**
	 * @Description:获取每一行宽度精算后的误差尺寸
	 * @param @param activity
	 * @param @return
	 */
	public static int getWidthWucha(int widthPixels){
		return widthPixels%2;
	}
	
	/**
	 * @Description:获取text文本的大小
	 * @param @param activity
	 * @param @return
	 */
	public static int getTextSize(Activity activity,int everyItem){
		return DisplayUtil.px2sp(activity,everyItem/2);
	}
	
	/**
	 * @Description:获取时间00:00
	 * @param @param timeValue,分钟
	 * @param @return
	 */
	public static String fixTime(int timeValue){
		
		if(timeValue<0)
//			return timeValue+"";//String.format("%1$7s", "0m");//timeValue+"";
			return "--";
		int timeCount = timeValue;
		int hour = timeCount/60;
		int min = timeCount%60;

		String hourStr = (hour==0?"":(String.valueOf(hour+"h")));
		String minStr = (min==0?"0m":String.valueOf(min+"m"));
		if(timeValue>(60*24)){
			return  String.format("%1$7s", "> 24h");
		}else{
			return  String.format("%1$3s", hourStr)+String.format("%1$4s", minStr);
		}
	}
	
	/**
	 * @Description:获取时间00:00
	 * @param @param timeValue,分钟
	 * @param @return
	 */
	public static String fixTimeReturnAllTime(int timeValue){
		
		if(timeValue<0)
			return timeValue+"";//String.format("%1$7s", "0m");//timeValue+"";
		
		int timeCount = timeValue;
		int hour = timeCount/60;
		int min = timeCount%60;

		String hourStr = (hour==0?"":(String.valueOf(hour+"h")));
		String minStr = (min==0?"0m":String.valueOf(min+"m"));
//		if(timeValue>(60*24)){
//			return  String.format("%1$7s", "> 24h");
//		}else{
			return  String.format("%1$3s", hourStr)+String.format("%1$4s", minStr);
//		}
	}
	
//	/**
//	 * @Description:获取时间00:00
//	 * @param @param timeValue
//	 * @param @return
//	 */
//	public static String getFixTimeHour(int timeValue){
//		int timeCount = timeValue;
//		int hour = timeCount/60;
//		return (hour==0?"":String.valueOf(hour+"hr"));
//	}
//	
//	/**
//	 * @Description:获取时间00:00
//	 * @param @param timeValue
//	 * @param @return
//	 */
//	public static String getFixTimeMin(int timeValue){
//		int timeCount = timeValue;
//		int min = timeCount%60;
//		return " "+String.format("%1$5s", (min<10?(""+min+"min"):String.valueOf(min+"min")));
//	}
	
	/**
	 * @Description:判断时间是否超过指定的小时
	 * @param @param timeValue
	 * @param @return
	 */
	public static boolean judgeTime(int timeValue,float setTime){
		int timeCount = timeValue;
//		int hour = timeCount/60;
		return timeCount<=((float)setTime*60);
	}
	
	public static String fixNumberType(String type){
    	String spaceStr = "";
	    if(type.length()<6){
	    	for(int i=0;i<=(6-type.length());i++){
	    		spaceStr+=" ";
	    	}
	    }
		return spaceStr+type;
	}
	
	/**
	 * @Description 解析JSON数据 获取总页码
	 * @return
	 */
	public static int getTotal(JSONObject json){
		return json.optInt("total");
	}
	
	/**
	 * @Description 解析JSON数据 获取总条数
	 * @return
	 */
	public static int getTotal_Num(JSONObject json){
		return json.optInt("total_num");
	}
	
	@SuppressLint("NewApi")
	public static boolean keyToolsForGirdView(int keyCode, KeyEvent event,BaseAdapter adapter,GridView gridView){
		int selectedItem = gridView.getSelectedItemPosition();
		int numColumns = gridView.getNumColumns();
		int count = adapter.getCount();
		int leftNextPosition = (selectedItem-1)<0?(count-1):(selectedItem-1);
		int rightNextPosition = (selectedItem+1)>=count?0:(selectedItem+1);
		int topNextPosition = 0;
		int bottomNextPosition = 0;
		
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
				gridView.setSelection(rightNextPosition);
				return true;
			}
			
			if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
				gridView.setSelection(leftNextPosition);
				return true;
			}
			
			
			if((selectedItem+1)== count){
				if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
					gridView.setSelection(0);
					return true;
				}
			}
			if((selectedItem+1) != count&&count>numColumns&&selectedItem>=((count/numColumns)*numColumns)){
				if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
					bottomNextPosition = selectedItem - (count/numColumns)*numColumns ;
					gridView.setSelection(bottomNextPosition);
					return true;
				}
			}
			
			if(gridView.getSelectedItemPosition()==0){
				if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
					gridView.setSelection(adapter.getCount()-1);
					return true;
				}
			}
			if(count>numColumns&&selectedItem<numColumns){
				if(count!=0&&count%numColumns==0){
					topNextPosition = (count/numColumns-1)*numColumns + selectedItem;
				}else{
					topNextPosition = ((selectedItem>(count%numColumns-1))?(count-1):((count/numColumns-1)*numColumns + selectedItem));
				}
				if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
					gridView.setSelection(topNextPosition);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @Description:计算获取大屏幕数据的第几页 
	 * @param @param beforeTheChangeSize 变化前每屏请求的数据量 例ScreenMonitorTools.FACILITY_SPOT_SIZE
	 * @param @param beforeTheChangePageNo 变化前屏幕当前的页码数
	 * @param @param beforeTheChangeTotal 变化前屏幕的总页码数
	 * @param @param afterTheChangeSize 变化后屏幕的总页码数
	 * @param @return
	 */
    public static int getPageNo(int beforeTheChangeSize,int beforeTheChangePageNo,int beforeTheChangeTotal,int afterTheChangeSize){
    	int downSmallCountData = beforeTheChangePageNo*beforeTheChangeSize;//计算已经下载下来的数据总条数范围
		boolean smallPageFlag = downSmallCountData % afterTheChangeSize == 0;//判断当前下载下来的数据对比大屏幕的尺寸后是否能被整除 如果能被整除则返回true
		int page = downSmallCountData/afterTheChangeSize;//由于int计算出来的数据并不准确 所以需要根据是否能被整除来判断 准确的数值
		page = smallPageFlag?page:(page+1);
		
		int serverData = beforeTheChangeTotal*beforeTheChangeSize;//求出服务器上的数据总数范围
		boolean serverDataFlag = serverData%afterTheChangeSize==0;//判断服务器上的数据总数对比大屏幕的尺寸后是否能被整除 如果能被整除则返回true
		int serverPageCount = beforeTheChangeTotal*beforeTheChangeSize/afterTheChangeSize;
		serverPageCount = serverDataFlag?serverPageCount:(serverPageCount+1);
    	return (page>serverPageCount)?1:page;
    }
    
    /**
     * @Description:由于每次请求后都会在原有请求的页码上自动加1  所以获取当前的页码并不准确 需要通过该方法获取真是的页码
     * @param @param pageNo
     * @param @param total
     * @param @return
     */
    public static int getRealPage(int pageNo,int total){
    	int pageNum = pageNo-1;
    	if((pageNo-1)<1||pageNo>total){
    		pageNum = 1;
    	}
    	return pageNum;
    }
    
    
    /**
     * @Description:用于判断返回的TaskNum数量
     * @param @param pageNo
     * @param @param total
     * @param @return
     */
    public static String getTaskNum(String tasks){
    	int taskNum = 0;
    	if(!StringUtil.isNullOfStr(tasks)){
    		String strs[] = tasks.split(",");
    		taskNum = strs.length;
    	}
    	return String.valueOf(taskNum);
    }
    
    public static int getSplitNum(String tasks){
    	int taskNum = 0;
    	if(!StringUtil.isNullOfStr(tasks)){
    		String strs[] = tasks.split(",");
    		taskNum = strs.length;
    	}
    	return taskNum;
    }
    
//    public static String getCtn_complex(String gate_container_no,String ctn_number){
//		String ret="";
//		ret=TextUtils.isEmpty(gate_container_no)?"NA":gate_container_no;
//		ret+=" | ";
//		
//		String append;
//		int ctnNum=ScreenMonitorTools.getSplitNum(ctn_number);
//		if(ctnNum==0)
//			append="NA";
//		else if(ctnNum==1)
//			append=ctn_number;
//		else
//			append=ScreenMonitorTools.getTaskStr(ctn_number)+"..."+ctnNum;
//		return ret+append;
//	}
    
    public static String getCtn_complex(String ctn_number){
		String append;
		int ctnNum=ScreenMonitorTools.getSplitNum(ctn_number);
		if(ctnNum==0)
			append="NA";
		else if(ctnNum==1)
			append=ctn_number;
		else
			append=ScreenMonitorTools.getTaskStr(ctn_number)+"..."+ctnNum;
		return append;
	}
    
    /**
     * @Description:用于返回的Task的第一个字符
     * @param @param pageNo
     * @param @param total
     * @param @return
     */
    public static String getTaskStr(String tasks){
    	String taskStr = "";
    	if(!StringUtil.isNullOfStr(tasks)){
    		String strs[] = tasks.split(",");
    		if(strs.length>0){
    			taskStr = strs[0];
    		}
    	}
    	return taskStr;
    }
    
    
    private static double multiple = 0.95;
    
    public static int getChangeTheSizeForTitle(int everyItem){
    	return (int)(everyItem*multiple);
    }
    
    public static int getChangeTheSizeForBottom(int everyItem){
    	return (int)(2*everyItem-everyItem*multiple);
    }
    
    
    public static int getSmallLayoutHeight(ScreenModeBean screenMode,int size){
    	return screenMode.getSmallEveryItem()*size+2*screenMode.getPadding()+screenMode.getSmallNavigationHeight()+screenMode.getSmallTitleHeight();
    }
    
    public static int getSixLayoutHeight(ScreenModeBean screenMode,int size){
    	return screenMode.getSixScreenEveryItem()*size+2*screenMode.getPadding()+screenMode.getSixScreenNavigationHeight()+screenMode.getSixScreenTitleHeight();
    }
    
    public static int getSixLayoutHeight(TheInitialScreenData screenMode,int size){
    	return screenMode.getSmallScreenEveryItem()*size+2*screenMode.getPadding()+screenMode.getSmallScreenNavigationHeight()+screenMode.getSmallScreenTitleHeight();
    }
    
    public static int getTitleSize(int size){
    	return size*7/8;
    }
}
