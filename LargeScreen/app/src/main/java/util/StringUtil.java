package util;

import java.security.MessageDigest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.loopj.android.http.RequestParams;

@SuppressLint("SimpleDateFormat")
public class StringUtil {


	private static final String NULL = "NULL";

	private static ForegroundColorSpan span = new ForegroundColorSpan(Color.RED);

	public static String HashBase64(String str) {
		String ret = "";
		try {
			// Hash算法
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(str.getBytes());
			ret = Base64.encodeToString(sha.digest(), Base64.NO_WRAP);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return ret;
	}

	/**
	 * 检查浮点数
	 * 
	 * @param num
	 * @param type
	 *            "0+":非负浮点数 "+":正浮点数 "-0":非正浮点数 "-":负浮点数 "":浮点数
	 * @return
	 */
	public static boolean checkFloat(String num, String type) {
		String eL = "";
		if (type.equals("0+"))
			eL = "^\\d+(\\.\\d+)?$";// 非负浮点数
		else if (type.equals("+"))
			eL = "^((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*))$";// 正浮点数
		else if (type.equals("-0"))
			eL = "^((-\\d+(\\.\\d+)?)|(0+(\\.0+)?))$";// 非正浮点数
		else if (type.equals("-"))
			eL = "^(-((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*)))$";// 负浮点数
		else
			eL = "^(-?\\d+)(\\.\\d+)?$";// 浮点数
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(num);
		boolean b = m.matches();
		return b;
	}

	public static SpannableStringBuilder getSpannable(String value) {
		SpannableStringBuilder spannable = null;
		if (null != value) {
			spannable = new SpannableStringBuilder(value);
			spannable.setSpan(span, 0, value.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			spannable = new SpannableStringBuilder();
		}
		return spannable;

	}

	public static String returnStringValue(String value) {
		if (value != null && !value.trim().toUpperCase().equals("NULL")) {
			return value;
		}
		return "";
	}



	public static double convert2Double(String value) {
		double result = 0.0d;
		if (value != null && value.trim().length() > 0) {
			try {
				result = Double.parseDouble(value);
			} catch (Exception e) {
			}
		}
		return result;
	}

	public static long convert2Long(String value) {
		long returnValue = 0l;
		if (value != null && value.trim().length() > 0) {
			try {
				returnValue = Long.parseLong(value.trim());
			} catch (Exception e) {
			}
		}
		return returnValue;
	}

	public static boolean isNullOfLong(String value) {
		long l = 0l;
		if (value != null && value.trim().length() > 0) {
			try {
				l = Long.parseLong(value);
			} catch (Exception e) {
				l = 0l;
			}
		}
		return l == 0l;
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		// 获取ListView对应的Adapter
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);
	}

	public static double convertToV(double hight, double width, double length) {
		return hight * width * length / 1000;
	}

	public static int convert2Inter(String value) {
		return (int) convert2Long(value);
	}

	public static String getUpFilePictureFileName(String fileName) {
		if (fileName != null && fileName.indexOf(".") != -1) {
			return fileName.substring(0, fileName.indexOf(".")) + "_up.jpg";

		}
		return fileName;
	}

	public static boolean isNullOfStr(String value) {
		return !(value != null && value.trim().length() > 0 && !NULL.equals(value.toUpperCase()));
	}

	/**
	 * @Description:判断JSONArray是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 */
	public static boolean isNullForJSONArray(JSONArray array){
		boolean flag = true;
		if(array!=null&&array.length()>0){
			flag = false;
		}
		return flag;
	}
	
	
	
	/**
	 * 验证ip是否合法
	 * 
	 * @param textip地址
	 * @return 验证信息
	 */
	public static boolean ipCheck(String text) {
		if (text != null && !TextUtils.isEmpty(text)) {
			// 定义正则表达式
			String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
					+ "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
			// 判断ip地址是否与正则表达式匹配
			if (text.matches(regex)) {
				// 返回判断信息
				return true;
			} else {
				// 返回判断信息
				return false;
			}
		}
		// 返回判断信息
		return false;
	}


	/**
	 * 验证端口是否合法
	 * 
	 * @param 端口
	 * @return 验证信息
	 */
	public static boolean portCheck(String text) {
		if (text != null && !TextUtils.isEmpty(text)) {
			// 定义正则表达式
			String regex = "^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-5][0-5][0-3][0-5]$)";
			// 判断ip地址是否与正则表达式匹配
			return Pattern.matches(regex, text);
		}
		// 返回判断信息
		return false;
	}
	
	public static void setLoginInfoParams(RequestParams params) {
		params.put("LoginAccount", Goable.LoginAccount);
		params.put("Password", Goable.Password);
		params.put("Machine", Goable.Machine);
		params.put("Version", Goable.Version);
	}
	
//	/**
//	 * 根据返回当前服务器的本地时间 以及起始时间来运算总共用了多少分钟
//	 * @param localTime
//	 * @param startTime
//	 * @return
//	 */
//	public static int getDateTime(String startTime){
////		2014-11-16 19:44:50
//		int timevalue = 0; 
//		try {
//			SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss"); 
//			Date localDate = new Date();
//			Date startDate = df.parse(startTime);
////			Date localDate = df.parse(localTime);
//			timevalue = (int) ((localDate.getTime()-startDate.getTime()))/60/1000;
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}  
//		return timevalue;
//	}
}
