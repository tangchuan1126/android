package ui;

import oso.com.vvmescreen.R;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

public class AnimationTools {
	
//	float fromX 动画起始时 X坐标上的伸缩尺寸 
//	float toX 动画结束时 X坐标上的伸缩尺寸 
//	float fromY 动画起始时Y坐标上的伸缩尺寸 
//	float toY 动画结束时Y坐标上的伸缩尺寸 
//	int pivotXType 动画在X轴相对于物件位置类型 
//	float pivotXValue 动画相对于物件的X坐标的开始位置 
//	int pivotYType 动画在Y轴相对于物件位置类型 
//	float pivotYValue 动画相对于物件的Y坐标的开始位置 
	
	public static Animation getBiggerAnimation(){
		/** 设置缩放动画 */
		ScaleAnimation animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f, Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f); //左上
		animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f, Animation.RELATIVE_TO_SELF, 1.0f,Animation.RELATIVE_TO_SELF, 0.0f); //右上
		animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f); //右上

		animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f); //右上

		animation = new ScaleAnimation(1f, 1.1f, 1f, 1.1f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f); //右上
		animation.setDuration(100);//设置动画持续时间 
		animation.setFillAfter(true);
		return animation;
	}
	
	public static void onFocusAnimation(Context c, View v, boolean hasFocus) {
		if (v.getId() != R.id.layout  && hasFocus) {
			Animation anim = AnimationUtils.loadAnimation(c, R.anim.scale_big);
			anim.setFillAfter(true);
			v.setAnimation(anim);
			v.bringToFront();
		} else {
			if(v.getAnimation() != null)
				v.setAnimation(null);
		}
	}
	
}
