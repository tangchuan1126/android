package ui;

import oso.com.vvmescreen.R;
import util.StringUtil;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.common.ServerClockView;

/**
 * 给定一个数组显示里面图片. 数组为fileName
 * 
 * @author zhangrui
 * @date 2014年6月19日 下午5:21:02
 * @description
 */
public class ScreenTitleLayout extends RelativeLayout {

	public LinearLayout showPhotoView; // 存放图片的LinearLayout
	private Resources resources;
	private View logo_icon_first;
	private View logo_icon_second;
	private TextView server_name;
	private TextView warehouse_name;
	private ServerClockView clockview;

	public ScreenTitleLayout(Context context) {
		super(context);
		resources = context.getResources();
		init();

	}

	public ScreenTitleLayout(Context context, AttributeSet attrs) {// 属性的值是通过AttributeSet传递给它的，当用代码创建的时候，就不会了
		super(context, attrs);
		resources = context.getResources();
		init();
	}

	public void init() {
		String infService = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
		View view = li.inflate(R.layout.screen_title_layout, this, true);
		logo_icon_first = (View) view.findViewById(R.id.logo_icon_first);
		logo_icon_second = (View) view.findViewById(R.id.logo_icon_second);
		server_name = (TextView) view.findViewById(R.id.server_name);
		warehouse_name = (TextView) view.findViewById(R.id.warehouse_name);
		clockview = (ServerClockView) view.findViewById(R.id.clockview);
	}

	public void setView(int height, int size, String titleValue, String serverName, String warehouseId, String serverTimeUrl) {
		int h = height / 3 * 2;
		warehouse_name.setText(titleValue);
		warehouse_name.setTextSize(size);
		if (!StringUtil.isNullOfStr(serverName)) {
			server_name.setText(serverName);
			server_name.setTextSize(size);
			server_name.setPadding(size, 0, 0, 0);
		} else {
			server_name.setVisibility(View.GONE);
		}

		Drawable logo_new1 = resources.getDrawable(R.drawable.logo_new4);
		double width1 = h * logo_new1.getIntrinsicWidth() / logo_new1.getIntrinsicHeight();

		LayoutParams logo_icon_firsts = (LayoutParams) logo_icon_first.getLayoutParams();
		logo_icon_firsts.height = h;
		logo_icon_firsts.width = (int) width1;
		logo_icon_first.setLayoutParams(logo_icon_firsts);
		logo_icon_first.setBackgroundDrawable(logo_new1);

		Drawable logo_new2 = resources.getDrawable(R.drawable.logo_new3);
		double width2 = h * logo_new2.getIntrinsicWidth() / logo_new2.getIntrinsicHeight();

		LayoutParams logo_icon_seconds = (LayoutParams) logo_icon_second.getLayoutParams();
		logo_icon_seconds.height = h;
		logo_icon_seconds.width = (int) width2;
		logo_icon_second.setLayoutParams(logo_icon_seconds);
		logo_icon_second.setBackgroundDrawable(logo_new2);

		clockview.initClock((int) width1, h, size, warehouseId, serverTimeUrl);
	}

	public void setView(int height, int size, String titleValue, String serverName) {
		setView(height, size, titleValue, serverName, null, null);
	}

	public void setCallbacks(boolean doRemove) {
		clockview.setCallbacks(doRemove);
	}

}
