package oso.com.vvmescreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
/**
 * @ClassName: ExcessiveActivity 
 * @Description: 过度界面防止无限首页
 * @author gcy
 * @date 2014-9-25 下午5:08:17
 */
public class ExcessiveActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		Intent intent = new Intent(this, WelcomeActivity.class);
		startActivity(intent);
		finish();
	}
}
