package oso.com.vvmescreen;

import oso.ui.monitor.screen.tool.MonitorUtils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.android.activity.platform.ChooseServerListActivity;
import com.android.activity.platform.model.ScreenModeBean;

/**
 * 
 * @ClassName: Login
 * @Description:欢迎画面
 * @author gcy
 * @date 2014年5月20日 上午9:53:23
 * 
 */
public class WelcomeActivity extends Activity {

	private Handler mHandler = new Handler();
	private MonitorUtils monitorUtils ;	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		getFromActivityData();
		
	}

	public void getFromActivityData() {
		Intent intent = this.getIntent();
		monitorUtils = (MonitorUtils) intent.getSerializableExtra("monitorUtils");
		
		
		if(monitorUtils!=null){
			MonitorUtils.setJumpInfo(monitorUtils, this);
		}else{
			initView(); // 获取控件
		}
	}
	
	
	public void initView() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub			
				ImageView main_is = (ImageView) findViewById(R.id.main_is);
				ScreenModeBean screenMode = ScreenModeBean.getScreenMode(WelcomeActivity.this, main_is.getHeight(), main_is.getWidth());
				
				Intent intent = ScreenModeBean.setIntent(screenMode);
				intent.setClass(getApplicationContext(), ChooseServerListActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
			}
		}, 3000);
	}

}
