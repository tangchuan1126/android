package oso.com.vvmescreen;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import util.HttpUrlPath;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.widget.Toast;

public class DownloadApk {


	public static void loadApk(final Context context) {
		// 下载apk
		// 判断是否有网络
		if (!isConnectNet(context)) {
			Toast.makeText(context, " network errors ", Toast.LENGTH_LONG).show();
			return;
		}
		final ProgressDialog pd = new ProgressDialog(context);
		pd.setTitle("Hold On");
		pd.setMessage("Loading....");
		pd.setCancelable(false);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.show();
		new Thread(new Runnable() {

			public void run() {
				try {
					File file = getFileFromServer(context, HttpUrlPath.getSP(context)+HttpUrlPath.LoadApkVersion, pd);
					if (file != null) {
						Thread.sleep(1000);
						FileInputStream fileInputStream = context.openFileInput("update.apk");
						fileInputStream.close();
						initApk(context, file);
						pd.dismiss();
					}
				} catch (Exception e) {
					pd.dismiss();
					e.printStackTrace();
					Toast.makeText(context, "Load Failed", Toast.LENGTH_SHORT).show();
					((Activity) context).finish();
				}
			}
		}).start();
	}

	public static void initApk(Context context, File file) {
		Intent intent = new Intent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");// 编者按：此处Android应为android，否则造成安装不了
		context.startActivity(intent); 
//		android.os.Process.killProcess(0);
		System.exit(0);
	}

	public static File getFileFromServer(Context context, String path, ProgressDialog pd) throws Exception {

		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(15000);
		// 获取到文件的大小
		pd.setMax(conn.getContentLength());
		InputStream is = conn.getInputStream();
		FileOutputStream fos = context.openFileOutput("update.apk", Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
		BufferedInputStream bis = new BufferedInputStream(is);
		byte[] buffer = new byte[1024];
		int len;
		int total = 0;
		while ((len = bis.read(buffer)) != -1) {
			fos.write(buffer, 0, len);
			total += len;
			pd.setProgress(total);
		}
		fos.close();
		bis.close();
		is.close();
		return new File(context.getFilesDir(), "update.apk");

	}

	public static boolean isConnectNet(Context context) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected()) {
					if (info.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		} catch (Exception e) {

		}
		return false;
	}
}
