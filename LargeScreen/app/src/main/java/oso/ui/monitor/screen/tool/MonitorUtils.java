package oso.ui.monitor.screen.tool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import util.HttpUrlPath;
import util.TheInitialScreenData;
import util.Utility;
import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.android.activity.common.ScreenModeTypeBean;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.sceen.SyncLargeScreenActivity;
import com.android.core.ScreenDatas;


public class MonitorUtils implements Serializable{
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 978935748182979333L;
	public String basePath;
	public String baseName;
	public String ps_name;
	public String ps_id;
	public int heightPixels;
	public int widthPixels;
	public String screenStr;
	
	public static void setJumpInfo(MonitorUtils m ,Activity activity ){
		HttpUrlPath.setSP(m.basePath, activity);
		
		ScreenModeBean screenMode = ScreenModeBean.getScreenMode(activity, m.heightPixels, m.widthPixels);
		
		ServerTypeBean serverData = new ServerTypeBean();
		serverData.setServer_name(m.baseName);
		serverData.setServer_url(m.basePath);
		
		ScreenModeTypeBean bean = getScreen(m.screenStr);
		
		if(bean==null){
			Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
			activity.finish();
			return;
		}
		
		int showRow = 21;
		int longitudinalNum = 3;
		
		TheInitialScreenData theInitialScreenData = TheInitialScreenData.getScreenModess(activity,TheInitialScreenData.getScreenMode(activity,
						screenMode.getHeightPixels(),
						screenMode.getWidthPixels()),showRow, longitudinalNum);
		
		
		Intent intent = ScreenModeBean.setIntent(screenMode);
		intent.putExtra("warehouseId", m.ps_id);
		intent.putExtra("warehouseName",m.ps_name);
		intent.putExtra("serverData", (Serializable)serverData);
		intent.putExtra("theInitialScreenData", (Serializable)theInitialScreenData);
		intent.putExtra("screenModeTypeBean", (Serializable)bean);
		intent.setClass(activity, SyncLargeScreenActivity.class);
		activity.startActivity(intent);
		activity.finish();
	}
	
	/**
	 * @Description:判断要显示哪一个屏幕
	 * @param @param screenStr
	 * @param @return
	 */
	private static ScreenModeTypeBean getScreen(String screenStr){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		if(!Utility.isNullForList(ScreenDatas.getScreen_Task())){
			list.addAll(ScreenDatas.getScreen_Task());
		}
		if(!Utility.isNullForList(ScreenDatas.getScreen_Facility())){
			list.addAll(ScreenDatas.getScreen_Facility());
		}
		if(!Utility.isNullForList(ScreenDatas.getWindow())){
			list.addAll(ScreenDatas.getWindow());
		}
		if(!Utility.isNullForList(ScreenDatas.getSync())){
			list.addAll(ScreenDatas.getSync());
		}
		if(Utility.isNullForList(list)){
			return null;
		}
		for(int i=0;i<list.size();i++){
			if((list.get(i).getFragment().toString()).indexOf(screenStr)>=0){
				return list.get(i);
			}
		}
		return null;
	}
	
}
