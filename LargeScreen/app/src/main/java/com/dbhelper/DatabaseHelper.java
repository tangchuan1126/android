package com.dbhelper;

 import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String name = "report.db"; //数据库名称   
	private static final int version = 1; 			  //数据库版本   
	
	public DatabaseHelper(Context context) {
		super(context, name, null, version);
		
		StringBuffer singleProductCode = new StringBuffer("create table if not exists ");
		singleProductCode.append(charge_standard+" (id integer primary key autoincrement,");
		singleProductCode.append("timeout real , ");
		singleProductCode.append("typename text");
		singleProductCode.append(")");
		
		getWritableDatabase().execSQL(singleProductCode.toString());
	}
	
	public static final String charge_standard = "ChargeStandard";	//用来记录不同功能的屏幕的收费标准。
 	public static final String tbl_single_product_code = "single_product_code";
	
     
	@Override
	public void onCreate(SQLiteDatabase db) {
//		StringBuffer singleProductCode = new StringBuffer("create table if not exists ");
//		singleProductCode.append(charge_standard+" (id integer primary key autoincrement,");
//		singleProductCode.append("timeout real , ");
//		singleProductCode.append("typename text");
//		singleProductCode.append(")");
//		db.execSQL(singleProductCode.toString());
		//single_product_code
		StringBuffer singleProductCode = new StringBuffer("create table if not exists ");
		singleProductCode.append(tbl_single_product_code+" (id integer primary key autoincrement,");
		singleProductCode.append("pcid integer , ");
		singleProductCode.append("barcode varchar(80),") ;
		singleProductCode.append("code_type varchar(10),");
		singleProductCode.append("p_name varchar(10)");
		singleProductCode.append(")");
		db.execSQL(singleProductCode.toString());
		
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 		onCreate(db);
	}
	

}
