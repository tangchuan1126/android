package com.android.application;

import java.io.File;

import ui.CrashHandler;
import util.Goable;
import util.UiTools;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.android.activity.sceen.iface.FacilityInterface;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import debug.SocketServer;

/**
 * 初始化Application
 * @author zhangrui
 *
 */
public class WareHouseApplication extends Application implements FacilityInterface {
	
	public static WareHouseApplication app = null;
	
	@Override
	public void onCreate() {
 		super.onCreate();
 		app = this;
 		CrashHandler crashHandler = CrashHandler.getInstance();  
        //注册crashHandler  
        crashHandler.init(getApplicationContext()); 
 		initImageLoader(getApplicationContext());
 		
 		initFolders();
 		
 		//debug
 		if(UiTools.DEBUG_BySelf)
 			new SocketServer(9090).beginListen();
 	}
	
	private void initFolders(){
		File thumbnail =    new File(Goable.file_image_path_thumbnail);
		if(!thumbnail.exists()){thumbnail.mkdirs();}
		File filePath = new File(Goable.file_path);
		if(!filePath.exists()){filePath.mkdirs();}
		File upFile = new File(Goable.file_image_path_up);
		if(!upFile.exists()){upFile.mkdirs();}
		
 	}
	/**初始化图片加载类配置信息**/
	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
			.threadPriority(Thread.NORM_PRIORITY - 8)//加载图片的线程数
			.denyCacheImageMultipleSizesInMemory() //解码图像的大尺寸将在内存中缓存先前解码图像的小尺寸。
			.discCacheFileNameGenerator(new Md5FileNameGenerator())//设置磁盘缓存文件名称
			.tasksProcessingOrder(QueueProcessingType.LIFO)//设置加载显示图片队列进程
			.writeDebugLogs() // Remove for release app
			.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
	
	private static Handler handler = new Handler();

	@Override
	public Handler getHandler() {
		// TODO Auto-generated method stub
		return handler;
	}
}
