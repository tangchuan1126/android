package com.android.application;

import oso.com.vvmescreen.ExcessiveActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class LocationLoggerServiceManager extends BroadcastReceiver {  
    @Override  
    public void onReceive(Context context, Intent intent) {  
        Intent i = new Intent(context, ExcessiveActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }  
}  