package com.android.common.ui;

import key.BCSKey;
import oso.com.vvmescreen.DownloadApk;
import oso.com.vvmescreen.R;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;

public class AlertUi {

	public static void showAlertBuilder(final Context context, int err) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		Resources resources = context.getResources();
		builder.setTitle(resources.getString(R.string.sys_holdon));
		builder.setMessage(BCSKey.getReturnStatusById(err));
		boolean onClickListenerFlag = (err==BCSKey.AppVersionException);
		builder.setNegativeButton(resources.getString(R.string.sys_button_queding), onClickListenerFlag?new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				DownloadApk.loadApk(context);
			}
		}:null);
		RewriteBuilderDialog dialog = builder.create();
		dialog.setCancelable(false);
		builder.setCancelable(false);
		dialog.show();
	}

}
