package com.android.common;


import oso.com.vvmescreen.R;
import util.TheInitialScreenData;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.activity.platform.model.ScreenModeBean;

public class ListViewTop {
	
	/** 
	 * @Description:初始化底部页码栏
	 * @param foot_layout 
	 * @param foot
	 * @param screenModeBean
	 */
	public static void setListViewBottom(Activity context,View view,ScreenModeBean screenModeBean,String titleValue,boolean lagerOrSmall){
		try {
			LinearLayout foot_layout = (LinearLayout) view.findViewById(R.id.foot_layout);
			foot_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.title_page_top));
			TextView screen_title = (TextView) view.findViewById(R.id.screen_title);
			TextView total_num = (TextView) view.findViewById(R.id.total_num);
			TextView foot = (TextView) view.findViewById(R.id.foot);
			
			
			LayoutParams lp = foot_layout.getLayoutParams();
			lp.height =  lagerOrSmall?screenModeBean.getLargeNavigationHeight():screenModeBean.getSmallNavigationHeight();
			foot_layout.setLayoutParams(lp);

			screen_title.setTextSize(screenModeBean.getTextSize()*7/8);
			screen_title.setText(titleValue);
			total_num.setTextSize(screenModeBean.getTextSize()*3/5);
			foot.setTextSize(screenModeBean.getTextSize()*3/5);//(screenModeBean.getTextSize()>1)?(screenModeBean.getTextSize()-1):screenModeBean.getTextSize());
			
		} catch (Exception e) { }
	}
	
	/** 
	 * @Description:初始化底部页码栏
	 * @param foot_layout 
	 * @param foot
	 * @param screenModeBean
	 */
	public static void setSixListViewBottom(Activity context,View view,ScreenModeBean screenModeBean,String titleValue,boolean lagerOrSmall){
		try {
			LinearLayout foot_layout = (LinearLayout) view.findViewById(R.id.foot_layout);
			foot_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.title_page_top));
			TextView screen_title = (TextView) view.findViewById(R.id.screen_title);
			TextView total_num = (TextView) view.findViewById(R.id.total_num);
			TextView foot = (TextView) view.findViewById(R.id.foot);
			
			
			LayoutParams lp = foot_layout.getLayoutParams();
			lp.height =  lagerOrSmall?screenModeBean.getLargeNavigationHeight():screenModeBean.getSixScreenNavigationHeight();
			foot_layout.setLayoutParams(lp);

			screen_title.setTextSize(screenModeBean.getSixScreenTextSize()*7/8);
			screen_title.setText(titleValue);
			total_num.setTextSize(screenModeBean.getSixScreenTextSize()*3/5);
			foot.setTextSize(screenModeBean.getSixScreenTextSize()*3/5);//(screenModeBean.getTextSize()>1)?(screenModeBean.getTextSize()-1):screenModeBean.getTextSize());
			
		} catch (Exception e) { }
	}
	
	/** 
	 * @Description:初始化顶部页码栏
	 * @param foot_layout 
	 * @param foot
	 * @param screenModeBean
	 */
	public static void setSixListViewTops(Activity context,View view,TheInitialScreenData screenModeBean,String titleValue,boolean lagerOrSmall){
		try {
			LinearLayout foot_layout = (LinearLayout) view.findViewById(R.id.foot_layout);
			foot_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.title_page_top));
			TextView screen_title = (TextView) view.findViewById(R.id.screen_title);
			TextView total_num = (TextView) view.findViewById(R.id.total_num);
			TextView foot = (TextView) view.findViewById(R.id.foot);
			
			
			LayoutParams lp = foot_layout.getLayoutParams();
			lp.height =  lagerOrSmall?screenModeBean.getLargeScreenNavigationHeight():screenModeBean.getSmallScreenNavigationHeight();
			foot_layout.setLayoutParams(lp);

			screen_title.setTextSize((lagerOrSmall?screenModeBean.getLargeTextSize():screenModeBean.getSmallScreenTextSize())*7/8);
			screen_title.setText(titleValue);
			total_num.setTextSize((lagerOrSmall?screenModeBean.getLargeTextSize():screenModeBean.getSmallScreenTextSize())*3/5);
			foot.setTextSize((lagerOrSmall?screenModeBean.getLargeTextSize():screenModeBean.getSmallScreenTextSize())*3/5);//(screenModeBean.getTextSize()>1)?(screenModeBean.getTextSize()-1):screenModeBean.getTextSize());
			
		} catch (Exception e) { }
	}
	
	/** 
	 * @Description:初始化顶部页码栏
	 * @param foot_layout 
	 * @param foot
	 * @param screenModeBean
	 */
	public static void setSixProcessViewTops(Activity context,View view,TheInitialScreenData screenModeBean,String titleValue,boolean lagerOrSmall){
		try {
			LinearLayout foot_layout = (LinearLayout) view.findViewById(R.id.foot_layout);
			foot_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.title_page_top));
			TextView screen_title = (TextView) view.findViewById(R.id.screen_title);
			TextView total_num = (TextView) view.findViewById(R.id.total_num);
			TextView foot = (TextView) view.findViewById(R.id.foot);
			
			
			LayoutParams lp = foot_layout.getLayoutParams();
			lp.height =  lagerOrSmall?screenModeBean.getLargeScreenNavigationHeight():screenModeBean.getSmallScreenNavigationHeight();
			foot_layout.setLayoutParams(lp);

			screen_title.setTextSize((lagerOrSmall?screenModeBean.getLargeTextSize():screenModeBean.getSmallScreenTextSize())*7/8);
			screen_title.setText(titleValue);
			total_num.setVisibility(View.GONE);
			foot.setVisibility(View.GONE);
			
		} catch (Exception e) { }
	}
	
//	/** 
//	 * @Description:初始化底部页码栏
//	 * @param foot_layout 
//	 * @param foot
//	 * @param screenModeBean
//	 */
//	public static void setListViewBottom(Activity context,View view,ScreenModeBean screenModeBean,String titleValue,int completionSize){
//		try {
//			LinearLayout foot_layout = (LinearLayout) view.findViewById(R.id.foot_layout);
//			foot_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.title_page_top));
//			TextView screen_title = (TextView) view.findViewById(R.id.screen_title);
//			TextView foot = (TextView) view.findViewById(R.id.foot);
//			
//			
//			LayoutParams lp = foot_layout.getLayoutParams();
//			lp.height =  ScreenMonitorTools.getChangeTheSizeForBottom(screenModeBean.getEveryItem())+completionSize;
//			foot_layout.setLayoutParams(lp);
//
//			int size = screenModeBean.getTextSize()*3/4;
//			screen_title.setTextSize(screenModeBean.getTextSize());
//			screen_title.setText(titleValue);
//			foot.setTextSize(size);//(screenModeBean.getTextSize()>1)?(screenModeBean.getTextSize()-1):screenModeBean.getTextSize());
//			
//		} catch (Exception e) { }
//	}
	
	/** 
	 * @Description:设置底部页码显示的信息
	 * @param foot_layout 
	 * @param foot
	 * @param screenModeBean
	 */
	public static void setCurrentPageAndConutPage(Activity context,View view,int currentPage,int conutPage,int total){
		TextView foot = (TextView) view.findViewById(R.id.foot);
		
		String str = "Page "+((conutPage==0)?1:currentPage)+" / "+((conutPage==0)?1:conutPage);
		foot.setText(str);
		
		TextView total_num = (TextView) view.findViewById(R.id.total_num);
		
		String total_numStr = "Total "+total;
		total_num.setText(total_numStr);
	}
	
//	/** 
//	 * @Description:设置底部页码显示的信息
//	 * @param foot_layout 
//	 * @param foot
//	 * @param screenModeBean
//	 */
//	public static void setCurrentPageAndConutPage(TextView foot,int currentPage,int conutPage){
//		String str = "PAGE "+((conutPage==0)?1:currentPage)+" / "+((conutPage==0)?1:conutPage);
//		foot.setText(str);
//	}
}
