package com.android.common;

import key.BCSKey;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.com.vvmescreen.R;
import util.Goable;
import util.StringUtil;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.widget.Toast;

import com.android.common.ui.AlertUi;
import com.android.common.ui.RewriteDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
/**
 * 0.这个类的目的是除去，很多错误信息的重复判断<br />
 * 1.只用于 请求简单的JSON数据格式的时候<br />
 * 2.handReponseJson 处理返回的JSON 格式的数据<br />
 * 3.handFinish 处理不管是错误，或者是正确执行的方法<br />
 * 4.现在很多的处理都是获取数据,然后存储在数据库中。所以在handReponseJson，写入数据库中 <br />
 * 5.handFinish 请求完成过后，然后读取数据库里面的数据(你也可以没有)
 * @author zhangrui
 *
 */
public abstract class SimpleJSONUtil {
	
	private static AsyncHttpClient client = new AsyncHttpClient();
	static{
		client.setTimeout(5 * 1000);
	}
	
	public abstract void handReponseJson(JSONObject json);
	
	
	public void handFail(){}; 
	
	public void versionFail(){};
	
	/**
	 * @Description:用自定义的方法去处理是否反填登陆信息
	 * @param @param json
	 * @param @return 返回false则用不填 返回 ture则填
	 */
	public boolean setLoginInfoParams() {
		return true;
	}
	
	private static boolean runFlag = true;
	
	/**
	 * @Description:大屏幕专用
	 * @param url
	 * @param params
	 * @param context
	 */
	public void doGetNotHaveDialog(final String url, final RequestParams params, final Context context  ) {

		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		
  		// 改成HttpClient 的方式，放回的为JSON的数据
		client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,JSONObject json) {
				// 处理结果
				if (statusCode == 200) {
					if (json.optInt("ret") == 1) {
						handReponseJson(json);
					}else{
						handFail(); 
						if(json.optInt("err")==BCSKey.AppVersionException){
	 						//提示系统的错误(更加返回的err值)
							if(runFlag){
								AlertUi.showAlertBuilder(context,json.optInt("err"));
								runFlag = false;
							}
							versionFail();
						}
					}
				}else{
					handFail(); 
				}
			}
			@Override
			public void onFailure(int statusCode, Throwable e,JSONObject errorResponse) {
				handFail(); 
			}
		
		});
	}
	
	public void doPost(final String url , final RequestParams params , final Context context ){
 		
		final ProgressDialog dialog = new ProgressDialog(context);
		final Resources resources = context.getResources();
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(resources.getString(R.string.sys_submitData));
		dialog.setCancelable(true);

		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		
  		// 改成HttpClient 的方式，放回的为JSON的数据 
		client.post(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,JSONObject json) {
 				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if(!useTheCustom(json)){
						if (json.optInt("ret") == 1) {
							handReponseJson(json);
						}else{
	 						//提示系统的错误(更加返回的err值)
							AlertUi.showAlertBuilder(context,json.optInt("err"));
//							Toast.makeText(context,BCSKey.getReturnStatusById(json.optInt("err")),Toast.LENGTH_SHORT).show();
							handFail(); 
	 					}
					}
				} else {
					//系统的错误 不是返回的200
					Toast.makeText(context,resources.getString(R.string.sys_syserror_submit_data_failed),Toast.LENGTH_SHORT).show();
					handFail(); 
				}
			}
			
			@Override
			public void onFailure(int statusCode, Throwable e,JSONObject errorResponse) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				handFail(); 
				Toast.makeText(context, "Connection Timeout！", Toast.LENGTH_LONG).show();
			}
		});
	
		
		
	}
	
	public void doGet(final String url, final RequestParams params, final Context context  ) {
		
		final RewriteDialog dialog = RewriteDialog.createDialog(context,1);
		final Resources resources = context.getResources();
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(resources.getString(R.string.sys_loadBase_data));
		dialog.setCancelable(true);
	 
		final String appendNofity = resources.getString(R.string.sys_load_data_failed) ;

		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		
  		// 改成HttpClient 的方式，放回的为JSON的数据
		client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,JSONObject json) {
 				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if(!useTheCustom(json)){
						if (json.optInt("ret") == 1) {
							handReponseJson(json);
						}else{
	 						//提示系统的错误(更加返回的err值)
							AlertUi.showAlertBuilder(context,json.optInt("err"));
//							Toast.makeText(context,BCSKey.getReturnStatusById(json.optInt("err")) + ","+ appendNofity,Toast.LENGTH_SHORT).show();
							handFail(); 
						}
					}
					
					
				} else {
					//系统的错误 不是返回的200
					Toast.makeText(context,resources.getString(R.string.sys_syserror_getData_failed) +", " +  appendNofity,Toast.LENGTH_SHORT).show();
					handFail(); 
				}
			}
	
			 
			
			@Override
			public void onFailure(int statusCode, Throwable e,JSONObject errorResponse) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				handFail(); 
				Toast.makeText(context, "Connection Timeout！", Toast.LENGTH_LONG).show();
			}
			
		});
	}
	
	/**
	 * @Description:用自定义的方法去处理返回的json数据
	 * @param @param json
	 * @param @return 返回false则用默认的方法  返回 ture则用自定义的方法
	 */
	public boolean useTheCustom(JSONObject json){
		return false;
	}; 
}
