package com.android.common.ui;

import oso.com.vvmescreen.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * 
 * @ClassName: RewriteBuilderDialog
 * @Description:
 * @author gcy
 * @date 2014年6月10日 下午5:20:47
 * 
 */
public class RewriteBuilderDialog extends Dialog {

	private static boolean mCancelable = true;

	public RewriteBuilderDialog(Context context) {
		super(context);
	}

	public RewriteBuilderDialog(Context context, int theme) {
		super(context, theme);
	}

	public static class Builder {
		private Context context;
		private String title;
		private String message;
		private String positiveButtonText;
		private String negativeButtonText;
		private View contentView;
		private DialogInterface.OnClickListener positiveButtonClickListener;
		private DialogInterface.OnClickListener negativeButtonClickListener;
		private boolean ifShowCancel;
		private boolean isDismiss = true;

		public Builder(Context context) {
			this.context = context;
		}

		public Builder setMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder setMessage(int message) {
			this.message = (String) context.getText(message);
			return this;
		}

		public Builder setTitle(int title) {
			this.title = (String) context.getText(title);
			return this;
		}

		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder setContentView(View v) {
			this.contentView = v;
			return this;
		}

		public boolean isDismiss() {
			return isDismiss;
		}

		// positiveBtn点击后dismiss
		public void setDismiss(boolean isDismiss) {
			this.isDismiss = isDismiss;
		}

		public Builder setPositiveButton(int positiveButtonText,
				DialogInterface.OnClickListener listener) {
			this.positiveButtonText = (String) context
					.getText(positiveButtonText);
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setPositiveButton(String positiveButtonText,
				DialogInterface.OnClickListener listener) {
			this.positiveButtonText = positiveButtonText;
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(int negativeButtonText,
				DialogInterface.OnClickListener listener) {
			this.negativeButtonText = (String) context
					.getText(negativeButtonText);
			this.negativeButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(String negativeButtonText,
				DialogInterface.OnClickListener listener) {
			this.negativeButtonText = negativeButtonText;
			this.negativeButtonClickListener = listener;
			return this;
		}

		// 隐藏-cancel
		public Builder setCancelNegativeButton(boolean flag) {
			this.ifShowCancel = flag;
			return this;
		}

		public RewriteBuilderDialog create() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final RewriteBuilderDialog dialog = new RewriteBuilderDialog(context, R.style.Dialog);
			View layout = inflater.inflate(R.layout.rewrite_dialog_layout, null);
			dialog.addContentView(layout, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			((TextView) layout.findViewById(R.id.title)).setText(title);
			if (positiveButtonText != null) {
				layout.findViewById(R.id.positiveButton).setVisibility(View.VISIBLE);
				((Button) layout.findViewById(R.id.positiveButton)).setText(positiveButtonText);
			}
			if (positiveButtonClickListener != null) {
				((Button) layout.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								positiveButtonClickListener.onClick(dialog,DialogInterface.BUTTON_POSITIVE);
								if (dialog.isShowing() && isDismiss) {
									dialog.dismiss();
								}
							}
						});
			} else {
				((Button) layout.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								if (dialog.isShowing()) {
									dialog.dismiss();
								}
							}
						});
			}

			if (negativeButtonText != null) {
				((Button) layout.findViewById(R.id.negativeButton)).setText(negativeButtonText);
				((Button) layout.findViewById(R.id.negativeButton)).setFocusable(true);
				((Button) layout.findViewById(R.id.negativeButton)).requestFocus();
			}
			if (negativeButtonClickListener != null) {
				((Button) layout.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								negativeButtonClickListener.onClick(dialog,DialogInterface.BUTTON_NEGATIVE);
								if (dialog.isShowing()) {
									dialog.dismiss();
								}
							}
						});
			} else {
				((Button) layout.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
			}

			if (ifShowCancel) {
				layout.findViewById(R.id.negativeButton).setVisibility(View.GONE);
			}

			if (message != null) {
				((TextView) layout.findViewById(R.id.message)).setVisibility(View.VISIBLE);
				((TextView) layout.findViewById(R.id.message)).setText(message);
			}
			if (contentView != null) {
				((FrameLayout) layout.findViewById(R.id.customPanel)).setVisibility(View.VISIBLE);
				((FrameLayout) layout.findViewById(R.id.customPanel)).removeAllViews();
				((FrameLayout) layout.findViewById(R.id.customPanel)).addView(contentView, new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
			}
			dialog.setContentView(layout);
			return dialog;
		}

		public void setCancelable(boolean b) {
			mCancelable = b;
		}

		public void dismiss() {
			dismiss();
		}
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking()
				&& !event.isCanceled()) {
			onBackPressed();
			return true;
		}
		return false;
	}

	public void onBackPressed() {
		if (mCancelable) {
			dismiss();
		}
	}

	// ===========static=====================================

	//显示-简单dlg
	public static void showSimpleDialog(Activity ac,String msg,
			DialogInterface.OnClickListener onClick_Positive) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				ac);
		builder.setTitle("Prompt");
		builder.setMessage(msg);
		builder.setPositiveButton("Yes", onClick_Positive);
		builder.setNegativeButton("No", null);
		builder.create().show();
	}

}
