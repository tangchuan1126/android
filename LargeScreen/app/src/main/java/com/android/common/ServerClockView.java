package com.android.common;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import util.TimeoutTools;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.activity.sceen.iface.FacilityInterface;
import com.android.application.WareHouseApplication;
import com.loopj.android.http.RequestParams;

public class ServerClockView extends LinearLayout {

	public ServerClockView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public ServerClockView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	private void init(Context context) {
		mContext = context;
		personIv = new ImageView(context);
		personTv = new TextView(context);
		printIv = new ImageView(context);
		printTv = new TextView(context);
		clockIv = new ImageView(context);
		clockTv = new TextView(context);
		int left = (int) context.getResources().getDimension(R.dimen.actionbar_vertival_padding);
		personIv.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_person));
		personTv.setTextColor(Color.BLACK);
		personTv.setText("0 / 0");
		personTv.setPadding(left*2, 0, 0, 0);
		printIv.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_print));
		printTv.setTextColor(Color.BLACK);
		printTv.setText("0 / 0");
		printTv.setPadding(left*2, 0, 0, 0);
		clockIv.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_clock));
		clockTv.setTextColor(Color.BLACK);
		clockTv.setText("--/--  --:--");
		clockTv.setPadding(left*2, 0, left*2, 0);
		setVisibility(View.GONE);
		face = WareHouseApplication.app;
	}

	/**
	 * 初始化数据
	 */
	public void initClock(int w, int h, int size, String warehouseId, String serverTimeUrl) {
		if (warehouseId != null && serverTimeUrl != null) {
			this.warehouseId = warehouseId;
			this.serverTimeUrl = serverTimeUrl;
			initView(w, h, size);
			startHandler();
		} else {
			setVisibility(View.GONE);
			if (face != null)
				setCallbacks(true);
		}
	}

	private void initView(int w, int h, int size) {
		addView(personIv, w, h);
		addView(personTv, -2, h);
		addView(new View(mContext), w, h);
		addView(printIv, w, h);
		addView(printTv, -2, h);
		addView(new View(mContext), w, h);
		addView(clockIv, w, h);
		addView(clockTv, -2, h);
		personTv.setTextSize(size);
		printTv.setTextSize(size);
		clockTv.setTextSize(size);
	}

	/**
	 * @Description:启动运行线程
	 * @param
	 */
	private void startHandler() {
		timeUtil.setTimeMS(System.currentTimeMillis());
		face.getHandler().post(going_to_warehouse_task);
		face.getHandler().post(alwaysFixGoingToWareHouseHandler);
	}

	/**
	 * 请求数据
	 */
	private Runnable going_to_warehouse_task = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (warehouseId != null)
					getHttpData(warehouseId);
			}
		}
	};

	/**
	 * 监视器用于循环守卫线程是否正常运行
	 */
	private Runnable alwaysFixGoingToWareHouseHandler = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (TimeoutTools.isFastPost(timeUtil, 15 * 1000)) {
					face.getHandler().removeCallbacks(going_to_warehouse_task);
					timeUtil.setTimeMS(System.currentTimeMillis());
					face.getHandler().postDelayed(going_to_warehouse_task, TASK_CLOCK_RUN_TIME);
				}
				face.getHandler().postDelayed(alwaysFixGoingToWareHouseHandler, 1000);
			}
		}
	};

	private void ChooseWhich() {
		if (whetherToRun) {
			face.getHandler().removeCallbacks(going_to_warehouse_task);
			timeUtil.setTimeMS(System.currentTimeMillis());
			face.getHandler().postDelayed(going_to_warehouse_task, TASK_CLOCK_RUN_TIME);
		}
	}

	/**
	 * @Description:控制线程的运行以及停止
	 * @param @param doRemove 判断是否停止以及移除线程 true移除 false不移除重新运行
	 */
	public void setCallbacks(boolean doRemove) {
		whetherToRun = !doRemove;
		if (doRemove) {
			face.getHandler().removeCallbacks(going_to_warehouse_task);
			face.getHandler().removeCallbacks(alwaysFixGoingToWareHouseHandler);
//			warehouseId = null;
		} else {
			startHandler();
		}
	}

	private void getHttpData(String warehouseId) {
		RequestParams params = new RequestParams();
		params.put("Method", "ServerPrintAndPersonTime");
		params.put("ps_id", warehouseId);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				setData(json);
				ChooseWhich();
			}

			@Override
			public void handFail() {
				ChooseWhich();
			}

			@Override
			public void versionFail(){
				whetherToRun = false;
			}
		}.doGetNotHaveDialog(serverTimeUrl, params, mContext);
	}

	private void setData(JSONObject json) {
		// Toast.makeText(mContext, json.toString(), Toast.LENGTH_SHORT).show();
		System.out.println("Clock= " + json.toString());
		setVisibility(View.VISIBLE);
		clockTv.setText(json.optString("server_time"));
		String free_person = json.optString("free_person");
		String printer = json.optString("printer");
		String printer_total = json.optString("printer_total");
		boolean isHasPerson = !TextUtils.isEmpty(free_person);
		boolean isHasPrinter = !TextUtils.isEmpty(printer) && !TextUtils.isEmpty(printer_total);
		if (isHasPerson) {
			personIv.setVisibility(View.VISIBLE);
			personTv.setVisibility(View.VISIBLE);
			personTv.setText(free_person);
		} else {
			personIv.setVisibility(View.GONE);
			personTv.setVisibility(View.GONE);
		}
		if (isHasPrinter) {
			printIv.setVisibility(View.VISIBLE);
			printTv.setVisibility(View.VISIBLE);
			printTv.setText(printer + " / " + printer_total);
		} else {
			printIv.setVisibility(View.GONE);
			printTv.setVisibility(View.GONE);
		}
	}

	private Context mContext;

	private FacilityInterface face;// 用于获取父级的 UI主线程的引用

	private boolean whetherToRun = true;// 控制线程的继续运行

	private String warehouseId = null;

	private String serverTimeUrl = null;

	private TextView personTv, printTv, clockTv;

	private ImageView personIv, printIv , clockIv;

	public static final int TASK_CLOCK_RUN_TIME = 10 * 1000;

	private TimeoutTools timeUtil = new TimeoutTools();
}
