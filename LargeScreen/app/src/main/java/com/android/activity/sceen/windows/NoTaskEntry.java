package com.android.activity.sceen.windows;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.windows.adapter.NoTaskAdapter;
import com.android.activity.sceen.windows.model.NoTaskBean;
import com.android.core.CommonFragment;

public class NoTaskEntry extends CommonFragment<NoTaskBean> {
	
	public static final String method = "noTaskEntry";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.no_task_entry_layout_frament;
	}

	@Override
	public List<NoTaskBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return NoTaskBean.getBaseTypeList(json);
	}

	@Override
	public NoTaskBean newBean() {
		// TODO Auto-generated method stub
		return new NoTaskBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new NoTaskAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}
	
}
