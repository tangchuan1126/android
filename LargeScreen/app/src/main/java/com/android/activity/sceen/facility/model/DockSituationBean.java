package com.android.activity.sceen.facility.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

public class DockSituationBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	

	

	
	//==========新======================
     
     public int total_time;
	 public String check_in_warehouse_time;
	 public String rel_type;
	 public String equipment_number;
	 public String doorid;
	 
	 public String tractor;
	 public String check_in_time;
	 public String equipment_id;
	 public String dlo_id;	
	 public boolean simulationData;//true:真数据
	/***************************************************************************/
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<DockSituationBean> getBaseTypeList(JSONObject json){
		
		List<DockSituationBean> dockSituationBaseList = new ArrayList<DockSituationBean>();
		JSONArray dockSituationBaseArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(dockSituationBaseArrays));
		if(flag){
			for (int i = 0; i < dockSituationBaseArrays.length(); i++) {
				JSONObject jsonItem = dockSituationBaseArrays.optJSONObject(i);
				DockSituationBean b = new DockSituationBean();
				
				 
				b.total_time = jsonItem.optInt("total_time");
				b.check_in_warehouse_time = jsonItem
						.optString("check_in_warehouse_time");
				b.rel_type = jsonItem.optString("rel_type");
				b.equipment_number = jsonItem.optString("equipment_number");
				b.doorid = jsonItem.optString("doorid");

				b.tractor = jsonItem.optString("tractor");
				b.check_in_time = jsonItem.optString("check_in_time");
				b.equipment_id = jsonItem.optString("equipment_id");
				b.dlo_id = jsonItem.optString("dlo_id");
				b.simulationData=true;
				
				dockSituationBaseList.add(b);
			}
			
			return dockSituationBaseList;
		}
		return null;
	}
	

	
	/***************************************************************************/

}
