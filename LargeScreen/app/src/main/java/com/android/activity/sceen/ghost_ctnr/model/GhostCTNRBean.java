package com.android.activity.sceen.ghost_ctnr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: GhostCTNRBean 
 * @Description: 
 * @author gcy
 * @date 2015-3-27 上午11:19:41
 */
public class GhostCTNRBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -6553436669948756969L;
	/*****************************************/	
	
	public String check_in_entry_id;
	public String equipment_number;
	public String location;
	public String rel_type;
	public String equipment_status;
	public int total_time;
	
	public boolean simulationData;//判断是否是真数据 true为真数据 false为假数据
	//=============================================
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<GhostCTNRBean> getBaseTypeList(JSONObject json){
		List<GhostCTNRBean> list = new ArrayList<GhostCTNRBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				GhostCTNRBean bean = new GhostCTNRBean();
				bean.check_in_entry_id=jsonItem.optString("check_in_entry_id");
				bean.equipment_number=jsonItem.optString("equipment_number");
				bean.equipment_status=jsonItem.optString("equipment_status");
				bean.location=jsonItem.optString("location");
				bean.rel_type=jsonItem.optString("rel_type");
				bean.total_time=jsonItem.optInt("total_time");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}

}