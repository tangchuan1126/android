package com.android.activity.sceen.facility.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class SpotSituationBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9034608115785529954L;

	
	//==================新===========================
     
    public int total_time;
	public String check_in_warehouse_time;
	public String yc_no;
	public String rel_type;
	public String equipment_number;
	
	public String tractor;
	public String check_in_time;
	public String equipment_id;
	public String dlo_id;
	public boolean simulationData;//判断是否是真数据
	
	/***************************************************************************/
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<SpotSituationBean> getBaseTypeList(JSONObject json){
		List<SpotSituationBean> spotSituationBaseList = new ArrayList<SpotSituationBean>();
		JSONArray spotSituationBaseArrays = json.optJSONArray("data");
		boolean flag =(spotSituationBaseArrays!=null&&spotSituationBaseArrays.length()>0);
		if(flag){
			for (int i = 0; i < spotSituationBaseArrays.length(); i++) {
				JSONObject jsonItem = spotSituationBaseArrays.optJSONObject(i);
				SpotSituationBean b = new SpotSituationBean();
				
				b.total_time=jsonItem.optInt("total_time");
				b.check_in_warehouse_time=jsonItem.optString("check_in_warehouse_time");
				b.yc_no=jsonItem.optString("yc_no");
				b.rel_type=jsonItem.optString("rel_type");
				b.equipment_number=jsonItem.optString("equipment_number");
				
				b.tractor=jsonItem.optString("tractor");
				b.check_in_time=jsonItem.optString("check_in_time");
				b.equipment_id=jsonItem.optString("equipment_id");
				b.dlo_id = jsonItem.optString("dlo_id");
				
				b.simulationData=true;
				spotSituationBaseList.add(b);
			}
			
			return spotSituationBaseList;
		}
		return null;
	}
	
	/***************************************************************************/
}