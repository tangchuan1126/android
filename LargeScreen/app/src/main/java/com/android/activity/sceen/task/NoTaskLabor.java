package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.NoTaskLaborAdapter;
import com.android.activity.sceen.task.model.NoTaskLaborBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class NoTaskLabor extends CommonFragment<NoTaskLaborBean> {

	public static final String method = "noTaskLabor";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.no_task_labor_layout;
	}

	@Override
	public List<NoTaskLaborBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return NoTaskLaborBean.getBaseTypeList(json);
	}

	@Override
	public NoTaskLaborBean newBean() {
		// TODO Auto-generated method stub
		return new NoTaskLaborBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new NoTaskLaborAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
