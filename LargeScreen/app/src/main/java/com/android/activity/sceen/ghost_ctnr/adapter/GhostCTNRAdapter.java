package com.android.activity.sceen.ghost_ctnr.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.ghost_ctnr.model.GhostCTNRBean;

public class GhostCTNRAdapter extends BaseAdapter  {
	
	private Context context;
	
	private List<GhostCTNRBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;

	private int everyItem;
	private int textSize;

	public GhostCTNRAdapter(Context context, List<GhostCTNRBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
		this.context=context;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public GhostCTNRBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LoadReceiveCloseTodayHoder holder = null;
		 if(convertView==null){
			holder = new LoadReceiveCloseTodayHoder();
			convertView = inflater.inflate(R.layout.ghost_ctnr_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);	
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			holder.loLocation=convertView.findViewById(R.id.loLocation); 
			holder.entry_id = (TextView)convertView.findViewById(R.id.entry_id);
			holder.equipment_number = (TextView)convertView.findViewById(R.id.equipment_number);
			holder.location = (TextView)convertView.findViewById(R.id.location);
			holder.rel_type = (TextView)convertView.findViewById(R.id.rel_type);
			holder.equipment_status = (TextView)convertView.findViewById(R.id.equipment_status);
			holder.total_time = (TextView)convertView.findViewById(R.id.total_time);
			
			holder.entry_id.setTextSize(textSize);
			holder.equipment_number.setTextSize(textSize);
			holder.location.setTextSize(textSize);
			holder.rel_type.setTextSize(textSize);
			holder.equipment_status.setTextSize(textSize);
			holder.total_time.setTextSize(textSize);
			
			
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,"Location");
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.location.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
 			convertView.setTag(holder);
		}else{
			holder = (LoadReceiveCloseTodayHoder) convertView.getTag();
		}
		
		GhostCTNRBean b  =  arrayList.get(position);

		holder.entry_id.setText((b.simulationData)?b.check_in_entry_id:"");
		holder.equipment_number.setText((b.simulationData)?b.equipment_number:"");
		holder.location.setText((b.simulationData)?b.location:"");
		holder.rel_type.setText((b.simulationData)?b.rel_type:"");
		holder.equipment_status.setText((b.simulationData)?b.equipment_status:"");
		holder.total_time.setText((b.simulationData)?ScreenMonitorTools.fixTime(b.total_time):"");
		
		
  
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}


	public void setList(List<GhostCTNRBean> arrayList){
		this.arrayList = arrayList;
	}
	
}
class LoadReceiveCloseTodayHoder {
	public View layout;
	public View layout_id;
	
	public TextView entry_id;
	public TextView equipment_number;
	
	public View loLocation;
	public TextView location;
	
	public TextView rel_type;
	public TextView equipment_status;
	public TextView total_time;
}