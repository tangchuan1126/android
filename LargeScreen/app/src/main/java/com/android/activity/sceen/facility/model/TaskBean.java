package com.android.activity.sceen.facility.model;

import java.io.Serializable;

public class TaskBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;

	private String numberType;
	private String number;
	/***************************************************************/
	

	/***************************************************************/
	public String getNumberType() {
		return numberType;
	}
	public void setNumberType(String numberType) {
		this.numberType = numberType;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
}
