package com.android.activity.sceen.facility;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.facility.adapter.DoorSituationAdapter;
import com.android.activity.sceen.facility.model.DockSituationBean;
import com.android.core.CommonFragment;

/**
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragCargoOnDoor extends CommonFragment<DockSituationBean> {
	
	public static final String method = "cargoOnDoor";

	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_door;
	}

	@Override
	public List<DockSituationBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return DockSituationBean.getBaseTypeList(json);
	}

	@Override
	public DockSituationBean newBean() {
		// TODO Auto-generated method stub
		return new DockSituationBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new DoorSituationAdapter(mActivity, getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
