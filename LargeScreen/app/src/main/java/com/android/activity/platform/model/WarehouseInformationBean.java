package com.android.activity.platform.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @ClassName: WarehouseInformationBase
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class WarehouseInformationBean implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/*****************************************/
	
	private int warehouseId;
	private String warehouseName;
	
	public static List<WarehouseInformationBean> parsingJSON(JSONObject json){
		List<WarehouseInformationBean> list = new ArrayList<WarehouseInformationBean>();
		JSONArray jsonArray = json.optJSONArray("data");
		if(jsonArray!=null&&jsonArray.length()>0){
			for(int i=0;i<jsonArray.length();i++){
				JSONObject jsonObject = jsonArray.optJSONObject(i);
				WarehouseInformationBean w = new WarehouseInformationBean();
				w.setWarehouseId(jsonObject.optInt("ps_id"));
				w.setWarehouseName(jsonObject.optString("ps_name"));
				list.add(w);
			}
		}		
		return list;
	}
	
	/*****************************************/
	
	public int getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(int warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

}
