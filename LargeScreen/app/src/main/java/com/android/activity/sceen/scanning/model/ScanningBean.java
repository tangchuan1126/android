package com.android.activity.sceen.scanning.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: ReceiveScanBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-23 下午3:05:08
 */
public class ScanningBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 377235418886992665L;
	/*****************************************/
//	Task Type  | Task Number |  Item | Process(进度) | Time | Operator 
	
	public String employe_name;//Operator
	public String item_id;//Item
	public String schedule_id;//Task Number
	public int total_time;
	public String task_type;//Task Type
	public double progress;
	public int qty;
	public String company_id;
	
	public boolean simulationData;//判断是否是真数据
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<ScanningBean> getBaseTypeList(JSONObject json){
		List<ScanningBean> list = new ArrayList<ScanningBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				ScanningBean bean = new ScanningBean();	
				
				bean.task_type = jsonItem.optString("task_type");
				bean.schedule_id = jsonItem.optString("schedule_id");
				bean.item_id = jsonItem.optString("item_id");
				bean.employe_name = jsonItem.optString("employe_name");
				bean.total_time = jsonItem.optInt("total_time");
				bean.progress = jsonItem.optDouble("progress");
				bean.qty = jsonItem.optInt("qty");
				bean.company_id = jsonItem.optString("company_id");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}
	
	
}
