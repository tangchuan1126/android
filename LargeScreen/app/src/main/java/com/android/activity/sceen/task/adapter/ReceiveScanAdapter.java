package com.android.activity.sceen.task.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.ReceiveScanBean;
import com.android.common.progresswheel.PieProgress;

public class ReceiveScanAdapter extends BaseAdapter  {
	
	private List<ReceiveScanBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public ReceiveScanAdapter(Context context, List<ReceiveScanBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ReceiveScanBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ReceiveScanHoder holder = null;
		 if(convertView==null){
			holder = new ReceiveScanHoder();
			convertView = inflater.inflate(R.layout.frag_fields_receive_scan_item,null);

			holder.layout = (View) convertView.findViewById(R.id.layout);		
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.receipt_no = (TextView) convertView.findViewById(R.id.receipt_no);
			holder.receipt_no.setTextSize(textSize);	
			holder.item_id = (TextView) convertView.findViewById(R.id.item_id);
			holder.item_id.setTextSize(textSize);	
			holder.status = (TextView) convertView.findViewById(R.id.status);
			holder.status.setTextSize(textSize);	
			holder.labor = (TextView) convertView.findViewById(R.id.labor);
			holder.labor.setTextSize(textSize);	
			holder.progress = (TextView) convertView.findViewById(R.id.progress);
			holder.progress.setTextSize(textSize);	
			holder.total_pallets = (TextView) convertView.findViewById(R.id.total_pallets);
			holder.total_pallets.setTextSize(textSize);	
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.time.setTextSize(textSize);	

			holder.donecount_percentage = (PieProgress) convertView.findViewById(R.id.donecount_percentage);
			LayoutParams lp_d = holder.donecount_percentage.getLayoutParams();
			lp_d.height = everyItem;
			lp_d.width = everyItem;
			holder.donecount_percentage.setLayoutParams(lp_d);
			
 			convertView.setTag(holder);
		}else{
			holder = (ReceiveScanHoder) convertView.getTag();
		}
		 
		ReceiveScanBean temp  =  arrayList.get(position);
		
		boolean debug = true;
		if(debug){
			Random r = new Random();
			holder.receipt_no.setText(temp.simulationData?temp.receipt_no:(r.nextInt(10)+1+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)));  
			holder.item_id.setText(temp.simulationData?temp.item_id:("M"+(r.nextInt(4)+1)+""+r.nextInt(10)+"C"+r.nextInt(10)));  
			int a = r.nextInt(3)+1;
			Map<Integer, String> map = new HashMap<Integer, String>();
			map.put(1, "Ramain");
			map.put(2, "Scaning");
			map.put(3, "Waiting Assign");
			holder.status.setText(map.get(a));
			
			
			int c = r.nextInt(7)+1;
			Map<Integer, String> cmap = new HashMap<Integer, String>();
			cmap.put(1, "Zhu Cheng");
			cmap.put(2, "Guo Chun Yang");
			cmap.put(3, "Jiang Zhen");
			cmap.put(4, "Li Jun Hao");
			cmap.put(5, "Xia Li Min");
			cmap.put(6, "Zhang Rui");
			cmap.put(7, "Zhang Rui");
			
			holder.labor.setText(cmap.get(c));   
			holder.total_pallets.setText(temp.simulationData?(temp.total_pallets+""):(r.nextInt(40)+20+""));  
			holder.time.setText(ScreenMonitorTools.fixTime(r.nextInt(300)+1));
			
			holder.progress.setVisibility(View.VISIBLE);
			holder.donecount_percentage.setVisibility(View.VISIBLE);
			
			double b = r.nextInt(100)+1.00;
			holder.donecount_percentage.setProgress((int)(b/100*360));
			holder.progress.setText(b+"%");
		}else{
			holder.receipt_no.setText(temp.simulationData?temp.receipt_no:"");  
			holder.item_id.setText(temp.simulationData?temp.item_id:"");  
			holder.status.setText(temp.simulationData?temp.status:"");   
			holder.labor.setText(temp.simulationData?temp.labor:"");   
			holder.total_pallets.setText(temp.simulationData?(temp.total_pallets+""):"");  
			holder.time.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.time):"");
			
			
			if(temp.simulationData){
				holder.progress.setVisibility(View.VISIBLE);
				holder.donecount_percentage.setVisibility(View.VISIBLE);
			}else{
				holder.progress.setVisibility(View.GONE);
				holder.donecount_percentage.setVisibility(View.GONE);
			}
			
			holder.progress.setText((int)temp.progress+"%");
			holder.donecount_percentage.setProgress((int)(temp.progress/100*360));
		}
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<ReceiveScanBean> arrayList){
		this.arrayList = arrayList;
	}
}
class ReceiveScanHoder {	
	public View layout;
	public View layout_id;

	public TextView receipt_no;
	public TextView item_id;
	public TextView status;
	public TextView labor;
	public TextView progress;
	public PieProgress donecount_percentage;
	
	public TextView total_pallets;
	public TextView time;
	
	public View color_layout;
}