package com.android.activity.sceen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.com.vvmescreen.R;
import ui.ScreenTitleLayout;
import util.HttpUrlPath;
import util.ScreenMonitorTools;
import util.StringUtil;
import util.TheInitialScreenData;
import util.TimeoutTools;
import util.Utility;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.activity.common.BaseBean;
import com.android.activity.common.ScreenModeTypeBean;
import com.android.activity.platform.ScreenTypeListActivity;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ScreenTypeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.sceen.iface.FacilityInterface;
import com.android.common.SimpleJSONUtil;
import com.android.core.CommonFragment;
import com.android.core.CommonProcessingFragment;
import com.loopj.android.http.RequestParams;

/**
 * 
 * 含多个小屏
 * @Description:
 * @author gcy
 * @date 2014年7月24日 下午3:09:55
 * 
 */
public class ScreensActivity extends FragmentActivity implements
		FacilityInterface, OnFocusChangeListener, OnClickListener,
		OnKeyListener {

	private Activity context;
	private String warehouseId;// 仓库ID
	private String warehouseName;
	private ServerTypeBean s;
	private ScreenModeBean screenMode;
	private ScreenTypeBean screenTypeBean;

	private boolean viewHasFocus = false;

	private Handler handler = new Handler();

	private ScreenTitleLayout layout;
	private static List<ScreenModeTypeBean> list;
	private List<BaseBean> beanList;

	private static final int SCREENFLAG = 110;
	private TheInitialScreenData theInitialScreenData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.more_monitor_layout);
		context = ScreensActivity.this;
		getInFrontOf();
		initView();
		initFragment();
		startHandler();
	}

	public static void initParams(List<ScreenModeTypeBean> arrayList) {
		list = arrayList;
	}	
	
//	@SuppressLint("NewApi")
	private void initView() {
		layout = (ScreenTitleLayout) findViewById(R.id.layout);
		LayoutParams lp_warehouse_name = layout.getLayoutParams();
		lp_warehouse_name.height = screenMode.getNavigationHeight();
		layout.setLayoutParams(lp_warehouse_name);
		layout.setView(lp_warehouse_name.height, screenMode.getTextSize(),
				(warehouseName + " " + screenTypeBean.getScreen_text()),
				s.getServer_name(),warehouseId, s.getServerTime());

		theInitialScreenData = TheInitialScreenData.getScreenModess(
				context,
				TheInitialScreenData.getScreenMode(context,
						screenMode.getHeightPixels(),
						screenMode.getWidthPixels()),
				ScreenModeTypeBean.getAllPostSize(list), TheInitialScreenData.fixLongitudinalNum(list));
		
		if(Utility.isNullForList(beanList)){
			beanList = new ArrayList<BaseBean>();
		}else{
			beanList.clear();
		}

		RelativeLayout rt = (RelativeLayout) findViewById(R.id.buju);
		int height = 0;
		int line = 1;
		int h = 0;// 记录屏幕上一行的高度 用于计算margin值
		for (int i = 0; i < list.size(); i++) {
			BaseBean baseBean = new BaseBean();
			ScreenModeTypeBean screenModeTypeBean = list.get(i);
			LinearLayout screenLayout = new LinearLayout(context);
			screenLayout.setBackgroundDrawable(context.getResources()
					.getDrawable(R.drawable.screen_background));
			screenLayout.setId(i + 1);
			// debug
			screenLayout.setTag(baseBean);

			int padding = theInitialScreenData.getPadding();
			screenLayout.setPadding(padding, padding, padding, padding);
			screenLayout.setFocusable(true);
			rt.addView(screenLayout);
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) screenLayout
					.getLayoutParams();
			lp.height = ScreenMonitorTools.getSixLayoutHeight(
					theInitialScreenData, screenModeTypeBean.getScreenPostSize());

			if (line != screenModeTypeBean.getLine()) {
				line = screenModeTypeBean.getLine();
				height = height + h;
			}

			if (screenModeTypeBean.getScreenType() == 0) {
				lp.width = screenMode.getWidthPixels() / 2;
				lp.rightMargin = screenMode.getWidthPixels() / 2
						+ screenMode.getWidthPixels() % 2;
				lp.leftMargin = 0;
			} else if (screenModeTypeBean.getScreenType() == 1) {
				lp.width = screenMode.getWidthPixels() / 2
						+ screenMode.getWidthPixels() % 2;
				lp.rightMargin = 0;
				lp.leftMargin = screenMode.getWidthPixels() / 2;
			} else {
				lp.width = screenMode.getWidthPixels();
			}
			lp.topMargin = height;

			screenLayout.setLayoutParams(lp);
			baseBean.setLayout(screenLayout);
			baseBean.setScreenModeTypeBean(screenModeTypeBean);
			try {
				if(baseBean.getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
					baseBean.setFragment((CommonFragment<?>)(list.get(i).getFragment().newInstance()));
				}else{
					baseBean.setpFragment((CommonProcessingFragment<?>)(list.get(i).getFragment().newInstance()));
				}
			} catch (InstantiationException e) {
				finish();
				break;
			} catch (IllegalAccessException e) {
				finish();
				break;
			}
			beanList.add(baseBean);
			setListener(screenLayout);
			h = lp.height;
		}

		layout.setOnKeyListener(this);
		layout.setOnFocusChangeListener(this);

		// setListener(layout_5,layout_4,layout_3,layout_6,layout_2,layout_1);
	}

	private void initFragment() {

		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		for (int i = 0; i < beanList.size(); i++) {
			Bundle bundle = new Bundle();
			bundle.putString("warehouseId", warehouseId);
			bundle.putSerializable("theInitialScreenData", theInitialScreenData);
			bundle.putBoolean("largeOrSmall", false);
			bundle.putInt("pageNo", 1);
			bundle.putSerializable("serverData", (Serializable) s);
			bundle.putSerializable("allSize",ScreenModeTypeBean.getAllPostSize(list));
			bundle.putSerializable("screenModeTypeBean", list.get(i));

			//判断是否是列表屏幕
			if(beanList.get(i).getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
				beanList.get(i).getFragment().setArguments(bundle);
				transaction.replace(beanList.get(i).getLayout().getId(), beanList.get(i)
						.getFragment());
			}else{
				beanList.get(i).getpFragment().setArguments(bundle);
				transaction.replace(beanList.get(i).getLayout().getId(), beanList.get(i)
						.getpFragment());
			}
		}
		transaction.commitAllowingStateLoss();
	}

	/**
	 * @Description:获取来自于上一个页面的仓库ID
	 * @param
	 */
	private void getInFrontOf() {
		Intent intent = this.getIntent();
		warehouseId = StringUtil.isNullOfStr(intent
				.getStringExtra("warehouseId")) ? "" : intent
				.getStringExtra("warehouseId");
		warehouseName = StringUtil.isNullOfStr(intent
				.getStringExtra("warehouseName")) ? "" : intent
				.getStringExtra("warehouseName");
		s = (ServerTypeBean) intent.getSerializableExtra("serverData");
		screenMode = (ScreenModeBean) intent.getSerializableExtra("screenMode");
		screenTypeBean = (ScreenTypeBean) intent
				.getSerializableExtra("screenTypeBean");
		if (screenMode == null) {
			screenMode = ScreenModeBean.getScreenMode(context);
		}
	}

	@Override
	public Handler getHandler() {
		// TODO Auto-generated method stub
		return handler;
	}

	private void closeThisActivity() {
		setFramentCallbacks(true);
		if(layout!=null){
			layout.setCallbacks(true);
		}
		Intent intent = new Intent(this, ScreenTypeListActivity.class);
		setResult(RESULT_OK, intent);
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		// 如果有焦点就不关闭当前activity 而是取消选中状态
		if (!viewHasFocus) {
			whetherToRun = false;
			closeThisActivity();
		} else {
			viewHasFocus = false;
			loseOnFocusable();
		}
	}

	private void loseOnFocusable() {
		for (int i = 0; i < beanList.size(); i++) {
			BaseBean bean = beanList.get(i);
			if (bean.getLayout() != null) {
				bean.getLayout().setBackgroundDrawable(
						getLayoutBackground(false));
			}
			if(bean.getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
				bean.getFragment().setTopBmg(false);
			}
		}
		layout.requestFocus();
	}

	// 焦点控制
	//注:1>bean需按顺序添加
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {

		// 按键方向
		final int Down = KeyEvent.KEYCODE_DPAD_DOWN;
		final int Up = KeyEvent.KEYCODE_DPAD_UP;
		final int Left = KeyEvent.KEYCODE_DPAD_LEFT;
		final int Right = KeyEvent.KEYCODE_DPAD_RIGHT;

		if (Utility.isNullForList(beanList))
			return false;
		// 仅处理-按下事件
		if (event.getAction() != KeyEvent.ACTION_DOWN)
			return false;

		// title
		if (R.id.layout == v.getId()) {
			// 下/右→焦点移至"第1小屏"
			if (keyCode == Down || keyCode == Right) {
				beanList.get(0).getLayout().requestFocus();
				return true;
			}
			// 上/左→焦点移至"最后小屏"
			if (keyCode == Up || keyCode == Left) {
				beanList.get(beanList.size() - 1).getLayout().requestFocus();
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_ENTER)
				return true;
		}

		if (v.getTag() == null || !(v.getTag() instanceof BaseBean))
			return false;

		BaseBean b = (BaseBean) v.getTag();
		int index = beanList.indexOf(b);
		int row = b.getScreenModeTypeBean().getLine();

		// 目标索引
		int targetIndex = 0;
		// 向左
		if (keyCode == Left) {
			targetIndex=index-1;
			if (targetIndex == -1)
				targetIndex = beanList.size() - 1;
			beanList.get(targetIndex).getLayout().requestFocus();
			return true;
		}
		// 向右
		else if (keyCode == Right) {
			targetIndex=index+1;
			if (targetIndex > beanList.size() - 1)
				targetIndex = 0;
			beanList.get(targetIndex).getLayout().requestFocus();
			return true;
		}
		// 第一行,向上
		else if (row == beanList.get(0).getScreenModeTypeBean().getLine() && keyCode == Up){
			beanList.get(beanList.size() - 1).getLayout().requestFocus();
			return true;
		}
		// 末行,向下
		else if (row == beanList.get(beanList.size() - 1).getScreenModeTypeBean().getLine() && keyCode == Down){
			beanList.get(0).getLayout().requestFocus();
			return true;
		}

		return false;
	}

	@Override
	public void onClick(View v) {
		if (Utility.isNullForList(beanList))
			return;
		if (v.getTag() == null || !(v.getTag() instanceof BaseBean))
			return;
		//--------如果只有一个屏幕那么不需要点击放大
		if(beanList.size()==1){
			return;
		}
		
		whetherToRun = false;
		setFramentCallbacks(true);
		if(layout!=null){
			layout.setCallbacks(true);
		}
		
		BaseBean b = (BaseBean) v.getTag();
		int index = beanList.indexOf(b);

		Intent intent = ScreenModeBean.setIntent(screenMode);
		intent.putExtra("warehouseName", warehouseName);
		intent.putExtra("warehouseId", warehouseId);
		intent.putExtra("serverData", (Serializable)s);
		intent.putExtra("screenTypeBean", (Serializable)screenTypeBean);
		intent.putExtra("theInitialScreenData", (Serializable)theInitialScreenData);
		intent.putExtra("screenModeTypeBean", (Serializable)b.getScreenModeTypeBean());
		intent.putExtra("screenflag", index);
		if(b.getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
			intent.putExtra("pageNo", (b.getFragment()).getBigScreenPageNo(ScreenMonitorTools.TASK_GOINGTOWAREHOUSE_SIZE));
			intent.setClass(context, GeneralLargeScreenActivity.class);
		}else{
			intent.setClass(context, GeneralProcessLargeScreenActivity.class);
		}
		startActivityForResult(intent, SCREENFLAG);
		overridePendingTransition(R.anim.big_left_screen,R.anim.smaller_screen);
		
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (v.getId() != R.id.layout && hasFocus)
			viewHasFocus = true;
		for (int i = 0; i < beanList.size(); i++) {
			BaseBean bean = beanList.get(i);
			if (bean.getLayout() != null) {
				bean.getLayout().setBackgroundDrawable(getLayoutBackground(v.getId() == bean.getLayout().getId()));
			}
			if(bean.getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
				bean.getFragment().setTopBmg(v.getId() == bean.getLayout().getId());
			}
		}
		
		// 添加屏幕切换焦点动画效果
//		AnimationTools.onFocusAnimation(getApplicationContext(), v, hasFocus);
	}

	/**
	 * @Description:对View设置参数
	 * @param @param v
	 */
	private void setListener(View... v) {
		if (v != null && v.length > 0) {
			for (int i = 0; i < v.length; i++) {
				v[i].setOnFocusChangeListener(this);
				v[i].setOnClickListener(this);
				v[i].setOnKeyListener(this);
			}
		}
	}

	private Drawable getLayoutBackground(boolean flag) {
		return context.getResources().getDrawable(
				flag ? R.drawable.screen_background_select
						: R.drawable.screen_background);
	} 

	private void setPageNo(int flag, int pageNo) {
		for(int i=0;i<beanList.size();i++){
			//判断是否是列表屏幕
			if(i==flag&&beanList.get(i).getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
				(beanList.get(i).getFragment()).setPageNo(pageNo);
				break;
			}
		}
	} 
	
	/***********************************************************************************************************/
	
	private int postNum = 0;//用于判断请求失效多少次后执行Connecting的弹窗
	private TimeoutTools timeUtil = new TimeoutTools();
	public int times = 9*1000;
	private boolean whetherToRun = true;// 控制线程的继续运行
	
	private void startHandler() {
		timeUtil.setTimeMS(System.currentTimeMillis());
//		UiTools.SPOT_TIME = System.currentTimeMillis();
		handler.post(spot_task);
		handler.post(alwaysFixSpotHandler);
	}
	
	/**
	 * 请求数据
	 */
	private Runnable spot_task = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (postNum > TimeoutTools.POSTNUM) {
					for (int i = 0; i < beanList.size(); i++) {
						//判断是否是列表屏幕
						if(beanList.get(i).getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
							beanList.get(i).getFragment().alertDialog();
						}else{
							beanList.get(i).getpFragment().alertDialog();
						}
					}
				}
				postNum++;
				getData();
			}
		}
	};

	/**
	 * 监视器用于循环守卫线程是否正常运行
	 */
	private Runnable alwaysFixSpotHandler = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (TimeoutTools.isFastPost(timeUtil,15*1000)) {
					handler.removeCallbacks(spot_task);
					timeUtil.setTimeMS(System.currentTimeMillis());
//					UiTools.SPOT_TIME = System.currentTimeMillis();
					handler.postDelayed(spot_task,times);
				}
				handler.postDelayed(alwaysFixSpotHandler, 1000);
			}
		}
	};
	
	/**
	 * @Description:获取请求数据
	 * @param @param flag
	 */
	private void getData() {
		
		String url = (s != null) ? s.getServer_url() : HttpUrlPath.CheckInWareHouseBigScreenAction;
		
		RequestParams params = new RequestParams();
		params.add("Method", "multipleScreen");
		params.add("ps_id", warehouseId);
		params.add("data", getPostJson().toString());

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				helpParsingJson(json);
				ChooseWhich();
			}

			@Override
			public void handFail() {
				ChooseWhich();
			}
			@Override
			public void versionFail(){
				whetherToRun = false;
			}
			
		}.doGetNotHaveDialog(url, params, this);
	}
	
	/**
	 * @Description:解析请求数据
	 * @param @param json`
	 * @param @param flag
	 */
	public void helpParsingJson(JSONObject json) {
		postNum = 0;//得到回执数据 则清空请求没得到数据的次数
		if(json==null){
			return;
		}
		for (int i = 0; i < beanList.size(); i++) {
			JSONObject j = json.optJSONObject((beanList.get(i).getScreenModeTypeBean().getMethod()).toLowerCase(Locale.ENGLISH));
			
			//-----判断是list列表还是进度列表 true 为list列表
			boolean flagListOrProcess = beanList.get(i).getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen;
			if(flagListOrProcess&&beanList.get(i).getFragment()!=null){
				beanList.get(i).getFragment().whetherToRun = true;
				if(j!=null){
					if (j.optInt("ret") == 1) {
						beanList.get(i).getFragment().helpParsingJson(j,beanList.get(i));
					}else{
						beanList.get(i).setPostFrequency(beanList.get(i).getPostFrequency()+1);
						if (beanList.get(i).getPostFrequency() > TimeoutTools.POSTNUM) {
							beanList.get(i).getFragment().alertDialog();
						}
					}
				}else{
					beanList.get(i).setPostFrequency(beanList.get(i).getPostFrequency()+1);
					if (beanList.get(i).getPostFrequency() > TimeoutTools.POSTNUM) {
						beanList.get(i).getFragment().alertDialog();
					}
				}
			}
			else if(!flagListOrProcess&&beanList.get(i).getpFragment()!=null){
				beanList.get(i).getpFragment().whetherToRun = true;
				if(j!=null){
					if (j.optInt("ret") == 1) {
						beanList.get(i).getpFragment().helpParsingJson(j,beanList.get(i));
					}else{
						beanList.get(i).setPostFrequency(beanList.get(i).getPostFrequency()+1);
						if (beanList.get(i).getPostFrequency() > TimeoutTools.POSTNUM) {
							beanList.get(i).getpFragment().alertDialog();
						}
					}
				}else{
					beanList.get(i).setPostFrequency(beanList.get(i).getPostFrequency()+1);
					if (beanList.get(i).getPostFrequency() > TimeoutTools.POSTNUM) {
						beanList.get(i).getpFragment().alertDialog();
					}
				}
			}
		}
	}
	
	/**
	 * @Description:选择继续运行哪个线程
	 * @param @param flag
	 */
	public void ChooseWhich() {
		if (whetherToRun) {
			handler.removeCallbacks(spot_task);
//			UiTools.SPOT_TIME = System.currentTimeMillis();
			timeUtil.setTimeMS(System.currentTimeMillis());
			handler.postDelayed(spot_task,times);
		}
	}

	/**
	 * @Description:控制线程的运行以及停止
	 * @param @param doRemove 判断是否停止以及移除线程 true移除 false不移除重新运行
	 */
	public void setFramentCallbacks(boolean doRemove) {
		whetherToRun = !doRemove;
		if (doRemove) {
			handler.removeCallbacks(spot_task);
			handler.removeCallbacks(alwaysFixSpotHandler);
		} else {
			startHandler();
		}
	}
	
	/**
	 * @Description:拼装JSON数据 用于发送请求得到屏幕数据
	 */
	private JSONObject getPostJson(){
		JSONObject js = new JSONObject();
		JSONArray ja = new JSONArray();
		try {
			for (int i = 0; i < beanList.size(); i++) {
				JSONObject jObj = new JSONObject();
				jObj.put("method", beanList.get(i).getScreenModeTypeBean().getMethod());
				jObj.put("pageSize", beanList.get(i).getScreenModeTypeBean().getScreenPostSize());

				//判断是否是列表屏幕
				if(beanList.get(i).getScreenModeTypeBean().getScreenStyle()==ScreenModeTypeBean.listScreen){
					jObj.put("pageNo", beanList.get(i).getFragment().pageNo);
				}else{
					jObj.put("pageNo", 1);
				}
				
				ja.put(jObj);
			}
			js.put("data", ja);
			return js;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SCREENFLAG) {
			if (resultCode == RESULT_OK) {
				boolean doRemove = false;
				int pageNo = data.getIntExtra("pageNo", 1);
				int screenflag = data.getIntExtra("screenflag", 1);
				whetherToRun = true;
				setPageNo(screenflag, pageNo);
				setFramentCallbacks(doRemove);
				layout.setCallbacks(doRemove);
			}
		}
	}
}
