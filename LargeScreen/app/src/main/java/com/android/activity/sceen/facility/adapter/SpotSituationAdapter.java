package com.android.activity.sceen.facility.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.facility.model.SpotSituationBean;

public class SpotSituationAdapter extends BaseAdapter {

	private List<SpotSituationBean> arrayList;

	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;

	public SpotSituationAdapter(Context context,
			List<SpotSituationBean> arrayList, int everyItem, int textSize) {
		super();
		resources = context.getResources();
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public SpotSituationBean getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		SpotSituationBaseHoder holder = null;
		if (convertView == null) {
			holder = new SpotSituationBaseHoder();
			convertView = inflater.inflate(
					R.layout.frag_spot_it, null);
			holder.layout = (View) convertView.findViewById(R.id.layout);

			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			holder.tvEntryId = (TextView) convertView.findViewById(R.id.tvEntryId);
			holder.tvEquip= (TextView) convertView.findViewById(R.id.tvEquip);
			holder.tvSpot= (TextView) convertView.findViewById(R.id.tvSpot);
			holder.tvTasks= (TextView) convertView.findViewById(R.id.tvTasks);
			holder.tvInOut= (TextView) convertView.findViewById(R.id.tvInOut);
			holder.tvTime= (TextView) convertView.findViewById(R.id.tvTime);

			holder.tvEntryId.setTextSize(textSize);
			holder.tvEquip.setTextSize(textSize);
			holder.tvSpot.setTextSize(textSize);
			holder.tvTasks.setTextSize(textSize);
			holder.tvInOut.setTextSize(textSize);
			holder.tvTime.setTextSize(textSize);

			convertView.setTag(holder);
		} else {
			holder = (SpotSituationBaseHoder) convertView.getTag();
		}

		SpotSituationBean b = arrayList.get(position);
		
		//debug
//		 b.simulationData=true;
//		 if(position==0)
//		 {
//			 b.gate_container_no="";
//			 b.ctn_number="";
//		 }
//		 else if(position==1)
//		 {
//			 b.gate_container_no="KK";
//			 b.ctn_number="KK";
//		 }
//		 else if(position==2)
//		 {
//			 b.gate_container_no="K2";
//			 b.ctn_number="K3,k4";
//		 }

		// 真数据
		if (b.simulationData) {
//			holder.tvSpot.setText(b.yc_no);
//			holder.tvEntry.setText(b.dlo_id);
//			holder.tvCtnr.setText((TextUtils.isEmpty(b.gate_container_no)?"NA":b.gate_container_no) + " ");
//			holder.tvCtnr2.setText("| "
//					+ ScreenMonitorTools.getCtn_complex(b.ctn_number));
//
//			holder.tvInOut.setText(b.rel_type);
//			holder.tvTime.setText(ScreenMonitorTools.fixTime(b.total_time));
			holder.tvEntryId.setText(b.dlo_id);
			holder.tvEquip.setText(b.equipment_number);
			holder.tvSpot.setText(b.yc_no);
			holder.tvTasks.setText(b.tractor);
			holder.tvInOut.setText(b.rel_type);
			holder.tvTime.setText(ScreenMonitorTools.fixTime(b.total_time));
			
		} else {
			holder.tvEntryId.setText("");
			holder.tvEquip.setText("");
			holder.tvSpot.setText("");
			holder.tvTasks.setText("");
			holder.tvInOut.setText("");
			holder.tvTime.setText("");
		}

		holder.layout.setBackgroundDrawable(resources
				.getDrawable(position % 2 == 0 ? R.drawable.list_center_gray
						: R.drawable.list_center));
		return convertView;
	}

	public void setList(List<SpotSituationBean> arrayList) {
		this.arrayList = arrayList;
	}

}

class SpotSituationBaseHoder {
	public View layout;
	public View layout_id;
//	public TextView tvEntry;
//	public TextView tvCtnr, tvCtnr2;
//	public TextView tvInOut;
//	public TextView tvTime;
//	public TextView tvSpot;
	
	public TextView tvEntryId,tvEquip,tvSpot,tvTasks,tvInOut,tvTime;
	
}