package com.android.activity.sceen.facility;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.facility.adapter.SpotSituationAdapter;
import com.android.activity.sceen.facility.model.SpotSituationBean;
import com.android.core.CommonFragment;

/**
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragCargoOnSpot extends CommonFragment<SpotSituationBean> {

	public static final String method = "cargoOnSpot";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_spot;
	}

	@Override
	public List<SpotSituationBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return SpotSituationBean.getBaseTypeList(json);
	}

	@Override
	public SpotSituationBean newBean() {
		// TODO Auto-generated method stub
		return new SpotSituationBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new SpotSituationAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
