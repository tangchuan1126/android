package com.android.activity.sceen.windows.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

public class WaitingBean implements Serializable {
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -3687902905474106419L;
	public String dlo_id;
	public String tasknumbers;
//	public String gate_driver_liscense;
	public String gate_driver_name;
	public int total_time;
	public boolean simulationData;//判断是否是真数据
	
	public String priority;
	
	public String waitingtype;
	
	/***************************************************************************/
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<WaitingBean> getBaseTypeList(JSONObject json){
		List<WaitingBean> baseList = new ArrayList<WaitingBean>();
		JSONArray dockSituationBaseArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(dockSituationBaseArrays));
		if(flag){
			for (int i = 0; i < dockSituationBaseArrays.length(); i++) {
				JSONObject jsonItem = dockSituationBaseArrays.optJSONObject(i);
				WaitingBean bean = new WaitingBean();
				
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.tasknumbers = jsonItem.optString("tasknumbers");
//				bean.gate_driver_liscense = jsonItem.optString("gate_driver_liscense");
				bean.gate_driver_name = jsonItem.optString("gate_driver_name");
				bean.total_time = jsonItem.optInt("total_time");
				bean.waitingtype = jsonItem.optString("waitingtype");
				
				bean.priority = jsonItem.optString("priority");
				
				bean.simulationData = true;
				
				baseList.add(bean);
			}
			
			return baseList;
		}
		return null;
	}

	
}