package com.android.activity.sceen.scanning.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: ReceiveScanBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-23 下午3:05:08
 */
public class NeedAssignBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5634489580628814703L;
	
	/*****************************************/
	public String employe_name;//Operator
	public String item_id;//Item
	public String schedule_id;//Task Number
	public int total_time;
	public String task_type;//Task Type
	public int qty;
	public String company_id;

	public boolean simulationData;//判断是否是真数据
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<NeedAssignBean> getBaseTypeList(JSONObject json){
		List<NeedAssignBean> list = new ArrayList<NeedAssignBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				NeedAssignBean bean = new NeedAssignBean();	
				
				bean.task_type = jsonItem.optString("task_type");
				bean.schedule_id = jsonItem.optString("schedule_id");
				bean.item_id = jsonItem.optString("item_id");
				bean.employe_name = jsonItem.optString("employe_name");
				bean.total_time = jsonItem.optInt("total_time");
				bean.qty = jsonItem.optInt("qty");
				bean.company_id = jsonItem.optString("company_id");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}
	
	
}
