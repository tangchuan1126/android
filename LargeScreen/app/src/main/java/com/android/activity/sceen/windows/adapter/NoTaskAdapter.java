package com.android.activity.sceen.windows.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.windows.model.NoTaskBean;

public class NoTaskAdapter extends BaseAdapter {

	private List<NoTaskBean> arrayList;

	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public NoTaskAdapter(Context context, List<NoTaskBean> arrayList,
			int everyItem, int textSize) {
		super();
		this.context = context;
		resources = context.getResources();
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public NoTaskBean getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NoTaskHoder holder = null;
		if (convertView == null) {
			holder = new NoTaskHoder();
			convertView = inflater.inflate(R.layout.no_task_entry_listview_item, null);
			holder.layout = (View) convertView.findViewById(R.id.layout);

			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
			holder.entry_id.setTextSize(textSize);
			holder.location = (TextView) convertView.findViewById(R.id.location);
			holder.location.setTextSize(textSize);
			holder.tractor = (TextView) convertView.findViewById(R.id.tractor);
			holder.tractor.setTextSize(textSize);
			holder.trailer = (TextView) convertView.findViewById(R.id.trailer);
			holder.trailer.setTextSize(textSize);
			holder.timevalue = (TextView) convertView.findViewById(R.id.timevalue);
			holder.timevalue.setTextSize(textSize);

			holder.lolocation = convertView.findViewById(R.id.lolocation);
			// location宽
			LayoutParams lp1 = holder.lolocation.getLayoutParams();
			lp1.width = (int) Utility.getTextLen(DisplayUtil.sp2px(context, textSize * 7 / 8),resources.getString(R.string.task_remain_jobs_location));
			ViewGroup.MarginLayoutParams lp2 = (ViewGroup.MarginLayoutParams) holder.location.getLayoutParams();
			lp2.leftMargin = -(int) Utility.getTextLen(DisplayUtil.sp2px(context, textSize), "    ");

			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
			convertView.setTag(holder);
		} else {
			holder = (NoTaskHoder) convertView.getTag();
		}

		NoTaskBean temp = arrayList.get(position);
		

//		public TextView entry_id;
//		public TextView location;
//		public TextView tractor;
//		public TextView trailer;
//		public TextView timevalue;
		holder.entry_id.setText(temp.simulationData ? temp.dlo_id : "");
		holder.location.setText(temp.simulationData ? temp.locations : "");
		holder.tractor.setText(temp.simulationData ? temp.tractor : "");
		holder.trailer.setText(temp.simulationData ? temp.trailer : "");
		holder.timevalue.setText(temp.simulationData ? ScreenMonitorTools
				.fixTime(temp.total_time) : "");

		if(temp.simulationData&&temp.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}
		
		holder.layout.setBackgroundDrawable(resources
				.getDrawable(position % 2 == 0 ? R.drawable.list_center_gray
						: R.drawable.list_center));
		return convertView;
	}

	public void setList(List<NoTaskBean> arrayList) {
		this.arrayList = arrayList;
	}

}

class NoTaskHoder {
	public View layout;
	public View layout_id, lolocation;
	public TextView entry_id;
	public TextView location;
	public TextView tractor;
	public TextView trailer;
	public TextView timevalue;
	
	public View color_layout;
}