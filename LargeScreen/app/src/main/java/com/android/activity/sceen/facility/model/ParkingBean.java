package com.android.activity.sceen.facility.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: DockCloseNoCheckOutBean 
 * @Description: DockClose 的bean类 但是没有checkout
 * @author gcy
 * @date 2014-9-26 上午9:48:50
 */
public class ParkingBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1302633291659553365L;
	
	public String location;
	public String dlo_id;
	public String ctnrs;
	public String equipment_number;
	public String company_name;
//	public String gate_driver_liscense;
	public String gate_driver_name;
	
	public boolean simulationData;//判断是否是真数据
	
	/*********************************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<ParkingBean> getBaseTypeList(JSONObject json){
		List<ParkingBean> dockCloseNoCheckOutBeanList = new ArrayList<ParkingBean>();
		JSONArray dockCloseNoCheckOutBeanArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(dockCloseNoCheckOutBeanArrays));
		if(flag){
			for (int i = 0; i < dockCloseNoCheckOutBeanArrays.length(); i++) {
				JSONObject jsonItem = dockCloseNoCheckOutBeanArrays.optJSONObject(i);
				ParkingBean b = new ParkingBean();				
				b.location=jsonItem.optString("location");
				b.dlo_id=jsonItem.optString("dlo_id");
				b.equipment_number=jsonItem.optString("equipment_number");
				b.ctnrs=jsonItem.optString("ctnrs");
				b.company_name=jsonItem.optString("company_name");
//				b.gate_driver_liscense=jsonItem.optString("gate_driver_liscense");
				b.gate_driver_name=jsonItem.optString("gate_driver_name");
				
				b.simulationData=true;
				dockCloseNoCheckOutBeanList.add(b);
			}			
			return dockCloseNoCheckOutBeanList;
		}
		return null;
	}

}
