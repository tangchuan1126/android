package com.android.activity.process.model;

import java.io.Serializable;


/**
 * @ClassName: StepDataBean 
 * @Description: 用于跳转大屏幕的菜单
 * @author gcy
 * @date 2014-12-29 下午8:51:19
 */
public class StepDataBean implements Serializable{
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -7476094447462286343L;
	public final static int LoadReceiveProcessShowColorRed = 1 ;			//显示红色
	public final static int LoadReceiveProcsssShowColorYellow = 2; 		//显示黄色
	public final static int LoadReceiveProcsssShowColorGreen = 3; 		//显示绿色
	
	public int count;//流程名称
	public int total;//流程名称
	public int color;//流程名称
	
	public StepDataBean(int count,int total,int color){
		this.count = count;
		this.total = total;
		this.color = color;
	}
	
	public StepDataBean(int color){this.color = color;}
	
}
