package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * @ClassName: PlatformAdvertisingBaseType
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class RemainJobsBean implements Serializable {

	private static final long serialVersionUID = -3100379055033386475L;
	/*****************************************/

	public String dlo_id;
	public String labor;
	public int total_time;
	public String equipment_number;
	public String tasks;
	public int is_late;
	
	public String location;

	public boolean simulationData;//判断是否是真数据 true为真数据 false为假数据
	//=============================================

	
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<RemainJobsBean> getBaseTypeList(JSONObject json){
		List<RemainJobsBean> list = new ArrayList<RemainJobsBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				RemainJobsBean bean = new RemainJobsBean();
//				bean.setNumber_type(jsonItem.optInt("number_type"));
//				bean.setNumber(jsonItem.optString("number"));
//				bean.setRel_type(jsonItem.optString("rel_type"));
//				bean.setDlo_id(jsonItem.optString("dlo_id"));
//				bean.setCtnr_number(jsonItem.optString("ctn_number","NA"));
//				bean.setLocation(jsonItem.optString("target"));
//				bean.setIs_late(jsonItem.optInt("is_late"));
//				bean.setTotal_time(jsonItem.optInt("total_time"));
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.labor=jsonItem.optString("labor");
				bean.total_time=jsonItem.optInt("total_time");
				bean.equipment_number=jsonItem.optString("equipment_number");
				bean.tasks=jsonItem.optString("tasks");
				bean.is_late=jsonItem.optInt("is_late");
				bean.location=jsonItem.optString("location");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}

}