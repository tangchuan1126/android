package com.android.activity.sceen.task.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.GoingToWareHouseBean;

public class GoingToWareHouseAdapter extends BaseAdapter  {
	
	private List<GoingToWareHouseBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public GoingToWareHouseAdapter(Context context, List<GoingToWareHouseBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public GoingToWareHouseBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		GoingToWareHouseHoder holder = null;
		 if(convertView==null){
			holder = new GoingToWareHouseHoder();
			convertView = inflater.inflate(R.layout.going_to_warehouse_listview_item,null);

			
			holder.layout=convertView;
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.loLocation=convertView.findViewById(R.id.loLocation); 
			holder.tvEntryId=(TextView)convertView.findViewById(R.id.tvEntryId); 
			holder.tvLoc_it=(TextView)convertView.findViewById(R.id.tvLoc_it); 
			holder.tvEquip_it=(TextView)convertView.findViewById(R.id.tvEquip_it);
			holder.tvTasks_it=(TextView)convertView.findViewById(R.id.tvTasks_it);
			holder.tvStartTime=(TextView)convertView.findViewById(R.id.tvStartTime);
			holder.waiting=(TextView)convertView.findViewById(R.id.waiting);
			holder.priority=(TextView)convertView.findViewById(R.id.priority);
			
			
			holder.tvEntryId.setTextSize(textSize);
			holder.tvLoc_it.setTextSize(textSize);
			holder.tvEquip_it.setTextSize(textSize);
			holder.tvTasks_it.setTextSize(textSize);
			holder.tvStartTime.setTextSize(textSize);
			holder.waiting.setTextSize(textSize);
			holder.priority.setTextSize(textSize);
			
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,"Location");
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.tvLoc_it.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
 			convertView.setTag(holder);
		}else{
			holder = (GoingToWareHouseHoder) convertView.getTag();
		}
		 
		GoingToWareHouseBean temp  =  arrayList.get(position);
				
		if(temp.simulationData){
			holder.tvEntryId.setText(temp.dlo_id);
			holder.tvLoc_it.setText(temp.location);
			holder.tvEquip_it.setText(temp.equipment_number);
			holder.tvTasks_it.setText(temp.tasks);
			holder.tvStartTime.setText(ScreenMonitorTools.fixTime(temp.total_time));
			holder.waiting.setText(temp.waiting);
			holder.priority.setText(temp.priority);
		}
		else
		{
			holder.tvEntryId.setText("");
			holder.tvLoc_it.setText("");
			holder.tvEquip_it.setText("");
			holder.tvTasks_it.setText("");
			holder.tvStartTime.setText("");
			holder.waiting.setText("");
			holder.priority.setText("");
		}
		
		if(temp.simulationData&&temp.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<GoingToWareHouseBean> arrayList){
		this.arrayList = arrayList;
	}
}
class GoingToWareHouseHoder {
	public View color_layout;
	
	public View layout,layout_id,loLocation;
	public TextView tvEntryId,tvLoc_it,tvEquip_it,tvTasks_it,tvStartTime,waiting,priority;
	
}