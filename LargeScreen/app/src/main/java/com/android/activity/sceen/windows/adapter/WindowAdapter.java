package com.android.activity.sceen.windows.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.windows.model.WindowBean;

public class WindowAdapter extends BaseAdapter  {
	
	private List<WindowBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public WindowAdapter(Context context, List<WindowBean> arrayList ,int everyItem,int textSize) {
		super();
		this.context = context;
		resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public WindowBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WindowBaseHoder holder = null;
		 if(convertView==null){
			holder = new WindowBaseHoder();
			convertView = inflater.inflate(R.layout.window_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);		
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			
			
			holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
			holder.entry_id.setTextSize(textSize);
			holder.tractor = (TextView) convertView.findViewById(R.id.tractor);
			holder.tractor.setTextSize(textSize);
			holder.lb = (TextView) convertView.findViewById(R.id.lb);
			holder.lb.setTextSize(textSize);
			holder.equipment = (TextView) convertView.findViewById(R.id.equipment);
			holder.equipment.setTextSize(textSize);
			holder.location = (TextView) convertView.findViewById(R.id.location);
			holder.location.setTextSize(textSize);
//			holder.task = (TextView) convertView.findViewById(R.id.task);
//			holder.task.setTextSize(textSize);
			holder.rel_type = (TextView) convertView.findViewById(R.id.rel_type);
			holder.rel_type.setTextSize(textSize);
			holder.timevalue = (TextView) convertView.findViewById(R.id.timevalue);
			holder.timevalue.setTextSize(textSize);
			
//			holder.loLocation=convertView.findViewById(R.id.lolocation);
//			//location宽
//			LayoutParams lp1=holder.loLocation.getLayoutParams();
//			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,resources.getString(R.string.task_remain_jobs_location));
//			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.location.getLayoutParams();
//			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	

 			convertView.setTag(holder);
		}else{
			holder = (WindowBaseHoder) convertView.getTag();
		}
		 
		WindowBean temp  =  arrayList.get(position);
				
		holder.entry_id.setText(temp.simulationData?temp.dlo_id:"");
		holder.tractor.setText(temp.simulationData?temp.tractor:"");
		holder.lb.setText(temp.simulationData?temp.equipment_purpose:"");
		holder.equipment.setText(temp.simulationData?temp.trailer:"");
		holder.location.setText(temp.simulationData?temp.locations:"");
//		holder.task.setText(temp.simulationData?temp.tasks:"");
		holder.rel_type.setText(temp.simulationData?temp.rel_type:"");
		holder.timevalue.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");
		
		if(temp.simulationData&&temp.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}

	public void setList(List<WindowBean> arrayList){
		this.arrayList = arrayList;
	}
	
}
class WindowBaseHoder {
	public View layout;
	public View layout_id,loLocation;
	public TextView entry_id;	
	public TextView tractor;
	public TextView equipment;
	public TextView location;	
	public TextView lb;	
//	public TextView task;  
	public TextView rel_type;
	public TextView timevalue;
	
	public View color_layout;
}