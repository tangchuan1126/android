package com.android.activity.sceen.windows;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.windows.adapter.WindowAdapter;
import com.android.activity.sceen.windows.model.WindowBean;
import com.android.core.CommonFragment;

public class WindowFrament extends CommonFragment<WindowBean> {
	
	public static final String method = "goingToWindow";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.window_layout_frament;
	}

	@Override
	public List<WindowBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return WindowBean.getBaseTypeList(json);
	}

	@Override
	public WindowBean newBean() {
		// TODO Auto-generated method stub
		return new WindowBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new WindowAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}
	
}
