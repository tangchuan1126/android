package com.android.activity.platform.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.activity.platform.model.WarehouseInformationBean;

public class WarehouseAdapter extends BaseAdapter {

	private List<WarehouseInformationBean> arrayList;
	private Context context;
	private LayoutInflater inflater;
	private int selectViewNum = -1;
	private int everySize;
	
	public WarehouseAdapter(Context context,List<WarehouseInformationBean> arrayList,int everySize) {
		super();
		this.context = context;
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
		this.everySize = everySize;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public WarehouseInformationBean getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WarehouseInformationHoder holder = null;
		if (convertView == null) {
			holder = new WarehouseInformationHoder();
			convertView = inflater.inflate(R.layout.warehouse_item, null);
			holder.warehouse_id = (TextView) convertView.findViewById(R.id.screen_text);
			holder.screen_image = (ImageView) convertView.findViewById(R.id.screen_image);
			holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
			convertView.setTag(holder);
		} else {
			holder = (WarehouseInformationHoder) convertView.getTag();
		}
		WarehouseInformationBean temp = arrayList.get(position);
		int text_height = everySize/7*2;
		LayoutParams lp_i = holder.screen_image.getLayoutParams();
		lp_i.width = everySize;
		lp_i.height = everySize;
		holder.screen_image.setLayoutParams(lp_i);
		
		LayoutParams lp_l = holder.layout.getLayoutParams();
		lp_l.width = everySize;
		lp_l.height = everySize+text_height/2;
		holder.layout.setLayoutParams(lp_l);
		
		LayoutParams lp_t = holder.warehouse_id.getLayoutParams();
		lp_t.width = everySize;
		lp_t.height = text_height;
		holder.warehouse_id.setLayoutParams(lp_t);
		int textSize = DisplayUtil.px2sp(context, text_height/2);
		holder.warehouse_id.setTextSize(textSize);
		
		holder.warehouse_id.setText(temp.getWarehouseName());
		holder.screen_image.setBackgroundDrawable(context.getResources().getDrawable(selectViewNum==position?R.drawable.warehouse_icon_selected:R.drawable.warehouse_icon));
		if(selectViewNum==position){
			holder.warehouse_id.setTextColor(context.getResources().getColor(R.color.select_color));
//			Animation testAnim = AnimationUtils.loadAnimation(context, R.anim.big_screen);
//			convertView.startAnimation(testAnim);
		}else{
			holder.warehouse_id.setTextColor(context.getResources().getColor(R.color.normal_color));
//			Animation testAnim = AnimationUtils.loadAnimation(context, R.anim.small_screen);
//			convertView.startAnimation(testAnim);
		}
		return convertView;
	}
	
	public void setSelected(int selectNum){
		selectViewNum = selectNum;
		super.notifyDataSetChanged();
	}
	public int getSelected(){
		return selectViewNum;
	}
}

class WarehouseInformationHoder {
	public ImageView screen_image;
	public TextView warehouse_id;
	public LinearLayout layout;
}
