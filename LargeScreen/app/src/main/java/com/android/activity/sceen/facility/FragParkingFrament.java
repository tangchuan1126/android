package com.android.activity.sceen.facility;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.facility.adapter.ParkingAdapter;
import com.android.activity.sceen.facility.model.ParkingBean;
import com.android.core.CommonFragment;

/**
 * @ClassName: DockCloseFrament
 * @Description: 用于显示DockClose中没有CheckOut的数据
 * @author gcy
 * @date 2014-9-26 上午9:52:12
 */
public class FragParkingFrament extends CommonFragment<ParkingBean> {
	
	public static final String method = "parking3rdPartyNone";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.fc_parking_layout_frament;
	}

	@Override
	public List<ParkingBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return ParkingBean.getBaseTypeList(json);
	}

	@Override
	public ParkingBean newBean() {
		// TODO Auto-generated method stub
		return new ParkingBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new ParkingAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}
}
