package com.android.activity.sceen.scanning;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.scanning.adapter.NeedAssignAdapter;
import com.android.activity.sceen.scanning.model.NeedAssignBean;
import com.android.core.CommonFragment;

public class NeedAssign extends CommonFragment<NeedAssignBean> {

	public static final String method = "ccschedule";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.scanning_need_assign;
	}

	@Override
	public List<NeedAssignBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return NeedAssignBean.getBaseTypeList(json);
	}

	@Override
	public NeedAssignBean newBean() {
		// TODO Auto-generated method stub
		return new NeedAssignBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new NeedAssignAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
