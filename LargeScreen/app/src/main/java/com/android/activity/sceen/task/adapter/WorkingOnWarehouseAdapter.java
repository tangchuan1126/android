package com.android.activity.sceen.task.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.WorkingOnWarehouseBean;
import com.android.common.progresswheel.PieProgress;
import com.android.common.ui.BadgeView;

public class WorkingOnWarehouseAdapter extends BaseAdapter  {
	
	private List<WorkingOnWarehouseBean> arrayList;
	private Context context;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;

	public WorkingOnWarehouseAdapter(Context context, List<WorkingOnWarehouseBean> arrayList ,int everyItem,int textSize) {
		super();
		this.context = context;
		resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public WorkingOnWarehouseBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ThreePlatformAdvertisingAdapterHoder holder = null;
		 if(convertView==null){
			holder = new ThreePlatformAdvertisingAdapterHoder();
			convertView = inflater.inflate(R.layout.working_on_warehouse_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);		
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.number_type = (TextView) convertView.findViewById(R.id.number_type);
			holder.number_type.setTextSize(textSize);	
			holder.number = (TextView) convertView.findViewById(R.id.number);
			holder.number.setTextSize(textSize);
			holder.dlo_id = (TextView) convertView.findViewById(R.id.dlo_id); 
			holder.dlo_id.setTextSize(textSize);
			
			holder.loader = (TextView) convertView.findViewById(R.id.loader); 
			holder.loader.setTextSize(textSize);
			holder.account_ts = (View) convertView.findViewById(R.id.account_ts); 
			


			
			holder.equipment_number = (TextView) convertView.findViewById(R.id.equipment_number); 
			holder.equipment_number.setTextSize(textSize);
			holder.location = (TextView) convertView.findViewById(R.id.location); 
			holder.location.setTextSize(textSize);
			

			holder.loLocation=convertView.findViewById(R.id.lolocation);
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,resources.getString(R.string.task_remain_jobs_location));
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.location.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
			
//			holder.rel_type = (TextView) convertView.findViewById(R.id.rel_type); 
//			holder.rel_type.setTextSize(textSize);
			
			holder.donecount = (TextView) convertView.findViewById(R.id.donecount); 
			holder.donecount.setTextSize(textSize);//剩余pallet数量    
			holder.donecount_percentage = (PieProgress) convertView.findViewById(R.id.donecount_percentage); 		
			LayoutParams lp_d = holder.donecount_percentage.getLayoutParams();
			lp_d.height = everyItem;
			lp_d.width = everyItem;
			holder.donecount_percentage.setLayoutParams(lp_d);
			
			holder.total_pallet = (TextView) convertView.findViewById(R.id.total_pallet);
			holder.total_pallet.setTextSize(textSize);	
			holder.total_time = (TextView) convertView.findViewById(R.id.total_time);
			holder.total_time.setTextSize(textSize);
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 			
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
			holder.badge = new BadgeView(context,holder.account_ts);
			holder.badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
			holder.badge.setTextSize(textSize/2);
			holder.badge.setText("help");
			holder.badge.setTextColor(Color.WHITE);
			
 			convertView.setTag(holder);
		}else{
			holder = (ThreePlatformAdvertisingAdapterHoder) convertView.getTag();
		}
		 

		
		WorkingOnWarehouseBean temp  =  arrayList.get(position);
		
		
		holder.number_type.setText(temp.simulationData?temp.number_type:"");
		holder.number.setText(temp.simulationData?temp.number:"");
		holder.dlo_id.setText(temp.simulationData?temp.dlo_id:"");
		holder.loader.setText(temp.simulationData?temp.labor:"");
		holder.equipment_number.setText(temp.simulationData?temp.equipment_number:"");
		holder.location.setText(temp.simulationData?temp.location:"");
//		holder.rel_type.setText(temp.simulationData?temp.rel_type:"");
		holder.donecount.setText(temp.simulationData?String.valueOf(temp.donecount):"");//剩余pallet数量    	
		holder.total_pallet.setText(temp.simulationData?String.valueOf(temp.total_pallet):"");	
		holder.total_time.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");

		
		if(temp.flagHelp==0){
			holder.badge.hide();
		}else{
			holder.badge.show();
		}
		
		if(temp.simulationData){
			holder.donecount.setVisibility(View.VISIBLE);
			holder.donecount_percentage.setVisibility(View.VISIBLE);
		}else{
			holder.badge.hide();
			holder.donecount.setVisibility(View.GONE);
			holder.donecount_percentage.setVisibility(View.GONE);
		}
		
		holder.donecount.setText((int)temp.donecount+"%");
		holder.donecount_percentage.setProgress((int)(temp.donecount/100*360));

		
		if(temp.simulationData&&temp.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
		
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
		
		return convertView;
	}
	
	public void setList(List<WorkingOnWarehouseBean> arrayList){
		this.arrayList = arrayList;
	}
}
class ThreePlatformAdvertisingAdapterHoder {
	public View layout;
	public View layout_id,loLocation;
	
	public TextView number_type;
	public TextView number;
	public TextView dlo_id;
	public TextView loader;
	public View account_ts;
	public TextView equipment_number;
	public TextView location;
//	public TextView rel_type;
//	public TextView donecount;//完成度百分比
	public TextView total_pallet;
	public TextView total_time;;
	
	public TextView donecount;//剩余pallet数量    
	public PieProgress donecount_percentage;

	public BadgeView badge;
	
	
	public View color_layout;
}

