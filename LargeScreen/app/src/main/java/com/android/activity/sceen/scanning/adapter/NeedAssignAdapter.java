package com.android.activity.sceen.scanning.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.scanning.model.NeedAssignBean;

public class NeedAssignAdapter extends BaseAdapter  {
	
	private List<NeedAssignBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public NeedAssignAdapter(Context context, List<NeedAssignBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public NeedAssignBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NeedAssignHoder holder = null;
		 if(convertView==null){
			holder = new NeedAssignHoder();
			convertView = inflater.inflate(R.layout.scanning_need_assign_item,null);

			holder.layout=convertView;
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.t_task_type=(TextView)convertView.findViewById(R.id.t_task_type); 			
			holder.t_task_type.setTextSize(textSize);
			holder.t_task_num=(TextView)convertView.findViewById(R.id.t_task_num); 			
			holder.t_task_num.setTextSize(textSize);
			holder.t_item=(TextView)convertView.findViewById(R.id.t_item);			
			holder.t_item.setTextSize(textSize);
			holder.t_time=(TextView)convertView.findViewById(R.id.t_time);			
			holder.t_time.setTextSize(textSize);
			holder.t_operator=(TextView)convertView.findViewById(R.id.t_operator);			
			holder.t_operator.setTextSize(textSize);
			holder.t_qty=(TextView)convertView.findViewById(R.id.t_qty);			
			holder.t_qty.setTextSize(textSize);
			holder.t_company=(TextView)convertView.findViewById(R.id.t_company);			
			holder.t_company.setTextSize(textSize);

			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
 			convertView.setTag(holder);
		}else{
			holder = (NeedAssignHoder) convertView.getTag();
		}
		 
		NeedAssignBean temp  =  arrayList.get(position);
		
		holder.t_task_type.setText(temp.simulationData?temp.task_type:"");
		holder.t_task_num.setText(temp.simulationData?temp.schedule_id:"");
		holder.t_item.setText(temp.simulationData?temp.item_id:"");
		holder.t_time.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");
		holder.t_operator.setText(temp.simulationData?temp.employe_name:"");
		holder.t_qty.setText(temp.simulationData?(temp.qty+""):"");
		holder.t_company.setText(temp.simulationData?temp.company_id:"");
		
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<NeedAssignBean> arrayList){
		this.arrayList = arrayList;
	}
}
class NeedAssignHoder {
	public View color_layout;
	
	public View layout,layout_id;
	public TextView t_task_type,t_task_num,t_item,t_time,t_operator,t_qty,t_company;
	
}