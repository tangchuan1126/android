package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * @ClassName: SelectGateCheckInBaseType
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class NoTaskLaborBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -1712821933287097674L;

	/*****************************************/
	public String employe_name;
	
	public boolean simulationData;//判断是否是真数据
	public int total_time;
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<NoTaskLaborBean> getBaseTypeList(JSONObject json){
		List<NoTaskLaborBean> list = new ArrayList<NoTaskLaborBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				NoTaskLaborBean bean = new NoTaskLaborBean();	
				
				bean.employe_name=jsonItem.optString("employe_name");
				bean.total_time = jsonItem.optInt("total_time");
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}
}
