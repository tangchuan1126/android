package com.android.activity.common;

import java.io.Serializable;
import java.util.List;

import util.Utility;
/**
 * @ClassName: ScreenModeTypeBean 
 * @Description: 拥有屏幕的类型以及基础样式
 * @author gcy
 * @date 2014-9-25 下午4:38:43
 */
/**
 * @ClassName: ScreenModeTypeBean 
 * @Description: 拥有屏幕的类型以及基础样式
 * @author gcy
 * @date 2014-9-25 下午4:38:43
 */
public class ScreenModeTypeBean implements Serializable  {
	
	public static final int Col_Left=0;			//所在栏,左、右、全屏
	public static final int Col_Right=1;
	public static final int Col_All=2;
	
	private static final long serialVersionUID = 2103324819698331490L;
//	private CommonFragment<?> fragment;
	private Class<?> fragment;
	private String screenName;
	private int screenPostSize;
	private int screenType;//取Col_x,0 左半屏半屏宽  1 右半屏  2 全屏宽  
	private int line;
	private String method;
//	private LinearLayout layout;
	
	public static int listScreen = 1;//列表屏幕
	public static int processScreen = 2;//进度屏幕
	private int screenStyle= ScreenModeTypeBean.listScreen;//用于该屏幕的基础数据是 列表屏幕还是进度屏幕 默认值为列表屏幕	
	
	
	public static int getAllPostSize(List<ScreenModeTypeBean> list){
		int num = 0;
		if(!Utility.isNullForList(list)){
			int line = 0;
			for(int i=0;i<list.size();i++){
				if(line!=list.get(i).getLine()){
					line =list.get(i).getLine();
					ScreenModeTypeBean bean = list.get(i);
					num+=bean.getScreenPostSize()+2;
				}
			}
		}
		return num;
	}
	
	/*********************************************************************************************/
	
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public int getScreenPostSize() {
		return screenPostSize;
	}
	public void setScreenPostSize(int screenPostSize) {
		this.screenPostSize = screenPostSize;
	}
	public int getScreenType() {
		return screenType;
	}
	public void setScreenType(int screenType) {
		this.screenType = screenType;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
//	public CommonFragment<?> getFragment() {
//		return fragment;
//	}
//	public void setFragment(CommonFragment<?> fragment) {
//		this.fragment = fragment;
//	}
//	public LinearLayout getLayout() {
//		return layout;
//	}
//	public void setLayout(LinearLayout layout) {
//		this.layout = layout;
//	}

	public Class<?> getFragment() {
		return fragment;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public void setFragment(Class<?> fragment) {
		this.fragment = fragment;
	}

	public int getScreenStyle() {
		return screenStyle;
	}

	public void setScreenStyle(int screenStyle) {
		this.screenStyle = screenStyle;
	}
	
	
}
