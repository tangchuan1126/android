package com.android.activity.platform.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import util.HttpUrlPath;
import util.UiTools;

public class ServerTypeBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	
	private String server_name;
	private String server_url;
	
	

	public static final String Sync10 = HttpUrlPath.getCheckInWareHouseBigScreenAction;
	public static final String MicroServices = HttpUrlPath.MicroServices;
	
	private String url_type = Sync10;
	
	public static List<ServerTypeBean> initServerList(){ 
		List<ServerTypeBean> l = new ArrayList<ServerTypeBean>();
		

		
		ServerTypeBean USA43 = new ServerTypeBean();
		USA43.setServer_url("http://sync.logisticsteam.com/Sync10/");
		USA43.setServer_name("USA Server AWS");
		l.add(USA43);
		
		if(UiTools.DEBUG){
			
//			ServerTypeBean usa = new ServerTypeBean();
//			usa.setServer_url("http://192.198.208.38/Sync10/");
//			usa.setServer_name("USA");
//			l.add(usa);
//			
//			ServerTypeBean zhangrui = new ServerTypeBean();
//			zhangrui.setServer_url("http://192.168.1.214:8000/Sync10/");
//			zhangrui.setServer_name("Zhang Rui");
//			l.add(zhangrui);
//			
//			ServerTypeBean zhangyanjie = new ServerTypeBean();
//			zhangyanjie.setServer_url("http://192.168.1.220:8080/Sync10/");
//			zhangyanjie.setServer_name("Zhang Yan Jie");
//			l.add(zhangyanjie);
//						
//			ServerTypeBean chenchen = new ServerTypeBean();
//			chenchen.setServer_url("http://192.168.1.170/Sync10/");
//			chenchen.setServer_name("Chen chen");
//			l.add(chenchen);
			
//			ServerTypeBean xujia = new ServerTypeBean();
//			xujia.setServer_url("http://192.168.1.176:8080/Sync10/");
//			xujia.setServer_name("Xu Jia");
//			l.add(xujia);
			
//			ServerTypeBean zhanjie = new ServerTypeBean();
//			zhanjie.setServer_url("http://192.168.1.156:8080/Sync10/");
//			zhanjie.setServer_name("Zhan Jie");
//			l.add(zhanjie);
			
			ServerTypeBean server15 = new ServerTypeBean();
			server15.setServer_url("http://sync-test.logisticsteam.com/Sync10/");
			server15.setServer_name("Sync AWS Test");
			l.add(server15);

			ServerTypeBean sTestW = new ServerTypeBean();
			sTestW.setServer_url("http://sync-test-walnut.logisticsteam.com/Sync10/");
			sTestW.setServer_name("Sync AWS Test Walnut");
			l.add(sTestW);

		}
		
		return l;
	}
	
	public String getServer_name() {
		return server_name;
	}
	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}
	public String getServer_url() {
		return server_url + url_type;
	}
	
	public String getServerTime() {
		return server_url + HttpUrlPath.getCheckInWareHouseBigScreenAction;
	}
	
	public String getUrl() {
		return server_url;
	}
	
	public void setServer_url(String server_url) {
		this.server_url = server_url;
	}

	public String getUrl_type() {
		return url_type;
	}

	public void setUrl_type(String url_type) {
		this.url_type = url_type;
	}
	
	
}
