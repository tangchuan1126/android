package com.android.activity.sceen.windows.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.sync.model.LoadReceiveNotFinishBean;

public class WindowLoadReceiveNotFinishAdapter extends BaseAdapter  {
	
	private Context context;
	
	private List<LoadReceiveNotFinishBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;

	private int everyItem;
	private int textSize;

	public WindowLoadReceiveNotFinishAdapter(Context context, List<LoadReceiveNotFinishBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
		this.context=context;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public LoadReceiveNotFinishBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WindowLoadReceiveNotFinishHoder holder = null;
		 if(convertView==null){
			holder = new WindowLoadReceiveNotFinishHoder();
			convertView = inflater.inflate(R.layout.window_load_receive_not_finish_list_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);	
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			
//			holder.title = (TextView)convertView.findViewById(R.id.title);
			holder.system_type = (TextView)convertView.findViewById(R.id.system_type);
			holder.number = (TextView)convertView.findViewById(R.id.number);
//			holder.account_id = (TextView)convertView.findViewById(R.id.account_id);
			holder.company_id = (TextView)convertView.findViewById(R.id.company_id);
//			holder.customer_id = (TextView)convertView.findViewById(R.id.customer_id);
			holder.total_time = (TextView)convertView.findViewById(R.id.total_time);
			holder.entry_id = (TextView)convertView.findViewById(R.id.entry_id);
			
			holder.lb = (TextView)convertView.findViewById(R.id.lb);	
			holder.rel_type = (TextView)convertView.findViewById(R.id.rel_type);
			
//			holder.title.setTextSize(textSize);
			holder.system_type.setTextSize(textSize);
			holder.number.setTextSize(textSize);
//			holder.account_id.setTextSize(textSize);
			holder.company_id.setTextSize(textSize);
//			holder.customer_id.setTextSize(textSize);
			holder.total_time.setTextSize(textSize);
			holder.entry_id.setTextSize(textSize);
			
			holder.lb.setTextSize(textSize);
			holder.rel_type.setTextSize(textSize);
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 			
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
 			convertView.setTag(holder);
		}else{
			holder = (WindowLoadReceiveNotFinishHoder) convertView.getTag();
		}
		
		LoadReceiveNotFinishBean b  =  arrayList.get(position);

//		holder.title.setText((b.simulationData)?b.title:"");
		holder.system_type.setText((b.simulationData)?b.system_type:"");
		holder.number.setText((b.simulationData)?b.number:"");
//		holder.account_id.setText((b.simulationData)?b.account_id:"");
		holder.company_id.setText((b.simulationData)?b.company_id:"");
//		holder.customer_id.setText((b.simulationData)?b.customer_id:"");
		holder.entry_id.setText((b.simulationData)?b.entry_id:"");
		holder.total_time.setText((b.simulationData)?ScreenMonitorTools.fixTime(b.total_time):"");

		holder.lb.setText((b.simulationData)?b.equipment_purpose:"");
		holder.rel_type.setText((b.simulationData)?b.rel_type:"");
		
		if(b.simulationData&&b.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}    
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}


	public void setList(List<LoadReceiveNotFinishBean> arrayList){
		this.arrayList = arrayList;
	}
	
}
class WindowLoadReceiveNotFinishHoder {
	public View layout;
	public View layout_id;
//	public TextView title;
	public TextView system_type;
	public TextView number;
//	public TextView account_id;
	public TextView company_id;
//	public TextView customer_id;
	public TextView entry_id;

	public TextView lb;	
	public TextView rel_type;
	
	public TextView total_time;
	public View color_layout;
}