package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.RemainJobsAdapter;
import com.android.activity.sceen.task.model.RemainJobsBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragRemainJobs extends CommonFragment<RemainJobsBean> {

	public static final String method = "remainJobs";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_remainjobs;
	}

	@Override
	public List<RemainJobsBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return RemainJobsBean.getBaseTypeList(json);
	}

	@Override
	public RemainJobsBean newBean() {
		// TODO Auto-generated method stub
		return new RemainJobsBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new RemainJobsAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
