package com.android.activity.sceen;

import java.io.Serializable;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import ui.ScreenTitleLayout;
import util.HttpUrlPath;
import util.StringUtil;
import util.TheInitialScreenData;
import util.TimeoutTools;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.android.activity.common.ScreenModeTypeBean;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.sceen.iface.FacilityInterface;
import com.android.common.SimpleJSONUtil;
import com.android.core.CommonFragment;
import com.loopj.android.http.RequestParams;

public class SyncLargeScreenActivity extends FragmentActivity implements FacilityInterface{
	
	private Activity context;
	private String warehouseId;//仓库ID
	private String warehouseName; 
	private ServerTypeBean s;
	private ScreenModeBean screenMode;
	private TheInitialScreenData theInitialScreenData;
	private Handler handler = new Handler();
	
	private ScreenTitleLayout layout;
	private ScreenModeTypeBean bean;
	private CommonFragment<?> fragment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.large_demo_monitor_layout);
		context = this;
		getInFrontOf();
		initView();
		initFragment();
		startHandler();
	}
	
	/**
	 * @Description:获取来自于上一个页面的仓库ID
	 * @param
	 */
	private void getInFrontOf(){
		Intent intent = this.getIntent();
		warehouseId = StringUtil.isNullOfStr(intent.getStringExtra("warehouseId"))?"":intent.getStringExtra("warehouseId");
		warehouseName = StringUtil.isNullOfStr(intent.getStringExtra("warehouseName"))?"":intent.getStringExtra("warehouseName");
		s = (ServerTypeBean) intent.getSerializableExtra("serverData");
		screenMode = (ScreenModeBean) intent.getSerializableExtra("screenMode");
		bean = (ScreenModeTypeBean) intent.getSerializableExtra("screenModeTypeBean");
		theInitialScreenData = (TheInitialScreenData) intent.getSerializableExtra("theInitialScreenData");
//		screenTypeBean = (ScreenTypeBean) intent.getSerializableExtra("screenTypeBean");
		if(screenMode==null){
			screenMode = ScreenModeBean.getScreenMode(context);
		}
		screenMode.setPageNo(1);
	}
	
	private void initView(){
		layout = (ScreenTitleLayout) findViewById(R.id.layout);
		LayoutParams lp_warehouse_name = layout.getLayoutParams();
		lp_warehouse_name.height = screenMode.getNavigationHeight();
		layout.setLayoutParams(lp_warehouse_name);
		layout.setView(lp_warehouse_name.height, screenMode.getTextSize(), (warehouseName),s.getServer_name(),warehouseId, s.getServerTime());
		
		((View)findViewById(R.id.layout_tap)).setBackgroundResource(R.drawable.screen_background_select);
	}
	
	private void initFragment(){	
		
		Bundle bundle = new Bundle();
		bundle.putString("warehouseId", warehouseId);
		bundle.putSerializable("theInitialScreenData", theInitialScreenData);
		bundle.putBoolean("largeOrSmall", true);
		bundle.putInt("pageNo", 1);
		bundle.putSerializable("screenMode", screenMode);
		bundle.putSerializable("serverData", (Serializable) s);
		bundle.putSerializable("allSize",theInitialScreenData.getDATAS_ALL_SIZE());
		bundle.putSerializable("screenModeTypeBean", bean);
		try {
			fragment = (CommonFragment<?>)(bean.getFragment().newInstance());
			(fragment).setArguments(bundle);
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.layout_tap, fragment);
			transaction.commitAllowingStateLoss();

		} catch (InstantiationException e) {
			finish();
		} catch (IllegalAccessException e) {
			finish();
		}
		
	}

	@Override
	public Handler getHandler() {
		// TODO Auto-generated method stub
		return handler;
	}
	
	 private void closeThisActivity(){
		boolean doRemove = true;
		setFramentCallbacks(doRemove);
		if(layout!=null){
			layout.setCallbacks(doRemove);
		}
		finish();
    	overridePendingTransition(0,0);
    }
    
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}
	
	/***********************************************************************************************************/
	
	private int postNum = 0;//用于判断请求失效多少次后执行Connecting的弹窗
	private TimeoutTools timeUtil = new TimeoutTools();
	public int times = 9*1000;
	private boolean whetherToRun = true;// 控制线程的继续运行
	
	private void startHandler() {
		fragment.pageNo = screenMode.getPageNo();
		timeUtil.setTimeMS(System.currentTimeMillis());
//		UiTools.SPOT_TIME = System.currentTimeMillis();
		handler.post(spot_task);
		handler.post(alwaysFixSpotHandler);
	}
	
	/**
	 * 请求数据
	 */
	private Runnable spot_task = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (postNum > TimeoutTools.POSTNUM) {
						fragment.alertDialog();
				}
				postNum++;
				getData();
			}
		}
	};

	/**
	 * 监视器用于循环守卫线程是否正常运行
	 */
	private Runnable alwaysFixSpotHandler = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (TimeoutTools.isFastPost(timeUtil,15*1000)) {
					handler.removeCallbacks(spot_task);
					timeUtil.setTimeMS(System.currentTimeMillis());
//					UiTools.SPOT_TIME = System.currentTimeMillis();
					handler.postDelayed(spot_task,times);
				}
				handler.postDelayed(alwaysFixSpotHandler, 1000);
			}
		}
	};
	
	/**
	 * @Description:获取请求数据
	 * @param @param flag
	 */
	private void getData() {
		
		String url = (s != null) ? s.getServer_url() : HttpUrlPath.CheckInWareHouseBigScreenAction;
		
		RequestParams params = new RequestParams();
		params.add("Method", bean.getMethod());
		params.add("ps_id", warehouseId);
		params.add("pageSize", String.valueOf(theInitialScreenData.getDATAS_ALL_SIZE()-2));
		params.add("pageNo", fragment.pageNo+"");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				helpParsingJson(json);
				ChooseWhich();
			}

			@Override
			public void handFail() {
				ChooseWhich();
			}

			@Override
			public void versionFail(){
				whetherToRun = false;
			}
			
		}.doGetNotHaveDialog(url, params, this);
	}
	
	/**
	 * @Description:解析请求数据
	 * @param @param json
	 * @param @param flag
	 */
	public void helpParsingJson(JSONObject json) {
		postNum = 0;//得到回执数据 则清空请求没得到数据的次数
		fragment.helpParsingJson(json, null);
	}
	
	/**
	 * @Description:选择继续运行哪个线程
	 * @param @param flag
	 */
	public void ChooseWhich() {
		if (whetherToRun) {
			handler.removeCallbacks(spot_task);
//			UiTools.SPOT_TIME = System.currentTimeMillis();
			timeUtil.setTimeMS(System.currentTimeMillis());
			handler.postDelayed(spot_task,times);
		}
	}

	/**
	 * @Description:控制线程的运行以及停止
	 * @param @param doRemove 判断是否停止以及移除线程 true移除 false不移除重新运行
	 */
	public void setFramentCallbacks(boolean doRemove) {
		whetherToRun = !doRemove;
		if (doRemove) {
			handler.removeCallbacks(spot_task);
			handler.removeCallbacks(alwaysFixSpotHandler);
		} else {
			startHandler();
		}
	}
}
