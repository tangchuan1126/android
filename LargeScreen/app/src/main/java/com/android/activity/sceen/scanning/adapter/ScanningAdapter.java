package com.android.activity.sceen.scanning.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.scanning.model.ScanningBean;
import com.android.common.progresswheel.PieProgress;

public class ScanningAdapter extends BaseAdapter  {
	
	private List<ScanningBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public ScanningAdapter(Context context, List<ScanningBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ScanningBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ScanningHoder holder = null;
		 if(convertView==null){
			holder = new ScanningHoder();
			convertView = inflater.inflate(R.layout.scanning_scanning_item,null);

			holder.layout=convertView;
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.t_task_type=(TextView)convertView.findViewById(R.id.t_task_type); 			
			holder.t_task_type.setTextSize(textSize);
			holder.t_task_num=(TextView)convertView.findViewById(R.id.t_task_num); 			
			holder.t_task_num.setTextSize(textSize);
			holder.t_item=(TextView)convertView.findViewById(R.id.t_item);			
			holder.t_item.setTextSize(textSize);
			holder.t_time=(TextView)convertView.findViewById(R.id.t_time);			
			holder.t_time.setTextSize(textSize);
			holder.t_operator=(TextView)convertView.findViewById(R.id.t_operator);			
			holder.t_operator.setTextSize(textSize);
			holder.t_qty=(TextView)convertView.findViewById(R.id.t_qty);			
			holder.t_qty.setTextSize(textSize);
			holder.t_company=(TextView)convertView.findViewById(R.id.t_company);			
			holder.t_company.setTextSize(textSize);
			
			
			holder.donecount = (TextView) convertView.findViewById(R.id.donecount); 
			holder.donecount.setTextSize(textSize);//剩余pallet数量    
			holder.donecount_percentage = (PieProgress) convertView.findViewById(R.id.donecount_percentage); 		
			LayoutParams lp_d = holder.donecount_percentage.getLayoutParams();
			lp_d.height = everyItem;
			lp_d.width = everyItem;
			holder.donecount_percentage.setLayoutParams(lp_d);
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
 			convertView.setTag(holder);
		}else{
			holder = (ScanningHoder) convertView.getTag();
		}
		 
		ScanningBean temp  =  arrayList.get(position);
				
		holder.t_task_type.setText(temp.simulationData?temp.task_type:"");
		holder.t_task_num.setText(temp.simulationData?temp.schedule_id:"");
		holder.t_item.setText(temp.simulationData?temp.item_id:"");
		holder.t_time.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");
		holder.t_operator.setText(temp.simulationData?temp.employe_name:"");
		holder.t_qty.setText(temp.simulationData?(temp.qty+""):"");
		holder.t_company.setText(temp.simulationData?temp.company_id:"");
		
		if(temp.simulationData){
			holder.donecount.setVisibility(View.VISIBLE);
			holder.donecount_percentage.setVisibility(View.VISIBLE);
		}else{
			holder.donecount.setVisibility(View.GONE);
			holder.donecount_percentage.setVisibility(View.GONE);
		}
		
		double dp = 0;
		if(temp.progress>=100){
			dp = 100;
		}else if(temp.progress<=0){
			dp = 0;
		}else{
			dp = temp.progress;
		}
		
		holder.donecount.setText((int)dp+"%");
		holder.donecount_percentage.setProgress((int)(dp/100*360));
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<ScanningBean> arrayList){
		this.arrayList = arrayList;
	}
}
class ScanningHoder {
	public View color_layout;
	
	public View layout,layout_id;
	public TextView t_task_type,t_task_num,t_item,t_time,t_operator,progress,t_qty,t_company;
	
	public TextView donecount;//剩余pallet数量    
	public PieProgress donecount_percentage;
	
}