package com.android.activity.sceen.facility.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.facility.model.DockSituationBean;

public class DoorSituationAdapter extends BaseAdapter  {
	
	private List<DockSituationBean> arrayList;
	private Context context;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
//	private boolean largeOrSmall;
	
	public DoorSituationAdapter(Context context, List<DockSituationBean> arrayList ,int everyItem,int textSize) {
		super();
		this.context = context;
		this.resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public DockSituationBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DoorSituationBaseHoder holder = null;
		 if(convertView==null){
			holder = new DoorSituationBaseHoder();
			convertView = inflater.inflate(R.layout.frag_door_it,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp_id = holder.layout_id.getLayoutParams();
			lp_id.height = everyItem;
			holder.layout_id.setLayoutParams(lp_id);			
			
//			holder.tvEntry = (TextView) convertView.findViewById(R.id.entry_id);
//			holder.tvEntry.setTextSize(textSize);
////			holder.gate_liscense_plate = (TextView) convertView.findViewById(R.id.gate_liscense_plate);
////			holder.gate_liscense_plate.setTextSize(textSize);
//			holder.tvCtnr = (TextView) convertView.findViewById(R.id.gate_container_no);
//			holder.tvCtnr.setTextSize(textSize);	
//			holder.tvCtnr2 = (TextView) convertView.findViewById(R.id.gate_container_no2);
//			holder.tvCtnr2.setTextSize(textSize);
//			holder.tvTime = (TextView) convertView.findViewById(R.id.timevalue);
//			holder.tvTime.setTextSize(textSize);
//			holder.tvDoor = (TextView) convertView.findViewById(R.id.doorid);
//			holder.tvDoor.setTextSize(textSize);
			
//			holder.tvInOut_it=(TextView) convertView.findViewById(R.id.tvInOut_it);
//			holder.tvInOut_it.setTextSize(textSize);

			holder.tvEntryId= (TextView) convertView.findViewById(R.id.tvEntryId);
			holder.tvEquip= (TextView) convertView.findViewById(R.id.tvEquip);
			holder.tvDoor= (TextView) convertView.findViewById(R.id.tvDoor);
			holder.tvTasks= (TextView) convertView.findViewById(R.id.tvTasks);
			holder.tvInOut= (TextView) convertView.findViewById(R.id.tvInOut);
			holder.tvTime= (TextView) convertView.findViewById(R.id.tvTime);
			
			holder.tvEntryId.setTextSize(textSize);
			holder.tvEquip.setTextSize(textSize);
			holder.tvDoor.setTextSize(textSize);
			holder.tvTasks.setTextSize(textSize);
			holder.tvInOut.setTextSize(textSize);
			holder.tvTime.setTextSize(textSize);
			
 			convertView.setTag(holder);
		}else{
			holder = (DoorSituationBaseHoder) convertView.getTag();
		}
		
		 DockSituationBean b=arrayList.get(position);
		 
		 //debug
//		 b.simulationData=true;
//		 if(position==0)
//		 {
//			 b.gate_container_no="";
//			 b.ctn_number="";
//		 }
//		 else if(position==1)
//		 {
//			 b.gate_container_no="KK";
//			 b.ctn_number="KK";
//		 }
//		 else if(position==2)
//		 {
//			 b.gate_container_no="K2";
//			 b.ctn_number="K3,k4";
//		 }
		 
		 //真数据
		 if(b.simulationData){
//			 holder.tvDoor.setText(b.doorid);
//			 holder.tvEntry.setText(b.dlo_id);
////			 holder.tvCtnr.setText(ScreenMonitorTools.getCtn_complex(b.gate_container_no, b.ctn_number));
////			 holder.tvCtnr.setText(b.gate_container_no+" ");
//			 holder.tvCtnr.setText((TextUtils.isEmpty(b.gate_container_no)?"NA":b.gate_container_no) + " ");
//			 holder.tvCtnr2.setText("| "+ScreenMonitorTools.getCtn_complex(b.ctn_number));
//			 
//			 holder.tvInOut_it.setText(b.rel_type);
//			 holder.tvTime.setText(ScreenMonitorTools.fixTime(b.total_time));
			holder.tvEntryId.setText(b.dlo_id);
			holder.tvEquip.setText(b.equipment_number);
			holder.tvDoor.setText(b.doorid);
			holder.tvTasks.setText(b.tractor);
			holder.tvInOut.setText(b.rel_type);
			holder.tvTime.setText(ScreenMonitorTools.fixTime(b.total_time));
		} else {
			holder.tvEntryId.setText("");
			holder.tvEquip.setText("");
			holder.tvDoor.setText("");
			holder.tvTasks.setText("");
			holder.tvInOut.setText("");
			holder.tvTime.setText("");
		 }
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	public void setList(List<DockSituationBean> arrayList){
		this.arrayList = arrayList;
	}
}
class DoorSituationBaseHoder {
	public View layout;
	public View layout_id;
//	public TextView tvEntry;	
//	public TextView tvCtnr,tvCtnr2,tvInOut_it;	
//	public TextView tvTime;
//	public TextView tvDoor;  
	
	public TextView tvEntryId,tvEquip,tvDoor,tvTasks,tvInOut,tvTime;
	
}