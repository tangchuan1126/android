package com.android.activity.sceen.facility.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.facility.model.ParkingBean;

public class ParkingAdapter extends BaseAdapter {
	
	private Context context;

	private List<ParkingBean> arrayList;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;

	public ParkingAdapter(Context context, List<ParkingBean> arrayList,
			int everyItem, int textSize) {
		super();
		this.resources = context.getResources();
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
		this.context=context;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ParkingBean getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DockCloseHoder1 holder = null;
		if (convertView == null) {
			holder = new DockCloseHoder1();
			convertView = inflater.inflate(R.layout.it_fc_parking, null);
			holder.layout = (View) convertView.findViewById(R.id.layout);
			holder.loLocation=convertView.findViewById(R.id.loLocation);

			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			holder.localtion = (TextView) convertView.findViewById(R.id.localtion);
			holder.localtion.setTextSize(textSize);
			holder.loLocation=convertView.findViewById(R.id.loLocation);
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,resources.getString(R.string.task_remain_jobs_location));
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.localtion.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
			
			holder.entryid = (TextView) convertView.findViewById(R.id.entryid);
			holder.entryid.setTextSize(textSize);
			holder.ctnrs = (TextView) convertView.findViewById(R.id.ctnrs);
			holder.ctnrs.setTextSize(textSize);
			holder.equipment_number = (TextView) convertView.findViewById(R.id.equipment_number);
			holder.equipment_number.setTextSize(textSize);
			holder.company_name = (TextView) convertView.findViewById(R.id.company_name);
			holder.company_name.setTextSize(textSize);
//			holder.gate_driver_liscense = (TextView) convertView.findViewById(R.id.gate_driver_liscense);
//			holder.gate_driver_liscense.setTextSize(textSize);
			holder.gate_driver_name = (TextView) convertView.findViewById(R.id.gate_driver_name);
			holder.gate_driver_name.setTextSize(textSize);
			
			convertView.setTag(holder);
		} else {
			holder = (DockCloseHoder1) convertView.getTag();
		}

		ParkingBean temp = arrayList.get(position);
		
		holder.localtion.setText(temp.simulationData?temp.location:"");
		holder.entryid.setText(temp.simulationData?(temp.dlo_id+""):"");
		holder.ctnrs.setText(temp.simulationData?temp.ctnrs:"");
		holder.equipment_number.setText(temp.simulationData?temp.equipment_number:"");	
		holder.company_name.setText(temp.simulationData?temp.company_name:"");	
//		holder.gate_driver_liscense.setText(temp.simulationData?temp.gate_driver_liscense:"");	
		holder.gate_driver_name.setText(temp.simulationData?temp.gate_driver_name:"");	

		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position % 2 == 0 ? R.drawable.list_center_gray: R.drawable.list_center));
		return convertView;
	}

	public void setList(List<ParkingBean> arrayList) {
		this.arrayList = arrayList;
	}
}

class DockCloseHoder1 {
	public View layout;
	public View layout_id,loLocation;
	public TextView localtion;
	public TextView entryid;
	public TextView ctnrs;
	public TextView equipment_number;
	public TextView company_name;
//	public TextView gate_driver_liscense;
	public TextView gate_driver_name;
}