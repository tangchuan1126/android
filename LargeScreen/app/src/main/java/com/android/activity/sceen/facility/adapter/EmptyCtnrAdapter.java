package com.android.activity.sceen.facility.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.facility.model.DockCloseNoCheckOutBean;

public class EmptyCtnrAdapter extends BaseAdapter  {
	
	private Context context;
	private List<DockCloseNoCheckOutBean> arrayList;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	
	//debug
	private Animation anim;
	
	public EmptyCtnrAdapter(Context context, List<DockCloseNoCheckOutBean> arrayList ,int everyItem,int textSize) {
		super();
		this.resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
		this.context=context;
		
		//debug
		anim=AnimationUtils.loadAnimation(context, R.anim.load_num_zoomin);
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public DockCloseNoCheckOutBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DockCloseHoder holder = null;
		 if(convertView==null){
			holder = new DockCloseHoder();
			convertView = inflater.inflate(R.layout.dockclose_not_checkout_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);	
			holder.loLocation=convertView.findViewById(R.id.loLocation);	
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
//			holder.tvLocation = (TextView) convertView.findViewById(R.id.entry_id);
//			holder.tvLocation.setTextSize(textSize);
//			holder.tvEntry = (TextView) convertView.findViewById(R.id.gate_liscense_plate);
//			holder.tvEntry.setTextSize(textSize);
//			holder.tvCtnr = (TextView) convertView.findViewById(R.id.gate_container_no);
//			holder.tvCtnr.setTextSize(textSize);	
//			holder.tvInOut = (TextView) convertView.findViewById(R.id.tvInOut_ir);
//			holder.tvInOut.setTextSize(textSize);
			holder.tvEntryId=(TextView) convertView.findViewById(R.id.tvEntryId);
			holder.tvLoc=(TextView) convertView.findViewById(R.id.tvLoc);
			holder.tvEquip=(TextView) convertView.findViewById(R.id.tvEquip);
			holder.tvInOut=(TextView) convertView.findViewById(R.id.tvInOut);
			holder.tvTasks=(TextView) convertView.findViewById(R.id.tvTasks);
			
			holder.tvEntryId.setTextSize(textSize);
			holder.tvLoc.setTextSize(textSize);
			holder.tvEquip.setTextSize(textSize);
			holder.tvInOut.setTextSize(textSize);
			holder.tvTasks.setTextSize(textSize);
			
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,"Location");
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.tvLoc.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
 			convertView.setTag(holder);
		}else{
			holder = (DockCloseHoder) convertView.getTag();
		}
		 
		DockCloseNoCheckOutBean b  =  arrayList.get(position);
		
		//真数据
		if(b.simulationData){
//			holder.tvLocation.setText(b.location);
//			holder.tvEntry.setText(b.dlo_id);
//			holder.tvCtnr.setText(b.ctn_number);
//			holder.tvInOut.setText(b.rel_type);
			
//			holder.tvLoc.setText(b.location);
//			holder.tvEquip.setText(b.);
//			holder.tvInOut.setText(b.location);
//			holder.tvTasks.setText(b.location);
			holder.tvEntryId.setText(b.dlo_id);
			holder.tvLoc.setText(b.location);
			holder.tvEquip.setText(b.equipment_number);
			holder.tvInOut.setText(b.rel_type);
			holder.tvTasks.setText(b.tractor);
		}
		else{
			holder.tvEntryId.setText("");
			holder.tvLoc.setText("");
			holder.tvEquip.setText("");
			holder.tvInOut.setText("");
			holder.tvTasks.setText("");
		}
		//debug
//		holder.tvLocation.setText("    Door 15");
		
		 //debug
//		 holder.tvLocation.startAnimation(anim);
//		 holder.tvEntry.startAnimation(anim);
//		 holder.tvCtnr.startAnimation(anim);
//		 holder.tvInOut.startAnimation(anim);
//		anim=AnimationUtils.loadAnimation(context, R.anim.load_num_zoomin);
//		convertView.startAnimation(anim);
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	public void setList(List<DockCloseNoCheckOutBean> arrayList){
		this.arrayList = arrayList;
	}
}
class DockCloseHoder {
	public View layout;
	public View layout_id,loLocation;
//	public TextView tvLocation;	
//	public TextView tvEntry;
//	public TextView tvCtnr,tvInOut;	
	
	TextView tvEntryId,tvLoc,tvEquip,tvInOut,tvTasks;
}