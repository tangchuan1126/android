package com.android.activity.platform;

import oso.com.vvmescreen.R;
import ui.ScreenTitleLayout;
import util.ScreenMonitorTools;
import util.StringUtil;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;

import com.android.activity.platform.adapter.ScreenTypeAdapter;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ScreenTypeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.sceen.ScreensActivity;
import com.android.core.ScreenDatas;
/**
 * @ClassName: MenuActivity 
 * @Description:用于显示大屏幕的类型
 * @author gcy
 * @date 2014-9-12 下午4:13:50
 */
public class ScreenTypeListActivity extends Activity implements OnItemSelectedListener,OnKeyListener{

	private Activity context;
	private GridView gridView;	
	private String warehouseName;
	private String warehouseId;
	private ScreenTypeAdapter adapter;
	private ScreenTitleLayout layout;
	private ServerTypeBean s;
	private ScreenModeBean screenMode;
	private static final int SCREEN_TYPEFLAG = 110;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_type);
		context=ScreenTypeListActivity.this;
		layout = (ScreenTitleLayout) findViewById(R.id.layout);
		gridView = (GridView) findViewById(R.id.GridViewId);
		getInFrontOf();
		
		LayoutParams lp_warehouse_name = layout.getLayoutParams();
		lp_warehouse_name.height = screenMode.getEveryItem()+screenMode.getHeightWucha()/3+screenMode.getHeightWucha()%3;
		layout.setLayoutParams(lp_warehouse_name);
		layout.setView(lp_warehouse_name.height, screenMode.getTextSize(), "Screen Type List",s.getServer_name(), warehouseId, s.getServer_url());
		
		int paddingSize = (screenMode.getHeightPixels()-lp_warehouse_name.height)/6;
		int haveSize = screenMode.getWidthPixels() - paddingSize*2;
		int everySize =  haveSize/4;
		
		gridView.setColumnWidth(everySize);
		gridView.setPadding(paddingSize, paddingSize/2, paddingSize, 0);
		
		adapter = new ScreenTypeAdapter(context, ScreenTypeBean.getBaseTypeList(null),everySize / 2);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					ScreenTypeBean screenTypeBean =(ScreenTypeBean)parent.getAdapter().getItem(position);
					jumpAnotherActivity(position,screenTypeBean);
			}
			
		});
		gridView.setOnKeyListener(this);
		gridView.setOnItemSelectedListener(this);
		initGirdView();
	}
	
	/**
	 * @Description:获取来自于上一个页面的仓库ID
	 * @param
	 */
	private void getInFrontOf(){
		Intent intent = this.getIntent();
		warehouseId = StringUtil.isNullOfStr(intent.getStringExtra("warehouseId"))?"":intent.getStringExtra("warehouseId");
		warehouseName = StringUtil.isNullOfStr(intent.getStringExtra("warehouseName"))?"":intent.getStringExtra("warehouseName");
		s = (ServerTypeBean) intent.getSerializableExtra("serverData");
		screenMode = (ScreenModeBean) intent.getSerializableExtra("screenMode");
		if(screenMode==null){
			screenMode = ScreenModeBean.getScreenMode(context);
		}
	}
	
	public void jumpAnotherActivity(int position,ScreenTypeBean screenTypeBean){
		Intent intent = ScreenModeBean.setIntent(screenMode);
		layout.setCallbacks(true);//停止时钟
		switch (position) {
		case 0:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getScreen_Task());
			intent.putExtra("serverData", s);
			break;
		case 1:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getScreen_Facility());
			intent.putExtra("serverData", s);
			break;
		case 2:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getWindow());
			intent.putExtra("serverData", s);
			break;
		case 3:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getSync());
			intent.putExtra("serverData", s);
			break;
		case 4:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getGhostCTNR());
			intent.putExtra("serverData", s);
			break;
		case 5:
			intent.setClass(context,ScreensActivity.class);
			ScreensActivity.initParams(ScreenDatas.getScanning());
			ServerTypeBean server = new ServerTypeBean();
			server.setServer_name(s.getServer_name());
			server.setServer_url(s.getUrl());
			server.setUrl_type(ServerTypeBean.MicroServices);
			intent.putExtra("serverData", server);
			break;
		default:
			break;
		}
		intent.putExtra("screenTypeBean", screenTypeBean);
		intent.putExtra("warehouseName", warehouseName);
		intent.putExtra("warehouseId", warehouseId);
//		startActivity(intent);
		startActivityForResult(intent, SCREEN_TYPEFLAG);
		overridePendingTransition(R.anim.push_from_left_in,R.anim.push_from_left_out);
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		switch (v.getId()) {
		case R.id.GridViewId:
			if(gridView!=null){
				if(adapter!=null&&adapter.getCount()>0){
					return ScreenMonitorTools.keyToolsForGirdView(keyCode,event,adapter,gridView);
				}
			}
			break;

		default:
			break;
		}
		return false;
	}
	    
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		layout.setCallbacks(true);
		finish();
    	overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if(adapter!=null&&adapter.getCount()>0){
			adapter.setSelected(position);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {}
	
	/**
	 * @Description:设置默认选中项
	 * @param
	 */
	public void initGirdView(){
		if(gridView!=null&&adapter!=null){
			gridView.requestFocus();
			gridView.setSelection(gridView.getSelectedItemPosition());
			adapter.setSelected(gridView.getSelectedItemPosition());
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initGirdView();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SCREEN_TYPEFLAG) {
			if (resultCode == RESULT_OK) {
				boolean doRemove = false;
				layout.setCallbacks(doRemove);
			}
		}
	}
}
