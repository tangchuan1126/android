package com.android.activity.sceen.windows.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class WindowBean implements Serializable {


	private static final long serialVersionUID = 6559370918361824899L;
	
	public String dlo_id;
	public String tractor;
	public String trailer;
	public String locations;
	public String rel_type;
	public int total_time;
	
	public String equipment_purpose;
//	public String tasks;
//	public String number;
//	public String equipment_id;
	public int is_late;
	
	
	public boolean simulationData;//判断是否是真数据
	
	/***************************************************************************/
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<WindowBean> getBaseTypeList(JSONObject json){
		List<WindowBean> baseList = new ArrayList<WindowBean>();
		JSONArray spotSituationBaseArrays = json.optJSONArray("data");
		boolean flag =(spotSituationBaseArrays!=null&&spotSituationBaseArrays.length()>0);
		if(flag){
			for (int i = 0; i < spotSituationBaseArrays.length(); i++) {
				JSONObject jsonItem = spotSituationBaseArrays.optJSONObject(i);
				WindowBean bean = new WindowBean();				
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.tractor = jsonItem.optString("tractor");
				bean.trailer = jsonItem.optString("trailer");
				bean.locations = jsonItem.optString("locations");
				bean.equipment_purpose = jsonItem.optString("equipment_purpose");
				bean.rel_type = jsonItem.optString("rel_type");
//				bean.tasks = jsonItem.optString("tasks");
//				bean.number = jsonItem.optString("number");
//				bean.equipment_id = jsonItem.optString("equipment_id");
				bean.total_time = jsonItem.optInt("total_time");
				bean.is_late = jsonItem.optInt("is_late");
				
				bean.simulationData = true;
				baseList.add(bean);
			}
			
			return baseList;
		}
		return null;
	}
	
}
