package com.android.activity.sceen.sync.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * @ClassName: PlatformAdvertisingBaseType
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class LoadReceiveNotFinishBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -7094692332139326252L;
	/*****************************************/

	public int total_time;// 0,
	public String company_id;// "",
	public String title;// "",
	public String system_type;// "SYNC",
	public String account_id;// "",
	public String number;// "SS",
	public String customer_id;// "",
	public String entry_id;
	

	public String rel_type;	
	public String equipment_purpose;
	
	public int is_late;
	
	public boolean simulationData;//判断是否是真数据 true为真数据 false为假数据
	//=============================================

	
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<LoadReceiveNotFinishBean> getBaseTypeList(JSONObject json){
		List<LoadReceiveNotFinishBean> list = new ArrayList<LoadReceiveNotFinishBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				LoadReceiveNotFinishBean bean = new LoadReceiveNotFinishBean();
				bean.total_time=jsonItem.optInt("total_time");
				bean.company_id=jsonItem.optString("company_id");
				bean.title=jsonItem.optString("title");
				bean.system_type=jsonItem.optString("system_type");
				bean.account_id=jsonItem.optString("account_id");
				bean.number=jsonItem.optString("number");
				bean.customer_id=jsonItem.optString("customer_id");
				bean.entry_id=jsonItem.optString("entry_id");
				

				bean.equipment_purpose = jsonItem.optString("equipment_purpose");
				bean.rel_type = jsonItem.optString("rel_type");
				
				bean.is_late = jsonItem.optInt("is_late");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}

}