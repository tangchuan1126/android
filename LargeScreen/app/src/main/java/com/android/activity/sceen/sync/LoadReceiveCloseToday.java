package com.android.activity.sceen.sync;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.sync.adapter.LoadReceiveCloseTodayAdapter;
import com.android.activity.sceen.sync.model.LoadReceiveCloseTodayBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class LoadReceiveCloseToday extends CommonFragment<LoadReceiveCloseTodayBean> {

	public static final String method = "LoadReceiveCloseToday";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.load_receive_close_today;
	}

	@Override
	public List<LoadReceiveCloseTodayBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return LoadReceiveCloseTodayBean.getBaseTypeList(json);
	}

	@Override
	public LoadReceiveCloseTodayBean newBean() {
		// TODO Auto-generated method stub
		return new LoadReceiveCloseTodayBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new LoadReceiveCloseTodayAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
