package com.android.activity.sceen.sync;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.sync.adapter.LoadReceiveNotFinishAdapter;
import com.android.activity.sceen.sync.model.LoadReceiveNotFinishBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class LoadReceiveNotFinish extends CommonFragment<LoadReceiveNotFinishBean> {

	public static final String method = "LoadReceiveNotFinish";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.load_receive_not_finish;
	}

	@Override
	public List<LoadReceiveNotFinishBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return LoadReceiveNotFinishBean.getBaseTypeList(json);
	}

	@Override
	public LoadReceiveNotFinishBean newBean() {
		// TODO Auto-generated method stub
		return new LoadReceiveNotFinishBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new LoadReceiveNotFinishAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
