package com.android.activity.sceen.task.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.NoTaskLaborBean;
import com.android.common.progresswheel.PieProgress;
import com.android.common.ui.BadgeView;

public class NoTaskLaborAdapter extends BaseAdapter  {
	
	private List<NoTaskLaborBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public NoTaskLaborAdapter(Context context, List<NoTaskLaborBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public NoTaskLaborBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NoTaskLaborHoder holder = null;
		 if(convertView==null){
			holder = new NoTaskLaborHoder();
			convertView = inflater.inflate(R.layout.no_task_labor_listview_item,null);

			holder.layout = (View) convertView.findViewById(R.id.layout);		
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			holder.labor_name = (TextView) convertView.findViewById(R.id.labor_name);
			holder.labor_name.setTextSize(textSize);	
			
			holder.free_time = (TextView) convertView.findViewById(R.id.free_time);
			holder.free_time.setTextSize(textSize);	

 			convertView.setTag(holder);
		}else{
			holder = (NoTaskLaborHoder) convertView.getTag();
		}
		 
		NoTaskLaborBean temp  =  arrayList.get(position);

		holder.labor_name.setText(temp.simulationData?temp.employe_name:"");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
		holder.free_time.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");
		
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<NoTaskLaborBean> arrayList){
		this.arrayList = arrayList;
	}
}
class NoTaskLaborHoder {	
	public View layout;
	public View layout_id;

	public TextView labor_name;
	public TextView free_time;

	public View color_layout;
}