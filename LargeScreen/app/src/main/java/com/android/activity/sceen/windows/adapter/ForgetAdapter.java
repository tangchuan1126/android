package com.android.activity.sceen.windows.adapter;

import java.util.List;

import key.EntryDetailNumberTypeKey;
import oso.com.vvmescreen.R;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.windows.model.ForgetCloseTaskBean;

public class ForgetAdapter extends BaseAdapter  {
	
	private List<ForgetCloseTaskBean> arrayList;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	
	public ForgetAdapter(Context context, List<ForgetCloseTaskBean> arrayList ,int everyItem,int textSize) {
		super();
		this.resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ForgetCloseTaskBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ForgetHoder holder = null;
		 if(convertView==null){
			holder = new ForgetHoder();
			convertView = inflater.inflate(R.layout.forget_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);		
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
			
			holder.task_type = (TextView) convertView.findViewById(R.id.task_type);
			holder.task_type.setTextSize(textSize);
			holder.task_num = (TextView) convertView.findViewById(R.id.task_num);
			holder.task_num.setTextSize(textSize);
			holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
			holder.entry_id.setTextSize(textSize);	
			holder.loader = (TextView) convertView.findViewById(R.id.loader);
			holder.loader.setTextSize(textSize);	
			holder.re_type = (TextView) convertView.findViewById(R.id.re_type);
			holder.re_type.setTextSize(textSize);	
			
 			convertView.setTag(holder);
		}else{
			holder = (ForgetHoder) convertView.getTag();
		}
		 
		ForgetCloseTaskBean temp  =  arrayList.get(position);
		holder.task_type.setText(temp.simulationData?/*EntryDetailNumberTypeKey.getModuleName(*/temp.number_type/*)*/:"");
		holder.task_num.setText(temp.simulationData?(temp.number+""):"");
		holder.entry_id.setText(temp.simulationData?temp.dlo_id:"");
		holder.loader.setText(temp.simulationData?temp.labor:"");	
		holder.re_type.setText(temp.simulationData?temp.rel_type:"");	
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	public void setList(List<ForgetCloseTaskBean> arrayList){
		this.arrayList = arrayList;
	}
}
class ForgetHoder {
	public View layout;
	public View layout_id;
	public TextView task_type;	
	public TextView task_num;
	public TextView entry_id;	
	public TextView loader;
	public TextView re_type;	
}