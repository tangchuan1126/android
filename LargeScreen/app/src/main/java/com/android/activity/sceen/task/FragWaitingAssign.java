package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.GoingToWareHouseAdapter;
import com.android.activity.sceen.task.model.GoingToWareHouseBean;
import com.android.core.CommonFragment;

/**
 * task-未到warehouse
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragWaitingAssign extends CommonFragment<GoingToWareHouseBean> {
	
	public static final String method = "goingToWareHouse";

	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_waitingassign;
	}

	@Override
	public List<GoingToWareHouseBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return GoingToWareHouseBean.getBaseTypeList(json);
	}
	
	@Override
	public GoingToWareHouseBean newBean() {
		// TODO Auto-generated method stub
		return new GoingToWareHouseBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new GoingToWareHouseAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
