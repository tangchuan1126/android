package com.android.activity.platform;

import java.util.List;

import oso.com.vvmescreen.R;
import ui.ScreenTitleLayout;
import util.ScreenMonitorTools;
import util.Utility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;

import com.android.activity.platform.adapter.WarehouseAdapter;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.platform.model.WarehouseInformationBean;

/**
 * @ClassName: WarehouseListActivity
 * @Description: 选择仓库
 * @author gcy
 * @date 2014-9-25 下午4:43:02
 */
@SuppressLint("NewApi")
public class WarehouseListActivity extends Activity implements
		OnItemSelectedListener, OnKeyListener {

	private Activity context;
	private GridView gridView;
	private WarehouseAdapter adapter;
	private ScreenTitleLayout layout;
	private ServerTypeBean s;
	private List<WarehouseInformationBean> wiList;
	private ScreenModeBean screenMode;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warehouse_list);
		context = WarehouseListActivity.this;
		layout = (ScreenTitleLayout) findViewById(R.id.layout);
		gridView = (GridView) findViewById(R.id.gridView);
		getInFrontOf();

	}

	/**
	 * @Description:获取来自于上一个页面的仓库ID
	 * @param
	 */
	@SuppressWarnings("unchecked")
	private void getInFrontOf() {
		Intent intent = this.getIntent();
		wiList = (List<WarehouseInformationBean>) getIntent().getSerializableExtra("wiList");
		screenMode = (ScreenModeBean) intent.getSerializableExtra("screenMode");
		s = (ServerTypeBean) intent.getSerializableExtra("serverData");
		if(screenMode==null){
			screenMode = ScreenModeBean.getScreenMode(context);
		}
		
		if(!Utility.isNullForList(wiList)){
			setView(wiList);
		}else{
			finish();
		}
	}
	
	public void setView(List<WarehouseInformationBean> list) {		
		LayoutParams lp_warehouse_name = layout.getLayoutParams();
		lp_warehouse_name.height = screenMode.getEveryItem()+screenMode.getHeightWucha()/3+screenMode.getHeightWucha()%3;
		layout.setLayoutParams(lp_warehouse_name);
		layout.setView(lp_warehouse_name.height, screenMode.getTextSize(), "Warehouse List",s.getServer_name());
		
		int paddingSize = (screenMode.getHeightPixels() - lp_warehouse_name.height) / 6;
		int haveSize = screenMode.getWidthPixels() - paddingSize * 2;
		int everySize = haveSize / 4;
		
		gridView.setColumnWidth(everySize);
		gridView.setPadding(paddingSize, paddingSize/2, paddingSize, 0);

		adapter = new WarehouseAdapter(context,list, everySize / 2);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				WarehouseInformationBean w = (WarehouseInformationBean) parent.getAdapter().getItem(position);
				jumpAnotherActivity(w);
			}
		});
		gridView.setOnKeyListener(this);
		gridView.setOnItemSelectedListener(this);

		initGirdView();
	}

	public void jumpAnotherActivity(WarehouseInformationBean w) {
		if (w != null) {
			Intent intent = ScreenModeBean.setIntent(screenMode);
			intent.setClass(context, ScreenTypeListActivity.class);
			intent.putExtra("warehouseId", String.valueOf(w.getWarehouseId()));
			intent.putExtra("warehouseName",String.valueOf(w.getWarehouseName()));
			intent.putExtra("serverData", s);
			startActivity(intent);
			overridePendingTransition(R.anim.push_from_left_in,R.anim.push_from_left_out);
		}
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		switch (v.getId()) {
		case R.id.gridView:
			if (gridView != null) {
				WarehouseAdapter adapter = ((WarehouseAdapter) gridView
						.getAdapter());
				if (adapter != null && adapter.getCount() > 0) {
					return ScreenMonitorTools.keyToolsForGirdView(keyCode,
							event, adapter, gridView);
				}
			}
			break;

		default:
			break;
		}
		return false;
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		WarehouseAdapter s = (WarehouseAdapter) parent.getAdapter();
		if (s != null && s.getCount() > 0) {
			s.setSelected(position);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

	/**
	 * @Description:设置默认选中项
	 * @param
	 */
	public void initGirdView() {
		if (gridView != null && adapter != null) {
			gridView.requestFocus();
			gridView.setSelection(gridView.getSelectedItemPosition());
			adapter.setSelected(gridView.getSelectedItemPosition());
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initGirdView();
	}
}
