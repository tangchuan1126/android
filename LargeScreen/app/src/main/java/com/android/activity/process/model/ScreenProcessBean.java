package com.android.activity.process.model;
/**
 * {
  "data": {
    "closetoday": {
      "count": 0,
      "total": 0,
      "color": 3
    },
    "cargoondoor": 0,
    "remainjobs": {
      "count": 0,
      "total": 0,
      "color": 3
    },
    "needassign": {
      "count": 2,
      "total": 2,
      "color": 1
    },
    "incomingtowindow": {
      "count": 3,
      "total": 3,
      "color": 1
    },
    "forgetclose": 0,
    "cargoonspot": 0,
    "loadreceive": {
      "count": 7,
      "total": 7,
      "color": 1
    },
    "underprocessing": {
      "count": 4,
      "total": 4,
      "color": 1
    }
  },
  "err": 0,
  "ret": 1
}
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/**
 * @ClassName: ScreenDataBean 
 * @Description: 用于跳转大屏幕的菜单
 * @author gcy
 * @date 2014-12-29 下午8:51:19
 */
public class ScreenProcessBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 5045981100147234876L;
	public String screenName;//流程名称
	public String screenClass;//流程种类 对应大屏幕的class类
	public StepDataBean stepDataBean;
	public int number;//右上角红圈内显示的数字
	

	public int monitorType;
	
	//--------------------用于判断按钮功能的静态变量
	public static final int Load_Or_Receive_Processing = 1;
	public static final int DN_Or_RN_Routy = 2;
//	public static final int RN_Routy = 3;
	public static final int Processing = 4;
	public static final int Transport = 5;
	/**
	 * @Description:初始化数据参数
	 * @param @return
	 */
	public static List<ScreenProcessBean> getMonitorMenuData(){
		List<ScreenProcessBean> list = new ArrayList<ScreenProcessBean>();		
		list.add(new ScreenProcessBean("Load / Receive Processing",Load_Or_Receive_Processing));
		list.add(new ScreenProcessBean("DN / RN Routing",DN_Or_RN_Routy));
//		list.add(new ScreenProcessBean("RN Routy",RN_Routy));
		list.add(new ScreenProcessBean("Order Processing",Processing));
		list.add(new ScreenProcessBean("Transport",Transport));
		
		return list;
	}
	
	/**
	 * @Description:根据type 类型选择对应的解析json的方法
	 * @param @param json JSON字符
	 * @param @param type 类型
	 * @param @return
	 */
	public static Map<String, List<ScreenProcessBean>> handJson(JSONObject json,int type){
		Map<String, List<ScreenProcessBean>> map = null;
		switch (type) {
			case ScreenProcessBean.Load_Or_Receive_Processing:
				map = handJsonForLoadOrReceiveProcessing(json);
				break;
			case ScreenProcessBean.DN_Or_RN_Routy:
				map = handJsonForDnRouty(json);
				break;
//			case ScreenProcessBean.RN_Routy:
//				map = handJsonForRnRouty(json);
//				break;
			case ScreenProcessBean.Processing:
				map = handJsonForProcessing(json);
				break;
			case ScreenProcessBean.Transport:
//				map = handJsonForProcessing(json);
				break;
		}
		return map;
	}
	
	/**
	 * @Description:针对Load/ReceiveProcessing的数据进行解析  由于是定制 所以很多数据都写死了
	 * @param @param json
	 * @param @return
	 */
	public static Map<String, List<ScreenProcessBean>> handJsonForLoadOrReceiveProcessing(JSONObject json){
		Map<String, List<ScreenProcessBean>> map = new HashMap<String, List<ScreenProcessBean>>();
		//-------------------------------初始化进度名称
		ScreenProcessBean incomingtowindow = new ScreenProcessBean("Incoming \n Window","WindowFrament");
		ScreenProcessBean loadreceive = new ScreenProcessBean("Open \n Load/Receive","LoadReceiveNotFinish");
		ScreenProcessBean needassign = new ScreenProcessBean("Waiting Assign Labor","FragWaitingAssign");
		ScreenProcessBean remainjobs = new ScreenProcessBean("Remain Jobs","FragRemainJobs");
		ScreenProcessBean underprocessing = new ScreenProcessBean("Under \n Processing","FragUnderProcessing");
		ScreenProcessBean closetoday = new ScreenProcessBean("Load/Receive \n Closed Today","LoadReceiveCloseToday");
		//------------------------------解析进度详细数据
		JSONObject jsonObject = json.optJSONObject("data");
		if(jsonObject!=null){
			JSONObject incomingtowindowJson = jsonObject.optJSONObject("incomingtowindow");	
			JSONObject loadreceiveJson = jsonObject.optJSONObject("loadreceive");
			JSONObject needassignJson = jsonObject.optJSONObject("needassign");
			JSONObject remainjobsJson = jsonObject.optJSONObject("remainjobs");
			JSONObject underprocessingJson = jsonObject.optJSONObject("underprocessing");
			JSONObject closetodayJson = jsonObject.optJSONObject("closetoday");	
			//--------------------调用辅助方法进行解析赋值
			getStepDataBean(remainjobsJson, remainjobs);
			getStepDataBean(loadreceiveJson, loadreceive);
			getStepDataBean(underprocessingJson, underprocessing);
			getStepDataBean(needassignJson, needassign);
			getStepDataBean(incomingtowindowJson, incomingtowindow);
			getStepDataBean(closetodayJson, closetoday);
		}
		//--------------------------按顺序进行填充进度方向条的数据集
		List<ScreenProcessBean> processList = new ArrayList<ScreenProcessBean>();
		processList.add(incomingtowindow);	//1
		processList.add(loadreceive);		//2
		processList.add(needassign);		//3
		processList.add(remainjobs);		//4
		processList.add(underprocessing);	//5
		processList.add(closetoday);		//6
		
		//------------------------------解析数据 并按顺序进行填充屏幕按钮的数据集
		List<ScreenProcessBean> screenList = new ArrayList<ScreenProcessBean>();
		if(jsonObject!=null){
			screenList.add(new ScreenProcessBean("Forget Close"     , "ForgetCloseTaskFrament"	,jsonObject.optInt("forgetclose")));
			screenList.add(new ScreenProcessBean("Cargo On Spot"    , "FragCargoOnSpot" 			,jsonObject.optInt("cargoonspot")));//Need Finish Number
			screenList.add(new ScreenProcessBean("Cargo On Door"    , "FragCargoOnDoor"			,jsonObject.optInt("cargoondoor")));
		}
		
		//------------------------------填充Map
		map.put("process", processList);
		map.put("screen", screenList);
		return map;
	}
	
	
	/**
	 * @Description:针对Load/ReceiveProcessing的数据进行解析  由于是定制 所以很多数据都写死了
	 * @param @param json
	 * @param @return
	 */
	public static Map<String, List<ScreenProcessBean>> handJsonForDnRouty(JSONObject json){
		Map<String, List<ScreenProcessBean>> map = new HashMap<String, List<ScreenProcessBean>>();
		//-------------------------------初始化进度名称
		ScreenProcessBean routing = new ScreenProcessBean("Routing\nDo(No BOL) ","");
		ScreenProcessBean committed = new ScreenProcessBean(" Committed\nInventroy","");
		ScreenProcessBean bol = new ScreenProcessBean("BOL -> Load","");
		ScreenProcessBean load = new ScreenProcessBean("Load\nAppointment","");
		
		List<ScreenProcessBean> processList = new ArrayList<ScreenProcessBean>();
		processList.add(routing);	//1
		processList.add(committed);		//2
		processList.add(bol);		//3
		processList.add(load);		//4
		
		//-------------------------------初始化进度名称
		ScreenProcessBean rn = new ScreenProcessBean("RN","");
		ScreenProcessBean appointment = new ScreenProcessBean("RN\nAppointment","");
		ScreenProcessBean received = new ScreenProcessBean("Received\n(Scaned)","");
		ScreenProcessBean putAway = new ScreenProcessBean("Put Away","");
		
		List<ScreenProcessBean> processRnList = new ArrayList<ScreenProcessBean>();
		processRnList.add(rn);	//1
		processRnList.add(appointment);		//2
		processRnList.add(received);		//3
		processRnList.add(putAway);		//4
		
		//------------------------------填充Map
		map.put("process", processList);
		map.put("processRn", processRnList);
		return map;
	}

	public static Map<String, List<ScreenProcessBean>> handJsonForRnRouty(JSONObject json){
		Map<String, List<ScreenProcessBean>> map = new HashMap<String, List<ScreenProcessBean>>();
		//-------------------------------初始化进度名称
		ScreenProcessBean rn = new ScreenProcessBean("RN","");
		ScreenProcessBean appointment = new ScreenProcessBean("RN\nAppointment","");
		ScreenProcessBean received = new ScreenProcessBean("Received\n(Scaned)","");
		ScreenProcessBean putAway = new ScreenProcessBean("Put Away","");
		
		List<ScreenProcessBean> processRnList = new ArrayList<ScreenProcessBean>();
		processRnList.add(rn);	//1
		processRnList.add(appointment);		//2
		processRnList.add(received);		//3
		processRnList.add(putAway);		//4
		
		
		//------------------------------填充Map
		map.put("process", processRnList);
		map.put("screen", null);
		return map;
	}
	
	public static Map<String, List<ScreenProcessBean>> handJsonForProcessing(JSONObject json){
		Map<String, List<ScreenProcessBean>> map = new HashMap<String, List<ScreenProcessBean>>();
		//-------------------------------初始化进度名称
		ScreenProcessBean dropped = new ScreenProcessBean("BOL Dropped","");
		ScreenProcessBean picking = new ScreenProcessBean("Picking Slip","");
		ScreenProcessBean staying = new ScreenProcessBean("Staying","");
		ScreenProcessBean consolidated = new ScreenProcessBean("Consolidated\n(Ready For Ship)","");
		
		List<ScreenProcessBean> processList = new ArrayList<ScreenProcessBean>();
		processList.add(dropped);	//1
		processList.add(picking);		//2
		processList.add(staying);		//3
		processList.add(consolidated);		//4
		
		//------------------------------填充Map
		map.put("process", processList);
		map.put("screen", null);
		return map;
	}
	
	/**
	 * @Description:辅助解析Json数据
	 * @param @param json
	 * @param @param s
	 */
	private static void getStepDataBean(JSONObject json,ScreenProcessBean s){
		if(json!=null){
			int count = json.optInt("count");//流程名称
			int total = json.optInt("total");//流程名称
			int color = json.optInt("color");//流程名称
			s.stepDataBean = new StepDataBean(count, total, color);
		}
	}	
	
	
	/*******************************************以下均为构造方法********************************************/

	/**
	 * @Description:构造方法
	 * @param
	 */
	public ScreenProcessBean(){
	}
	/**
	 * @Description:构造方法
	 * @param
	 */
	public ScreenProcessBean(String screenName){
		this.screenName = screenName;
	}
	
	/**
	 * @Description:构造方法
	 * @param
	 */
	public ScreenProcessBean(String screenName,int monitorType){
		this.screenName = screenName;
		this.monitorType = monitorType;
	}
	
	/**
	 * @Description:构造方法
	 * @param
	 */
	public ScreenProcessBean(String screenName,String screenClass){
		this.screenName = screenName;
		this.screenClass = screenClass;
		//Debug
		this.stepDataBean = new StepDataBean(1);
	}
	
	/**
	 * @Description:构造方法
	 * @param
	 */
	public ScreenProcessBean(String screenName,String screenClass,StepDataBean stepDataBean){
		this.screenName = screenName;
		this.screenClass = screenClass;
		this.stepDataBean = stepDataBean;
	}
	
	/**
	 * @Description:构造方法 用于新建屏幕按钮
	 * @param @param screenName 屏幕名称 
	 * @param @param screenType 屏幕对应的触发类
	 * @param @param number 屏幕按钮右上方的数字提示
	 */
	public ScreenProcessBean(String screenName,String screenClass,int number){
		this.screenName = screenName;
		this.screenClass = screenClass;
		this.number = number;
	}
	
}
