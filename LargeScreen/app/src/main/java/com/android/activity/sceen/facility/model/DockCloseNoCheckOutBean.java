package com.android.activity.sceen.facility.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: DockCloseNoCheckOutBean 
 * @Description: DockClose 的bean类 但是没有checkout
 * @author gcy
 * @date 2014-9-26 上午9:48:50
 */
public class DockCloseNoCheckOutBean implements Serializable {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 4463277041756377242L;

	
	
	//========新============================
	
	public int total_time;
	public String equipment_number;
	public String tractor;
	public String check_in_time;
	public String equipment_id;
	
	public String location;
	public String rel_type;
	public String dlo_id;

	public boolean simulationData;//判断是否是真数据
	/*********************************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<DockCloseNoCheckOutBean> getBaseTypeList(JSONObject json){
		List<DockCloseNoCheckOutBean> dockCloseNoCheckOutBeanList = new ArrayList<DockCloseNoCheckOutBean>();
		JSONArray dockCloseNoCheckOutBeanArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(dockCloseNoCheckOutBeanArrays));
		if(flag){
			for (int i = 0; i < dockCloseNoCheckOutBeanArrays.length(); i++) {
				JSONObject jsonItem = dockCloseNoCheckOutBeanArrays.optJSONObject(i);
				DockCloseNoCheckOutBean b = new DockCloseNoCheckOutBean();
				
				b.total_time=jsonItem.optInt("total_time");
				b.equipment_number=jsonItem.optString("equipment_number");
				b.tractor=jsonItem.optString("tractor");
				b.equipment_id=jsonItem.optString("equipment_id");
				
				b.location=jsonItem.optString("location");
				b.rel_type=jsonItem.optString("rel_type");
				b.dlo_id = jsonItem.optString("dlo_id");
				
				b.simulationData=true;
				dockCloseNoCheckOutBeanList.add(b);
			}			
			return dockCloseNoCheckOutBeanList;
		}
		return null;
	}
	
	
	/*********************************************************/

}
