package com.android.activity.common;

import android.widget.LinearLayout;

import com.android.core.CommonFragment;
import com.android.core.CommonProcessingFragment;
/**
 * @ClassName: BaseBean 
 * @Description: 
 * @author gcy
 * @date 2014-12-5 上午10:44:01
 */
public class BaseBean { 
	
	private ScreenModeTypeBean screenModeTypeBean;
	private LinearLayout layout;
	private CommonFragment<?> fragment;//列表形式的小屏幕主体
	private CommonProcessingFragment<?> pFragment;//进度形式的小屏幕主体
	
	private int postFrequency = 0;//请求次数
	
	public ScreenModeTypeBean getScreenModeTypeBean() {
		return screenModeTypeBean;
	}
	public void setScreenModeTypeBean(ScreenModeTypeBean screenModeTypeBean) {
		this.screenModeTypeBean = screenModeTypeBean;
	}
	public LinearLayout getLayout() {
		return layout;
	}
	public void setLayout(LinearLayout layout) {
		this.layout = layout;
	}
	public CommonFragment<?> getFragment() {
		return fragment;
	}
	public void setFragment(CommonFragment<?> fragment) {
		this.fragment = fragment;
	}
	public int getPostFrequency() {
		return postFrequency;
	}
	public void setPostFrequency(int postFrequency) {
		this.postFrequency = postFrequency;
	}
	public CommonProcessingFragment<?> getpFragment() {
		return pFragment;
	}
	public void setpFragment(CommonProcessingFragment<?> pFragment) {
		this.pFragment = pFragment;
	}
	
}
