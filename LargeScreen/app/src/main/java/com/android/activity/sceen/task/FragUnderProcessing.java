package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.WorkingOnWarehouseAdapter;
import com.android.activity.sceen.task.model.WorkingOnWarehouseBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragUnderProcessing extends CommonFragment<WorkingOnWarehouseBean> {

	public static final String method = "loadProcessing";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_processing; 
	}

	@Override
	public List<WorkingOnWarehouseBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return WorkingOnWarehouseBean.getBaseTypeList(json);
	}

	@Override
	public WorkingOnWarehouseBean newBean() {
		// TODO Auto-generated method stub
		return new WorkingOnWarehouseBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new WorkingOnWarehouseAdapter(mActivity, getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
