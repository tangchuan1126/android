package com.android.activity.sceen.scanning;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.scanning.adapter.ScanningAdapter;
import com.android.activity.sceen.scanning.model.ScanningBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class Scanning extends CommonFragment<ScanningBean> {

	public static final String method = "scanningschedule";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.scanning_scanning;
	}

	@Override
	public List<ScanningBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return ScanningBean.getBaseTypeList(json);
	}

	@Override
	public ScanningBean newBean() {
		// TODO Auto-generated method stub
		return new ScanningBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new ScanningAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
