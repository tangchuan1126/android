package com.android.activity.platform.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.activity.platform.model.ScreenTypeBean;

public class ScreenTypeAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater inflater;
	private List<ScreenTypeBean> arrayList;
	private int selectViewNum = -1;
	private int everySize;
	
	public ScreenTypeAdapter(Context context, List<ScreenTypeBean> arrayList,int everySize) {
		this.arrayList = arrayList;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.everySize = everySize;
	}

	@Override
	public int getCount() {
		return arrayList.size();
	}

	@Override
	public ScreenTypeBean getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ScreenTypeAdapterHoder holder = null;
		if (convertView == null) {
			holder = new ScreenTypeAdapterHoder();
			convertView = inflater.inflate(R.layout.screen_type_item, null);
			holder.screen_image = (ImageView) convertView.findViewById(R.id.screen_image);
			holder.screen_text = (TextView) convertView.findViewById(R.id.screen_text);
			holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
			convertView.setTag(holder);
		} else {
			holder = (ScreenTypeAdapterHoder) convertView.getTag();
		}
		ScreenTypeBean temp = arrayList.get(position);
		
		int text_height = everySize/7*2;
		LayoutParams lp_i = holder.screen_image.getLayoutParams();
		lp_i.width = everySize*3/2;
		lp_i.height = everySize*3/2;
		holder.screen_image.setLayoutParams(lp_i);
		
		LayoutParams lp_l = holder.layout.getLayoutParams();
		lp_l.width = lp_i.width;
		lp_l.height = lp_i.height+text_height;
		holder.layout.setLayoutParams(lp_l);
		
		LayoutParams lp_t = holder.screen_text.getLayoutParams();
		lp_t.width = lp_i.width;
		lp_t.height = text_height;
		holder.screen_text.setLayoutParams(lp_t);
		int textSize = DisplayUtil.px2sp(context, text_height/2);
		holder.screen_text.setTextSize(textSize);
		
		holder.screen_text.setText(temp.getScreen_text());
		
		holder.screen_image.setBackgroundDrawable(context.getResources().getDrawable(selectViewNum==position?R.drawable.screen_type_selected:R.drawable.screen_type));
		if(selectViewNum==position){
			holder.screen_text.setTextColor(context.getResources().getColor(R.color.select_color));
//			Animation testAnim = AnimationUtils.loadAnimation(context, R.anim.big_screen);
//			convertView.startAnimation(testAnim);
		}else{
			holder.screen_text.setTextColor(context.getResources().getColor(R.color.normal_color));
//			Animation testAnim = AnimationUtils.loadAnimation(context, R.anim.small_screen);
//			convertView.startAnimation(testAnim);
		}
		return convertView;
	}

	
	public void setSelected(int selectNum){
		selectViewNum = selectNum;
		super.notifyDataSetChanged();
	}
	
//	public static Bitmap createReflectedImage(Bitmap originalImage) {  
//        int width = originalImage.getWidth();  
//        int height = originalImage.getHeight();  
//        Matrix matrix = new Matrix();  
//        // 实现图片翻转90度  
//        matrix.preScale(1, -1);  
//        // 创建倒影图片（是原始图片的一半大小）  
//        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height / 2, width, height / 2, matrix, false);  
//        // 创建总图片（原图片 + 倒影图片）  
//        Bitmap finalReflection = Bitmap.createBitmap(width, (height + height / 2), Config.ARGB_8888);  
//        // 创建画布  
//        Canvas canvas = new Canvas(finalReflection);  
//        canvas.drawBitmap(originalImage, 0, 0, null);  
//        //把倒影图片画到画布上  
//        canvas.drawBitmap(reflectionImage, 0, height + 1, null);  
//        Paint shaderPaint = new Paint();  
//        //创建线性渐变LinearGradient对象  
//        LinearGradient shader = new LinearGradient(0, originalImage.getHeight(), 0, finalReflection.getHeight() + 1, 0x70ffffff,  
//                0x00ffffff, TileMode.MIRROR);  
//        shaderPaint.setShader(shader);  
//        shaderPaint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));  
//        //画布画出反转图片大小区域，然后把渐变效果加到其中，就出现了图片的倒影效果。  
//        canvas.drawRect(0, height + 1, width, finalReflection.getHeight(), shaderPaint);  
//        return finalReflection;  
//    }
}

class ScreenTypeAdapterHoder {
	public ImageView screen_image;
	public TextView screen_text;
	public LinearLayout layout;
}
