package com.android.activity.platform;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import ui.ScreenTitleLayout;
import util.HttpUrlPath;
import util.ScreenMonitorTools;
import util.Utility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;
import android.widget.Toast;

import com.android.activity.platform.adapter.ServerAdapter;
import com.android.activity.platform.model.ScreenModeBean;
import com.android.activity.platform.model.ServerTypeBean;
import com.android.activity.platform.model.WarehouseInformationBean;
import com.android.common.SimpleJSONUtil;
import com.loopj.android.http.RequestParams;
/**
 * @ClassName: ChooseServerListActivity 
 * @Description: 选择大屏幕的服务地址
 * @author gcy
 * @date 2014-9-25 下午4:42:33
 */
@SuppressLint("NewApi")
public class ChooseServerListActivity extends Activity implements OnItemSelectedListener,OnKeyListener{

	private Activity context;
	private GridView gridView;
	private	ServerAdapter adapter;
	private ScreenTitleLayout layout;
	private ScreenModeBean screenMode;
	private View main_s;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.server_list);
		context=ChooseServerListActivity.this;
		getInFrontOf();
		layout = (ScreenTitleLayout) findViewById(R.id.layout);
		gridView = (GridView) findViewById(R.id.gridView);
		setView();
	}
	
	/**
	 * @Description:获取来自于上一个页面的仓库ID
	 * @param
	 */
	private void getInFrontOf() {
		Intent intent = this.getIntent();
		screenMode = (ScreenModeBean) intent.getSerializableExtra("screenMode");
		if(screenMode.getHeightPixels()==0||screenMode.getWidthPixels()==0){
			screenMode = ScreenModeBean.getScreenMode(context);
		}
	}
	
	public void setView(){
		main_s = (View) findViewById(R.id.main_s);
		LayoutParams lp_warehouse_name = layout.getLayoutParams();
		lp_warehouse_name.height = screenMode.getEveryItem()+screenMode.getHeightWucha()/3+screenMode.getHeightWucha()%3;
		layout.setLayoutParams(lp_warehouse_name);
		layout.setView(lp_warehouse_name.height, screenMode.getTextSize(), "Server List",null);
		
		int paddingSize = (screenMode.getHeightPixels()-lp_warehouse_name.height)/6;
		int haveSize = screenMode.getWidthPixels() - paddingSize*2;
		int everySize =  haveSize/4;
		gridView.setColumnWidth(everySize);
		gridView.setPadding(paddingSize, paddingSize/2, paddingSize, 0);
		
		adapter = new ServerAdapter(context, ServerTypeBean.initServerList(),everySize/2);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ServerTypeBean s =(ServerTypeBean)parent.getAdapter().getItem(position);
				getHttpData(s);
 			}
		}); 
		gridView.setOnKeyListener(this);
		gridView.setOnItemSelectedListener(this);
		
		initGirdView();
	}
	
	public void getHttpData(final ServerTypeBean s){
		HttpUrlPath.setSP(s.getUrl(), context);
		RequestParams params = new RequestParams();
		params.put("Method", "getProductStorageCatalogTree");
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<WarehouseInformationBean> list = WarehouseInformationBean.parsingJSON(json);
				if(!Utility.isNullForList(list)){
					jumpAnotherActivity(s,list);
				}else{
					Toast.makeText(context, "Failed to obtain data!", Toast.LENGTH_SHORT).show();
				}
			}
		}.doGet((s != null) ? s.getServer_url(): HttpUrlPath.CheckInWareHouseBigScreenAction, params,ChooseServerListActivity.this);
	}
	
	public void jumpAnotherActivity(ServerTypeBean s,List<WarehouseInformationBean> list){
		if(main_s.getHeight()>screenMode.getHeightPixels()||main_s.getWidth()>screenMode.getWidthPixels()){
			screenMode = ScreenModeBean.getScreenMode(context, main_s.getHeight(), main_s.getWidth());
		}
		Intent intent = ScreenModeBean.setIntent(screenMode);
		intent.setClass(context,WarehouseListActivity.class);
		intent.putExtra("wiList", (Serializable)list);
		intent.putExtra("serverData", s);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in,R.anim.push_from_left_out);
	}
	
	
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		switch (v.getId()) {
		case R.id.gridView:		
			if(gridView!=null){
				if(adapter!=null&&adapter.getCount()>0){
					return ScreenMonitorTools.keyToolsForGirdView(keyCode,event,adapter,gridView);
				}
			}
			break;
		default:
			break;
		}
		return false;
	}	    
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		finish();
    	overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if(adapter!=null&&adapter.getCount()>0){
			adapter.setSelected(position);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {	}
	
	/**
	 * @Description:设置默认选中项
	 * @param
	 */
	public void initGirdView(){
		if(gridView!=null&&adapter!=null){
			gridView.requestFocus();
			gridView.setSelection(gridView.getSelectedItemPosition());
			adapter.setSelected(gridView.getSelectedItemPosition());
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initGirdView();
	}
}
