package com.android.activity.sceen.task.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.RemainJobsLaborBean;

public class RemainJobsLaborAdapter extends BaseAdapter  {
	
	private List<RemainJobsLaborBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	private Context context;

	public RemainJobsLaborAdapter(Context context, List<RemainJobsLaborBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.context=context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public RemainJobsLaborBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		RemainJobsLaborHoder holder = null;
		 if(convertView==null){
			holder = new RemainJobsLaborHoder();
			convertView = inflater.inflate(R.layout.remainjobs_labor_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);		
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);
	
			holder.labor_name = (TextView) convertView.findViewById(R.id.labor_name);
			holder.labor_name.setTextSize(textSize);	
			holder.location = (TextView) convertView.findViewById(R.id.location);
			holder.location.setTextSize(textSize);
//			holder.equipments = (TextView) convertView.findViewById(R.id.equipments); 
//			holder.equipments.setTextSize(textSize);
			holder.task_first = (TextView) convertView.findViewById(R.id.task_first); 
			holder.task_first.setTextSize(textSize);
			holder.task_num = (TextView) convertView.findViewById(R.id.task_num); 
			holder.task_num.setTextSize(textSize);
			holder.timevalue = (TextView) convertView.findViewById(R.id.timevalue); 
			holder.timevalue.setTextSize(textSize);

			
//			holder.lolocation=convertView.findViewById(R.id.lolocation);
//			//location宽
//			LayoutParams lp1=holder.lolocation.getLayoutParams();
//			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,resources.getString(R.string.task_remain_jobs_location));
//			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.location.getLayoutParams();
//			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
 			convertView.setTag(holder);
		}else{
			holder = (RemainJobsLaborHoder) convertView.getTag();
		}
		 
		RemainJobsLaborBean temp  =  arrayList.get(position);

		holder.labor_name.setText(temp.simulationData?temp.labor:"");
		holder.location.setText(temp.simulationData?temp.locations:"");
//		holder.equipments.setText(temp.simulationData?temp.equipments:"");
//		String str = "";
//		if(Integer.parseInt(ScreenMonitorTools.getTaskNum(temp.tasks))>1){
//			str = "...";
//		}
//		holder.task_first.setText(temp.simulationData?(ScreenMonitorTools.getTaskStr(temp.tasks)+str):"");
		holder.task_first.setText(temp.simulationData?temp.tasks:"");
		holder.task_num.setText(temp.simulationData?ScreenMonitorTools.getTaskNum(temp.tasks):"");
		holder.timevalue.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.timeValue):"");
                                                                               
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	
	public void setList(List<RemainJobsLaborBean> arrayList){
		this.arrayList = arrayList;
	}
}
class RemainJobsLaborHoder {
	public View layout;
	public View layout_id;//,lolocation;
	
	public TextView labor_name;
	public TextView location;
//	public TextView equipments;
	public TextView task_first;
	public TextView task_num;
	public TextView timevalue;
	public View color_layout;
}