package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.ReceiveScanAdapter;
import com.android.activity.sceen.task.model.ReceiveScanBean;
import com.android.core.CommonFragment;
/**
 * @ClassName: ReceiveScan 
 * @Description: 
 * @author gcy
 * @date 2015-4-23 下午3:03:27
 */
public class ReceiveScan extends CommonFragment<ReceiveScanBean> {

	public static final String method = "remainJobs";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_receive_scan;
	}

	@Override
	public List<ReceiveScanBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return ReceiveScanBean.getBaseTypeList(json);
	}

	@Override
	public ReceiveScanBean newBean() {
		// TODO Auto-generated method stub
		return new ReceiveScanBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new ReceiveScanAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
