package com.android.activity.sceen.windows.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: DockCloseNoCheckOutBean 
 * @Description: DockClose 的bean类 但是没有checkout
 * @author gcy
 * @date 2014-9-26 上午9:48:50
 */
public class ForgetCloseTaskBean implements Serializable {

	private static final long serialVersionUID = -8571376779684765735L;
	
//    "labor": "Printer",  
//    "number": "MIGUEL GARCIA", Task 
//    "dlo_id": 120946,
//    "number_type": 14, Task  TYPE
//    "rel_type": "In"
	
	public String labor;	
	public String number;
	public String dlo_id;	
	public String number_type;
	public String rel_type;
	public boolean simulationData;//判断是否是真数据
	
	/*********************************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<ForgetCloseTaskBean> getBaseTypeList(JSONObject json){
		List<ForgetCloseTaskBean> beanList = new ArrayList<ForgetCloseTaskBean>();
		JSONArray dockCloseNoCheckOutBeanArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(dockCloseNoCheckOutBeanArrays));
		if(flag){
			for (int i = 0; i < dockCloseNoCheckOutBeanArrays.length(); i++) {
				JSONObject jsonItem = dockCloseNoCheckOutBeanArrays.optJSONObject(i);
				ForgetCloseTaskBean bean = new ForgetCloseTaskBean();
				bean.labor = jsonItem.optString("labor");
				bean.number_type = jsonItem.optString("number_type");
				bean.number = jsonItem.optString("number");
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.rel_type = jsonItem.optString("rel_type");
				
				bean.simulationData = true;
				beanList.add(bean);
			}			
			return beanList;
		}
		return null;
	}
}
