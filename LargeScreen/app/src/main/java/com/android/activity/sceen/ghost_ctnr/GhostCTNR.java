package com.android.activity.sceen.ghost_ctnr;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.ghost_ctnr.adapter.GhostCTNRAdapter;
import com.android.activity.sceen.ghost_ctnr.model.GhostCTNRBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class GhostCTNR extends CommonFragment<GhostCTNRBean> {

	public static final String method = "ghostCTNR";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.ghost_ctnr;
	}

	@Override
	public List<GhostCTNRBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return GhostCTNRBean.getBaseTypeList(json);
	}

	@Override
	public GhostCTNRBean newBean() {
		// TODO Auto-generated method stub
		return new GhostCTNRBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new GhostCTNRAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
