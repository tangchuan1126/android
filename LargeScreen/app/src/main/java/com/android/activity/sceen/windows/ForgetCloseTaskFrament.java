package com.android.activity.sceen.windows;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.windows.adapter.ForgetAdapter;
import com.android.activity.sceen.windows.model.ForgetCloseTaskBean;
import com.android.core.CommonFragment;
/**
 * @ClassName: DockCloseFrament 
 * @Description: 用于显示DockClose中没有CheckOut的数据
 * @author gcy
 * @date 2014-9-26 上午9:52:12
 */
public class ForgetCloseTaskFrament extends CommonFragment<ForgetCloseTaskBean> {
	
	public static final String method = "forgetCloseTask";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.forget_close_task_layout_frament;
	}

	@Override
	public List<ForgetCloseTaskBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return ForgetCloseTaskBean.getBaseTypeList(json);
	}

	@Override
	public ForgetCloseTaskBean newBean() {
		// TODO Auto-generated method stub
		return new ForgetCloseTaskBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new ForgetAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}
}
