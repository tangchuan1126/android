package com.android.activity.sceen.scanning;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.scanning.adapter.RemainScanningJobAdapter;
import com.android.activity.sceen.scanning.model.RemainScanningJobBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class RemainScanningJob extends CommonFragment<RemainScanningJobBean> {

	public static final String method = "scanschedule";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.scanning_remain_scanning_job;
	}

	@Override
	public List<RemainScanningJobBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return RemainScanningJobBean.getBaseTypeList(json);
	}

	@Override
	public RemainScanningJobBean newBean() {
		// TODO Auto-generated method stub
		return new RemainScanningJobBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new RemainScanningJobAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
