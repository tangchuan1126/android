package com.android.activity.sceen.windows.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.windows.model.WaitingBean;

public class WaitingAdapter extends BaseAdapter  {
	
	private List<WaitingBean> arrayList;
	private Context context;
	private LayoutInflater inflater;
	private Resources resources;
	private int everyItem;
	private int textSize;
	
	public WaitingAdapter(Context context, List<WaitingBean> arrayList ,int everyItem,int textSize) {
		super();
		this.context = context;
		this.resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public WaitingBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WaitingBaseHoder holder = null;
		 if(convertView==null){
			holder = new WaitingBaseHoder();
			convertView = inflater.inflate(R.layout.waiting_listview_item,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);		
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);
			LayoutParams lp_id = holder.layout_id.getLayoutParams();
			lp_id.height = everyItem;
			holder.layout_id.setLayoutParams(lp_id);			
						
			holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
			holder.entry_id.setTextSize(textSize);
			holder.task_first = (TextView) convertView.findViewById(R.id.task_first);
			holder.task_first.setTextSize(textSize);
			holder.task_num = (TextView) convertView.findViewById(R.id.task_num);
			holder.task_num.setTextSize(textSize);
			
//			holder.gate_driver_liscense = (TextView) convertView.findViewById(R.id.gate_driver_liscense);
//			holder.gate_driver_liscense.setTextSize(textSize);
			holder.gate_driver_name = (TextView) convertView.findViewById(R.id.gate_driver_name);
			holder.gate_driver_name.setTextSize(textSize);
			holder.timevalue = (TextView) convertView.findViewById(R.id.timevalue);
			holder.timevalue.setTextSize(textSize);
			holder.waitingType = (TextView) convertView.findViewById(R.id.waitingtype);
			holder.waitingType.setTextSize(textSize);
			

			holder.priority=(TextView)convertView.findViewById(R.id.priority);
			holder.priority.setTextSize(textSize);
			
 			convertView.setTag(holder);
		}else{
			holder = (WaitingBaseHoder) convertView.getTag();
		}
		 
		WaitingBean temp  =  arrayList.get(position);
		holder.entry_id.setText(temp.simulationData?temp.dlo_id:"");
//		String str = "";
//		if(Integer.parseInt(ScreenMonitorTools.getTaskNum(temp.tasknumbers))>1){
//			str = "...";
//		}
		
		holder.waitingType.setText(temp.simulationData?temp.waitingtype:"");
		holder.task_first.setText(temp.simulationData?temp.tasknumbers:"");
		holder.task_num.setText(temp.simulationData?ScreenMonitorTools.getTaskNum(temp.tasknumbers):"");

//		holder.gate_driver_liscense.setText(temp.simulationData?temp.gate_driver_liscense:"");
		holder.gate_driver_name.setText(temp.simulationData?temp.gate_driver_name:"");
		holder.timevalue.setText(temp.simulationData?ScreenMonitorTools.fixTime(temp.total_time):"");
		

		holder.priority.setText(temp.simulationData?temp.priority:"");
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}
	public void setList(List<WaitingBean> arrayList){
		this.arrayList = arrayList;
	}
}
class WaitingBaseHoder {
	public View layout;
	public View layout_id;
	public TextView entry_id;	
//	public TextView tasks;
	public TextView task_first;
	public TextView task_num;
//	public TextView gate_driver_liscense;
	public TextView gate_driver_name;  
	public TextView timevalue;
	public TextView waitingType;
	public TextView priority;
}