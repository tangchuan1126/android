package com.android.activity.sceen.windows.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class NoTaskBean implements Serializable {

	private static final long serialVersionUID = 551957807026183363L;
	public String dlo_id;
	public String locations;
	public String tractor;
	public String trailer;
	public int total_time;
	public int is_late;
	public boolean simulationData;//判断是否是真数据
	
	/***************************************************************************/
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<NoTaskBean> getBaseTypeList(JSONObject json){
		List<NoTaskBean> baseList = new ArrayList<NoTaskBean>();
		JSONArray spotSituationBaseArrays = json.optJSONArray("data");
		boolean flag =(spotSituationBaseArrays!=null&&spotSituationBaseArrays.length()>0);
		if(flag){
			for (int i = 0; i < spotSituationBaseArrays.length(); i++) {
				JSONObject jsonItem = spotSituationBaseArrays.optJSONObject(i);
				NoTaskBean bean = new NoTaskBean();	
				
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.locations = jsonItem.optString("locations");
				bean.tractor = jsonItem.optString("tractor");
				bean.trailer = jsonItem.optString("trailer");
				bean.total_time = jsonItem.optInt("total_time");
				bean.is_late = jsonItem.optInt("is_late");
				
				bean.simulationData = true;
				baseList.add(bean);
			}
			
			return baseList;
		}
		return null;
	}
	
}
