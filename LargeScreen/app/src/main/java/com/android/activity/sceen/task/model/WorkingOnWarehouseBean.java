package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * @ClassName: PlatformAdvertisingBaseType
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class WorkingOnWarehouseBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6628695388274588551L;	
	public String location;
	public String dlo_id;
	public String rel_type;
	public int receipt_no;
	public int linesqty;
	
	public String dlo_detail_id;//不显示
	public String number_type;
	public String number;
	public String labor;
	public String equipment_number;
	
	public double donecount;//完成度百分比
	public String total_pallet;
	public int total_time;;

	public int is_late;
	public int flagHelp;
	public boolean simulationData;//判断是否是真数据 true为真数据 false为假数据
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<WorkingOnWarehouseBean> getBaseTypeList(JSONObject json){
		List<WorkingOnWarehouseBean> list = new ArrayList<WorkingOnWarehouseBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				WorkingOnWarehouseBean bean = new WorkingOnWarehouseBean();

				bean.location = jsonItem.optString("location");
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.rel_type = jsonItem.optString("rel_type");
				bean.receipt_no = jsonItem.optInt("receipt_no");
				bean.linesqty = jsonItem.optInt("linesqty");
				bean.dlo_detail_id = jsonItem.optString("dlo_detail_id");
				bean.number_type = jsonItem.optString("number_type");
				bean.number = jsonItem.optString("number");
				bean.labor = jsonItem.optString("labor");
				bean.equipment_number = jsonItem.optString("equipment_number");
				
				bean.donecount = jsonItem.optDouble("donecount");
				bean.total_pallet = jsonItem.optString("total_pallet");
				bean.total_time = jsonItem.optInt("total_time");

				bean.is_late = jsonItem.optInt("is_late");
				bean.flagHelp = jsonItem.optInt("flag_help", 0);
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}
	
	
//	public static String getNumber_type(int number_type){
////		10,11,17,18 ..  显示OutBound
////		13,14，12 显示Inbound
//		if(number_type==12||number_type==13||number_type==14){
//			return "Inbound";
//		}else{
//			return "Outbound";
//		}
//	}
	
}
