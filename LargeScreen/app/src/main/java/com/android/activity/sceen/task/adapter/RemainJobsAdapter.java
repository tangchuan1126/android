package com.android.activity.sceen.task.adapter;

import java.util.List;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.Utility;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.activity.sceen.task.model.RemainJobsBean;

public class RemainJobsAdapter extends BaseAdapter  {
	
	private Context context;
	
	private List<RemainJobsBean> arrayList;
	
	private LayoutInflater inflater;
	private Resources resources;

	private int everyItem;
	private int textSize;

	public RemainJobsAdapter(Context context, List<RemainJobsBean> arrayList ,int everyItem,int textSize) {
		super();
		resources = context.getResources();
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
		this.everyItem = everyItem;
		this.textSize = textSize;
		this.context=context;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public RemainJobsBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TwoPlatformAdvertisingAdapterHoder holder = null;
		 if(convertView==null){
			holder = new TwoPlatformAdvertisingAdapterHoder();
			convertView = inflater.inflate(R.layout.frag_remainjob_it,null);
			holder.layout = (View) convertView.findViewById(R.id.layout);	
			
			holder.layout_id = (View) convertView.findViewById(R.id.layout_id);		
			LayoutParams lp = holder.layout_id.getLayoutParams();
			lp.height = everyItem;
			holder.layout_id.setLayoutParams(lp);

			
			holder.loLocation=convertView.findViewById(R.id.loLocation);
			holder.tvLoc=(TextView)convertView.findViewById(R.id.tvLoc);
			holder.tvEntryId=(TextView)convertView.findViewById(R.id.tvEntryId);
			holder.tvLabor=(TextView)convertView.findViewById(R.id.tvLabor);
			holder.tvTasks=(TextView)convertView.findViewById(R.id.tvTasks);
			holder.tvStartTime=(TextView)convertView.findViewById(R.id.tvStartTime);
 
			holder.tvLoc.setTextSize(textSize);
			holder.tvEntryId.setTextSize(textSize);
			holder.tvLabor.setTextSize(textSize);
			holder.tvTasks.setTextSize(textSize);
			holder.tvStartTime.setTextSize(textSize);
			
			holder.color_layout = (View) convertView.findViewById(R.id.color_layout); 
			LayoutParams lp_l = holder.color_layout.getLayoutParams();
			lp_l.height = everyItem;
			holder.color_layout.getBackground().setAlpha(80);
			holder.color_layout.setLayoutParams(lp_l);	
			
			//location宽
			LayoutParams lp1=holder.loLocation.getLayoutParams();
			lp1.width=(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize*7/8) ,"Location");
			ViewGroup.MarginLayoutParams lp2=(ViewGroup.MarginLayoutParams)holder.tvLoc.getLayoutParams();
			lp2.leftMargin=-(int)Utility.getTextLen(DisplayUtil.sp2px(context, textSize) ,"    ");
			
 			convertView.setTag(holder);
		}else{
			holder = (TwoPlatformAdvertisingAdapterHoder) convertView.getTag();
		}
		
		RemainJobsBean b  =  arrayList.get(position);
		
		if(b.simulationData){
			holder.tvLoc.setText(b.location);
			holder.tvEntryId.setText(b.dlo_id);
			holder.tvLabor.setText(b.labor);
			holder.tvTasks.setText(b.tasks);
			holder.tvStartTime.setText(ScreenMonitorTools.fixTime(b.total_time));
		}
		else
		{
			holder.tvLoc.setText("");
			holder.tvEntryId.setText("");
			holder.tvLabor.setText("");
			holder.tvTasks.setText("");
			holder.tvStartTime.setText("");
		}
		
		if(b.simulationData&&b.is_late==1){
			holder.color_layout.setVisibility(View.VISIBLE);
		}else{
			holder.color_layout.setVisibility(View.GONE);
		}
		
		holder.layout.setBackgroundDrawable(resources.getDrawable(position%2==0?R.drawable.list_center_gray:R.drawable.list_center));
 		return convertView;
	}


	public void setList(List<RemainJobsBean> arrayList){
		this.arrayList = arrayList;
	}
	
}
class TwoPlatformAdvertisingAdapterHoder {
	public View layout;
	public View layout_id,loLocation;
	TextView tvLoc,tvEntryId,tvLabor,tvTasks,tvStartTime;
	
	public View color_layout;
}