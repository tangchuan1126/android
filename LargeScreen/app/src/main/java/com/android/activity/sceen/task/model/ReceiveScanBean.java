package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;
/**
 * @ClassName: ReceiveScanBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-23 下午3:05:08
 */
public class ReceiveScanBean implements Serializable {


	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 4321168962538550156L;

	/*****************************************/
	
	public String receipt_no;
	public String item_id;
	public String status;
	public String labor;
	public int progress;
	public int total_pallets;
	public int time;
	
	
	public boolean simulationData;//判断是否是真数据
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<ReceiveScanBean> getBaseTypeList(JSONObject json){
		List<ReceiveScanBean> list = new ArrayList<ReceiveScanBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				ReceiveScanBean bean = new ReceiveScanBean();	
				
				bean.receipt_no = jsonItem.optString("receipt_no");
				bean.item_id = jsonItem.optString("item_id");
				bean.status = jsonItem.optString("status");
				bean.labor = jsonItem.optString("labor");
				bean.progress = jsonItem.optInt("progress");
				bean.total_pallets = jsonItem.optInt("total_pallets");
				bean.time = jsonItem.optInt("time");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}
	
	
}
