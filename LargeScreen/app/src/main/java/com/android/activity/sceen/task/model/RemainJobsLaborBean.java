package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * @ClassName: SelectGateCheckInBaseType
 * @Description: bean
 * @author A18ccms a18ccms_gmail_com
 * @date 2014-8-21 上午11:04:41
 */
public class RemainJobsLaborBean implements Serializable {


	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 5432563042950163869L;
	/*****************************************/
	public String labor;
	public String locations;
	public String equipments;
	public String tasks;
	public String task_count;
	public int timeValue;
	public boolean simulationData;//判断是否是真数据
	/*****************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<RemainJobsLaborBean> getBaseTypeList(JSONObject json){
		List<RemainJobsLaborBean> list = new ArrayList<RemainJobsLaborBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				RemainJobsLaborBean bean = new RemainJobsLaborBean();	
				bean.labor=jsonItem.optString("labor");
				bean.locations=jsonItem.optString("locations");
				bean.equipments=jsonItem.optString("equipments");
				bean.tasks=jsonItem.optString("tasks");
				bean.task_count=jsonItem.optString("task_count");
				bean.timeValue=jsonItem.optInt("total_time");
				
				bean.simulationData = true;
				list.add(bean);
			}
			return list;
		}
		return null;
	}	
}