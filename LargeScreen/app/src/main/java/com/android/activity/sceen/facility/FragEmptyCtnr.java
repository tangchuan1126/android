package com.android.activity.sceen.facility;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.facility.adapter.EmptyCtnrAdapter;
import com.android.activity.sceen.facility.model.DockCloseNoCheckOutBean;
import com.android.core.CommonFragment;

/**
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class FragEmptyCtnr extends CommonFragment<DockCloseNoCheckOutBean> {
	
	public static final String method = "emptyCTNR";

	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.frag_fields_emptyctnr;
	}

	@Override
	public List<DockCloseNoCheckOutBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return DockCloseNoCheckOutBean.getBaseTypeList(json);
	}


	@Override
	public DockCloseNoCheckOutBean newBean() {
		// TODO Auto-generated method stub
		return new DockCloseNoCheckOutBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new EmptyCtnrAdapter(mActivity,  getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
