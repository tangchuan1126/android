package com.android.activity.sceen.task;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.task.adapter.RemainJobsLaborAdapter;
import com.android.activity.sceen.task.model.RemainJobsLaborBean;
import com.android.core.CommonFragment;

/**
 * task-处理中
 * 
 * @author 朱成
 * @date 2014-12-13
 */
public class RemainJobsLabor extends CommonFragment<RemainJobsLaborBean> {

	public static final String method = "remainJobsLabor";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.remainjobs_labor_layout;
	}

	@Override
	public List<RemainJobsLaborBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return RemainJobsLaborBean.getBaseTypeList(json);
	}

	@Override
	public RemainJobsLaborBean newBean() {
		// TODO Auto-generated method stub
		return new RemainJobsLaborBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new RemainJobsLaborAdapter(mActivity, getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}

}
