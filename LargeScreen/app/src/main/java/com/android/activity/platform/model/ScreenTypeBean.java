package com.android.activity.platform.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class ScreenTypeBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	private String screen_image;
	private String screen_text;
	
	/**************************************************************/
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<ScreenTypeBean> getBaseTypeList(JSONObject json){
		List<ScreenTypeBean> list = new ArrayList<ScreenTypeBean>();
		ScreenTypeBean tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Task Monitor");
		list.add(tmpBean);
		
		tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Facility Monitor");
		list.add(tmpBean);
		
		tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Window Monitor");
		list.add(tmpBean);
		
		tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Load / Receive");
		list.add(tmpBean);
		
		tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Variance Monitor");
		list.add(tmpBean);
		
		tmpBean = new ScreenTypeBean();
		tmpBean.setScreen_text("Scanning Monitor");
		list.add(tmpBean);
		
		return list;		
	}
	
	
	/**************************************************************/
	public String getScreen_image() {
		return screen_image;
	}
	public void setScreen_image(String screen_image) {
		this.screen_image = screen_image;
	}
	public String getScreen_text() {
		return screen_text;
	}
	public void setScreen_text(String screen_text) {
		this.screen_text = screen_text;
	}
	
	
	
}
