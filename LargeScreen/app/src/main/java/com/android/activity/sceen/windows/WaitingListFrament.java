package com.android.activity.sceen.windows;

import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import android.widget.BaseAdapter;

import com.android.activity.sceen.windows.adapter.WaitingAdapter;
import com.android.activity.sceen.windows.model.WaitingBean;
import com.android.core.CommonFragment;

public class WaitingListFrament extends CommonFragment<WaitingBean> {
	
	public static final String method = "waitingList";
	
	@Override
	public int getFieldsView() {
		// TODO Auto-generated method stub
		return R.layout.waiting_layout_frament;
	}

	@Override
	public List<WaitingBean> parseBean(JSONObject json) {
		// TODO Auto-generated method stub
		return WaitingBean.getBaseTypeList(json);
	}

	@Override
	public WaitingBean newBean() {
		// TODO Auto-generated method stub
		return new WaitingBean();
	}

	@Override
	public BaseAdapter getAdapter() {
		// TODO Auto-generated method stub
		return new WaitingAdapter(mActivity, getList(),largeOrSmall ? tData.getLargeScreenEveryItem() : tData.getSmallScreenEveryItem(),largeOrSmall?tData.getLargeTextSize():tData.getSmallScreenTextSize());
	}
}
