package com.android.activity.platform.model;

import java.io.Serializable;

import util.DisplayUtil;
import util.ScreenMonitorTools;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
/**
 * @ClassName: ScreenMode 
 * @Description: 该bean类为全局通用类 含有屏幕尺寸以及通用行高及字体大小
 * @author gcy
 * @date 2014-9-25 下午4:38:43
 */
public class ScreenModeBean implements Serializable  {
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	private int pageSize;//大屏幕请求的数据总数
	private int pageNo;//大屏幕的的页码
	private int everyItem;//通用21行行高px
	private int widthPixels;//屏幕的宽px
	private int heightPixels;//屏幕的高px
	private int heightWucha;//误差 由于所求尺寸均为int类型 浮点后的数据被省略掉了 
	private int padding;//padding的尺寸 px
	private int widthWucha;//误差 由于所求尺寸均为int类型 浮点后的数据被省略掉了  
	private int textSize;//字体大小

	private int navigationHeight;
	
	private int smallEveryItem;
	private int smallTitleHeight;
	private int smallNavigationHeight;
	
	
	private int sixScreenEveryItem;
	private int sixScreenTitleHeight;
	private int sixScreenNavigationHeight;
	private int sixScreenTextSize;
	private int sixWucha;
	
	private int largeEveryItem;
	private int largeTitleHeight;
	private int largeNavigationHeight;
	
	/*********************************************************/
	
	public static ScreenModeBean getScreenMode(Activity activity){
		DisplayMetrics  dm = ScreenMonitorTools.getDisplayMetrics(activity);
		ScreenModeBean s = getScreenMode(activity, dm.heightPixels, dm.widthPixels);
		return s;
	}
	
	public static ScreenModeBean getScreenMode(Activity activity,int heightPixels,int widthPixels){
		ScreenModeBean s = new ScreenModeBean();
		s.setPageSize(ScreenMonitorTools.TASK_BIG_SCREEN_SIZE_NUM);//设置屏幕有多少条数据
		s.setHeightPixels(heightPixels);
		s.setWidthPixels(widthPixels);
		s.setPadding(ScreenMonitorTools.getPadding(activity));
		s.setEveryItem(ScreenMonitorTools.getEveryItem(s.getHeightPixels() - 4*s.getPadding())); //meiyixiang
		s.setTextSize(DisplayUtil.px2sp(activity,s.getEveryItem()/2));
		s.setHeightWucha(ScreenMonitorTools.getHeightWucha(s.getHeightPixels() - 4*s.getPadding()));
		s.setWidthWucha(ScreenMonitorTools.getWidthWucha(s.getWidthPixels()));

		
		double biaozhun = 0.8;
		int small = (int)(s.getEveryItem()*biaozhun);
		int navigationDefult = s.getEveryItem()+s.getHeightWucha()/3+s.getHeightWucha()%3;
		
		int smallTitle = small;
		int smallNavigation = small+s.getHeightWucha()/3;
		int smallzong = s.getHeightPixels()-s.getPadding()*4-(smallTitle+smallNavigation)*2-navigationDefult;
		int everyItem = smallzong/(ScreenMonitorTools.itemNum-5);
		int smallwc = smallzong-everyItem*(ScreenMonitorTools.itemNum-5);
		smallNavigation = smallNavigation + smallwc/2;
		s.setSmallEveryItem(everyItem);
		s.setSmallTitleHeight(smallTitle);
		s.setSmallNavigationHeight(smallNavigation);
		s.setNavigationHeight(navigationDefult+smallwc%2);
		//---------------------------------------------------------------------------------
		
		int height = s.getHeightPixels()-s.getNavigationHeight()-s.getPadding()*6;
		int everyItes = height/ScreenMonitorTools.DATAS_ALL_SIZE;
		int sixwucha = height%ScreenMonitorTools.DATAS_ALL_SIZE;
		int sixsmall = (int)(everyItes*biaozhun);
		int haveH = (everyItes-sixsmall)*6+everyItes*(ScreenMonitorTools.DATAS_ALL_SIZE-6)+sixwucha;
		everyItes = haveH/(ScreenMonitorTools.DATAS_ALL_SIZE-6);
		sixwucha = haveH%(ScreenMonitorTools.DATAS_ALL_SIZE-6);
		int sixNavigation = sixsmall+sixwucha/3;
		s.setSixScreenEveryItem(everyItes);
		s.setSixScreenNavigationHeight(sixNavigation);
		s.setSixScreenTitleHeight(sixsmall);
		s.setSixScreenTextSize(DisplayUtil.px2sp(activity,everyItes/2));
		s.setSixWucha(everyItes%2);
		
		//---------------------------------------------------------------------------------
		int largeTitle = small+s.getHeightWucha()/3;
		int largeNavigation = small+s.getHeightWucha()/3+s.getPadding()*2;
		int largezong = s.getHeightPixels()-largeTitle-largeNavigation-navigationDefult-s.getPadding()*2;
		int largeItem = largezong/(ScreenMonitorTools.itemNum-3);
		int largewc = largezong-largeItem*(ScreenMonitorTools.itemNum-3);
		largeTitle = largeTitle+largewc/2;
		largeNavigation = largeNavigation + largewc/2;
		s.setLargeEveryItem(largeItem);
		s.setLargeTitleHeight(largeTitle);
		s.setLargeNavigationHeight(largeNavigation);
		
		return s;
	}
	
	public static Intent setIntent(ScreenModeBean screenModeBean){
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable("screenMode", screenModeBean);
		intent.putExtras(bundle);
		return intent;
	}
	
	/*********************************************************/
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getEveryItem() {
		return everyItem;
	}
	public void setEveryItem(int everyItem) {
		this.everyItem = everyItem;
	}
	public int getWidthPixels() {
		return widthPixels;
	}
	public void setWidthPixels(int widthPixels) {
		this.widthPixels = widthPixels;
	}
	public int getHeightPixels() {
		return heightPixels;
	}
	public void setHeightPixels(int heightPixels) {
		this.heightPixels = heightPixels;
	}
	public int getHeightWucha() {
		return heightWucha;
	}
	public void setHeightWucha(int heightWucha) {
		this.heightWucha = heightWucha;
	}
	public int getPadding() {
		return padding;
	}
	public void setPadding(int padding) {
		this.padding = padding;
	}
	public int getWidthWucha() {
		return widthWucha;
	}
	public void setWidthWucha(int widthWucha) {
		this.widthWucha = widthWucha;
	}
	public int getTextSize() {
		return textSize;
	}
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	public int getSmallEveryItem() {
		return smallEveryItem;
	}

	public void setSmallEveryItem(int smallEveryItem) {
		this.smallEveryItem = smallEveryItem;
	}

	public int getSmallTitleHeight() {
		return smallTitleHeight;
	}

	public void setSmallTitleHeight(int smallTitleHeight) {
		this.smallTitleHeight = smallTitleHeight;
	}

	public int getSmallNavigationHeight() {
		return smallNavigationHeight;
	}

	public void setSmallNavigationHeight(int smallNavigationHeight) {
		this.smallNavigationHeight = smallNavigationHeight;
	}

	public int getLargeEveryItem() {
		return largeEveryItem;
	}

	public void setLargeEveryItem(int largeEveryItem) {
		this.largeEveryItem = largeEveryItem;
	}

	public int getLargeTitleHeight() {
		return largeTitleHeight;
	}

	public void setLargeTitleHeight(int largeTitleHeight) {
		this.largeTitleHeight = largeTitleHeight;
	}

	public int getLargeNavigationHeight() {
		return largeNavigationHeight;
	}

	public void setLargeNavigationHeight(int largeNavigationHeight) {
		this.largeNavigationHeight = largeNavigationHeight;
	}

	public int getNavigationHeight() {
		return navigationHeight;
	}

	public void setNavigationHeight(int navigationHeight) {
		this.navigationHeight = navigationHeight;
	}

	public int getSixScreenEveryItem() {
		return sixScreenEveryItem;
	}

	public void setSixScreenEveryItem(int sixScreenEveryItem) {
		this.sixScreenEveryItem = sixScreenEveryItem;
	}

	public int getSixScreenTitleHeight() {
		return sixScreenTitleHeight;
	}

	public void setSixScreenTitleHeight(int sixScreenTitleHeight) {
		this.sixScreenTitleHeight = sixScreenTitleHeight;
	}

	public int getSixScreenNavigationHeight() {
		return sixScreenNavigationHeight;
	}

	public void setSixScreenNavigationHeight(int sixScreenNavigationHeight) {
		this.sixScreenNavigationHeight = sixScreenNavigationHeight;
	}

	public int getSixScreenTextSize() {
		return sixScreenTextSize;
	}

	public void setSixScreenTextSize(int sixScreenTextSize) {
		this.sixScreenTextSize = sixScreenTextSize;
	}

	public int getSixWucha() {
		return sixWucha;
	}

	public void setSixWucha(int sixWucha) {
		this.sixWucha = sixWucha;
	}
}
