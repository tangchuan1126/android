package com.android.activity.sceen.task.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import util.StringUtil;

/**
 * 
 * @author Forever
 *
 */
public class GoingToWareHouseBean implements Serializable {

	private static final long serialVersionUID = 207138713268955304L;


	public String dlo_id;
	public int total_time;
	public String equipment_number;
	public String tasks;
	public int is_late;
	public String location;
	public String waiting;
	
	public String priority;
	
	public boolean simulationData;//判断是否是真数据
	
	
	
	/**
	 * @Description 解析JSON数据
	 * @return
	 */
	public static List<GoingToWareHouseBean> getBaseTypeList(JSONObject json){
		List<GoingToWareHouseBean> list = new ArrayList<GoingToWareHouseBean>();
		JSONArray platformAdvertisingBaseTypeArrays = json.optJSONArray("data");
		boolean flag =(!StringUtil.isNullForJSONArray(platformAdvertisingBaseTypeArrays));
		if(flag){
			for (int i = 0; i < platformAdvertisingBaseTypeArrays.length(); i++) {
				JSONObject jsonItem = platformAdvertisingBaseTypeArrays.optJSONObject(i);
				GoingToWareHouseBean bean = new GoingToWareHouseBean();
	
				bean.dlo_id = jsonItem.optString("dlo_id");
				bean.total_time=jsonItem.optInt("total_time");
				bean.equipment_number=jsonItem.optString("equipment_number");
				bean.tasks=jsonItem.optString("tasks");
				bean.is_late=jsonItem.optInt("is_late");
				bean.location=jsonItem.optString("location");
				bean.waiting=jsonItem.optString("waiting");

				bean.priority = jsonItem.optString("priority");
				
				bean.simulationData = true;
				list.add(bean);
			}

			
			return list;
		}
		return null;
	}

	
	
	
	
}