package com.android.core;

import oso.com.vvmescreen.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

public abstract class BaseActivity extends Activity {
	
	protected Activity thisActivity;

	public View vTopbar;
	public View vMiddle;

	public TextView warehouse_name;
	protected Button imgBack;
	protected Button btnRight;

	// 标题
	private String titleString;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		thisActivity = this;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// 若在edittext外 则下放
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
				InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}

		return super.onTouchEvent(event);
	}

	// ---
	private boolean hasNotifyReady = false;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		// 页面首次出现时
		if (hasFocus && !hasNotifyReady) {
			onFirstVisible();
			hasNotifyReady = true;
		}
	}

	// 首次显示时,用于提示ready等
	protected void onFirstVisible() {

	}

	// ---

	public String getTitleString() {
		return titleString;
	}

	public void setTitleString(String titleString) {
		this.titleString = titleString;

		if (warehouse_name != null)
			warehouse_name.setText(titleString);
	}

	public void setTitleString(int resId) {
		this.titleString = getString(resId);

		if (warehouse_name != null)
			warehouse_name.setText(titleString);
	}

	
	/**
	 * @param middleResID
	 * @param topbarResID
	 */
	public void setContentView(int middleResID, int topbarResID) {
		// TODO Auto-generated method stub
		super.setContentView(R.layout.lo_base);

		// 顶边栏
		ViewStub stub = (ViewStub) findViewById(R.id.base_topbar_before);
		if (topbarResID != 0)
			stub.setLayoutResource(topbarResID);
		vTopbar = (View) stub.inflate();

		// 内容区
		stub = (ViewStub) findViewById(R.id.base_middle_before);
		if (middleResID != 0)
			stub.setLayoutResource(middleResID);
		vMiddle = (View) stub.inflate();

		// 标题
		warehouse_name = (TextView) findViewById(R.id.warehouse_name);
	}

	private boolean isTouchInterrupt = false;

	/**
	 * 屏蔽界面控件的所有点击事件
	 */
	public void interruptTouchEvent(boolean isTouchInterrupt) {
		this.isTouchInterrupt = isTouchInterrupt;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		return isTouchInterrupt ? true : super.dispatchTouchEvent(ev);
	}

	public void onBackPressed() {
		// super.onBackPressed();
		onBackBtnOrKey();
	};

	// 点返回按钮、返回键时
	protected void onBackBtnOrKey() {
		BaseActivity.this.finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

}
