package com.android.core;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import util.ScreenMonitorTools;
import util.TheInitialScreenData;
import util.Utility;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.activity.common.BaseBean;
import com.android.activity.common.ScreenModeTypeBean;
import com.android.common.ListViewTop;

/**
 * @ClassName: CommonFragment 
 * @Description: 小屏幕的基类
 * @author gcy
 * @date 2015-4-1 下午4:13:33 
 * @param <TBean>
 */
public abstract class CommonFragment<TBean> extends Fragment {

	public Activity mActivity;
	private View view;
	private Bundle bundle;
	private ScreenModeTypeBean screenModeTypeBean;
	public TheInitialScreenData tData;
	private BaseAdapter adp;

	public boolean whetherToRun = true;// 控制线程的继续运行
	public boolean largeOrSmall;// 判断是大屏幕还是小屏幕
	private int total;//总数
	private int allSize;

	private TextView connection_dialog;// 请求数据时显示

	public int pageNo = 1;
	private int pageSize;// 每页请求的条数
	
	
	// debug
	private ListView lv;
	private List<TBean> listDatas;

	public abstract BaseAdapter getAdapter();

	public abstract List<TBean> parseBean(JSONObject json);

	public abstract TBean newBean();
	
	// 字段名行-xml
	public abstract int getFieldsView();
	
	public List<TBean> getList() {
		if (listDatas == null)
			listDatas = new ArrayList<TBean>();

		return listDatas;
	}

	// =================================

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.lo_basefrag, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mActivity = this.getActivity();
		bundle = getArguments();
		allSize = bundle.getInt("allSize");
		largeOrSmall = bundle.getBoolean("largeOrSmall");
		screenModeTypeBean = (ScreenModeTypeBean) bundle.getSerializable("screenModeTypeBean");
		tData = (TheInitialScreenData) bundle.getSerializable("theInitialScreenData");
		initView();
	}


	private void initView() {
		pageSize = largeOrSmall ? (allSize-2)/*减去导航和小标题*/ : screenModeTypeBean.getScreenPostSize();
		Log.v("--------------------------", pageSize+"");
		connection_dialog = (TextView) view.findViewById(R.id.connection_dialog);
		connection_dialog.setVisibility(View.VISIBLE);
		connection_dialog.setTextSize(tData.getTextSize() * 2);

		// 初始化-字段行textsize
		ViewStub vsFields = (ViewStub) view.findViewById(R.id.vsFields);
		vsFields.setLayoutResource(getFieldsView());
		ViewGroup loFields = (ViewGroup) vsFields.inflate();
		LayoutParams lp_f = loFields.getLayoutParams();
		lp_f.height = largeOrSmall ? tData.getLargeScreenTitleHeight(): tData.getSmallScreenTitleHeight();
		
		int size = ScreenMonitorTools.getTitleSize(largeOrSmall ? tData.getLargeTextSize():tData.getSmallScreenTextSize());
		for (int i = 0; i < loFields.getChildCount(); i++) {
			View tmpV = loFields.getChildAt(i);
			if (tmpV instanceof TextView)
				((TextView) tmpV).setTextSize(size);
		}

		lv = (ListView) view.findViewById(R.id.lv);
		LayoutParams lp_v1 = lv.getLayoutParams();
		lp_v1.height = (largeOrSmall ? tData.getLargeScreenEveryItem(): tData.getSmallScreenEveryItem()) * pageSize;
		lv.setLayoutParams(lp_v1);

		adp = getAdapter();
		lv.setAdapter(adp);

		ListViewTop.setSixListViewTops(mActivity, view, tData, screenModeTypeBean.getScreenName(), largeOrSmall);

		
		setTopBmg(largeOrSmall);
	}

	public void alertDialog() {
		connection_dialog.setVisibility(View.VISIBLE);
	};

	/**
	 * @Description:设置标题栏的颜色
	 * @param @param onFocus
	 */
	public void setTopBmg(boolean onFocus) {
		(view.findViewById(R.id.spot_top)).setBackgroundResource(onFocus ? R.drawable.list_top_blue : R.drawable.list_top);
	}

	/**
	 * @Description:计算获取大屏幕数据的第几页 例ScreenMonitorTools.FACILITY_SPOT_SIZE
	 * @param @return
	 */
	public int getBigScreenPageNo(int size) {
		return ScreenMonitorTools.getPageNo(size,ScreenMonitorTools.getRealPage(pageNo, total), total,allSize);
	}
	/**
	 * @Description:计算获取大屏幕数据的第几页 例ScreenMonitorTools.FACILITY_SPOT_SIZE
	 * @param @return
	 */
	public int getSmallScreenPageNo() {
		return ScreenMonitorTools.getPageNo(allSize,ScreenMonitorTools.getRealPage(pageNo, total), total,screenModeTypeBean.getScreenPostSize());
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo > total ? 1 : pageNo;
	}

	/**
	 * @Description:解析请求数据
	 * @param @param json
	 * @param @param bean 可以为空 如果是多个小屏显示则不能为空 只显示一个独屏可以为空 用于判断请求几次无响应后为超时
	 */
	public void helpParsingJson(JSONObject json,BaseBean bean) {
		List<TBean> lists = parseBean(json);

		total = ScreenMonitorTools.getTotal(json);
		int getSize = (whetherToRun && lists != null) ? lists.size() : 0;

		if (getSize < pageSize) {
			if (Utility.isNullForList(lists)) {
				lists = new ArrayList<TBean>();
			}
			for (int i = 0; i < pageSize - getSize; i++) {
				lists.add(newBean());
			}
		}

		getList().clear();
		getList().addAll(lists);
		adp.notifyDataSetChanged();
		// 渐变动画
		Animation anim = AnimationUtils.loadAnimation(mActivity,
				R.anim.fade_in_fromhalf);
		lv.startAnimation(anim);

		ListViewTop.setCurrentPageAndConutPage(mActivity, view, pageNo, total,
				ScreenMonitorTools.getTotal_Num(json));
		connection_dialog.setVisibility(View.GONE);
		
		//---------用于判断请求几次无响应后为超时
		if(bean!=null){
			bean.setPostFrequency(0);//清空请求次数 使之为0
		}

		pageNo++;
		pageNo = (pageNo > total) ? 1 : pageNo;
	}

}
