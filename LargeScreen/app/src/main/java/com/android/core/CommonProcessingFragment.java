package com.android.core;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import oso.com.vvmescreen.R;
import util.DisplayUtil;
import util.ScreenMonitorTools;
import util.TheInitialScreenData;
import util.Utility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.activity.common.BaseBean;
import com.android.activity.common.ScreenModeTypeBean;
import com.android.activity.process.model.ScreenProcessBean;
import com.android.activity.process.model.StepDataBean;
import com.android.common.FlowLayout;
import com.android.common.ListViewTop;
import com.android.common.ui.BadgeView;

/**
 * @ClassName: CommonFragment 
 * @Description: 小屏幕的基类
 * @author gcy
 * @date 2015-4-1 下午4:13:33 
 * @param <TBean>
 */
public abstract class CommonProcessingFragment<TBean> extends Fragment {

	public Activity mActivity;
	private View view;
	private Bundle bundle;
	private ScreenModeTypeBean screenModeTypeBean;
	public TheInitialScreenData tData;
	public abstract int getJsonType();
	
	
	public boolean whetherToRun = true;// 控制线程的继续运行
	public boolean largeOrSmall;// 判断是大屏幕还是小屏幕
	private int total;//总数
	private int allSize;

	private TextView connection_dialog;// 请求数据时显示

	public int pageNo = 1;
	private int pageSize;// 每页请求的条数
	
	private LinearLayout layout;
	private FlowLayout workflowLt;//点选工作流程
	private FlowLayout screenLt;//点选大屏幕
//	private List<ScreenProcessBean> screenList;//大屏幕数据集
	// =================================

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.lo_process_basefrag, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mActivity = this.getActivity();
		bundle = getArguments();
		allSize = bundle.getInt("allSize");
		largeOrSmall = bundle.getBoolean("largeOrSmall");
		screenModeTypeBean = (ScreenModeTypeBean) bundle.getSerializable("screenModeTypeBean");
		tData = (TheInitialScreenData) bundle.getSerializable("theInitialScreenData");
		initView();
	}


	private void initView() {
		pageSize = largeOrSmall ? (allSize-2)/*减去导航和小标题*/ : screenModeTypeBean.getScreenPostSize();

		connection_dialog = (TextView) view.findViewById(R.id.connection_dialog);
		connection_dialog.setVisibility(View.VISIBLE);
		connection_dialog.setTextSize(tData.getTextSize() * 2);

		layout = (LinearLayout) view.findViewById(R.id.layout);
		workflowLt = (FlowLayout) view.findViewById(R.id.flowLayout);
		screenLt = (FlowLayout) view.findViewById(R.id.screen_layout);
		
		LayoutParams lp_v1 = layout.getLayoutParams();
		lp_v1.height = (largeOrSmall ? tData.getLargeScreenEveryItem(): tData.getSmallScreenEveryItem()) * pageSize;
		layout.setLayoutParams(lp_v1);
		
		ListViewTop.setSixProcessViewTops(mActivity, view, tData, screenModeTypeBean.getScreenName(), largeOrSmall);
	}
	
	/**
	 * @Description:填充进度数据
	 * @param @param processList
	 */
	public void setProcessView(List<ScreenProcessBean> processList){
		if(!Utility.isNullForList(processList)){
			workflowLt.removeAllViews();			
			
			int titleHeight = largeOrSmall ? tData.getLargeScreenTitleHeight(): tData.getSmallScreenTitleHeight();
			int navigationHeight =  largeOrSmall?tData.getLargeScreenNavigationHeight():tData.getSmallScreenNavigationHeight();
			int bodyHeight = (largeOrSmall ? tData.getLargeScreenEveryItem(): tData.getSmallScreenEveryItem()) * pageSize;
			
			int height = titleHeight + navigationHeight + bodyHeight;
			
			int everyWidth = 0;
			int everyHeight = 0;
			
			
			double w = height/2*17/11;    //17*height/22;
			
			if(tData.getWidthPixels()/(w*6)>=1.5){ 
				everyHeight = height/2+height%2;
				everyWidth = everyHeight*17/11;
				layout.setOrientation(LinearLayout.HORIZONTAL);
				layout.setGravity(Gravity.CENTER_VERTICAL);
			}else{
				everyWidth = tData.getWidthPixels()/6/*processList.size()*/;
				everyHeight = everyWidth*11/17;
				layout.setOrientation(LinearLayout.VERTICAL);
				layout.setPadding(0, (int)mActivity.getResources().getDimension(R.dimen.platform_android_paddingLeft)*2, 0, 0);
			}
			
			int ts1 = DisplayUtil.px2sp(mActivity,everyWidth*15/17/8);
			int ts2 = ts1+3;
			
			for (int i = 0; i < processList.size(); i++) {
				LinearLayout ly = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.monitor_process_layout, null);
				View layout_view = (View) ly.findViewById(R.id.layout_view);
				LayoutParams a = layout_view.getLayoutParams();
				a.width = everyWidth;
				a.height= everyHeight;
				layout_view.setLayoutParams(a);
				
				TextView screen_name = (TextView) ly.findViewById(R.id.screen_name);
				TextView num_process = (TextView) ly.findViewById(R.id.num_process);
				screen_name.setTextSize(ts1);
				num_process.setTextSize(ts2);
				
				if(i==0){
					judgeHeadBg(processList.get(i).stepDataBean.color, layout_view);
				}else if ((i+1)==processList.size()) {
					judgeTailBg(processList.get(i).stepDataBean.color, layout_view);
				}else{
					judgeCenterBg(processList.get(i).stepDataBean.color, layout_view);
				}
				screen_name.setText(processList.get(i).screenName);
				num_process.setText(processList.get(i).stepDataBean.count+"/"+processList.get(i).stepDataBean.total);
				final ScreenProcessBean s = processList.get(i);
				layout_view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
//						jumpScreenActivity(s);
					}
				});
				layout_view.setOnTouchListener(new View.OnTouchListener() {
					@SuppressLint("NewApi")
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						 //按下操作
				        if(event.getAction()==MotionEvent.ACTION_DOWN){
				             v.setAlpha(0.8f);
				        }
				        //抬起操作
				        if(event.getAction()==MotionEvent.ACTION_UP){
				        	v.setAlpha(1.0f);
				        }
				        return false;
					}
				});
				workflowLt.addView(ly);
			}
		}
	}
	
	/**
	 * @Description:填充需要显示的屏幕
	 * @param @param list
	 */
	public void setScreenView(List<ScreenProcessBean> list){
		if(!Utility.isNullForList(list)){
			screenLt.removeAllViews();
			

			int titleHeight = largeOrSmall ? tData.getLargeScreenTitleHeight(): tData.getSmallScreenTitleHeight();
			int navigationHeight =  largeOrSmall?tData.getLargeScreenNavigationHeight():tData.getSmallScreenNavigationHeight();
			int bodyHeight = (largeOrSmall ? tData.getLargeScreenEveryItem(): tData.getSmallScreenEveryItem()) * pageSize;
			
			int height = titleHeight + navigationHeight + bodyHeight;
			
			int everyWidth = 0;
			int everyHeight = 0;
			
			
			double w = height/2*17/11;    //17*height/22;
			
			if(tData.getWidthPixels()/(w*6)>=1.5){ 
				everyHeight = height/2+height%2;
				everyWidth = everyHeight*7/4;
			}else{
				everyWidth = tData.getWidthPixels()/6/*processList.size()*/;
				everyHeight = everyWidth*4/7;
			}
			
			int ts = DisplayUtil.px2sp(mActivity,everyHeight/8)+3;
			
			for (int i = 0; i < list.size(); i++) {
				final ScreenProcessBean s = list.get(i);
				LinearLayout ly = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.monitor_button_layout, null);
				Button screen_name = (Button) ly.findViewById(R.id.screen_name);
				LayoutParams a = screen_name.getLayoutParams();
				a.width = everyWidth;
				a.height= everyHeight;
				screen_name.setLayoutParams(a);
				
				screen_name.setText(s.screenName);
				screen_name.setTextSize(ts);
				screen_name.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
//						jumpScreenActivity(s);
					}
				});
				
				BadgeView badge = new BadgeView(mActivity,screen_name);
				badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
				badge.setTextSize(ts);
				badge.setText(s.number+"");
				badge.setTextColor(Color.WHITE);
				badge.show();
				
				screenLt.addView(ly);
			}
		}
	}

	/**
	 * @Description:判断头背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeHeadBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red_head);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green_head);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow_head);
			break;
		}
	}
	/**
	 * @Description:判断中间背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeCenterBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow);
			break;
		}
	}
	/**
	 * @Description:判断尾背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeTailBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red_tail);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green_tail);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow_tail);
			break;
		}
	}
	
	
	
	public void alertDialog() {
		connection_dialog.setVisibility(View.VISIBLE);
	};

	/**
	 * @Description:计算获取大屏幕数据的第几页 例ScreenMonitorTools.FACILITY_SPOT_SIZE
	 * @param @return
	 */
	public int getBigScreenPageNo(int size) {
		return ScreenMonitorTools.getPageNo(size,ScreenMonitorTools.getRealPage(pageNo, total), total,allSize);
	}
	/**
	 * @Description:计算获取大屏幕数据的第几页 例ScreenMonitorTools.FACILITY_SPOT_SIZE
	 * @param @return
	 */
	public int getSmallScreenPageNo() {
		return ScreenMonitorTools.getPageNo(allSize,ScreenMonitorTools.getRealPage(pageNo, total), total,screenModeTypeBean.getScreenPostSize());
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo > total ? 1 : pageNo;
	}

	/**
	 * @Description:解析请求数据
	 * @param @param json
	 * @param @param bean 可以为空 如果是多个小屏显示则不能为空 只显示一个独屏可以为空 用于判断请求几次无响应后为超时
	 */
	public void helpParsingJson(JSONObject json,BaseBean bean) {
		Map<String, List<ScreenProcessBean>> map = ScreenProcessBean.handJson(json, getJsonType());
		List<ScreenProcessBean> processList = map.get("process");
		List<ScreenProcessBean> screenList = map.get("screen");
		connection_dialog.setVisibility(View.GONE);
		if(!Utility.isNullForList(processList)){
			setProcessView(processList);
			setScreenView(screenList);	
		}
		
		//---------用于判断请求几次无响应后为超时
		if(bean!=null){
			bean.setPostFrequency(0);//清空请求次数 使之为0
		}
	}

}
