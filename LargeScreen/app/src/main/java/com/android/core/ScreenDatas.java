package com.android.core;

import java.util.ArrayList;
import java.util.List;

import com.android.activity.common.ScreenModeTypeBean;
import com.android.activity.sceen.facility.FragCargoOnDoor;
import com.android.activity.sceen.facility.FragCargoOnSpot;
import com.android.activity.sceen.facility.FragEmptyCtnr;
import com.android.activity.sceen.facility.FragParkingFrament;
import com.android.activity.sceen.ghost_ctnr.GhostCTNR;
import com.android.activity.sceen.scanning.NeedAssign;
import com.android.activity.sceen.scanning.RemainScanningJob;
import com.android.activity.sceen.scanning.Scanning;
import com.android.activity.sceen.sync.LoadReceiveCloseToday;
import com.android.activity.sceen.sync.LoadReceiveNotFinish;
import com.android.activity.sceen.task.FragRemainJobs;
import com.android.activity.sceen.task.FragUnderProcessing;
import com.android.activity.sceen.task.FragWaitingAssign;
import com.android.activity.sceen.task.ReceiveProcessing;
import com.android.activity.sceen.windows.ForgetCloseTaskFrament;
import com.android.activity.sceen.windows.WaitingListFrament;
import com.android.activity.sceen.windows.WindowFrament;
import com.android.activity.sceen.windows.WindowLoadReceiveNotFinish;

/**
 * 
 * @Description: 
 * @author gcy
 * @date 2014-12-4 下午7:47:24
 */
public class ScreenDatas{
	
	public static List<ScreenModeTypeBean> getScreen_Task(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Labor Assignment"/*"Waiting Assign Labor"*/);
		bean1.setScreenPostSize(8);
		bean1.setScreenType(ScreenModeTypeBean.Col_Left);
		bean1.setLine(1);
		bean1.setMethod(FragWaitingAssign.method);
		bean1.setFragment(FragWaitingAssign.class);
		
		ScreenModeTypeBean bean2 = new ScreenModeTypeBean();
		bean2.setScreenName("Remain Jobs");
		bean2.setScreenPostSize(8);
		bean2.setScreenType(ScreenModeTypeBean.Col_Right);
		bean2.setLine(1);
		bean2.setMethod(FragRemainJobs.method);
		bean2.setFragment(FragRemainJobs.class);
		
		ScreenModeTypeBean bean3 = new ScreenModeTypeBean();

		bean3.setScreenName("Loading");
		bean3.setScreenPostSize(8);
		bean3.setScreenType(ScreenModeTypeBean.Col_All);
		bean3.setLine(2);
		bean3.setMethod(FragUnderProcessing.method);
		bean3.setFragment(FragUnderProcessing.class);
		

		ScreenModeTypeBean bean8 = new ScreenModeTypeBean();

		bean8.setScreenName("Offloading");
		bean8.setScreenPostSize(8);
		bean8.setScreenType(ScreenModeTypeBean.Col_All);
		bean8.setLine(3);
		bean8.setMethod(ReceiveProcessing.method);
		bean8.setFragment(ReceiveProcessing.class);
		
		
//		ScreenModeTypeBean bean4 = new ScreenModeTypeBean();
//
//		bean4.setScreenName("No Task Loader");
//		bean4.setScreenPostSize(5);
//		bean4.setScreenType(ScreenModeTypeBean.Col_Left);
//		bean4.setLine(3);
//		bean4.setMethod(NoTaskLabor.method);
//		bean4.setFragment(NoTaskLabor.class);
//		
//		ScreenModeTypeBean bean5 = new ScreenModeTypeBean();
//
//		bean5.setScreenName("Labor Remain Jobs");
//		bean5.setScreenPostSize(5);
//		bean5.setScreenType(ScreenModeTypeBean.Col_Right);
//		bean5.setLine(3);
//		bean5.setMethod(RemainJobsLabor.method);
//		bean5.setFragment(RemainJobsLabor.class);
//		
//		ScreenModeTypeBean bean6 = new ScreenModeTypeBean();
//
//		bean6.setScreenName("Load / Receive");
//		bean6.setScreenStyle(ScreenModeTypeBean.processScreen);
//		bean6.setScreenPostSize(5);
//		bean6.setScreenType(ScreenModeTypeBean.Col_All);
//		bean6.setLine(3);
//		bean6.setMethod(LR_Activity.method);
//		bean6.setFragment(LR_Activity.class);
//		
//		
//		ScreenModeTypeBean bean7 = new ScreenModeTypeBean();
//
//		bean7.setScreenName("Receive Scan");
//		bean7.setScreenPostSize(5);
//		bean7.setScreenType(ScreenModeTypeBean.Col_All);
//		bean7.setLine(3);
//		bean7.setMethod(LR_Activity.method);
//		bean7.setFragment(ReceiveScan.class);
		
		
		
		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
//		list.add(bean4);
//		list.add(bean5);
		
//		list.add(bean6);
//		list.add(bean7);
		list.add(bean8);
		
		return list;
	}
	
	public static List<ScreenModeTypeBean> getScreen_Facility(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Cargo On Spot");
		bean1.setScreenPostSize(10);
		bean1.setScreenType(ScreenModeTypeBean.Col_Left);
		bean1.setLine(1);
		bean1.setMethod(FragCargoOnSpot.method);
		bean1.setFragment(FragCargoOnSpot.class);
		
		ScreenModeTypeBean bean2 = new ScreenModeTypeBean();
		bean2.setScreenName("Cargo On Door");
		bean2.setScreenPostSize(10);
		bean2.setScreenType(ScreenModeTypeBean.Col_Right);
		bean2.setLine(1);
		bean2.setMethod(FragCargoOnDoor.method);
		bean2.setFragment(FragCargoOnDoor.class);
		
		ScreenModeTypeBean bean3 = new ScreenModeTypeBean();
		bean3.setScreenName("Empty CTNR");
		bean3.setScreenPostSize(10);
		bean3.setScreenType(ScreenModeTypeBean.Col_Left);
		bean3.setLine(2);
		bean3.setMethod(FragEmptyCtnr.method);
		bean3.setFragment(FragEmptyCtnr.class);
		
		ScreenModeTypeBean bean4 = new ScreenModeTypeBean();
		bean4.setScreenName("Visitor / Parking");
		bean4.setScreenPostSize(10);
		bean4.setScreenType(ScreenModeTypeBean.Col_Right);
		bean4.setLine(2);
		bean4.setMethod(FragParkingFrament.method);
		bean4.setFragment(FragParkingFrament.class);
		
		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
		list.add(bean4);
		
		return list;
	}
	
	public static List<ScreenModeTypeBean> getWindow(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
//		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
//		bean1.setScreenName("No Task Entry");
//		bean1.setScreenPostSize(10);
//		bean1.setScreenType(ScreenModeTypeBean.Col_Left);
//		bean1.setLine(1);
//		bean1.setMethod(NoTaskEntry.method);
//		bean1.setFragment(NoTaskEntry.class);
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Incoming Window");
		bean1.setScreenPostSize(10);
		bean1.setScreenType(ScreenModeTypeBean.Col_Left);
		bean1.setLine(1);
		bean1.setMethod(WindowFrament.method);
		bean1.setFragment(WindowFrament.class);
		
		ScreenModeTypeBean bean2 = new ScreenModeTypeBean();
		bean2.setScreenName("Open Load / Receive");
		bean2.setScreenPostSize(10);
		bean2.setScreenType(ScreenModeTypeBean.Col_Right);
		bean2.setLine(1);
		bean2.setMethod(WindowLoadReceiveNotFinish.method);
		bean2.setFragment(WindowLoadReceiveNotFinish.class);
		
		ScreenModeTypeBean bean3 = new ScreenModeTypeBean();
		bean3.setScreenName("Waiting List");
		bean3.setScreenPostSize(10);
		bean3.setScreenType(ScreenModeTypeBean.Col_Left);
		bean3.setLine(2);
		bean3.setMethod(WaitingListFrament.method);
		bean3.setFragment(WaitingListFrament.class);
		
		ScreenModeTypeBean bean4 = new ScreenModeTypeBean();
		bean4.setScreenName("Forget Close Task");
		bean4.setScreenPostSize(10);
		bean4.setScreenType(ScreenModeTypeBean.Col_Right);
		bean4.setLine(2);
		bean4.setMethod(ForgetCloseTaskFrament.method);
		bean4.setFragment(ForgetCloseTaskFrament.class);
		
		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
		list.add(bean4);
		
		return list;
	}

	public static List<ScreenModeTypeBean> getSync(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Open Load / Receive");
		bean1.setScreenPostSize(10);
		bean1.setScreenType(ScreenModeTypeBean.Col_All);
		bean1.setLine(2);
		bean1.setMethod(LoadReceiveNotFinish.method);
		bean1.setFragment(LoadReceiveNotFinish.class);
		
		ScreenModeTypeBean bean2 = new ScreenModeTypeBean();
		bean2.setScreenName("Load / Receive Closed Today");
		bean2.setScreenPostSize(10);
		bean2.setScreenType(ScreenModeTypeBean.Col_All);
		bean2.setLine(1);
		bean2.setMethod(LoadReceiveCloseToday.method);
		bean2.setFragment(LoadReceiveCloseToday.class);

		list.add(bean1);
		list.add(bean2);
		
		return list;
	}
	
	public static List<ScreenModeTypeBean> getGhostCTNR(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Facility Variance");
		bean1.setScreenPostSize(20);
		bean1.setScreenType(ScreenModeTypeBean.Col_All);
		bean1.setLine(2);
		bean1.setMethod(GhostCTNR.method);
		bean1.setFragment(GhostCTNR.class);

		list.add(bean1);
		
		return list;
	}
	
	public static List<ScreenModeTypeBean> getScanning(){
		List<ScreenModeTypeBean> list = new ArrayList<ScreenModeTypeBean>();
		
		ScreenModeTypeBean bean1 = new ScreenModeTypeBean();
		bean1.setScreenName("Need Assign");
		bean1.setScreenPostSize(10);
		bean1.setScreenType(ScreenModeTypeBean.Col_Left);
		bean1.setLine(1);
		bean1.setMethod(NeedAssign.method);
		bean1.setFragment(NeedAssign.class);
		
		ScreenModeTypeBean bean2 = new ScreenModeTypeBean();
		bean2.setScreenName("Remain Scanning Job");
		bean2.setScreenPostSize(10);
		bean2.setScreenType(ScreenModeTypeBean.Col_Right);
		bean2.setLine(1);
		bean2.setMethod(RemainScanningJob.method);
		bean2.setFragment(RemainScanningJob.class);
		
		ScreenModeTypeBean bean3 = new ScreenModeTypeBean();

		bean3.setScreenName("Scanning");
		bean3.setScreenPostSize(10);
		bean3.setScreenType(ScreenModeTypeBean.Col_All);
		bean3.setLine(2);
		bean3.setMethod(Scanning.method);
		bean3.setFragment(Scanning.class);
		

		list.add(bean1);
		list.add(bean2);
		list.add(bean3);
		
		return list;
	}
	
}
