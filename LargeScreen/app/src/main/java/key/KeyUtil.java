package key;

import java.util.Locale;

import android.text.TextUtils;

public class KeyUtil {
	
//	/**
//	 * 根据rel_type的值 来判断是Inbount 还是Outbount
//	 * @param rel_type
//	 * @return
//	 */
//	public static String getInOut(int rel_type){
//		//1,3
//		if(rel_type==1 || rel_type==3)
//			return "In";
//		//2
//		else if(rel_type==2)
//			return "Out";
//		else
//			return "";
//	}
	
	//str转全大写
	public static String toUpper(String str){
		if(TextUtils.isEmpty(str))
			return "";
		
		return str.toUpperCase(Locale.ENGLISH);
	}

	
	/**
	 * 根据返回的patrol状态 返回 Yes  Not
	 * @param str
	 * @return
	 */
	public static String patrolFlag(int flag){
		if(flag==1){
			return "Yes";
		}else{
			return "No";
		}
	}
}
