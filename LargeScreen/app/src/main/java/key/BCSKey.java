package key;

import java.util.HashMap;
import java.util.Map;


public class BCSKey
{
	static Map<String, String> row;
	
	
	
	/**
	 * ret == 1 表示成功
	 * ret == 0 表示服务器错误 然后err 的数字对应下面的错误
	 */
 	
	 
	public static int FAIL = 0;		//操作失败
	public static int SUCCESS = 1;	//操作成功 
	
  	public static int SYSTEMERROR = 2;//系统内部错误
	public static int LOGINERROR = 3;//登录失败,账号密码不匹配
	public static int OPERATIONNOTPERMIT = 7;//无权限操作
	public static int NOTEXITSPRODUCT = 10;//系统内无此商品 
	public static int NETWORKEXCEPTION = 11;//网络传输错误 
	public static int NOEXITSDELIVERYORDER = 12;//交货单不存在 
	public static int SerialNumberRepeat = 20;	//序列号重复
	public static int JSONERROR = 21 ;	//序列号解析出错
	public static int ProductCodeIsExist = 26; //商品条码已经存在
	public static int LPNotFound = 33 ;	//container 不存在
	public static int SerchLoadingNotFoundException = 36 ;	//SerchLoadingNot	checkin Loading search没有找到

	public static int ProductPictureDeleteFailed = 35 ;	//图片删除失败
	public static int TitleNotFoundException = 37 ;	//TitleNotFoundException
	public static int CheckInNotFound = 40 ;				//CHECKIN主单据没
	public static int AppVersionException = 41 ;			//android 版本的更新问题
	public static int AndroidPrintServerUnEable = 43 ; 	// android 打印服务器不可用
	public static int NoPermiessionEntryIdException = 44 ; 	// 当前CheckInEntry的 没有权限
	public static int PrintServerNoPrintException = 45 ; 	// 当前的PrintServer没有设置print
	public static int NoRecordsException=46;
	public static int LoadIsCloseException = 47;				//load is close 
	public static int LoadNumberNotExistException = 48;				//load is close 
	public static int DataFormateException = 49 ;				//数据格式错误
	
	static 
	{
		row = new HashMap<String, String>();
		row.put(String.valueOf(FAIL), "操作失败");	//操作失败
		row.put(String.valueOf(SUCCESS), "操作成功");//
 		row.put(String.valueOf(SYSTEMERROR), "System Error");	//系统内部错误
		row.put(String.valueOf(LOGINERROR), "Login Failed");//登录失败,账号密码不匹配
		row.put(String.valueOf(OPERATIONNOTPERMIT), "无权限操作");
		row.put(String.valueOf(NOTEXITSPRODUCT), "系统内无此商品 ");
		row.put(String.valueOf(NETWORKEXCEPTION), "Net Error");	//网络传输错误
		row.put(String.valueOf(NOEXITSDELIVERYORDER), "交货单不存在 ");
		row.put(String.valueOf(SerialNumberRepeat), "序列号重复");
		row.put(String.valueOf(JSONERROR), "序列号解析出错");
		row.put(String.valueOf(ProductCodeIsExist), "商品条码已经存在");
		row.put(String.valueOf(LPNotFound), "Container Not Found"); //容器不存在
		row.put(String.valueOf(ProductPictureDeleteFailed), "Delete Failed"); //图片删除失败
		row.put(String.valueOf(SerchLoadingNotFoundException), "SerchLoading NotFound.");
		row.put(String.valueOf(TitleNotFoundException), "Title NotFound.");
		row.put(String.valueOf(CheckInNotFound), "Entry ID Not Found.");
		row.put(String.valueOf(AppVersionException), "New version is ready for download, please remove old APP first.");
		row.put(String.valueOf(AndroidPrintServerUnEable), "Print Server Unable");
		row.put(String.valueOf(NoPermiessionEntryIdException), "No Access");
		row.put(String.valueOf(PrintServerNoPrintException), "The Print Server No Printer");
		row.put(String.valueOf(NoRecordsException), "No Records");
		row.put(String.valueOf(LoadIsCloseException), "LoadNumber Is Closed");
		row.put(String.valueOf(DataFormateException), "Data Formate Error ");

	}

	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(int id)
	{
		return getReturnStatusById(id+"");
	}
	
	
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(String id)
	{
		return String.valueOf(row.get(id));
	}
	
}
