package key;

import java.util.HashMap;
import java.util.Map;

import util.StringUtil;

public class EntryDetailNumberTypeKey {

	private static Map<String, String> row;
	 
	public static int CHECK_IN_LOAD = 10;    //load
	public static int CHECK_IN_ORDER = 11;    //order
	public static int CHECK_IN_CTN = 12;    //ctnr
	public static int CHECK_IN_BOL = 13; //bol
	public static int CHECK_IN_DELIVERY_ORTHERS = 14;//delivery other
  	public static int CHECK_IN_PONO = 17; //pono
	public static int CHECK_IN_PICKUP_ORTHERS = 18;//pickup other
	
//	public EntryDetailNumberTypeKey() {
	static{
		row = new HashMap<String, String>();
	 
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_LOAD), "LOAD");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_ORDER), "ORDER");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_CTN), "CTNR");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_BOL), "BOL");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS), "OTHERS");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS), "OTHERS");
  		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_PONO), "PO");
	}
//	}
	

	public static String getModuleName(String id)
	{
		return(StringUtil.isNullOfStr(String.valueOf(row.get(id)))?"":String.valueOf(row.get(id)));
	}
	
	public static String getModuleName(int id)
	{
		return getModuleName(String.valueOf(id));
	}
	
	
}
