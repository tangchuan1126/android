package debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Instrumentation;
import android.view.KeyEvent;

/**
 * @author 朱成
 * @date 2014-8-8
 */
public class SocketServer {

	ServerSocket sever;

	public SocketServer(int port) {
		try {
			sever = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void beginListen() {
		if (sever == null)
			return;
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						final Socket socket = sever.accept();
						BufferedReader in;
						try {
							in = new BufferedReader(new InputStreamReader(
									socket.getInputStream(), "UTF-8"));
							OutputStream out_bytes = socket.getOutputStream();
							PrintWriter out = new PrintWriter(out_bytes);

							// 读取
							String str = "";
							// String line;
							// while ((line=in.readLine())!=null) {
							// str+=line;
							// }
							str = in.readLine();

							// String value=str;
							if (str == null || str.indexOf("GET /?v=") == -1) {
								// socket.close();
								continue;
							}

							//取"键值v"
							str = str.substring("GET /?v=".length(),
									str.indexOf(" HTTP/"));

							System.out.println("nimei>>req:" + str);

							Instrumentation ins = new Instrumentation();
							//模拟按键,左右上下中→46825
							if("4".equals(str))
								ins.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_LEFT);
							else if("6".equals(str))
								ins.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_RIGHT);
							else if("8".equals(str))
								ins.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_UP);
							else if("2".equals(str))
								ins.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_DOWN);
							else if("5".equals(str))
								ins.sendKeyDownUpSync(KeyEvent.KEYCODE_DPAD_CENTER);
							//模拟输入字符串
							else
								ins.sendStringSync(str + "\n");
							

//							// 网页
//							if ("0".equals(str)) {
//								String strOut = Util.readAsset("101.html");
//								out.println(strOut);
//								out.flush();
//							}
//							// 图片
//							else if (str.contains("img")) {
//								MainActivity.isUploading=true;
//								//yuv转jpg
//								YuvImage image = new YuvImage(
//										MainActivity.data_k, ImageFormat.NV21,
//										960, 720, null);
//								image.compressToJpeg(new Rect(0, 0, 959, 719),
//										70, out_bytes);
//								out_bytes.flush();
//								MainActivity.isUploading=false;
//							}

							socket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
