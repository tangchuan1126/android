/*
Navicat SQLite Data Transfer

Source Server         : sync
Source Server Version : 30714
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30714
File Encoding         : 65001

Date: 2015-01-20 14:34:53
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for chat_msg
-- ----------------------------
DROP TABLE IF EXISTS "main"."chat_msg";
CREATE TABLE "chat_msg" (
"ID"  VARCHAR(12) NOT NULL ,
"uname_me"  TEXT,
"uname_peer"  TEXT,
"alias_peer"  TEXT,
"msg"  TEXT,
"out"  INTEGER,
"isread" INTEGER,
"createtime"  INTEGER,
PRIMARY KEY ("ID")
);

-- ----------------------------
-- Records of chat_msg
-- ----------------------------
