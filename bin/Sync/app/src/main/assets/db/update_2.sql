
PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS "main"."file";
CREATE TABLE "file" (
"file_id"  INTEGER,
"uri"  varchar(100),
"file_with_class"  INTEGER,
"file_with_type"  INTEGER,
"file_with_id"  INTEGER,
"file_with_detail_id"  INTEGER,
"option_file_param"  INTEGER
);

-- ----------------------------
-- Records of file
-- ----------------------------
