package oso.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewStub;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import declare.com.vvme.R;
import oso.SyncApplication;
import oso.ui.MainActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.msg.TaskTypesAc;
import oso.widget.ResizeLayout;
import oso.widget.ResizeLayout.OnSoftKbListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import oso.widget.swipebacklayout.app.SwipeBackActivity;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import utility.HttpUrlPath;
import utility.Utility;

public abstract class BaseActivity extends SwipeBackActivity implements IBaseActivity, OnSoftKbListener
// 请大家不要在BaseActivity里面添加OnClickLietener
{

	protected Activity mActivity;
	protected Resources res;

	public ResizeLayout rootLay;
	public View vTopbar;
	public View vMsg;
	public View vMiddle;

	private TextView txtTitle;
	protected Button imgBack;
	protected Button btnRight0, btnRight;
	protected ImageView imgRight;
	private TextSwitcher ts;
	private ConnectionChangeReceiver myReceiver;

	// 标题
	private CharSequence titleString;

	// 是否-显示最新消息
	// public static boolean isClose = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setSwipeBackEnable(false);

		mActivity = this;
		res = getResources();
		addToStack();
		// 初始化tts
		TTS.getInstance().init(this);
		Utility.updateConfigurationLanguage(getApplicationContext());
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// 修改语言配置
		Utility.updateConfigurationLanguage(getApplicationContext());
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 修改ServerUrl地址
		HttpUrlPath.initBaseUrls();
		// 注册网络监听
		registerReceiver();
		// 修改语言配置
		Utility.updateConfigurationLanguage(getApplicationContext());
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		boolean toShowMsg = shouldShowMsg();
		vMsg.setVisibility(toShowMsg ? View.VISIBLE : View.GONE);
		if (toShowMsg)
			ts.setText(last_msg);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// 取消网络监听
		unregisterReceiver();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		removeFromStack();
	}

	public final int MenuItem_Chat = 101;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case MenuItem_Chat:
			startActivity(new Intent(mActivity, ChatTabActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean supportChatMenu = true; // 支持-聊天菜单

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (supportChatMenu)
			menu.add(0, MenuItem_Chat, Menu.NONE, "Chat");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void addToStack() {
		// TODO Auto-generated method stub
		ActivityStack.addAct(this);
	}

	@Override
	public void removeFromStack() {
		// TODO Auto-generated method stub
		ActivityStack.removeAct(this);
	}

	private OnClickListener onBaseClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.base_topbar_msg: // 消息
				tipToMyTasks();
				break;

			default:
				break;
			}
		}
	};

	private void registerReceiver() {
		IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		myReceiver = new ConnectionChangeReceiver();
		this.registerReceiver(myReceiver, filter);
	}

	private void unregisterReceiver() {
		this.unregisterReceiver(myReceiver);
	}

	// 最近消息
	public static String last_msg;

	// 允许显示消息
	protected boolean allowShowMsg = true;

	// 是否应显示-最新任务,有消息、且允许显示
	private boolean shouldShowMsg() {
		return !TextUtils.isEmpty(last_msg) && allowShowMsg;
	}

	// 有新消息时"触发",默认在布局上显示"消息条"
	// 注:1>若不希望显示"消息条",需给定allowShowMsg=false
	public void onPush() {
		if (!shouldShowMsg())
			return;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				ts.setText(last_msg);
				if (vMsg.getVisibility() == View.GONE) {
					vMsg.setVisibility(View.VISIBLE);
					vMsg.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_top_in));
				}
				SyncApplication.getThis().startVibrator();
				// SoundUtil.play(1);
			}
		});
	}

	/**
	 * 被踢后 重新登录时(点击relogin时触发,而非已经登录上openfire),如:用于刷新界面
	 */
	public void onRelogin() {

	}

	/**
	 * 移除-页面顶部"提醒"
	 * 
	 * @param removeNotify
	 *            移除"通知栏"中notification
	 */
	public void removeMsg(boolean removeNotify) {
		last_msg = "";
		vMsg.setVisibility(View.GONE);
		// 移除-通知
		if (removeNotify)
			Utility.cancelNotify(Utility.NotifyID_Task);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// 若在edittext外 则下放
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
				InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
		return super.onTouchEvent(event);
	}

	// ---
	private boolean hasNotifyReady = false;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		// 页面首次出现时
		if (hasFocus && !hasNotifyReady) {
			onFirstVisible();
			hasNotifyReady = true;
		}
	}

	// 首次显示时,用于提示ready等
	protected void onFirstVisible() {

	}

	// ---

	public String getTitleString() {
		return titleString.toString();
	}

	public void setTitleString(CharSequence titleString) {
		this.titleString = titleString;

		if (txtTitle != null)
			txtTitle.setText(titleString);
	}

	public void setTitleString(int resId) {
		this.titleString = getString(resId);

		if (txtTitle != null)
			txtTitle.setText(titleString);
	}

	// ---方法---------

	public void showBackButton(boolean visible) {
		if (imgBack == null)
			return;

		if (visible)
			imgBack.setVisibility(View.VISIBLE);
		else
			imgBack.setVisibility(View.INVISIBLE);
	}

	public static final int RIGHT_BTN_NO_BG = -1; // 右边-按钮,无背景、默认背景
	public static final int RIGHT_BTN_DEF_BG = 0;

	// 显示-右边btn
	public void showRightButton(int resId, String label, OnClickListener onClick) {
		btnRight = (Button) findViewById(R.id.btnRight);

		// 无背景
		if (resId == RIGHT_BTN_NO_BG)
			btnRight.setBackgroundResource(0);
		// 默认背景
		else if (resId == RIGHT_BTN_DEF_BG) {
		}
		// 自定义-背景
		else {
			btnRight.setBackgroundResource(resId);
			// 自定义背景，扩大点击区域
			// int w = (int) (20 * Util.getDpiScale());
			// int h = (int) (10 * Util.getDpiScale());
			// btnRight.setPadding(w, h, w, h);
		}

		btnRight.setText(label);
		btnRight.setOnClickListener(onClick);
		btnRight.setVisibility(View.VISIBLE);
	}

	public ImageView showRightImage(OnClickListener onClick) {
		imgRight = (ImageView) findViewById(R.id.imgRight);

		imgRight.setOnClickListener(onClick);
		imgRight.setVisibility(View.VISIBLE);

		return imgRight;
	}

	// 显示-右边btn
	public void showRightButton0(int resId, String label, OnClickListener onClick) {
		btnRight0 = (Button) findViewById(R.id.btnRight0);

		// 无背景
		if (resId == RIGHT_BTN_NO_BG)
			btnRight0.setBackgroundResource(0);
		// 默认背景
		else if (resId == RIGHT_BTN_DEF_BG) {
		}
		// 自定义-背景
		else {
			btnRight0.setBackgroundResource(resId);
			// 自定义背景，扩大点击区域
			// int w = (int) (20 * Util.getDpiScale());
			// int h = (int) (10 * Util.getDpiScale());
			// btnRight.setPadding(w, h, w, h);
		}

		btnRight0.setText(label);
		btnRight0.setOnClickListener(onClick);
		btnRight0.setVisibility(View.VISIBLE);
	}

	// // 返回-返回主页,右边
	// public void showBtnToHome() {
	// showRightButton(R.drawable.btn_home, "", onClick);
	// }

	/**
	 * @param middleResID
	 * @param topbarResID
	 */
	public void setContentView(int middleResID, int topbarResID) {
		// TODO Auto-generated method stub
		super.setContentView(R.layout.lo_base);

		rootLay = (ResizeLayout) findViewById(R.id.rootLay);
		rootLay.setOnSoftKbListener(this);

		// 顶边栏
		ViewStub stub = (ViewStub) findViewById(R.id.base_topbar_before);
		if (topbarResID != 0)
			stub.setLayoutResource(topbarResID);
		vTopbar = (View) stub.inflate();

		// 消息栏
		stub = (ViewStub) findViewById(R.id.base_msg);
		if (topbarResID != 0)
			stub.setLayoutResource(topbarResID);
		vMsg = (View) stub.inflate();
		vMsg.setVisibility(View.GONE);

		// 内容区
		stub = (ViewStub) findViewById(R.id.base_middle_before);
		if (middleResID != 0)
			stub.setLayoutResource(middleResID);
		vMiddle = (View) stub.inflate();

		// 标题
		txtTitle = (TextView) findViewById(R.id.topbar_title);

		ts = (TextSwitcher) findViewById(R.id.ts);
		final LayoutInflater inflater = LayoutInflater.from(mActivity);
		ts.setFactory(new ViewFactory() {
			public View makeView() {
				return (TextView) inflater.inflate(R.layout.text_sticky, null);
			}
		});
		// 设置切入动画
		ts.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left));
		// 设置切出动画
		ts.setOutAnimation(AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right));

		imgBack = (Button) findViewById(R.id.topbar_back);
		if (imgBack != null){
			imgBack.setOnClickListener(onClick);
			Utility.addLs_backHome_onLongclick(mActivity, imgBack);
		}
		btnRight = (Button) findViewById(R.id.btnRight);

		vMsg.setOnClickListener(onBaseClick);
		findViewById(R.id.remove_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				removeMsg(true);
			}
		});
	}
	
//	private OnLongClickListener onLongClick_btnBack=new View.OnLongClickListener() {
//		
//		@Override
//		public boolean onLongClick(View v) {
//			// TODO Auto-generated method stub
//			if(!StoredData.isLogin())
//				return false;
//			
//			String tip=getString(R.string.back_to_home);
//			RewriteBuilderDialog.showSimpleDialog(mActivity,tip, new DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					Utility.popTo(mActivity, MainActivity.class);
//				}
//			});
//			
//			return false;
//		}
//	};

	/**
	 * 点击消息时
	 */
	private void tipToMyTasks() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_jump_task), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				startActivity(new Intent(mActivity, TaskTypesAc.class));
				removeMsg(true);
			}
		});
	}

	private boolean isTouchInterrupt = false;

	/**
	 * 屏蔽界面控件的所有点击事件
	 */
	public void interruptTouchEvent(boolean isTouchInterrupt) {
		this.isTouchInterrupt = isTouchInterrupt;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		return isTouchInterrupt ? true : super.dispatchTouchEvent(ev);
	}

	private OnClickListener onClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.topbar_back: // 返回
				onBackBtnOrKey();
				break;

			// case R.id.btnRight: // 返回-主页
			// Util.popTo(BaseActivity.this, MainActivity.class);
			// break;

			default:
				break;
			}

		}
	};

	public void onBackPressed() {
		// super.onBackPressed();
		onBackBtnOrKey();
	};

	private List<Checkable> checkbleList = new ArrayList<Checkable>();

	public void addCheckable(Checkable able) {
		checkbleList.add(able);
	}

	public void addCheckable(Collection<Checkable> ables) {
		checkbleList.addAll(ables);
	}

	// 校验相关条件是否满足,若不满足 则弹框
	public boolean check() {
		for (Checkable able : checkbleList) {
			boolean flag = able.isCheck();
			if (!flag) {
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
				builder.setTitle(getString(R.string.sync_notice));
				builder.setMessage(able.notifyMessage());
				builder.isHideCancelBtn(true);
				builder.setPositiveButton(getString(R.string.sync_ok), null);
				builder.create().show();
				return false;
			}
		}
		return true;
	}

	/*
	 * 切换软键盘时-回调,1.不适用于adjust_pan 2.setContentView(vMiddle,vTopbar)时才有效
	 * (non-Javadoc)
	 * 
	 * @see ui.ResizeLayout.OnSoftKbListener#onShowSoftKb(boolean)
	 */
	@Override
	public void onShowSoftKb(boolean isShow) {
		// TODO Auto-generated method stub

	}

	// 点返回按钮、返回键时
	protected void onBackBtnOrKey() {
		BaseActivity.this.finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

	public int px2dp(int px) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, mActivity.getResources().getDisplayMetrics());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == ChooseUsersActivity.requestCode && requestCode == ChooseUsersActivity.requestCode) {
			TextView text = (TextView) findViewById(data.getExtras().getInt("resoures_text_id"));
			TextView hide = (TextView) findViewById(data.getExtras().getInt("resoures_hide_id"));
			if (text != null && hide != null) {
				text.setText(data.getExtras().getString("select_user_names"));
				hide.setText(data.getExtras().getString("select_user_adids"));
			}
		}
		// 初始化-tts
		TTS.getInstance().onTTSActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	public class ConnectionChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			if (mobNetInfo == null || wifiNetInfo == null) {
				return;
			}
			if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
				// 处理网络的全局变量
				findViewById(R.id.remove_btn).setVisibility(View.INVISIBLE);
				vMsg.setVisibility(View.VISIBLE);
				ts.setText("Network unavailable");
				vMsg.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// UIHelper.showToast("Network unavailable");
						// 跳至网络设置
						Intent wifiSettingsIntent = new Intent("android.settings.WIFI_SETTINGS");
						startActivity(wifiSettingsIntent);
					}
				});
			} else {
				// 处理网络的全局变量
				vMsg.setVisibility(View.GONE);
				findViewById(R.id.remove_btn).setVisibility(View.VISIBLE);
				ts.setText("");
				vMsg.setOnClickListener(onBaseClick);
			}
		}
	}

}
