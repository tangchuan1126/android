package oso.base;


/**
 * activity基类,用于集中管理
 * @author 朱成
 * @date 2015-2-2
 */
public interface IBaseActivity {
	
	/**
	 * 添加至"ac堆栈",需在onCreate中调用
	 */
	public abstract void addToStack();
	
	/**
	 * 从"ac堆栈"移除,需在onDestroy中调用
	 */
	public abstract void removeFromStack();
	
	
}
