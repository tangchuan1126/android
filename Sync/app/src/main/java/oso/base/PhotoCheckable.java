package oso.base;

import java.util.List;

import oso.widget.photo.TabToPhoto;

public class PhotoCheckable implements Checkable {

	private int photoCount; // 最少应上传的"图片数"
	private String message;
	public TabToPhoto photoCapture;
	private int currentIndex;

	public PhotoCheckable(int currentIndex, String title, TabToPhoto photoCapture) {
		this(currentIndex, title, photoCapture, 0);
	}

	public PhotoCheckable(int currentIndex, String title, TabToPhoto photoCapture, int photoCount) {
		this.photoCount = photoCount;
		this.message = title + " 至少 " + photoCount + "张图片";
		this.photoCapture = photoCapture;
		this.currentIndex = currentIndex;
	}

	// 检查"图片数目"是否大于最小值
	@Override
	public boolean isCheck() {
		if (photoCount <= 0) {
			return true;
		}

		List<String> fileNames = photoCapture.getPhotoNames(currentIndex, true);
		if (fileNames != null && fileNames.size() >= photoCount) {
			return true;
		}
		return false;
	}

	@Override
	public String notifyMessage() {
		return message;
	}

	public int getPhotoCount() {
		return photoCount;
	}

}
