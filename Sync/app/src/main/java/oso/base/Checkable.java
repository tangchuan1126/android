package oso.base;

/**
 * 用于"校验",如:1.页面跳出时校验TabToPhoto中是否有图片
 * @author 朱成
 * @date 2014-11-7
 */
public interface Checkable {
	
	//校验
	boolean isCheck();

	//校验错误时的"提示消息"
	String notifyMessage();
}
