package oso;

import java.io.File;

import support.common.AuthImageDownloader;
import support.common.push.MyPushService;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import utility.CrashHandler;
import utility.HttpUrlPath;
import utility.Utility;
import utility.WakeLockManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Vibrator;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * 初始化Application
 * 
 * @author zhangrui
 * 
 */
public class SyncApplication extends Application {

	private static SyncApplication instance;
	private static Handler handler = new Handler(); // 公共handler

	public static SyncApplication getThis() {
		return instance;
	}

	private Vibrator vibrator;

	private long[] v = { 50, 100, 50, 200 };

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		System.out.println("nimei>>app.onCreate");
		HttpUrlPath.initBaseUrls();

		// 仅"主进程"才初始化
		if (!Utility.isInMainProcess(this))
			return;

		// app被kill掉重启时,也应当已登陆
		if (Utility.isAppKilled(this))
			StoredData.isLogin_inRam = true;

		CrashHandler crashHandler = CrashHandler.getInstance();
		// 注册crashHandler
		crashHandler.init(getApplicationContext());
		initImageLoader(getApplicationContext());
		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

		initFolders();

		// 启动-推送服务
		Intent in = new Intent(MyPushService.Action_MyPushService);
		in.setPackage(getPackageName());
		startService(in);

		WakeLockManager.getInstants().acquireWakeLock(this);

		// 5分钟调用发一次广播 防止熄屏后被杀
		Utility.startAlarm(this, 5);
	}

	public Handler getHandler() {
		return handler;
	}

	/**
	 * 震动
	 */
	public void startVibrator() {
		vibrator.vibrate(v, -1);
	}

	private void initFolders() {
		File thumbnail = new File(Goable.file_image_path_thumbnail);
		if (!thumbnail.exists()) {
			thumbnail.mkdirs();
		}
		File filePath = new File(Goable.file_path);
		if (!filePath.exists()) {
			filePath.mkdirs();
		}
		File upFile = new File(Goable.file_image_path_up);
		if (!upFile.exists()) {
			upFile.mkdirs();
		}
	}

	/** 初始化图片加载类配置信息 **/
	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).threadPriority(Thread.NORM_PRIORITY - 8)// 加载图片的线程数
				.denyCacheImageMultipleSizesInMemory() // 解码图像的大尺寸将在内存中缓存先前解码图像的小尺寸。
				.discCacheFileNameGenerator(new Md5FileNameGenerator())// 设置磁盘缓存文件名称
				.tasksProcessingOrder(QueueProcessingType.LIFO)// 设置加载显示图片队列进程
				.writeDebugLogs() // Remove for release app
				// debug
				.imageDownloader(new AuthImageDownloader(context)).build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

}
