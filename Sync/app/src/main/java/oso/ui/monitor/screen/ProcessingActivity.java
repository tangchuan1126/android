package oso.ui.monitor.screen;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.monitor.screen.bean.ScreenProcessBean;
import oso.ui.monitor.screen.bean.StepDataBean;
import oso.ui.monitor.screen.tool.DownloadScreenApk;
import oso.ui.monitor.screen.tool.MonitorUtils;
import oso.ui.monitor.screen.tool.TimeoutTools;
import oso.widget.BadgeView;
import oso.widget.orderlistview.FlowLayout;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class ProcessingActivity extends BaseActivity{
	
	private int postNum = 0;//用于判断请求失效多少次后执行Connecting的弹窗
	private TimeoutTools timeUtil = new TimeoutTools();//判断超时的工具类
	private boolean whetherToRun = true;// 控制线程的继续运行
	public int postTime = 10*1000;//请求时间间隔
	public int timeOut = 11*1000;//判断超时的标准
	Handler handler = new Handler();
	private TextView connection_dialog;// 请求数据超时显示
	
	private FlowLayout workflowLt;//点选工作流程
	private FlowLayout screenLt;//点选大屏幕
//	private List<ScreenProcessBean> screenList;//大屏幕数据集
	
	private int type; //用于判断解析JSON数据需要调用的方法
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.monitor_sub_layout, 0);
		getFromActivityData();
		initView();
		startHandler();
	}
	/**
	 * @Description:获取来自于上一个页面的数据
	 */
	public void getFromActivityData(){
		type = getIntent().getIntExtra("type", 0);
		String small_title = getIntent().getStringExtra("small_title");
		setTitleString(small_title);
	}
	/**
	 * @Description:初始化UI
	 */
	public void initView(){
		workflowLt = (FlowLayout) findViewById(R.id.flowLayout);
		screenLt = (FlowLayout) findViewById(R.id.screen_layout);
		connection_dialog = (TextView) findViewById(R.id.connection_dialog);
		connection_dialog.setVisibility(View.VISIBLE);
	}
	
	/**
	 * @Description:填充进度数据
	 * @param @param processList
	 */
	public void setProcessView(List<ScreenProcessBean> processList){
		if(!Utility.isNullForList(processList)){
			workflowLt.removeAllViews();
			
			int padding = mActivity.getResources().getDimensionPixelSize(R.dimen.screen_margin);
			int width = ((rootLay.getWidth()>rootLay.getHeight())?rootLay.getWidth():rootLay.getHeight()) - 2*padding;
			int everyWidth = width/6/*processList.size()*/;
			int everyHeight = everyWidth*11/17;
			
			int ts1 = Utility.px2sp(mActivity,everyWidth*15/17/8);
			int ts2 = ts1+3;
			
			for (int i = 0; i < processList.size(); i++) {
				LinearLayout ly = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.monitor_process_layout, null);
				View layout_view = (View) ly.findViewById(R.id.layout_view);
				LayoutParams a = layout_view.getLayoutParams();
				a.width = everyWidth;
				a.height= everyHeight;
				layout_view.setLayoutParams(a);
				
				TextView screen_name = (TextView) ly.findViewById(R.id.screen_name);
				TextView num_process = (TextView) ly.findViewById(R.id.num_process);
				screen_name.setTextSize(ts1);
				num_process.setTextSize(ts2);
				
				if(i==0){
					judgeHeadBg(processList.get(i).stepDataBean.color, layout_view);
				}else if ((i+1)==processList.size()) {
					judgeTailBg(processList.get(i).stepDataBean.color, layout_view);
				}else{
					judgeCenterBg(processList.get(i).stepDataBean.color, layout_view);
				}
				screen_name.setText(processList.get(i).screenName);
				num_process.setText(processList.get(i).stepDataBean.count+"/"+processList.get(i).stepDataBean.total);
				final ScreenProcessBean s = processList.get(i);
				layout_view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						jumpScreenActivity(s);
					}
				});
				layout_view.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						 //按下操作
				        if(event.getAction()==MotionEvent.ACTION_DOWN){
				             v.setAlpha(0.8f);
				        }
				        //抬起操作
				        if(event.getAction()==MotionEvent.ACTION_UP){
				        	v.setAlpha(1.0f);
				        }
				        return false;
					}
				});
				workflowLt.addView(ly);
			}
		}
	}
	
	/**
	 * @Description:填充需要显示的屏幕
	 * @param @param list
	 */
	public void setScreenView(List<ScreenProcessBean> list){
		if(!Utility.isNullForList(list)){
			screenLt.removeAllViews();
			
			int padding = mActivity.getResources().getDimensionPixelSize(R.dimen.screen_margin);
			int width = ((rootLay.getWidth()>rootLay.getHeight())?rootLay.getWidth():rootLay.getHeight()) - 2*padding;
			int everyWidth = width/6;
			int everyHeight = everyWidth*4/7;
			
			int ts = Utility.px2sp(mActivity,everyHeight/8)+3;
			
			for (int i = 0; i < list.size(); i++) {
				final ScreenProcessBean s = list.get(i);
				LinearLayout ly = (LinearLayout) LayoutInflater.from(mActivity).inflate(R.layout.monitor_button_layout, null);
				Button screen_name = (Button) ly.findViewById(R.id.screen_name);
				LayoutParams a = screen_name.getLayoutParams();
				a.width = everyWidth;
				a.height= everyHeight;
				screen_name.setLayoutParams(a);
				
				screen_name.setText(s.screenName);
				screen_name.setTextSize(ts);
				screen_name.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						jumpScreenActivity(s);
					}
				});
				
				BadgeView badge = new BadgeView(mActivity,screen_name);
				badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
				badge.setTextSize(ts);
				badge.setText(s.number+"");
				badge.setTextColor(Color.WHITE);
				badge.show();
				
				screenLt.addView(ly);
			}
		}
	}
	
	/**
	 * @Description:跳转到大屏幕
	 * @param @param bean
	 */
	public void jumpScreenActivity(ScreenProcessBean bean){
		if(StringUtil.isNullOfStr(bean.screenClass)){
			UIHelper.showToast(mActivity, getString(R.string.sync_developing), Toast.LENGTH_SHORT).show();
			return;
		}
		if(!MonitorUtils.checkPackage(mActivity,"oso.com.vvmescreen")){
			DownloadScreenApk.loadApk(mActivity);
			return;
		}
		try {
			ComponentName componetName = new ComponentName("oso.com.vvmescreen","oso.com.vvmescreen.WelcomeActivity"); 
            Intent intent = new Intent(); 
            intent.setComponent(componetName);
//          startActivity(intent); 
//			PackageManager packageManager =getPackageManager();
//			Intent intent = packageManager.getLaunchIntentForPackage("oso.com.vvmescreen.WelcomeActivity");
			intent.putExtra("monitorUtils", (Serializable)MonitorUtils.getScreenData(mActivity,rootLay,bean.screenClass));
            startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
			UIHelper.showToast(mActivity, "Please check the latest version of the Report Screen application!", Toast.LENGTH_SHORT).show();
			DownloadScreenApk.loadApk(mActivity);
		}
	}
	/**
	 * @Description:判断头背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeHeadBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red_head);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green_head);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow_head);
			break;
		}
	}
	/**
	 * @Description:判断中间背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeCenterBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow);
			break;
		}
	}
	/**
	 * @Description:判断尾背景颜色
	 * @param @param color
	 * @param @param layout_view
	 */
	public void judgeTailBg(int color,View layout_view){
		switch (color) {
		case StepDataBean.LoadReceiveProcessShowColorRed:
			layout_view.setBackgroundResource(R.drawable.big_arrow_red_tail);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorGreen:
			layout_view.setBackgroundResource(R.drawable.big_arrow_green_tail);
			break;
		case StepDataBean.LoadReceiveProcsssShowColorYellow:
			layout_view.setBackgroundResource(R.drawable.big_arrow_yellow_tail);
			break;
		}
	}
	/**
	 * @Description:获取数据
	 */
	public void getData(){
		RequestParams params = new RequestParams();
		params.add("Method", "LoadReceiveProcess");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				Map<String, List<ScreenProcessBean>> map = ScreenProcessBean.handJson(json, type);
				List<ScreenProcessBean> processList = map.get("process");
				List<ScreenProcessBean> screenList = map.get("screen");
				postNum = 0;
				connection_dialog.setVisibility(View.GONE);
				if(!Utility.isNullForList(processList)){
					setProcessView(processList);
					setScreenView(screenList);	
					ContinueToRun();
				}
			}
		@Override
		public void handFail() {
			ContinueToRun();
		}
		}.doGetNotHaveDialog(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);		
	}
	/**
	 * @Description:开始进程
	 */
	private void startHandler() {
		timeUtil.setTimeMS(System.currentTimeMillis());
		handler.post(spot_task);
		handler.post(alwaysFixSpotHandler);
	}
	
	/**
	 * 请求数据
	 */
	private Runnable spot_task = new Runnable() {
		public void run() {
			if (postNum > TimeoutTools.POSTNUM) {
				connection_dialog.setVisibility(View.VISIBLE);
			}
			postNum++;
			getData();
		}
	};

	/**
	 * 监视器用于循环守卫线程是否正常运行
	 */
	private Runnable alwaysFixSpotHandler = new Runnable() {
		public void run() {
			if (whetherToRun) {
				if (TimeoutTools.isFastPost(timeUtil,timeOut)) {
					handler.removeCallbacks(spot_task);
					timeUtil.setTimeMS(System.currentTimeMillis());
					handler.postDelayed(spot_task,postTime);
				}
				handler.postDelayed(alwaysFixSpotHandler, 1000);
			}
		}
	};
	
	/**
	 * @Description:急需运行
	 */
	public void ContinueToRun() {
		if (whetherToRun) {
			handler.removeCallbacks(spot_task);
			timeUtil.setTimeMS(System.currentTimeMillis());
			handler.postDelayed(spot_task,postTime);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		whetherToRun = false;
		handler.removeCallbacks(spot_task);
		handler.removeCallbacks(alwaysFixSpotHandler);
	}
}
