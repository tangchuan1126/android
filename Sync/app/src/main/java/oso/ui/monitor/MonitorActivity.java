package oso.ui.monitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.monitor.screen.ProcessingActivity;
import oso.ui.monitor.screen.RN_Or_DN_Activity;
import oso.ui.monitor.screen.bean.ScreenProcessBean;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.FileUtils;
import utility.HttpUrlPath;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class MonitorActivity extends BaseActivity {

	private ListView listview;
	private List<ScreenProcessBean> screenList;
	private ScreenMenuAdapter adp;

	public static final String apkName = "Sync-GIS.apk";
	public static final String apkPath = Goable.file_base_path + apkName;
	public static final String apkPackageName = "oso.gis";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.monitor_main_layout, 0);
		initView();

		// 赋值apk到SD
		copyFilesFassets(mActivity, apkName, apkPath);
		// registerSDCardListener();
	}

	public void toMapOnClick(View v) {
		// final RewriteDialog dialog = RewriteDialog.createDialog(mActivity,
		// 1);
		// dialog.show();
		// RequestParams params = new RequestParams();
		// params.add("version", StoredData.getMapDataVersion() + "");
		// new SimpleFileUtil() {
		// @Override
		// public void handResponse(int statusCode, Header[] headers, byte[]
		// responseBody) {
		// if (responseBody != null && responseBody.length != 0) {
		// parseMapData(responseBody, dialog);
		// } else {
		// UIHelper.showToast("Server No Datas! (" +
		// StoredData.getMapDataVersion() + ")");
		// }
		// }
		//
		// @Override
		// public void handFail() {
		// if (dialog != null && dialog.isShowing()) {
		// dialog.dismiss();
		// }
		// if (!FileUtils.isFileExist(GMapUtils.unZipPath + File.separator +
		// GMapUtils.FILE_VERSION)) {
		// if (!FileUtils.isFileExist(GMapUtils.zipFilePath)) {
		// UIHelper.showToast("Server No Datas! (" +
		// StoredData.getMapDataVersion() + ")");
		// return;
		// } else {
		// try {
		// ZipUtil.UnZipFolder(GMapUtils.zipFilePath, GMapUtils.unZipPath);//
		// 解压数据
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// }
		// }
		// // 界面跳转
		// Intent intent = new Intent(mActivity, GMapActivity.class);
		// startActivity(intent);
		// };
		//
		// }.doGetFile(HttpUrlPath.storageDatas, params, mActivity);
		// }
		//
		// private void parseMapData(final byte[] responseBody, final
		// RewriteDialog dialog) {
		// GMapActivity.mainHandler.post(new Runnable() {
		// @Override
		// public void run() {
		// try {
		// InputStream is = new ByteArrayInputStream(responseBody);
		// FileUtils.deleteFile(GMapUtils.zipFilePath); // 删除之前的zip文件
		// FileUtils.deleteFile(GMapUtils.unZipPath); // 删除之前的文件
		// FileUtils.writeFile(GMapUtils.zipFilePath, is); // 写入数据
		// ZipUtil.UnZipFolder(GMapUtils.zipFilePath, GMapUtils.unZipPath); //
		// 解压数据
		// StoredData.setMapDataVersion(GMapUtils.unZipPath + File.separator +
		// GMapUtils.FILE_VERSION);
		// if (dialog != null && dialog.isShowing()) {
		// dialog.dismiss();
		// }
		// // 界面跳转
		// Intent intent = new Intent(mActivity, GMapActivity.class);
		// startActivity(intent);
		// } catch (Exception e) {
		// if (dialog != null && dialog.isShowing()) {
		// dialog.dismiss();
		// }
		// UIHelper.showToast(mActivity, "解析zip出错了!", Toast.LENGTH_SHORT).show();
		// e.printStackTrace();
		// }
		// }
		// });
		toGIS(apkPath, apkPackageName);
	}

	public static final String ACTION_GIS = "android.intent.action.SYNC_GIS";
	
	private void toGIS(String apkPathName, String apkPackageName) {
		// 检查apk文件是否存在
		if (FileUtils.isFileExist(apkPathName)) {
			// 检查apk是否安装
			if (ActivityUtils.isAppInstalled(mActivity, apkPackageName)) {
				// 检查已安装版本与本地apk版本是否一致
				if (ActivityUtils.getUninstallAPKVersionCode(mActivity, apkPathName) == 
						ActivityUtils.getInstallAPKVersionCode(mActivity, apkPackageName)) {
					// ActivityUtils.openApk(mActivity, apkPathName);
					Intent intent = new Intent(ACTION_GIS);
					Bundle b = new Bundle();
					b.putString("ps_id", StoredData.getPs_id());
					b.putString("cookie", SimpleJSONUtil.getCookie());
					b.putString("base_url", HttpUrlPath.basePath);
					intent.putExtras(b);
					startActivity(intent);
				} else {
					ActivityUtils.installApk(mActivity, new File(apkPathName));
				}
			} else {
				ActivityUtils.installApk(mActivity, new File(apkPathName));
			}
		} else {
			UIHelper.showToast(getString(R.string.sync_gis_nofound));
		}
	}

	/**
	 * 从assets目录中复制整个文件夹内容
	 * 
	 * @param context
	 *            Context 使用CopyFiles类的Activity
	 * @param oldPath
	 *            String 原文件路径 如：/aa
	 * @param newPath
	 *            String 复制后路径 如：xx:/bb/cc
	 */
	public void copyFilesFassets(Context context, String oldPath, String newPath) {
		try {
			String fileNames[] = context.getAssets().list(oldPath);// 获取assets目录下的所有文件及目录名
			if (fileNames.length > 0) {// 如果是目录
				File file = new File(newPath);
				file.mkdirs();// 如果文件夹不存在，则递归
				for (String fileName : fileNames) {
					copyFilesFassets(context, oldPath + "/" + fileName, newPath + "/" + fileName);
				}
			} else {// 如果是文件
				InputStream is = context.getAssets().open(oldPath);
				FileOutputStream fos = new FileOutputStream(new File(newPath));
				byte[] buffer = new byte[1024];
				int byteCount = 0;
				while ((byteCount = is.read(buffer)) != -1) {// 循环从输入流读取
					fos.write(buffer, 0, byteCount);// 将读取的输入流写入到输出流
				}
				fos.flush();// 刷新缓冲区
				is.close();
				fos.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 如果捕捉到错误则通知UI线程
			// MainActivity.handler.sendEmptyMessage(COPY_FALSE);
		}
	}

	public void toChatOnClick(View v) {

	}

	public void initView() {
		setTitleString("Monitor");
		listview = (ListView) findViewById(R.id.listview);
		screenList = ScreenProcessBean.getMonitorMenuData();
		adp = new ScreenMenuAdapter();
		listview.setAdapter(adp);
		listview.setDivider(null);
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ScreenProcessBean bean = screenList.get(position);
				jumpScreenActivity(bean);
			}
		});
	}

	public void jumpScreenActivity(final ScreenProcessBean bean) {
		Intent intent = new Intent();

		if (bean.monitorType == ScreenProcessBean.Transport) {
			UIHelper.showToast(mActivity, getString(R.string.sync_developing));
			return;
		}

		switch (bean.monitorType) {
		case ScreenProcessBean.Load_Or_Receive_Processing:
			intent.setClass(mActivity, ProcessingActivity.class);
			break;
		case ScreenProcessBean.DN_Or_RN_Routy:
			intent.setClass(mActivity, RN_Or_DN_Activity.class);
			break;
		// case ScreenProcessBean.RN_Routy:
		// intent.setClass(mActivity, ProcessingActivity.class);
		// break;
		case ScreenProcessBean.Processing:
			intent.setClass(mActivity, ProcessingActivity.class);
			break;
		}
		intent.putExtra("type", bean.monitorType);
		intent.putExtra("small_title", bean.screenName);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	// ==================nested===============================

	class ScreenMenuAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return screenList == null ? 0 : screenList.size();
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.screen_menu_item_layout, null);
				holder = new Holder();
				holder.screen_name = (TextView) convertView.findViewById(R.id.screen_name);
				holder.layout = (View) convertView.findViewById(R.id.layout);
				holder.arrow = (ImageView) convertView.findViewById(R.id.arrow);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			ScreenProcessBean bean = screenList.get(position);

			// //---------------判断显示背景
			// if(getCount()==1){
			// holder.layout.setBackgroundResource(R.drawable.list_select_onlyone_style);//(holder.layout_onlyone);
			// }else{
			// if(position==0){
			// holder.layout.setBackgroundResource(R.drawable.list_select_top_style);
			// }else if((position+1)==getCount()){
			// holder.layout.setBackgroundResource(R.drawable.list_select_bottom_style);
			// }else{
			// holder.layout.setBackgroundResource(R.drawable.list_select_center_style);
			// }
			// }

			// switch (position%3) {
			// case 0:
			// holder.arrow.setBackgroundResource(R.drawable.monitor_right_arrow_red);
			// holder.screen_name.setTextColor(mActivity.getResources().getColor(R.color.monitor_color_red));
			// break;
			// case 1:
			// holder.arrow.setBackgroundResource(R.drawable.monitor_right_arrow_yellow);
			// holder.screen_name.setTextColor(mActivity.getResources().getColor(R.color.monitor_color_yellow));
			// break;
			// case 2:
			// holder.arrow.setBackgroundResource(R.drawable.monitor_right_arrow_blue);
			// holder.screen_name.setTextColor(mActivity.getResources().getColor(R.color.monitor_color_blue));
			// break;
			// }
			holder.screen_name.setText(bean.screenName);

			return convertView;
		}

		private class Holder {
			TextView screen_name;
			ImageView arrow;
			View layout;
		}
	}

	private final BroadcastReceiver apkInstallListener = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())) {
				// Intent i = new Intent("android.intent.action.SYNC_GIS");
				// i.putExtra("ps_id", StoredData.getPs_id());
				// startActivity(i);
			}
		}
	};

	// 注册监听
	private void registerSDCardListener() {
		IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
		intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
		intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
		intentFilter.addDataScheme("package");
		registerReceiver(apkInstallListener, intentFilter);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// unregisterReceiver(apkInstallListener);
	}

}
