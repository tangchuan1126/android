package oso.ui.monitor.screen.tool;

import java.io.Serializable;

import support.dbhelper.StoredData;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;

public class MonitorUtils implements Serializable {
	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = 978935748182979333L;
	public String basePath;
	public String baseName;
	public String ps_name;
	public String ps_id;
	public int heightPixels;
	public int widthPixels;
	public String screenStr;

	public static MonitorUtils getScreenData(Activity activity, View rootLay, String screenStr) {
		MonitorUtils m = null;
		try {
			m = new MonitorUtils();
			String serverName = StoredData.readBaseServerName();
			m.basePath = HttpUrlPath.serverUrls.get(serverName);
			m.baseName = serverName;
			m.ps_id = StoredData.getPs_id();
			m.ps_name = StoredData.getPs_name();

			int a = rootLay.getHeight() + Utility.getStatusBarHeight(activity);
			int b = rootLay.getWidth();

			m.heightPixels = (a <= b) ? a : b;
			m.widthPixels = (a >= b) ? a : b;

			m.screenStr = screenStr;

			return m;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @Description:判断该应用是否存在
	 * @param @param packageName
	 * @param @return
	 */
	public static boolean checkPackage(Activity activity, String packageName) {
		if (packageName == null || "".equals(packageName)) {
			return false;
		}
		try {
			activity.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

}
