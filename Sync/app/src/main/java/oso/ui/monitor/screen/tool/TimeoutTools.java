package oso.ui.monitor.screen.tool;

/**
 * @ClassName: ScreenMonitorTools 
 * @Description: 大屏幕工具类  包括屏幕轮换时间 以及通用尺寸
 * @author gcy
 * @date 2014-9-24 下午3:47:22  
 */
public class TimeoutTools {
	
//	//selectGateCheckIn
//	public static final float TASK_GATE_CHECKIN = 0.5f;
//	//selectWareHouseCheckIn
//	public static final float TASK_WAREHOUSE_CHECKIN = 1.0f;
//	//selectWareHouseNumberProcessing
//	public static final float TASK_WAREHOUSENUMBER_PROCESSING = 2.0f;
	
	
	public static final int POSTNUM = 2;//设置请求几次 弹出Connation Dialog
	
	
	private long timeMS = 0;

	public static boolean isFastPost(TimeoutTools t,int screen_expiration_time) {
		long time = System.currentTimeMillis();
		long timeD = time - t.getTimeMS();
		
		if (timeD > screen_expiration_time) {
			t.setTimeMS(time);			
			return true;
		}
		
		return false;
	}
	
	public long getTimeMS() {
		return timeMS;
	}
	public void setTimeMS(long timeMS) {
		this.timeMS = timeMS;
	}
	
	


}
