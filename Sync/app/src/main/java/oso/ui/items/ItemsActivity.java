package oso.ui.items;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.SearchAssignTaskActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskMainActivity;
import oso.ui.load_receive.print.PrintEntryActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import declare.com.vvme.R;

public class ItemsActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_items_main, 0);
		setTitleString("Items");
		initView();
	}

	private void initView() {
		findViewById(R.id.btn_Assign).setOnClickListener(this);
		findViewById(R.id.btn_Process).setOnClickListener(this);
		findViewById(R.id.btn_print).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.btn_Assign:
			intent.setClass(mActivity, SearchAssignTaskActivity.class);
			break;
		case R.id.btn_Process:
			intent.setClass(mActivity, CheckInTaskMainActivity.class);
			break;
		case R.id.btn_print:
			intent.setClass(mActivity, PrintEntryActivity.class);
			break;

		default:
			break;
		}
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
