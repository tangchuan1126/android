package oso.ui.load_receive.window;

import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.FragSelRes;
import support.common.UIHelper;
import support.key.EntryDetailNumberStateKey;
import support.key.ModuleKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

@Deprecated
public class ChangeDoorActivity extends BaseActivity {

	private TextView moduleTypeTv, moduleNumTv, stateTv, notifyTv;
	private Button submitBtn;

	private List<ResourceInfo> doorlist;
	private EntryBean mEntry;
	private EquipmentBean equipment;
	private WindowTaskBean taskBean;

	private FragSelRes fragRes;

	private int type = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_change_door, 0);
		initData();
		initView();
		setView();
	}

	private void initData() {
		mEntry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		equipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		doorlist = (List<ResourceInfo>) getIntent().getSerializableExtra("doorlist");
		taskBean = (WindowTaskBean) getIntent().getSerializableExtra("WindowTaskBean");
		type = getIntent().getIntExtra("type", 0);
		setTitleString(type == OccupyTypeKey.DOOR ? getString(R.string.shuttle_select_door) : getString(R.string.shuttle_select_spot));
	}

	private void setView() {
		moduleTypeTv.setText(ModuleKey.getCheckInModuleKey(taskBean.number_type));
		moduleNumTv.setText(taskBean.number);
		stateTv.setText(EntryDetailNumberStateKey.getType(mActivity,taskBean.number_status));
		notifyTv.setText(taskBean.close_user);
		// 初始化fragRes
		fragRes.init(doorlist,(equipment!=null&&type==equipment.getResources_type())?equipment.getResources_id():0);
		submitBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ResourceInfo selectDoorInfo = fragRes.getSelectRes();
				if (selectDoorInfo == null) {
					UIHelper.showToast(mActivity, OccupyTypeKey.getPrompt(mActivity,type), Toast.LENGTH_SHORT).show();
					return;
				}
				reqChangeResources(selectDoorInfo);
			}
		});
	}

	private void reqChangeResources(ResourceInfo selectDoorInfo) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.TaskChangeResourceOrEquipment);
		params.add("detail_id", taskBean.detail_id);
		params.add("occupy_type", selectDoorInfo.resource_type + "");
		params.add("occupy_id", selectDoorInfo.resource_id + "");
		params.add("equipment_id", equipment.getEquipment_id() + "");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				Utility.popTo(mActivity, TaskListActivity.class);
			}
		}.doGet(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private void initView() {
		fragRes = (FragSelRes) getFragmentManager().findFragmentById(R.id.fragRes);
		moduleTypeTv = (TextView) findViewById(R.id.moduleTypeTv);
		moduleNumTv = (TextView) findViewById(R.id.moduleNumTv);
		stateTv = (TextView) findViewById(R.id.stateTv);
		notifyTv = (TextView) findViewById(R.id.notifyTv);
		submitBtn = (Button) findViewById(R.id.submitBtn);
	}

}
