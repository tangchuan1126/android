package oso.ui.load_receive.do_task.load.smallparcel.bean;

import java.io.Serializable;
import java.util.List;

import utility.Utility;

public class CarrierBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6646825643131189814L;
	
	public String carrier_name;
	public String carrier_id;
	
	
	//==================static===============================
	
	public static String[] getCarrierNames(List<CarrierBean> list){
		if(Utility.isEmpty(list))
			return null;
		
		String[] listRet=new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			listRet[i]=list.get(i).carrier_name;
		}
		return listRet;
	}
	
	
	
	
	
}
