package oso.ui.load_receive.do_task.receive.task.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.receive.task.Rec_ScanTask_PalletsAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import support.common.print.OnInnerClickListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * Created by Administrator on 2015-5-29.
 */
public class AdpRecScan_Plts extends BaseAdapter {

    private Rec_ScanTask_PalletsAc ct;
    private List<Rec_PalletBean> listPlts;
    private OnInnerClickListener onInnerClick;

    public AdpRecScan_Plts(Rec_ScanTask_PalletsAc ct,List<Rec_PalletBean> listPlts,OnInnerClickListener onInnerClick){
        this.ct=ct;
        this.listPlts=listPlts;
        this.onInnerClick=onInnerClick;
    }

    @Override
    public int getCount() {
        return listPlts == null ? 0 : listPlts.size();
    }

    @Override
    public Object getItem(int position) {
        return listPlts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Rec_PalletBean b = listPlts.get(position);
        View.OnClickListener onClick_ls = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(onInnerClick!=null)
                    onInnerClick.onInnerClick(v,position);
            }
        };

        //============================================

        Holder h;
        if (convertView == null) {
            convertView = LayoutInflater.from(ct).inflate(R.layout.item_rec_plt_list_new, null);
            h = new Holder();
            h.tvPltNO = (TextView) convertView.findViewById(R.id.tvPltNO);
            h.tvQty = (TextView) convertView.findViewById(R.id.tvQty);
            h.locTv = (TextView) convertView.findViewById(R.id.locTv);
            h.pltIv = (ImageView) convertView.findViewById(R.id.pltIv);
            h.loBg = convertView.findViewById(R.id.loBg);
            h.itemBgLayout = convertView.findViewById(R.id.itemBgLayout);
            h.tvRepeat = convertView.findViewById(R.id.tvRepeat);
            h.printBtn = (TextView) convertView.findViewById(R.id.printBtn);
            h.tvDamage_it = (TextView) convertView.findViewById(R.id.tvDamage_it);
            h.scanBtn = (TextView) convertView.findViewById(R.id.scanBtn);
            h.photoIv = (ImageView) convertView.findViewById(R.id.iv);
            h.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
            h.tvPltType=(TextView) convertView.findViewById(R.id.tvPltType);
            h.tvLocType=(TextView) convertView.findViewById(R.id.tvLocType);
            h.tvPackagingType= (TextView) convertView.findViewById(R.id.tvPackagingType);
            convertView.setTag(h);
        } else
            h = (Holder) convertView.getTag();

        // 刷ui
        h.tvPltNO.setText(b.wms_container_id);
        h.locTv.setText(b.location);
        h.tvLocType.setText(b.getLoc_str()+": ");

        // debug
        // h.printBtn.setSelected(b.isPrinted());
        // if(true)
        // return convertView;

        // qty
        String disQty = b.getSN_cnt_ok() + "";
        int totalQty = b.getSnQty_needScan();

        if (totalQty > 0)
            disQty = disQty + " / " + totalQty;
        h.tvQty.setText(disQty);

        h.pltIv.setImageResource(b.isDamagePlt() ? R.drawable.plt_dmg : R.drawable.plt_normal);
        int pltNO_colorRes = b.isDamagePlt() ? R.color.sync_red_light : R.color.sync_blue;
        int pltNO_color = ct.getResources().getColor(pltNO_colorRes);
        h.tvPltNO.setTextColor(pltNO_color);
        h.tvQty.setTextColor(pltNO_color);

        int snDmg = b.getSn_dmg();
        boolean showDmg = !b.isDamagePlt() && snDmg > 0;
        h.tvDamage_it.setText("D: " + snDmg);
        h.tvDamage_it.setVisibility(showDmg ? View.VISIBLE : View.GONE);

        //tlp/clp
        h.tvPltType.setText(b.isClp()?"CLP":"TLP");

        h.tvRepeat.setVisibility(b.is_repeat() ? View.VISIBLE : View.GONE);
        // 需打印时 高亮
        h.itemBgLayout.setSelected(b.isClosed());
        // 注:h.itemBgLayout.setSelected会影响子布局,所以"子按钮"放在下面处理
        h.printBtn.setSelected(b.isPrinted());
        h.scanBtn.setSelected(b.isClosed());
        
        if(!TextUtils.isEmpty(b.type_name)){
        	h.tvPackagingType.setText(b.type_name);
        }

        // photo
        boolean hasPhoto = b.hasPhoto();
        h.photoIv.setSelected(hasPhoto);
        h.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
        h.tvImgCnt.setText(b.getPhotoCnt() + "");

        // 事件
        h.photoIv.setOnClickListener(onClick_ls);
        h.printBtn.setOnClickListener(onClick_ls);
        h.scanBtn.setOnClickListener(onClick_ls);
        h.loBg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ct.showMenuDialog(b);
                return true;
            }
        });

        return convertView;
    }

    class Holder {
        View loBg, tvRepeat, itemBgLayout;
        TextView tvPltNO, tvQty, locTv, tvImgCnt,tvLocType;
        ImageView pltIv, photoIv;
        TextView scanBtn, printBtn, tvDamage_it,tvPltType;
        TextView tvPackagingType;
    }

}

