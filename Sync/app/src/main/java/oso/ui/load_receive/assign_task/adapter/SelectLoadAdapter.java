package oso.ui.load_receive.assign_task.adapter;

import java.util.List;

import support.common.bean.SelectLoadBase;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * 
 * @ClassName: ReceiptsTicketBase 
 * @Description:
 * @author gcy 
 * @date 2014年7月17日 上午11:24:47 
 *
 */
public class SelectLoadAdapter extends BaseAdapter {
	
	private List<SelectLoadBase> arrayList;
	private LayoutInflater inflater;
	private Context context;
	private Resources resources;
	
	public SelectLoadAdapter(Context context, List<SelectLoadBase> arrayList ) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		resources = context.getResources();
		this.inflater  = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public SelectLoadBase getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		SelectLoadHoder holder = null;
		 
		 if(convertView==null){
			holder = new SelectLoadHoder();
			convertView = inflater.inflate(R.layout.printiconbar_select_load_layout_listview_item,null);
			holder.company_id = (TextView) convertView.findViewById(R.id.company_id);
			holder.load_no = (TextView) convertView.findViewById(R.id.load_no);
			holder.customer_id = (TextView) convertView.findViewById(R.id.customer_id);
 			convertView.setTag(holder);
		}else{
			holder = (SelectLoadHoder) convertView.getTag();
		}
		 
		final SelectLoadBase temp  =  arrayList.get(position);
 
		holder.company_id.setText(temp.getOmpany_id()+"");
		holder.load_no.setText(temp.getLoad_no()+"");
		holder.customer_id.setText(temp.getCustomer_id()+"");
	 
		 
 		return convertView;
	}
 
}
class SelectLoadHoder {
	public TextView company_id;
	public TextView load_no;
	public TextView customer_id;

}
