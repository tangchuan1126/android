package oso.ui.load_receive.do_task.load.smallparcel.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.dbhelper.StoredData;
import utility.Utility;
import android.graphics.Point;
import android.text.TextUtils;

public class SP_OrderBean {

	public String wms_order_id;
	public String order_numbers;
	public String customer_id;
	public String reference_no;
	public String shipped_date;
	public List<SP_TNOrPltBean> pallets;

	// 号数
	public int getNOCnt() {
		return pallets == null ? 0 : pallets.size();
	}

	public SP_TNOrPltBean getNOBean(int index) {
		return pallets.get(index);
	}

//	/**
//	 * @return (x:scaned,y:total)
//	 */
//	public Point getPlt_scaned() {
//		Point ret = new Point();
//		if (Utility.isEmpty(pallets))
//			return ret;
//
//		for (int i = 0; i < pallets.size(); i++) {
//			SP_TNOrPltBean plt = pallets.get(i);
//			if (plt.isTN())
//				continue;
//			ret.y++;
//			if (plt.isScanned())
//				ret.x++;
//		}
//		return ret;
//	}

	/**
	 * @return (x:scaned,y:total)
	 */
	public Point getTN_scaned() {
		Point ret = new Point();
		if (Utility.isEmpty(pallets))
			return ret;

		for (int i = 0; i < pallets.size(); i++) {
			SP_TNOrPltBean plt = pallets.get(i);
			// 跳过plt
//			if (!plt.isTN())
//				continue;
			ret.y++;
			if (plt.isScanned())
				ret.x++;
		}
		return ret;
	}

	public SP_TNOrPltBean getTN(String tnNO) {
		if (Utility.isEmpty(pallets) || TextUtils.isEmpty(tnNO))
			return null;

		for (int i = 0; i < pallets.size(); i++) {
			SP_TNOrPltBean plt = pallets.get(i);
//			if (plt.isTN() && tnNO.equals(plt.wms_pallet_number))
			if (tnNO.equals(plt.wms_pallet_number))
				return plt;
		}
		return null;
	}

	public boolean hasTN(String tnNO) {
		return getTN(tnNO) != null;
	}

	/**
	 * 装整个order
	 * @param isScaned_local 为本地load 暂未提交
	 */
	public void loadOrder(boolean isScaned_local) {
		if (Utility.isEmpty(pallets))
			return;
		String adid = StoredData.getAdid();
		for (int i = 0; i < pallets.size(); i++) {
			SP_TNOrPltBean no = pallets.get(i);
			// 没load 才标load
			if (!no.isScanned()) {
				no.scan_adid = adid;
//			    no.isScaned_local =true;
				no.isScaned_local = isScaned_local;
			}
		}
	}

	// public void unloadOrder() {
	// if (Utility.isEmpty(pallets))
	// return;
	// for (int i = 0; i < pallets.size(); i++) {
	// pallets.get(i).scan_adid ="";
	// }
	// }

//	public void unloadNOs(boolean toUnload_TNs) {
//		if (Utility.isEmpty(pallets))
//			return;
//		for (int i = 0; i < pallets.size(); i++) {
//			SP_TNOrPltBean noBean = pallets.get(i);
//			if (toUnload_TNs == noBean.isTN()) {
//				noBean.scan_adid = "";
//				// debug
//				noBean.isScaned_local = false;
//			}
//		}
//	}

//	public void unloadPlts() {
//		if (Utility.isEmpty(pallets))
//			return;
//		for (int i = 0; i < pallets.size(); i++) {
//			SP_TNOrPltBean noBean = pallets.get(i);
//			if (!noBean.isTN())
//				noBean.scan_adid = "";
//		}
//	}

	/**
	 * 同步-scan状态,即order扫完时 整个order扫掉
	 * 
	 * @return order已load
	 */
//	public boolean syncScanState() {
//		boolean isOk = isScanned();
//		if (isOk){
//			//被同步的no 不标记本地
//			loadOrder(true);
//		}
//		return isOk;
//	}

	/**
	 * order扫了的条件:1.TN(仅1个)扫了,或2.所有plt都扫了
	 * 
	 * @return
	 */
	public boolean isScanned() {
		if (Utility.isEmpty(pallets))
			return true;

//		Point scaned_tn = getTN_scaned();
//		// 若tn已扫
//		if (scaned_tn.y > 0 && (scaned_tn.x == scaned_tn.y))
//			return true;
//
//		Point scaned_plt = getPlt_scaned();
//		// plt全扫
//		if (scaned_plt.y > 0 && (scaned_plt.x == scaned_plt.y))
//			return true;
//		return false;
		
		Point scaned_tn = getTN_scaned();
		return scaned_tn.x==scaned_tn.y;
	}

	public int getNO_scaned() {
		if (Utility.isEmpty(pallets))
			return 0;

		int ret = 0;
		for (int i = 0; i < pallets.size(); i++) {
			if (pallets.get(i).isScanned())
				ret++;
		}
		return ret;
	}

	/**
	 * 部分扫描
	 * 
	 * @return
	 */
	public boolean isPartiallyScaned() {
		if (pallets == null)
			return false;

		// order已扫,加此 防止数据有问题 提交不了
		if (isScanned())
			return false;

		int scaned_no = getNO_scaned();
		return scaned_no > 0 && scaned_no < pallets.size();
	}

	/**
	 * 排序,1.tn在上面 2.扫过的plt在下面
	 * 
	 * @param listOrders
	 */
	public void sort() {

		if (Utility.isEmpty(pallets))
			return;

		// int len = pallets.size();
		// for (int i = len - 1; i >= 0; i--) {
		// SP_TNOrPltBean no = pallets.get(i);
		// // TN移至首部
		// if (no.isTN() && i != 0)
		// pallets.add(0, pallets.remove(i));
		//
		// // 扫过的plt 挪至尾部
		// if (!no.isTN() && no.isScanned())
		// pallets.add(pallets.remove(i));
		// }

		Collections.sort(pallets, comp_tnOrPlt);
	}

	private Comparator<SP_TNOrPltBean> comp_tnOrPlt = new Comparator<SP_TNOrPltBean>() {

		@Override
		public int compare(SP_TNOrPltBean lhs, SP_TNOrPltBean rhs) {
			// TODO Auto-generated method stub
			// 1个tn 1个plt
//			if (lhs.isTN() != rhs.isTN()) {
//				// tn在前面
//				return lhs.isTN() ? -1 : 1;
//			}
//			// 没扫的在前面
//			return !lhs.isScanned() ? -1 : 1;
			
			//扫描状态相同 不换顺序
			if(lhs.isScanned()==rhs.isScanned())
				return 0;
			
			return !lhs.isScanned() ? -1 : 1;
		}
	};

	/**
	 * 变换了 扫描类型
	 * 
	 * @param noIndex
	 * @return
	 */
//	public boolean isChange_loadType(int noIndex) {
//		if (Utility.isEmpty(pallets))
//			return false;
//
//		SP_TNOrPltBean no = pallets.get(noIndex);
//		int tn_scaned = getTN_scaned().x;
//		int plt_scaned = getPlt_scaned().x;
//
//		// tn
//		if (no.isTN() && tn_scaned == 0 && plt_scaned > 0)
//			return true;
//		// plt
//		if (!no.isTN() && plt_scaned == 0 && tn_scaned > 0)
//			return true;
//		return false;
//	}

	// ==================static===============================
	
	public static void keepScaned(List<SP_OrderBean> listNew,List<SP_OrderBean> listOld){
		
		if(Utility.isEmpty(listNew)||Utility.isEmpty(listOld))
			return;
		HashMap<String, SP_TNOrPltBean> mapOld=getLocalScaned_tnID_bean(listOld);
		
		for (int i = 0; i < listNew.size(); i++) {
			SP_OrderBean order=listNew.get(i);
			List<SP_TNOrPltBean> tns=order.pallets;
			if(tns==null)
				continue;
			int len_plts=tns.size();
			for (int j = 0; j < len_plts; j++) {
				SP_TNOrPltBean tn=tns.get(j);
				SP_TNOrPltBean tn_old=mapOld.get(tn.wms_order_type_id);
				//若本地扫过
				if(tn_old!=null)
				{
					tn.isScaned_local=true;
					tn.scan_adid=tn_old.scan_adid;
				}
			}
		}
		
	}
	
	/**
	 * 获取-localScan后未提交的,<tnID,SP_TNOrPltBean>键值对
	 * @param listOrders
	 * @return
	 */
	public static HashMap<String, SP_TNOrPltBean> getLocalScaned_tnID_bean(List<SP_OrderBean> listOrders){
		HashMap<String, SP_TNOrPltBean> mapOld=new HashMap<String, SP_TNOrPltBean>();
		if(Utility.isEmpty(listOrders))
			return mapOld;
		
		for (int i = 0; i < listOrders.size(); i++) {
			SP_OrderBean order=listOrders.get(i);
			List<SP_TNOrPltBean> tns=order.pallets;
			if(tns==null)
				continue;
			int len_plts=tns.size();
			for (int j = 0; j < len_plts; j++) {
				SP_TNOrPltBean tn=tns.get(j);
				if(tn.isScanned()&&tn.isScaned_local)
					mapOld.put(tn.wms_order_type_id, tn);
			}
		}
		return mapOld;
	}
	
	
	public static String getNOs_localScaned(List<SP_OrderBean> listOrders) {
		if (Utility.isEmpty(listOrders))
			return "[]";

		JSONArray ja = new JSONArray();

		int len = listOrders.size();
		for (int i = 0; i < len; i++) {
			SP_OrderBean tmpOrder = listOrders.get(i);
			List<SP_TNOrPltBean> listNO = tmpOrder.pallets;
			if (listNO == null)
				continue;
			int no_len = listNO.size();
			for (int j = 0; j < no_len; j++) {
				JSONObject jo = new JSONObject();
				SP_TNOrPltBean no = listNO.get(j);
				// 为本地扫描
				if (no.isScanned() && no.isScaned_local) {
					try {
						jo.put("scan_number", no.wms_pallet_number);
						jo.put("wms_scan_number_type", no.wms_scan_number_type);
						jo.put("wms_order_type_id", no.wms_order_type_id);
						// 没order的no
						if (!TextUtils.isEmpty(tmpOrder.wms_order_id))
							jo.put("wms_order_id", tmpOrder.wms_order_id);
						ja.put(jo);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}

		return ja.toString();
	}

	public static SP_OrderBean getOrder_PartiallyScaned(
			List<SP_OrderBean> listOrders) {
		if (listOrders == null)
			return null;
		for (int i = 0; i < listOrders.size(); i++) {
			SP_OrderBean tmpOrder = listOrders.get(i);
			if (tmpOrder.isPartiallyScaned())
				return tmpOrder;
		}
		return null;
	}

	public static boolean isAllScaned_no(List<SP_OrderBean> listOrders,
			List<Point> listNO) {
		if (Utility.isEmpty(listOrders) || Utility.isEmpty(listNO))
			return true;
		for (int i = 0; i < listNO.size(); i++) {
			if (!getNOBean(listOrders, listNO.get(i)).isScanned())
				return false;
		}
		return true;
	}

	public static boolean isAllNotScaned_no(List<SP_OrderBean> listOrders,
			List<Point> listNO) {
		if (Utility.isEmpty(listOrders) || Utility.isEmpty(listNO))
			return false;
		for (int i = 0; i < listNO.size(); i++) {
			if (getNOBean(listOrders, listNO.get(i)).isScanned())
				return false;
		}
		return true;
	}

	/**
	 * 排序,1.tn在上面 2.扫过的plt在下面 3.扫过的order在下面
	 * 
	 * @param listOrders
	 */
	public static void sort(List<SP_OrderBean> listOrders) {
		if (Utility.isEmpty(listOrders))
			return;

		int len = listOrders.size();
		for (int i = len - 1; i >= 0; i--) {
			SP_OrderBean order = listOrders.get(i);
			// 扫过的order后挪
			if (order.isScanned())
				listOrders.add(listOrders.remove(i));
			// 对order排序
			order.sort();
		}
	}

	public static SP_TNOrPltBean getNOBean(List<SP_OrderBean> listOrders,
			Point pt) {
		return listOrders.get(pt.x).getNOBean(pt.y);
	}

	/**
	 * order若完成 挪至最后,若未完成 挪至首部
	 * 
	 * @param listOrders
	 * @param pt
	 */
	public static void load(List<SP_OrderBean> listOrders, Point pt) {
		if (Utility.isEmpty(listOrders))
			return;
		final SP_OrderBean order = listOrders.get(pt.x);
		final SP_TNOrPltBean tnBean = order.pallets.get(pt.y);

		tnBean.scan_adid = StoredData.getAdid();
		tnBean.isScaned_local=true;
//		boolean order_loaded = order.syncScanState();
		boolean order_loaded=order.isScanned();

		// 若orderok,挪至后方
		if (order_loaded)
			listOrders.add(listOrders.remove(pt.x));
		// order未完,挪至首部
		else {
			// no拍下序
			order.sort();

			listOrders.add(0, listOrders.remove(pt.x));
		}
	}

	public static void unload(List<SP_OrderBean> listOrders, Point pt) {
		if (Utility.isEmpty(listOrders))
			return;
		final SP_OrderBean order = listOrders.get(pt.x);
		final SP_TNOrPltBean tnBean = order.pallets.get(pt.y);

		tnBean.scan_adid = "";
//		boolean toUnload_tn = !tnBean.isTN();
//		// 若为tn unload所有plts,若为plt unload所有tn
//		order.unloadNOs(toUnload_tn);

		// no排下序
		order.sort();

		// 挪至前方
		listOrders.add(0, listOrders.remove(pt.x));
	}

	public static void loadAll(List<SP_OrderBean> listOrders) {
		if (Utility.isEmpty(listOrders))
			return;
		for (int i = 0; i < listOrders.size(); i++) {
			listOrders.get(i).loadOrder(false);
		}
	}

	/**
	 * @param listOrders
	 * @param no
	 * @return (orderIndex,pt)
	 */
	public static List<Point> getBean_byNO(List<SP_OrderBean> listOrders,
			String no) {

		if (Utility.isEmpty(listOrders) || TextUtils.isEmpty(no))
			return null;

		List<Point> listRet = new ArrayList<Point>();
		for (int i = 0; i < listOrders.size(); i++) {
			SP_OrderBean tmpOrder = listOrders.get(i);
			int tnPos = SP_TNOrPltBean.getNOBean_ByNO(tmpOrder.pallets, no);
			// 若存在
			if (tnPos >= 0) {
//				boolean isTN = tmpOrder.getNOBean(tnPos).isTN();
//				listRet.add(new Point(i, tnPos));
//				// 若为plt 则仅1个(同1no不会有多个plt)
//				if (!isTN)
//					return listRet;
				
				listRet.add(new Point(i, tnPos));
			}
		}
		return listRet;
	}

//	public static Point getPlts_Scaned(List<SP_OrderBean> listOrders) {
//		Point ret = new Point();
//		if (Utility.isEmpty(listOrders))
//			return ret;
//		for (int i = 0; i < listOrders.size(); i++) {
//			Point tmp = listOrders.get(i).getPlt_scaned();
//			ret.offset(tmp.x, tmp.y);
//		}
//		return ret;
//	}

	public static Point getTNs_Scaned(List<SP_OrderBean> listOrders) {
		Point ret = new Point();
		if (Utility.isEmpty(listOrders))
			return ret;
		for (int i = 0; i < listOrders.size(); i++) {
			Point tmp = listOrders.get(i).getTN_scaned();
			ret.offset(tmp.x, tmp.y);
		}
		return ret;
	}

	public static int getOrders_Scaned(List<SP_OrderBean> listOrders) {
		if (Utility.isEmpty(listOrders))
			return 0;
		int ret = 0;
		for (int i = 0; i < listOrders.size(); i++) {
			if (listOrders.get(i).isScanned())
				ret++;
		}
		return ret;
	}

}
