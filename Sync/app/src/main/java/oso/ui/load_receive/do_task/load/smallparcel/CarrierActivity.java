package oso.ui.load_receive.do_task.load.smallparcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.load.smallparcel.util.CarrierListView;
import oso.ui.load_receive.do_task.load.smallparcel.util.CarrierListView.OnItemOrderClickListener;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_ShipToBean;
import utility.ActivityUtils;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class CarrierActivity extends BaseActivity {

	private CarrierListView lv;
	private EditText searchEt;
	private ImageView delBtn;
	
	
	private LinearLayout searchLayout;
	private ListView searchLv;
	
	private List<Rec_ShipToBean> searchShipTo;
	
	private SearchAdapter searchAdapter;

	private Rec_ShipToBean shipTo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_carrier, 0);
		applyParams();
		setTitleString("Ship To");
		initView();
		setData();
	}

	private void initView() {
		searchEt = (EditText) findViewById(R.id.searchEt);
		delBtn = (ImageView) findViewById(R.id.delBtn);
		lv = (CarrierListView) findViewById(R.id.lv);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
	}

	private void setData() {
		searchShipTo = new ArrayList<Rec_ShipToBean>();
		searchAdapter = new SearchAdapter();
		searchLv.setAdapter(searchAdapter);
		
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s)) {
					delBtn.setVisibility(View.GONE);
				} else {
					delBtn.setVisibility(View.VISIBLE);
				}
			}
		});
		delBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchEt.setText("");
			}
		});
		// lv.setEmptyView(findViewById(R.id.nodataTv));
		lv.setAdapter(list);
		lv.setOnItemClickListener(new OnItemOrderClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id, Rec_ShipToBean letter) {
				Intent in=new Intent();
				in.putExtra("index", position);
				setResult(RESULT_OK,in);
                finish();
			}
		});
		
		searchLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent in = new Intent();
				in.putExtra("index",list.indexOf(searchShipTo.get(position)));
				setResult(RESULT_OK,in);
				finish();
				
			}
		});
	}

	private List<Rec_ShipToBean> getDebugDatas() {
		List<Rec_ShipToBean> datas = new ArrayList<Rec_ShipToBean>();
		for (int i = 0; i < 104; i++) {
			//debug
//			datas.add(new CarrierBean(((char) ('A' + i / 4)) + ". Test Name"));
		}
		return datas;
	}
	
	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();
			searchShipTo.clear();
			for(Rec_ShipToBean shipto : list){
				if(shipto.ship_to_name.startsWith(searchStr.toLowerCase()) || shipto.ship_to_name.startsWith(searchStr.toUpperCase())){
					searchShipTo.add(shipto);
				}
			}
			
			searchAdapter.notifyDataSetChanged();
			
		} else {
			hideSearch();
		}
	}
	
	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
		searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}
	
	
	//=========传参===============================
	
	private List<Rec_ShipToBean> list;

	public static void initParams(Intent in,List<Rec_ShipToBean> list,Rec_ShipToBean shipTo) {
		in.putExtra("list",(Serializable)list);
		in.putExtra("shipto", (Serializable)shipTo);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		list=(List<Rec_ShipToBean>)params.getSerializable("list");
		shipTo = (Rec_ShipToBean) params.getSerializable("shipto");
		if(shipTo != null){
			for(Rec_ShipToBean user : list){
				if(user.ship_to_id.equals(shipTo.ship_to_id))
				{
					user.isSelect = true;
				}
			}
		}
	}
	
	//---------------------------------------
	
	private class SearchAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return searchShipTo == null ? 0 : searchShipTo.size();
		}

		@Override
		public Object getItem(int position) {
			return searchShipTo.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				convertView = View.inflate(mActivity, android.R.layout.simple_list_item_1, null);
			}
			((TextView) convertView).setTextColor(Color.BLACK);
			((TextView) convertView).setTextSize(12);
			((TextView) convertView).setText(searchShipTo.get(position).ship_to_name);
			
			return convertView;
		}
		
	}
}
