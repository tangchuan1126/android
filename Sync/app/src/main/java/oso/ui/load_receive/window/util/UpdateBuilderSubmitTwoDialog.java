package oso.ui.load_receive.window.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import declare.com.vvme.R;

public class UpdateBuilderSubmitTwoDialog extends Dialog {

	private static boolean mCancelable = true;

	public UpdateBuilderSubmitTwoDialog(Context context) {
		super(context);
	}

	public UpdateBuilderSubmitTwoDialog(Context context, int theme) {
		super(context, theme);
	}

	public static class Builder {
		private Context context;
		private String title;
		private String mc_name;
		private String mc_value;
		private String carrier_name;
		private String carrier_value;
		private DialogInterface.OnClickListener submit;
		private DialogInterface.OnClickListener close;
		
		private View contentView;
		private boolean isDismiss = true;

		public Builder(Context context) {
			this.context = context;
		}
		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}
		public Builder setMc_name(String mc_name) {
			this.mc_name = mc_name;
			return this;
		}
		public Builder setMc_value(String mc_value) {
			this.mc_value = mc_value;
			return this;
		}
		public Builder setCarrier_name(String carrier_name) {
			this.carrier_name = carrier_name;
			return this;
		}
		public Builder setCarrier_value(String carrier_value) {
			this.carrier_value = carrier_value;
			return this;
		}
		public Builder setContentView(View v) {
			this.contentView = v;
			return this;
		}
		public Builder setSubmit(DialogInterface.OnClickListener listener) {
			this.submit = listener;
			return this;
		}
		public Builder setClose(DialogInterface.OnClickListener listener) {
			this.close = listener;
			return this;
		}
		public boolean isDismiss() {
			return isDismiss;
		}

		// positiveBtn点击后dismiss
		public void setDismiss(boolean isDismiss) {
			this.isDismiss = isDismiss;
		}

		public UpdateBuilderSubmitTwoDialog create() {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final UpdateBuilderSubmitTwoDialog dialog = new UpdateBuilderSubmitTwoDialog(
					context, R.style.Dialog);
			final View layout = inflater
					.inflate(R.layout.builder_update_mcorcrr_layout_two, null);
			dialog.addContentView(layout, new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			
			((TextView) layout.findViewById(R.id.title)).setText(title);
			((TextView) layout.findViewById(R.id.mc_name)).setText(mc_name);
			((TextView) layout.findViewById(R.id.mc_value)).setText(mc_value);
			((TextView) layout.findViewById(R.id.carrier_name)).setText(carrier_name);
			((TextView) layout.findViewById(R.id.carrier_value)).setText(carrier_value);
			((Button) layout.findViewById(R.id.sure)).setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							submit.onClick(dialog,DialogInterface.BUTTON_NEGATIVE);
							if (dialog.isShowing() && isDismiss) {
								dialog.dismiss();
							}
						}
					});
			((Button) layout.findViewById(R.id.cancle))
			.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					close.onClick(dialog,DialogInterface.BUTTON_NEGATIVE);
					if (dialog.isShowing() && isDismiss) {
						dialog.dismiss();
					}
				}
			});
			dialog.setContentView(layout);
			return dialog;
		}

		public void setCancelable(boolean b) {
			mCancelable = b;
		}

		public void dismiss() {
			dismiss();
		}
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking()
				&& !event.isCanceled()) {
			onBackPressed();
			return true;
		}
		return false;
	}
	
	@Override
	public void onBackPressed() {
		if (mCancelable) {
			super.onBackPressed();
		}
	}
}
