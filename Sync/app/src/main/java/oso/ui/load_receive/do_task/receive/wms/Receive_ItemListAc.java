package oso.ui.load_receive.do_task.receive.wms;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskExceptionActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskFinishActivity;
import oso.ui.load_receive.do_task.receive.wms.adapter.Receive_ItemListAcAdapter;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.VpWithTab;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.VSealBox;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.common.datas.HoldDoubleValue;
import support.common.print.PrintTool;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * item列表
 * 
 * @author 朱成
 * @date 2015-3-2
 */
public class Receive_ItemListAc extends BaseActivity implements OnClickListener {

	private SingleSelectBar tab;
	private VpWithTab vp;

	private TabToPhoto ttp;

	private ListView lvItems;
	private Receive_ItemListAcAdapter adapter;

	private TextView tctnr_id, tcompany_id, tcustomer_id, ttitle_id, tnote_id;

	private Rec_ReceiveBean recBean;
	
	private Button btnClose_bot,btnExp_bot;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_receive_itemlist, 0);
		applyParams();
		// 取v
		tab = (SingleSelectBar) findViewById(R.id.tab);
		vp = (VpWithTab) findViewById(R.id.vp);
		lvItems = (ListView) findViewById(R.id.lvItems);
		btnClose_bot=(Button)findViewById(R.id.btnClose_bot);
		btnExp_bot=(Button)findViewById(R.id.btnExp_bot);
		
		//事件
		btnClose_bot.setOnClickListener(this);
		btnExp_bot.setOnClickListener(this);

		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>("Receive", 0));
		list.add(new HoldDoubleValue<String, Integer>("Others", 1));
		tab.setUserDefineClickItems(list);
		// 初始化vp
		vp.init(tab);
//		refPhotoTab();
		initOtherview();

		// lv
		adapter = new Receive_ItemListAcAdapter(mActivity, recBean);
		lvItems.setAdapter(adapter);

		// 菜单
		showRightButton(R.drawable.menu_btn_style, "", this);
		// 初始化
		tctnr_id.setText(recBean.ctnr);
		tcompany_id.setText(recBean.company_id);
		tcustomer_id.setText(recBean.customer_id);
		ttitle_id.setText(recBean.title);
//		receipt_id = recBean.receipt_id;
		tnote_id.setText(recBean.osoNote);
		tnote_id.setVisibility(TextUtils.isEmpty(recBean.osoNote) ? View.GONE : View.VISIBLE);
		setTitleString("Receipt: " + recBean.receipt_no);
		refSummaryUI();
	
		//弹框-提示note
		if (recBean.hasOsoNote())
			RewriteBuilderDialog.showSimpleDialog_Tip(this, "Note: "+recBean.osoNote);


	}
	
	public static final String NewIn_tipPrintRT="tipPrintRT";
	
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		
		//提示打-receiptTicket
		boolean tipPrintRT=intent.getBooleanExtra(NewIn_tipPrintRT,false);
		if(tipPrintRT)
			tipPrintReceiptTicket();
	}
	
	//提示-打receiptTicket
	private void tipPrintReceiptTicket() {
		RewriteBuilderDialog.Builder bd = RewriteBuilderDialog.newSimpleDialog(this, "Print Receipt Ticket?", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 打签
				printTool.showDlg_printers(printLs_RNTicket);
			}
		});
		bd.show();
	}
	
	private boolean isRefreshTTP_onResume=true;	//第一次进需刷

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
		
		if(TabToPhoto.isFromTTP(requestCode))
			isRefreshTTP_onResume=false;
		
	}

	private boolean isFirst_onResume = true;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// 第一次-不刷
		if (!isFirst_onResume){
			refreshItems();
		}
		isFirst_onResume = false;
	
		//刷ttp
		if(isRefreshTTP_onResume)
			refPhotoTab();
		isRefreshTTP_onResume=true;
	}
	
	private void refSummaryUI(){
		boolean showNormalClose=false;
		boolean showExpClose=false;
		
		//都cnt过
		if(recBean.isAllLineCnted()){
			if(recBean.isCan_NormalClose())
				showNormalClose=true;
			else
				showExpClose=true;
		}
		btnClose_bot.setVisibility(showNormalClose?View.VISIBLE:View.GONE);
		btnExp_bot.setVisibility(showExpClose?View.VISIBLE:View.GONE);
	}

	private void refreshItems() {
		final RequestParams params = new RequestParams();
		params.add("company_id", recBean.company_id); // ABL
		params.add("receipt_no", recBean.receipt_no); // 4
		params.add("detail_id", recBean.dlo_detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				Rec_ReceiveBean bean = Rec_ReceiveBean.handJsonForBean(json);
				// Intent in = new Intent(mActivity, Receive_ItemListAc.class);
				// Receive_ItemListAc.initParams(in, bean);
				// startActivity(in);
				recBean.receipt_ticket_printed=bean.receipt_ticket_printed;
				recBean.status=bean.status;
				
				//刷lv
				recBean.lines.clear();
				recBean.lines.addAll(bean.lines);
				adapter.notifyDataSetChanged();
				refSummaryUI();
			}

		}.doGet(HttpUrlPath.acquireRNLineInfo, params, this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRight:
			showDlg_menu();
			break;
		case R.id.btnClose_bot:		//正常关闭
			onClick_normalClose();
			break;
		case R.id.btnExp_bot:		//异常关闭
			onClick_expClose();
			break;

		default:
			break;
		}
	}

	private void showDlg_menu() {

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_operation));
//		"Reload",
		builder.setArrowItems(new String[] { "Print Receipt Ticket"/*, "Close Task"*/, "Exception Close" }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//reload
//				if (position == 0) {
//					UIHelper.showToast(getString(R.string.sync_developing));
//				}
				
				switch (position) {
				case 0:						//打印receiptTicket
					//没cnt完
					if(!recBean.isAllLineCnted()){
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please Finish Count first!");
						return;
					}
					printTool.showDlg_printers(printLs_RNTicket);
					break;
				case 1:		// 不需要在里面做normalClose
					//normalClose
//					//没cnt完
//					if(!recBean.isAllLineCnted()){
//						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please Finish Count first!");
//						return;
//					}
//					onClick_normalClose();
//					break;	
//				case 2:						
					
					//exceptionClose
					onClick_expClose();
					break;

				default:
					break;
				}
			}
		});
		builder.show();
	}
	
	private void onClick_normalClose(){
		//同预期不服
		if(!recBean.isCan_NormalClose()){
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, 
					"The actual receipt is not as expected, you can just Exception Close!");
			return;
		}
		//还没打rnTicket,debug
//		if(!recBean.receipt_ticket_printed){
//			tipPrintRNTicket_beforeClose(true);
//		}
//		else{
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setMessage(getString(R.string.tms_close_task) + "?");
		builder.setPositiveButton(getString(R.string.sync_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						closeway();
					}
				});
		builder.show();
//		}
	}
	
	private void onClick_expClose(){
		//若已cnt完 则提示打rnTicket		
//		if(recBean.isAllLineCnted()&&!recBean.receipt_ticket_printed){
//			tipPrintRNTicket_beforeClose(false);
//		}
//		else{
		jumpToExceptionAc();
//		}
	}
	
	private void jumpToExceptionAc(){
		Intent intent = new Intent(mActivity, CheckInTaskExceptionActivity.class);
		intent.putExtra("entry_id", recBean.doorBean.entry_id);
		intent.putExtra("detail_id", recBean.dlo_detail_id);
		intent.putExtra("closeType", ScanLoadActivity.CloseType_Exception);
		intent.putExtra("taskmainlist", recBean.doorBean);// CheckInTaskBeanMain
		intent.putExtra("title", "Receipt: " + recBean.receipt_no);
		startActivity(intent);
	}
	
	/**
	 * 关闭前-提示打签
	 * @param isNormalClose
	 */
	@Deprecated
	private void tipPrintRNTicket_beforeClose_x(final boolean isNormalClose){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setMessage(getString(R.string.sync_receipt_print));
		builder.setPositiveButton(getString(R.string.print_linear), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				printTool.showDlg_printers(printLs_RNTicket);
			}
		});
		builder.setNegativeButton(isNormalClose? getString(R.string.sys_button_close) : getString(R.string.tms_exception), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//正常关闭
				if(isNormalClose)
					closeway();
				//异常关闭
				else
					jumpToExceptionAc();
			}
		});
		builder.show();
		
	}
	
	private void reqPrintRNTicket(long printServerID){
		RequestParams p=new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		p.add("printer", printServerID+"");
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				recBean.receipt_ticket_printed=true;
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/printRNCount", p, mActivity);
		
	}
	
	private PrintTool printTool=new PrintTool(this);
	private PrintTool.OnPrintLs printLs_RNTicket=new PrintTool.OnPrintLs() {
		
		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintRNTicket(printServerID);
		}
	};
	
	public void closeway() {
		String entry_id=recBean.doorBean.entry_id;
		RequestParams params=NetInterface.closeTask_p(entry_id, 
				recBean.dlo_detail_id, ScanLoadActivity.CloseType_Normal, recBean.doorBean.equipment_id,
				recBean.doorBean.resources_type, recBean.doorBean.resources_id, ttp,true);
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				int returnflag = json.optInt("returnflag");
				// 若为门下最后1个(1/2/4跳,3不跳),直接至finish页
				if (returnflag != CheckInTaskFinishActivity.LoadCloseNoifyNormal) {
					Intent in = new Intent(mActivity,
							CheckInTaskFinishActivity.class);
					CheckInTaskFinishActivity.initParams(in, recBean.doorBean);
					mActivity.startActivity(in);
				}
				//若为normalClose,至"任务列表"
				else
					Utility.popTo(mActivity, CheckInTaskDoorItemActivity.class);
				
				mActivity.finish();
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	private void reqReload() {
		RequestParams p = new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");

			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/reload", p, this);

	}

	private void initOtherview() {
		tctnr_id = (TextView) findViewById(R.id.ctnr_text_id);
		tcompany_id = (TextView) findViewById(R.id.company_text_id);
		tcustomer_id = (TextView) findViewById(R.id.customer_text_id);
		ttitle_id = (TextView) findViewById(R.id.title_text_id);
		tnote_id = (TextView) findViewById(R.id.note_text_id);
	}

//	public static String receipt_id;

	// ==================photoTab===============================

	private void refPhotoTab() {

		if (doorBean == null)
			return;

		// 取view
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		VSealBox sealBox = (VSealBox) findViewById(R.id.sealBox);
		// 初始化ttp
		TabParam param = getTab_load(ttp, doorBean.entry_id,recBean.dlo_detail_id);
		ttp.initNoTitleBar(this, param);

		// 初始化seal
		sealBox.init(doorBean);
	}
	
	private TabParam getTab_load(TabToPhoto ttp, String entry,
			String detailID) {
		// 基于entry
		String loadKey = TTPKey.getTaskProcessKey(entry);
		TabParam p = TabParam.new_NoTitle(loadKey, ttp);
		p.setWebImgsParams(FileWithCheckInClassKey.PhotoReceive + "",
				FileWithTypeKey.OCCUPANCY_MAIN + "", entry,detailID);
		return p;
	}

	// =========传参===============================

	private CheckInTaskBeanMain doorBean;

	public static void initParams(Intent in, Rec_ReceiveBean recBean) {
		in.putExtra("recBean", recBean);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		recBean = (Rec_ReceiveBean) params.getSerializable("recBean");
		doorBean = recBean.doorBean;

	}

}
