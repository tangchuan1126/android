package oso.ui.load_receive.do_task.load.smallparcel.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.ui.load_receive.do_task.load.smallparcel.SmallParcelAc;
import oso.ui.load_receive.do_task.load.smallparcel.bean.SP_TNOrPltBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.common.tts.TTS;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SParcel_NoSheetPage implements OnClickListener {

	private SmallParcelAc ac;
	private View contentV;

	private ListView lv_ns;
	private AdpNOSheet adpNOSheet;
	private List<SP_TNOrPltBean> listNO = new ArrayList<SP_TNOrPltBean>();

	private SearchEditText searchEt_ns;
	private TextView tvResType_ns, tvDock_ns, tvCarrier_ns, tvTotal_ns;

	public CheckInTaskBeanMain doorBean;
	public CheckInTaskItemBeanMain taskBean;

	public SParcel_NoSheetPage(SmallParcelAc ac, View contentV) {
		// TODO Auto-generated constructor stub
		this.ac = ac;
		this.contentV = contentV;
		doorBean = ac.doorBean;
		taskBean = ac.taskBean;

		// v
		btnAddOrDel = (ImageButton) contentV.findViewById(R.id.btnAddOrDel_ns);
		searchEt_ns = (SearchEditText) contentV.findViewById(R.id.searchEt_ns);
		lv_ns = (ListView) contentV.findViewById(R.id.lv_ns);
		tvResType_ns = (TextView) contentV.findViewById(R.id.tvResType_ns);
		tvDock_ns = (TextView) contentV.findViewById(R.id.tvDock_ns);
		tvCarrier_ns = (TextView) contentV.findViewById(R.id.tvCarrier_ns);
		tvTotal_ns = (TextView) contentV.findViewById(R.id.tvTotal_ns);

		// 事件
		btnAddOrDel.setOnClickListener(this);
		searchEt_ns.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				doScan(value);
				searchEt_ns.setText("");
				Utility.colseInputMethod(SParcel_NoSheetPage.this.ac, searchEt_ns);
			}
		});

		// lv
		adpNOSheet = new AdpNOSheet(ac);
		lv_ns.setAdapter(adpNOSheet);
		lv_ns.setEmptyView(contentV.findViewById(R.id.tvEmpty_ns));

		// 初始化
		tvResType_ns.setText(doorBean.getResTypeStr(ac) + ": ");
		tvDock_ns.setText(doorBean.resources_name);
		searchEt_ns.setScanMode();
	}

	public String getNOs_localScaned() {
		
		if(Utility.isEmpty(listNO))
			return "[]";
		
		JSONArray ja = new JSONArray();
		int len = listNO.size();
		for (int i = 0; i < len; i++) {
			SP_TNOrPltBean tmpNO = listNO.get(i);
			JSONObject jo = new JSONObject();
			//若不是本地数据
			if(!TextUtils.isEmpty(tmpNO.wms_order_type_id))
				continue;
			try {
				jo.put("scan_number", tmpNO.wms_pallet_number);
				jo.put("wms_scan_number_type", tmpNO.wms_scan_number_type);
				ja.put(jo);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return ja.toString();
	}

	public void startPage(List<SP_TNOrPltBean> listNO) {

		// 事件
		ac.showRightButton(R.drawable.menu_btn_style, "", this);

		// 复位
		this.listNO.clear();
		if (listNO != null)
			this.listNO.addAll(listNO);
		adpNOSheet.notifyDataSetChanged();
		refSumUI();

		// 初始化
		if (ac.selCarrier != null)
			tvCarrier_ns.setText(ac.selCarrier.carrier_name);
	}

	// ==================removePallet===============================

	private ImageButton btnAddOrDel;
	private RewriteBuilderDialog dlg_comfirmDel;

	private void setAddMode(boolean isAdd) {
		btnAddOrDel.setSelected(!isAdd);
	}

	/**
	 * 添加或移除托盘
	 * 
	 * @return
	 */
	private boolean isAdd() {
		return !btnAddOrDel.isSelected();
	}

	private void showDlg_confirmDel(int noIndex) {
		TTS.getInstance().speakAll("Confirm Remove!");

		// view
		View vContent = LayoutInflater.from(ac).inflate(
				R.layout.dlg_smallparcel_comfirmdel, null);
		TextView tvPlt_title = (TextView) vContent
				.findViewById(R.id.tvPlt_title);
		TextView tvPallet_dlg = (TextView) vContent
				.findViewById(R.id.tvPallet_dlg);
		final SearchEditText etPallet_dlg = (SearchEditText) vContent
				.findViewById(R.id.etPallet_dlg);

		// 初始化
		final SP_TNOrPltBean noBean = listNO.get(noIndex);
		tvPlt_title.setText(" ");
		tvPallet_dlg.setText(noBean.wms_pallet_number);
		etPallet_dlg.setHint("Please Scan the Number");

		// 事件
		etPallet_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				onComfirmDel(noBean, value);
				etPallet_dlg.setText("");
			}
		});

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(ac);
		bd.setTitle("Scan Again To Remove");
		bd.setContentView(vContent);
		bd.setPositiveButtonOnClickDismiss(false);
		bd.setPositiveButton(ac.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				onComfirmDel(noBean, etPallet_dlg.getEditableText().toString());
				etPallet_dlg.setText("");
			}
		});
		dlg_comfirmDel = bd.show();
	}

	private void onComfirmDel(SP_TNOrPltBean no, String palletNo_confirm) {
		if (TextUtils.isEmpty(palletNo_confirm)) {
			TTS.getInstance().speakAll_withToast("Empty", true);
			return;
		}
		// 不匹配
		if (!palletNo_confirm.equals(no.wms_pallet_number)) {
			TTS.getInstance().speakAll_withToast("Not Match!", true);
			return;
		}
		dlg_comfirmDel.dismiss();

		// 若为本地数据
		if (TextUtils.isEmpty(no.wms_order_type_id))
			removeBean(no);
		// 若为服务端数据
		else
			reqRemovePallet(no);
	}

	private void reqRemovePallet(final SP_TNOrPltBean no) {

		RequestParams p = new RequestParams();
		p.add("Method", "removeScanState");
		p.add("wms_scan_number_type", no.wms_scan_number_type + "");
		p.add("wms_order_type_id", no.wms_order_type_id);
		p.add("detail_id", taskBean.dlo_detail_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub

				// // 卸载
				// listNO.remove(no);
				// adpNOSheet.notifyDataSetChanged();
				// refSumUI();
				//
				// UIHelper.showToast(ac, "Success!");
				// TTS.getInstance().speakAll(
				// "Removed" + TTS.comma() + listNO.size());
				removeBean(no);
			}
		}.doPost(HttpUrlPath.androidSmallParcel, p, ac);
	}

	private void removeBean(SP_TNOrPltBean no) {
		// 卸载
		listNO.remove(no);
		adpNOSheet.notifyDataSetChanged();
		refSumUI();

		UIHelper.showToast(ac, ac.getString(R.string.sync_success));
		TTS.getInstance().speakAll("Removed" + TTS.comma() + listNO.size());
	}

	// =================================================

	private void doScan(String value) {
		// 校验
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Invalid Number!", true);
			return;
		}

		int no = SP_TNOrPltBean.getNOBean_ByNO(listNO, value);
		// load
		if (isAdd()) {
			// 已load
			if (no >= 0) {
				TTS.getInstance().speakAll_withToast("Repeat!", true);
				return;
			}
			reqLoad(value);
		}
		// unload
		else {
			// 未load
			if (no < 0) {
				TTS.getInstance().speakAll_withToast("Not Loaded!", true);
				return;
			}
			showDlg_confirmDel(no);
		}
	}

	private void reqLoad(String value) {

		// RequestParams p = new RequestParams();
		// p.add("method", "scanPalletOrTrackNo");
		// p.add("detail_id", ac.taskBean.dlo_detail_id);
		// p.add("scan_numer", value);
		// p.add("wms_scan_number_type", SP_TNOrPltBean.Type_Unknown + "");
		// new SimpleJSONUtil() {
		//
		// @Override
		// public void handReponseJson(JSONObject json) {
		// // TODO Auto-generated method stub
		// JSONObject ja = json.optJSONObject("data");
		// SP_TNOrPltBean b = new Gson().fromJson(ja.toString(),
		// SP_TNOrPltBean.class);
		// listNO.add(0, b);
		// adpNOSheet.notifyDataSetChanged();
		// refSumUI();
		//
		// TTS.getInstance().speakAll(listNO.size() + "");
		// }
		// }
		// // .setCancelable(false)
		// .doGet(HttpUrlPath.androidSmallParcel, p, ac);

		SP_TNOrPltBean b = new SP_TNOrPltBean();
		b.wms_pallet_number = value;
		b.wms_scan_number_type = SP_TNOrPltBean.Type_Unknown;
		listNO.add(0, b);
		adpNOSheet.notifyDataSetChanged();
		refSumUI();

		TTS.getInstance().speakAll(listNO.size() + "");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRight:
			showDlg_menu();
			break;
		case R.id.btnAddOrDel_ns: // 增减切换
			setAddMode(!isAdd());
			UIHelper.showToast(isAdd() ? ac.getString(R.string.tms_r_addmodel) : ac.getString(R.string.tms_r_removemodel));
			break;

		default:
			break;
		}
	}

	private void showDlg_menu() {
		final String[] list = new String[] { "Reload All",
				"Filter Small Parcel Orders", "Normal Close" };
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(ac);
		bd.setTitle(ac.getString(R.string.sync_operation));
		bd.setArrowItems(list, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				switch (position) {
				case 0: // reloadAll
					RewriteBuilderDialog.showSimpleDialog(ac, list[position]
							+ "?", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							reqReload();
						}
					});
					break;
				case 1: // filter
					ac.showDlg_filterOrders();
					break;
				case 2: // normalClose
					RewriteBuilderDialog.showSimpleDialog(ac, list[position]
							+ "?", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							ac.reqCloseTask();
						}
					});
					break;

				default:
					break;
				}
			}
		});
		bd.show();
	}

	private void reqReload() {
		RequestParams p = new RequestParams();
		p.add("method", "reloadOrder");
		p.add("detail_id", ac.taskBean.dlo_detail_id);
		p.add("is_keep_order", "0");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listNO.clear();
				adpNOSheet.notifyDataSetChanged();
				refSumUI();
			}
		}.doGet(HttpUrlPath.androidSmallParcel, p, ac);
	}

	private void refSumUI() {
		tvTotal_ns.setText(listNO.size() + "");
	}

	// ==================nested===============================

	/**
	 * 没单据的-smallParcel
	 * 
	 * @author 朱成
	 * @date 2015-5-12
	 */
	public class AdpNOSheet extends BaseAdapter {

		private Context ct;

		public AdpNOSheet(Context ct) {
			// TODO Auto-generated constructor stub
			this.ct = ct;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listNO == null ? 0 : listNO.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = LayoutInflater.from(ct).inflate(
						R.layout.smallparcel_nosheet_item, null);
				h = new Holder();
				h.tvNO = (TextView) convertView.findViewById(R.id.tvNO);

				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷ui
			SP_TNOrPltBean no = listNO.get(position);
			h.tvNO.setText(no.wms_pallet_number);

			return convertView;
		}
	}

	class Holder {
		TextView tvNO;
	}
}
