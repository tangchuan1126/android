package oso.ui.load_receive.do_task.load.smallparcel.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.load.smallparcel.bean.SP_OrderBean;
import oso.ui.load_receive.do_task.load.smallparcel.bean.SP_TNOrPltBean;
import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpSP_SelOrder extends BaseAdapter {
	
	private Context ct;
	private List<Point> listNO;
	private List<SP_OrderBean> listOrders;
	
	public AdpSP_SelOrder(Context ct,List<Point> listNO,List<SP_OrderBean> listOrders) {
		// TODO Auto-generated constructor stub
		this.ct=ct;
		this.listNO=listNO;
		this.listOrders=listOrders;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listNO==null?0:listNO.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		Holder h;
		if (convertView == null) {
			convertView=LayoutInflater.from(ct).inflate(R.layout.simple_list_item_1_sel, null);
			h = new Holder();
			h.tv=(TextView)convertView.findViewById(android.R.id.text1);
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();
		
		//刷ui
		Point pt=listNO.get(position);
		SP_OrderBean order=listOrders.get(pt.x);
		SP_TNOrPltBean no=order.getNOBean(pt.y);
		h.tv.setText("Order: "+order.order_numbers);
		h.tv.setSelected(no.isScanned());
		return convertView;
	}
	
	class Holder{
		TextView tv;
	}

}
