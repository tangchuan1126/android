package oso.ui.load_receive.do_task.receive.task.movement.bean;

import java.io.Serializable;

public class Rec_Movement_TaskBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1088937925046018832L;
	
	public int receipt_id;
	public String receipt_no;
	
	public String schedule_id;
	public String customer;
	public String title;
	public String create_user;
	public String create_time;

}
