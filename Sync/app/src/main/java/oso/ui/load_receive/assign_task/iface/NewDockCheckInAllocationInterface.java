package oso.ui.load_receive.assign_task.iface;

import support.common.bean.DockCheckInDoorBase;

public interface NewDockCheckInAllocationInterface {
	public void AssignLoader(DockCheckInDoorBase temp);
	public void ChangeDoor(DockCheckInDoorBase temp);
}
