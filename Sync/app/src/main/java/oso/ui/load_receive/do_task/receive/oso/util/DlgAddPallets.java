package oso.ui.load_receive.do_task.receive.oso.util;

import support.common.UIHelper;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

/**
 * 用于"添加/修改-无号的pallet"
 * 
 * @author 朱成
 * @date 2014-12-26
 */
/**
 * @author 朱成
 * @date 2014-12-26
 */
/**
 * @author 朱成
 * @date 2014-12-26
 */
public class DlgAddPallets extends Dialog implements View.OnClickListener{

	private TextView tvBarCnt;
	private TextView tvType_dlg;
	
	private View.OnClickListener onInnerClick;
	
	public boolean isChange=false;	//修改或新增

	public DlgAddPallets(Context context,View.OnClickListener onInnerClick,String initPalletTypeID,
			int initCnt) {
		super(context, R.style.signDialog);
		this.onInnerClick=onInnerClick;

		// 初始化
		Window window = getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setWindowAnimations(R.style.signAnim); // 添加动画
		setContentView(R.layout.dlg_addpallets);
		setCanceledOnTouchOutside(true);
		setCancelable(true);

		// 取view
		tvBarCnt = (TextView) findViewById(R.id.tvBarCnt);
		tvType_dlg=(TextView)findViewById(R.id.tvType_dlg);

		// 监听事件
		findViewById(R.id.btnMinus).setOnClickListener(onClick_plusMinus);
		findViewById(R.id.btnPlus).setOnClickListener(onClick_plusMinus);
		tvType_dlg.setOnClickListener(this);
		findViewById(R.id.btnSubmit_dlg).setOnClickListener(this);
		
		//初始化
		tvType_dlg.setText(initPalletTypeID);
		tvBarCnt.setText(initCnt+"");
		
		
	}
	
	public void setPalletType(String palletTypeID){
		tvType_dlg.setText(palletTypeID);
	}
	
	public String getPalletTypeID(){
		return tvType_dlg.getText().toString();
	}
	
	public int getPalletCnt() {
		return Integer.parseInt(tvBarCnt.getText().toString());
	}
	
	//=================================================
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvType_dlg:
			onInnerClick.onClick(v);
			break;
		case R.id.btnSubmit_dlg:
			onInnerClick.onClick(v);
			dismiss();
			break;

		default:
			break;
		}
	}

	private View.OnClickListener onClick_plusMinus = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int newCnt = 0;
			switch (v.getId()) {
			case R.id.btnPlus:
				newCnt = getPalletCnt() + 1;
				break;
			case R.id.btnMinus:
				newCnt = getPalletCnt() - 1;
				break;

			default:
				break;
			}

			// 最大99
			if (newCnt > 99) {
				UIHelper.showToast(getContext(), "Must less than 99!",
						Toast.LENGTH_SHORT).show();
				return;
			}
			// 修改时,最小为0
			if (isChange && newCnt < 0) {
				UIHelper.showToast(getContext(), "Must greater than 0!",
						Toast.LENGTH_SHORT).show();
				return;
			}
			// 新增时,最小为1
			if (!isChange && newCnt < 1) {
				UIHelper.showToast(getContext(), "Must greater than 1!",
						Toast.LENGTH_SHORT).show();
				return;
			}
			tvBarCnt.setText(newCnt + "");
		}
	};
	
	

	

}
