package oso.ui.load_receive.do_task.receive.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.task.movement.Rec_Movement_Assign;
import oso.ui.load_receive.do_task.receive.wms.Receive_ConfigDetailAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.VpWithTab;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteBuilderDialog.Builder;
import oso.widget.location.ChooseLocationActivity;
import oso.widget.location.ChooseStagingActivity;
import oso.widget.location.bean.LocationBean;
import oso.widget.location.bean.ZoonBean;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.PrintTool;
import support.common.print.PrintTool.OnPrintLs;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import declare.com.vvme.R.color;

/**
 * 收货-扫描任务-item详细
 * @author 朱成
 * @date 2015-3-11
 */
public class Rec_ScanTask_ItemAc extends BaseActivity {

    private final int Req_ScanBreakPlts=1;
    private final int Req_ScanTaskPallet = 2;
    
	
	private TextView tvItemID,tvExpQty,tvRelID,tvBreakQty,tvScanedQty,tvCountQty,tvStepTwo;
	
	private TextView tvLotNO,tvStagingLocation;
	
	private View loScanBreakPlts;

	private Rec_ItemBean itemBean;
	//Rec_ItemBean
	private SingleSelectBar ssb;
	
	private LinearLayout llPallet,llPalletCC;
	
	public static final int Mode_Pallet = 0;
	public static final int Mode_PalletCC = 1;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_rec_scantask_itemac, 0);
        applyParams();

        //v
        tvItemID=(TextView)findViewById(R.id.tvItemID);
        tvExpQty=(TextView)findViewById(R.id.tvExpQty);
        tvRelID=(TextView)findViewById(R.id.tvRelID);
        tvBreakQty=(TextView)findViewById(R.id.tvBreakQty);
        tvScanedQty=(TextView)findViewById(R.id.tvScanedQty);
        tvCountQty = (TextView) findViewById(R.id.tvCountQty);
        tvStepTwo = (TextView) findViewById(R.id.tvStep2);
        tvLotNO= (TextView) findViewById(R.id.tvLotNO);
        tvStagingLocation= (TextView) findViewById(R.id.tvStagingLocation);
        loScanBreakPlts = findViewById(R.id.loScanBreakPlts);
        
        llPallet = (LinearLayout) findViewById(R.id.llPallet);
        llPalletCC = (LinearLayout) findViewById(R.id.llPalletCC);
        
		initSingleSelectBar();

		setTitleString("Receipt: " + itemBean.receipt_no);
        tvRelID.setText(itemBean.receipt_no);
        tvItemID.setText(itemBean.item_id);
        tvExpQty.setText(itemBean.expected_qty+"");
        tvCountQty.setText(itemBean.getQtyCout());
        tvScanedQty.setText(itemBean.getQtySN() +" / " + itemBean.expected_qty);
        if(TextUtils.isEmpty(itemBean.lot_no)){
        	tvLotNO.setText("");
        	tvLotNO.setHint("NA");
        }else{
        	tvLotNO.setText(itemBean.lot_no);
        }
        tvStagingLocation.setText(itemBean.location);
        
        showRightButton(R.drawable.print_all_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog_Print();
			}
		});
        
        refTvBreakQty();
        
	}
	private void initSingleSelectBar() {
		ssb = (SingleSelectBar) findViewById(R.id.ssb);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Inbound", 0));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("New Config.", 1));
		ssb.setUserDefineClickItems(clickItems);
		ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case Mode_Pallet:
					llPallet.setVisibility(View.VISIBLE);
					llPalletCC.setVisibility(View.GONE);
					
					showPallet();
					
					break;
				case Mode_PalletCC:
					llPallet.setVisibility(View.GONE);
					llPalletCC.setVisibility(View.VISIBLE);
					
					showPalletCC();
					break;

				default:
					break;
				}
			}
		});
		ssb.userDefineSelectIndexExcuteClick(0);
	}
	public int getAddMode() {
		return ssb.getCurrentSelectItem().b;
	}
	
	private void showPallet(){
		llPallet.removeAllViews();
		if(listPallet != null){
			for(final Rec_PalletConfigBean b : listPallet){
				
				View v = View.inflate(mActivity, R.layout.item_cc_configpallets, null);
				
				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						showMenu_item(b);
					}
				});
				
				Holder h = new Holder();
				
				h.tvType = (TextView) v.findViewById(R.id.tvType);
				h.tvTotal = (TextView) v.findViewById(R.id.tvTotal);
				h.tvConfig = (TextView) v.findViewById(R.id.tvConfig);
				h.tvPalletType = (TextView) v.findViewById(R.id.tvPalletType);
				h.tvKeepInbound = (TextView) v.findViewById(R.id.tvKeepInbound);
				h.loConfig = (LinearLayout) v.findViewById(R.id.loConfig);
				h.tvKeepInbound2 = (TextView) v.findViewById(R.id.tvKeepInbound2);
				h.tvReceiptNo = (TextView) v.findViewById(R.id.tvReceiptNO);
				h.tvPrint = (TextView) v.findViewById(R.id.tvPrint);
				h.tvPrint2 = (TextView) v.findViewById(R.id.tvPrint2);
				h.tvNumber = (TextView) v.findViewById(R.id.tvNumber);
				h.tvShipTo = (TextView) v.findViewById(R.id.tvShipTo);
				
				h.loReceiptNO = (LinearLayout) v.findViewById(R.id.loReceiptNO);
				h.loType = (LinearLayout) v.findViewById(R.id.loType);
				h.loShipTo = (LinearLayout) v.findViewById(R.id.loShipTo);
				
				h.tvPalletType.setText(b.pallet_name);
				h.tvNumber.setText("× "+b.plate_number);
				
				if(b.goods_qty == 0){
					h.tvTotal.setText(b.getTotalQty()+"");
				}else{
					h.tvTotal.setText(b.goods_qty+"");
				}
				
				h.tvPrint.setVisibility(b.isPrinted() ? View.VISIBLE : View.GONE);
				h.tvPrint2.setVisibility(b.isPrinted() ? View.VISIBLE : View.GONE);
				
				if(!b.isCLP()){
					
					if(b.is_partial == 1){
						h.loReceiptNO.setVisibility(View.VISIBLE);
						h.loType.setVisibility(View.GONE);
						h.loShipTo.setVisibility(View.GONE);
						
						h.tvConfig.setText("");
						h.tvConfig.setHint("NA");
						
						h.tvReceiptNo.setText(b.con_id);
						h.tvKeepInbound2.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
						if(!b.hasConfig()){
							h.tvTotal.setText(b.getTotalQty()+"");
						}
					}else{
						h.loReceiptNO.setVisibility(View.GONE);
						h.loType.setVisibility(View.VISIBLE);
						h.loShipTo.setVisibility(View.GONE);
						
						h.tvType.setText("TLP");
						h.tvConfig.setText(b.getConfigDesc());
						h.tvKeepInbound.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
						
					}
					
				}else{
					
					h.loReceiptNO.setVisibility(View.GONE);
					h.loType.setVisibility(View.VISIBLE);
					h.loShipTo.setVisibility(TextUtils.isEmpty(b.ship_to_name) ? View.GONE : View.VISIBLE);
					
					h.tvConfig.setText(b.getConfigDesc());
					
					h.tvType.setText("CLP");
					h.tvKeepInbound.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
					h.tvShipTo.setText(b.ship_to_name);
					
				}
				
				llPallet.addView(v);
			}
		}
	}
	
	private void showPalletCC(){
		llPalletCC.removeAllViews();
		if(listPalletCC != null){
			for(final Rec_PalletConfigBean b : listPalletCC){
				
				View v = View.inflate(mActivity, R.layout.item_cc_configpallets, null);
				
				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						showMenu_item(b);
					}
				});
				
				Holder h = new Holder();
				
				h.tvType = (TextView) v.findViewById(R.id.tvType);
				h.tvTotal = (TextView) v.findViewById(R.id.tvTotal);
				h.tvConfig = (TextView) v.findViewById(R.id.tvConfig);
				h.tvPalletType = (TextView) v.findViewById(R.id.tvPalletType);
				h.tvKeepInbound = (TextView) v.findViewById(R.id.tvKeepInbound);
				h.loConfig = (LinearLayout) v.findViewById(R.id.loConfig);
				h.tvKeepInbound2 = (TextView) v.findViewById(R.id.tvKeepInbound2);
				h.tvReceiptNo = (TextView) v.findViewById(R.id.tvReceiptNO);
				h.tvPrint = (TextView) v.findViewById(R.id.tvPrint);
				h.tvPrint2 = (TextView) v.findViewById(R.id.tvPrint2);
				h.tvNumber = (TextView) v.findViewById(R.id.tvNumber);
				h.tvShipTo = (TextView) v.findViewById(R.id.tvShipTo);
				
				h.loReceiptNO = (LinearLayout) v.findViewById(R.id.loReceiptNO);
				h.loType = (LinearLayout) v.findViewById(R.id.loType);
				h.loShipTo = (LinearLayout) v.findViewById(R.id.loShipTo);
				
				h.tvPalletType.setText(b.pallet_name);
				h.tvNumber.setText("× "+b.plate_number);
				
				if(b.goods_qty == 0){
					h.tvTotal.setText(b.getTotalQty()+"");
				}else{
					h.tvTotal.setText(b.goods_qty+"");
				}
				
				h.tvPrint.setVisibility(b.isPrinted() ? View.VISIBLE : View.GONE);
				h.tvPrint2.setVisibility(b.isPrinted() ? View.VISIBLE : View.GONE);
				
				if(!b.isCLP()){
					
					if(b.is_partial == 1){
						h.loReceiptNO.setVisibility(View.VISIBLE);
						h.loType.setVisibility(View.GONE);
						h.loShipTo.setVisibility(View.GONE);
						h.tvConfig.setText("");
						h.tvConfig.setHint("NA");
						
						h.tvReceiptNo.setText(b.con_id);
						h.tvKeepInbound2.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
						if(!b.hasConfig()){
							h.tvTotal.setText(b.getTotalQty()+"");
						}
					}else{
						h.loReceiptNO.setVisibility(View.GONE);
						h.loType.setVisibility(View.VISIBLE);
						h.loShipTo.setVisibility(View.GONE);
						
						h.tvType.setText("TLP");
						h.tvConfig.setText(b.getConfigDesc());
						h.tvKeepInbound.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
						
					}
					
				}else{
					
					h.loReceiptNO.setVisibility(View.GONE);
					h.loType.setVisibility(View.VISIBLE);
					h.loShipTo.setVisibility(TextUtils.isEmpty(b.ship_to_name) ? View.GONE : View.VISIBLE);
					
					h.tvConfig.setText(b.getConfigDesc());
					
					h.tvType.setText("CLP");
					h.tvKeepInbound.setText(b.keep_inbound == 1 ? "(Keep Inbound)" : "");
					h.tvShipTo.setText(b.ship_to_name);
					
				}
				
				
				llPalletCC.addView(v);
			}
		}
		
	}
	
    private void refTvBreakQty(){
    	if(itemBean.clp_expected_qty == 0)
    	{
    		loScanBreakPlts.setVisibility(View.GONE);
    		tvStepTwo.setText("Step One");
    	}
    	
        tvBreakQty.setText(itemBean.already_breark_qty+" / "+ itemBean.clp_expected_qty);
        
    }
    
    public final static int Req_ScanTask_ViewPlts=2000;
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_CANCELED)
            return;

        switch (requestCode){
            case Req_ScanBreakPlts:
                int breakQty=data.getIntExtra("breakQty",0);
                boolean issteptwo =data.getBooleanExtra("steptwo", false);
                itemBean.go_to_scan = issteptwo;
                itemBean.already_breark_qty=breakQty;
               
                reqBreakRefresh();
                break;
            case Req_ScanTaskPallet:
            	boolean finishScan = data.getBooleanExtra("FinishScan",false);
            	if(finishScan){
            		finish();
            		return;
            	}
            	refScanTask();
            	break;
            case Req_ScanTask_ViewPlts:
            	refresh();
            	break;
        }
    }
    
    private void reqBreakRefresh(){
    	RequestParams p = new RequestParams();
    	p.add("line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				
				//---------Create Pallet
				JSONArray jPallet = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> pallet = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPallet)) {
					pallet = new Gson().fromJson(jPallet.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//---------Create Pallet
				JSONArray jPalletCC = json.optJSONArray("pallets_cc");
				List<Rec_PalletConfigBean> palletCC = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					palletCC = new Gson().fromJson(jPalletCC.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//----------Break Partial-------
				JSONArray jBreakPartial = json.optJSONArray("break_partial");
				List<Rec_PalletConfigBean> listBreakPartial = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listBreakPartial = new Gson().fromJson(jBreakPartial.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
                
                listPallet.clear();
                listPallet.addAll(pallet);
                listPalletCC.clear();
                listPalletCC.addAll(palletCC);
                listPalletCC.addAll(listBreakPartial);
                
                if(getAddMode() == Mode_Pallet){
                	showPallet();
                }else{
                	showPalletCC();
                }
                
                //判断是否需要提示打印
                if(itemBean.already_breark_qty == itemBean.clp_expected_qty){
                	if(Rec_PalletConfigBean.isPrint(listPalletCC)){
                		RewriteBuilderDialog.showSimpleDialog(mActivity, "Print New Config.?", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								printTool.showDlg_printers(newConfigAll);
							}
						});
                		
                	}
                }
                refTvBreakQty();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquireReceiptLinesByLineId", p, mActivity);
    }
    
    private void refresh() {
    	RequestParams p = new RequestParams();
    	p.add("line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				
				//---------Create Pallet
				JSONArray jPallet = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> pallet = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPallet)) {
					pallet = new Gson().fromJson(jPallet.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//---------Create Pallet
				JSONArray jPalletCC = json.optJSONArray("pallets_cc");
				List<Rec_PalletConfigBean> palletCC = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					palletCC = new Gson().fromJson(jPalletCC.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//----------Break Partial-------
				JSONArray jBreakPartial = json.optJSONArray("break_partial");
				List<Rec_PalletConfigBean> listBreakPartial = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listBreakPartial = new Gson().fromJson(jBreakPartial.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
                
                listPallet.clear();
                listPallet.addAll(pallet);
                listPalletCC.clear();
                listPalletCC.addAll(palletCC);
                listPalletCC.addAll(listBreakPartial);
                
                if(getAddMode() == Mode_Pallet){
                	showPallet();
                }else{
                	showPalletCC();
                }
                refTvBreakQty();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquireReceiptLinesByLineId", p, mActivity);
	}
    
    private void showMenu_item(final Rec_PalletConfigBean bean) {
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));

        String[] strs=null;
        strs=new String[] { "View Pallets","Print Pallets"};

		bd.setArrowItems(strs, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
				// TODO Auto-generated method stub
				switch (position_dlg) {
				case 0: // 查看pallets
					reqConfigPlts(bean);
					break;
				case 1: // 打印
					if (bean.isPrinted()) {
						RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
						String msg = String.format("These Labels has been printed by %s. Reprint?", bean.print_user_name);
						builder.setMessage(msg);
						builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
//								config_toPrint = listConfigs_cc.get(position);
								config_toPrint=bean;
								printTool.showDlg_printers(printLs_pConfig);
							}
						});
						builder.create().show();
					} else {
//						config_toPrint = listConfigs_cc.get(position);
						config_toPrint=bean;
						printTool.showDlg_printers(printLs_pConfig);
					}
					break;
				default:
					break;
				}
			}
		});
		bd.show();
	}
    
    private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs_pConfig = new PrintTool.OnPrintLs() {

		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintConfig(printServerID);
		}
	};
	
	private Rec_PalletConfigBean config_toPrint; // 右键菜单-选中的bean
	
	private void reqPrintConfig(long printServerID) {

		final Rec_PalletConfigBean tmpConfig = config_toPrint;
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_ConfigIDs,  tmpConfig.container_config_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// debug
				tmpConfig.print_user_name = StoredData.getUsername();
				// tmpConfig.print_date=new
				// SimpleDateFormat("yyyy-mm-dd ").format(new
				// Date().toString());
//				adp.notifyDataSetChanged();
//				adp_cc.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, mActivity);
	}
    
    private void reqConfigPlts(final Rec_PalletConfigBean bean) {

		RequestParams p = new RequestParams();
		p.add("receipt_line_id", bean.receipt_line_id);
		p.add("container_config_id", bean.container_config_id);
		p.add("process", bean.process+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				// Intent in = new Intent(mActivity,
				// Receive_ConfigDetailAc.class);
				// startActivity(in);
                String strJson="pallets";
				JSONArray jaPlts = json.optJSONArray(strJson);
				if (jaPlts == null || jaPlts.length() == 0) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No Pallets!");
					return;
				}

				List<Rec_PalletBean> list = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
				}.getType());

				Intent in = new Intent(mActivity, Receive_ConfigDetailAc.class);
				Receive_ConfigDetailAc.initParams(in, list, itemBean.receipt_line_id, bean.getQtyPerPlt(),false);
				//startActivity(in);
				startActivityForResult(in, Req_ScanTask_ViewPlts);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletsInfoByLine", p, this);

	}
    
    private void refScanTask(){
    	RequestParams p = new RequestParams();
		p.add("line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				itemBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_ItemBean.class);
				
				tvScanedQty.setText(itemBean.getQtySN() +" / " + itemBean.expected_qty);
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/acquireReceiptLinesByLineId",p,mActivity);
    }

    public void onClick_(View v) {
		switch (v.getId()) {
		case R.id.loScanBreakPlts:			//扫breakfrom
            reqBreakPlts();
			break;
		case R.id.loStartScan:				//开始扫描
            //若未break完
			if(!checkBreakFinish()) return;
			if(!checkNewConfigPrint()) return;
			if(!checkStartScan()) return;
            //已开始
            reqLinePlts(false);
			break;

		default:
			break;
		}
	}

    private boolean checkBreakFinish(){
    	if(!itemBean.go_to_scan) {
			if(itemBean.already_breark_qty!=itemBean.clp_expected_qty){
				RewriteBuilderDialog.showSimpleDialog(mActivity, "Please scan pallets to break first!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                reqBreakPlts();
                            }
                        });
				return false;
			}
        }
    	return true;
    }
    private boolean checkNewConfigPrint(){
    	if(Rec_PalletConfigBean.isPrint(listPalletCC)){
    		RewriteBuilderDialog.showSimpleDialog(mActivity, "Print New Config.", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					printTool.showDlg_printers(newConfigAll);
				}
			});
    		return false;
    	}
    	return true;
    }
    private boolean checkStartScan(){
    	// 未开始扫描
        if (!itemBean.hasStartScan()) {
            RewriteBuilderDialog.showSimpleDialog(mActivity,
                    "Start Scan Serial Numbers?",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            reqLinePlts(true);
                        }
                    });
            return false;
        }
        return true;
    }
    private void reqLinePlts(final boolean start) {

        final RequestParams params = new RequestParams();
        params.add("receipt_line_id", itemBean.receipt_line_id);
        // Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
        params.add("container_config_id", "0");
        params.add("is_start_scan", start ? "1" : "0");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // 打开rn(如:不需扫sn的rn,start后 状态需变)
                //注:1>rn关闭时 不能再进line(故此处可直接改为open)
                //debug
//                recBean.status = Rec_ReceiveBean.RNStatus_Open;
//                refBtnFinish();

                JSONArray jaPlts = json.optJSONArray("pallets");
                // plts
                String strPlts = jaPlts == null ? "[]" : jaPlts.toString();
                List<Rec_PalletBean> listPlts = new Gson().fromJson(strPlts,
                        new TypeToken<List<Rec_PalletBean>>() {
                        }.getType());

                // dmgPlts
                JSONArray jaDmg = json.optJSONArray("damaged");
                List<Rec_PalletBean> listDmg = null;
                if (!Utility.isEmpty(jaDmg)) {
                    listDmg = new Gson().fromJson(jaDmg.toString(),
                            new TypeToken<List<Rec_PalletBean>>() {
                            }.getType());
                    listPlts.addAll(listDmg);
                }
                
                List<Rec_PalletType> listTypes = new Gson().fromJson(json.optJSONArray("contypes").toString(), new TypeToken<List<Rec_PalletType>>(){}.getType());
				

                // debug,刷新状态
                // try {
                // recBean.status =
                // json.optJSONObject("datas").optJSONObject("receipt").optString("status");
                // } catch (Exception e) {
                // // TODO: handle exception
                // e.printStackTrace();
                // }

                Intent in = new Intent(mActivity, Rec_ScanTask_PalletsAc.class);
                Rec_ReceiveBean recBean=new Rec_ReceiveBean();
                recBean.curItemBean=itemBean;
                recBean.receipt_no=itemBean.receipt_no;
                recBean.receipt_id=itemBean.receipt_id;
                Rec_ScanTask_PalletsAc.initParams(in, listPlts, listTypes, recBean,isFromTask);
                startActivityForResult(in,Req_ScanTaskPallet);
            }

        }.setCancelable(false).doGet(HttpUrlPath.acquirePalletsInfoByLine, params, this);
    }

    private void reqBreakPlts(){
        RequestParams params = new RequestParams();
        params.put("receipt_line_id", itemBean.receipt_line_id);
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
            	int is_start_scan = json.optInt("is_start_scan");
                List<Rec_PalletBean> listTlps = new Gson().fromJson(json.optJSONArray("pallets").toString(), new TypeToken<List<Rec_PalletBean>>() {
                }.getType());
                List<Rec_PalletBean> listBreakPlts = new Gson().fromJson(json.optJSONArray("breakpallets").toString(), new TypeToken<List<Rec_PalletBean>>() {
                }.getType());
                Intent in=new Intent(mActivity,Rec_ScanTask_BreakAc.class);
                Rec_ScanTask_BreakAc.initParams(in, itemBean,listTlps,listBreakPlts,is_start_scan);
                startActivityForResult(in, Req_ScanBreakPlts);
            }
        }.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletsInfoByLineForBreak", params, mActivity);
    }
    
    
    private void showDialog_Print(){
    	RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
    	
    	String[] strs = new String[]{"Print Inbound","Print New Config."};
    	
    	builder.setArrowItems(strs, new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if(position == 0){
					printTool.showDlg_printers(inboundAll);
				}else{
					printTool.showDlg_printers(newConfigAll);
				}
				
			}
		});
    	
    	builder.show();
    }
    
    OnPrintLs inboundAll = new OnPrintLs() {
		@Override
		public void onPrint(long printServerID) {
            
            RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_Inbound, itemBean.receipt_line_id);
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					String name = StoredData.getUsername();
					for (Rec_PalletConfigBean bean : listPallet) {
						bean.print_user_name = name;
					}
					showPallet();
					UIHelper.showToast(getString(R.string.sync_print_success));
				}
			}.doGet(NetInterface.recPrintLP_url(), p, mActivity);

		}
	};
	  OnPrintLs newConfigAll = new OnPrintLs() {
			@Override
			public void onPrint(long printServerID) {
	            
	            RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_NewConfig, itemBean.receipt_line_id);
				new SimpleJSONUtil() {

					@Override
					public void handReponseJson(JSONObject json) {
						String name = StoredData.getUsername();
						for (Rec_PalletConfigBean bean : listPalletCC) {
							bean.print_user_name = name;
						}
						showPalletCC();
						UIHelper.showToast(getString(R.string.sync_print_success));
					}
				}.doGet(NetInterface.recPrintLP_url(), p, mActivity);

			}
		};
	
	/**
     * 获取-选中的plts的ids,","分割
     * @return
     */
    public static String getPltIDs(List<Rec_PalletConfigBean> list){
        if(Utility.isEmpty(list))
            return "";

        String ret="";
        for (int i = 0; i < list.size(); i++) {
            Rec_PalletConfigBean b=list.get(i);
            ret=ret+b.container_config_id+",";
        }
        if(!TextUtils.isEmpty(ret)){
            //去掉尾部的","
            ret=ret.substring(0, ret.length()-1);
        }
        return ret;
    }
	
	
    private boolean isFromTask = true;                 //true:来自-扫描任务,false:来自搜sn
    private List<Rec_PalletConfigBean> listPallet;
    private List<Rec_PalletConfigBean> listPalletCC;
    private List<Rec_PalletConfigBean> listPartial;
	//=========传参===============================

	public static void initParams(Intent in,Rec_ItemBean itemBean, boolean isFromTask,List<Rec_PalletConfigBean> pallet,List<Rec_PalletConfigBean> palletCC,List<Rec_PalletConfigBean> partialBreak) {
        in.putExtra("itemBean", (Serializable) itemBean);
        in.putExtra("isFromTask",isFromTask);
        in.putExtra("listPallet", (Serializable) pallet);
        in.putExtra("listPalletCC", (Serializable) palletCC);
        in.putExtra("listPartial", (Serializable) partialBreak);
	}

	private void applyParams() {
        Bundle params = getIntent().getExtras();
        itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
        isFromTask=params.getBoolean("isFromTask");
        listPallet = (List<Rec_PalletConfigBean>) params.getSerializable("listPallet");
        listPalletCC = (List<Rec_PalletConfigBean>) params.getSerializable("listPalletCC");
        listPartial = (List<Rec_PalletConfigBean>) params.getSerializable("listPartial");
        
        if(listPalletCC != null && !Utility.isEmpty(listPartial)){
        	listPalletCC.addAll(listPartial);
        }
	}
	
	
	private class Holder {
		
		TextView tvType,tvTotal,tvConfig,tvPalletType,tvKeepInbound,tvKeepInbound2,tvReceiptNo,tvPrint,tvPrint2,tvNumber,tvShipTo;
		
		LinearLayout loType,loConfig,loReceiptNO,loShipTo;
	}
	
}