package oso.ui.load_receive.print;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.ReceiptTicketModel;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.DockCheckInDoorForLoadListBase;
import support.common.print.OnInnerClickListener;
import support.key.EntryDetailNumberTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

@SuppressLint("SetJavaScriptEnabled")
public class DockCtnBolWebViewActivity extends BaseActivity {

	private WebView show_webview;
	private FrameLayout loadingLayout;
	private TextView psTv;
	private String printer_server_name;
	private String printer_server_id;
	private	ReceiptTicketModel receiptTicketModel;
	private	DockCheckInDoorForLoadListBase newDockCheckInDoorForLoadListBase;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_ctnbol_webview_layout,0);
		getFromActivityData();
		initView();
		setViewOnclick();
		setViewData();
	}
	/**
	 * @Description:获取从上一个页面获取到的数据
	 * @param
	 */
	public void getFromActivityData() {
		Intent intent = getIntent();
		receiptTicketModel = (ReceiptTicketModel) intent.getSerializableExtra("receiptTicketModel");
		newDockCheckInDoorForLoadListBase = (DockCheckInDoorForLoadListBase) 
				intent.getSerializableExtra("newDockCheckInDoorForLoadListBase");
		if(receiptTicketModel!=null)
			printer_server_id = receiptTicketModel.getDefaultNum();
	}
	/**
	 * @Description:初始化当前页面上的View
	 * @param
	 */
	private void initView() {
		psTv = (TextView) findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) findViewById(R.id.loadingLayout);
		show_webview = (WebView) findViewById(R.id.show_webview);
	}
	/**
	 * @Description:初始化View的点击事件
	 * @param
	 */
	private void setViewOnclick() {
		imgBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
		btnRight.setVisibility(View.VISIBLE);
		btnRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPrinters();
			}
		});
	}
	/**
	 * @Description:初始化当前页面的webView控件
	 * @param
	 */
	private void setViewData() {
		setTitleString("DockCheckIn");		
		show_webview.setInitialScale(120);
		show_webview.getSettings().setJavaScriptEnabled(true);
		WebSettings settings = show_webview.getSettings();
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);
		show_webview.setInitialScale(180);
		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});
		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				UIHelper.showToast(DockCtnBolWebViewActivity.this, "Oh no! " + description,
						Toast.LENGTH_SHORT).show();
			}
		});
		StringBuilder url = new StringBuilder(HttpUrlPath.basePath);
					  url.append(receiptTicketModel.getPathForCtn());
					  url.append("?entryId=").append(receiptTicketModel.getEntryId());
					  url.append("&door_name=").append(receiptTicketModel.getDoor_name());
					  url.append("&CompanyID=").append(newDockCheckInDoorForLoadListBase.getCompany_id());
					  url.append("&CustomerID=").append(newDockCheckInDoorForLoadListBase.getCustomer_id());
					  url.append("&out_seal=").append(receiptTicketModel.getOut_seal());
					  url.append("&number=").append(newDockCheckInDoorForLoadListBase.getId());
					  url.append("&number_type=").append(newDockCheckInDoorForLoadListBase.getNumber_type());
					  url.append((newDockCheckInDoorForLoadListBase.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_CTN) 
							? "&ctnr="+ newDockCheckInDoorForLoadListBase.getId()
							: "&bol=" + newDockCheckInDoorForLoadListBase.getId());
					  url.append(Utility.getSharedPreferencesUrlValue(DockCtnBolWebViewActivity.this, "adid"));
		Utility.synCookies(mActivity, url.toString(), SimpleJSONUtil.getCookie());
		show_webview.loadUrl(url.toString());
	}
	
	
	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;
	/**
	 * @Description:弹出打印列表并确认打印
	 * @param
	 */
	private void showPrinters() {
		if (Utility.isNullForList(receiptTicketModel.getParsePrintServer())) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}
		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) (DockCtnBolWebViewActivity.this)
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,
				null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(
				DockCtnBolWebViewActivity.this,
				receiptTicketModel.getParsePrintServer(), onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (receiptTicketModel.getParsePrintServer().size() < 3) ? dm.heightPixels
				/ 7 * receiptTicketModel.getParsePrintServer().size()
				: (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (receiptTicketModel.getParsePrintServer().size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getDefaultPrint_index());
		// 初始-滑至可见
		listView.setSelection(getDefaultPrint_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (!StringUtil.isNullOfStr(printer_server_id)
								&& !printer_server_id.equals("0")) {
							submitData_ctnbol();
							dialog.dismiss();
						} else {
							UIHelper.showToast(getString(R.string.sync_select_printserver));
						}

					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		builder.create().show();
	}
	
	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			printer_server_id = receiptTicketModel.getParsePrintServer()
					.get(position).getPrinter_server_id();
			receiptTicketModel.setDefaultNum(printer_server_id);
			printer_server_name = receiptTicketModel.getParsePrintServer()
					.get(position).getPrinter_server_name();
			adpPrinters.setCurItem(position);
		};
	};

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getDefaultPrint_index() {
		if (receiptTicketModel.getParsePrintServer() == null)
			return -1;

		for (int i = 0; i < receiptTicketModel.getParsePrintServer().size(); i++) {
			String tmpId = receiptTicketModel.getParsePrintServer().get(i)
					.getPrinter_server_id();
			if (receiptTicketModel.getDefaultNum().equals(tmpId))
				return i;
		}
		return -1;
	}
	/**
	 * @Description:提交打印方法
	 * @param
	 */
	private void submitData_ctnbol() {
		RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params);
		params.add("Method", HttpPostMethod.ReceiptsTicketPrintTask);
		params.add("who", printer_server_name);
		params.add("print_server_id", printer_server_id);
		params.add("path", receiptTicketModel.getPathForCtn() + "");
		params.add("entryId", receiptTicketModel.getEntryId() + "");
		params.add("door_name", receiptTicketModel.getDoor_name() + "");
		params.add("number_type", String.valueOf(newDockCheckInDoorForLoadListBase.getNumber_type()));
		params.add("number", newDockCheckInDoorForLoadListBase.getId());
		if (receiptTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_CTN) {
			params.add("type", "ctnr");
		} else {
			params.add("type", "bol");
		}

		params.add("type_value", newDockCheckInDoorForLoadListBase.getId() + "");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(DockCtnBolWebViewActivity.this, getString(R.string.sync_success),
						Toast.LENGTH_SHORT).show();
			}
		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params,
				DockCtnBolWebViewActivity.this);
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}
}
