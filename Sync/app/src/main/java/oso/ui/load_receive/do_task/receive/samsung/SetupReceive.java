package oso.ui.load_receive.do_task.receive.samsung;

public class SetupReceive {
	
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getBox_qty() {
		return box_qty;
	}
	public void setBox_qty(String box_qty) {
		this.box_qty = box_qty;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public boolean isNABox = false;
	public boolean isNAWeight = false;
	private String area = "NA";
	private String box_qty = "NA";
	private String weight = "NA";
	
}
