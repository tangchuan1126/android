package oso.ui.load_receive.do_task.load.smallparcel.bean;

import java.util.List;

import utility.Utility;
import android.graphics.Point;
import android.text.TextUtils;

public class SP_TNOrPltBean {
	
//	public final static int Type_Plt=1;
	public final static int Type_TN=2;
	public final static int Type_Unknown=3;
	
	public String wms_order_type_id;
	public String wms_pallet_number;
	public int wms_scan_number_type;			//取Type_x
	
	public String scan_adid;
	
//	1.本地标load时 标isLocal
//	1>若已经load 则不再标
//	2.unload时 isLocal=false
	public boolean isScaned_local=false;		
	
//	public boolean isTN(){
//		return wms_scan_number_type==Type_TN;
//	}
	
	public boolean isScanned(){
		return !TextUtils.isEmpty(scan_adid);
	}
	
	//==================static===============================

	public static int getNOBean_ByNO(List<SP_TNOrPltBean> listTNs,String no){
		if(Utility.isEmpty(listTNs) || TextUtils.isEmpty(no))
			return -1;
		for (int i = 0; i < listTNs.size(); i++) {
			if(no.equals(listTNs.get(i).wms_pallet_number))
				return i;
		}
		return -1;
	}
	
	public static SP_TNOrPltBean getTN(List<SP_OrderBean> listOrders,Point pt){
		try {
			return listOrders.get(pt.x).pallets.get(pt.y);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
}
