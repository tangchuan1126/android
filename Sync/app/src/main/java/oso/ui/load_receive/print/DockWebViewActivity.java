package oso.ui.load_receive.print;

import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.Utility;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

public class DockWebViewActivity extends Activity {

	private WebView show_webview;
	private Button close_activity;
	private FrameLayout loadingLayout;
	private TextView psTv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_webview_layout);
		initView();
	}
	
	private void initView(){
		psTv = (TextView) findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) findViewById(R.id.loadingLayout);
		show_webview = (WebView) findViewById(R.id.show_webview);
		show_webview.getSettings().setJavaScriptEnabled(true);

		final Activity activity = this;
//		show_webview.setWebChromeClient(new WebChromeClient() {
//		  public void onProgressChanged(WebView view, int progress) {
//		     // Activities and WebViews measure progress with different scales.
//		     // The progress meter will automatically disappear when we reach 100%
//		     activity.setProgress(progress * 1000);
//		  }
//		});
		
		WebSettings settings = show_webview.getSettings(); 
	    settings.setUseWideViewPort(true); 
	    settings.setLoadWithOverviewMode(true); 
	    settings.setSupportZoom(true);
	    settings.setBuiltInZoomControls(true);
		
		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});

		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				UIHelper.showToast(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
			}
		});
		Utility.synCookies(this, "http://192.168.1.36:8080/a/jennifer.html", SimpleJSONUtil.getCookie());
		show_webview.loadUrl("http://192.168.1.36:8080/a/jennifer.html");
		
	    close_activity = (Button) findViewById(R.id.close_activity);
	    close_activity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
	}
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}
}
