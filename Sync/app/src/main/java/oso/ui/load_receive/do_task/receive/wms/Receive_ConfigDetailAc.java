package oso.ui.load_receive.do_task.receive.wms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.MultiSelTool;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 一种配置的pallet列表
 * 
 * @author 朱成
 * @date 2015-3-11
 */
public class Receive_ConfigDetailAc extends BaseActivity implements OnClickListener {

	private ListView lvPallets;
	private List<Rec_PalletBean> listPlts;

	private CheckBox cbAll;
	private TextView tvQtyPerPlt;
	
	private Button btnDel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_rec_configdetail, 0);
		setTitleString("View Pallets");
		applyParams();

		// 取v
		lvPallets = (ListView) findViewById(R.id.lvPallets);
		cbAll = (CheckBox) findViewById(R.id.cbAll);
		tvQtyPerPlt=(TextView)findViewById(R.id.tvQtyPerPlt);
		btnDel= (Button) findViewById(R.id.btnDel);
		btnDel.setVisibility(isDelete ? View.VISIBLE : View.GONE);

		// 事件
		btnDel.setOnClickListener(this);
		findViewById(R.id.btnPrint).setOnClickListener(this);
		// cbAll.setOnClickListener(this);

		// lv
		lvPallets.setAdapter(adp);

		// 初始化
		mulSel.init(cbAll, adp);
		tvQtyPerPlt.setText(qtyPerPlt+"");
	}

	private List<Rec_PalletBean> tmpPlts; // 批处理时使用

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnDel: // 删除
			tmpPlts = getPlts_sel();
			if (Utility.isEmpty(tmpPlts)) {
				UIHelper.showToast(getString(R.string.sync_select_pallet));
				return;
			}

			StringBuilder delSb = new StringBuilder();
			delSb.append(getPrintInfo(tmpPlts));
			delSb.append(getString(R.string.sync_delete_notify));
			RewriteBuilderDialog.showSimpleDialog(mActivity, delSb.toString(), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					reqDelPlts(tmpPlts);
				}
			});
			break;

		case R.id.btnPrint: // 打印plt
			tmpPlts = getPlts_sel();
			if (Utility.isEmpty(tmpPlts)) {
				UIHelper.showToast(getString(R.string.sync_select_pallet));
				return;
			}
			
			String printUsers=Rec_PalletBean.getPrintUsers(tmpPlts);
			String tip="";
			//已有人打过
			if(!TextUtils.isEmpty(printUsers))
				tip= String.format("These Labels has been printed by %s. Reprint?",printUsers);
			else {
				tip = getPrintInfo(tmpPlts);
				tip += "Print?";
			}
			RewriteBuilderDialog.showSimpleDialog(this, tip, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						printTool.showDlg_printers(printLs);
					}
				});
			break;

		default:
			break;
		}
	}

	/**
	 * 获得打印信息
	 * @param tmpPlts
	 * @return
	 */
	private String getPrintInfo(List<Rec_PalletBean> tmpPlts) {
		if (Utility.isNullForList(tmpPlts)) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (Rec_PalletBean palletBean : tmpPlts) {
			sb.append(palletBean.isClp() ? "CLP: " : "TLP: ");
			sb.append(palletBean.wms_container_id + "\n");
		}
		return sb.toString();
	}

	private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs=new PrintTool.OnPrintLs() {
		
		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintPlts(printServerID);
		}
	};

	private void reqPrintPlts(long printServerID) {
		String ids = Rec_PalletBean.getPltIDs(tmpPlts);
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, ids);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				String name = StoredData.getUsername();
				//同步-打印人
				for (int i = 0; i < tmpPlts.size(); i++) {
					Rec_PalletBean b = tmpPlts.get(i);
					b.print_user_name = name;
					b.isSelected = false;
				}
				adp.notifyDataSetChanged();
				mulSel.adjustPrintAll();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);

	}

	private void reqDelPlts(final List<Rec_PalletBean> listToDel) {

		String ids = Rec_PalletBean.getPltIDs(listToDel);
		RequestParams p = new RequestParams();
		p.add("con_ids", ids);
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", lineID);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listPlts.removeAll(listToDel);
				adp.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_success));

				if (listPlts.size() == 0)
					onBackBtnOrKey();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/deletePallet", p, this);
	}
	
	@Override
	protected void onBackBtnOrKey() {
		setResult(RESULT_OK);
		super.onBackBtnOrKey();
	}

	private List<Rec_PalletBean> getPlts_sel() {
		List<Rec_PalletBean> list = new ArrayList<Rec_PalletBean>();
		for (int i = 0; i < listPlts.size(); i++) {
			Rec_PalletBean b = listPlts.get(i);
			if (b.isSelected)
				list.add(b);
		}
		return list;
	}

	// =================================================

	private MultiSelTool mulSel = new MultiSelTool() {

		@Override
		public void onSelAll(boolean toSelect) {
			// TODO Auto-generated method stub
			for (int i = 0; i < listPlts.size(); i++) {
				listPlts.get(i).isSelected = toSelect;
			}
		}

		@Override
		public boolean isAllSel() {
			// TODO Auto-generated method stub
			if (listPlts.isEmpty())
				return false;
			for (int i = 0; i < listPlts.size(); i++) {
				if (!listPlts.get(i).isSelected)
					return false;
			}
			return true;
		}
	};

	// =========传参===============================

	private String lineID;
	private int qtyPerPlt;
	private boolean isDelete;

	public static void initParams(Intent in, List<Rec_PalletBean> listPlts, String lineID,int qtyPerPlt,boolean isDelete) {
		in.putExtra("listPlts", (Serializable) listPlts);
		in.putExtra("lineID", lineID);
		in.putExtra("qtyPerPlt", qtyPerPlt);
		in.putExtra("isDelete", isDelete);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		listPlts = (List<Rec_PalletBean>) params.getSerializable("listPlts");
		lineID = params.getString("lineID");
		qtyPerPlt=params.getInt("qtyPerPlt");
		isDelete=params.getBoolean("isDelete");
	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listPlts == null ? 0 : listPlts.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_configdetail, null);
				h = new Holder();
				h.lo = convertView;
				h.tvPlt = (TextView) convertView.findViewById(R.id.tvPlt);
				// h.tvQty=(TextView)convertView.findViewById(R.id.tvQty);
				h.cb = (CheckBox) convertView.findViewById(R.id.cb);
				h.tvPrintMark = convertView.findViewById(R.id.tvPrintMark);
				h.tvDamage_it = (TextView) convertView.findViewById(R.id.tvDamage_it);
				h.tvLoc = (TextView) convertView.findViewById(R.id.tvLoc);
				h.tvStockOut_it = (TextView) convertView.findViewById(R.id.tvStockOut_it);
				h.tvGood_it = (TextView) convertView.findViewById(R.id.tvGood_it);
				h.tvPallet = (TextView) convertView.findViewById(R.id.tvPallet);
				h.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
				
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷ui
			final Rec_PalletBean b = listPlts.get(position);
			h.tvPlt.setText(b.wms_container_id);
			// h.tvQty.setText(b.sn_count+"/"+b.config_qty);
			h.cb.setSelected(b.isSelected);
			h.tvPrintMark.setVisibility(b.isPrinted() ? View.VISIBLE : View.GONE);

			h.tvGood_it.setText("G: " + b.normal_qty);
			h.tvDamage_it.setText("D: " + b.damage_qty);
			h.tvLoc.setText(b.location);
//			h.tvStockOut_it.setText("S: " + b.getShortage());
			int sCnt=b.getShortage();
			String sCntTitle=sCnt>0?"S: -":"O: +";
			h.tvStockOut_it.setText(sCntTitle+Math.abs(sCnt));
			
			h.tvGood_it.setVisibility(b.normal_qty == 0 ? View.GONE : View.VISIBLE);
			h.tvDamage_it.setVisibility(b.damage_qty == 0 ? View.GONE : View.VISIBLE);
			
			h.tvLocation.setText(b.location_type == 1 ? "Location: " : "Staging: ");
			
			if(b.isClp()){
				h.tvPallet.setText("CLP : ");
			}else{
				h.tvPallet.setText("TLP : ");
			}
			
			//若没配置 则无超出
			if(!b.hasConfig())
				h.tvStockOut_it.setVisibility(View.GONE);
			else
				h.tvStockOut_it.setVisibility(b.getShortage() == 0 ? View.GONE : View.VISIBLE);

			// 事件
			h.lo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					b.isSelected = !b.isSelected;
					mulSel.adjustPrintAll();
					adp.notifyDataSetChanged();
				}
			});

			return convertView;
		}

	};

	private class Holder {
		View lo, tvPrintMark;
		TextView tvPlt, tvQty, tvDamage_it, tvStockOut_it, tvGood_it, tvLoc,tvPallet;
		CheckBox cb;
		TextView tvLocation;
	}

}
