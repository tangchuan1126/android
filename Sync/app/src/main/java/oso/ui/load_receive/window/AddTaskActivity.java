package oso.ui.load_receive.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.InfoBean;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import oso.ui.load_receive.window.util.SelectOrderTypeAdapter;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.orderlistview.ChooseUsersActivity;
import oso.widget.selectoccupybutton.SelectOccupyBar;
import oso.widget.selectoccupybutton.SelectOccupyBar.OnSelectBarListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.anim.AnimUtils;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class AddTaskActivity extends BaseActivity {

	private SingleSelectBar singleSelectBar;
	private TextView tractorTv;
	private ImageView tractorTypeIv;
	private LinearLayout stagingLay, customerLay, titleLay;
	private TextView numTv, stagingTv, customerTv, titleTv, dateTv;
	private SearchEditText searchEt;
	private TextView notifyEt, hideEt;
	// private View lineTv;
	private SelectOccupyBar soBar;
	private Button submitBtn;

	private EntryBean mEntry;
	private EquipmentBean mEquipment;
	private ResourceInfo selectDoorInfo;
	private InfoBean currentInfo;
	private WindowTaskBean lastCloseUserTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_add_task, 0);
		initData();
		initView();
		initSingleSelectBarDialog();
		setView();
	}

	private void initData() {
		mEntry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		mEquipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		selectDoorInfo = (ResourceInfo) getIntent().getSerializableExtra("ResourceInfo");
		lastCloseUserTask = (WindowTaskBean) getIntent().getSerializableExtra("LastCloseUserTask");
		setTitleString("E" + mEntry.getEntry_id()); // setTitleString("Add Tasks");
	}

	private void initView() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		soBar = (SelectOccupyBar) findViewById(R.id.soBar);
		numTv = (TextView) findViewById(R.id.numTv);
		searchEt = (SearchEditText) findViewById(R.id.searchEt);
		searchEt.setTransformationMethod(new AllCapTransformationMethod());
		tractorTv = (TextView) findViewById(R.id.tractorTv);
		tractorTypeIv = (ImageView) findViewById(R.id.tractorTypeIv);
		notifyEt = (TextView) findViewById(R.id.notifyEt);
		hideEt = (TextView) findViewById(R.id.hideEt);
		stagingTv = (TextView) findViewById(R.id.stagingTv);
		customerTv = (TextView) findViewById(R.id.customerTv);
		titleTv = (TextView) findViewById(R.id.titleTv);
		stagingLay = (LinearLayout) findViewById(R.id.stagingLay);
		customerLay = (LinearLayout) findViewById(R.id.customerLay);
		titleLay = (LinearLayout) findViewById(R.id.titleLay);
		// lineTv = findViewById(R.id.lineTv);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		dateTv = (TextView) findViewById(R.id.dateTv);
	}

	public void submitOnClick(View v) {
		Utility.colseInputMethod(mActivity, searchEt);
		String numStr = searchEt.getText().toString().trim();
		String notifyStr = hideEt.getText().toString().trim();
		if (TextUtils.isEmpty(numStr)) {
			UIHelper.showToast(mActivity, singleSelectBar.getCurrentSelectItem().b == 0 ? "Enter LOAD#/PO#/ORDER#." : "Enter CTNR#/BOL#.", Toast.LENGTH_SHORT).show();
			return;
		}
		if (TextUtils.isEmpty(notifyStr)) {
			UIHelper.showToast(mActivity, getString(R.string.sync_select_notify), Toast.LENGTH_SHORT).show();
			return;
		}
		if (soBar.getCurrentValue().equals("")) {
			UIHelper.showToast(mActivity, getString(R.string.window_select_location), Toast.LENGTH_SHORT).show();
			AnimUtils.horizontalShake(mActivity, soBar);
			return;
		}
		if (numTv.getText().toString().trim().equals("")) {
			return;
		}
		reqAddTask();
	}

	private void reqAddTask() {
		RequestParams params = new RequestParams();
		params.add("Method", "AddOrUpdateTask");
		params.add("mainId", mEntry.getEntry_id());
		params.add("items", changeDataToJson());
		params.add("appointment_date", currentInfo.appointmentdate);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(final JSONObject json) {
				if (onRefreshListener != null)
					onRefreshListener.onRefresh();
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
				builder.setMessage(getString(R.string.sync_continue_task));
				builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						searchEt.setText("");
						clearViewData();
					}
				});
				builder.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Utility.popTo(mActivity, TaskListActivity.class);
						mActivity.finish();
					}
				});
				builder.show();

			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	public String changeDataToJson() {
		try {
			JSONArray array = new JSONArray();
			JSONObject beanObject = new JSONObject();
			beanObject.put("ra", singleSelectBar.getCurrentSelectItem().b + "");
			beanObject.put("occupancy_type", selectDoorInfo.resource_type);
			beanObject.put("number", currentInfo.number);
			beanObject.put("doorId", selectDoorInfo.resource_id);
			beanObject.put("tongzhi", hideEt.getText().toString().trim());
			beanObject.put("container_no", mEquipment.getEquipment_id());
			beanObject.put("zoneId", selectDoorInfo.area_id);
			// beanObject.put("detailId", ""); //修改用到的
			// beanObject.put("beizhu", "");
			beanObject.put("number", currentInfo.number);
			beanObject.put("number_type", currentInfo.order_type);// 单据类型
			beanObject.put("customer_id", currentInfo.customerid);
			beanObject.put("company_id", currentInfo.companyid);
			beanObject.put("account_id", currentInfo.accountid);
			beanObject.put("order_no", currentInfo.orderno);
			beanObject.put("po_no", currentInfo.pono);
			beanObject.put("supplier_id", currentInfo.supplierid);
			beanObject.put("staging_area_id", currentInfo.stagingareaid);
			beanObject.put("freight_term", currentInfo.freightterm);
			beanObject.put("appointment_date", "" + currentInfo.appointmentdate);
			beanObject.put("receipt_no", currentInfo.receiptno);
			beanObject.put("number_order_status", currentInfo.status);
			beanObject.put("order_system_type", currentInfo.system_type);
			array.put(beanObject);
			System.out.println("item= " + array.toString());
			return array.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void initSingleSelectBarDialog() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_pickup), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_delivery), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					numTv.setHint("LOAD/PO/ORDER");
					stagingLay.setVisibility(View.VISIBLE);
					customerLay.setVisibility(View.VISIBLE);
					titleLay.setVisibility(View.GONE);
					break;
				case 1:
					numTv.setHint("BOL/CTNR");
					stagingLay.setVisibility(View.GONE);
					customerLay.setVisibility(View.GONE);
					titleLay.setVisibility(View.VISIBLE);
					break;
				}
				clearViewData();
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void setView() {
		if (lastCloseUserTask != null && !TextUtils.isEmpty(lastCloseUserTask.close_user)) {
			System.out.println("lastCloseUserTask= " + lastCloseUserTask.close_user_ids);
			notifyEt.setText(lastCloseUserTask.close_user);
			hideEt.setText(lastCloseUserTask.close_user_ids);
		} else {
			RequestParams params = new RequestParams();
			params.add("Method", "FindSupervisors");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					System.out.println("FindSupervisors " + json.toString());
					notifyEt.setText(StringUtil.getJsonString(json, "notice_names"));
					hideEt.setText(StringUtil.getJsonString(json, "notice_ids"));
				}
			}.doGet(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
		}
		soBar.init(mActivity,mEntry.getEntry_id());
		soBar.setOnSelectBarListener(new OnSelectBarListener() {
			@Override
			public void onClick(List<ResourceInfo> resourceList) {
				Intent intent = new Intent(mActivity, AddDoorActivity.class);
				intent.putExtra("entryBean", mEntry);
				intent.putExtra("equipmentBean", mEquipment);
				intent.putExtra("doorlist", (Serializable) resourceList);
				intent.putExtra("type", soBar.getCurrentType());
				startActivityForResult(intent, AddDoorActivity.Code);
			}
		});
		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(mEquipment.getEquipment_type(), tractorTypeIv);
		tractorTv.setText(mEquipment.getEquipment_number());
		if (currentInfo != null)
			dateTv.setText(formatDate(currentInfo.appointmentdate));
		setDoorView(true);
		searchEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					getTaskData(searchEt.getText().toString().trim());
					Utility.colseInputMethod(mActivity, searchEt);
					return true;
				}
				return false;
			}
		});
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getTaskData(value);
				Utility.colseInputMethod(mActivity, searchEt);
			}

		});
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				clearViewData();
			}
		});
		notifyEt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
				intent.putExtra("resoures_text_id", notifyEt.getId());
				intent.putExtra("resoures_hide_id", R.id.hideEt);
				intent.putExtra("selected_adids", hideEt.getText().toString().trim());
				intent.putExtra("role_ids", BaseDataDepartmentKey.Warehouse);
				intent.putExtra("single_select", false);
				intent.putExtra("select_key", ChooseUsersActivity.KEY_WINDOW_CHECK_IN);
				intent.putExtra("proJsId", BaseDataSetUpStaffJobKey.LeadSuperVisor);
				startActivityForResult(intent, ChooseUsersActivity.requestCode);
			}
		});
	}

	private String formatDate(String appointmentdate) {
		if (appointmentdate == null || TextUtils.isEmpty(appointmentdate)) {
			return "NA";
		}
		if (appointmentdate.length() > 2 && appointmentdate.endsWith(".0")) {
			return appointmentdate.substring(0, appointmentdate.length() - 2);
		}
		return appointmentdate;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AddDoorActivity.Code && resultCode == AddDoorActivity.Code && data != null) {
			selectDoorInfo = (ResourceInfo) data.getSerializableExtra("selectDoorInfo");
			setDoorView(false);
		}
	}

	private void setDoorView(boolean isInit) {
		if (selectDoorInfo.resource_type != 0 && selectDoorInfo.resource_name != null) {
			soBar.setCurrentType(mActivity,selectDoorInfo.resource_type);
			soBar.setCurrentValue(selectDoorInfo.resource_name);
		} else {
			if (isInit) {
				soBar.initState(mActivity);
			}
		}
	}

	private void clearViewData() {
		setViewData(new InfoBean());
	}

	private void getTaskData(String value) {
		RequestParams params = new RequestParams();
		params.add("Method", "CheckOrderTypeSystem");
		params.add("search_number", value.toUpperCase());
		params.add("mainId", mEntry.getEntry_id());
		if (singleSelectBar.getCurrentSelectItem().b == 0)
			params.add("type", "1");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("CheckOrderTypeSystem" + json.toString());
				int num = StringUtil.getJsonInt(json, "num");
				JSONObject pickupJo = StringUtil.getJsonObjectFromJSONObject(json, "pickup");
				JSONArray infosJa = StringUtil.getJsonArrayFromJson(pickupJo, "infos");
				system_type = StringUtil.getJsonInt(json, "system_type");
				Gson gson = new Gson();
				final List<InfoBean> infoList = gson.fromJson(infosJa.toString(), new TypeToken<List<InfoBean>>() {
				}.getType());
				if (num == 0 || num == 1) {
					if (num == 0)
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, getString(R.string.window_not_found));
					currentInfo = infoList.get(0);
					searchEt.setText(currentInfo.number);
					setViewData(currentInfo);
					info2doorInfo();
					setDoorView(false);
				} else {
					showSelectDialog(infoList);
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	int system_type = 0;

	private void info2doorInfo() {
		if (currentInfo.resources_id != 0)
			selectDoorInfo.resource_id = currentInfo.resources_id;
		if (currentInfo.resources_type != 0)
			selectDoorInfo.resource_type = currentInfo.resources_type;
		if (currentInfo.resources_name != null)
			selectDoorInfo.resource_name = currentInfo.resources_name;
		if (currentInfo.area_id != null)
			selectDoorInfo.area_id = currentInfo.area_id;

		currentInfo.system_type = system_type + "";
	}

	private void setViewData(InfoBean info) {
		numTv.setText(info.order_type_value == null ? "" : info.order_type_value);
		stagingTv.setText(info.stagingareaid == null ? "NA" : info.stagingareaid);
		customerTv.setText(info.customerid == null ? "NA" : info.customerid);
		titleTv.setText(info.supplierid == null ? "NA" : info.supplierid);
		dateTv.setText(formatDate(info.appointmentdate));
		if (numTv.getText().toString().trim().equals("")) {
			submitBtn.setVisibility(View.GONE);
		} else {
			submitBtn.setVisibility(View.VISIBLE);
		}
	}

	private void showSelectDialog(final List<InfoBean> infoList) {
		final View view = getLayoutInflater().inflate(R.layout.dialog_select_data, null);
		final ListView lv = (ListView) view.findViewById(R.id.lv);
		final TextView orderTv = (TextView) view.findViewById(R.id.orderTv);
		final TextView poTv = (TextView) view.findViewById(R.id.poTv);
		orderTv.setText(singleSelectBar.getCurrentSelectItem().b == 0 ? "ORDER" : "SupplierID");
		poTv.setText(singleSelectBar.getCurrentSelectItem().b == 0 ? "PO" : "ReceiptNo");
		lv.setAdapter(new SelectOrderTypeAdapter(mActivity, infoList, singleSelectBar.getCurrentSelectItem().b));

		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Select");
		builder.setView(view);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				currentInfo = infoList.get(position);
				searchEt.setText(currentInfo.number);
				setViewData(currentInfo);
				info2doorInfo();
				setDoorView(false);
				builder.dismiss();
			}
		});
		builder.show();
	}

	@Override
	protected void onBackBtnOrKey() {
		// StoredData.saveNotifyUser(notifyEt.getText().toString().trim());
		// StoredData.saveNotifyUserId(hideEt.getText().toString().trim());
		// Intent data = new Intent();
		// data.putExtra("flag", true);
		// setResult(AddDoorActivity.REQUEST_CODE, data);
		super.onBackBtnOrKey();
	}

	public static OnRefreshListener getOnRefreshListener() {
		return onRefreshListener;
	}

	public static void setOnRefreshListener(OnRefreshListener onRefreshListener) {
		AddTaskActivity.onRefreshListener = onRefreshListener;
	}

	private static OnRefreshListener onRefreshListener;

	public interface OnRefreshListener {
		void onRefresh();
	}

}
