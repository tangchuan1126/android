package oso.ui.load_receive.do_task.receive.process;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.process.bean.Rec_RepeatPalletBean;
import oso.ui.load_receive.do_task.receive.task.Rec_CC_ConfigAc;
import oso.ui.load_receive.do_task.receive.task.Rec_ScanTask_ItemAc;
import oso.ui.load_receive.do_task.receive.task.Rec_ScanTask_PalletsAc;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.task.movement.Rec_Movement_Assign;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoLinesBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class Rec_LineListAc extends BaseActivity implements OnClickListener {

	private ListView lv;
	private List<Rec_ItemBean> listItems;

	private TextView tvCtnr, tvComp, tvCustomer, tvTitle;
	private Button btnFinish;
	
	public final static int Rec_LineListAc_ViewPalletsAc = 1500;
	public final static int Rec_LineListAc_PalletsAc = 1501;
	public final static int Rec_LineListAc_ItemAc = 1502;
	
	private static final int Rec_LineListAc_FromAssing = 1503;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_rec_line_list, 0);
		applyParams();
		setTitleString("Receipt: " + recBean.receipt_no);

		initView();
		
//		reqLines();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == RESULT_CANCELED)
			return;
		
		if(requestCode == Rec_LineListAc_ViewPalletsAc || requestCode == Rec_LineListAc_PalletsAc
				|| requestCode == Rec_LineListAc_ItemAc){
			listItems.clear();
			adapter.notifyDataSetChanged();
			reqLines();
		}
		if(requestCode == Rec_LineListAc_FromAssing){
			tipFinish();
		}
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnFinish: // 关闭rn
			reqgetData();
			break;

		default:
			break;
		}
	}
	
	private void reqgetData(){
		
		RequestParams params = new RequestParams();
		params.add("company_id", recBean.company_id);
		params.add("receipt_no", recBean.receipt_no);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				JSONObject joDatas = json.optJSONObject("datas");
				int is_receipt_multiply = joDatas.optInt("is_receipt_multiply");

				if (is_receipt_multiply == 1) {
				}
				else {
					Rec_ReceiveBean bean = Rec_ReceiveBean.handJsonForBean(json);
					bean.receipt_no = recBean.receipt_no;
					recBean = bean;
					
					updateCloseRN();
				}
			}
		}.doGet(HttpUrlPath.acquireRNLineInfoByReceiptID, params, this);
	}
	
	private void updateCloseRN(){
		refBtnFinish();
		if (recBean.isClosed()) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this,
					"This Receipt NO has been closed!");
			return;
		}
		// reqCloseRN();
		showDlg_confirmSubmit();
	}

	private void showDlg_confirmSubmit() {
		// v
		View v = getLayoutInflater().inflate(R.layout.dlg_rec_lines_confirm,
				null);
		TextView tvExpQty_dlg = (TextView) v.findViewById(R.id.tvExpQty_dlg);
		TextView tvCntQty = (TextView) v.findViewById(R.id.tvCntQty);
		TextView tvSnQty_dlg = (TextView) v.findViewById(R.id.tvSnQty_dlg);

		// 初始化
		// expQty
		int expQty = Rec_ItemBean.getTotal_expQty(listItems);
		tvExpQty_dlg.setText(expQty + "");
		// cntQty
		int cntQty_good = Rec_ItemBean.getTotal_cntQty_good(listItems);
		int cntQty_dmg = Rec_ItemBean.getTotal_cntQty_dmg(listItems);
		String cntQty = (cntQty_good + cntQty_dmg) + "";
		if (cntQty_dmg > 0)
			cntQty = cntQty + " (G:" + cntQty_good + " , D: " + cntQty_dmg
					+ ")";
		tvCntQty.setText(cntQty);
		// snQty
		int snGood = Rec_ItemBean.getTotal_snQty_good(listItems);
		int snDmg = Rec_ItemBean.getTotal_snQty_dmg(listItems);
		String snQty = (snGood + snDmg) + "";
		if (snDmg > 0)
			snQty = snQty + " (G:" + snGood + " , D: " + snDmg + ")";
		tvSnQty_dlg.setText(snQty);

		// sn若数量不对 则显示"红色"
		int snColor = (snGood + snDmg) == expQty ? R.color.sync_blue
				: R.color.sync_red_light;
		tvSnQty_dlg.setTextColor(getResources().getColor(snColor));

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setContentView(v);
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqCloseRN();
			}
		});
		bd.show();
	}

	private void reqCloseRN() {
		RequestParams p = new RequestParams();
	    p.add("receipt_id", recBean.receipt_id);
	    p.add("status", Rec_ReceiveBean.RNStatus_Closed);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				
				if(json.optInt("err") == 91){
					Rec_RepeatPalletBean repeatPalletBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_RepeatPalletBean.class);
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
					
					View v = View.inflate(mActivity, R.layout.item_repeatpallet, null);
					TextView tvSn = (TextView) v.findViewById(R.id.tvSN);
					TextView tvPallet1 = (TextView) v.findViewById(R.id.tvPallet1);
					TextView tvPallet2 = (TextView) v.findViewById(R.id.tvPallet2);
					tvSn.setText(repeatPalletBean.serial_number);
					String[] pallets = repeatPalletBean.getContainer();
					tvPallet1.setText(pallets[0]);
					tvPallet2.setText(pallets[1]);
					
					builder.setContentView(v);
					
					builder.isHideCancelBtn(true);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
					});
					builder.show();
					return;
				}
				
//				if(json.optInt("is_print_rn") == 1){
//					tipPrintReceiptTicket();
//				}else{
//					tipFinish();
//				}
//				tipFinish();
				reqAssignMovement();
			}
		}.setCancelable(false).doGet(
				HttpUrlPath.basePath + "_receive/android/closeRN", p, this);

	}
	
	
	private void reqAssignMovement(){
		RequestParams p = new RequestParams();
	    p.add("ps_id", StoredData.getPs_id());
	    p.add("receipt_id", recBean.receipt_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				
				String object = json.optJSONObject("data").toString();
				Rec_Movement_InfoBean infoBean = new Gson().fromJson(object,Rec_Movement_InfoBean.class);
				String array = json.optJSONObject("data").optJSONArray("lines").toString();
				List<Rec_Movement_InfoLinesBean> linesBean = new Gson().fromJson(array, new TypeToken<List<Rec_Movement_InfoLinesBean>>(){}.getType());
				
				Intent in = new Intent(mActivity,Rec_Movement_Assign.class);
				Rec_Movement_Assign.initParams(in, infoBean, linesBean,Rec_Movement_Assign.To_Assign);
				startActivityForResult(in,Rec_LineListAc_FromAssing);
			}
		}.setCancelable(false).doGet(
				HttpUrlPath.basePath + "_receive/android/acquireFinallyPallets", p, this);
	}
	
	private void tipPrintReceiptTicket() {
		RewriteBuilderDialog.Builder bd = RewriteBuilderDialog.newSimpleDialog(this, "Print Receipt Ticket?", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 打签
				printTool.showDlg_printers(new PrintTool.OnPrintLs() {

					@Override
					public void onPrint(long printServerID) {
						reqPrintRNTicket(printServerID);
					}
				});
			}
		});
		bd.setNegativeButton(R.string.sync_no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				tipFinish();
			}
		});
		bd.show();
	}
	
	private void tipFinish(){
		finish();
		UIHelper.showToast(getString(R.string.sync_success));
	}

	private void refBtnFinish() {
		btnFinish
				.setVisibility(Rec_ItemBean.isAllLineClosed(listItems) ? View.VISIBLE
						: View.GONE);
		btnFinish.setText(recBean.isClosed() ? "Close RN ( Closed )"
				: "Close RN");
	}

	private void reqLines() {

		final RequestParams params = new RequestParams();
		params.add("company_id", recBean.company_id); // ABL
		params.add("receipt_no", recBean.receipt_no); // 4
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				Rec_ReceiveBean bean = Rec_ReceiveBean.handJsonForBean(json);
				// Intent in = new Intent(mActivity, Receive_ItemListAc.class);
				// Receive_ItemListAc.initParams(in, bean);
				// startActivity(in);

				if (Utility.isEmpty(bean.lines))
					return;
				
				listItems.clear();
				listItems.addAll(bean.lines);
				adapter.notifyDataSetChanged();

				// 刷新状态
				try {
					recBean.status = json.optJSONObject("datas")
							.optJSONObject("receipt").optString("status");
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				refBtnFinish();
			}
			
//			private void OnFinished() {
//				listItems.clear();
//				adapter.notifyDataSetChanged();
//			}
			

		}.setCancelable(false).doGet(HttpUrlPath.acquireRNLineInfoByReceiptID, params, this);

	}

	private void initView() {
		// 取view
		tvCtnr = (TextView) findViewById(R.id.tvCtnr);
		tvComp = (TextView) findViewById(R.id.tvComp);
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		btnFinish = (Button) findViewById(R.id.btnFinish);

		// 事件
		btnFinish.setOnClickListener(this);

		lv = (ListView) findViewById(R.id.lv);
		
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// reqLinePlts(position);
//				Rec_ItemBean itemBean = listItems.get(position);
//				if(current_status == 2){
//					if(itemBean.line_status == 2){
//						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please do CC first!");
//						return;
//					}
//				}
				tipStart(position);
			}
		});

		// 初始化
		recBean.setTextCtnr(tvCtnr);
		tvComp.setText(recBean.company_id);
		tvCustomer.setText(recBean.customer_id);
		tvTitle.setText(recBean.title);
		
		tvTitle.requestFocus();
		
		refBtnFinish();
		showRightButton(R.drawable.menu_btn_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
						mActivity);
				String[] strs;
				if(recBean.isClosed()){
					strs = new String[] {"View Pallets","Print Receipt Ticket"};
				}else{
					strs = new String[] {"View Pallets"};
				}
				builder.setArrowItems(strs,
						new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								if(position == 0){
									reqGetAllPallets(recBean.receipt_id);
								}else if(position == 1){
									printTool.showDlg_printers(new PrintTool.OnPrintLs() {

										@Override
										public void onPrint(long printServerID) {
											reqPrintRNTicket(printServerID);
										}
									});
								}
							}
						});
				builder.show();
			}
		});
	}

	private PrintTool printTool = new PrintTool(this);
	
	private void reqPrintRNTicket(long printServerID) {
		RequestParams p = new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		p.add("printer", printServerID + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/printRNCount", p, mActivity);

	}
	
	private void reqGetAllPallets(String receipt_id) {
		RequestParams params = new RequestParams();
		params.add("receipt_id", receipt_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "data");
				List<Rec_ItemBean> list = new Gson().fromJson(ja.toString(),
						new TypeToken<List<Rec_ItemBean>>() {
						}.getType());
				if (list == null)
					return;
				// Intent intent = new Intent(mActivity,
				// Rec_ViewPalletsAc.class);
				// intent.putExtra("lines", (Serializable) list);
				// startActivity(intent);

				Intent in = new Intent(mActivity, Rec_ViewPalletsAc.class);
				Rec_ViewPalletsAc.initParams(in, list, recBean.receipt_no);
				startActivityForResult(in, Rec_LineListAc_ViewPalletsAc);
			}
		}.doGet(HttpUrlPath.findAllPallet, params, mActivity);
	}

	private int state_test_index = 0; // 用于测试-状态

	private void tipStart(int position) {
		final Rec_ItemBean itemBean = listItems.get(position);
		itemBean.customer = recBean.customer_id;
		itemBean.supplier = recBean.title;

		// debug,测试状态
		// if(AppConfig.isDebug){
		// recBean.status="";
		// switch (state_test_index) {
		// case 0: //uncnt
		// itemBean.line_status=Rec_ItemBean.LineState_Unprocess;
		// break;
		// case 1:
		// itemBean.line_status=Rec_ItemBean.LineState_ConfigAndPrint;
		// break;
		// case 2:
		// itemBean.line_status=Rec_ItemBean.LineState_CCCreated;
		// break;
		// case 3:
		// itemBean.line_status=Rec_ItemBean.LineState_ScanCreated;
		// itemBean.scan_start_user="Fei Han";
		// itemBean.scan_start_time="2015-4-3 17:40";
		// break;
		// case 4:
		// itemBean.line_status=Rec_ItemBean.LineState_Closed;
		// break;
		// case 5:
		// recBean.status=Rec_ReceiveBean.RNStatus_Closed;
		// break;
		// }
		// state_test_index=(state_test_index+1)%6;
		// }

		// uncnt:提示uncnt、cnted:提示start、scaning:直接进、line.closed:提示、rn.closed:不能进
		// rn.closed:不能进,注:1.不需扫sn若没扫sn 可进(扫了且rn.Close了,则不可进)

		boolean rnClosed_real = false;
		if (recBean.isClosed()) {
			rnClosed_real = true;
			// 不需扫sn的line,且sn.qty=0时,仍然可扫
			if (!itemBean.is_need_sn() && itemBean.getTotalSN() == 0)
				rnClosed_real = false;
		}

		//rn已close 不能再进
		if (rnClosed_real) {
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
					"The Receipt NO has been closed!");
			return;
		}
		switch (itemBean.line_status) {
		case Rec_ItemBean.LineState_Unprocess: // 没cnt完
		case Rec_ItemBean.LineState_ConfigAndPrint:
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
					"This Item hasn't been counted!");
			return;
		case Rec_ItemBean.LineState_CCStart:  //以cnt    CC Task
			//Rec_ScanTask_ItemAc
			if(current_status == 2){
				recScanCCTask(itemBean);
			}else{
				recCCTask(itemBean);
			}
			break;
		case Rec_ItemBean.LineState_ScanStart: // 已cnt  scan Task
			//Rec_ScanTask_ItemAc    step页面
			// 未开始扫描
			if(current_status == 3){
				recCCTask(itemBean);
			}else{
				recScanTask(itemBean);
			}
			break;
		case Rec_ItemBean.LineState_Closed: // 已关闭
			// 不需sn 且未开始扫,则提示"开始"
			//注:不需sn 若扫描过,则直接进去(不开始)
//			recCloseTask(itemBean);
			if(current_status == 3){
			}else{
				recScanTask(itemBean);
			}
			break;
		}
	}
	
	private void recScanCCTask(final Rec_ItemBean itemBean){
		//为true
		if(!itemBean.hasCCScan()){
			RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
			String msg = getString(R.string.rec_linelist_msg)+itemBean.lot_no+"?";
			bd.setMessage(msg);
			bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					//
					reqAssign(itemBean);
				}
			});
			bd.show();
		}else{
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please do CC first!");
		}
	}
	
	private void reqAssign(final Rec_ItemBean itemBean ) {

		RequestParams p = new RequestParams();
		p.add("receipt_id", itemBean.receipt_id);
		p.add("receipt_line_id", itemBean.receipt_line_id);
		p.add("operator", StoredData.getAdid());
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//finish();
				recScanTask(itemBean);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/createScanTaskToScaner", p, this);
//		assignScanTask
	}
	
	
	private void recCCTask(final Rec_ItemBean itemBean){
		//为true
//		itemBean.is_supervisor
		//可以修改cc
		if (itemBean.is_start_break == 0) {
			Rec_CC_ConfigAc.toThis(mActivity, itemBean, true,true, "", "", recBean.receipt_no);
		}else{
			//只能查看cc
			Rec_CC_ConfigAc.toThis(mActivity, itemBean, false,false, "", "", recBean.receipt_no);
		}
	}
	
	private void recScanTask(final Rec_ItemBean itemBean){
		if(itemBean.clp_expected_qty == 0){
			if (!itemBean.hasStartScan()) {
				RewriteBuilderDialog.showSimpleDialog(mActivity,
						"Start Scan Serial Numbers?",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								reqLinePlts(itemBean, true);
							}
						});
			}else{
				reqLinePlts(itemBean, false);
			}
		}else{
			reqScanPlts(itemBean);
		}

	}
	
	private void recCloseTask(final Rec_ItemBean itemBean){
		if (!itemBean.is_need_sn() && !itemBean.hasStartScan()) {
			String tip = "The Item needn't scan SN's, are you sure to start scan?";
			RewriteBuilderDialog.showSimpleDialog(mActivity, tip,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							// TODO Auto-generated method stub
							//开始扫-不需sn的
							reqLinePlts(itemBean, true);
						}
					});
		}else{
			reqLinePlts(itemBean, false);
		}
	}
	
	private void reqScanPlts(final Rec_ItemBean itemBean){
		RequestParams p = new RequestParams();
		p.add("line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
				if(json.optInt("err") == 92){
					RewriteBuilderDialog.showSimpleDialog(mActivity, "The task is not assigned to you . do this task ?", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqJoinSchedule(itemBean);
						}
					});
					return;
				}
				
				
				Rec_ItemBean itemBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_ItemBean.class);
				//---------Create Pallet
				JSONArray jPallet = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> listPallet = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPallet)) {
					listPallet = new Gson().fromJson(jPallet.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//---------Create Pallet
				JSONArray jPalletCC = json.optJSONArray("pallets_cc");
				List<Rec_PalletConfigBean> listPalletCC = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listPalletCC = new Gson().fromJson(jPalletCC.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//----------Break Partial-------
				JSONArray jBreakPartial = json.optJSONArray("break_partial");
				List<Rec_PalletConfigBean> listBreakPartial = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listBreakPartial = new Gson().fromJson(jBreakPartial.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				
				Intent in = new Intent(mActivity,Rec_ScanTask_ItemAc.class);
				Rec_ScanTask_ItemAc.initParams(in, itemBean, false,listPallet,listPalletCC,listBreakPartial);
				startActivityForResult(in, Rec_LineListAc_ItemAc);
				
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/acquireScanReceiptLinesByLineId",p,mActivity);
	}
	private void reqLinePlts(final Rec_ItemBean itemBean, final boolean start) {

		final RequestParams params = new RequestParams();
		params.add("receipt_line_id", itemBean.receipt_line_id);
		// Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
		params.add("container_config_id", "0");
		params.add("is_start_scan", start ? "1" : "0");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				
				if(json.optInt("err") == 92){
					RewriteBuilderDialog.showSimpleDialog(mActivity, "The task is not assigned to you . do this task ?", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqJoinSchedule(itemBean,start);
						}
					});
					return;
				}
				// 打开rn(如:不需扫sn的rn,start后 状态需变)
				//注:1>rn关闭时 不能再进line(故此处可直接改为open)
				recBean.status = Rec_ReceiveBean.RNStatus_Open;
				refBtnFinish();

				JSONArray jaPlts = json.optJSONArray("pallets");
				// if (Utility.isEmpty(jaPlts)) {
				// RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
				// "No pallets! Please config pallets first!");
				// return;
				// }
				// plts
				String strPlts = jaPlts == null ? "[]" : jaPlts.toString();
				List<Rec_PalletBean> listPlts = new Gson().fromJson(strPlts,
						new TypeToken<List<Rec_PalletBean>>() {
						}.getType());

				// dmgPlts
				JSONArray jaDmg = json.optJSONArray("damaged");
				List<Rec_PalletBean> listDmg = null;
				if (!Utility.isEmpty(jaDmg)) {
					listDmg = new Gson().fromJson(jaDmg.toString(),
							new TypeToken<List<Rec_PalletBean>>() {
							}.getType());
					listPlts.addAll(listDmg);
				}
				
				 List<Rec_PalletType> listTypes = new Gson().fromJson(json.optJSONArray("contypes").toString(), new TypeToken<List<Rec_PalletType>>(){}.getType());
					

				// debug,刷新状态
				// try {
				// recBean.status =
				// json.optJSONObject("datas").optJSONObject("receipt").optString("status");
				// } catch (Exception e) {
				// // TODO: handle exception
				// e.printStackTrace();
				// }
//                Rec_PltListAc
				Intent in = new Intent(mActivity, Rec_ScanTask_PalletsAc.class);
				recBean.curItemBean = itemBean;
                Rec_ScanTask_PalletsAc.initParams(in, listPlts,listTypes, recBean,false);
				startActivityForResult(in, Rec_LineListAc_PalletsAc);
			}

		}.doGet(HttpUrlPath.basePath+"_receive/android/acquireScanPalletsInfoByLine", params, this);
		
	}
	
	private void reqJoinSchedule(final Rec_ItemBean itemBean){
		RequestParams params = new RequestParams();
		params.put("receipt_line_id", itemBean.receipt_line_id);
		params.put("type", current_status == 2 ? 0 : 1);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				reqScanPlts(itemBean);
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/joinSchedule", params, mActivity);
	}
	
	private void reqJoinSchedule(final Rec_ItemBean itemBean,final boolean start){
		RequestParams params = new RequestParams();
		params.put("receipt_line_id", itemBean.receipt_line_id);
		params.put("type", current_status == 2 ? 0 : 1);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				reqLinePlts(itemBean,start);
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/joinSchedule", params, mActivity);
	}

	// =========传参===============================

	// private CheckInTaskBeanMain doorBean;
	private Rec_ReceiveBean recBean;
	private int current_status = -1;

	public static void initParams(Intent in, Rec_ReceiveBean recBean,int status) {
		in.putExtra("recBean", recBean);
		in.putExtra("status", status);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		recBean = (Rec_ReceiveBean) params.getSerializable("recBean");
		current_status = params.getInt("status");
		// doorBean = recBean.doorBean;
		if (recBean.lines == null)
			recBean.lines = new ArrayList<Rec_ItemBean>();
		listItems = recBean.lines;
	}

	// ==================nested===============================

	private final MAdapter adapter = new MAdapter();

	private class MAdapter extends BaseAdapter {
		

		@Override
		public int getCount() {
			return listItems == null ? 0 : listItems.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_rec_recno, null);
				h = new Holder();
				h.tvItemNO = (TextView) convertView.findViewById(R.id.tvItemNO);
				h.tvLotNO = (TextView) convertView.findViewById(R.id.tvLotNO);
				h.tvGoodQty = (TextView) convertView
						.findViewById(R.id.tvGoodQty);
				h.tvDmgQty = (TextView) convertView.findViewById(R.id.tvDmgQty);
				h.tvReceivedPlts = (TextView) convertView
						.findViewById(R.id.tvReceivedPlts);
				h.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
				h.tvCntTime = (TextView) convertView
						.findViewById(R.id.tvCntTime);
				h.loDamage_it = convertView.findViewById(R.id.loDamage_it);
				h.tvRepeat = convertView.findViewById(R.id.tvRepeat);
				h.tvLocation = (TextView) convertView
						.findViewById(R.id.tvLocation);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷ui
			Rec_ItemBean b = listItems.get(position);
			h.tvItemNO.setText(b.item_id);
			h.tvLotNO.setText(b.lot_no);
			h.tvGoodQty.setText(b.sn_normal_qty + " / " + b.normal_qty + "");
			h.tvDmgQty.setText(b.sn_damage_qty + " / " + b.damage_qty + "");
			// 有1个 则显示
			boolean showDmgQty = b.damage_qty > 0 || b.sn_damage_qty > 0;
			h.loDamage_it.setVisibility(showDmgQty ? View.VISIBLE : View.GONE);
			h.tvReceivedPlts.setText(b.count_pallet + "");
			h.tvLocation.setText("Staging Location: " + b.location);
			h.tvLocation
					.setVisibility(TextUtils.isEmpty(b.location) ? View.GONE
							: View.VISIBLE);

			String status = "";
			String time = "";
			switch (b.line_status) {
			case Rec_ItemBean.LineState_Unprocess: // 没cnt完
			case Rec_ItemBean.LineState_ConfigAndPrint:
				status = "Uncounted";
				time = "";
				break;
			case Rec_ItemBean.LineState_ScanStart: // 已cnt
				// 已开始扫描
				if (b.hasStartScan()) {
					status = "Scanning By " + b.scan_start_user;
					time = b.scan_start_time;
				}//做过cc
				else if(b.hasCCScan()){
					status = "CC By " + b.cc_start_user;
					time = b.cc_start_time;
				}
				// 未开始扫描
				else {
					status = "Counted By " + b.counted_user_name;
					time = b.unloading_finish_time;
				}
				break;
			case Rec_ItemBean.LineState_CCStart:
				if(b.hasCCScan()){
					status = "CC By " + b.cc_start_user;
					time = b.cc_start_time;
				}else{
					status = "Counted By " + b.counted_user_name;
					time = b.unloading_finish_time;
				}
				break;
			case Rec_ItemBean.LineState_Closed: // 已关闭
				status = "Closed By " + b.scan_end_user;
				time = b.scan_end_time;
				break;
			default:
				status = "Error";
				time = "";
				break;
			}

			h.tvStatus.setText(status);
			h.tvCntTime.setText(time);
			h.tvCntTime.setVisibility(TextUtils.isEmpty(time) ? View.GONE
					: View.VISIBLE);
			h.tvRepeat.setVisibility(b.is_repeat() ? View.VISIBLE : View.GONE);
			return convertView;
		}
	}

	class Holder {
		TextView tvItemNO, tvLotNO, tvGoodQty, tvDmgQty, tvReceivedPlts,
				tvStatus, tvCntTime, tvLocation;
		View loDamage_it, tvRepeat;
	}

}
