package oso.ui.load_receive.assign_task;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.adapter.DockCheckInAssignAdapter;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.FragSelRes;
import support.common.UIHelper;
import support.common.bean.DockCheckInBase;
import support.common.bean.DockCheckInDoorBase;
import support.common.bean.DockCheckInDoorForLoadListBase;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: SelectDoorActivity
 * @Description:
 * @author gcy
 * @date 2014-12-3 下午2:21:00
 */
public class SelectDoorActivity extends BaseActivity implements OnClickListener {

	public static final int DockCheckInAllocationFlag = 1;			//assign页
//	public static final int AssignActivityFlag = 2;
	public static final int CheckInTaskDoorItemActivityFlag = 3;	//task、load
	
	public static final int SELECTDOOR = 102;

	private Button submit_id;
	private FragSelRes fragRes;

	private IntentSelectDoorBean intentSelectDoorBean;
	private List<ResourceInfo> resourceList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dci_s_door_layout, 0);
		setTitleString(getString(R.string.shuttle_select_door));

		getFromActivityData(getIntent());
		// initView();

		// 取view
		fragRes = (FragSelRes) getFragmentManager().findFragmentById(
				R.id.fragRes);
		submit_id = (Button) findViewById(R.id.submit_id);

		// 监听事件
		submit_id.setOnClickListener(this);

		// 初始化
		((TextView) findViewById(R.id.entry_id_tx))
				.setText(intentSelectDoorBean.check_in_entry_id);
		((TextView) findViewById(R.id.equipment_type))
				.setText(intentSelectDoorBean.equipment_type_value);
		((TextView) findViewById(R.id.equipment_tx))
				.setText(intentSelectDoorBean.equipment_number);
		((TextView) findViewById(R.id.resource)).setText(OccupyTypeKey
				.getOccupyTypeKeyName(mActivity,intentSelectDoorBean.resources_type)
				+ " [" + intentSelectDoorBean.resources_type_value + "]");

		// 初始化fragRes
		fragRes.init(resourceList,(intentSelectDoorBean!=null&&intentSelectDoorBean.resources_type==OccupyTypeKey.DOOR)?intentSelectDoorBean.resources_id:0);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.submit_id:
			doSubmit();
			break;

		default:
			break;
		}
	}

	private void doSubmit(){
//		if(adapter==null){
//			UIHelper.showToast(mActivity, "Data Error", Toast.LENGTH_SHORT).show();
//			return;
//		}
//		if(adapter.getCurrentId()<0){
//			UIHelper.showToast(mActivity, "Please select door !", Toast.LENGTH_SHORT).show();
//			return;
//		}
		
		ResourceInfo selectDoorInfo=fragRes.getSelectRes();
		if(selectDoorInfo==null)
		{
			UIHelper.showToast(this, OccupyTypeKey.getPrompt(mActivity,OccupyTypeKey.DOOR));
			return;
		}
		
		RequestParams params = new RequestParams();
		String method = "";
		String url = "";
		String from = "";
		switch (intentSelectDoorBean.intentType) { 
		case DockCheckInAllocationFlag:;				
//		case AssignActivityFlag:
			from = "supervisor";
			method = "AssignTaskChangeDoorByDetail";
			url = HttpUrlPath.AndroidAssignTaskAction;
			DockCheckInDoorBase temp = (DockCheckInDoorBase) getIntent().getSerializableExtra("dockCheckInDoorBase");
			//------添加需要更改到的门的相关信息
			params.add("resouces_id", selectDoorInfo.resource_id+"");
			params.add("resouces_type", selectDoorInfo.resource_type+"");
			//------获取 需要
			String detail_ids = "";
			for (int j = 0; j < temp.getLoadList().size(); j++) {
				DockCheckInDoorForLoadListBase d = temp.getLoadList().get(j);
				if (d.isSelect() && DockCheckInAssignAdapter.judgeTaskNoClose(d.getNumber_status())) {
					
					if((j+1)==temp.getLoadList().size()){
						detail_ids += d.getDlo_detail_id();
					}else{
						detail_ids += d.getDlo_detail_id() + ",";
					}
				}
			}
			params.add("detail_ids", detail_ids);
			
			
			break;
		case CheckInTaskDoorItemActivityFlag:
			from = "";
			method = "TaskProcessingMoveToDoor";
			url = HttpUrlPath.AndroidTaskProcessingAction;
			params.add("moved_resource_type", intentSelectDoorBean.resources_type+"");
			params.add("moved_resource_id", intentSelectDoorBean.resources_id+"");
			params.add("sd_id", String.valueOf(selectDoorInfo.resource_id));
			break;
		default:
			finish();
			break;
		}
		params.add("Method", method);
		params.add("equipment_id", intentSelectDoorBean.equipment_id+"");
		params.add("entry_id", intentSelectDoorBean.check_in_entry_id);
		params.add("from", from);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				jumpToAnotherActivity(json);
			}
		}.doGet(url, params, mActivity);
	}

	/**
	 * @Description:根据条件跳转到entry所对应的类型
	 * @param flag
	 */
	protected void jumpToAnotherActivity(JSONObject json) {
		ResourceInfo selectDoorInfo=fragRes.getSelectRes();
		
		Intent intent = new Intent();
		switch (intentSelectDoorBean.intentType) {
		case DockCheckInAllocationFlag:
//		case AssignActivityFlag:
			DockCheckInBase dockCheckInBase = DockCheckInBase.parsingJSON(json,
					intentSelectDoorBean.check_in_entry_id);
			if (dockCheckInBase != null) {
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT)
						.show();
				intent.setClass(mActivity, AssignTaskActivity.class);
				intent.putExtra("dockCheckInBase", dockCheckInBase);
				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,
						R.anim.push_from_right_in);
			} else {
				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		case CheckInTaskDoorItemActivityFlag:
			if (json.optInt("ret") == 1) {
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT)
						.show();
				intent.setClass(mActivity, CheckInTaskDoorItemActivity.class);

				intentSelectDoorBean.resources_id = selectDoorInfo.resource_id;
				intentSelectDoorBean.resources_type_value = selectDoorInfo.resource_name
						+ "";
				intentSelectDoorBean.resources_type = selectDoorInfo.resource_type;
				intent.putExtra("intentSelectDoorBean",
						(Serializable) intentSelectDoorBean);// 给 李俊昊

				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,
						R.anim.push_from_right_in);
			} else {

				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		default:
			finish();
			break;
		}

	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}

	protected void getFromActivityData(Intent intent) {
		intentSelectDoorBean = (IntentSelectDoorBean) intent
				.getSerializableExtra("intentSelectDoorBean");
		resourceList = (List<ResourceInfo>) intent
				.getSerializableExtra("resourceList");
	}

}