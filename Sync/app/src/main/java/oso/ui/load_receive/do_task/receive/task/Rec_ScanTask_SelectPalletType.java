package oso.ui.load_receive.do_task.receive.task;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.SelectTypeListView.OnItemOrderClickListener;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.widget.edittext.SeacherMethod;
import utility.ActivityUtils;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class Rec_ScanTask_SelectPalletType extends BaseActivity {
	
	private SelectTypeListView lv;
	private EditText searchEt;
	private ImageView delBtn;
	
	private LinearLayout searchLayout;
	private ListView searchLv;
	
	private List<Rec_PalletType> palletTypes;  //可选类型（48*48）
	private Rec_PalletType palletType;
	
	private List<Rec_PalletType> searchPalletTypes;
	
	public static final int Rec_SelectPalletType  = 2000; 
	
	private SearchAdapter searchAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lo_rec_scan_selecttype, 0);
		setTitleString("Select Type");
		applyParams();
        initView();
        setData();
	}
	
	
	private void initView(){
		searchEt = (EditText) findViewById(R.id.searchEt);
		delBtn = (ImageView) findViewById(R.id.delBtn);
		lv = (SelectTypeListView) findViewById(R.id.lv);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
	}
	
	private void setData(){
		
		searchPalletTypes = new ArrayList<Rec_PalletType>();
		searchAdapter = new SearchAdapter();
		searchLv.setAdapter(searchAdapter);
		
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s)) {
					delBtn.setVisibility(View.GONE);
				} else {
					delBtn.setVisibility(View.VISIBLE);
				}
			}
		});
		
		delBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchEt.setText("");
			}
		});
		
		lv.setAdapter(palletTypes);
		lv.setOnItemClickListener(new OnItemOrderClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id, Rec_PalletType letter) {
				Intent in=new Intent();
				in.putExtra("index", position);
				setResult(RESULT_OK,in);
				onBackBtnOrKey();
				
			}
		});
		
		searchLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent in = new Intent();
				in.putExtra("index",palletTypes.indexOf(searchPalletTypes.get(position)));
				setResult(RESULT_OK,in);
				onBackBtnOrKey();
			}
		});
		
	}
	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();
			
			searchPalletTypes.clear();
			for(Rec_PalletType palletType : palletTypes){
				if(palletType.type_name.toLowerCase().contains(searchStr.toLowerCase())){
					searchPalletTypes.add(palletType);
				}
			}
			
			searchAdapter.notifyDataSetChanged();
			
		} else {
			hideSearch();
		}
	}
	
	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
		searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}
	
	@Override
	protected void onBackBtnOrKey() {
		super.onBackBtnOrKey();
	}
	
	 //===========================传参==========================================

    public static void initParams(Intent in, List<Rec_PalletType> types, Rec_PalletType type) {
        in.putExtra("palletTypes", (Serializable) types);
        in.putExtra("type", (Serializable) type);
    }

    private void applyParams() {
        Bundle params = getIntent().getExtras();
        
        palletTypes = (List<Rec_PalletType>) params.getSerializable("palletTypes");
        palletType  = (Rec_PalletType) params.getSerializable("type");
        
        if(palletType != null){
        	for(Rec_PalletType type : palletTypes){
        		if(type.type_id == palletType.type_id){
        			type.isselect = true;
        			return;
        		}
        	}
        }
    }
    
  //---------------------------------------
	
  	private class SearchAdapter extends BaseAdapter{

  		@Override
  		public int getCount() {
  			return searchPalletTypes == null ? 0 : searchPalletTypes.size();
  		}

  		@Override
  		public Object getItem(int position) {
  			return searchPalletTypes.get(position);
  		}

  		@Override
  		public long getItemId(int position) {
  			return position;
  		}

  		@Override
  		public View getView(int position, View convertView, ViewGroup parent) {
  			if(convertView == null){
  				convertView = View.inflate(mActivity, android.R.layout.simple_list_item_1, null);
  			}
  			((TextView) convertView).setTextColor(Color.BLACK);
  			((TextView) convertView).setTextSize(12);
  			((TextView) convertView).setText(searchPalletTypes.get(position).type_name);
  			
  			return convertView;
  		}
  		
  	}

}
