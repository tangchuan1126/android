package oso.ui.load_receive.do_task.receive.oso;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.receive.nolist.LoadNListAc;
import oso.ui.load_receive.do_task.receive.oso.bean.ComplexReceiveOsoBean;
import oso.ui.load_receive.do_task.receive.oso.bean.ReceiveOsoBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.LoadBarLayout;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 
 * @author 朱成
 * @date 2015-1-13
 */
public class NoListLoad_ComfirmAc extends BaseActivity implements
		OnClickListener {

	private TextView tvPallets, tvResType, tvRes;

	private ComplexReceiveOsoBean complexBean;
	private CheckInTaskBeanMain doorBean;
	private CheckInTaskItemBeanMain taskBean;

	private ListView lvPalletTypes;
	private List<Map<String, String>> listReceive;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_nolist_comfirm, 0);
		applyParams();
		setTitleString(taskBean.getTaskTypeName() + ": " + taskBean.number);

		// 取view
		loadbar = (LoadBarLayout) findViewById(R.id.loadbar);
		ttp = loadbar.getTabToPhoto();
		// ---
		lvPalletTypes = (ListView) findViewById(R.id.lvPallets);
		tvPallets = (TextView) findViewById(R.id.tvPallets);
		tvResType = (TextView) findViewById(R.id.tvResType);
		tvRes = (TextView) findViewById(R.id.tvRes);

		// 监听事件
		findViewById(R.id.btnConfirm).setOnClickListener(this);

		// lv
		listReceive = ReceiveOsoBean.getMapTypesCnt(complexBean.listPallets);
		lvPalletTypes.setAdapter(adp);

		// 初始化
		refreshTopTabs();
		tvPallets.setText(ReceiveOsoBean.getPalletCnt(complexBean.listPallets)
				+ "");
		tvResType.setText(doorBean.getResTypeStr(mActivity) + ": ");
		tvRes.setText(doorBean.resources_name);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnConfirm:
			RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_close_question),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							LoadNListAc.reqFinish(
									ScanLoadActivity.CloseType_Normal,
									complexBean, ttp, mActivity);
						}
					});
			break;

		default:
			break;
		}
	}

	// =================tabPhoto===============================

	private LoadBarLayout loadbar;
	private TabToPhoto ttp;

	/**
	 * 初始化-tabs
	 */
	private void refreshTopTabs() {
		// 初始化ttp
//		TabParam param = LoadNListAc.getTabParamList(doorBean.entry_id,
//				taskBean.dlo_detail_id, ttp);
		TabParam param= ScanLoadActivity.getTab_load(ttp, doorBean.entry_id);
		loadbar.initData(doorBean, param,taskBean.number);
	}

	// =========传参===============================

	public static void initParams(Intent in, ComplexReceiveOsoBean b) {
		in.putExtra("datas", (Serializable) b);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		complexBean = (ComplexReceiveOsoBean) params.getSerializable("datas");
		doorBean = complexBean.doorBean;
		taskBean = complexBean.taskBean;

		// listReceive = complexBean.listPallets;
	}

	// ===============nested==================================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.nolist_typecnt_item, null);
				h = new Holder();
				h.tvPalletType = (TextView) convertView
						.findViewById(R.id.tvPalletType);
				h.tvCnt = (TextView) convertView.findViewById(R.id.tvCnt);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			Map<String, String> b = listReceive.get(position);
			h.tvPalletType.setText(b.get("0"));
			h.tvCnt.setText(b.get("1"));
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listReceive==null?0:listReceive.size();
		}
	};

	class Holder {
		TextView tvPalletType;
		TextView tvCnt;
	}
}
