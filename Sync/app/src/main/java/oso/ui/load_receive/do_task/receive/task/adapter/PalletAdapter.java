package oso.ui.load_receive.do_task.receive.task.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PalletAdapter  extends BaseAdapter {
	
	private List<Rec_PalletConfigBean> listConfigs;
	
	private Context context;
	
	
	public PalletAdapter(Context context,List<Rec_PalletConfigBean> listConfigs) {
		super();
		this.listConfigs = listConfigs;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		Holder h;
		if(convertView == null){
			convertView = View.inflate(context, R.layout.item_rec_configpallet, null);
			h = new Holder();
			
			h.tvType = (TextView) convertView.findViewById(R.id.tvType);
			h.tvTotal = (TextView) convertView.findViewById(R.id.tvTotal);
			h.tvConfig = (TextView) convertView.findViewById(R.id.tvConfig);
			h.tvPalletType = (TextView) convertView.findViewById(R.id.tvPalletType);
			h.tvKeepInbound = (TextView) convertView.findViewById(R.id.tvKeepInbound);
			h.loConfig = (LinearLayout) convertView.findViewById(R.id.loConfig);
			h.tvKeepInbound2 = (TextView) convertView.findViewById(R.id.tvKeepInbound2);
			h.tvReceiptNo = (TextView) convertView.findViewById(R.id.tvReceiptNO);
			h.tvPrint = (TextView) convertView.findViewById(R.id.tvPrint);
			h.tvPrint2 = (TextView) convertView.findViewById(R.id.tvPrint2);
			h.tvNumber = (TextView) convertView.findViewById(R.id.tvNumber);
			h.tvShipTo = (TextView) convertView.findViewById(R.id.tvShipTo);
			
			h.loReceiptNO = (LinearLayout) convertView.findViewById(R.id.loReceiptNO);
			h.loType = (LinearLayout) convertView.findViewById(R.id.loType);
			h.loShipTo = (LinearLayout) convertView.findViewById(R.id.loShipTo);
			
			convertView.setTag(h);
		}else{
			h = (Holder) convertView.getTag();
		}
		Rec_PalletConfigBean bean = listConfigs.get(position);

		h.tvPalletType.setText(bean.pallet_name);
		h.tvNumber.setText("× "+bean.plate_number);
		if(bean.goods_qty == 0){
			h.tvTotal.setText(bean.getTotalQty()+"");
		}else{
			h.tvTotal.setText(bean.goods_qty+"");
		}
		
		
		h.tvPrint.setVisibility(bean.isPrinted() ? View.VISIBLE : View.GONE);
		h.tvPrint2.setVisibility(bean.isPrinted() ? View.VISIBLE : View.GONE);
		
		if(!bean.isCLP()){
			
			if(bean.is_partial == 1){
				h.loReceiptNO.setVisibility(View.VISIBLE);
				h.loType.setVisibility(View.GONE);
				h.loShipTo.setVisibility(View.GONE);
				h.tvConfig.setText("");
				h.tvConfig.setHint("NA");
				
				h.tvReceiptNo.setText(bean.getWMSContainerId());
				h.tvKeepInbound2.setText(bean.keep_inbound == 1 ? "(Keep Inbound)" : "");
				if(!bean.hasConfig()){
					h.tvTotal.setText(bean.getTotalQty()+"");
				}
			}else{
				h.loReceiptNO.setVisibility(View.GONE);
				h.loType.setVisibility(View.VISIBLE);
				h.loShipTo.setVisibility(View.GONE);
				
				h.tvType.setText("TLP");
				h.tvConfig.setText(bean.getConfigDesc());
				h.tvKeepInbound.setText(bean.keep_inbound == 1 ? "(Keep Inbound)" : "");
				
			}
			
		}else{
			
			h.loReceiptNO.setVisibility(View.GONE);
			h.loType.setVisibility(View.VISIBLE);
			h.loShipTo.setVisibility(TextUtils.isEmpty(bean.ship_to_name) ? View.GONE : View.VISIBLE);
			
			h.tvConfig.setText(bean.getConfigDesc());
			
			h.tvType.setText("CLP");
			h.tvKeepInbound.setText(bean.keep_inbound == 1 ? "(Keep Inbound)" : "");
			h.tvShipTo.setText(bean.ship_to_name);
			
		}
		return convertView;
	}
	
	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public Object getItem(int position) {
		return null;
	}
	
	@Override
	public int getCount() {
		return listConfigs == null ? 0 : listConfigs.size();
	}
	
	private class Holder {
		
		TextView tvType,tvTotal,tvConfig,tvPalletType,tvKeepInbound,tvKeepInbound2,tvReceiptNo,tvPrint,tvPrint2,tvNumber,tvShipTo;
		
		LinearLayout loType,loConfig,loReceiptNO,loShipTo;
	}
}
