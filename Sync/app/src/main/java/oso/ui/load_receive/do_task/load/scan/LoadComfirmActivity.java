package oso.ui.load_receive.do_task.load.scan;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.LoadBarLayout;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.bean.CheckInLoadOrderBean;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInLoad_SubLoadBean;
import support.common.tts.TTS;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class LoadComfirmActivity extends BaseActivity implements
		OnClickListener {

	private ListView lvOrders;

	private final int Req_RefreshTopTabs = 123;

	private TextView tvResType, tvMBolOrOrder, tvMBol, tvDock, tvEmpty;
	private LoadBarLayout loadbar;
	private TabToPhoto ttp;

	private CheckInLoad_ComplexSubLoads complexSubLoads;
	private List<CheckInLoadOrderBean> listOrders;

//	private boolean hasUploadImgs = false; // 若已上传图片 则下次点confirm不上传

	private Button btnConfirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_checkin_load_comfirm, 0);

		applyParams();
		if (complexSubLoads == null) {
			finish();
			return;
		}

		// 取view
		loadbar = (LoadBarLayout) findViewById(R.id.loadbar);
		ttp = loadbar.getTabToPhoto();
		lvOrders = (ListView) findViewById(R.id.lvOrders);
		tvMBolOrOrder = (TextView) findViewById(R.id.tvMBolOrOrder);
		tvMBol = (TextView) findViewById(R.id.tvMBol);
		tvDock = (TextView) findViewById(R.id.tvDock);
		tvEmpty = (TextView) findViewById(R.id.tvEmpty);
		tvResType = (TextView) findViewById(R.id.tvResType);
		btnConfirm = (Button) findViewById(R.id.btnConfirm);

		// 监听事件
		btnConfirm.setOnClickListener(this);

		// 初始化lvOrders
		lvOrders.setEmptyView(tvEmpty);
		lvOrders.setAdapter(adpOrders);
		lvOrders.setDivider(null);

		// debug
		refreshTopTabs();

		if (complexSubLoads.load != null) {
			String title_1 = complexSubLoads.load.isMainType_load() ? "Load"
					: "Order";
			setTitleString(title_1 + ": " + complexSubLoads.load.getMainNo());
		}
		// 基本信息
		CheckInLoad_SubLoadBean subload = complexSubLoads.getCurSubLoad();
		tvDock.setText(complexSubLoads.doorBean.resources_name);
		tvResType.setText(complexSubLoads.doorBean.getResTypeStr(mActivity) + ": ");

		boolean isLoad = complexSubLoads.load.isMainType_load();
		tvMBolOrOrder.setText(isLoad ? "MBOL: " : "Order: ");
		tvMBol.setText(isLoad ? subload.master_bol_no + ""
				: complexSubLoads.load.getMainNo());

		boolean isNormalClose = complexSubLoads.getCurSubLoad()
				.isToNormalClose();
		btnConfirm.setText(isNormalClose ? "Confirm Close"
				: "Confirm Exception Close");
	}

	private void refreshTopTabs() {
		//debug
//		TabParam p = ScanLoadActivity
//				.getTab_load(ttp, complexSubLoads.entry_id);
		TabParam p =getTab_load(ttp, complexSubLoads.entry_id);
		loadbar.initData(complexSubLoads.doorBean, p,
				complexSubLoads.taskBean.number);
	}

	public TabParam getTab_load(TabToPhoto ttp, String entry) {
		// 基于entry
		String loadKey = TTPKey.getTaskProcessKey(entry);
		TabParam p = TabParam.new_NoTitle(loadKey, ttp);
//		p.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "",
//				FileWithTypeKey.OCCUPANCY_MAIN + "", entry);
		//debug
		p.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "",
				FileWithTypeKey.OCCUPANCY_MAIN + "", entry,complexSubLoads.taskBean.dlo_detail_id);
		return p;
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						ttp.clearCache();
						LoadComfirmActivity.super.onBackBtnOrKey();

						// 直接退出时,清空扫描状态
						// complexSubLoads.getCurSubLoad().resetPallets();
					}
				});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnConfirm:
			//若有待上传图片,则先上传
//			if(!ttp.isPhotoEmpty(false))
//				uploadImgs();
//			else
				// showDlg_tipPrintLabel();
				toNextAc();
			break;

		default:
			break;
		}
	}

	// @Override
	// protected void onResume() {
	// super.onResume();
	// ttp.onResume(getTabParamList());
	// }

	// private List<TabParam> getTabParamList() {
	// List<TabParam> params = new ArrayList<TabParam>();
	// params.add(new TabParam("Load", ScanLoadActivity.loadKey, new
	// PhotoCheckable(0, "Load", ttp)));
	// return params;
	// }

	// 输入-proNo,扫完order时
	// 入参:isChangeManually:true(手动修改,不执行后续"报数/跳转")
	private void showDlg_inputProNo(final CheckInLoadOrderBean order) {
		// 语音-提示
		TTS.getInstance().speakAll("Input Prono Number!");

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle("Input ProNo");
		builder.setPositiveButtonOnClickDismiss(false);
		// LinearLayout layout = new LinearLayout(context);
		// layout.setPadding(10, 10, 10, 10);
		// 内容视图
		View layout = getLayoutInflater().inflate(R.layout.dlg_load_inputprono,
				null);
		final EditText etProNo = (EditText) layout.findViewById(R.id.etProNo);
		TextView tvMBolOrOrder = (TextView) layout
				.findViewById(R.id.tvMBolOrOrder);
		TextView tvMBol = (TextView) layout.findViewById(R.id.tvMBol);

		// hint取上1值
		String tmpProNo = order.proNo;
		tmpProNo = tmpProNo == null ? "" : tmpProNo;
		etProNo.setText(tmpProNo);
		etProNo.setSelection(tmpProNo.length());

		boolean isLoad = complexSubLoads.load.isMainType_load();
		tvMBolOrOrder.setText(isLoad ? "MBOL: " : "Order: ");
		tvMBol.setText(isLoad ? complexSubLoads.getCurSubLoad().master_bol_no
				+ "" : complexSubLoads.load.getMainNo());

		builder.setContentView(layout);
		builder.setPositiveButton(getString(R.string.sync_submit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						// 设置-ok
						if (onConfirm_proNo(etProNo, order))
							dialog.dismiss();
					}
				});
		builder.setNegativeButton("No ProNo",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						// 清空
						etProNo.setText("");
						onConfirm_proNo(etProNo, order);
					}
				});
		final RewriteBuilderDialog dlg = builder.create();
		dlg.show();

		// 监听-事件
		etProNo.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == KeyEvent.ACTION_UP
						&& keyCode == KeyEvent.KEYCODE_ENTER) {
					if (onConfirm_proNo(etProNo, order))
						dlg.dismiss();
					// 防止-焦点转移
					return true;
				}
				return false;
			}
		});
	}

	// 确定"proNo"时
	// 入参:isChangeManually(true:手动修改proNo)
	// 返回:true(设置成功时)
	private boolean onConfirm_proNo(EditText etProNo, CheckInLoadOrderBean order) {
		String newProNo = etProNo.getEditableText().toString();
		if (newProNo == null)
			newProNo = "";
		notifyServer(order, newProNo);
		return true;
	}

	private void notifyServer(final CheckInLoadOrderBean order,
			final String newProNo) {
		// CheckInLoadOrderBean order = flowStatus.lastScaned_order;
		// PalletBean pallet = flowStatus.lastScaned_pallet;

		RequestParams params = new RequestParams();
		params.add("Method", "LoadOnePallet");
		params.add("cmd", ScanLoadActivity.TNotify_change_pro_no);
		// if (pallet != null) {
		// params.add("pallet_number", pallet.palletNo);
		// params.add("pallet_type", pallet.get_TypeId());
		// }
		if (order != null) {
			params.add("order", order.orderno);
			params.add("pro_no", newProNo);
		}
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		// debug
		params.add("lr_id", complexSubLoads.lr_id);
		params.add("system_type", complexSubLoads.system_type + "");
		params.add("order_type", complexSubLoads.order_type + "");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// System.out.println("nimei>>notifyServer");
				order.proNo = newProNo;
				adpOrders.notifyDataSetChanged();
			}

			@Override
			public void handFinish() {
				System.out.println("nimei>>finish");
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Fail!");
			}
		}.doGet(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	// -----------------------

	private static CheckInLoad_ComplexSubLoads orders_bridge;

	// 入参:closeType(取ScanLoadActivity.CloseType_x)
	public static void initParams(Intent in,
			CheckInLoad_ComplexSubLoads subloads) {
		orders_bridge = subloads;
	}

	private void applyParams() {
		if (orders_bridge == null)
			return;

		complexSubLoads = orders_bridge;
		orders_bridge = null;

		// 提取-有扫描的order
		List<CheckInLoadOrderBean> tmpOrders = complexSubLoads.getCurSubLoad().listOrders;
		if (tmpOrders != null && !tmpOrders.isEmpty()) {
			listOrders = new ArrayList<CheckInLoadOrderBean>();
			for (int i = 0; i < tmpOrders.size(); i++) {
				CheckInLoadOrderBean tmpOrder = tmpOrders.get(i);
				// 有扫描的,才显示
				if (tmpOrder.getPalletsScaned() > 0)
					listOrders.add(tmpOrder);
			}
		}

	}

	// =================nested=======================================

	private BaseAdapter adpOrders = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder holder;
			if (convertView == null) {
				// convertView =
				// getLayoutInflater().inflate(R.layout.checkin_comfirmload_order_item,
				// null);
				convertView = getLayoutInflater().inflate(
						R.layout.checkin_comfirmload_order_item_x, null);
				holder = new Holder();
				holder.tvOrder = (TextView) convertView
						.findViewById(R.id.tvOrder);
				holder.tvTotalPallet = (TextView) convertView
						.findViewById(R.id.tvTotalPallet);
				holder.loPallets = (LinearLayout) convertView
						.findViewById(R.id.loPallets);
				holder.tvStaging = (TextView) convertView
						.findViewById(R.id.tvStaging);
				holder.tv_prono_num_layout = (View) convertView
						.findViewById(R.id.loProNo);
				holder.tv_prono_num = (TextView) convertView
						.findViewById(R.id.tvProNo);
				// holder.lineAbovePallets = convertView
				// .findViewById(R.id.lineAbovePallets);
				holder.loGroupView = convertView.findViewById(R.id.loGroupView);
				holder.tvCaseCnt = (TextView) convertView
						.findViewById(R.id.tvCaseCnt);

				holder.item = convertView.findViewById(R.id.item);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新-数据
			final CheckInLoadOrderBean b = listOrders.get(position);
			holder.tvOrder.setText(b.orderno);
			holder.tvTotalPallet.setText((b.code_nos == null ? 0 : b.code_nos
					.size()) + "");
			holder.tvStaging.setText(b.staging_area_id);
			holder.tvCaseCnt.setText(b.case_total);

			if (!TextUtils.isEmpty(b.proNo)) {
				holder.tv_prono_num.setText(b.proNo);
				holder.tv_prono_num_layout.setVisibility(View.VISIBLE);
			} else
				holder.tv_prono_num_layout.setVisibility(View.GONE);
			// 生成-pallets
			generatePallets(holder.loPallets, b, holder);

			// 监听事件
			holder.loGroupView
					.setOnLongClickListener(new View.OnLongClickListener() {

						@Override
						public boolean onLongClick(View v) {
							// TODO Auto-generated method stub
							showDlg_inputProNo(b);
							return false;
						}
					});

			// 关闭|无条目时,显示圆背景
			boolean showOne = b.getPalletCnt() == 0;
			holder.item
					.setBackgroundResource(showOne ? R.drawable.sh_load_lv_one
							: R.drawable.sh_load_lv_top);
			return convertView;
		}

		// 生成-pallettype数量,含"显示/隐藏"容器
		private void generatePallets(LinearLayout loPalletTypes,
				CheckInLoadOrderBean order, Holder holder) {
			loPalletTypes.removeAllViews();

			Map<String, Integer> map_PalletType_Cnt = order
					.getMapPalletTypeCnt();
			Set<String> types = map_PalletType_Cnt == null ? null
					: map_PalletType_Cnt.keySet();

			// Set<String> types = map_PalletType_Cnt.keySet();
			if (types == null || types.isEmpty()) {
				loPalletTypes.setVisibility(View.GONE);
				// holder.lineAbovePallets.setVisibility(View.GONE);
				return;
			} else {
				loPalletTypes.setVisibility(View.VISIBLE);
				// holder.lineAbovePallets.setVisibility(View.VISIBLE);
			}

			int types_num = 0;
			LinearLayout loPalletType = null;
			for (String type : types) {
				loPalletType = (LinearLayout) getLayoutInflater().inflate(
						R.layout.checkin_comfirmload_pallet_item, null);
				if (types_num == 0) {
					((View) loPalletType.findViewById(R.id.dashed_line))
							.setVisibility(View.GONE);
					types_num++;
				}
				TextView tvPalletType = (TextView) loPalletType
						.findViewById(R.id.tvPalletType);
				TextView tvCnt = (TextView) loPalletType
						.findViewById(R.id.tvCnt);

				// 刷新-数据
				tvPalletType.setText(type);
				tvCnt.setText(map_PalletType_Cnt.get(type) + "");

				// 背景
				loPalletType.setBackgroundResource(R.drawable.sh_load_lv_mid);

				loPalletTypes.addView(loPalletType);
			}

			// 背景
			loPalletType.setBackgroundResource(R.drawable.sh_load_lv_bot);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if (listOrders == null)
				return 0;
			return listOrders.size();
		}
	};

	private class Holder {
		TextView tvOrder, tvTotalPallet, tvStaging, tv_prono_num;
		View tv_prono_num_layout;
		LinearLayout loPallets;
		// View lineAbovePallets;
		View loGroupView;
		View item;

		TextView tvCaseCnt;
	}

	private void uploadImgs() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.SubmitLoadPalletInfo);
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		params.add("master_bol_no ",
				complexSubLoads.getCurSubLoad().master_bol_no + "");
		params.add("entry_id", complexSubLoads.entry_id);
		ttp.uploadZip(params, "LoadPhoto");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// jumpToAnotherActivity();
				ttp.clearData(true);
//				hasUploadImgs = true;
				// showDlg_tipPrintLabel();
				toNextAc();
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	private void toNextAc() {
		Intent in = new Intent(mActivity, LoadToPrintActivity.class);
		LoadToPrintActivity.initParams(in, complexSubLoads);
		startActivityForResult(in, Req_RefreshTopTabs);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 若返回,刷新数据
		if (requestCode == Req_RefreshTopTabs) {
			refreshTopTabs();
			return;
		}

		if (resultCode == RESULT_CANCELED)
			return;

		ttp.onActivityResult(requestCode, resultCode, data);
	}

}
