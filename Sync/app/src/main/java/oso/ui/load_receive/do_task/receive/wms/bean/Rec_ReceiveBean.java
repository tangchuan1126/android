package oso.ui.load_receive.do_task.receive.wms.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.common.bean.CheckInTaskBeanMain;
import utility.StringUtil;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * receive对象
 * @author 朱成
 * @date 2015-3-18
 */
public class Rec_ReceiveBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6394834031504537735L;
	
	public final static String RNStatus_Closed="Closed";
	public final static String RNStatus_Open="Open";
	
	public String company_id;
	public String ctnr;
	public String customer_id;
//	public String note;
	public String receipt_id;			//和receipt_no不同
	public String title;
	public List<Rec_ItemBean> lines;
	
	public String status;
	
	public boolean receipt_ticket_printed;
	
	//==================临时数据===============================

	public String receipt_no;
	public String dlo_detail_id;
	public Rec_ItemBean curItemBean;	//用于页面-传数据,跳转时手动加上
	public CheckInTaskBeanMain doorBean;
	public String osoNote;				//window时-添加的note
	
	
	public boolean hasOsoNote(){
		return !TextUtils.isEmpty(osoNote);
	}
	
	public boolean isClosed(){
		return RNStatus_Closed.equals(status);
	}
	
	/**
	 * 所有line都关了,但并非rn关了
	 * @return
	 */
	public boolean isAllLineCnted(){
//		return Rec_ItemBean.isAllLineClosed(lines);
		if(lines==null)
			return true;
		for (int i = 0; i < lines.size(); i++) {
			if(!lines.get(i).isCnt_complete())
				return false;
		}
		return true;
	}
	
	/**
	 * @return
	 */
	public boolean isCan_NormalClose(){
		if(lines==null)
			return true;
		for (int i = 0; i < lines.size(); i++) {
			Rec_ItemBean b=lines.get(i);
			
			//已cnt完、且数量如期(有坏的也不要紧)
//			boolean isOk=b.isCnt_complete()&&b.isAsExpected();
			boolean isOk=b.isCnt_complete()&&(b.getCntQty()==b.expected_qty);
			if(!isOk)
				return false;
		}
		return true;
	}
	
	
	//=================================================

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getCtnr() {
		return ctnr;
	}

	public void setCtnr(String ctnr) {
		this.ctnr = ctnr;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

//	public String getNote() {
//		return note;
//	}
//
//	public void setNote(String note) {
//		this.note = note;
//	}

	public String getReceipt_id() {
		return receipt_id;
	}

	public void setReceipt_id(String receipt_id) {
		this.receipt_id = receipt_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setTextCtnr(TextView tv){
		if(ctnr == null || ctnr.isEmpty()){
			tv.setHint("NA");
		}else{
			tv.setText(ctnr);
		}
	}
	public static Rec_ReceiveBean handJsonForBean(JSONObject json) {
		Rec_ReceiveBean bean = new Rec_ReceiveBean();

		JSONObject datas = json.optJSONObject("datas");
		JSONObject receipt = datas.optJSONObject("receipt");
		bean.company_id = receipt.optString("company_id");
		bean.ctnr = receipt.optString("ctnr");
		bean.customer_id = receipt.optString("customer_id");
		//debug
//		bean.note = receipt.optString("note");
		bean.receipt_id = receipt.optString("receipt_id");
		bean.title = receipt.optString("title");
		bean.status=receipt.optString("status");
		bean.receipt_ticket_printed=receipt.optBoolean("receipt_ticket_printed");
		bean.lines=handJsonForList(receipt);
		return bean;

	}

	public static List<Rec_ItemBean> handJsonForList(
			JSONObject json) {

		List<Rec_ItemBean> list = null;
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "lines");

		if (jsonArray != null && jsonArray.length() > 0) {
			list = new ArrayList<Rec_ItemBean>();
			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject item = jsonArray.optJSONObject(i);
				if (item != null) {
					Rec_ItemBean b = new Rec_ItemBean();
					b.expected_qty = item.optInt("expected_qty");
					b.config_qty=item.optInt("config_qty");
					b.item_id = item.optString("item_id");
					b.lot_no = item.optString("lot_no");
					b.note = item.optString("note");
					b.pallets_info = item.optString("pallets_info");
					b.receipt_line_id = item.optString("receipt_line_id");
					b.config_pallet = item.optInt("config_pallet");
					b.receive_qty = item.optInt("receive_qty");
					b.count_pallet=item.optInt("count_pallet");
					
					b.damage_qty=item.optInt("damage_qty");
					b.normal_qty=item.optInt("normal_qty");
//					b.stock_out_qty=item.optInt("stock_out_qty");
					
					b.counted_user_name=item.optString("counted_user_name");
					b.unloading_finish_time=item.optString("unloading_finish_time");
				
					b.scan_end_user=item.optString("scan_end_user");
					b.scan_end_time=item.optString("scan_end_time");
					
					b.scan_start_time=item.optString("scan_start_time");
					b.scan_start_user=item.optString("scan_start_user");
					b.sn_size=item.optString("sn_size");
					b.cc_start_time=item.optString("cc_start_time");
					b.cc_user=item.optString("cc_user");
					b.cc_start_user=item.optString("cc_start_user");
					b.is_start_break=item.optInt("is_start_break");
					
					Receive_ItemSetUp itembean = new Receive_ItemSetUp();
					JSONObject itemObj = item.optJSONObject("itemsetup");
					itembean.is_check_lot_no=itemObj
							.optBoolean("is_check_lot_no");
					itembean.is_need_sn=itemObj.optInt("is_need_sn");
					itembean.sn_size=itemObj.optString("sn_size");
					b.line_status=item.optInt("line_status");
					b.is_repeat=item.optInt("is_repeat");
					
					b.sn_normal_qty=item.optInt("sn_normal_qty");
					b.sn_damage_qty=item.optInt("sn_damage_qty");
					b.location=item.optString("location");
					
					b.is_supervisor=item.optBoolean("is_supervisor");
					
					b.clp_expected_qty=item.optInt("clp_expected_qty");
					
					b.itemsetup = itembean;
					list.add(b);

				}

			}
		}

		return list;

	}

//	public static List<Rec_ItemBean> getUalue(JSONObject json) {
//
//		JSONObject datas = json.optJSONObject("datas");
//		Rec_ReceiveBean bean = new Rec_ReceiveBean();
//		JSONObject receipt = datas.optJSONObject("receipt");
//		bean.company_id = receipt.optString("company_id");
//		bean.ctnr = receipt.optString("ctnr");
//		bean.customer_id = receipt.optString("customer_id");
//		//debug
////		bean.note = receipt.optString("note");
//		bean.receipt_id = receipt.optString("receipt_id");
//		bean.title = receipt.optString("title");
//
//		return null;
//	}
}
