package oso.ui.load_receive.do_task.main;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.load.scan.LoadToPrintActivity;
import oso.ui.load_receive.do_task.main.adapter.CheckInAddLoadbarListAdapter;
import oso.ui.load_receive.do_task.main.bean.Load_useBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.VLoadBarBox;
import oso.widget.loadbar.VSealBox;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.bean.CheckInTaskBeanMain;
import support.dbhelper.Goable;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;

/**
 * equip在某door下task都close时,至该页面
 * 
 * @author 朱成
 * @date 2014-12-17
 */
public class CheckInTaskFinishActivity extends BaseActivity{

	private TabToPhoto ttp;
//	private EditText finish_inseal, finish_outseal;
	Button finish_submit;
	// String entry_id;
	String detail_id;
	int caneditseal; // 是否可编辑seal

	public Resources resources;

	CheckInTaskBeanMain mainbean = new CheckInTaskBeanMain();

	CheckInAddLoadbarListAdapter adapter;

	public static int LoadCloseNotifyStayOrLeave = 1; // 关闭load的时候提示停留或者的是离开 ，
														// 当前最后一个单据下最后一个子单据
	public static int LoadCloseNotifyReleaseDoor = 2; // 关闭load的时候提示是否relaeaseDoor
														// ,当前门下面最后一个子单据
	public static int LoadCloseNoifyNormal = 3; // 关闭load的时候 如果不是最后一个（door
												// 或者是Entry ）正常
	public static int LoadCloseNotifyInputSeal = 4; // 本单据下面最后一个子单据的时候提示输入Seal

	// =========传参===============================

	// ,int returnflag
	public static void initParams(Intent in, CheckInTaskBeanMain taskBean) {
		in.putExtra("taskBean", taskBean);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		mainbean = (CheckInTaskBeanMain) params.getSerializable("taskBean");
	}

	// ================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_task_finish_layout, 0);
		resources = getResources();
		applyParams();
		initData();
		initView();

		// 初始化
		ttp.init(mActivity, getTabParamList());
		ttp.setTTPEnabled(mainbean);
		setTitleString(getString(R.string.tms_release)+mainbean.getResTypeStr(mActivity));
	}

	private void initData() {
		finish_submit = (Button) findViewById(R.id.finish_submit);
		caneditseal = getIntent().getIntExtra("caneditseal", 1);
		(((TextView) findViewById(R.id.equipment_number))).setText(mainbean.equipment_number);
		CheckInTractorOrTrailerTypeKey.setImageViewBgd(mainbean.equipment_type, (((ImageView) findViewById(R.id.equipment_type))));
		(((TextView) findViewById(R.id.resouse_info))).setText(mainbean.getResTypeStr(mActivity)+": "+mainbean.resources_name);
	}

	private void initView() {
		//获取view
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		VLoadBarBox vLoadBarBox = (VLoadBarBox) findViewById(R.id.vLoadBarBox);
		VSealBox sealBox=(VSealBox)findViewById(R.id.sealBox);
		
		vLoadBarBox.init(mainbean,"");
		sealBox.init(mainbean);
		ttp.setTTPEnabled(!CheckInMainDocumentsStatusTypeKey.isEquipmentLeft(mainbean.equipment_status));
		
		//监听事件
		finish_submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// debug
				// finishSubmitway();
				showDlg_confirm();
			}
		});
	}

	/**
	 * finish-确认
	 */
	private void showDlg_confirm() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		// 初始化dlg
		View vContent = getLayoutInflater().inflate(
				R.layout.dlg_normalclose_confirm, null);
		builder.setContentView(vContent);
		builder.setTitle(getString(R.string.patrol_confirm));
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finishSubmitway();
			}
		});

		// 取view
		TextView tvInSeal = (TextView) vContent.findViewById(R.id.tvInSeal);
		TextView tvOutSeal = (TextView) vContent.findViewById(R.id.tvOutSeal);
		LinearLayout loLoadbars = (LinearLayout) vContent
				.findViewById(R.id.loLoadbars);
		View tvLoadbar_NA=vContent.findViewById(R.id.tvLoadbar_NA);
		
		//隐藏seal,debug
		View loInSeal_confirm=vContent.findViewById(R.id.loInSeal_confirm);
		View loOutSeal_confirm=vContent.findViewById(R.id.loOutSeal_confirm);
		loInSeal_confirm.setVisibility(mainbean.hasInSeal()?View.VISIBLE:View.GONE);
		loOutSeal_confirm.setVisibility(mainbean.hasOutSeal()?View.VISIBLE:View.GONE);
		
		// 初始化-数据
		String inSeal=mainbean.in_seal;
		String outSeal=mainbean.out_seal;
		tvInSeal.setText(TextUtils.isEmpty(inSeal)?"NA":inSeal);
		tvOutSeal.setText(TextUtils.isEmpty(outSeal)?"NA":outSeal);
		addLoadbars_confrimDlg(loLoadbars,tvLoadbar_NA);
		builder.show();
	}

	private void addLoadbars_confrimDlg(LinearLayout lo,View vNA) {
		List<Load_useBean> listBars = mainbean.load_UseList;
		//debug
		if (listBars == null || listBars.size() == 0) {
			vNA.setVisibility(View.VISIBLE);
			return;
		}
		vNA.setVisibility(View.GONE);
		for (int i = 0; i < listBars.size(); i++) {
			Load_useBean tmpB = listBars.get(i);
			View tmpV = getLayoutInflater().inflate(
					R.layout.dlg_taskclose_loadbar_item, null);
			((TextView) tmpV.findViewById(R.id.tvBar_name))
					.setText(tmpB.load_bar_name + "");
			((TextView) tmpV.findViewById(R.id.tvBar_cnt)).setText(tmpB.count
					+ "");
			lo.addView(tmpV);
		}
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						CheckInTaskFinishActivity.super.onBackBtnOrKey();
					}
				});
		// RewriteBuilderDialog.showSimpleDialog_Tip(this, "Submit first!");
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
//		TabParam p = new TabParam("Dock Close", "Task_finish_"
//				+ mainbean.entry_id + mainbean.resources_id,
//				new PhotoCheckable(0, "TaskFinishImage", ttp), 40);
		
		String key=TTPKey.getTaskProcessKey(mainbean.entry_id);
//		TabParam p = new TabParam("Dock Close",key,
//				new PhotoCheckable(0, "TaskFinishImage", ttp), 40);
//		p.setWebImgsParams(
//				String.valueOf(FileWithCheckInClassKey.PhotoDockClose),
//				String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN),
//				mainbean.entry_id);
		TabParam p = new TabParam(getString(R.string.qeuipment_type_processing),key,
				new PhotoCheckable(0,getString(R.string.qeuipment_type_processing), ttp), 40);
		p.setWebImgsParams(
				String.valueOf(FileWithCheckInClassKey.PhotoTaskProcessing),
				String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN),
				mainbean.entry_id);
		params.add(p);

		return params;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	PopupWindow promptPopupWindow;

	/**
	 * finish
	 */
	public void finishSubmitway() {

		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingFinishSubmit");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
//		params.add("in_seal", finish_inseal.getText().toString() + "");
//		params.add("out_seal", finish_outseal.getText().toString() + "");
		params.add("in_seal", mainbean.in_seal + "");
		params.add("out_seal", mainbean.out_seal + "");
		params.add("caneditseal", caneditseal + "");
		ttp.uploadZip(params, "FinishSeal");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				try {
					int data = StringUtil.getJsonInt(json, "returnflag");
					if(LoadToPrintActivity.LoadClose_DirectFinishDoor==data){
						finishway();
					}
					else if (LoadCloseNotifyStayOrLeave == data) {
						leavebuilder();
					} else if (LoadCloseNotifyReleaseDoor == data) {
						releasebuilder();
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);

	}

	// 停留 离开
	private void leavebuilder() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setTitle(resources.getString(R.string.sync_notice));
//		builder.setMessage("Does Load Leave ? ");
		builder.setMessage(getString(R.string.sync_leave_text));
		builder.setCancelable(false);
		builder.setPositiveButton(
				resources.getString(R.string.check_in_leave_n),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						leaveway(CheckInMainDocumentsStatusTypeKey.INYARD);
					}
				});
		builder.setNegativeButton(
				resources.getString(R.string.check_in_leave_y),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						leaveway(CheckInMainDocumentsStatusTypeKey.LEAVING);
					}
				});
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private void releasebuilder() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setTitle(resources.getString(R.string.sync_notice));
		String tip =LoadToPrintActivity.getReleaseTip(mActivity, mainbean.resources_type);

		builder.setMessage(tip);
		
		builder.setCancelable(false);
		builder.setPositiveButton(resources.getString(R.string.check_in_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						releaseway();
					}
				});
		builder.setNegativeButton(resources.getString(R.string.check_in_no),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finishway();
					}
				});
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private void leaveway(final int leave) {
		Goable.initGoable(mActivity);
		final RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params);
		params.add("Method", "TaskProcessingStayOrLeave");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_id", mainbean.resources_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		params.add("status", leave + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				finishway();
			}

			public void handFail() {
			};
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	private void releaseway() {
		Goable.initGoable(mActivity);
		final RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params);
		params.add("Method", "TaskProcessingReleaseDoor");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_id", mainbean.resources_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				finishway();
			}

			public void handFail() {
			};
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	// 跳转
	public void finishway() {
		CheckInTaskDoorItemActivity.toLastAc(mActivity);
	}

	

	// 请求,关门所需数据
	// public void reqFinishDoorData() {
	// RequestParams params = new RequestParams();
	// params.add("Method", "TaskProcessingFinish");
	// params.add("entry_id", mainbean.entry_id + "");
	// params.add("equipment_id", mainbean.equipment_id + "");
	// params.add("resources_id", mainbean.resources_id + "");
	// params.add("resources_type", mainbean.resources_type + "");
	// // 改成LoadJson 数据的方式
	// new SimpleJSONUtil() {
	//
	// @Override
	// public void handReponseJson(JSONObject json) {
	// // Intent intent = new Intent(mActivity,
	// // CheckInTaskFinishActivity.class);
	// int caneditseal = json.optInt("caneditseal");
	//
	// // intent.putExtra("caneditseal", caneditseal);
	// // intent.putExtra("taskmainlist", (Serializable) mainbean);
	// // mActivity.startActivity(intent);
	// }
	// }.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	// }
}
