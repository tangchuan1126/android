package oso.ui.load_receive.do_task.receive.samsung;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.receive.samsung.bean.Ctnr;
import oso.ui.load_receive.do_task.receive.samsung.bean.Received;
import oso.ui.load_receive.do_task.receive.samsung.bean.Staging;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.dbhelper.Goable;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.FileUtils;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class ReceiveDetailActivity extends BaseActivity implements OnItemClickListener, OnClickListener {

	private LayoutInflater inflater;
	private SingleSelectBar singleSelectBar;
	private TabToPhoto ttp;
	private ListView ctnrLv, receivedLv;
	private CtnrAdapter ctnrAdapter;
	private ReceivedAdapter receivedAdapter;
	private StagingAdapter stagingAdapter;
	private SearchEditText etPallet;
	private LinearLayout receiveLayout, receivedLayout;
	// private BadgeView badge;
	private LinearLayout boxTotalLay, weightTotalLay;
	private TextView palletTotalTv, boxTotalTv, weightTotalTv;
	private LinearLayout liLayout, inLayout;
	private Button scanOkBtn, scanCancelBtn, areaBtn;
	private EditText mQTYEt, mWeightEt;
	private LinearLayout mWeightLay, mQTYLay;
	private boolean isSetting = false;
	private RewriteBuilderDialog settingDialog;
	private Received newReceived;
	SetupReceive mSetupReceive = new SetupReceive();
	private Ctnr mCtnr;
	private String entry_id;
	List<Received> dataAll = new ArrayList<Received>();
	List<Received> receivedList = new ArrayList<Received>();
	List<Staging> stagingAll = new ArrayList<Staging>();
	List<Staging> stagingCon = new ArrayList<Staging>();

	private final String MSG_ERROR = "Submit Receive First!";

	private static enum AreaType {
		ADD, UPDATE, SUBMIT, SUBMIT_UPDATE, SETUP
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_receive_detail, 0);
		initData();
		initView();
		initSingleSelectBar();
		ttp.init(mActivity, getTabParamList());
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				setReceivedData(true);
			}
		}, 300);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ttp.onResume(getTabParamList());
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Receive", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("Received", 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					receiveLayout.setVisibility(View.VISIBLE);
					receivedLayout.setVisibility(View.GONE);
					receiveLayout.setAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_top_in));
					break;
				case 1:
					receivedLayout.setVisibility(View.VISIBLE);
					receiveLayout.setVisibility(View.GONE);
					receivedLayout.setAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_top_in));
					setReceivedData(false);
					break;
				}
				ActivityUtils.hideSoftInput(mActivity);
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void initData() {
		inflater = LayoutInflater.from(mActivity);
		mCtnr = (Ctnr) getIntent().getSerializableExtra("ctnrs");
		entry_id = getIntent().getStringExtra("entry_id");
		setTitleString("CTNR: " + mCtnr.getNumber());
		stagingCon = readStaging();
		if (stagingCon == null)
			stagingCon = new ArrayList<Staging>();
	}

	private String getCachePath() {
		String cacheName = "staging.cache";
		cacheName = "en_" + entry_id + ".cache";
		return Goable.file_cache_path + cacheName;
	}

	private void initView() {
		inLayout = (LinearLayout) findViewById(R.id.inLayout);
		liLayout = (LinearLayout) findViewById(R.id.liLayout);
		scanOkBtn = (Button) findViewById(R.id.scanOkBtn);
		scanCancelBtn = (Button) findViewById(R.id.scanCancelBtn);
		areaBtn = (Button) findViewById(R.id.areaBtn);
		mQTYEt = (EditText) findViewById(R.id.mQTYEt);
		mWeightEt = (EditText) findViewById(R.id.mWeightEt);
		mWeightLay = (LinearLayout) findViewById(R.id.mWeightLay);
		mWeightLay.setVisibility(View.GONE);
		mQTYLay = (LinearLayout) findViewById(R.id.mQTYLay);
		mQTYLay.setVisibility(View.GONE);
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		ctnrLv = (ListView) findViewById(R.id.ctnrLv);
		receivedLv = (ListView) findViewById(R.id.receivedLv);
		etPallet = (SearchEditText) findViewById(R.id.etPallet);
		receiveLayout = (LinearLayout) findViewById(R.id.receiveLayout);
		receivedLayout = (LinearLayout) findViewById(R.id.receivedLayout);
		palletTotalTv = (TextView) findViewById(R.id.palletTotalTv);
		boxTotalTv = (TextView) findViewById(R.id.boxTotalTv);
		weightTotalTv = (TextView) findViewById(R.id.weightTotalTv);
		boxTotalLay = (LinearLayout) findViewById(R.id.boxTotalLay);
		weightTotalLay = (LinearLayout) findViewById(R.id.weightTotalLay);
		areaBtn.setOnClickListener(this);
		scanOkBtn.setOnClickListener(this);
		scanCancelBtn.setOnClickListener(this);
		ctnrLv.setEmptyView(findViewById(R.id.nodataTv));
		ctnrLv.setOnItemClickListener(this);
		receivedLv.setOnItemClickListener(this);
		receivedLv.setEmptyView(findViewById(R.id.nodata2Tv));
		btnRight.setVisibility(View.VISIBLE);
		btnRight.setBackgroundResource(R.drawable.print_all_style);
		btnRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				print();
			}
		});
		adddate(SearchReceiveActivity.jsonObject);
	}

	EditText qtyEt;
	Button plTrueBtn, plNoBtn;
	private Spinner spinner;
	private RewriteBuilderDialog rewriteBuilderDialog;

	private void print() {
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Print Plate");
		View view = LayoutInflater.from(mActivity).inflate(R.layout.act_receive_doordk_dialog, null);
		qtyEt = (EditText) view.findViewById(R.id.edittext_na);
		qtyEt.setInputType(EditorInfo.TYPE_CLASS_PHONE);
		plTrueBtn = (Button) view.findViewById(R.id.sure);
		plTrueBtn.setOnClickListener(printOnClick);
		plNoBtn = (Button) view.findViewById(R.id.cancle);
		plNoBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rewriteBuilderDialog.dismiss();
			}
		});
		spinner = (Spinner) (view).findViewById(R.id.my_spinner);
		setSpinner();
		builder.isHideCancelBtn(true);
		// builder.hideCancelBtn(true);
		builder.setContentView(view);
		rewriteBuilderDialog = builder.create();
		rewriteBuilderDialog.show();
		qtyEt.setFocusable(true);
		qtyEt.setFocusableInTouchMode(true);
		qtyEt.requestFocus();
	}

	private OnClickListener printOnClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Utility.colseInputMethod(mActivity, qtyEt);
			if (!TextUtils.isEmpty(qtyEt.getText().toString())) {
				if (!TextUtils.isEmpty(spValue)) {
					if (Integer.parseInt(qtyEt.getText().toString()) < 20) {

						donumRequest(qtyEt.getText().toString());
					} else if (Integer.parseInt(qtyEt.getText().toString()) > 20 && numtype == true) {
						donumRequest(qtyEt.getText().toString());
						numtype = false;
					} else {
						numjudge();
					}
				} else {
					UIHelper.showToast(mActivity, getString(R.string.sync_print_empty), Toast.LENGTH_SHORT).show();
				}

			} else {
				UIHelper.showToast(mActivity, getString(R.string.sync_input_empty), Toast.LENGTH_SHORT).show();
			}
		}
	};

	private void donumRequest(String count) {
		final RequestParams params = new RequestParams();
		params.add("Method", "EntryCtnrPrintPlateLabel");
		params.add("entry_id", entry_id);
		params.add("dlo_detail_id", mCtnr.getDlo_detail_id());
		params.add("count", count);
		params.add("print_server_id", spValueId);
		Log.e("update date", "entryId  " + entry_id + ",count  " + count + ",spValueId  " + spValueId);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				Log.e("json ", json.toString());
				int spRet = StringUtil.getJsonInt(json, "ret");
				if (spRet == 1) {
					rewriteBuilderDialog.dismiss();
				}

			}

		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, mActivity);

	}

	boolean numtype = false;

	public boolean numjudge() {

		RewriteBuilderDialog.Builder numbuilder = new RewriteBuilderDialog.Builder(mActivity);
		numbuilder.setTitle(getString(R.string.sync_notice));
		// Digital it determines whether the input is greater than 20 ?
		numbuilder.setMessage("Is Print " + qtyEt.getText().toString().trim() + " Lables?");
		numbuilder.setPositiveButton(getResources().getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				numtype = true;

			}
		});
		numbuilder.setNegativeButton(getResources().getString(R.string.sync_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				numtype = false;

			}
		});
		numbuilder.create().show();

		return numtype;
	}

	// 打印机型号 对应的ID
	private String spValue, spValueId;

	// 添加机器号到下拉列表
	public void setSpinner() {
		final ArrayAdapter<String> adapterd = new ArrayAdapter<String>(this, R.layout.checkin_spinner_layout_for_size, printList);

		adapterd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapterd);
		spinner.setSelection(-1, true);
		// 添加事件Spinner事件监听
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				// TODO Auto-generated method stub
				spValue = machinelists.get(arg2).getPrinter_server_name();
				spValueId = machinelists.get(arg2).getPrinter_server_id();
				Log.e("selectr spinner ", "spValue" + spValue + "spValueId" + spValueId);
				// spValue=adapterd.getItem(arg2).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
		// 设置默认值

		spinner.setVisibility(View.VISIBLE);
	}

	private List<PrintServer> machinelists;

	ArrayList<String> printList = new ArrayList<String>();
	int defaultvalue;

	public void adddate(JSONObject json) {

		// spinner 数据
		machinelists = PrintPackMainBeanModel.parsePrintServer(json);
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json, "printserver");
		defaultvalue = StringUtil.getJsonInt(jo, "default");
		Log.e("default vliue", "" + defaultvalue);
		printList = new ArrayList<String>();
		if (machinelists == null) {
			return;
		}
		for (int i = 0, sercount = machinelists.size(); i < sercount; i++) {

			printList.add(machinelists.get(i).getPrinter_server_name());
		}
	}

	// 监听获取选择的下拉列表的值
	public class SpinnerSelectedListener implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			// spValue = spinneradapter.getItem(arg2).toString();
			spValue = machinelists.get(arg2).getPrinter_server_name();
		}

		public void onNothingSelected(AdapterView<?> arg0) {

		}
	}

	private boolean isShowScanView = false; // 是否在扫描界面

	private void showScanView() {
		liLayout.setVisibility(View.GONE);
		inLayout.setVisibility(View.VISIBLE);
		inLayout.setAnimation(AnimationUtils.loadAnimation(this, R.anim.push_top_in));
		isShowScanView = true;

		// 第一个为空， 第二个有值
		if (menu_qtyCb.isChecked() && mSetupReceive.isNABox && menu_weightCb.isChecked() && !mSetupReceive.isNAWeight) {
			mWeightEt.setText(mSetupReceive.getWeight());
			mQTYEt.setText("");
			mQTYEt.requestFocus();
		}

		// 第一个有值， 第二个为空
		if (menu_qtyCb.isChecked() && !mSetupReceive.isNABox && menu_weightCb.isChecked() && mSetupReceive.isNAWeight) {
			mQTYEt.setText(mSetupReceive.getBox_qty());
			mWeightEt.setText("");
			mWeightEt.requestFocus();
		}

		// 第一个禁用， 第二个为空
		if (!menu_qtyCb.isChecked() && menu_weightCb.isChecked() && mSetupReceive.isNAWeight) {
			mQTYEt.setText("");
			mWeightEt.setText("");
			mWeightEt.requestFocus();
		}

		// 第一个为空， 第二个禁用
		if (!menu_weightCb.isChecked() && menu_qtyCb.isChecked() && mSetupReceive.isNABox) {
			mQTYEt.setText("");
			mWeightEt.setText("");
			mQTYEt.requestFocus();
		}

		// 两个都为空
		if (menu_qtyCb.isChecked() && menu_weightCb.isChecked() && mSetupReceive.isNAWeight && mSetupReceive.isNABox) {
			mQTYEt.setText("");
			mWeightEt.setText("");
			mQTYEt.requestFocus();
		}

		if (!mSetupReceive.getArea().equals("NA"))
			areaBtn.setText(mSetupReceive.getArea());
	}

	private void showListView() {
		liLayout.setVisibility(View.VISIBLE);
		inLayout.setVisibility(View.GONE);
		inLayout.setAnimation(AnimationUtils.loadAnimation(this, R.anim.push_top_out));
		isShowScanView = false;
		etPallet.setText("");
		Utility.colseInputMethod(mActivity, etPallet);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.scanOkBtn:
			scanOkBtnOnClick(newReceived);
			break;
		case R.id.scanCancelBtn:
			TTS.getInstance().speakAll("Cancel");
			showListView();
			break;
		case R.id.areaBtn:
			showSelectStagingDialog(AreaType.ADD, newReceived);
			break;
		}
	}

	private void scanOkBtnOnClick(Received received) {
		// box QTY
		if (menu_qtyCb.isChecked()) {
			String qtyStr = mQTYEt.getText().toString().trim();
			if (TextUtils.isEmpty(qtyStr) || qtyStr.equals("0")) {
				UIHelper.showToast(mActivity, getString(R.string.sync_input_qty), Toast.LENGTH_SHORT).show();
				return;
			}
			received.setBox_number(qtyStr);
			if (!menu_weightCb.isChecked())
				received.setWeight("NA");
		}

		// weight
		if (menu_weightCb.isChecked()) {
			String weightStr = mWeightEt.getText().toString().trim();
			if (TextUtils.isEmpty(weightStr) || weightStr.equals("0")) {
				UIHelper.showToast(mActivity, getString(R.string.sync_input_weight), Toast.LENGTH_SHORT).show();
				return;
			}
			if (!isDoubleValue(weightStr))
				return; // Double验证
			received.setWeight(weightStr);
			if (!menu_qtyCb.isChecked())
				received.setBox_number("NA");
		}

		// System.out.println("received= "+received.getArea());
		// System.out.println("mSetupReceive= "+mSetupReceive.getArea());

		// Staging Location
		if (!isNA(areaBtn.getText().toString().trim()))
			received.setArea(areaBtn.getText().toString().trim());
		// if (!isNA(mSetupReceive.getArea())) {
		// received.setArea(mSetupReceive.getArea());
		// }
		// if (!isNA(received.getArea()) && isNA(mSetupReceive.getArea())) {
		// received.setArea(received.getArea());
		// }
		scanSuccess(received);
		ctnrAdapter.notifyDataSetChanged();
		showListView();
		TTS.getInstance().speakAll("OK");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			showSettingMenu();
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			showSettingMenu();
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			showSettingMenu();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void showSettingMenu() {
		settingDialog.show();
		isSetting = true;
	}

	Button menu_areaBtn;
	CheckBox menu_qtyCb;
	CheckBox menu_weightCb;

	private RewriteBuilderDialog createSettingMenu() {
		final RewriteBuilderDialog.Builder settingBuilder = new RewriteBuilderDialog.Builder(mActivity);
		settingBuilder.setTitle("Setting");
		settingBuilder.setCancelable(false);
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.menu_recrive, null);
		final EditText menu_qtyEt = (EditText) view.findViewById(R.id.menu_qtyEt);
		final EditText menu_weightEt = (EditText) view.findViewById(R.id.menu_weightEt);
		menu_qtyCb = (CheckBox) view.findViewById(R.id.menu_qtyCb);
		menu_weightCb = (CheckBox) view.findViewById(R.id.menu_weightCb);

		menu_qtyCb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					menu_qtyEt.setEnabled(true);
					menu_qtyEt.setBackgroundDrawable(getResources().getDrawable(R.drawable.search_bar_edit_selector));
					menu_qtyEt.setTextColor(Color.BLACK);
					menu_qtyEt.requestFocus();
					mQTYLay.setVisibility(View.VISIBLE);
					boxTotalLay.setVisibility(View.VISIBLE);
				} else {
					// QTY按钮禁用状态
					menu_qtyEt.setEnabled(false);
					menu_qtyEt.setBackgroundColor(0xFFDBDBDB);
					menu_qtyEt.setText("");
					menu_qtyEt.setTextColor(Color.GRAY);
					mQTYLay.setVisibility(View.GONE);
					boxTotalLay.setVisibility(View.GONE);
					mSetupReceive.isNABox = true;
					mSetupReceive.setBox_qty("NA");
				}
			}
		});

		menu_weightCb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					menu_weightEt.setEnabled(true);
					menu_weightEt.setBackgroundDrawable(getResources().getDrawable(R.drawable.search_bar_edit_selector));
					menu_weightEt.setTextColor(Color.BLACK);
					menu_weightEt.requestFocus();
					mWeightLay.setVisibility(View.VISIBLE);
					weightTotalLay.setVisibility(View.VISIBLE);
				} else {
					menu_weightEt.setEnabled(false);
					menu_weightEt.setBackgroundColor(0xFFDBDBDB);
					menu_weightEt.setText("");
					menu_weightEt.setTextColor(Color.GRAY);
					mWeightLay.setVisibility(View.GONE);
					weightTotalLay.setVisibility(View.GONE);
					mSetupReceive.isNAWeight = true;
					mSetupReceive.setWeight("NA");
				}
			}
		});

		menu_areaBtn = (Button) view.findViewById(R.id.menu_areaBtn);
		menu_areaBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showSelectStagingDialog(AreaType.SETUP, null);
			}
		});
		settingBuilder.setContentView(view);
		settingBuilder.isHideCancelBtn(true);
		settingBuilder.setPositiveButton(getString(R.string.sync_done), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int which) {
				String weightStr = menu_weightEt.getText().toString().trim();
				settingBuilder.setPositiveButtonOnClickDismiss(isDoubleValue(weightStr));
				// 获取box值
				String boxStr = menu_qtyEt.getText().toString().trim();
				mSetupReceive.isNABox = TextUtils.isEmpty(boxStr);
				mSetupReceive.setBox_qty(mSetupReceive.isNABox ? "NA" : boxStr);
				// 获取weight值
				mSetupReceive.isNAWeight = TextUtils.isEmpty(weightStr);
				mSetupReceive.setWeight(mSetupReceive.isNAWeight ? "NA" : weightStr);

				isSetting = false;
			}
		});
		return settingBuilder.create();
	}

	private boolean isDoubleValue(String weightStr) {
		if (isNA(weightStr)) {
			return true;
		}
		try {
			Double.parseDouble(weightStr);
		} catch (Exception e) {
			UIHelper.showToast(mActivity, getString(R.string.sync_weight_error), Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private void toItemFinish() {
		if (receivedList.isEmpty()) {
			TTS.getInstance().speakAll_withToast("No Data!");
			return;
		}
		if (!dataAll.isEmpty()) {
			TTS.getInstance().speakAll_withToast(MSG_ERROR);
			return;
		}
		if (isShowScanView) {
			TTS.getInstance().speakAll_withToast(MSG_ERROR);
			return;
		}

		Intent intent = new Intent(mActivity, ReceivedFinishActivity.class);
		intent.putExtra("pallet", palletTotalTv.getText().toString().trim());
		intent.putExtra("box", boxTotalTv.getText().toString().trim());
		intent.putExtra("weight", weightTotalTv.getText().toString().trim());
		intent.putExtra("entry_id", entry_id);
		intent.putExtra("mCtnr", mCtnr);
		startActivity(intent);
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key0 = TTPKey.getReceiveKey(mCtnr.getDlo_detail_id(), 0);
		params.add(new TabParam("Pallets", key0, new PhotoCheckable(0, "Pallets", ttp)));
		params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing+"", FileWithTypeKey.OCCUPANCY_MAIN+"", entry_id);
		return params;
	}

	private void setReceivedData(final boolean showMenu) {
		RequestParams params = new RequestParams();
		params.add("Method", "ReceivedSumSangDetail");
		params.add("dlo_detail_id", mCtnr.getDlo_detail_id());
		params.add("ic_id", mCtnr.getIc_id());
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				String resultStr = json.toString();
				System.out.println("ReceivedSumSangDetail= " + resultStr);
				// StringUtil.getJsonInt(json, "totalbox");

				Gson gson = new Gson();
				JSONArray stagingArray = StringUtil.getJsonArrayFromJson(json, "staging");
				stagingAll = gson.fromJson(stagingArray.toString(), new TypeToken<List<Staging>>() {
				}.getType());

				JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "datas");
				receivedList = gson.fromJson(dataArray.toString(), new TypeToken<List<Received>>() {
				}.getType());
				for (Received r : receivedList) {
					r.setArea(getAreaName(r.getArea()));
				}
				Collections.reverse(receivedList);
				receivedAdapter = new ReceivedAdapter(receivedList);
				receivedLv.setAdapter(receivedAdapter);
				// getTabNum();

				ctnrAdapter = new CtnrAdapter();
				ctnrLv.setAdapter(ctnrAdapter);
				setPalletTotal();
				receivedLv.setSelection(receivedPosition);

				if (showMenu) {
					etPallet.setSeacherMethod(new SeacherMethod() {
						@Override
						public void seacher(String value) {
							search(value);
						}
					});
					etPallet.setOnKeyListener(new View.OnKeyListener() {
						@Override
						public boolean onKey(View v, int keyCode, KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
								String value = etPallet.getText().toString().trim();
								search(value);
								return true;
							}
							return false;
						}
					});
					// 初始化菜单
					settingDialog = createSettingMenu();
					showSettingMenu();
				}
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Network Error!");
				mActivity.finish();
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	/**
	 * SelectBar上的红色徽章
	 * 
	 * @param value
	 */
	// private void getTabNum() {
	// badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
	// badge.setText(receivedList == null ? 0 + "" : receivedList.size() + "");
	// badge.show();
	// }

	private void search(String value) {
		if (TextUtils.isEmpty(value)) {
			return;
		}

		newReceived = new Received(value);

		if (!isRepeat(value)) {
			scan(value);
			if (value.length() > 4)
				TTS.getInstance().speakAll(StringUtil.addSpace(value.substring(value.length() - 4, value.length())));
			else
				TTS.getInstance().speakAll(StringUtil.addSpace(value));
		} else {
			TTS.getInstance().speakAll_withToast("Repeat!");
			etPallet.setText("");
			Utility.colseInputMethod(mActivity, etPallet);
		}
	}

	private void scan(String value) {
		if (isAutoScan()) {
			autoScan(value);
		} else {
			showScanView();
		}
	}

	/**
	 * 1. 两个都禁用 auto
	 * 
	 * 2. 两个都有值 auto
	 * 
	 * 3. 第一个有值， 第二个禁用 auto
	 * 
	 * 4. 第一个禁用， 第二个有值 auto
	 * 
	 * 5. 第一个禁用， 第二个为空
	 * 
	 * 6. 第一个为空， 第二个禁用
	 * 
	 * 7. 第一个为空， 第二个有值
	 * 
	 * 8. 第一个有值， 第二个为空
	 * 
	 * 9. 两个都为空
	 */
	private boolean isAutoScan() {
		if (menu_qtyCb.isChecked() && !menu_weightCb.isChecked()) {
			return !mSetupReceive.isNABox;
		}
		if (!menu_qtyCb.isChecked() && menu_weightCb.isChecked()) {
			return !mSetupReceive.isNAWeight;
		}
		if (menu_qtyCb.isChecked() && menu_weightCb.isChecked()) { // 都选中
			return !(mSetupReceive.isNABox || mSetupReceive.isNAWeight);
		}
		if (!(menu_qtyCb.isChecked() && menu_weightCb.isChecked())) { // 都没选中
			return true;
		}
		return false;
	}

	private void autoScan(String value) {
		String str = "";
		str += "Box QTY= " + mSetupReceive.getBox_qty() + "\n";
		str += "Weight= " + mSetupReceive.getWeight() + "\n";
		str += "Area= " + mSetupReceive.getArea();
		System.out.println(str);

		newReceived.setBox_number(mSetupReceive.getBox_qty());
		newReceived.setWeight(mSetupReceive.getWeight());
		newReceived.setArea(mSetupReceive.getArea());

		scanSuccess(newReceived);
		ctnrAdapter.notifyDataSetChanged();
		etPallet.setText("");
		Utility.colseInputMethod(mActivity, etPallet);
	}

	Button update_areaBtn;
	EditText update_qtyEt, update_weightEt;

	private void showSelectQty(final AreaType type, final Received received) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Pallet: " + received.getPallet_number());
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_select_qty, null);
		final LinearLayout mWeightLay = (LinearLayout) view.findViewById(R.id.mWeightLay);
		final LinearLayout mQTYLay = (LinearLayout) view.findViewById(R.id.mQTYLay);
		mQTYLay.setVisibility(menu_qtyCb.isChecked() ? View.VISIBLE : View.GONE);
		mWeightLay.setVisibility(menu_weightCb.isChecked() ? View.VISIBLE : View.GONE);
		update_qtyEt = (EditText) view.findViewById(R.id.mQTYEt);
		update_weightEt = (EditText) view.findViewById(R.id.mWeightEt);
		Utility.endEtCursor(update_qtyEt);
		Utility.endEtCursor(update_weightEt);
		update_areaBtn = (Button) view.findViewById(R.id.areaBtn);
		update_areaBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showSelectStagingDialog(type, received);
			}
		});
		builder.setContentView(view);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.isHideCancelBtn(false);
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int which) {
				String qtyStr = update_qtyEt.getText().toString().trim();
				if (menu_qtyCb.isChecked()) {
					if (TextUtils.isEmpty(qtyStr) || qtyStr.equals("0")) {
						return;
					}
					received.setBox_number(qtyStr);
				}
				String weightStr = update_weightEt.getText().toString().trim();
				if (menu_weightCb.isChecked()) {
					if (TextUtils.isEmpty(weightStr) || weightStr.equals("0")) {
						return;
					}
					received.setWeight(weightStr);
				}
				if (type == AreaType.SUBMIT_UPDATE) {
					submitUpdate(received);
				} else {
					setPalletTotal();
					ctnrAdapter.notifyDataSetChanged();
				}
				dialog.dismiss();
			}
		});
		builder.create().show();
		if (type == AreaType.UPDATE) {
			update_areaBtn.setText(received.getArea());
			update_qtyEt.setText(isNA(received.getBox_number()) ? "0" : received.getBox_number());
			update_weightEt.setText(isNA(received.getWeight()) ? "0" : received.getWeight());
			Utility.endEtCursor(update_qtyEt);
		}
		if (type == AreaType.SUBMIT_UPDATE) {
			update_qtyEt.setText(isNA(received.getBox_number()) ? "0" : received.getBox_number());
			update_weightEt.setText(isNA(received.getWeight()) ? "0" : received.getWeight());
			update_areaBtn.setText(received.getArea());
			Utility.endEtCursor(update_qtyEt);
		}
	}

	private boolean isNA(String str) {
		return str == null || str.length() == 0 || str.equals("NA");
	}

	private void submitUpdate(Received received) {
		RequestParams params = new RequestParams();
		params.add("Method", "UpdatePallet");
		if (!isNA(received.getBox_number()))
			params.add("box_number", received.getBox_number());
		if (!isNA(received.getWeight()))
			params.add("weight", received.getWeight());
		params.add("icp_id", received.getIcp_id());
		params.add("area", getAreaId(received.getArea()));
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				setReceivedData(false);
				TTS.getInstance().speakAll_withToast("Update Success!");
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	private void scanSuccess(Received received) {
		Collections.reverse(dataAll);
		dataAll.add(received);
		Collections.reverse(dataAll);
		ctnrAdapter.notifyDataSetChanged();
		etPallet.requestFocus();

		setPalletTotal();
	}

	private void setPalletTotal() {
		// PalletTotal
		String total = dataAll.size() + receivedList.size() + "";
		palletTotalTv.setText(total);

		// BoxTotal
		int qty = 0;
		for (Received r : dataAll) {
			if (!isNA(r.getBox_number())) {
				qty += Integer.parseInt(r.getBox_number());
			}
		}
		for (Received r : receivedList) {
			if (!isNA(r.getBox_number())) {
				qty += Integer.parseInt(r.getBox_number());
			}
		}
		boxTotalTv.setText(qty + "");

		// WeightTotal
		double weight = 0;
		for (Received r : dataAll) {
			if (!isNA(r.getWeight())) {
				try {
					weight += Double.parseDouble(r.getWeight());
				} catch (Exception e) {
				}
			}
		}
		for (Received r : receivedList) {
			if (!isNA(r.getWeight())) {
				try {
					weight += Double.parseDouble(r.getWeight());
				} catch (Exception e) {
				}
			}
		}
		weightTotalTv.setText(String.format("%.2f", weight));

	}

	private boolean isRepeat(String value) {
		for (Received bean : dataAll) {
			if (bean.getPallet_number().equals(value)) {
				return true;
			}
		}
		for (Received bean : receivedList) {
			if (bean.getPallet_number().equals(value)) {
				return true;
			}
		}
		return false;
	}

	public void submitOnClick(View v) {
		if (dataAll.isEmpty()) {
			TTS.getInstance().speakAll_withToast("No Data");
			return;
		}
		List<Received> rds = getNullErrorBeans();
		if (rds == null || rds.size() == 0) {
			doSubmit();
		} else {
			showNoSelectDialog(rds);
		}
	}

	public void finshOnClick(View v) {
		toItemFinish();
	}

	private void showNoSelectDialog(final List<Received> rds) {
		final RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
		String[] pnNos = new String[rds.size()];
		for (int i = 0, length = rds.size(); i < length; i++) {
			pnNos[i] = rds.get(i).getPallet_number();
		}
		View layout = LayoutInflater.from(mActivity).inflate(R.layout.dlg_receive_noselect, null);
		ListView lv = (ListView) layout.findViewById(R.id.noselectLv);

		DialogAdapter adapter = new DialogAdapter(rds);
		lv.setAdapter(adapter);
		LayoutParams lp = lv.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		if (rds.size() >= 3)
			lp.height = (dm.heightPixels / 3);
		lv.setLayoutParams(lp);

		dialog.setTitle(" Select Staging Location");
		dialog.setContentView(layout);
		dialog.setPositiveButtonOnClickDismiss(false);
		dialog.isHideCancelBtn(false);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int which) {
				showSelectStagingDialog(AreaType.SUBMIT, null);
				dialog.dismiss();
			}
		});
		dialog.create().show();
	}

	private void showSelectStagingDialog(final AreaType type, final Received rd) {
		final Dialog dialog = new Dialog(mActivity);
		final View layout = LayoutInflater.from(mActivity).inflate(R.layout.dlg_item_select_staging, null);
		final ListView lv = (ListView) layout.findViewById(R.id.selectLv);
		final SingleSelectBar ssb = (SingleSelectBar) layout.findViewById(R.id.selectBar);
		final LinearLayout clearBtn = (LinearLayout) layout.findViewById(R.id.clearBtn);
		stagingAdapter = new StagingAdapter(stagingAll);
		lv.setAdapter(stagingAdapter);
		initDialogSelectBar(ssb, clearBtn);
		LayoutParams lp = lv.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		if (stagingAll.size() >= 3)
			lp.height = (dm.heightPixels / 2);
		lv.setLayoutParams(lp);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(layout);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Staging staging = ssb.getCurrentSelectItem().b == 0 ? stagingCon.get(position) : stagingAll.get(position);
				if (type == AreaType.ADD) {
					rd.setArea(staging.getLocation_name());
					areaBtn.setText(rd.getArea());
				}
				if (type == AreaType.UPDATE) {
					update_areaBtn.setText(staging.getLocation_name());
					rd.setArea(staging.getLocation_name());
					ctnrAdapter.notifyDataSetChanged();
				}
				if (type == AreaType.SUBMIT) {
					setRds(position, ssb.getCurrentSelectItem());
					submitPallets();
				}
				if (type == AreaType.SUBMIT_UPDATE) {
					update_areaBtn.setText(staging.getLocation_name());
					rd.setArea(staging.getLocation_name());
				}
				if (type == AreaType.SETUP) {
					menu_areaBtn.setText(staging.getLocation_name());
					areaBtn.setText(staging.getLocation_name());
					mSetupReceive.setArea(staging.getLocation_name());
				}

				if (ssb.getCurrentSelectItem().b != 0) {
					if (!isStagingCon(staging)) {
						Collections.reverse(stagingCon);
						stagingCon.add(staging);
						Collections.reverse(stagingCon);
					}
				}

				saveStaging(stagingCon);
				dialog.dismiss();
			}
		});

		if (!stagingCon.isEmpty()) {
			clearBtn.setVisibility(View.VISIBLE);
		} else {
			clearBtn.setVisibility(View.GONE);
		}
		clearBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				stagingCon.clear();
				FileUtils.writeFile(getCachePath(), "[]");
				stagingAdapter.setStagingList(stagingCon);
				stagingAdapter.notifyDataSetChanged();
				clearBtn.setVisibility(View.GONE);
			}
		});
	}

	private boolean isStagingCon(Staging staging) {
		if (!stagingCon.isEmpty()) {
			for (int i = 0; i < stagingCon.size(); i++) {
				if (stagingCon.get(i).getLocation_id().equals(staging.getLocation_id())) {
					return true;
				}
			}
		}
		return false;
	}

	private void initDialogSelectBar(SingleSelectBar ssb, final LinearLayout clearBtn) {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Recently", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("All", 1));
		ssb.setUserDefineClickItems(clickItems);
		ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					stagingAdapter.setStagingList(stagingCon);
					stagingAdapter.notifyDataSetChanged();
					if (!stagingCon.isEmpty()) {
						clearBtn.setVisibility(View.VISIBLE);
					} else {
						clearBtn.setVisibility(View.GONE);
					}
					break;
				case 1:
					stagingAdapter.setStagingList(stagingAll);
					stagingAdapter.notifyDataSetChanged();
					clearBtn.setVisibility(View.GONE);
					break;
				}
			}
		});
		ssb.userDefineSelectIndexExcuteClick(stagingCon == null ? 1 : (stagingCon.isEmpty() ? 1 : 0));
	}

	private void setRds(int which, HoldDoubleValue<String, Integer> value) {
		Staging staging = value.b == 0 ? stagingCon.get(which) : stagingAll.get(which);
		List<Integer> positions = getNullErrorPositions();
		for (Integer index : positions) {
			dataAll.get(index).setArea(staging.getLocation_name());
		}
	}

	private void doSubmit() {
		if (super.check()) {
			RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
			dialog.setTitle(getString(R.string.sync_notice));
			dialog.setMessage(getString(R.string.sync_submit)+"?");
			dialog.setNegativeButton(R.string.sync_cancel, null);
			dialog.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					submitPallets();
				}
			});
			dialog.create().show();
		}
	}

	private void submitPallets() {
		RequestParams params = new RequestParams();
		params.add("Method", "SubmitSumSang");
		params.add("jsonInfo", changeDataToJson());
		ttp.uploadZip(params, "ReceiveDetailPhoto");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("json= " + json.toString());

				dataAll.clear();
				ctnrAdapter.notifyDataSetChanged();
				ttp.clearData();
				setReceivedData(false);

				TTS.getInstance().speakAll_withToast("Submit Success!");
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Submit Failed!");
			}
		}.doPost(HttpUrlPath.ReceiveWorkAction, params, this);

	}

	private List<Integer> getNullErrorPositions() {
		List<Integer> positions = new ArrayList<Integer>();
		for (int i = 0; i < dataAll.size(); i++) {
			String areaStr = dataAll.get(i).getArea();
			if (TextUtils.isEmpty(areaStr) || areaStr.equals(Received.AREA_DEF)) {
				positions.add(i);
			}
		}
		return positions;
	}

	private List<Received> getNullErrorBeans() {
		List<Integer> positions = getNullErrorPositions();
		if (positions.isEmpty()) {
			return null;
		}

		List<Received> rds = new ArrayList<Received>();
		for (int i = 0; i < positions.size(); i++) {
			rds.add(dataAll.get(positions.get(i)));
		}

		return rds;
	}

	int receivedPosition = 0;

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch (parent.getId()) {
		case R.id.ctnrLv:
			showSelectQty(AreaType.UPDATE, dataAll.get(position));
			break;

		case R.id.receivedLv:
			if (!dataAll.isEmpty()) {
				TTS.getInstance().speakAll_withToast(MSG_ERROR);
				return;
			}
			if (isShowScanView) {
				TTS.getInstance().speakAll_withToast(MSG_ERROR);
				return;
			}
			receivedPosition = position;
			showSelectQty(AreaType.SUBMIT_UPDATE, receivedList.get(position));
			break;
		}
	}

	public String changeDataToJson() {
		try {
			JSONObject json = new JSONObject();
			json.put("entry_id", entry_id);
			json.put("dlo_detail_id", mCtnr.getDlo_detail_id());
			json.put("ic_id", mCtnr.getIc_id());

			JSONArray array = new JSONArray();
			for (int i = 0, length = dataAll.size(); i < length; i++) {
				Received bean = dataAll.get(i);
				JSONObject beanObject = new JSONObject();
				beanObject.put("pallet_number", bean.getPallet_number());
				beanObject.put("area", getAreaId(bean.getArea()));
				if (!isNA(bean.getBox_number()))
					beanObject.put("box_number", bean.getBox_number());
				if (!isNA(bean.getWeight()))
					beanObject.put("weight", bean.getWeight());
				array.put(beanObject);
			}
			json.put("pallets", array);
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("ready");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		if (!dataAll.isEmpty()) {
			TTS.getInstance().speakAll_withToast(MSG_ERROR);
			return;
		}
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ttp.clearCache();
				ReceiveDetailActivity.super.onBackBtnOrKey();
			}
		});
	}

	private List<Staging> readStaging() {
		StringBuilder sb = FileUtils.readFile(getCachePath(), "UTF-8");
		if (sb != null) {
			return new Gson().fromJson(sb.toString(), new TypeToken<List<Staging>>() {
			}.getType());
		}
		return null;
	}

	private void saveStaging(List<Staging> ss) {
		Gson gson = new Gson();
		String jsonStr = gson.toJson(ss);
		FileUtils.writeFile(getCachePath(), jsonStr);
	}

	class CtnrAdapter extends BaseAdapter {

		public CtnrAdapter() {
		}

		@Override
		public int getCount() {
			return dataAll == null ? 0 : dataAll.size();
		}

		@Override
		public Object getItem(int position) {
			return dataAll.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			CtnrHoder holder = null;
			if (convertView == null) {
				holder = new CtnrHoder();
				convertView = inflater.inflate(R.layout.item_receive_detail, null);
				holder.pnTv = (TextView) convertView.findViewById(R.id.pnTv);
				holder.deleteBtn = (Button) convertView.findViewById(R.id.deleteBtn);
				holder.areaTv = (TextView) convertView.findViewById(R.id.areaTv);
				holder.boxTv = (TextView) convertView.findViewById(R.id.qtyTv);
				holder.weightTv = (TextView) convertView.findViewById(R.id.weightTv);
				holder.boxLay = (LinearLayout) convertView.findViewById(R.id.boxLay);
				holder.weightLay = (LinearLayout) convertView.findViewById(R.id.weightLay);
				convertView.setTag(holder);
			} else {
				holder = (CtnrHoder) convertView.getTag();
			}

			final Received rd = dataAll.get(position);
			holder.pnTv.setText(rd.getPallet_number());
			holder.boxTv.setText(rd.getBox_number());
			holder.weightTv.setText(rd.getWeight());
			holder.areaTv.setText(rd.getArea());

			holder.deleteBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
					dialog.setTitle(getString(R.string.sync_notice));
					dialog.setMessage(getString(R.string.sync_delete_notify) + Html.fromHtml("<font color='red'>" + rd.getPallet_number() + "?</font>").toString());
					dialog.setPositiveButton(getString(R.string.tms_delete), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dataAll.remove(position);
							notifyDataSetChanged();
							setPalletTotal();
						}
					});
					dialog.create().show();
				}
			});

			return convertView;
		}

		class CtnrHoder {
			public TextView pnTv;
			public Button deleteBtn;
			public TextView areaTv, boxTv, weightTv;
			public LinearLayout boxLay, weightLay;

		}

	}

	class ReceivedAdapter extends BaseAdapter {

		List<Received> receivedDatas;

		public ReceivedAdapter(List<Received> receivedDatas) {
			this.receivedDatas = receivedDatas;
		}

		@Override
		public int getCount() {
			return receivedDatas == null ? 0 : receivedDatas.size();
		}

		@Override
		public Object getItem(int position) {
			return receivedDatas.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ReceivedHoder holder = null;
			if (convertView == null) {
				holder = new ReceivedHoder();
				convertView = inflater.inflate(R.layout.item_received_detail, null);
				holder.pnTv = (TextView) convertView.findViewById(R.id.pnTv);
				holder.areaTv = (TextView) convertView.findViewById(R.id.areaTv);
				holder.qtyTv = (TextView) convertView.findViewById(R.id.qtyTv);
				holder.weightTv = (TextView) convertView.findViewById(R.id.weightTv);
				convertView.setTag(holder);
			} else {
				holder = (ReceivedHoder) convertView.getTag();
			}

			final Received r = receivedDatas.get(position);
			holder.pnTv.setText(r.getPallet_number());
			holder.areaTv.setText(r.getArea());
			holder.qtyTv.setText(TextUtils.isEmpty(r.getBox_number()) ? "NA" : r.getBox_number());
			holder.weightTv.setText(TextUtils.isEmpty(r.getWeight()) ? "NA" : r.getWeight());

			return convertView;
		}

		class ReceivedHoder {
			public TextView pnTv;
			public TextView areaTv;
			public TextView qtyTv;
			public TextView weightTv;
		}
	}

	class DialogAdapter extends BaseAdapter {

		List<Received> receivedDatas;

		public DialogAdapter(List<Received> rds) {
			this.receivedDatas = rds;
		}

		@Override
		public int getCount() {
			return receivedDatas == null ? 0 : receivedDatas.size();
		}

		@Override
		public Object getItem(int position) {
			return receivedDatas.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ReceivedHoder holder = null;
			if (convertView == null) {
				holder = new ReceivedHoder();
				convertView = inflater.inflate(R.layout.dlg_item_received_detail, null);
				holder.pnTv = (TextView) convertView.findViewById(R.id.pnTv);
				convertView.setTag(holder);
			} else {
				holder = (ReceivedHoder) convertView.getTag();
			}

			final Received r = receivedDatas.get(position);
			holder.pnTv.setText(r.getPallet_number());

			return convertView;
		}

		class ReceivedHoder {
			public TextView pnTv;
		}
	}

	public class StagingAdapter extends BaseAdapter {

		private List<Staging> stagingList;

		public StagingAdapter(List<Staging> stagings) {
			stagingList = stagings;
		}

		@Override
		public int getCount() {
			return stagingList == null ? 0 : stagingList.size();
		}

		@Override
		public Object getItem(int position) {
			return stagingList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			CheckedTextView ctv = (CheckedTextView) inflater.inflate(R.layout.simple_dialog_item_single_choice, null);
			ctv.setText(stagingList.get(position).getLocation_name());
			return ctv;
		}

		public List<Staging> getStagingList() {
			return stagingList;
		}

		public void setStagingList(List<Staging> stagingList) {
			this.stagingList = stagingList;
		}

	}

	private int getIndex(String[] array, String s) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(s)) {
				return i;
			}
		}
		return 0;
	}

	private String getAreaId(String areaName) {
		for (int i = 0, length = stagingAll.size(); i < length; i++) {
			if (stagingAll.get(i).getLocation_name().equals(areaName)) {
				return stagingAll.get(i).getLocation_id();
			}
		}
		return "";
	}

	private String getAreaName(String areaId) {
		for (int i = 0, length = stagingAll.size(); i < length; i++) {
			if (stagingAll.get(i).getLocation_id().equals(areaId)) {
				return stagingAll.get(i).getLocation_name();
			}
		}
		return "Exception Location";
	}

	// public static OnStateListener getOnStateListener() {
	// return onStateListener;
	// }
	//
	// public static void setOnStateListener(OnStateListener onStateListener) {
	// ReceiveDetailActivity.onStateListener = onStateListener;
	// }
	//
	// private static OnStateListener onStateListener;
	//
	// interface OnStateListener {
	// public void onState(Ctnr ctnr);
	// }
}
