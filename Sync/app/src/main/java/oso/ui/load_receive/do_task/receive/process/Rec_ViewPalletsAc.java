package oso.ui.load_receive.do_task.receive.process;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class Rec_ViewPalletsAc extends BaseActivity {

	private ExpandableListView exLv;
	private Button printBtn;

	protected static List<Rec_ItemBean> lines;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_rec_view_pallets, 0);
		// initData();
		applyParams();
		setTitleString("Receipt: " + rnNO);

		initView();
		setData();
	}

	// private void initData() {
	// setTitleString("View Pallets");
	// lines = (List<Rec_ItemBean>)
	// getIntent().getExtras().getSerializable("lines");
	// }

	private void initView() {
		exLv = (ExpandableListView) findViewById(R.id.exLv);
		printBtn = (Button) findViewById(R.id.printBtn);
		
		//debug
		exLv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				//禁止-group展开
				return true;
			}
		});
	}

	private void setData() {
		exLv.setAdapter(adapter);
		exLv.setGroupIndicator(null);
		for (int i = 0; i < lines.size(); i++) {
			exLv.expandGroup(i);
		}
		printBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String ids = getPltIDs();
		        
		        if(TextUtils.isEmpty(ids)){
		        	UIHelper.showToast(getString(R.string.sync_chose_print));
		        	return;
		        }
				printTool.showDlg_printers(printLs);
			}
		});
		
		
	}

	private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs = new PrintTool.OnPrintLs() {
		public void onPrint(long printServerID) {
			reqPrintPlts(printServerID);
		}
	};

    private void reqPrintPlts(long printServerID) {
        String ids = getPltIDs();
        
        RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, ids);
        new SimpleJSONUtil() {
            public void handReponseJson(JSONObject json) {
                String name = StoredData.getUsername();
                for (int i = 0; i < lines.size(); ++i) {
                    Rec_ItemBean bean = lines.get(i);
                    for (Rec_PalletBean b : bean.pallets) {
                        if(b.isPrintSelected) {
                            b.print_user_name = name;
                            b.isPrintSelected = false;
                        }
                    }
                }
                adapter.notifyDataSetChanged();
                UIHelper.showToast(getString(R.string.sync_print_success));
            }
        }.doGet(NetInterface.recPrintLP_url(), p, mActivity);
    }
    
	public static String getPltIDs() {
		if (Utility.isEmpty(lines))
			return "";
		String ret = "";
		for (Rec_ItemBean bean : lines) {
			for (Rec_PalletBean b : bean.pallets) {
				if (b.isPrintSelected)
					ret = ret + b.con_id + ",";
			}
		}

		if (!TextUtils.isEmpty(ret)) {
			ret = ret.substring(0, ret.length() - 1);
		}

		return ret;
	}

	// =========传参===============================

	private String rnNO;

	public static void initParams(Intent in, List<Rec_ItemBean> listItems, String rnNO) {
		in.putExtra("lines", (Serializable) listItems);
		in.putExtra("rnNO", rnNO);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		lines = (List<Rec_ItemBean>) params.getSerializable("lines");
		rnNO = params.getString("rnNO");
	}

	// ==================nested===============================

	private final ExAdapter adapter = new ExAdapter();

	private final class ExAdapter extends BaseExpandableListAdapter {

		@Override
		public int getGroupCount() {
			return lines == null ? 0 : lines.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return lines.get(groupPosition) == null ? 0 : lines.get(groupPosition).pallets.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return null;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		Holder gHolder = null;

//		@Override
//		public void onGroupCollapsed(int groupPosition) {
//			// TODO Auto-generated method stub
////			super.onGroupCollapsed(groupPosition);
//			//防止-grpLv收缩,debug
//			exLv.expandGroup(groupPosition);
//		}
		
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = View.inflate(mActivity, R.layout.item_rec_view_pallets, null);
				gHolder = new Holder();
				gHolder.layout = convertView.findViewById(R.id.layout);
				gHolder.cbAll = (CheckBox) convertView.findViewById(R.id.cb);
				gHolder.pltTv = (TextView) convertView.findViewById(R.id.tv);
				gHolder.tvLotNo = (TextView) convertView.findViewById(R.id.tvLotNo);
				convertView.setTag(gHolder);
			} else
				gHolder = (Holder) convertView.getTag();

			final Rec_ItemBean bean = lines.get(groupPosition);
			gHolder.pltTv.setText(bean.item_id);
			gHolder.tvLotNo.setText(bean.lot_no);
			gHolder.cbAll.setChecked(isAllCheck(bean));
			// debug
			gHolder.cbAll.setVisibility(Utility.isEmpty(bean.pallets) ? View.INVISIBLE : View.VISIBLE);
			// gHolder.cbAll.setOnClickListener(new View.OnClickListener() {
			// @Override
			// public void onClick(View v) {
			// bean.isAllCheck = !bean.isAllCheck;
			// for (Rec_PalletBean b : bean.pallets) {
			// b.isPrintSelected = bean.isAllCheck;
			// }
			// notifyDataSetChanged();
			// }
			// });
			gHolder.layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					bean.isAllCheck = !bean.isAllCheck;
					for (Rec_PalletBean b : bean.pallets) {
						b.isPrintSelected = bean.isAllCheck;
					}
					notifyDataSetChanged();
				}
			});
			return convertView;
		}

		private boolean isAllCheck(Rec_ItemBean bean) {
			for (Rec_PalletBean b : bean.pallets) {
				if (!b.isPrintSelected)
					return false;
			}
			return true;
		}

		private boolean isNotAllCheck(Rec_ItemBean bean) {
			for (Rec_PalletBean b : bean.pallets) {
				if (b.isPrintSelected)
					return false;
			}
			return true;
		}

		Holder cHolder = null;

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = View.inflate(mActivity, R.layout.item_rec_view_pallets_child, null);
				cHolder = new Holder();
				cHolder.pltTv = (TextView) convertView.findViewById(R.id.pltTv);
				cHolder.cb = (CheckBox) convertView.findViewById(R.id.cb);
				cHolder.layout = convertView.findViewById(R.id.layout);
				cHolder.flagTv = (TextView) convertView.findViewById(R.id.flagTv);
				convertView.setTag(cHolder);
			} else
				cHolder = (Holder) convertView.getTag();

			final Rec_PalletBean bean = lines.get(groupPosition).pallets.get(childPosition);
			cHolder.pltTv.setText(bean.container_no);
			cHolder.cb.setChecked(bean.isPrintSelected);
			// cHolder.cb.setOnClickListener(new View.OnClickListener() {
			// @Override
			// public void onClick(View v) {
			// bean.isPrintSelected = !bean.isPrintSelected;
			// notifyDataSetChanged();
			// }
			// });
			cHolder.layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					bean.isPrintSelected = !bean.isPrintSelected;
					notifyDataSetChanged();
				}
			});
			cHolder.flagTv.setVisibility(bean.isPrinted() ? View.VISIBLE : View.GONE);
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		class Holder {
			TextView pltTv, flagTv,tvLotNo;
			CheckBox cbAll, cb;
			View layout;
		}
	}

}
