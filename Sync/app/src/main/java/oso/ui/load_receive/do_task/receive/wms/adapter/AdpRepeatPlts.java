package oso.ui.load_receive.do_task.receive.wms.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpRepeatPlts extends BaseAdapter {
	
	private Context ct;
	private List<Rec_PalletBean> listPlts;
	
	public AdpRepeatPlts(Context ct,List<Rec_PalletBean> listPlts) {
		// TODO Auto-generated constructor stub
		this.ct=ct;
		this.listPlts=listPlts;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listPlts==null?0:listPlts.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder h;
		if (convertView == null) {
			convertView=LayoutInflater.from(ct).inflate(R.layout.rec_repeatplt_item_1, null);
			h = new Holder();
			h.tvPlt_item=(TextView)convertView.findViewById(R.id.tvPlt_item);
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();
		
		//刷ui
		Rec_PalletBean b=listPlts.get(position);
		h.tvPlt_item.setText(b.getDisPltNO());
		return convertView;
	}
	
	
	class Holder{
		TextView tvPlt_item;
	}
	

}
