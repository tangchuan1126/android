package oso.ui.load_receive.print;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.print.adapter.SelectCtnBolAdapter;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.bean.ReceiptTicketBase;
import oso.ui.load_receive.print.bean.SelectCtnBolBase;
import support.common.bean.DockCheckInDoorForLoadListBase;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import declare.com.vvme.R;

/**
 * 
 * @ClassName: CheckInDemoActivity
 * @Description:
 * @author gcy
 * @date 2014年5月7日 下午4:10:35
 * 
 */
public class SelectCtnBolActivity extends Activity {

	private static Context context;
	private List<SelectCtnBolBase> selectBolBaseList;
	private ListView selectCtnBase_listview;
	private SelectCtnBolAdapter selectCtnAdapter;
	private Resources resources;
	private List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
	
	private String entryId;
 	private String door_name;
	private List<PrintServer> printServerList;
	private String htmlurl;
	private Button close_pup ;
	
	private ReceiptTicketBase receiptTicketBase;
	DockCheckInDoorForLoadListBase bean;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.printiconbar_select_ctnbol_layout);
		context = SelectCtnBolActivity.this;
		getFromDockCheckInActivityData();
		initView();
		setViewOnClick();
		setViewData();
	}
	
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
 	 */
	@SuppressWarnings("unchecked")
	protected void getFromDockCheckInActivityData(){
		Intent intent = this.getIntent(); 
		selectBolBaseList =(List<SelectCtnBolBase>) intent.getSerializableExtra("selectBolBaseList");
		receiptTicketBase = (ReceiptTicketBase) intent.getSerializableExtra("receiptTicketBase");

		entryId = intent.getStringExtra("entryId");
		door_name = intent.getStringExtra("door_name");
		htmlurl = intent.getStringExtra("htmlurl");
		printServerList = (List<PrintServer>) intent.getSerializableExtra("printServerList");
		bean = (DockCheckInDoorForLoadListBase) intent.getSerializableExtra("bean");
	}
	
	/**
	 * @Description:初始化UI控件
	 */
	private void initView(){
		close_pup = ((Button) findViewById(R.id.close_pup));
		selectCtnBase_listview = (ListView) findViewById(R.id.select_ctn_listview);
	}
	
	/**
	 * @Description:赋予UI主控件点击事件
	 */
	private void setViewOnClick(){
		close_pup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});

		selectCtnBase_listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				SelectCtnBolBase classBean = (SelectCtnBolBase) selectCtnBase_listview.getItemAtPosition(position);
				jumpToAnotherActivity(classBean);
 			}
		}); 
	}
	
	/**
	 * @Description:赋予UI主控件点击事件
	 */
	private void setViewData(){
		selectCtnAdapter = new SelectCtnBolAdapter(context, this.selectBolBaseList);
		selectCtnBase_listview.setAdapter(selectCtnAdapter);
	}
	
	private void jumpToAnotherActivity(SelectCtnBolBase classBean){
		Intent intent = new Intent();
		intent.putExtra("receiptsTicketBaseList", (Serializable)receiptsTicketBaseList);
		intent.putExtra("entryId", entryId);
 		intent.putExtra("door_name", door_name);
		intent.putExtra("htmlurl", htmlurl);
		intent.putExtra("printServerList", (Serializable) printServerList);
		intent.putExtra("bean", (Serializable)bean);
		intent.putExtra("receiptTicketBase", receiptTicketBase);
		intent.setClass(context, DockCtnBolWebViewActivity.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.push_from_left_in,R.anim.push_from_left_out);
	}
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}
}
