package oso.ui.load_receive.do_task.load.smallparcel.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import utility.Utility;
import android.content.Context;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import declare.com.vvme.R;
import declare.com.vvme.R.color;

public class ClpTypeAdapter extends BaseAdapter {

	private List<Rec_CLPType> clpTypes;
	private Context mContext;
	
	
	public ClpTypeAdapter(Context mContext, List<Rec_CLPType> datas) {
		super();
		this.clpTypes = datas;
		this.mContext = mContext;
	}

	@Override
	public int getCount() {
		return clpTypes == null ? 0 : clpTypes.size();
	}

	@Override
	public Object getItem(int position) {
		return clpTypes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		OHolder holder = null;
		if (convertView == null) {
			holder = new OHolder();
			convertView = View.inflate(mContext, R.layout.item_cc_clptypes, null);
			holder.carrierTv = (TextView) convertView.findViewById(R.id.carrierTv);
			holder.shipToTv = (TextView) convertView.findViewById(R.id.shipToTv);
			holder.ship = (TextView) convertView.findViewById(R.id.ship);
			holder.box = (CheckBox) convertView.findViewById(R.id.box);
			holder.configTv = (TextView) convertView.findViewById(R.id.configTv);
			convertView.setTag(holder);
		} else {
			holder = (OHolder) convertView.getTag();
		}

		final Rec_CLPType user = clpTypes.get(position);
		//debug
		String str2 = user.stack_length_qty+"×"+user.stack_width_qty+"×"+user.stack_height_qty;
    	holder.carrierTv.setText(user.lp_name);
    	holder.configTv.setText("Config.: "+str2);
		holder.box.setChecked(user.isSelect);
		if(TextUtils.isEmpty(user.ship_to_names)){
			holder.shipToTv.setVisibility(View.GONE);
			holder.ship.setVisibility(View.GONE);
		}else{
			holder.shipToTv.setVisibility(View.VISIBLE);
			holder.ship.setVisibility(View.VISIBLE);
			holder.shipToTv.setText(" "+user.ship_to_names);
		}

		return convertView;
	}
	
	private class OHolder {
		public TextView carrierTv;
		public TextView shipToTv;
		public TextView ship;
		public CheckBox box;
		public TextView configTv;
	}

}
