package oso.ui.load_receive.do_task.receive.task.movement;

import java.util.ArrayList;

import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoLinesBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_TaskBean;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

public class Rec_Movement_ListAc extends BaseActivity {
	
	private ListView lv;
	
	List<Rec_Movement_TaskBean> listTask = new ArrayList<Rec_Movement_TaskBean>();
	
	private static final int Rec_From_Assign = 0x2111;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_rec_movement_listac, 0);
		
		lv = (ListView) findViewById(R.id.lv);
		lv.setAdapter(movementAdapter);
		
		getDefaultData();
		setData();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode != RESULT_OK)
			return;
		
		if(requestCode == Rec_From_Assign){
			getDefaultData();
		}
	}
	
	private void setData(){
		setTitleString("Movement Info");
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Rec_Movement_TaskBean bean = listTask.get(position);
				
				reqMovementTask(bean);
			}
		});
	}
	
	private void reqMovementTask(Rec_Movement_TaskBean taskBean){
		RequestParams p = new RequestParams();
	    p.add("ps_id", StoredData.getPs_id());
	    p.add("receipt_id", taskBean.receipt_id+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				String object = json.optJSONObject("data").toString();
				Rec_Movement_InfoBean infoBean = new Gson().fromJson(object,Rec_Movement_InfoBean.class);
				String array = json.optJSONObject("data").optJSONArray("lines").toString();
				List<Rec_Movement_InfoLinesBean> linesBean = new Gson().fromJson(array, new TypeToken<List<Rec_Movement_InfoLinesBean>>(){}.getType());
				
				Intent in = new Intent(mActivity,Rec_Movement_Assign.class);
				Rec_Movement_Assign.initParams(in, infoBean, linesBean,Rec_Movement_Assign.To_Finish);
				startActivityForResult(in, Rec_From_Assign);
			}
		}.setCancelable(false).doGet(
				HttpUrlPath.basePath + "_receive/android/acquireFinallyPallets", p, this);
	}
	
	private void getDefaultData(){
		RequestParams params = new RequestParams();
		params.add("adid", StoredData.getAdid());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
				listTask = new Gson().fromJson(json.optJSONArray("data").toString(), new TypeToken<List<Rec_Movement_TaskBean>>(){}.getType());
				
				movementAdapter.notifyDataSetChanged();
				
				//若无task,则返回
				if(Utility.isEmpty(listTask))
					finish();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/findMovementTaskList", params, mActivity);
	}
	
	//-------------------------------------
	
	private BaseAdapter movementAdapter = new BaseAdapter() {
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h;
			if (convertView == null) {
				convertView=getLayoutInflater().inflate(R.layout.item_rec_movementtasklistac, null);
				h = new Holder();
				h.tvReceipt=(TextView)convertView.findViewById(R.id.tvReceipt);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();
			
			Rec_Movement_TaskBean bean = listTask.get(position);
			h.tvReceipt.setText(bean.receipt_no);
			
			return convertView;
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public Object getItem(int position) {
			return listTask.get(position);
		}
		
		@Override
		public int getCount() {
			return listTask == null ? 0 : listTask.size();
		}
		
	};
	private class Holder{
		TextView tvReceipt;
	}
}
