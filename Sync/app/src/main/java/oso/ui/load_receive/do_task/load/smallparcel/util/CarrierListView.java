package oso.ui.load_receive.do_task.load.smallparcel.util;

import java.util.List;

import oso.ui.load_receive.do_task.load.smallparcel.adapter.CarrierAdapter;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_ShipToBean;
import oso.widget.orderlistview.SideBar;
import oso.widget.orderlistview.SideBar.OnTouchingLetterChangedListener;
import utility.Utility;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

public class CarrierListView extends FrameLayout implements OnTouchingLetterChangedListener, OnScrollListener, OnItemClickListener {

	private TextView letterTv; // 显示点击的字母
	private SideBar sideBar; // ListView右侧导航选项卡
	private ListView lv;
	private CarrierAdapter adapter;

	private List<Rec_ShipToBean> users = null;

	public CarrierListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public CarrierListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public CarrierListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context);
	}

	private void init(Context context) {
		lv = new ListView(context);
		lv.setBackgroundColor(Color.TRANSPARENT);
		lv.setCacheColorHint(Color.TRANSPARENT);
		// lv.setSelector(null);
		// 右侧导航条初始化
		sideBar = new SideBar(context);
		FrameLayout.LayoutParams sbParams = new FrameLayout.LayoutParams(Utility.pxTodip(getContext(), 30), -1, Gravity.RIGHT);
		// 中间的字母控件初始化
		letterTv = new TextView(context);
		letterTv.setMinWidth(Utility.pxTodip(context, 90));
		letterTv.setMinHeight(Utility.pxTodip(context, 90));
		letterTv.setBackgroundColor(0x40000000);
		letterTv.setTextColor(Color.WHITE);
		letterTv.setTypeface(Typeface.MONOSPACE);
		letterTv.setTextSize(Utility.pxTosp(context, 24));
		letterTv.setVisibility(View.GONE);
		letterTv.setGravity(Gravity.CENTER);
		// 添加View
		FrameLayout.LayoutParams ltParams = new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER);
		addView(lv, -1, -1);
		addView(sideBar, sbParams);
		addView(letterTv, ltParams);
		// 设置监听事件
		lv.setOnScrollListener(this);
		lv.setOnItemClickListener(this);
		sideBar.setOnTouchingLetterChangedListener(this);// 右侧导航条监听
		// initDataHandler.sendEmptyMessage(0);
	}

	public void setAdapter(List<Rec_ShipToBean> dataAll) {
		if (dataAll == null || dataAll.size() == 0) {
			return;
		}
		users = dataAll;
		initDataHandler.sendEmptyMessage(0);
	}

	private Handler initDataHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (adapter == null) {
				adapter = new CarrierAdapter(getContext(), users);
				lv.setAdapter(adapter);
			}
			adapter.notifyDataSetChanged();
		}

	};

	private Handler _handler = new Handler();
	private Runnable letterThread = new Runnable() {
		public void run() {
			letterTv.setVisibility(View.GONE);
		}
	};

	@Override
	public void onTouchingLetterChanged(String s) {
		if (s.equals("A")) {
			lv.setSelection(0);
			showLetter(s);
		}
		if (users == null) {
			return;
		}
		if (alphaIndexer(s) > 0) {
			int position = alphaIndexer(s);
			lv.setSelection(position);
			showLetter(s);
		}
	}

	private void showLetter(String s) {
		letterTv.setText(s);
		letterTv.setVisibility(View.VISIBLE);
		_handler.removeCallbacks(letterThread);
		_handler.postDelayed(letterThread, 1000);
	}

	// 返回滑动条所指位置的索引
	public int alphaIndexer(String s) {
		int position = 0;
		//debug
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).upperCase().equalsIgnoreCase(s)) {
				position = i;
				break;
			}
		}
		return position;
	}

	public int getSelectedItemPosition() {
		return lv.getSelectedItemPosition();
	}

	public void setSelection(int position) {
		lv.setSelection(position);
	}

	public void setEmptyView(View emptyView) {
		lv.setEmptyView(emptyView);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (getOnItemClickListener() != null)
			getOnItemClickListener().onItemClick(parent, view, position, id, users.get(position));
	}

	boolean isStateChanged = false;

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:// 空闲状态
			isStateChanged = false;
			break;
		case OnScrollListener.SCROLL_STATE_FLING:// 滚动状态
			isStateChanged = true;
			break;
		case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:// 触摸后滚动
			isStateChanged = true;
			break;
		}
	}

	@Override
	public void onScroll(AbsListView view, final int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				if (isStateChanged) {
					//debug
					showLetter(users.get(firstVisibleItem).upperCase());
				}
			}
		});
	}

	public OnItemOrderClickListener getOnItemClickListener() {
		return onItemClickListener;
	}

	public void setOnItemClickListener(OnItemOrderClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	private OnItemOrderClickListener onItemClickListener;

	public interface OnItemOrderClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id, Rec_ShipToBean letter);
	}
}
