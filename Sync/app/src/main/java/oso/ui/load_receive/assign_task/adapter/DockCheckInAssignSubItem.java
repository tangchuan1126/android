package oso.ui.load_receive.assign_task.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class DockCheckInAssignSubItem extends LinearLayout {

	public View chose_layout;
	public CheckBox chose_child_door_checkbox;
	public TextView child_door_name;
	public TextView number_status;
	public TextView execute_user;
	public View dashed_line;

	public DockCheckInAssignSubItem(Context context) {
		super(context);
		initDockDetail(context);
	}

	public DockCheckInAssignSubItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		initDockDetail(context);
	}

	private void initDockDetail(Context context) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dock_checkin_assign_child_item,this);
		chose_layout = (View) view.findViewById(R.id.chose_layout);
		chose_child_door_checkbox = (CheckBox) view.findViewById(R.id.chose_child_door_checkbox);
		child_door_name = (TextView) view.findViewById(R.id.child_door_name);
		number_status = (TextView) view.findViewById(R.id.number_status);
		execute_user = (TextView) view.findViewById(R.id.execute_user);
		dashed_line = (View) view.findViewById(R.id.dashed_line);
	}

	//-----------------------------------------------------------------------------------
	
	
	
	

	
}
