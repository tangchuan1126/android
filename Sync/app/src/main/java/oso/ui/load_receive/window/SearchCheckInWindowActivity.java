package oso.ui.load_receive.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryMainBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.DlgEditView.OnPromptListener;
import oso.widget.dlgeditview.adapter.SimpleDlgAdapter;
import oso.widget.dlgeditview.adapter.SimpleDlgAdapter.OnDlgItemListener;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.CheckInTaskMainItem;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SearchCheckInWindowActivity extends BaseActivity {

	private SingleSelectBar singleSelectBar;
	private SearchEditText entryEt, orderEt;
	private EditText ctnrEt;

	// private LinearLayout entrylay;
	// private TextView entryTv;
	private TextView any_data;

	private ListView entryLv;
	private EntryAdapter entryAdapter;

	// private List<TaskBean> taskBean;
	// private String searchvalue;

	private static int searchtype;
	private final int SEARCH_ENTRY = 1;
	private final int SEARCH_CTNR = 2;
	private final int SEARCH_LOAD = 3;

	public static final int REQUEST_CODE = 10001;

	public static boolean isRefreshDatas = false;

	private List<EntryMainBean> mEntryList = new ArrayList<EntryMainBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_search_check_window, 0);
		initView();
		initSingleSelectBar();
		setData();
	}

	String entry_id;// 刷新后传回来的entryId
	EntryMainBean entry; // 刷新后用到的entry

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
//		if (REQUEST_CODE == requestCode && REQUEST_CODE == resultCode) {
//			if (data.getBooleanExtra("flag", false) && isRefreshDatas) {
//				isRefreshDatas = false;
//				entry_id = data.getStringExtra("entry_id");
//				if (entry_id != null && !TextUtils.isEmpty(entry_id)) {
//					requestToServer(true);
//				}
//			}
//		}
//		if (REQUEST_CODE == requestCode && EquipmentListActivity.RESULT_CODE == resultCode) {
//			if (data.getBooleanExtra("refresh", false)) {
				requestToServer(false);
//			}
//		}
	}

	private void requestToServer(final boolean refresh) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInList);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {

				mEntryList = EntryMainBean.helpJson(json);
				EntryAdapter adapter = new EntryAdapter(mEntryList);
				entryLv.setAdapter(adapter);
				if (refresh) {
					if (!isEntryList()) {
						getTaskData(entry_id);
					}
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private boolean isEntryList() {
		for (EntryMainBean bean : mEntryList) {
			if (bean.entry_id.equals(entry_id)) {
				entry = bean;
				return true;
			}
		}
		return false;
	}

	private void requestToWindowCheckInEntryEquipmentList(final EntryMainBean entry) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInEntryEquipmentList);
		params.add("entry_id", entry.entry_id + "");
		// Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInEntryEquipmentList= " + json.toString());
				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "datas");
				Gson gson = new Gson();
				List<EquipmentBean> mEquipmentList = gson.fromJson(ja.toString(), new TypeToken<List<EquipmentBean>>() {
				}.getType());
				if (mEquipmentList == null || mEquipmentList.isEmpty()) {
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent = new Intent(mActivity, EquipmentListActivity.class);
				intent.putExtra("mEntry", entry);
				intent.putExtra("mEquipmentList", (Serializable) mEquipmentList);
				startActivityForResult(intent, REQUEST_CODE);
			}

			@Override
			public void handFail() {
				UIHelper.showToast(mActivity, getString(R.string.sync_failed), Toast.LENGTH_SHORT).show();
			};
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	/**
	 * @Description:初始化主Ui的控件
	 */
	private void initView() {
		setTitleString(getString(R.string.window_checkin_title));
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestToServer(false);
			}
		});

		entryLv = (ListView) findViewById(R.id.entryLv);
		// entrylay = (LinearLayout) findViewById(R.id.entrylay);
		any_data = (TextView) findViewById(R.id.any_data);
		// entryTv = (TextView) findViewById(R.id.task_entry);
		entryEt = (SearchEditText) findViewById(R.id.search);
		orderEt = (SearchEditText) findViewById(R.id.orderEt);
		entryEt.setScanMode();
		ctnrEt = (EditText) findViewById(R.id.sv);
		entryLv.setEmptyView(any_data);
		entryLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				EntryMainBean entry = (EntryMainBean) mEntryList.get(position);
				requestToWindowCheckInEntryEquipmentList(entry);
			}
		});
	}

	/**
	 * 初始化SingleSelectBar
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.sync_entryid), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.sync_ctnr), 1));
		// clickItems.add(new HoldDoubleValue<String, Integer>("Order NO", 2));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					entryEt.requestFocus();
					entryEt.setHint(R.string.entry_id);
					entryEt.setVisibility(View.VISIBLE);
					ctnrEt.setVisibility(View.GONE);
					orderEt.setVisibility(View.GONE);
					any_data.setText(R.string.check_in_scan_entry_id);
					searchtype = SEARCH_ENTRY;
					break;
				case 1:
					// ctnrEt.getInputEt().requestFocus();
					ctnrEt.setHint(R.string.ctnr);
					ctnrEt.setVisibility(View.VISIBLE);
					entryEt.setVisibility(View.GONE);
					orderEt.setVisibility(View.GONE);
					any_data.setText(R.string.check_in_scan_ctnr);
					searchtype = SEARCH_CTNR;
					break;
				case 2:
					orderEt.requestFocus();
					orderEt.setHint("Order No");
					orderEt.setVisibility(View.VISIBLE);
					entryEt.setVisibility(View.GONE);
					ctnrEt.setVisibility(View.GONE);
					any_data.setText("Please Scan The Order NO");
					searchtype = SEARCH_LOAD;
					break;
				}
				entryEt.setText("");
				ctnrEt.setText("");
				orderEt.setText("");
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void setData() {
		requestToServer(false);

		// ctnrEt.getInputEt().setOnKeyListener(new View.OnKeyListener() {
		// @Override
		// public boolean onKey(View v, int keyCode, KeyEvent event) {
		// if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() ==
		// KeyEvent.ACTION_UP) {
		// String value = ctnrEt.getInputEt().getText().toString().trim();
		// getTaskData(value);
		// Utility.colseInputMethod(mActivity, entryEt);
		// return true;
		// }
		// return false;
		// }
		// });
		// ctnrEt.setOnSearchListener(new OnSearchListener() {
		// @Override
		// public void onSearch(String value) {
		// getTaskData(value);
		// Utility.colseInputMethod(mActivity, entryEt);
		// }
		// });

		initDlgEditView();

		entryEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getTaskData(value);
				Utility.colseInputMethod(mActivity, entryEt);
			}
		});

		entryAdapter = new EntryAdapter(mEntryList);
		entryLv.setAdapter(entryAdapter);
	}

	private void initDlgEditView() {
		final SimpleDlgAdapter adapter = new SimpleDlgAdapter(mActivity, ctnrEt);

		DlgEditView.attachOnFocus(ctnrEt, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v, "Search").setMethod("SearchEquipment").setHint("Search CTNR").setDefKeyboard(true)
						.setAdp(adapter).setPromptOnClick(new OnPromptListener() {
							@Override
							public void onClick(String value) {
								getTaskData(value);
								Utility.colseInputMethod(mActivity, entryEt);
							}
						});
			}
		}, false);
		adapter.setOnDlgItemListener(new OnDlgItemListener() {
			@Override
			public void onClick(String value) {
				getTaskData(value);
				Utility.colseInputMethod(mActivity, entryEt);
			}
		});
	}

	/**
	 * @Description:获取数据
	 * @param imgId
	 */
	private void getTaskData(final String searchvalue) {
		if (!StringUtil.isNullOfStr(searchvalue)) {
			RequestParams params = new RequestParams();
			params.add("Method", "WindowCheckInSearchEntry");
			params.add("searchvalue", searchvalue.toUpperCase());
			params.add("searchtype", searchtype + "");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					// JSONArray ja = StringUtil.getJsonArrayFromJson(json,
					// "datas");
					// Gson gson = new Gson();
					// mEntryList = gson.fromJson(ja.toString(), new
					// TypeToken<List<EntryBean>>() {
					// }.getType());
					mEntryList = EntryMainBean.helpJson(json);
					if (!mEntryList.isEmpty()) {
						entryAdapter = new EntryAdapter(mEntryList);
						entryLv.setAdapter(entryAdapter);
					} else {
						UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
					}
					entryAdapter.notifyDataSetChanged();
					// if (refresh) {
					// if (isEntryList()) {
					// requestToWindowCheckInEntryEquipmentList(entry);
					// }
					// }
				}
			}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
		} else {
			UIHelper.showToast(mActivity, getString(R.string.task_entryid_null), Toast.LENGTH_SHORT).show();
		}

		entryEt.setText("");
		entryEt.requestFocus();
	}

	class EntryAdapter extends BaseAdapter {

		private List<EntryMainBean> entryList;

		public EntryAdapter(List<EntryMainBean> mEntryList) {
			entryList = mEntryList;
		}

		@Override
		public int getCount() {
			return entryList == null ? 0 : entryList.size();
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return entryList.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_entry_list, null);
				holder = new Holder();
				holder.entryTv = (TextView) convertView.findViewById(R.id.entryTv);
				holder.userTv = (TextView) convertView.findViewById(R.id.userTv);
				holder.gateinTv = (TextView) convertView.findViewById(R.id.gateinTv);
				holder.addLinear = (LinearLayout) convertView.findViewById(R.id.addLinear);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			EntryMainBean bean = entryList.get(position);

			holder.entryTv.setText("E" + bean.entry_id);
			holder.userTv.setText(bean.gate_driver_name);
			holder.gateinTv.setText("Gate In: " + bean.gate_check_in_time);

			addLinear(bean, holder.addLinear);

			return convertView;
		}

		private void addLinear(EntryMainBean bean, LinearLayout addLinear) {
			if (addLinear != null) {
				addLinear.removeAllViews();
			}

			for (int i = 0; i < bean.complexDoorList.size(); i++) {
				CheckInTaskMainItem b = new CheckInTaskMainItem();
				b = bean.complexDoorList.get(i);
				View view = ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_cheakin_main_layout,
						null);
				ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView1);
				TextView tractor_txt = (TextView) view.findViewById(R.id.tractor_txt);
				TextView door_txt = (TextView) view.findViewById(R.id.door_txt);
				View dashed_line = (View) view.findViewById(R.id.dashed_line);
				dashed_line.setVisibility(View.GONE);
				door_txt.setVisibility(View.GONE);
				tractor_txt.setText(b.equipment_number);
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
					imageView1.setImageResource(R.drawable.ic_tractor);
				}
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
					imageView1.setImageResource(R.drawable.ic_trailer);
				}
				addLinear.addView(view);
			}
		}

		private class Holder {
			TextView entryTv;
			TextView userTv;
			TextView gateinTv;
			LinearLayout addLinear;
		}
	}

}
