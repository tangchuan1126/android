package oso.ui.load_receive.do_task.receive.samsung;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.receive.samsung.bean.Ctnr;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class ReceivedExceptionActivity extends BaseActivity {

	private TextView palletTotalTv, boxTotalTv, weightTotalTv;
	private LinearLayout boxTotalLay, weightTotalLay;
	private TabToPhoto ttp;
	private EditText desEt;

	private Ctnr mCtnr;
	String palletStr, boxStr, weightStr, entry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_receive_exception, 0);
		initData();
		initView();
		setData();
	}

	private void initData() {
		palletStr = getIntent().getStringExtra("pallet");
		boxStr = getIntent().getStringExtra("box");
		weightStr = getIntent().getStringExtra("weight");
		entry_id = getIntent().getStringExtra("entry_id");
		mCtnr = (Ctnr) getIntent().getSerializableExtra("mCtnr");
		setTitleString("Recevid Exception");
	}

	private void initView() {
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		palletTotalTv = (TextView) findViewById(R.id.palletTotalTv);
		boxTotalTv = (TextView) findViewById(R.id.boxTotalTv);
		weightTotalTv = (TextView) findViewById(R.id.weightTotalTv);
		boxTotalLay = (LinearLayout) findViewById(R.id.boxTotalLay);
		weightTotalLay = (LinearLayout) findViewById(R.id.weightTotalLay);
		desEt = (EditText) findViewById(R.id.desEt);
	}

	private void setData() {
		ttp.init(mActivity, getTabParamList());
		boxTotalLay.setVisibility(View.VISIBLE);
		weightTotalLay.setVisibility(View.VISIBLE);
		palletTotalTv.setText(palletStr);
		boxTotalTv.setText(boxStr);
		weightTotalTv.setText(weightStr);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ttp.onResume(getTabParamList());
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key0 = TTPKey.getReceiveExceptionKey(mCtnr.getDlo_detail_id(), 0);
		params.add(new TabParam("Processing", key0, new PhotoCheckable(0, "Processing", ttp)));
		params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing+"", FileWithTypeKey.OCCUPANCY_MAIN+"", entry_id);
		return params;
	}

	public void submitOnClick(View v) {
		if (desEt.getText().toString().trim().equals("")) {
			UIHelper.showToast(mActivity, getString(R.string.sync_input_reason_result), Toast.LENGTH_SHORT).show();
			return;
		}
		RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
		dialog.setTitle(getString(R.string.sync_notice));
		dialog.setMessage(getString(R.string.sync_submit)+"?");
		dialog.setCancelable(false);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				submitException();
			}
		});
		dialog.create().show();
	}

	private void submitException() {
		RequestParams params = new RequestParams();
		params.add("Method", "ReceivedException");
		params.add("reason", desEt.getText().toString().trim());
		params.add("entry_id", entry_id);
		params.add("dlo_detail_id", mCtnr.getDlo_detail_id());
		params.add("ic_id", mCtnr.getIc_id());
		params.add("equipment_id", mCtnr.getEquipment_id());
		params.add("resources_id", mCtnr.getResources_id());
		params.add("resources_type", mCtnr.getResources_type());
		ttp.uploadZip(params, "ReceivedExceptionPhoto");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("json= " + json.toString());
				RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
				dialog.setTitle(getString(R.string.sync_notice));
				dialog.setMessage(getString(R.string.sync_success));
				dialog.isHideCancelBtn(true);
				dialog.setPositiveButton(getString(R.string.checkout_finish), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(mActivity, CheckInTaskDoorItemActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intent.putExtra("refresh", true);
						startActivity(intent);
					}
				});
				dialog.create().show();
				ttp.clearData();
				desEt.setText("");
				TTS.getInstance().speakAll("Submit Success!");
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Submit Failed!");
			}
		}.doPost(HttpUrlPath.ReceiveWorkAction, params, this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onBackBtnOrKey() {
		ttp.clearCache();
		super.onBackBtnOrKey();
	}
}
