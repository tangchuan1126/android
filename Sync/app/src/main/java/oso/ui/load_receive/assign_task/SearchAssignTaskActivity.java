package oso.ui.load_receive.assign_task;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.bean.EntryTaskMainBean;
import oso.ui.load_receive.assign_task.bean.TaskBean;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.bean.CheckInTaskMainItem;
import support.common.bean.DockCheckInBase;
import support.common.bean.DockEquipmentBaseBean;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: DockCheckInActivity
 * @Description: 通过EntryId 查询获取指派任务列表 或者 设备列表 跳转页面根据返回的JSON值select_equipment 来判断
 *               如果为1则跳转到设备列表 否则为指派任务列表
 * @author gcy
 * @date 2014-12-1 下午12:41:01
 */
public class SearchAssignTaskActivity extends BaseActivity {

	private SearchEditText searchEt;
	private ListView lv;
	private List<TaskBean> taskList;
	private List<EntryTaskMainBean> entryTaskList;
	private TextView task_entry;
	private String entryValue;
	private TextView any_data;
	private LinearLayout entrylay;

	public static final int AssignTaskListActivity = 232;
	
	private boolean showEntryList = true;//判断时显示entry列表 还是equipment列表 true为entry列表
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.assign_task_list_layout, 0);
		initView();
		getDefaultData();
	}

	/**
	 * @Description:初始化主Ui的控件
	 */
	private void initView() {
		setTitleString(getString(R.string.assign_task_title));
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getDefaultData();
			}
		});
		
		task_entry = (TextView) findViewById(R.id.task_entry);

		searchEt = (SearchEditText) findViewById(R.id.search);

		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
		searchEt.setScanMode();
		searchEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					getTaskData(searchEt.getText().toString());
					Utility.colseInputMethod(mActivity, searchEt);
					return true;
				}
				return false;
			}
		});
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getTaskData(value);
				Utility.colseInputMethod(mActivity, searchEt);
			}
		});
		lv = (ListView) findViewById(R.id.listview);
		entrylay = (LinearLayout) findViewById(R.id.entrylay);
		any_data = (TextView) findViewById(R.id.any_data);
		lv.setEmptyView(any_data);
	}

	private void getEquipmentData(final TaskBean bean) {
		if (!StringUtil.isNullOfStr(bean.entry_id)) {
			RequestParams params = new RequestParams();
			params.add("Method", "AssginTaskDetailByEntryAndEquipment");
			params.add("entry_id", bean.entry_id);
			params.add("equipment_id", bean.equipment_id + "");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					DockCheckInBase n = DockCheckInBase.parsingJSON(json, bean.entry_id);
					jumpToAllocationActivity(n, bean.entry_id, bean);
				}
			}.doPost(HttpUrlPath.AndroidAssignTaskAction, params, mActivity);
		}
	}

	/**
	 * @Description:根据条件跳转到entry所对应的类型
	 * @param flag
	 */
	protected void jumpToAllocationActivity(DockCheckInBase n, String checkin_id, TaskBean bean) {
		if (n == null) {
			UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
			return;
		}
		DockEquipmentBaseBean dBean = new DockEquipmentBaseBean();
		dBean.check_in_entry_id = bean.entry_id;
		dBean.equipment_id = bean.equipment_id;
		dBean.equipment_number = bean.equipment_number;
		dBean.equipment_type_value = CheckInTractorOrTrailerTypeKey.getContainerTypeKeyValue(mActivity,bean.equipment_type);
		dBean.equipment_type = bean.equipment_type;
		dBean.total_task = bean.total_task;

		Intent intent = new Intent(mActivity, AssignTaskActivity.class);
		intent.putExtra("dockCheckInBase", (Serializable) n);
		intent.putExtra("dlo_id", checkin_id);
		intent.putExtra("equipment", (Serializable) dBean);
		startActivityForResult(intent, AssignTaskListActivity);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	/**
	 * @Description:进入系统后默认的加载方法
	 * @param
	 */
	public void getDefaultData(){
		RequestParams params = new RequestParams();
		params.add("Method", "AssignTaskList");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				showEntryList = true;
				entryTaskList = EntryTaskMainBean.helpJson(json);

				task_entry.setText("");
				task_entry.setVisibility(View.GONE);
				entrylay.setVisibility(View.GONE);
				EntryTaskAdapter adapter = new EntryTaskAdapter(mActivity, entryTaskList);
				lv.setAdapter(adapter);
				lv.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						EntryTaskMainBean bean = entryTaskList.get(position);
						getTaskData(bean.entry_id);
					}
				});
			}
			@Override
			public void handFail(){
				showEntryList = true;
			}
			
		}.doPost(HttpUrlPath.AndroidAssignTaskAction, params, mActivity);		
	}
	
	
	/**
	 * @Description:获取tree的数据列表
	 * @param imgId
	 */
	private void getTaskData(final String entry_id) {
		entryValue = "";
		if (!StringUtil.isNullOfStr(entry_id)) {
			entryValue = entry_id;
			RequestParams params = new RequestParams();
			params.add("Method", "AssginTaskListByEntry");
			params.add("entry_id", entry_id);
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					showEntryList = false;
					taskList = TaskBean.helpJson(json);
					if (!Utility.isNullForList(taskList)) {
						entryValue = entry_id;
						task_entry.setText("Entry ID: " + entryValue);
						entrylay.setVisibility(View.VISIBLE);
						task_entry.setVisibility(View.VISIBLE);
						
						AssignTaskAdapter adapter = new AssignTaskAdapter(mActivity,taskList);
						lv.setAdapter(adapter);
						lv.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
								TaskBean bean = taskList.get(position);
								getEquipmentData(bean);
							}
						});
					} else {
						any_data.setText(getString(R.string.sync_nodatas));
						task_entry.setText("");
						task_entry.setVisibility(View.GONE);
						entrylay.setVisibility(View.GONE);
					}
				}
			}.doPost(HttpUrlPath.AndroidAssignTaskAction, params, mActivity);
		} else {
			UIHelper.showToast(mActivity, getString(R.string.task_entryid_null), Toast.LENGTH_SHORT).show();
		}

		searchEt.setText("");
		searchEt.requestFocus();
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		if(showEntryList){
			finish();
			overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
		}else{
			getDefaultData();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (AssignTaskListActivity == requestCode && AssignTaskListActivity == resultCode) {
			if (data.getBooleanExtra("refresh", false)) {
				if(StringUtil.isNullOfStr(entryValue)){
					getDefaultData();
				}else{
					getTaskData(entryValue);
				}
			}
		}
	}
	
	// ==================nested===============================

	class AssignTaskAdapter extends BaseAdapter {

		private List<TaskBean> list;
		public AssignTaskAdapter(Context context,List<TaskBean> arrayList) {
			super();
			this.list = arrayList;
		}
		
		@Override
		public int getCount() {
			return list == null ? 0 : list.size();
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.assign_task_list_item_layout, null);
				holder = new Holder();
//				holder.equipment_type = (TextView) convertView.findViewById(R.id.equipment_type);
				holder.equipment_number = (TextView) convertView.findViewById(R.id.equipment_number);
				holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
				holder.rel_type = (TextView) convertView.findViewById(R.id.rel_type);
				holder.assign_task = (TextView) convertView.findViewById(R.id.assign_task);
				holder.assign_alltask = (TextView) convertView.findViewById(R.id.assign_alltask);
				holder.total_tasks = (TextView) convertView.findViewById(R.id.total_tasks);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			TaskBean bean = list.get(position);
//			if(bean.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR){
//				holder.imageView1.setImageResource(R.drawable.ic_tractor);
//			}
//			if(bean.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER){
//				holder.imageView1.setImageResource(R.drawable.ic_trailer);
//			}
			CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(bean.equipment_type, holder.imageView1);
//			holder.equipment_type.setText(CheckInTractorOrTrailerTypeKey.getContainerTypeKeyValue(bean.equipment_type) + ": ");
			holder.equipment_number.setText(bean.equipment_number);
			holder.rel_type.setText("("+CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(mActivity,bean.rel_type)+")");
			holder.assign_task.setText(bean.assign_task + "");
			holder.assign_alltask.setText(bean.total_task+"");
			holder.total_tasks.setText(bean.total_task + ""); 
			
			return convertView;
		}

		private class Holder {
			TextView assign_alltask;
			TextView equipment_number;
			TextView assign_task;
			TextView total_tasks;
//			BadgeView badge;
			ImageView imageView1;
			TextView rel_type;
		}
	}
	
	class EntryTaskAdapter extends BaseAdapter {

		private List<EntryTaskMainBean> list;
		public EntryTaskAdapter(Context context,List<EntryTaskMainBean> arrayList) {
			super();
			this.list = arrayList;
		}
		
		@Override
		public int getCount() {
			return list == null ? 0 : list.size();
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.assign_entry_task_list_item_layout, null);
				holder = new Holder();
				
				holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
				holder.assign_task = (TextView) convertView.findViewById(R.id.assign_task);
				holder.total_task = (TextView) convertView.findViewById(R.id.total_task);
				holder.addLinear =(LinearLayout) convertView.findViewById(R.id.addLinear);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			EntryTaskMainBean bean = list.get(position);
			holder.entry_id.setText("E"+bean.entry_id);
			holder.assign_task.setText(bean.task_assigned+"");
			holder.total_task.setText(" / "+bean.task_total+"");
			
			addLinear(bean,holder.addLinear);
			
			return convertView;
		}
		public void addLinear(EntryTaskMainBean bean,LinearLayout addLinear) {
			if(addLinear != null){
				addLinear.removeAllViews();
			}
			for (int i = 0; i < bean.complexDoorList.size(); i++) {
				CheckInTaskMainItem b = new CheckInTaskMainItem();
				b = bean.complexDoorList.get(i);
				View view = ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
						inflate(R.layout.item_cheakin_main_layout, null);
				ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView1);
				TextView tractor_txt = (TextView) view.findViewById(R.id.tractor_txt);
				TextView door_txt = (TextView) view.findViewById(R.id.door_txt);
				View dashed_line = (View) view.findViewById(R.id.dashed_line);
				dashed_line.setVisibility(View.GONE);
				door_txt.setVisibility(View.GONE);
				tractor_txt.setText(b.equipment_number);
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
					imageView1.setImageResource(R.drawable.ic_tractor);
				}
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
					imageView1.setImageResource(R.drawable.ic_trailer);
				}

				addLinear.addView(view);
			}
		}
		private class Holder {
			TextView entry_id;
			TextView assign_task;
			TextView total_task;
			LinearLayout addLinear;
		}
	}
	
}
