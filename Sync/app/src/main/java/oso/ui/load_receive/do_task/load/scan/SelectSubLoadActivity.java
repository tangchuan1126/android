package oso.ui.load_receive.do_task.load.scan;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInLoad_SubLoadBean;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 选择-子单据,若load仅1条子单据 则略过该界面
 * 
 * @author 朱成
 * @date 2014-8-20
 */
public class SelectSubLoadActivity extends BaseActivity implements
		OnItemClickListener {

	private ListView lvSubLoads;

	private CheckInLoad_ComplexSubLoads complexSubLoads;
	private List<CheckInLoad_SubLoadBean> listSubLoads;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_checkin_load_selsubload, 0);
		setTitleString("Select");

		applyParams();
		if (complexSubLoads == null)
			return;

		// 取view
		lvSubLoads = (ListView) findViewById(R.id.lvSubLoads);

		// 初始化lvSubLoads
		lvSubLoads.setAdapter(adpSubLoads);
		lvSubLoads.setOnItemClickListener(this);

		if (complexSubLoads.isMbolSeq()) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this, getString(R.string.loading_by_sequence));
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		adpSubLoads.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view,final int position,
			long id) {
		// TODO Auto-generated method stub
		if (Utility.isFastClick())
			return;
		
		final CheckInLoad_SubLoadBean mbol=listSubLoads.get(position);
		if(mbol.isClosed())
		{
			UIHelper.showToast(this, "The item has bean closed!");
			return;
		}
		
		RequestParams p=new RequestParams();
		p.add("Method", "getOrderInfosByMasterBol");
		p.add("dlo_detail_id", complexSubLoads.taskBean.dlo_detail_id+"");
		p.add("master_bol_no", mbol.master_bol_no+"");
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//提示-未扫sn
//				if(mbol.hasNOScanOrders()){
//					tip_loadNotScanSN(mbol.noscanorders);
//					return;
//				}
				
				JSONObject joLoad=json.optJSONObject("load_info");
				boolean hasNOScanSNs=joLoad.optInt("is_all_picked")==2;
				List<String> listNOScan=CheckInLoad_SubLoadBean.parseNOScanedSNs(
						joLoad.optJSONArray("noscanorders"));
				if(hasNOScanSNs)
				{
					tip_loadNotScanSN(listNOScan);
					return;
				}
				toScanAc(position);
			}
		}.doGet(HttpUrlPath.LoadPaperWorkAction, p, this);
		
		
	}
	
	/**
	 * 提示有没扫sn的order
	 */
	private void tip_loadNotScanSN(List<String> listOrders){
		//即使无orders 也显示,防止数据不对
		if(listOrders==null)
			listOrders=new ArrayList<String>();
		
		//dlg
		RewriteBuilderDialog.Builder bd=new RewriteBuilderDialog.Builder(this);
		bd.setTitle("No Scanning");
		bd.setItems(listOrders.toArray(new String[]{}),null);
		bd.setNegativeButton(getString(R.string.sync_yes), null);
		bd.show();
	}

	private void toScanAc(int subload_index) {
		// 至-装载界面
		Intent in = new Intent(this, ScanLoadActivity.class);
		complexSubLoads.curSubload = subload_index;
		ScanLoadActivity.initParams(in, complexSubLoads);
		startActivity(in);
	}

	// --api----

	private static CheckInLoad_ComplexSubLoads complexSubLoads_bridge;

	public static void initParams(CheckInLoad_ComplexSubLoads complexSubLoads) {
		complexSubLoads_bridge = complexSubLoads;
	}

	private void applyParams() {
		if (complexSubLoads_bridge == null)
			return;
		complexSubLoads = complexSubLoads_bridge;
		listSubLoads = complexSubLoads.listSubLoads;
	}

	// =============nested=========================================

	private BaseAdapter adpSubLoads = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.checkin_selsubload_item, null);
				holder = new Holder();
				holder.tvLoad = (TextView) convertView
						.findViewById(R.id.tvLoad);
				holder.tvMbolNo = (TextView) convertView
						.findViewById(R.id.tvMbolNo);
				holder.tvStaging = (TextView) convertView
						.findViewById(R.id.tvStaging);
				holder.loStatus = convertView.findViewById(R.id.loStatus);
				holder.tvStatus = (TextView) convertView
						.findViewById(R.id.tvStatus);

				holder.tvSeq = (TextView) convertView.findViewById(R.id.tvSeq);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新-数据
			CheckInLoad_SubLoadBean b = listSubLoads.get(position);
			holder.tvLoad.setText(b.load_no);
			holder.tvMbolNo.setText(b.master_bol_no + "");
			holder.tvStaging.setText(b.staging_area_id);

			// 来自load,才有状态
			if (complexSubLoads.isFromLoad) {
				holder.loStatus.setVisibility(View.VISIBLE);
				holder.tvStatus.setText(b.isClosed() ? "Closed" : "Open");
				
				int c_status_green = getResources().getColor(
						R.color.checkin_patrol_green);
				int c_status_red = getResources().getColor(R.color.Red);
				holder.tvStatus.setTextColor(b.isClosed() ? c_status_red
						: c_status_green);
			} else
				holder.loStatus.setVisibility(View.GONE);

			if (complexSubLoads.isMbolSeq()) {
				holder.tvSeq.setVisibility(View.VISIBLE);
				holder.tvSeq.setText(b.loadingSequence);
				holder.tvSeq.setTextColor(b.loadingSeqColor);
			} else {
				holder.tvSeq.setVisibility(View.GONE);
			}

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if (listSubLoads == null)
				return 0;
			return listSubLoads.size();
		}
	};

	class Holder {
		TextView tvLoad, tvMbolNo, tvStaging, tvStatus;
		TextView tvSeq;
		View loStatus;
	}

}
