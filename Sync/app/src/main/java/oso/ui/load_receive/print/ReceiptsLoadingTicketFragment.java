package oso.ui.load_receive.print;

import oso.ui.load_receive.print.bean.ReceiptTicketModel;
import support.common.UIHelper;
import support.key.EntryDetailNumberTypeKey;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

public class ReceiptsLoadingTicketFragment extends Fragment {

	private View view;
	private WebView show_webview;
	Activity activity;
	private Bundle bundle;
	private FrameLayout loadingLayout;
	private TextView psTv;
	private ReceiptTicketModel receiptTicketModel;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.dock_checkin_webview_bill_count_layout, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		activity = this.getActivity();
		bundle = getArguments();
		receiptTicketModel = (ReceiptTicketModel) bundle.getSerializable("receiptTicketModel");
		initView();
	}

	private void initView() {
		psTv = (TextView) view.findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) view.findViewById(R.id.loadingLayout);
		show_webview = (WebView) view.findViewById(R.id.show_webview);
		show_webview.setInitialScale(100);
		show_webview.getSettings().setJavaScriptEnabled(true);
		
		WebSettings settings = show_webview.getSettings();
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);
		show_webview.setInitialScale(180);
		
		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});

		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				UIHelper.showToast(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
			}
		});
		StringBuilder url = new StringBuilder(HttpUrlPath.basePath);
				boolean flag = receiptTicketModel.getNumber_type()==EntryDetailNumberTypeKey.CHECK_IN_LOAD;
				url.append(flag?receiptTicketModel.getReceiptTicketBase().getPath():"check_in/print_order_no_master_wms_order.html");
				url.append("?loadNo=").append(receiptTicketModel.getReceiptTicketBase().getLoad_no());
				url.append("&entryId=").append(receiptTicketModel.getReceiptTicketBase().getEntryid());
				url.append("&window_check_in_time=").append(receiptTicketModel.getReceiptTicketBase().getTime());
				url.append("&DockID="+receiptTicketModel.getDoor_name() + "");
				url.append("&company_name=").append(receiptTicketModel.getReceiptTicketBase().getCompany_name());
				url.append("&gate_container_no=").append(receiptTicketModel.getReceiptTicketBase().getContainer_no());
				url.append("&seal=").append(receiptTicketModel.getOut_seal());
				url.append("&CompanyID=").append(receiptTicketModel.getReceiptTicketBase().getCompanyid());
				url.append("&CustomerID=").append(receiptTicketModel.getReceiptTicketBase().getCustomerid());
				url.append("&number=").append(receiptTicketModel.getFixNumber());
				url.append("&number_type=").append(receiptTicketModel.getNumber_type());
				url.append("&order_no=").append(receiptTicketModel.getFixOrder());
				url.append("&container_no="+receiptTicketModel.getContainer_no());
				url.append(Utility.getSharedPreferencesUrlValue(activity, "adid"));
		show_webview.loadUrl(url.toString());

	}

}
