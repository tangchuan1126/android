package oso.ui.load_receive.print;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.load_receive.do_task.load.scan.LoadToPrintActivity;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.bean.ReceiptTicketBase;
import oso.ui.load_receive.print.bean.ReceiptTicketModel;
import oso.ui.load_receive.print.bean.ReceiptsTicketPartenBase;
import oso.ui.load_receive.print.iface.TicketData;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.OnInnerClickListener;
import support.key.EntryDetailNumberTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 
 * @ClassName: CheckInDemoActivity
 * @Description:
 * @author gcy
 * @date 2014年5月7日 下午4:10:35
 * 
 */
public class ReceiptsTicketDatasActivity extends FragmentActivity implements
		TicketData {

	private static Context context;
	
	private String serverNameForSpinner;
	private String serverNameForSpinnerId;
	private Button close_pup;
	private Button submit;
	private SingleSelectBar checkinSelectBar;
	private boolean isPring = false;//判断是否提交打印  true 为提交打印成功 false 为未提交以及失败
	private boolean isFromLoadToPrintActivity = false;
	int submitnum = 0;
	
	private ReceiptTicketModel receiptTicketModel;
	private List<ReceiptsTicketPartenBase> receiptsTicketPartenBaseList;
	
	public static final int ReceiptsBillOfLoadingFragment = 233;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.printiconbar_receipts_ticket_layout);
		context = ReceiptsTicketDatasActivity.this;
		getFromDockCheckInActivityData();
		initview();
		barway();
	}

	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	protected void getFromDockCheckInActivityData() {
		checkinSelectBar = (SingleSelectBar) findViewById(R.id.check_in_select_bar);
		Intent intent = this.getIntent();
		receiptTicketModel = (ReceiptTicketModel) intent.getSerializableExtra("receiptTicketModel");
		receiptsTicketPartenBaseList = analyticalData(receiptTicketModel);
		
		isFromLoadToPrintActivity = intent.getBooleanExtra("isFromLoadToPrintActivity", false);
		
		if(receiptTicketModel!=null)
			serverNameForSpinnerId = receiptTicketModel.getDefaultNum();
		
		if(isFromLoadToPrintActivity){
			checkinSelectBar.setVisibility(View.GONE);
		}else{
			checkinSelectBar.setVisibility(View.VISIBLE);
		}
	}

	// 1.defaultNum(默认打印机) printServerList receiptsTicketBaseList
	// receiptTicketBase entryId out_seal
	// serverName(打印机名[]) serverNameId(打印机id[]) door_name
	public static void initParams(Intent intent, String defaultNum,
			List<PrintServer> printServerList, String entryId,String door_name,String out_seal,
			ReceiptTicketBase receiptTicketBase,List<ReceiptBillBase> receiptsTicketBaseList
			,boolean isFromLoadToPrintActivity,int number_type) {
		ReceiptTicketModel r = new ReceiptTicketModel();
		r.setDefaultNum(defaultNum);
		r.setOut_seal(out_seal);
		r.setDoor_name(door_name);
		r.setParsePrintServer(printServerList);
		r.setReceiptsTicketBaseList(receiptsTicketBaseList);
		r.setReceiptTicketBase(receiptTicketBase);
		r.setEntryId(entryId);
		r.setNumber_type(number_type);
		intent.putExtra("receiptTicketModel", r);
		intent.putExtra("isFromLoadToPrintActivity", isFromLoadToPrintActivity);
	}


	public void initview() {
		submit = ((Button) findViewById(R.id.submit));
		close_pup = (Button) findViewById(R.id.close_pup);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				submitjudgment();
			}
		});
		close_pup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
	}
	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;
	private void showPrinters() {
		List<PrintServer> printServer = receiptTicketModel.getParsePrintServer();
		if (Utility.isNullForList(printServer)) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,
				null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(context,
				printServer, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServer.size() < 3) ? dm.heightPixels / 7
				* printServer.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServer.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getDefaultPrint_index());
		// 初始-滑至可见
		listView.setSelection(getDefaultPrint_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(!StringUtil.isNullOfStr(serverNameForSpinnerId)&&!serverNameForSpinnerId.equals("0")){
							if (submitnum == 1) {
								submitData_bill();
							} else if (submitnum == 2) {
								submitData_ticket();
							} else if (submitnum == 3) {
								submitData_counting();
							}
							dialog.dismiss();
						}else{
							UIHelper.showToast(context, getString(R.string.sync_select_printserver));
						}
						
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		builder.create().show();
	}
	
	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			serverNameForSpinnerId = receiptTicketModel.getParsePrintServer().get(position).getPrinter_server_id();
			serverNameForSpinner = receiptTicketModel.getParsePrintServer().get(position).getPrinter_server_name();
			receiptTicketModel.setDefaultNum(serverNameForSpinnerId);
			adpPrinters.setCurItem(position);
		};
	};
	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getDefaultPrint_index() {
		if (receiptTicketModel.getParsePrintServer() == null)
			return -1;

		for (int i = 0; i < receiptTicketModel.getParsePrintServer().size(); i++) {
			String tmpId = receiptTicketModel.getParsePrintServer().get(i).getPrinter_server_id();
			if (receiptTicketModel.getDefaultNum().equals(tmpId))
				return i;
		}
		return -1;
	}

	public void barway() {

		// spotStatus : all -1 , Ocuppied 2 ,No Ocuppied 1
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("BILL", 1));
		clickItems.add(new HoldDoubleValue<String, Integer>("Ticket", 2));
		clickItems.add(new HoldDoubleValue<String, Integer>("Counting", 3));

		checkinSelectBar.setUserDefineClickItems(clickItems);

		checkinSelectBar
				.setUserDefineCallBack(new SelectBarItemClickCallBack() {
					@Override
					public void clickCallBack(
							HoldDoubleValue<String, Integer> selectValue) {
						showTabView(selectValue.b, true);
					}
				});
		checkinSelectBar.userDefineSelectIndexExcuteClick(0);

	}

	private void showTabView(int index, boolean isrefreshViewType) {
		Fragment fragment = null;

		if (index == 1) {
			fragment = new ReceiptsBillOfLoadingFragment();
			submitnum = 1;
		}

		if (index == 2) {
			fragment = new ReceiptsLoadingTicketFragment();
			submitnum = 2;
		}

		if (index == 3) {
			fragment = new ReceiptsCountingSheetFragment();
			submitnum = 3;
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable("receiptTicketModel", receiptTicketModel);
		bundle.putSerializable("receiptsTicketPartenBaseList",(Serializable) receiptsTicketPartenBaseList);
		fragment.setArguments(bundle);
		// 改成fragment replace
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.checkin_park_l, fragment);
		transaction.commitAllowingStateLoss();
	}

	private List<ReceiptsTicketPartenBase> analyticalData(ReceiptTicketModel receiptTicketModel) {
		List<ReceiptsTicketPartenBase> receiptsTicketPartenBaseList = new ArrayList<ReceiptsTicketPartenBase>();
		if (!Utility.isNullForList(receiptTicketModel.getReceiptsTicketBaseList())) {
			for (int i = 0; i < receiptTicketModel.getReceiptsTicketBaseList().size(); i++) {
				findDuplicateData(receiptTicketModel.getReceiptsTicketBaseList().get(i),
						receiptsTicketPartenBaseList);
			}
		}
		return receiptsTicketPartenBaseList;
	}

	private void findDuplicateData(ReceiptBillBase receiptsTicketBase,List<ReceiptsTicketPartenBase> receiptsTicketPartenBaseList) {
		if (!Utility.isNullForList(receiptsTicketPartenBaseList)) {
			boolean flag = false;
			for (int i = 0; i < receiptsTicketPartenBaseList.size(); i++) {
				if ((receiptsTicketPartenBaseList.get(i).getLoad_no().equals(receiptsTicketBase.getLoad_no()))
						&& (receiptsTicketPartenBaseList.get(i).getMaster_bol_no()).equals(receiptsTicketBase.getMaster_bol_no())
						&&(receiptsTicketPartenBaseList.get(i).getNumber_type())==receiptsTicketBase.getNumber_type()) {
					receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList().add(receiptsTicketBase);
					flag = true;
					break;
				}
			}
			if (!flag) {
				ReceiptsTicketPartenBase receiptsTicketPartenBase = new ReceiptsTicketPartenBase();
				receiptsTicketPartenBase.setLoad_no(receiptsTicketBase.getLoad_no());
				receiptsTicketPartenBase.setNumber_type(receiptsTicketBase.getNumber_type());
				receiptsTicketPartenBase.setMaster_bol_no(receiptsTicketBase.getMaster_bol_no());
				receiptsTicketPartenBase.setCustomerid(receiptsTicketBase.getCustomerid());
				receiptsTicketPartenBase.getReceiptsTicketBaseList().add(receiptsTicketBase);
				receiptsTicketPartenBaseList.add(receiptsTicketPartenBase);
			}
		} else {
			ReceiptsTicketPartenBase receiptsTicketPartenBase = new ReceiptsTicketPartenBase();
			receiptsTicketPartenBase.setLoad_no(receiptsTicketBase.getLoad_no());
			receiptsTicketPartenBase.setNumber_type(receiptsTicketBase.getNumber_type());
			receiptsTicketPartenBase.setMaster_bol_no(receiptsTicketBase.getMaster_bol_no());
			receiptsTicketPartenBase.setCustomerid(receiptsTicketBase.getCustomerid());
			receiptsTicketPartenBase.getReceiptsTicketBaseList().add(receiptsTicketBase);
			receiptsTicketPartenBaseList.add(receiptsTicketPartenBase);
		}
	}

	public void submitjudgment() {
		if(submitnum==1){
			List<ReceiptBillBase> receiptsTicketBaseList = getSubmitData();
			if (receiptsTicketBaseList == null) {
				UIHelper.showToast(context, getString(R.string.print_data_null), Toast.LENGTH_SHORT).show();
				return;
			}
		}
		showPrinters();
	}

	private void submitData_bill() {
		List<ReceiptBillBase> receiptsTicketBaseList = getSubmitData();
		if (receiptsTicketBaseList == null) {
			UIHelper.showToast(context, getString(R.string.print_data_null), Toast.LENGTH_SHORT)
					.show();
		} else {
				RequestParams params = new RequestParams();
				StringUtil.setLoginInfoParams(params);
				JSONObject datasJsonObject = new JSONObject();
				JSONArray jSONArray = new JSONArray();
				try {
					for (int i = 0; i < receiptsTicketBaseList.size(); i++) {
						ReceiptBillBase receiptsTicketBase = receiptsTicketBaseList
								.get(i);
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("checkDataType", "PICKUP");
						jsonObject.put("number",receiptsTicketBase.getLoad_no());// receiptsTicketBase.getDlo_detail_id());
						jsonObject.put("number_type",receiptsTicketBase.getNumber_type());
						jsonObject.put("door_name", receiptTicketModel.getDoor_name());
						jsonObject.put("companyId",receiptsTicketBase.getCompanyid());
						jsonObject.put("customerId",receiptsTicketBase.getCustomerid());
						jsonObject.put("master_bol_nos",receiptsTicketBase.getMaster_bol_no());
						jsonObject.put("order_nos",receiptsTicketBase.getOrder_no());
						jsonObject.put("order_no",receiptsTicketBase.getOrder_no());
						jsonObject.put("path", receiptsTicketBase.getPath());
						jsonObject.put("checkLen", 1);
						jSONArray.put(jsonObject);
					}

					datasJsonObject.put("datas", jSONArray);
					datasJsonObject.put("who", serverNameForSpinner);
					datasJsonObject.put("detail_id", receiptTicketModel.getDetail_id());
					datasJsonObject.put("print_server_id",serverNameForSpinnerId);
					datasJsonObject.put("entry_id", receiptTicketModel.getEntryId());
					datasJsonObject.put("out_seal",StringUtil.isNullOfStr(receiptTicketModel.getOut_seal()) ? "NA" : receiptTicketModel.getOut_seal());
					datasJsonObject.put("container_no",receiptTicketModel.getContainer_no());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				params.add("values", datasJsonObject.toString());
				params.add("Method", HttpPostMethod.BillOfLoadingPrintTask);

				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
						isPring = true;
					}
				}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
		}
	}

	private List<ReceiptBillBase> getSubmitData() {
		List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
		if (receiptsTicketPartenBaseList != null&& receiptsTicketPartenBaseList.size() > 0) {
			for (int i = 0; i < receiptsTicketPartenBaseList.size(); i++) {
				if (receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList() != null&& receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList().size() > 0) {
					for (int j = 0; j < receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList().size(); j++) {
						if (receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList().get(j).isChecked()) {
							receiptsTicketBaseList.add(receiptsTicketPartenBaseList.get(i).getReceiptsTicketBaseList().get(j));
						}
					}
				}
			}
		}
		return receiptsTicketBaseList != null&& receiptsTicketBaseList.size() > 0 ? receiptsTicketBaseList: null;
	}

	private void submitData_ticket() {
			RequestParams params = new RequestParams();
			StringUtil.setLoginInfoParams(params);
			params.add("Method", HttpPostMethod.LoadingTickPrintTask);
			params.add("who", serverNameForSpinner);
			params.add("print_server_id", serverNameForSpinnerId);
			boolean flag = receiptTicketModel.getNumber_type()==EntryDetailNumberTypeKey.CHECK_IN_LOAD;
			params.add("path", flag?receiptTicketModel.getReceiptTicketBase().getPath():"check_in/print_order_no_master_wms_order.html");
			params.add("window_check_in_time", receiptTicketModel.getReceiptTicketBase().getTime() + "");
			params.add("entryId", receiptTicketModel.getReceiptTicketBase().getEntryid() + "");
			params.add("loadNo", receiptTicketModel.getReceiptTicketBase().getLoad_no() + "");
			params.add("company_name", receiptTicketModel.getReceiptTicketBase().getCompany_name() + "");
			params.add("gate_container_no", receiptTicketModel.getReceiptTicketBase().getContainer_no()
					+ "");
			params.add("seal", receiptTicketModel.getOut_seal() + "");
			params.add("CompanyID", receiptTicketModel.getReceiptTicketBase().getCompanyid() + "");
			params.add("CustomerID", receiptTicketModel.getReceiptTicketBase().getCustomerid() + "");
			params.add("DockID", receiptTicketModel.getDoor_name() + "");
			params.add("number", String.valueOf(receiptTicketModel.getFixNumber()));
			params.add("number_type", String.valueOf(receiptTicketModel.getNumber_type()));
			params.add("order_no", String.valueOf(receiptTicketModel.getFixOrder()));
			
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT)
							.show();
				}

				@Override
				public void handFinish() {
				}

				@Override
				public void handFail() {
				}
			}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);

//		}

	}

	private void submitData_counting() {
			RequestParams params = new RequestParams();
			StringUtil.setLoginInfoParams(params);
			params.add("Method", HttpPostMethod.CountingSheetPrintTask);
			params.add("who", serverNameForSpinner);
			params.add("print_server_id", serverNameForSpinnerId);
			params.add("checkDataType", "LOAD");
			params.add("number", receiptTicketModel.getFixNumber());
			params.add("entryId", receiptTicketModel.getReceiptTicketBase().getEntryid() + "");
			params.add("door_name", receiptTicketModel.getDoor_name());
			params.add("CompanyID", receiptTicketModel.getReceiptTicketBase().getCompanyid() + "");
			params.add("CustomerID", receiptTicketModel.getReceiptTicketBase().getCustomerid() + "");
			
			params.add("number_type", String.valueOf(receiptTicketModel.getNumber_type()));
			boolean flag = receiptTicketModel.getNumber_type()==EntryDetailNumberTypeKey.CHECK_IN_ORDER
					||receiptTicketModel.getNumber_type()==EntryDetailNumberTypeKey.CHECK_IN_PONO;
			params.add("url", flag?"a4_counting_sheet_order.html":"a4_counting_sheet.html");
			params.add("order", receiptTicketModel.getFixOrder());
			
			params.add("checkLen", 1 + "");
			params.add("isprint", 1 + "");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT)
							.show();
				}

				@Override
				public void handFail() {
				}
			}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		//李君浩注释
		if(isFromLoadToPrintActivity){
			Intent intent = new Intent(this, LoadToPrintActivity.class);
			intent.putExtra("isPring", isPring);
			setResult(RESULT_OK, intent);
			finish();
		}else{
			finish();
		}
		overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}

	@Override
	public void onItemSelected(String serverNameI) {
	}

	public static onKeyDownListener getOnKeyDownListener() {
		return onKeyDownListener;
	}

	public static void setOnKeyDownListener(onKeyDownListener onKeyDownListener) {
		ReceiptsTicketDatasActivity.onKeyDownListener = onKeyDownListener;
	}

	private static onKeyDownListener onKeyDownListener;

	public interface onKeyDownListener {
		boolean onKeyDown(int keyCode, KeyEvent event);
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//--------------Bol单页面如果有更新的话则刷新当前bol主页面
		if (ReceiptsTicketDatasActivity.ReceiptsBillOfLoadingFragment == resultCode) {
			boolean updateFlag = data.getBooleanExtra("updateFlag", false);
			if(updateFlag){
				ReceiptTicketBase r = (ReceiptTicketBase) data.getSerializableExtra("ReceiptTicketBase");
				receiptTicketModel.setReceiptTicketBase(r);
				showTabView(submitnum, true);
			}			
		}
	}
}
