package oso.ui.load_receive.print;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.bean.CheckInLoadComplexDoor;
import support.common.bean.CheckInLoadLoadBean;
import support.key.EntryDetailNumberTypeKey;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PrintEntryActivity extends BaseActivity {

	private LinearLayout entryLayout, doorLayout;
	private SearchEditText searchEt;
	private ListView entryLv;
	private Context context;
	private List<CheckInLoadComplexDoor> listLoads = null;
	private String mEntry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.print_search_entry, 0);
		context = PrintEntryActivity.this;
		setTitleString(getString(R.string.print_entry));
		initView();
		setdata();
	}

	private void initView() {
		entryLv = (ListView) findViewById(R.id.entryLv);
		searchEt = (SearchEditText) findViewById(R.id.searchEt);
		entryLayout = (LinearLayout) findViewById(R.id.entryLayout);
		doorLayout = (LinearLayout) findViewById(R.id.doorLayout);

	}

	private void setdata() {
		searchEt.setScanMode();
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {

				doRequest(value);
				Utility.colseInputMethod(mActivity, searchEt);
			}
		});
		searchEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
					doRequest(searchEt.getEditableText().toString());
					Utility.colseInputMethod(mActivity, searchEt);
					return true;
				}
				return false;
			}
		});
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		if (doorLayout.getVisibility() == 0) {
			RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					PrintEntryActivity.super.onBackBtnOrKey();
				}
			});
		} else {
			PrintEntryActivity.super.onBackBtnOrKey();
		}

	}

	// 可搜-entryId
	private void doRequest(final String entry_id) {

		RequestParams params = new RequestParams();
		params.add("Method", "EntryPrint");
		params.add("entry_id", entry_id);

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				listLoads = new ArrayList<CheckInLoadComplexDoor>();
				CheckInLoadComplexDoor.parseBean2(json, listLoads);
				entryLv.setAdapter(mAdapter);
				entryLayout.setVisibility(View.GONE);
				doorLayout.setVisibility(View.VISIBLE);
				searchEt.setText("");
				mEntry_id = entry_id;
				setTitleString("E" + entry_id);
			}

			@Override
			public void handFinish() {
				System.out.println("nimei>>finish");
			}

			@Override
			public void handFail() {
				searchEt.setText("");
				searchEt.requestFocus();
			}
		}.doGet(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	boolean isDoor = false; // 当前为"load列表"
	int mPosition = -1;

	// --------nested-------------------------------

	private BaseAdapter mAdapter = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			mPosition = position;
			// TODO Auto-generated method stub
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_searchprint_entry, null);
				holder = new Holder();
				holder.tvDoor = (TextView) convertView.findViewById(R.id.tvDoor);
				holder.loLoads = (LinearLayout) convertView.findViewById(R.id.loLoads);
				holder.doororspot_name = (TextView) convertView.findViewById(R.id.doororspot_name);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新-数据
			CheckInLoadComplexDoor b = listLoads.get(position);
			holder.doororspot_name.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,b.resources_type)+": ");
			holder.tvDoor.setText(b.resources_name.equals("NA") ? "NA" : b.resources_name);
			generateDoorLoads(holder.loLoads, b);

			return convertView;
		}

		private void generateDoorLoads(LinearLayout loLoads, final CheckInLoadComplexDoor doorBean) {
			List<CheckInLoadLoadBean> listLoads = doorBean.listLoads;

			List<CheckInLoadLoadBean> AlllistLoads = new ArrayList<CheckInLoadLoadBean>();
			
			for(int i =0 ;i<listLoads.size();i++){
				if(listLoads.get(i).number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD 
						|| listLoads.get(i).number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO 
						|| listLoads.get(i).number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER){
					AlllistLoads.add(0,listLoads.get(i));
				}else if(listLoads.get(i).number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN 
						|| listLoads.get(i).number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL){
					AlllistLoads.add(0,listLoads.get(i));
				}else{
					AlllistLoads.add(listLoads.get(i));
				}
			}
	 
			loLoads.removeAllViews();
			if (listLoads == null)
				return;

			// 添加-loads
			for (int i = 0; i < AlllistLoads.size(); i++) {

				LinearLayout loLoad = (LinearLayout) getLayoutInflater().inflate(R.layout.item_searchprint_load, null);
				TextView tvLoad = (TextView) loLoad.findViewById(R.id.tvLoad);
				TextView tvStatus = (TextView) loLoad.findViewById(R.id.tvStatus);
				ImageView arrowIv = (ImageView) loLoad.findViewById(R.id.arrowIv);
 
				TextView tvTitle_main = (TextView) loLoad.findViewById(R.id.tvTitle_main);

//				View vDash_load = loLoad.findViewById(R.id.vDash_load);
//				vDash_load.setVisibility(isDoor ? View.GONE : View.VISIBLE);
//				if(i==(listLoads.size()-1)){vDash_load.setVisibility(View.GONE);}
				// tvStatus.setPadding(0, 0, isDoor?0:Utility.dip2px(mActivity,
				// 10f), 0);

				LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				if (i > 0)
					lp.topMargin = 3;
				loLoad.setLayoutParams(lp);

				// 初始化-数据
				final CheckInLoadLoadBean tmpLoad = AlllistLoads.get(i);
				// debug
				tvTitle_main.setText(EntryDetailNumberTypeKey.getModuleName(tmpLoad.number_type) + ": ");
				tvLoad.setText(tmpLoad.number);
				tvStatus.setText(tmpLoad.status);
				if(tmpLoad.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL 
						|| tmpLoad.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN 
						|| tmpLoad.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD
						|| tmpLoad.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO
						|| tmpLoad.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER){
					//debug
//					loLoad.setFocusableInTouchMode(true);
					loLoad.setEnabled(true);
					arrowIv.setVisibility(View.VISIBLE);
				}else{
					//debug
//					loLoad.setFocusableInTouchMode(false);
					loLoad.setEnabled(false);
					arrowIv.setVisibility(View.INVISIBLE);
				}
				// 监听-事件
				loLoad.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						setCheckInDockBaseType(doorBean,tmpLoad);
					}
				});
				loLoads.addView(loLoad);
			}
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return listLoads.get(arg0);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listLoads.size();
		}
	};

	private class Holder {
		TextView doororspot_name;
		// --entry---------
		TextView tvDoor;
		LinearLayout loLoads;
	}

	public void setCheckInDockBaseType(final CheckInLoadComplexDoor doorBean,final CheckInLoadLoadBean item) {
		if (item.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL) {
			RequestParams params = new RequestParams();
			params.add("Method", "AndroidSelectLabel");
			params.add("load_no", item.number+"");
			params.add("number_type",item.number_type+"");
			params.add("dlo_detail_id", item.dlo_detail_id+"");
			params.add("entry_id", mEntry_id);
			params.add("resources_name", doorBean.resources_name);
			params.add("resources_type", doorBean.resources_type+"");
			params.add("resources_id", doorBean.resources_id+"");
			params.add("load_no",item.number+"");
            params.add("receipt_no",item.receipt_no+"");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					helpJsonctn(json, item.number_type,doorBean,item);
				}
			}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, context);
			return ;
		} //Debug
		if (item.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER) {
			RequestParams params = new RequestParams();
			params.add("Method", "AndroidSelectLabel");
			params.add("load_no", item.number+"");
			params.add("number_type",item.number_type+"");
			params.add("dlo_detail_id", item.dlo_detail_id+"");
			params.add("entry_id", mEntry_id);
			params.add("resources_name", doorBean.resources_name);
			params.add("resources_type", doorBean.resources_type+"");
			params.add("resources_id", doorBean.resources_id+"");
			params.add("load_no",item.number+"");
            params.add("receipt_no",item.receipt_no+"");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					helpJson(json, item,doorBean);
				}
			}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, context);
			return ;
		}
//		AlertUi.showAlertBuilder(context, "Can't Print ");

 	}

	/**
	 * @Description:解析返回后的json数组 从中根据count的返回值来判断 应该去做的操作
	 * @param json
	 * @return count=1 表示 直接进到 选择打印类型的页面 count》=2表示先弹出选择load页面筛选然后在进入 选择打印类型的页面
	 */
	private void helpJson(JSONObject json, CheckInLoadLoadBean item,CheckInLoadComplexDoor bean) {

		Intent intent = new Intent();
		PrintPackMainBeanModel r = PrintPackMainBeanModel.parseJSON(json);
		r.setDetail_id(item.dlo_detail_id+"");
		r.setResources_id(bean.resources_id+"");
		r.setResources_type(bean.resources_type+"");
		r.setEquipment_number(item.equipment_number+"");
		r.setAppointment_date(item.appointment_date+"");
		intent.putExtra("printTicketModel", r);
		intent.setClass(context, PrintTicketLoadDataActivity.class);
		startActivity(intent);

		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);

	}

	private void helpJsonctn(JSONObject json, int number_type,CheckInLoadComplexDoor bean,CheckInLoadLoadBean taskBean) {
		Intent intent = new Intent();
		PrintPackMainBeanModel r = PrintPackMainBeanModel.parseJSONs(json);
		r.setNumber_type(number_type);
		r.setResources_id(bean.resources_id+"");
		r.setResources_type(bean.resources_type+"");
        //debug
        r.receipt_no=taskBean.receipt_no;
		intent.putExtra("printTicketModel", r);
		intent.setClass(context, PrintCtnBolWebViewActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
