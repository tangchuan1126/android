package oso.ui.load_receive.do_task.receive.wms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog.OnSubmitQtyListener;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Receive_palletsSn;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.SimpleTextWatcher;
import oso.widget.VpWithTab;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.location.ChooseLocationActivity;
import oso.widget.location.ChooseStagingActivity;
import oso.widget.location.bean.LocationBean;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.selectoccupybutton.CardButton;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.OnInnerClickListener;
import support.common.print.PrintTool;
import support.common.print.PrintTool.OnPrintLs;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.dbhelper.TmpDataUtil;
import support.key.TTPKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * pallet列表
 * 
 * @author 朱成
 * @date 2015-3-2
 */
public class Receive_CntAc extends BaseActivity implements OnClickListener {

	private SearchEditText etSearch;
	// private TextView tvQty_ok, tvQty_total;
	private TextView tvTotalQty, tvStockOut, tvOverage_title; // tvDamage,
																// tvNormal;
	private ExpandableListView lvPallets;
	private List<Rec_PalletBean> listPlts;
	private Button btnCreateCCTask, btnHandDmg;
	private TextView tvPalletCnt;
	private SingleSelectBar ssb;
	private VpWithTab vp;
	private ListView damageLv;
	
	private TextView tvLoc;
	private CardButton btnLocType;

	private Rec_ItemBean itemBean;

	private List<Rec_PalletBean> listDmg = new ArrayList<Rec_PalletBean>();
	
	private TextView tvGoodPlts_cnt,tvDmgPlts_cnt;
	
	private ClpConfigAdapter adapter;
	
	//标记当前选择项
	private int currentClpType = -1;
	
	private final static int Count_Location_Back = 0x3333;
	private final static int Count_Staging_Back = 0x3334;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_receive_pallets, 0);
		showRightButton(R.drawable.menu_btn_style, "", this);
		applyParams();
		// 取v
		vp = (VpWithTab) findViewById(R.id.vp);
		etSearch = (SearchEditText) findViewById(R.id.etSearch);
		lvPallets = (ExpandableListView) findViewById(R.id.lvPallets);
		btnCreateCCTask = (Button) findViewById(R.id.btnCreateCCTask);
		tvPalletCnt = (TextView) findViewById(R.id.tvPalletCnt);
		tvTotalQty = (TextView) findViewById(R.id.tvTotalQty);
		tvStockOut = (TextView) findViewById(R.id.tvStockOut);
		// tvDamage = (TextView) findViewById(R.id.tvDamage);
		// tvNormal = (TextView) findViewById(R.id.tvNormal);
		btnHandDmg = (Button) findViewById(R.id.btnHandDmg);
		damageLv = (ListView) findViewById(R.id.damgaeLv);
		damageLv.setEmptyView(findViewById(R.id.emptyView));
		damageLv.setAdapter(adpDamagePallets);
		tvOverage_title = (TextView) findViewById(R.id.tvOverage_title);
		tvGoodPlts_cnt=(TextView) findViewById(R.id.tvGoodPlts_cnt);
		tvDmgPlts_cnt=(TextView) findViewById(R.id.tvDmgPlts_cnt);

		initSingleSelectBar();
		vp.init(ssb);

		// 事件
		btnCreateCCTask.setOnClickListener(this);
		btnHandDmg.setOnClickListener(this);
		findViewById(R.id.btnAddDmgPlt).setOnClickListener(this);
		findViewById(R.id.btnAddGoodPlts).setOnClickListener(this);

		// 初始化lv
		lvPallets.setGroupIndicator(null);
		lvPallets.setAdapter(adpPallets);
		lvPallets.setEmptyView(findViewById(R.id.loEmpty_GoodPlts));

		// 初始化
		// tvPalletCnt.setText(listPlts.size() + "");
		setTitleString("Item: " + itemBean.item_id);
		refSumUI();

		// etSearch
		etSearch.setScanMode();
		etSearch.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				doScan(value);
				etSearch.setText("");
			}
		});

	}

	private boolean isTab_GoodMode() {
		return ssb.getCurrentSelectItem().b == 0;
	}

	/**
	 * 初始化SingleSelectBar
	 */
	// int i = 1;

	private void initSingleSelectBar() {
		ssb = (SingleSelectBar) findViewById(R.id.ssb);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Good (0)", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("Damage (0)", 1));
		ssb.setUserDefineClickItems(clickItems);
		ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				// switch (selectValue.b) {
				// case 0:
				// break;
				// case 1:
				// break;
				// }
			}
		});
		ssb.userDefineSelectIndexExcuteClick(0);
	}

	private TabToPhoto ttp_dlg; // 当前-正在添加图片ttp,用于onActivityRst

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (ttp_dlg != null)
			ttp_dlg.onActivityResult(requestCode, resultCode, data);
		
		if(qtyDialog != null)
			qtyDialog.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode != Activity.RESULT_OK)
			return;
		
		switch(requestCode){
		case Count_Location_Back:
			List<LocationBean> lists = (List<LocationBean>) data.getSerializableExtra("SelectLocation");
			if(!Utility.isEmpty(lists))tvLoc.setText(lists.get(0).location_name);
			break;
		case Count_Staging_Back:
			List<LocationBean> stagings = (List<LocationBean>) data.getSerializableExtra("SelectStaging");
			if(!Utility.isEmpty(stagings))tvLoc.setText(stagings.get(0).location_name);
			break;
		}
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("Ready!");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(Utility.isFastClick())
			return;
		
		switch (v.getId()) {
		case R.id.btnRight: // 菜单
			showDlg_menu();
			break;

		case R.id.btnCreateCCTask: // Finish Count
			// 提示-打印pltLabels
			int unprint = Rec_PalletBean.getFirst_UnprintedPlt(listPlts);
			if (unprint >= 0) {
				RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please print all Pallet Labels first!");
				// 选中未打印项
				selGoodPltTab(true);
				lvPallets.setSelectedGroup(unprint);
				return;
			}
			int unprint_dmg = Rec_PalletBean.getFirst_UnprintedPlt(listDmg);
			if (unprint_dmg >= 0) {
				RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please print all Pallet Labels first!");
				// 选中未打印项
				selGoodPltTab(false);
				damageLv.setSelection(unprint_dmg);
				return;
			}

			// 确认finish
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(getString(R.string.patrol_confirm));
			builder.setAdapter(new BaseAdapter() {
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					final View view = View.inflate(mActivity, R.layout.dlg_finish_count_confirm, null);
					final TextView key = (TextView) view.findViewById(R.id.keyTv);
					final TextView value = (TextView) view.findViewById(R.id.valueTv);
					if (position == 0) {
						key.setText("Expected Qty:");
						value.setText(itemBean.expected_qty + "");
					}
					if (position == 1) {
						key.setText("Received Qty:");
						// 总的-好的、坏的
						int goodQty = Rec_PalletBean.getTotalQty_Good(listPlts);
						int dmgQty = Rec_PalletBean.getTotalQty_Damage(listPlts) + Rec_PalletBean.getTotalQty_Damage(listDmg);
						value.setText((goodQty + dmgQty) + "");
						// 有坏的
						if (dmgQty > 0) {
							String str = String.format("(G: %d + D: %d)", goodQty, dmgQty);
							((TextView) view.findViewById(R.id.valueTv2)).setText(str);
						}
					}
					if (position == 2) {
						key.setText("Received Pallets:");
						// plt数
						int pltCnt = listPlts.size() + listDmg.size();
						value.setText(pltCnt + "");
					}
					return view;
				}

				@Override
				public long getItemId(int position) {
					return 0;
				}

				@Override
				public Object getItem(int position) {
					return null;
				}

				@Override
				public int getCount() {
					return 3;
				}
			}, null);
			builder.setPositiveButton(R.string.sync_yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//debug
					if(itemBean.is_need_sn()){
						Intent in = new Intent(mActivity, Receive_ScanTask.class);
						Receive_ScanTask.initParams(in, recBean,itemBean.receipt_line_id);
						startActivity(in);
						return;
					}
					reqFinishCount();
				}
			});
			builder.show();
			break;
		case R.id.btnHandDmg: // 处理-坏掉的
			// 若无托盘 则弹出创建
			if (listDmg.size() == 0) {
				selGoodPltTab(false);
				showDlg_addPallet(false);
			}
			// 若有 则提示cntAll
			// else if (Rec_PalletBean.getTotalQty_Damage(listDmg) <=
			// Rec_PalletBean.getTotalQty_Damage(listPlts))
			// RewriteBuilderDialog.showSimpleDialog_Tip(this,
			// "Please count all damaged goods!");
			else if (Rec_PalletBean.getTotalQty_Damage(listPlts) > 0) {
				vp.setCurrentItem(0);
				int firstDmg = Rec_PalletBean.getFirstDmgPlt(listPlts);
				if (firstDmg >= 0)
					lvPallets.setSelectedGroup(firstDmg);
				RewriteBuilderDialog.showSimpleDialog_Tip(this, "Please move all damaged goods to Damaged Plts!");
			}
			break;

		// =====================
		case R.id.btnAddGoodPlts:		//添加-好plt
			showDlg_addPallet(true);
			break;
		case R.id.btnAddDmgPlt:			//添加-坏plt
			showDlg_addPallet(false);
			break;
		
		default:
			break;
		}
	}

	/**
	 * 当期tab
	 * 
	 * @param isTab_goodPlts
	 */
	private void selGoodPltTab(boolean isTab_goodPlts) {
		vp.setCurrentItem(isTab_goodPlts ? 0 : 1);
	}

	private void reqFinishCount() {

		// debug
		RequestParams p = new RequestParams();
		p.add("line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {
			//
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(getString(R.string.sync_success));
				itemBean.line_status = Rec_ItemBean.LineState_ScanStart;

				// 若全cnt完,提示打印
//				if (recBean.isAllLineCnted()) {
//					tipPrintReceiptTicket();
//				}
//				// 直接跳出
//				else
				Utility.popTo(mActivity, Receive_ItemListAc.class);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/finishCount", p, this);
	}

//	private void tipPrintReceiptTicket() {
//		RewriteBuilderDialog.Builder bd = RewriteBuilderDialog.newSimpleDialog(this, "Print Receipt Ticket?", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				// 打签
//				printTool.showDlg_printers(new PrintTool.OnPrintLs() {
//
//					@Override
//					public void onPrint(long printServerID) {
//						// TODO Auto-generated method stub
//						reqPrintRNTicket(printServerID);
//					}
//				});
//			}
//		});
//		bd.setNegativeButton(R.string.sync_no, new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				Utility.popTo(mActivity, Receive_ItemListAc.class);
//			}
//		});
//		bd.show();
//	}

	private void reqPrintRNTicket(long printServerID) {
		RequestParams p = new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		p.add("printer", printServerID + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(getString(R.string.sync_print_success));
				Utility.popTo(mActivity, Receive_ItemListAc.class);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/printRNCount", p, mActivity);

	}
	
	private QtyDialog qtyDialog;
	/**
	 * 添加-goodPlt
	 * 
	 * @param def_addGood
	 *            默认添加好plt
	 */
	private void showDlg_addPallet(boolean def_addGood) {
		int remain = itemBean.expected_qty - Rec_PalletBean.getTotalQty_need(listPlts);
		
		LocBean defLoc=TmpDataUtil.getRec_def_loc(recBean.receipt_no,
				Utility.combineList(listPlts, listDmg));
		qtyDialog = new QtyDialog(mActivity, remain, def_addGood,recBean.receipt_no,defLoc, listTypes, true);
		qtyDialog.setOnSubmitQtyListener(new OnSubmitQtyListener() {
			public void onSubmit() {
				reqAddConfig(qtyDialog,1,1);
			}
		});
		qtyDialog.show();
		
	}

	/**
	 * 添加plt
	 * 
	 * @param dlg
	 */
	private void reqAddConfig(final QtyDialog dlg,int check_location,final int clp_config) {
		Rec_ItemBean bean = itemBean;
		RequestParams p = dlg.getNetParam(bean.receipt_line_id, bean.item_id, bean.lot_no, recBean.dlo_detail_id, true, false,false,check_location);
		p.add("is_match_clp_config", clp_config+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				if(json.optInt("err") == 90 && json.optInt("is_check_location",-1) == 0){
					RewriteBuilderDialog.showSimpleDialog(mActivity, json.optString("data"), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqAddConfig(dlg,0,clp_config);
						}
					});
					
					return;
				}
				
				//clp type已存在
				if(json.optInt("err") == 90 && json.optInt("is_match_clp_config",-1) == 0){
					List<Rec_CLPType> clpTypes = new Gson().fromJson(json.optJSONArray("clpconfigrows").toString(), new TypeToken<List<Rec_CLPType>>(){}.getType());
					
					Rec_CLPType clpType = new Rec_CLPType();
					clpType.stack_height_qty = dlg.geth();
					clpType.stack_length_qty = dlg.getl();
					clpType.stack_width_qty = dlg.getw();
					clpType.lp_name = "TLP"+dlg.getl()+"*"+dlg.getw()+"*"+dlg.geth();
					clpType.type_name = dlg.getType();
					clpTypes.add(clpType);
					
					SelectClpType(dlg,clpTypes);
					return;
				}
				
				// 好托盘
				if (!dlg.isCreateDmgPlts_mode()) {
					JSONArray ja = json.optJSONArray("containers");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpList = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
					}.getType());
					listPlts.addAll(0, tmpList);
					adpPallets.notifyDataSetChanged();
					selGoodPltTab(true);
					UIHelper.showToast(getString(R.string.sync_success));
				}
				// 坏托盘
				else {
					JSONArray ja = json.optJSONArray("damage_container");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpPlt = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
					}.getType());
					listDmg.addAll(0, tmpPlt);
					adpDamagePallets.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_success));
					selGoodPltTab(false);
				}
				refSumUI();
				dlg.dismiss();
			}
		}.doGet(dlg.getReqUrl(), p, this);
	}
	
	private void reqAddConfig(final QtyDialog dlg,int check_location,final int clp_config,Rec_CLPType type) {
		Rec_ItemBean bean = itemBean;
		RequestParams p = dlg.getNetParam(bean.receipt_line_id, bean.item_id, bean.lot_no, recBean.dlo_detail_id, true, false,false,check_location);
		p.add("is_match_clp_config", clp_config+"");
		p.remove("pallet_type");
		p.add("pallet_type",""+type.type_id);
		p.add("license_plate_type_id", ""+type.lpt_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// 好托盘
				if (!dlg.isCreateDmgPlts_mode()) {
					JSONArray ja = json.optJSONArray("containers");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpList = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
					}.getType());
					listPlts.addAll(0, tmpList);
					adpPallets.notifyDataSetChanged();
					selGoodPltTab(true);
					UIHelper.showToast(getString(R.string.sync_success));
				}
				// 坏托盘
				else {
					JSONArray ja = json.optJSONArray("damage_container");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpPlt = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
					}.getType());
					listDmg.addAll(0, tmpPlt);
					adpDamagePallets.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_success));
					selGoodPltTab(false);
				}
				refSumUI();
				dlg.dismiss();
			}
		}.doGet(dlg.getReqUrl(), p, this);
	}
	
	private void SelectClpType(final QtyDialog dlg,final List<Rec_CLPType> types){
		
		View layout = View.inflate(mActivity, R.layout.dialog_receive_clpconfig, null);
		ListView lv = (ListView) layout.findViewById(R.id.lvClpconfig);
		
		LayoutParams lp = lv.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (types.size() < 3) ? dm.heightPixels / 7 * types.size() : (dm.heightPixels / 3);
		lv.setLayoutParams(lp);
		
		adapter = new ClpConfigAdapter(types,onInnerClick_Select);
		lv.setAdapter(adapter);
		
		adapter.setCurItem(types.size()-1);
		currentClpType = types.size()-1;
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Select LP Type");
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.sync_submit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				if(types.size() -1 == currentClpType){
					reqAddConfig(dlg,0,0);
				}else{
					reqAddConfig(dlg,0,0,types.get(currentClpType));
				}
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	// private void showDlg_addDmgPlt() {
	// final View view = View.inflate(mActivity, R.layout.dlg_add_dmg_plt,
	// null);
	// final EditText qtyEt = (EditText) view.findViewById(R.id.et_qty);
	// final EditText locEt = (EditText) view.findViewById(R.id.et_loc);
	//
	// // 初始化
	// TransformationMethod trans = AllCapTransformationMethod.getThis();
	// locEt.setTransformationMethod(trans);
	//
	// BottomDialog.showCustomViewDialog(mActivity, "Add Damage Pallets", view,
	// false, new OnSubmitClickListener() {
	// @Override
	// public void onSubmitClick(BottomDialog dlg, String value) {
	// String qtyValue = qtyEt.getText().toString().trim();
	// String locValue = locEt.getText().toString().trim().toUpperCase();
	// int qty = TextUtils.isEmpty(qtyValue) ? 1 : Utility.parseInt(qtyValue);
	// // 数量
	// if (qty == 0) {
	// UIHelper.showToast("Please Input Plt Qty!");
	// return;
	// }
	// // 位置
	// if (TextUtils.isEmpty(locValue)) {
	// // locValue = "W12";
	// UIHelper.showToast("Please Input Location!");
	// return;
	// }
	// reqAddDmgPlt(qty, Rec_PalletBean.PltType_Damage, locValue);
	// dlg.dismiss();
	// }
	// });
	// }

	private int getDmgQty_uncnted() {
		return Rec_PalletBean.getTotalQty_Damage(listPlts) - Rec_PalletBean.getTotalQty_Damage(listDmg);
	}

	private void showDlg_cntDmgPlt(int position) {
		final Rec_PalletBean pltBean = listDmg.get(position);
		// v
		View view = LayoutInflater.from(mActivity).inflate(R.layout.item_dlg_plt, null);
		final EditText etItem = (EditText) view.findViewById(R.id.etItem);
		final TabToPhoto ttp = (TabToPhoto) view.findViewById(R.id.ttp);
        btnLocType=(CardButton) view.findViewById(R.id.btnLocType);
        tvLoc = (TextView) view.findViewById(R.id.tvDLoc);
        
        
        tvLoc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(btnLocType.isCover()){
					Intent in = new Intent(mActivity,ChooseLocationActivity.class);
					in.putExtra("single_select", true);
					startActivityForResult(in, Count_Location_Back);
				}else{
					Intent in = new Intent(mActivity,ChooseStagingActivity.class);
					in.putExtra("single_select", true);
					startActivityForResult(in, Count_Staging_Back);
				}
			}
		});
		
		// 初始化
		//loc
//		locEt.setHint(TmpDataUtil.getRec_def_loc());
		initTTP(ttp, pltBean);
		if (pltBean.damage_qty > 0)
			etItem.setText(pltBean.damage_qty + "");
		final int dmgQty_uncnted = getDmgQty_uncnted();
		// hint取好的托盘中"总dmgQty"减"当前dmgPlt中qty"
		// int hintQty = dmgQty_uncnted + pltBean.damage_qty;

		// debug
		int hintQty = Rec_PalletBean.getTotalQty_Damage(listPlts);
		if (hintQty < 0)
			hintQty = 0;
		etItem.setHint(hintQty + "");

		if (!TextUtils.isEmpty(pltBean.location))
			tvLoc.setText(pltBean.location);
        btnLocType.showCover(pltBean.isAtLocation());
        
        btnLocType.setOnChangeTypeListener(new CardButton.OnChangeTypeListener() {
			@Override
			public void onChange(String value) {
				tvLoc.setText("");
				tvLoc.setHint("NA");
			}
		});

		final BottomDialog countDialog = new BottomDialog(mActivity);
		countDialog.setTitle("D. Plt: " + pltBean.getDisPltNO());
		countDialog.setView(view);
		countDialog.setCanceledOnTouchOutside(true);
		countDialog.setShowCancelBtn(false);
		countDialog.showTitleRight(0, new OnClickListener() {
			@Override
			public void onClick(View v) {
				countDialog.dismiss();
				tipDelPlt(pltBean);
			}
		});
		countDialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				String qtyStr = etItem.getEditableText().toString();
				String qtyHint = etItem.getHint().toString();
				String locStr = tvLoc.getText().toString();
				// 未填取"dmgRemain"
				// int qty = TextUtils.isEmpty(qtyStr) ? dmgQty_uncnted :
				// Utility.parseInt(qtyStr);
				int qty = TextUtils.isEmpty(qtyStr) ? Utility.parseInt(qtyHint) : Utility.parseInt(qtyStr);
				if (qty == 0) {
					UIHelper.showToast(getString(R.string.sync_input_damageqty));
					return;
				}
				if (TextUtils.isEmpty(locStr)) {
					UIHelper.showToast(getString(R.string.sync_location_empty));
					return;
				}
				reqCnt(itemBean.item_id, itemBean.lot_no,btnLocType.isCover() ? "1" : "2", 0, qty, pltBean, locStr, "", ttp,dlg);
//				TmpDataUtil.setRec_def_loc(locEt);
			}
		});
		countDialog.show();
	}

	private void updateGoodQty() {

		int leftGood = Rec_PalletBean.getTotalQty_Good(listPlts);
		int leftDmg = Rec_PalletBean.getTotalQty_Damage(listPlts);
		int leftTotal = leftGood + leftDmg;
		String str = leftDmg > 0 ? "Receive (" + leftTotal + ")" : "Good (" + leftTotal + ")";

		// "Good (" + qty+ ")"
		((TextView) ssb.getTabView(0)).setText(str);
	}

	private void updateDamageQty(int qty) {
		((TextView) ssb.getTabView(2)).setText("Damage (" + qty + ")");
	}

	private void refreshPlts() {
		final RequestParams params = new RequestParams();
		params.add("receipt_line_id", itemBean.receipt_line_id);
		// Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
		params.add("container_config_id", "0");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				JSONArray jaPlts = json.optJSONArray("pallets");
				if (Utility.isEmpty(jaPlts)) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No pallets! Please config pallets first!");
					return;
				}
				List<Rec_PalletBean> tmpPlts = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
				}.getType());
				listPlts.clear();
				listPlts.addAll(tmpPlts);
				adpPallets.notifyDataSetChanged();
				refSumUI();
			}

		}.doGet(HttpUrlPath.acquirePalletsInfoByLine, params, mActivity);
	}

	private void showDlg_menu() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_operation));
		// , "Add Damage Pallets"
		builder.setArrowItems(new String[] { "Receive All Pallets", "Print All Pallets", "Create Pallets" }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
				case 0: // cntAll
					showDlg_cntAll();
					break;
				case 1: // printAll
					printTool.showDlg_printers(lsAll);
					break;
				case 2: // createPlts
					// vp.setCurrentItem(0);
					showDlg_addPallet(isTab_GoodMode());
					break;

				default:
					break;
				}
			}
		});
		builder.show();
	}

	OnPrintLs lsAll = new OnPrintLs() {
		@Override
		public void onPrint(long printServerID) {
			RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_LineID, itemBean.receipt_line_id);
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					// TODO Auto-generated method stub
					String name = StoredData.getUsername();
					for (Rec_PalletBean bean : listPlts) {
						bean.print_user_name = name;
					}
					for (Rec_PalletBean bean : listDmg) {
						bean.print_user_name = name;
					}
					adpPallets.notifyDataSetChanged();
					adpDamagePallets.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_print_success));
				}
			}.doGet(NetInterface.recPrintLP_url(), p, mActivity);

		}
	};

	private void doScan(String plt) {

		List<Rec_PalletBean> tmpList = isTab_GoodMode() ? listPlts : listDmg;

		// 空
		if (TextUtils.isEmpty(plt)) {
			TTS.getInstance().speakAll("Invalid Pallet!");
			return;
		}
		Rec_PalletBean pltBean = Rec_PalletBean.getPallet_byScanNO(tmpList, plt);
		// 未找到
		if (pltBean == null) {
			TTS.getInstance().speakAll("Pallet Not Found!");
			return;
		}

		// 调整-顺序
		tmpList.remove(pltBean);
		tmpList.add(0, pltBean);
		if (isTab_GoodMode()) {
			adpPallets.notifyDataSetChanged();
			lvPallets.setSelectedGroup(0);
			// count
			showDlg_cnt(0);
		} else {
			adpDamagePallets.notifyDataSetChanged();
			damageLv.setSelection(0);
			showDlg_cntDmgPlt(0);
		}

	}

	private void showDlg_contextMenu(final int groupPosition) {

		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("Menu");
		bd.setArrowItems(new String[] { "Clear Pallet" }, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				switch (position) {
				case 0: // 删除
					delectPallets(groupPosition);
					break;

				default:
					break;
				}
			}
		});
		bd.show();

	}

	private void delectPallets(int position) {
		final Rec_PalletBean b = listPlts.get(position);
		final RequestParams params = new RequestParams();
		params.add("con_id", b.con_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				b.sn.clear();
				adpPallets.notifyDataSetChanged();
			}

		}.doGet(HttpUrlPath.deleteQTY, params, mActivity);
	}

	private void showDlg_cnt(int pos) {
		final Rec_PalletBean pltBean = listPlts.get(pos);
		// 若有扫sn
		if (pltBean.getSN_cnt_ok() > 0) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this, "Please remove all the SNs in this pallet!");
			return;
		}

		View view = LayoutInflater.from(mActivity).inflate(R.layout.item_rec_pallet_grp_dia, null);
		final TextView etItem = (TextView) view.findViewById(R.id.etItem);
		final TextView etLotNo = (TextView) view.findViewById(R.id.etLotNo);
		final EditText etNQty = (EditText) view.findViewById(R.id.etNQty);
		final EditText etDQty = (EditText) view.findViewById(R.id.etDQty);
		final SearchEditText originalEt = (SearchEditText) view.findViewById(R.id.originalEt);
		ttp_dlg = (TabToPhoto) view.findViewById(R.id.ttp);
		
		
		tvLoc = (TextView) view.findViewById(R.id.tvLoc);
		btnLocType = (CardButton) view.findViewById(R.id.btnLocType);
		
		tvLoc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(btnLocType.isCover()){
					Intent in = new Intent(mActivity,ChooseLocationActivity.class);
					in.putExtra("single_select", true);
					startActivityForResult(in, QtyDialog.Location_Back);
				}else{
					Intent in = new Intent(mActivity,ChooseStagingActivity.class);
					in.putExtra("single_select", true);
					startActivityForResult(in, QtyDialog.Staging_Back);
				}
			}
		});
		//初始化
		originalEt.setScanMode();
		//location
//		locEt.setHint(TmpDataUtil.getRec_def_loc());
		
		initTTP(ttp_dlg, pltBean);
		etNQty.setHint(pltBean.config_qty + "");
		if (pltBean.isCnted()) {
			etNQty.setText(pltBean.normal_qty + "");
			etDQty.setText(pltBean.damage_qty + "");
			// etSQty.setText(pltBean.stock_out_qty + "");
		}
		originalEt.setText(pltBean.customer_con_id == 0 ? "" : pltBean.customer_con_id + "");

		// ======etItem、etLotNo=======================================
		
		etItem.setText(itemBean.item_id);
		if(TextUtils.isEmpty(itemBean.lot_no)){
			etLotNo.setText("");
			etLotNo.setHint("NA");
		}else{
			etLotNo.setText(itemBean.lot_no);
		}
        btnLocType.showCover(pltBean.isAtLocation());
        
        btnLocType.setOnChangeTypeListener(new CardButton.OnChangeTypeListener() {
			@Override
			public void onChange(String value) {
				tvLoc.setText("");
				tvLoc.setHint("NA");
			}
		});

		// 若cnt过 则不用验证item、lotNO
		if (pltBean.isCnted()) {
			etItem.setText(itemBean.item_id);
			etLotNo.setText(itemBean.lot_no);
			etItem.setEnabled(false);
			etLotNo.setEnabled(false);
		}
		
		// =================================================

		TextWatcher twatcher = new SimpleTextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				String qtyNormal_str = etNQty.getEditableText().toString();
				int qtyNormal = 0;
				int qtyDam = Utility.parseInt(etDQty.getEditableText().toString());
				// int qtyStockOut =
				// Utility.parseInt(etSQty.getEditableText().toString());
				if (TextUtils.isEmpty(qtyNormal_str))
					qtyNormal = pltBean.config_qty;
				else
					qtyNormal = Utility.parseInt(qtyNormal_str);

				// 调整normal
				// int newNormal = pltBean.config_qty - qtyDam - qtyStockOut;
				int newNormal = pltBean.config_qty - qtyDam;
				if (newNormal < 0)
					newNormal = 0;
				// debug,不适合用联动
				etNQty.setText(newNormal + "");
			}
		};
		// 有配置 才联动
//		if (pltBean.hasConfig())
//			etDQty.addTextChangedListener(twatcher);

		if (!TextUtils.isEmpty(pltBean.location))
			tvLoc.setText(pltBean.location);

		final BottomDialog countDialog = new BottomDialog(mActivity);
		countDialog.setTitle("Plt: " + pltBean.getDisPltNO());
		countDialog.setView(view).setShowCancelBtn(false);
		countDialog.setAdjustResize(false);
		countDialog.showTitleRight(0, new OnClickListener() {
			@Override
			public void onClick(View v) {
				countDialog.dismiss();
				tipDelPlt(pltBean);
			}
		});
		countDialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				String locStr = tvLoc.getText().toString();
				String item = etItem.getText().toString();
				String lot = etLotNo.getText().toString();
				String qtyNormal_str = etNQty.getEditableText().toString();
				int qtyNormal = 0;
				int qtyDam = Utility.parseInt(etDQty.getEditableText().toString());
				// int qtyStockOut =
				// Utility.parseInt(etSQty.getEditableText().toString());

				if (TextUtils.isEmpty(qtyNormal_str))
					qtyNormal = pltBean.config_qty;
				else
					qtyNormal = Utility.parseInt(qtyNormal_str);

				// 校验
				if (TextUtils.isEmpty(locStr)) {
					UIHelper.showToast(getString(R.string.sync_location_empty));
					return;
				}
				if (TextUtils.isEmpty(item)) {
					UIHelper.showToast(getString(R.string.sync_item_empty));
					return;
				}
//				if (TextUtils.isEmpty(lot)) {
//					UIHelper.showToast(getString(R.string.sync_lotno_empty));
//					return;
//				}
				// item、lot是否相同
				if (!item.equals(itemBean.item_id)) {
					UIHelper.showToast(getString(R.string.sync_item_wrong));
					return;
				}
//				if (!lot.equals(itemBean.lot_no)) {
//					UIHelper.showToast(getString(R.string.sync_lotno_wrong));
//					return;
//				}
				// 总数校验
				// int totalInput = qtyNormal + qtyDam + qtyStockOut;
				// int totalInput = qtyNormal + qtyDam;
				// if (totalInput != pltBean.config_qty) {
				// UIHelper.showToast("Total Qty doesn't match!");
				// return;
				// }

				// 若数量不对
				int imgCnt = ttp_dlg.getAllTabPhotoNames(true).size();
				// if (qtyNormal != pltBean.config_qty && imgCnt == 0) {
				// debug,有坏的 就许图片
				if (qtyDam > 0 && imgCnt == 0) {
					UIHelper.showToast(getString(R.string.sync_pallet_addimage));
					return;
				}
				// 提交
				reqCnt(item, lot,btnLocType.isCover() ? "1" : "2", qtyNormal, qtyDam, pltBean, locStr, originalEt.getEditableText().toString(), ttp_dlg,dlg);
				//默认loc
//				TmpDataUtil.setRec_def_loc(locEt);

			}
		});
		countDialog.show();
	}

	private void initTTP(TabToPhoto ttp, final Rec_PalletBean pltBean) {
		ttp_dlg = ttp;
		String key = TTPKey.getRecPltKey(recBean.receipt_no, pltBean.con_id);
		ttp.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp_dlg));
		ttp.pvs[0].addWebPhotoToIv(pltBean.photos);
		ttp.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = pltBean.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adpPallets.notifyDataSetChanged();
					adpDamagePallets.notifyDataSetChanged();
				}
			}
		});
	}

	private void showDlg_cntAll() {
		int qty=Rec_PalletBean.getConfigQty_uncntPlt(listPlts);
		if(qty==0)
		{
			RewriteBuilderDialog.showSimpleDialog_Tip(this, "No configured pallets to receive!");
			return;
		}
		
		View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_rec_plts_cntall, null);
		final TextView etItem = (TextView) view.findViewById(R.id.etItem);
		final TextView etLotNo = (TextView) view.findViewById(R.id.etLotNo);
		final TextView tvLineQty = (TextView) view.findViewById(R.id.tvLineQty);
		
		//初始化
		tvLineQty.setText(qty+"");

		// ======etItem、etLotNo=======================================

		// etItem.setScanMode();
		// etLotNo.setScanMode();

		// if (AppConfig.Debug_Receive) {
		etItem.setText(itemBean.item_id);
		if(TextUtils.isEmpty(itemBean.lot_no)){
			etLotNo.setText("");
			etLotNo.setHint("NA");
		}else{
			etLotNo.setText(itemBean.lot_no);
		}
		// }

		// etItem.setSeacherMethod(new SeacherMethod() {
		//
		// @Override
		// public void seacher(String value) {
		// // TODO Auto-generated method stub
		// //校验
		// if(TextUtils.isEmpty(value) || !value.equals(itemBean.item_id))
		// {
		// TTS.getInstance().speakAll_withToast("Invalid Item Number!");
		// etItem.setText("");
		// return;
		// }
		// TTS.getInstance().speakAll("Scan Lot Number!");
		// etLotNo.requestFocus();
		// }
		// });
		// etLotNo.setSeacherMethod(new SeacherMethod() {
		//
		// @Override
		// public void seacher(String value) {
		// // TODO Auto-generated method stub
		// //校验
		// if(TextUtils.isEmpty(value) || !value.equals(itemBean.lot_no))
		// {
		// TTS.getInstance().speakAll_withToast("Invalid Lot Number!");
		// etLotNo.setText("");
		// return;
		// }
		// TTS.getInstance().speakAll("Confirm Receive All!");
		// }
		// });

		// =================================================

		BottomDialog.showCustomViewDialog(mActivity, "Receive All Pallets", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				String item = etItem.getText().toString();
				String lot = etLotNo.getText().toString();

				// 校验
				if (TextUtils.isEmpty(item)) {
					UIHelper.showToast(getString(R.string.sync_item_empty));
					return;
				}
//				if (TextUtils.isEmpty(lot)) {
//					UIHelper.showToast(getString(R.string.sync_lotno_empty));
//					return;
//				}
				// item、lot是否相同
				if (!item.equals(itemBean.item_id)) {
					UIHelper.showToast(getString(R.string.sync_item_wrong));
					return;
				}
//				if (!lot.equals(itemBean.lot_no)) {
//					UIHelper.showToast(getString(R.string.sync_lotno_wrong));
//					return;
//				}

				// 提交
				reqCntAll(item, lot);

				dlg.dismiss();
			}
		});
	}

	private void reqCntAll(String item_id, String lot_no) {
		final RequestParams params = new RequestParams();
		params.add("item_id", item_id);
		params.add("lot_no", lot_no);
		params.add("line_id", itemBean.receipt_line_id);
		params.add("detail_id", recBean.dlo_detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				refreshPlts();
				UIHelper.showToast(getString(R.string.sync_success));
			}

		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/countLineAllQTY", params, mActivity);
	}

	private void reqCnt(String item, final String lotno,String locationType, final int qtyCount, final int qtyDamange, final Rec_PalletBean pltBean,
			final String location, final String originalPn, final TabToPhoto ttp,final BottomDialog dlg) {
		final RequestParams params = new RequestParams();
		// params.add("qty", qtyCount);
		params.add("con_id", pltBean.con_id);
		params.add("item_id", item);
		params.add("lot_no", lotno);

		params.add("normal_qty", qtyCount + "");
		params.add("damage_qty", qtyDamange + "");
		params.add("location", location.toUpperCase());
		// params.add("stock_out_qty", qtyStockOut + "");
		params.add("goods_type", pltBean.goods_type + "");
		params.add("detail_id", recBean.dlo_detail_id);
		params.add("customer_con_id", originalPn);
		params.add("location_type", locationType);

		if (ttp != null)
			ttp.uploadZip(params, "receive");
		SimpleJSONUtil req = new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (ttp != null)
					ttp.clearData();

				JSONObject joData = json.optJSONObject("data");
				JSONObject joPlt = joData.optJSONObject("updatepallet");
				if (joPlt == null) {
					UIHelper.showToast(getString(R.string.sync_error));
					return;
				}

				Rec_PalletBean newPltBean = new Gson().fromJson(joPlt.toString(), Rec_PalletBean.class);
				// // 刷新-bean
				// int index = listPlts.indexOf(pltBean);
				// if (index < 0)
				// return;
				// listPlts.remove(index);
				// listPlts.add(index, newPltBean);
				// adpPallets.notifyDataSetChanged();

				if (pltBean.goods_type == Rec_PalletBean.PltType_Good) {
					// 刷新-bean
					int index = listPlts.indexOf(pltBean);
					if (index < 0)
						return;
					listPlts.remove(index);
					listPlts.add(index, newPltBean);
					adpPallets.notifyDataSetChanged();
				} else if (pltBean.goods_type == Rec_PalletBean.PltType_Damage) {
					// 刷新-bean
					int index = listDmg.indexOf(pltBean);
					if (index < 0)
						return;
					listDmg.remove(index);
					listDmg.add(index, newPltBean);
					adpDamagePallets.notifyDataSetChanged();

					// 刷新goodPlts
					// JSONArray joGoods=joData.optJSONArray("palletsarray");
					// if(!Utility.isEmpty(joGoods)){
					// List<Rec_PalletBean> listGoods = new Gson().fromJson(
					// joGoods.toString(),
					// new TypeToken<List<Rec_PalletBean>>() {
					// }.getType());
					// listPlts.clear();
					// listPlts.addAll(listGoods);
					// adpPallets.notifyDataSetChanged();
					// }

					for (int i = 0; i < listPlts.size(); i++) {
						Rec_PalletBean tmpP = listPlts.get(i);
						// 重置有dmg的plt
						if (tmpP.damage_qty > 0) {
							tmpP.normal_qty = 0;
							tmpP.damage_qty = 0;
						}
					}
					adpPallets.notifyDataSetChanged();
				}

				refSumUI();
				dlg.dismiss();
				UIHelper.showToast(getString(R.string.sync_success));
			}

		};
		// req.client.addHeader("Content-Type", "multipart/form-data");
		req.setCancelable(false).doPost(HttpUrlPath.bindQTYToPallet, params, mActivity);
	}

	private void refSumUI() {
		// goodPlt的dmgQty
		int dmgQty = Rec_PalletBean.getTotalQty_Damage(listPlts);

		// tvTotalQty.setText(Rec_PalletBean.getTotalQty_need(listPlts)+"");
		tvTotalQty.setText(itemBean.expected_qty + "");
		// 超出
		int overage_qty = Rec_PalletBean.getTotalQty_realRec(listPlts) + Rec_PalletBean.getTotalQty_realRec(listDmg) - itemBean.expected_qty;
		// 缺
		String sQty_total_str = overage_qty <= 0 ? "Remain: " : "Overage: ";
		tvOverage_title.setText(sQty_total_str);
		tvStockOut.setText(Math.abs(overage_qty) + "");

		// tvDamage.setText(dmgQty+ "");
		// tvNormal.setText(Rec_PalletBean.getTotalQty_Good(listPlts) + "");
		int processed_dmgQty = Rec_PalletBean.getTotalQty_Damage(listDmg);
		updateDamageQty(processed_dmgQty);
		updateGoodQty();

		// 张睿-设计→
		// if(expectedQty==G+D){
		// if(D>0&&Dmg not hand)
		// showBtn_handDmg;
		// else
		// showBtn_Assign;
		// }
		// 注:1>若有少的 不显示btn?

		// line所有数量均已cnt完
		// boolean allCnted = Rec_PalletBean.isAllCounted_ok(listPlts) &&
		// itemBean.isAllConfigured();

		// cnt,达到了期望(可能有少于预期)
		// boolean allCnted = Rec_PalletBean.getTotalQty_cntedPlt(listPlts) >=
		// itemBean.expected_qty;
		boolean allCnted = Rec_PalletBean.isAllCnted(listPlts)&&Rec_PalletBean.isAllCnted(listDmg);

		// 都cnt了,且有好的plt
		if (allCnted && listPlts.size() > 0) {
			// int processed_dmgQty =
			// Rec_PalletBean.getTotalQty_Damage(listDmg);
			// boolean isDmgOk = processed_dmgQty >= dmgQty;
			boolean isDmgOk = dmgQty == 0;

			btnCreateCCTask.setVisibility(isDmgOk ? View.VISIBLE : View.GONE);
			btnCreateCCTask.setText(itemBean.is_need_sn()?"Create CC/Scan Task":"Finish Count");
			btnHandDmg.setVisibility(!isDmgOk ? View.VISIBLE : View.GONE);
			// Hand Damage Items
			// btnHandDmg.setText("Damage Process ( " + (dmgQty -
			// processed_dmgQty) + " )");
			btnHandDmg.setText("Damage Process ( " + dmgQty + " )");
		} else {
			btnCreateCCTask.setVisibility(View.GONE);
			btnHandDmg.setVisibility(View.GONE);
		}

		// 禁用damageTab
		
		//刷plts数
		int gootPltCnt=listPlts.size();
		int dmgPltCnt=listDmg.size();
		tvGoodPlts_cnt.setText(gootPltCnt+"");
		tvDmgPlts_cnt.setText(dmgPltCnt+"");
		tvGoodPlts_cnt.setVisibility(gootPltCnt>0?View.VISIBLE:View.INVISIBLE);
		tvDmgPlts_cnt.setVisibility(dmgPltCnt>0?View.VISIBLE:View.INVISIBLE);
	}

	private void reqUploadPhotos(final Rec_PalletBean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		p.add("con_id", b.con_id);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("photo");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = new Gson().fromJson(ja.toString(), new TypeToken<List<TTPImgBean>>() {
				}.getType());
				b.photos = listPhotos;
				adpPallets.notifyDataSetChanged();
				adpDamagePallets.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.basePath + "_receive/android/uploadPhoto", p, this);

	}

	private void showDlg_photos(final Rec_PalletBean pltBean) {
		// v
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp_dlg = (TabToPhoto) view.findViewById(R.id.ttp);
		// ttp.initNoTitleBar(mActivity, new TabParam("tabName", "key", new
		// PhotoCheckable(0, "title", ttp)));
		initTTP(ttp_dlg, pltBean);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Pallet Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp_dlg.getPhotoCnt(false) > 0)
					reqUploadPhotos(pltBean, ttp_dlg);
				dlg.dismiss();
			}
		});
	}

	// =========传参===============================

	private List<Rec_PalletType> listTypes;
	
	private Rec_ReceiveBean recBean;

	public static void initParams(Intent in, List<Rec_PalletBean> listPlts, List<Rec_PalletBean> listDmg, List<Rec_PalletType> types, Rec_ReceiveBean recBean) {
		in.putExtra("listPlts", (Serializable) listPlts);
		in.putExtra("listDmg", (Serializable) listDmg);
		in.putExtra("recBean", (Serializable) recBean);
		in.putExtra("types", (Serializable)types);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		listPlts = (List<Rec_PalletBean>) params.getSerializable("listPlts");
		listDmg = (List<Rec_PalletBean>) params.getSerializable("listDmg");
		recBean = (Rec_ReceiveBean) params.getSerializable("recBean");
		listTypes = (List<Rec_PalletType>) params.getSerializable("types");
		itemBean = recBean.curItemBean;

		if (listDmg == null)
			listDmg = new ArrayList<Rec_PalletBean>();
	}

	private void tipDelPlt(final Rec_PalletBean pltBean) {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqDelPlt(pltBean);
			}
		});
	}

	private void reqDelPlt(final Rec_PalletBean pltBean) {

		RequestParams p = NetInterface.recDelPlt_p(pltBean.con_id, itemBean.receipt_line_id, false);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// 防止-异步出错
				// 坏的
				if (pltBean.isDamagePlt() && listDmg.contains(pltBean)) {
					listDmg.remove(pltBean);
					adpDamagePallets.notifyDataSetChanged();
				}
				// 正常的
				else if (listPlts.contains(pltBean)) {
					listPlts.remove(pltBean);
					adpPallets.notifyDataSetChanged();
				}
				refSumUI();
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(NetInterface.recDelPlt_url(), p, this);
	}

	private Rec_PalletBean pltToPrint;

	private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs = new PrintTool.OnPrintLs() {

		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintPlts(printServerID);
		}
	};

	// {
	// public void doPrint(long printServerID) {
	// reqPrintPlts(printServerID);
	// };
	// };

	private void reqPrintPlts(long printServerID) {
		// String ids = Rec_PalletBean.getPltIDs(tmpPlts);
		final Rec_PalletBean tmpPlt = pltToPrint;
		if (tmpPlt == null)
			return;

		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, tmpPlt.con_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				String name = StoredData.getUsername();
				tmpPlt.print_user_name = name;
				adpDamagePallets.notifyDataSetChanged();
				adpPallets.notifyDataSetChanged();

				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);

	}

	/**
	 * 若有人打印过 则提示
	 * 
	 * @param p
	 */
	private void tipPrint(Rec_PalletBean p) {
		pltToPrint = p;
		if (p.isPrinted()) {
			String tip = String.format("These Labels has been printed by %s, reprint?", p.print_user_name);
			RewriteBuilderDialog.showSimpleDialog(this, tip, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					printTool.showDlg_printers(printLs);
				}
			});
		} else
			printTool.showDlg_printers(printLs);
	}

	// ==================nested=====================================

	private AdpDamagePallets adpDamagePallets = new AdpDamagePallets();

	private class AdpDamagePallets extends BaseAdapter {

		@Override
		public int getCount() {
			return listDmg == null ? 0 : listDmg.size();
		}

		@Override
		public Object getItem(int position) {
			return listDmg.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			Holder h;

			// final Rec_PalletBean palletsbean = listDmg.get(position);
			final Rec_PalletBean pltBean = listDmg.get(position);
			OnClickListener ls = new OnClickListener() {
				public void onClick(View v) {
					if (Utility.isFastClick())
						return;
					switch (v.getId()) {
					case R.id.btnCount: // count
						// showDlg_cnt(position);
						showDlg_cntDmgPlt(position);
						break;

					case R.id.iv: // 图片
						showDlg_photos(listDmg.get(position));
						break;

					case R.id.btnPrint: // 打印
						// pltToPrint = pltBean;
						// printTool.showDlg_printers();
						tipPrint(pltBean);
						break;

					default:
						break;
					}

				};
			};
			// =================================================

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_pallet_grp, null);
				h = new Holder();
				// h.item = convertView.findViewById(R.id.item);
				h.item = convertView;
				h.btnPrint = convertView.findViewById(R.id.btnPrint);
				h.btnCount = convertView.findViewById(R.id.btnCount);
				h.item_bottom = convertView.findViewById(R.id.item_bottom);
				// h.btnEnter = convertView.findViewById(R.id.btnEnter);
				// h.palletsId = (TextView)
				// convertView.findViewById(R.id.pallet_gry_id);
				// h.tvSN_total = (TextView)
				// convertView.findViewById(R.id.tvSN_total);
				h.tvPlt = (TextView) convertView.findViewById(R.id.tvPlt);
				h.tvDamage_it = (TextView) convertView.findViewById(R.id.tvDamage_it);
				h.tvStockOut_it = (TextView) convertView.findViewById(R.id.tvStockOut_it);
				h.tvGood_it = (TextView) convertView.findViewById(R.id.tvGood_it);
				h.tvProMark = convertView.findViewById(R.id.tvProMark);
				h.tvTotalQty_it = (TextView) convertView.findViewById(R.id.tvTotalQty_it);
				h.iv = (ImageView) convertView.findViewById(R.id.iv);
				h.locTv = (TextView) convertView.findViewById(R.id.locTv);
                h.tvLocType = (TextView) convertView.findViewById(R.id.tvLocType);
				h.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 关闭|无条目时,显示圆背景
			// boolean showOne = !isExpanded || palletsbean.getSN_cnt_ok() == 0;
			// h.item_bottom
			// .setBackgroundResource(showOne ? R.drawable.item_recrive_group_w
			// : R.drawable.item_recrive_group_w2);

			// 刷ui
			h.tvPlt.setText(pltBean.getDisPltNO());
			h.tvTotalQty_it.setText(pltBean.config_qty + "");

			h.tvGood_it.setText("G: " + pltBean.normal_qty);
			h.tvDamage_it.setText("D: " + pltBean.damage_qty);
			h.tvStockOut_it.setText("S: " + pltBean.getShortage());
			// h.tvGood_it.setVisibility(pltBean.normal_qty == 0 ? View.GONE :
			// View.VISIBLE);
			h.tvGood_it.setVisibility(View.GONE);
			h.tvDamage_it.setVisibility(pltBean.damage_qty == 0 ? View.GONE : View.VISIBLE);
			h.tvStockOut_it.setVisibility(pltBean.getShortage() == 0 ? View.GONE : View.VISIBLE);

			h.tvProMark.setVisibility(pltBean.isProblem() ? View.VISIBLE : View.GONE);
			h.locTv.setText(pltBean.location);
            h.tvLocType.setText(pltBean.getLoc_str()+": ");
			// h.iv.setSelected(pltBean.hasPhoto());

			// photo
			boolean hasPhoto = pltBean.hasPhoto();
			h.iv.setSelected(hasPhoto);
			h.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
			h.tvImgCnt.setText(pltBean.getPhotoCnt() + "");

			// cnt/print高亮
			h.btnCount.setSelected(pltBean.isCnted());
			h.btnPrint.setSelected(pltBean.isPrinted());

			// 事件
			h.btnCount.setOnClickListener(ls);
			h.iv.setOnClickListener(ls);
			h.btnPrint.setOnClickListener(ls);
			h.item.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					tipDelPlt(pltBean);
					return true;
				}
			});

			return convertView;
		}

	}

	// ==================nested===============================
	private AdpPallets adpPallets = new AdpPallets();

	private class AdpPallets extends BaseExpandableListAdapter {

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			return listPlts != null ? listPlts.size() : 0;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return listPlts.get(groupPosition).sn != null ? listPlts.get(groupPosition).sn.size() : 0;
		}

		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return listPlts.get(groupPosition).sn.get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;

			// final Rec_PalletBean palletsbean = listPlts.get(groupPosition);
			final Rec_PalletBean pltBean = listPlts.get(groupPosition);
			OnClickListener ls = new OnClickListener() {
				public void onClick(View v) {
					if (Utility.isFastClick())
						return;
					switch (v.getId()) {
					case R.id.item: // 展开、收起
						if (isExpanded)
							lvPallets.collapseGroup(groupPosition);
						else
							lvPallets.expandGroup(groupPosition);
						break;
					// case R.id.btnEnter: // 进入
					//
					// Intent in = new Intent(mActivity,
					// Receive_ReceiveAc.class);
					// Receive_ReceiveAc.initParams(in, itemBean, palletsbean);
					// startActivity(in);
					// break;
					case R.id.btnCount: // count
						showDlg_cnt(groupPosition);
						break;

					case R.id.iv: // 图片
						showDlg_photos(pltBean);
						break;
					case R.id.btnPrint: // 打印
						// pltToPrint = pltBean;
						// printTool.showDlg_printers();
						tipPrint(pltBean);
						break;

					default:
						break;
					}

				};
			};
			// =================================================

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_pallet_grp, null);
				h = new Holder();
				h.item = convertView;
				h.btnPrint = convertView.findViewById(R.id.btnPrint);
				h.btnCount = convertView.findViewById(R.id.btnCount);
				h.item_bottom = convertView.findViewById(R.id.item_bottom);
				// h.btnEnter = convertView.findViewById(R.id.btnEnter);
				// h.palletsId = (TextView)
				// convertView.findViewById(R.id.pallet_gry_id);
				// h.tvSN_total = (TextView)
				// convertView.findViewById(R.id.tvSN_total);
				h.tvPlt = (TextView) convertView.findViewById(R.id.tvPlt);
				h.tvDamage_it = (TextView) convertView.findViewById(R.id.tvDamage_it);
				h.tvStockOut_it = (TextView) convertView.findViewById(R.id.tvStockOut_it);
				h.tvGood_it = (TextView) convertView.findViewById(R.id.tvGood_it);
				h.tvProMark = convertView.findViewById(R.id.tvProMark);
				h.tvTotalQty_it = (TextView) convertView.findViewById(R.id.tvTotalQty_it);
				h.iv = (ImageView) convertView.findViewById(R.id.iv);
				h.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
				h.locTv = (TextView) convertView.findViewById(R.id.locTv);
                h.tvLocType=(TextView) convertView.findViewById(R.id.tvLocType);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || pltBean.getSN_cnt_ok() == 0;
			h.item_bottom.setBackgroundResource(showOne ? R.drawable.item_recrive_group_w : R.drawable.item_recrive_group_w2);

			// 刷ui
			h.tvPlt.setText(pltBean.wms_container_id);
			h.tvTotalQty_it.setText(pltBean.config_qty + "");
			h.locTv.setText(pltBean.location);
            h.tvLocType.setText(pltBean.getLoc_str()+": ");

			// h.tvGood_it.setText("G: " + pltBean.normal_qty);

			String goodQty = "G. Qty: " + pltBean.normal_qty;
			// 有配置-才带总数
			if (pltBean.hasConfig())
				goodQty = goodQty + " / " + pltBean.config_qty;
			h.tvGood_it.setText(goodQty);

			h.tvDamage_it.setText("D: " + pltBean.damage_qty);

			int sCnt = pltBean.getShortage();
			String sCntTitle = sCnt > 0 ? "S: -" : "O: +";
			h.tvStockOut_it.setText(sCntTitle + Math.abs(sCnt));
			h.tvDamage_it.setVisibility(pltBean.damage_qty == 0 ? View.GONE : View.VISIBLE);
			// 无配置时 不显示超出
			if (!pltBean.hasConfig())
				h.tvStockOut_it.setVisibility(View.GONE);
			else
				h.tvStockOut_it.setVisibility(pltBean.getShortage() == 0 ? View.GONE : View.VISIBLE);

			h.tvProMark.setVisibility(pltBean.isProblem() ? View.VISIBLE : View.GONE);

			// photo
			boolean hasPhoto = pltBean.hasPhoto();
			h.iv.setSelected(hasPhoto);
			h.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
			h.tvImgCnt.setText(pltBean.getPhotoCnt() + "");
			// cnt/print高亮
			h.btnCount.setSelected(pltBean.isCnted());
			h.btnPrint.setSelected(pltBean.isPrinted());

			// 事件
			h.btnCount.setOnClickListener(ls);
			// h.item.setOnClickListener(ls);
			// h.btnEnter.setOnClickListener(ls);
			// h.item.setOnLongClickListener(new View.OnLongClickListener() {
			//
			// @Override
			// public boolean onLongClick(View v) {
			// // TODO Auto-generated method stub
			// showDlg_contextMenu(groupPosition);
			// return true;
			// }
			// });
			h.iv.setOnClickListener(ls);
			h.btnPrint.setOnClickListener(ls);
			h.item.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					tipDelPlt(pltBean);
					return true;
				}
			});

			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Child h;

			List<Receive_palletsSn> palletsSnList = listPlts.get(groupPosition).sn;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_pallet_child, null);
				h = new Holder_Child();
				h.tvPallet = (TextView) convertView.findViewById(R.id.tvPallet);
				h.item = convertView.findViewById(R.id.item);

				convertView.setTag(h);
			} else
				h = (Holder_Child) convertView.getTag();
			h.tvPallet.setText("SN: " + palletsSnList.get(childPosition).serial_number);

			// 背景
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				h.item.setBackgroundResource(R.drawable.item_recrive_child_bottom);
			} else {
				h.item.setBackgroundResource(R.drawable.item_recrive_child_center);
			}

			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return false;
		}

	}

	private class Holder {
		View item, btnCount, item_bottom, tvProMark, btnPrint;
		TextView palletsId;
		TextView tvDamage_it, tvStockOut_it, tvGood_it, tvPlt, tvTotalQty_it, tvImgCnt, locTv,tvLocType;
		ImageView iv;

	}

	private class Holder_Child {
		View item;
		TextView tvPallet, locTv;
	}
	
	//---------clp config----------------------------------
	
		private OnInnerClickListener onInnerClick_Select = new OnInnerClickListener() {
			public void onInnerClick(View v, int position) {
				adapter.setCurItem(position);
				currentClpType = position;
			};
		};
		
		private class ClpConfigAdapter extends BaseAdapter{

			private List<Rec_CLPType> types;
			
			private OnInnerClickListener clickListener;

			public ClpConfigAdapter(List<Rec_CLPType> types,
					OnInnerClickListener clickListener) {
				super();
				this.types = types;
				this.clickListener = clickListener;
			}
			
			private int curItem = 0; // 当前-索引

			public void setCurItem(int curItem) {
				this.curItem = curItem;
				notifyDataSetChanged();
			}

			@Override
			public int getCount() {
				return types == null ? 0 : types.size();
			}

			@Override
			public Object getItem(int position) {
				return types.get(position);
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				
				ClpHolder holder;
				if(convertView == null){
					convertView = View.inflate(mActivity, R.layout.item_receive_clpconfig, null);
					holder = new ClpHolder();
					holder.llItem = (LinearLayout) convertView.findViewById(R.id.llItem);
					holder.tvClpName = (TextView) convertView.findViewById(R.id.tvClpName);
					holder.tvPallet = (TextView) convertView.findViewById(R.id.tvPallet);
					holder.tvShipTo = (TextView) convertView.findViewById(R.id.tvShipTo);
					holder.rbSelect = (RadioButton) convertView.findViewById(R.id.rbSelect);
				
					convertView.setTag(holder);
				}else{
					holder = (ClpHolder) convertView.getTag();
				}
				
				Rec_CLPType type = types.get(position);
				holder.tvClpName.setText(type.lp_name);
				holder.tvPallet.setText(type.type_name);
				holder.rbSelect.setChecked(curItem == position);
				
				if(TextUtils.isEmpty(type.ship_to_names)){
					holder.tvShipTo.setVisibility(View.GONE);
				}else{
					holder.tvShipTo.setVisibility(View.VISIBLE);
					holder.tvShipTo.setText(type.ship_to_names);
				}
				
				holder.llItem.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(clickListener != null)
							clickListener.onInnerClick(v, position);
					}
				});
				return convertView;
			}
			
		}
		
		private class ClpHolder{
			TextView tvClpName;
			TextView tvPallet;
			TextView tvShipTo;
			RadioButton rbSelect;
			LinearLayout llItem;
		}

}
