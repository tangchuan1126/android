package oso.ui.load_receive.window.bean;

import java.io.Serializable;
import java.util.List;

public class InfoBean implements Serializable {

	private static final long serialVersionUID = -6198971107212954404L;
	
	public String storeid;
	public String area_id;
	public String loadno;
	public String status;
	public String door_name;
	public String stagingareaid;
	public String pono;
	public String orderno;
	public String search_number_type;
	public String number;
	public String search_number;
	public String companyid;
	public String system_type;
	public String num;
	public String customerid;
	public String area_name;
	public String door_id;
	public String accountid;
	public String order_type;
	public String supplierid;
	public String receiptno;
	public String freightterm;
	public String appointmentdate = "";
	public String order_type_value;

	public List<OPS> ponos;
	public List<OPS> orders;

	public class OPS {
		public String pono;
		public String orderno;
	}
	
	public int resources_type;
	public int resources_id;
	public String resources_name;
}
