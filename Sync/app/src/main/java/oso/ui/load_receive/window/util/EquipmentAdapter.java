package oso.ui.load_receive.window.util;

import java.util.List;

import oso.ui.load_receive.window.bean.EntryMainBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyStatusTypeKey;
import support.key.OccupyTypeKey;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class EquipmentAdapter extends BaseAdapter {

	private List<EquipmentBean> mEquipmentList;
	private Context context;
	private EntryMainBean mEntry;

	public EquipmentAdapter(Context c, List<EquipmentBean> mEquipmentList, EntryMainBean eb) {
		this.mEquipmentList = mEquipmentList;
		context = c;
		mEntry = eb;
	}

	@Override
	public int getCount() {
		return mEquipmentList == null ? 0 : mEquipmentList.size();
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return mEquipmentList.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.item_select_equipment, null);
			holder = new Holder();
			holder.numTv = (TextView) convertView.findViewById(R.id.numTv);
			holder.spotTv = (TextView) convertView.findViewById(R.id.spotTv);
			holder.ordertypeTv = (TextView) convertView.findViewById(R.id.ordertypeTv);
			holder.iv = (ImageView) convertView.findViewById(R.id.iv);
			holder.notifyTv = (TextView) convertView.findViewById(R.id.notifyTv);
			holder.stateTv = (TextView) convertView.findViewById(R.id.stateTv);
			holder.relTypeTv = (TextView) convertView.findViewById(R.id.relTypeTv);
			convertView.setTag(holder);
		} else
			holder = (Holder) convertView.getTag();

		EquipmentBean bean = mEquipmentList.get(position);
		holder.numTv.setText(bean.getEquipment_number());
		holder.notifyTv.setText(bean.getTotal_task());
		// 资源判断
		if (bean.getResources_type() != 0 && bean.getResources_name() != null) {
			holder.spotTv.setVisibility(View.VISIBLE);
			holder.spotTv.setText(OccupyStatusTypeKey.getContainerTypeKeyValue(bean.getOccupy_status(),context)+" "+OccupyTypeKey.getOccupyTypeKeyName(context,bean.getResources_type()) + " : " + bean.getResources_name());
		} else {
			holder.spotTv.setText("-- --");
			holder.spotTv.setVisibility(View.GONE);
		}
		// 提货方式
		if (bean.getRel_type() == null || bean.getRel_type().equals("0")) {
			holder.relTypeTv.setText("-- --");
			holder.relTypeTv.setVisibility(View.GONE);
		} else {
			holder.relTypeTv.setText(CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(context,bean.getRel_type()) + "  ");
			holder.relTypeTv.setVisibility(View.VISIBLE);
		}
		// 单据类型
		if ((bean.getCheck_in_entry_id() + "").equals(mEntry.entry_id)) {
			holder.ordertypeTv.setText(CheckInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(context,bean.getEquipment_purpose()) + "  ");
		} else {
			holder.ordertypeTv.setText("PickUp CTNR  ");
		}
		// 状态
		holder.stateTv.setText(CheckInMainDocumentsStatusTypeKey.getReturnStatusById(context,bean.getEquipment_status()) + "  ");
		// 车头与车尾
		holder.notifyTv.setVisibility(View.VISIBLE);
		if (bean.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			// 是车头
			holder.iv.setImageResource(R.drawable.ic_tractor);
			// 有车尾并且车头没任务，隐藏状态
			if (mEquipmentList.size() > 1 && bean.getTotal_task().equals("0")) {
				holder.notifyTv.setVisibility(View.INVISIBLE);
				holder.stateTv.setVisibility(View.INVISIBLE);
				holder.relTypeTv.setVisibility(View.INVISIBLE);
				// holder.btn.setVisibility(View.GONE);
			}
			// 如果车头Left需要显示状态
			if (bean.getEquipment_status() != CheckInMainDocumentsStatusTypeKey.PROCESSING
					&& bean.getEquipment_status() != CheckInMainDocumentsStatusTypeKey.UNPROCESS) {
				holder.stateTv.setVisibility(View.VISIBLE);
			}
		} else {
			// 是车尾
			holder.iv.setImageResource(R.drawable.ic_trailer);
		}

		return convertView;
	}

	private class Holder {
		TextView numTv;
		TextView spotTv;
		ImageView iv;
		TextView notifyTv;
		TextView ordertypeTv;
		TextView relTypeTv;
		TextView stateTv;
	}

}
