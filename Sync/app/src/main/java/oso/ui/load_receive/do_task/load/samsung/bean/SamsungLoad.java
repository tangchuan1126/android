package oso.ui.load_receive.do_task.load.samsung.bean;

import java.io.Serializable;

public class SamsungLoad implements Serializable {

	private static final long serialVersionUID = -161790144920588325L;

	public static final String AREA_DEF = "Area";

	public SamsungLoad() {
	}

	public SamsungLoad(String pn, String area) {
		this.plate_no = pn;
		this.staging_area = area;
	}

	public String getIcp_id() {
		return icp_id;
	}

	public void setIcp_id(String icp_id) {
		this.icp_id = icp_id;
	}

	public String getStaging_area() {
		return staging_area;
	}

	public void setStaging_area(String staging_area) {
		this.staging_area = staging_area;
	}

	public String getPlate_no() {
		return plate_no;
	}

	public void setPlate_no(String plate_no) {
		this.plate_no = plate_no;
	}

	public String getIc_id() {
		return ic_id;
	}

	public void setIc_id(String ic_id) {
		this.ic_id = ic_id;
	}

//	public boolean isScan() {
//		return isScan;
//	}
//
//	public void setScan(boolean isScan) {
//		this.isScan = isScan;
//	}

	public String getLocation_name() {
		return location_name;
	}

	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}

	public String getShip_entryd_id() {
		return ship_entryd_id;
	}

	public void setShip_entryd_id(String ship_entryd_id) {
 		this.ship_entryd_id = ship_entryd_id;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	private String icp_id;
	private String staging_area;
	private String plate_no;
	private String ic_id;
	private String box_number;
	private String weight;

	// samsung load用
//	private boolean isScan;
	private String ship_entryd_id;
	
	private String location_name;
}
