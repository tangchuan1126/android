package oso.ui.load_receive.window.bean;

import java.io.Serializable;

public class EntryBean implements Serializable {

	private static final long serialVersionUID = 2786689135996496387L;

	public String getGate_check_in_time() {
		return gate_check_in_time;
	}

	public void setGate_check_in_time(String gate_check_in_time) {
		this.gate_check_in_time = gate_check_in_time;
	}

	public String getGate_driver_name() {
		return gate_driver_name;
	}

	public void setGate_driver_name(String gate_driver_name) {
		this.gate_driver_name = gate_driver_name;
	}

	public String getEntry_id() {
		return entry_id;
	}

	public void setEntry_id(String entry_id) {
		this.entry_id = entry_id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	private String gate_check_in_time;
	private String gate_driver_name;
	private String entry_id;
	private String company_name;

}
