package oso.ui.load_receive.window;

import java.util.List;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.EntryDetailNumberStateKey;
import support.key.ModuleKey;
import support.key.OccupyStatusTypeKey;
import support.key.OccupyTypeKey;
import utility.Utility;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickupListActivity extends BaseActivity {

	private TextView equipment_info, dropoffTv, locationTv;
	private View header, lineTv, inSealLay, outSealLay;
	private TextView inSealTv, outSealTv;

	private ExpandableListView taskLv;
	private TaskAdapter adapter;

	private EntryBean entry;
	private EquipmentBean mEquipment;
	private List<List<WindowTaskBean>> datas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_show_pickup, 0);
		initData();
		initView();
		setView();
	}

	private void initData() {
		entry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		mEquipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		datas = (List<List<WindowTaskBean>>) getIntent().getSerializableExtra("datas");
	}

	private void initView() {
		equipment_info = (TextView) findViewById(R.id.equipment_info);
		header = View.inflate(mActivity, R.layout.vp_show_pickup_header, null);
		dropoffTv = (TextView) header.findViewById(R.id.dropoffTv);
		inSealLay = header.findViewById(R.id.inSealLay);
		outSealLay = header.findViewById(R.id.outSealLay);
		lineTv = header.findViewById(R.id.lineTv);
		inSealTv = (TextView) header.findViewById(R.id.inSealTv);
		outSealTv = (TextView) header.findViewById(R.id.outSealTv);
		locationTv = (TextView) header.findViewById(R.id.locationTv);
		taskLv = (ExpandableListView) findViewById(R.id.taskLv);
		taskLv.addHeaderView(header);
		taskLv.setEmptyView(findViewById(R.id.nodataTv));
		taskLv.setGroupIndicator(null);
	}

	private void setView() {
		setTitleString("E" + mEquipment.getCheck_out_entry_id());
		equipment_info.setText(/*
								 * CheckInTractorOrTrailerTypeKey.
								 * getContainerTypeKeyValue
								 * (mEquipment.getEquipment_type()) + " : " +
								 */mEquipment.getEquipment_number());

		if (mEquipment.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			((ImageView) findViewById(R.id.equipment_type)).setImageResource(R.drawable.ic_s_tractor_write);
		}
		if (mEquipment.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRAILER) {
			((ImageView) findViewById(R.id.equipment_type)).setImageResource(R.drawable.ic_s_trailer_write);
		}

		if (mEquipment.getEquipment_purpose() == 0) {
			dropoffTv.setText("");
		} else {
			dropoffTv.setText(CheckInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(mActivity,mEquipment.getEquipment_purpose()) + " By E"
					+ entry.getEntry_id());
		}

		if (mEquipment.getResources_type() == 0 || mEquipment.getResources_name() == null) {
			locationTv.setText("");
		} else {
			locationTv.setText(OccupyStatusTypeKey.getContainerTypeKeyValue(mEquipment.getOccupy_status(),mActivity) + " "
					+ OccupyTypeKey.getOccupyTypeKeyName(mActivity,mEquipment.getResources_type()) + " " + mEquipment.getResources_name());
		}

		inSealTv.setText(TextUtils.isEmpty(mEquipment.getSeal_delivery()) ? "NA" : mEquipment.getSeal_delivery());
		outSealTv.setText(TextUtils.isEmpty(mEquipment.getSeal_pick_up()) ? "NA" : mEquipment.getSeal_pick_up());

		if (mEquipment.getRel_type() == null) {
			inSealLay.setVisibility(View.VISIBLE);
			outSealLay.setVisibility(View.VISIBLE);
			lineTv.setVisibility(View.VISIBLE);
		} else {
			if (mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.DELIVERY + "")) {
				inSealLay.setVisibility(View.VISIBLE);
			} else if (mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.PICK_UP + "")) {
				outSealLay.setVisibility(View.VISIBLE);
			} else if (mEquipment.getRel_type().equals("0") || mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.BOTH + "")) {
				inSealLay.setVisibility(View.VISIBLE);
				outSealLay.setVisibility(View.VISIBLE);
				lineTv.setVisibility(View.VISIBLE);
			}
		}
		// dockInTv.setText(equipment.getCheck_in_warehouse_time());
		// dockOutTv.setText(equipment.getCheck_out_warehouse_time());
		// stateTv.setText(CheckInMainDocumentsStatusTypeKey.getReturnStatusById(equipment.getEquipment_status()));

		adapter = new TaskAdapter(datas);
		taskLv.setAdapter(adapter);
		for (int i = 0; i < adapter.getGroupCount(); i++) {
			taskLv.expandGroup(i);
		}
		taskLv.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
	}

	private class TaskAdapter extends BaseExpandableListAdapter {

		private List<List<WindowTaskBean>> datas;

		public TaskAdapter(List<List<WindowTaskBean>> datas) {
			this.datas = datas;
		}

		@Override
		public int getGroupCount() {
			return datas == null ? 0 : datas.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return datas.get(groupPosition) == null ? 0 : datas.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return datas.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return datas.get(groupPosition).get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_pickup_list_g, null);
				holder = new Holder();
				holder.doorTv = (TextView) convertView.findViewById(R.id.doorTv);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			final WindowTaskBean bean = datas.get(groupPosition).get(0);
			if (bean.resources_type != 0) {
				holder.doorTv.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,bean.resources_type) + ": " + bean.resources_type_value);
				holder.doorTv.setTextColor(Color.WHITE);
			} else {
				holder.doorTv.setText("NA");
				holder.doorTv.setTextColor(Color.RED);
			}
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_pickup_list, null);
				holder = new Holder();
				holder.main = convertView.findViewById(R.id.main);
				holder.main.setEnabled(false);
				holder.loadTv = (TextView) convertView.findViewById(R.id.loadTv);
				holder.stateTv = (TextView) convertView.findViewById(R.id.stateTv);
				holder.customer_id = (TextView) convertView.findViewById(R.id.customer_id);
				holder.company_id = (TextView) convertView.findViewById(R.id.company_id);

				// holder.timeTv = (TextView)
				// convertView.findViewById(R.id.timeTv);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			final WindowTaskBean bean = (WindowTaskBean) getChild(groupPosition, childPosition);

			Spannable spLoad = Utility.getCompoundText(ModuleKey.getCheckInModuleKey(bean.number_type) + " ", bean.number, new StyleSpan(
					Typeface.NORMAL));

			// 单据类型，单据号
			holder.loadTv.setText(spLoad);
			holder.customer_id.setText(bean.customer_id);
			holder.company_id.setText(bean.company_id);

			// 单据状态
			if (bean.number_status == 1 || bean.number_status == 2) {
				// holder.timeTv.setVisibility(View.GONE);
				holder.stateTv.setText(EntryDetailNumberStateKey.getType(mActivity,bean.number_status));
			} else {
				// holder.timeTv.setVisibility(View.VISIBLE);
				// holder.timeTv.setText("At " + bean.finish_time);
				String closeUserStr = TextUtils.isEmpty(bean.close_user) ? "" : " by " + bean.close_user;
				holder.stateTv.setText(EntryDetailNumberStateKey.getType(mActivity,bean.number_status) + closeUserStr);
			}
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				holder.main.setBackgroundResource(R.drawable.list_select_bottom_style);
			} else {
				holder.main.setBackgroundResource(R.drawable.list_select_center_style);
			}
			return convertView;
		}

		private class Holder {
			View main;
			TextView doorTv, loadTv, stateTv, customer_id, company_id/* timeTv */;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}
	}
}
