package oso.ui.load_receive.print;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.load_receive.print.bean.PrintPackinglistBean;
import oso.widget.sign.SignatureBoard;
import oso.widget.sign.SignatureBoard.OnSignListener;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import utility.HttpUrlPath;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

public class PrintPackingListWebViewActivity extends Activity {

	private WebView show_webview;
	private Button close_activity;
	private SignatureBoard sb;
	PrintPackinglistBean persons = new PrintPackinglistBean();
	private FrameLayout loadingLayout;
	private TextView psTv;
	String order_no;
	String companyId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.print_packing_webview_layout);

		intentView();
		initView();
	}

	public void intentView() {
		Intent intent = getIntent();
		persons = (PrintPackinglistBean) intent
				.getSerializableExtra("packitem");
	}

	private void initView() {
		order_no = persons.getOrder_no();
		companyId = persons.getCompanyid();
		psTv = (TextView) findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) findViewById(R.id.loadingLayout);
		show_webview = (WebView) findViewById(R.id.show_webview);
		show_webview.getSettings().setJavaScriptEnabled(true);
		WebSettings ws = show_webview.getSettings();
		ws.setSupportZoom(true);
		ws.setBuiltInZoomControls(true);

		final Activity activity = this;

		sb = new SignatureBoard(this);
		sb.setOnSignListener(new OnSignListener() {
			@Override
			public void onSignSuccessed(String savePath, Bitmap bm,String imgUrl) {
				// 刷新页面
				loadingLayout.setVisibility(View.VISIBLE);
				show_webview.reload();
			}
		});
		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});

		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				UIHelper.showToast(activity, "Oh no! " + description,
						Toast.LENGTH_SHORT).show();
			}
		});
		/******************************** 拼接JSON字符串 ******************************/
		StringBuilder url = new StringBuilder(HttpUrlPath.basePath);
		url.append("check_in/a4print_packing_list.html");
		JSONArray jSONArray = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("order_no", order_no + "");
			jsonObject.put("companyId", companyId + "");
			jsonObject.put("customerId", persons.getCustomerid() + "");
			jSONArray.put(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		url.append("?jsonString=").append(jSONArray.toString());
		url.append("&adid=").append(StoredData.getAdid());
		url.append("&isprint=1");
		/************************************************************************/
		show_webview.loadUrl(url.toString());
		System.out.println("签名网址= " + url.toString());
		WebSettings settings = show_webview.getSettings();
		settings.setUseWideViewPort(true);

		settings.setLoadWithOverviewMode(true);

		close_activity = (Button) findViewById(R.id.close_activity);
		close_activity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}
}
