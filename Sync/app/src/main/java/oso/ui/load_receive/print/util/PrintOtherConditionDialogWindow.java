package oso.ui.load_receive.print.util;

import org.json.JSONObject;

import oso.ui.load_receive.print.bean.ReceiptTicketBase;
import oso.ui.load_receive.print.iface.PrintOtherConditionIface;
import support.common.UIHelper;
import support.key.PrintOtherConditionKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: PrintOtherConditionDialogWindow 
 * @Description: 
 * @author gcy
 * @date 2015-3-20 下午12:20:38
 */
public class PrintOtherConditionDialogWindow implements OnClickListener {

	private Context context;
	private Dialog dialog;

	int scWidth = 480;
	int scHeight = 800;
	
	private RadioGroup trailer_loader;
	private RadioGroup freight_counted;
	
	private int trailer_loader_str = 0;
	private int freight_counted_str = 0;
	
	private ReceiptTicketBase receiptTicketBase;
	private PrintOtherConditionIface iface;
	boolean flag = false;//判断按钮是否都已经选中 如果都选中则为true 否则为false
	
	
	public PrintOtherConditionDialogWindow(Context context,ReceiptTicketBase r,PrintOtherConditionIface iface) {
		this.context = context;
		this.receiptTicketBase = r;
		this.iface = iface;
		initDialog(context);
	}

	private void initDialog(Context context) {

		scWidth = getScreenWidth(context);
		scHeight = getScreenHeight(context);
		dialog = new Dialog(context, R.style.signDialog);
		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		window.setWindowAnimations(R.style.signAnim); // 添加动画
		dialog.setContentView(R.layout.show_other_condition_dialog);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(false);

		trailer_loader = (RadioGroup) dialog.findViewById(R.id.trailer_loader);
		freight_counted = (RadioGroup) dialog.findViewById(R.id.freight_counted);

		
		if(receiptTicketBase!=null){
			trailer_loader_str = receiptTicketBase.getTrailer_loader();
			switch (trailer_loader_str) {
			case PrintOtherConditionKey.T_By_Shipper:
				((RadioButton)dialog.findViewById(R.id.t_shipper)).setChecked(true);
				break;
			case PrintOtherConditionKey.T_By_Driver:
				((RadioButton)dialog.findViewById(R.id.t_driver)).setChecked(true);
				break;
			default:
				break;
			}
			
			freight_counted_str = receiptTicketBase.getFreight_counted();
			switch (freight_counted_str) {
			case PrintOtherConditionKey.F_By_Shipper:
				((RadioButton)dialog.findViewById(R.id.f_shipper)).setChecked(true);
				break;
			case PrintOtherConditionKey.F_By_Driver_pallets_said_to_contain:
				((RadioButton)dialog.findViewById(R.id.f_driver_c)).setChecked(true);
				break;
			case PrintOtherConditionKey.F_By_Driver_Pieces:
				((RadioButton)dialog.findViewById(R.id.f_driver_p)).setChecked(true);
				break;
			default:
				break;
			}
			if(trailer_loader_str!=0&&freight_counted_str!=0){
				flag = true;
			}
		}		
		if(!flag){
			dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
					if(keyCode == KeyEvent.KEYCODE_BACK ){
						iface.CloseActivity();
					}
					return false;
				}
			});
		}
		
		trailer_loader.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
		
				@Override
				public void onCheckedChanged(RadioGroup group, int r_id) {
					switch (r_id) {
					case R.id.t_shipper:
						trailer_loader_str = PrintOtherConditionKey.T_By_Shipper;
						break;
					case R.id.t_driver:
						trailer_loader_str = PrintOtherConditionKey.T_By_Driver;
						break;
					default:
						break;
					}
				}
		});
		
		freight_counted.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int r_id) {
				switch (r_id) {
				case R.id.f_shipper:
					freight_counted_str = PrintOtherConditionKey.F_By_Shipper;
					break;
				case R.id.f_driver_c:
					freight_counted_str = PrintOtherConditionKey.F_By_Driver_pallets_said_to_contain;
					break;
				case R.id.f_driver_p:
					freight_counted_str = PrintOtherConditionKey.F_By_Driver_Pieces;
					break;
				default:
					break;
				}
			}
		});
		
		
		(dialog.findViewById(R.id.submit)).setOnClickListener(this);
		
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			submitData();
			break;

		default:
			break;
		}
	}
	
	/**
	 * @Description:提交seal信息
	 * @param @param activity
	 * @param @param mainbean
	 */
	public void dialogForSeal(){
		close();
	}
	
	public void submitData(){
		if(trailer_loader_str==0){
			UIHelper.showToast("Please select Trailer Loader!");
			return;
		}
		if(freight_counted_str==0){
			UIHelper.showToast("Please select Freight Counted!");
			return;
		}
		
		RequestParams params = new RequestParams();
		params.add("Method", "AndroidUpdateLabel");
		params.add("entry_id",receiptTicketBase.getEntryid());
		params.add("load_no", receiptTicketBase.getLoad_no());
 		params.add("trailer_loader", trailer_loader_str+"");
 		params.add("freight_counted", freight_counted_str+"");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				receiptTicketBase.setTrailer_loader(trailer_loader_str);
				receiptTicketBase.setFreight_counted(freight_counted_str);
				iface.ChangeValue(receiptTicketBase);
				close();
			}
		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
	}

	public void show() {
		dialog.show();
	}

	public void close() {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	public void setCancelable(boolean flag){
		if (dialog!=null&&dialog.isShowing()) {
			dialog.setCancelable(flag);
		}
	}
	
	private static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	private static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm;
	}

}
