package oso.ui.load_receive.do_task.receive.task.bean;

import java.io.Serializable;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;

public class Rec_AssignTask_ItemBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1650316586864687090L;
	
	public String receipt_line_id;
	public String receipt_id;
	public String item_id;
	public String line_no;
	public String lot_no;
	
	public int expected_qty;
	public int received_qty;
	public String company_id;
	public String receipt_no;
    public String sn_size;

    //debug
    public int clp_expected_qty;
    public int already_breark_qty;

    public int status;

    //================================================
    public int normal_qty;
    public int damage_qty;
    
    public int sn_normal_qty;
    public int sn_damage_qty;
    //-----------------------------
    public String scan_end_user; // 扫sn的人、时间
	public String scan_end_time;
	
	public String scan_start_time,scan_start_user;	//开始人,时间
	
	
	public int line_status;		//状态,取LineState_x
	public String location;
	
	public String customer;
	public String supplier;
    
    public Rec_ItemBean to_Rec_ItemBean(){
        Rec_AssignTask_ItemBean taskBean=this;
        Rec_ItemBean item=new Rec_ItemBean();
        item.receipt_line_id=taskBean.receipt_line_id;
        item.item_id=taskBean.item_id;
        item.lot_no=taskBean.lot_no;
        item.receipt_id=taskBean.receipt_id;
        item.expected_qty=taskBean.expected_qty;
        item.normal_qty=taskBean.normal_qty;
        item.damage_qty=taskBean.damage_qty;
        item.clp_expected_qty=taskBean.clp_expected_qty;
        item.already_breark_qty=taskBean.already_breark_qty;
        item.sn_size=taskBean.sn_size;
        item.receipt_no=taskBean.receipt_no;
        item.scan_end_time=taskBean.scan_end_time;
        item.scan_end_user=taskBean.scan_end_user;
        item.scan_start_time=taskBean.scan_start_time;
        item.scan_start_user=taskBean.scan_start_user;

        return item;
    }
    
    
    public int getTotoalQty(){
    	return sn_normal_qty + sn_damage_qty;
    }
	
}
