package oso.ui.load_receive.do_task.receive.wms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.process.Rec_RemoveDupAc;
import oso.ui.load_receive.do_task.receive.process.bean.Rec_RepeatPalletBean;
import oso.ui.load_receive.do_task.receive.wms.adapter.AdpRepeatPlts;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Receive_palletsSn;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import support.AppConfig;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.PrintTool;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.key.TTPKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 装货界面
 * 
 * @author 朱成
 * @date 2015-3-2
 */
public class Receive_ReceiveAc extends BaseActivity implements OnCheckedChangeListener, OnClickListener {

	private final int Req_RemoveSN = 1; // 移除sn

	// private SingleSelectBar tab;
	private ListView lvSN;

	private TextView tvSN_title, tvSN_ok, locTv, itemTv;
	private CheckBox checkBoxSku, checkBoxLNO;
	Button MyCheckBox_X;
	private EditText edittextySku;
	private SearchEditText edittextyLNO;
	// private String snString, skuString, lnoString;
	private SearchEditText etSn;
	// private Receive_ItemSetUp bean;
	// private Button snbtnSub;
	// private Context context;
	// Rec_PalletBean palletsbean;
	private List<Receive_palletsSn> listSn;
	Receive_palletsSn receive_palletsSn/* = new Receive_palletsSn() */;
	private ImageView photoIv;
	private TextView tvImgCnt;

	private TabToPhoto ttp_dlg;

	private Button btnSubmit;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_rec_rec, 0);
		applyParams();

		// context = Receive_ReceiveAc.this;
		// 取v
		// tab = (SingleSelectBar) findViewById(R.id.tab);
		lvSN = (ListView) findViewById(R.id.lvSN);
		tvSN_title = (TextView) findViewById(R.id.tvSN_title);
		checkBoxSku = (CheckBox) findViewById(R.id.sku_checkbox);
		checkBoxLNO = (CheckBox) findViewById(R.id.lno_checkbox);
		edittextySku = (EditText) findViewById(R.id.sku_editText);
		edittextyLNO = (SearchEditText) findViewById(R.id.lno_editText);
		etSn = (SearchEditText) findViewById(R.id.etMcDot);
		// snbtnSub = (Button) findViewById(R.id.snbtnSub);
		MyCheckBox_X = (Button) findViewById(R.id.checkAllBox);
		tvSN_ok = (TextView) findViewById(R.id.tvSN_ok);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		itemTv = (TextView) findViewById(R.id.itemTv);
		locTv = (TextView) findViewById(R.id.locTv);
		photoIv = (ImageView) findViewById(R.id.photoIv);
		tvImgCnt = (TextView) findViewById(R.id.tvImgCnt);

		// 事件
		btnSubmit.setOnClickListener(this);
		checkBoxSku.setOnCheckedChangeListener(this);
		checkBoxLNO.setOnCheckedChangeListener(this);
		MyCheckBox_X.setOnClickListener(this);
		photoIv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPhotoDialog(pltBean);
			}
		});
		etSn.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				etSn.setText("");
				Utility.colseInputMethod(mActivity, etSn);
				

				// 超出了总数
				// if (listSn.size() >= pltBean.getSnQty_needScan()) {
				// TTS.getInstance().speakAll_withToast("total quantity override!",true);
				// return;
				// }
				// 空校验
				if (TextUtils.isEmpty(value)) {
					TTS.getInstance().speakAll_withToast("Invalid SN!", true);
					return;
				}
				
				
				value = value.toUpperCase();
				// 只能是 数字  字母
				String regEx="^[A-Za-z0-9-]+$";
				Pattern pattern = Pattern.compile(regEx);
				Matcher matcher = pattern.matcher(value);
				
				if(!matcher.find()){
					TTS.getInstance().speakAll_withToast("Invalid SN!", true);
					return;
				}
				
				// 验证-长度
				if (!itemBean.checkSNSize(value)) {
					TTS.getInstance().speakAll_withToast("Invalid SN size!", true);
					return;
				}

				// 本托盘-不允许扫重
				// if (pltBean.hasSn(value)) {
				// TTS.getInstance().speakAll_withToast("Duplicate SN in same pallet!",true);
				// Utility.vibrate();
				// return;
				// }

				// 本托盘-不允许扫重
				Receive_palletsSn sn_sameplt = pltBean.getFirstSN(value);
				if (sn_sameplt != null) {
//					"Duplicate SN in same pallet!"
					TTS.getInstance().speakAll_withToast(
							"Duplicate!", true);
					Utility.vibrate();

					// 选中-第一项
					sn_lastScan = sn_sameplt.serial_number;
					int k = pltBean.sn.indexOf(sn_sameplt);
					adpSN.notifyDataSetChanged();
					lvSN.setSelection(k);
					return;
				}

				List<Rec_PalletBean> listRep = Rec_PalletBean.getPlts_hasSn(value, listPlts, pltBean);
				// 其他plt有重的,弹框提示确认
				if (!Utility.isEmpty(listRep)) {
					TTS.getInstance().speakAll_withToast("Duplicate SN! " + "Please remove!", true);
					duplMnger.showDlg_tipRepeat(listRep, value);
				} else
					addSn(value, false);

			}
		});
		edittextyLNO.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				edittextyLNO.setText("");
				// 若相同
				if (itemBean.lot_no.equals(value)) {
					lockLotNo(true);
					TTS.getInstance().speakAll_withToast("Ready for scan!");
				} else
					TTS.getInstance().speakAll_withToast("Lot Number doesn't match!", true);
			}
		});

		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>("SN", 0));
		list.add(new HoldDoubleValue<String, Integer>("SKU", 1));
		// tab.setUserDefineClickItems(list);
		// tab.userDefineSelectIndexExcuteClick(0);
		// tab.setUserDefineCallBack(new SelectBarItemClickCallBack() {
		//
		// @Override
		// public void clickCallBack(HoldDoubleValue<String, Integer>
		// selectValue) {
		// // TODO Auto-generated method stub
		// update_snTitle();
		// }
		// });

		// 补上-缺少的,debug
		int initIndex = listSn.size();
		int lack = pltBean.getSnQty_needScan() - initIndex;
		// debug,自动补全sn
		if (AppConfig.Debug_Receive && lack > 0) {
			int base = (int) (Math.random() * 100000);
			for (int i = 0; i < lack; i++) {
				Receive_palletsSn tmpSn = new Receive_palletsSn();
				// tmpSn.serial_number = "SN_" + (i + initIndex);
				tmpSn.serial_number = getMockSn(base, itemBean.sn_size, i + initIndex);
				if(pltBean.isDamagePlt())
					tmpSn.is_damage=1;
				listSn.add(tmpSn);
			}
		}
		// 补全line
		if (AppConfig.Debug_Receive) {
			showRightButton(R.drawable.menu_btn_style, "补全line", new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int lack = itemBean.getCntQty() - Rec_PalletBean.getScanedSn(listPlts);
					int base = (int) (Math.random() * 100000);
					for (int i = 0; i < lack; i++) {
						Receive_palletsSn tmpSn = new Receive_palletsSn();
						// tmpSn.serial_number = "SN_" + (i + initIndex);
						tmpSn.serial_number = getMockSn(base, itemBean.sn_size, i);
						listSn.add(tmpSn);
					}
					adpSN.notifyDataSetChanged();
					refSummaryUI();
				}
			});
		}

		// 初始化
        String lpType=pltBean.isClp()?"CLP:  ":"TLP:  ";
        setTitleString(lpType + pltBean.getDisPltNO());
//		setTitleString("Pallet:  " + pltBean.getDisPltNO());
		refrashPhotoNum(pltBean);
		locTv.setText(pltBean.location);
		itemTv.setText(itemBean.item_id);
		// debug
		// checkBoxSku.setChecked(itemBean.itemsetup.isIs_need_sn());
		// checkBoxLNO.setChecked(itemBean.itemsetup.isIs_check_lot_no());
		refSummaryUI();
		// update_snTitle();
		lockLotNo(true);
		edittextyLNO.setScanMode();
		etSn.setScanMode();
		
		if(itemBean.lot_no.equalsIgnoreCase("None")){
			edittextyLNO.setHint(itemBean.lot_no);
			edittextyLNO.setEnabled(false);
		}else{
			showDlg_verifyLot();
		}

		// lv
		lvSN.setAdapter(adpSN);
		lvSN.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showDlg_contextMenu(position);
			}
		});
	}
	
	private void showDlg_contextMenu(final int position_item) {
		final Receive_palletsSn tmpSn=listSn.get(position_item);
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_operation));
		String toType=tmpSn.is_damage()?"Good SN":"Damage SN";
		
		String[] menu;
		//坏plt 不能改状态
		if(pltBean.isDamagePlt())
			menu=new String[]{"Delete SN"};
		else 
			menu=new String[]{"Delete SN",toType};
		builder.setArrowItems(menu, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				switch (position) {	
				case 0:					//删除
					showDlg_delSn(position_item);
					break;
				case 1:					//转类型
					boolean isToDmg=!tmpSn.is_damage();
					//服务端的-直接改
					if(tmpSn.isServer())
						reqChangeSNType(tmpSn, isToDmg);
					else
						tmpSn.setIs_damage(isToDmg);
					adpSN.notifyDataSetChanged();
					break;

				default:
					break;
				}
			}
		});
		builder.show();
	}
	
	private void reqChangeSNType(final Receive_palletsSn snBean,final boolean toDmg){
//		/android/markSN?sp_id=1&is_damage=1 ;
		
		RequestParams p=new RequestParams();
		p.add("sp_id", snBean.serial_product_id);
		p.add("is_damage", toDmg?"1":"0");
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				snBean.setIs_damage(toDmg);
				adpSN.notifyDataSetChanged();
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/markSN", p, this);
		
	}
	
	private void refrashPhotoNum(Rec_PalletBean b) {
		photoIv.setSelected(b.hasPhoto());
		tvImgCnt.setVisibility(b.hasPhoto() ? View.VISIBLE : View.GONE);
		tvImgCnt.setText(b.getPhotoCnt() + "");
	}

	private void showPhotoDialog(final Rec_PalletBean b) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp_dlg = (TabToPhoto) view.findViewById(R.id.ttp);
		// ttp.initNoTitleBar(mActivity, new TabParam("tabName", "key", new
		// PhotoCheckable(0, "title", ttp)));
		initTTP(ttp_dlg, b);
		BottomDialog.showCustomViewDialog(mActivity, "Pallet Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp_dlg.getPhotoCnt(false) > 0)
					reqUploadPhotos(b, ttp_dlg);
				dlg.dismiss();
			}
		});
	}

	private void reqUploadPhotos(final Rec_PalletBean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		p.add("con_id", b.con_id);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("photo");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = new Gson().fromJson(ja.toString(), new TypeToken<List<TTPImgBean>>() {
				}.getType());
				b.photos = listPhotos;
				refrashPhotoNum(b);
			}
		}.doPost(HttpUrlPath.basePath + "_receive/android/uploadPhoto", p, this);

	}

	private void initTTP(TabToPhoto ttp, final Rec_PalletBean pltBean) {
		String key = TTPKey.getRecPltKey(receipt_no, pltBean.con_id);
		ttp.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp.pvs[0].addWebPhotoToIv(pltBean.photos);
		ttp.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = pltBean.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					refrashPhotoNum(pltBean);
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp_dlg != null)
			ttp_dlg.onActivityResult(requestCode, resultCode, data);
		// 刷新重复dlg
		if (requestCode == Req_RemoveSN)
			duplMnger.refreshDlg();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.lno_checkbox: // lotNO锁
			lockLotNo(isChecked);
			UIHelper.showToast(isChecked ? "Scan SN!" : "Scan Lot NO!");
			break;

		default:
			break;
		}

	}

	private void addSn(String snNO, boolean isRepeat) {
		receive_palletsSn = new Receive_palletsSn();
		receive_palletsSn.serial_number = snNO;
		//debug,坏托盘
		if(pltBean.isDamagePlt())
			receive_palletsSn.is_damage=1;
		listSn.add(0, receive_palletsSn);
		sn_lastScan = snNO;
		// 标识重复
		if (isRepeat)
			pltBean.markRepeatSN(snNO);

		adpSN.notifyDataSetChanged();
		// debug
		lvSN.setSelection(0);

		etSn.setText("");
		boolean finish = refSummaryUI();
		Utility.colseInputMethod(mActivity, etSn);

		// 会超出 叫毛的finish
//		TTS.getInstance().speakAll("SN " + listSn.size());
//		TTS.getInstance().speakAll(listSn.size()+"");

		 // tts
		 if (!finish)
			 TTS.getInstance().speakAll(listSn.size()+"");
		 // plt完成时
		 else {
			 showDlg_confirmSubmit();
			 TTS.getInstance().speakAll("Pallet finish!");
		 }
	}

	private void toRemoveDuplAc(Rec_PalletBean pltDupl, String snNO_dulp) {
		Intent in = new Intent(mActivity, Rec_RemoveDupAc.class);
		Rec_RemoveDupAc.initParams(in, itemBean, listPlts, pltDupl, snNO_dulp);
		startActivityForResult(in, Req_RemoveSN);
	}

	public static String getMockSn(int base, String sn_size, int index) {
		if(TextUtils.isEmpty(sn_size)){
			return "SN_" + base + "_" + index;
		}
		String[] strSizes = sn_size.split(",");
		List<String> strLists = Arrays.asList(strSizes);
		//无限制
		if(strLists.contains("0")){
			return "SN_" + base + "_" + index;
		}

		long k = (long) Math.pow(10, Integer.valueOf(strLists.get(0))) + index;
		k = k + base;

		String str = k + "";
		return str.substring(1);
	}

	// @Override
	// protected void onFirstVisible() {
	// // TODO Auto-generated method stub
	// super.onFirstVisible();
	// TTS.getInstance().speakAll("Ready!");
	// }

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Receive_ReceiveAc.super.onBackBtnOrKey();
			}
		});
	}

	/**
	 * 验证lot
	 */
	private void showDlg_verifyLot() {

		View v = getLayoutInflater().inflate(R.layout.dlg_rec_scan_verifylot, null);
		final SearchEditText etLotNO_dlg = (SearchEditText) v.findViewById(R.id.etLotNO_dlg);
		
		etLotNO_dlg.setScanMode();

		// debug
//		if (AppConfig.Debug_Receive)
//			etLotNO_dlg.setText(itemBean.lot_no);
		etLotNO_dlg.setHint(itemBean.lot_no);
		etLotNO_dlg.setTransformationMethod(AllCapTransformationMethod.getThis());

		final BottomDialog dlg = new BottomDialog(mActivity).setTitle("Verify Lot NO").setView(v).setCanceledOnTouchOutside(false)
				.setShowCancelBtn(false).hideButton();
		dlg.getDlg().setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				// TODO Auto-generated method stub
				TTS.getInstance().speakAll("Scan Lot Number!");
			}
		});
		dlg.show();

		// 事件
		etLotNO_dlg.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				// 消耗掉长按,防止dlg.dismiss后 还弹出"粘贴" 返回后报"popwindow没附加到window"
				return true;
			}
		});
		etLotNO_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				if(TextUtils.isEmpty(value))
				{
					String customerId = pltBean.customer_id;
					if(!TextUtils.isEmpty(customerId) && customerId.toLowerCase().indexOf("vizio")!=-1)
					{
						UIHelper.showToast("Please Scan Lot NO!");
						return;
					}
					show_confirmLot_whenNoInput(dlg);
					return;
				}
					
				etLotNO_dlg.setText("");
				// 若相同
				if (itemBean.lot_no.equalsIgnoreCase(value)) {
					dlg.dismiss();

					lockLotNo(true);
					TTS.getInstance().speakAll_withToast("Ready for scan!");
				} else
					TTS.getInstance().speakAll_withToast("Lot Number doesn't match!", true);
			}
		});

		dlg.getDlg().setOnKeyListener(new DialogInterface.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				// 返回时,直接退出
				if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
					//销毁窗体,否则会"WindowLeaked"
					dialog.dismiss();
					Receive_ReceiveAc.super.finish();
					return true;
				}
				return false;
			}
		});

	}
	
	private void show_confirmLot_whenNoInput(final BottomDialog dlg){
		RewriteBuilderDialog.Builder bd=RewriteBuilderDialog.newSimpleDialog(this, "Lot NO: "+itemBean.lot_no, 
				new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dlg.dismiss();

						lockLotNo(true);
						TTS.getInstance().speakAll_withToast("Ready for scan!");
					}
				});
		bd.hideCancelBtn();
		bd.show();
	}

	private void lockLotNo(boolean toLock) {
		edittextyLNO.setText(toLock ? itemBean.lot_no : "");
		edittextyLNO.setEnabled(!toLock);
		etSn.setEnabled(toLock);

		if (toLock)
			etSn.requestFocus();
		else
			edittextyLNO.requestFocus();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSubmit: // 提交
			// int snSize = listSn.size();
			// RewriteBuilderDialog.showSimpleDialog(this,
			// snSize + " SN, finish?",
			// new DialogInterface.OnClickListener() {
			//
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// // TODO Auto-generated method stub
			// reqSubmit();
			// }
			// });
			showDlg_confirmSubmit();
			break;
		case R.id.checkAllBox: // 删除所有
			RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_all), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					RequestParams params = new RequestParams();
					params.put("con_id", pltBean.con_id);
					new SimpleJSONUtil() {
						@Override
						public void handReponseJson(JSONObject json) {
							listSn.clear();
							adpSN.notifyDataSetChanged();
							refSummaryUI();
							UIHelper.showToast(getString(R.string.sync_delete_success));
						}

					}.doPost(HttpUrlPath.deletePalletSN, params, mActivity);
				}
			});
			break;

		default:
			break;
		}
	}

	/**
	 * 确认
	 */
	private void showDlg_confirmSubmit() {
		// v
		View v = getLayoutInflater().inflate(R.layout.dlg_rec_rec_confirm, null);
		TextView tvExpQty_dlg = (TextView) v.findViewById(R.id.tvExpQty_dlg);
		TextView tvScanedQty_dlg = (TextView) v.findViewById(R.id.tvScanedQty_dlg);
		View loExpQty_dlg = v.findViewById(R.id.loExpQty_dlg);

		// 初始化
//		tvExpQty_dlg.setText(pltBean.getCntQty() + "");
        int snQty_needScan=pltBean.getSnQty_needScan();
        tvExpQty_dlg.setText(snQty_needScan + "");
		tvScanedQty_dlg.setText(listSn.size() + "");
		loExpQty_dlg.setVisibility(snQty_needScan>0? View.VISIBLE : View.GONE);

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setContentView(v);
		bd.setPositiveButton(getString(R.string.task_item_close_title), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqSubmit(true);
			}
		});
		// 若已关闭 不显示submit
		boolean showSubmit=true;
		if(pltBean.isClosed())
			showSubmit=false;
//		if(pltBean.getCntQty()==listSn.size())
        //debug
        if(pltBean.getSnQty_needScan()==listSn.size())
			showSubmit=false;
		
		if (showSubmit) {
			bd.setMidButton(getString(R.string.sync_submit), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					reqSubmit(false);
				}
			});
		}
		bd.show();

	}

	/**
	 * 刷新已扫描数
	 */
	private boolean refSummaryUI() {
		int totalSn = pltBean.getSnQty_needScan();
		int scanedSn = listSn.size();

		// 若超出 则跟着变 'x,line.cnt数不会因提交sn改变了
		// if (scanedSn > totalSn)
		// totalSn = scanedSn;
		// tvSN_ok.setText(scanedSn + " / " + totalSn);
		String disCnt = scanedSn + "";
		// 有期望值 才加
		if (totalSn > 0)
			disCnt = disCnt + " / " + totalSn;
		tvSN_ok.setText(disCnt);

		boolean scanedFinish = totalSn == scanedSn;
		// btnSubmit.setVisibility(scanedFinish?View.VISIBLE:View.GONE);
		return scanedFinish;
	}

	private PrintTool printTool = new PrintTool(this);

	private void reqPrint(long printServerID) {
		RequestParams p = NetInterface.recPrintLP_p(printServerID,NetInterface.PrintLP_PltIDs , pltBean.con_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				String name = StoredData.getUsername();
				pltBean.print_user_name = name;

				// debug,打印完后-提交
				// reqSubmit();
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);
	}

	private void reqSubmit(boolean isClose) {

		// 提示-打印
		// if (!pltBean.isPrinted()) {
		// printTool.showDlg_printers(new PrintTool.OnPrintLs() {
		//
		// @Override
		// public void onPrint(long printServerID) {
		// // TODO Auto-generated method stub
		// reqPrint(printServerID);
		// }
		// }, "Print Pallet Label");
		// return;
		// }
		if(!Receive_palletsSn.isequalsLength(listSn)){
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please Check SN");
			return;
		}

		final JSONArray json = new JSONArray();
		try {
			for (int i = 0; i < listSn.size(); i++) {
				JSONObject jo = new JSONObject();
				Receive_palletsSn tmpSn=listSn.get(i);
				jo.put("sn", tmpSn.serial_number);
				jo.put("is_damage", tmpSn.is_damage()?"1":"0");
				json.put(jo);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final RequestParams params = new RequestParams();
		params.add("con_id", pltBean.con_id);
		params.add("sns", json.toString());
		params.add("receipt_id", itemBean.receipt_id);
		params.add("lot_no", pltBean.lot_no);
		params.add("item_id", pltBean.item_id);
		params.add("is_close", isClose ? "1" : "0"); // 1:关闭plt
		// params.add("isTask", "0");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_success));
				// 若为最后一个,至lineList
				// if (pltBean.isLastPlt_inLine)
				// Utility.popTo(mActivity, Rec_LineListAc.class);
				// else
				if(json.optInt("err") == 91){
					Rec_RepeatPalletBean repeatPalletBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_RepeatPalletBean.class);
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
					
					View v = View.inflate(mActivity, R.layout.item_repeatpallet, null);
					TextView tvSn = (TextView) v.findViewById(R.id.tvSN);
					TextView tvPallet1 = (TextView) v.findViewById(R.id.tvPallet1);
					TextView tvPallet2 = (TextView) v.findViewById(R.id.tvPallet2);
					tvSn.setText(repeatPalletBean.serial_number);
					String[] pallets = repeatPalletBean.getContainer();
					tvPallet1.setText(pallets[0]);
					tvPallet2.setText(pallets[1]);
					
					builder.setContentView(v);
					
					builder.isHideCancelBtn(true);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//如果 有重复， 什么也不做
						}
					});
					builder.show();
					return;
				}
				finish();
			}

		}.setCancelable(false).doPost(HttpUrlPath.bindSNInfoToPallet, params, mActivity);
	}

	// private void update_snTitle() {
	// tvSN_title.setText(isScan_Sn() ? "SN " : "Qty ");
	// }

	// private boolean isScan_Sn() {
	// return tab.getCurrentSelectItem().b == 0;
	// }

	private void showDlg_delSn(final int pos) {

		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Receive_palletsSn bean = listSn.get(pos);
				final String snNO = bean.serial_number;
				if (!Receive_palletsSn.isServer(bean)) {
					// 移除-重复标识
					Receive_palletsSn.removeRepeatMark(listPlts, pltBean, snNO);
					listSn.remove(pos);
					adpSN.notifyDataSetChanged();
					refSummaryUI();
					UIHelper.showToast(getString(R.string.sync_delete_success));
					return;
				}

				RequestParams params = new RequestParams();
				params.put("sp_id", bean.serial_product_id);
				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						// 移除-重复标识
						Receive_palletsSn.removeRepeatMark(listPlts, pltBean, snNO);
						listSn.remove(pos);
						adpSN.notifyDataSetChanged();
						refSummaryUI();
						UIHelper.showToast(getString(R.string.sync_delete_success));
					}

				}.doPost(HttpUrlPath.deleteSN, params, mActivity);
			}
		});

	}

	// =========传参===============================

	private static List<Rec_PalletBean> listPlts_bridge;
	private static Rec_PalletBean pltBean_bridge;

	private Rec_ItemBean itemBean;
	private Rec_PalletBean pltBean;

	private List<Rec_PalletBean> listPlts;

	private static String receipt_no;
	

	public static void initParams(Intent in, Rec_ItemBean itemBean, List<Rec_PalletBean> listPlts, Rec_PalletBean pltBean, String receiptNo) {
		in.putExtra("itemBean", itemBean);
		listPlts_bridge = listPlts;
		pltBean_bridge = pltBean;
		receipt_no = receiptNo;
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
		pltBean = pltBean_bridge;
		if (pltBean.sn == null)
			pltBean.sn = new ArrayList<Receive_palletsSn>();
		listSn = pltBean.sn;

		listPlts = listPlts_bridge;
		listPlts_bridge = null;
		pltBean_bridge = null;
	}

	// ==================nested===============================

	private String sn_lastScan = ""; // 上一scan的sn

	private BaseAdapter adpSN = new BaseAdapter() {

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_scan_sn, null);
				h = new Holder();
				h.vItem = convertView.findViewById(R.id.vItem);
				h.tvSN = (TextView) convertView.findViewById(R.id.tvSN);
				h.tvRepeat = convertView.findViewById(R.id.tvRepeat);
				h.vLastScan = convertView.findViewById(R.id.vLastScan);
				h.tvD = convertView.findViewById(R.id.tvD);
				convertView.setTag(h);
			} else {
				h = (Holder) convertView.getTag();
			}

			// 刷ui
			Receive_palletsSn b = listSn.get(position);
			h.tvSN.setText("SN: " + b.serial_number + "");
			h.tvRepeat.setVisibility(b.is_repeat() ? View.VISIBLE : View.GONE);
			
			boolean showDmg=b.is_damage();
			if(pltBean.isDamagePlt())
				showDmg=true;
			h.tvD.setVisibility(showDmg?View.VISIBLE:View.GONE);
			
			// h.vItem.setSelected(b.isEqual(sn_lastScan));
			// h.vItem.setSelected(true);
			// sync_gray_line,sync_gray_bg
			// int
			// bgColor_res=b.isEqual(sn_lastScan)?R.color.sync_gray_line:android.R.color.transparent;
			// h.vItem.setBackgroundResource(bgColor_res);
			h.vLastScan.setVisibility(b.isEqual(sn_lastScan) ? View.VISIBLE : View.INVISIBLE);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listSn != null ? listSn.size() : 0;
		}
	};

	private class Holder {
		TextView tvSN;
		View tvRepeat, vItem, vLastScan, tvD;
	}

	// ==================nested===============================

	private DulpMnger duplMnger = new DulpMnger();

	private class DulpMnger {

		// public List<Rec_PalletBean> listPlts_rep;
		private String sn_rep;

		private RewriteBuilderDialog dlg_repeat;
		private AdpRepeatPlts adp_repeat;
		private List<Rec_PalletBean> listRep = new ArrayList<Rec_PalletBean>();

		public DulpMnger() {
		}

		public void refreshDlg() {
			// 若dlg无效
			if (!checkRes()) {
				clearRes();
				return;
			}

			List<Rec_PalletBean> tmpRep = Rec_PalletBean.getPlts_hasSn(sn_rep, listPlts, pltBean);
			// 仍有重的
			if (!Utility.isEmpty(tmpRep)) {
				listRep.clear();
				listRep.addAll(tmpRep);
				adp_repeat.notifyDataSetChanged();
			} else {
				clearRes();
				RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Dupl. SN " + sn_rep + " removed success!");
			}
		}

		/**
		 * dlg是否有效
		 * 
		 * @return
		 */
		private boolean checkRes() {
			if (dlg_repeat == null || !dlg_repeat.isShowing() || adp_repeat == null || listRep == null)
				return false;
			return true;
		}

		private void clearRes() {
			if (dlg_repeat != null && dlg_repeat.isShowing())
				dlg_repeat.dismiss();
			dlg_repeat = null;
			adp_repeat = null;
			// listRep=null;
		}

		/**
		 * 显示有重的plts
		 * 
		 * @param listPlts_rep
		 * @param snNO
		 */
		public void showDlg_tipRepeat(final List<Rec_PalletBean> listPlts_rep, final String snNO) {
			sn_rep = snNO;
			listRep = listPlts_rep;

			View v = getLayoutInflater().inflate(R.layout.rec_repeatsn_item, null);
			TextView tvSN_rep = (TextView) v.findViewById(R.id.tvSN_rep);
			ListView lvRepeatPlts = (ListView) v.findViewById(R.id.lvRepeatPlts);
			final SearchEditText etDuplPlt = (SearchEditText) v.findViewById(R.id.etDuplPlt);
			// 初始化
			tvSN_rep.setText("SN " + snNO + " has been scanned in:");
			etDuplPlt.setScanMode();

			// 事件
			etDuplPlt.setSeacherMethod(new SeacherMethod() {

				@Override
				public void seacher(String value) {
					// TODO Auto-generated method stub
					etDuplPlt.setText("");

					Rec_PalletBean tmpPlt = Rec_PalletBean.getPallet_byScanNO(listPlts_rep, value);
					if (tmpPlt == null) {
						TTS.getInstance().speakAll_withToast("Pallet not match!");
						return;
					}
					TTS.getInstance().speakAll_withToast("Pallet match!");
					toRemoveDuplAc(tmpPlt, snNO);
				}
			});

			// lv
			adp_repeat = new AdpRepeatPlts(mActivity, listPlts_rep);
			lvRepeatPlts.setAdapter(adp_repeat);
			lvRepeatPlts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					toRemoveDuplAc(listPlts_rep.get(position), snNO);
				}
			});

			RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(mActivity);
			bd.setTitle("Dupl. SN");
			bd.setContentView(v);
			bd.setPositiveButton(getString(R.string.sync_cancel), null);
			// addSn(snNO, true);
			bd.hideCancelBtn();
			dlg_repeat = bd.show();
		}

	}

}
