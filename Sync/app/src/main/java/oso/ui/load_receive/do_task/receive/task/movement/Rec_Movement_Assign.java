package oso.ui.load_receive.do_task.receive.task.movement;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONObject;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoLinesBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoPalletBean;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteBuilderDialog.Builder;
import oso.widget.orderlistview.ChooseUsersActivity;
import support.AppConfig;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;

public class Rec_Movement_Assign extends BaseActivity implements OnClickListener{
	
	private final int What_ChooseSupervisor = 1;
	private String adid_notify; // 人
	
	
	
	private TextView tvReceiptNo,tvCustomer,tvTitle,tvNote;
	private ExpandableListView lvMovement;
	private Button btnAssign;
	private Button btnFinish;
	
	private MovementAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_rec_movement_assign,0);
		applyParams();
		
		tvReceiptNo = (TextView) findViewById(R.id.tvReceiptNo);
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		tvNote = (TextView) findViewById(R.id.tvNote);
		lvMovement =  (ExpandableListView) findViewById(R.id.lvMovement);
		btnAssign = (Button) findViewById(R.id.btnAssign);
		btnFinish = (Button) findViewById(R.id.btnFinish);
		
		btnAssign.setOnClickListener(this);
		btnFinish.setOnClickListener(this);
		
		
		setData();
	}
	
	private void setData(){
		
		setTitleString("Request Movement Task");
		
		if(fromSign == To_Assign){
			btnAssign.setVisibility(View.VISIBLE);
			btnFinish.setVisibility(View.GONE);
		}else{
			btnAssign.setVisibility(View.GONE);
			btnFinish.setVisibility(View.VISIBLE);
		}
		
		tvReceiptNo.setText(infoBean.receipt_no);
		tvCustomer.setText(infoBean.customer_id);
		tvTitle.setText(infoBean.supplier_id);
		
		adapter = new MovementAdapter();
		lvMovement.setAdapter(adapter);
		lvMovement.setEmptyView(findViewById(R.id.tvEmpty));
		
		for(int i=0; i < adapter.getGroupCount(); i++){
			lvMovement.expandGroup(i);
		}
		
		lvMovement.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				return true;
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_CANCELED)
			return;
		
		switch (requestCode) {
		case What_ChooseSupervisor: // 选管理员
			String svNames = data.getStringExtra("select_user_names");
			adid_notify = data.getStringExtra("select_user_adids");
			
			showConfirm_Choose(svNames);
			break;

		default:
			break;
		}
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnAssign:
			adid_notify = "";
			toChooseUser();
			break;
		case R.id.btnFinish:
			toFinish();
			break;
		}
	}
	
	private void toFinish(){
		RequestParams p = new RequestParams();
		p.add("receipt_id", infoBean.receipt_id+"");
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_success));
				setResult(RESULT_OK);
				finish();
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/closeMovenmentSchedule", p, this);
	}
	
	@Override
	protected void onBackBtnOrKey() {
		if(fromSign == To_Assign){
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle("Request Movement Task");
			builder.setMessage("Exit!");
			builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					setResult(RESULT_OK);
					finish();
					
				}
			});
			builder.show();
			return;
		}else{
			super.onBackBtnOrKey();
		}
		
	}
	private void reqAssign() {

		if (TextUtils.isEmpty(adid_notify)) {
			UIHelper.showToast(getString(R.string.sync_select_labor));
			return;
		}
		
		RequestParams p = new RequestParams();
		p.add("receipt_id", infoBean.receipt_id+"");
		p.add("operator", adid_notify);
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finish();
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/assignMovementTaskToSupervisor", p, this);
	}
	
	private void showConfirm_Choose(String person){
		
		
		final BottomDialog bd = new BottomDialog(mActivity);
		
		bd.setTitle("Assign Movement Task");
		
		View v = View.inflate(mActivity, R.layout.dialog_cc_configac_choose, null);
		
		bd.setContentView(v);
		
		TextView tv = (TextView) v.findViewById(R.id.notifyEt);
		tv.setText(person);
		
		tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toChooseUser();
				bd.dismiss();
			}
		});
		
		bd.setOnSubmitClickListener(getString(R.string.assign_task_assign), new BottomDialog.OnSubmitClickListener() {
			
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				reqAssign();
				dlg.dismiss();
			}
		});
		
		bd.show();
	}
	
	private void toChooseUser(){
		int dep=BaseDataDepartmentKey.Scanner;
		int pos= BaseDataSetUpStaffJobKey.Supervisor;
        //debug
		if (AppConfig.isDebug) {
			dep = BaseDataDepartmentKey.Warehouse;
			pos = BaseDataSetUpStaffJobKey.All;
		}

		Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
		intent.putExtra("selected_adids", adid_notify);
		ChooseUsersActivity.initParams(intent, true, dep, pos, ChooseUsersActivity.KEY_REC_SCANLABOR);
		startActivityForResult(intent, What_ChooseSupervisor);
	}
	
	//--
	public static final int To_Assign = 0;
	public static final int To_Finish = 1;
	
	private Rec_Movement_InfoBean infoBean;
	private List<Rec_Movement_InfoLinesBean> listLines;
	private int fromSign;
	//-----------------------------------------------
	public static void initParams(Intent in,Rec_Movement_InfoBean bean,List<Rec_Movement_InfoLinesBean> lines,int sign){
		in.putExtra("infoBean", bean);
		in.putExtra("listLines", (Serializable)lines);
		in.putExtra("sign", sign);
	}
	private void applyParams(){
		Intent in = getIntent();
		infoBean = (Rec_Movement_InfoBean) in.getSerializableExtra("infoBean");
		listLines = (List<Rec_Movement_InfoLinesBean>) in.getSerializableExtra("listLines");
		fromSign = in.getIntExtra("sign", 0);
		
		for(Rec_Movement_InfoLinesBean bean : listLines){
			Collections.sort(bean.pallets,new BaseComparator());
		}
	}
	
	private class BaseComparator implements Comparator<Rec_Movement_InfoPalletBean> {

		@Override
		public int compare(Rec_Movement_InfoPalletBean lhs,
				Rec_Movement_InfoPalletBean rhs) {
			return new Integer(lhs.location_exists).compareTo(new Integer(rhs.location_exists));
		}
		
	}
	
	
	//------------------------------------
	
	private class MovementAdapter extends BaseExpandableListAdapter{

		@Override
		public int getGroupCount() {
			return listLines.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return listLines.get(groupPosition).pallets.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return null;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			
			if(convertView == null){
				convertView = View.inflate(mActivity, R.layout.item_movement_pallet, null);
				
			}
			
			TextView tvItem = (TextView) convertView.findViewById(R.id.tvItemID);
			tvItem.setText("Item ID: "+listLines.get(groupPosition).item_id);
			
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			
			ChildHolder holder;
			if(convertView == null){
				convertView = View.inflate(mActivity, R.layout.item_movement_palletinfo, null);
				holder = new ChildHolder();
				
				holder.tvReceipt = (TextView) convertView.findViewById(R.id.tvReceipt);
				holder.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
				holder.tvError = (TextView) convertView.findViewById(R.id.tvError);
				holder.main = (RelativeLayout) convertView.findViewById(R.id.main);
				
				convertView.setTag(holder);
			}else{
				holder = (ChildHolder) convertView.getTag();
			}
			Rec_Movement_InfoPalletBean palletBean = listLines.get(groupPosition).pallets.get(childPosition);
			holder.tvReceipt.setText(palletBean.container+"");
			holder.tvLocation.setText(palletBean.location);
			holder.tvError.setVisibility(palletBean.location_exists == 0 ? View.VISIBLE:View.GONE);
			
			// 背景
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				holder.main.setBackgroundResource(R.drawable.list_select_bottom_style);
			} else {
				holder.main.setBackgroundResource(R.drawable.list_select_center_style);
			}
			
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
	}
	
	private class ChildHolder{
		private TextView tvReceipt,tvLocation,tvError;
		private RelativeLayout main;
	}
	

}
