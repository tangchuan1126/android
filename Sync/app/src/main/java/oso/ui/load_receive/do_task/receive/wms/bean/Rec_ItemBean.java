package oso.ui.load_receive.do_task.receive.wms.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import utility.Utility;
import android.text.TextUtils;

public class Rec_ItemBean implements Serializable {

	public static final int LineState_Unprocess = 0; // 状态,0:WMS原始数据;1:已配置托盘并打印(没用);2:cc task;3:scan task;4:关闭
    public static final int LineState_ConfigAndPrint = 1;
    public static final int LineState_CCStart = 2;
    public static final int LineState_ScanStart = 3;
    public static final int LineState_Closed = 4;


//	/**
//	 * 可scan
//	 * @param bean
//	 * @return
//	 */
//	public boolean isCanScan() {
////		return line_status == LineState_CCCreated || line_status == LineState_ScanCreated;
//		//cnt完-即可扫
//		return line_status>=LineState_CCCreated;
//	}
	
//	/**
//	 * 不能扫描-的原因
//	 * @return
//	 */
//	public String getCannotScanReson(){
//		if(line_status<LineState_CCCreated)
//			return "This Item hasn't been counted!";
//		//关了也能扫
////		else if(line_status==LineState_Closed)
////			return "This Item has been closed!";
//		return "";
//	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 790281404279528074L;

	public int expected_qty; // 期望qty
	public int config_qty; // 配置-qty
	public int receive_qty;
	public int config_pallet; // 配置的-托盘数
	public int count_pallet; // cnt的托盘数
	
	public int sn_normal_qty;	//sn数,好的、坏的
	public int sn_damage_qty;

//	stock_out_qty
	public int damage_qty, normal_qty;	//cnt数

	public String counted_user_name, unloading_finish_time; // cnt的用户、时间

	public String item_id;
	public String lot_no;
	public String sn_size;		//sn-字符串长度            15,17,19  可能有多个的拼接字符串
	
	public String note;
	public String pallets_info;
	public String receipt_line_id;
	public Receive_ItemSetUp itemsetup;
	
	public String cc_user;
	public String cc_start_time;  //cc开始时间
	public String cc_start_user;  

	public String scan_end_user; // 扫sn的人、时间
	public String scan_end_time;
	
	public String scan_start_time,scan_start_user;	//开始人,时间
	
	public int line_status;		//状态,取LineState_x
	public int is_repeat;		//是否有扫重的
	
	public String location;
	
	//==================临时数据===============================

	public String receipt_id;
	public String detail_id;
	
	//---------------------------
	public String receipt_no;
	public int already_breark_qty;
	public int clp_expected_qty;
	public boolean go_to_scan;      //true(已break完)
	
	//---------------------
	public boolean is_supervisor;
	
	public int is_start_break;

	public List<Rec_PalletBean> pallets; //ViewPallets里面用到
	public boolean isAllCheck;
	
	public String customer;
	public String supplier;
	
	
	public int getTotalSN(){
		return sn_normal_qty+sn_damage_qty;
	}
	
	public String getQtySN(){
		if(sn_damage_qty == 0){
			return sn_normal_qty+"";
		}else{
			return getTotalSN()+"(G:"+sn_normal_qty+" D:"+sn_damage_qty+")";
		}
	}
	public String getQtyCout(){
		if(damage_qty == 0){
			return normal_qty+"";
		}else{
			return getTotalCout()+"(G:"+normal_qty+" D:"+damage_qty+")";
		}
	}
	
	public int getTotalCout(){
		return normal_qty+damage_qty;
	}
	
	public boolean is_need_sn(){
		if(itemsetup==null)
			return true;
		return itemsetup.is_need_sn>0;
	}
	
	public boolean is_repeat(){
		return is_repeat==1;
	}
	
	public int getShortage(){
		return expected_qty-normal_qty-damage_qty;
	}
	
	public int getCntQty(){
		return normal_qty+damage_qty;
	}
	
	/**
	 * 需验证
	 * @return true:满足要求
	 */
	public boolean checkSNSize(String sn){
		if(TextUtils.isEmpty(sn))
			return false;
		if(TextUtils.isEmpty(sn_size)){
			return true;
		}
		String[] strSizes = sn_size.split(",");
		List<String> strLists = Arrays.asList(strSizes);
		//无限制
		if(strLists.contains("0")){
			return true;
		}
		return strLists.contains(sn.length()+"");
	}
	
	public boolean hasCCScan(){
		return !TextUtils.isEmpty(cc_start_time);
	}
	
	/**
	 * 已开始扫描sn,有可能已经关了
	 * @return
	 */
	public boolean hasStartScan(){
		return !TextUtils.isEmpty(scan_start_time);
	}
	
	/**
	 * 全不都配置ok
	 * 
	 * @return
	 */
	public boolean isAllConfigured() {
		return expected_qty <= config_qty;
	}

	/**
	 * 已count,
	 * 
	 * @return
	 */
	public boolean isCnt_begin() {
		return receive_qty > 0;
	}

	public boolean isCnt_complete() {
		return line_status > LineState_ConfigAndPrint;
	}
	
	/**
	 * 实收同期望
	 * @return
	 */
	public boolean isAsExpected(){
		return normal_qty==expected_qty&&damage_qty==0;
	}
	
	// ==================nested===============================
	
	public static int getTotal_expQty(List<Rec_ItemBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).expected_qty;
		}
		return ret;
	}
	
	/**
	 * 注:直接基于line上的字段, 不适用于正在cnt
	 * @param list
	 * @return
	 */
	public static int getTotal_cntQty_good(List<Rec_ItemBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).normal_qty;
		}
		return ret;
	}
	
	/**
	 * 注:直接基于line上的字段, 不适用于正在cnt
	 * @param list
	 * @return
	 */
	public static int getTotal_cntQty_dmg(List<Rec_ItemBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).damage_qty;
		}
		return ret;
	}
	
	/**
	 * 注:直接基于line上的字段, 不适用于正在扫sn
	 * @param list
	 * @return
	 */
	public static int getTotal_snQty_good(List<Rec_ItemBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).sn_normal_qty;
		}
		return ret;
	}
	
	/**
	 * 注:直接基于line上的字段, 不适用于正在扫sn
	 * @param list
	 * @return
	 */
	public static int getTotal_snQty_dmg(List<Rec_ItemBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).sn_damage_qty;
		}
		return ret;
	}

	/**
	 * @param list
	 * @return
	 */
	public static boolean isAllLineClosed(List<Rec_ItemBean> list) {
		if (Utility.isEmpty(list))
			return true;

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).line_status != Rec_ItemBean.LineState_Closed)
				return false;
		}
		return true;
	}
	
	/**
	 * 所有line-都已cnt
	 * @param list
	 * @return
	 */
	public static boolean isAllLineCounted(List<Rec_ItemBean> list) {
		if (Utility.isEmpty(list))
			return true;

		for (int i = 0; i < list.size(); i++) {
			if (!list.get(i).isCnt_complete())
				return false;
		}
		return true;
	}

}
