package oso.ui.load_receive.do_task.receive.process;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_AssignTask_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class Rec_SearchRecCCNOAc extends BaseActivity {

	private SearchEditText searchEt;

	private ListView lvComps;
	private TextView tvEmpty;
	
	private LinearLayout llSearchScan,llAllScan;
	private ListView lvAll;
	
	private List<Rec_AssignTask_ItemBean> listLines;
	
	private final static int Rec_SearchRecCCNOAc = 4000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.rec_search_ccno,0);
		setTitleString("CC Config");
		
		initView();
		setData();
		getDefaultData();
	}
	
	private void initView() {
		searchEt = (SearchEditText) findViewById(R.id.searchEt);
		lvComps = (ListView) findViewById(R.id.lvComps);
		tvEmpty = (TextView) findViewById(R.id.tvEmpty);
		llSearchScan = (LinearLayout) findViewById(R.id.searchScan);
		llAllScan = (LinearLayout) findViewById(R.id.allScan);
		lvAll = (ListView) findViewById(R.id.lvAll);

		// 初始化
		searchEt.setScanMode();
	}
	
	private void getDefaultData(){
		RequestParams params = new RequestParams();
		params.add("adid", StoredData.getAdid());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				JSONArray jaLines=json.optJSONArray("data");
				if(jaLines==null)
					return;
				
				listLines = new Gson().fromJson(
						jaLines.toString(),
						new TypeToken<List<Rec_AssignTask_ItemBean>>(){}.getType());
				
				adp_all.notifyDataSetChanged();
				
				if(listLines.size()>0){
					llSearchScan.setVisibility(View.INVISIBLE);
					llAllScan.setVisibility(View.VISIBLE);
				}
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/findSupervisorTaskList", params,mActivity);
	}

	private void setData() {
		
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getDefaultData();
			}
		});
		
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getData(value, "");
				searchEt.setText("");
			}
		});

		// lv
		lvComps.setAdapter(adp);
		lvComps.setOnItemClickListener(new AbsListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Rec_ReceiveBean b = listRec.get(position);
				getData(b.receipt_no, b.company_id);
			}
		});
		lvComps.setEmptyView(tvEmpty);
		
		lvAll.setAdapter(adp_all);
		lvAll.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Rec_AssignTask_ItemBean bean = listLines.get(position);
				getData(bean.receipt_no,bean.company_id);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == Rec_SearchRecCCNOAc)
			getDefaultData();
	}
	
	private List<Rec_ReceiveBean> listRec = new ArrayList<Rec_ReceiveBean>();

	private void getData(final String value, String companyID) {

		if (TextUtils.isEmpty(value)) {
			UIHelper.showToast(getString(R.string.sync_scan_receipt_no));
			return;
		}

		final RequestParams params = new RequestParams();
		if (!TextUtils.isEmpty(companyID))
			params.add("company_id", companyID); // ABL
		params.add("receipt_no", value); // 4
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				JSONObject joDatas = json.optJSONObject("datas");
				int is_receipt_multiply = joDatas.optInt("is_receipt_multiply");

				// 有多个company
				if (is_receipt_multiply == 1) {
					JSONArray jaReceipts = joDatas.optJSONArray("receipt");
					if (Utility.isEmpty(jaReceipts)) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, getString(R.string.bcs_key_norecordsexception)+"!");
						return;
					}
					List<Rec_ReceiveBean> tmpList = new Gson().fromJson(jaReceipts.toString(), new TypeToken<List<Rec_ReceiveBean>>() {
					}.getType());
					listRec.clear();
					listRec.addAll(tmpList);
					adp.notifyDataSetChanged();
				}
				// 仅1个company
				else {
					Rec_ReceiveBean bean = Rec_ReceiveBean.handJsonForBean(json);
					// Intent in = new Intent(mActivity,
					// Receive_ItemListAc.class);
					// Receive_ItemListAc.initParams(in, bean);
					// startActivity(in);
					// debug
//					for(int i = bean.lines.size()-1; i>=0 ; i--){
//						Rec_ItemBean itemBean = bean.lines.get(i);
//						if(itemBean.line_status == 3)
//						{
//							bean.lines.remove(itemBean);
//						}
//					}
					if(bean.lines.size() == 0){
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Receipt number does not exist");
						return;
					}
					
					bean.receipt_no = value;
					Intent in = new Intent(mActivity, Rec_LineListAc.class);
					Rec_LineListAc.initParams(in, bean, 3);
					startActivityForResult(in, Rec_SearchRecCCNOAc);
				}

			}

		}.doGet(HttpUrlPath.acquireRNLineInfoByReceiptID, params, this);
	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_mulitcomp, null);
				h = new Holder();
				h.tvComp = (TextView) convertView.findViewById(R.id.tvComp);
				h.tvCustomer = (TextView) convertView.findViewById(R.id.tvCustomer);
				h.tvReceiptNO = (TextView) convertView.findViewById(R.id.tvReceiptNO);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷ui
			Rec_ReceiveBean b = listRec.get(position);
			h.tvComp.setText(b.company_id);
			h.tvCustomer.setText(b.customer_id);
			h.tvReceiptNO.setText(b.receipt_no);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listRec == null ? 0 : listRec.size();
		}
	};

	class Holder {
		TextView tvComp, tvCustomer, tvReceiptNO;
	}
	
	//-----------------------------------------------------
		private BaseAdapter adp_all=new BaseAdapter() {

			@Override
			public int getCount() {
				return listLines == null ? 0 : listLines.size();
			}

			@Override
			public Object getItem(int position) {
				return null;
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ViewHolder h;
				if (convertView == null) {
					convertView=getLayoutInflater().inflate(R.layout.item_rec_searchrecnoac, null);
					h = new ViewHolder();
					h.tvReceiptID=(TextView)convertView.findViewById(R.id.tvReceiptID);
					h.tvItemID=(TextView)convertView.findViewById(R.id.tvItemID);
					h.tvLotNO=(TextView)convertView.findViewById(R.id.tvLot);
					h.rlGoodQty = (RelativeLayout) convertView.findViewById(R.id.rlGoodQty);
					convertView.setTag(h);
				} else
					h = (ViewHolder) convertView.getTag();
				
				//刷ui
				Rec_AssignTask_ItemBean b=listLines.get(position);
//				h.tvReceiptID.setText("Receipt"+b.receipt_id);
	            h.tvReceiptID.setText("Receipt: "+b.receipt_no);
				h.tvItemID.setText(b.item_id);
				h.tvLotNO.setText(b.lot_no);
				
				h.rlGoodQty.setVisibility(View.GONE);
				
				return convertView;
			}
			
		};
		
		private class ViewHolder{
			TextView tvReceiptID,tvItemID,tvLotNO;
			RelativeLayout rlGoodQty;
		}
}
