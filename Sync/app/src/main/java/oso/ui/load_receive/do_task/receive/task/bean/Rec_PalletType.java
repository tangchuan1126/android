package oso.ui.load_receive.do_task.receive.task.bean;


import java.io.Serializable;
import java.util.List;

public class Rec_PalletType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1674322890366899002L;
	
	public int container_type;
	public double height;
	public double length;
	public double length_uom;
	public double max_height;
	public double max_load;
	public int type_id;
	public String type_name;
	public double weight;
	public double weight_uom;
	public double width;
	
	public boolean isselect = false;
	
	
	
	
	////
	public static String getTypeName(Rec_PalletType type){
		if(type == null)
			return "";
		
		return type.type_name;
	}
	
	public static Rec_PalletType getContainerId(List<Rec_PalletType> types){
		if(types == null)
			return null;
		for(Rec_PalletType type : types){
			if(type.type_name.equalsIgnoreCase("PALLETFLOORLOAD")){
				return type;
			}
		}
		return null;
	}
	
}

