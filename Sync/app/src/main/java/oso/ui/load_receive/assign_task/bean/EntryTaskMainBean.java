package oso.ui.load_receive.assign_task.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.common.bean.CheckInTaskMainItem;
import utility.StringUtil;
import android.text.TextUtils;


public class EntryTaskMainBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -325868124802234794L;

	public String entry_id;
	public int task_assigned;
	public int task_closed;
	public int task_total;
	public List<CheckInTaskMainItem> complexDoorList;

	public static List<EntryTaskMainBean> helpJson(JSONObject json) {
		List<EntryTaskMainBean> list = null;
		JSONArray jsonArray = json.optJSONArray("data");
		if (!StringUtil.isNullForJSONArray(jsonArray)) {
			list = new ArrayList<EntryTaskMainBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				EntryTaskMainBean bean = new EntryTaskMainBean();
				bean.entry_id = item.optString("entry_id");
				bean.task_closed = item.optInt("task_closed");
				bean.task_assigned = item.optInt("task_assigned");
				bean.task_total = item.optInt("task_total");
				JSONArray jsonArrays = StringUtil.getJsonArrayFromJson(item, "equipments");
				if(jsonArray != null && jsonArrays.length() > 0){
				List<CheckInTaskMainItem>	lists = new ArrayList<CheckInTaskMainItem>();
					for (int j = 0; j < jsonArrays.length(); j++) {
						JSONObject items = jsonArrays.optJSONObject(j);
						if(item!=null){
							CheckInTaskMainItem b = new CheckInTaskMainItem();
							b.equipment_status = item.optInt("equipment_status");
							b.equipment_purpose = item.optInt("equipment_purpose");
							b.equipment_type = items.optInt("equipment_type");
							b.equipment_number = items.optString("equipment_number");
							
							//debug,若设备无效 则不加入
							if(TextUtils.isEmpty(b.equipment_number))
								continue;
							
							lists.add(b);
						}
					}
					bean.complexDoorList = lists;
				}
				list.add(bean);
			}
		}
		return list;
	}
}
