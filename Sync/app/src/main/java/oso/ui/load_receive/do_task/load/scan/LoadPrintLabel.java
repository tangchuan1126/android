package oso.ui.load_receive.do_task.load.scan;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.widget.AcWebpage;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.CheckInLoadOrderBean;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.print.OnInnerClickListener;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 打印标签,从LoadConfirm过来
 * 
 * @author 朱成
 * @date 2014-9-13
 */
public class LoadPrintLabel extends BaseActivity implements OnClickListener {

	private CheckBox cbSelAll;

	private ListView lvOrders;
	private List<CheckInLoadOrderBean> listOrders;

	private CheckInLoad_ComplexSubLoads complexSubLoads;
	private List<PrintServer> listPrinters1=new ArrayList<PrintServer>();
	private long curPrinterId;		//当前printer

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_load_printlabel, 0);
		setTitleString("Print Packing List");
		// btnRight.setVisibility(View.VISIBLE);
		// btnRight.setBackgroundResource(R.drawable.print_all_style);
		showRightButton(R.drawable.print_all_style, "", this);
		if (!applyParams()) {
			finish();
			return;
		}

		// 取view
		lvOrders = (ListView) findViewById(R.id.lvOrders);
		cbSelAll = (CheckBox) findViewById(R.id.cbSelAll);

		// 监听事件
		// btnRight.setOnClickListener(this);
		cbSelAll.setOnClickListener(this);
//		findViewById(R.id.btnNext).setOnClickListener(this);

		// 初始化lvOrders
		CheckInLoadOrderBean.resetPrintState(listOrders);
		lvOrders.setAdapter(adpOrders);
	}
	
	//请求printers
	private void reqPrinters(){
		//若已加载,则直接显示
		if(listPrinters1.size()>0)
		{
			showDlg_printers();
			return;
		}
		
		RequestParams params=new RequestParams();
		params.add("Method", "GetLetterPrintServer");
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listPrinters1.clear();
				
				//解析
				JSONObject joData=json.optJSONObject("printserver");
				curPrinterId=joData.optLong("default");
				PrintServer.parseBeans(joData.optJSONArray("allprintserver"), listPrinters1);
				if(listPrinters1.size()==0){
					UIHelper.showToast(mActivity, getString(R.string.sync_no_printserver));
					return;
				}
				
				//显示-列表
				showDlg_printers();
			}
		}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRight:
			if (hasOrderToPrint())
				reqPrinters();
			else
				UIHelper.showToast(mActivity, "Please Select Orders To Print!");
			break;
//		case R.id.btnNext:
//			Intent in = new Intent(mActivity, LoadToPrintActivity.class);
//			LoadToPrintActivity.initParams(in, complexSubLoads);
//			startActivity(in);
//			finish();
//			break;
		case R.id.cbSelAll:
			boolean toSelAll = is_btnSelAll_toSelectAll();
			selectAll_print(toSelAll);
			adpOrders.notifyDataSetChanged();
			set_btnSelAll_toSelectAll(!toSelAll);
			break;

		default:
			break;
		}
	}

	// 将选择所有(选中时)
	private boolean is_btnSelAll_toSelectAll() {
		return cbSelAll.isChecked();
	}

	// 打印-全选/全不选
	private void selectAll_print(boolean toSelect) {
		for (int i = 0; i < listOrders.size(); i++)
			listOrders.get(i).isPrintSelected = toSelect;
	}

	// ------------打印-------------------------------------

	// 勾选了order
	private boolean hasOrderToPrint() {
		for (int i = 0; i < listOrders.size(); i++) {
			if (listOrders.get(i).isPrintSelected)
				return true;
		}
		return false;
	}

	private void showDlg_printers() {
		List<PrintServer> printServerList = listPrinters1;
		if (printServerList == null || printServerList.size() == 0) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) mActivity
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,
				null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(mActivity,
				printServerList, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServerList.size() < 3) ? dm.heightPixels / 7
				* printServerList.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServerList.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getCurPrinter_index());
		// 初始-滑至可见
		listView.setSelection(getCurPrinter_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (getCurPrinter_index() != -1) {
							doPrint();
							dialog.dismiss();
						} else
							UIHelper.showToast(getString(R.string.sync_select_printserver));
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getCurPrinter_index() {

		List<PrintServer> listPrinters = listPrinters1;
		if (listPrinters == null)
			return -1;

		for (int i = 0; i < listPrinters.size(); i++) {
			String tmpId = listPrinters.get(i).getPrinter_server_id();
			if ((curPrinterId+"").equals(tmpId))
				return i;
		}
		return -1;
	}

	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;

	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			String id = listPrinters1.get(position)
					.getPrinter_server_id();
			curPrinterId =Long.parseLong(id);
			adpPrinters.setCurItem(position);
		};
	};

	// 打印,同scanLoadActivity中打印不同
	private void doPrint() {
		RequestParams params = new RequestParams();
		params.add("Method", "PackingListTask");
		params.put("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		params.put("print_server_id", curPrinterId + "");
		params.put("order_nos", getOrdersToPrint());
		
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(mActivity, getString(R.string.sync_success));
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	// 是否-打印所有order
	private boolean isToPrintAll() {
		for (int i = 0; i < listOrders.size(); i++) {
			if (!listOrders.get(i).isPrintSelected)
				return false;
		}
		return true;
	}

	// 待打印的orders,","分割
	private String getOrdersToPrint() {

		StringBuilder ret = new StringBuilder();
		int len = listOrders.size();
		for (int i = 0; i < len; i++) {
			if (listOrders.get(i).isPrintSelected)
				ret.append(listOrders.get(i).orderno + ",");
		}
		String retStr = ret.toString();
		// 移除-最后的","
		if (!TextUtils.isEmpty(retStr))
			retStr = retStr.substring(0, retStr.length() - 1);
		return retStr;
	}

	// 根据-选中状态,调整"全选/全不选"
	private void adjustPrintAll() {

		int len = listOrders.size();
		if (len == 0)
			return;
		int cntSelected = 0;
		for (int i = 0; i < len; i++) {
			if (listOrders.get(i).isPrintSelected)
				cntSelected++;
		}

		// 若已全选 则显示"全不选"
		set_btnSelAll_toSelectAll(cntSelected != len);
	}

	private void set_btnSelAll_toSelectAll(boolean toSelectAll) {
		cbSelAll.setChecked(!toSelectAll);
		// cbSelAll.setText(!toSelectAll ? "Deselect All" : "Select All");
	}

	// ----------------------

	private static CheckInLoad_ComplexSubLoads complexSubLoads_bridge;

	public static void initParams(Intent in,
			CheckInLoad_ComplexSubLoads complexSubLoads) {
		complexSubLoads_bridge = complexSubLoads;
	}

	// 返回:数据成功传过来了
	private boolean applyParams() {
		if (complexSubLoads_bridge == null)
			return false;

		listOrders = complexSubLoads_bridge.getCurSubLoad().listOrders;

		complexSubLoads = complexSubLoads_bridge;
		complexSubLoads_bridge = null;
		return true;
	}

	private void toPreviewAc(int position) {

		CheckInLoadOrderBean b = listOrders.get(position);

		StringBuilder url = new StringBuilder(HttpUrlPath.Load_PrintLabel);
		// order串行化
		JSONArray jaOrders = null;
		try {
			jaOrders = new JSONArray();
			JSONObject joOrder = new JSONObject();
			joOrder.put("order_no", b.orderno);
			joOrder.put("companyId", complexSubLoads.company_id);
			joOrder.put("customerId", complexSubLoads.customer_id);
			jaOrders.put(joOrder);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		RequestParams params = new RequestParams();
		params.put("jsonString", jaOrders.toString());
		params.put("adid",StoredData.getAdid());
		params.put("isprint","1");
		url.append("?").append(params.toString());

		Intent in = new Intent(LoadPrintLabel.this, AcWebpage.class);
		AcWebpage.initParams(in, url.toString());
		startActivity(in);
	}

	// ==============nested=================================

	private BaseAdapter adpOrders = new BaseAdapter() {

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_load_printlabel, null);
				holder = new Holder();
				holder.lo = convertView;
				holder.cbOrder = (CheckBox) convertView
						.findViewById(R.id.cbOrder);
				holder.tvOrder = (TextView) convertView
						.findViewById(R.id.tvOrder);
				holder.btnPreview = (Button) convertView
						.findViewById(R.id.btnPreview);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 更新数据
			final CheckInLoadOrderBean b = listOrders.get(position);
			holder.tvOrder.setText(b.orderno);
			holder.cbOrder.setChecked(b.isPrintSelected);

			// 监听事件
			holder.lo.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					b.isPrintSelected = !b.isPrintSelected;
					adjustPrintAll();
					adpOrders.notifyDataSetChanged();
				}
			});
			holder.btnPreview.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					toPreviewAc(position);
				}
			});

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listOrders == null ? 0 : listOrders.size();
		}
	};

	class Holder {
		View lo;
		CheckBox cbOrder;
		TextView tvOrder;
		Button btnPreview;
	}

}
