package oso.ui.load_receive.window.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class WindowBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 313052287207382913L;
	public String gate_driver_liscense;
	public String entry_id;
	public String mc_dot;
	public String company_name;
	public String gate_driver_name;
	public int priority;
	
	public static WindowBean getJsonWindowCheckIn(JSONObject json,WindowBean bean){
		
			try {
				JSONObject datasArray = json.getJSONObject("data");
				
				if(datasArray != null && datasArray.length() > 0){
					bean = new WindowBean();
					bean.gate_driver_liscense = datasArray.optString("gate_driver_liscense");
					bean.entry_id = datasArray.optString("entry_id");
					bean.mc_dot = datasArray.optString("mc_dot");
					bean.company_name = datasArray.optString("company_name");
					bean.gate_driver_name = datasArray.optString("gate_driver_name");
					bean.priority = datasArray.optInt("priority");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 
		return bean;
	}
}
