package oso.ui.load_receive.do_task.receive.samsung;

import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.samsung.bean.Ctnr;
import oso.ui.load_receive.do_task.receive.samsung.bean.ReceiveBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.key.EntryDetailNumberStateKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SearchReceiveActivity extends BaseActivity {

	private LinearLayout entryLayout, doorLayout;
	private SearchEditText searchEt, doorEt;
	private ListView entryLv;
	private ReceiveAdapter adapter;

	private List<ReceiveBean> mData, receiveList = null;
	private String entryId;
	private Context context;
	public static Resources resources;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_receive_search_entry, 0);
		context = SearchReceiveActivity.this;
		resources = getResources();
		setTitleString("Search Receive");
		initView();
		setdata();
	}

	private void initView() {
		entryLv = (ListView) findViewById(R.id.entryLv);
		searchEt = (SearchEditText) findViewById(R.id.searchEt);
		doorEt = (SearchEditText) findViewById(R.id.doorEt);
		entryLayout = (LinearLayout) findViewById(R.id.entryLayout);
		doorLayout = (LinearLayout) findViewById(R.id.doorLayout);

		entryLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 若为"door列表",则进入load列表
				if (!isDoor)
					isDoor(receiveList.get(position).getDoor());
			}
		});
		// ReceiveDetailActivity.setOnStateListener(this);
	}

	private void setdata() {
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				doRequest(value);
				Utility.colseInputMethod(mActivity, searchEt);
			}
		});
		searchEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
					doRequest(searchEt.getEditableText().toString());
					Utility.colseInputMethod(mActivity, searchEt);

					return true;
				}
				return false;
			}
		});
		doorEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				isDoor(value);
				Utility.colseInputMethod(mActivity, doorEt);
			}
		});
		doorEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
					String value = doorEt.getText().toString().trim();
					isDoor(value);
					Utility.colseInputMethod(mActivity, doorEt);
					return true;
				}
				return false;
			}
		});
	}

	// 切换至-单个door(即load列表)
	private void isDoor(String value) {
		ReceiveBean door = valiDoor(value);
		// 若找到door
		if (door != null) {
			receiveList.clear();
			receiveList.add(door);
			adapter.notifyDataSetChanged();

			// 调整-ui,title、隐藏et
			setTitleString("Door:DK" + door.getDoor());
			// lineDoor doorEt searchEt lineEntry
			findViewById(R.id.lineDoor).setVisibility(View.GONE);
			findViewById(R.id.doorEt).setVisibility(View.GONE);
			findViewById(R.id.searchEt).setVisibility(View.GONE);
			findViewById(R.id.lineEntry).setVisibility(View.GONE);

		} else {
			UIHelper.showToast(mActivity, getString(R.string.sync_input_error), Toast.LENGTH_SHORT).show();
			doorEt.setText("");
		}
		doorEt.clearFocus();
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				SearchReceiveActivity.super.onBackBtnOrKey();
			}
		});
	}

	private ReceiveBean valiDoor(String value) {
		for (int i = 0; i < mData.size(); i++) {
			ReceiveBean door = mData.get(i);
			if (value.equals(door.getDoor()) || value.equals("dk" + door.getDoor()) || value.equals("DK" + door.getDoor())) {
				isDoor = true;
				return door;
			}
		}
		return null;
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("ready");
	}

	public static JSONObject jsonObject = null;

	// 可搜-entryId
	private void doRequest(final String entry_id) {
		RequestParams params = new RequestParams();
		params.add("Method", "EntryReceive");
		params.add("entry_id", entry_id);

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				jsonObject = json;
				System.out.println("json= " + json.toString());
				Log.e("getjson date", json.toString());
				entryId = StringUtil.getJsonString(json, "entry_id");
				receiveList = ReceiveBean.parseReceiveBean(json);
				mData = receiveList;
				adapter = new ReceiveAdapter(mData);
				entryLv.setAdapter(adapter);
				entryLayout.setVisibility(View.GONE);
				doorLayout.setVisibility(View.VISIBLE);
				searchEt.setText("");
				setTitleString("Entry:" + entry_id);
				doorEt.requestFocus();
			}

			@Override
			public void handFail() {
				searchEt.setText("");
				searchEt.requestFocus();
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	boolean isDoor = false; // 当前为"load列表"
	int mPosition = -1;

	// --------nested-------------------------------

	class ReceiveAdapter extends BaseAdapter {

		private List<ReceiveBean> receives;

		public ReceiveAdapter(List<ReceiveBean> list) {
			receives = list;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {

			mPosition = position;
			// TODO Auto-generated method stub
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_searchload_entry, null);
				holder = new Holder();
				holder.tvEntry = (TextView) convertView.findViewById(R.id.tvEntry);
				holder.tvDoor = (TextView) convertView.findViewById(R.id.tvDoor);
				holder.loLoads = (LinearLayout) convertView.findViewById(R.id.loLoads);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新-数据
			ReceiveBean r = receives.get(position);
			holder.tvEntry.setText(r.getEntry_id());
			holder.tvDoor.setText("DK" + r.getDoor());
			generateDoorLoads(holder.loLoads, r);

			return convertView;
		}

		private void generateDoorLoads(LinearLayout ctnrLayout, final ReceiveBean doorBean) {
			List<Ctnr> listCtnrs = doorBean.getCtnrList();

			ctnrLayout.removeAllViews();
			if (listCtnrs == null)
				return;

			// 添加-ctnrs
			for (int i = 0; i < listCtnrs.size(); i++) {
				LinearLayout loLoad = (LinearLayout) getLayoutInflater().inflate(R.layout.item_search_receive, null);
				TextView ctnrTv = (TextView) loLoad.findViewById(R.id.ctnrTv);
				TextView statusTv = (TextView) loLoad.findViewById(R.id.statusTv);
				ImageView arrowIv = (ImageView) loLoad.findViewById(R.id.arrowIv);
				LinearLayout itemBtn = (LinearLayout) loLoad.findViewById(R.id.itemBtn);
				View vDash_load = loLoad.findViewById(R.id.vDash_load);
				vDash_load.setVisibility(isDoor ? View.GONE : View.VISIBLE);

				LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				if (i > 0)
					lp.topMargin = 3;
				loLoad.setLayoutParams(lp);

				// 初始化-数据F
				final Ctnr tmpLoad = listCtnrs.get(i);

				ctnrTv.setText(tmpLoad.getNumber());
				statusTv.setText(tmpLoad.getStatus());

				if (isDoor) {
					if (tmpLoad.getStatus().equals(EntryDetailNumberStateKey.getType(mActivity,EntryDetailNumberStateKey.Close))
							|| tmpLoad.getStatus().equals(EntryDetailNumberStateKey.getType(mActivity,EntryDetailNumberStateKey.Exception))) {
						itemBtn.setBackgroundResource(R.drawable.pick_btn_text_style);
						ctnrLayout.setBackgroundColor(Color.TRANSPARENT);
						arrowIv.setVisibility(View.GONE);
					} else {
						itemBtn.setBackgroundResource(R.drawable.pick_btn_text_style);
						ctnrLayout.setBackgroundColor(Color.TRANSPARENT);
						arrowIv.setVisibility(View.VISIBLE);
						// 监听-事件
						loLoad.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(mActivity, ReceiveDetailActivity.class);
								intent.putExtra("entry_id", doorBean.getEntry_id());
								intent.putExtra("ctnrs", tmpLoad);
								startActivity(intent);
							}
						});
					}

				} else {
					ctnrLayout.setBackgroundResource(R.drawable.kuang);
					itemBtn.setBackgroundColor(Color.TRANSPARENT);
					arrowIv.setVisibility(View.GONE);
				}

				ctnrLayout.addView(loLoad);
			}
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return receives.get(arg0);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return receives.size();
		}
	};

	private class Holder {
		TextView tvEntry, tvLoad, tvStatus;

		// --entry---------
		TextView tvDoor;
		LinearLayout loLoads;
	}

}
