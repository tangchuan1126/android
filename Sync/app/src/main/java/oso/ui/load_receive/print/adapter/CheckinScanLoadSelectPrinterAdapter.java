package oso.ui.load_receive.print.adapter;

import java.util.List;

import oso.ui.load_receive.print.bean.PrintServer;
import support.common.print.OnInnerClickListener;
import utility.StringUtil;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckinScanLoadSelectPrinterAdapter extends BaseAdapter {

	private List<PrintServer> arrayList;

	private LayoutInflater inflater;

	private Context context;
	private CheckinScanLoadSelectPrinterAdapter oThis = CheckinScanLoadSelectPrinterAdapter.this;
	
	private OnInnerClickListener onInnerClick;
	private int curItem=0;		//当前-索引
	
	public void setCurItem(int curItem){
		this.curItem=curItem;
		notifyDataSetChanged();
	}
	
	//-------------------
	public CheckinScanLoadSelectPrinterAdapter(Context context,List<PrintServer> arrayList,
			OnInnerClickListener onInnerClick) {
		super();
		this.context = context;
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
		this.onInnerClick=onInnerClick;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public PrintServer getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		CheckinScanLoadSelectPrinterAdapterHoder holder = null;
		if (convertView == null) {
			holder = new CheckinScanLoadSelectPrinterAdapterHoder();
			convertView = inflater.inflate(R.layout.checkin_scan_load_selectprinter_item, null);
			holder.item = (View) convertView.findViewById(R.id.item);
			holder.printer_name = (TextView) convertView.findViewById(R.id.printer_name);
			holder.zone_layout = (View) convertView.findViewById(R.id.zone_layout);
			holder.printer_zone = (TextView) convertView.findViewById(R.id.printer_zone);
			holder.cbPrint = (RadioButton) convertView.findViewById(R.id.cbPrint);
			convertView.setTag(holder);
		} else {
			holder = (CheckinScanLoadSelectPrinterAdapterHoder) convertView.getTag();
		}
		final PrintServer temp = arrayList.get(position);
		holder.printer_name.setText(temp.getPrinter_server_name() + "");
		if(StringUtil.isNullOfStr(temp.getZone())){
			holder.zone_layout.setVisibility(View.GONE);
		}else{
			holder.zone_layout.setVisibility(View.VISIBLE);
			holder.printer_zone.setText(temp.getZone()+"");
		}
		holder.cbPrint.setChecked(curItem==position);
		holder.item.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(onInnerClick!=null)
					onInnerClick.onInnerClick(v, position);
			}
		});
		return convertView;
	}
	
	public void cancelChecked(PrintServer temp){
		for(int i=0;i<arrayList.size();i++){
			if(!(arrayList.get(i).getPrinter_server_id()).equals(temp.getPrinter_server_id())){
				arrayList.get(i).setCheckedFlag(false);
			}
		}
	}
	public PrintServer getCheckedPrintServer(){
		for(int i=0;i<arrayList.size();i++){
			if(arrayList.get(i).isCheckedFlag()){
				return arrayList.get(i);
			}
		}
		return null;
	}
}

class CheckinScanLoadSelectPrinterAdapterHoder {
	public View item;
	public TextView printer_name;
	public View zone_layout;
	public TextView printer_zone;
	public RadioButton cbPrint;
	

}
