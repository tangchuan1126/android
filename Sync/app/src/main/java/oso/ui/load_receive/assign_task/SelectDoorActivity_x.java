package oso.ui.load_receive.assign_task;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import support.common.UIHelper;
import support.common.bean.DockCheckInBase;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: SelectDoorActivity
 * @Description:
 * @author gcy
 * @date 2014-12-3 下午2:21:00
 */
public class SelectDoorActivity_x extends BaseActivity {
	private ListView listview;
	private IntentSelectDoorBean intentSelectDoorBean;
	private List<ResourceInfo> resourceList;
	private DoorAdapter adapter;
	private Button submit_id;
	private DockHelpUtil.ResourceInfo  selectDoorInfo;
	public static final int DockCheckInAllocationFlag = 1;
	public static final int AssignActivityFlag = 2;
	public static final int CheckInTaskDoorItemActivityFlag = 3;
	public static final int SELECTDOOR = 102;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.dci_s_door_layout, 0);
		getFromActivityData(getIntent());
		initView();

	}

	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	@SuppressWarnings("unchecked")
	protected void getFromActivityData(Intent intent) {
		intentSelectDoorBean = (IntentSelectDoorBean) intent.getSerializableExtra("intentSelectDoorBean");
		resourceList = (List<ResourceInfo>) intent.getSerializableExtra("resourceList");
	}
	
	/**
 	 * @Description:初始化主Ui的控件
 	 */
	private void initView() {
		setTitleString(getString(R.string.shuttle_select_door));
		submit_id = (Button)findViewById(R.id.submit_id);
		((TextView)findViewById(R.id.entry_id_tx)).setText(intentSelectDoorBean.check_in_entry_id);
		((TextView)findViewById(R.id.equipment_type)).setText(intentSelectDoorBean.equipment_type_value);
		((TextView)findViewById(R.id.equipment_tx)).setText(intentSelectDoorBean.equipment_number);
		((TextView)findViewById(R.id.resource)).setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,intentSelectDoorBean.resources_type)+" ["+intentSelectDoorBean.resources_type_value+"]");
		
		listview  = (ListView)findViewById(R.id.listview);
		listview.setEmptyView((TextView) findViewById(R.id.any_data));
		
		adapter = new DoorAdapter(mActivity, resourceList);
		listview.setAdapter(adapter);
		
		//-----------------判断选中状态
		if(adapter!=null&&!Utility.isNullForList(resourceList)){
			for(int i=0;i<resourceList.size();i++){
				if(intentSelectDoorBean.resources_id==resourceList.get(i).resource_id){
					adapter.setCurrentID(i);
					break;
				}
			}
		}
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectDoorInfo = (DockHelpUtil.ResourceInfo) listview.getItemAtPosition(position);
//				sd_id = String.valueOf(bean.resource_id);
				if(adapter!=null){
					adapter.setCurrentID(position);
				}
			}
		});
		
		
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				closeThisActivity();
			}
		});
		
		submit_id.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(adapter==null){
					UIHelper.showToast(mActivity, getString(R.string.sync_data_error), Toast.LENGTH_SHORT).show();
					return;
				}
				if(adapter.getCurrentId()<0){
					UIHelper.showToast(mActivity, getString(R.string.sync_select_door), Toast.LENGTH_SHORT).show();
					return;
				}
				
				RequestParams params = new RequestParams();
				String method = "";
				String url = "";
				String from = "";
				switch (intentSelectDoorBean.intentType) {
				case DockCheckInAllocationFlag:;				
				case AssignActivityFlag:
					from = "supervisor";
					method = "AssignTaskMoveToDoor";
					url = HttpUrlPath.GCMAction;
					break;
				case CheckInTaskDoorItemActivityFlag:
					from = "";
					method = "TaskProcessingMoveToDoor";
					url = HttpUrlPath.AndroidTaskProcessingAction;
					break;
				default:
					finish();
					break;
				}
				params.add("Method", method);
				params.add("entry_id", intentSelectDoorBean.check_in_entry_id);
				params.add("from", from);
				params.add("equipment_id", intentSelectDoorBean.equipment_id+"");
				params.add("moved_resource_type", intentSelectDoorBean.resources_type+"");
				params.add("moved_resource_id", intentSelectDoorBean.resources_id+"");
				params.add("sd_id", String.valueOf(selectDoorInfo.resource_id));
				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						jumpToAnotherActivity(json);
					}
				}.doGet(url, params, mActivity);
			}
		});
	}
	
	/**
	 * @Description:根据条件跳转到entry所对应的类型
	 * @param flag
	 */
	protected void jumpToAnotherActivity(JSONObject json){	
		Intent intent = new Intent();
		switch (intentSelectDoorBean.intentType) {
		case DockCheckInAllocationFlag:		
		case AssignActivityFlag:
			DockCheckInBase dockCheckInBase = DockCheckInBase.parsingJSON(json, intentSelectDoorBean.check_in_entry_id);
			if(dockCheckInBase!=null){
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				intent.setClass(mActivity,AssignTaskActivity.class);
				intent.putExtra("dockCheckInBase", dockCheckInBase);
				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
			}else{
				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		case CheckInTaskDoorItemActivityFlag:
			if(json.optInt("ret")==1){
				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				intent.setClass(mActivity,CheckInTaskDoorItemActivity.class);
				
				intentSelectDoorBean.resources_id = selectDoorInfo.resource_id;
				intentSelectDoorBean.resources_type_value = selectDoorInfo.resource_name+"";
				intentSelectDoorBean.resources_type = selectDoorInfo.resource_type;
				intent.putExtra("intentSelectDoorBean", (Serializable) intentSelectDoorBean);//给 李俊昊
				
				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
			}else{

				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		default:
			finish();
			break;
		}
		
		
	}
	
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}
	
	
	public class DoorAdapter extends BaseAdapter  {
		
		private List<DockHelpUtil.ResourceInfo> arrayList;
		
		private LayoutInflater inflater;
		
		private Context context;
		
		int currentID = -1;
		
		public DoorAdapter(Context context,List<DockHelpUtil.ResourceInfo> arrayList ) {
			super();
	 		this.context = context;
			this.arrayList = arrayList ;
			this.inflater  = LayoutInflater.from(this.context);
		}
		
		@Override
		public int getCount() {
	 		return arrayList != null ? arrayList.size() : 0;
		}

		@Override
		public DockHelpUtil.ResourceInfo getItem(int position) {
	 		return arrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
	 		return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			DoorHoder holder = null;
			 if(convertView==null){
				holder = new DoorHoder();
				convertView = inflater.inflate(R.layout.dci_s_door_item,null);
				holder.select_img = (ImageView) convertView.findViewById(R.id.select_img);
				holder.door_name = (TextView) convertView.findViewById(R.id.door_name);
	 			convertView.setTag(holder);
			}else{
				holder = (DoorHoder) convertView.getTag();
			}
			 final DockHelpUtil.ResourceInfo temp  =  arrayList.get(position);
			if (position == this.currentID){
				holder.select_img.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.radio_clk));
			}else{
				holder.select_img.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.radio));
			}
			 holder.door_name.setText("Door : [ "+temp.resource_name + " ]");	
			 
			
			 return convertView;
		}
		
		public void setCurrentID(int currentID) {
			this.currentID = currentID;
			notifyDataSetChanged();
		}
		public int getCurrentId(){
			return this.currentID;
		}
	}
	class DoorHoder {
		public TextView door_name;		
		public ImageView select_img;
	}
	
}
