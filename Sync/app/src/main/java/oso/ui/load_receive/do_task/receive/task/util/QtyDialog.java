package oso.ui.load_receive.do_task.receive.task.util;

import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.do_task.receive.task.Rec_ScanTask_SelectPalletType;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.SimpleTextWatcher;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.location.ChooseLocationActivity;
import oso.widget.location.ChooseStagingActivity;
import oso.widget.location.bean.LocationBean;
import oso.widget.selectoccupybutton.CardButton;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.dbhelper.StoredData;
import support.dbhelper.TmpDataUtil;
import support.network.NetInterface;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class QtyDialog implements OnClickListener {

	private Activity mContext;
	private BottomDialog dialog;

	public SingleSelectBar ssb;

	public int remain; // 剩余qty
	public boolean def_addGoodPlts = true;
	private String rnNO;

	// public QtyDialog(Context c, int remain){
	// this(c,remain,true,"");
	// }

	private boolean isFromCnt = true; // true:来自cnt,支持auto
										// false:来自scanSN,无auto,提交前弹框确认

	private LocBean defLoc; // 默认location

	private List<Rec_PalletType> palletTypes; // 可选类型（48*48）
	private Rec_PalletType palletType;
	
	private LocationBean sel_locationBean;
	
	public final static int Location_Back = 0x1200;
	public final static int Staging_Back = 0x1201;

	public QtyDialog(Activity c, int remain, boolean def_addGoodPlts, String rnNO, LocBean defLoc, List<Rec_PalletType> palletTypes, boolean isFromCnt) {
		mContext = c;
		this.remain = remain;
		this.def_addGoodPlts = def_addGoodPlts;
		this.rnNO = rnNO;
		this.isFromCnt = isFromCnt;
		this.defLoc = defLoc;
		this.palletTypes = palletTypes;
		initDialog();
	}

	public static View view;
	private static EditText wedit, ledit, hedit, qtyedit;

	public int getw() {
		return Utility.parseInt(wedit.getText().toString());
	}

	public int getl() {
		return Utility.parseInt(ledit.getText().toString());
	}

	public int geth() {
		return Utility.parseInt(hedit.getText().toString());
	}
	
	public String getLoc(){
		String loc = tvLoc.getText().toString();
		String hint = tvLoc.getHint().toString();
		
		return TextUtils.isEmpty(loc) ? hint : loc;
	}

	/**
	 * plt数
	 * 
	 * @return
	 */
	public int getQty() {
		return Utility.parseInt(qtyedit.getText().toString());
	}

	public boolean isTypeEmpty() {
		return TextUtils.isEmpty(getType());
	}

	public String getType() {
		return tvPalletType.getText().toString();
	}

	public String getTypeId() {
		if (palletType == null)
			return "";
		return palletType.type_id + "";
	}
	
	public int getCardConver(){
		return btnLocType.isCover() ? 1 : 2;
	}

	/**
	 * 填充网络参数
	 * 
	 * @param p
	 */
	// public void getAutoRemain(RequestParams p) {
	// if (!hasAutoRemainQty())
	// return;
	// p.add("remainder_length", auto_remainQty + "");
	// p.add("remainder_width", "1");
	// p.add("remainder_height", "1");
	// }

	/**
	 * 获取-网络参数
	 * 
	 * @param is_return_plts
	 *            好托盘才有效
	 * @param addToReceive_whenAdd
	 *            true:添加时即算收 false:cnt后才算收
	 * @return
	 */
	public RequestParams getNetParam(String line_id, String item_id, String lot_no, String detail_id, boolean is_return_plts, boolean isTask,
			boolean addToReceive_whenAdd, int check_location) {

		RequestParams p = new RequestParams();
		// 坏托盘
		if (getAddMode() == Mode_DmgPlts) {
			if(sel_locationBean !=null){
				p = NetInterface.recAddDmgPlts_p(getQty(), line_id, item_id, lot_no, detail_id, sel_locationBean, btnLocType.isCover(), addToReceive_whenAdd,
						getTypeId(), check_location);
			}else{
				p = NetInterface.recAddDmgPlts_p(getQty(), line_id, item_id, lot_no, detail_id, defLoc, btnLocType.isCover(), addToReceive_whenAdd,
						getTypeId(), check_location);
			}
			return p;
		}

		p.add("isTask", isTask ? "1" : "0");
		p.add("is_return_plts", is_return_plts ? "1" : "0");
		p.add("ps_id", StoredData.getPs_id());
		if(sel_locationBean !=null){
			p.add("location", sel_locationBean.location_name);
			p.add("location_id", sel_locationBean.id+"");
		}else{
			p.add("location", defLoc.name);
			p.add("location_id", defLoc.id+"");
		}
		
		// 1:location 2:staging
		p.add("location_type", btnLocType.isCover() ? "1" : "2");
		p.add("detail_id", detail_id);
		// debug,48X40
		p.add("note", "");
		p.add("receipt_line_id", line_id);
		p.add("item_id", item_id);
		p.add("lot_no", lot_no);
		p.add("is_scan_create", addToReceive_whenAdd ? "1" : "0");
		p.add("is_check_location", check_location + "");
		switch (getAddMode()) {
		case Mode_QtyPerPlt: // 有配置plts
			// p.add("adid", StoredData.getAdid());
			p.add("config_type", "1"); // config 配置模式:1(QtyPerPlt)、2(Total
										// Remain)
			p.add("length", getl() + "");
			p.add("width", getw() + "");
			p.add("height", geth() + "");
			p.add("pallet_qty", getQty() + "");
			p.add("pallet_type", getTypeId());
			// auto时,剩余的qty
			if (hasAutoRemainQty()) {
				p.add("remainder_length", auto_remainQty + "");
				p.add("remainder_width", "1");
				p.add("remainder_height", "1");
			}
			break;
		case Mode_PltQty: // 无配置plts
			p.add("config_type", "1");
			p.add("pallet_qty", getQty() + "");
			p.add("pallet_type", getTypeId());
			break;

		default:
			break;
		}

		return p;
	}

	public String getReqUrl() {
		// 坏托盘
		if (getAddMode() == Mode_DmgPlts)
			return NetInterface.recAddDmgPlts_url();
		// 好托盘
		else
			// return HttpUrlPath.createContainer;
			return HttpUrlPath.basePath + "_receive/android/createContainerConcernCLPConfig";
	}

	private TextView tvQtyPerPlt, tvQty_title, tvRemainPlt;
	private TextView tvPltCnt;
	private Button btnAuto;
	private View loConfigLWH;
	
	private TextView tvLoc;

	private void initDialog() {
		view = LayoutInflater.from(mContext).inflate(R.layout.dlg_rec_configpallets, null);
		dialog = new BottomDialog(mContext);
		dialog.setContentView(view);
		dialog.setTitle("Pallet Configuration");
		initEditView();
		setData();
	}

	private View loRemainPlt;
	private TextView tvPltQty_title; // pltQty前的标题
	private CardButton btnLocType;

	private View loPalletType;
	private TextView tvPalletType;

	private void initEditView() {
		wedit = (EditText) view.findViewById(R.id.etW_dlg);

		ledit = (EditText) view.findViewById(R.id.etL_dlg);

		hedit = (EditText) view.findViewById(R.id.etH_dlg);

		qtyedit = (EditText) view.findViewById(R.id.etQty_dlg);
		tvQty_title = (TextView) view.findViewById(R.id.tvQty_title);
		tvPltQty_title = (TextView) view.findViewById(R.id.tvPltQty_title);

		tvQtyPerPlt = (TextView) view.findViewById(R.id.tvQtyPerPlt);
		tvPltCnt = (TextView) view.findViewById(R.id.tvPltCnt);
		btnAuto = (Button) view.findViewById(R.id.btnAuto);
		tvRemainPlt = (TextView) view.findViewById(R.id.tvRemainPlt);
		loRemainPlt = view.findViewById(R.id.loRemainPlt);
		loConfigLWH = view.findViewById(R.id.loConfigLWH);
		btnLocType = (CardButton) view.findViewById(R.id.btnLocType);

		loPalletType = view.findViewById(R.id.loPalletType);
		tvPalletType = (TextView) view.findViewById(R.id.tvPalletType);
		
		tvLoc = (TextView) view.findViewById(R.id.tvLoc);

		// 事件
		btnAuto.setOnClickListener(this);
		tvPalletType.setOnClickListener(this);
		tvLoc.setOnClickListener(this);
		qtyedit.addTextChangedListener(textWatcher);
		wedit.addTextChangedListener(textWatcher);
		ledit.addTextChangedListener(textWatcher);
		hedit.addTextChangedListener(textWatcher);

		palletType = Rec_PalletType.getContainerId(palletTypes);
		tvPalletType.setText(Rec_PalletType.getTypeName(palletType));

		ssb = (SingleSelectBar) view.findViewById(R.id.ssb);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (!checkForm())
					return;

				// 保留loc
				if(sel_locationBean != null)
				     TmpDataUtil.setRec_def_loc(new LocBean(sel_locationBean.location_name, btnLocType.isCover(),sel_locationBean.id), rnNO);
				
				if (onSubmitQtyListener != null)
					onSubmitQtyListener.onSubmit();

			}
		});

		// 初始化
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		// debug
		if (LocBean.isValid(defLoc)) {
			tvLoc.setHint(defLoc.name);
			btnLocType.showCover(defLoc.isAtLocation());
		}
		
		btnLocType.setOnChangeTypeListener(new CardButton.OnChangeTypeListener() {
			@Override
			public void onChange(String value) {
				tvLoc.setText("");
				tvLoc.setHint("NA");
				sel_locationBean = null;
				defLoc = null;
			}
		});
	}

	private void refBtnAuto_visiblity() {
		boolean showCnt = false;
		if (isFromCnt && getAddMode() == Mode_QtyPerPlt)
			showCnt = true;
		btnAuto.setVisibility(showCnt ? View.VISIBLE : View.GONE);
	}

	private SimpleTextWatcher textWatcher = new SimpleTextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			super.afterTextChanged(s);
			// 若修改pltQty,则隐藏remain
			// loRemainPlt.setVisibility(View.GONE);
			auto_remainQty = 0;
			refAutoRemainQty();
		}
	};

	private int auto_remainQty;

	/**
	 * 有partialPlt(auto剩余的)
	 * 
	 * @return
	 */
	private boolean hasAutoRemainQty() {
		return loRemainPlt.getVisibility() == View.VISIBLE && auto_remainQty > 0;
	}

	private void refAutoRemainQty() {
		// 为第一个tab,且有partialPlt才显示
		loRemainPlt.setVisibility((getAddMode() == Mode_QtyPerPlt && auto_remainQty > 0) ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAuto: // 自动
			int qtyPerPlt = getl() * getw() * geth();
			if (qtyPerPlt == 0) {
				UIHelper.showToast("Please Input \"Item Qty / Pallet\" first!");
				return;
			}
			if (remain <= 0) {
				qtyedit.setText("1");
				return;
			}

			int pltCnt = remain / qtyPerPlt;
			// 先设置et 再改值,防止触发afterTextChange改了auto_remainQty
			qtyedit.setText(pltCnt + "");
			auto_remainQty = remain % qtyPerPlt;
			tvRemainPlt.setText(auto_remainQty + " Items");

			// 先改值 再显示,防止触发qtyedit.addTextChange
			// loRemainPlt.setVisibility(auto_remainQty > 0 ? View.VISIBLE
			// : View.GONE);
			refAutoRemainQty();
			break;
		case R.id.tvPalletType:
			// pallet type
			if (Utility.isEmpty(palletTypes)) {
				RewriteBuilderDialog.showSimpleDialog_Tip(mContext, "No Pallet Type!");
				return;
			}
			toPalletTypeAc();
			break;
		case R.id.tvLoc:
			if(btnLocType.isCover()){
				Intent in = new Intent(mContext,ChooseLocationActivity.class);
				in.putExtra("single_select", true);
				mContext.startActivityForResult(in, Location_Back);
			}else{
				Intent in = new Intent(mContext,ChooseStagingActivity.class);
				in.putExtra("single_select", true);
				mContext.startActivityForResult(in, Staging_Back);
			}
			break;

		default:
			break;
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK)
			return;
		
		switch(requestCode){
		case Rec_ScanTask_SelectPalletType.Rec_SelectPalletType:
			Bundle params = data.getExtras();
			int index = params.getInt("index");
			palletType = palletTypes.get(index);

			tvPalletType.setText(palletType.type_name);
			break;
		case Location_Back:
			List<LocationBean> lists = (List<LocationBean>) data.getSerializableExtra("SelectLocation");
			sel_locationBean = lists.get(0);
			if(!Utility.isEmpty(lists))tvLoc.setText(lists.get(0).location_name);
			break;
		case Staging_Back:
			List<LocationBean> stagings = (List<LocationBean>) data.getSerializableExtra("SelectStaging");
			sel_locationBean = stagings.get(0);
			if(!Utility.isEmpty(stagings))tvLoc.setText(stagings.get(0).location_name);
			break;
		}
	}

	private void toPalletTypeAc() {
		Intent in = new Intent(mContext, Rec_ScanTask_SelectPalletType.class);
		Rec_ScanTask_SelectPalletType.initParams(in, palletTypes, palletType);
		mContext.startActivityForResult(in, Rec_ScanTask_SelectPalletType.Rec_SelectPalletType);
	}

	private String[] getType_strs(List<Rec_PalletType> list) {
		if (Utility.isEmpty(list))
			return new String[0];

		String[] ret = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			ret[i] = list.get(i).type_name;
		}
		return ret;
	}

	private void setData() {
		// 之前设计的tab:Input Per Qty、Plt Qty、Total Remain
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Item Based", Mode_QtyPerPlt));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("Pallet Based", Mode_PltQty));
		clickItems.add(new HoldDoubleValue<String, Integer>("Damage Pallet", Mode_DmgPlts));
		ssb.setUserDefineClickItems(clickItems);
		ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0: // Input Per Qty
					view.findViewById(R.id.dlg_layout1).setVisibility(View.VISIBLE);
					qtyedit.setVisibility(View.VISIBLE);
					tvQtyPerPlt.setVisibility(View.GONE);
					tvPltCnt.setVisibility(View.GONE);

					// tvQty_title.setText("Item QTY / Pallet: ");
					tvPltQty_title.setText("Full Pallet Qty: ");
					// debug
					// loRemainPlt.setVisibility(View.GONE);
					refAutoRemainQty();

					refBtnAuto_visiblity();

					// debug
					loConfigLWH.setVisibility(View.VISIBLE);

					break;
				// case 1: // Total Remain
				// view.findViewById(R.id.dlg_layout1)
				// .setVisibility(View.GONE);
				// qtyedit.setVisibility(View.GONE);
				// tvQtyPerPlt.setVisibility(View.VISIBLE);
				// tvPltCnt.setVisibility(View.VISIBLE);
				// tvQty_title.setText("Remain: ");
				//
				// tvQtyPerPlt.setText(remain + "");
				// // debug
				// loRemainPlt.setVisibility(View.GONE);
				// btnAuto.setVisibility(View.GONE);
				//
				// //debug
				// loConfigLWH.setVisibility(View.VISIBLE);
				// break;
				case 2: // plt qty
					// debug
					qtyedit.setVisibility(View.VISIBLE);
					tvPltCnt.setVisibility(View.GONE);

					// debug
					// loRemainPlt.setVisibility(View.GONE);
					refAutoRemainQty();

					refBtnAuto_visiblity();

					tvPltQty_title.setText("Pallet QTY: ");

					// debug
					loConfigLWH.setVisibility(View.GONE);

					break;

				case 1: // dmgPlt
					// debug
					qtyedit.setVisibility(View.VISIBLE);
					tvPltCnt.setVisibility(View.GONE);

					// debug
					// loRemainPlt.setVisibility(View.GONE);
					refAutoRemainQty();
					refBtnAuto_visiblity();

					tvPltQty_title.setText("Pallet QTY: ");

					// debug
					loConfigLWH.setVisibility(View.GONE);

					break;
				}
				qtyedit.setText("");
			}
		});
		ssb.userDefineSelectIndexExcuteClick(def_addGoodPlts ? 0 : 2);

		// 初始化
		refBtnAuto_visiblity();
	}

	public boolean isQtyPerPlt_mode() {
		return ssb.getCurrentSelectItem().b == Mode_QtyPerPlt;
	}

	public boolean isCreateDmgPlts_mode() {
		return ssb.getCurrentSelectItem().b == Mode_DmgPlts;
	}

	/**
	 * 添加-模式,取Mode_x
	 * 
	 * @return
	 */
	public int getAddMode() {
		return ssb.getCurrentSelectItem().b;
	}

	public static final int Mode_QtyPerPlt = 0;
	public static final int Mode_DmgPlts = 1;
	public static final int Mode_PltQty = 2;


	/**
	 * @return true:有效
	 */
	private boolean checkForm() {

		// ==========pltQty===================================

		if (getAddMode() == Mode_PltQty) {

			// plt数
			if (getQty() == 0) {
				UIHelper.showToast(mContext.getString(R.string.sync_select_pallet_qty));
				return false;
			}

			// 位置
			if (sel_locationBean == null && defLoc == null) {
				UIHelper.showToast(mContext.getString(R.string.sync_input_location));
				return false;
			}

			return true;
		}

		// dmgPlt,直接过=======================================
		if (getAddMode() == Mode_DmgPlts) {

			// 没有多余的
			// if (remain <= 0) {
			// UIHelper.showToast("No Piece Remain!");
			// return false;
			// }

			// plt数
			if (getQty() == 0) {
				UIHelper.showToast(mContext.getString(R.string.sync_select_pallet_qty));
				return false;
			}

			// 位置
			if (sel_locationBean == null && defLoc == null) {
				UIHelper.showToast(mContext.getString(R.string.sync_input_location));
				return false;
			}

			return true;
		}

		// ===========qtyPerPlt=============================

		// 校验
		if (getl() == 0) {
			UIHelper.showToast(mContext.getString(R.string.sync_input_length));
			return false;
		}
		if (getw() == 0) {
			UIHelper.showToast(mContext.getString(R.string.sync_input_width));
			return false;
		}
		if (geth() == 0) {
			UIHelper.showToast(mContext.getString(R.string.sync_input_hieght));
			return false;
		}
		if (getQty() == 0 && auto_remainQty == 0) {
			UIHelper.showToast(mContext.getString(R.string.sync_select_pallet_qty));
			return false;
		}
		// 位置
		if (sel_locationBean == null && defLoc == null) {
			UIHelper.showToast(mContext.getString(R.string.sync_input_location));
			return false;
		}

		return true;
	}

	public void show() {
		dialog.show();
	}

	public void dismiss() {
		dialog.dismiss();
	}

	public OnSubmitQtyListener getOnSubmitQtyListener() {
		return onSubmitQtyListener;
	}

	public void setOnSubmitQtyListener(OnSubmitQtyListener onSubmitQtyListener) {
		this.onSubmitQtyListener = onSubmitQtyListener;
	}

	private OnSubmitQtyListener onSubmitQtyListener;

	public interface OnSubmitQtyListener {
		void onSubmit();
	}
}
