package oso.ui.load_receive.print.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;
/**
 * @ClassName: ReceiptTicketModel 
 * @Description: 解析dockcheckin界面的json数据 用于全局之间的数据传递
 * @author gcy
 * @date 2014-9-4 下午3:39:46
 */
public class ReceiptTicketModel implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	private String defaultNum;//默认打印机
	private List<PrintServer> parsePrintServer;//打印机列表
	private String entryId;
	private String out_seal;
	private String door_name;

	private int number_type;//类型key
	private String fixNumber;
	private String fixOrder;
	
	private String detail_id;
	
	private List<ReceiptBillBase> receiptsTicketBaseList;//打印BILL界面数据的数据集合
	private ReceiptTicketBase receiptTicketBase;//loading_ticket的bean数据
	
	private String pathForCtn;
	

	private String container_no;
	
	
	public static int parseCount(JSONObject json) {
		return StringUtil.getJsonInt(json, "count");
	}

	public static ReceiptTicketModel parseJSON(JSONObject json,String order_no) {
		ReceiptTicketModel r = new ReceiptTicketModel();
		r.setContainer_no(json.optString("container_no"));
		
		r.setDefaultNum(ReceiptTicketModel.getDefault(json));
		r.setParsePrintServer(ReceiptTicketModel.parsePrintServer(json));
		r.setReceiptsTicketBaseList(ReceiptTicketModel.parseReceiptsTicketBaseList(json,order_no));
		
		
		r.setReceiptTicketBase(ReceiptTicketBase.parsePrintTicketBase(json));
		r.getReceiptTicketBase().setTrailer_loader(json.optInt("trailer_loader",0));
		r.getReceiptTicketBase().setFreight_counted(json.optInt("freight_counted",0));
		
		return r;
	}
	
	//----------代码冗余 注释掉了
//	/**
//	 * @Description:解析获取loading_ticket的bean
//	 * @param @param json
//	 * @param @return
//	 */
//	public static ReceiptTicketBase parseReceiptTicketBase(JSONObject json) {
//		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "loading_ticket");
//		ReceiptTicketBase receiptTicketBase = new ReceiptTicketBase();
//		receiptTicketBase.setEntryid(StringUtil.getJsonString(loading_ticket, "entryid"));
//		receiptTicketBase.setLoad_no(StringUtil.getJsonString(loading_ticket, "loadno"));
//		receiptTicketBase.setTime(StringUtil.getJsonString(loading_ticket, "window_check_in_time"));
//		receiptTicketBase.setContainer_no(StringUtil.getJsonString(loading_ticket, "gate_container_no"));
//		receiptTicketBase.setCompany_name(StringUtil.getJsonString(loading_ticket, "company_name"));
//		receiptTicketBase.setPath(StringUtil.getJsonString(loading_ticket, "path"));
//		receiptTicketBase.setCompanyid(StringUtil.getJsonString(loading_ticket, "companyid"));
//		receiptTicketBase.setCustomerid(StringUtil.getJsonString(loading_ticket, "customerid"));
//		return receiptTicketBase;
//	}
	/**
	 * @Description:解析打印BILL界面数据的数据集合
	 * @param @param json
	 * @param @return
	 */
	public static List<ReceiptBillBase> parseReceiptsTicketBaseList(JSONObject json,String order_no) {
		JSONArray receiptsTicketBaseArrays = StringUtil.getJsonArrayFromJson(json, "datas");
		List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
		if(!StringUtil.isNullForJSONArray(receiptsTicketBaseArrays)){
			for (int i = 0; i < receiptsTicketBaseArrays.length(); i++) {
				JSONObject jsonItem = StringUtil.getJsonObjectFromArray(receiptsTicketBaseArrays, i);
				ReceiptBillBase receiptsTicketBase = new ReceiptBillBase();
				receiptsTicketBase.setMaster_bol_no(StringUtil.getJsonString(jsonItem, "master_bol_no"));
				receiptsTicketBase.setName(StringUtil.getJsonString(jsonItem, "name"));
				receiptsTicketBase.setOrder_no(StringUtil.isNullOfStr(StringUtil.getJsonString(jsonItem, "order_no"))?order_no:StringUtil.getJsonString(jsonItem, "order_no"));
				receiptsTicketBase.setPath(StringUtil.getJsonString(jsonItem, "path"));
				receiptsTicketBase.setType(StringUtil.getJsonString(jsonItem, "type"));
				receiptsTicketBase.setLoad_no(StringUtil.getJsonString(jsonItem, "number"));
				receiptsTicketBase.setNumber_type(StringUtil.getJsonInt(jsonItem, "number_type"));
				receiptsTicketBase.setCustomerid(StringUtil.getJsonString(jsonItem, "customerid"));
				receiptsTicketBase.setCompanyid(StringUtil.getJsonString(jsonItem, "companyid"));
				receiptsTicketBaseList.add(receiptsTicketBase);
			}
		}		
		return receiptsTicketBaseList;
	}
	
	/**
	 * @Description:获取打印机列表
	 * @param @param json
	 * @param @return
	 */
	public static List<PrintServer> parsePrintServer(JSONObject json) {
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json, "printserver");
		JSONArray ja = StringUtil.getJsonArrayFromJson(jo, "allprintserver");
		if(!StringUtil.isNullForJSONArray(ja)){
			List<PrintServer> printServerList = new ArrayList<PrintServer>();
			for (int i = 0; i < ja.length(); i++) {
				JSONObject j = StringUtil.getJsonObjectFromArray(ja, i);
				PrintServer ps = new PrintServer();
	 			ps.setPrinter_server_id(StringUtil.getJsonString(j, "printer_server_id"));
				ps.setPrinter_server_name(StringUtil.getJsonString(j, "printer_server_name"));
				ps.setZone(StringUtil.getJsonString(j, "zone"));
				printServerList.add(ps);
			}
			return printServerList;
		}
		return null;
	}
	
	public static String getDefault(JSONObject json) {
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json, "printserver");
		return StringUtil.getJsonString(jo, "default");
	}

	public List<PrintServer> getParsePrintServer() {
		return parsePrintServer;
	}

	public void setParsePrintServer(List<PrintServer> parsePrintServer) {
		this.parsePrintServer = parsePrintServer;
	}

	public String getEntryId() {
		return entryId;
	}

	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}

	public int getNumber_type() {
		return number_type;
	}

	public void setNumber_type(int number_type) {
		this.number_type = number_type;
	}

	public String getOut_seal() {
		return out_seal;
	}

	public void setOut_seal(String out_seal) {
		this.out_seal = out_seal;
	}

	public String getDoor_name() {
		return door_name;
	}

	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}

	public List<ReceiptBillBase> getReceiptsTicketBaseList() {
		return receiptsTicketBaseList;
	}

	public void setReceiptsTicketBaseList(
			List<ReceiptBillBase> receiptsTicketBaseList) {
		this.receiptsTicketBaseList = receiptsTicketBaseList;
	}

	public ReceiptTicketBase getReceiptTicketBase() {
		return receiptTicketBase;
	}

	public void setReceiptTicketBase(ReceiptTicketBase receiptTicketBase) {
		this.receiptTicketBase = receiptTicketBase;
	}

	public String getDefaultNum() {
		return defaultNum;
	}

	public void setDefaultNum(String defaultNum) {
		this.defaultNum = defaultNum;
	}

	public String getPathForCtn() {
		return pathForCtn;
	}

	public void setPathForCtn(String pathForCtn) {
		this.pathForCtn = pathForCtn;
	}

	public String getFixNumber() {
		return fixNumber;
	}

	public void setFixNumber(String fixNumber) {
		this.fixNumber = fixNumber;
	}

	public String getFixOrder() {
		return fixOrder;
	}

	public void setFixOrder(String fixOrder) {
		this.fixOrder = fixOrder;
	}

	public String getContainer_no() {
		return container_no;
	}

	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}

	public String getDetail_id() {
		return detail_id;
	}

	public void setDetail_id(String detail_id) {
		this.detail_id = detail_id;
	}	
}
