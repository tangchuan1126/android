package oso.ui.load_receive.print.bean;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


public class PrintServer implements Serializable {
	
	private static final long serialVersionUID = 3353958799504675972L;
	
   	private String printer_server_id;//打印机ID
 	private String printer_server_name;//打印机名字
 	private String zone;//打印机区域
 	private boolean checkedFlag;
 	private String default_server;
 	public boolean isonline;
	 
	public String getPrinter_server_id() {
		return printer_server_id;
	}
	public void setPrinter_server_id(String printer_server_id) {
		this.printer_server_id = printer_server_id;
	}
	 
	public String getPrinter_server_name() {
		return printer_server_name;
	}
	public void setPrinter_server_name(String printer_server_name) {
		this.printer_server_name = printer_server_name;
	}
	
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public boolean isCheckedFlag() {
		return checkedFlag;
	}
	public void setCheckedFlag(boolean checkedFlag) {
		this.checkedFlag = checkedFlag;
	}
	//-----------------------------
	


	public static void parseBean(JSONObject jo,PrintServer b){
		b.printer_server_id=jo.optString("printer_server_id");
		b.printer_server_name=jo.optString("printer_server_name");
		b.isonline = jo.optString("isonline").equals("true");
		b.zone=jo.optString("zone");
	}
	
	public static void parseBeans(JSONArray ja,List<PrintServer> list){
		if(ja==null)
			return;
		
//		PrintServer promptName = new PrintServer();
//		promptName.setPrinter_server_id("0");
//		promptName.setPrinter_server_name((ja.length()>0)?"Please Select Print Server!":"No Print Server!");
//		list.add(promptName);
		
		for (int i = 0; i < ja.length(); i++) {
			PrintServer b=new PrintServer();
			parseBean(ja.optJSONObject(i), b);
			list.add(b);
		}
	}
	public String getDefault_server() {
		return default_server;
	}
	public void setDefault_server(String default_server) {
		this.default_server = default_server;
	}
	
	
}
