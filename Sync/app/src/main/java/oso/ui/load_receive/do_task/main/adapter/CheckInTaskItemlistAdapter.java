package oso.ui.load_receive.do_task.main.adapter;

import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.do_task.main.iface.CheckInTaskInterface;
import support.common.bean.CheckInTaskItemBeanMain;
import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckInTaskItemlistAdapter extends BaseAdapter {
//	public List<Boolean> mChecked;
	List<CheckInTaskItemBeanMain> listPerson;
	private LayoutInflater inflater;
	CheckInTaskInterface checkinItem;
	private Context context;

	public CheckInTaskItemlistAdapter(Context context,
			CheckInTaskInterface checkinItem, List<CheckInTaskItemBeanMain> list) {
		this.context = context;
		listPerson = new ArrayList<CheckInTaskItemBeanMain>();
		listPerson = list;
		this.checkinItem = checkinItem;
		this.inflater = LayoutInflater.from(this.context);
//		mChecked = new ArrayList<Boolean>();
		// 可能为空
		if (list == null)
			return;
//		for (int i = 0; i < list.size(); i++) {
//			mChecked.add(false);
//		}
	}

	@Override
	public int getCount() {
		return listPerson == null ? 0 : listPerson.size();
	}

	@Override
	public Object getItem(int position) {
		return listPerson.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final CheckInTaskItemBeanMain bean = listPerson.get(position);
		OnClickListener onClick_ls=new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.box_one:				//cb外容器
					if (bean.number_status != EntryDetailNumberStateKey.Close
							&& bean.number_status != EntryDetailNumberStateKey.Exception
							&& bean.number_status != EntryDetailNumberStateKey.Partially) {
						checkinItem.selectItem(bean, position);
					}
					break;
				case R.id.box_two:				//号、执行人等
					if (bean.number_status != EntryDetailNumberStateKey.Close
							&& bean.number_status != EntryDetailNumberStateKey.Exception
							&& bean.number_status != EntryDetailNumberStateKey.Partially) {
						checkinItem.selectItem(bean, position);
					}
					break;
				case R.id.adapter_take:			//takeOver
					checkinItem.all_btn(bean, 1);
					break;
				case R.id.adapter_start:		//start
					checkinItem.all_btn(bean, 2);
					break;
				case R.id.adapter_close:		//operation
					checkinItem.all_btn(bean, 3);
					break;

				default:
					break;
				}
			}
		};
		
		//=================================================
		
		ItemHolder holder = null;
		if (convertView == null) {
			holder = new ItemHolder();
			convertView = inflater.inflate(
					R.layout.checkin_task_listitem_layout, null);
			holder.box_one = (LinearLayout) convertView
					.findViewById(R.id.box_one);
			holder.box_two = (LinearLayout) convertView
					.findViewById(R.id.box_two);
			holder.three_linear = (LinearLayout) convertView
					.findViewById(R.id.three_linear);
			holder.selected = (CheckBox) convertView.findViewById(R.id.cbOrder);
			holder.task_type = (TextView) convertView
					.findViewById(R.id.task_type);
			holder.task_name = (TextView) convertView
					.findViewById(R.id.task_name);
			holder.executer_txt = (TextView) convertView
					.findViewById(R.id.executer_txt);
			holder.executer_txts = (TextView) convertView
					.findViewById(R.id.executer_txts);
			holder.adapter_take = (Button) convertView
					.findViewById(R.id.adapter_take);
			holder.adapter_start = (Button) convertView
					.findViewById(R.id.adapter_start);
			holder.adapter_close = (Button) convertView
					.findViewById(R.id.adapter_close);
			holder.number_status = (TextView) convertView
					.findViewById(R.id.number_status);
			holder.number_statuss = (TextView) convertView
					.findViewById(R.id.number_statuss);
			holder.three_text = (TextView) convertView
					.findViewById(R.id.three_text);
			holder.four_text = (TextView) convertView
					.findViewById(R.id.four_text);
			holder.t_number = (TextView) convertView
					.findViewById(R.id.t_number);
			holder.tvReceiptNO = (TextView) convertView
					.findViewById(R.id.tvReceiptNO);
			holder.selected.setOnCheckedChangeListener(null);
			convertView.setTag(holder);
		} else {
			holder = (ItemHolder) convertView.getTag();
		}

		// isforget 直接gatechekcout 返回1
		if (bean.is_forget_task == 1) {
			holder.t_number.setVisibility(View.VISIBLE);
			if (bean.number_status == -1 || bean.number_status == 1) {
				bean.number_status = 2;
				bean.takeoverflag = 0;
				checkinItem.setnewnumberstates(position);
			}
		} else {
			holder.t_number.setVisibility(View.GONE);
		}
		// 点击事件
//		holder.box_one.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if (bean.number_status != EntryDetailNumberStateKey.Close
//						&& bean.number_status != EntryDetailNumberStateKey.Exception
//						&& bean.number_status != EntryDetailNumberStateKey.Partially) {
//					checkinItem.selectItem(bean, position);
//				}
//			}
//		});
//		holder.box_two.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if (bean.number_status != EntryDetailNumberStateKey.Close
//						&& bean.number_status != EntryDetailNumberStateKey.Exception
//						&& bean.number_status != EntryDetailNumberStateKey.Partially) {
//					checkinItem.selectItem(bean, position);
//				}
//			}
//		});
//		holder.adapter_take.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stubX
//				checkinItem.all_btn(bean, 1);
//			}
//		});
//		holder.adapter_start.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				checkinItem.all_btn(bean, 2);
//			}
//		});
//		holder.adapter_close.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				checkinItem.all_btn(bean, 3);
//			}
//		});
		
		//事件
		holder.box_one.setOnClickListener(onClick_ls);
		holder.box_two.setOnClickListener(onClick_ls);
		holder.adapter_take.setOnClickListener(onClick_ls);
		holder.adapter_start.setOnClickListener(onClick_ls);
		holder.adapter_close.setOnClickListener(onClick_ls);

		// 赋值和显示
		// 第二行 状态及通知人 three状态 executer_txts通知人 在close之后显示
		holder.three_text.setText(EntryDetailNumberStateKey
				.getType(context,bean.number_status));
		holder.executer_txts.setText("By " + bean.executer);
		// 第三行 staging_area_id 只有不为空就显示 但在isforget的时候不显示
		if (bean.staging_area_id != null && bean.staging_area_id.length() > 0
				&& bean.is_forget_task == 0) {
			holder.four_text.setVisibility(View.VISIBLE);
			holder.four_text.setText(bean.staging_area_id + "");
		} else {
			holder.four_text.setVisibility(View.GONE);
		}
		//holder.selected.setChecked(mChecked.get(position));
		holder.selected.setChecked(bean.checked);

		// task_type 类型 task_name 名字 一直显示
		holder.task_type.setText(EntryDetailNumberTypeKey
				.getModuleName(bean.number_type) + ": ");
		boolean text_color_flag = (bean.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN
				|| bean.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL || bean.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS);
		holder.task_type.setTextColor(context.getResources().getColor(
				text_color_flag ? R.color.Red : R.color.checkin_blue));
		holder.task_name.setTextColor(context.getResources().getColor(
				text_color_flag ? R.color.Red : R.color.checkin_blue));

		// wms收货,显示receiptNO
		boolean showReceiptNO = bean.isWmsReceive()
				&& !TextUtils.isEmpty(bean.receipt_no);
		holder.tvReceiptNO.setVisibility(showReceiptNO ? View.VISIBLE
				: View.GONE);
		if (showReceiptNO)
			holder.tvReceiptNO.setText("Receipt: " + bean.receipt_no);

		holder.task_name.setText(bean.number + "");
		// 按钮区域的通知人 只有在close之后才会显示
		holder.executer_txt.setText(bean.executer + "");
		// checkbox在close之后消失
		if (bean.number_status == 3 || bean.number_status == 4
				|| bean.number_status == 5) {
			holder.selected.setVisibility(View.INVISIBLE);
		} else {
			holder.selected.setVisibility(View.VISIBLE);
		}
		// 按钮区域的显示与隐藏
		if (bean.is_forget_task == 0) {
			if (bean.takeoverflag == 1) {
				holder.adapter_take.setVisibility(View.VISIBLE);
				holder.adapter_start.setVisibility(View.GONE);
				holder.adapter_close.setVisibility(View.GONE);
				holder.executer_txt.setVisibility(View.VISIBLE);
			} else {
				holder.executer_txt.setVisibility(View.GONE);
			}
		}
		if (bean.number_status == EntryDetailNumberStateKey.Unprocess
				&& bean.takeoverflag == 0) {
			holder.adapter_take.setVisibility(View.GONE);
			holder.adapter_start.setVisibility(View.VISIBLE);
			holder.adapter_close.setVisibility(View.GONE);
		}
		if (bean.is_forget_task == 0) {
			if (bean.number_status == EntryDetailNumberStateKey.Processing
					&& bean.takeoverflag == 0) {
				holder.adapter_take.setVisibility(View.GONE);
				holder.adapter_start.setVisibility(View.GONE);
				holder.adapter_close.setVisibility(View.VISIBLE);
			}
		} else {
			if (bean.number_status == EntryDetailNumberStateKey.Processing) {
				holder.adapter_take.setVisibility(View.GONE);
				holder.adapter_start.setVisibility(View.GONE);
				holder.adapter_close.setVisibility(View.VISIBLE);
			}
		}

		if (bean.is_forget_task == 0) {
			if ((bean.number_status == EntryDetailNumberStateKey.Processing || bean.number_status == EntryDetailNumberStateKey.Unprocess)
					&& bean.takeoverflag == 0) {
				holder.three_linear.setVisibility(View.VISIBLE);
				if (bean.executer.length() > 0 && bean.executer != null
						&& !bean.executer.equals("Need Assign")) {
					holder.executer_txts.setVisibility(View.VISIBLE);
				} else {
					holder.executer_txts.setVisibility(View.GONE);
				}
			} else {
				holder.three_linear.setVisibility(View.GONE);
			}
		}
		if ((bean.number_status == EntryDetailNumberStateKey.Close
				|| bean.number_status == EntryDetailNumberStateKey.Exception || bean.number_status == EntryDetailNumberStateKey.Partially)
				&& bean.takeoverflag == 0) {
			holder.adapter_take.setVisibility(View.GONE);
			holder.adapter_start.setVisibility(View.GONE);
			holder.adapter_close.setVisibility(View.GONE);
			holder.three_linear.setVisibility(View.GONE);

			holder.number_status.setVisibility(View.VISIBLE);
			if (bean.executer.length() > 0 && bean.executer != null) {
				holder.number_statuss.setVisibility(View.VISIBLE);
			} else {
				holder.number_statuss.setVisibility(View.GONE);
			}
			holder.number_status.setText(EntryDetailNumberStateKey
					.getType(context,bean.number_status));
			holder.number_statuss.setText("By " + bean.executer);
		} else {
			holder.number_statuss.setVisibility(View.GONE);
		}

		return convertView;
	}

}

class ItemHolder {
	LinearLayout box_one;
	LinearLayout box_two;
	LinearLayout three_linear;
	CheckBox selected;
	TextView task_type;
	TextView task_name;
	TextView executer_txt, executer_txts;
	TextView number_status, number_statuss;
	TextView three_text;
	TextView four_text;
	TextView t_number, tvReceiptNO;
	Button adapter_take, adapter_start, adapter_close;
}
