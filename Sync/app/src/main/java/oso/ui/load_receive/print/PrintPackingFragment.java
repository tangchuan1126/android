package oso.ui.load_receive.print;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.ui.load_receive.print.adapter.PrintPackinglistAdapter;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.PrintPackinglistBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.CheckInLoadBarBean;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PrintPackingFragment extends Fragment implements
		PackingItemInterface {

	Button show;
	ListView lv;
	List<PrintPackinglistBean> persons = new ArrayList<PrintPackinglistBean>();
	Context mContext;
	PrintPackinglistAdapter adapter;
	List<Integer> listItemID = new ArrayList<Integer>();
	private View view;
	private CheckBox check_all;
	Activity activity;
	private Bundle bundle;
	private PrintPackMainBeanModel receiptTicketModel;
	int type = 0;
	StringBuilder sb = new StringBuilder();
	String retStr;
	LinearLayout packlist_linear;
	ImageView print_nodata_image;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.print_packing_list_layout, container,
				false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mContext = getActivity();
		activity = this.getActivity();
		bundle = getArguments();
		receiptTicketModel = (PrintPackMainBeanModel) bundle
				.getSerializable("receiptTicketModel");
		persons = receiptTicketModel.getPrintPackinglistBeanList();
		initView();
	}

	public void initView() {
		lv = (ListView) view.findViewById(R.id.print_packing_list);
		packlist_linear = (LinearLayout) view.findViewById(R.id.packlist_linear);
		print_nodata_image = (ImageView) view.findViewById(R.id.print_nodata_image);
		check_all = (CheckBox) view.findViewById(R.id.check_all);
		PrintTicketLoadDataActivity.setmorepackBack(this);
		adapter = new PrintPackinglistAdapter(mContext,
				PrintPackingFragment.this, persons);
		lv.setDivider(null);
		lv.setAdapter(adapter);

		if(persons.size() > 0 && persons!= null){
			packlist_linear.setVisibility(View.VISIBLE);
			print_nodata_image.setVisibility(View.GONE);
		}else{
			packlist_linear.setVisibility(View.GONE);
			print_nodata_image.setVisibility(View.VISIBLE);
		}
		
		check_all.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (type == 0) {
					for (int i = 0; i < persons.size(); i++) {
						adapter.mChecked.set(i, true);
					}
					adapter.notifyDataSetChanged();
					type = 1;          
				} else if (type == 1) {
					for (int i = 0; i < persons.size(); i++) {
						adapter.mChecked.set(i, false);
					}
					adapter.notifyDataSetChanged();
					type = 0;
				}
			}
		});
	}

	public void timedata(final String con) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mContext);
		Resources resources = mContext.getResources();
		builder.setCancelable(false);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		builder.setMessage(con);
		builder.setNegativeButton(
				resources.getString(R.string.sync_yes),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		builder.create().show();
	}

	@Override
	public void selectItemFinish(PrintPackinglistBean packitem, int i) {
		// TODO Auto-generated method stub
		if (i == -1) {
			Intent intent = new Intent(activity,
					PrintPackingListWebViewActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable("packitem", packitem);
			intent.putExtras(bundle);
			startActivity(intent);
		} else {
			if (adapter.mChecked.get(i).booleanValue() == true) {
				adapter.mChecked.set(i, false);
			} else if (adapter.mChecked.get(i).booleanValue() == false) {
				adapter.mChecked.set(i, true);
			}
			listItemIDnumber();
			if(listItemID.size() == persons.size()){
				check_all.setChecked(true);
				type = 1; 
			}else{
				check_all.setChecked(false);
				type = 0; 
			}
			adapter.notifyDataSetChanged();
		}
	}

	public void selectItemFinish(List<PrintPackinglistBean> choosepersons) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(activity,
				PrintPackingListWebViewActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("choosepersons", (Serializable) choosepersons);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	public void listItemIDnumber(){
		listItemID.clear();
		for (int i = 0; i < adapter.mChecked.size(); i++) {
			if (adapter.mChecked.get(i)) {
				listItemID.add(i);
			}
		}
	}
	@Override
	public void morepackinglist(String  id) {
		// TODO Auto-generated method stub
		listItemIDnumber();
		if (listItemID.size() == 0) {
			timedata("No records selected!");
		} else {
			for (int i = 0; i < listItemID.size(); i++) {
				sb.append(persons.get(listItemID.get(i).intValue())
								.getOrder_no() + ",");
			}
			retStr = sb.toString();
			// 移除-最后的","
			if (!TextUtils.isEmpty(retStr)){
				retStr = retStr.substring(0, retStr.length() - 1);
			}
			morepackinglists(id);
		}
	}
	// 打印,同scanLoadActivity中打印不同
	public void morepackinglists(String  id) {
			RequestParams params = new RequestParams();
			params.add("Method", "PackingListTask");
			params.put("dlo_detail_id", receiptTicketModel.getDetail_id());
			params.put("print_server_id", id + "");
			params.put("order_nos", retStr+"");
			params.put("resources_id", receiptTicketModel.getResources_id());
			params.put("resources_type", receiptTicketModel.getResources_type());
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					// TODO Auto-generated method stub
					UIHelper.showToast(activity, getString(R.string.sync_success));
				}

				@Override
				public void handFinish() {
				}

				@Override
				public void handFail() {
				}
			}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, activity);
		}

	@Override
	public int morepackinglist() {
		// TODO Auto-generated method stub
		listItemID.clear();
		for (int i = 0; i < adapter.mChecked.size(); i++) {
			if (adapter.mChecked.get(i)) {
				listItemID.add(i);
			}
		}
		return listItemID.size();
	}

	@Override
	public void selectItemFinish(CheckInLoadBarBean item,int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadbarnumber(int i,
			String s) {
		// TODO Auto-generated method stub
		
	}
}
