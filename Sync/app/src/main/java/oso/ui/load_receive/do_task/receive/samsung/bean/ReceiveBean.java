package oso.ui.load_receive.do_task.receive.samsung.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

public class ReceiveBean implements Serializable {

	@Override
	public String toString() {
		return "ReceiveBean [door=" + door + ", entry_id=" + entry_id + "]";
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getEntry_id() {
		return entry_id;
	}

	public void setEntry_id(String entry_id) {
		this.entry_id = entry_id;
	}

	private static final long serialVersionUID = -3408994998249469532L;

	/**
	 * 解析bean
	 * 
	 * @param joEntry
	 * @param receiveList
	 */
	public static List<ReceiveBean> parseReceiveBean(JSONObject jo) {
		List<ReceiveBean> datas = new ArrayList<ReceiveBean>();
		if (jo == null)
			return null;
		String entryId = StringUtil.getJsonString(jo, "entry_id");

		JSONArray ja = jo.optJSONArray("details");

		// 解析-doors
		for (int i = 0; i < ja.length(); i++) {
			List<Ctnr> ctnrList = new ArrayList<Ctnr>();
			JSONObject item = StringUtil.getJsonObjectFromArray(ja, i);
			String door = StringUtil.getJsonString(item, "door");
			JSONArray ctnrJa = StringUtil.getJsonArrayFromJson(item, "ctnrs");

			ReceiveBean receive = new ReceiveBean();
			receive.setDoor(door);
			receive.setEntry_id(entryId);

			// 解析-loads
			Ctnr.parseBeans(ctnrJa, ctnrList);

			receive.setCtnrList(ctnrList);
			datas.add(receive);
		}

		return datas;
	}

	public List<Ctnr> getCtnrList() {
		return ctnrList;
	}

	public void setCtnrList(List<Ctnr> ctnrList) {
		this.ctnrList = ctnrList;
	}

	private String door;
	private String entry_id;
	private List<Ctnr> ctnrList;
}
