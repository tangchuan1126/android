package oso.ui.load_receive.do_task.main.adapter;

import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.do_task.main.bean.Load_barsBean;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckInAddLoadbarListAdapter extends BaseAdapter {
	List<Load_barsBean> listPerson;
	private LayoutInflater inflater;
	private Context context;

	public CheckInAddLoadbarListAdapter(Context context, List<Load_barsBean> list) {
		this.context = context;
		listPerson = new ArrayList<Load_barsBean>();
		listPerson = list;
		this.inflater = LayoutInflater.from(this.context);
	}

	@Override
	public int getCount() {
		return listPerson.size();
	}

	@Override
	public Object getItem(int position) {
		return listPerson.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AddLoadbarHolder holder = null;
		if (convertView == null) {
			holder = new AddLoadbarHolder();
			convertView = inflater.inflate(R.layout.load_ber_additem_layout, null);
			holder.allview = convertView;
			holder.bar_name = (TextView) convertView.findViewById(R.id.bar_name);
			convertView.setTag(holder);
		} else {
			holder = (AddLoadbarHolder) convertView.getTag();
		}
		holder.bar_name.setText(listPerson.get(position).load_bar_name + "");
		return convertView;
	}
}

class AddLoadbarHolder {
	View allview;
	TextView name;
	TextView bar_name;
	LinearLayout left_box;
}
