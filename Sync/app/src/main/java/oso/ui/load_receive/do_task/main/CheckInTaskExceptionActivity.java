package oso.ui.load_receive.do_task.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.load.scan.SelectSubLoadActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInLoad_SubLoadBean;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.key.EntryDetailNumberTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * exception/partially,来自:CheckInTaskDoorItemActivity、ReceiveOsoAc
 * 
 * @author 朱成
 * @date 2015-1-4
 */
public class CheckInTaskExceptionActivity extends BaseActivity {

	public Resources resources;

	private TabToPhoto ttp;
	private EditText desEt;
	private Button btnSubmit;

	// private List<CheckInTaskItemBeanMain> taskitemlist = new
	// ArrayList<CheckInTaskItemBeanMain>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_task_exception, 0);
		resources = getResources();
		// initData();
		applyParams();
		initView();
		setData();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(
								CheckInTaskExceptionActivity.this,
								CheckInTaskDoorItemActivity.class);
						intent.putExtra("onback", 1);
						setResult(CheckInTaskDoorItemActivity.EXCEPTIONNUM,
								intent);
						CheckInTaskExceptionActivity.super.onBackBtnOrKey();
					}
				});
	}

	private void initView() {
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		desEt = (EditText) findViewById(R.id.desEt);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
	}

	private void setData() {
		ttp.init(mActivity, getTabParamList());
		ttp.setTTPEnabled(doorBean);
 
		btnSubmit
				.setText(closeType == ScanLoadActivity.CloseType_Exception ? "Exception"
						: "Partially");//李君浩注释
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
//		TabParam tab = new TabParam("Processing", "Task_processing_" + entry_id
//				+ doorBean.resources_id, new PhotoCheckable(0, "Image", ttp),
//				40);
		String key=TTPKey.getTaskProcessKey(entry_id);
		TabParam tab = new TabParam("Processing", key, new PhotoCheckable(0, "Image", ttp),
				40);
		tab.setWebImgsParams(
				String.valueOf(FileWithCheckInClassKey.PhotoTaskProcessing),
				String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN),
				doorBean.entry_id);
		params.add(tab);
		return params;
	}

	public void submitOnClick(View v) {
		if (desEt.getText().toString().trim().equals("")) {
			UIHelper.showToast(mActivity, getString(R.string.sync_input_reason_result), 0)
					.show();
			return;
		}
		RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(
				mActivity);
		dialog.setMessage(getString(R.string.sync_submit)+"?");
		dialog.setCancelable(false);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				submitException();
			}
		});
		dialog.create().show();
	}

	/**
	 * exception
	 */
	private void submitException() {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingCloseDetail");
		params.add("entry_id", entry_id + "");
		params.add("detail_id", detail_id + "");
		params.add("note", desEt.getText().toString() + "");
		params.add("number_status", closeType + "");
		params.add("equipment_id", doorBean.equipment_id + "");
		params.add("resources_type", doorBean.resources_type + "");
		params.add("resources_id", doorBean.resources_id + "");
		// debug
		params.add("request_type", "notaskinfos");

		// 有单据
		params.add("closefrom", isFrom_hasDoc() ? "1" : "0");
		if (isFrom_hasDoc())
			params.add("master_bol_no", mbol.master_bol_no + "");

		ttp.uploadZip(params, "TaskExceptionPhoto");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// closeDoor
				int data = StringUtil.getJsonInt(json, "returnflag");
				//若为有单据,则关闭bol
				if(isFrom_hasDoc())
					mbol.close();
				
				// 非最后1个,至door
				if (CheckInTaskFinishActivity.LoadCloseNoifyNormal == data) {
					// 有单据load
					if (isFrom_hasDoc()) {
						// 全关闭,至-task列表
						if (complexSubLoads.isAllClosed())
							Utility.popTo(mActivity,
									CheckInTaskDoorItemActivity.class);
						// 至-子单据列表
						else
							Utility.popTo(mActivity,
									SelectSubLoadActivity.class);//李君浩注释
					}
					// 无单据task
					else
						// 至-task列表
						Utility.popTo(mActivity,
								CheckInTaskDoorItemActivity.class);
				}
				// 最后1个,至finish
				else
					jumpToFinishDoorAc();
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, this);
	}

	private void jumpToFinishDoorAc() {
		Intent in = new Intent(mActivity, CheckInTaskFinishActivity.class);
		CheckInTaskFinishActivity.initParams(in, doorBean);
		startActivity(in);
		finish();
	}

	// 跳转
	public void finishway() {
		CheckInTaskDoorItemActivity.toLastAc(mActivity);
	}

	// =========传参===============================

	// 入参
	private String entry_id;
	private String detail_id;
	private CheckInTaskBeanMain doorBean = new CheckInTaskBeanMain();
	private int closeType;

	// 有单据load-相关参数================
//	private boolean isFrom_hasDoc = false; // 来自有单据load
	private CheckInLoad_ComplexSubLoads complexSubLoads;
	private CheckInLoad_SubLoadBean mbol;
//	private boolean isLastBol = false; // 为最后1个bol
	
	/**
	 * 来自有单据load
	 * @return
	 */
	public boolean isFrom_hasDoc(){
		return mbol!=null;
	}

	private static CheckInLoad_ComplexSubLoads bridge_complexSubLoads;

	public static void initParams(Intent intent, CheckInTaskBeanMain doorBean,
			CheckInTaskItemBeanMain taskBean, int closeType) {
		// Intent intent = new
		// Intent(mActivity,CheckInTaskExceptionActivity.class);
		intent.putExtra("entry_id", doorBean.entry_id + "");
		intent.putExtra("detail_id", taskBean.dlo_detail_id);
		intent.putExtra("title",
				EntryDetailNumberTypeKey.getModuleName(taskBean.number_type)
						+ ": " + taskBean.number + "");
		intent.putExtra("taskmainlist", (Serializable) doorBean);
		intent.putExtra("closeType", closeType);
		intent.putExtra("isFrom_hasDoc", false);
	}

	/**
	 * 来自有单据load
	 * 
	 * @param intent
	 * @param doorBean
	 * @param taskBean
	 * @param closeType
	 * @param mbol
	 */
	public static void initParams_hasDoc(Intent intent,
			CheckInTaskBeanMain doorBean, CheckInTaskItemBeanMain taskBean,
			int closeType, CheckInLoad_ComplexSubLoads complexSubLoads) {
		// Intent intent = new
		// Intent(mActivity,CheckInTaskExceptionActivity.class);
		intent.putExtra("entry_id", doorBean.entry_id + "");
		intent.putExtra("detail_id", taskBean.dlo_detail_id);
		intent.putExtra("title",
				EntryDetailNumberTypeKey.getModuleName(taskBean.number_type)
						+ ": " + taskBean.number + "");
		intent.putExtra("taskmainlist", (Serializable) doorBean);
		intent.putExtra("closeType", closeType);
		
		//有单据load
		intent.putExtra("isFrom_hasDoc", true);
		bridge_complexSubLoads = complexSubLoads;
	}

	private void applyParams() {
		// Bundle params = getIntent().getExtras();
		entry_id = getIntent().getStringExtra("entry_id");
		detail_id = getIntent().getStringExtra("detail_id");
		closeType = getIntent().getIntExtra("closeType",
				ScanLoadActivity.CloseType_Exception);//李君浩注释
		doorBean = (CheckInTaskBeanMain) getIntent().getSerializableExtra(
				"taskmainlist");
		// mbol= getIntent().getStringExtra("mbol");
		setTitleString(getIntent().getStringExtra("title") + "");

//		isFrom_hasDoc=getIntent().getBooleanExtra("isFrom_hasDoc", false);
		complexSubLoads = bridge_complexSubLoads;
		if (complexSubLoads != null)
			mbol = complexSubLoads.getCurSubLoad();
		bridge_complexSubLoads = null;
	}

}
