package oso.ui.load_receive.print.bean;

import java.io.Serializable;
/**
 * 
 * @ClassName: ReceiptsTicketBase 
 * @Description:
 * @author gcy 
 * @date 2014年7月17日 上午11:24:47 
 *
 */
public class ReceiptBillBase implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String companyid;
	private String customerid;
	private String path; // "check_in/a4print.html",
	private String type; // "MasterBOL",
	private String load_no;
	private int number_type;
	private String order_no; // 5778,
	private String master_bol_no; // 5778,
	private String name; // "Generic"
	private boolean checked;
	private boolean bolchecked;
	private String bolname;
	private String door_name;
	private String masterbolpath;//masterbolpath
	private String finishpath;

	private String shipToName;
	private String shipToID;
	
	
	public String getFinishpath() {
		return finishpath;
	}
	public void setFinishpath(String finishpath) {
		this.finishpath = finishpath;
	}
	public String getMasterbolpath() {
		return masterbolpath;
	}
	public void setMasterbolpath(String masterbolpath) {
		this.masterbolpath = masterbolpath;
	}
	public String getDoor_name() {
		return door_name;
	}
	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}
	public String getBolname() {
		return bolname;
	}
	public void setBolname(String bolname) {
		this.bolname = bolname;
	}
	/**
	 * @Description:
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @Description:
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @Description:
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @Description:
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @Description:
	 * @return the order_no
	 */
	public String getOrder_no() {
		return order_no;
	}
	/**
	 * @Description:
	 * @param order_no the order_no to set
	 */
	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}
	/**
	 * @Description:
	 * @return the master_bol_no
	 */
	public String getMaster_bol_no() {
		return master_bol_no;
	}
	/**
	 * @Description:
	 * @param master_bol_no the master_bol_no to set
	 */
	public void setMaster_bol_no(String master_bol_no) {
		this.master_bol_no = master_bol_no;
	}
	/**
	 * @Description:
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @Description:
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @Description:
	 * @return the load_no
	 */
	public String getLoad_no() {
		return load_no;
	}
	/**
	 * @Description:
	 * @param load_no the load_no to set
	 */
	public void setLoad_no(String load_no) {
		this.load_no = load_no;
	}
	/**
	 * @Description:
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}
	/**
	 * @Description:
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	/**
	 * @Description:
	 * @return the customerid
	 */
	public String getCustomerid() {
		return customerid;
	}
	/**
	 * @Description:
	 * @param customerid the customerid to set
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	/**
	 * @Description:
	 * @return the companyid
	 */
	public String getCompanyid() {
		return companyid;
	}
	/**
	 * @Description:
	 * @param companyid the companyid to set
	 */
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public int getNumber_type() {
		return number_type;
	}
	public void setNumber_type(int number_type) {
		this.number_type = number_type;
	}
	public boolean isBolchecked() {
		return bolchecked;
	}
	public void setBolchecked(boolean bolchecked) {
		this.bolchecked = bolchecked;
	}

	public void setShipToName(String name) {
		shipToName = name;
	}
	public String getShipToName() {
		return shipToName;
	}

	public void setShipToID(String id) {
		shipToID = id;
	}

	public String getShipToID() {
		return shipToID;
	}
}
