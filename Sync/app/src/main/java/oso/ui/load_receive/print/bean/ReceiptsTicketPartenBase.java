package oso.ui.load_receive.print.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @ClassName: ReceiptsTicketBase 
 * @Description:
 * @author gcy 
 * @date 2014年7月17日 上午11:24:47 
 *
 */
public class ReceiptsTicketPartenBase implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String customerid; // "MasterBOL",
	private String load_no;
	private int number_type;
	private String master_bol_no; // 5778,
	private List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
	
	
	/**
	 * @Description:
	 * @return the customerid
	 */
	public String getCustomerid() {
		return customerid;
	}
	/**
	 * @Description:
	 * @param customerid the customerid to set
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	/**
	 * @Description:
	 * @return the load_no
	 */
	public String getLoad_no() {
		return load_no;
	}
	/**
	 * @Description:
	 * @param load_no the load_no to set
	 */
	public void setLoad_no(String load_no) {
		this.load_no = load_no;
	}
	/**
	 * @Description:
	 * @return the master_bol_no
	 */
	public String getMaster_bol_no() {
		return master_bol_no;
	}
	/**
	 * @Description:
	 * @param master_bol_no the master_bol_no to set
	 */
	public void setMaster_bol_no(String master_bol_no) {
		this.master_bol_no = master_bol_no;
	}
	/**
	 * @Description:
	 * @return the receiptsTicketBaseList
	 */
	public List<ReceiptBillBase> getReceiptsTicketBaseList() {
		return receiptsTicketBaseList;
	}
	public int getNumber_type() {
		return number_type;
	}
	public void setNumber_type(int number_type) {
		this.number_type = number_type;
	}
	
}
