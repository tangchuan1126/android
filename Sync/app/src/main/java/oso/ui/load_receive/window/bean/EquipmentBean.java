package oso.ui.load_receive.window.bean;

import java.io.Serializable;

import support.key.CheckInTractorOrTrailerTypeKey;
import android.content.Context;

public class EquipmentBean implements Serializable {

	public String getEquipType_str(Context c){
		return CheckInTractorOrTrailerTypeKey.getContainerTypeKeyValue(c,equipment_type);
	}
	
	//=================================================

	public String getModule_id() {
		return module_id;
	}

	public void setModule_id(String module_id) {
		this.module_id = module_id;
	}

	public String getModule_type() {
		return module_type;
	}

	public void setModule_type(String module_type) {
		this.module_type = module_type;
	}

	public String getOccupy_status() {
		return occupy_status;
	}

	public void setOccupy_status(String occupy_status) {
		this.occupy_status = occupy_status;
	}

	public String getRelation_id() {
		return relation_id;
	}

	public void setRelation_id(String relation_id) {
		this.relation_id = relation_id;
	}

	public String getRelation_type() {
		return relation_type;
	}

	public void setRelation_type(String relation_type) {
		this.relation_type = relation_type;
	}

	public int getResources_id() {
		return resources_id;
	}

	public void setResources_id(int resources_id) {
		this.resources_id = resources_id;
	}

	public String getResources_name() {
		return resources_name;
	}

	public void setResources_name(String resources_name) {
		this.resources_name = resources_name;
	}

	public int getResources_type() {
		return resources_type;
	}

	public void setResources_type(int resources_type) {
		this.resources_type = resources_type;
	}

	public String getSrr_id() {
		return srr_id;
	}

	public void setSrr_id(String srr_id) {
		this.srr_id = srr_id;
	}

	public int getCheck_in_entry_id() {
		return check_in_entry_id;
	}

	public void setCheck_in_entry_id(int check_in_entry_id) {
		this.check_in_entry_id = check_in_entry_id;
	}

	public String getCheck_in_time() {
		return check_in_time;
	}

	public void setCheck_in_time(String check_in_time) {
		this.check_in_time = check_in_time;
	}

	public String getCheck_in_warehouse_time() {
		return check_in_warehouse_time;
	}

	public void setCheck_in_warehouse_time(String check_in_warehouse_time) {
		this.check_in_warehouse_time = check_in_warehouse_time;
	}

	public int getEquipment_id() {
		return equipment_id;
	}

	public void setEquipment_id(int equipment_id) {
		this.equipment_id = equipment_id;
	}

	public String getEquipment_number() {
		return equipment_number;
	}

	public void setEquipment_number(String equipment_number) {
		this.equipment_number = equipment_number;
	}

	public int getEquipment_purpose() {
		return equipment_purpose;
	}

	public void setEquipment_purpose(int equipment_purpose) {
		this.equipment_purpose = equipment_purpose;
	}

	public int getEquipment_status() {
		return equipment_status;
	}

	public void setEquipment_status(int equipment_status) {
		this.equipment_status = equipment_status;
	}

	public int getEquipment_type() {
		return equipment_type;
	}

	public void setEquipment_type(int equipment_type) {
		this.equipment_type = equipment_type;
	}

	public String getPhone_equipment_number() {
		return phone_equipment_number;
	}

	public void setPhone_equipment_number(String phone_equipment_number) {
		this.phone_equipment_number = phone_equipment_number;
	}

	public String getPs_id() {
		return ps_id;
	}

	public void setPs_id(String ps_id) {
		this.ps_id = ps_id;
	}

	public String getRel_type() {
		return rel_type;
	}

	public void setRel_type(String rel_type) {
		this.rel_type = rel_type;
	}

	public String getSeal_delivery() {
		return seal_delivery;
	}

	public void setSeal_delivery(String seal_delivery) {
		this.seal_delivery = seal_delivery;
	}

	public String getTotal_task() {
		return total_task;
	}

	public void setTotal_task(String total_task) {
		this.total_task = total_task;
	}

	public String getCheck_out_warehouse_time() {
		return check_out_warehouse_time;
	}

	public void setCheck_out_warehouse_time(String check_out_warehouse_time) {
		this.check_out_warehouse_time = check_out_warehouse_time;
	}

	public String getSeal_pick_up() {
		return seal_pick_up;
	}

	public void setSeal_pick_up(String seal_pick_up) {
		this.seal_pick_up = seal_pick_up;
	}

	public int getCheck_out_entry_id() {
		return check_out_entry_id;
	}

	public void setCheck_out_entry_id(int check_out_entry_id) {
		this.check_out_entry_id = check_out_entry_id;
	}

	public String getResources_type_value() {
		return resources_type_value;
	}

	public void setResources_type_value(String resources_type_value) {
		this.resources_type_value = resources_type_value;
	}

	private static final long serialVersionUID = -6577677757658641162L;

	private int equipment_purpose; // 操作类型,如:pickUp
	private int resources_id;
	private int resources_type;
	private String resources_name;
	private String resources_type_value; // 与resources_name相同，兼容gson解析

	private int check_out_entry_id;
	private int check_in_entry_id = 0;
	private String check_in_time;
	private String check_in_warehouse_time;
	private int equipment_id;
	private String equipment_number;
	private int equipment_status; // 状态,如:Processing
	private int equipment_type;
	private String phone_equipment_number;
	private String ps_id;
	private String rel_type;
	private String seal_delivery; // seal_in
	private String seal_pick_up; // seal_out

	private String total_task;

	private String module_id;
	private String module_type;
	private String occupy_status;
	private String relation_id;
	private String relation_type;

	private String srr_id;

	private String check_out_warehouse_time;

}
