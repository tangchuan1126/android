package oso.ui.load_receive.do_task.receive.task;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.process.bean.Rec_RepeatPalletBean;
import oso.ui.load_receive.do_task.receive.task.adapter.AdpRecScan_Plts;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.task.movement.Rec_Movement_Assign;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoBean;
import oso.ui.load_receive.do_task.receive.task.movement.bean.Rec_Movement_InfoLinesBean;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog.OnSubmitQtyListener;
import oso.ui.load_receive.do_task.receive.wms.Receive_ReceiveAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import support.common.UIHelper;
import support.common.print.OnInnerClickListener;
import support.common.print.PrintTool;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.dbhelper.TmpDataUtil;
import support.key.TTPKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class Rec_ScanTask_PalletsAc extends BaseActivity implements OnClickListener {
	
	

    private SearchEditText etSearch;

    private Button stagingBtn, btnFinishLine;
    private ListView lv;
    private TextView tvItemID, tvLotNO, tvPltCnt, tvSNCnt;


    Rec_PalletBean bean; // 待操作的bean(删除/打印)
    
    private static final int Rec_Pallet_FromAssing = 0x2300;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rec_scan_plts, 0);
        applyParams();

        // 取v
        etSearch = (SearchEditText) findViewById(R.id.etSearch);
        // btnCreateCCTask = (Button) findViewById(R.id.btnCreateCCTask);
        // btnHandDmg = (Button) findViewById(R.id.btnHandDmg);
        btnFinishLine = (Button) findViewById(R.id.btnFinishLine);
        lv = (ListView) findViewById(R.id.lv);
        tvItemID = (TextView) findViewById(R.id.tvItemID);
        tvLotNO = (TextView) findViewById(R.id.tvLotNO);
        tvPltCnt = (TextView) findViewById(R.id.tvPltCnt);
        tvSNCnt = (TextView) findViewById(R.id.tvSNCnt);
        stagingBtn = (Button) findViewById(R.id.stagingBtn);
//        tab = (SingleSelectBar) findViewById(R.id.tab);
//        lvClp=(ListView)findViewById(R.id.lvClp);

        // 初始化vp
//        vp = (VpWithTab) findViewById(R.id.vp);
//        initTab();
//        vp.init(tab);

        // lv
        lv.setEmptyView(findViewById(R.id.loEmpty));
        adpTlps =new AdpRecScan_Plts(this,listPlts,onInner_Tlps);
        lv.setAdapter(adpTlps);
        sortList();

        //lvClps
//        adpClps=new AdpRecScan_Plts(this,listClps,onInner_Clps);
//        lvClp.setAdapter(adpClps);

        // 事件
        // btnCreateCCTask.setOnClickListener(this);
        // btnHandDmg.setOnClickListener(this);
        btnFinishLine.setOnClickListener(this);
        findViewById(R.id.btnAddPlts).setOnClickListener(this);

        // 初始化
        setTitleString("Receipt: " + recBean.receipt_no);
        tvItemID.setText(itemBean.item_id);
        if(TextUtils.isEmpty(itemBean.lot_no)){
			tvLotNO.setText("");
			tvLotNO.setHint("NA");
		}else{
			tvLotNO.setText(itemBean.lot_no);
		}
        refSummaryUI();

        // etSearch
        etSearch.setScanMode();
        etSearch.setSeacherMethod(new SeacherMethod() {

            @Override
            public void seacher(String value) {
                // TODO Auto-generated method stub
                doScan(value);
                etSearch.setText("");
                Utility.colseInputMethod(mActivity, etSearch);
            }
        });

        showRightButton(R.drawable.menu_btn_style, "", new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDlg_menu();
            }
        });

        stagingBtn.setVisibility(View.GONE);
        stagingBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditStagingDialog();
            }
        });


    }

    /**
     * tlp列表-内部点击
     */
    private OnInnerClickListener onInner_Tlps=new OnInnerClickListener() {
        @Override
        public void onInnerClick(View v, int position) {
            if (Utility.isFastClick())
                return;

            Rec_PalletBean b=listPlts.get(position);
            switch (v.getId()) {
                case R.id.iv:                //图片
                    showPhotoDialog(b);
                    break;
                case R.id.printBtn:            //打印
                    tipPrint(b);
                    break;
                case R.id.scanBtn:            //进-扫描页
                    toScanAc(b);
                    break;

                default:
                    break;
            }
        }
    };

    /**
     * tlp列表-内部点击
     */
//    private OnInnerClickListener onInner_Clps=new OnInnerClickListener() {
//        @Override
//        public void onInnerClick(View v, int position) {
//            if (Utility.isFastClick())
//                return;
//
//            Rec_PalletBean b=listClps.get(position);
//            switch (v.getId()) {
//                case R.id.iv:                //图片
//                    showPhotoDialog(b);
//                    break;
//                case R.id.printBtn:            //打印
//                    tipPrint(b);
//                    break;
//                case R.id.scanBtn:            //进-扫描页
//                    toScanAc(b);
//                    break;
//
//                default:
//                    break;
//            }
//        }
//    };

//    private void initTab() {
//        List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
//        clickItems.add(new HoldDoubleValue<String, Integer>("TLP", 0));
//        clickItems.add(new HoldDoubleValue<String, Integer>("CLP", 1));
//        tab.setUserDefineClickItems(clickItems);
//    }

    /**
     * 重新-排序,扫过的到下面
     */
    private void sortList() {
        boolean orderChanged = false;
        // 倒叙扫描,若扫过 则后移
        int len = listPlts.size();
        for (int i = len - 1; i >= 0; i--) {
            Rec_PalletBean order = listPlts.get(i);
            // if (order.isScanedSn()) {
            if (order.isClosed()) {
                listPlts.add(listPlts.remove(i));
                orderChanged = true;
            }
        }
        if (orderChanged)
            adpTlps.notifyDataSetChanged();
    }

    private void tipDelDmgPlt(final Rec_PalletBean dmgPlt) {
        String tip = Utility.isEmpty(dmgPlt.sn) ? getString(R.string.sync_delete_notify) : getString(R.string.sync_delete_sn);
        RewriteBuilderDialog.showSimpleDialog(mActivity, tip, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                RequestParams p = NetInterface.recDelPlt_p(dmgPlt.con_id, itemBean.receipt_line_id, true);
                new SimpleJSONUtil() {

                    @Override
                    public void handReponseJson(JSONObject json) {
                        // TODO Auto-generated method stub
                        listPlts.remove(dmgPlt);
                        adpTlps.notifyDataSetChanged();
                        refSummaryUI();
                        UIHelper.showToast(getString(R.string.sync_delete_success));
                    }
                }.doGet(NetInterface.recDelPlt_url(), p, mActivity);
            }
        });
    }

    private void showEditStagingDialog() {
        BottomDialog dialog = new BottomDialog(mActivity);
        dialog.setTitle(getString(R.string.sync_notice));
        dialog.setDefTv("Location: ");
        dialog.setDefHint("");
        dialog.setCanceledOnTouchOutside(true);
        dialog.setShowCancelBtn(true);
        dialog.setDefEtToUpperCase(true);
        dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                dlg.dismiss();
                if (TextUtils.isEmpty(value)) {
                    return;
                }
                stagingBtn.setText(value);
            }
        });
        dialog.show();
    }

    private PrintTool printTool = new PrintTool(this);
    private PrintTool.OnPrintLs printLs = new PrintTool.OnPrintLs() {

        @Override
        public void onPrint(long printServerID) {
            // TODO Auto-generated method stub
            reqPrintPlt(printServerID);
        }
    };

    private PrintTool.OnPrintLs printLsAll = new PrintTool.OnPrintLs() {

        @Override
        public void onPrint(long printServerID) {
            RequestParams p = NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_LineID,
                    itemBean.receipt_line_id);
            new SimpleJSONUtil() {

                @Override
                public void handReponseJson(JSONObject json) {
                    // TODO Auto-generated method stub
                    String name = StoredData.getUsername();
                    List<Rec_PalletBean> listAll=getAllPlts();
                    for (Rec_PalletBean bean : listAll) {
                        bean.print_user_name = name;
                    }
                    adpTlps.notifyDataSetChanged();
                    UIHelper.showToast(getString(R.string.sync_print_success));
                }
            }.doGet(NetInterface.recPrintLP_url(), p, mActivity);
        }
    };

    private List<Rec_PalletBean> getAllPlts(){
//        List<Rec_PalletBean> listAll=new ArrayList<Rec_PalletBean>();
//        listAll.addAll(listPlts);
//        listAll.addAll(listClps);
//        return listAll;
        return listPlts;
    }

    /**
     * 打单个plt
     *
     * @param printServerID
     */
    private void reqPrintPlt(long printServerID) {
        RequestParams p = NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, bean.con_id);
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
                String name = StoredData.getUsername();
                bean.print_user_name = name;
                notifyAllAdp();
                UIHelper.showToast(getString(R.string.sync_print_success));
            }
        }.doGet(NetInterface.recPrintLP_url(), p, this);

    }

    private void showDlg_menu() {
        RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.sync_operation));
        builder.setArrowItems(new String[]{"Refresh","Print All Pallets", "Create Pallets"}, new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	if(position == 0){
            		reqLinePlts();
            	}
                if (position == 1) {
                    printTool.showDlg_printers(printLsAll);
                }
                if (position == 2) {
                    showDlg_addPallet();
                }
            }
        });
        builder.show();
    }
    
    private QtyDialog qtyDialog;
    //
    private void showDlg_addPallet() {
        LocBean defLoc = TmpDataUtil.getRec_def_loc(recBean.receipt_no, listPlts);
        qtyDialog = new QtyDialog(mActivity, 0, true, recBean.receipt_no, defLoc, listTypes, false);
        qtyDialog.setOnSubmitQtyListener(new OnSubmitQtyListener() {
            public void onSubmit() {
                boolean isMode_QtyPerPlt = qtyDialog.ssb.getCurrentSelectItem().b == QtyDialog.Mode_QtyPerPlt;
                showConfirmDialog(qtyDialog, isMode_QtyPerPlt);
            }
        });
        qtyDialog.show();
    }

    private void showConfirmDialog(final QtyDialog dialog, final boolean isMode_QtyPerPlt) {
        // 确认finish
        RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.patrol_confirm));
        
        View v = View.inflate(mActivity, R.layout.dlg_pallet_configuration_confirm, null);
        TextView packagingType,itemQty,palletQty,stagingLocation,tvLocationName;
        LinearLayout ll_packagingType,ll_itemQty;
        
        packagingType = (TextView) v.findViewById(R.id.packagingType);
        itemQty = (TextView) v.findViewById(R.id.itemQty);
        palletQty = (TextView) v.findViewById(R.id.palletQty);
        stagingLocation = (TextView) v.findViewById(R.id.stagingLocation);
        ll_packagingType = (LinearLayout) v.findViewById(R.id.ll_packagingType);
        ll_itemQty = (LinearLayout) v.findViewById(R.id.ll_itemQty);
        tvLocationName = (TextView) v.findViewById(R.id.tvLocationName);
        
        palletQty.setText(dialog.getQty() + "");
        stagingLocation.setText(dialog.getLoc());
        tvLocationName.setText(dialog.getCardConver() == 1 ? "Location :" : "Staging :");
        if(isMode_QtyPerPlt){
        	itemQty.setText(dialog.getl() + "x" + dialog.getw() + "x" + dialog.geth());
        }else{
        	ll_itemQty.setVisibility(View.GONE);
        }
        if(!dialog.isTypeEmpty()){
        	packagingType.setText(dialog.getType());
        }else{
        	ll_packagingType.setVisibility(View.GONE);
        }
        builder.setContentView(v);
//        builder.setAdapter(new BaseAdapter() {
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                final View view = View.inflate(mActivity, R.layout.dlg_finish_count_confirm, null);
//                final TextView key = (TextView) view.findViewById(R.id.keyTv);
//                final TextView value = (TextView) view.findViewById(R.id.valueTv);
//                key.setEms(9);
//                if (isMode_QtyPerPlt) {
//                    switch (position) {
//                        case 0:
//                        	key.setText("Packaging Type:");
//                        	value.setText(dialog.getType());
//                            break;
//                        case 1:
//                        	key.setText("Item Qty / Pallet:");
//                            value.setText(dialog.getl() + "x" + dialog.getw() + "x" + dialog.geth());
//                        	break;
//                        case 2:
//                            key.setText("Pallet Qty:");
//                            value.setText(dialog.getQty() + "");
//                            break;
//                        case 3:
//                            key.setText("Staging Location:");
//                            value.setText(dialog.getLoc());
//                            break;
//                    }
//                } else {
//                    switch (position) {
//                        case 0:
//                        	key.setText("Packaging Type:");
//                        	value.setText(dialog.getType());
//                        	break;
//                        case 1:
//                            key.setText("Pallet Qty:");
//                            value.setText(dialog.getQty() + "");
//                            break;
//                        case 2:
//                            key.setText("Staging Location:");
//                            value.setText(dialog.getLoc());
//                            break;
//                    }
//                }
//                return view;
//            }
//
//            @Override
//            public long getItemId(int position) {
//                return 0;
//            }
//
//            @Override
//            public Object getItem(int position) {
//                return null;
//            }
//
//            @Override
//            public int getCount() {
//                return isMode_QtyPerPlt ? 4 : 3;
//            }
//        }, null);
        builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                reqAddConfig(dialog,1);
            }
        });
        builder.show();
    }

    /**
     * 添加plt
     *
     * @param dlg
     */
    private void reqAddConfig(final QtyDialog dlg,int check_location) {
        Rec_ItemBean bean = itemBean;
        RequestParams p = dlg.getNetParam(bean.receipt_line_id, bean.item_id, bean.lot_no, recBean.dlo_detail_id, true, false, true,check_location);
        p.add("is_match_clp_config", "0");   //0  不检查  clp  type 类型
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
            	
            	if(json.optInt("err") == 90 && json.optInt("is_check_location") == 0){
					RewriteBuilderDialog.showSimpleDialog(mActivity, json.optString("data"), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqAddConfig(dlg,0);
						}
					});
					
					return;
				}
                // 好托盘
                if (!dlg.isCreateDmgPlts_mode()) {
                    JSONArray ja = json.optJSONArray("containers");
                    if (Utility.isEmpty(ja))
                        return;
                    List<Rec_PalletBean> tmpList = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
                    }.getType());
                    listPlts.addAll(0, tmpList);
                    adpTlps.notifyDataSetChanged();
                    UIHelper.showToast(getString(R.string.sync_success));
                }
                // 坏托盘
                else {
                    JSONArray ja = json.optJSONArray("damage_container");
                    if (Utility.isEmpty(ja))
                        return;
                    List<Rec_PalletBean> tmpPlt = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletBean>>() {
                    }.getType());
                    listPlts.addAll(0, tmpPlt);
                    adpTlps.notifyDataSetChanged();
                    UIHelper.showToast(getString(R.string.sync_success));
                }
                refSummaryUI();
                dlg.dismiss();
            }
        }
                // .doGet(HttpUrlPath.createContainer, p, this);
                .doGet(dlg.getReqUrl(), p, this);
    }
    
    
    

    // /**
    // * 添加-goodPlt
    // */
    // private void showDlg_addPallet() {
    // final View view = View.inflate(mActivity,
    // R.layout.dlg_scan_sn_configpallets, null);
    // final EditText etL_dlg = (EditText) view.findViewById(R.id.etL_dlg);
    // final EditText etW_dlg = (EditText) view.findViewById(R.id.etW_dlg);
    // final EditText etH_dlg = (EditText) view.findViewById(R.id.etH_dlg);
    // final EditText locEt = (EditText) view.findViewById(R.id.etLoc);
    //
    // // 初始化
    // locEt.setTransformationMethod(AllCapTransformationMethod.getThis());
    //
    // BottomDialog.showCustomViewDialog(mActivity, "Add Good Pallet", view,
    // false, new OnSubmitClickListener() {
    // @Override
    // public void onSubmitClick(BottomDialog dlg, String value) {
    // int l = Utility.parseInt(etL_dlg.getText().toString());
    // int w = Utility.parseInt(etW_dlg.getText().toString());
    // int h = Utility.parseInt(etH_dlg.getText().toString());
    // String location = locEt.getEditableText().toString();
    // int qtyPerPlt = l * w * h;
    // if (qtyPerPlt == 0) {
    // UIHelper.showToast("Please Input \"Qty Per Pallet\" first!");
    // return;
    // }
    // if (TextUtils.isEmpty(location)) {
    // UIHelper.showToast("Please Input Location!");
    // return;
    // }
    // reqAddConfig(itemBean, l, w, h, 1, location, dlg);
    // }
    // });
    // }

    // /**
    // * @param goods_type
    // * 2 是damage的托盘,0是goods的托盘 也可以不传
    // * @param dlg
    // */
    // private void reqAddConfig(Rec_ItemBean bean, final int l, final int w,
    // final int h, final int qty, String location, final BottomDialog dlg) {
    // RequestParams params = new RequestParams();
    // params.put("receipt_line_id", bean.receipt_line_id);
    // params.put("detail_id", recBean.dlo_detail_id);
    // params.put("item_id", bean.item_id);
    // params.put("lot_no", bean.lot_no);
    // params.put("length", l + "");
    // params.put("width", w + "");
    // params.put("height", h + "");
    // params.put("pallet_type", "");
    // params.put("location", location);
    // params.put("note", "");
    // params.put("pallet_qty", qty);
    // params.put("adid", StoredData.getAdid());
    // // params.put("goods_type", goods_type + "");
    //
    // new SimpleJSONUtil() {
    //
    // @Override
    // public void handReponseJson(JSONObject json) {
    // // TODO Auto-generated method stub
    // System.out.println(json.toString());
    // JSONArray ja = json.optJSONArray("containers");
    // if (Utility.isEmpty(ja))
    // return;
    //
    // List<Rec_PalletBean> tmpList = new Gson().fromJson(ja.toString(), new
    // TypeToken<List<Rec_PalletBean>>() {
    // }.getType());
    // listPlts.addAll(0, tmpList);
    // adapter.notifyDataSetChanged();
    // refSummaryUI();
    // dlg.dismiss();
    // }
    // }.doGet(HttpUrlPath.scanCreateContainer, params, this);
    // }

    // private void showDlg_addDmgPlt() {
    // final View view = View.inflate(mActivity, R.layout.dlg_add_dmg_plt,
    // null);
    // final EditText qtyEt = (EditText) view.findViewById(R.id.et_qty);
    // final EditText locEt = (EditText) view.findViewById(R.id.et_loc);
    // // TextView tvQty_title = (TextView)
    // // view.findViewById(R.id.tvQty_title);
    // // tvQty_title.setText("Item Qty:");
    //
    // // 初始化
    // locEt.setTransformationMethod(AllCapTransformationMethod.getThis());
    // locEt.setHint(TmpDataUtil.getRec_def_loc(recBean.receipt_no));
    //
    // qtyEt.setHint("");
    //
    // BottomDialog.showCustomViewDialog(mActivity, "Create Damage Pallets",
    // view, false, new OnSubmitClickListener() {
    // @Override
    // public void onSubmitClick(BottomDialog dlg, String value) {
    // String qtyValue = qtyEt.getText().toString().trim();
    // String locValue = Utility.getEt_valueOrHint(locEt).trim().toUpperCase();
    // // int qty = TextUtils.isEmpty(qtyValue) ? 1 :
    // // Utility.parseInt(qtyValue);
    // int qty = Utility.parseInt(qtyValue);
    // // 数量
    // if (qty == 0) {
    // UIHelper.showToast("Please Input Item Qty!");
    // return;
    // }
    // // 位置
    // if (TextUtils.isEmpty(locValue)) {
    // UIHelper.showToast("Please Input Location!");
    // return;
    // }
    // // reqAddConfig(itemBean, qty, 1, 1, 1, locValue,
    // // Rec_PalletBean.PltType_Damage, dlg);
    // reqAddDmgPlt(qty, Rec_PalletBean.PltType_Damage, locValue, dlg);
    // TmpDataUtil.setRec_def_loc(locEt,recBean.receipt_no);
    // }
    // });
    // }

    // private void reqAddDmgPlt(int pltQty, int pltType, String loc, final
    // BottomDialog dlg) {
    // // RequestParams p = new RequestParams();
    // // p.add("receipt_line_id", itemBean.receipt_line_id);
    // // p.add("item_id", itemBean.item_id);
    // // p.add("lot_no", itemBean.lot_no);
    // // p.add("pallet_type", "");
    // // p.add("pallet_qty", pltQty + "");
    // // p.add("ps_id", StoredData.getPs_id());
    // // p.add("goods_type", pltType + "");
    // // p.add("detail_id", recBean.dlo_detail_id);
    // // p.add("location", loc);
    // RequestParams p = NetInterface.recAddDmgPlts_p(pltQty,
    // itemBean.receipt_line_id, itemBean.item_id, itemBean.lot_no,
    // recBean.dlo_detail_id,
    // loc, true);
    // new SimpleJSONUtil() {
    //
    // @Override
    // public void handReponseJson(JSONObject json) {
    // // TODO Auto-generated method stub
    // JSONArray ja = json.optJSONArray("damage_container");
    // if (Utility.isEmpty(ja))
    // return;
    // List<Rec_PalletBean> tmpList = new Gson().fromJson(ja.toString(), new
    // TypeToken<List<Rec_PalletBean>>() {
    // }.getType());
    // listPlts.addAll(0, tmpList);
    // adapter.notifyDataSetChanged();
    // refSummaryUI();
    // dlg.dismiss();
    // UIHelper.showToast("Success!");
    // }
    // }.doGet(NetInterface.recAddDmgPlts_url(), p, this);
    // }

    // private boolean isFristResume = true;
    //
    // @Override
    // protected void onResume() {
    // // TODO Auto-generated method stub
    // super.onResume();
    //
    // if (!isFristResume)
    // reqLinePlts();
    // isFristResume = false;
    // }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (ttp_dlg != null)
            ttp_dlg.onActivityResult(requestCode, resultCode, data);
        
        if(qtyDialog != null)
        	qtyDialog.onActivityResult(requestCode, resultCode, data);

        // 刷列表
        if (requestCode == Req_ScanAc)
            reqLinePlts();
        
        if(requestCode == Rec_Pallet_FromAssing){
        	toLastAc();
            UIHelper.showToast(getString(R.string.sync_success));
        }
    }

    @Override
    protected void onFirstVisible() {
        // TODO Auto-generated method stub
        super.onFirstVisible();
        TTS.getInstance().speakAll("Ready!");
    }

    private void refSummaryUI() {
    	//
    	reqRefreshState();
    	
        int plts_total = listPlts.size();
        // int plts_closed = Rec_PalletBean.getScanedPlts(listPlts);
        int plts_closed = Rec_PalletBean.getClosedPlts(listPlts);
        int sn_scaned = Rec_PalletBean.getScanedSn(listPlts);

        tvPltCnt.setText(plts_closed + " / " + plts_total);
		tvSNCnt.setText(sn_scaned + " / " + itemBean.getCntQty());
//      tvSNCnt.setText(sn_scaned + " / " + itemBean.expected_qty);

        // 关闭line
//        boolean showFinish = Rec_PalletBean.isAllClosed(listPlts)&&Rec_PalletBean.isAllClosed(listClps);
        boolean showFinish = Rec_PalletBean.isAllClosed(listPlts);
        //删光了也不出来
        if (listPlts.size() == 0)
            showFinish = false;
        // sn_scaned == itemBean.getCntQty()
        btnFinishLine.setVisibility(showFinish ? View.VISIBLE : View.GONE);
    }

    private void reqLinePlts() {

        final RequestParams params = new RequestParams();
        params.add("receipt_line_id", itemBean.receipt_line_id);
        // Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
        params.add("container_config_id", "0");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                JSONArray jaPlts = json.optJSONArray("pallets");
                if (Utility.isEmpty(jaPlts)) {
                    RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No pallets! Please config pallets first!");
                    return;
                }
                // plts
                List<Rec_PalletBean> tmpPlts = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
                }.getType());

                // dmgPlts
                JSONArray jaDmg = json.optJSONArray("damaged");
                List<Rec_PalletBean> listDmg = null;
                if (!Utility.isEmpty(jaDmg)) {
                    listDmg = new Gson().fromJson(jaDmg.toString(), new TypeToken<List<Rec_PalletBean>>() {
                    }.getType());
                    tmpPlts.addAll(listDmg);
                }

                listPlts.clear();
                listPlts.addAll(tmpPlts);
                adpTlps.notifyDataSetChanged();
                refSummaryUI();
                sortList();
            }

        }.doGet(HttpUrlPath.acquirePalletsInfoByLine, params, this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btnAddPlts:    //添加plts
                showDlg_addPallet();
                break;
            case R.id.btnFinishLine: // 结束line

                //不能有空plt
                int emptyPlt = Rec_PalletBean.getFirst_emptyPlt(listPlts);
                if (emptyPlt >= 0) {
                    String pltNO = listPlts.get(emptyPlt).getDisPltNO();
                    RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Pallet " + pltNO + " has no SN, please delete first!");
                    // 选中空plt
                    lv.setSelection(emptyPlt);
                    return;
                }

                // goodPlt不能有dmgSn
                int goodPlt_withDmgGood = Rec_PalletBean.getFirst_goodPlt_withDmgGoods(listPlts);
                if (goodPlt_withDmgGood >= 0) {
                    RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please move damage goods to damage pallets!");
                    // 选中未打印项
                    lv.setSelection(goodPlt_withDmgGood);
                    return;
                }

                // 提示-打印pltLabels
                int unprint = Rec_PalletBean.getFirst_UnprintedPlt(listPlts);
                if (unprint >= 0) {
                    RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Please print all Pallet Labels first!");
                    // 选中未打印项
                    lv.setSelection(unprint);
                    return;
                }

                showDlg_confirmSubmit();
                break;
            default:
                break;
        }
    }

    private void showDlg_confirmSubmit() {
        // v
        View v = getLayoutInflater().inflate(R.layout.dlg_rec_plts_confirm, null);
        TextView tvExpQty_dlg = (TextView) v.findViewById(R.id.tvExpQty_dlg);
        TextView tvCntQty = (TextView) v.findViewById(R.id.tvCntQty);
        TextView tvSnQty_dlg = (TextView) v.findViewById(R.id.tvSnQty_dlg);
        TextView tvSN_notMatch = (TextView) v.findViewById(R.id.tvSN_notMatch);

        // 初始化
        // expQty
        tvExpQty_dlg.setText(itemBean.expected_qty + "");
        // cntQty
        String cntQty = itemBean.getCntQty() + "";
        if (itemBean.damage_qty > 0)
            cntQty = cntQty + " (G:" + itemBean.normal_qty + " , D: " + itemBean.damage_qty + ")";
        tvCntQty.setText(cntQty);
        // snQty
        int snGood = Rec_PalletBean.getScanedSn_good(listPlts);
        int snDmg = Rec_PalletBean.getScanedSn_dmg(listPlts);
        String snQty = (snGood + snDmg) + "";
        if (snDmg > 0)
            snQty = snQty + " (G:" + snGood + " , D: " + snDmg + ")";
        tvSnQty_dlg.setText(snQty);

        int snColor = (snGood + snDmg) == itemBean.expected_qty ? R.color.sync_blue : R.color.sync_red_light;
        tvSnQty_dlg.setTextColor(getResources().getColor(snColor));

        //数量不对时-提示
//		tvSN_notMatch.setVisibility((snGood+snDmg)==itemBean.expected_qty?View.GONE:View.VISIBLE);

        // dlg
        RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
        bd.setContentView(v);
        bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                reqFinishLine();
            }
        });
        bd.show();
    }

    private void reqFinishLine() {
        RequestParams p = new RequestParams();
        p.add("line_id", itemBean.receipt_line_id);
        //标记是从task进    ，还是通过rn
        p.add("type", isFromTask ? "0":"1");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
                boolean isLastLine=json.optBoolean("is_last_line");
                //通过搜rn过来==========================
                if(!isFromTask){
                    UIHelper.showToast(getString(R.string.sync_finish_success));
                    Intent in = new Intent();
                    in.putExtra("FinishScan", true);
                    setResult(RESULT_OK, in);
//                    toLastAc();
                    finish();
                }
                //=====来自task==========================

                //为最后1个line
                if (isLastLine)
                    tipCloseRN();
                //不是最后1个,直接跳出
                else {
                    UIHelper.showToast(getString(R.string.sync_finish_success));
                    toLastAc();
                }
            }
        }.doPost(HttpUrlPath.basePath + "_receive/android/closeLine", p, this);
    }

    private void toLastAc(){
        //来自task 跳出line
        if(isFromTask)
            Utility.popTo(this,Rec_ScanTask_ListAc.class);
        else
            onBackPressed();
    }
    
    
    private void tipCloseRN(){
       RewriteBuilderDialog.Builder bd=new RewriteBuilderDialog.Builder(this);
       bd.setMessage("All Items of the Receipt has finished, close RN?");
       bd.hideCancelBtn();
       bd.setCancelable(false);
       bd.setCanceledOnTouchOutside(false);
       bd.setPositiveButton(R.string.sync_yes,new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
                reqCloseRN();
           }
       });
       bd.show();
    }
    private void reqCloseRN() {
        RequestParams p = new RequestParams();
//        p.add("receipt_id", recBean.receipt_id);
//        p.add("status", Rec_ReceiveBean.RNStatus_Closed);
        p.add("line_id", recBean.curItemBean.receipt_line_id);
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
            	if(json.optInt("err") == 91){
					Rec_RepeatPalletBean repeatPalletBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_RepeatPalletBean.class);
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
					
					View v = View.inflate(mActivity, R.layout.item_repeatpallet, null);
					TextView tvSn = (TextView) v.findViewById(R.id.tvSN);
					TextView tvPallet1 = (TextView) v.findViewById(R.id.tvPallet1);
					TextView tvPallet2 = (TextView) v.findViewById(R.id.tvPallet2);
					tvSn.setText(repeatPalletBean.serial_number);
					String[] pallets = repeatPalletBean.getContainer();
					tvPallet1.setText(pallets[0]);
					tvPallet2.setText(pallets[1]);
					
					builder.setContentView(v);
					
					builder.isHideCancelBtn(true);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
					});
					builder.show();
					return;
				}
//            	if(json.optInt("is_print_rn") == 1){
//					tipPrintReceiptTicket();
//				}else{
//					 toLastAc();
//		             UIHelper.showToast(getString(R.string.sync_success));
//				}
//            	toLastAc();
//	            UIHelper.showToast(getString(R.string.sync_success));
            	reqAssignMovement();
            }
        }.setCancelable(false).doGet(
                HttpUrlPath.basePath + "_receive/android/closeLineAndRn", p, this);

    }
    
    private void reqAssignMovement(){
		RequestParams p = new RequestParams();
	    p.add("ps_id", StoredData.getPs_id());
	    p.add("receipt_id", recBean.receipt_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				String object = json.optJSONObject("data").toString();
				Rec_Movement_InfoBean infoBean = new Gson().fromJson(object,Rec_Movement_InfoBean.class);
				String array = json.optJSONObject("data").optJSONArray("lines").toString();
				List<Rec_Movement_InfoLinesBean> linesBean = new Gson().fromJson(array, new TypeToken<List<Rec_Movement_InfoLinesBean>>(){}.getType());
				
				Intent in = new Intent(mActivity,Rec_Movement_Assign.class);
				Rec_Movement_Assign.initParams(in, infoBean, linesBean,Rec_Movement_Assign.To_Assign);
				startActivityForResult(in,Rec_Pallet_FromAssing);
			}
		}.setCancelable(false).doGet(
				HttpUrlPath.basePath + "_receive/android/acquireFinallyPallets", p, this);
	}
    
    private void tipPrintReceiptTicket() {
		RewriteBuilderDialog.Builder bd = RewriteBuilderDialog.newSimpleDialog(this, "Print Receipt Ticket?", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 打签
				printTool.showDlg_printers(new PrintTool.OnPrintLs() {

					@Override
					public void onPrint(long printServerID) {
						reqPrintRNTicket(printServerID);
					}
				});
			}
		});
		bd.setNegativeButton(R.string.sync_no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 toLastAc();
	             UIHelper.showToast(getString(R.string.sync_success));
			}
		});
		bd.show();
	}
    
	
	private void reqPrintRNTicket(long printServerID) {
		RequestParams p = new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		p.add("printer", printServerID + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
			
			@Override
			public void handFinish() {
				super.handFinish();
				
				 toLastAc();
	             UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/printRNCount", p, mActivity);

	}
	

    private void doScan(String plt) {

        // 空
        if (TextUtils.isEmpty(plt)) {
            TTS.getInstance().speakAll_withToast("Invalid Pallet!", true);
            return;
        }

        Rec_PalletBean pltBean = Rec_PalletBean.getPallet_byScanNO(listPlts, plt);

        // 未找到
        if (pltBean == null) {
            TTS.getInstance().speakAll_withToast("Pallet Not Found!", true);
            return;
        }

        listPlts.remove(pltBean);
        listPlts.add(pltBean);
        adpTlps.notifyDataSetChanged();

        // 进入
        toScanAc(pltBean);
    }

    private final int Req_ScanAc = 1;

    private void toScanAc(Rec_PalletBean b) {
//        List<Rec_PalletBean> listAll=new ArrayList<Rec_PalletBean>();
//        listAll.addAll(listPlts);
//        listAll.addAll(listClps);

        Intent in = new Intent(mActivity, Receive_ReceiveAc.class);
        b.isLastPlt_inLine = Rec_PalletBean.isLastPlt_inLine(listPlts, b);
        // 临时-数据
        itemBean.receipt_id = recBean.receipt_id;
        Receive_ReceiveAc.initParams(in, itemBean, listPlts, b, recBean.receipt_no);
        startActivityForResult(in, Req_ScanAc);
    }

    /**
     * 若有人打印过 则提示
     *
     * @param p
     */
    private void tipPrint(Rec_PalletBean p) {
        bean = p;
        if (p.isPrinted()) {
            String tip = String.format("This Pallet Label has been printed by %s, reprint?", p.print_user_name);
            RewriteBuilderDialog.showSimpleDialog(this, tip, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    printTool.showDlg_printers(printLs);
                }
            });
        } else
            printTool.showDlg_printers(printLs);
    }

    // =========传参===============================

    private Rec_ReceiveBean recBean;
    private List<Rec_PalletBean> listPlts;
    
    private List<Rec_PalletType> listTypes;

//    private List<Rec_PalletBean> listClps;

    private Rec_ItemBean itemBean;
    private boolean isFromTask;                 //true:来自-扫描任务,false:来自搜sn

    public static void initParams(Intent in, List<Rec_PalletBean> listPlts, List<Rec_PalletType> types, Rec_ReceiveBean recBean,boolean isFromTask) {
        in.putExtra("listPlts", (Serializable) listPlts);
        in.putExtra("recBean", (Serializable) recBean);
        in.putExtra("isFromTask",isFromTask);
        in.putExtra("types", (Serializable)types);
    }

    private void applyParams() {
        Bundle params = getIntent().getExtras();
        listPlts = (List<Rec_PalletBean>) params.getSerializable("listPlts");
        recBean = (Rec_ReceiveBean) params.getSerializable("recBean");
        listTypes = (List<Rec_PalletType>) params.getSerializable("types");
        isFromTask=params.getBoolean("isFromTask");
        itemBean = recBean.curItemBean;
    }

    private TabToPhoto ttp_dlg; // 当前-正在添加图片ttp,用于onActivityRst

    // ==================nested=====================================

    private AdpRecScan_Plts adpTlps;
//    private AdpRecScan_Plts adpClps;

    public void showMenuDialog(final Rec_PalletBean b) {
        String[] menus = new String[] { "Print", "Delete" };
        //tlp
//        if (!b.isClp())
//            menus = new String[] { "Print", "Delete" };
//        //clp
//        else
//            menus = new String[] { "Print" };
        RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
        builder.setTitle(R.string.menutitle_operation);
        builder.setArrowItems(menus, new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // 打印
                        tipPrint(b);
                        break;
                    case 1: // delete
                    	tipDelDmgPlt(b);
                        break;

                    default:
                        break;
                }
            }
        });
        builder.show();
    }
    
    
    private boolean is_tlp_type = false;
    
    private void reqRefreshState(){
    	for(Rec_PalletBean bean : listPlts){
    		if(bean.container_type == Rec_PalletBean.ConType_TLP){
    			if(!bean.isDamagePlt()){
    				is_tlp_type = true;
    			}
    		}
    	}
    }

    private void showPhotoDialog(final Rec_PalletBean b) {
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
        ttp_dlg = (TabToPhoto) view.findViewById(R.id.ttp);
        // ttp.initNoTitleBar(mActivity, new TabParam("tabName", "key", new
        // PhotoCheckable(0, "title", ttp)));
        initTTP(ttp_dlg, b);
        BottomDialog.showCustomViewDialog(mActivity, "Pallet Photos", view, false, new OnSubmitClickListener() {
            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                if (ttp_dlg.getPhotoCnt(false) > 0)
                    reqUploadPhotos(b, ttp_dlg);
                dlg.dismiss();
            }
        });
    }

    private void reqUploadPhotos(final Rec_PalletBean b, final TabToPhoto ttp) {

        RequestParams p = new RequestParams();
        p.add("con_id", b.con_id);
        ttp.uploadZip(p, "receive");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
                ttp.clearData();
                // UIHelper.showToast("Success!");
                JSONArray ja = json.optJSONArray("photo");
                if (Utility.isEmpty(ja))
                    return;
                List<TTPImgBean> listPhotos = new Gson().fromJson(ja.toString(), new TypeToken<List<TTPImgBean>>() {
                }.getType());
                b.photos = listPhotos;
                notifyAllAdp();
            }
        }.doPost(HttpUrlPath.basePath + "_receive/android/uploadPhoto", p, this);

    }

    private void notifyAllAdp(){
        adpTlps.notifyDataSetChanged();
//        adpClps.notifyDataSetChanged();
    }

    private void initTTP(TabToPhoto ttp, final Rec_PalletBean pltBean) {
        String key = TTPKey.getRecPltKey(recBean.receipt_no, pltBean.con_id);
        ttp.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
        ttp.pvs[0].addWebPhotoToIv(pltBean.photos);
        ttp.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

            @Override
            public void onDeleteComplete(int tabIndex, TTPImgBean b) {
                // TODO Auto-generated method stub
                List<TTPImgBean> imgs = pltBean.photos;
                if (imgs != null && imgs.contains(b)) {
                    imgs.remove(b);
                    adpTlps.notifyDataSetChanged();
                }
            }
        });
    }
    
    @Override
    protected void onBackBtnOrKey() {
    	Intent in = new Intent();
    	setResult(RESULT_OK, in);
    	super.onBackBtnOrKey();
    }
}
