package oso.ui.load_receive.print;

import java.util.List;

import oso.ui.load_receive.print.adapter.ReceiptsTicketAdapter;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.bean.ReceiptTicketModel;
import oso.ui.load_receive.print.bean.ReceiptsTicketPartenBase;
import oso.ui.load_receive.print.iface.ReceiptsTicketDataInterface;
import oso.ui.load_receive.print.iface.TicketData;
import oso.ui.load_receive.print.util.PrintOtherCondition;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import declare.com.vvme.R;

public class ReceiptsBillOfLoadingFragment extends Fragment {
	
	private View view ;
	private Context context;
	private Bundle bundle ;
	Activity activity;
	private List<ReceiptsTicketPartenBase> receiptsTicketPartenBaseList;
	private ReceiptsTicketAdapter receiptsTicketAdapter;
	private ListView receiptsTicketBase_listview;
	
	TicketData ticketData;
	private ReceiptTicketModel receiptTicketModel;
	private ReceiptsTicketDataInterface receiptsTicketDataInterface;
	
	private PrintOtherCondition printOtherCondition;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.printiconbar_receipts_ticket_left_layout, container, false);
		return view;
	}
	 
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 		context =  getActivity();
		bundle = getArguments() ;
		activity = this.getActivity();
		receiptTicketModel = (ReceiptTicketModel)bundle.getSerializable("receiptTicketModel");
		receiptsTicketPartenBaseList = (List<ReceiptsTicketPartenBase>) bundle.getSerializable("receiptsTicketPartenBaseList");		
		initViews();
		setViewData();
	}
	@Override
	public void onAttach(Activity activity) {
 		super.onAttach(activity);
 		ticketData = (TicketData)activity;
	}
	private void initViews() {
		receiptsTicketBase_listview = (ListView) view.findViewById(R.id.receipts_ticketbase_listview);
		
		printOtherCondition = (PrintOtherCondition) view.findViewById(R.id.PrintOtherCondition);
		if(printOtherCondition==null){
			return;
		}
		if(printOtherCondition.jumpShowOtherCondition(receiptTicketModel.getReceiptTicketBase())){
			printOtherCondition.setVisibility(View.VISIBLE);
			printOtherCondition.init(receiptTicketModel.getReceiptTicketBase(),activity);
		}
	
	}
	private void setViewData(){
		receiptsTicketDataInterface = new ReceiptsTicketDataInterface() {
			@Override
			public void preview(ReceiptBillBase r) {
				
				Intent intent = new Intent();
				intent.setClass(activity, PreViewWebViewActivity.class);
				intent.putExtra("receiptTicketModel", receiptTicketModel);
				intent.putExtra("receiptBillBase", r);
				startActivityForResult(intent, ReceiptsTicketDatasActivity.ReceiptsBillOfLoadingFragment);
			}

			@Override
			public void preview(ReceiptBillBase r, int type) {
				// TODO Auto-generated method stub
				
			}
		};
		
		receiptsTicketAdapter = new ReceiptsTicketAdapter(context, this.receiptsTicketPartenBaseList,receiptsTicketDataInterface);
		receiptsTicketBase_listview.setDivider(null);
		receiptsTicketBase_listview.setAdapter(receiptsTicketAdapter);
	}
}
