package oso.ui.load_receive.do_task.receive.wms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.adapter.PalletAdapter;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog.OnSubmitQtyListener;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.VpWithTab;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import oso.widget.singleSelect.SingleSelectBar;
import support.AppConfig;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.OnInnerClickListener;
import support.common.print.PrintTool;
import support.common.print.PrintTool.OnPrintLs;
import support.dbhelper.StoredData;
import support.dbhelper.TmpDataUtil;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 创建pallets
 * 
 * @author 朱成
 * @date 2015-3-11
 */
public class Receive_CreatePalletsAc extends BaseActivity implements OnClickListener {

	private final int What_ChooseSupervisor = 1;

	private ListView lvPallets, lvDmg;

	private View loOperator, btnAssign, loEmpty, imgAdd,loEmpty_dmg;
	private TextView tvOperator, tvItem, tvLotNO, tvRemain, tvTotalQty, tvRemain_title;

	private SingleSelectBar ssb;
	private VpWithTab vp;

	// private Rec_ItemBean linesBean;
	
	private TextView tvGoodPlts_cnt,tvDmgPlts_cnt;
	
	private final static int Receive_ConfigDetailAc_Back = 1000;
	
	private ClpConfigAdapter adapter;
	
	//标记当前选择项
	private int currentClpType = -1;
	
	private PalletAdapter adp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_rec_configpallets, 0);
		setTitleString("Pallet Configuration");
		applyParams();

		// 取v
		vp = (VpWithTab) findViewById(R.id.vp);
		lvPallets = (ListView) findViewById(R.id.lvPallets);
		lvDmg = (ListView) findViewById(R.id.lvDmg);
		loOperator = findViewById(R.id.loOperator);
		tvOperator = (TextView) findViewById(R.id.tvOperator);
		btnAssign = findViewById(R.id.btnAssign);
		tvItem = (TextView) findViewById(R.id.tvItem);
		tvLotNO = (TextView) findViewById(R.id.tvLotNO);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
		tvTotalQty = (TextView) findViewById(R.id.tvTotalQty);
		loEmpty = findViewById(R.id.loEmpty);
		imgAdd = findViewById(R.id.imgAdd);
		tvRemain_title = (TextView) findViewById(R.id.tvRemain_title);
		loEmpty_dmg=findViewById(R.id.loEmpty_dmg);
		tvGoodPlts_cnt=(TextView) findViewById(R.id.tvGoodPlts_cnt);
		tvDmgPlts_cnt=(TextView) findViewById(R.id.tvDmgPlts_cnt);

		// tab
		initSingleSelectBar();
		vp.init(ssb);
//		vp.setOnVpPageChangeListener(onVpPageChangeListener);
//		// 若无dmg,则隐藏tab
//		if (Utility.isEmpty(listDmg)) {
//			vp.isScrollable = false;
//			ssb.setVisibility(View.GONE);
//		}

		// 事件
		imgAdd.setOnClickListener(this);
		loOperator.setOnClickListener(this);
		btnAssign.setOnClickListener(this);
		findViewById(R.id.btnAddGoodPlts).setOnClickListener(this);
		findViewById(R.id.btnAddDmgPlts).setOnClickListener(this);

		adp = new PalletAdapter(mActivity, listConfigs);
		// lv
		lvPallets.setEmptyView(loEmpty);
		lvPallets.setAdapter(adp);
		lvPallets.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// reqConfigPlts(position);
				showMenu_item(position);
			}
		});

		// lvDmg
		lvDmg.setEmptyView(loEmpty_dmg);
		lvDmg.setAdapter(adpDmg);
		lvDmg.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showDmgMenu(position);
			}
		});

		// 初始化
		loOperator.setVisibility(isFromSupvisor ? View.VISIBLE : View.GONE);
		btnAssign.setVisibility(isFromSupvisor ? View.VISIBLE : View.GONE);
		// linesBean = (Rec_ItemBean) getIntent()
		// .getSerializableExtra("personBean");
		tvItem.setText(itemBean.item_id);
		if(TextUtils.isEmpty(itemBean.lot_no)){
			tvLotNO.setText("");
			tvLotNO.setHint("NA");
		}else{
			tvLotNO.setText(itemBean.lot_no);
		}
		refSumUI();
		tvTotalQty.setText(itemBean.expected_qty + "");

		showRightButton(R.drawable.print_all_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				boolean isPrinted = false;
				String strPrint="";
				for(Rec_PalletConfigBean bean : listConfigs){
					if(bean.isPrinted()){
						isPrinted = true;
						strPrint = bean.print_user_name;
						break;
					}
				}
				if(!isPrinted){
					for(Rec_PalletBean pltDmg : listDmg){
						if(pltDmg.isPrinted()){
							isPrinted = true;
							strPrint = pltDmg.print_user_name;
							break;
						}
					}
				}
				if(isPrinted){
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
					String msg = String.format("These Labels has been printed by %s. Reprint?", strPrint);
					builder.setMessage(msg);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							printTool.showDlg_printers(lsAll);
						}
					});
					builder.create().show();
				}else{
					printTool.showDlg_printers(lsAll);
				}
				// itemBean.receipt_id
			}
		});
	}

	OnPrintLs lsAll = new OnPrintLs() {
		@Override
		public void onPrint(long printServerID) {
			RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_LineID, itemBean.receipt_line_id);
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					// TODO Auto-generated method stub
					String name = StoredData.getUsername();
					for (Rec_PalletConfigBean bean : listConfigs) {
						bean.print_user_name = name;
					}
					for (Rec_PalletBean pltDmg : listDmg) {
						pltDmg.print_user_name=name;
					}
					adp.notifyDataSetChanged();
					adpDmg.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_print_success));
				}
			}.doGet(NetInterface.recPrintLP_url(), p, mActivity);

		}
	};

	private void showDmgMenu(final int position) {
		final Rec_PalletBean bean = (Rec_PalletBean) adpDmg.getItem(position);
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));
		bd.setArrowItems(new String[] { "Print Pallets", "Delete" }, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
				// TODO Auto-generated method stub
				switch (position_dlg) {
				case 0: // 打印
					tipPrintDmgPlt(bean);
					break;
				case 1: // 删除
					RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqDelPlt(bean);
						}
					});
					break;

				default:
					break;
				}
			}
		});
		bd.show();
	}

	private void tipPrintDmgPlt(final Rec_PalletBean bean) {

		final PrintTool.OnPrintLs printLs = new PrintTool.OnPrintLs() {

			@Override
			public void onPrint(long printServerID) {
				// TODO Auto-generated method stub
				reqPrintPlts(printServerID, bean);
			}
		};

		if (bean.isPrinted()) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			String msg = String.format("These Labels has been printed by %s. Reprint?", bean.print_user_name);
			builder.setMessage(msg);
			builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// config_toPrint = listConfigs.get(position);
					printTool.showDlg_printers(printLs);
				}
			});
			builder.create().show();
		} else {
			// config_toPrint = listConfigs.get(position);
			printTool.showDlg_printers(printLs);
		}
	}

	private void reqPrintPlts(long printServerID, Rec_PalletBean dmgPlt) {
		// String ids = Rec_PalletBean.getPltIDs(tmpPlts);
		final Rec_PalletBean tmpPlt = dmgPlt;
		if (tmpPlt == null)
			return;
		
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, tmpPlt.con_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				String name = StoredData.getUsername();
				tmpPlt.print_user_name = name;
				// debug
				// adpDamagePallets.notifyDataSetChanged();
				// adpPallets.notifyDataSetChanged();
				adpDmg.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);

	}

	/**
	 * tab改变时
	 */
//	private OnVpPageChangeListener onVpPageChangeListener = new OnVpPageChangeListener() {
//
//		@Override
//		public void onPageChange(int index) {
//			// TODO Auto-generated method stub
//			imgAdd.setVisibility(isTab_GoodMode() ? View.VISIBLE : View.INVISIBLE);
//		}
//	};

	private boolean isTab_GoodMode() {
		return ssb.getCurrentSelectItem().b == 0;
	}

	private void initSingleSelectBar() {
		ssb = (SingleSelectBar) findViewById(R.id.ssb);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Good", 0));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("Damage", 1));
		// ssb.setVisibility(View.GONE);
		ssb.setUserDefineClickItems(clickItems);
		// ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
		// @Override
		// public void clickCallBack(HoldDoubleValue<String, Integer>
		// selectValue) {
		// // switch (selectValue.b) {
		// // case 0:
		// // break;
		// // case 1:
		// // break;
		// // }
		// }
		// });
		ssb.userDefineSelectIndexExcuteClick(0);
	}

	private String adid_notify; // 人

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_CANCELED)
			return;
		
		if(requestCode == Receive_ConfigDetailAc_Back)
			refresh();
		
		if(qtyDialog != null)
			qtyDialog.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case What_ChooseSupervisor: // 选管理员
			String svNames = data.getStringExtra("select_user_names");
			adid_notify = data.getStringExtra("select_user_adids");
			tvOperator.setText(svNames);
			break;

		default:
			break;
		}
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void refresh() {
		RequestParams p = new RequestParams();
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray ja = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
				}.getType());

				// Intent in = new Intent(ct, Receive_CreatePalletsAc.class);
				// Receive_CreatePalletsAc.initParams(in, itemBean, list,
				// isFromSupvisor);
				// ct.startActivity(in);

				listConfigs.clear();
				listConfigs.addAll(list);
				adp.notifyDataSetChanged();
				refSumUI();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/findItemPalletConfig", p, mActivity);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(Utility.isFastClick())
			return;
		switch (v.getId()) {
		case R.id.imgAdd: // 添加pallet
			showDlg_addPallet(isTab_GoodMode());
			break;
		case R.id.loOperator: // labor
			int dep;
			int pos;
			if (AppConfig.isDebug) {
//				dep = BaseDataDepartmentKey.Manager;
				dep = BaseDataDepartmentKey.Warehouse;
				pos = BaseDataSetUpStaffJobKey.All;
			} else {
				dep = BaseDataDepartmentKey.Warehouse;
				pos = BaseDataSetUpStaffJobKey.Employee;
			}

			Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
			ChooseUsersActivity.initParams(intent, true, dep, pos, ChooseUsersActivity.KEY_REC_SCANLABOR);
			startActivityForResult(intent, What_ChooseSupervisor);
			break;
		case R.id.btnAssign: // 分配
			reqAssign();
			break;
		
		case R.id.btnAddGoodPlts:	//添加好托盘
			showDlg_addPallet(true);
			break;
		case R.id.btnAddDmgPlts:	//添加-坏托盘
			showDlg_addPallet(false);
			break;

		default:
			break;
		}
	}

	private void reqAssign() {

		if (TextUtils.isEmpty(adid_notify)) {
			UIHelper.showToast(getString(R.string.sync_select_labor));
			return;
		}
		
		RequestParams p = new RequestParams();
		p.add("receipt_id", itemBean.receipt_id);
		p.add("receipt_line_id", itemBean.receipt_line_id);
		p.add("operator", adid_notify);
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finish();
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/createScanTaskToScaner", p, this);
//		assignScanTask
	}

	int palletsType;

	private List<Rec_PalletConfigBean> listConfigs = new ArrayList<Rec_PalletConfigBean>();
	
	private QtyDialog qtyDialog;

	private void showDlg_addPallet(boolean def_good) {
		int remain = itemBean.expected_qty - Rec_PalletConfigBean.getTotalQty(listConfigs);

//		if (remain == 0) {
//			RewriteBuilderDialog.showSimpleDialog_Tip(this, "No piece remain!");
//			return;
//		}

		LocBean defLoc=TmpDataUtil.getRec_def_loc(receipt_no, null);
		qtyDialog = new QtyDialog(mActivity, remain,def_good,receipt_no,defLoc,listTypes,true);
		qtyDialog.setOnSubmitQtyListener(new OnSubmitQtyListener() {
			public void onSubmit() {
				reqAddConfig(qtyDialog,1,1);
			}
		});
		qtyDialog.show();
	}
	
	/**
	 * 当期tab
	 * 
	 * @param isTab_goodPlts
	 */
	private void selGoodPltTab(boolean isTab_goodPlts) {
		vp.setCurrentItem(isTab_goodPlts ? 0 : 1);
	}

	/**
	 * 添加plts
	 * @param dlg
	 * @param check_location 1验证是否存在 location  0 直接添加 不验证
	 * @param clp_config  1 验证clp type是否存在                 0  直接添加 不验证
	 */
	private void reqAddConfig(final QtyDialog dlg,int check_location,final int clp_config) {
		Rec_ItemBean bean=itemBean;
		RequestParams p=dlg.getNetParam(itemBean.receipt_line_id, bean.item_id, bean.lot_no,detail_id, false, false,false,check_location);
		p.add("is_match_clp_config", clp_config+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				
				if(json.optInt("err") == 90 && json.optInt("is_check_location",-1) == 0){
					RewriteBuilderDialog.showSimpleDialog(mActivity, json.optString("data"), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							reqAddConfig(dlg,0,clp_config);
						}
					});
					
					return;
				}
				
				//clp type已存在
				if(json.optInt("err") == 90 && json.optInt("is_match_clp_config",-1) == 0){
					List<Rec_CLPType> clpTypes = new Gson().fromJson(json.optJSONArray("clpconfigrows").toString(), new TypeToken<List<Rec_CLPType>>(){}.getType());
					
					Rec_CLPType clpType = new Rec_CLPType();
					clpType.stack_height_qty = dlg.geth();
					clpType.stack_length_qty = dlg.getl();
					clpType.stack_width_qty = dlg.getw();
					clpType.lp_name = "TLP"+dlg.getl()+"*"+dlg.getw()+"*"+dlg.geth();
					clpType.type_name = dlg.getType();
					clpTypes.add(clpType);
					
					SelectClpType(dlg,clpTypes);
					return;
				}
				
				
				//创建-goodPlts
				if(!dlg.isCreateDmgPlts_mode()){
					JSONArray joConfig = json.optJSONArray("container_config");
					if (Utility.isEmpty(joConfig)) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
								"Invalid add return!");
						return;
					}

					List<Rec_PalletConfigBean> b = new Gson().fromJson(
							joConfig.toString(),
							new TypeToken<List<Rec_PalletConfigBean>>() {
							}.getType());

//					// 无配置,替换或添加
//					if (dlg.getAddMode() == QtyDialog.Mode_PltQty
//							&& b.size() == 1)
//						Rec_PalletConfigBean.repalceOrAdd_noConfig(listConfigs,
//								b.get(0));
//					// 有配置
//					else
//						listConfigs.addAll(0, b);
                    Rec_PalletConfigBean.repalceOrAdd_noConfig(listConfigs,b);
					adp.notifyDataSetChanged();
					selGoodPltTab(true);
				}
				//dmgPlts
				else
				{
					JSONArray ja = json.optJSONArray("damage_container");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpPlt = new Gson().fromJson(
							ja.toString(),
							new TypeToken<List<Rec_PalletBean>>() {
							}.getType());
					listDmg.addAll(0, tmpPlt);
					adpDmg.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_success));
					selGoodPltTab(false);
				}
				refSumUI();
				dlg.dismiss();
			}
		}.doGet(dlg.getReqUrl(), p, this);
	}
	private void reqAddConfig(final QtyDialog dlg,int check_location,final int clp_config,Rec_CLPType type) {
		Rec_ItemBean bean=itemBean;
		RequestParams p=dlg.getNetParam(itemBean.receipt_line_id, bean.item_id, bean.lot_no,detail_id, false, false,false,check_location);
		p.add("is_match_clp_config", clp_config+"");
		p.remove("pallet_type");
		p.add("pallet_type",""+type.type_id);
		p.add("license_plate_type_id", ""+type.lpt_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				
				//创建-goodPlts
				if(!dlg.isCreateDmgPlts_mode()){
					JSONArray joConfig = json.optJSONArray("container_config");
					if (Utility.isEmpty(joConfig)) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
								"Invalid add return!");
						return;
					}

					List<Rec_PalletConfigBean> b = new Gson().fromJson(
							joConfig.toString(),
							new TypeToken<List<Rec_PalletConfigBean>>() {
							}.getType());

                    Rec_PalletConfigBean.repalceOrAdd_noConfig(listConfigs,b);
					adp.notifyDataSetChanged();
					selGoodPltTab(true);
				}
				//dmgPlts
				else
				{
					JSONArray ja = json.optJSONArray("damage_container");
					if (Utility.isEmpty(ja))
						return;
					List<Rec_PalletBean> tmpPlt = new Gson().fromJson(
							ja.toString(),
							new TypeToken<List<Rec_PalletBean>>() {
							}.getType());
					listDmg.addAll(0, tmpPlt);
					adpDmg.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_success));
					selGoodPltTab(false);
				}
				refSumUI();
				dlg.dismiss();
			}
		}.doGet(dlg.getReqUrl(), p, this);
	}
	
	private void SelectClpType(final QtyDialog dlg,final List<Rec_CLPType> types){
		
		View layout = View.inflate(mActivity, R.layout.dialog_receive_clpconfig, null);
		ListView lv = (ListView) layout.findViewById(R.id.lvClpconfig);
		
		LayoutParams lp = lv.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (types.size() < 3) ? dm.heightPixels / 7 * types.size() : (dm.heightPixels / 3);
		lv.setLayoutParams(lp);
		
		adapter = new ClpConfigAdapter(types,onInnerClick_Select);
		lv.setAdapter(adapter);
		
		adapter.setCurItem(types.size()-1);
		currentClpType = types.size()-1;
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Select LP Type");
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.sync_submit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				if(types.size() -1 == currentClpType){
					reqAddConfig(dlg,0,0);
				}else{
					reqAddConfig(dlg,0,0,types.get(currentClpType));
				}
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	private void tipPrint() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_print_label), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});
	}

	private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs_pConfig = new PrintTool.OnPrintLs() {

		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintConfig(printServerID);
		}
	};

	private void reqPrintConfig(long printServerID) {

		final Rec_PalletConfigBean tmpConfig = config_toPrint;
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_ConfigIDs,  tmpConfig.container_config_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// debug
				tmpConfig.print_user_name = StoredData.getUsername();
				// tmpConfig.print_date=new
				// SimpleDateFormat("yyyy-mm-dd ").format(new
				// Date().toString());
				adp.notifyDataSetChanged();
				adpDmg.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, mActivity);
	}

	private Rec_PalletConfigBean config_toPrint; // 右键菜单-选中的bean

	private void showMenu_item(final int position) {
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));
        String[] strs=new String[] { "View Pallets", "Print Pallets", "Delete" };
		bd.setArrowItems(strs, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
				// TODO Auto-generated method stub
				final Rec_PalletConfigBean bean = listConfigs.get(position);
				switch (position_dlg) {
				case 0: // 查看pallets
					reqConfigPlts(position);
					break;
				case 1: // 打印
					if (bean.isPrinted()) {
						RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
						String msg = String.format("These Labels has been printed by %s. Reprint?", bean.print_user_name);
						builder.setMessage(msg);
						builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								config_toPrint = listConfigs.get(position);
								printTool.showDlg_printers(printLs_pConfig);
							}
						});
						builder.create().show();
					} else {
						config_toPrint = listConfigs.get(position);
						printTool.showDlg_printers(printLs_pConfig);
					}
					break;
				case 2: // 删除
					RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							reqDelConfig(position);
						}
					});

					break;

				default:
					break;
				}
			}
		});
		bd.show();

	}

	private void reqConfigPlts(int pos) {
		final Rec_PalletConfigBean b = listConfigs.get(pos);

		RequestParams p = new RequestParams();
		p.add("receipt_line_id", b.receipt_line_id);
		p.add("container_config_id", b.container_config_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				// Intent in = new Intent(mActivity,
				// Receive_ConfigDetailAc.class);
				// startActivity(in);
				JSONArray jaPlts = json.optJSONArray("pallets");
				if (jaPlts == null || jaPlts.length() == 0) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No Pallets!");
					return;
				}

				List<Rec_PalletBean> list = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
				}.getType());

				Intent in = new Intent(mActivity, Receive_ConfigDetailAc.class);
				Receive_ConfigDetailAc.initParams(in, list, itemBean.receipt_line_id, b.getQtyPerPlt(),true);
				startActivityForResult(in, Receive_ConfigDetailAc_Back);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletsInfoByLine", p, this);

	}

	private void reqDelConfig(final int pos) {
		Rec_PalletConfigBean b = listConfigs.get(pos);

		RequestParams p = new RequestParams();
		p.add("container_config_id", b.container_config_id + "");
		p.add("receipt_line_id", b.receipt_line_id + "");
		p.add("adid", StoredData.getAdid());
		p.add("isTask", "0"); // 是否为cc
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listConfigs.remove(pos);
				adp.notifyDataSetChanged();
				refSumUI();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/deletePalletConfig", p, this);

	}

	private void reqDelPlt(final Rec_PalletBean pltBean) {
		RequestParams p = new RequestParams();
		p.add("con_ids", pltBean.con_id);
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// 防止-异步出错
				// 坏的
				if (pltBean.isDamagePlt() && listDmg.contains(pltBean)) {
					listDmg.remove(pltBean);
					adpDmg.notifyDataSetChanged();
				}
				// 正常的
				else if (listDmg.contains(pltBean)) {
					listDmg.remove(pltBean);
					adpDmg.notifyDataSetChanged();
				}
				// refBtn_createCCTask();
				UIHelper.showToast(getString(R.string.sync_success));
				refSumUI();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/deletePallet", p, this);
	}

	private void refSumUI() {
		int remain = itemBean.expected_qty - Rec_PalletConfigBean.getTotalQty(listConfigs);
		if (remain >= 0) {
			tvRemain_title.setText("Item Remain: ");
			tvRemain.setText(remain + "");
		} else {
			tvRemain_title.setText("Item Overage: ");
			tvRemain.setText((-remain) + "");
		}
		
		//刷新plts
		int gootPltCnt=Rec_PalletConfigBean.getTotalPlts(listConfigs);
		int dmgPltCnt=listDmg.size();
		tvGoodPlts_cnt.setText(gootPltCnt+"");
		tvDmgPlts_cnt.setText(dmgPltCnt+"");
		tvGoodPlts_cnt.setVisibility(gootPltCnt>0?View.VISIBLE:View.INVISIBLE);
		tvDmgPlts_cnt.setVisibility(dmgPltCnt>0?View.VISIBLE:View.INVISIBLE);
	}

	// =========传参===============================

	private boolean isFromSupvisor;
	private String detail_id;
	private String receipt_no;

	/**
	 * @param in
	 * @param isFromSupvisor
	 *            true:来自supervisor,false:来自receive第一步的配置
	 */
	public static void initParams(Intent in, Rec_ItemBean itemBean, List<Rec_PalletConfigBean> listConfigs, List<Rec_PalletBean> listDmg,
			String detail_id,String receipt_no, boolean isFromSupvisor
			,List<Rec_PalletType> types) {
		in.putExtra("isFromSupvisor", isFromSupvisor);
		in.putExtra("listConfigs", (Serializable) listConfigs);
		in.putExtra("listDmg", (Serializable) listDmg);
		in.putExtra("itemBean", (Serializable) itemBean);
		in.putExtra("detail_id", detail_id);
		in.putExtra("receipt_no", receipt_no);
		in.putExtra("types", (Serializable)types);
	}

	private Rec_ItemBean itemBean;

	private List<Rec_PalletBean> listDmg;
	
	private List<Rec_PalletType> listTypes;

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		isFromSupvisor = params.getBoolean("isFromSupvisor", false);
		listConfigs = (List<Rec_PalletConfigBean>) params.getSerializable("listConfigs");
		listDmg = (List<Rec_PalletBean>) params.getSerializable("listDmg");
		itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
		detail_id = params.getString("detail_id");
		receipt_no=params.getString("receipt_no");
		listTypes = (List<Rec_PalletType>)params.getSerializable("types");
	}

	public static void toThis(final Activity ct, final Rec_ItemBean itemBean, final boolean isFromSupvisor, final String detail_id,
			final String receipt_id,final String receipt_no) {
		RequestParams p = new RequestParams();
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray ja = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
				}.getType());
				// dmgPlts
				JSONArray jaDmg = json.optJSONArray("damaged");
				List<Rec_PalletBean> listDmg = new ArrayList<Rec_PalletBean>();
				if (!Utility.isEmpty(jaDmg)) {
					listDmg = new Gson().fromJson(jaDmg.toString(), new TypeToken<List<Rec_PalletBean>>() {
					}.getType());
				}
				
				List<Rec_PalletType> types = new Gson().fromJson(json.optJSONArray("contypes").toString(), new TypeToken<List<Rec_PalletType>>(){}.getType());
				
				Intent in = new Intent(ct, Receive_CreatePalletsAc.class);
				itemBean.receipt_id = receipt_id;
				Receive_CreatePalletsAc.initParams(in, itemBean, list, listDmg, detail_id,receipt_no, isFromSupvisor ,types);
				ct.startActivity(in);

			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/findItemPalletConfig", p, ct);
	}

	// ==================nested===============================

	// ==================nested===============================

	private BaseAdapter adpDmg = new BaseAdapter() {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listDmg == null ? 0 : listDmg.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listDmg.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Dmg h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_plt_dam, null);
				h = new Holder_Dmg();
				h.tvPlt = (TextView) convertView.findViewById(R.id.tvPlt);
				h.tvDamage_it = (TextView) convertView.findViewById(R.id.tvDamage_it);
				h.imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);
				h.tvPrintMark = (TextView) convertView.findViewById(R.id.tvPrintMark);
				convertView.setTag(h);
			} else
				h = (Holder_Dmg) convertView.getTag();

			// 刷ui
			Rec_PalletBean b = listDmg.get(position);
			h.tvPlt.setText("Pallet:"+b.wms_container_id + "");
			h.tvDamage_it.setText("D: " + b.damage_qty);
			h.tvPrintMark.setVisibility(b.isPrinted() ? View.VISIBLE: View.GONE);
			return convertView;
		}
	};

	class Holder_Dmg {
		TextView tvPlt, tvDamage_it, tvPrintMark;
		ImageView imgArrow;
	}
	
	//---------clp config----------------------------------
	
	private OnInnerClickListener onInnerClick_Select = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			adapter.setCurItem(position);
			currentClpType = position;
		};
	};
	
	private class ClpConfigAdapter extends BaseAdapter{

		private List<Rec_CLPType> types;
		
		private OnInnerClickListener clickListener;

		public ClpConfigAdapter(List<Rec_CLPType> types,
				OnInnerClickListener clickListener) {
			super();
			this.types = types;
			this.clickListener = clickListener;
		}
		
		private int curItem = 0; // 当前-索引

		public void setCurItem(int curItem) {
			this.curItem = curItem;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return types == null ? 0 : types.size();
		}

		@Override
		public Object getItem(int position) {
			return types.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			
			ClpHolder holder;
			if(convertView == null){
				convertView = View.inflate(mActivity, R.layout.item_receive_clpconfig, null);
				holder = new ClpHolder();
				holder.llItem = (LinearLayout) convertView.findViewById(R.id.llItem);
				holder.tvClpName = (TextView) convertView.findViewById(R.id.tvClpName);
				holder.tvPallet = (TextView) convertView.findViewById(R.id.tvPallet);
				holder.tvShipTo = (TextView) convertView.findViewById(R.id.tvShipTo);
				holder.rbSelect = (RadioButton) convertView.findViewById(R.id.rbSelect);
			
				convertView.setTag(holder);
			}else{
				holder = (ClpHolder) convertView.getTag();
			}
			
			Rec_CLPType type = types.get(position);
			holder.tvClpName.setText(type.lp_name);
			holder.tvPallet.setText(type.type_name);
			holder.rbSelect.setChecked(curItem == position);
			
			if(TextUtils.isEmpty(type.ship_to_names)){
				holder.tvShipTo.setVisibility(View.GONE);
			}else{
				holder.tvShipTo.setVisibility(View.VISIBLE);
				holder.tvShipTo.setText(type.ship_to_names);
			}
			
			holder.llItem.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(clickListener != null)
						clickListener.onInnerClick(v, position);
				}
			});
			return convertView;
		}
		
	}
	
	private class ClpHolder{
		TextView tvClpName;
		TextView tvPallet;
		TextView tvShipTo;
		RadioButton rbSelect;
		LinearLayout llItem;
	}
}
