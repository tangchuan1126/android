package oso.ui.load_receive.do_task.receive.wms.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.photo.bean.TTPImgBean;
import utility.Utility;
import android.text.TextUtils;


public class Rec_PalletBean implements Serializable{

	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = 142823365982510258L;
	
	public static final int Process_Count_Create=0;     
	public static final int Process_CC_Create=1; 
	public static final int Process_Scan_Create=2; 
	
    public static final int ConType_CLP=1;              //container_type取其
    public static final int ConType_TLP=3;

    public static final int PltType_Good=0;				//plt类型,goods_type取其,好的、超出的、坏的
	public static final int PltType_Overage=1;
	public static final int PltType_Damage=2;
	
	public static final int PltStatus_Open=1;
	public static final int PltStatus_Closed=2;


	
	public String adid;
	public String con_id;						//plt号
//	public String container_no;					//带(94)00000,显示用
	public String wms_container_id;				//显示需用其
	public String config_type;
	public String container_config_id;
	public int container_type;
	
	public String create_date;
	public String employe_name;
	public int height;
	public String item_id;
	public int length;
	
	public String lot_no;
	public String note;
	public String pallet_type;
	public String type_name;
	public int qty_per_pallet;		//期望qty
	public int config_qty;			//配置qty
	
	public int normal_qty;			
	public int damage_qty;
	public int customer_con_id; //原来的Plt NO
//	public int stock_out_qty;
	
	public List<Receive_palletsSn> sn /*=new ArrayList<Receive_palletsSn>()*/;
	public String sn_count;				//已收的sn数
	public int width;
	
	public String receipt_line_id;
	public int cp_quantity;			//即count了的qty,已确认的数量(不一定都扫了sn)
//	public static   List<Rec_PalletBean> palletso;
	
	public String print_user_name = "";		//打印人、打印时间
	public String print_date;
	
	public int goods_type;
	public int is_repeat;				//有重的sn
	public String location = "";
    //debug
    public int location_type;       //位置类别,取LocBean.LocType_x,1:location、2:staging
    public int location_id;
	
	public List<TTPImgBean> photos;
	
	public int status=PltStatus_Open;			//状态,1:open 2:close
	
	//==================临时参数===============================

	public boolean isSelected=false;
	public boolean isLastPlt_inLine=false;
	public String container_no;
	public boolean isPrintSelected=false;
	
	//------------------------------------------
//	public int confirm_qty;
	public boolean isLocalScaned = false;           //本地已扫ss
	public int break_qty;
	public int cc_from_id;
	
	public String customer_id;
	
	public int process;
	public int keep_inbound;

    //============CLP================================

    public boolean isClp(){
        return container_type==ConType_CLP;
    }

    //=============================================
	
	public boolean isClosed(){
		return status==PltStatus_Closed;
	}

    public String getLoc_str(){
        return location_type==LocBean.LocType_Staging?"Staging":"Location";
    }

    public boolean isAtLocation(){
        //向上兼容,以前tmp没区分
        return location_type!=LocBean.LocType_Staging;
    }
	
	public boolean hasSn(String sn_no){
		if(TextUtils.isEmpty(sn_no))
			return false;
		if(sn==null)
			return false;
		for (int i = 0; i < sn.size(); i++) {
			if(sn_no.equals(sn.get(i).serial_number))
				return true;
		}
		return false;
	}
	
	public boolean is_repeat(){
		return is_repeat==1;
	}
	
	public boolean isEqual(Rec_PalletBean p){
		if(TextUtils.isEmpty(con_id) || p==null)
			return false;
		return con_id.equals(p.con_id);
	}
	
	/**
	 * true:坏托盘 
	 * @return
	 */
	public boolean isDamagePlt(){
		return goods_type==PltType_Damage;
	}
	
	/**
	 * 有配置
	 * @return
	 */
	public boolean hasConfig(){
		return config_qty>0;
	}
	
	public boolean hasPhoto(){
		return !Utility.isEmpty(photos);
	}
	
	public int getPhotoCnt(){
		return photos==null?0:photos.size();
	}
	
	/**
	 * 已count过
	 * @return
	 */
	public boolean isCnted(){
//		return normal_qty!=0||damage_qty!=0||stock_out_qty!=0;
		return normal_qty!=0||damage_qty!=0;
	}
	
	public int getCntQty(){
		return normal_qty+damage_qty;
	}
	
	/**
	 * 已扫描sn
	 * @return
	 */
	public boolean isScanedSn(){
//		if(isDamagePlt())
//			return getSN_cnt_ok()==damage_qty;
//		else
//			return getSN_cnt_ok()==normal_qty;
		
		return getSN_cnt_ok()>0;
	}
	
	/**
	 * 获取-已扫的sn
	 * @return
	 */
	public int getSN_cnt_ok(){
		return sn==null?0:sn.size();
	}
	
	
	public int getSnQty_needScan(){
        //clp
        if(process == Process_CC_Create)
            return length*width*height;
        //tlp
//		return isDamagePlt()?damage_qty:normal_qty;
        //debug,一般只会有一种
        return normal_qty+damage_qty;
	}
	
	public int getSn_dmg(){
		if(sn==null)
			return 0;
		int ret=0;
		for (int i = 0; i < sn.size(); i++) {
			if(sn.get(i).is_damage())
				ret++;
		}
		return ret;
	}
	
	public boolean hasDmgSn(){
		if(sn==null)
			return false;
		for (int i = 0; i < sn.size(); i++) {
			if(sn.get(i).is_damage())
				return true;
		}
		return false;
	}
	
	/**
	 * 已打印过
	 * @return
	 */
	public boolean isPrinted(){
		return !TextUtils.isEmpty(print_user_name);
	}
	
	/**
	 * 有问题,有坏的、数量不对
	 * @return
	 */
	public boolean isProblem(){
//		return damage_qty>0||stock_out_qty>0;
		//未cnt 不算
		if(!isCnted())
			return false;
		
		//若有配置,总数不对 或有坏的
		if(hasConfig())
			return getCntQty()!=config_qty||damage_qty>0;
		//无配置时,有坏的
		else
			return damage_qty>0;
	}
	
	/**
	 * cnt后才有值
	 * @return
	 */
	public int getShortage(){
		if(!isCnted() || isDamagePlt())
			return 0;
		return config_qty-normal_qty-damage_qty;
	}
	
	/**
	 * 获取-完整的plt号,94000001234567,注:无括号
	 * @param with94Brackets
	 * @return
	 */
	public String getFullPltNO_wms_94x(boolean with94Brackets){
		String pltID=wms_container_id;
		if(TextUtils.isEmpty(pltID))
			return "";
	
		int len=pltID.length();
		int zeroLen=12-len;
		StringBuffer sb = new StringBuffer("");
		for(int i=0;i<zeroLen;i++)
		   sb.append("0");
		if(!with94Brackets)
			return "94"+sb.toString()+pltID;
		else
			return "(94)"+sb.toString()+pltID;
	}
	
	/**
	 * 显示的pltNO
	 * @return
	 */
	public String getDisPltNO(){
//		return getFullPltNO_wms_94x(false);
		
		return wms_container_id; 
	}


    //===================breakPlt==========================

    /*
    * 会联动修改normalQty
     */
    public void setBreakQty(int pBreakQty){
       int total=normal_qty+break_qty;
       break_qty=pBreakQty;
       normal_qty=total-pBreakQty;

       //若有break,则待上传
       isLocalScaned=pBreakQty>0;
    }
	
	//==================统计===============================
	
	/**
	 * @param snNO
	 * @return
	 */
	public List<Receive_palletsSn> getSNs(String snNO){
		if(TextUtils.isEmpty(snNO))
			return null;
		
		List<Receive_palletsSn> listRet=new ArrayList<Receive_palletsSn>();
		for (int i = 0; i < sn.size(); i++) {
			Receive_palletsSn tmpSn=sn.get(i);
			if(snNO.equals(tmpSn.serial_number))
				listRet.add(tmpSn);
		}
		return listRet;
	}
	
	public Receive_palletsSn getFirstSN(String snNO){
		if(TextUtils.isEmpty(snNO))
			return null;
		
		List<Receive_palletsSn> listRet=new ArrayList<Receive_palletsSn>();
		for (int i = 0; i < sn.size(); i++) {
			Receive_palletsSn tmpSn=sn.get(i);
			if(snNO.equals(tmpSn.serial_number))
				return tmpSn;
		}
		return null;
	}
	
	/**
	 * 标识重复sn,注前提是该snNO之前已判断是重复了 此处不做判断
	 */
	public void markRepeatSN(String snNO){
		
		if(sn==null || snNO==null)
			return;
		
		for (int i = 0; i < sn.size(); i++) {
			Receive_palletsSn tmpSn=sn.get(i);
			if(snNO.equals(tmpSn.serial_number))
				tmpSn.is_repeat=1;
		}
	}
	
	//==================static===============================
	
	/**
	 * 除了该plt 其他是否都已扫完
	 * @param list
	 * @param plt
	 * @return
	 */
	public static boolean isLastPlt_inLine(List<Rec_PalletBean> list,Rec_PalletBean plt){
		if(Utility.isEmpty(list))
			return false;
		
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmpPlt=list.get(i);
			//未扫完 且非当前plt时,则false
			if(!tmpPlt.isScanedSn()&&!tmpPlt.isEqual(plt))
				return false;
		}
		return true;
	}
	
	/**
	 * count过的qty
	 * @param list
	 * @return
	 */
	public static int getTotalQty_cnted(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).cp_quantity;
		}
		return sum;
	}
	
	/**
	 * sn数量
	 * @param list
	 * @return
	 */
	public static int getTotalQty_scanedSN(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).getSN_cnt_ok();
		}
		return sum;
	}
	
	
	/**
	 * 已确定的qty
	 * @return
	 */
	public static int getTotalQty_ok(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).cp_quantity;
		}
		return sum;
	}
	
	/**
	 * 总的qty
	 * @return
	 */
	public static int getTotalQty_need(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).config_qty;
		}
		return sum;
	}
	
	/**
	 * 总的uncnt.qty,只算有配置的
	 * @return
	 */
	public static int getConfigQty_uncntPlt(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean b=list.get(i);
			if(!b.isCnted())
				sum+= b.config_qty;
		}
		return sum;
	}
	
	/**
	 * normal的qty
	 * @return
	 */
	public static int getTotalQty_Good(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).normal_qty;
		}
		return sum;
	}
	
	/**
	 * damage的qty
	 * @return
	 */
	public static int getTotalQty_Damage(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).damage_qty;
		}
		return sum;
	}
	
	/**
	 * 获取第一个有坏的plt
	 * @param list
	 * @return
	 */
	public static int getFirstDmgPlt(List<Rec_PalletBean> list){
		if(list==null)
			return -1;
		
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).damage_qty>0)
				return i;
		}
		return -1;
	}
	
	/**
	 * stockout的qty
	 * @return
	 */
	public static int getTotalQty_StockOut(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+= list.get(i).getShortage();
		}
		return sum;
	}
	
	/**
	 * 实收,含坏的
	 * @param list
	 * @return
	 */
	public static int getTotalQty_realRec(List<Rec_PalletBean> list){
		if(list==null)
			return 0;
		
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmp=list.get(i);
			sum= sum+tmp.normal_qty+tmp.damage_qty;
		}
		return sum;
	}
	
//	/**
//	 * 已cnt过的plt,配置值-总和
//	 * @param list
//	 * @return
//	 */
//	public static int getTotalQty_cntedPlt(List<Rec_PalletBean> list){
//		if(list==null)
//			return 0;
//		
//		int sum=0;
//		for (int i = 0; i < list.size(); i++) {
//			Rec_PalletBean tmp=list.get(i);
//			if(tmp.isCnted())
//				sum= sum+tmp.config_qty;
//		}
//		return sum;
//	}
	
	/**
	 * 都cnt过
	 * @param list
	 * @return
	 */
	public static boolean isAllCnted(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return true;
		
		for (int i = 0; i < list.size(); i++) {
			if(!list.get(i).isCnted())
				return false;
		}
		return true;
	}
	
	/**
	 * 都关闭了
	 * @param list
	 * @return
	 */
	public static boolean isAllClosed(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return true;
		
		for (int i = 0; i < list.size(); i++) {
			if(!list.get(i).isClosed())
				return false;
		}
		return true;
	}
	
	public static int getClosedPlts(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).isClosed())
				ret++;
		}
		return ret;
	}
	
	/**
	 * 最多用到的位置,用于"默认位置"
	 * @param list
	 * @return
	 */
	public static LocBean getMostLoc(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return null;

        final String delim="_ 1a";
		Map<String, Integer> mapLocCnt=new HashMap<String, Integer>();  //key取name+type
		//统计loc数
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmp=list.get(i);
			String loc=tmp.location;
			if(TextUtils.isEmpty(loc))
				continue;

            String keyLoc_Type=tmp.location+delim+tmp.location_type+delim+tmp.location_id;
			if(mapLocCnt.containsKey(keyLoc_Type))
			{
				int tmpCnt=mapLocCnt.get(keyLoc_Type);
				mapLocCnt.put(keyLoc_Type, tmpCnt+1);
			}
			else
				mapLocCnt.put(keyLoc_Type, 1);
		}
		
		Map.Entry<String, Integer> locCnt_m=null;
		for (Map.Entry<String, Integer> locCnt : mapLocCnt.entrySet()) {
			if(locCnt_m==null)
			{
				locCnt_m=locCnt;
				continue;
			}
			int lastMost=locCnt_m.getValue();
			int tmpCnt=locCnt.getValue();
			//保留-较大的
			if(tmpCnt>lastMost)
				locCnt_m=locCnt;
		}
        if(locCnt_m==null)
            return null;
        String[]  str=locCnt_m.getKey().split(delim);
		return new LocBean(str[0],Utility.parseInt(str[1]),Utility.parseInt(str[2]));
	}
	
	/**
	 * 获取第一个未打印的托盘
	 * @param list
	 * @return
	 */
	public static int getFirst_UnprintedPlt(List<Rec_PalletBean> list){
		if(list==null)
			return -1;
		
		for (int i = 0; i < list.size(); i++) {
			if(!list.get(i).isPrinted())
				return i; 
		}
		return -1;
	}
	
	/**
	 * @param list
	 * @return
	 */
	public static int getFirst_goodPlt_withDmgGoods(List<Rec_PalletBean> list){
		if(list==null)
			return -1;
		
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmpB=list.get(i);
			//好plt、 且有坏sn
			if(!tmpB.isDamagePlt()&&tmpB.hasDmgSn())
				return i;
		}
		return -1;
	}
	
	/**
	 * 获取-第一个空plt
	 * @param list
	 * @return
	 */
	public static int getFirst_emptyPlt(List<Rec_PalletBean> list){
		if(list==null)
			return -1;
		
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmpB=list.get(i);
			if(tmpB.getSN_cnt_ok()==0)
				return i;
		}
		return -1;
	}
	
	/**
	 * @param list
	 * @param pltID_scan 如:94xxxxx,无括号
	 * @return
	 */
	public static Rec_PalletBean getPallet_byScanNO(List<Rec_PalletBean> list,String pltID_scan){
		if(list==null || TextUtils.isEmpty(pltID_scan))
			return null;
		
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean b=list.get(i);
			if(pltID_scan.equals(b.wms_container_id) || pltID_scan.equals(b.getFullPltNO_wms_94x(false)))
				return list.get(i);
		}
		return null;
	}

	/**
	 * 是否都点过数,且数量不少
	 * @param list
	 * @return
	 */
	public static boolean isAllCounted_ok(List<Rec_PalletBean> list){
		if(list==null)
			return true;
		
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean pltBean=list.get(i);
			//若未cnt
			if(!pltBean.isCnted())
				return false;
		}
		return true;
	}
 
	/**
	 * 获取-选中的plts的ids,","分割
	 * @return
	 */
	public static String getPltIDs(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return "";
			
		String ret="";
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean b=list.get(i);
			if(b.isSelected)
				ret=ret+b.con_id+",";
		}
		
		if(!TextUtils.isEmpty(ret)){
			//去掉尾部的","
			ret=ret.substring(0, ret.length()-1);
		}
		
		return ret;
	}
	
	/**
	 * 获取-打印plts的人,且去重复
	 * @return
	 */
	public static String getPrintUsers(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return "";
			
		Set<String> set=new HashSet<String>();
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean b=list.get(i);
			set.add(b.print_user_name);
		}
		
		String users="";
		Iterator<String> it=set.iterator();
		while(it.hasNext()){
			String tmpUser=it.next();
			if(!TextUtils.isEmpty(tmpUser))
				users=users+tmpUser+", ";
		}
		if(!TextUtils.isEmpty(users))
			users=users.substring(0,users.length()-2);
		return users;
	}
	
	public static int getScanedPlts(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int cnt=0;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).isScanedSn())
				cnt++;
		}
		return cnt;
	}
	 
	/**
	 * 已扫描sn
	 * @param list
	 * @return
	 */
	public static int getScanedSn(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int cnt=0;
		for (int i = 0; i < list.size(); i++) {
			cnt+=list.get(i).getSN_cnt_ok();
		}
		return cnt;
	}
	
	public static int getScanedSn_good(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int cnt=0;
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmp=list.get(i);
			//好托盘-才算
			if(!tmp.isDamagePlt())
				cnt+=tmp.getSN_cnt_ok();
		}
		return cnt;
	}
	
	public static int getScanedSn_dmg(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int cnt=0;
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmp=list.get(i);
			//坏托盘-才算
			if(tmp.isDamagePlt())
				cnt+=tmp.getSN_cnt_ok();
		}
		return cnt;
	}
	
	
	/**
	 * 总sn
	 * @param list
	 * @return
	 */
	public static int getTotalSn_needScan(List<Rec_PalletBean> list){
		if(Utility.isEmpty(list))
			return 0;
		
		int cnt=0;
		for (int i = 0; i < list.size(); i++) {
			Rec_PalletBean tmpPlt=list.get(i);
			cnt+=tmpPlt.getSnQty_needScan();
		}
		return cnt;
	}
	
	/**
	 * 获取已有该sn的plts
	 * @param sn
	 * @param listFrom
	 * @return
	 */
	public static List<Rec_PalletBean> getPlts_hasSn(String sn,List<Rec_PalletBean> listFrom,Rec_PalletBean pltExcept){
		if(TextUtils.isEmpty(sn) || Utility.isEmpty(listFrom))
			return null;
		
		List<Rec_PalletBean> listRet=new ArrayList<Rec_PalletBean>();
		for (int i = 0; i < listFrom.size(); i++) {
			Rec_PalletBean b=listFrom.get(i);
			
			if(b.isEqual(pltExcept))
				continue;
			
			if(b.hasSn(sn))
				listRet.add(b);
		}
		return listRet;
	}
	
    //=========================cc===================================================
	
    public static int getTotalQty_break(List<Rec_PalletBean> list){
        if(Utility.isEmpty(list))
            return 0;
        int ret=0;
        for (int i = 0; i <list.size(); i++)
           ret+=list.get(i).break_qty;
        return ret;
    }
	
	
	
	
	
}
