package oso.ui.load_receive.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.AddTaskActivity.OnRefreshListener;
import oso.ui.load_receive.window.bean.ChangeTaskBean;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.InfoBean;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.EntryDetailNumberStateKey;
import support.key.ModuleKey;
import support.key.OccupyStatusTypeKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class TaskListActivity extends BaseActivity {

	private TextView liveloadTv, locationTv, tractorTv;
	private View header, lineTv, inSealLay, outSealLay;
	private TextView inSealTv, outSealTv;

	private ExpandableListView taskLv;
	private TaskAdapter adapter;

	private EntryBean mEntry;
	private EquipmentBean mEquipment;
	private List<List<WindowTaskBean>> datas;
	private boolean isOnlyTractor;

	private List<ResourceInfo> resourceList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_show_task, 0);
		initData();
		initView();
		setView();
	}

	private void initData() {
		mEntry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		mEquipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		datas = (List<List<WindowTaskBean>>) getIntent().getSerializableExtra("datas");
		isOnlyTractor = getIntent().getBooleanExtra("isOnlyTractor", false);
		resourceList = new ArrayList<DockHelpUtil.ResourceInfo>();
	}

	private void initView() {
		tractorTv = (TextView) findViewById(R.id.tractorTv);
		header = getLayoutInflater().from(mActivity).inflate(R.layout.vp_show_task_header, null);
		liveloadTv = (TextView) header.findViewById(R.id.liveloadTv);
		locationTv = (TextView) header.findViewById(R.id.locationTv);
		inSealLay = header.findViewById(R.id.inSealLay);
		outSealLay = header.findViewById(R.id.outSealLay);
		lineTv = header.findViewById(R.id.lineTv);
		inSealTv = (TextView) header.findViewById(R.id.inSealTv);
		outSealTv = (TextView) header.findViewById(R.id.outSealTv);
		taskLv = (ExpandableListView) findViewById(R.id.taskLv);
		taskLv.addHeaderView(header);
		taskLv.setGroupIndicator(null);
	}

	private void setView() {
		setTitleString("E" + mEntry.getEntry_id());
		tractorTv.setText(/*
						 * CheckInTractorOrTrailerTypeKey.getContainerTypeKeyValue
						 * (mEquipment.getEquipment_type()) + " : " +
						 */mEquipment.getEquipment_number());

		if (mEquipment.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			((ImageView) findViewById(R.id.equipment_type)).setImageResource(R.drawable.ic_s_tractor_write);
		}
		if (mEquipment.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRAILER) {
			((ImageView) findViewById(R.id.equipment_type)).setImageResource(R.drawable.ic_s_trailer_write);
		}

		// ------------live load 显示的内容
		if (mEquipment.getEquipment_purpose() == 0) {
			liveloadTv.setText("");
		} else {
			liveloadTv.setText(CheckInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(mActivity,mEquipment.getEquipment_purpose()));
		}

		if (mEquipment.getResources_type() == 0 || mEquipment.getResources_name() == null) {
			locationTv.setText("");
		} else {
			locationTv.setText(OccupyStatusTypeKey.getContainerTypeKeyValue(mEquipment.getOccupy_status(),mActivity) + " "
					+ OccupyTypeKey.getOccupyTypeKeyName(mActivity,mEquipment.getResources_type()) + " " + mEquipment.getResources_name());
		}
		inSealTv.setText(TextUtils.isEmpty(mEquipment.getSeal_delivery()) ? "NA" : mEquipment.getSeal_delivery());
		outSealTv.setText(TextUtils.isEmpty(mEquipment.getSeal_pick_up()) ? "NA" : mEquipment.getSeal_pick_up());
		if (mEquipment.getRel_type() == null) {
			inSealLay.setVisibility(View.VISIBLE);
			outSealLay.setVisibility(View.VISIBLE);
			lineTv.setVisibility(View.VISIBLE);
		} else {
			if (mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.DELIVERY + "")) {
				inSealLay.setVisibility(View.VISIBLE);
			} else if (mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.PICK_UP + "")) {
				lineTv.setVisibility(View.VISIBLE);
				outSealLay.setVisibility(View.VISIBLE);
			} else if (mEquipment.getRel_type().equals("0") || mEquipment.getRel_type().equals(CheckInMainDocumentsRelTypeKey.BOTH + "")) {
				inSealLay.setVisibility(View.VISIBLE);
				outSealLay.setVisibility(View.VISIBLE);
				lineTv.setVisibility(View.VISIBLE);
			}
		}

		adapter = new TaskAdapter(datas);
		taskLv.setAdapter(adapter);
		findViewById(R.id.nodataTv).setVisibility((datas == null || datas.isEmpty()) ? View.VISIBLE : View.GONE);
		// 全部展开
		for (int i = 0; i < adapter.getGroupCount(); i++) {
			taskLv.expandGroup(i);
		}
		// 禁止Group点击
		taskLv.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});

		taskLv.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				WindowTaskBean bean = (WindowTaskBean) adapter.getChild(groupPosition, childPosition);
				if (bean.number_status == 1 || bean.number_status == 2) {
					if (EquipmentListActivity.isEquipmentStatus(mActivity, mEquipment, "Cannot edit")) {
						// showEditTaskDialog(bean);
						reqModify(bean);
					}
				} else {
					UIHelper.showToast(mActivity, getString(R.string.sync_cannot_exit), Toast.LENGTH_SHORT).show();
				}
				return true;
			}
		});

		// 长按事件
		taskLv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// header不需处理,debug
				if (position == 0)
					return true;
				int groupPosition = (Integer) view.getTag(R.layout.item_task_list_g);
				// 参数值是在setTag时使用的对应资源id号
				int childPosition = (Integer) view.getTag(R.layout.item_task_list);
				if (childPosition != -1) {
					final WindowTaskBean bean = (WindowTaskBean) adapter.getChild(groupPosition, childPosition);
					if (bean.number_status == 1 || bean.number_status == 2) {
						if (EquipmentListActivity.isEquipmentStatus(mActivity, mEquipment, "Cannot edit")) {
							RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
							builder.setMessage(getString(R.string.sync_delete_notify));
							builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									reqDelete(bean);
								}
							});
							builder.create().show();
						}
					} else {
						UIHelper.showToast(mActivity, getString(R.string.sync_cannot_exit), Toast.LENGTH_SHORT).show();
					}
				}
				return true;
			}
		});
		showRightButton(R.drawable.add_button_style2, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				toAddTasks();
			}
		});
		AddTaskActivity.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				refreshData();
			}
		});
	}

	private void toAddTasks() {
		if (!EquipmentListActivity.isEquipmentStatus(mActivity, mEquipment, "Cannot Add Task")) {
			return;
		}
		if (!isOnlyTractor && !isTrailer()) {
			UIHelper.showToast(mActivity, getString(R.string.window_tractor_noaddtask), Toast.LENGTH_SHORT).show();
			return;
		}
		Intent intent = new Intent(mActivity, AddTaskActivity.class);
		intent.putExtra("ResourceInfo", (Serializable) new ResourceInfo());
		intent.putExtra("entryBean", mEntry);
		intent.putExtra("equipmentBean", mEquipment);
		intent.putExtra("LastCloseUserTask", getLastCloseUserTask());
		startActivity(intent);
	}

	// private void showEditTaskDialog(final WindowTaskBean bean) {
	// RewriteBuilderDialog.Builder builder = new
	// RewriteBuilderDialog.Builder(mActivity);
	// builder.setTitle("Operation");
	// builder.setArrowItems(new String[] { "Modify", "Delete" }, new
	// OnItemClickListener() {
	// @Override
	// public void onItemClick(AdapterView<?> parent, View view, int position,
	// long id) {
	// if (position == 0) { // Modify, 修改设备
	// reqModify(bean);
	// }
	// // if (position == 1) { // Change Tractor/Tralier/CTNR
	// // reqChangeTTC(bean);
	// // }
	// // if (position == 2) { // Change Resources
	// // selectDoorOrSpotDialog(bean, false);
	// // }
	// // if (position == 3) { // Delete
	// if (position == 1) { // Delete
	// RewriteBuilderDialog.Builder builder = new
	// RewriteBuilderDialog.Builder(mActivity);
	// builder.setMessage(getString(R.string.sync_delete_notify));
	// builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// reqDelete(bean);
	// }
	// });
	// builder.create().show();
	// }
	// }
	// });
	// builder.create().show();
	// }

	private void reqModify(final WindowTaskBean bean) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.TaskInfosByTaskId);
		params.add("detail_id", bean.detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("reqModify= " + json.toString());
				Gson gson = new Gson();
				ChangeTaskBean bean = gson.fromJson(json.toString(), ChangeTaskBean.class);
				InfoBean info = gson.fromJson(json.toString(), InfoBean.class);

				// Intent intent = new Intent(mActivity,
				// TaskDetailActivity.class);
				Intent intent = new Intent(mActivity, EditTaskActivity.class);
				intent.putExtra("entryBean", mEntry);
				intent.putExtra("ChangeTaskBean", bean);
				intent.putExtra("equipmentBean", mEquipment);
				intent.putExtra("dlo_detail_id", bean.dlo_detail_id);
				intent.putExtra("mInfo", info);
				startActivity(intent);
			}
		}.doGet(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

//	private void reqChangeTTC(final WindowTaskBean bean) {
//		RequestParams params = new RequestParams();
//		params.add("Method", HttpPostMethod.WindowCheckInEntryEquipmentList);
//		params.add("entry_id", mEntry.getEntry_id());
//		Goable.initGoable(mActivity);
//		StringUtil.setLoginInfoParams(params);
//		new SimpleJSONUtil() {
//			@Override
//			public void handReponseJson(JSONObject json) {
//				System.out.println("WindowCheckInEntryEquipmentList= " + json.toString());
//				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "datas");
//				Gson gson = new Gson();
//				List<EquipmentBean> mEquipmentList = gson.fromJson(ja.toString(), new TypeToken<List<EquipmentBean>>() {
//				}.getType());
//				Intent intent = new Intent(mActivity, ChangeTTCActivity.class);
//				intent.putExtra("mEntry", mEntry);
//				intent.putExtra("mEquipmentList", (Serializable) mEquipmentList);
//				intent.putExtra("WindowTaskBean", bean);
//				startActivity(intent);
//			}
//		}.doGet(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
//	}

	private void reqDelete(final WindowTaskBean bean) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.DeleteTask);
		params.add("detail_id", bean.detail_id);
		params.add("entry_id", mEntry.getEntry_id());
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("reqDelete= " + json.toString());
				refreshData();
			}
		}.doGet(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	// private void changeDoor(final WindowTaskBean bean, final String entry_id,
	// final int type) {
	// RequestParams params = new RequestParams();
	// params.add("Method", HttpPostMethod.GetDoorAndSpot);
	// params.add("request_type", "2"); // 1:可用res 2:所有
	// params.add("occupy_type", type == 0 ? OccupyTypeKey.DOOR + "" :
	// OccupyTypeKey.SPOT + "");
	// params.add("entry_id", entry_id);
	// new SimpleJSONUtil() {
	// @Override
	// public void handReponseJson(JSONObject json) {
	// if (!Utility.isNullForList(resourceList)) {
	// resourceList.clear();
	// }
	// resourceList = DockHelpUtil.handjson_big(json);
	// Intent intent = new Intent(mActivity, ChangeDoorActivity.class);
	// intent.putExtra("entryBean", mEntry);
	// intent.putExtra("equipmentBean", mEquipment);
	// intent.putExtra("doorlist", (Serializable) resourceList);
	// intent.putExtra("WindowTaskBean", bean);
	// intent.putExtra("type", type);
	// startActivity(intent);
	// }
	// }.doGet(HttpUrlPath.AndroidDockCheckInAction, params, mActivity);
	// }

	private boolean isTrailer() {
		return mEquipment.getEquipment_type() != CheckInTractorOrTrailerTypeKey.TRACTOR;
	}

	public void editOnClick(View v) {
		if (!EquipmentListActivity.isEquipmentStatus(mActivity, mEquipment, "Cannot edit")) {
			return;
		}
		Intent intent = new Intent(mActivity, UpdateEquipmentInfoActivity.class);
		intent.putExtra("entryBean", mEntry);
		intent.putExtra("equipmentBean", mEquipment);
		intent.putExtra("doorlist", (Serializable) resourceList);
		startActivity(intent);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		refreshData();
	}

	private void refreshData() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInGetEquipmentInfos);
		params.add("equipment_id", mEquipment.getEquipment_id() + "");
		params.add("entry_id", mEquipment.getCheck_in_entry_id() + ""); // zhangrui
																		// 修改
																		// fresh的时候应该要传递EntryＩＤ
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInGetEquipmentInfos= " + json.toString());
				JSONObject entryJo = StringUtil.getJsonObjectFromJSONObject(json, "entry");
				JSONObject equipmentJo = StringUtil.getJsonObjectFromJSONObject(json, "equipment");
				JSONArray tasksJa = StringUtil.getJsonArrayFromJson(json, "tasks");
				Gson gson = new Gson();
				mEntry = gson.fromJson(entryJo.toString(), EntryBean.class);
				mEquipment = gson.fromJson(equipmentJo.toString(), EquipmentBean.class);
				datas = WindowTaskBean.parseDatas(tasksJa);
				setView();
				if (onRefreshDataListener != null)
					onRefreshDataListener.onRefresh(mEquipment, getTotalTask());
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);

	}

	private int getTotalTask() {
		int total = 0;
		for (List<WindowTaskBean> tasks : datas) {
			total += tasks.size();
		}
		return total;
	}

	private WindowTaskBean getLastCloseUserTask() {
		WindowTaskBean task = null;
		if (datas != null && !datas.isEmpty()) {
			List<WindowTaskBean> tasks = datas.get(datas.size() - 1);
			if (tasks != null && !tasks.isEmpty()) {
				task = tasks.get(tasks.size() - 1);
			}
		}
		return task;
	}

	public static OnRefreshDataListener getOnRefreshDataListener() {
		return onRefreshDataListener;
	}

	public static void setOnRefreshDataListener(OnRefreshDataListener onRefreshDataListener) {
		TaskListActivity.onRefreshDataListener = onRefreshDataListener;
	}

	private class TaskAdapter extends BaseExpandableListAdapter {

		private List<List<WindowTaskBean>> datas;

		public TaskAdapter(List<List<WindowTaskBean>> datas) {
			this.datas = datas;
		}

		@Override
		public int getGroupCount() {
			return (datas == null || datas.isEmpty()) ? 1 : datas.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return (datas == null || datas.isEmpty() || datas.get(groupPosition) == null) ? 0 : datas.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return datas.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return datas.get(groupPosition).get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if (datas == null || datas.isEmpty()) {
				convertView = getLayoutInflater().inflate(R.layout.item_task_list_null, null);
				return convertView;
			}
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_task_list_g, null);
				holder = new Holder();
				holder.doorTv = (TextView) convertView.findViewById(R.id.doorTv);
				holder.addIv = (ImageView) convertView.findViewById(R.id.addIv);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			convertView.setTag(R.layout.item_task_list_g, groupPosition);
			convertView.setTag(R.layout.item_task_list, -1);

			final List<WindowTaskBean> tasks = datas.get(groupPosition);
			final WindowTaskBean bean = datas.get(groupPosition).get(0);

			if (bean.resources_type != 0) {
				holder.doorTv.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,bean.resources_type) + ": " + bean.resources_type_value);
				holder.doorTv.setTextColor(Color.WHITE);
				// 在该资源下添加任务
				holder.addIv.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!EquipmentListActivity.isEquipmentStatus(mActivity, mEquipment, "Cannot Add Task")) {
							return;
						}
						if (!isOnlyTractor && !isTrailer()) {
							UIHelper.showToast(mActivity, getString(R.string.window_tractor_noaddtask), Toast.LENGTH_SHORT).show();
							return;
						}
						ResourceInfo selectDoorInfo = new ResourceInfo();
						selectDoorInfo.resource_id = bean.resources_id;
						selectDoorInfo.resource_name = bean.resources_type_value;
						selectDoorInfo.resource_type = bean.resources_type;
						selectDoorInfo.area_id = bean.area_id;

						Intent intent = new Intent(mActivity, AddTaskActivity.class);
						intent.putExtra("ResourceInfo", (Serializable) selectDoorInfo);
						intent.putExtra("entryBean", mEntry);
						intent.putExtra("equipmentBean", mEquipment);
						intent.putExtra("LastCloseUserTask", (tasks == null || tasks.isEmpty()) ? null : tasks.get(tasks.size() - 1));
						startActivity(intent);
					}
				});
			} else {
				holder.doorTv.setText("NA");
				holder.doorTv.setTextColor(Color.RED);
				holder.addIv.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
					}
				});
			}
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_task_list, null);
				holder = new Holder();
				holder.main = convertView.findViewById(R.id.main);
				holder.loadTv = (TextView) convertView.findViewById(R.id.loadTv);
				holder.stateTv = (TextView) convertView.findViewById(R.id.stateTv);
				holder.customer_id = (TextView) convertView.findViewById(R.id.customer_id);
				holder.company_id = (TextView) convertView.findViewById(R.id.company_id);
				holder.arrawIv = (ImageView) convertView.findViewById(R.id.arrawIv);
				holder.titleTv = (TextView) convertView.findViewById(R.id.titleTv);
				holder.ccLay = (LinearLayout) convertView.findViewById(R.id.ccLay);
				holder.titleLay = (LinearLayout) convertView.findViewById(R.id.titleLay);
				holder.notifyTv = (TextView) convertView.findViewById(R.id.notifyTv);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			convertView.setTag(R.layout.item_task_list_g, groupPosition);
			convertView.setTag(R.layout.item_task_list, childPosition);

			final WindowTaskBean bean = (WindowTaskBean) getChild(groupPosition, childPosition);

			Spannable spLoad = Utility.getCompoundText(ModuleKey.getCheckInModuleKey(bean.number_type) + " ", bean.number, new StyleSpan(
					Typeface.NORMAL));
			// 单据类型，单据号
			holder.loadTv.setText(spLoad);
			// 单据状态
			holder.stateTv.setText(EntryDetailNumberStateKey.getType(mActivity,bean.number_status));
			if (EntryDetailNumberStateKey.isClose(bean.number_status)) {
				holder.stateTv.setTextColor(Color.RED);
			} else {
				holder.stateTv.setTextColor(mActivity.getResources().getColor(R.color.font_bg3));
			}

			if (bean.number_type.equals(ModuleKey.CHECK_IN_LOAD + "") || bean.number_type.equals(ModuleKey.CHECK_IN_PONO + "")
					|| bean.number_type.equals(ModuleKey.CHECK_IN_PICKUP_ORTHERS + "") || bean.number_type.equals(ModuleKey.CHECK_IN_ORDER + "")) {
				holder.ccLay.setVisibility(View.VISIBLE);
				holder.titleLay.setVisibility(View.GONE);
			} else {
				holder.titleLay.setVisibility(View.VISIBLE);
				holder.ccLay.setVisibility(View.GONE);
			}
			holder.customer_id.setText(bean.customer_id);
			holder.company_id.setText(bean.company_id);
			holder.titleTv.setText(bean.title);

			// 通知人, 最多显示两人
			if (!TextUtils.isEmpty(bean.close_user)) {
				String[] users = bean.close_user.split(",");
				if (users.length > 1) {
					holder.notifyTv.setText(String.format("%s,%s...", users[0], users[1]));
				} else {
					holder.notifyTv.setText(bean.close_user);
				}
			} else {
				holder.notifyTv.setText("");
			}

			// 任务状态
			if (bean.number_status == CheckInMainDocumentsStatusTypeKey.PROCESSING
					|| bean.number_status == CheckInMainDocumentsStatusTypeKey.UNPROCESS || bean.number_status == CheckInLiveLoadOrDropOffKey.PICK_UP) {
				holder.arrawIv.setVisibility(View.VISIBLE);
			} else {
				holder.arrawIv.setVisibility(View.INVISIBLE);
			}

			// 背景
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				holder.main.setBackgroundResource(R.drawable.list_select_bottom_style);
			} else {
				holder.main.setBackgroundResource(R.drawable.list_select_center_style);
			}
			return convertView;
		}

		private class Holder {
			View main;
			TextView doorTv, loadTv, stateTv, customer_id, company_id, notifyTv;
			TextView titleTv;
			LinearLayout ccLay, titleLay;
			ImageView addIv, arrawIv;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}
	}

	private static OnRefreshDataListener onRefreshDataListener;

	public interface OnRefreshDataListener {
		void onRefresh(EquipmentBean e, int totalTask);
	}
}
