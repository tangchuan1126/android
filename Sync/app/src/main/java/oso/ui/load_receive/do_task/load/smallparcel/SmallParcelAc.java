package oso.ui.load_receive.do_task.load.smallparcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.load.smallparcel.adapter.AdpSP_SelOrder;
import oso.ui.load_receive.do_task.load.smallparcel.bean.CarrierBean;
import oso.ui.load_receive.do_task.load.smallparcel.bean.SP_OrderBean;
import oso.ui.load_receive.do_task.load.smallparcel.bean.SP_TNOrPltBean;
import oso.ui.load_receive.do_task.load.smallparcel.util.SParcel_NoSheetPage;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskFinishActivity;
import oso.widget.dialog.AdpDlgSingleSel;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.AppConfig;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.common.tts.TTS;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 小包裹-处理
 * 
 * @author 朱成
 * @date 2015-5-5
 */
public class SmallParcelAc extends BaseActivity implements OnClickListener {

	private ExpandableListView exLv;
	private SearchEditText searchEt;
	private TextView tvCarrier;

	private TextView tvResType, tvDock, tvOrdersScaned, tvTotalOrders,
			tvTNScaned, tvTotalTNs;
	// tvPltScaned, tvTotalPlt;

	private SParcel_NoSheetPage noSheetPage;

	private View loSP_noOrder, loSP_hasOrder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_smallparcel, 0);
		applyParams();
		setTitleString("Small Parcel: " + taskBean.number);

		// nosheet
		noSheetPage = new SParcel_NoSheetPage(this, vMiddle);

		// v
		loSP_noOrder = findViewById(R.id.loSP_noOrder);
		loSP_hasOrder = findViewById(R.id.loSP_hasOrder);

		exLv = (ExpandableListView) findViewById(R.id.lvOrders);
		searchEt = (SearchEditText) findViewById(R.id.searchEt);
		searchEt.setScanMode();
		tvCarrier = (TextView) findViewById(R.id.tvCarrier);
		tvResType = (TextView) findViewById(R.id.tvResType);
		tvDock = (TextView) findViewById(R.id.tvDock);
		tvOrdersScaned = (TextView) findViewById(R.id.tvOrdersScaned);
		tvTotalOrders = (TextView) findViewById(R.id.tvTotalOrders);
		tvTNScaned = (TextView) findViewById(R.id.tvTNScaned);
		tvTotalTNs = (TextView) findViewById(R.id.tvTotalTNs);
		// tvPltScaned = (TextView) findViewById(R.id.tvPltScaned);
		// tvTotalPlt = (TextView) findViewById(R.id.tvTotalPlt);
		btnAddOrDel = (ImageButton) findViewById(R.id.btnAddOrDel);

		// 事件
		showRightButton(R.drawable.menu_btn_style, "", this);
		btnAddOrDel.setOnClickListener(this);
		searchEt.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				// 调试模式
				if (AppConfig.isDebug && TextUtils.isEmpty(value))
					value = getNextNO_toScan();

				doScan(value);
				searchEt.setText("");
				Utility.colseInputMethod(mActivity, searchEt);
			}
		});

		// lv
		exLv.setEmptyView(findViewById(R.id.tvEmpty));
		exLv.setGroupIndicator(null);
		exLv.setAdapter(adapter);
		showDlg_filterOrders();

		// 初始化
		tvResType.setText(doorBean.getResTypeStr(mActivity) + ": ");
		tvDock.setText(doorBean.resources_name);
		refSumUI();

		showPage(true, null);
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		// RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// // TODO Auto-generated method stub
		// SmallParcelAc.super.onBackBtnOrKey();
		// }
		// });

		// RewriteBuilderDialog.Builder bd=RewriteBuilderDialog.newSimpleDialog(
		// this, "Close Task?", new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// // TODO Auto-generated method stub
		// reqLoad_scaned(true);
		// }
		// });

//		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(
//				mActivity);
//		bd.setTitle(getString(R.string.sync_notice));
//		bd.setMessage("Close Task?");
//		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				// 有order时 不能部分扫描
//				if (isMode_hasOrders()) {
//					SP_OrderBean par = SP_OrderBean
//							.getOrder_PartiallyScaned(listOrders);
//					// 有部分扫描
//					if (par != null) {
//						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
//								"Order " + par.order_numbers
//										+ " is partially loaded!");
//						return;
//					}
//				}
//				reqCloseTask();
//			}
//		});
//		bd.setMidButton("NO", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				reqLoad_scaned();
//			}
//		});
//		bd.setNegativeButton("Cancel", null);
//		bd.show();

		// s================================================================

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setArrowItems(new String[] { "Normal Close", "Back" },
				new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						switch (position) {
						case 0:			//close task
							// 有order时 不能部分扫描
							if (isMode_hasOrders()) {
								SP_OrderBean par = SP_OrderBean
										.getOrder_PartiallyScaned(listOrders);
								// 有部分扫描
								if (par != null) {
									RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
											"Order " + par.order_numbers
													+ " is partially loaded!");
									return;
								}
							}
							reqCloseTask();
							break;
						case 1:			//back
							reqLoad_scaned();
							break;

						default:
							break;
						}
					}
				});
		builder.create().show();
	}

	/**
	 * 当前为有order模式
	 * 
	 * @return
	 */
	private boolean isMode_hasOrders() {
		return loSP_hasOrder.getVisibility() == View.VISIBLE;
	}

	private void showPage(boolean hasOrder, List<SP_TNOrPltBean> listNO) {
		// 有order
		if (hasOrder) {
			loSP_hasOrder.setVisibility(View.VISIBLE);
			loSP_noOrder.setVisibility(View.GONE);

			showRightButton(R.drawable.menu_btn_style, "", this);
		}
		// 无order
		else {
			loSP_hasOrder.setVisibility(View.GONE);
			loSP_noOrder.setVisibility(View.VISIBLE);

			noSheetPage.startPage(listNO);
		}
	}

	private String getNextNO_toScan() {
		for (int i = 0; i < listOrders.size(); i++) {
			SP_OrderBean od = listOrders.get(i);
			List<SP_TNOrPltBean> tns = od.pallets;
			if (tns == null)
				continue;
			for (int j = 0; j < tns.size(); j++) {
				SP_TNOrPltBean tn = tns.get(j);
				if (!tn.isScanned())
					return tn.wms_pallet_number;
			}
		}
		return "";
	}

	private void doScan(String value) {
		// 校验
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Invalid Number!", true);
			return;
		}

		List<Point> pt = SP_OrderBean.getBean_byNO(listOrders, value);
		if (Utility.isEmpty(pt)) {
			TTS.getInstance().speakAll_withToast("Invalid Number!", true);
			return;
		}

		// load
		if (isAdd()) {
			// 已load
			if (SP_OrderBean.isAllScaned_no(listOrders, pt)) {
				TTS.getInstance().speakAll_withToast("Repeat!", true);
				return;
			}
			if (pt.size() == 1)
				// check_LoadType(pt.get(0));
				reqLoad(pt.get(0));
			else
				showDlg_selOrder(pt, true);
		} else {
			// 均未load,debug
			if (SP_OrderBean.isAllNotScaned_no(listOrders, pt)) {
				TTS.getInstance().speakAll_withToast("Not Loaded!", true);
				return;
			}
			// showDlg_confirmDel(pt);

			if (pt.size() == 1)
				showDlg_confirmDel(pt.get(0));
			else
				showDlg_selOrder(pt, false);
		}
	}

	/**
	 * 若有变换-扫描类型(同1order下),则提示
	 * 
	 * @param pt
	 */
	// private void check_LoadType(final Point pt) {
	//
	// SP_OrderBean order = listOrders.get(pt.x);
	// SP_TNOrPltBean no = order.getNOBean(pt.y);
	// boolean isChangeType = order.isChange_loadType(pt.y);
	// // 未变类型=============================
	// if (!isChangeType) {
	// reqLoad(pt);
	// return;
	// }
	//
	// // 变了类型==============================
	// String str_before = no.isTN() ? "Pallet #" : "Tracking #";
	// String str_now = no.isTN() ? "Tracking #" : "Pallet #";
	//
	// TTS.getInstance().speakAll(
	// "Scan type changed" + TTS.comma() + "please confirm!");
	// RewriteBuilderDialog.showSimpleDialog(this, "You have scanned "
	// + str_before + " before, now " + str_now + ", continue?",
	// new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// reqLoad(pt);
	// }
	// });
	//
	// }

	private void showDlg_selOrder(final List<Point> listTN, final boolean toLoad) {
		TTS.getInstance().speakAll("Select Order!");

		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("Select Order");
		// bd.setArrowItems(getOrderStrs(listTN), new
		// AdapterView.OnItemClickListener() {
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// // TODO Auto-generated method stub
		// reqLoad(listTN.get(position));
		// }
		// });
		// bd.show();

		SP_TNOrPltBean no = SP_OrderBean.getNOBean(listOrders, listTN.get(0));
		// String strPre = no.isTN() ? "Tracking #: " : "Pallet #: ";
		String strPre = "Tracking #: ";
		bd.setMessage(strPre + no.wms_pallet_number);

		AdpSP_SelOrder adp = new AdpSP_SelOrder(this, listTN, listOrders);
		bd.setAdapter(adp, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Point ptSel = listTN.get(position);
				boolean hasLoaded = SP_OrderBean.getNOBean(listOrders, ptSel)
						.isScanned();
				// load
				if (toLoad) {
					// 若已load
					if (hasLoaded) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
								"Already loaded!");
						return;
					}
					// check_LoadType(ptSel);
					reqLoad(ptSel);
				}
				// unload
				else {
					// 若未load
					if (!hasLoaded) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
								"Not loaded!");
						return;
					}
					showDlg_confirmDel(ptSel);
				}
			}
		});
		bd.show();

	}

	private String[] getOrderStrs(List<Point> listTN) {
		String[] str = new String[listTN.size()];
		for (int i = 0; i < listTN.size(); i++) {
			Point no = listTN.get(i);
			str[i] = listOrders.get(no.x).order_numbers;
		}
		return str;
	}

	/**
	 * @return ok的TN
	 */
	private int refSumUI() {
		int scaned_orders = SP_OrderBean.getOrders_Scaned(listOrders);
		tvOrdersScaned.setText(scaned_orders + "");
		tvTotalOrders.setText(listOrders.size() + "");

		Point scaned_TNs = SP_OrderBean.getTNs_Scaned(listOrders);
		tvTNScaned.setText(scaned_TNs.x + "");
		tvTotalTNs.setText(scaned_TNs.y + "");

		// Point scaned_plts = SP_OrderBean.getPlts_Scaned(listOrders);
		// tvPltScaned.setText(scaned_plts.x + "");
		// tvTotalPlt.setText(scaned_plts.y + "");
		return scaned_TNs.x;
	}

	// ==================removePallet===============================

	private ImageButton btnAddOrDel;
	private RewriteBuilderDialog dlg_comfirmDel;

	private void setAddMode(boolean isAdd) {
		btnAddOrDel.setSelected(!isAdd);
	}

	/**
	 * 添加或移除托盘
	 * 
	 * @return
	 */
	private boolean isAdd() {
		return !btnAddOrDel.isSelected();
	}

	private void showDlg_confirmDel(final Point pt) {
		TTS.getInstance().speakAll("Confirm Remove!");

		// view
		View vContent = getLayoutInflater().inflate(
				R.layout.dlg_smallparcel_comfirmdel, null);
		TextView tvPlt_title = (TextView) vContent
				.findViewById(R.id.tvPlt_title);
		TextView tvPallet_dlg = (TextView) vContent
				.findViewById(R.id.tvPallet_dlg);
		final SearchEditText etPallet_dlg = (SearchEditText) vContent
				.findViewById(R.id.etPallet_dlg);

		// 初始化
		SP_OrderBean order = listOrders.get(pt.x);
		SP_TNOrPltBean noBean = order.getNOBean(pt.y);
		// tvPlt_title.setText(noBean.isTN() ? "Tacking #: " : "Pallet #: ");
		tvPlt_title.setText("Tacking #: ");
		tvPallet_dlg.setText(noBean.wms_pallet_number);
		// etPallet_dlg.setHint("Please Scan the "
		// + (noBean.isTN() ? "Tacking #" : "Pallet #"));
		etPallet_dlg.setHint("Please Scan the Tacking #");

		// 事件
		etPallet_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				onComfirmDel(pt, value);
				etPallet_dlg.setText("");
			}
		});

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("Scan Again To Remove");
		bd.setContentView(vContent);
		bd.setPositiveButtonOnClickDismiss(false);
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				onComfirmDel(pt, etPallet_dlg.getEditableText().toString());
				etPallet_dlg.setText("");
			}
		});
		dlg_comfirmDel = bd.show();
	}

	private void onComfirmDel(Point pt, String palletNo_confirm) {
		if (TextUtils.isEmpty(palletNo_confirm)) {
			TTS.getInstance().speakAll_withToast("Empty", true);
			return;
		}
		SP_TNOrPltBean noBean = listOrders.get(pt.x).getNOBean(pt.y);
		String origin_no = noBean.wms_pallet_number;
		// 不匹配
		if (!palletNo_confirm.equals(origin_no)) {
			TTS.getInstance().speakAll_withToast("Not Match!", true);
			return;
		}
		dlg_comfirmDel.dismiss();

		// 本地扫的,直接移除
		if (noBean.isScaned_local)
			removeBean(pt);
		// server端扫的
		else
			reqRemovePallet(pt);
	}

	private void reqRemovePallet(final Point pt) {

		SP_OrderBean order = listOrders.get(pt.x);
		SP_TNOrPltBean noBean = order.getNOBean(pt.y);

		RequestParams p = new RequestParams();
		p.add("Method", "removeScanState");
		p.add("wms_scan_number_type", noBean.wms_scan_number_type + "");
		p.add("wms_order_type_id", noBean.wms_order_type_id);
		p.add("detail_id", taskBean.dlo_detail_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub

				// 卸载
				// SP_OrderBean.unload(listOrders, pt);
				// adapter.notifyDataSetChanged();
				// // 收齐
				// Utility.expandAll(exLv, false);
				// exLv.setSelection(0);
				//
				// int scaned_orders = refSumUI();
				// UIHelper.showToast(mActivity, "Success!");
				// TTS.getInstance().speakAll(
				// "Removed" + TTS.comma() + "Order " + scaned_orders);
				removeBean(pt);
			}

			public void handFail() {
				TTS.getInstance().speakAll("Fail!");
			};
		}.doPost(HttpUrlPath.androidSmallParcel, p, this);
	}

	private void removeBean(Point pt) {
		// 卸载
		SP_OrderBean.unload(listOrders, pt);
		adapter.notifyDataSetChanged();
		// 收齐
		Utility.expandAll(exLv, false);
		exLv.setSelection(0);

		int scaned_tns = refSumUI();
		UIHelper.showToast(mActivity, getString(R.string.sync_success));
		TTS.getInstance().speakAll(
				"Removed" + TTS.comma() + "TN " + scaned_tns);
	}

	// =================================================

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAddOrDel: // 增减切换
			// debug
			// showDlg_confirmDel(null);

			setAddMode(!isAdd());
			UIHelper.showToast(isAdd() ? getString(R.string.tms_r_addmodel) : getString(R.string.tms_r_removemodel));
			break;
		case R.id.btnRight:
			showDlg_menu();
			break;

		default:
			break;
		}
	}

	public CarrierBean selCarrier;
	private List<SP_OrderBean> listOrders = new ArrayList<SP_OrderBean>();

	private void reqOrders(final CarrierBean cr, final BottomDialog dlg) {
		RequestParams p = new RequestParams();
		p.add("method", "filterSmallParcel");
		p.add("carrier_id", cr.carrier_id);
		p.add("detail_id", taskBean.dlo_detail_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub

				JSONArray ja = json.optJSONArray("data");
				boolean hasOrder = json.optInt("is_order") == 1;

				// debug
				// if(AppConfig.isDebug)
				// hasOrder=true;

				// 无order============================================
				if (!hasOrder) {
					final List<SP_TNOrPltBean> listNO = new Gson().fromJson(
							ja.toString(),
							new TypeToken<List<SP_TNOrPltBean>>() {
							}.getType());
					RewriteBuilderDialog.Builder bd = RewriteBuilderDialog
							.newSimpleDialog(mActivity,
									"No orders found, continue load?",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
											// 当前-carrier
											selCarrier = cr;
											showPage(false, listNO);
										}
									});
					bd.setCancelable(false);
					bd.setNegativeButton(R.string.sync_no,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									// 初始时 取消直接退出
									if (!hasSelCarrier())
										SmallParcelAc.super.finish();
								}
							});
					bd.show();

					// 防止-窗体泄露
					dlg.dismiss();
					return;
				}

				// 有order=============================================
				showPage(true, null);
				List<SP_OrderBean> list = new Gson().fromJson(ja.toString(),
						new TypeToken<List<SP_OrderBean>>() {
						}.getType());

				// debug
				// if (AppConfig.isDebug) {
				// selCarrier = cr;
				// list = getMockOrders();
				// }

				// 排序
				SP_OrderBean.sort(list);
				listOrders.clear();
				listOrders.addAll(list);
				// adapter.notifyDataSetChanged();
				Utility.resetExLv(exLv, adapter);

				refSumUI();

				// 当前-carrier
				selCarrier = cr;
				tvCarrier.setText(selCarrier.carrier_name);

				dlg.dismiss();
			}
		}.doGet(HttpUrlPath.androidSmallParcel, p, this);
	}

	private boolean isUPS_debug() {
		if (selCarrier == null)
			return true;
		return "1".equals(selCarrier.carrier_id);
	}

	private List<SP_OrderBean> getMockOrders() {

		List<SP_OrderBean> listOrders = new ArrayList<SP_OrderBean>();
		int len_order = isUPS_debug() ? 9 : 4;
		for (int i = 0; i < len_order; i++) {

			SP_OrderBean order = new SP_OrderBean();
			order.order_numbers = "order_" + i;
			order.reference_no = "ref_" + i;
			order.customer_id = "VISIO";

			order.pallets = new ArrayList<SP_TNOrPltBean>();

			int len_tn = i == 0 ? 1 : 5;
			for (int j = 0; j < len_tn; j++) {
				SP_TNOrPltBean no = new SP_TNOrPltBean();
				no.wms_pallet_number = "plt_" + j;
				// no.wms_scan_number_type = (j == 0 || j == 1 || j == 2) ?
				// SP_TNOrPltBean.Type_TN
				// : SP_TNOrPltBean.Type_Plt;
				if (!(i == 0 || i == 1))
					no.scan_adid = "12345";
				order.pallets.add(no);
			}
			listOrders.add(order);
		}
		return listOrders;
	}

	public void reqCloseTask() {

		String entry_id = doorBean.entry_id;
		RequestParams params = NetInterface.closeTask_p(entry_id,
				taskBean.dlo_detail_id, ScanLoadActivity.CloseType_Normal,
				doorBean.equipment_id, doorBean.resources_type,
				doorBean.resources_id, null, true);

		String nos = getNOs_localScaned();
		params.add("scan_numer", nos);
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// ttp.clearData();
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT)
						.show();
				int returnflag = json.optInt("returnflag");
				// 若为门下最后1个(1/2/4跳,3不跳),直接至finish页
				if (returnflag != CheckInTaskFinishActivity.LoadCloseNoifyNormal) {
					Intent in = new Intent(mActivity,
							CheckInTaskFinishActivity.class);
					CheckInTaskFinishActivity.initParams(in, doorBean);
					mActivity.startActivity(in);
				}
				// 若为normalClose,至"任务列表"
				else
					Utility.popTo(mActivity, CheckInTaskDoorItemActivity.class);

				mActivity.finish();
			}
		}.setCancelable(false).doPost(HttpUrlPath.AndroidTaskProcessingAction,
				params, mActivity);
	}

	public void showDlg_filterOrders() {
		// final View v =
		// getLayoutInflater().inflate(R.layout.dlg_smallparcel_filter, null);

		final ListView lv = (ListView) getLayoutInflater().inflate(
				R.layout.dlg_smallparcel_filter, null);
		// lv
		// String[] crs = new String[] { "UPS", "FEDEX" };
		String[] crs = CarrierBean.getCarrierNames(listCarriers);
		int defIndex = listCarriers.indexOf(selCarrier);
//		defIndex < 0 ? 0 : defIndex
		final AdpDlgSingleSel mAdapter = new AdpDlgSingleSel(this, crs,
				defIndex, true);
		mAdapter.isStyle_rightRButton = true;
		lv.setAdapter(mAdapter);
		// 高度
		int heightLimitCount = 6;
		int cnt_visible = crs.length > heightLimitCount ? heightLimitCount
				: crs.length;
		int height = Utility.pxTodip(this, 40);
		// dividerHeight
		LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.height = cnt_visible * height;
		lv.setLayoutParams(params);

		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setTitle("Filter Small Parcel Orders");
		dialog.setView(lv);

		// 没选carrier
		if (!hasSelCarrier()) {
			dialog.getDlg().setOnKeyListener(
					new DialogInterface.OnKeyListener() {

						@Override
						public boolean onKey(DialogInterface dialog,
								int keyCode, KeyEvent event) {
							// TODO Auto-generated method stub
							// 返回时,直接退出
							if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
									&& event.getAction() == KeyEvent.ACTION_UP) {
								// 销毁窗体,否则会"WindowLeaked"
								dialog.dismiss();
								SmallParcelAc.super.finish();
								return true;
							}
							return false;
						}
					});
			dialog.setShowCancelBtn(false);
		}

		dialog.setOnSubmitClickListener(getString(R.string.patrol_confirm),
				new BottomDialog.OnSubmitClickListener() {

					@Override
					public void onSubmitClick(BottomDialog dlg, String value) {
						// TODO Auto-generated method stub
						int index=mAdapter.curIndex;
						if(index<0){
							UIHelper.showToast(getString(R.string.sync_select_carrier));
						}else{
							CarrierBean cr = listCarriers.get(mAdapter.curIndex);
							reqOrders(cr, dialog);
						}
						
					}
				});
		// 空白不可点
		dialog.setCanceledOnTouchOutside(false);

		dialog.show();
	}

	private boolean hasSelCarrier() {
		return selCarrier != null;
	}

	private void reqReload(final boolean is_keep_order) {
		RequestParams p = new RequestParams();
		p.add("method", "reloadOrder");
		p.add("detail_id", taskBean.dlo_detail_id);
		p.add("is_keep_order", is_keep_order ? "1" : "0");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray ja = json.optJSONArray("data");
				List<SP_OrderBean> list = new Gson().fromJson(ja.toString(),
						new TypeToken<List<SP_OrderBean>>() {
						}.getType());

				// 保持-本地扫描过的
				if (is_keep_order)
					SP_OrderBean.keepScaned(list, listOrders);

				// 排序
				SP_OrderBean.sort(list);
				listOrders.clear();
				listOrders.addAll(list);
				// adapter.notifyDataSetChanged();
				Utility.resetExLv(exLv, adapter);
				refSumUI();
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(HttpUrlPath.androidSmallParcel, p, this);
	}

	private void reqLoadAll() {
		// 若已全load
		if (SP_OrderBean.getOrders_Scaned(listOrders) == listOrders.size()) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this,
					"Already all loaded!");
			return;
		}

		RequestParams p = new RequestParams();
		p.add("method", "scanAllPalletOrTackNO");
		p.add("detail_id", taskBean.dlo_detail_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				SP_OrderBean.loadAll(listOrders);
				adapter.notifyDataSetChanged();
				refSumUI();

				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doGet(HttpUrlPath.androidSmallParcel, p, this);
	}

	private void showDlg_menu() {
		final String[] list = new String[] { "Reload All",
				"Refresh Orders(Keep Scaned Orders)",
				"Filter Small Parcel Orders", "Load All", "Normal Close" };
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));
		bd.setArrowItems(list, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				switch (position) {
				case 0: // reload
					RewriteBuilderDialog.showSimpleDialog(mActivity,
							list[position] + "?",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									reqReload(false);
								}
							});
					break;
				case 1: // refresh
					RewriteBuilderDialog.showSimpleDialog(mActivity,
							list[position] + "?",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									reqReload(true);
								}
							});
					break;
				case 2: // filter
					showDlg_filterOrders();
					break;
				case 3: // load All
					RewriteBuilderDialog.showSimpleDialog(mActivity,
							list[position] + "?",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									reqLoadAll();
								}
							});
					break;
				case 4: // normal Close
					SP_OrderBean par = SP_OrderBean
							.getOrder_PartiallyScaned(listOrders);
					// 有部分扫描
					if (par != null) {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
								"Order " + par.order_numbers
										+ " is partially loaded!");
						return;
					}

					RewriteBuilderDialog.showSimpleDialog(mActivity,
							list[position] + "?",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									reqCloseTask();
								}
							});
					break;

				default:
					break;
				}
			}
		});
		bd.show();
	}

	private void reqLoad(final Point pt) {

		// final SP_OrderBean order = listOrders.get(pt.x);
		// final SP_TNOrPltBean tnBean = order.pallets.get(pt.y);
		// RequestParams p = new RequestParams();
		// p.add("method", "scanPalletOrTrackNo");
		// p.add("detail_id", taskBean.dlo_detail_id);
		// p.add("scan_numer", tnBean.wms_pallet_number);
		// p.add("wms_scan_number_type", tnBean.wms_scan_number_type + "");
		// // 扫plt时才需
		// p.add("wms_order_id", order.wms_order_id);
		// new SimpleJSONUtil() {
		//
		// @Override
		// public void handReponseJson(JSONObject json) {
		// // TODO Auto-generated method stub
		// SP_OrderBean.load(listOrders, pt);
		// adapter.notifyDataSetChanged();
		// // 收起 至第一个
		// Utility.expandAll(exLv, false);
		// exLv.setSelection(0);
		//
		// int scaned_orders = refSumUI();
		//
		// TTS.getInstance().speakAll("order " + scaned_orders);
		// }
		//
		// public void handFail() {
		// TTS.getInstance().speakAll("Fail!");
		// };
		// }
		// // .setCancelable(false)
		// .doGet(HttpUrlPath.androidSmallParcel, p, this);

		final SP_OrderBean order = listOrders.get(pt.x);
		final SP_TNOrPltBean tnBean = order.pallets.get(pt.y);
		SP_OrderBean.load(listOrders, pt);
		adapter.notifyDataSetChanged();
		// 收起 至第一个
		Utility.expandAll(exLv, false);
		exLv.setSelection(0);

		int scaned_tns = refSumUI();

		TTS.getInstance().speakAll("TN " + scaned_tns);
	}

	private String getNOs_localScaned() {
		if (isMode_hasOrders())
			return SP_OrderBean.getNOs_localScaned(listOrders);
		else
			return noSheetPage.getNOs_localScaned();
	}

	private void reqLoad_scaned() {

		String no = getNOs_localScaned();
		// 若无提交 直接退出
		if (TextUtils.isEmpty(no) || "[]".equals(no)) {
			finish();
			return;
		}

		// final SP_OrderBean order = listOrders.get(pt.x);
		// final SP_TNOrPltBean tnBean = order.pallets.get(pt.y);
		RequestParams p = new RequestParams();
		p.add("method", "scanPalletOrTrackNo");
		p.add("detail_id", taskBean.dlo_detail_id);
		p.add("scan_numer", no);
		// p.add("wms_scan_number_type", tnBean.wms_scan_number_type + "");
		// // 扫plt时才需
		// p.add("wms_order_id", order.wms_order_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finish();
			}
		}.setCancelable(false).doPost(HttpUrlPath.androidSmallParcel, p, this);
	}

	// =========传参===============================
	private List<CarrierBean> listCarriers;
	public CheckInTaskBeanMain doorBean;
	public CheckInTaskItemBeanMain taskBean;

	public static void initParams(Intent in, List<CarrierBean> listCarriers,
			CheckInTaskBeanMain doorBean, CheckInTaskItemBeanMain taskBean) {
		in.putExtra("listCarriers", (Serializable) listCarriers);
		in.putExtra("doorBean", (Serializable) doorBean);
		in.putExtra("taskBean", (Serializable) taskBean);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		listCarriers = (List<CarrierBean>) params
				.getSerializable("listCarriers");
		doorBean = (CheckInTaskBeanMain) params.getSerializable("doorBean");
		taskBean = (CheckInTaskItemBeanMain) params.getSerializable("taskBean");
	}

	// ==================nested===============================

	private final ExAdapter adapter = new ExAdapter();

	private final class ExAdapter extends BaseExpandableListAdapter {

		// @Override
		// public boolean areAllItemsEnabled() {
		// // TODO Auto-generated method stub
		// return super.areAllItemsEnabled();
		// }

		@Override
		public int getGroupCount() {
			return listOrders == null ? 0 : listOrders.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			List<SP_TNOrPltBean> listTNs = listOrders.get(groupPosition).pallets;
			return listTNs == null ? 0 : listTNs.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return null;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return 0;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {

			Holder gHolder = null;
			if (convertView == null) {
				convertView = View.inflate(mActivity,
						R.layout.item_small_parcel_group, null);
				gHolder = new Holder();
				gHolder.item = convertView.findViewById(R.id.item);
				gHolder.orderTv = (TextView) convertView
						.findViewById(R.id.orderTv);
				gHolder.customerTv = (TextView) convertView
						.findViewById(R.id.customerTv);
				gHolder.rnTv = (TextView) convertView.findViewById(R.id.rnTv);
				gHolder.tvShipData = (TextView) convertView
						.findViewById(R.id.tvShipData);
				convertView.setTag(gHolder);
			} else
				gHolder = (Holder) convertView.getTag();

			// 刷ui
			SP_OrderBean order = listOrders.get(groupPosition);
			gHolder.orderTv.setText(order.order_numbers);
			gHolder.customerTv.setText(order.customer_id);
			gHolder.rnTv.setText(order.reference_no);
			gHolder.tvShipData.setText(order.shipped_date);

			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || order.getNOCnt() == 0;
			gHolder.item
					.setBackgroundResource(showOne ? R.drawable.sh_load_lv_one
							: R.drawable.sh_load_lv_top);
			// debug
			gHolder.item.setSelected(order.isScanned());

			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			C_Holder cHolder = null;
			if (convertView == null) {
				convertView = View.inflate(mActivity,
						R.layout.item_small_parcel_child, null);
				cHolder = new C_Holder();
				cHolder.item = convertView.findViewById(R.id.item);
				cHolder.tvTN_title = (TextView) convertView
						.findViewById(R.id.tvTN_title);
				cHolder.tvTN_value = (TextView) convertView
						.findViewById(R.id.tvTN_value);
				cHolder.imgTN_tn = (ImageView) convertView
						.findViewById(R.id.imgTN_tn);
				cHolder.imgTN_plt = (ImageView) convertView
						.findViewById(R.id.imgTN_plt);
				cHolder.vTopLine = convertView.findViewById(R.id.vTopLine);
				convertView.setTag(cHolder);
			} else
				cHolder = (C_Holder) convertView.getTag();

			// 刷ui
			SP_OrderBean order = listOrders.get(groupPosition);
			SP_TNOrPltBean plt = order.pallets.get(childPosition);
			// cHolder.tvTN_title.setText(plt.isTN() ? "Tracking #: "
			// : "Pallet #: ");
			// 图标
			// cHolder.imgTN_tn.setVisibility(plt.isTN() ? View.VISIBLE
			// : View.GONE);
			// cHolder.imgTN_plt.setVisibility(plt.isTN() ? View.GONE
			// : View.VISIBLE);

			cHolder.tvTN_value.setText(plt.wms_pallet_number);

			// 背景
			boolean isBottom = childPosition == order.getNOCnt() - 1;
			int bg;
			if (isBottom)
				bg = plt.isScanned() ? R.drawable.sh_load_lv_bot_sel
						: R.drawable.sh_load_lv_bot_def;
			else
				bg = plt.isScanned() ? R.drawable.sh_load_lv_mid_sel
						: R.drawable.sh_load_lv_mid_def;
			cHolder.item.setBackgroundResource(bg);

			// boolean showLine = false;
			// // 1.为第一项 或2.当前为plt 且上一个为TN
			// if (childPosition == 0)
			// showLine = true;
			// else {
			// // 当前为plt 且上一个为TN
			// if (!plt.isTN() && order.getNOBean(childPosition - 1).isTN())
			// showLine = true;
			// }
			// cHolder.vTopLine.setVisibility(showLine ? View.VISIBLE :
			// View.GONE);
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return false;
		}

		final class Holder {
			View item;
			TextView orderTv, customerTv, rnTv, tvShipData;
		}

		final class C_Holder {
			View item, vTopLine;
			ImageView imgTN_tn, imgTN_plt;
			TextView tvTN_title, tvTN_value;
		}
	}

}
