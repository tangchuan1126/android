package oso.ui.load_receive.print.bean;

import java.io.Serializable;
 
public class SelectCtnBolBase implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	private String companyid; // "W12",
	private String bolno; // "125745687",
	private String customerid; // "VIZIO"
	private String containerno;// UMXU234726

	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getBolno() {
		return bolno;
	}
	public void setBolno(String bolno) {
		this.bolno = bolno;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getContainerno() {
		return containerno;
	}
	public void setContainerno(String containerno) {
		this.containerno = containerno;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	 
	
}
