package oso.ui.load_receive.do_task.load.samsung;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.load.samsung.bean.SamsungLoad;
import oso.widget.BadgeView;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.bean.CheckInLoadLoadBean;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SamsungLoadActivity extends BaseActivity {

	private LayoutInflater inflater;
	private SingleSelectBar singleSelectBar;
	private TabToPhoto ttp;
	private LinearLayout loadLayout, loadedLayout;
	private LinearLayout boxLay, weightLay;
	private ListView loadLv, loadedLv;
	private LoadAdapter loadAdapter, loadedAdapter;
	private SearchEditText etPallet;
	private TextView palletTv, palletTotalTv, qtyTv, qtyTotalTv, weightTv, weightTotalTv;
	private Button exceptionBtn, finishBtn;
	private BadgeView badge;

	CheckInLoadLoadBean loadBean;
	List<SamsungLoad> mDatas, loadDatas, loadedDatas;
	String mEntryID;

	private boolean isDebug = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_load_samsung, 0);
		initView();
		initData();
		initSingleSelectBar();
		setData();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ttp.onResume(getTabParamList());
	}

	private void initData() {
		inflater = LayoutInflater.from(mActivity);
		loadBean = (CheckInLoadLoadBean) getIntent().getSerializableExtra("loadBean");
		mEntryID = getIntent().getStringExtra("entryID");
		setTitleString("Samsung Load");
		//
		mDatas = new ArrayList<SamsungLoad>();
		loadDatas = new ArrayList<SamsungLoad>();
		loadedDatas = new ArrayList<SamsungLoad>();
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key0 = TTPKey.getSamsungLoadKey(loadBean.dlo_detail_id, 0);
		params.add(new TabParam("Load", key0, new PhotoCheckable(0, "Load", ttp)));
		params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing+"", FileWithTypeKey.OCCUPANCY_MAIN+"", mEntryID);
		return params;
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Need Load", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("Loaded", 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					loadLayout.setVisibility(View.VISIBLE);
					loadedLayout.setVisibility(View.GONE);
					loadLayout.setAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_top_in));
					break;
				case 1:
					loadLayout.setVisibility(View.GONE);
					loadedLayout.setVisibility(View.VISIBLE);
					loadedLayout.setAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.push_top_in));
					break;
				}
				ActivityUtils.hideSoftInput(mActivity);
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
		View receiveTab = singleSelectBar.getTabView(2);
		badge = new BadgeView(this, receiveTab);
	}

	/**
	 * SelectBar上的红色徽章
	 * 
	 * @param value
	 */
	private void getTabNum() {
		badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
		badge.setText(loadedDatas.size() + "");
		badge.show();
	}

	private void setData() {
		ttp.init(mActivity, getTabParamList());
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				loadServerData();
			}
		}, 300);
	}

	private void loadServerData() {
		RequestParams params = new RequestParams();
		params.add("Method", "GetHasLoadPallet");
		params.add("ic_id", loadBean.ic_id);
		params.add("dlo_detail_id", loadBean.dlo_detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("Samsung= " + json.toString());
				JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "datas");
				Gson gson = new Gson();
				mDatas = gson.fromJson(dataArray.toString(), new TypeToken<List<SamsungLoad>>() {
				}.getType());

				// 计算Box qty
				int boxTotal = StringUtil.getJsonInt(json, "totalbox");
				if (boxTotal != 0)
					qtyTotalTv.setText(boxTotal + "");
				else
					boxLay.setVisibility(View.GONE);

				// 计算weight
				double weightTotal = 0.00;
				for (SamsungLoad load : mDatas) {
					if (load.getShip_entryd_id() != null && !load.getShip_entryd_id().equals("0")) {
						loadedDatas.add(load);
					} else {
						loadDatas.add(load);
					}

					weightTotal += Double.parseDouble(load.getWeight());
				}

				if (weightTotal != 0.00)
					weightTotalTv.setText(String.format("%.2f", weightTotal));
				else
					weightLay.setVisibility(View.GONE);

				setLoadData();
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	private void initView() {
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		loadLayout = (LinearLayout) findViewById(R.id.loadLayout);
		loadedLayout = (LinearLayout) findViewById(R.id.loadedLayout);
		boxLay = (LinearLayout) findViewById(R.id.boxLay);
		weightLay = (LinearLayout) findViewById(R.id.weightLay);
		qtyTv = (TextView) findViewById(R.id.qtyTv);
		qtyTotalTv = (TextView) findViewById(R.id.qtyTotalTv);
		weightTv = (TextView) findViewById(R.id.weightTv);
		weightTotalTv = (TextView) findViewById(R.id.weightTotalTv);
		loadLv = (ListView) findViewById(R.id.loadLv);
		loadedLv = (ListView) findViewById(R.id.loadedLv);
		etPallet = (SearchEditText) findViewById(R.id.etPallet);
		palletTv = (TextView) findViewById(R.id.palletTv);
		palletTotalTv = (TextView) findViewById(R.id.palletTotalTv);
		exceptionBtn = (Button) findViewById(R.id.exceptionBtn);
		finishBtn = (Button) findViewById(R.id.finishBtn);
		btnRight.setVisibility(View.VISIBLE);
		loadLv.setEmptyView(findViewById(R.id.nodataTv));
		loadedLv.setEmptyView(findViewById(R.id.nodata2Tv));
		btnRight.setBackgroundResource(R.drawable.menu_btn_style);
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("ready");
	}

	private void setLoadData() {
		loadAdapter = new LoadAdapter(loadDatas);
		loadLv.setAdapter(loadAdapter);

		loadedAdapter = new LoadAdapter(loadedDatas);
		loadedLv.setAdapter(loadedAdapter);

		setTotal();

		etPallet.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				if (isDebug)
					value = getNextPallet_toScan();
				search(value);
			}
		});
		etPallet.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
					String value = etPallet.getText().toString().trim();
					if (isDebug)
						value = getNextPallet_toScan();
					search(value);
					return true;
				}
				return false;
			}
		});
		btnRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDatas == null || mDatas.isEmpty()) {
					TTS.getInstance().speakAll_withToast("Invalid Pallet!");
					return;
				}
				AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
				dialog.setItems(new String[] { "ReLoad" }, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {
							RewriteBuilderDialog.Builder d = new RewriteBuilderDialog.Builder(mActivity);
							d.setTitle(getString(R.string.sync_notice));
							d.setMessage("ReLoad?");
							d.isHideCancelBtn(false);
							d.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									reLoad();
								}
							});
							d.create().show();
						}
					}
				});
				dialog.show();

			}

		});
	}

	private void setTotal() {
		getTabNum();
		palletTv.setText(loadedDatas.size() + "");
		palletTv.setTextColor(Color.RED);
		palletTotalTv.setText(mDatas.size() + "");

		// 计算Box QTY
		int boxStr = 0;
		for (int i = 0; i < loadedDatas.size(); i++) {
			SamsungLoad load = loadedDatas.get(i);
			boxStr += Integer.parseInt(load.getBox_number());
		}
		qtyTv.setText(boxStr + "");

		// 计算Weight
		double weight = 0.00;
		for (int i = 0; i < loadedDatas.size(); i++) {
			SamsungLoad load = loadedDatas.get(i);
			weight += Double.parseDouble(load.getWeight());
		}
		weightTv.setText(String.format("%.2f", weight));

		if (loadedDatas.size() >= mDatas.size()) {
			finishBtn.setVisibility(View.VISIBLE);
			exceptionBtn.setVisibility(View.GONE);
			palletTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
			palletTotalTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
			qtyTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
			qtyTotalTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
			weightTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
			weightTotalTv.setTextColor(getResources().getColor(R.color.checkin_patrol_green));
		} else {
			finishBtn.setVisibility(View.GONE);
			exceptionBtn.setVisibility(View.VISIBLE);
			palletTv.setTextColor(Color.RED);
			palletTotalTv.setTextColor(getResources().getColor(R.color.checkin_blue));
			qtyTv.setTextColor(Color.RED);
			qtyTotalTv.setTextColor(getResources().getColor(R.color.checkin_blue));
			weightTv.setTextColor(Color.RED);
			weightTotalTv.setTextColor(getResources().getColor(R.color.checkin_blue));
		}

	}

	private void search(final String value) {
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Value Is Empty");
			return;
		}
		// 错误数据
		if (isErrorData(value)) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet!");
			etPallet.setText("");
			Utility.colseInputMethod(mActivity, etPallet);
			return;
		}
		// 重复数据
		if (isRepeat(value)) {
			TTS.getInstance().speakAll_withToast("Pallet Repeat!");
			etPallet.setText("");
			Utility.colseInputMethod(mActivity, etPallet);
			return;
		}

		// 数据处理
		RequestParams params = new RequestParams();
		params.add("Method", "LoadPallet");
		params.add("icp_id", getSamsungLoad(value).getIcp_id());
		params.add("dlo_detail_id", loadBean.dlo_detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				loadSuccess(value, true);
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll("Scan Failed!");
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);

		etPallet.setText(isDebug ? "debug" : "");
		Utility.colseInputMethod(mActivity, etPallet);
	}

	private void loadSuccess(String value, boolean isRead) {
		// 界面处理
		for (SamsungLoad load : loadDatas) {
			if (load.getPlate_no().equals(value)) {
				loadedDatas.add(load);
				loadDatas.remove(load);
				if (isRead) {
					if (value.length() > 4) {
						TTS.getInstance().speakAll(StringUtil.addSpace(value.substring(value.length() - 4, value.length())));
						if (!isNA(load.getBox_number()))
							TTS.getInstance().speakAll("Box " + load.getBox_number());
					} else {
						TTS.getInstance().speakAll(StringUtil.addSpace(value));
						if (!isNA(load.getBox_number()))
							TTS.getInstance().speakAll("Box " + load.getBox_number());
					}
				}
				break;
			}
		}
		loadAdapter.notifyDataSetChanged();
		loadedAdapter.notifyDataSetChanged();
		setTotal();
	}

	private boolean isNA(String str) {
		return str == null || str.length() == 0 || str.equals("NA") || str.equals("0");
	}

	// debug
	private String getNextPallet_toScan() {
		for (int i = 0; i < loadDatas.size(); i++) {
			SamsungLoad b = loadDatas.get(i);
			if (b == null)
				continue;

			return b.getPlate_no();
		}
		return etPallet.getText().toString().trim();
	}

	private SamsungLoad getSamsungLoad(String value) {
		for (SamsungLoad sl : loadDatas) {
			if (value.equals(sl.getPlate_no())) {
				return sl;
			}
		}
		return null;
	}

	private void reLoad() {
		RequestParams params = new RequestParams();
		params.add("Method", "ReLoadSumSang");
		params.add("ic_id", loadBean.ic_id);
		params.add("dlo_detail_id", loadBean.dlo_detail_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				loadDatas.clear();
				loadDatas.addAll(mDatas);
				loadedAdapter.notifyDataSetChanged();

				loadedDatas.clear();
				loadAdapter.notifyDataSetChanged();

				exceptionBtn.setVisibility(View.VISIBLE);
				finishBtn.setVisibility(View.GONE);
				setTotal();
				TTS.getInstance().speakAll("Reload Success!");
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Reload Failed!");
			}
		}.doGet(HttpUrlPath.ReceiveWorkAction, params, this);

	}

	private boolean isRepeat(String value) {
		for (SamsungLoad bean : loadedDatas) {
			if (bean.getPlate_no().equals(value)) {
				return true;
			}
		}
		return false;
	}

	private boolean isErrorData(String value) {
		for (SamsungLoad bean : mDatas) {
			if (bean.getPlate_no().equals(value)) {
				return false;
			}
		}
		return true;
	}

	public void finishOnClick(View v) {
		if (!ttp.isPhotoEmpty(true)) {
			uploadPhoto(false);
		} else {
			toFinishActivity();
		}
	}

	public void exceptionOnClick(View v) {
		if (!ttp.isPhotoEmpty(false)) {
			uploadPhoto(false);
		} else {
			toExceptionActivity();
		}
	}

	private void toFinishActivity() {
		Intent intent = new Intent(mActivity, SamsungFinishActivity.class);
		intent.putExtra("box", qtyTotalTv.getText().toString().trim());
		intent.putExtra("pallet", palletTotalTv.getText().toString().trim());
		intent.putExtra("weight", weightTotalTv.getText().toString().trim());
		intent.putExtra("entry_id", mEntryID);
		intent.putExtra("loadBean", loadBean);
		startActivity(intent);
	}

	private void toExceptionActivity() {
		Intent intent = new Intent(mActivity, SamsungExceptionActivity.class);
		intent.putExtra("box", qtyTv.getText().toString().trim());
		intent.putExtra("pallet", palletTv.getText().toString().trim());
		intent.putExtra("weight", weightTv.getText().toString().trim());
		intent.putExtra("boxTotal", qtyTotalTv.getText().toString().trim());
		intent.putExtra("palletTotal", palletTotalTv.getText().toString().trim());
		intent.putExtra("weightTotal", weightTotalTv.getText().toString().trim());
		intent.putExtra("entry_id", mEntryID);
		intent.putExtra("loadBean", loadBean);
		startActivity(intent);
	}

	/**
	 * true为Finish flase为Exception
	 * 
	 * @param flag
	 */
	public void uploadPhoto(final boolean flag) {
		RequestParams params = new RequestParams();
		params.add("Method", "SumsangLoadPhoto");
		params.add("entry_id", mEntryID);
		ttp.uploadZip(params, "SamsungLoadPhoto");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				if (flag) {
					toFinishActivity();
				} else {
					toExceptionActivity();
				}
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Failed!");
			}
		}.doPost(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ttp.clearCache();
				SamsungLoadActivity.super.onBackBtnOrKey();
			}
		});
	}

	class LoadAdapter extends BaseAdapter {

		List<SamsungLoad> loadData;

		public void setDatas(List<SamsungLoad> SamsungLoadDatas) {
			this.loadData = SamsungLoadDatas;
			notifyDataSetChanged();
		}

		public LoadAdapter(List<SamsungLoad> SamsungLoadDatas) {
			this.loadData = SamsungLoadDatas;
		}

		@Override
		public int getCount() {
			return loadData == null ? 0 : loadData.size();
		}

		@Override
		public Object getItem(int position) {
			return loadData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			LoadHolder holder = null;
			if (convertView == null) {
				holder = new LoadHolder();
				convertView = inflater.inflate(R.layout.item_load_samsung, null);
				holder.pnTv = (TextView) convertView.findViewById(R.id.pnTv);
				holder.qtyTv = (TextView) convertView.findViewById(R.id.qtyTv);
				holder.areaTv = (TextView) convertView.findViewById(R.id.areaTv);
				holder.weightTv = (TextView) convertView.findViewById(R.id.weightTv);
				holder.boxLay = (LinearLayout) convertView.findViewById(R.id.boxLay);
				holder.weightLay = (LinearLayout) convertView.findViewById(R.id.weightLay);
				convertView.setTag(holder);
			} else {
				holder = (LoadHolder) convertView.getTag();
			}

			final SamsungLoad load = loadData.get(position);
			holder.pnTv.setText(load.getPlate_no());
			holder.areaTv.setText(load.getLocation_name());
			if (!load.getBox_number().equals("0")) {
				holder.qtyTv.setText(load.getBox_number());
				holder.boxLay.setVisibility(View.VISIBLE);
			} else {
				holder.boxLay.setVisibility(View.GONE);
			}
			if (!load.getWeight().equals("0")) {
				holder.weightTv.setText(load.getWeight());
				holder.boxLay.setVisibility(View.VISIBLE);
			} else {
				holder.weightLay.setVisibility(View.GONE);
			}

			return convertView;
		}

		class LoadHolder {
			public TextView pnTv;
			public TextView qtyTv;
			public TextView areaTv;
			public TextView weightTv;
			public LinearLayout boxLay, weightLay;
		}
	}

}
