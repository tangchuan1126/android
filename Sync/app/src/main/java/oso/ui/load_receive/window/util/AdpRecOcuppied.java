package oso.ui.load_receive.window.util;

import java.util.List;

import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import support.key.OccupyTypeKey;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpRecOcuppied extends BaseAdapter {

	private Activity mActivity;
	private List<DockHelpUtil.ResourceInfo> arrayList;

	private LayoutInflater inflater;

	// int currentID = -1;
	ResourceInfo selBean;

	private boolean isSelectable = true; // 可选

	public AdpRecOcuppied(Activity mActivity, List<ResourceInfo> arrayList) {
		this(mActivity, arrayList, true);
	}

	public AdpRecOcuppied(Activity mActivity, List<ResourceInfo> arrayList, boolean isSelectable) {
		this.mActivity = mActivity;
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(mActivity);
		this.isSelectable = isSelectable;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public DockHelpUtil.ResourceInfo getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DoorHoder holder = null;
		if (convertView == null) {
			holder = new DoorHoder();
			convertView = inflater.inflate(R.layout.dci_s_door_item_2, null);
			holder.select_img = (ImageView) convertView.findViewById(R.id.select_img);
			holder.door_name = (TextView) convertView.findViewById(R.id.door_name);
			holder.tvTaskCnt_it = (TextView) convertView.findViewById(R.id.tvTaskCnt_it);
			holder.tvEquipCnt_it = (TextView) convertView.findViewById(R.id.tvEquipCnt_it);
			convertView.setTag(holder);
		} else
			holder = (DoorHoder) convertView.getTag();

		// 刷新数据
		final DockHelpUtil.ResourceInfo temp = arrayList.get(position);
		holder.door_name.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity, temp.resource_type) + " : [ " + temp.resource_name + " ]");
		holder.tvTaskCnt_it.setText("Tasks : [ " + temp.task + " ]");
		holder.tvEquipCnt_it.setText("Equipments : [ " + temp.equipment + " ]");

		holder.tvTaskCnt_it.setVisibility(temp.task > 0 ? View.VISIBLE : View.GONE);
		holder.tvEquipCnt_it.setVisibility(temp.equipment > 0 ? View.VISIBLE : View.GONE);

		holder.select_img.setVisibility(isSelectable ? View.VISIBLE : View.GONE);

		// 选中效果
		holder.select_img.setBackgroundResource(temp.isEqual(selBean) ? R.drawable.radio_clk : R.drawable.radio);
		return convertView;
	}

	public void setCurrentID(ResourceInfo currentID) {
		this.selBean = currentID;
		notifyDataSetChanged();
	}

	public ResourceInfo getCurrentId() {
		return this.selBean;
	}
}

class DoorHoder {
	public TextView door_name, tvTaskCnt_it, tvEquipCnt_it;
	public ImageView select_img;
}