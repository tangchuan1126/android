package oso.ui.load_receive.window.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

import com.google.gson.Gson;

/**
 * 任务对象
 * 
 * @author 朱成
 * @date 2014-12-24
 */
public class WindowTaskBean implements Serializable {

	private static final long serialVersionUID = 8426111154593102669L;

	public String company_id = "NA";
	public String detail_id;
	public String number_type;
	public String finish_time;
	public String title = "NA";
	public String number;

	public String customer_id = "NA";
	public String close_user = "";
	public String close_user_ids = "";
	public int number_status;
	public String area_id;

	public int resources_type;
	public int resources_id;
	public String resources_type_value;

	public static List<List<WindowTaskBean>> parseDatas(JSONArray tasksJa) {
		Gson gson = new Gson();
		List<List<WindowTaskBean>> datas = new ArrayList<List<WindowTaskBean>>();
		if (tasksJa != null || tasksJa.length() > 0) {
			for (int i = 0; i < tasksJa.length(); i++) {
				List<WindowTaskBean> list = new ArrayList<WindowTaskBean>();
				JSONObject jo = StringUtil.getJsonObjectFromArray(tasksJa, i);
				int resources_id = StringUtil.getJsonInt(jo, "resources_id");
				int resources_type = StringUtil.getJsonInt(jo, "resources_type");
				String resources_type_value = StringUtil.getJsonString(jo, "resources_type_value");
				String area_id = StringUtil.getJsonString(jo, "area_id");

				JSONArray load_listJa = StringUtil.getJsonArrayFromJson(jo, "load_list");
				for (int j = 0; j < load_listJa.length(); j++) {
					JSONObject loadJo = StringUtil.getJsonObjectFromArray(load_listJa, j);
					WindowTaskBean bean = gson.fromJson(loadJo.toString(), WindowTaskBean.class);
					bean.resources_id = resources_id;
					bean.resources_type = resources_type;
					bean.resources_type_value = resources_type_value;
					bean.area_id = area_id;
					list.add(bean);
				}
				datas.add(list);
			}
		}
		return datas;
	}

}
