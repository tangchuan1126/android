package oso.ui.load_receive.print.adapter;

import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.print.PackingItemInterface;
import oso.ui.load_receive.print.bean.PrintPackinglistBean;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PrintPackinglistAdapter extends BaseAdapter {
		public List<Boolean> mChecked;
		List<PrintPackinglistBean> listPerson;
		private LayoutInflater inflater;
		PackingItemInterface packingItem;
		private Context context;
		public PrintPackinglistAdapter(Context context, PackingItemInterface packingItem,List<PrintPackinglistBean> list) {
			this.context = context;
			listPerson = new ArrayList<PrintPackinglistBean>();
			listPerson = list;
			this.packingItem = packingItem;
			this.inflater  = LayoutInflater.from(this.context);
			mChecked = new ArrayList<Boolean>();
			for (int i = 0; i < list.size(); i++) {
				mChecked.add(false);
			}
		}
	
		@Override
		public int getCount() {
			return listPerson.size();
		}
	
		@Override
		public Object getItem(int position) {
			return listPerson.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return position;
		}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView==null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.print_packinglist_item_layout,null);
			holder.allview = convertView;
			holder.selected = (CheckBox) convertView.findViewById(R.id.cbOrder);
			holder.address = (TextView) convertView.findViewById(R.id.tvOrder);
			holder.packtxt = (Button) convertView.findViewById(R.id.btnPreview);
			holder.selected.setOnCheckedChangeListener(null);
			holder.allview.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					packingItem.selectItemFinish(listPerson.get(position),position);
 				}
			});
			holder.packtxt.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					packingItem.selectItemFinish(listPerson.get(position),-1);
				}
			});
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.selected.setChecked(mChecked.get(position));
		holder.address.setText(listPerson.get(position).getOrder_no());

		return convertView;
	}

}

class ViewHolder {
	View allview;
	CheckBox selected;
	TextView name;
	TextView address;
	Button packtxt;
	LinearLayout left_box;
}
