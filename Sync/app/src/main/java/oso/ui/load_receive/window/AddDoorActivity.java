package oso.ui.load_receive.window;

import java.util.List;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.FragSelRes;
import support.common.UIHelper;
import support.key.OccupyTypeKey;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

public class AddDoorActivity extends BaseActivity {

	private TextView entry_id_tx;
	private TextView equipment_tx;
	private Button submitBtn;

	private List<ResourceInfo> doorlist;
	private EntryBean mEntry;
	private EquipmentBean equipment;

	private FragSelRes fragRes;

	public static final int Code = 10020;

	private int type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_change_door_e, 0);
		initData();
		initView();
		setView();
	}

	private void initData() {
		mEntry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		equipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		doorlist = (List<ResourceInfo>) getIntent().getSerializableExtra("doorlist");
		type = getIntent().getIntExtra("type", 0);
		setTitleString(type == OccupyTypeKey.DOOR ? getString(R.string.shuttle_select_door) : getString(R.string.shuttle_select_spot));

		((TextView) findViewById(R.id.equipment_type)).setText(equipment.getEquipType_str(mActivity));
	}

	private void setView() {
		entry_id_tx.setText(mEntry.getEntry_id() + "");
		equipment_tx.setText(equipment.getEquipment_number());
		// 初始化fragRes
		fragRes.init(doorlist,(equipment!=null&&type==equipment.getResources_type())?equipment.getResources_id():0);
		submitBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ResourceInfo selectDoorInfo = fragRes.getSelectRes();
				if (selectDoorInfo == null) {
					UIHelper.showToast(mActivity, OccupyTypeKey.getPrompt(mActivity,type), Toast.LENGTH_SHORT).show();
					return;
				}
				Intent data = new Intent();
				data.putExtra("selectDoorInfo", selectDoorInfo);
				setResult(Code, data);
				finish();
			}
		});
	}

	private void initView() {
		entry_id_tx = (TextView) findViewById(R.id.entry_id_tx);
		equipment_tx = (TextView) findViewById(R.id.equipment_tx);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		fragRes = (FragSelRes) getFragmentManager().findFragmentById(R.id.fragRes);
	}

}
