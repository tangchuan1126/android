package oso.ui.load_receive.do_task.receive.oso.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.Utility;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

/**
 * 无单据收货,pallet/无号pallet
 * 
 * @author 朱成
 * @date 2015-1-3
 */
public class ReceiveOsoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4676434575632193447L;

	public String palletNo;
	public String typeId;

	public int cnt_noNoPallet; // 无号pallet-数量
	public String id_; // 用于删除

	public ReceiveOsoBean() {

	}

	public ReceiveOsoBean(String palletNo, String typeId) {
		this.palletNo = palletNo;
		this.typeId = typeId;
	}

	public ReceiveOsoBean(String typeId, int cnt) {
		this.typeId = typeId;
		this.cnt_noNoPallet = cnt;
	}

	/**
	 * 为无号pallet
	 * 
	 * @return
	 */
	public boolean is_noNoPallet() {
		return TextUtils.isEmpty(palletNo);
	}
	
	/**
	 * @return 托盘数,考虑了批量、单个
	 */
	public int getCnt(){
		return is_noNoPallet()?cnt_noNoPallet:1;
	}

	/**
	 * 复制
	 * 
	 * @param toBean
	 */
	public void copyTo(ReceiveOsoBean toBean) {
		toBean.palletNo = palletNo;
		toBean.typeId = typeId;
		toBean.cnt_noNoPallet = cnt_noNoPallet;
		toBean.id_ = id_;
	}

	public boolean hasPalletType() {
		return !TextUtils.isEmpty(typeId);
	}

	/**
	 * 移除提示
	 * 
	 * @return
	 */
	public Spannable getRemoveTip() {
		ReceiveOsoBean pallet = this;
		Spannable tip = null;
		// 有号
		if (!pallet.is_noNoPallet()) {
			tip = new SpannableString("Remove pallet " + pallet.palletNo + "?");
			Utility.addSpan(tip, "Remove pallet ", pallet.palletNo,
					new ForegroundColorSpan(0xffff0000));
		}
		// 无号
		else {
			tip = new SpannableString("Remove " + pallet.cnt_noNoPallet
					+ " pallets?");
			Utility.addSpan(tip, "Remove ", pallet.cnt_noNoPallet + "",
					new ForegroundColorSpan(0xffff0000));
		}
		return tip;
	}

	// ==================static===============================

	/**
	 * 获取-palletType统计数量
	 * @param listPallets
	 * @return map为{"0":typeID,"1":cnt}
	 */
	public static List<Map<String, String>> getMapTypesCnt(
			List<ReceiveOsoBean> listPallets) {
		
		if(listPallets==null || listPallets.size()==0)
			return null;
		
		//统计,type-cnt数
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < listPallets.size(); i++) {
			ReceiveOsoBean pallet = listPallets.get(i);
			// null也能map.put,故需跳出
			if (!pallet.hasPalletType())
				continue;
			String type = pallet.typeId;
			if (map.containsKey(type)) {
				int tmpCnt = map.get(type);
				map.put(type, tmpCnt + pallet.getCnt());
			} else
				map.put(type, pallet.getCnt());
		}
		
		Set<String> types = map == null ? null
				: map.keySet();
		if (types == null || types.isEmpty()) 
			return null;
		
		List<Map<String, String>> listRet=new ArrayList<Map<String,String>>();
		for (String type : types) {
			Map<String, String> tmpMap=new HashMap<String, String>();
			tmpMap.put("0", type);
			tmpMap.put("1", map.get(type)+"");
			listRet.add(tmpMap);
		}
		return listRet;
	}

	/**
	 * 查找bean
	 * 
	 * @param list
	 * @param palletNo
	 * @return
	 */
	public static ReceiveOsoBean getPalletByNo(List<ReceiveOsoBean> list,
			String palletNo) {
		if (list == null)
			return null;
		if (TextUtils.isEmpty(palletNo))
			return null;

		for (int i = 0; i < list.size(); i++) {
			ReceiveOsoBean tmpB = list.get(i);
			if (palletNo.equals(tmpB.palletNo))
				return tmpB;
		}
		return null;
	}

	/**
	 * pallet总数
	 * 
	 * @param list
	 * @return
	 */
	public static int getPalletCnt(List<ReceiveOsoBean> list) {
		if(list==null)
			return 0;
			
		int cnt = 0;
		for (int i = 0; i < list.size(); i++) {
			ReceiveOsoBean tmpB = list.get(i);
			cnt += (tmpB.is_noNoPallet() ? tmpB.cnt_noNoPallet : 1);
		}
		return cnt;
	}

	// ==================parse===============================

	public static void parseBean(JSONObject jo, ReceiveOsoBean b, boolean isLoad) {
		// load
		if (isLoad) {
			b.palletNo = jo.optString("wms_pallet_number");
			b.typeId = jo.optString("wms_pallet_type");
			b.cnt_noNoPallet = jo.optInt("count");
			b.id_ = jo.optString("wms_order_type_id");
		}
		// receive
		else {
			b.palletNo = jo.optString("pallet_number");
			b.typeId = jo.optString("pallet_type");
			b.cnt_noNoPallet = jo.optInt("pallet_type_count");
			b.id_ = jo.optString("receive_pallet_id");
		}
	}

	// 入参:excludeConsolidated(true:排除consolidated掉的pallet)
	public static void parseBeans(JSONArray ja, List<ReceiveOsoBean> list,
			boolean isLoad) {
		if (ja == null)
			return;

		for (int i = 0; i < ja.length(); i++) {
			ReceiveOsoBean b = new ReceiveOsoBean();
			parseBean(ja.optJSONObject(i), b, isLoad);
			list.add(b);
		}
	}

}
