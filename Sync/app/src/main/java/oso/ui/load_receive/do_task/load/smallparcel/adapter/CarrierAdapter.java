package oso.ui.load_receive.do_task.load.smallparcel.adapter;

import java.util.List;

import oso.ui.load_receive.do_task.receive.task.bean.Rec_ShipToBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import declare.com.vvme.R;

public class CarrierAdapter extends BaseAdapter {

	private List<Rec_ShipToBean> users;
	private Context mContext;

	public CarrierAdapter(Context c, List<Rec_ShipToBean> datas) {
		mContext = c;
		users = datas;
	}

	@Override
	public int getCount() {
		return users == null ? 0 : users.size();
	}

	@Override
	public Object getItem(int position) {
		return users;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		OHolder holder = null;
		if (convertView == null) {
			holder = new OHolder();
			convertView = View.inflate(mContext, R.layout.item_carriers, null);
			holder.carrierTv = (TextView) convertView.findViewById(R.id.carrierTv);
			holder.box = (CheckBox) convertView.findViewById(R.id.box);
			convertView.setTag(holder);
		} else {
			holder = (OHolder) convertView.getTag();
		}

		final Rec_ShipToBean user = users.get(position);
		//debug
		holder.carrierTv.setText(user.ship_to_name);
		holder.box.setChecked(user.isSelect);

		return convertView;
	}

	class OHolder {
		public TextView carrierTv;
		public CheckBox box;
	}
}
