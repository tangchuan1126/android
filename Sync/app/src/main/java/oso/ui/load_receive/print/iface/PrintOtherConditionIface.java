package oso.ui.load_receive.print.iface;

import oso.ui.load_receive.print.bean.ReceiptTicketBase;

public interface PrintOtherConditionIface {
	public void ChangeValue(ReceiptTicketBase receiptTicketBase);
	
	public void CloseActivity();
}
