package oso.ui.load_receive.do_task.receive.wms.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oso.ui.load_receive.do_task.receive.wms.util.LocBean;

import utility.Utility;
import android.text.TextUtils;

/**
 * pallet配置
 * @author 朱成
 * @date 2015-3-17
 */
public class Rec_PalletConfigBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7558374744631656819L;

	public int length,width,height;
	public int plate_number;
	public String receipt_line_id;
	
	public String create_date;
	public String container_config_id;  //tlp/clp、有配置/无配置,均有该值
	public String item_id;
//	public int config_type;				//配置类型,1:有长宽高 2:仅qty
	public String lot_no;
	public String create_user;
	public String pallet_name;
	public String note;
	
	//----------
	public String lp_name;
	public String ship_to_name;
	
	public String print_user_name;		//打印人、打印时间
	public String print_date;

    //===========CLP.config================================
    public int goods_qty;
    public long license_plate_type_id;       //clp、shipto关联id,有值则为clp
    public int goods_type;                      //类别,好的/坏的,取Rec_PalletBean.PltType_x
    
    
    public int keep_inbound;
    
    public String receipt_no;
    public String con_id;          //这里实际返回的是wms_container_id;
    
    public int is_partial;
    public int process;
    
    //
    public int location_type;
    public String location;
    public int location_id;
	
	public Rec_PalletConfigBean(int l,int w,int h,int pltCnt){
		this.length=l;
		this.width=w;
		this.height=h;
		this.plate_number=pltCnt;
	}
	
	public String getWMSContainerId(){
		return con_id;
	}

    public boolean isCLP(){
        return license_plate_type_id>0;
    }

    public boolean isDmg(){
        return goods_type==Rec_PalletBean.PltType_Damage;
    }
	
	public int getQtyPerPlt(){
		return length*width*height;
	}
	
	/**
	 * config总qty
	 * @return
	 */
	public int getTotalQty(){
		return getQtyPerPlt()*plate_number;
	}
	
	public String getConfigDesc(){
		return length+"x"+width+"x"+height;
	}
	
	/**
	 * 有配置
	 * @return
	 */
	public boolean hasConfig(){
		return getQtyPerPlt()>0;
	}
	
	/**
	 * 已打印过
	 * @return
	 */
	public boolean isPrinted(){
		return !TextUtils.isEmpty(print_user_name);
	}
	
	
	/**
	 * Ship To是否为空
	 * @return
	 */
	public boolean hasShipToName(){
		return !TextUtils.isEmpty(ship_to_name);
	}
	
	//==================static===============================
	
	public static boolean isPrint(List<Rec_PalletConfigBean> list){
		if(Utility.isEmpty(list))
			return false;
		for(Rec_PalletConfigBean bean : list){
			if(!bean.isPrinted()){
				return true;
			}
		}
		return false;
	}
	
	public static int getTotalPlts(List<Rec_PalletConfigBean> list){
		if(Utility.isEmpty(list))
			return 0;
		int ret=0;
		for (int i = 0; i < list.size(); i++) {
			ret+=list.get(i).plate_number;
		}
		return ret;
	}
	
	public static void repalceOrAdd_noConfig(List<Rec_PalletConfigBean> listSrc,List<Rec_PalletConfigBean> listToAdd){
        //listSrc不应为null 到可能size=0
		if(listSrc==null || listToAdd==null)
			return;
//		for (int i = 0; i < list.size(); i++) {
//			//若无配置===
//			if(!list.get(i).hasConfig())
//			{
//				list.remove(i);
//				list.add(i,bToSet);
//				return;
//			}
//		}

        //倒着加,免得把partial的加到前面了
        for (int i = listToAdd.size()-1; i >=0; i--) {
            Rec_PalletConfigBean bToSet=listToAdd.get(i);
            int oldIndex=Rec_PalletConfigBean.getConfig_byID(listSrc,bToSet.container_config_id);
            //若已存在 先移除旧的
            if(oldIndex>=0)
                listSrc.remove(oldIndex);
            listSrc.add(0,bToSet);
        }

	}

	/**
	 * 获取-configs总qty
	 * @param list
	 * @return
	 */
	public static int getTotalQty(List<Rec_PalletConfigBean> list){
		if(list==null)
			return 0;
		int sum=0;
		for (int i = 0; i < list.size(); i++) {
			sum+=list.get(i).getTotalQty();
		}
		return sum;
	}
	
	public static int getTotalQty_keep(List<Rec_PalletConfigBean> list){
		if(list==null)
			return 0;
		int sum=0;
		for(Rec_PalletConfigBean bean : list){
			if(!bean.isDmg()){
				if(bean.keep_inbound == 0){
					sum += bean.goods_qty;
				}
			}
		}
		return sum;
	}

    public static int getTotalQty_good(List<Rec_PalletConfigBean> list){
        if(list==null)
            return 0;
        int sum=0;
        for (int i = 0; i < list.size(); i++) {
        	Rec_PalletConfigBean b=list.get(i);
        	if(!b.isDmg())
        		sum+=b.goods_qty;
        }
        return sum;
    }

    public static int getConfig_byID(List<Rec_PalletConfigBean> list,String configID){
        if(Utility.isEmpty(list)||TextUtils.isEmpty(configID))
            return -1;
        for (int i = 0; i < list.size(); i++) {
            Rec_PalletConfigBean b=list.get(i);
            if(configID.equals(b.container_config_id))
                return i;
        }
        return -1;
    }
    
    
    //
    public static LocBean getMostLoc(List<Rec_PalletConfigBean> list){
	if(Utility.isEmpty(list))
		return null;

    final String delim="_ 1a";
	Map<String, Integer> mapLocCnt=new HashMap<String, Integer>();  //key取name+type
	//统计loc数
	for (int i = 0; i < list.size(); i++) {
		Rec_PalletConfigBean tmp=list.get(i);
		String loc=tmp.location;
		if(TextUtils.isEmpty(loc))
			continue;

        String keyLoc_Type=tmp.location+delim+tmp.location_type+delim+tmp.location_id;
		if(mapLocCnt.containsKey(keyLoc_Type))
		{
			int tmpCnt=mapLocCnt.get(keyLoc_Type);
			mapLocCnt.put(keyLoc_Type, tmpCnt+1);
		}
		else
			mapLocCnt.put(keyLoc_Type, 1);
	}
	
	Map.Entry<String, Integer> locCnt_m=null;
	for (Map.Entry<String, Integer> locCnt : mapLocCnt.entrySet()) {
		if(locCnt_m==null)
		{
			locCnt_m=locCnt;
			continue;
		}
		int lastMost=locCnt_m.getValue();
		int tmpCnt=locCnt.getValue();
		//保留-较大的
		if(tmpCnt>lastMost)
			locCnt_m=locCnt;
	}
    if(locCnt_m==null)
        return null;
    String[]  str=locCnt_m.getKey().split(delim);
	return new LocBean(str[0],Utility.parseInt(str[1]),Utility.parseInt(str[2]));
}
	
}
