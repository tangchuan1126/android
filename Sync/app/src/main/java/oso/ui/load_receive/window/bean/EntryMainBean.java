package oso.ui.load_receive.window.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.common.bean.CheckInTaskMainItem;
import utility.StringUtil;
import android.text.TextUtils;

public class EntryMainBean implements Serializable {

	private static final long serialVersionUID = 2786689135996496387L;

	public String gate_check_in_time;
	public String gate_driver_name;
	public String entry_id;
	public String company_name;
	public List<CheckInTaskMainItem> complexDoorList;
	
	public static List<EntryMainBean> helpJson(JSONObject json) {
		List<EntryMainBean> list = null;
		JSONArray jsonArray = json.optJSONArray("datas");
		if (!StringUtil.isNullForJSONArray(jsonArray)) {
			list = new ArrayList<EntryMainBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				EntryMainBean bean = new EntryMainBean();
				bean.entry_id = item.optString("entry_id");
				bean.company_name = item.optString("company_name");
				bean.gate_driver_name = item.optString("gate_driver_name");
				bean.gate_check_in_time = item.optString("gate_check_in_time");
				JSONArray jsonArrays = StringUtil.getJsonArrayFromJson(item, "equipments");
				if(jsonArray != null && jsonArrays.length() > 0){
				List<CheckInTaskMainItem>	lists = new ArrayList<CheckInTaskMainItem>();
					for (int j = 0; j < jsonArrays.length(); j++) {
						JSONObject items = jsonArrays.optJSONObject(j);
						if(item!=null){
							CheckInTaskMainItem b = new CheckInTaskMainItem();
							b.equipment_status = item.optInt("equipment_status");
							b.equipment_purpose = item.optInt("equipment_purpose");
							b.equipment_type = items.optInt("equipment_type");
							b.equipment_number = items.optString("equipment_number");
							
							//debug,若设备无效 则不加入
							if(TextUtils.isEmpty(b.equipment_number))
								continue;
							
							lists.add(b);
						}
					}
					bean.complexDoorList = lists;
				}
				list.add(bean);
			}
		}
		return list;
	}
}
