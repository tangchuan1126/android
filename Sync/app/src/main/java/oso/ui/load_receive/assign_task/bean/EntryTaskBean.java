package oso.ui.load_receive.assign_task.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;


public class EntryTaskBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -325868124802234794L;

	public String entry_id;
	public int rel_type;
	public String equipment_purpose;
	public String equipment_number;
	public int equipment_id;
	public int task_assigned;
	public int task_total;

	public static List<EntryTaskBean> helpJson(JSONObject json) {
		List<EntryTaskBean> list = null;
		JSONArray jsonArray = json.optJSONArray("data");
		if (!StringUtil.isNullForJSONArray(jsonArray)) {
			list = new ArrayList<EntryTaskBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				EntryTaskBean bean = new EntryTaskBean();
				bean.entry_id = item.optString("entry_id");
				bean.rel_type = item.optInt("rel_type");
				bean.equipment_purpose = item.optString("equipment_purpose");// 1,
				bean.equipment_number = item.optString("equipment_number");
				bean.equipment_id = item.optInt("equipment_id");
				bean.task_assigned = item.optInt("task_closed");
				bean.task_total = item.optInt("task_total");
				list.add(bean);
			}
		}
		return list;
	}
}
