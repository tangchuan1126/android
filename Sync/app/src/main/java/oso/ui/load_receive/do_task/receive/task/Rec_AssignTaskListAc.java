package oso.ui.load_receive.do_task.receive.task;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_AssignTask_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 收货-分配任务-item列表,supervisor使用
 * @author 朱成
 * @date 2015-3-11
 */
public class Rec_AssignTaskListAc extends BaseActivity {
	
	private ListView lv;
	private List<Rec_AssignTask_ItemBean> listLines=new ArrayList<Rec_AssignTask_ItemBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_rec_assigntasklistac, 0);
		initData();
		initView();
		setData();
	}
	
	private boolean isFirstResume=false;
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
//		if(!isFirstResume)
		refresh();
	}
	
	private void refresh(){
		
		RequestParams p=new RequestParams();
		p.add("adid", StoredData.getAdid());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray jaLines=json.optJSONArray("data");
				if(jaLines==null)
					return;
				List<Rec_AssignTask_ItemBean> tmpLines=new Gson().fromJson(
						jaLines.toString(),
						new TypeToken<List<Rec_AssignTask_ItemBean>>(){}.getType());
				
				listLines.clear();
				listLines.addAll(tmpLines);
				
				if(listLines.size() == 0){
					onBackBtnOrKey();
					return;
				}
				adp.notifyDataSetChanged();
				
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/findSupervisorTaskList", p,this);
		
	}

	private void initData() {
//		setTitleString("Task Number: " + "100001");
//		setTitleString("Assign CC/Scan Task");
		setTitleString("My Scan/CC Task");
	}

	private void initView() {
		lv = (ListView) findViewById(R.id.lv);
	}

	private void setData() {
		lv.setEmptyView(findViewById(R.id.tvEmpty));
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				Intent in = new Intent(mActivity, Receive_CreatePalletsAc.class);
//				Receive_CreatePalletsAc.initParams(in,null,null, true);
//				startActivity(in);
				
				Rec_AssignTask_ItemBean taskBean=listLines.get(position);
				
				Rec_ItemBean item=new Rec_ItemBean();
				item.receipt_line_id=taskBean.receipt_line_id;
//				item.re=taskBean.receipt_id;
				item.item_id=taskBean.item_id;
				item.lot_no=taskBean.lot_no;
				item.receipt_id=taskBean.receipt_id;
				item.expected_qty=taskBean.expected_qty;
				item.normal_qty=taskBean.normal_qty;
				item.damage_qty=taskBean.damage_qty;
				item.customer=taskBean.customer;
				item.supplier=taskBean.supplier;
				Rec_CC_ConfigAc.toThis(mActivity, item, true,true,"","",taskBean.receipt_no);
			}
		});
	}

	//==================nested===============================
	
	private BaseAdapter adp=new BaseAdapter() {

		@Override
		public int getCount() {
			return listLines.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h;
			if (convertView == null) {
				convertView=getLayoutInflater().inflate(R.layout.item_rec_assigntasklistac, null);
				h = new Holder();
				h.tvCCTask=(TextView)convertView.findViewById(R.id.tvCCTask);
				h.tvReceiptID=(TextView)convertView.findViewById(R.id.tvReceiptID);
				h.tvItemID=(TextView)convertView.findViewById(R.id.tvItemID);
				h.tvLotNO=(TextView)convertView.findViewById(R.id.tvLot);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();
			
			//刷ui
			Rec_AssignTask_ItemBean b=listLines.get(position);
			h.tvCCTask.setText(b.receipt_line_id);
//			h.tvReceiptID.setText("Receipt"+b.receipt_id);
            h.tvReceiptID.setText(b.receipt_no);
			h.tvItemID.setText(b.item_id);
			
			if(TextUtils.isEmpty(b.lot_no)){
				h.tvLotNO.setText("");
				h.tvLotNO.setHint("NA");
			}else{
				h.tvLotNO.setText(b.lot_no);
			}
			
			return convertView;
		}
		
	};
	
	private class Holder{
		TextView tvCCTask,tvReceiptID,tvItemID,tvLotNO;
	}
}
