package oso.ui.load_receive.do_task.receive.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.process.bean.Rec_RepeatPalletBean;
import oso.ui.load_receive.do_task.receive.wms.adapter.AdpRepeatPlts;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Receive_palletsSn;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.AppConfig;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.PrintTool;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 装货界面
 * 
 * @author 朱成
 * @date 2015-3-2
 */
public class Rec_RemoveDupAc extends BaseActivity implements OnCheckedChangeListener, OnClickListener {

	private SingleSelectBar tab;
	private ListView lvSN;

	private TextView tvSN_title, tvSN_ok;
//	private View btnAddQty;
	private CheckBox checkBoxSku, checkBoxLNO;
	Button MyCheckBox_X;
	private EditText edittextySku;
	private SearchEditText edittextyLNO;
	// private String snString, skuString, lnoString;
	private SearchEditText etSn;
	// private Receive_ItemSetUp bean;
	// private Button snbtnSub;
	// private Context context;
	// Rec_PalletBean palletsbean;
	private List<Receive_palletsSn> listSn;
	Receive_palletsSn receive_palletsSn/* = new Receive_palletsSn() */;

	private Button btnSubmit;
	private TextView tvDuplSN,itemTv,locTv;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_rec_remove_dup, 0);
		applyParams();

		// context = Receive_ReceiveAc.this;
		// 取v
		tab = (SingleSelectBar) findViewById(R.id.tab);
		lvSN = (ListView) findViewById(R.id.lvSN);
		tvSN_title = (TextView) findViewById(R.id.tvSN_title);
		checkBoxSku = (CheckBox) findViewById(R.id.sku_checkbox);
		checkBoxLNO = (CheckBox) findViewById(R.id.lno_checkbox);
		edittextySku = (EditText) findViewById(R.id.sku_editText);
		edittextyLNO = (SearchEditText) findViewById(R.id.lno_editText);
		etSn = (SearchEditText) findViewById(R.id.etMcDot);
		// snbtnSub = (Button) findViewById(R.id.snbtnSub);
		MyCheckBox_X = (Button) findViewById(R.id.checkAllBox);
		tvSN_ok = (TextView) findViewById(R.id.tvSN_ok);
//		btnAddQty = findViewById(R.id.btnAddQty);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		tvDuplSN=(TextView) findViewById(R.id.tvDuplSN);
		itemTv=(TextView) findViewById(R.id.itemTv);
		locTv=(TextView) findViewById(R.id.locTv);
		
		// 事件
		btnSubmit.setOnClickListener(this);
		checkBoxSku.setOnCheckedChangeListener(this);
		checkBoxLNO.setOnCheckedChangeListener(this);
		MyCheckBox_X.setOnClickListener(this);
		etSn.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				etSn.setText("");

				// 超出了总数
//				if (listSn.size() >= pltBean.getSnQty_needScan()) {
//					TTS.getInstance().speakAll_withToast("total quantity override!",true);
//					return;
//				}
				// 空校验
				if (TextUtils.isEmpty(value)) {
					TTS.getInstance().speakAll_withToast("Invalid SN!",true);
					return;
				}
				// 验证-长度
				if (!itemBean.checkSNSize(value)) {
					TTS.getInstance().speakAll_withToast("Invalid SN size!",true);
					return;
				}
				
				// 本托盘-不允许扫重
//				if (pltBean.hasSn(value)) {
//					TTS.getInstance().speakAll_withToast("Duplicate SN in same pallet!",true);
//					Utility.vibrate();
//					return;
//				}
			    
				// 本托盘-不允许扫重
//				Receive_palletsSn sn_sameplt=pltBean.getFirstSN(value);
//			    if (sn_sameplt!=null) {
				
			
				Receive_palletsSn sn_sameplt=pltBean.getFirstSN(value);
				//不在该plt
				if(sn_sameplt==null)
				{
					TTS.getInstance().speakAll_withToast("New SN found, please rescan the pallet!");
					RewriteBuilderDialog.showSimpleDialog(mActivity, "New SN "+value+" found, clear the pallet to rescan later?", 
							new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									reqDelAll();
								}
							});
					return;
				}
				
				sn_lastScan=value;
				//选中-扫到的sn
				final int k=pltBean.sn.indexOf(sn_sameplt);
				adpSN.notifyDataSetChanged();
				lvSN.setSelection(k);
				//若找到-重的SN
				if(sn_dupl.equals(value)){
					TTS.getInstance().speakAll_withToast("Find Dulplicate SN, please remove!",true);
					Utility.vibrate();
					
					//提示移除
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Find Dulp. SN "+sn_sameplt.serial_number
							+", remove?", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							reqDel(k);
						}
					});
					return;
				}
				else
					TTS.getInstance().speakAll_withToast("Not Match");
				

//				List<Rec_PalletBean> listRep = Rec_PalletBean.getPlts_hasSn(value, listPlts,pltBean);
//				// 其他plt有重的,弹框提示确认
//				if (!Utility.isEmpty(listRep)) {
//					TTS.getInstance().speakAll_withToast("Duplicate SN! " + "Please Confirm!",true);
//					showDlg_tipRepeat(listRep, value);
//				} 
//				//添加
//				else
//					addSn(value, false);

			}
		});
		edittextyLNO.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				edittextyLNO.setText("");
				// 若相同
				if (itemBean.lot_no.equals(value)) {
					lockLotNo(true);
					// debug
					TTS.getInstance().speakAll_withToast("Ready for SN!");
				} else
					TTS.getInstance().speakAll_withToast("Lot Number doesn't match!",true);
			}
		});

		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>("SN", 0));
		list.add(new HoldDoubleValue<String, Integer>("SKU", 1));
		tab.setUserDefineClickItems(list);
		tab.userDefineSelectIndexExcuteClick(0);
		tab.setUserDefineCallBack(new SelectBarItemClickCallBack() {

			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				// TODO Auto-generated method stub
				update_snTitle();
			}
		});

		// 补上-缺少的,debug
		int initIndex = listSn.size();
		int lack = pltBean.getSnQty_needScan() - initIndex;

		// debug,自动补全sn
		if (AppConfig.Debug_Receive && lack > 0) {
			int base = (int) (Math.random() * 100000);
			for (int i = 0; i < lack; i++) {
				Receive_palletsSn tmpSn = new Receive_palletsSn();
				// tmpSn.serial_number = "SN_" + (i + initIndex);
				tmpSn.serial_number = getMockSn(base, itemBean.sn_size, i + initIndex);
				listSn.add(tmpSn);
			}
		}

		// 初始化
        String lpType=pltBean.isClp()?"CLP:  ":"TLP:  ";
        setTitleString(lpType + pltBean.getDisPltNO());
//		setTitleString("Pallet:  " + pltBean.getDisPltNO());
		// debug
		// checkBoxSku.setChecked(itemBean.itemsetup.isIs_need_sn());
		// checkBoxLNO.setChecked(itemBean.itemsetup.isIs_check_lot_no());
		refSumUI();
		update_snTitle();
		lockLotNo(true);
		edittextyLNO.setScanMode();
		etSn.setScanMode();
//		showDlg_verifyLot();
		tvDuplSN.setText(sn_dupl+"");
		findViewById(R.id.loDuplSN).setVisibility(View.VISIBLE);
		//debug,不支持提交
		btnSubmit.setVisibility(View.GONE);
		itemTv.setText(itemBean.item_id);
		locTv.setText(pltBean.location);

		// lv
		lvSN.setAdapter(adpSN);
	}
	
	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll_withToast("Ready! Scan duplicate SN!");
	}
	
//	@Override
//	protected void onBackBtnOrKey() {
//		// TODO Auto-generated method stub
//		setRst_beforeReturn();
//		super.onBackBtnOrKey();
//	}

//	private void setRst_beforeReturn() {
//		// 若已移除
//		if (!pltBean.hasSn(dulpSN))
//			setResult(RESULT_OK);
//	}

	private void addSn(String snNO, boolean isRepeat) {
		receive_palletsSn = new Receive_palletsSn();
		receive_palletsSn.serial_number = snNO;
		listSn.add(0, receive_palletsSn);
		// 标识重复
		if (isRepeat)
			pltBean.markRepeatSN(snNO);

		adpSN.notifyDataSetChanged();

		etSn.setText("");
		boolean finish = refSumUI();
		Utility.colseInputMethod(mActivity, etSn);
		
		//会超出 叫毛的finish
		TTS.getInstance().speakAll("SN " + listSn.size());
		
//		// tts
//		if (!finish)
//			TTS.getInstance().speakAll("SN " + listSn.size());
//		// plt完成时
//		else {
//			TTS.getInstance().speakAll("Pallet finish!");
//		}
	}

	private void showDlg_tipRepeat(List<Rec_PalletBean> listPlts_rep, final String snNO) {

		View v = getLayoutInflater().inflate(R.layout.rec_repeatsn_item, null);
		TextView tvSN_rep = (TextView) v.findViewById(R.id.tvSN_rep);
		ListView lvRepeatPlts = (ListView) v.findViewById(R.id.lvRepeatPlts);
		// 初始化
		tvSN_rep.setText("SN " + snNO + " has been scanned in:");
		// lv
		AdpRepeatPlts adp = new AdpRepeatPlts(this, listPlts_rep);
		lvRepeatPlts.setAdapter(adp);

		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.patrol_confirm));
		bd.setContentView(v);
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				addSn(snNO, true);
			}
		});
		bd.hideCancelBtn();
		bd.show();
	}

	public static String getMockSn(int base, String sn_size, int index) {
		if(TextUtils.isEmpty(sn_size)){
			return "SN_" + base + "_" + index;
		}
		String[] strSizes = sn_size.split(",");
		List<String> strLists = Arrays.asList(strSizes);
		//无限制
		if(strLists.contains("0")){
			return "SN_" + base + "_" + index;
		}

		long k = (long) Math.pow(10, Integer.valueOf(strLists.get(0))) + index;
		k = k + base;

		String str = k + "";
		return str.substring(1);
	}

	// @Override
	// protected void onFirstVisible() {
	// // TODO Auto-generated method stub
	// super.onFirstVisible();
	// TTS.getInstance().speakAll("Ready!");
	// }

//	@Override
//	protected void onBackBtnOrKey() {
//		// TODO Auto-generated method stub
//		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				Rec_RemoveDupAc.super.onBackBtnOrKey();
//			}
//		});
//	}

	/**
	 * 验证lot
	 */
	private void showDlg_verifyLot() {

		View v = getLayoutInflater().inflate(R.layout.dlg_rec_scan_verifylot, null);
		final SearchEditText etLotNO_dlg = (SearchEditText) v.findViewById(R.id.etLotNO_dlg);
		etLotNO_dlg.setScanMode();

		// debug
		if (AppConfig.Debug_Receive)
			etLotNO_dlg.setText(itemBean.lot_no);

		final BottomDialog dlg = new BottomDialog(mActivity).setTitle("Verify Lot NO").setView(v).setCanceledOnTouchOutside(false)
				.setShowCancelBtn(false).hideButton();
		dlg.getDlg().setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				// TODO Auto-generated method stub
				TTS.getInstance().speakAll("Scan Lot Number!");
			}
		});
		dlg.show();

		// 事件
		etLotNO_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				etLotNO_dlg.setText("");
				// 若相同
				if (itemBean.lot_no.equals(value)) {
					lockLotNo(true);
					// debug
					TTS.getInstance().speakAll_withToast("Ready for SN!");

					dlg.dismiss();
				} else
					TTS.getInstance().speakAll_withToast("Lot Number doesn't match!",true);
			}
		});

		dlg.getDlg().setOnKeyListener(new DialogInterface.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				// 返回时,直接退出
				if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
					Rec_RemoveDupAc.super.finish();
					return true;
				}
				return false;
			}
		});

	}

	private void lockLotNo(boolean toLock) {
		edittextyLNO.setText(toLock ? itemBean.lot_no : "");
		edittextyLNO.setEnabled(!toLock);
		etSn.setEnabled(toLock);

		if (toLock)
			etSn.requestFocus();
		else
			edittextyLNO.requestFocus();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSubmit: // 提交
			int snSize=listSn.size();
			RewriteBuilderDialog.showSimpleDialog(this, snSize+" SN, finish?", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					reqSubmit();
				}
			});
			break;
		case R.id.checkAllBox: // 删除所有
			RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_all), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					reqDelAll();
				}
			});
			break;

		default:
			break;
		}
	}

	private void reqDelAll(){
		RequestParams params = new RequestParams();
		params.put("con_id", pltBean.con_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				listSn.clear();
				adpSN.notifyDataSetChanged();
				refSumUI();
				UIHelper.showToast(getString(R.string.sync_delete_success));
				
				//debug
				finish();
			}

		}.doPost(HttpUrlPath.deletePalletSN, params, mActivity);
	}
	
	/**
	 * 刷新已扫描数
	 */
	private boolean refSumUI() {
		int totalSn = pltBean.getSnQty_needScan();
		int scanedSn = listSn.size();
		
		//若超出 则跟着变
		if(scanedSn>totalSn)
			totalSn=scanedSn;
//		tvSN_ok.setText(scanedSn + " / " + totalSn);
		//debug
		tvSN_ok.setText(scanedSn+"");

		boolean scanedFinish = totalSn == scanedSn;
		// btnSubmit.setVisibility(scanedFinish?View.VISIBLE:View.GONE);
		return scanedFinish;
	}
	
	private PrintTool printTool=new PrintTool(this);

	private void reqPrint(long printServerID){
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, pltBean.con_id);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				String name = StoredData.getUsername();
				pltBean.print_user_name = name;
				
				//打印完后-提交
				reqSubmit();
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);
	}
	
	private void reqSubmit() {
		
		//提示-打印
		if(!pltBean.isPrinted()){
			printTool.showDlg_printers(new PrintTool.OnPrintLs() {
				
				@Override
				public void onPrint(long printServerID) {
					// TODO Auto-generated method stub
					reqPrint(printServerID);
				}
			},"Print Pallet Label");
			return;
		}
		
		final JSONArray json = new JSONArray();
		try {
			for (int i = 0; i < listSn.size(); i++) {
				JSONObject jo = new JSONObject();
				jo.put("sn", listSn.get(i).serial_number);
				json.put(jo);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final RequestParams params = new RequestParams();
		params.add("con_id", pltBean.con_id);
		params.add("sns", json.toString());
		params.add("receipt_id",itemBean.receipt_id);
		params.add("lot_no", pltBean.lot_no);
		params.add("item_id", pltBean.item_id);
		// params.add("isTask", "0");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_success));
				// 若为最后一个,至lineList
//				if (pltBean.isLastPlt_inLine)
//					Utility.popTo(mActivity, Rec_LineListAc.class);
//				else
//				setRst_beforeReturn();
				if(json.optInt("err") == 91){
					Rec_RepeatPalletBean repeatPalletBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_RepeatPalletBean.class);
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
					
					View v = View.inflate(mActivity, R.layout.item_repeatpallet, null);
					TextView tvSn = (TextView) v.findViewById(R.id.tvSN);
					TextView tvPallet1 = (TextView) v.findViewById(R.id.tvPallet1);
					TextView tvPallet2 = (TextView) v.findViewById(R.id.tvPallet2);
					tvSn.setText(repeatPalletBean.serial_number);
					String[] pallets = repeatPalletBean.getContainer();
					tvPallet1.setText(pallets[0]);
					tvPallet2.setText(pallets[1]);
					
					builder.setContentView(v);
					
					builder.isHideCancelBtn(true);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
					});
					builder.show();
					return;
				}
				finish();
			}

		}.doPost(HttpUrlPath.bindSNInfoToPallet, params, mActivity);
	}

	private void update_snTitle() {
		tvSN_title.setText(isScan_Sn() ? "SN " : "Qty ");
		//debug
//		btnAddQty.setVisibility(isScan_Sn() ? View.GONE : View.VISIBLE);
	}

	private boolean isScan_Sn() {
		return tab.getCurrentSelectItem().b == 0;
	}
	
	private void showDlg_delSn(final int pos) {

		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqDel(pos);
			}
		});

	}
	
	private void reqDel(final int pos){
		Receive_palletsSn bean = listSn.get(pos);
		final String snNO = bean.serial_number;
		if (!Receive_palletsSn.isServer(bean)) {
			// 移除-重复标识
			Receive_palletsSn.removeRepeatMark(listPlts, pltBean, snNO);
			listSn.remove(pos);
			adpSN.notifyDataSetChanged();
			refSumUI();
			UIHelper.showToast(getString(R.string.sync_delete_success));
			
			tipReturn_whenRemoveDupl();
			return;
		}
		
		RequestParams params = new RequestParams();
		params.put("sp_id", bean.serial_product_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// 移除-重复标识
				Receive_palletsSn.removeRepeatMark(listPlts, pltBean, snNO);
				listSn.remove(pos);
				adpSN.notifyDataSetChanged();
				refSumUI();
				UIHelper.showToast(getString(R.string.sync_delete_success));
			
				tipReturn_whenRemoveDupl();
			}

		}.doPost(HttpUrlPath.deleteSN, params, mActivity);
	}
	
	private void tipReturn_whenRemoveDupl(){
		if(!pltBean.hasSn(sn_dupl))
		{
			RewriteBuilderDialog.Builder bd=RewriteBuilderDialog.newSimpleDialog(this, "Dulplicate SN removed, Return?", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			bd.hideCancelBtn();
			bd.show();
		}
	}

	// =========传参===============================
	
	private static List<Rec_PalletBean> listPlts_bridge;
	private static Rec_PalletBean pltBean_bridge;

	private Rec_ItemBean itemBean;
	private Rec_PalletBean pltBean;

	private List<Rec_PalletBean> listPlts;
	private String sn_dupl;				//重复的sn,待移除

	public static void initParams(Intent in, Rec_ItemBean itemBean, List<Rec_PalletBean> listPlts, Rec_PalletBean pltBean,
			String dulpSN) {
		in.putExtra("itemBean", itemBean);
//		in.putExtra("pltBean", pltBean);
//		in.putExtra("listPlts", (Serializable) listPlts);
		in.putExtra("dulpSN", dulpSN);
		
		//debug
		listPlts_bridge=listPlts;
		pltBean_bridge=pltBean;
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
//		pltBean = (Rec_PalletBean) params.getSerializable("pltBean");
		pltBean=pltBean_bridge;
		if (pltBean.sn == null)
			pltBean.sn = new ArrayList<Receive_palletsSn>();
		listSn = pltBean.sn;

//		listPlts = (List<Rec_PalletBean>) params.getSerializable("listPlts");
		listPlts=listPlts_bridge;
		sn_dupl=params.getString("dulpSN");
		
		//debug
		listPlts_bridge=null;
		pltBean_bridge=null;
	}

	// ==================nested===============================
	
	private String sn_lastScan="";		//上一scan的sn

	private BaseAdapter adpSN = new BaseAdapter() {

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_rec_sn, null);
				h = new Holder();
				h.vItem=convertView;
				h.btnDel = (Button) convertView.findViewById(R.id.btnDel);
				h.tvSN = (TextView) convertView.findViewById(R.id.tvSN);
				h.tvRepeat = convertView.findViewById(R.id.tvRepeat);
				h.vLastScan= convertView.findViewById(R.id.vLastScan);
				convertView.setTag(h);
			} else {
				h = (Holder) convertView.getTag();
			}

			// 刷ui
			Receive_palletsSn b = listSn.get(position);
			h.tvSN.setText("SN: " + b.serial_number + "");
			h.tvRepeat.setVisibility(b.is_repeat()||b.isEqual(sn_dupl) ? View.VISIBLE : View.GONE);
			h.vLastScan.setVisibility(b.isEqual(sn_lastScan)?View.VISIBLE:View.INVISIBLE);

			// 事件
			h.btnDel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDlg_delSn(position);
				}
			});

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listSn != null ? listSn.size() : 0;
		}
	};

	private class Holder {
		Button btnDel;
		TextView tvSN;
		View tvRepeat,vItem,vLastScan;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.lno_checkbox: // lotNO锁
			lockLotNo(isChecked);
			// 若开锁,提示扫描
			// if(!isChecked)
			// TTS.getInstance().speakAll_withToast("Scan Lot Number!");

			UIHelper.showToast(isChecked ? "Scan SN!" : "Scan Lot NO!");
			break;

		default:
			break;
		}

	}
	
	
}
