package oso.ui.load_receive.print;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.bean.ReceiptTicketBase;
import oso.ui.load_receive.print.iface.TicketData;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.OnInnerClickListener;
import support.key.EntryDetailNumberTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PrintTicketLoadDataActivity extends FragmentActivity implements
		TicketData {

	private static Context context;

	private String serverNameForSpinner;
	private String serverNameForSpinnerId;
	private Button close_pup;
	private Button submit;
	private SingleSelectBar checkinSelectBar;
	private boolean isPring = false;// 判断是否提交打印 true 为提交打印成功 false 为未提交以及失败
	int submitnum = 0;
	String str = "123456";
	private PrintPackMainBeanModel printTicketModel;
	private List<ReceiptBillBase> receiptsTicketPartenBaseList;


	public static final int PrintBillOfLoadingFragment = 231;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.print_receipt_ticket_layout);
		context = PrintTicketLoadDataActivity.this;
		getFromDockCheckInActivityData(this.getIntent());
		initview();
		barway();
	}

	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	protected void getFromDockCheckInActivityData(Intent intent) {
		checkinSelectBar = (SingleSelectBar) findViewById(R.id.check_in_select_bar);
		printTicketModel = (PrintPackMainBeanModel) intent.getSerializableExtra("printTicketModel");
		receiptsTicketPartenBaseList = printTicketModel.getReceiptsTicketBaseList();
		if(printTicketModel!=null)
			serverNameForSpinnerId = printTicketModel.getDefaultNum();
	}

	public void initview() {
		submit = ((Button) findViewById(R.id.submit));
		close_pup = (Button) findViewById(R.id.close_pup);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				submitjudgment();
			}
		});
		close_pup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
	}

	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;

	private void showPrinters() {
		List<PrintServer> printServer = printTicketModel.getParsePrintServer();
		if (Utility.isNullForList(printServer)) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,
				null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(context,
				printServer, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServer.size() < 3) ? dm.heightPixels / 7
				* printServer.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServer.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getDefaultPrint_index());
		// 初始-滑至可见
		listView.setSelection(getDefaultPrint_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (!StringUtil.isNullOfStr(serverNameForSpinnerId)
								&& !serverNameForSpinnerId.equals("0")) {
							if (submitnum == 1) {
								submitData_bill();
							} else if (submitnum == 2) {
								submitData_ticket();
							} else if (submitnum == 3) {
								submitData_counting();
							} else if (submitnum == 4) {
								pckingItemInterface.morepackinglist(serverNameForSpinnerId);
							}
							dialog.dismiss();
						} else {
							UIHelper.showToast(
									getString(R.string.sync_select_printserver));
						}

					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		builder.create().show();
	}

	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			serverNameForSpinnerId = printTicketModel.getParsePrintServer()
					.get(position).getPrinter_server_id();
			serverNameForSpinner = printTicketModel.getParsePrintServer()
					.get(position).getPrinter_server_name();
			printTicketModel.setDefaultNum(serverNameForSpinnerId);
			adpPrinters.setCurItem(position);
		};
	};

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getDefaultPrint_index() {
		if (printTicketModel.getParsePrintServer() == null)
			return -1;

		for (int i = 0; i < printTicketModel.getParsePrintServer().size(); i++) {
			String tmpId = printTicketModel.getParsePrintServer().get(i)
					.getPrinter_server_id();
			if (printTicketModel.getDefaultNum().equals(tmpId))
				return i;
		}
		return -1;
	}

	public void barway() {

		// spotStatus : all -1 , Ocuppied 2 ,No Ocuppied 1
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		// bollist[] = new PrintTicketLoadDataActivity();
		clickItems.add(new HoldDoubleValue<String, Integer>("BOL", 1));
		clickItems.add(new HoldDoubleValue<String, Integer>("Ticket", 2));
		clickItems.add(new HoldDoubleValue<String, Integer>("Counting", 3));
		clickItems.add(new HoldDoubleValue<String, Integer>("Packing", 4));
		checkinSelectBar.setUserDefineClickItems(clickItems);
		checkinSelectBar
				.setUserDefineCallBack(new SelectBarItemClickCallBack() {
					@Override
					public void clickCallBack(
							HoldDoubleValue<String, Integer> selectValue) {
						showTabView(selectValue.b, true);
						// UIHelper.showToast(context, selectValue.a,
						// Toast.LENGTH_SHORT).show();
					}
				});
		checkinSelectBar.userDefineSelectIndexExcuteClick(0);

	}

	private void showTabView(int index, boolean isrefreshViewType) {
		Fragment fragment = null;

		if (index == 1) {
			fragment = new PrintBillOfLoadingFragment();
			submitnum = 1;
		}

		if (index == 2) {
			fragment = new PrintLoadingTicketFragment();
			submitnum = 2;
		}

		if (index == 3) {
			fragment = new PrintCountingSheetFragment();
			submitnum = 3;
		}

		if (index == 4) {
			fragment = new PrintPackingFragment();
			submitnum = 4;
		}

		Bundle bundle = new Bundle();
		bundle.putSerializable("receiptTicketModel", printTicketModel);
		bundle.putSerializable("receiptsTicketPartenBaseList",
				(Serializable) receiptsTicketPartenBaseList);
		fragment.setArguments(bundle);
		// 改成fragment replace
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.checkin_park_l, fragment);
		transaction.commitAllowingStateLoss();
	}

	public void submitjudgment() {
		if (submitnum == 1) {
			List<ReceiptBillBase> receiptsTicketBaseList = getSubmitData();
			if (receiptsTicketBaseList == null) {
				UIHelper.showToast(context, getString(R.string.print_data_null),
						Toast.LENGTH_SHORT).show();
				return;
			}
		} else if (submitnum == 4) {
			if (pckingItemInterface.morepackinglist() <= 0) {
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
						context);
				Resources resources = context.getResources();
				builder.setCancelable(false);
				builder.setTitle(resources.getString(R.string.sys_holdon));
				builder.setMessage(getString(R.string.print_noselect_records));
				builder.setNegativeButton(
						resources.getString(R.string.sync_yes),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
				builder.create().show();
				return;
			}
		}
		showPrinters();
	}

	private void submitData_bill() {
		List<ReceiptBillBase> receiptsTicketBaseList = getSubmitData();
		if (receiptsTicketBaseList == null) {
			UIHelper.showToast(context, getString(R.string.print_data_null), Toast.LENGTH_SHORT)
					.show();
		} else {
			RequestParams params = new RequestParams();
			StringUtil.setLoginInfoParams(params);
			JSONObject datasJsonObject = new JSONObject();
			JSONArray jSONArray = new JSONArray();
			try {
				for (int i = 0; i < receiptsTicketBaseList.size(); i++) {
					ReceiptBillBase receiptsTicketBase = receiptsTicketBaseList
							.get(i);
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("checkDataType", "PICKUP");
					jsonObject.put("number", receiptsTicketBase.getLoad_no());// receiptsTicketBase.getDlo_detail_id());
					jsonObject.put("number_type",receiptsTicketBase.getNumber_type());
					jsonObject.put("door_name", printTicketModel.getDoor_name());
					jsonObject.put("companyId", printTicketModel.getCompanyid());
					jsonObject.put("customerId",printTicketModel.getCustomer_id());
					jsonObject.put("master_bol_nos",receiptsTicketBase.getMaster_bol_no());
					jsonObject.put("order_nos", printTicketModel.getFixOrder());
					jsonObject.put("order_no", printTicketModel.getFixOrder());
					jsonObject.put("resources_id", printTicketModel.getResources_id());
					jsonObject.put("resources_type", printTicketModel.getResources_type());
					jsonObject.put("path", receiptsTicketBase.getFinishpath());
					jsonObject.put("checkLen", 1);
					jSONArray.put(jsonObject);
				}

				datasJsonObject.put("datas", jSONArray);
				datasJsonObject.put("who", serverNameForSpinner);
				datasJsonObject.put("detail_id", printTicketModel.getDetail_id());
				datasJsonObject.put("resources_id", printTicketModel.getResources_id());
				datasJsonObject.put("resources_type", printTicketModel.getResources_type());
				datasJsonObject.put("print_server_id", serverNameForSpinnerId);
				datasJsonObject.put("entry_id", printTicketModel.getReceiptTicketBase().getEntryid());
				datasJsonObject.put("out_seal", StringUtil.isNullOfStr(printTicketModel.getOut_seal()) ? "NA"
						: printTicketModel.getOut_seal());
				datasJsonObject.put("container_no",printTicketModel.getContainer_no());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			params.add("values", datasJsonObject.toString());
			params.add("Method", HttpPostMethod.BillOfLoadingPrintTask);

			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
					isPring = true;
				}
			}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
		}
	}

	private List<ReceiptBillBase> getSubmitData() {
		List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
		if(receiptsTicketBaseList != null && receiptsTicketBaseList.size() > 0){
			receiptsTicketBaseList.clear();
		}
		if (receiptsTicketPartenBaseList != null
				&& receiptsTicketPartenBaseList.size() > 0) {
			for (int i = 0; i < receiptsTicketPartenBaseList.size(); i++) {
				if (receiptsTicketPartenBaseList.get(i).isChecked()
						&& !receiptsTicketPartenBaseList.get(i).isBolchecked()) {
					ReceiptBillBase receiptBillBase = new ReceiptBillBase();
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptBillBase.setLoad_no(receiptsTicketPartenBaseList.get(i).getLoad_no());
					receiptBillBase.setNumber_type(receiptsTicketPartenBaseList.get(i).getNumber_type());
					receiptBillBase.setCompanyid(receiptsTicketPartenBaseList.get(i).getCompanyid());
					receiptBillBase.setCustomerid(receiptsTicketPartenBaseList.get(i).getCustomerid());
					receiptBillBase.setMaster_bol_no(receiptsTicketPartenBaseList.get(i).getMaster_bol_no());
					receiptBillBase.setOrder_no(receiptsTicketPartenBaseList.get(i).getOrder_no());
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getMasterbolpath());
					receiptsTicketBaseList.add(receiptBillBase);
				} else if (receiptsTicketPartenBaseList.get(i).isBolchecked()
						&& !receiptsTicketPartenBaseList.get(i).isChecked()) {
					ReceiptBillBase receiptBillBase = new ReceiptBillBase();
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptBillBase.setLoad_no(receiptsTicketPartenBaseList.get(i).getLoad_no());
					receiptBillBase.setNumber_type(receiptsTicketPartenBaseList.get(i).getNumber_type());
					receiptBillBase.setCompanyid(receiptsTicketPartenBaseList.get(i).getCompanyid());
					receiptBillBase.setCustomerid(receiptsTicketPartenBaseList.get(i).getCustomerid());
					receiptBillBase.setMaster_bol_no(receiptsTicketPartenBaseList.get(i).getMaster_bol_no());
					receiptBillBase.setOrder_no(receiptsTicketPartenBaseList.get(i).getOrder_no());
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptsTicketBaseList.add(receiptBillBase);
				} else if (receiptsTicketPartenBaseList.get(i).isChecked()
						&& receiptsTicketPartenBaseList.get(i).isBolchecked()) {
					ReceiptBillBase receiptBillBase = new ReceiptBillBase();
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptBillBase.setLoad_no(receiptsTicketPartenBaseList.get(i).getLoad_no());
					receiptBillBase.setNumber_type(receiptsTicketPartenBaseList.get(i).getNumber_type());
					receiptBillBase.setCompanyid(receiptsTicketPartenBaseList.get(i).getCompanyid());
					receiptBillBase.setCustomerid(receiptsTicketPartenBaseList.get(i).getCustomerid());
					receiptBillBase.setMaster_bol_no(receiptsTicketPartenBaseList.get(i).getMaster_bol_no());
					receiptBillBase.setOrder_no(receiptsTicketPartenBaseList.get(i).getOrder_no());
					receiptBillBase.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptsTicketBaseList.add(receiptBillBase);

					ReceiptBillBase receiptBillBases = new ReceiptBillBase();
					receiptBillBases.setFinishpath(receiptsTicketPartenBaseList.get(i).getPath());
					receiptBillBases.setLoad_no(receiptsTicketPartenBaseList.get(i).getLoad_no());
					receiptBillBases.setNumber_type(receiptsTicketPartenBaseList.get(i).getNumber_type());
					receiptBillBases.setCompanyid(receiptsTicketPartenBaseList.get(i).getCompanyid());
					receiptBillBases.setCustomerid(receiptsTicketPartenBaseList.get(i).getCustomerid());
					receiptBillBases.setMaster_bol_no(receiptsTicketPartenBaseList.get(i).getMaster_bol_no());
					receiptBillBases.setOrder_no(receiptsTicketPartenBaseList.get(i).getOrder_no());
					receiptBillBases.setFinishpath(receiptsTicketPartenBaseList.get(i).getMasterbolpath());
					receiptsTicketBaseList.add(receiptBillBases);
				}
			}
		}
		return receiptsTicketBaseList != null
				&& receiptsTicketBaseList.size() > 0 ? receiptsTicketBaseList: null;
	}

	private void submitData_ticket() {
		RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params);
		params.add("Method", HttpPostMethod.LoadingTickPrintTask);
		params.add("who", serverNameForSpinner);
		params.add("print_server_id", serverNameForSpinnerId);
		boolean flag = printTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_LOAD;
		params.add("path", flag ? printTicketModel.getReceiptTicketBase().getPath() : "check_in/print_order_no_master_wms_order.html");
		params.add("window_check_in_time", printTicketModel.getReceiptTicketBase().getTime() + "");
		params.add("entryId", printTicketModel.getReceiptTicketBase().getEntryid() + "");
		params.add("loadNo", printTicketModel.getReceiptTicketBase().getLoad_no() + "");
		params.add("company_name", printTicketModel.getReceiptTicketBase().getCompany_name() + "");
		params.add("gate_container_no", printTicketModel.getReceiptTicketBase().getContainer_no() + "");
		params.add("seal", printTicketModel.getOut_seal() + "");
		params.add("CompanyID", printTicketModel.getCompanyid() + "");
		params.add("CustomerID", printTicketModel.getCustomer_id() + "");
		params.add("DockID", printTicketModel.getDoor_name() + "");
		params.add("number", String.valueOf(printTicketModel.getFixNumber()));
		params.add("number_type",String.valueOf(printTicketModel.getNumber_type()));
		params.add("order_no", String.valueOf(printTicketModel.getReceiptTicketBase().getOrder_no()));
		params.add("resources_id", String.valueOf(printTicketModel.getResources_id()));
		params.add("resources_type", String.valueOf(printTicketModel.getResources_type()));
		params.add("containerNo", String.valueOf(printTicketModel.getEquipment_number()));
		params.add("appointmentDate", String.valueOf(printTicketModel.getAppointment_date()));
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);

		// }

	}

	private void submitData_counting() {
		RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params);
		params.add("Method", HttpPostMethod.CountingSheetPrintTask);
		params.add("who", serverNameForSpinner);
		params.add("print_server_id", serverNameForSpinnerId);
		params.add("checkDataType", "LOAD");
		params.add("number", printTicketModel.getFixNumber());
		params.add("entryId", printTicketModel.getReceiptTicketBase().getEntryid() + "");
		params.add("door_name", printTicketModel.getDoor_name());
		params.add("CompanyID", printTicketModel.getCompanyid() + "");
		params.add("CustomerID", printTicketModel.getCustomer_id() + "");

		params.add("number_type",String.valueOf(printTicketModel.getNumber_type()));
		boolean flag = printTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_ORDER
				|| printTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_PONO;
		params.add("url", flag ? "a4_counting_sheet_order.html": "a4_counting_sheet.html");
		params.add("order", printTicketModel.getFixOrder());
		params.add("resources_id", String.valueOf(printTicketModel.getResources_id()));
		params.add("resources_type", String.valueOf(printTicketModel.getResources_type()));
		params.add("containerNo", String.valueOf(printTicketModel.getEquipment_number()));
		params.add("appointmentDate", String.valueOf(printTicketModel.getAppointment_date()));
		params.add("checkLen", 1 + "");
		params.add("isprint", 1 + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
			finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}

	@Override
	public void onItemSelected(String serverNameI) {
	}

	public static onKeyDownListener getOnKeyDownListener() {
		return onKeyDownListener;
	}

	public static void setOnKeyDownListener(onKeyDownListener onKeyDownListener) {
		PrintTicketLoadDataActivity.onKeyDownListener = onKeyDownListener;
	}

	private static onKeyDownListener onKeyDownListener;

	public interface onKeyDownListener {
		boolean onKeyDown(int keyCode, KeyEvent event);
	}

	private static PackingItemInterface pckingItemInterface;

	public static void setmorepackBack(PackingItemInterface pckingItemInterface) {
		PrintTicketLoadDataActivity.pckingItemInterface = pckingItemInterface;
	}

	public static PackingItemInterface getLoadToPrintBack() {
		return pckingItemInterface;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//--------------Bol单页面如果有更新的话则刷新当前bol主页面
		if (PrintTicketLoadDataActivity.PrintBillOfLoadingFragment == resultCode) {
			boolean updateFlag = data.getBooleanExtra("updateFlag", false);
			if(updateFlag){
				ReceiptTicketBase r = (ReceiptTicketBase) data.getSerializableExtra("ReceiptTicketBase");
				printTicketModel.setReceiptTicketBase(r);
				showTabView(submitnum, true);
			}			
		}
	}
}
