package oso.ui.load_receive;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.SearchAssignTaskActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskMainActivity;
import oso.ui.load_receive.do_task.receive.process.Rec_SearchRecCCNOAc;
import oso.ui.load_receive.do_task.receive.process.Rec_SearchRecNOAc;
import oso.ui.load_receive.photo.PhotoMainAc_LR;
import oso.ui.load_receive.print.PrintEntryActivity;
import oso.ui.load_receive.window.SearchCheckInWindowActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import declare.com.vvme.R;

public class LoadReceiveMainActivity extends BaseActivity implements OnClickListener {

	private TextView notifyTv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_inout_main, 0);
		setTitleString(getString(R.string.load_receive_title));
		initView();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// 刷cnt
		int count = PhotoMainAc_LR.getEntryCnt();
		if (count > 0)
			notifyTv.setText(String.valueOf(count));
		notifyTv.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
	}

	private void initView() {
		// 取view
		notifyTv = (TextView) findViewById(R.id.notifyTv);

		findViewById(R.id.btn_Assign).setOnClickListener(this);
		findViewById(R.id.btn_Process).setOnClickListener(this);
		findViewById(R.id.btn_print).setOnClickListener(this);
		findViewById(R.id.btn_CheckIn).setOnClickListener(this);
		findViewById(R.id.btn_Photo).setOnClickListener(this);
		findViewById(R.id.btn_SerialNOLP).setOnClickListener(this);
		findViewById(R.id.btn_CCNoLP).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_Assign:
			// if(!StoredData.canAccess_assignTask())
			// {
			// UIHelper.showToast("No Access!");
			// return;
			// }
			popTo(SearchAssignTaskActivity.class);
			break;
		case R.id.btn_Process:
			// if(!StoredData.canAccess_Process())
			// {
			// UIHelper.showToast("No Access!");
			// return;
			// }
			popTo(CheckInTaskMainActivity.class);
			break;
		case R.id.btn_print:
			popTo(PrintEntryActivity.class);
			break;
		case R.id.btn_CheckIn:
			// if(!StoredData.canAccess_WindowCheckin())
			// {
			// UIHelper.showToast("No Access!");
			// return;
			// }
			popTo(SearchCheckInWindowActivity.class);
			break;

		case R.id.btn_Photo:
			popTo(PhotoMainAc_LR.class);
			break;
		// case R.id.btn_Receive: //收货
		// popTo(InboundCheckInTransportActivity.class);
		// break;

		case R.id.btn_SerialNOLP:
			popTo(Rec_SearchRecNOAc.class);
			break;
		case R.id.btn_CCNoLP:
			popTo(Rec_SearchRecCCNOAc.class);
			break;

		default:
			break;
		}
	}

	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
