package oso.ui.load_receive.print.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;
 
public class PrintPackMainBeanModel implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 1L;
	private String defaultNum;//默认打印机
	private List<PrintServer> parsePrintServer;//打印机列表
	private String out_seal;
	private String door_name;

	private String detail_id;
	
	private int number_type;//类型key
	private String fixNumber;
	private String fixOrder;
	
	private String companyid;
	private String customer_id;
	
	private String resources_id;
	private String resources_type;
	
	private String equipment_number;
	private String appointment_date;
	
	private String container_no;
	
	private List<ReceiptBillBase> receiptsTicketBaseList;//打印BILL界面数据的数据集合
	private List<PrintPackinglistBean> printPackinglistBeanList;//packing_list数据的数据集合
	private ReceiptTicketBase receiptTicketBase;//loading_ticket的bean数据
	private PrintCountingsheetBase printCountingsheetBase;//counting_sheet的bean的数据
	private PrintdeLiveryLabelBean printdeLiveryLabelBean;//delivery_label的bean数据
	
	private String pathForCtn;

    //===============debug==========================
    public String receipt_no;
	
	public static int parseCount(JSONObject json) {
		return StringUtil.getJsonInt(json, "count");
	}

	public static PrintPackMainBeanModel parseJSON(JSONObject json) {
		PrintPackMainBeanModel r = new PrintPackMainBeanModel();
		
		r.setContainer_no(json.optString("container_no"));
		
		r.setDefaultNum(PrintPackMainBeanModel.getDefault(json));
		r.setParsePrintServer(PrintPackMainBeanModel.parsePrintServer(json));
		r.setReceiptsTicketBaseList(PrintPackMainBeanModel.parsePrintTicketBaseList(json));
		r.setPrintCountingsheetBase(PrintPackMainBeanModel.parsePrintCountBase(json));
		r.setPrintPackinglistBeanList(PrintPackMainBeanModel.parsePrintPackinglistBaseList(json));

		parsePrintData(json,r);
		
		r.setReceiptTicketBase(ReceiptTicketBase.parsePrintTicketBase(json));
		r.getReceiptTicketBase().setTrailer_loader(json.optInt("trailer_loader",0));
		r.getReceiptTicketBase().setFreight_counted(json.optInt("freight_counted",0));
		//--------判断该信息 并手动赋值 用于打印页面的window弹窗
		if(StringUtil.isNullOfStr(r.getReceiptTicketBase().getPrint_customer_id())){
			r.getReceiptTicketBase().setPrint_customer_id(r.getCustomer_id());
		}
		
		return r;
	}
	public static PrintPackMainBeanModel parseJSONs(JSONObject json) {
		PrintPackMainBeanModel r = new PrintPackMainBeanModel();
		r.setDefaultNum(PrintPackMainBeanModel.getDefault(json));
		r.setParsePrintServer(PrintPackMainBeanModel.parsePrintServer(json));
		r.setPrintdeLiveryLabelBean(PrintPackMainBeanModel.parsePrintDeliverylabelBase(json));
		return r;
	}
	/**
	 * @Description:解析获取delivery_label的bean
	 * @param @param json
	 * @param @return
	 */
	public static PrintdeLiveryLabelBean parsePrintDeliverylabelBase(JSONObject json) {
		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "delivery_label");
		PrintdeLiveryLabelBean printdeLiveryLabelBean = new PrintdeLiveryLabelBean();
		printdeLiveryLabelBean.setEntry_id(StringUtil.getJsonString(loading_ticket, "entry_id"));
		printdeLiveryLabelBean.setCompanyid(StringUtil.getJsonString(loading_ticket, "companyid"));
		printdeLiveryLabelBean.setAdid(StringUtil.getJsonInt(loading_ticket, "adid"));
		printdeLiveryLabelBean.setBol_number(StringUtil.getJsonString(loading_ticket, "bol_number"));
		printdeLiveryLabelBean.setCustomerid(StringUtil.getJsonString(loading_ticket, "customerid"));
		printdeLiveryLabelBean.setDoor_name(StringUtil.getJsonString(loading_ticket, "door_name"));
		printdeLiveryLabelBean.setPath(StringUtil.getJsonString(loading_ticket, "path"));
		printdeLiveryLabelBean.setCtnr_number(StringUtil.getJsonString(loading_ticket, "ctnr_number"));
		return printdeLiveryLabelBean;
	}
	/**
	 * @Description:解析获取counting_sheet的bean
	 * @param @param json
	 * @param @return
	 */
	public static PrintCountingsheetBase parsePrintCountBase(JSONObject json) {
		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "counting_sheet");
		PrintCountingsheetBase printCountingsheetBase = new PrintCountingsheetBase();
		printCountingsheetBase.setPath(StringUtil.getJsonString(loading_ticket, "path"));
		return printCountingsheetBase;
	}
	
	//-----------代码冗余 注释掉了
//	/**
//	 * @Description:解析获取loading_ticket的bean
//	 * @param @param json
//	 * @param @return
//	 */
//	public static ReceiptTicketBase parsePrintTicketBase(JSONObject json) {
//		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "loading_ticket");
//		ReceiptTicketBase receiptTicketBase = new ReceiptTicketBase();
//		receiptTicketBase.setEntryid(StringUtil.getJsonString(loading_ticket, "entryid"));
//		receiptTicketBase.setLoad_no(StringUtil.getJsonString(loading_ticket, "loadno"));
//		receiptTicketBase.setTime(StringUtil.getJsonString(loading_ticket, "window_check_in_time"));
//		receiptTicketBase.setContainer_no(StringUtil.getJsonString(loading_ticket, "gate_container_no"));
//		receiptTicketBase.setCompany_name(StringUtil.getJsonString(loading_ticket, "company_name"));
//		receiptTicketBase.setPath(StringUtil.getJsonString(loading_ticket, "path"));
//		receiptTicketBase.setOrder_no(StringUtil.getJsonString(loading_ticket, "order_no"));
//		return receiptTicketBase;
//	}
	
	/**
	 * @Description:解析获取data
	 * @param @param json
	 * @param @return
	 */
	public static void parsePrintData(JSONObject json,PrintPackMainBeanModel r) {
		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "data");
		r.setCompanyid(StringUtil.getJsonString(loading_ticket, "company_id"));
		r.setCustomer_id(StringUtil.getJsonString(loading_ticket, "customer_id"));
		r.setFixNumber(StringUtil.getJsonString(loading_ticket, "number"));
		r.setNumber_type(StringUtil.getJsonInt(loading_ticket, "number_type"));
		r.setDoor_name(StringUtil.getJsonString(loading_ticket, "door_name"));
		r.setFixOrder(StringUtil.getJsonString(loading_ticket, "order_no"));
		r.setOut_seal(StringUtil.getJsonString(loading_ticket, "out_seal"));
	}
	
	/**
	 * @Description:解析打印BILL界面数据的数据集合
	 * @param @param json
	 * @param @return
	 */
	public static List<ReceiptBillBase> parsePrintTicketBaseList(JSONObject json) {
		JSONArray receiptsTicketBaseArrays = StringUtil.getJsonArrayFromJson(json, "bill_of_lading");
		List<ReceiptBillBase> receiptsTicketBaseList = new ArrayList<ReceiptBillBase>();
		if(!StringUtil.isNullForJSONArray(receiptsTicketBaseArrays)){
			for (int i = 0; i < receiptsTicketBaseArrays.length(); i++) {
				JSONObject jsonItem = StringUtil.getJsonObjectFromArray(receiptsTicketBaseArrays, i);
				ReceiptBillBase receiptsTicketBase = new ReceiptBillBase();
				receiptsTicketBase.setMaster_bol_no(StringUtil.getJsonString(jsonItem, "master_bol_no"));
				receiptsTicketBase.setOrder_no(StringUtil.getJsonString(jsonItem, "order_no"));
				receiptsTicketBase.setPath(StringUtil.getJsonString(jsonItem, "bolpath"));
				receiptsTicketBase.setMasterbolpath(StringUtil.getJsonString(jsonItem, "masterbolpath"));
				receiptsTicketBase.setLoad_no(StringUtil.getJsonString(jsonItem, "number"));
				receiptsTicketBase.setNumber_type(StringUtil.getJsonInt(jsonItem, "number_type"));
				receiptsTicketBase.setCustomerid(StringUtil.getJsonString(jsonItem, "customerid"));
				receiptsTicketBase.setCompanyid(StringUtil.getJsonString(jsonItem, "companyid"));
				receiptsTicketBase.setBolname(StringUtil.getJsonString(jsonItem, "bolname"));
				receiptsTicketBase.setDoor_name(StringUtil.getJsonString(jsonItem, "door_name"));
				receiptsTicketBase.setShipToName(StringUtil.getJsonString(jsonItem, "ship_to_name"));
				receiptsTicketBase.setShipToID(StringUtil.getJsonString(jsonItem, "ship_to_id"));
				receiptsTicketBaseList.add(receiptsTicketBase);
			}
		}		
		return receiptsTicketBaseList;
	}
	/**
	 * @Description:解析packing_list界面数据的数据集合
	 * @param @param json
	 * @param @return
	 */
	public static List<PrintPackinglistBean> parsePrintPackinglistBaseList(JSONObject json) {
		JSONArray receiptsTicketBaseArrays = StringUtil.getJsonArrayFromJson(json, "packing_list");
		List<PrintPackinglistBean> receiptsTicketBaseList = new ArrayList<PrintPackinglistBean>();
		if(!StringUtil.isNullForJSONArray(receiptsTicketBaseArrays)){
			for (int i = 0; i < receiptsTicketBaseArrays.length(); i++) {
				JSONObject jsonItem = StringUtil.getJsonObjectFromArray(receiptsTicketBaseArrays, i);
				PrintPackinglistBean receiptsTicketBase = new PrintPackinglistBean();
				receiptsTicketBase.setOrder_no(StringUtil.getJsonString(jsonItem, "orderno"));
				receiptsTicketBase.setCustomerid(StringUtil.getJsonString(jsonItem, "customerid"));
				receiptsTicketBase.setCompanyid(StringUtil.getJsonString(jsonItem, "companyid"));
				receiptsTicketBaseList.add(receiptsTicketBase);
			}
		}		
		return receiptsTicketBaseList;
	}
	
	public static String getDefault(JSONObject json) {
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json, "printserver");
		return StringUtil.getJsonString(jo, "default");
	}

	/**
	 * @Description:获取打印机列表
	 * @param @param json
	 * @param @return
	 */
	public static List<PrintServer> parsePrintServer(JSONObject json) {
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json, "printserver");
		JSONArray ja = StringUtil.getJsonArrayFromJson(jo, "allprintserver");
		if(!StringUtil.isNullForJSONArray(ja)){
			List<PrintServer> printServerList = new ArrayList<PrintServer>();
			for (int i = 0; i < ja.length(); i++) {
				JSONObject j = StringUtil.getJsonObjectFromArray(ja, i);
				PrintServer ps = new PrintServer();
	 			ps.setPrinter_server_id(StringUtil.getJsonString(j, "printer_server_id"));
				ps.setPrinter_server_name(StringUtil.getJsonString(j, "printer_server_name"));
				ps.setZone(StringUtil.getJsonString(j, "zone"));
				printServerList.add(ps);
			}
			return printServerList;
		}
		return null;
	}
	

	public PrintCountingsheetBase getPrintCountingsheetBase() {
		return printCountingsheetBase;
	}

	public void setPrintCountingsheetBase(
			PrintCountingsheetBase printCountingsheetBase) {
		this.printCountingsheetBase = printCountingsheetBase;
	}


	public int getNumber_type() {
		return number_type;
	}

	public void setNumber_type(int number_type) {
		this.number_type = number_type;
	}

	public String getOut_seal() {
		return out_seal;
	}

	public void setOut_seal(String out_seal) {
		this.out_seal = out_seal;
	}

	public String getDoor_name() {
		return door_name;
	}

	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}

	public List<PrintPackinglistBean> getPrintPackinglistBeanList() {
		return printPackinglistBeanList;
	}

	public void setPrintPackinglistBeanList(
			List<PrintPackinglistBean> printPackinglistBeanList) {
		this.printPackinglistBeanList = printPackinglistBeanList;
	}

	public List<ReceiptBillBase> getReceiptsTicketBaseList() {
		return receiptsTicketBaseList;
	}

	public List<PrintServer> getParsePrintServer() {
		return parsePrintServer;
	}

	public void setParsePrintServer(List<PrintServer> parsePrintServer) {
		this.parsePrintServer = parsePrintServer;
	}

	public void setReceiptsTicketBaseList(
			List<ReceiptBillBase> receiptsTicketBaseList) {
		this.receiptsTicketBaseList = receiptsTicketBaseList;
	}

	public ReceiptTicketBase getReceiptTicketBase() {
		return receiptTicketBase;
	}

	public void setReceiptTicketBase(ReceiptTicketBase receiptTicketBase) {
		this.receiptTicketBase = receiptTicketBase;
	}

	public PrintdeLiveryLabelBean getPrintdeLiveryLabelBean() {
		return printdeLiveryLabelBean;
	}

	public void setPrintdeLiveryLabelBean(
			PrintdeLiveryLabelBean printdeLiveryLabelBean) {
		this.printdeLiveryLabelBean = printdeLiveryLabelBean;
	}

	public String getDefaultNum() {
		return StringUtil.isNullOfStr(defaultNum)?"":defaultNum;
	}

	public void setDefaultNum(String defaultNum) {
		this.defaultNum = defaultNum;
	}

	public String getPathForCtn() {
		return pathForCtn;
	}

	public void setPathForCtn(String pathForCtn) {
		this.pathForCtn = pathForCtn;
	}

	public String getFixNumber() {
		return fixNumber;
	}

	public void setFixNumber(String fixNumber) {
		this.fixNumber = fixNumber;
	}

	public String getFixOrder() {
		return fixOrder;
	}

	public void setFixOrder(String fixOrder) {
		this.fixOrder = fixOrder;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getDetail_id() {
		return detail_id;
	}

	public void setDetail_id(String detail_id) {
		this.detail_id = detail_id;
	}

	public String getResources_type() {
		return resources_type;
	}

	public void setResources_type(String resources_type) {
		this.resources_type = resources_type;
	}

	public String getResources_id() {
		return resources_id;
	}

	public void setResources_id(String resources_id) {
		this.resources_id = resources_id;
	}

	public String getEquipment_number() {
		return equipment_number;
	}

	public void setEquipment_number(String equipment_number) {
		this.equipment_number = equipment_number;
	}

	public String getAppointment_date() {
		return appointment_date;
	}

	public void setAppointment_date(String appointment_date) {
		this.appointment_date = appointment_date;
	}

	public String getContainer_no() {
		return container_no;
	}

	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}
}
