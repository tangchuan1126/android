package oso.ui.load_receive.do_task.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.bean.EntryTaskMainBean;
import oso.ui.load_receive.do_task.main.adapter.CheckInEquipmentListAdapter;
import oso.ui.load_receive.do_task.main.iface.CheckInTaskInterface;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.loadingview.LoadingView;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.photo.pager.ImagePagerAdapter;
import oso.widget.photo.pagerindicator.CirclePageIndicator;
import oso.widget.photo.pagerindicator.HackyViewPager;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.common.bean.CheckInTaskMainItem;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.OccupyTypeKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CheckInTaskMainActivity extends BaseActivity implements CheckInTaskInterface {

	private SearchEditText searchEt;
	private LinearLayout taskLayout;
	private ListView leftlistView;
	private ListView rightlistview;
	private TextView entryTv;
	private TextView nodataTv;

	private List<CheckInTaskBeanMain> taskmainlist = new ArrayList<CheckInTaskBeanMain>();
	private List<CheckInTaskBeanMain> equipmentlist = new ArrayList<CheckInTaskBeanMain>();
	private String general_entry_id;
	public final static int ITEMCLOSE = 120;

	private SingleSelectBar singleSelectBar;// 选项卡
	private ArrayList<View> mViewList = new ArrayList<View>();
	private ViewPager mViewPager;
	private View task, other;
	private TextView leftdata, rightdata;
	CheckInEquipmentListAdapter adapter;
	// CheckInTaskListAdapter mAdapter;

	private LinearLayout entry_layout;
	private ListView entry_lv;
	private List<EntryTaskMainBean> entryTaskList;
	private String truck_size;//车型

	private boolean showEntryList = true;// 判断时显示entry列表 还是equipment列表
											// true为entry列表

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_task_main_layout, 0);
		setTitleString(getString(R.string.task_title));
		initView();
		initViewPager();
		getDefaultData();
	}

	/**
	 * @Description:进入系统后默认的加载方法
	 * @param
	 */
	public void getDefaultData() {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingList");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				showEntryList = true;
				entry_layout.setVisibility(View.VISIBLE);
				taskLayout.setVisibility(View.GONE);

				entryTaskList = EntryTaskMainBean.helpJson(json);
				EntryTaskProcessAdapter adapter = new EntryTaskProcessAdapter(mActivity, entryTaskList);
				entry_lv.setAdapter(adapter);
				entry_lv.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						EntryTaskMainBean bean = entryTaskList.get(position);
						getData(bean.entry_id);
					}
				});
			}

			@Override
			public void handFail() {
				showEntryList = true;
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	// private boolean isFirstVisible = true; // 第一次oncreate

	// @Override
	// protected void onResume() {
	// // TODO Auto-generated method stub
	// super.onResume();
	// // 进来即刷新,除了第一次
	// if (!isFirstVisible)
	// getData(general_entry_id);
	//
	// isFirstVisible = false;
	// }

	public void initView() {
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				getDefaultData();
			}
		});
		entryTv = (TextView) findViewById(R.id.task_entry);
		taskLayout = (LinearLayout) findViewById(R.id.taskLayout);
		nodataTv = (TextView) findViewById(R.id.nodataTv);

		entry_layout = (LinearLayout) findViewById(R.id.entry_layout);
		entry_lv = (ListView) findViewById(R.id.entry_lv);
		entry_lv.setEmptyView((TextView) findViewById(R.id.any_data));

		searchEt = (SearchEditText) findViewById(R.id.searchEditTextmain);
		searchEt.setScanMode();
		searchEt.requestFocus();
		// searchEditTextmain.setOnKeyListener(new View.OnKeyListener() {
		// @Override
		// public boolean onKey(View v, int keyCode, KeyEvent event) {
		// if (Utility.isEnterUp(event)) {
		// getData(searchEditTextmain.getText().toString());
		// return true;
		// }
		// return false;
		// }
		// });
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getData(value);
			}
		});
	}

	/**
	 * @Description:输入EntryId请求数据
	 * @param checkin
	 */
	public void getData(final String checkin) {
		if (StringUtil.isNullOfStr(checkin)) {
			UIHelper.showToast(mActivity, getString(R.string.task_entryid_null), Toast.LENGTH_SHORT).show();
			return;
		}
		entry_layout.setVisibility(View.GONE);
		
		//-------使之为空
		truck_size = "";
		
		// debug,若清空 从下一页面返回时 然后刷新 若刷新没完成 点击设备会找不到该entry
		// general_entry_id = "";
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingListByEntry");
		params.add("entry_id", checkin + "");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				showEntryList = false;
				general_entry_id = checkin;
				Utility.colseInputMethod(mActivity, searchEt);

				taskmainlist = CheckInTaskBeanMain.handJsonForList(json);
				equipmentlist = CheckInTaskBeanMain.handJsonEquipmentsList(json);
				truck_size = json.optString("truck_size_id");//获取车型
				
				// mAdapter = new
				// CheckInTaskListAdapter(mActivity,CheckInTaskMainActivity.this,
				// taskmainlist);
				adapter = new CheckInEquipmentListAdapter(mActivity, equipmentlist);
				if (!Utility.isNullForList(taskmainlist) || !Utility.isNullForList(equipmentlist)) {
					leftlistView.setAdapter(mAdapter);
					rightlistview.setAdapter(adapter);
					entryTv.setText("E" + checkin);
					nodataTv.setVisibility(View.GONE);
					taskLayout.setVisibility(View.VISIBLE);
					singleSelectBar.setVisibility(View.VISIBLE);
				}

				if (taskmainlist != null && taskmainlist.size() > 0) {
					leftdata.setVisibility(View.GONE);
				} else {
					leftdata.setVisibility(View.VISIBLE);
				}
				if (equipmentlist != null && equipmentlist.size() > 0) {
					rightdata.setVisibility(View.GONE);
				} else {
					rightdata.setVisibility(View.VISIBLE);
				}

			}
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);

		searchEt.setText("");
		searchEt.requestFocus();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (StringUtil.isNullOfStr(general_entry_id)) {
			getDefaultData();
		} else {
			getData(general_entry_id);
		}
	}

	// @Override
	// protected void onBackBtnOrKey() {
	// if (taskmainlist != null && taskmainlist.size() > 0) {
	// // TODO Auto-generated method stub
	// RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
	// new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// CheckInTaskMainActivity.super.onBackBtnOrKey();
	// }
	// });
	// } else {
	// CheckInTaskMainActivity.super.onBackBtnOrKey();
	// }
	// }
	@Override
	public void DetailByEntry(final CheckInTaskBeanMain bean) {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingDetailByEntryEquipmentAndResouces");
		params.add("entry_id", general_entry_id + "");
		params.add("equipment_id", bean.equipment_id + "");
		params.add("resources_id", bean.resources_id + "");
		params.add("resources_type", bean.resources_type + "");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				CheckInTaskBeanMain.handJson(json, bean);
				if (!Utility.isNullForList(bean.complexDoorList)) {
					Intent intent = new Intent(mActivity, CheckInTaskDoorItemActivity.class);
					bean.entry_id = general_entry_id;
					intent.putExtra("taskmainlist", (Serializable) bean);
					intent.putExtra("truck_size", truck_size);
					intent.putExtra("mainnumber", 1);
					startActivityForResult(intent, ITEMCLOSE);
				}
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	boolean isDoor = false; // 当前为"load列表"
	int mPosition = -1;

	private BaseAdapter mAdapter = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			mPosition = position;
			// TODO Auto-generated method stub
			TaskHolder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_task_entry, null);
				holder = new TaskHolder();
				holder.resources_type_value = (TextView) convertView.findViewById(R.id.resources_type_value);
				holder.equipment_type_value = (TextView) convertView.findViewById(R.id.equipment_type_value);
				holder.resources_name = (TextView) convertView.findViewById(R.id.resources_name);
				holder.equipment_number = (TextView) convertView.findViewById(R.id.equipment_number);
				holder.total_task = (TextView) convertView.findViewById(R.id.total_tasks);
				holder.all_task = (TextView) convertView.findViewById(R.id.all_task);
				holder.finish_task = (TextView) convertView.findViewById(R.id.finish_task);
				holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
				holder.rel_type = (TextView) convertView.findViewById(R.id.rel_type);
				convertView.setTag(holder);
			} else {
				holder = (TaskHolder) convertView.getTag();
			}
			// 刷新-数据
			CheckInTaskBeanMain b = taskmainlist.get(position);

			if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
				holder.imageView1.setImageResource(R.drawable.ic_tractor);
			}
			if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
				holder.imageView1.setImageResource(R.drawable.ic_trailer);
			}
			if(b.resources_type == OccupyTypeKey.NULL){
				holder.resources_type_value.setText("");
			}else{
				holder.resources_type_value.setText(OccupyTypeKey
		                .getOccupyTypeKeyName(mActivity, b.resources_type) + ": ");
			}
			holder.equipment_type_value.setText(b.equipment_type_value + ": ");
			holder.resources_name.setText(b.resources_name + "");
			holder.equipment_number.setText(b.equipment_number + " ");
			holder.rel_type.setText("(" + CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(mActivity,b.rel_type) + ")");
			holder.finish_task.setText(b.finish_task + "");
			holder.all_task.setText(b.total_task + "");
			holder.total_task.setText(b.total_task + "");
			if (b.is_forget_close > 0) {
				try {
					XmlResourceParser xrp = getResources().getXml(R.drawable.text_font_red);
					ColorStateList csl = ColorStateList.createFromXml(getResources(), xrp);
					holder.resources_type_value.setTextColor(csl);
					holder.equipment_type_value.setTextColor(csl);
					holder.resources_name.setTextColor(csl);
					holder.equipment_number.setTextColor(csl);
				} catch (Exception e) {
				}
			}
			return convertView;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return taskmainlist.get(arg0);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return taskmainlist != null ? taskmainlist.size() : 0;
		}
	};

	private class TaskHolder {
		TextView resources_type_value, equipment_type_value;
		TextView resources_name, equipment_number, total_task, finish_task;
		ImageView imageView1;
		TextView rel_type;
		TextView all_task;
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) mActivity.findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.task_task_text), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.task_equipment_text), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void initViewPager() {

		LayoutInflater inflater = LayoutInflater.from(mActivity);
		task = inflater.inflate(R.layout.checkin_task_main_left_layout, null);
		other = inflater.inflate(R.layout.checkin_task_main_right_layout, null);
		mViewList.add(task);
		mViewList.add(other);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(new VPAdapter());
		initSingleSelectBar();
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				singleSelectBar.userDefineSelectIndexExcuteClick(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		mViewPager.setCurrentItem(0);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				mViewPager.setCurrentItem(selectValue.b);
			}
		});
		leftlistView = (ListView) task.findViewById(R.id.taskLv);
		rightlistview = (ListView) other.findViewById(R.id.taskLv);
		leftdata = (TextView) task.findViewById(R.id.no_data);
		rightdata = (TextView) other.findViewById(R.id.no_data);
		leftlistView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (Utility.isFastClick())
					return;
				CheckInTaskBeanMain bean = taskmainlist.get(arg2);
				showGateCheckInPhoto(bean);
			}
		});
	}

	private void showGateCheckInPhoto(final CheckInTaskBeanMain bean) {
		// final View contentView = View.inflate(mActivity,
		// R.layout.dlg_gatecheckin_photo, null);
		// final TabToPhoto ttp = (TabToPhoto)
		// contentView.findViewById(R.id.ttp);
		// List<TabParam> params = new ArrayList<TabParam>();
		// params.add(new TabParam("Gate Check In", null, new PhotoCheckable(0,
		// "Gate Check In", ttp)));
		// params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoGateCheckIn
		// + "", FileWithTypeKey.OCCUPANCY_MAIN + "", general_entry_id);
		// ttp.init(mActivity, params);
		// ttp.setAddBtnVisibility(false);
		// new BottomDialog(mActivity).setTitle("E" +
		// general_entry_id).setView(contentView).setCanceledOnTouchOutside(true).setShowNextBtn(true)
		// .setOnSubmitClickListener("Enter", new OnSubmitClickListener() {
		// @Override
		// public void onSubmitClick(BottomDialog dlg, String value) {
		// DetailByEntry(bean);
		// dlg.dismiss();
		// }
		// }).show();

		// v,debug
		final View v = View.inflate(mActivity, R.layout.ac_image_pager_task, null);
		final HackyViewPager vp = (HackyViewPager) v.findViewById(R.id.pager);
		final LoadingView lv = (LoadingView) v.findViewById(R.id.lv);
		lv.setShowBorder(false);
		lv.setLoadingTextColor(Color.WHITE);
		final CirclePageIndicator indicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
		View loGrp_dlg = v.findViewById(R.id.loGrp_dlg);
		int screenH = Utility.getScreenHeight(this);
		LayoutParams lp = loGrp_dlg.getLayoutParams();
		lp.height = screenH - Utility.dip2px(this, 180f);	
		
		final View loLoading_dlg=v.findViewById(R.id.loLoading_dlg);

		// vp
		List<String> list = new ArrayList<String>();
		ImagePagerAdapter adp = new ImagePagerAdapter(list, this);
		vp.setAdapter(adp);
		indicator.setViewPager(vp);
		int blue = getResources().getColor(R.color.sync_blue);
		// indicator.setPageColor(blue);
		indicator.setStrokeColor(blue);
		indicator.setFillColor(blue);

		// dlg
		new BottomDialog(mActivity).setTitle("E" + general_entry_id).setView(v).setCanceledOnTouchOutside(true).setShowCancelBtn(true)
				.setOnSubmitClickListener(getString(R.string.task_enter), new OnSubmitClickListener() {
					@Override
					public void onSubmitClick(BottomDialog dlg, String value) {
						DetailByEntry(bean);
						dlg.dismiss();
					}
				}).show();
		reqServerUrls(list, adp,loLoading_dlg);
	}

	// 拉取-服务端图片
	public void reqServerUrls(final List<String> list, final PagerAdapter adp,final View loLoading_dlg) {
		RequestParams p = NetInterface.getImgs_p(FileWithCheckInClassKey.PhotoGateCheckIn + "", FileWithTypeKey.OCCUPANCY_MAIN + "",
				general_entry_id, "", "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//隐藏加载数据框
				loLoading_dlg.setVisibility(View.GONE);

				JSONArray jaDatas = json.optJSONArray("datas");
				if (jaDatas == null || jaDatas.length() == 0)
					return;
				List<TTPImgBean> tmpList = new Gson().fromJson(jaDatas.toString(), new TypeToken<List<TTPImgBean>>() {
				}.getType());

				System.out.println("");
				for (int i = 0; i < tmpList.size(); i++)
					list.add(HttpUrlPath.basePath + tmpList.get(i).uri);
				adp.notifyDataSetChanged();
			}

			public void handFail() {
				System.err.println("nimei>>pv.reqServerUrls.fail");
			};
		}.doPost(HttpUrlPath.FileUpZipAction, p, this, true);
	}

	private class VPAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return mViewList.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {
			((ViewPager) container).addView(mViewList.get(position), 0);
			return mViewList.get(position);
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView(mViewList.get(position));
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	}

	@Override
	public void selectItem(CheckInTaskItemBeanMain item, int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectItem(CheckInTaskItemBeanMain item) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addLoadBatData(int load_bar_id, String cnt,boolean isChange) {
		// TODO Auto-generated method stub

	}

	@Override
	public void all_btn(CheckInTaskItemBeanMain item, int btn_type) {
		// TODO Auto-generated method stub

	}

	class EntryTaskProcessAdapter extends BaseAdapter {

		private List<EntryTaskMainBean> list;

		public EntryTaskProcessAdapter(Context context, List<EntryTaskMainBean> arrayList) {
			super();
			this.list = arrayList;
		}

		@Override
		public int getCount() {
			return list == null ? 0 : list.size();
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.entry_task_process_list_item_layout, null);
				holder = new Holder();

				holder.entry_id = (TextView) convertView.findViewById(R.id.entry_id);
				holder.assign_task = (TextView) convertView.findViewById(R.id.assign_task);
				holder.total_task = (TextView) convertView.findViewById(R.id.total_task);
				holder.addLinear = (LinearLayout) convertView.findViewById(R.id.addLinear);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			EntryTaskMainBean bean = list.get(position);
			holder.entry_id.setText("E" + bean.entry_id);
			holder.assign_task.setText(bean.task_closed + "");
			// holder.total_task.setText(bean.task_total+"");
			holder.total_task.setText(" / " + bean.task_total + "");

			addLinear(bean, holder.addLinear);

			return convertView;
		}

		public void addLinear(EntryTaskMainBean bean, LinearLayout addLinear) {
			if (addLinear != null) {
				addLinear.removeAllViews();
			}

			for (int i = 0; i < bean.complexDoorList.size(); i++) {
				CheckInTaskMainItem b = new CheckInTaskMainItem();
				b = bean.complexDoorList.get(i);
				View view = ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_cheakin_main_layout,
						null);
				ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView1);
				TextView tractor_txt = (TextView) view.findViewById(R.id.tractor_txt);
				TextView door_txt = (TextView) view.findViewById(R.id.door_txt);
				View dashed_line = (View) view.findViewById(R.id.dashed_line);
				dashed_line.setVisibility(View.GONE);
				door_txt.setVisibility(View.GONE);
				tractor_txt.setText(b.equipment_number);
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
					imageView1.setImageResource(R.drawable.ic_tractor);
				}
				if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
					imageView1.setImageResource(R.drawable.ic_trailer);
				}
				addLinear.addView(view);
			}
		}

		private class Holder {
			TextView entry_id;
			TextView assign_task;
			TextView total_task;
			LinearLayout addLinear;
		}
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
			if (showEntryList) {
				finish();
				overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
			} else {
				getDefaultData();
			}
	}

	@Override
	public void setnewnumberstates(int i) {
		// TODO Auto-generated method stub

	}

}
