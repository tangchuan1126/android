package oso.ui.load_receive.print.bean;

import java.io.Serializable;
 
public class PrintdeLiveryLabelBean implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
 
	private static final long serialVersionUID = 1L;
	
	private String entry_id;
	private String companyid;
	private String customerid;
	private int adid;
	private String bol_number;
	private String door_name;
	private String path;
	private String ctnr_number;
	public String getEntry_id() {
		return entry_id;
	}
	public void setEntry_id(String entry_id) {
		this.entry_id = entry_id;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public int getAdid() {
		return adid;
	}
	public void setAdid(int adid) {
		this.adid = adid;
	}
	public String getBol_number() {
		return bol_number;
	}
	public void setBol_number(String bol_number) {
		this.bol_number = bol_number;
	}
	public String getDoor_name() {
		return door_name;
	}
	public void setDoor_name(String door_name) {
		this.door_name = door_name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getCtnr_number() {
		return ctnr_number;
	}
	public void setCtnr_number(String ctnr_number) {
		this.ctnr_number = ctnr_number;
	}
	
	
}
