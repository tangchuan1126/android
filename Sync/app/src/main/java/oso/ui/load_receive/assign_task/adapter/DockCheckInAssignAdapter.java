package oso.ui.load_receive.assign_task.adapter;

import java.util.List;

import oso.ui.load_receive.assign_task.iface.NewDockCheckInAllocationInterface;
import support.common.bean.DockCheckInDoorBase;
import support.common.bean.DockCheckInDoorForLoadListBase;
import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import support.key.OccupyTypeKey;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class DockCheckInAssignAdapter extends BaseAdapter {

	private Context context;
	List<DockCheckInDoorBase> arrayList;
	private LayoutInflater inflater;
	private DockCheckInAssignAdapter oThis;
	private NewDockCheckInAllocationInterface dockCheckInAllocationInterface;
//	private DockEquipmentBaseBean equipment;
	
	
	public DockCheckInAssignAdapter(Context context, List<DockCheckInDoorBase> arrayList,
			NewDockCheckInAllocationInterface dockCheckInAllocationInterface) {
		super();
		this.context = context;
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(this.context);
		oThis = this;
//		this.equipment = equipment;
		this.dockCheckInAllocationInterface = dockCheckInAllocationInterface;
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public DockCheckInDoorBase getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		DockCheckInAssignHoder holder = null;
		if (convertView == null) {
			holder = new DockCheckInAssignHoder();
			convertView = inflater.inflate(R.layout.dock_checkin_assign_item, null);
			holder.resource_name = (TextView) convertView.findViewById(R.id.resource_name);
			holder.all_click = (View) convertView.findViewById(R.id.all_click);
			holder.chose_checkbox = (CheckBox) convertView.findViewById(R.id.chose_checkbox);
			holder.body_layout = (LinearLayout) convertView.findViewById(R.id.body_layout);
			holder.body = (View) convertView.findViewById(R.id.body);
			holder.assign_btn = (Button) convertView.findViewById(R.id.assign_btn);
			holder.change_door = (Button) convertView.findViewById(R.id.change_door);
			holder.show_operating = (View) convertView.findViewById(R.id.show_operating);
			convertView.setTag(holder);
		} else {
			holder = (DockCheckInAssignHoder) convertView.getTag();
		}

		final DockCheckInDoorBase groupBean = arrayList.get(position);
		
		if(groupBean.getResources_id()==0){
			holder.resource_name.setText("NA");
		}else{
			holder.resource_name.setText(OccupyTypeKey.getOccupyTypeKeyName(context,groupBean.getResources_type()) + " : " + groupBean.getResources_type_value());
		}

		setBodyLayout(holder.body_layout, groupBean);

		if(judgeAllTaskNoClose(groupBean.getLoadList())){
			holder.body.setBackgroundResource(R.drawable.assign_style_for_checkin_bottom_white);
			holder.resource_name.setTextColor(context.getResources().getColor(R.color.assign_changedoor_text_style));
			
			holder.show_operating.setVisibility(View.VISIBLE);
			holder.chose_checkbox.setVisibility(View.VISIBLE);
			holder.chose_checkbox.setChecked(groupBean.isHaveSelected()?true:false);
			holder.all_click.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					groupBean.setSelected(!groupBean.isSelected());
					groupBean.setHaveSelected(groupBean.isSelected());
					adjustSelectAll(position);
					oThis.notifyDataSetChanged();
				}
			});
			
			holder.body.setPadding(0, 0, 0, 0);
		}else{
			holder.body.setBackgroundResource(R.drawable.assign_style_for_checkin_bottom_gray);

			
			holder.resource_name.setTextColor(context.getResources().getColor(R.color.dark_gray));
			
			holder.show_operating.setVisibility(View.GONE);
			holder.chose_checkbox.setVisibility(View.GONE);
			holder.all_click.setOnClickListener(null);
			
			
			holder.body.setPadding(0, 0, 0, (int)(context.getResources().getDimension(R.dimen.assign_bottom_padding)));
		}
		
		holder.assign_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dockCheckInAllocationInterface.AssignLoader(groupBean);
			}
			
		});
		holder.change_door.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dockCheckInAllocationInterface.ChangeDoor(groupBean);
			}
		});
		
		return convertView;
	}

	private void setBodyLayout(LinearLayout body_layout, DockCheckInDoorBase temp) {
		body_layout.removeAllViews();
		List<DockCheckInDoorForLoadListBase> loadList = temp.getLoadList();
		if (!Utility.isNullForList(loadList)) {
			for (int i = 0; i < loadList.size(); i++) {
				boolean showLineForChild = (i != 0);
				body_layout.addView(getDetailView(loadList.get(i), temp, showLineForChild)); 
			}
		}
	}

	/**
	 * @Description:
	 * @param @param childBean
	 * @param @param groupBean
	 * @param @param showLineForChild
	 * @param @return
	 */
	private LinearLayout getDetailView(final DockCheckInDoorForLoadListBase childBean, final DockCheckInDoorBase groupBean, boolean showLineForChild) {

		DockCheckInAssignSubItem item = new DockCheckInAssignSubItem(context);
		// -------------是否显示虚线
		item.dashed_line.setVisibility(showLineForChild ? View.VISIBLE : View.GONE);
		// -------------设置资源名称
		item.child_door_name.setText(EntryDetailNumberTypeKey.getModuleName(childBean.getNumber_type()) + ":" + childBean.getId());
/*		
 * 		颜色太多暂时注掉
 * 		// false 蓝色、 true 红色 //-------------判断颜色
		boolean text_color_flag = (childBean.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_CTN
				|| childBean.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_BOL || childBean.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS);

		item.child_door_name.setTextColor(context.getResources().getColor(text_color_flag ? R.color.Red : R.color.checkin_blue));
*/
		item.child_door_name.setTextColor(context.getResources().getColor(R.color.checkin_blue));
		// -------------设置资源状态
		String numberState = null;
		if(childBean.getNumber_status()==EntryDetailNumberStateKey.Close||childBean.getNumber_status()==EntryDetailNumberStateKey.Exception){
			numberState = EntryDetailNumberStateKey.getType(context,childBean.getNumber_status())+" By";
		}else{
			numberState = EntryDetailNumberStateKey.getType(context,childBean.getNumber_status());
		}
		
		
		item.number_status.setText(numberState);
		// -------------设置指派人名称
		item.execute_user.setText(!StringUtil.isNullOfStr(childBean.getExecute_user())?childBean.getExecute_user():"");


		// -------------复选框事件 以及回显
		item.chose_child_door_checkbox.setOnCheckedChangeListener(null);
		item.chose_child_door_checkbox.setChecked(childBean.isSelect());
		item.chose_child_door_checkbox.setButtonDrawable(context.getResources().getDrawable(R.drawable.chickbox_style_forcheckin));

		//若未关闭
		if(judgeTaskNoClose(childBean.getNumber_status())){
			item.chose_child_door_checkbox.setVisibility(View.VISIBLE);
			item.chose_layout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					childBean.setSelect(!childBean.isSelect());
					groupBean.setSelected(judgeSelectAllFlag(groupBean.getLoadList()));
					groupBean.setHaveSelected(judgeHaveSelectFlag(groupBean.getLoadList()));
					oThis.notifyDataSetChanged();
				}
			});
			if(!StringUtil.isNullOfStr(childBean.getExecute_user())){
				item.chose_layout.setBackgroundResource(R.drawable.list_center_selector_gray_task);
			}
		}
		//已关闭
		else{
			item.chose_layout.setBackgroundResource(R.color.close_task_color);
			item.chose_child_door_checkbox.setVisibility(View.GONE);
			item.chose_layout.setOnClickListener(null);
			
			item.number_status.setTextColor(context.getResources().getColor(R.color.checkin_blue));
//			item.execute_user.setTextColor(context.getResources().getColor(R.color.checkin_blue));
		}
		
		//debug
		item.execute_user.setTextColor(context.getResources().getColor(R.color.checkin_blue));
		item.execute_user.setTextSize(11f);

		return item;
	}

	public void setArrayList(List<DockCheckInDoorBase> arrayList) {
		this.arrayList = arrayList;
	}

	public List<DockCheckInDoorBase> getArrayList() {
		return arrayList;
	}
	
	/** 
	 * @Description:根据-选中状态,调整"全选/全不选"
	 * @param @param groupPosition
	 */
	private void adjustSelectAll(int groupPosition) {
		if(!Utility.isNullForList(arrayList)){
			for(int i=0;i<arrayList.size();i++){
				if(i==groupPosition){
					DockCheckInDoorBase temp = arrayList.get(i);
					if(!Utility.isNullForList(temp.getLoadList())){
						for(int j=0;j<temp.getLoadList().size();j++){
							temp.getLoadList().get(j).setSelect(temp.isSelected());
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * @Description:判断全选按钮是否选中
	 * @param @return
	 */
	private boolean judgeSelectAllFlag(List<DockCheckInDoorForLoadListBase> list){
		if(!Utility.isNullForList(list)){
			int selectNum = 0;
			int noSelectNum = 0;
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelect()&&judgeTaskNoClose(list.get(i).getNumber_status())){
					selectNum++;
				}
				if(!judgeTaskNoClose(list.get(i).getNumber_status())){
					noSelectNum++;
				}
			}
			return (selectNum==(list.size()-noSelectNum))?true:false;
		}
		return false;
	}
	
	/**
	 * @Description:判断是否有选中的子选项
	 * @param @return
	 */
	private boolean judgeHaveSelectFlag(List<DockCheckInDoorForLoadListBase> list){
		if(!Utility.isNullForList(list)){
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelect()&&judgeTaskNoClose(list.get(i).getNumber_status())){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @Description:判断是否都已经处理完毕
	 * @param @return
	 */
	private boolean judgeAllTaskNoClose(List<DockCheckInDoorForLoadListBase> list){
		if(!Utility.isNullForList(list)){
			int num = 0;
			for(int i=0;i<list.size();i++){
				if(!judgeTaskNoClose(list.get(i).getNumber_status())){
					num++;
				}
			}
			if(num==list.size()){ return false;}
		}
		return true;
	}
	/**
	 * @Description:判断是不是未处理的  是否显示CheckBox
	 * @param @param status
	 * @param @return
	 */
	public static boolean judgeTaskNoClose(int status){
		if(status==1||status==2){
			return true;
		}
		return false;
	}
}

class DockCheckInAssignHoder {
	public View all_click;
//	public View change_door;
//	public View arrow;
	public CheckBox chose_checkbox;
//	public TextView equipment_info;
	public TextView resource_name;
	public LinearLayout body_layout;
	public View body;
	
	public View show_operating;
	public Button assign_btn;
	public Button change_door;
}