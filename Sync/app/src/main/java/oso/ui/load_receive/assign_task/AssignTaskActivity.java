package oso.ui.load_receive.assign_task;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.adapter.DockCheckInAssignAdapter;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.assign_task.iface.NewDockCheckInAllocationInterface;
import oso.ui.load_receive.do_task.main.CheckInTaskMainActivity;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import support.common.UIHelper;
import support.common.bean.DockCheckInBase;
import support.common.bean.DockCheckInDoorBase;
import support.common.bean.DockCheckInDoorForLoadListBase;
import support.common.bean.DockEquipmentBaseBean;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyStatusTypeKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * 
 * @ClassName: CheckInDemoActivity
 * @Description:
 * @author gcy
 * @date 2014年5月7日 下午4:10:35
 * 
 */
public class AssignTaskActivity extends BaseActivity implements NewDockCheckInAllocationInterface {

	private Context context;
	private	DockCheckInBase dockCheckInBase;
	private DockEquipmentBaseBean equipment;
	private ListView listView;
	private DockCheckInAssignAdapter newDockCheckinAdapter;
	private List<ResourceInfo> resourceList;
//	private int selectAdpPosition = -1; 
	public static final int FORRESULT = 101;
	public static final int ASSINGNTASK = 1;
	
	private int assignNum = 0;//用于判断当前页面有没有任务操作 如果有操作则返回上一页面时 进行刷新
	
	private TextView entry_id;
	private TextView equipment_number;
	private TextView resource_info;
	private ImageView equipment_type;
	
	//----------本Activity的标志
	public static final int MARK = 41035;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.assign_task_sub_layout,0);
		context = this;
		getFromActivityData(this.getIntent());// 获取来自于上一个页面的数据
		initView();
	}
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	protected void getFromActivityData(Intent intent) {
		dockCheckInBase = (DockCheckInBase) intent.getSerializableExtra("dockCheckInBase");
		equipment = (DockEquipmentBaseBean) intent.getSerializableExtra("equipment");
	}
	
	/**
	 * @Description:初始化主Ui的控件
	 */
	private void initView() {
		setTitleString(getString(R.string.assign_task_title));
		
		entry_id = (TextView)findViewById(R.id.entry_id);
		equipment_number = (TextView)findViewById(R.id.equipment_number);
		resource_info = (TextView)findViewById(R.id.resource_info);
		equipment_type = (ImageView)findViewById(R.id.equipment_type);
		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(equipment.equipment_type, equipment_type);
		
		
		entry_id.setText("E" + dockCheckInBase.getEntryId());
		equipment_number.setText(equipment.equipment_number);
		
		if(dockCheckInBase.resources_id!=0){
			String resource_info_str = " "+OccupyStatusTypeKey.getContainerTypeKeyValue(dockCheckInBase.occupy_status,mActivity)+" ";
			   	   resource_info_str += OccupyTypeKey.getOccupyTypeKeyName(mActivity,dockCheckInBase.resources_type)+" "+dockCheckInBase.resources_name;
			resource_info.setText(resource_info_str);
		}
		
		
		listView = (ListView)findViewById(R.id.show_data);
		listView.setEmptyView(findViewById(R.id.any_data));
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
		newDockCheckinAdapter = new DockCheckInAssignAdapter(context,dockCheckInBase.getTreeList(),this);
		listView.setAdapter(newDockCheckinAdapter);
	}

	/**
	 * 处理返回其他activity关闭后返回的相关信息
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case FORRESULT:
			getFromActivityData(data);
			break;
			
		case ChooseUsersActivity.requestCode:
			RefreshLoaderForAdp(data);
			break;
			
		case CheckInTaskMainActivity.ITEMCLOSE:
			reData();
			break;
			
		case SelectDoorActivity.SELECTDOOR:
			DockCheckInBase base = (DockCheckInBase) data.getSerializableExtra("dockCheckInBase");
			if(base!=null){
				dockCheckInBase = base;
				if(newDockCheckinAdapter!=null){
					newDockCheckinAdapter.setArrayList(dockCheckInBase.getTreeList());
					newDockCheckinAdapter.notifyDataSetChanged();
				}
			}
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * 改变资源所在的实际位置
	 */
	@Override
	public void ChangeDoor(final DockCheckInDoorBase temp){
		if(!Utility.isFastClick(1000)){
			
			if(newDockCheckinAdapter==null){
				return;
			}
		
			if(temp==null){
				UIHelper.showToast(context, getString(R.string.assign_task_errordata), Toast.LENGTH_SHORT);
				return;
			}
			if (Utility.isNullForList(temp.getLoadList())) {
				UIHelper.showToast(context, getString(R.string.assign_task_nodata), Toast.LENGTH_SHORT);
				return;
			}
			if (!JudgeHaveSelectFlag(temp.getLoadList())) {
				UIHelper.showToast(mActivity, getString(R.string.assign_task_nobox), Toast.LENGTH_SHORT);
				return;
			}
			
			if(Utility.isNullForList(resourceList)){
				RequestParams params = new RequestParams();
				params.add("Method", HttpPostMethod.GetDoorAndSpot);
//				params.add("request_type", "1");
				params.add("request_type", "2"); // 1:可用res 2:所有
				params.add("occupy_type", OccupyTypeKey.DOOR + "");
		 		params.add("entry_id", dockCheckInBase.getEntryId());
				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						if(!Utility.isNullForList(resourceList)){
							resourceList.clear();
						}
						resourceList = DockHelpUtil.handjson_big(json);
						jumpToSelectDoor(temp);
					}
				}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, context);
			}else{
				jumpToSelectDoor(temp);
			}
		}
	}
	
	private void jumpToSelectDoor(DockCheckInDoorBase temp){
		Intent intent = new Intent(mActivity, SelectDoorActivity.class);
		
		IntentSelectDoorBean bean = new IntentSelectDoorBean();
		bean.resources_id = temp.getResources_id();
		bean.resources_type = temp.getResources_type();
		bean.resources_type_value = temp.getResources_type_value();
		
		bean.equipment_id = equipment.equipment_id;
		bean.equipment_number = equipment.equipment_number;
		bean.equipment_type = equipment.equipment_type;
		bean.equipment_type_value = equipment.equipment_type_value;
		bean.check_in_entry_id = dockCheckInBase.getEntryId();
		
		bean.intentType =  SelectDoorActivity.DockCheckInAllocationFlag;
		
		
		intent.putExtra("dockCheckInDoorBase",(Serializable)temp);
		intent.putExtra("intentSelectDoorBean",(Serializable)bean);
		intent.putExtra("resourceList", (Serializable)resourceList);
		startActivityForResult(intent,SelectDoorActivity.SELECTDOOR);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	@Override
	public void AssignLoader(final DockCheckInDoorBase temp){
		if(!Utility.isFastClick(1000)){
//			selectAdpPosition = position;
			if(newDockCheckinAdapter==null){
				return;
			}
			
//			DockCheckInDoorBase temp = newDockCheckinAdapter.getArrayList().get(selectAdpPosition);
			
			if(temp==null){
				UIHelper.showToast(context, getString(R.string.assign_task_errordata), Toast.LENGTH_SHORT);
				return;
			}
			if (Utility.isNullForList(temp.getLoadList())) {
				UIHelper.showToast(context, getString(R.string.assign_task_nodata), Toast.LENGTH_SHORT);
				return;
			}
			if (!JudgeHaveSelectFlag(temp.getLoadList())) {
				UIHelper.showToast(mActivity, getString(R.string.assign_task_nobox), Toast.LENGTH_SHORT);
				return;
			}

			//----初始化选择人员后的事件
			ChooseUsersActivity.setOnSubmitClickListener(new ChooseUsersActivity.OnSubmitClickListener() {
				@Override
				public void onClick(View v, final Intent data, final ChooseUsersActivity c) {
					//------如果传过来的参数为空则不能继续执行
					if (data == null) {
						UIHelper.showToast(getString(R.string.sync_data_error));
						return;
					}

					//------弹出提示框
					RewriteBuilderDialog.Builder builder = new
							RewriteBuilderDialog.Builder(c);
					builder.setTitle(getString(R.string.sync_notice));
					builder.setMessage(getString(R.string.sync_submit) + "?");
					builder.setCancelable(false);
					builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//--------确认指派
							assignSubmit(temp, data, c);
						}
					});
					builder.setNegativeButton(getString(R.string.sync_no), null);
					builder.create().show();
				}
			});

			
			Intent intent = new Intent(context, ChooseUsersActivity.class);
			intent.putExtra("role_ids",  BaseDataDepartmentKey.Warehouse);
			intent.putExtra("select_key", ChooseUsersActivity.KEY_DOCK_CHECK_IN);
			intent.putExtra("single_select", true);
			intent.putExtra("proJsId", BaseDataSetUpStaffJobKey.Employee);
			intent.putExtra("entry_id", dockCheckInBase.getEntryId());
			intent.putExtra("equipment_id", String.valueOf(equipment.equipment_id));
			intent.putExtra("DockCheckInDoorBase", temp);
			intent.putExtra("fromWhichActivity", MARK);
			intent.putExtra("buttonName", getString(R.string.assign_task_assign));
			startActivityForResult(intent, ChooseUsersActivity.requestCode);
			overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
		}
	}

	/**
	 * 确认指派人员的请求事件
	 * @param temp
	 * @param data
	 * @param c
	 */
	private void assignSubmit(DockCheckInDoorBase temp,final Intent data,final ChooseUsersActivity c){
		 RequestParams params = new RequestParams();
		 JSONArray jSONArray = new JSONArray();
		 try {
			 for (int j = 0; j < temp.getLoadList().size(); j++) {
				 DockCheckInDoorForLoadListBase d = temp.getLoadList().get(j);
				 if (d.isSelect() &&
					 DockCheckInAssignAdapter.judgeTaskNoClose(d.getNumber_status())) {
					 JSONObject jsonObject = new JSONObject();
					 jsonObject.put("detail_id", d.getDlo_detail_id());
					 jsonObject.put("isupdate", StringUtil.isNullOfStr(d.getExecute_user()) ? "0" : "1");
					 jSONArray.put(jsonObject);
				 }
			 }
		 } catch (JSONException e) {
			 e.printStackTrace();
		 }
		 params.add("values", jSONArray.toString());
		 params.add("Method", "AssginTaskByEntry");
		 params.add("entry_id", dockCheckInBase.getEntryId());
		 params.add("execute_user_ids", data.getStringExtra("select_user_adids"));
		 params.add("equipment_id", String.valueOf(equipment.equipment_id));
		 new SimpleJSONUtil() {
		 @Override
		 public void handReponseJson(JSONObject json) {
			 DockCheckInBase dockCheckInBases = DockCheckInBase.parsingJSON(json,dockCheckInBase.getEntryId());
			 Intent i = new Intent();
			 i.putExtra("dockCheckInBase", dockCheckInBases);
			 i.putExtra("resoures_text_id", data.getStringExtra("resoures_text_id"));
			 i.putExtra("resoures_hide_id", data.getStringExtra("resoures_hide_id"));
			 i.putExtra("select_user_names", data.getStringExtra("select_user_names"));
			 i.putExtra("select_user_adids", data.getStringExtra("select_user_adids"));
			 c.finish();
			 RefreshLoaderForAdp(i);
		 }
		 }.doPost(HttpUrlPath.AndroidAssignTaskAction, params, c);
		}

	/**
	 * @Description:判断是否有选择的数据
	 * @param @param list
	 * @param @return
	 */
	private boolean JudgeHaveSelectFlag(List<DockCheckInDoorForLoadListBase> list){
		if(!Utility.isNullForList(list)){
			for(int i=0;i<list.size();i++){
				if(list.get(i).isSelect()){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @Description:刷新选择loader后的数据集合
	 * @param @param data 从选择人员界面关闭后所传回的Intent
	 */
	private void RefreshLoaderForAdp(Intent data){		
		assignNum++;//判断已操作
		dockCheckInBase = (DockCheckInBase) data.getSerializableExtra("dockCheckInBase");
		if(newDockCheckinAdapter!=null){
			newDockCheckinAdapter.setArrayList(dockCheckInBase.getTreeList());
			newDockCheckinAdapter.notifyDataSetChanged();
		}
	}
	
	// ==================nested===============================
	
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}
	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		Intent data = new Intent(mActivity,SearchAssignTaskActivity.class);
		data.putExtra("refresh", (assignNum>0/*如果有操作则刷新*/));
		setResult(SearchAssignTaskActivity.AssignTaskListActivity, data);
		finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

	
	private void reData(){
		if(!StringUtil.isNullOfStr(equipment.check_in_entry_id)){
			RequestParams params = new RequestParams();
	  		params.add("Method", "AssginTaskDetailByEntryAndEquipment");
			params.add("entry_id", equipment.check_in_entry_id);	
			params.add("equipment_id", equipment.equipment_id+"");	
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					dockCheckInBase = DockCheckInBase.parsingJSON(json,  equipment.check_in_entry_id);
					if(newDockCheckinAdapter!=null){
						newDockCheckinAdapter.setArrayList(dockCheckInBase.getTreeList());
						newDockCheckinAdapter.notifyDataSetChanged();
					}
				}
			}.doPost(HttpUrlPath.AndroidAssignTaskAction, params, mActivity);
    	}
	}
}
