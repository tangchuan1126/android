package oso.ui.load_receive.do_task.receive.samsung.bean;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Ctnr implements Serializable {

	private static final long serialVersionUID = -8403801596509660006L;

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getDlo_detail_id() {
		return dlo_detail_id;
	}

	public void setDlo_detail_id(String dlo_detail_id) {
		this.dlo_detail_id = dlo_detail_id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getNumber_type() {
		return number_type;
	}

	public void setNumber_type(String number_type) {
		this.number_type = number_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus_int() {
		return status_int;
	}

	public void setStatus_int(String status_int) {
		this.status_int = status_int;
	}

	public static void parseBeans(JSONArray ja, List<Ctnr> list) {
		if (ja == null)
			return;

		for (int i = 0; i < ja.length(); i++) {
			Ctnr load = new Ctnr();
			parseBean(ja.optJSONObject(i), load);
			list.add(load);
		}
	}

	public static void parseBean(JSONObject jo, Ctnr l) {
		l.setAccount_id(jo.optString("account_id"));
		l.setCompany_id(jo.optString("company_id"));
		l.setCustomer_id(jo.optString("customer_id"));
		l.setDlo_detail_id(jo.optString("dlo_detail_id"));
		l.setNumber(jo.optString("number"));
		l.setNumber_type(jo.optString("number_type"));
		l.setStatus(jo.optString("status"));
		l.setStatus_int(jo.optString("status_int"));
		l.setIc_id(jo.optString("ic_id"));
	}

	public String getIc_id() {
		return ic_id;
	}

	public void setIc_id(String ic_id) {
		this.ic_id = ic_id;
	}

	public String getEquipment_id() {
		return equipment_id;
	}

	public void setEquipment_id(String equipment_id) {
		this.equipment_id = equipment_id;
	}

	public String getResources_type() {
		return resources_type;
	}

	public void setResources_type(String resources_type) {
		this.resources_type = resources_type;
	}

	public String getResources_id() {
		return resources_id;
	}

	public void setResources_id(String resources_id) {
		this.resources_id = resources_id;
	}

	private String account_id;
	private String company_id;
	private String customer_id;
	private String dlo_detail_id;
	private String number; // CTNR
	private String number_type;
	private String status;
	private String status_int;
	private String ic_id;
	private String equipment_id;
	private String resources_type;
	private String resources_id;

}
