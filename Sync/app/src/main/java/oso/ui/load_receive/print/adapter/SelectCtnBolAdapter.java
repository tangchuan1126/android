package oso.ui.load_receive.print.adapter;

import java.util.List;

import oso.ui.load_receive.print.bean.SelectCtnBolBase;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;
 
public class SelectCtnBolAdapter extends BaseAdapter {
	
	private List<SelectCtnBolBase> arrayList;
	private LayoutInflater inflater;
	private Context context;
	private Resources resources;
	
	public SelectCtnBolAdapter(Context context, List<SelectCtnBolBase> arrayList ) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		resources = context.getResources();
		this.inflater  = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public SelectCtnBolBase getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		SelectCtnHoder holder = null;
		 
		 if(convertView==null){
			holder = new SelectCtnHoder();
			convertView = inflater.inflate(R.layout.printiconbar_select_load_layout_listview_item,null);
			holder.company_id = (TextView) convertView.findViewById(R.id.company_id);
			holder.load_no = (TextView) convertView.findViewById(R.id.load_no);
			holder.customer_id = (TextView) convertView.findViewById(R.id.customer_id);
 			convertView.setTag(holder);
		}else{
			holder = (SelectCtnHoder) convertView.getTag();
		}
		 
		final SelectCtnBolBase temp  =  arrayList.get(position);
 
		holder.company_id.setText(temp.getCompanyid()+"");
		holder.load_no.setText(temp.getBolno()+"");
		holder.customer_id.setText(temp.getCustomerid()+"");
	 
 		return convertView;
	}
 
}
class SelectCtnHoder {
	public TextView company_id;
	public TextView load_no;
	public TextView customer_id;

}
