package oso.ui.load_receive.print;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.iface.RefreshConditionIface;
import oso.ui.load_receive.print.util.PrintOtherCondition;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.sign.SignatureBoard;
import oso.widget.sign.SignatureBoard.OnSignListener;
import support.common.UIHelper;
import support.common.print.OnInnerClickListener;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PrintPreViewWebViewActivity extends BaseActivity implements RefreshConditionIface{

	private String serverNameForSpinner;
	private String serverNameForSpinnerId;
	
	private WebView show_webview;
	private PrintPackMainBeanModel receiptTicketModel;
	private ReceiptBillBase receiptBillBase;
	// private String htmlurl;
	// private String master_bol_nos;
	private SignatureBoard sb;

	private FrameLayout loadingLayout;
	private TextView psTv;
	private int type;

	private Context context;

	private PrintOtherCondition printOtherCondition;
	private int trailer_loader;
	private int freight_counted;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_preview_webview_layout,0);
		context = PrintPreViewWebViewActivity.this;
		Intent intent = getIntent();
		type = intent.getIntExtra("type", 1);
		receiptTicketModel = (PrintPackMainBeanModel) intent
				.getSerializableExtra("receiptTicketModel");
		receiptBillBase = (ReceiptBillBase) intent
				.getSerializableExtra("receiptBillBase");
		if(receiptTicketModel!=null)
			serverNameForSpinnerId = receiptTicketModel.getDefaultNum();
		initView();
	}

	private void initView() {
		setTitleString(getString(R.string.sync_preview));
		
		printOtherCondition = (PrintOtherCondition) findViewById(R.id.PrintOtherCondition);
		if(printOtherCondition==null){
			return;
		}
		if(printOtherCondition.jumpShowOtherCondition(receiptTicketModel.getReceiptTicketBase())){
			printOtherCondition.setVisibility(View.VISIBLE);
			((View) findViewById(R.id.line)).setVisibility(View.VISIBLE);
			printOtherCondition.init(receiptTicketModel.getReceiptTicketBase(),this,this);
		}
		trailer_loader = receiptTicketModel.getReceiptTicketBase().getTrailer_loader();
		freight_counted = receiptTicketModel.getReceiptTicketBase().getFreight_counted();
		
		
		psTv = (TextView) findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) findViewById(R.id.loadingLayout);
		
		showRightButton(R.drawable.print_all_style, "", new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPrinters();
			}
		});
		
		show_webview = (WebView) findViewById(R.id.show_webview);
		show_webview.getSettings().setJavaScriptEnabled(true);
		show_webview.addJavascriptInterface(new JsOperation(), "Android");
		WebSettings ws = show_webview.getSettings();
		ws.setSupportZoom(true);
		ws.setBuiltInZoomControls(true);

		final Activity activity = this;

		sb = new SignatureBoard(this);
		sb.setOnSignListener(new OnSignListener() {
			@Override
			public void onSignSuccessed(String savePath, Bitmap bm,String imgUrl) {
				// 刷新页面
				// loadingLayout.setVisibility(View.VISIBLE);
				// show_webview.reload();

				// debug
				show_webview.loadUrl("javascript:androidGetImage('"
						+ relativeValue_last + "','"+key_last+"','"+imgUrl+"')");
			}
		});
		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});

		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				UIHelper.showToast(activity, "Oh no! " + description,
						Toast.LENGTH_SHORT).show();
			}
		});
		/******************************** 拼接JSON字符串 ******************************/
		StringBuilder url = new StringBuilder(HttpUrlPath.basePath);
		if (type == 1) {
			url.append(receiptBillBase.getMasterbolpath());
		} else {
			url.append(receiptBillBase.getPath());
		}

		url.append("?out_seal=").append(receiptTicketModel.getOut_seal());
		url.append("&entry_id=").append(
				receiptTicketModel.getReceiptTicketBase().getEntryid());
		url.append("&isprint=1");
		if (receiptTicketModel != null && receiptTicketModel.getContainer_no() != null) {
			url.append("&container_no=" + receiptTicketModel.getContainer_no().replace("#", "%67"));
		} else {
			url.append("&container_no=" + receiptTicketModel.getContainer_no());
		}
		//url.append("&container_no="+receiptTicketModel.getContainer_no());
		// url.append("&number_type=").append(receiptTicketModel.getNumber_type());
		url.append(Utility.getSharedPreferencesUrlValue(
				PrintPreViewWebViewActivity.this, "adid"));

		JSONArray jSONArray = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("checkDataType", "PICKUP");
			jsonObject.put("number", receiptTicketModel.getFixNumber());
			jsonObject.put("number_type", receiptTicketModel.getNumber_type());
			jsonObject.put("door_name", receiptTicketModel.getDoor_name());
			jsonObject.put("companyId", receiptTicketModel.getCompanyid());
			jsonObject.put("customerId", receiptTicketModel.getCustomer_id());
			jsonObject.put("checkLen", "1");
			jsonObject.put("master_bol_nos", receiptBillBase.getMaster_bol_no());
			jsonObject.put("order_nos", receiptTicketModel.getFixOrder());
			jsonObject.put("order_no", receiptTicketModel.getFixOrder());
			jsonObject.put("resources_id", receiptTicketModel.getResources_id());
			jsonObject.put("resources_type", receiptTicketModel.getResources_type());
			jSONArray.put(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		url.append("&jsonString=").append(jSONArray.toString());
		/************************************************************************/
		show_webview.loadUrl(url.toString());
		System.out.println("签名网址= " + url.toString());
		WebSettings settings = show_webview.getSettings();
		settings.setUseWideViewPort(true);

		settings.setLoadWithOverviewMode(true);

		imgBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
	}

	private String relativeValue_last; // 上一签名-位置
	private String key_last;

	public class JsOperation {
		// javascriptInterface
		public void androidSign(String relativeValue, String key, String type) {
			// UIHelper.showToast(getApplicationContext(), "relativeValue= " +
			// relativeValue + "\nkey= " + key + "\ntype= " + type, Toast.LENGTH_SHORT).show();
			if (sb == null) {
				return;
			}
			sb.init(relativeValue, key, type);
			sb.show();

			relativeValue_last = relativeValue;
			key_last = key;
		}
	}
	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;

	private void showPrinters() {
		List<PrintServer> printServer = receiptTicketModel.getParsePrintServer();
		if (Utility.isNullForList(printServer)) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(context,printServer, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServer.size() < 3) ? dm.heightPixels / 7* printServer.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServer.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getDefaultPrint_index());
		// 初始-滑至可见
		listView.setSelection(getDefaultPrint_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(!StringUtil.isNullOfStr(serverNameForSpinnerId)&&!serverNameForSpinnerId.equals("0")){
							submitData_bill();
							dialog.dismiss();
						}else{
							UIHelper.showToast(getString(R.string.sync_select_printserver));
						}
						
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) { }
				});
		builder.create().show();
	}
	
	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getDefaultPrint_index() {
		if (receiptTicketModel.getParsePrintServer() == null)
			return -1;

		for (int i = 0; i < receiptTicketModel.getParsePrintServer().size(); i++) {
			String tmpId = receiptTicketModel.getParsePrintServer().get(i).getPrinter_server_id();
			if (receiptTicketModel.getDefaultNum().equals(tmpId))
				return i;
		}
		return -1;
	}
	
	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			serverNameForSpinnerId = receiptTicketModel.getParsePrintServer().get(position).getPrinter_server_id();
			serverNameForSpinner = receiptTicketModel.getParsePrintServer().get(position).getPrinter_server_name();
			receiptTicketModel.setDefaultNum(serverNameForSpinnerId);
			adpPrinters.setCurItem(position);
		};
	};
	private void submitData_bill() {
				RequestParams params = new RequestParams();
				StringUtil.setLoginInfoParams(params);
				JSONObject datasJsonObject = new JSONObject();
				JSONArray jSONArray = new JSONArray();
				try {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("checkDataType", "PICKUP");
						jsonObject.put("number",receiptBillBase.getLoad_no());// receiptsTicketBase.getDlo_detail_id());
						jsonObject.put("number_type",receiptBillBase.getNumber_type());
						jsonObject.put("door_name", receiptTicketModel.getDoor_name());
						jsonObject.put("companyId",receiptTicketModel.getCompanyid());
						jsonObject.put("customerId",receiptTicketModel.getCustomer_id());
						jsonObject.put("master_bol_nos",receiptBillBase.getMaster_bol_no());
						jsonObject.put("order_nos",receiptTicketModel.getFixOrder());
						jsonObject.put("order_no",receiptTicketModel.getFixOrder());
						jsonObject.put("resources_id", receiptTicketModel.getResources_id());
						jsonObject.put("resources_type", receiptTicketModel.getResources_type());
						if (type == 1) {
							jsonObject.put("path", receiptBillBase.getMasterbolpath());
						} else {
							jsonObject.put("path", receiptBillBase.getPath());
						}
						jsonObject.put("checkLen", 1);
						jSONArray.put(jsonObject);

					datasJsonObject.put("datas", jSONArray);
					datasJsonObject.put("who", serverNameForSpinner);
					datasJsonObject.put("detail_id", receiptTicketModel.getDetail_id());
					datasJsonObject.put("print_server_id",serverNameForSpinnerId);
					datasJsonObject.put("entry_id", receiptTicketModel.getReceiptTicketBase().getEntryid());
					datasJsonObject.put("out_seal",StringUtil.isNullOfStr(receiptTicketModel.getOut_seal()) ? "NA" : receiptTicketModel.getOut_seal());
					datasJsonObject.put("container_no",receiptTicketModel.getContainer_no());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				params.add("values", datasJsonObject.toString());
				params.add("Method", HttpPostMethod.BillOfLoadingPrintTask);

				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
						finish();
					}
				}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, context);
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}

	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		Intent data = new Intent(mActivity,PrintTicketLoadDataActivity.class);
		data.putExtra("ReceiptTicketBase", (Serializable)receiptTicketModel.getReceiptTicketBase());
		boolean updateFlag = false;
		if(trailer_loader != receiptTicketModel.getReceiptTicketBase().getTrailer_loader()||freight_counted != receiptTicketModel.getReceiptTicketBase().getFreight_counted()){
			updateFlag = true;
		}
		data.putExtra("updateFlag", updateFlag);
		setResult(PrintTicketLoadDataActivity.PrintBillOfLoadingFragment, data);
		finish();
		overridePendingTransition(R.anim.push_from_right_out,
				R.anim.push_from_right_in);
	}
	
	/**
	 * 刷新当前页面
	 */
	@Override
	public void RefreshValue() {
			show_webview.reload();
	}
}
