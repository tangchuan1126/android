package oso.ui.load_receive.do_task.receive.wms.util;

import android.text.TextUtils;

/**
 * 位置
 * Created by Administrator on 2015-6-5.
 */
public class LocBean {

    //位置类型,用于location_type
    public static final int LocType_Location=1;
    public static final int LocType_Staging=2;

    public int locType;         //位置类型,取locType_x
    public String name;
    public int id;

    public LocBean(String name,int locType,int id){
        this.name=name;
        this.locType=locType;
        this.id=id;
    }

    public LocBean(String name,boolean isAtLocation,int id){
        this.name=name;
        this.locType=isAtLocation?LocType_Location:LocType_Staging;
        this.id=id;
    }

    public boolean isAtLocation(){
        //向上兼容,以前tmp没区分
        return locType!=LocBean.LocType_Staging;
    }

    //===========static===============================

    public static boolean isValid(LocBean loc){
        return loc!=null && !TextUtils.isEmpty(loc.name);
    }

}
