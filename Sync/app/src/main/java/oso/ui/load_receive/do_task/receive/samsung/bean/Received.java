package oso.ui.load_receive.do_task.receive.samsung.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Received implements Serializable {

	private static final long serialVersionUID = -5546661754380728915L;
	
	public static final String AREA_DEF = "NA";

	public Received() {
	}

	public Received(String pn) {
		this.pallet_number = pn;
		this.area = AREA_DEF;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getIcp_id() {
		return icp_id;
	}

	public void setIcp_id(String icp_id) {
		this.icp_id = icp_id;
	}

	public String getIc_id() {
		return ic_id;
	}

	public void setIc_id(String ic_id) {
		this.ic_id = ic_id;
	}

	public String getPallet_number() {
		return pallet_number;
	}

	public void setPallet_number(String pallet_number) {
		this.pallet_number = pallet_number;
	}

	public List<Received> getReceivedList() {
		List<Received> datas = new ArrayList<Received>();
		return datas;
	}

	public boolean isScan() {
		return isScan;
	}

	public void setScan(boolean isScan) {
		this.isScan = isScan;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	private String area;
	private String icp_id;
	private String ic_id;
	private String pallet_number;
	private String box_number;
	private String weight;

	// samsung load用
	private boolean isScan;
}
