package oso.ui.load_receive.window.util;

import java.util.List;

import oso.ui.load_receive.window.bean.InfoBean;
import support.key.ModuleKey;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class SelectOrderTypeAdapter extends BaseAdapter {

	private Context context;
	private List<InfoBean> infoList;
	private int type;

	public SelectOrderTypeAdapter(Context c, List<InfoBean> infos, int t) {
		infoList = infos;
		context = c;
		type = t;
	}

	@Override
	public int getCount() {
		return infoList.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.item_select_data, null);
			holder = new Holder();
			holder.numTv = (TextView) convertView.findViewById(R.id.numTv);
			holder.numTypeTv = (TextView) convertView.findViewById(R.id.numTypeTv);
			holder.searchNoTypeTv = (TextView) convertView.findViewById(R.id.searchNoTypeTv);
			holder.companyidTv = (TextView) convertView.findViewById(R.id.companyidTv);
			holder.customeridTv = (TextView) convertView.findViewById(R.id.customeridTv);
			holder.orderTv = (TextView) convertView.findViewById(R.id.orderTv);
			holder.poTv = (TextView) convertView.findViewById(R.id.poTv);
			convertView.setTag(holder);
		} else
			holder = (Holder) convertView.getTag();

		final InfoBean bean = infoList.get(position);
		holder.numTv.setText(bean.number);
		holder.numTypeTv.setText(ModuleKey.getCheckInModuleKey(bean.order_type));
		holder.searchNoTypeTv.setText(ModuleKey.getCheckInModuleKey(bean.search_number_type));
		holder.companyidTv.setText(bean.companyid);
		holder.customeridTv.setText(bean.customerid);

		if (type != 0) {
			holder.orderTv.setText(bean.supplierid);
			holder.poTv.setText(bean.receiptno);
			return convertView;
		}

		if ((bean.orderno == null || TextUtils.isEmpty(bean.orderno)) && (bean.orders != null && !bean.orders.isEmpty())) {
			if (bean.orders.size() == 1)
				holder.orderTv.setText(bean.orders.get(0).orderno);
			else
				holder.orderTv.setText(bean.orders.get(0).orderno + "...(" + bean.orders.size() + ")");
		} else {
			holder.orderTv.setText(bean.orderno);
		}

		if ((bean.pono == null || TextUtils.isEmpty(bean.pono)) && (bean.ponos != null && !bean.ponos.isEmpty())) {
			if (bean.ponos.size() == 1)
				holder.poTv.setText(bean.ponos.get(0).pono);
			else
				holder.poTv.setText(bean.ponos.get(0).pono + "...(" + bean.ponos.size() + ")");
		} else {
			holder.poTv.setText(bean.pono);
		}
		return convertView;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return infoList.get(position);
	}

	private class Holder {
		TextView numTv, numTypeTv, searchNoTypeTv, companyidTv, customeridTv, orderTv, poTv;
	}

}
