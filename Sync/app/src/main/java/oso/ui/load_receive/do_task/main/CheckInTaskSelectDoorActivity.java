package oso.ui.load_receive.do_task.main;

import java.io.Serializable;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.AssignTaskActivity;
import oso.ui.load_receive.assign_task.SelectDoorActivity;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.FragSelRes;
import support.common.UIHelper;
import support.common.bean.DockCheckInBase;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CheckInTaskSelectDoorActivity extends BaseActivity {
	private List<ResourceInfo> doorlist;
	private ResourceInfo selectDoorInfo;

	private IntentSelectDoorBean intentSelectDoorBean;

	public static final int DockCheckInAllocationFlag = 1;
	public static final int AssignActivityFlag = 2;
	public static final int CheckInTaskDoorItemActivityFlag = 3;
	public static final int SELECTDOOR = 102;
	
	private FragSelRes fragRes;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_task_select_door, 0);
		initData();
		setView();
	}

	private void initData() {
		intentSelectDoorBean = (IntentSelectDoorBean) getIntent().getSerializableExtra("intentSelectDoorBean");
		doorlist = (List<ResourceInfo>) getIntent().getSerializableExtra("resourceList");
		setTitleString(getString(R.string.shuttle_select_door));
		fragRes=(FragSelRes)getFragmentManager().findFragmentById(R.id.fragRes);
		fragRes.init(doorlist,(intentSelectDoorBean!=null)?intentSelectDoorBean.resources_id:0);
	}
	

	private void setView() {
		((TextView)findViewById(R.id.entry_id_tx)).setText(intentSelectDoorBean.check_in_entry_id);
		((TextView)findViewById(R.id.equipment_type)).setText(intentSelectDoorBean.equipment_type_value);
		((TextView)findViewById(R.id.equipment_tx)).setText(intentSelectDoorBean.equipment_number);
		((TextView)findViewById(R.id.resource)).setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,intentSelectDoorBean.resources_type)+" ["+intentSelectDoorBean.resources_type_value+"]");
		
	}
	

	public void addTaskOnClick(View v) {
		 selectDoorInfo=fragRes.getSelectRes();
		if (selectDoorInfo == null) {
			UIHelper.showToast(mActivity,OccupyTypeKey.getPrompt(mActivity,OccupyTypeKey.DOOR), Toast.LENGTH_SHORT).show();
			return;
		}
		RequestParams params = new RequestParams();
		String method = "";
		String url = "";
		String from = "";
		switch (intentSelectDoorBean.intentType) {
		case DockCheckInAllocationFlag:;				
		case AssignActivityFlag:
			from = "supervisor";
			method = "AssignTaskMoveToDoor";
			url = HttpUrlPath.GCMAction;
			break;
		case CheckInTaskDoorItemActivityFlag:
			from = "";
			method = "TaskProcessingMoveToDoor";
			url = HttpUrlPath.AndroidTaskProcessingAction;
			break;
		default:
			finish();
			break;
		}
		params.add("Method", method);
		params.add("entry_id", intentSelectDoorBean.check_in_entry_id);
		params.add("from", from);
		params.add("equipment_id", intentSelectDoorBean.equipment_id+"");
		params.add("moved_resource_type", intentSelectDoorBean.resources_type+"");
		params.add("moved_resource_id", intentSelectDoorBean.resources_id+"");
		params.add("sd_id", String.valueOf(selectDoorInfo.resource_id));
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				jumpToAnotherActivity(json);
			}
		}.doGet(url, params, mActivity);
	}
	/**
	 * @Description:根据条件跳转到entry所对应的类型
	 * @param flag
	 */
	public void jumpToAnotherActivity(JSONObject json){	
		Intent intent = new Intent();
		switch (intentSelectDoorBean.intentType) {
		case DockCheckInAllocationFlag:		
		case AssignActivityFlag:
			DockCheckInBase dockCheckInBase = DockCheckInBase.parsingJSON(json, intentSelectDoorBean.check_in_entry_id);
			if(dockCheckInBase!=null){
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				intent.setClass(mActivity,AssignTaskActivity.class); 
				intent.putExtra("dockCheckInBase", dockCheckInBase);
				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
			}else{
				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		case CheckInTaskDoorItemActivityFlag:
			if(json.optInt("ret")==1){
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				intent.setClass(mActivity,CheckInTaskDoorItemActivity.class);
				
				intentSelectDoorBean.resources_id = selectDoorInfo.resource_id;
				intentSelectDoorBean.resources_type_value = selectDoorInfo.resource_name+"";
				intentSelectDoorBean.resources_type = selectDoorInfo.resource_type;
				intent.putExtra("intentSelectDoorBean", (Serializable) intentSelectDoorBean);//给 李俊昊
				
				setResult(SelectDoorActivity.SELECTDOOR, intent);
				finish();
				overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
			}else{

				UIHelper.showToast(mActivity, getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
				closeThisActivity();
			}
			break;
		default:
			finish();
			break;
		}
	}
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}
	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		finish();
		overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
	}
	/**
	 * @Description:初始化singleBarSelect
	 */
}
