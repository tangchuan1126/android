package oso.ui.load_receive.do_task.load.scan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.bean.Load_useBean;
import oso.ui.load_receive.print.ReceiptsTicketDatasActivity;
import oso.ui.load_receive.print.bean.ReceiptTicketModel;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.LoadBarLayout;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.TempPhoto;
import support.common.UIHelper;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInTaskBeanMain;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.EntryDetailNumberTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.OccupyTypeKey;
import support.key.TTPKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * load_close页面
 * 
 * @author 朱成
 * @date 2014-12-2
 */
public class LoadToPrintActivity extends BaseActivity implements OnClickListener {
	private static int OPENRTDATAS = 103;
	/*****************************************************/
	// private static int OPEN_CAMERA_ACTIVITY = 3;
	// private LinearLayoutAddPhotoSmall linearLayoutPhoto;
	private TempPhoto tempPhoto;
	// private PhotoLinearLayoutCallBack layoutCallBack;
	private LoadBarLayout loadbar;
	private TabToPhoto ttp;

	/**********************************************/
	private Button btnPrint;
	private TextView btnClose, tvFreight, tvCustomer;
	// etSeal
	private EditText etProNo;
	// loLoadbar,loClose
	private View loFreight;
	private Spinner spLoadbar;
	private CheckInLoad_ComplexSubLoads complexSubLoads;

	private boolean isPrint = false;

	// private static List<CheckInLoadBarBean> barlist;
	// private static List<String> barlisttext;

	public final static int LoadCloseNotifyStayOrLeave = 1; // 关闭load的时候提示停留或者的是离开
															// 当前最后一个单据下最后一个子单据
															// line number 759
	public final static int LoadCloseNotifyReleaseDoor = 2; // 关闭load的时候提示是否relaeaseDoor
															// ,当前门下面最后一个子单据
															// line number 729
	public final static int LoadCloseNoifyNormal = 3; // 关闭load的时候 如果不是最后一个（door
														// 或者是Entry ）正常 line
														// number 354
	public final static int LoadCloseNotifyInputSeal = 4; // 本单据下面最后一个子单据的时候提示输入Seal
	public final static int LoadClose_DirectFinishDoor = 5; // 结束door,不弹框

	private static Context context;
	public static Resources resources;

	public Button set_na, btnBolOrOrder,btnProNo;

	private boolean isLastLoad_lastMol = false; // 为最后一个load,最后一个bol

	private EditText etExReason;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_load_toprint, 0);
		applyParams();
		context = LoadToPrintActivity.this;
		resources = getResources();
		if (complexSubLoads == null) {
			finish();
			return;
		}
		// title
		String title_1 = complexSubLoads.load.getMainType() == EntryDetailNumberTypeKey.CHECK_IN_LOAD ? "Load" : "Order";
		setTitleString(title_1 + ": " + complexSubLoads.load.getMainNo());

		// 取view
		loadbar = (LoadBarLayout) findViewById(R.id.loadbar);
		ttp = loadbar.getTabToPhoto();
		btnPrint = (Button) findViewById(R.id.btnPrint);
		btnClose = (TextView) findViewById(R.id.close_text);
		etProNo = (EditText) findViewById(R.id.pono_txt);
		loFreight = findViewById(R.id.loFreight);
		tvFreight = (TextView) findViewById(R.id.tvFreight);
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		TextView tvProNo_title = (TextView) findViewById(R.id.tvProNo_title);
		btnBolOrOrder = (Button) findViewById(R.id.btnBolOrOrder);
		
		btnProNo = (Button) findViewById(R.id.btnProNo);
		set_na = (Button) findViewById(R.id.set_na);
		set_na.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etProNo.setText("NA");
				etProNo.setSelection("NA".length());// 重新设置光标位置
			}
		});
		etExReason = (EditText) findViewById(R.id.etExReason);

		// 监听事件
		btnBolOrOrder.setOnClickListener(this);
		btnProNo.setOnClickListener(this);
		btnPrint.setOnClickListener(this);
		btnClose.setOnClickListener(this);
		loFreight.setOnClickListener(this);
		findViewById(R.id.btnPrintPackingList).setOnClickListener(this);

		// 初始化
		tvCustomer.setText(complexSubLoads.customer_id);
		tvProNo_title.setText(Utility.getCompoundText("* ", "Pro NO: ", new ForegroundColorSpan(0xff000000)));
		if(!TextUtils.isEmpty(complexSubLoads.getCurSubLoad().pro_no) && !complexSubLoads.getCurSubLoad().pro_no.equalsIgnoreCase("NA")){
			etProNo.setText(complexSubLoads.getCurSubLoad().pro_no);
			
			btnProNo.setVisibility(View.VISIBLE);
			btnProNo.setText("WMS Pro:"+complexSubLoads.getCurSubLoad().pro_no);
		}else{
			etProNo.setText(complexSubLoads.getCurSubLoad().getCommonProNo());
		}
		Utility.endEtCursor(etProNo);
		btnBolOrOrder.setText(getBolOrOrder(true));

		// 若已关闭过 则禁用close
		if (complexSubLoads.getCurSubLoad().isClosed()) {
			// 禁止-点击
			// btnClose.setEnabled(false);
			btnClose.setTextColor(Color.RED);
		}

		loadbar.init(complexSubLoads.doorBean, complexSubLoads.taskBean.number);
		ttp.init(mActivity, getTabParamList(complexSubLoads.entry_id));
//		ttp.setCompress(false);
		// loadbar.getTabToPhoto().setBackgroundColor(Color.WHITE);

		// 若为最后一个load 且为最后一个bol
		isLastLoad_lastMol = complexSubLoads.isLastLoad_andLastSubLoad();
		if (isLastLoad_lastMol) {
			loFreight.setVisibility(View.VISIBLE);
			tvFreight.setText(complexSubLoads.load.freight_term);
		} else {
			loFreight.setVisibility(View.GONE);
		}

		isNormalClose = complexSubLoads.getCurSubLoad().isToNormalClose();
		etExReason.setVisibility(isNormalClose ? View.GONE : View.VISIBLE);
		btnClose.setText(isNormalClose ? "Close" : "Exception Close");
	}

	private List<TabParam> getTabParamList(String entry) {
		String dlo= complexSubLoads.taskBean.dlo_detail_id;
		String customer=complexSubLoads.getCurSubLoad().customerid+"";
		
		List<TabParam> params = new ArrayList<TabParam>();
		String loadKey = TTPKey.getTaskProcessKey(entry);
		String countSheetKey = TTPKey.getCountSheetKey(complexSubLoads.entry_id,dlo,customer);

		TabParam p1 = new TabParam("Task Processing", loadKey, new PhotoCheckable(0, "TaskProcessing", ttp));
//		p1.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry);
		//debug
		p1.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry,dlo);
		//debug
//		p1.disable_autoUpload=true;
		
		String str0 = "Tally Sheet";
		String str1 = " *";
		Spannable sp = Utility.getCompoundText(str0, str1, new ForegroundColorSpan(Color.RED));
		Utility.addSpan(sp, str0, str1, new RelativeSizeSpan(1.2f));
		CharSequence tab2 = complexSubLoads.getCurSubLoad().hasScaned1Pallet() ? sp : str0;
		TabParam p2 = new TabParam(tab2, countSheetKey, new PhotoCheckable(1, "CountingSheet", ttp));
//		p2.setZoomMaxSize(200); //压缩200KB左右
		p2.maxSize_upload=new PointF(1500f,1500f);
		p2.setWebImgsParams(FileWithCheckInClassKey.PhotoCountingSheet + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry,dlo,customer);
		//debug
//		p2.disable_autoUpload=true;
		params.add(p1);
		params.add(p2);
		return params;
	}

	private boolean isNormalClose;

	public String getBolOrOrder(boolean withKey) {
		boolean isLoad = complexSubLoads.load.isMainType_load();
		String orderNo = complexSubLoads.load.getMainNo();
		String tmpStr = complexSubLoads.getCurSubLoad().getBolOrOrder(isLoad, orderNo, withKey);
		return tmpStr;
	}

	private boolean isDoorFinish_ret = false; // door下都已完成

	// 结束并调至指定页
	private void finishAndJump() {
		// 若door下都关了,则至搜entry
		if (isDoorFinish_ret) {
			CheckInTaskDoorItemActivity.toLastAc(this);
		}
		// 所有子单据ok
		else if (complexSubLoads.isAllClosed()) {
			// 返回至task,并刷新
			Intent intent = new Intent(context, CheckInTaskDoorItemActivity.class);
			intent.putExtra("refresh", true);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
		}
		// 若有未关,则到"子单据-列表"
		else
			Utility.popTo(this, SelectSubLoadActivity.class);
		finish();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_CANCELED)
			return;

		ttp.onActivityResult(requestCode, resultCode, data);

		if (requestCode == OPENRTDATAS) {
			if (resultCode == RESULT_OK) {
				isPrint = data.getBooleanExtra("isPrint", false);
			}
		}
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ttp.clearCache();
				LoadToPrintActivity.super.onBackBtnOrKey();
			}
		});
	}

	// 验证-表单
	// 返回值:true(表单有效)
	private boolean checkForm() {
		
		boolean hasScaned1Pallet= complexSubLoads.getCurSubLoad().hasScaned1Pallet();
		if (hasScaned1Pallet && ttp.getPhotoNames(1, true).size() < 1) {
//			UIHelper.showToast(this, "At lease upload 1 Tally Sheet Photo!");
			RewriteBuilderDialog.showSimpleDialog_Tip(context, "At lease upload 1 Tally Sheet Photo!");
			return false;
		}
//		FileUtil.createZipFile("798", ttp.getAllTabPhotoFiles());
		
		String proNo = etProNo.getEditableText().toString();
		String expReason = etExReason.getEditableText().toString();

		if (!isNormalClose && TextUtils.isEmpty(expReason)) {
//			UIHelper.showToast(this, "");
			RewriteBuilderDialog.showSimpleDialog_Tip(context, getString(R.string.sync_reason_null));
			return false;
		}

		if (TextUtils.isEmpty(proNo)) {
//			UIHelper.showToast(this, "Pro No is empty!");
			RewriteBuilderDialog.showSimpleDialog_Tip(context, "Pro No is empty!");
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnPrint: // 打印bol,不用检查
			getData_toPrint();
			break;
		case R.id.btnPrintPackingList: // 打packingList
			Intent in = new Intent(mActivity, LoadPrintLabel.class);
			LoadPrintLabel.initParams(in, complexSubLoads);
			startActivity(in);
			break;
		// case R.id.btnClose:
		case R.id.close_text:
			if (!checkForm())
				return;

			// 若已关闭 先提示
			if (complexSubLoads.getCurSubLoad().isClosed()) {
				RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_wms_continue), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						showDlg_confirm();
					}
				});
				return;
			}

			showDlg_confirm();
			break;
		case R.id.loFreight:
			openFreightDetail();
			break;
		case R.id.btnBolOrOrder: // 输入bol或order
			etProNo.setText(getBolOrOrder(false));
			Utility.endEtCursor(etProNo);
			break;
		case R.id.btnProNo:
			etProNo.setText(complexSubLoads.getCurSubLoad().pro_no);
			Utility.endEtCursor(etProNo);
			break;
		default:
			break;
		}
	}

	private void openFreightDetail() {
//		Intent i = new Intent(this, CheckInDockCloseFreightActivity.class);
//		i.putExtra("main", complexSubLoads.entry_id + "");
//		startActivity(i);李君浩注释
	}
	
	private void showDlg_CheckConfirm(){

		if(TextUtils.isEmpty(complexSubLoads.getCurSubLoad().pro_no)){
			showDlg_confirm();
		}
		String proNo = etProNo.getEditableText().toString();
		if(!proNo.equals(complexSubLoads.getCurSubLoad().pro_no)){
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			
			View view = View.inflate(mActivity, R.layout.dlg_loadtoprintactivity_tip, null);
			
			TextView tvDefaultPro = (TextView) view.findViewById(R.id.tvDefaultPro);
			TextView tvPro = (TextView) view.findViewById(R.id.tvPro);
			
			tvDefaultPro.setText(complexSubLoads.getCurSubLoad().pro_no);
			tvPro.setText(proNo);
			
			builder.setContentView(view);
			
			builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					showDlg_confirm();
				}
			});
			
			builder.show();
		}
			
	}

	private void showDlg_confirm() {
		CheckInTaskBeanMain taskBean = complexSubLoads.doorBean;
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		// 初始化dlg
		View vContent = getLayoutInflater().inflate(R.layout.dlg_loadclose, null);
		builder.setContentView(vContent);
		builder.setTitle(getString(R.string.patrol_confirm));
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				doSubmit();
			}
		});

		// 隐藏seal
		View loInSeal_confirm = vContent.findViewById(R.id.loInSeal_confirm);
		View loOutSeal_confirm = vContent.findViewById(R.id.loOutSeal_confirm);
		loInSeal_confirm.setVisibility(taskBean.hasInSeal() ? View.VISIBLE : View.GONE);
		loOutSeal_confirm.setVisibility(taskBean.hasOutSeal() ? View.VISIBLE : View.GONE);

		// 取view
		TextView tvProNo_dlg = (TextView) vContent.findViewById(R.id.tvProNo_dlg);
		TextView tvInSeal = (TextView) vContent.findViewById(R.id.tvInSeal);
		TextView tvOutSeal = (TextView) vContent.findViewById(R.id.tvOutSeal);
		LinearLayout loLoadbars = (LinearLayout) vContent.findViewById(R.id.loLoadbars);
		View tvLoadbar_NA = vContent.findViewById(R.id.tvLoadbar_NA);
		// 初始化-数据
		tvProNo_dlg.setText(etProNo.getEditableText().toString());
		tvInSeal.setText(taskBean.in_seal);
		tvOutSeal.setText(taskBean.out_seal);
		addLoadbars_confrimDlg(loLoadbars, tvLoadbar_NA);
		builder.show();
	}

	private void addLoadbars_confrimDlg(LinearLayout lo, View vNA) {
		List<Load_useBean> listBars = complexSubLoads.doorBean.load_UseList;
		if (listBars == null || listBars.size() == 0) {
			vNA.setVisibility(View.VISIBLE);
			return;
		}
		vNA.setVisibility(View.GONE);
		for (int i = 0; i < listBars.size(); i++) {
			Load_useBean tmpB = listBars.get(i);
			View tmpV = getLayoutInflater().inflate(R.layout.dlg_taskclose_loadbar_item, null);
			((TextView) tmpV.findViewById(R.id.tvBar_name)).setText(tmpB.load_bar_name + "");
			((TextView) tmpV.findViewById(R.id.tvBar_cnt)).setText(tmpB.count + "");
			lo.addView(tmpV);
		}
	}

	// private List<TabParam> getTabParamList() {
	// List<TabParam> params = new ArrayList<TabParam>();
	// String key0 =
	// TTPKey.getLoadToPrintKey(complexSubLoads.getDlo_detail_id(),
	// complexSubLoads.getCurSubLoad().master_bol_no + "", 0);
	// String key1 =
	// TTPKey.getLoadToPrintKey(complexSubLoads.getDlo_detail_id(),
	// complexSubLoads.getCurSubLoad().master_bol_no + "", 1);
	// params.add(new TabParam("Close", key0, new PhotoCheckable(0, "Close",
	// ttp)));
	// params.add(new TabParam("Seal", key1, new PhotoCheckable(1, "Seal",
	// ttp)));
	// return params;
	// }

	// finish submit
	private void doSubmit() {
		
		//停止自动上传,防止传重
		ttp.abortUploading();
//		if(true)
//			return;
		
		String fixProNo = StringUtil.isNullOfStr(etProNo.getText().toString()) ? "NA" : etProNo.getText().toString();
		// String inSeal = complexSubLoads.taskBean.in_seal;
		String outSeal = complexSubLoads.doorBean.out_seal;

		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.CloseEntryDetail);
		params.add("equipment_id", complexSubLoads.equipment_id);
		params.add("entry_id", complexSubLoads.entry_id + "");
		params.add("close_type", complexSubLoads.getCurSubLoad().closeType + "");
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id() + "");
		params.add("master_bol_no", complexSubLoads.getCurSubLoad().master_bol_no + "");
		params.add("pro_no", fixProNo);
		params.add("seal", TextUtils.isEmpty(outSeal) ? "NA" : outSeal);
		params.add("is_last", isLastLoad_lastMol ? "1" : "0");
		params.add("resources_type", complexSubLoads.doorBean.resources_type + "");
		params.add("resources_id", complexSubLoads.doorBean.resources_id + "");
		params.add("option_file_param", complexSubLoads.getCurSubLoad().customerid);
		// 异常关闭
		if (!isNormalClose)
			params.add("note", etExReason.getText().toString());

		ttp.uploadZip(params, "LoadToPrintPhoto");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				// 关闭
				complexSubLoads.getCurSubLoad().close();

				// 回调-状态
				int closenum = complexSubLoads.getCurSubLoad().closeType;
				String number = complexSubLoads.load.number;
				// 单个load完成时,才回调
				if (loadToPrintBack != null && complexSubLoads.isAllClosed())
					loadToPrintBack.data(closenum, number);

				int data = StringUtil.getJsonInt(json, "data");
				// show_finish_layout.setVisibility(View.VISIBLE);

				isDoorFinish_ret = false;
				if (LoadClose_DirectFinishDoor == data) {
					isDoorFinish_ret = true;
					finishAndJump();
				} else if (LoadCloseNotifyStayOrLeave == data) {
					isDoorFinish_ret = true;
					leavebuilder(complexSubLoads.entry_id);
				} else if (LoadCloseNotifyReleaseDoor == data) {
					isDoorFinish_ret = true;
					releasebuilder(complexSubLoads.entry_id);
				} else if (LoadCloseNoifyNormal == data) {
					finishAndJump();
				}
			}
		}.doPost(HttpUrlPath.CheckInActionMutiRequest, params, this);
	}

	private void getData_toPrint() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.LoadGetBillOfLadingInfo);
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id() + "");
		params.add("equipment_id", complexSubLoads.equipment_id);
		//-------由于打印页面新添功能条件 所以需要要新加参数
		params.add("load_no", complexSubLoads.load.number);
		params.add("number_type",complexSubLoads.load.number_type+"");
		params.add("entry_id", complexSubLoads.entry_id);
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				helpJson(json);
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	/**
	 * @Description:解析返回后的json数组 从中根据count的返回值来判断 应该去做的操作
	 * @param json
	 * @return count=1 表示 直接进到 选择打印类型的页面 count》=2表示先弹出选择load页面筛选然后在进入 选择打印类型的页面
	 */
	private void helpJson(JSONObject json) {

		Intent intent = new Intent();
		ReceiptTicketModel r = ReceiptTicketModel.parseJSON(json, "");

		r.setFixNumber(complexSubLoads.load.getMainNo());

		int number_type = complexSubLoads.load.getMainType();
		boolean orderNoIsZero = !(number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER || number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO);
		boolean isOrderNo = (number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER);

		r.setFixOrder(orderNoIsZero ? "0" : isOrderNo ? complexSubLoads.load.getMainNo() : "");
		r.setNumber_type(number_type);

		r.setEntryId(complexSubLoads.entry_id);
		r.setDoor_name(complexSubLoads.doorBean.resources_name);
		String outSeal = complexSubLoads.doorBean.out_seal;
		r.setOut_seal(StringUtil.isNullOfStr(outSeal) ? "NA" : outSeal);
		r.setDetail_id(complexSubLoads.getDlo_detail_id());
		
		if(r.getReceiptTicketBase()!=null&&StringUtil.isNullOfStr(r.getReceiptTicketBase().getPrint_customer_id())){
			r.getReceiptTicketBase().setPrint_customer_id(complexSubLoads.customer_id);//用于打印现实的条件
		}
		
		intent.putExtra("receiptTicketModel", r);
		intent.putExtra("isFromLoadToPrintActivity", true);

		intent.setClass(context, ReceiptsTicketDatasActivity.class);
		startActivityForResult(intent, OPENRTDATAS);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);

	}

	// -----------------------
	private static CheckInLoad_ComplexSubLoads orders_bridge;

	public static void initParams(Intent in, CheckInLoad_ComplexSubLoads orders) {
		orders_bridge = orders;
	}

	private void applyParams() {
		if (orders_bridge == null)
			return;

		complexSubLoads = orders_bridge;
		orders_bridge = null;
	}

	// -------------------------------------------分割线---------------------------------------------------------------------
	private PopupWindow promptPopupWindows;// 弹出窗体提示用户

	/**
	 * 关闭窗口
	 */
	private void closeLocalDataPopupWindows() {
		if (promptPopupWindows != null && promptPopupWindows.isShowing()) {
			promptPopupWindows.dismiss();
		}
	}

	// 停留离开的时候调用 传参entryid和门id
	private void leavebuilder(final String main) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(resources.getString(R.string.sync_notice));
		// builder.setMessage("Does Load Leave ?");
		builder.setMessage(getString(R.string.sync_leave_text));
		builder.setPositiveButton(resources.getString(R.string.check_in_leave_n), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// b.setVisibility(View.VISIBLE);
				// leavepost(main, 0,1);
				leavepost(main, 0, CheckInMainDocumentsStatusTypeKey.INYARD);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.check_in_leave_y), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// b.setVisibility(View.VISIBLE);
				// leavepost(main, 3, 2);
				leavepost(main, 3, CheckInMainDocumentsStatusTypeKey.LEAVING);
			}
		});
		builder.create().show();
	}

	public static String getReleaseTip(Context context,int resType_id) {
		String res = OccupyTypeKey.getOccupyTypeKeyName(context,resType_id);
		return context.getString(R.string.tms_relese) + res +context.getString(R.string.tms_ma)+ "?";
	}

	// released 的时候调用 传参 entry id 和要关闭的门id
	private void releasebuilder(final String main) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(resources.getString(R.string.sync_notice));
		String tip = LoadToPrintActivity.getReleaseTip(context,complexSubLoads.doorBean.resources_type);
		builder.setMessage(tip);
		builder.setPositiveButton(resources.getString(R.string.check_in_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				releasepost(main, 3, 0);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.check_in_no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finishAndJump();
			}
		});
		builder.create().show();
	}

	private void releasepost(final String main, final int i, final int leave) {
		// Goable.initGoable(context);
		// final RequestParams params = new RequestParams();
		// StringUtil.setLoginInfoParams(params);
		// params.add("Method", HttpPostMethod.CloseLoadRelaseDoor);
		// params.add("equipment_id", complexSubLoads.equipment_id);
		// params.add("entry_id", main + "");
		// params.add("door_status", i + "");
		// params.add("order_id", null);
		// params.add("type", null);
		// params.add("status", null);
		// params.add("is_leave", leave + "");
		// params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id() + "");

		final RequestParams params = new RequestParams();
		CheckInTaskBeanMain mainbean = complexSubLoads.doorBean;
		params.add("Method", "TaskProcessingReleaseDoor");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_id", mainbean.resources_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finishAndJump();
			}

			@Override
			public void handFail() {
				// TODO Auto-generated method stub
				UIHelper.showToast(context, i + resources.getString(R.string.check_in_fail) + "", Toast.LENGTH_LONG).show();
			}
			// }.doPost(HttpUrlPath.CheckInAction, params, context);
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, context);
	}

	private void leavepost(final String main, final int i, final int leave) {
		// Goable.initGoable(context);
		// final RequestParams params = new RequestParams();
		// StringUtil.setLoginInfoParams(params);
		// params.add("Method", HttpPostMethod.CloseLoadStayOrLeave);
		// params.add("equipment_id", complexSubLoads.equipment_id);
		// params.add("entry_id", main + "");
		// params.add("door_status", i + "");
		// params.add("order_id", null);
		// params.add("type", null);
		// params.add("status", null);
		// params.add("is_leave", leave + "");
		// params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id() + "");

		// debug
		final RequestParams params = new RequestParams();
		CheckInTaskBeanMain mainbean = complexSubLoads.doorBean;
		params.add("Method", "TaskProcessingStayOrLeave");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_id", mainbean.resources_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		params.add("status", leave + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finishAndJump();
			}

			@Override
			public void handFail() {
				// TODO Auto-generated method stub
				UIHelper.showToast(context, i + resources.getString(R.string.check_in_fail) + "", Toast.LENGTH_LONG).show();
			}
			// }.doGet(HttpUrlPath.CheckInAction, params, context);
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, context);
	}

	public static LoadToPrintBack getLoadToPrintBack() {
		return loadToPrintBack;
	}

	public static void setLoadToPrintBack(LoadToPrintBack loadToPrintBack) {
		LoadToPrintActivity.loadToPrintBack = loadToPrintBack;
	}

	private static LoadToPrintBack loadToPrintBack;

	interface LoadToPrintBack {
		// finish(需所有子单据均已关闭)时回传状态
		public void data(int closenum, String number);
	}

}
