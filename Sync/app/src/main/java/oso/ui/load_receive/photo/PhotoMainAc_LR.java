package oso.ui.load_receive.photo;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.photo.util.Run_UploadLRPhoto;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.TTPKey;
import utility.Utility;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * Load/Receive添加图片-入口
 * @author 朱成
 * @date 2014-12-11
 */
public class PhotoMainAc_LR extends BaseActivity {

	private ListView listView;
	private List<String> listEntries = new ArrayList<String>();

	private Resources resources;
	private ProgressDialog dialog;
	
	private Button submit_btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_photo_capture_main_layout_1, 0);
		setTitleString(getString(R.string.warehouse_linear_taskphoto));
		showRightButton(R.drawable.add_button_style2 ,"" , new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toNextAc("");
			}
		});

		submit_btn = (Button)findViewById(R.id.submit);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listEntries.size() == 0) {
					UIHelper.showToast(mActivity, getString(R.string.task_photo_null));
					return;
				}
				submit();
			}
		});

		// 取view
		listView = (ListView) findViewById(R.id.listview);


		// 初始化
		resources = getResources();
		// dlg
		dialog = new ProgressDialog(this);
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(resources.getString(R.string.sys_submitData));
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setCancelable(true);

		// 初始化listview
		listView.setDivider(null);
		listView.setAdapter(adp);
		listView.setEmptyView(findViewById(R.id.no_data));
		// refresh();
		listView.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				refresh();
			}
		}, 100);

	}
	
	/**
	 * @Description:长按点击事件 弹出的菜单
	 * @param @param position
	 */
	private void ContextMenu(final int position){
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		View layout = LayoutInflater.from(mActivity).inflate(R.layout.dialog_checkout_for_photo, null);
		Button delect = (Button) layout.findViewById(R.id.delect);
		delect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteEntry(position);
				builder.dismiss();
			}
		});
		final String entry = listEntries.get(position);
		Button submit = (Button) layout.findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submitSingle(entry);
				builder.dismiss();
			}
		});
		builder.setContentView(layout);
		builder.hideCancelBtn();
 		builder.create().show();
	}

	private void deleteEntry(int pos) {
		// 删除目录
		String entry = listEntries.get(pos);
		File file0 = new File(Goable.getDir_takephoto(),
				TTPKey.getPhotoLRDir(entry, 0));
		Utility.delFolder(file0);

		// 刷新
		listEntries.remove(pos);
		adp.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		// 延迟刷新,防止卡顿
		if (requestCode == Req_NextAc) {
			listView.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					refresh();
				}
			}, 100);
		}

		if (ttp_inShot != null)
			ttp_inShot.onActivityResult(requestCode, resultCode, data);
	}

	private void submit() {
		if (listEntries.size() > 0) {
			dialog.setMax(listEntries.size());
			dialog.show();
			Thread thread = new Thread(new Run_UploadLRPhoto(
					this, listEntries, dialog, handler));
			thread.start();
		}
	}

	private void submitSingle(String entry) {
		dialog.setMax(listEntries.size());
		dialog.show();
		dialog.setMax(1);
		// debug
		List<String> list = new ArrayList<String>();
		list.add(entry);
		Thread thread = new Thread(new Run_UploadLRPhoto(
				this, list, dialog, handler));
		thread.start();
	}

	// ===================static,相关目录操作=================

	// 获取-entry上传图片
	public static List<File> getUploadImgs(String entry) {

		final List<File> listRet = new ArrayList<File>();

		File tabDir = new File(Goable.getDir_takephoto(),
				TTPKey.getPhotoLRDir(entry, 0) + "/" + TTPKey.DirN_Upload);
		tabDir.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				listRet.add(pathname);
				return false;
			}
		});
		return listRet;
	}

	// 删除-entry图片列表
	public static void delEntryImgs(String entry) {
		File tabDir = new File(Goable.getDir_PhotoLR(), entry + "_0");
		Utility.delFolder(tabDir);
	}

	// 获取-entry名列表
	public static void getEntries(final List<String> listEntries) {
		File dir_takephoto = Goable.getDir_PhotoLR();
		dir_takephoto.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String filename) {
				// TODO Auto-generated method stub

				//跳过-临时文件夹
				if (filename.contains("new_"))
					return false;

				int line_index = filename.indexOf('_');
				if (line_index < 0)
					return false;
				String entry = filename.substring(0, line_index);
				
				//若不含图片 则删除
				File fileCam=new File(dir,filename+"/"+TTPKey.DirN_Cam);
				if(Utility.isDirEmpty(fileCam)){
					Utility.delFolder(new File(dir,filename));
					return false;
				}

				if (!listEntries.contains(entry))
					listEntries.add(entry);
				return false;
			}
		});
	}

	// 已缓存的-entry数量
	public static int getEntryCnt() {
		List<String> list = new ArrayList<String>();
		getEntries(list);
		return list.size();
	}

	// entry含图片,暂时仅检查"目录"是否存在
	public static boolean isEntry_hasImgs(String entry) {
		File dir = new File(Goable.getDir_takephoto(),
				TTPKey.getPhotoLRDir(entry, 0) + "/" + TTPKey.DirN_Cam);
		if(!Utility.isDirEmpty(dir))
			return true;
		
		dir = new File(Goable.getDir_takephoto(), TTPKey.getPhotoLRDir(
				entry, 1) + "/" + TTPKey.DirN_Cam);
		if(!Utility.isDirEmpty(dir))
			return true;
		return false;
	}
	
	// =====================================================

	// private List<HoldDoubleValue<String, ArrayList<UpFile>>>
	// getSingleSubmitData(String fileWithId){
	// for(HoldDoubleValue<String, ArrayList<UpFile>> hold : datas){
	// if(hold.a.equalsIgnoreCase(fileWithId)){
	// return Arrays.asList(hold);
	// }
	// }
	// return null ;
	// }

	// 提取-entry列表
	private void refresh() {
		listEntries.clear();
		getEntries(listEntries);
		adp.notifyDataSetChanged();
		submit_btn.setVisibility((listEntries.size() == 0)?View.GONE:View.VISIBLE);
	}

	private final int Req_NextAc = 1;

	private void toNextAc(String entry) {
		Intent in = new Intent(this, PhotoAddAc_LR.class);
		PhotoAddAc_LR.initParams(in, entry);
		startActivityForResult(in, Req_NextAc);
	}

	private TabToPhoto ttp_inShot; // 当前打开相机的ttb

	public static final int What_UploadOK = 1;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case What_UploadOK:
				if (dialog.isShowing())
					dialog.dismiss();
				refresh();
				break;

			default:
				break;
			}
		};
	};

	// ==================nested===============================
	
	public static String TabPhoto_Name="L/R";

	private BaseAdapter adp = new BaseAdapter() {

		private List<TabParam> getTabParamList(int position, TabToPhoto ttb) {
			// debug
			List<TabParam> params = new ArrayList<TabParam>();
			String entry = listEntries.get(position);
			params.add(new TabParam(TabPhoto_Name, TTPKey.getPhotoLRDir(
					entry, 0), new PhotoCheckable(0, TabPhoto_Name, ttb)));
			return params;
		}

		// -----

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.checkin_photo_capture_adapter_item_1, null);
				h = new Holder();
				h.entryId = (TextView) convertView.findViewById(R.id.entry_id);
				h.photoContainer = (TabToPhoto) convertView
						.findViewById(R.id.linearLayoutPhoto);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷新-数据
			final String entry = listEntries.get(position);
			h.entryId.setText(entry);
			h.photoContainer.init(mActivity,
					getTabParamList(position, h.photoContainer));
			h.photoContainer.setAddBtnVisibility(false);

			// 监听事件
			h.photoContainer.onClick_btnAdd = new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ttp_inShot = h.photoContainer;
				}
			};
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					toNextAc(entry);
				}
			});
			convertView.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ContextMenu(position);
					return false;
				}
			});
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listEntries.size();
		}
	};

	private class Holder {
		TextView entryId;
		TabToPhoto photoContainer;
	}

}
