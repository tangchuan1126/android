package oso.ui.load_receive.window.util;

import java.util.List;

import oso.ui.load_receive.window.bean.TmpEquipBean;
import support.key.CheckInTractorOrTrailerTypeKey;
import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpEquipOccupy extends BaseAdapter {
	
	private Activity ct;

	private List<TmpEquipBean> list;
	
	public AdpEquipOccupy(Activity ct,List<TmpEquipBean> list) {
		// TODO Auto-generated constructor stub
		this.ct=ct;
		this.list=list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder h;
		if(convertView==null){
			convertView=LayoutInflater.from(ct).inflate(R.layout.it_window_addequip_occupy, null);
			h=new Holder();
			h.loRes=convertView.findViewById(R.id.loRes);
//			h.tvEquip_title=(TextView)convertView.findViewById(R.id.tvEquip_title);
			h.equipment_number=(TextView)convertView.findViewById(R.id.tvEquip_value);
			h.entry_id=(TextView)convertView.findViewById(R.id.tvEntry_i);
			h.resource_info=(TextView)convertView.findViewById(R.id.resource_info);
			
			h.equipment_type = (ImageView)convertView.findViewById(R.id.equipment_type);
			convertView.setTag(h);
		}
		else
			h=(Holder)convertView.getTag();
		
		//刷新-数据
		TmpEquipBean b=list.get(position);
//		h.tvEquip_title.setText(b.equipment_type_value+": ");
//		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(b.equipment_type_value, view)
		h.entry_id.setText("E"+b.entry_id);
		h.equipment_number.setText(b.equipment_number);
		h.loRes.setVisibility(TextUtils.isEmpty(b.resources_name)?View.GONE:View.VISIBLE);
		
		if(b.equipment_type!=0)CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(b.equipment_type, h.equipment_type);
		String resourceStr = b.resources_type_value+": "+b.resources_name;
		h.resource_info.setText(b.occupy_status_value+" "+resourceStr);
		
		return convertView;
	}
	
	class Holder{
		TextView /*tvEquip_title,*/equipment_number,entry_id;
		TextView resource_info;
		ImageView equipment_type;
		View loRes;
	}

}
