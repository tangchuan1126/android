package oso.ui.load_receive.do_task.receive.wms.adapter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.ui.debug.product.CreateProductActivity;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.wms.Receive_CntAc;
import oso.ui.load_receive.do_task.receive.wms.Receive_CreatePalletsAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class Receive_ItemListAcAdapter extends BaseAdapter {
    List<Rec_ItemBean> listItems;
    private LayoutInflater inflater;
    private Activity context;

    private Rec_ReceiveBean recBean;

    public Receive_ItemListAcAdapter(Activity context, Rec_ReceiveBean recBean) {
        this.context = context;
        this.recBean = recBean;
        this.listItems = recBean.lines;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listItems != null ? listItems.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private void toConfigAc(int position) {
        Rec_ItemBean b = listItems.get(position);
        Receive_CreatePalletsAc.toThis(context, b, false, recBean.dlo_detail_id, recBean.receipt_id, recBean.receipt_no);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {

        OnClickListener ls = new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (Utility.isFastClick())
                    return;
                switch (v.getId()) {
                    case R.id.btnReceive:
                        getReceiveValue(position);
                        break;

                    case R.id.btnPallets: // 生成pallet
                        toConfigAc(position);
                        break;
                    default:
                        break;
                }
            }
        };


        // =================================================

        EHolder h;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.receive_item_it, null);
            h = new EHolder();
            h.btnReceive = (Button) convertView.findViewById(R.id.btnReceive);
            h.btnCreateProduct = (Button) convertView.findViewById(R.id.btnCreateProduct);
            h.btnPallets = (Button) convertView.findViewById(R.id.btnPallets);
            h.itemid = (TextView) convertView.findViewById(R.id.entry_id);
            h.lot_no = (TextView) convertView.findViewById(R.id.lntno_task);
            // h.receiveQty = (TextView)
            // convertView.findViewById(R.id.assign_task);
            // h.totalQty = (TextView)
            // convertView.findViewById(R.id.total_task);
            h.receive_pallet = (TextView) convertView.findViewById(R.id.receive_pallet);
            h.tvQty_title = (TextView) convertView.findViewById(R.id.tvQty_title);
            h.tvStockOut_it_1 = (TextView) convertView.findViewById(R.id.tvStockOut_it_1);
            h.loPltCnt = convertView.findViewById(R.id.loPltCnt);
            h.itemLayout = convertView.findViewById(R.id.itemLayout);
            h.tsc_linear = convertView.findViewById(R.id.tsc_linear);
            h.tsc_linear2 = convertView.findViewById(R.id.tsc_linear2);

            // h.tvGoodQty_it=(TextView)convertView.findViewById(R.id.tvGoodQty_it);
            h.tvStockOut_it = (TextView) convertView.findViewById(R.id.tvStockOut_it);
            // h.tvDamange_it=(TextView)convertView.findViewById(R.id.tvDamange_it);
            h.tvExpectedQty_it = (TextView) convertView.findViewById(R.id.tvExpectedQty_it);

            h.tvCntUser = (TextView) convertView.findViewById(R.id.tvCntUser);
            h.tvCntTime = (TextView) convertView.findViewById(R.id.tvCntTime);

            h.loGoodQty_it = convertView.findViewById(R.id.loGoodQty_it);
            h.loStockOut_it = convertView.findViewById(R.id.loStockOut_it);
            h.loDamage_it = convertView.findViewById(R.id.loDamage_it);
            h.tvShortage_title = (TextView) convertView.findViewById(R.id.tvShortage_title);
            convertView.setTag(h);
        } else
            h = (EHolder) convertView.getTag();

        //debug
//        h.btnCreateProduct.setVisibility(AppConfig.isDebug?View.VISIBLE:View.GONE);

        // 刷ui
        final Rec_ItemBean b = listItems.get(position);
        h.itemid.setText(b.item_id);
        h.receive_pallet.setText(b.count_pallet + "");
        h.tvExpectedQty_it.setText(b.expected_qty + "");

        h.tvCntUser.setText(b.counted_user_name);
        h.tvCntTime.setText(b.unloading_finish_time);
        
        if(TextUtils.isEmpty(b.lot_no)){
        	h.lot_no.setText("");
        	h.lot_no.setHint("NA");
        }else{
        	h.lot_no.setText(b.lot_no);
        }

        // h.receiveQty.setText(b.receive_qty + "");
        // h.totalQty.setText(b.expected_qty + "");

        // h.tvGoodQty_it.setText(b.normal_qty+"");
        // h.tvDamange_it.setText(b.damage_qty+"");

        // int sQty=b.getShortage();
        // h.tvStockOut_it.setText(Math.abs(sQty)+"");
        // h.tvShortage_title.setText(sQty>0?"R. Qty: ":"O. Qty: ");

        // h.loGoodQty_it.setVisibility(b.normal_qty==0?View.GONE:View.VISIBLE);
        // h.loDamage_it.setVisibility(b.damage_qty==0?View.GONE:View.VISIBLE);
        // h.loStockOut_it.setVisibility(b.getShortage()==0?View.GONE:View.VISIBLE);

        // debug
        int total = b.normal_qty + b.damage_qty;
        h.tvStockOut_it.setText(total + "");
        String qty = "";
        if (b.damage_qty != 0)
            qty = "(G: " + b.normal_qty + "  +  D: " + b.damage_qty + ")";
        h.tvStockOut_it_1.setText(qty);

        // 事件
        h.btnReceive.setOnClickListener(ls);
        h.btnPallets.setOnClickListener(ls);
        h.btnCreateProduct.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toCreateProduct(recBean.company_id, recBean.customer_id, b.item_id);
            }
        });

        // 若未cnt
        if (!b.isCnt_complete()) {
            h.itemLayout.setBackgroundResource(R.drawable.sh_rec_items_lv_top);
            h.tsc_linear.setVisibility(View.VISIBLE);
            h.tsc_linear2.setVisibility(View.GONE);
        }
        // 已cnt完成
        else {
            h.itemLayout.setBackgroundResource(R.drawable.sh_rec_items_lv_top2);
            h.tsc_linear.setVisibility(View.GONE);
            h.tsc_linear2.setVisibility(View.VISIBLE);
        }

        return convertView;
    }


    private void toCreateProduct(String companyId, String customerId, String itemId) {
//        NetConnection_YMS.getThis().reqProduct_data(new NetInterface.SyncJsonHandler(context) {
//            @Override
//            public void handReponseJson(JSONObject json) {
//                ProductBean bean = new Gson().fromJson(json.optString("product"), ProductBean.class);
//                reqContextData(bean);
//            }
//        }, companyId, customerId, itemId);
    }

    private void reqContextData(final ProductBean product) {
        new SimpleJSONUtil() {
            @Override
            public void handReponseJson(JSONObject json) {
                if (json.optInt("err") == 90) {
                    UIHelper.showToast(json.optString("data"));
                    return;
                }
                ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
                }.getType());
                Intent intent = new Intent(context, CreateProductActivity.class);
                intent.putExtra("ContextData", data);
                intent.putExtra("product", product);
                context.startActivity(intent);
            }
        }.doGet(HttpUrlPath.product_context, new RequestParams(), context);
    }

    class EHolder {
        View loPltCnt;
        Button btnReceive, btnPallets, btnCreateProduct;
        TextView itemid, lot_no, pallets, receive_pallet, tvQty_title, tvStockOut_it_1;
        TextView tvCntUser, tvCntTime;
        // receiveQty,totalQty
        // tvDamange_it,tvGoodQty_it,
        TextView tvStockOut_it, tvExpectedQty_it;
        View loGoodQty_it, loStockOut_it, loDamage_it;
        View itemLayout, tsc_linear, tsc_linear2;
        TextView tvShortage_title;
    }

    private void getReceiveValue(final int position) {
        final RequestParams params = new RequestParams();
        params.add("receipt_line_id", listItems.get(position).receipt_line_id);
        // Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
        params.add("container_config_id", "0");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
//				pallets
                JSONArray jaPlts = json.optJSONArray("pallets");
                
                // dmgPlts
                JSONArray jaDmg = json.optJSONArray("damaged");
                
                if (Utility.isEmpty(jaPlts) && Utility.isEmpty(jaDmg)) {
                    RewriteBuilderDialog.newSimpleDialog(context,
                            "No pallets! Please config pallets first!",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    toConfigAc(position);
                                }

                                ;
                            }).hideCancelBtn().show();
                    return;
                }
                // plts
                List<Rec_PalletBean> listPlts = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
                }.getType());

                
                List<Rec_PalletBean> listDmg = null;
                if (!Utility.isEmpty(jaDmg)) {
                    listDmg = new Gson().fromJson(jaDmg.toString(), new TypeToken<List<Rec_PalletBean>>() {
                    }.getType());
                }
                
                List<Rec_PalletType> listTypes = new Gson().fromJson(json.optJSONArray("contypes").toString(), new TypeToken<List<Rec_PalletType>>(){}.getType());

                Intent in = new Intent(context, Receive_CntAc.class);
                recBean.curItemBean = listItems.get(position);
                Receive_CntAc.initParams(in, listPlts, listDmg,listTypes, recBean);
                context.startActivity(in);
            }

        }.doGet(HttpUrlPath.acquirePalletsInfoByLine, params, context);
    }
}
