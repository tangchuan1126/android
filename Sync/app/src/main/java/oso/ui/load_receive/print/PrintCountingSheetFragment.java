package oso.ui.load_receive.print;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.widget.sign.SignatureBoard;
import oso.widget.sign.SignatureBoard.OnSignListener;
import support.common.UIHelper;
import support.key.EntryDetailNumberTypeKey;
import support.network.HttpPostMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PrintCountingSheetFragment extends Fragment {

	private View view;
	private WebView show_webview;
	Activity activity;
	private Bundle bundle;
	private FrameLayout loadingLayout;
	private TextView psTv;
	private SignatureBoard sb;
	private PrintPackMainBeanModel receiptTicketModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.print_webview_bill_loading_layout, container, false);
		return view;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		bundle = getArguments();
		activity = this.getActivity();
		receiptTicketModel = (PrintPackMainBeanModel) bundle.getSerializable("receiptTicketModel");
		initView();
	}

	private void initView() {
		psTv = (TextView) view.findViewById(R.id.psTv);
		loadingLayout = (FrameLayout) view.findViewById(R.id.loadingLayout);
		show_webview = (WebView) view.findViewById(R.id.show_webview);
		show_webview.getSettings().setJavaScriptEnabled(true);

		sb = new SignatureBoard(getActivity());
		sb.setOnSignListener(new OnSignListener() {
			@Override
			public void onSignSuccessed(String savePath, Bitmap bm,String imgUrl) {
				// 刷新页面
				loadingLayout.setVisibility(View.VISIBLE);
				show_webview.reload();
			}
		});

		WebSettings settings = show_webview.getSettings();
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);

		show_webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				psTv.setText(progress + "%");
			}
		});

		show_webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingLayout.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				UIHelper.showToast(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
			}
		});
		StringBuilder url = new StringBuilder(HttpUrlPath.basePath);
		boolean flag = receiptTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_ORDER
				|| receiptTicketModel.getNumber_type() == EntryDetailNumberTypeKey.CHECK_IN_PONO;
		url.append(flag ? "check_in/a4_counting_sheet_order.html" : "check_in/a4_counting_sheet.html");
		JSONArray jSONArray = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("checkDataType", "PICKUP");
			jsonObject.put("number", receiptTicketModel.getFixNumber());
			jsonObject.put("door_name", receiptTicketModel.getDoor_name());
			jsonObject.put("companyId", receiptTicketModel.getCompanyid());
			jsonObject.put("customerId", receiptTicketModel.getCustomer_id());
			jsonObject.put("checkLen", "1");
			jsonObject.put("number_type", receiptTicketModel.getNumber_type());
			jsonObject.put("order", receiptTicketModel.getFixOrder());
			jsonObject.put("resources_id", receiptTicketModel.getResources_id());
			jsonObject.put("resources_type", receiptTicketModel.getResources_type());
			jsonObject.put("containerNo", receiptTicketModel.getEquipment_number());
			jsonObject.put("appointmentDate", receiptTicketModel.getAppointment_date());
			jSONArray.put(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		url.append("?jsonString=").append(jSONArray.toString());
		url.append("&entryId=").append(receiptTicketModel.getReceiptTicketBase().getEntryid());
		url.append("&isprint=1");
		url.append("&resources_id="+receiptTicketModel.getResources_id());
		url.append("&resources_type="+receiptTicketModel.getResources_type());
		url.append("&containerNo="+receiptTicketModel.getEquipment_number());
		url.append("&appointmentDate="+receiptTicketModel.getAppointment_date());
 
		url.append(Utility.getSharedPreferencesUrlValue(activity, "adid"));
		show_webview.loadUrl(url.toString());

	}

	private RequestParams getRequestParams() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.BillOfLoadingSignature);
		return params;
	}

}
