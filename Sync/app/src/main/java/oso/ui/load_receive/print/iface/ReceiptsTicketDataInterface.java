package oso.ui.load_receive.print.iface;

import oso.ui.load_receive.print.bean.ReceiptBillBase;


public interface ReceiptsTicketDataInterface {
	public void preview(ReceiptBillBase r);
	public void preview(ReceiptBillBase r,int type); 
}
