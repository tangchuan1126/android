package oso.ui.load_receive.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.TaskListActivity.OnRefreshDataListener;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EntryMainBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.TmpEquipBean;
import oso.ui.load_receive.window.bean.WindowBean;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import oso.ui.load_receive.window.util.AdpEquipOccupy;
import oso.ui.load_receive.window.util.EquipmentAdapter;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.dbhelper.Goable;
import support.key.CheckInEntryPriorityKey;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class EquipmentListActivity extends BaseActivity {

	private TextView entryTv;
	private TextView userTv;
	private TextView gateinTv;
	private ListView lv;
	private EquipmentAdapter adapter;

	private RewriteBuilderDialog.Builder addEquipmentDialog;

	private EntryMainBean mEntry;
	private List<EquipmentBean> mEquipmentList;

	private View equipment_frame;
	WindowBean bean;
	private boolean toAddTractor = false; // 添加设备,临时标签
	private String numStr;

	public static int RESULT_CODE = 10023;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_select_equipment, 0);
		initData();
		initView();
		setView();
	}

	@Override
	protected void onStart() {
		super.onStart();
		TaskListActivity.setOnRefreshDataListener(new OnRefreshDataListener() {
			@Override
			public void onRefresh(EquipmentBean e, int totalTask) {
				EquipmentBean self = isMySelf(e);
				if (self == null) {
					return;
				}
				// 修改自己的数据
				setNewData(self, e, totalTask);
				// 判断设备类型
				if (self.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
					// 车头
					for (EquipmentBean bean : mEquipmentList) {
						if (bean.getEquipment_purpose() == CheckInLiveLoadOrDropOffKey.LIVE) {
							bean.setResources_type(e.getResources_type());
							bean.setResources_name(e.getResources_name());
						}
					}
				} else {
					// 车尾
					if(self.getEquipment_purpose() == CheckInLiveLoadOrDropOffKey.LIVE) {
						for (EquipmentBean bean : mEquipmentList) {
							if (bean.getEquipment_purpose() == CheckInLiveLoadOrDropOffKey.LIVE) {
								bean.setResources_type(e.getResources_type());
								bean.setResources_name(e.getResources_name());
							}
						}
					} else {
						
					}
				}
				adapter.notifyDataSetChanged();
			}
		});
	}

	private EquipmentBean isMySelf(EquipmentBean e) {
		for (EquipmentBean bean : mEquipmentList) {
			if (bean.getEquipment_id() == e.getEquipment_id()) {
				return bean;
			}
		}
		return null;
	}

	private void setNewData(EquipmentBean self, EquipmentBean e, int totalTask) {
		self.setTotal_task(totalTask + "");
		self.setEquipment_number(e.getEquipment_number());
		self.setResources_type(e.getResources_type());
		self.setResources_name(e.getResources_name());
		self.setRel_type(e.getRel_type());
		self.setEquipment_purpose(e.getEquipment_purpose());
		self.setEquipment_status(e.getEquipment_status());
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		// if (entry_id != null && !TextUtils.isEmpty(entry_id)) {
		// requestToWindowCheckInEntryEquipmentList(mEntry);
		// }
		if (UpdateUserInfoActivity.windowNameStr != null) {
			userTv.setText(UpdateUserInfoActivity.windowNameStr.toUpperCase());
		}
		Intent data = new Intent();
		data.putExtra("flag", true);
		data.putExtra("entry_id", intent.getStringExtra("entry_id"));
		setResult(SearchCheckInWindowActivity.REQUEST_CODE, data);
	}

	private void initData() {
		setTitleString(getString(R.string.window_equipments));
		mEntry = (EntryMainBean) getIntent().getSerializableExtra("mEntry");
		mEquipmentList = (List<EquipmentBean>) getIntent().getSerializableExtra("mEquipmentList");
		showRightButton(R.drawable.add_button_style2, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				addTrailerDialog();
			}
		});
		// showRightButton0(-1, "OK", new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// showBackDialog();
		// }
		// });
	}

	private boolean isNotTractor() {
		for (EquipmentBean bean : mEquipmentList) {
			if (bean.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
				return false;
			}
		}
		return true;
	}

	private void requestToServer() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInEntryEquipmentList);
		params.add("entry_id", mEntry.entry_id);
		Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInEntryEquipmentList= " + json.toString());
				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "datas");
				Gson gson = new Gson();
				mEquipmentList = gson.fromJson(ja.toString(), new TypeToken<List<EquipmentBean>>() {
				}.getType());
				if (!mEquipmentList.isEmpty()) {
					adapter = new EquipmentAdapter(mActivity, mEquipmentList, mEntry);
					lv.setAdapter(adapter);
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	// 添加设备
	public void addTrailerDialog() {
		// 添加头,暂时不支持添加车头
		// toAddTractor=mEquipmentList.isEmpty();
		final View view = getLayoutInflater().inflate(R.layout.dialog_add_equipment, null);
		final EditText et = (EditText) view.findViewById(R.id.et);
		et.setTransformationMethod(new AllCapTransformationMethod());
		initSingleSelectBarDialog(view);
		addEquipmentDialog = new RewriteBuilderDialog.Builder(mActivity);
		addEquipmentDialog.setTitle(getString(R.string.patrol_add_text) +" "+ (toAddTractor ? getString(R.string.patrol_tractor_title) : getString(R.string.patrol_trailer_title)));
		addEquipmentDialog.setContentView(view);
		addEquipmentDialog.setPositiveButtonOnClickDismiss(false);
		addEquipmentDialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Utility.colseInputMethod(mActivity, et);
				numStr = et.getText().toString().trim().toUpperCase();
				if (TextUtils.isEmpty(numStr)) {
					UIHelper.showToast(mActivity, getString(R.string.patrol_equipment_input), Toast.LENGTH_SHORT).show();
					return;
				}
				getHttpAddTrailer(false);
			}
		});
		addEquipmentDialog.create().show();
	}

	private void getHttpAddTrailer(boolean reAssignEquip) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.AddOrUpdateEquipment);
		params.add("check_in_entry_id", mEntry.entry_id);
		params.add("equipment_number", numStr);
		int equipType = toAddTractor ? CheckInTractorOrTrailerTypeKey.TRACTOR : CheckInTractorOrTrailerTypeKey.TRAILER;
		params.add("equipment_type", equipType + ""); // tractor、trailer
		params.add("equipment_purpose", getPurpose() + ""); // liveLoad等
		params.add("is_check_success", reAssignEquip ? "1" : "0");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				int flag = json.optInt("flag");
				// 设备已存在
				if (flag == 2) {
					showDlg_quitEquip(json);
					return;
				}
				// 关闭dialog
				addEquipmentDialog.dismiss();
				// 刷新-设备列表
				requestToServer();
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, this);
	}

	// 提示-释放设备
	private void showDlg_quitEquip(JSONObject json) {
		List<TmpEquipBean> listEquips = null;
		try {
			listEquips = new Gson().fromJson(json.optJSONArray("equipment_not_entry").toString(), new TypeToken<List<TmpEquipBean>>() {
			}.getType());
		} catch (Exception e) {
			// TODO: handle exception
		}
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle(getString(R.string.checkout_equipment));
		builder.setAdapter(new AdpEquipOccupy(this, listEquips), null);
		builder.setPositiveButton(getString(R.string.yms_gate_checkout), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				getHttpAddTrailer(true);
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.show();
	}
	
	// 新增设备时,的purpose
	private int getPurpose() {
		int ret = 0;
		int sel = singleSelectBar.getCurrentSelectItem().b;
		if (sel == 0)
			ret = CheckInLiveLoadOrDropOffKey.LIVE;
		else if (sel == 1)
			ret = CheckInLiveLoadOrDropOffKey.DROP;
		else if (sel == 2)
			ret = CheckInLiveLoadOrDropOffKey.PICK_UP;
		return ret;
	}

	private SingleSelectBar singleSelectBar;

	private void initSingleSelectBarDialog(View parent) {
		singleSelectBar = (SingleSelectBar) parent.findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.qeuipment_tasktype_live), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.qeuipment_tasktype_drop), 1));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.qeuipment_tasktype_pickup), 2));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					break;
				case 1:
					break;
				}
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void initView() {
		entryTv = (TextView) findViewById(R.id.entryTv);
		userTv = (TextView) findViewById(R.id.userTv);
		gateinTv = (TextView) findViewById(R.id.gateinTv);
		lv = (ListView) findViewById(R.id.lv);
		lv.setEmptyView(findViewById(R.id.nodataTv));
		equipment_frame = findViewById(R.id.equipment_frame);
		equipment_frame.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (!isAllEequipmentLeft("Cannot edit")) { // zhangrui2015-01-06修改
					driverway();
				}
			}
		});
	}

	private void showDlg_delEquip() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqDelEquip(false);
			}
		});
	}

	private void driverway() {
		RequestParams params = new RequestParams();
		params.add("Method", "WindowCheckInEntryInfos");
		params.add("entry_id", mEntry.entry_id + "");
		Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				bean = WindowBean.getJsonWindowCheckIn(json, bean);
				if (bean.entry_id != null && bean.entry_id.length() > 0) {
					Intent intent = new Intent(mActivity, UpdateUserInfoActivity.class);
					intent.putExtra("windowmain", bean);
					startActivity(intent);
				} else {
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private int index_rightDlg;

	private void reqDelEquip(boolean delWithTasks) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.DeleteEquipment);
		params.add("id", mEquipmentList.get(index_rightDlg).getEquipment_id() + "");
		params.add("is_check_success", delWithTasks ? "1" : "0");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// 有任务,先提示
				if (json.optInt("is_check_success") != 1)
					showDlg_delEquip_withTasks();
				// 无任务,直接删除
				else {
					mEquipmentList.remove(index_rightDlg);
					adapter.notifyDataSetChanged();
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private void showDlg_delEquip_withTasks() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_et), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqDelEquip(true);
			}
		});
	}

	private void setView() {
		entryTv.setText("E" + mEntry.entry_id);
		userTv.setText(mEntry.gate_driver_name);
		gateinTv.setText("Gate In: " + mEntry.gate_check_in_time);
		adapter = new EquipmentAdapter(mActivity, mEquipmentList, mEntry);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				final EquipmentBean bean = (EquipmentBean) adapter.getItem(position);
				boolean isTask = (bean.getCheck_in_entry_id() + "").equals(mEntry.entry_id);
				reqToTask(bean, isTask);
			}
		});
		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				final EquipmentBean bean = mEquipmentList.get(position);
				if (isEquipmentStatus(mActivity, bean, "Cannot delete") && bean.getEquipment_type() != CheckInTractorOrTrailerTypeKey.TRACTOR) {
					index_rightDlg = position;
					showDlg_delEquip();
				}
				return true;
			}
		});
	}

	public static boolean isEquipmentStatus(Context c, EquipmentBean e, String prompt) {
		if (e.getEquipment_status() != CheckInMainDocumentsStatusTypeKey.LEFT) {
			return true;
		} else {
			UIHelper.showToast(c, prompt + ". equipment has left", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	private boolean isAllEequipmentLeft(String prompt) {
		for (EquipmentBean e : mEquipmentList) {
			if (e.getEquipment_status() != CheckInMainDocumentsStatusTypeKey.LEFT) {
				return false;
			}
		}
		UIHelper.showToast(mActivity, prompt + getString(R.string.sync_allequipment_left), Toast.LENGTH_SHORT).show();
		return true;
	}

	private boolean isOnlyTractor(EquipmentBean e) {
		return e.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR && mEquipmentList.size() == 1;
	}

	private void requestToWindowCheckInEntryEquipmentList(final EntryBean entry) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInEntryEquipmentList);
		params.add("entry_id", entry.getEntry_id());
		Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInEntryEquipmentList= " + json.toString());
				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "datas");
				Gson gson = new Gson();
				mEquipmentList = gson.fromJson(ja.toString(), new TypeToken<List<EquipmentBean>>() {
				}.getType());
				if (mEquipmentList == null || mEquipmentList.isEmpty()) {
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
					return;
				}
				setView();
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private void reqToTask(final EquipmentBean bean, final boolean isTask) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.WindowCheckInGetEquipmentInfos);
		params.add("entry_id", mEntry.entry_id + "");
		params.add("equipment_id", bean.getEquipment_id() + "");
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInGetEquipmentInfos= " + json.toString());
				JSONObject entryJo = StringUtil.getJsonObjectFromJSONObject(json, "entry");
				JSONObject equipmentJo = StringUtil.getJsonObjectFromJSONObject(json, "equipment");
				JSONArray tasksJa = StringUtil.getJsonArrayFromJson(json, "tasks");
				Gson gson = new Gson();
				final EntryBean entryBean = gson.fromJson(entryJo.toString(), EntryBean.class);
				final EquipmentBean equipmentBean = gson.fromJson(equipmentJo.toString(), EquipmentBean.class);
				final List<List<WindowTaskBean>> datas = WindowTaskBean.parseDatas(tasksJa);
				Intent intent = new Intent(mActivity, isTask ? TaskListActivity.class : PickupListActivity.class);
				intent.putExtra("entryBean", entryBean);
				intent.putExtra("equipmentBean", equipmentBean);
				intent.putExtra("datas", (Serializable) datas);
				intent.putExtra("isOnlyTractor", isOnlyTractor(bean));
				startActivity(intent);
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);

	}

	@Override
	protected void onBackBtnOrKey() {
		// RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new
		// DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// // TODO Auto-generated method stub
		// RequestParams params = new RequestParams();
		// params.add("checkin", "1");
		// reqSubmit(params);
		// }
		// });
		showBackDialog();
	}

	private void showBackDialog() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setArrowItems(new String[] { getString(R.string.window_check_in),
				getString(R.string.window_waiting),
				getString(R.string.tms_back) }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
					checkinOnClick(null);
				}
				if (position == 1) {
					waitingOnClick(null);
				}
				if (position == 2) {
					Utility.popTo(mActivity, SearchCheckInWindowActivity.class);
				}
			}
		});
		builder.create().show();
	}

	public void checkinOnClick(View v) {
		showPriorityBuilder(true);
	}

	public void waitingOnClick(View v) {
		showPriorityBuilder(false);
	}

	// public void backOnClick(View v) {
	// Utility.popTo(mActivity, SearchCheckInWindowActivity.class);
	// }

	private void reqSubmit(boolean isCheckIn, int priority) {
		RequestParams params = new RequestParams();
		params.add("Method", "AddOrUpdateTask");
		params.add("mainId", mEntry.entry_id);
		params.add("items", "[]");
		params.add("appointment_date", "");
		params.add(isCheckIn ? "checkin" : "waiting", "1");
		params.add("priority", priority + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(final JSONObject json) {
				Intent data = new Intent();
				data.putExtra("refresh", true);
				setResult(RESULT_CODE, data);
				finish();
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	private void showPriorityBuilder(final boolean isCheckIn) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.window_select_priority));
		builder.setArrowItems(new String[] { getString(R.string.window_priority_h), getString(R.string.window_priority_m), getString(R.string.window_priority_l), "NA" }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int priority = CheckInEntryPriorityKey.getPositionToKey(position);
				reqSubmit(isCheckIn, priority);
			}
		});
		builder.create().show();
	}

}
