package oso.ui.load_receive.print;

import oso.ui.load_receive.print.bean.PrintPackinglistBean;
import support.common.bean.CheckInLoadBarBean;


public interface PackingItemInterface {
	public void selectItemFinish(PrintPackinglistBean packitem,int i) ;
	public void selectItemFinish(CheckInLoadBarBean item,int i);
	public void loadbarnumber(int i,String s);
	public void morepackinglist(String id);
	public int morepackinglist();
}
