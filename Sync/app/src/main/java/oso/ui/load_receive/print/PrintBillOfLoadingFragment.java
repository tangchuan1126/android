package oso.ui.load_receive.print;

import java.util.List;

import oso.ui.load_receive.print.adapter.PrintTicketAdapter;
import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.iface.ReceiptsTicketDataInterface;
import oso.ui.load_receive.print.iface.TicketData;
import oso.ui.load_receive.print.util.PrintOtherCondition;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import declare.com.vvme.R;
import oso.widget.dialog.RewriteBuilderDialog;

public class PrintBillOfLoadingFragment extends Fragment {

	private static String SHIP_TO_BLACK_LIST_NAME = "kohls";
	private View view ;
	private Context context;
	private Bundle bundle ;
	Activity activity;
	private List<ReceiptBillBase> receiptsTicketPartenBaseList;
	private PrintTicketAdapter receiptsTicketAdapter;
	private ListView receiptsTicketBase_listview;
	TicketData ticketData;
	private PrintPackMainBeanModel receiptTicketModel;
	private ReceiptsTicketDataInterface receiptsTicketDataInterface;
	
	private PrintOtherCondition printOtherCondition;
	
	private ImageView print_nodata_image;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.print_bill_loading_layout, container, false);
		return view;
	}
	 
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 		context =  getActivity();
		bundle = getArguments() ;
		activity = this.getActivity();
		receiptTicketModel = (PrintPackMainBeanModel)bundle.getSerializable("receiptTicketModel");
		receiptsTicketPartenBaseList = receiptTicketModel.getReceiptsTicketBaseList();
		if (!isShipToInBlackList()) {
			initViews();
			setViewData();
		} else {
			RewriteBuilderDialog.showSimpleDialog_Tip(context, getString(R.string.bol_print_restrict_msg));
		}
	}
	@Override
	public void onAttach(Activity activity) {
 		super.onAttach(activity);
 		ticketData = (TicketData)activity;
	}
	private void initViews() {
		receiptsTicketBase_listview = (ListView) view.findViewById(R.id.receipts_ticketbase_listview);
		printOtherCondition = (PrintOtherCondition) view.findViewById(R.id.PrintOtherCondition);
		print_nodata_image = (ImageView) view.findViewById(R.id.print_nodata_image);
		if(printOtherCondition==null){
			return;
		}
		if(printOtherCondition.jumpShowOtherCondition(receiptTicketModel.getReceiptTicketBase())){
			printOtherCondition.setVisibility(View.VISIBLE);
			printOtherCondition.init(receiptTicketModel.getReceiptTicketBase(),activity);
		}
	}
	private void setViewData(){
		
		receiptsTicketDataInterface = new ReceiptsTicketDataInterface() {
			@Override
			public void preview(ReceiptBillBase r,int type) {
				Intent intent = new Intent();
				intent.setClass(activity, PrintPreViewWebViewActivity.class);
				intent.putExtra("receiptTicketModel", receiptTicketModel);
				intent.putExtra("receiptBillBase", r);
				intent.putExtra("type", type);
				startActivityForResult(intent, PrintTicketLoadDataActivity.PrintBillOfLoadingFragment);
			}

			@Override
			public void preview(ReceiptBillBase r) {
				// TODO Auto-generated method stub
				
			}
		};
		receiptsTicketAdapter = new PrintTicketAdapter(context, this.receiptsTicketPartenBaseList,receiptsTicketDataInterface,receiptTicketModel);
		receiptsTicketBase_listview.setDivider(null);
		receiptsTicketBase_listview.setAdapter(receiptsTicketAdapter);
		if(receiptsTicketPartenBaseList.size() > 0 && receiptsTicketPartenBaseList!= null){
			print_nodata_image.setVisibility(View.GONE);
		}else{
			print_nodata_image.setVisibility(View.VISIBLE);
		}
	}

	private boolean isShipToInBlackList() {
		if (receiptsTicketPartenBaseList != null && receiptsTicketPartenBaseList.size() > 0) {
			for (ReceiptBillBase base:receiptsTicketPartenBaseList) {
				String shipToName = base.getShipToName();
				String shipToID = base.getShipToID();
				if (shipToName != null && shipToName.equalsIgnoreCase(SHIP_TO_BLACK_LIST_NAME)) {
					return true;
				}

				if (shipToID != null && shipToID.equalsIgnoreCase(SHIP_TO_BLACK_LIST_NAME)) {
					return true;
				}
			}
			return false;

		} else {
			return false;
		}
	}
	
	
}
