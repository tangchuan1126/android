package oso.ui.load_receive.do_task.receive.oso.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import support.common.bean.CheckInLoadPalletTypeBean;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;

public class ComplexReceiveOsoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1090914080328810994L;
	
	//order相关
	public int system_type;
	public int order_type;
	public int order_level;
	
	public List<CheckInLoadPalletTypeBean> listPalletTypes;
	public List<ReceiveOsoBean> listPallets;
	
	//手工加入===================
	public CheckInTaskBeanMain doorBean;
	public CheckInTaskItemBeanMain taskBean;
//	//TMS
//	public TmsTaskBeanMain tmsmainBean;
//	public TmsTaskItemBeanMain tmstaskBean;

	public static void parseBean(JSONObject jo,ComplexReceiveOsoBean b,boolean isLoad){
		b.listPalletTypes=new ArrayList<CheckInLoadPalletTypeBean>();
		CheckInLoadPalletTypeBean.parseBeans(jo.optJSONArray("pallettypes"), b.listPalletTypes);
		
		JSONObject joOrder=jo.optJSONObject("ordersystem");
		b.system_type=joOrder.optInt("system_type");
		b.order_type=joOrder.optInt("order_type");
		b.order_level=joOrder.optInt("order_level");
		
		//pallets
		b.listPallets=new ArrayList<ReceiveOsoBean>();
		ReceiveOsoBean.parseBeans(jo.optJSONArray("datas"), b.listPallets,isLoad);
	}
	
}
