package oso.ui.load_receive.do_task.main.adapter;

import java.util.ArrayList;
import java.util.List;

import support.common.bean.CheckInTaskBeanMain;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyStatusTypeKey;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckInEquipmentListAdapter extends BaseAdapter {
		List<CheckInTaskBeanMain> listPerson;
		private LayoutInflater inflater;
		private Context context;
		public CheckInEquipmentListAdapter(Context context,List<CheckInTaskBeanMain> list) {
			this.context = context;
			listPerson = new ArrayList<CheckInTaskBeanMain>();
			listPerson = list;
			this.inflater  = LayoutInflater.from(this.context);
		}
	
		@Override
		public int getCount() {
			return listPerson != null ? listPerson.size() : 0;
		}
	
		@Override
		public Object getItem(int position) {
			return listPerson.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return position;
		}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		EHolder holder;
		if (convertView==null) {
			convertView = inflater.inflate(R.layout.item_task_entry_right_two,null);
			holder = new EHolder();
			holder.tractor_txt = (TextView) convertView.findViewById(R.id.tractor_txt);
			holder.door_txt = (TextView) convertView.findViewById(R.id.door_txt);
			holder.t_number = (TextView) convertView.findViewById(R.id.t_number);
			holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
			holder.equipment_txt = (TextView) convertView.findViewById(R.id.equipment_txt);
			holder.rel_type = (TextView) convertView.findViewById(R.id.rel_type);
			convertView.setTag(holder);
		} else {
			holder = (EHolder) convertView.getTag();
		}
		// 刷新-数据
			CheckInTaskBeanMain b = listPerson.get(position);
			if(b.resources_id == 0 || b.resources_type == 0){
				holder.door_txt.setVisibility(View.GONE);
			}
			if(b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR){
				holder.imageView1.setImageResource(R.drawable.ic_tractor);
				
			}
			if(b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER){
				holder.imageView1.setImageResource(R.drawable.ic_trailer);
			}
			if(b.total_task == 0 ){
				holder.t_number.setVisibility(View.GONE);
			}
			holder.t_number.setText(b.total_task+"");
			holder.equipment_txt.setText(CheckInMainDocumentsStatusTypeKey.getReturnStatusById(context,b.equipment_status)+"");
			if(b.rel_type > 0){
			holder.rel_type.setText(CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(context,b.rel_type)+"");
			}
			holder.tractor_txt.setText(b.equipment_number + "");
			holder.door_txt.setText(OccupyStatusTypeKey.getContainerTypeKeyValue(b.occupy_status,context)+" "+b.resources_type_value + ":"+b.resources_name.toString() + "");
			return convertView;
	}
}

class EHolder {
	TextView tractor_txt;
	TextView door_txt;
	TextView t_number;
	ImageView imageView1;
	TextView equipment_txt;
	TextView rel_type;
}
