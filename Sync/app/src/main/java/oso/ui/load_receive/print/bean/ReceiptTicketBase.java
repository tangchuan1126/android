package oso.ui.load_receive.print.bean;

import java.io.Serializable;

import org.json.JSONObject;

import utility.StringUtil;
 
public class ReceiptTicketBase implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	private String companyid;
	private String entryid;
	private String load_no;
	private String time;  
	private String container_no;  
	private String customerid; 
	private String company_name;
	private String path;
	private String order_no;
	
	
	//-----------用于打印页面的window弹窗  由外部赋值解析赋值
	private int trailer_loader;
	private int freight_counted;
	private String print_customer_id;//由于是后加信息 所以需手动添加 不用于解析
	
	
	/**
	 * @Description:解析获取loading_ticket的bean
	 * @param @param json
	 * @param @return
	 */
	public static ReceiptTicketBase parsePrintTicketBase(JSONObject json) {
		JSONObject loading_ticket = StringUtil.getJsonObjectFromJSONObject(json, "loading_ticket");
		ReceiptTicketBase receiptTicketBase = new ReceiptTicketBase();
		receiptTicketBase.setEntryid(StringUtil.getJsonString(loading_ticket, "entryid"));
		receiptTicketBase.setLoad_no(StringUtil.getJsonString(loading_ticket, "loadno"));
		receiptTicketBase.setTime(StringUtil.getJsonString(loading_ticket, "window_check_in_time"));
		receiptTicketBase.setContainer_no(StringUtil.getJsonString(loading_ticket, "gate_container_no"));
		receiptTicketBase.setCompany_name(StringUtil.getJsonString(loading_ticket, "company_name"));
		receiptTicketBase.setPath(StringUtil.getJsonString(loading_ticket, "path"));
		
		receiptTicketBase.setOrder_no(StringUtil.getJsonString(loading_ticket, "order_no"));
		
		receiptTicketBase.setCompanyid(StringUtil.getJsonString(loading_ticket, "companyid"));
		receiptTicketBase.setCustomerid(StringUtil.getJsonString(loading_ticket, "customerid"));
		return receiptTicketBase;
	}
	
	
	public String getOrder_no() {
		return order_no;
	}
	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getEntryid() {
		return entryid;
	}
	public void setEntryid(String entryid) {
		this.entryid = entryid;
	}
	public String getLoad_no() {
		return load_no;
	}
	public void setLoad_no(String load_no) {
		this.load_no = load_no;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getContainer_no() {
		return container_no;
	}
	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}


	public int getTrailer_loader() {
		return trailer_loader;
	}


	public void setTrailer_loader(int trailer_loader) {
		this.trailer_loader = trailer_loader;
	}


	public int getFreight_counted() {
		return freight_counted;
	}


	public void setFreight_counted(int freight_counted) {
		this.freight_counted = freight_counted;
	}


	public String getPrint_customer_id() {
		return print_customer_id;
	}


	public void setPrint_customer_id(String print_customer_id) {
		this.print_customer_id = print_customer_id;
	} 

 
	
}
