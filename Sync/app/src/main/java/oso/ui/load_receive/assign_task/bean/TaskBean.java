package oso.ui.load_receive.assign_task.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

public class TaskBean implements Serializable {
	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = -2935066810608957944L;
	public String entry_id;
	public String equipment_number;
	public int total_task;
	public int equipment_id;
	public int equipment_type;
	public int assign_task;
	public int rel_type;

	public static List<TaskBean> helpJson(JSONObject json) {
		List<TaskBean> list = null;
		JSONArray jsonArray = json.optJSONArray("data");
		if (!StringUtil.isNullForJSONArray(jsonArray)) {
			list = new ArrayList<TaskBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				TaskBean bean = new TaskBean();
				bean.entry_id = item.optString("entry_id");
				bean.equipment_type = item.optInt("equipment_type");// 1,
				bean.equipment_number = item.optString("equipment_number");
				bean.total_task = item.optInt("total_task");
				bean.equipment_id = item.optInt("equipment_id");
				bean.assign_task = item.optInt("assign_task");
				bean.rel_type = item.optInt("rel_type");
				list.add(bean);
			}
		}
		return list;
	}
}
