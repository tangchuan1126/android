package oso.ui.load_receive.do_task.receive.task.bean;

import java.io.Serializable;

import android.text.TextUtils;

public class Rec_CLPType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2779667566408861615L;
	
	public String lpt_id;
	public String lp_name;				//名称,如:CLP1*2*3[48*48]
    public String type_name;          //lp类型,如:48*48
    public int type_id;
	public int stack_height_qty;		
	public int stack_length_qty;
	public int stack_width_qty;
	//
	public String ship_to_ids = "0";
	//
	public String ship_to_names;
	
	
	
	public boolean isSelect = false;
	
	public String upperCase(){
		return TextUtils.isEmpty(lp_name)? "":lp_name.substring(0,1);
	}
	
	public boolean isOverrageCount(int count,int remain){
		int num = stack_height_qty* stack_length_qty* stack_width_qty * count;
		return remain >= num;
	}
	
}
