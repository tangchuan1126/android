package oso.ui.load_receive.do_task.receive.task.movement.bean;

import java.io.Serializable;

public class Rec_Movement_InfoPalletBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4742674196919478147L;
	
	public String container;
	
	public String location;
	
	public int location_exists;

}
