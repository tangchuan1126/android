package oso.ui.load_receive.do_task.receive.task.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import oso.ui.load_receive.do_task.load.smallparcel.CC_ClpActivity;
import oso.ui.load_receive.do_task.load.smallparcel.CarrierActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_ShipToBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.SimpleTextWatcher;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.location.ChooseLocationActivity;
import oso.widget.location.ChooseStagingActivity;
import oso.widget.location.bean.LocationBean;
import oso.widget.selectoccupybutton.CardButton;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import declare.com.vvme.R.color;

public class QtyDialog_CC implements OnClickListener {

	private Activity mContext;
	private BottomDialog dialog;

	public int remain; // 剩余qty
	private String rnNO;

	private LocBean defLoc; // 默认location
	
	//private String receipt_line_id;
	
	private List<Rec_ShipToBean> shipToList = new ArrayList<Rec_ShipToBean>();
	private List<Rec_CLPType> clpTypeList = new ArrayList<Rec_CLPType>();
	
	private List<Rec_ShipToBean> shipToSelects = new ArrayList<Rec_ShipToBean>();
	

	public QtyDialog_CC(Activity c, int remain,
			String rnNO, LocBean defLoc,List<Rec_ShipToBean> shipTo,List<Rec_CLPType> clpType) {
		mContext = c;
		this.remain = remain;
		this.rnNO = rnNO;
		this.defLoc = defLoc;
		
		if(!Utility.isEmpty(shipTo)){
			this.shipToList.addAll(shipTo);
			//paixu
			Collections.sort(shipToList, new ShipToComparator());
		}
		if(!Utility.isEmpty(clpType)) 
		{  
			this.clpTypeList.addAll(clpType);
		}
		
		initDialog();
	}

	public static View view;
	private static EditText qtyedit;
	private TextView tvLoc;

	public int getAddMode() {
		return ssb.getCurrentSelectItem().b;
	}

	private boolean hasAutoRemainQty() {
		return loRemainPlt.getVisibility() == View.VISIBLE && auto_remainQty > 0;
	}
	/**
	 * plt数
	 * 
	 * @return
	 */
	public int getQty() {
		return Utility.parseInt(qtyedit.getText().toString());
	}
	
	public int getTotalQty(){
		return Utility.parseInt(etTotal.getText().toString());
	}

	/**
	 * 获取-网络参数
	 */
	public RequestParams getNetParam(String receipt_line_id,String item_id,String lot_no,String detail_id) {

		// createContainerForCC?
		// receipt_line_id=428&
		// item_id=M551D-A2&
		// lot_no=125088734001
		// &length=2&width=2&height=1&
		// pallet_type=40X80&
		// location=A004&location_type=2&
		// note=urgency&
		// pallet_qty=2&
		// license_plate_type_id=10010&
		// adid=100198&
		// is_return_plts=1

		RequestParams p = new RequestParams();
		p.add("receipt_line_id", receipt_line_id);
		p.add("item_id", item_id);
		p.add("lot_no", lot_no);
		p.add("detail_id", detail_id);
		p.add("length",sel_clpType.stack_length_qty+"");
		p.add("width", sel_clpType.stack_width_qty+"");
		
		p.add("height",sel_clpType.stack_height_qty+"");
		p.add("pallet_type", sel_clpType.type_id+"");
        //A004
		if(sel_locationBean !=null){
			p.add("location", sel_locationBean.location_name);
			p.add("location_id", sel_locationBean.id+"");
		}else{
			p.add("location", defLoc.name);
			p.add("location_id", defLoc.id+"");
		}
		
        //1:location 2:staging
		p.add("location_type", cardBtn.isCover()?"1":"2");
		p.add("note","");
		
//        "10010"
		p.add("license_plate_type_id",sel_clpType.lpt_id);
		p.add("is_return_plts", "0");
		if(shipTo != null)p.add("ship_to", shipTo.ship_to_id);
		
		switch (getAddMode()) {
		case Mode_Plt:
			p.add("pallet_qty",getQty()+"");
			if(hasAutoRemainQty()){
				p.add("remainder_length", auto_remainQty + "");
			}
			break;
		case Mode_Qty:
			int totalQty = getTotalQty();
			int hlw = sel_clpType.stack_height_qty*sel_clpType.stack_length_qty*sel_clpType.stack_width_qty;
			int pltInteger= totalQty / hlw;
			int pltRemainder = totalQty % hlw;
			p.add("pallet_qty", pltInteger+"");
			if(pltRemainder > 0){
				p.add("remainder_length", pltRemainder + "");
			}
			break;
		}
		return p;
	}

	public String getReqUrl() {
		return HttpUrlPath.createContainerForCC;
	}
	
	private TextView tvShipTo,tvCLPType,tvRemain_dlg;
	private TextView tvRemainPlt;
    private View loClpType,loRemainPlt;
    private CardButton cardBtn;
    
    private EditText etTotal;
    
    private Button shipToDel,clpTypeDel;
    private Button btnAuto;
    
    private LinearLayout llShipTo;
    
    private SingleSelectBar ssb;
    
    private int auto_remainQty;
    
    public static final int Mode_Plt = 0;
    public static final int Mode_Qty = 1;

	private void initDialog() {
		view = LayoutInflater.from(mContext).inflate(R.layout.dlg_rec_ccqty,
				null);
		dialog = new BottomDialog(mContext);
		dialog.setContentView(view);
		dialog.setTitle("Pallet Configuration");
		// setData();
		
		tvLoc = (TextView) view.findViewById(R.id.tvLoc);
		etTotal= (EditText) view.findViewById(R.id.etTotal);
		qtyedit=(EditText)view.findViewById(R.id.qtyedit);
		tvShipTo=(TextView)view.findViewById(R.id.tvShipTo);
        loClpType=view.findViewById(R.id.loClpType);
        loRemainPlt=view.findViewById(R.id.loRemainPlt);
        tvCLPType=(TextView)view.findViewById(R.id.tvCLPType);
        cardBtn=(CardButton)view.findViewById(R.id.cardBtn);
        tvRemain_dlg=(TextView)view.findViewById(R.id.tvRemain_dlg);
        tvRemainPlt= (TextView) view.findViewById(R.id.tvRemainPlt);
        
        shipToDel = (Button) view.findViewById(R.id.btnShipToDel);
        clpTypeDel = (Button) view.findViewById(R.id.btnClpDel);
        btnAuto = (Button) view.findViewById(R.id.btnAuto);
        
        llShipTo = (LinearLayout) view.findViewById(R.id.llShipTo);
        
        shipToDel.setOnClickListener(this);
        btnAuto.setOnClickListener(this);
        
        tvLoc.setOnClickListener(this);
        
        qtyedit.addTextChangedListener(textWatcher);
        
        //v
  		ssb = (SingleSelectBar) view.findViewById(R.id.ssb);
  		initSingleSelectBar();
		
		//事件
		tvShipTo.setOnClickListener(this);
		tvCLPType.setOnClickListener(this);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {

            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                // TODO Auto-generated method stub
                if (!checkForm())
                    return;
                
                if(!sel_clpType.isOverrageCount(getQty(), remain)){
                	RewriteBuilderDialog.showSimpleDialog_Tip(mContext, mContext.getString(R.string.sync_create_clp));
                	return;
                }
                
                if (onSubmitQtyListener != null)
                    onSubmitQtyListener.onSubmit();
            }
        });

        //初始化
        tvRemain_dlg.setText(remain+"");
        
 		if (LocBean.isValid(defLoc)) {
 			tvLoc.setHint(defLoc.name);
 			cardBtn.showCover(defLoc.isAtLocation());
 		}
        
        cardBtn.setOnChangeTypeListener(new CardButton.OnChangeTypeListener() {
			@Override
			public void onChange(String value) {
				tvLoc.setText("");
				tvLoc.setHint("NA");
				sel_locationBean = null;
				defLoc = null;
			}
		});
	}
	
	private void initSingleSelectBar() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Plt. Based", 0));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("Qty. Based", 1));
		ssb.setUserDefineClickItems(clickItems);
		ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case Mode_Plt:
					view.findViewById(R.id.llTotal).setVisibility(View.GONE);
					view.findViewById(R.id.llQty).setVisibility(View.VISIBLE);
					view.findViewById(R.id.loRemainPlt).setVisibility(View.GONE);
					loRemainPlt.setVisibility(auto_remainQty > 0 ? View.VISIBLE : View.GONE);
					break;
				case Mode_Qty:
					view.findViewById(R.id.llTotal).setVisibility(View.VISIBLE);
					view.findViewById(R.id.llQty).setVisibility(View.GONE);
					view.findViewById(R.id.loRemainPlt).setVisibility(View.GONE);
					break;

				default:
					break;
				}
			}
		});
		ssb.userDefineSelectIndexExcuteClick(0);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvShipTo:	
			if(Utility.isEmpty(shipToSelects)){
				RewriteBuilderDialog.showSimpleDialog_Tip(mContext,"No Ship To!");
	             return;
			}
			//防止键盘弹起时 切换页面后 再返回时,对话框剧烈跳动
			Utility.colseInputMethod(mContext, loClpType);
			toShipToAc();
			break;
		case R.id.tvCLPType:
			if(Utility.isEmpty(clpTypeList)){
				RewriteBuilderDialog.showSimpleDialog_Tip(mContext, "No available CLP!");
				return;
			}
			//防止键盘弹起时 切换页面后 再返回时,对话框剧烈跳动
			Utility.colseInputMethod(mContext, loClpType);
			toClpTypeAc();
			break;
		case R.id.btnShipToDel:
			clearTvShipTo();
			break;
		case R.id.btnAuto:
			autoSumQty();
			break;
		case R.id.tvLoc:
			//防止键盘弹起时 切换页面后 再返回时,对话框剧烈跳动
			Utility.colseInputMethod(mContext, v);
			
			if(cardBtn.isCover()){
				Intent in = new Intent(mContext,ChooseLocationActivity.class);
				in.putExtra("single_select", true);
				mContext.startActivityForResult(in, QtyDialog.Location_Back);
			}else{
				Intent in = new Intent(mContext,ChooseStagingActivity.class);
				in.putExtra("single_select", true);
				mContext.startActivityForResult(in, QtyDialog.Staging_Back);
			}
			break;
		default:
			break;
		}
	}
	
	private void autoSumQty(){
		if(sel_clpType == null){
			RewriteBuilderDialog.showSimpleDialog_Tip(mContext, "Please select type CLP");
			return;
		}
		
		if(remain <=0){
			return;
		}
		
		int hlw = sel_clpType.stack_height_qty*sel_clpType.stack_length_qty*sel_clpType.stack_width_qty;
		
		int pltCnt= remain / hlw;
		
		qtyedit.setText(pltCnt+"");
		auto_remainQty = remain % hlw;
		tvRemainPlt.setText(auto_remainQty + " Items");
		
		loRemainPlt.setVisibility(auto_remainQty > 0 ? View.VISIBLE : View.GONE);
		
	}
	
	private String[] getCLP_strs(List<Rec_CLPType> list){
		if(Utility.isEmpty(list))
			return new String[0];
		
		String[] ret=new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			ret[i]=list.get(i).lp_name;
		}
		return ret;
	}
	private CharSequence[] getCLP_charSequence(List<Rec_CLPType> list){
		if(Utility.isEmpty(list))
			return new CharSequence[0];
		
		CharSequence[] rets = new CharSequence[list.size()];
		for (int i = 0; i < list.size(); i++) {
			Rec_CLPType clpType = list.get(i);
			String str2 = "("+clpType.stack_length_qty+"×"+clpType.stack_width_qty+"×"+clpType.stack_height_qty+")";
			rets[i] = Utility.getCompoundText(clpType.lp_name+"   ", str2,new ForegroundColorSpan(color.gray));
		}
		return rets;
	}
	
	private void clearTvShipTo(){
		tvShipTo.setHint("NA");
		tvShipTo.setText("");
		shipToDel.setVisibility(View.INVISIBLE);
		clpTypeDel.setVisibility(View.INVISIBLE);
		shipTo = null;
	}
	private void toShipToAc(){
		Intent in = new Intent(mContext, CarrierActivity.class);
		CarrierActivity.initParams(in, shipToSelects,shipTo);
		mContext.startActivityForResult(in,Req_SelShipTo);
	}
	
	private void toClpTypeAc(){
		Intent in = new Intent(mContext, CC_ClpActivity.class);
		CC_ClpActivity.initParams(in, clpTypeList,sel_clpType);
		mContext.startActivityForResult(in,Req_SelClpType);
	}
	
	private int Req_SelShipTo=123;
	private int Req_SelClpType = 124;
	private Rec_ShipToBean shipTo;
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode!=Activity.RESULT_OK)
			return;
		
		
		if (requestCode == Req_SelShipTo) {
			Bundle params = data.getExtras();
			int position = params.getInt("index");
			shipToResult(position);
		} else if (requestCode == Req_SelClpType) {
			Bundle params = data.getExtras();
			int position = params.getInt("index");
			clpTypeResult(position);
		}else if(requestCode == QtyDialog.Location_Back){
			List<LocationBean> lists = (List<LocationBean>) data.getSerializableExtra("SelectLocation");
			sel_locationBean = lists.get(0);
			if(!Utility.isEmpty(lists))tvLoc.setText(sel_locationBean.location_name);
			
		}else if(requestCode == QtyDialog.Staging_Back){
			List<LocationBean> stagings = (List<LocationBean>) data.getSerializableExtra("SelectStaging");
			sel_locationBean = stagings.get(0);
			if(!Utility.isEmpty(stagings))tvLoc.setText(sel_locationBean.location_name);
		}
	}

    private void clpTypeResult(int position) {
    	sel_clpType = clpTypeList.get(position);
    	
    	filterShipTo(sel_clpType);
    	
    	SetclpType(sel_clpType);
	}
    
    private void filterShipTo(Rec_CLPType clpType) {
    	
    	shipToSelects.clear();
    	
    	if(TextUtils.isEmpty(clpType.ship_to_names)){
    		shipToSelects.addAll(shipToList);
    		return;
    	}
    	if(!Utility.isEmpty(shipToList)){
    		String[] strs = clpType.ship_to_names.split(",");
    		for(Rec_ShipToBean bean : shipToList){
    			for(String str : strs){
    				if(bean.ship_to_name.equals(str)){
    					shipToSelects.add(bean);
    				}
    			}
    		}
    	}
	}

	private void SetclpType(Rec_CLPType clpType){
		//debug,修改clpType时 移除partial
    	clearAutoRemain();
		
		//清空
		if(clpType==null)
		{
			sel_clpType = null;
			tvCLPType.setText("");
			return;
		}
			
		//配置
    	String str2 = "("+clpType.stack_length_qty+"×"+clpType.stack_width_qty+"×"+clpType.stack_height_qty+")";
    	tvCLPType.setText(Utility.getCompoundText(clpType.lp_name+"   ", str2,new ForegroundColorSpan(color.gray)));
    
    	llShipTo.setVisibility(View.VISIBLE);
	}

	private void shipToResult(int position) {
		shipTo=shipToSelects.get(position);
        
        
        tvShipTo.setText(shipTo.ship_to_name);
        
        shipToDel.setVisibility(View.VISIBLE);
	}

	private Rec_CLPType sel_clpType;
	private LocationBean sel_locationBean;

	private void showDlg_selCLP(final  List<Rec_CLPType> list){
       // final String[] clps= getCLP_strs(list);
        final CharSequence[] clps = getCLP_charSequence(list);
		RewriteBuilderDialog.Builder bd=new RewriteBuilderDialog.Builder(mContext);
        bd.setTitle("Select CLP");
		bd.setItems_singleChoice(clps, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
                tvCLPType.setText(clps[position]);
                sel_clpType=list.get(position);
			}
		}, -1);
		bd.show();
	}
	
	private SimpleTextWatcher textWatcher = new SimpleTextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			super.afterTextChanged(s);
			clearAutoRemain();
		}
	};
	
	
	/**
	 * 移除auto多的partial,修改clpType/pltQty时
	 */
	private void clearAutoRemain(){
		auto_remainQty = 0;
//		loRemainPlt.setVisibility(getAddMode() == Mode_Plt && auto_remainQty > 0 ? View.VISIBLE : View.GONE);
		
		loRemainPlt.setVisibility(View.GONE);
	}

	/**
	 * @return true:有效
	 */
	private boolean checkForm() {

		// ===========qtyPerPlt=============================
        //clp
        if(sel_clpType==null){
            UIHelper.showToast(mContext.getString(R.string.sync_select_clp));
            return false;
        }
        // 位置
        if (sel_locationBean==null && defLoc == null) {
            UIHelper.showToast(mContext.getString(R.string.sync_input_location));
            return false;
        }
        
        switch(getAddMode()){
        case Mode_Plt:
        	if (getQty() == 0 && auto_remainQty == 0) {
    			UIHelper.showToast(mContext.getString(R.string.sync_select_pallet_qty));
    			return false;
    		}
        	
        	break;
        case Mode_Qty:
        	if(getTotalQty() == 0){
        		UIHelper.showToast(mContext.getString(R.string.sync_input_total));
        		return false;
        	}
        	if(getTotalQty() > remain){
        		UIHelper.showToast(mContext.getString(R.string.sync_input_beyond));
        		return false;
        	}
        	break;
        }

		return true;
	}

	public void show() {
		dialog.show();
	}

	public void dismiss() {
		dialog.dismiss();
	}

	public OnSubmitQtyListener getOnSubmitQtyListener() {
		return onSubmitQtyListener;
	}

	public void setOnSubmitQtyListener(OnSubmitQtyListener onSubmitQtyListener) {
		this.onSubmitQtyListener = onSubmitQtyListener;
	}

	private OnSubmitQtyListener onSubmitQtyListener;

	public interface OnSubmitQtyListener {
		void onSubmit();
	}
	
	
	private class ShipToComparator implements Comparator<Rec_ShipToBean>{

		@Override
		public int compare(Rec_ShipToBean lhs, Rec_ShipToBean rhs) {
			String str1 = lhs.ship_to_name.toUpperCase();
			String str2 = rhs.ship_to_name.toUpperCase();
			return str1.compareTo(str2);
		}
		
	}
}
