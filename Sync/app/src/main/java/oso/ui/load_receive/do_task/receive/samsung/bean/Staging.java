package oso.ui.load_receive.do_task.receive.samsung.bean;

import java.io.Serializable;

public class Staging implements Serializable {
	
	private static final long serialVersionUID = -138216054235202209L;
	
	private String location_name;
	private String location_id;
	
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getLocation_id() {
		return location_id;
	}
	public void setLocation_id(String location_id) {
		this.location_id = location_id;
	}
	
	@Override
	public String toString() {
		return "Staging [location_name=" + location_name + ", location_id=" + location_id + "]";
	}
	
}
