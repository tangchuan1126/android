package oso.ui.load_receive.do_task.receive.task;


import java.util.List;

import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;



import declare.com.vvme.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SelectTypeListView extends FrameLayout implements OnItemClickListener {

	
	private Context mContext;
	private ListView lv;
	
	private List<Rec_PalletType> palletTypes;
	
	private SelectAdapter adapter;
	
	public SelectTypeListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public SelectTypeListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SelectTypeListView(Context context) {
		super(context);
		init(context);
	}
	
	private void init(Context context){
		
		mContext = context;
		lv = new ListView(context);
		lv.setBackgroundColor(Color.TRANSPARENT);
		lv.setCacheColorHint(Color.TRANSPARENT);
		
		addView(lv,-1,-1);
		
		lv.setOnItemClickListener(this);
		
		lv.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				return true;
			}
		});
	}
	
	public void setAdapter(List<Rec_PalletType> dataAll) {
		if (dataAll == null || dataAll.size() == 0) {
			return;
		}
		palletTypes = dataAll;
		initDataHandler.sendEmptyMessage(0);
	}
	
	private Handler initDataHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (adapter == null) {
				adapter = new SelectAdapter();
				lv.setAdapter(adapter);
			}
			adapter.notifyDataSetChanged();
		}

	};


	public OnItemOrderClickListener getOnItemClickListener() {
		return onItemClickListener;
	}

	public void setOnItemClickListener(OnItemOrderClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	private OnItemOrderClickListener onItemClickListener;

	public interface OnItemOrderClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id, Rec_PalletType letter);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (getOnItemClickListener() != null)
			getOnItemClickListener().onItemClick(parent, view, position, id, palletTypes.get(position));
	}
	
	public int getSelectedItemPosition() {
		return lv.getSelectedItemPosition();
	}

	public void setSelection(int position) {
		lv.setSelection(position);
	}

	public void setEmptyView(View emptyView) {
		lv.setEmptyView(emptyView);
	}
	
	
	private class SelectAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return palletTypes==null ? 0 : palletTypes.size();
		}

		@Override
		public Object getItem(int position) {
			return palletTypes.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder;
			if(convertView == null){
				convertView = View.inflate(mContext, R.layout.item_selecttypepallet, null);
				holder = new ViewHolder();
				holder.carrierTv = (TextView) convertView.findViewById(R.id.carrierTv);
				holder.box = (CheckBox) convertView.findViewById(R.id.box);
				
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			Rec_PalletType type = palletTypes.get(position);
			holder.carrierTv.setText(type.type_name);
			
			holder.box.setChecked(type.isselect);
			
			return convertView;
		}
    	
    }
    
    class ViewHolder{
    	public TextView carrierTv;
		public CheckBox box;
    }
}
