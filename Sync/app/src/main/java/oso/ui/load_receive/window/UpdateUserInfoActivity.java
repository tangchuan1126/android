package oso.ui.load_receive.window;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.window.bean.WindowBean;
import oso.ui.load_receive.window.util.UpdateBuilderSubmitTwoDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.adapter.AdpCarrier;
import oso.widget.dlgeditview.adapter.AdpDriver;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.CheckInEntryPriorityKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class UpdateUserInfoActivity extends BaseActivity {

	private TabToPhoto ttp;
	WindowBean bean = new WindowBean();
	TextView window_entryid;
	EditText etMcDot;
	EditText etCarrier;
	EditText window_liscense;
	EditText window_name;
	TextView window_proprity;
	Button submit_button;
	String backfillcrr;
	String backfillmc;

	int oldId = 0;

	public final static int ALLMATCH = 1; // 全部匹配 mcdot && carrier 直接添加不用提示
	public final static int ONLYMCDOTMATCH = 2; // 只有DOT匹配 mcdot && !carrier
												// 返回系统的值 和 填入的值，让他选择更新Carrier
												// 也可以不用更新
	public final static int ONLYCARRIERMATCH = 3; // 只有CARRIER匹配 !mcdot &&
													// carrier 返回系统的值 和填入的值
													// ，让他选择更新McDot , 也可以不用更新
	public final static int NOTMATCH = 4; // 都不匹配 !mcdot && !carrier
											// 提示他是否添加这组关系到数据库
	public final static int NotValidate = 5;

	ArrayAdapter<String> arrayadapter;

	public static String windowNameStr = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.window_checkin_layout, 0);
		initData();
	}

	private void initData() {
		setTitleString(getString(R.string.checkin_driver_info));
		bean = (WindowBean) getIntent().getSerializableExtra("windowmain");
		window_entryid = (TextView) findViewById(R.id.window_entryid);
		etMcDot = (EditText) findViewById(R.id.window_mc_dot);
		etCarrier = (EditText) findViewById(R.id.window_carrier);
		// etMcDot.setMainLayout(vMiddle);
		// etMcDot.setMethod("SearchMcDot");
		// etMcDot.hideKbLayout(0);
		// etCarrier.setMainLayout(vMiddle);
		// etCarrier.setMethod("SearchCarrier");
		// etCarrier.hideKbLayout(1);
		window_liscense = (EditText) findViewById(R.id.window_liscense);
		window_liscense.setTransformationMethod(new AllCapTransformationMethod());
		window_name = (EditText) findViewById(R.id.window_name);
		window_name.setTransformationMethod(new AllCapTransformationMethod());
		window_proprity = (TextView) findViewById(R.id.window_proprity);
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		ttp.init(mActivity, getTabParamList());
		submit_button = (Button) findViewById(R.id.submit_button);

		// etCarrier.setOnSearchListener(new
		// SearchTextTwoView.OnSearchListener() {
		//
		// @Override
		// public void onSearch(MCBean value) {
		// // TODO Auto-generated method stub
		// etMcDot.setText(value.mc_dot + "");
		//
		// }
		// });
		// etMcDot.setOnSearchListener(new SearchTextTwoView.OnSearchListener()
		// {
		//
		// @Override
		// public void onSearch(MCBean value) {
		// // TODO Auto-generated method stub
		// etCarrier.setText(value.carrier + "");
		// }
		// });

		initCarrierTip();
		initDriverTip();

		initView();
	}

	// ==================nested===============================
	private AdpDriver adpDriver;

	private void initDriverTip() {
		adpDriver = new AdpDriver(window_liscense, window_name);

		// debug
		DlgEditView.attachOnFocus(window_liscense, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v).setMethod("SearchGateDriverLiscense").setHint("Driver License").setDefKeyboard(true)
						.setAdp(adpDriver);
			}
		});
	}

	private AdpCarrier adpCarrier;

	private void initCarrierTip() {
		adpCarrier = new AdpCarrier(etCarrier, etMcDot);

		// debug
		DlgEditView.attachOnFocus(etMcDot, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				adpCarrier.isChange_carrier = false;
				DlgEditView.show(mActivity, (TextView) v).setMethod("SearchMcDot").setHint("MC / DOT").setAdp(adpCarrier);
			}
		});
		// debug
		DlgEditView.attachOnFocus(etCarrier, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				adpCarrier.isChange_carrier = true;
				DlgEditView.show(mActivity, (TextView) v).setMethod("SearchCarrier").setHint("Carrier").setAdp(adpCarrier);
			}
		});

	}

	// =================================================

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		TabParam tab = new TabParam("Driver", "WindowCheckIn", new PhotoCheckable(0, "WindowImage", ttp), 40);
		tab.setWebImgsParams(String.valueOf(FileWithCheckInClassKey.PhotoGateCheckIn), String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN), bean.entry_id
				+ "");
		params.add(tab);
		return params;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return etCarrier.onKeyDown(keyCode, event) ? true : super.onKeyDown(keyCode, event);
	}

	public void initView() {
		if (bean != null) {
			window_entryid.setText(bean.entry_id + "");
			etMcDot.setText(bean.mc_dot + "");
			etCarrier.setText(bean.company_name);
			window_liscense.setText(bean.gate_driver_liscense + "");
			window_name.setText(bean.gate_driver_name);
			if (bean.priority != 0) {
				window_proprity.setText(CheckInEntryPriorityKey.getCheckInEntryPriorityKey(bean.priority));
			} else {
				window_proprity.setText("NA");
			}

		}
		window_proprity.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showPriorityBuilder();
			}
		});
		submit_button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				setwindow();
			}
		});
	}

	private void showPriorityBuilder() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.window_select_priority));
		builder.setArrowItems(new String[] { getString(R.string.window_priority_h), getString(R.string.window_priority_m), getString(R.string.window_priority_l), "NA" }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int priority = CheckInEntryPriorityKey.getPositionToKey(position);
				window_proprity.setText(CheckInEntryPriorityKey.getCheckInEntryPriorityKey(priority));
			}
		});
		builder.create().show();
	}

	private void setwindow() {
		RequestParams params = new RequestParams();
		params.add("Method", "UpdateEntryDriverInfo");
		params.add("entry_id", bean.entry_id + "");
		params.add("mc_dot", etMcDot.getText() + "");
		params.add("company_name", etCarrier.getText() + "");
		params.add("gate_driver_liscense", window_liscense.getText() + "");
		params.add("gate_driver_name", window_name.getText() + "");
		if (window_proprity.getText().equals(CheckInEntryPriorityKey.getCheckInEntryPriorityKey(CheckInEntryPriorityKey.HIGH))) {
			params.add("priority", "1");
		}
		if (window_proprity.getText().equals(CheckInEntryPriorityKey.getCheckInEntryPriorityKey(CheckInEntryPriorityKey.MIDDLE))) {
			params.add("priority", "2");
		}
		if (window_proprity.getText().equals(CheckInEntryPriorityKey.getCheckInEntryPriorityKey(CheckInEntryPriorityKey.LOW))) {
			params.add("priority", "3");
		}
		if (window_proprity.getText().equals("NA")) {
			params.add("priority", "0");
		}
		ttp.uploadZip(params, "Driver_Info");
		Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				int flag = json.optInt("flag");
				WindowBean newitem = new WindowBean();
				if (flag == ALLMATCH || flag == NotValidate) {
					success();
				}
				if (flag == ONLYMCDOTMATCH) {
					getnewitemjson(json, newitem);
					getoldId(json);
					openBuilder(json, newitem, ONLYMCDOTMATCH);
				}
				if (flag == ONLYCARRIERMATCH) {
					getnewitemjson(json, newitem);
					getoldId(json);
					openBuilder(json, newitem, ONLYCARRIERMATCH);
				}
				if (flag == NOTMATCH) {
					getnewitemjson(json, newitem);
					openBuilder(json, newitem, NOTMATCH);
				}
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	public WindowBean getnewitemjson(JSONObject json, WindowBean newitem) {
		try {
			JSONObject itemjson = json.getJSONObject("inputvalue");
			newitem.company_name = itemjson.getString("carrier");
			newitem.mc_dot = itemjson.getString("mc_dot");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newitem;
	}

	public void getoldId(JSONObject json) {
		try {
			JSONObject itemjson = json.getJSONObject("systemvalue");
			oldId = StringUtil.getJsonInt(itemjson, "id");// itemjson.getInt("id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void openBuilder(final JSONObject json, final WindowBean newitem, final int type) {
		final UpdateBuilderSubmitTwoDialog.Builder builder = new UpdateBuilderSubmitTwoDialog.Builder(mActivity);
		if (type == 2) {
			builder.setTitle("Update Carrier");
		}
		if (type == 3) {
			builder.setTitle("Update MC DOT");
		}
		if (type == 4) {
			builder.setTitle("Message");
		}
		builder.setMc_name("MC/DOT: ");
		builder.setMc_value(newitem.mc_dot + "");
		builder.setCarrier_name("Carrier: ");
		builder.setCarrier_value(newitem.company_name + "");
		builder.setSubmit(new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finishsubmit(newitem, type);
			}
		});
		builder.setClose(new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				UpdateUserInfoActivity.this.finish();
			}
		});

		builder.setCancelable(false);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private void finishsubmit(WindowBean item, int type) {
		RequestParams params = new RequestParams();
		params.add("Method", "HandMcDotAndCarrier");
		params.add("entry_id", bean.entry_id + "");
		params.add("id", oldId + "");
		params.add("flag", type + "");
		params.add("mc_dot", item.mc_dot + "");
		params.add("carrier", item.company_name + "");
		Goable.initGoable(mActivity);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				success();
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	public void success() {
		UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
		windowNameStr = window_name.getText().toString().trim();
		SearchCheckInWindowActivity.isRefreshDatas = true;
		Intent intent = new Intent(mActivity, EquipmentListActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("entry_id", bean.entry_id + "");
		startActivity(intent);
	}
}
