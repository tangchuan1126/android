package oso.ui.load_receive.do_task.receive.wms;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.ui.msg.bean.TaskTypeBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import support.AppConfig;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 创建ccTask
 * @author 朱成
 * @date 2015-5-18
 */
public class Receive_ScanTask extends BaseActivity implements OnClickListener {

	private TextView  tvPerson,tvRecNO, tvItem, tvCtnr, tvCompany,
			tvCustomer, tvTitle;
	private EditText etNote;
	
	private TextView notifyEt;

    
    private RadioGroup rgTask;
    private RadioButton rbCC;
    private RadioButton rbScan;
    
    private boolean cctask = false;
    
    private Rec_ItemBean itemBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_receive_pallets_sub, 0);
		setTitleString("Request CC/Scan Task");
		applyParams();

		// v
		tvRecNO = (TextView) findViewById(R.id.tvRecNO);
		tvItem = (TextView) findViewById(R.id.tvItem);
		tvPerson = (TextView) findViewById(R.id.tvPerson);
		etNote = (EditText) findViewById(R.id.etNote);
		tvCompany = (TextView) findViewById(R.id.tvCompany);
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		tvCtnr = (TextView) findViewById(R.id.tvCtnr);
        
        rgTask = (RadioGroup) findViewById(R.id.rg_task);
        rbCC = (RadioButton) findViewById(R.id.rb_cc);
        rbScan = (RadioButton) findViewById(R.id.rb_scan);
        
        notifyEt = (TextView)findViewById(R.id.notifyEt);

		// 事件
		//tvPerson.setOnClickListener(this);
        notifyEt.setOnClickListener(this);
		findViewById(R.id.btnSubmit).setOnClickListener(this);
        rgTask.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				if(rbCC.getId() == checkedId){
					cctask = true;
					notifyEt.setHint(getString(R.string.sync_leader_select));
				}else if(rbScan.getId() == checkedId){
					cctask = false;
					notifyEt.setHint(getString(R.string.sync_scanner_select));
				}
				notifyEt.setText("");
				svAdids = null;
			}
		});

		// 初始化
		tvRecNO.setText(recBean.receipt_no);
		tvItem.setText(recBean.curItemBean.item_id);
		tvCompany.setText(recBean.company_id);
		tvCustomer.setText(recBean.customer_id);
		tvTitle.setText(recBean.title);
		recBean.setTextCtnr(tvCtnr);
		
		notifyEt.setHint(getString(R.string.sync_scanner_select));
	}


	private final int What_ChooseSupervisor = 1;

	private String svAdids; // 管理员

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_CANCELED)
			return;

		switch (requestCode) {
		case What_ChooseSupervisor: // 选管理员
			String svNames = data.getStringExtra("select_user_names");
			svAdids = data.getStringExtra("select_user_adids");
			//tvPerson.setText(svNames);
			notifyEt.setText(svNames);
			break;

		default:
			break;
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSubmit: // 提交
			//debug
			reqSubmit();
			break;
		case R.id.tvPerson: // 选人
			
			break;
		case R.id.notifyEt:
				int dep=BaseDataDepartmentKey.Scanner;
				int pos=cctask? BaseDataSetUpStaffJobKey.LeadSuperVisor:BaseDataSetUpStaffJobKey.All;
		        //debug
				if (AppConfig.isDebug) {
					dep = BaseDataDepartmentKey.Warehouse;
					pos = BaseDataSetUpStaffJobKey.All;
				}
				Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
				intent.putExtra("selected_adids", svAdids);
				ChooseUsersActivity.initParams(intent, cctask? true : false, dep, pos,
						ChooseUsersActivity.KEY_REC_SUPERVISOR);
				startActivityForResult(intent, What_ChooseSupervisor);
			break;

		default:
			break;
		}
	}

	private void reqSubmit() {
		if (TextUtils.isEmpty(svAdids)) {
            UIHelper.showToast(cctask?getString(R.string.sync_leader_select) : getString(R.string.sync_scanner_select));
            return;
        }
		RequestParams p = new RequestParams();
		// receipt_line_id=560&operator=1000066&adid=100198&ps_id=1&task_type=62
		p.add("receipt_line_id", receipt_line_id);
		p.add("operator", svAdids);
		p.add("task_type", cctask ? TaskTypeBean.Type_Rec_CCTask + ""
				: TaskTypeBean.Type_Rec_ScanSN + ""); // 任务类型,62:CC 63:scan
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
//					itemBean.line_status = Rec_ItemBean.LineState_CCStart;
//					// 若全cnt完,提示打印
//					if (recBean.isAllLineCnted()) {
//						tipPrintReceiptTicket();
//					}
//					// 直接跳出
//					else
//					   Utility.popTo(mActivity, Receive_ItemListAc.class);
					
					itemBean.line_status = Rec_ItemBean.LineState_CCStart;
					//弹至"Item列表"
					Intent intent = new Intent(mActivity, Receive_ItemListAc.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					// 若全cnt完,提示打印
//					intent.putExtra(Receive_ItemListAc.NewIn_tipPrintRT, recBean.isAllLineCnted());
					//暂时-不提示打receiptTicket
					intent.putExtra(Receive_ItemListAc.NewIn_tipPrintRT,false);
					mActivity.startActivity(intent);	
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath
				+ "_receive/android/createCCAndScanTask", p, this);

		// .doGet(HttpUrlPath.basePath+"_receive/android/assignSupervisorTask",
		// p, this);

	}
	
	private PrintTool printTool = new PrintTool(this);
	
//	private void tipPrintReceiptTicket() {
//		RewriteBuilderDialog.Builder bd = RewriteBuilderDialog.newSimpleDialog(this, "Print Receipt Ticket?", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				// 打签
//				printTool.showDlg_printers(new PrintTool.OnPrintLs() {
//
//					@Override
//					public void onPrint(long printServerID) {
//						// TODO Auto-generated method stub
//						reqPrintRNTicket(printServerID);
//					}
//				});
//			}
//		});
//		bd.setNegativeButton(R.string.sync_no, new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				 Utility.popTo(mActivity, Receive_ItemListAc.class);
//			}
//		});
//		bd.show();
//	}

	private void reqPrintRNTicket(long printServerID) {
		RequestParams p = new RequestParams();
		p.add("company_id", recBean.company_id);
		p.add("receipt_no", recBean.receipt_no);
		p.add("printer", printServerID + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(getString(R.string.sync_print_success));
				 Utility.popTo(mActivity, Receive_ItemListAc.class);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/printRNCount", p, mActivity);

	}

	// =========传参===============================

	private String receipt_line_id;
	private Rec_ReceiveBean recBean;

	public static void initParams(Intent in, Rec_ReceiveBean recBean,
			String receipt_line_id) {
		in.putExtra("recBean", recBean);
		in.putExtra("receipt_line_id", receipt_line_id);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		recBean = (Rec_ReceiveBean) params.getSerializable("recBean");
		receipt_line_id = params.getString("receipt_line_id");
		itemBean = recBean.curItemBean;
	}

}
