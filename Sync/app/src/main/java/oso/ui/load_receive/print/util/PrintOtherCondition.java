package oso.ui.load_receive.print.util;

import oso.ui.load_receive.print.bean.ReceiptTicketBase;
import oso.ui.load_receive.print.iface.PrintOtherConditionIface;
import oso.ui.load_receive.print.iface.RefreshConditionIface;
import support.key.CustomerKey;
import support.key.PrintOtherConditionKey;
import utility.StringUtil;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * @ClassName: PrintOtherCondition 
 * @Description: 
 * @author gcy
 * @date 2015-3-20 下午12:08:55
 */
public class PrintOtherCondition extends LinearLayout implements OnClickListener,PrintOtherConditionIface{

	private Context context;
	private TextView trailer_loader;
	private TextView freight_counted;
	private ImageView edit_btn;
	private ReceiptTicketBase receiptTicketBase;
	private RefreshConditionIface iface;
	private Activity activity;
	
	public PrintOtherCondition(Context context) {
		super(context);
	}

	public PrintOtherCondition(Context context, AttributeSet attrs) {
		super(context, attrs);
		init_inner();
	}
	

	private void init_inner() {
		context=getContext();
		LayoutInflater.from(context).inflate(R.layout.show_other_condition_layout, this);
		trailer_loader = (TextView) findViewById(R.id.trailer_loader);
		freight_counted = (TextView) findViewById(R.id.freight_counted);
		edit_btn = (ImageView) findViewById(R.id.edit_btn);
		edit_btn.setOnClickListener(this);
	}
	/**
	 * @Description:辅助刷新web上面的信息 用于签字打印页面
	 */
	public void init(ReceiptTicketBase r,RefreshConditionIface iface,Activity activity){
		this.iface = iface;
		init(r,activity);
	}
	/**
	 * @Description:初始化当前页面信息
	 * @param @param r
	 */
	public void init(ReceiptTicketBase r,Activity activity){
		this.activity = activity;
		if(r==null){
			return;
		}
		this.receiptTicketBase = r;
		if(r.getFreight_counted()!=0){
			freight_counted.setText(PrintOtherConditionKey.getEightCounted(receiptTicketBase.getFreight_counted()));
		}
		if(r.getTrailer_loader()!=0){
			trailer_loader.setText(PrintOtherConditionKey.getTailerLoader(receiptTicketBase.getTrailer_loader()));
		}
		if(jumpShowOtherCondition(r)&&r.getFreight_counted()==0||r.getTrailer_loader()==0){
			PrintOtherConditionDialogWindow d = new PrintOtherConditionDialogWindow(context,receiptTicketBase,this);
			d.show();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.edit_btn:
			if(receiptTicketBase!=null){
				PrintOtherConditionDialogWindow d = new PrintOtherConditionDialogWindow(context,receiptTicketBase,this);
				d.show();
				d.setCancelable(true);
			}
			break;

		default:
			break;
		}
	}
	/**
	 * @Description:判断是否显示Trailer Loader，Freight Counted 这两个选项 ，如果为True的时候则显示 为false的时候不显示
	 * @param @return
	 */
	public boolean jumpShowOtherCondition(ReceiptTicketBase r){
		String customer = r.getPrint_customer_id();
		if(StringUtil.isNullOfStr(customer)){
			return true;
		}
		return StringUtil.isNullOfStr(CustomerKey.getType(customer.toUpperCase()));
	}
//	private void 

	@Override
	public void ChangeValue(ReceiptTicketBase receiptTicketBase) {
		trailer_loader.setText(PrintOtherConditionKey.getTailerLoader(receiptTicketBase.getTrailer_loader()));
		freight_counted.setText(PrintOtherConditionKey.getEightCounted(receiptTicketBase.getFreight_counted()));
		if(iface!=null){
			iface.RefreshValue();
		}
	}
	@Override
	public void CloseActivity(){
		if(activity!=null){
			activity.finish();
			activity.overridePendingTransition(R.anim.push_from_right_out,
					R.anim.push_from_right_in);
		}
	};
}
