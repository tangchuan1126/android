package oso.ui.load_receive.print.iface;

/**
 * @ClassName: RefreshConditionIface 
 * @Description: 用于签字打印页面更新条件选择之后的页面刷新
 * @author gcy
 * @date 2015-3-24 上午10:58:02
 */
public interface RefreshConditionIface {
	/**
	 * @Description:刷新页面的方法
	 */
	public void RefreshValue();
}
