package oso.ui.load_receive.do_task.load.scan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.MainActivity;
import oso.ui.load_receive.assign_task.SelectDoorActivity;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.do_task.load.scan.adapter.CheckInLoadPalletTypeBeanAdapter;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskExceptionActivity;
import oso.ui.load_receive.do_task.receive.oso.ReceiveOsoAc;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.VpWithTab;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.loadbar.VLoadBarBox;
import oso.widget.loadbar.VSealBox;
import oso.widget.lock.CheckScanTimes;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.AppConfig;
import support.common.UIHelper;
import support.common.bean.CheckInLoadOrderBean;
import support.common.bean.CheckInLoadOrderBean.PalletBean;
import support.common.bean.CheckInLoadPalletTypeBean;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInLoad_SubLoadBean;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.common.bean.LoginPostname;
import support.common.datas.HoldDoubleValue;
import support.common.print.OnInnerClickListener;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.key.BaseDataSetUpStaffJobKey;
import support.key.EntryDetailNumberTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.OccupyTypeKey;
import support.key.TTPKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @author 朱成
 * @date 2014-8-7
 */
// tts→
// 1.xx remain
// 2.order finish,xx remain 'xx:pallet数
// 3.load finish
// 4.invalid pallet
// 5.repeat
public class ScanLoadActivity extends BaseActivity implements OnClickListener {

	// 关闭-类型
	public static final int CloseType_Normal = 3; // order全加载完
	public static final int CloseType_Partially = 5; // 部分order加载完(不应有order加载到一半)
	public static final int CloseType_Exception = 4; // 异常(无条件限制)

	// ---------------------

	private final int SCANRST_TOO_FAST = -1;
	private final int ScanRst_NotFind = 0;
	private final int ScanRst_Find = 1;
	private final int ScanRst_HasScaned = 2;
	private Context context;

	// private LoadBarLayout loadbar;
	private TabToPhoto ttp;
	private SearchEditText etPallet;
	private TextView tvPalletsScaned, tvTotalPallets, tvOrdersScaned, tvTotalOrders, tvProNo;
	private Button btnNext;
	private CheckBox cbSelAll; // 全选(isSelected=false)/去全选
	private RewriteBuilderDialog dlgSelectPalletType;
	private VpWithTab vp;
	private SingleSelectBar tab;

	private TextView tvResType, tvMBolOrOrder, tvMBol, tvDock;// tvStaging

	private ExpandableListView lvOrders;
	private List<CheckInLoadOrderBean> listOrders;

	private CheckInLoad_ComplexSubLoads complexSubLoads;
	private CheckInTaskBeanMain doorBean;
	private CheckInTaskItemBeanMain taskBean;

	private CheckScanTimes ct; // 防止工人偷懒

	// ===============================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_load_scanload_main, 0);

		btnRight.setVisibility(View.VISIBLE);
		context = ScanLoadActivity.this;
		btnRight.setOnClickListener(this);
		btnRight.setBackgroundResource(R.drawable.menu_btn_style);

		applyParams();
		if (complexSubLoads == null) {
			// 重新登录,此处被kill的概率较大
			Utility.popTo(this, MainActivity.class);
			return;
		}
		if (complexSubLoads.load != null) {
			String title_1 = complexSubLoads.load.getMainType() == EntryDetailNumberTypeKey.CHECK_IN_LOAD ? "Load" : "Order";
			setTitleString(title_1 + ": " + complexSubLoads.load.getMainNo());
		}
		doorBean = complexSubLoads.doorBean;
		taskBean = complexSubLoads.taskBean;

		ct = new CheckScanTimes(mActivity);

		// 初始化vp
		vp = (VpWithTab) findViewById(R.id.vp);
		tab = (SingleSelectBar) findViewById(R.id.tab);
		initTab();
		vp.init(tab);
		initPhotoTab();
		// loadbar.initData(this,complexSubLoads.taskBean,p);

		// 取view
		lvOrders = (ExpandableListView) findViewById(R.id.lvOrders);
		etPallet = (SearchEditText) findViewById(R.id.etPallet);
		btnNext = (Button) findViewById(R.id.btnNext);
		cbSelAll = (CheckBox) findViewById(R.id.cbSelAll);

		tvOrdersScaned = (TextView) findViewById(R.id.tvOrdersScaned);
		tvTotalOrders = (TextView) findViewById(R.id.tvTotalOrders);
		tvPalletsScaned = (TextView) findViewById(R.id.tvPalletsScaned);
		tvTotalPallets = (TextView) findViewById(R.id.tvTotalPallets);
		tvMBolOrOrder = (TextView) findViewById(R.id.tvMBolOrOrder);
		tvMBol = (TextView) findViewById(R.id.tvMBol);
		tvDock = (TextView) findViewById(R.id.tvDock);
		tvResType = (TextView) findViewById(R.id.tvResType);

		tvProNo = (TextView) findViewById(R.id.tvProNo);
		// debug
		tvPalletType = (TextView) findViewById(R.id.tvPalletType);
		btnDisableDef = (Button) findViewById(R.id.btnDisableDef);
		// debug
		btnAddOrDel = (ImageButton) findViewById(R.id.btnAddOrDel);

		// 监听事件
		tvPalletType.setOnClickListener(this);
		btnDisableDef.setOnClickListener(this);
		findViewById(R.id.btnPrintSL).setOnClickListener(this);
		btnNext.setOnClickListener(this);
		// debug
		btnAddOrDel.setOnClickListener(this);

		// findViewById(R.id.cbPrint_layout_x).setOnClickListener(this);
		etPallet.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				// debug,防止-重复扫描,或扫描过快
				if (Utility.isFastClick())
					return;
				doScan();
			}
		});

		// 初始化
		refreshUI_summary();
		etPallet.setIconMode(SearchEditText.IconMode_Scan);

		// 基本信息
		CheckInLoad_SubLoadBean subload = complexSubLoads.getCurSubLoad();
		// tvResType.setText(complexSubLoads.taskBean.resources_type_value +
		// ": ");
		// tvDock.setText(complexSubLoads.taskBean.resources_name);
		refreshRes();

		boolean isLoad = complexSubLoads.load.isMainType_load();
		tvMBolOrOrder.setText(isLoad ? "MBOL: " : "Order: ");
		tvMBol.setText(isLoad ? subload.master_bol_no + "" : complexSubLoads.load.getMainNo());

		// 初始化lvLoads
		lvOrders.setAdapter(adpOrders);
		lvOrders.setEmptyView(findViewById(R.id.tvEmpty));
		lvOrders.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				onTouchEvent(event);
				return false;
			}
		});
		// debug
		lvOrders.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				// 若为子元素 不响应
				if (!adpOrders.isGrp(view))
					return false;

				int grpPos = adpOrders.getGrpPos(view);
				showDlg_onLongPress(listOrders.get(grpPos));

				// // debug
				// flowStatus.saveState(listOrders.get(position), null);
				// flowStatus.refreshFinishState();
				// if (flowStatus.isLastOrder_finished)
				// showDlg_inputProNo(true);
				// else
				// UIHelper.showToast(ScanLoadActivity.this,
				// "You can set the Pro NO after the order finished!");
				// 防止弹起时 列表展合
				return true;
			}
		});
		sortOrders();

		// 设置-默认焦点
		etPallet.requestFocus();

		// debug
		setDefType(null);
		// debug
		// taskBean.osoNote="Please be quick";
		if (taskBean.hasOsoNote()) {
			tipOsoNote();
			findViewById(R.id.vRightHint).setVisibility(View.VISIBLE);
		}

		if (complexSubLoads.isSequenceLoading()) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this, getString(R.string.loading_by_sequence));
		}
	}

	private void tipOsoNote() {
		RewriteBuilderDialog.showSimpleDialog_Tip(this, "Note: " + taskBean.osoNote);
	}

	// ==================removePallet===============================

	private ImageButton btnAddOrDel;
	private RewriteBuilderDialog dlg_comfirmDel;

	private void setAddMode(boolean isAdd) {
		btnAddOrDel.setSelected(!isAdd);
	}

	/**
	 * 添加或移除托盘
	 * 
	 * @return
	 */
	private boolean isAdd() {
		return !btnAddOrDel.isSelected();
	}

	private void showDlg_confirmDel(final PalletBean b, final String orderNo) {
		TTS.getInstance().speakAll("Confirm Remove!");

		// view
		View vContent = getLayoutInflater().inflate(R.layout.dlg_load_comfirmdel, null);
		TextView tvPallet_dlg = (TextView) vContent.findViewById(R.id.tvPallet_dlg);
		final SearchEditText etPallet_dlg = (SearchEditText) vContent.findViewById(R.id.etPallet_dlg);
		View loOrder_dlg = vContent.findViewById(R.id.loOrder_dlg);
		TextView tvOrder_dlg = (TextView) vContent.findViewById(R.id.tvOrder_dlg);

		// 初始化
		tvPallet_dlg.setText(b.palletNo);
		loOrder_dlg.setVisibility(View.VISIBLE);
		tvOrder_dlg.setText(orderNo);

		// 事件
		etPallet_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				onComfirmDel(b, value, orderNo);
				etPallet_dlg.setText("");
			}
		});

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("Scan Again To Remove");
		bd.setContentView(vContent);
		bd.setPositiveButtonOnClickDismiss(false);
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				onComfirmDel(b, etPallet_dlg.getEditableText().toString(), orderNo);
				etPallet_dlg.setText("");
			}
		});
		dlg_comfirmDel = bd.show();
	}

	private void onComfirmDel(PalletBean b, String palletNo_confirm, String orderNo) {
		if (TextUtils.isEmpty(palletNo_confirm)) {
			TTS.getInstance().speakAll_withToast("Empty");
			return;
		}
		// 不匹配
		if (!palletNo_confirm.equals(b.palletNo)) {
			TTS.getInstance().speakAll_withToast("Not Match!");
			return;
		}
		dlg_comfirmDel.dismiss();
		reqRemovePallet(b, orderNo);
	}

	private void reqRemovePallet(final PalletBean pallet, final String orderNo) {
		RequestParams p = new RequestParams();
		p.add("Method", "DeleteScanPalletNo");
		p.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		p.add("order_number", orderNo);
		p.add("pallet_no", pallet.palletNo);
		// debug
		p.add("lr_id", complexSubLoads.lr_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(mActivity, getString(R.string.sync_success));
				pallet.isScaned = false;
				pallet.setTypeId("");
				adpOrders.notifyDataSetChanged();
				refreshUI_summary();

				int remain = getTotalPallets() - getPalletsScaned();
				TTS.getInstance().speakAll("Removed" + TTS.comma() + remain + " remain");
			}
		}.setCancelable(false).doPost(HttpUrlPath.LoadPaperWorkAction, p, this);
	}

	// =================================================

	/**
	 * 长按order
	 */
	private void showDlg_onLongPress(final CheckInLoadOrderBean order) {

		flowStatus.saveState(order, null);
		flowStatus.refreshFinishState();
		// 可改proNo
		final boolean canChangeProNo = flowStatus.isLastOrder_finished;
		String[] str = null;
		if (canChangeProNo)
			str = new String[] { "Print Shipping Label", "Change Pro NO" };
		else
			str = new String[] { "Print Shipping Label" };

		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		b.setTitle(R.string.menutitle_operation);
		b.setArrowItems(str, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				switch (position) {
				case 0: // 打shippingLabel
					if (complepostid()) {
						order_toPrint_sl = order;
						showDlg_printers();
					} else {
						UIHelper.showToast(getString(R.string.bcs_key_operationnotpermitexception));
					}
					break;
				case 1: // 改ProNo
					showDlg_inputProNo(true);
					break;

				default:
					break;
				}
			}
		});
		b.show();
	}

	public boolean complepostid() {
		for (LoginPostname item : StoredData.getAllpost()) {
			if (item.postid.equals(BaseDataSetUpStaffJobKey.LeadSuperVisor + "") || item.postid.equals(BaseDataSetUpStaffJobKey.Supervisor + "")) {
				return true;
			}
		}
		return false;
	}

	// private void changeProNo_onLongPress(CheckInLoadOrderBean order){
	// debug
	// flowStatus.saveState(order, null);
	// flowStatus.refreshFinishState();
	// if (flowStatus.isLastOrder_finished)
	// showDlg_inputProNo(true);
	// else
	// UIHelper.showToast(ScanLoadActivity.this,
	// "You can set the Pro NO after the order finished!");
	// }

	// 刷新-资源
	private void refreshRes() {
		tvResType.setText(complexSubLoads.doorBean.getResTypeStr(mActivity) + ": ");
		tvDock.setText(complexSubLoads.doorBean.resources_name);

		if (!TextUtils.isEmpty(complexSubLoads.getCurSubLoad().pro_no) && !complexSubLoads.getCurSubLoad().pro_no.equalsIgnoreCase("NA")) {
			tvProNo.setText(complexSubLoads.getCurSubLoad().pro_no);
		} else {
			tvProNo.setText("");
			tvProNo.setHint("NA");
		}
	}

	private void initPhotoTab() {

		// 取view
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		VLoadBarBox load_bar = (VLoadBarBox) findViewById(R.id.vLoadBarBox);
		VSealBox sealBox = (VSealBox) findViewById(R.id.sealBox);

		// 初始化ttp
		// TabParam p =
		// ScanLoadActivity.getTab_load(ttp,complexSubLoads.entry_id);

		// debug
		TabParam p = ScanLoadActivity.getTab_load(ttp, complexSubLoads.entry_id, complexSubLoads.taskBean.dlo_detail_id);
		ttp.initNoTitleBar(this, p);
		ttp.setTTPEnabled(doorBean);

		// 初始化seal
		load_bar.init(doorBean, complexSubLoads.taskBean.number);
		sealBox.init(doorBean);
	}

	// =================================================

	private void initTab() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Load", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("Others", 1));
		tab.setUserDefineClickItems(clickItems);
	}

	public static TabParam getTab_load(TabToPhoto ttp, String entry, String detailID) {
		// 基于entry
		String loadKey = TTPKey.getTaskProcessKey(entry);
		TabParam p = TabParam.new_NoTitle(loadKey, ttp);
		p.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry, detailID);
		return p;
	}

	public static TabParam getTab_load(TabToPhoto ttp, String entry) {
		// public static TabParam getTab_load(TabToPhoto ttp, String entry,
		// String dlo, String mbol) {
		// String loadKey = TTPKey.getLoadKey(dlo, mbol, 0);
		// TabParam p = TabParam.new_NoTitle(loadKey, ttp);
		// p.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "",
		// FileWithTypeKey.OCCUPANCY_MAIN + "", entry, dlo);

		// 基于entry
		String loadKey = TTPKey.getTaskProcessKey(entry);
		TabParam p = TabParam.new_NoTitle(loadKey, ttp);
		p.setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry);
		return p;
	}

	// 调整-order顺序,pallets=0的后移
	private void sortOrders() {
		boolean orderChanged = false;
		// 倒叙扫描,若pallets=0则后移
		for (int i = listOrders.size() - 1; i >= 0; i--) {
			CheckInLoadOrderBean order = listOrders.get(i);
			if (order.isAllPalletsScanned()) {
				listOrders.add(listOrders.remove(i));
				orderChanged = true;
			}
		}
		if (orderChanged)
			adpOrders.notifyDataSetChanged();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_CANCELED)
			return;

		ttp.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case SelectDoorActivity.SELECTDOOR:
			onChangeDoorReturn(data);
			break;

		default:
			break;
		}
	}

	private void onChangeDoorReturn(Intent data) {
		Bundle p = data.getExtras();
		IntentSelectDoorBean b = (IntentSelectDoorBean) p.getSerializable("intentSelectDoorBean");
		if (b == null) {
			UIHelper.showToast(this, "Error! Change door no return!");
			return;
		}
		CheckInTaskBeanMain task = complexSubLoads.doorBean;
		task.resources_id = b.resources_id;
		task.resources_type = b.resources_type;
		task.resources_name = b.resources_type_value;
		task.setResTypeValue_byTypeID(mActivity);

		// complexSubLoads.door_name=task.resources_name;
		// tvDock.setText(task.resources_name);
		refreshRes();

		CheckInTaskDoorItemActivity.sendBC_changeDoor(this, b);
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("ready");
	}

	// 主onClick
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Utility.colseInputMethod(this, v);

		switch (v.getId()) {
		// debug
		case R.id.btnAddOrDel: // 增减切换
			// showDlg_confirmDel();
			setAddMode(!isAdd());
			UIHelper.showToast(isAdd() ? getString(R.string.tms_r_addmodel) : getString(R.string.tms_r_removemodel));
			break;

		case R.id.btnRight:
			showDlg_menu();
			break;
		// ---
		case R.id.btnPrintSL: // 打shippingLabel
			if (listOrders.size() > 0) {
				order_toPrint_sl = null;
				showDlg_printers();
			} else
				UIHelper.showToast(ScanLoadActivity.this, getString(R.string.sync_select_order));
			break;
		case R.id.tvPalletType: // 默认palletType
			showDlg_selectPalletType_x();
			break;

		case R.id.btnDisableDef:// 移除默认pallet
			RewriteBuilderDialog.showSimpleDialog(this, "Disable default pallet type?", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					setDefType(null);
				}
			});
			break;
		case R.id.btnNext:
			toNextAc(CloseType_Normal);
			break;

		// case R.id.cbPrint_layout_x: // 全选/去全选
		// boolean toSelAll = is_btnSelAll_toSelectAll();
		// selectAll_print(toSelAll);
		// adpOrders.notifyDataSetChanged();
		// set_btnSelAll_toSelectAll(!toSelAll);
		// break;

		default:
			break;
		}
	}

	// 根据-选中状态,调整"全选/全不选"
	// private void adjustPrintAll() {
	//
	// int len = listOrders.size();
	// if (len == 0)
	// return;
	// int cntSelected = 0;
	// for (int i = 0; i < len; i++) {
	// if (listOrders.get(i).isPrintSelected)
	// cntSelected++;
	// }
	//
	// // 若已全选 则显示"全不选"
	// set_btnSelAll_toSelectAll(cntSelected != len);
	// }

	// private void set_btnSelAll_toSelectAll(boolean toSelectAll) {
	// cbSelAll.setChecked(!toSelectAll);
	// // cbSelAll.setText(!toSelectAll ? "Deselect All" : "Select All");
	// }

	// private boolean is_btnSelAll_toSelectAll() {
	// return !cbSelAll.isChecked();
	// }
	//
	// // 打印-全选/全不选
	// private void selectAll_print(boolean toSelect) {
	// for (int i = 0; i < listOrders.size(); i++)
	// listOrders.get(i).isPrintSelected = toSelect;
	// }

	private void doPrint() {
		RequestParams params = new RequestParams();
		params.add("Method", "ShippingLabelPrintTask");
		params.add("company_id", complexSubLoads.company_id);
		params.add("customer_id", complexSubLoads.customer_id);
		params.add("loadnumber", complexSubLoads.getLoadNo()); // 若为order
																// 没有loadNo,因有printType
																// 故不影响
		params.add("print_server_id", complexSubLoads.defaultPrinter + "");
		params.add("master_bol_no", complexSubLoads.getCurSubLoad().master_bol_no + "");

		int printType = 2;
		if (complexSubLoads.load.isMainType_load() && isToPrintAll())
			printType = 1;
		// 为load、且全选时,打印load
		params.add("print_type", printType + "");
		params.add("orders", getOrdersToPrint());

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(ScanLoadActivity.this, getString(R.string.sync_success));
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	private CheckInLoadOrderBean order_toPrint_sl = null; // shippinglabel打印,null:打印所有

	// 是否-打印所有order
	private boolean isToPrintAll() {
		// for (int i = 0; i < listOrders.size(); i++) {
		// if (!listOrders.get(i).isPrintSelected)
		// return false;
		// }
		// return true;

		return order_toPrint_sl == null;
	}

	// 勾选了order
	private boolean hasOrderToPrint() {
		for (int i = 0; i < listOrders.size(); i++) {
			if (listOrders.get(i).isPrintSelected)
				return true;
		}
		return false;
	}

	// // 待打印的orders,","分割
	// private String getOrdersToPrint() {
	//
	// StringBuilder ret = new StringBuilder();
	// int len = listOrders.size();
	// for (int i = 0; i < len; i++) {
	// if (listOrders.get(i).isPrintSelected)
	// ret.append(listOrders.get(i).orderno + ",");
	// }
	//
	// String retStr = ret.toString();
	// // 移除-最后的","
	// if (!TextUtils.isEmpty(retStr))
	// retStr = retStr.substring(0, retStr.length() - 1);
	// return retStr;
	// }

	// 待打印的orders,","分割
	private String getOrdersToPrint() {
		// 打印所有
		if (isToPrintAll()) {
			StringBuilder ret = new StringBuilder();
			int len = listOrders.size();
			for (int i = 0; i < len; i++)
				ret.append(listOrders.get(i).orderno + ",");

			String retStr = ret.toString();
			// 移除-最后的","
			if (!TextUtils.isEmpty(retStr))
				retStr = retStr.substring(0, retStr.length() - 1);
			return retStr;
		}
		// 打印-单个
		return order_toPrint_sl.orderno;
	}

	private void doScan() {
		// debug,防止重复扫描
		// if(dlgSelectPalletType!=null&&dlgSelectPalletType.isShowing())
		// return;

		if (AppConfig.isDebug && TextUtils.isEmpty(etPallet.getEditableText().toString())) {
			etPallet.setText(getNextPallet_toScan());
		}

		String palletNo = etPallet.getEditableText().toString();
		// 空
		if (TextUtils.isEmpty(palletNo)) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet!", true);
			return;
		}

		// 添加
		if (isAdd()) {
			int rst = scanPallet(palletNo);
			if (rst == ScanRst_NotFind)
				TTS.getInstance().speakAll_withToast("Invalid Pallet!", true);

		}
		// 删除
		else {
			CheckInLoadOrderBean orderB = CheckInLoadOrderBean.getOrder_byPallet(listOrders, palletNo);
			if (orderB == null) {
				TTS.getInstance().speakAll_withToast("Pallet Not Found!", true);
			} else {
				PalletBean palletB = orderB.getPallet(palletNo);
				if (palletB.isScaned)
					showDlg_confirmDel(palletB, orderB.orderno);
				else
					TTS.getInstance().speakAll_withToast("Pallet Not Loaded!", true);
			}
		}

		etPallet.setText("");
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ttp.clearCache();
				ScanLoadActivity.super.onBackBtnOrKey();
			}
		});
	}

	// 至"确认页"
	private void toNextAc(int closeType) {

		complexSubLoads.getCurSubLoad().closeType = closeType;
		Intent in = null;
		// debug
		if (closeType == CloseType_Normal || closeType == CloseType_Exception) {
			in = new Intent(this, LoadComfirmActivity.class);
			LoadComfirmActivity.initParams(in, complexSubLoads);
			startActivity(in);
			finish();
		} else if (closeType == CloseType_Partially) {
			in = new Intent(this, CheckInTaskExceptionActivity.class);
			CheckInTaskExceptionActivity.initParams_hasDoc(in, complexSubLoads.doorBean, complexSubLoads.taskBean, closeType, complexSubLoads);
			startActivity(in);
			// debug
			finish();
		} else {
			in = new Intent(this, LoadToPrintActivity.class);
			// LoadToPrintActivity.initParams(in, complexSubLoads,
			// linearLayoutPhoto.getAddPhotoNames());
			LoadToPrintActivity.initParams(in, complexSubLoads);
			startActivity(in);
			finish();
		}
	}

	// debug
	private String getNextPallet_toScan() {
		for (int i = 0; i < listOrders.size(); i++) {
			CheckInLoadOrderBean b = listOrders.get(i);
			List<PalletBean> scaned = b.code_nos;
			if (scaned == null)
				continue;

			for (int j = 0; j < scaned.size(); j++) {

				if (scaned.get(j).isScaned)
					continue;
				// 若匹配
				else
					return scaned.get(j).palletNo;
			}
		}
		return "";
	}

	// 成功结束一个时(pallet/order),语音提示
	private void onFinishOne_Ok() {

		// 调整order顺序
		adjustOrder_afterScan();
		// 更新ui
		refreshUI_summary();

		// =====tts===========================

		if (flowStatus.isLastOrder_finished) {
			// 移除默认
			setDefType(null);

			// load finish
			if (flowStatus.isLoadFinished) {
				String str = complexSubLoads.load == null ? "" : complexSubLoads.load.getMainType_Str();
				TTS.getInstance().speakAll(str + " finish");
				return;
			}

			// order finish,xx remain
			TTS.getInstance().speakAll("order finish" + TTS.comma() + (getTotalPallets() - getPalletsScaned()) + " remain");
		}
		// xx remain
		else {
			TTS.getInstance().speakAll((getTotalPallets() - getPalletsScaned()) + " remain");

		}

		// 遮罩
		int palletNum = getPalletsScaned();
		int palletTotal = getTotalPallets();
		if (isAdd() && palletNum + 2 <= palletTotal) {
			ct.startWaitTimer();
		}
	}

	// 刷新-统计量ui,总量、扫描量、btnNext
	private void refreshUI_summary() {
		// 总量
		tvTotalOrders.setText(listOrders.size() + "");
		tvTotalPallets.setText(getTotalPallets() + "");

		// 扫描量
		int newOrderNum = getOrdersScaned();
		int newPalletNum = getPalletsScaned();
		tvOrdersScaned.setText(newOrderNum + "");
		tvPalletsScaned.setText(newPalletNum + "");

		// 下一步-按钮
		boolean loadFinish = newOrderNum == listOrders.size();
		btnNext.setVisibility(loadFinish ? View.VISIBLE : View.GONE);
		// if(loadFinish)
		// showDlg_toNext();
	}

	private RewriteBuilderDialog dlg_toNext;

	private void showDlg_toNext() {
		// 防止-重复显示
		if (dlg_toNext != null && dlg_toNext.isShowing())
			return;
		dlg_toNext = RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_loadfinish_next), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				toNextAc(CloseType_Normal);
			}
		});

	}

	private int getTotalPallets() {
		int total = 0;
		for (int i = 0; i < listOrders.size(); i++) {
			List<PalletBean> pallets = listOrders.get(i).code_nos;
			total += (pallets == null ? 0 : pallets.size());
		}
		return total;
	}

	private int getPalletsScaned() {
		int cnt = 0;
		for (int i = 0; i < listOrders.size(); i++)
			cnt += listOrders.get(i).getPalletsScaned();
		return cnt;
	}

	private int getOrdersScaned() {
		int cnt = 0;
		for (int i = 0; i < listOrders.size(); i++) {
			if (listOrders.get(i).isAllPalletsScanned())
				cnt++;
		}
		return cnt;
	}

	/**
	 * 已全部扫完
	 * 
	 * @return
	 */
	private boolean isAllScaned() {
		for (int i = 0; i < listOrders.size(); i++) {
			if (!listOrders.get(i).isAllPalletsScanned())
				return false;
		}
		return true;
	}

	// 扫描target
	// 返回值:取ScanRst_x
	private int scanPallet(String target) {
		ct.checkScanTimes();
		if (ct.isLockRunning) {
			etPallet.setText("");
			return SCANRST_TOO_FAST;
		}

		for (int i = 0; i < listOrders.size(); i++) {
			CheckInLoadOrderBean b = listOrders.get(i);
			List<PalletBean> pallets = b.code_nos;
			if (pallets == null)
				continue;

			for (int j = 0; j < pallets.size(); j++) {
				PalletBean pallet = pallets.get(j);
				// 若匹配
				if (target.equals(pallet.palletNo)) {
					// 备份-状态
					flowStatus.saveState(b, pallet);

					boolean hasScaned = pallet.isScaned;
					pallet.isScaned = true;
					// 刷新-扫描状态
					// flowStatus.refreshScanState(b, pallet);
					flowStatus.refreshFinishState();

					// 已匹配过,则仅修改palletType
					if (hasScaned) {
						showDlg_selectPalletType(pallet, true);
						return ScanRst_HasScaned;
					}

					// ---选palletType---
					showDlg_selectPalletType(pallet, false);
					return ScanRst_Find;
				}
			}
		}
		return ScanRst_NotFind;
	}

	// 扫描后-调整位置,order、pallet
	private void adjustOrder_afterScan() {
		CheckInLoadOrderBean order = flowStatus.lastScaned_order;
		PalletBean pallet = flowStatus.lastScaned_pallet;
		List<PalletBean> listPallets = order.code_nos;

		// 若order扫描完,挪至末尾
		if (flowStatus.isLastOrder_finished) {
			listOrders.remove(order);
			listOrders.add(order);
		}
		// 若未扫描完,挪至首部
		else {
			listOrders.remove(order);
			listOrders.add(0, order);
		}
		// 当前pallet-置顶
		listPallets.remove(pallet);
		listPallets.add(0, pallet);

		adpOrders.collapseAll();
		// lvOrders.expandGroup(0);
		lvOrders.setSelectedGroup(0);
		adpOrders.notifyDataSetChanged();
	}

	private List<CheckInLoadPalletTypeBean> tmpPalletTypes; // 调整后的palletTypes(order的第一个palletType抬前)

	// 默认palletType,扫每个order的第一个pallet时提示设定,清空默认、切换order后均提示设定
	private CheckInLoadPalletTypeBean default_PalletType;
	private CheckInLoadOrderBean lastScanned_order; // 上一order(正常扫描时,非修改)

	// 选择-pallet类型,若orderFinish 则之后输入proNo
	// 入参:1>justModify_palletType:仅修改,不作后续动作
	/**
	 * @param pallet
	 * @param justModify_palletType
	 */
	private void showDlg_selectPalletType(final PalletBean pallet, final boolean justModify_palletType) {
		if (complexSubLoads.listPalletTypes == null) {
			UIHelper.showToast(getString(R.string.sync_pallet_type_null));
			return;
		}

		// debug,防止显示两个
		// if(dlgSelectPalletType!=null&&dlgSelectPalletType.isShowing())
		// dlgSelectPalletType.dismiss();

		CheckInLoadOrderBean tmpOrder = flowStatus.lastScaned_order;
		tmpPalletTypes = flowStatus.lastScaned_order.adjustPalletTypes_order(complexSubLoads.listPalletTypes);

		// PalletBean tmpPallet=flowStatus.lastScaned_pallet;

		// 扫描时,若切换order 则清空默认palletType
		// if (!justModify_palletType && !tmpOrder.isEqual(lastScanned_order)) {
		// default_PalletType = null;
		// lastScanned_order = tmpOrder;
		// }

		boolean useDefault = !justModify_palletType && hasDefPalletType();
		// 使用默认,直接提交
		if (useDefault) {
			onSearch_PalletType(default_PalletType.pallettypeid, pallet, justModify_palletType, false);
			return;
		}

		TTS.getInstance().speakAll("select type");
		RewriteBuilderDialog.Builder buidler = new RewriteBuilderDialog.Builder(context);
		// 非修改,则不能取消
		if (!justModify_palletType)
			buidler.setCancelable(false);
		// View layout = (View) LayoutInflater.from(context).inflate(
		// R.layout.scan_load_activity_dialog, null);
		View layout = (View) LayoutInflater.from(context).inflate(R.layout.dlg_receive_oso_selecttype, null);
		final CheckBox cbAsDefault = (CheckBox) layout.findViewById(R.id.cbAsDefault);
		final SearchEditText etPalletType = (SearchEditText) layout.findViewById(R.id.etPallet);

		// 初始化ui
		cbAsDefault.setText("Set as default pallet type in this order");// 和其他扫货有点不同
		// cbAsDefault.setVisibility(!justModify_palletType ? View.VISIBLE
		// : View.GONE);
		cbAsDefault.setVisibility(false ? View.VISIBLE : View.GONE);
		if (pallet.hasType())
			etPalletType.setHint(pallet.get_TypeId());

		etPalletType.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				onSearch_PalletType(etPalletType.getEditableText().toString(), pallet, justModify_palletType, cbAsDefault.isChecked());
				// debug
				etPalletType.setText("");
			}
		});

		// 初始化-列表
		ListView listview = (ListView) layout.findViewById(R.id.list_view);
		CheckInLoadPalletTypeBeanAdapter adp = new CheckInLoadPalletTypeBeanAdapter(context, tmpPalletTypes);
		// 选中当前项
		if (pallet.hasType())
			adp.setCurPType(pallet.get_TypeId());
		listview.setAdapter(adp);

		// 列表高度
		LayoutParams lp = listview.getLayoutParams();
		lp.width = LayoutParams.MATCH_PARENT;
		int height = (int) context.getResources().getDimension(R.dimen.dialog_listview_item_height);
		lp.height = (complexSubLoads.listPalletTypes.size() > ReceiveOsoAc.MaxItems_PType) ? height * ReceiveOsoAc.MaxItems_PType : lp.height;// 李君浩注释
		listview.setLayoutParams(lp);
		listview.setOnItemClickListener(new OnItemClickListener() {// 子序列点击事件
			@Override
			public void onItemClick(AdapterView<?> dapter, View view, int index, long id) {
				pallet.setTypeId(tmpPalletTypes.get(index).pallettypeid);

				if (cbAsDefault.isChecked())
					default_PalletType = tmpPalletTypes.get(index);

				// 正常扫描,下行
				if (!justModify_palletType) {
					afterSelect_PalletType();
					return;
				}

				// 若为修改=============================

				dlgSelectPalletType.dismiss();
				notifyServer(TNotify_change_pallet_type, new IOnNotify() {

					@Override
					public void onNotify_ok() {
						// TODO Auto-generated method stub
						// 若成功,刷ui
						adpOrders.notifyDataSetChanged();
					}
				});
			}
		});
		buidler.setTitle("Select Type");
		buidler.isHideCancelBtn(true);
		buidler.setContentView(layout);
		dlgSelectPalletType = buidler.create();
		dlgSelectPalletType.show();

		// 选择type时暂停计时器
		ct.cancelWaitTimer();
	}

	// ==================改默认pallet===============================

	private boolean hasDefPalletType() {
		return default_PalletType != null;
	}

	// 改默认pallet
	private void showDlg_selectPalletType_x() {

		// view
		View layout = getLayoutInflater().inflate(R.layout.dlg_receive_oso_selecttype, null);
		final SearchEditText etPalletType = (SearchEditText) layout.findViewById(R.id.etPallet);
		final CheckBox cbAsDefault = (CheckBox) layout.findViewById(R.id.cbAsDefault);
		cbAsDefault.setVisibility(View.GONE);

		// tts
		// TTS.getInstance().speakAll("select type");
		// 初始化dlg
		RewriteBuilderDialog.Builder buidler = new RewriteBuilderDialog.Builder(this);
		buidler.setTitle("Select Type");
		buidler.isHideCancelBtn(true);
		buidler.setContentView(layout);
		// 初始化-列表
		ListView listview = (ListView) layout.findViewById(R.id.list_view);
		CheckInLoadPalletTypeBeanAdapter adp = new CheckInLoadPalletTypeBeanAdapter(mActivity, complexSubLoads.listPalletTypes);
		if (hasDefPalletType()) {
			adp.setCurPType(default_PalletType.pallettypeid);
			etPalletType.setHint(default_PalletType.pallettypeid);
		}
		listview.setAdapter(adp);

		dlgSelectPalletType = buidler.show();
		// 初始化
		etPalletType.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				onSearch_setDefPType(value);
				etPalletType.setText("");
			}
		});

		// 列表高度
		LayoutParams lp = listview.getLayoutParams();
		lp.width = LayoutParams.MATCH_PARENT;
		int height = (int) getResources().getDimension(R.dimen.dialog_listview_item_height);
		lp.height = (complexSubLoads.listPalletTypes.size() > ReceiveOsoAc.MaxItems_PType) ? height * ReceiveOsoAc.MaxItems_PType : lp.height;// 李君浩注释
		listview.setLayoutParams(lp);
		// ----
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				final String palletTypeId = complexSubLoads.listPalletTypes.get(position).pallettypeid;
				onSearch_setDefPType(palletTypeId);
			}
		});
	}

	private TextView tvPalletType;
	private Button btnDisableDef;

	// 默认palletType,为空时显示NA
	private void setDefType(CheckInLoadPalletTypeBean palletType) {
		default_PalletType = palletType;
		boolean isEmpty = palletType == null;
		tvPalletType.setText(!isEmpty ? palletType.pallettypeid : "NA");
		btnDisableDef.setVisibility(isEmpty ? View.INVISIBLE : View.VISIBLE);
	}

	// 设置-默认palletType
	private void onSearch_setDefPType(String value) {

		// ===========校验===============
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Empty!");
			return;
		}
		// debug
		CheckInLoadPalletTypeBean tmpType = CheckInLoadPalletTypeBean.getPalletType_byID(complexSubLoads.listPalletTypes, value);
		if (tmpType == null) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet Type!");
			return;
		}
		// ===============================

		// 设置-默认值
		setDefType(tmpType);
		dlgSelectPalletType.dismiss();
	}

	// =================================================

	// 从"文本框"搜索palletType时
	private void onSearch_PalletType(String value, PalletBean pallet, boolean justModify_palletType, boolean setAsDefault) {

		// ===========校验===============
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Empty!");
			return;
		}
		CheckInLoadPalletTypeBean tmpType = CheckInLoadPalletTypeBean.getPalletType_byID(tmpPalletTypes, value);
		if (tmpType == null) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet Type!");
			return;
		}

		// ===============================
		pallet.setTypeId(tmpType.pallettypeid);
		if (setAsDefault)
			default_PalletType = tmpType;

		// 只是修改-则不下行
		if (justModify_palletType) {
			notifyServer(TNotify_change_pallet_type, new IOnNotify() {

				@Override
				public void onNotify_ok() {
					// TODO Auto-generated method stub
					// 刷新ui
					adpOrders.notifyDataSetChanged();
				}
			});
			dlgSelectPalletType.dismiss();
		}
		// 正常扫描,下行
		else
			afterSelect_PalletType();
	}

	// 选择-palletType后,order完时 输proNo,load完时 跳转
	// 前导流程:扫描palletType、点选palletType
	private void afterSelect_PalletType() {

		// 保留order-第一个palletType
		flowStatus.lastScaned_order.setFirstPalletType_whenNone(flowStatus.lastScaned_pallet.get_TypeId());

		// order完成时,选择proNo
		if (flowStatus.isLastOrder_finished)
			showDlg_inputProNo(false);
		// order未完
		else {
			notifyServer(TNotify_load_one_pallet, new IOnNotify() {

				@Override
				public void onNotify_ok() {
					// TODO Auto-generated method stub
					// 若ok,则下行
					onFinishOne_Ok();
				}
			});
		}
		dlgSelectPalletType.dismiss();
	}

	public static final String TNotify_load_one_pallet = "load_one_pallet"; // pallet/order正常完成时
	public static final String TNotify_change_pallet_type = "change_pallet_type"; // 修改-palletType时
	public static final String TNotify_change_pro_no = "change_pro_no"; // 修改-proNo时

	// 通知服务端-数据更新
	// 情形:选type后(order未完)、设proNum后(order已完)、修改type、修改proNum
	private void notifyServer(String notifyType, final IOnNotify onNotify) {
		CheckInLoadOrderBean order = flowStatus.lastScaned_order;
		PalletBean pallet = flowStatus.lastScaned_pallet;

		RequestParams params = new RequestParams();
		params.add("Method", "LoadOnePallet");
		params.add("cmd", notifyType);
		if (pallet != null) {
			params.add("pallet_number", pallet.palletNo);
			params.add("pallet_type", pallet.get_TypeId());
		}
		if (order != null) {
			params.add("order", order.orderno);
			params.add("pro_no", order.proNo);
		}
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		// debug
		params.add("lr_id", complexSubLoads.lr_id);
		params.add("system_type", complexSubLoads.system_type + "");
		params.add("order_type", complexSubLoads.order_type + "");
		// debug
		params.add("resources_type", doorBean.resources_type + "");
		params.add("resources_id", doorBean.resources_id + "");
		// debug
		params.add("company_id", taskBean.company_id);

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// System.out.println("nimei>>notifyServer");
				if (onNotify != null)
					onNotify.onNotify_ok();

				// pallet_repeat=1,若已扫过
			}

			@Override
			public void handFail() {
				// 恢复-状态
				flowStatus.restoreState();
				if (onNotify != null)
					onNotify.onNotify_fail(0);
				// 重置次数
				ct.resetData();
			}
		}.setCancelable(false).doGet(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	// 输入-proNo,扫完order时
	// 入参:isChangeManually:true(手动修改,不执行后续"报数/跳转")
	private void showDlg_inputProNo(final boolean isChangeManually) {
		// 语音-提示
		TTS.getInstance().speakAll("Input Pro Number!");

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle("Input Pro NO");
		builder.setPositiveButtonOnClickDismiss(false);
		// LinearLayout layout = new LinearLayout(context);
		// layout.setPadding(10, 10, 10, 10);
		// 内容视图
		View layout = getLayoutInflater().inflate(R.layout.dlg_load_inputprono, null);
		final EditText etProNo = (EditText) layout.findViewById(R.id.etProNo);
		TextView tvMBolOrOrder = (TextView) layout.findViewById(R.id.tvMBolOrOrder);
		TextView tvMBol = (TextView) layout.findViewById(R.id.tvMBol);
		// debug,扫描plt时 禁止取消
		if (!isChangeManually)
			builder.setCancelable(false);

		// hint取上1值
		String tmpProNo = flowStatus.lastScaned_order.proNo;
		tmpProNo = tmpProNo == null ? "" : tmpProNo;
		etProNo.setText(tmpProNo);
		etProNo.setSelection(tmpProNo.length());

		boolean isLoad = complexSubLoads.load.isMainType_load();
		tvMBolOrOrder.setText(isLoad ? "MBOL: " : "Order: ");
		tvMBol.setText(isLoad ? complexSubLoads.getCurSubLoad().master_bol_no + "" : complexSubLoads.load.getMainNo());

		builder.setContentView(layout);
		builder.setPositiveButton(getString(R.string.sync_submit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 设置-ok
				if (onConfirm_proNo(etProNo, isChangeManually))
					dialog.dismiss();
			}
		});
		builder.setNegativeButton("No ProNO", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 清空
				etProNo.setText("");
				onConfirm_proNo(etProNo, isChangeManually);
			}
		});
		final RewriteBuilderDialog dlg = builder.create();
		dlg.show();

		// 监听-事件
		etProNo.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
					if (onConfirm_proNo(etProNo, isChangeManually))
						dlg.dismiss();
					// 防止-焦点转移
					return true;
				}
				return false;
			}
		});
	}

	// 确定"proNo"时
	// 入参:isChangeManually(true:手动修改proNo)
	// 返回:true(设置成功时)
	private boolean onConfirm_proNo(EditText etProNo, final boolean isChangeManually) {
		String newProNo = etProNo.getEditableText().toString();
		if (newProNo == null)
			newProNo = "";

		flowStatus.lastScaned_order.proNo = newProNo;
		// adpOrders.notifyDataSetChanged();

		String notifyType = isChangeManually ? TNotify_change_pro_no : TNotify_load_one_pallet;
		notifyServer(notifyType, new IOnNotify() {
			@Override
			public void onNotify_ok() {
				// TODO Auto-generated method stub
				// 正常扫描
				if (!isChangeManually)
					onFinishOne_Ok();
				// 手动修改,直接刷新
				else
					adpOrders.notifyDataSetChanged();
			}
		});

		return true;
	}

	// 点击-右上角时
	private void showDlg_menu() {
		// final Dialog dialog = new Dialog(this);

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);

		builder.setTitle(getString(R.string.sync_operation));
		builder.setCanceledOnTouchOutside(true);
		builder.hideCancelBtn();
		// debug
		builder.setCancelable(true);

		final View vDlg = LayoutInflater.from(mActivity).inflate(R.layout.item_menu_load, null);
		final View printItem = vDlg.findViewById(R.id.printItem);
		final View reloadItem = vDlg.findViewById(R.id.reloadItem);
		final View consolidateItem = vDlg.findViewById(R.id.consolidateItem);
		// debug
		// final View loDisableDefault_palletType = vDlg
		// .findViewById(R.id.loDisableDefault_palletType);
		// TextView tvDefPalletType = (TextView) vDlg
		// .findViewById(R.id.tvDefPalletType);
		View loHelp = vDlg.findViewById(R.id.loHelp);
		// View loChangeDoor = layout.findViewById(R.id.loChangeDoor);
		TextView tvHelp = (TextView) vDlg.findViewById(R.id.tvHelp);
		ImageView imgHelp = (ImageView) vDlg.findViewById(R.id.imgHelp);
		// debug
		View loTaskNote = vDlg.findViewById(R.id.loTaskNote);
		TextView tvTaskNote = (TextView) vDlg.findViewById(R.id.tvTaskNote);
		loTaskNote.setVisibility(complexSubLoads.taskBean.hasOsoNote() ? View.VISIBLE : View.GONE);
		tvTaskNote.setText(complexSubLoads.taskBean.osoNote);
		builder.setContentView(vDlg);
		final Dialog dlg = builder.create();

		// 初始化ui,debug
		// loDisableDefault_palletType
		// .setVisibility(default_PalletType != null ? View.VISIBLE
		// : View.GONE);
		// if (default_PalletType != null)
		// tvDefPalletType.setText(default_PalletType.pallettypeid);
		// loChangeDoor.setVisibility(taskBean.canChangeDoor() ? View.VISIBLE
		// : View.GONE);

		String msg = !isRequestHelp ? "Requst Help" : "Finish Requesting Help";
		tvHelp.setText(msg);
		imgHelp.setSelected(isRequestHelp);

		OnClickListener mItemOnClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				// 求助
				case R.id.loHelp:
					showDlg_help();
					break;
				// case R.id.loChangeDoor:
				// reqDoors();
				// break;
				// normalClose
				case R.id.loClose:
					if (isAllScaned()) {
						toNextAc(CloseType_Normal);
						return;
					}
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Please finish all pallets!", null);
					break;
				// exception
				case R.id.loExp: // exceptionClose
					CheckInLoadOrderBean par = CheckInLoadOrderBean.getOrder_PartiallyScaned(listOrders);
					// 没有部分扫描
					if (par == null)
						toNextAc(CloseType_Exception);
					else {
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Order " + par.orderno + " is partially loaded!");
					}
					// debug
					// toNextAc(CloseType_Exception);
					break;
				// debug
				// case R.id.loPartially:
				// // showDlg_expOrPartially(false);
				// RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
				// "Please cut orders,then refresh orders!");
				// break;
				// print
				case R.id.printItem:
					if (hasOrderToPrint())
						showDlg_printers();
					else
						UIHelper.showToast(ScanLoadActivity.this, "Please Select Orders To Print!");
					break;

				// reload
				case R.id.reloadItem:
					showDlg_toReload();
					break;

				// consolidate
				case R.id.consolidateItem:
					showDlg_toConsolidate();
					break;

				// case R.id.loDisableDefault_palletType:
				// default_PalletType = null;
				// lastScanned_order = null;
				// break;
				case R.id.loTaskNote:
					tipOsoNote();
					break;

				default:
					break;
				}
				// debug
				dlg.dismiss();
			}
		};

		// 监听事件
		printItem.setOnClickListener(mItemOnClick);
		reloadItem.setOnClickListener(mItemOnClick);
		consolidateItem.setOnClickListener(mItemOnClick);
		// loDisableDefault_palletType.setOnClickListener(mItemOnClick);
		loHelp.setOnClickListener(mItemOnClick);
		vDlg.findViewById(R.id.loClose).setOnClickListener(mItemOnClick);
		vDlg.findViewById(R.id.loExp).setOnClickListener(mItemOnClick);
		vDlg.findViewById(R.id.loPartially).setOnClickListener(mItemOnClick);
		// loChangeDoor.setOnClickListener(mItemOnClick);
		loTaskNote.setOnClickListener(mItemOnClick);

		dlg.show();
	}

	private void jumpToSelectDoor(List<ResourceInfo> listRes) {
		CheckInTaskBeanMain task = complexSubLoads.doorBean;
		Intent intent = new Intent(mActivity, SelectDoorActivity.class);
		IntentSelectDoorBean bean = new IntentSelectDoorBean();
		bean.resources_id = task.resources_id;
		bean.resources_type = task.resources_type;
		bean.resources_type_value = task.resources_name;

		bean.equipment_id = task.equipment_id;
		bean.equipment_number = task.equipment_number;
		bean.equipment_type = task.equipment_type;
		bean.equipment_type_value = task.equipment_type_value;
		bean.check_in_entry_id = task.entry_id;

		bean.intentType = SelectDoorActivity.CheckInTaskDoorItemActivityFlag;

		intent.putExtra("intentSelectDoorBean", (Serializable) bean);
		intent.putExtra("resourceList", (Serializable) listRes);
		startActivityForResult(intent, SelectDoorActivity.SELECTDOOR);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	public void reqDoors() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		// params.add("request_type", "1");
		params.add("request_type", "2"); // 1:可用res 2:所有
		params.add("occupy_type", OccupyTypeKey.DOOR + "");
		params.add("entry_id", complexSubLoads.doorBean.entry_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<ResourceInfo> listRes = DockHelpUtil.handjson_big(json);
				if (listRes == null || listRes.size() == 0) {
					UIHelper.showToast(mActivity, getString(R.string.sync_no_door));
					return;
				}
				jumpToSelectDoor(listRes);
			}
		}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, context);
	}

	// private void showDlg_expOrPartially(final boolean isException) {
	// RewriteBuilderDialog.showSimpleDialog(this, isException ?
	// "Exception Close?"
	// : "Partially?", new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// toNextAc(isException ? CloseType_Exception
	// : CloseType_Partially);
	// }
	// });
	// }

	private boolean isRequestHelp = false; // 正在请求帮助

	private void showDlg_help() {
		final boolean toRequest = !isRequestHelp;

		String msg = toRequest ? "Request?" : "Finish Requesting?";
		RewriteBuilderDialog.showSimpleDialog(this, msg, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				RequestParams params = new RequestParams();
				params.add("Method", "NeedHelp");
				params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
				params.add("flag_help", toRequest ? "1" : "0");

				new SimpleJSONUtil() {

					@Override
					public void handReponseJson(JSONObject json) {
						// TODO Auto-generated method stub
						isRequestHelp = toRequest;
						UIHelper.showToast(mActivity, getString(R.string.sync_success));
					}

					@Override
					public void handFail() {
					}
				}.doGet(HttpUrlPath.LoadPaperWorkAction, params, mActivity);
			}
		});
	}

	// 合并-原因
	private String[] reasons_consolidate = new String[] { "Low cube trailer", "Trailer damaged", "Pre-load freight inside", "Wrong configuration" };

	// 输入consoliate.from/to
	private void showDlg_toConsolidate() {

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle("Consolidate");
		builder.setPositiveButtonOnClickDismiss(false);
		// 内容视图
		View loDlg = getLayoutInflater().inflate(R.layout.dlg_checkinload_consolidate, null);
		final EditText etPalletFrom = (EditText) loDlg.findViewById(R.id.etPalletFrom);
		final EditText etPalletTo = (EditText) loDlg.findViewById(R.id.etPalletTo);
		final TextView tvReason = (TextView) loDlg.findViewById(R.id.tvReason);
		final SingleSelectBar tabReason = (SingleSelectBar) loDlg.findViewById(R.id.tabReason);
		builder.setContentView(loDlg);

		// 初始化-tabReason
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("1", 1));
		clickItems.add(new HoldDoubleValue<String, Integer>("2", 2));
		clickItems.add(new HoldDoubleValue<String, Integer>("3", 3));
		clickItems.add(new HoldDoubleValue<String, Integer>("4", 4));
		tabReason.setUserDefineClickItems(clickItems);
		tabReason.userDefineSelectIndex(0);
		tvReason.setText(reasons_consolidate[0]);
		tabReason.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				// getcheckinbarInfo(selectValue.b) ;
				tvReason.setText(reasons_consolidate[selectValue.b - 1]);
			}
		});

		builder.setPositiveButton("Consolidate", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String palletFrom = etPalletFrom.getEditableText().toString();
				String palletTo = etPalletTo.getEditableText().toString();

				// 空、存在-校验
				CheckInLoadOrderBean orderFrom = checkEt_consolidate(etPalletFrom, true);
				if (orderFrom == null)
					return;
				CheckInLoadOrderBean orderTo = checkEt_consolidate(etPalletTo, false);
				if (orderTo == null) {
					return;
				}
				// 不同-校验
				if (!TextUtils.isEmpty(palletTo) && palletTo.equals(palletFrom)) {
					TTS.getInstance().speakAll_withToast("Same Pallet!");
					return;
				}
				// 若不是一个order
				if (!orderFrom.orderno.equals(orderTo.orderno)) {
					TTS.getInstance().speakAll_withToast("The two should in the same order!");
					return;
				}
				// 合并
				doConsolidate(orderFrom, palletFrom, palletTo, tabReason.getCurrentSelectItem().a);
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		final RewriteBuilderDialog dlg = builder.create();
		dlg.show();

		// -------监听回车-------------------

		etPalletFrom.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {

					CheckInLoadOrderBean order = checkEt_consolidate(etPalletFrom, true);
					// 若无效,则防止焦点下移
					if (order == null)
						return true;
				}
				return false;
			}
		});
		etPalletTo.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {

					CheckInLoadOrderBean orderTo = checkEt_consolidate(etPalletTo, false);
					// 若无效,防止焦点下移
					if (orderTo == null)
						return true;

					// ------palletFrom再次校验-------------------------
					String palletTo = etPalletTo.getEditableText().toString();
					String palletFrom = etPalletFrom.getEditableText().toString();
					// 若相同
					if (!TextUtils.isEmpty(palletTo) && palletTo.equals(palletFrom)) {
						TTS.getInstance().speakAll_withToast("Same Pallet!");
						return true;
					}

					CheckInLoadOrderBean orderFrom = checkEt_consolidate(etPalletFrom, true);
					// 焦点-转移
					if (orderFrom == null) {
						etPalletFrom.requestFocus();
						etPalletFrom.setText("");
						return true;
					}

					// -------------------------------------------

					// 不是一个order
					if (!orderFrom.orderno.equals(orderTo.orderno)) {
						TTS.getInstance().speakAll_withToast("The two should in the same order!");
						return true;
					}

					// 合并
					// doConsolidate(orderFrom, palletFrom, palletTo,
					// tabReason.getCurrentSelectItem().a);
					// dlg.dismiss();
					// debug
					TTS.getInstance().speakAll_withToast("Select Reason!");

					// 防止-转移焦点
					return true;
				}
				return false;
			}
		});
	}

	// 校验-et,语音提示、无效时清空
	// 返回:pallet所在order
	private CheckInLoadOrderBean checkEt_consolidate(EditText etPallet, boolean isPalletFrom) {
		String strPallet = isPalletFrom ? "Pallet From" : "Pallet To";
		String palletNo = etPallet.getEditableText().toString();
		// 空-校验
		if (TextUtils.isEmpty(palletNo)) {
			TTS.getInstance().speakAll_withToast(strPallet + " is Empty!");
			return null;
		}
		CheckInLoadOrderBean order = CheckInLoadOrderBean.getOrder_byPallet(listOrders, palletNo);
		// 有效性-校验
		if (order == null) {
			TTS.getInstance().speakAll_withToast(strPallet + " doesn't find!");
			etPallet.setText("");
			return null;
		}

		// palletFrom必须未扫描
		if (isPalletFrom && order.getPallet(palletNo).isScaned) {
			TTS.getInstance().speakAll_withToast(strPallet + " has bean loaded!");
			etPallet.setText("");
			return null;
		}

		return order;
	}

	// 提示-重装
	private void showDlg_toReload() {

		// Spannable str2 = Utility.getCompoundText("Keep Loaded Pallets\n",
		// "(For Cut Order)", new RelativeSizeSpan(0.7f));

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		// "Keep Loaded Pallets"
		builder.setArrowItems(new String[] { "Reload All Pallets", "Refresh Orders" }, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 重装所有
				if (position == 0)
					doRequest_mbol(true);
				// 保留已装载部分
				else if (position == 1)
					doRequest_mbol(false);

				// 重新计时
				ct.resetData();
			}
		});
		builder.show();
	}

	// 重刷mbol,reload处理
	private void doRequest_mbol(final boolean reloadAll) {
		CheckInLoad_SubLoadBean b = complexSubLoads.getCurSubLoad();
		// CheckInLoadLoadBean loadBean = complexSubLoads.load;

		RequestParams params = new RequestParams();
		params.add("Method", "RefreshOrderInfos");
		params.add("master_bol", b.master_bol_no + "");
		params.add("company_id", complexSubLoads.company_id);
		params.add("customer_id", complexSubLoads.customer_id);
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());
		params.add("isClearData", (reloadAll ? 1 : 0) + "");
		// debug
		params.add("lr_id", complexSubLoads.lr_id);
		// StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray jaOrders = null;
				try {
					jaOrders = json.optJSONObject("datas").optJSONArray("load_info").optJSONObject(0).optJSONArray("order_info");
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				// 刷新-orders
				if (jaOrders != null) {
					listOrders.clear();
					CheckInLoadOrderBean.parseBeans(jaOrders, listOrders);
					adpOrders.notifyDataSetChanged();

					// 刷新ui、跳转
					afterChangePalletCnt();

					lastScanned_order = null;
					// default_PalletType = null;
					// debug
					setDefType(null);
				}
			}
		}.setCancelable(false).doGet(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	// 获取扫到一半的order
	// 返回:扫到一半的索引,-1表未找到
	public int getOrder_partialLoaded() {
		for (int i = 0; i < listOrders.size(); i++) {
			if (listOrders.get(i).isPartiallyScaned())
				return i;
		}
		return -1;
	}

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getDefaultPrint_index() {// ???

		List<PrintServer> listPrinters = complexSubLoads.listPrinters;
		if (listPrinters == null)
			return -1;

		for (int i = 0; i < listPrinters.size(); i++) {
			String tmpId = listPrinters.get(i).getPrinter_server_id();
			if ((complexSubLoads.defaultPrinter + "").equals(tmpId))
				return i;
		}
		return -1;
	}

	private void showDlg_printers() {

		List<PrintServer> listPrinters = complexSubLoads.listPrinters;
		if (listPrinters == null || listPrinters.size() == 0) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		List<PrintServer> printServerList = complexSubLoads.listPrinters;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver, null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(context, printServerList, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServerList.size() < 3) ? dm.heightPixels / 7 * printServerList.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServerList.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getDefaultPrint_index());
		// 初始-滑至可见
		listView.setSelection(getDefaultPrint_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (getDefaultPrint_index() != -1) {
					doPrint();
					dialog.dismiss();
				} else
					UIHelper.showToast(getString(R.string.sync_select_printserver));
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;

	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			String id = complexSubLoads.listPrinters.get(position).getPrinter_server_id();
			complexSubLoads.defaultPrinter = Integer.parseInt(id);
			adpPrinters.setCurItem(position);
		};
	};

	// 合并
	private void doConsolidate(final CheckInLoadOrderBean b, final String palletFrom, final String palletTo, String reason) {
		if (b.code_nos == null)
			return;
		int oldCnt = b.code_nos.size();

		RequestParams params = new RequestParams();
		params.add("Method", "LoadConsolidatePallets");
		params.add("master_bol", complexSubLoads.getCurSubLoad().master_bol_no + "");

		params.add("order_no", b.orderno);
		params.add("customer_id", complexSubLoads.customer_id);
		params.add("company_id", complexSubLoads.company_id);
		params.add("original_pallets", oldCnt + "");
		params.add("current_pallets", (oldCnt - 1) + "");
		params.add("dlo_detail_id", complexSubLoads.getDlo_detail_id());

		params.add("from_pallet", palletFrom);
		params.add("to_pallet", palletTo);
		params.add("reason", reason);
		// debug
		params.add("lr_id", complexSubLoads.lr_id);
		params.add("order_type", complexSubLoads.order_type + "");
		params.add("system_type", complexSubLoads.system_type + "");
		// debug
		params.add("resources_type", doorBean.resources_type + "");
		params.add("resources_id", doorBean.resources_id + "");

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				b.removePallet(palletFrom);
				adpOrders.notifyDataSetChanged();
				// 刷新ui,判断跳转
				afterChangePalletCnt();

				TTS.getInstance().speakAll_withToast("Success!");
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.LoadPaperWorkAction, params, this);
	}

	// 刷新"顶部ui" 若已全扫描则直接跳转
	// 前导流程:consolidate、reload
	private void afterChangePalletCnt() {
		// 刷新-ui
		refreshUI_summary();

		// 若有未扫的,则调整顺序
		if (getOrdersScaned() != listOrders.size())
			sortOrders();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (ct != null)
			ct.onDestory();
	}

	// ================================

	private static CheckInLoad_ComplexSubLoads complexSubLoads_bridge;

	public static void initParams(Intent in, CheckInLoad_ComplexSubLoads complexSubLoads) {
		complexSubLoads_bridge = complexSubLoads;

	}

	private void applyParams() {
		if (complexSubLoads_bridge == null)
			return;

		listOrders = complexSubLoads_bridge.getCurSubLoad().listOrders;

		complexSubLoads = complexSubLoads_bridge;
		complexSubLoads_bridge = null;

	}

	// ===========nested========================================

	private AdpOrders adpOrders = new AdpOrders();

	private class AdpOrders extends BaseExpandableListAdapter {
		private AdpOrders oThis = this;

		public void collapseAll() {
			for (int i = 0; i < getGroupCount(); i++)
				lvOrders.collapseGroup(i);
		}

		// ====================

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder_Grp holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_scanload_order, null);
				holder = new Holder_Grp();
				holder.item = convertView.findViewById(R.id.item);
				holder.tvOrder = (TextView) convertView.findViewById(R.id.tvOrder);
				holder.tvPlatesScaned = (TextView) convertView.findViewById(R.id.tvPlatesScaned);
				// holder.cbPrint = (CheckBox) convertView
				// .findViewById(R.id.cbPrint);
				// holder.cbPrint_layout = (View) convertView
				// .findViewById(R.id.cbPrint_layout);
				holder.tvStaging = (TextView) convertView.findViewById(R.id.tvStaging);
				holder.loProNo = (LinearLayout) convertView.findViewById(R.id.loProNo);
				holder.tvProNo = (TextView) convertView.findViewById(R.id.tvProNo);
				holder.tvNote = (TextView) convertView.findViewById(R.id.tvNote);
				holder.tvCaseCnt = (TextView) convertView.findViewById(R.id.tvCaseCnt);
				holder.loadingSequence = (LinearLayout) convertView.findViewById(R.id.loading_seq_ly);
				holder.tvLoadingSequence = (TextView) convertView.findViewById(R.id.tvLoadingSeq);
				holder.poType = (LinearLayout) convertView.findViewById(R.id.loading_seq_po_ly);
				holder.tvPoType = (TextView) convertView.findViewById(R.id.tvLoadingSeqPo);
				holder.loadingSeqAllLy = (LinearLayout) convertView.findViewById(R.id.loading_seq_all_ly);
				convertView.setTag(holder);
			} else
				holder = (Holder_Grp) convertView.getTag();

			// 刷新-数据
			final CheckInLoadOrderBean b = listOrders.get(groupPosition);
			holder.tvOrder.setText(b.orderno);
			holder.tvPlatesScaned.setText(getSpPallet(b));

			// holder.tvTotalPlates.setText(b.getPalletCnt() + "");
			holder.tvStaging.setText(b.staging_area_id);
			holder.tvCaseCnt.setText(b.case_total);
			holder.tvProNo.setText(b.proNo);
			holder.loProNo.setVisibility(TextUtils.isEmpty(b.proNo) ? View.GONE : View.VISIBLE);

			holder.tvPoType.setText(b.externalID);
			if (complexSubLoads.isSequenceLoading()) {
				if (!TextUtils.isEmpty(b.loadingSequence)) {
					holder.loadingSequence.setVisibility(View.VISIBLE);
					holder.tvLoadingSequence.setText(b.loadingSequence);
					holder.tvLoadingSequence.setTextColor(b.loadingSeqColor);
				} else {
					if (holder.loadingSequence.getVisibility() != View.GONE)
						holder.loadingSequence.setVisibility(View.GONE);
				}
			}

			if (TextUtils.isEmpty(b.externalID))
				holder.loadingSeqAllLy.setVisibility(View.GONE);

			// boolean toShowNote = lvOrders.isGroupExpanded(groupPosition)
			// && !TextUtils.isEmpty(b.note);
			boolean toShowNote = !TextUtils.isEmpty(b.note);
			Spannable note = Utility.getCompoundText("Note: ", b.note, new ForegroundColorSpan(getResources().getColor(R.color.checkin_blue)));
			holder.tvNote.setText(note);
			holder.tvNote.setVisibility(toShowNote ? View.VISIBLE : View.GONE);
			// 防止-误触发
			// holder.cbPrint.setOnCheckedChangeListener(null);
			// holder.cbPrint.setChecked(b.isPrintSelected);
			// 监听事件
			// holder.cbPrint_layout.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View arg0) {
			// b.isPrintSelected = !b.isPrintSelected;
			// adjustPrintAll();
			// oThis.notifyDataSetChanged();
			// }
			// });

			// 背景
			// holder.item
			// .setBackgroundResource(b.isAllPalletsScanned() ?
			// R.drawable.list_center_selector_gray
			// : R.drawable.list_center_selector);

			// 记录pos
			convertView.setTag(R.id.expLv_grpPos_tag, groupPosition);

			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || b.getPalletCnt() == 0;
			holder.item.setBackgroundResource(showOne ? R.drawable.sh_load_lv_one : R.drawable.sh_load_lv_top);
			// holder.item.setPadding(10, 8, 10, 8);

			// debug
			holder.item.setSelected(b.isAllPalletsScanned());

			return convertView;
		}

		private Spannable getSpPallet(CheckInLoadOrderBean b) {
			if (b.getPalletCnt() == 0)
				return new SpannableString("Consolidated");

			String scaned = b.getPalletsScaned() + "";
			String total = b.getPalletCnt() + "";
			Spannable spPallet = new SpannableString(scaned + " / " + total);
			// 红色/蓝色
			Utility.addSpan(spPallet, "", scaned, new ForegroundColorSpan(0xffff0000));
			Utility.addSpan(spPallet, scaned + " / ", total, new ForegroundColorSpan(0xff0066ca));
			return spPallet;
		}

		@Override
		public long getGroupId(int groupPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			if (listOrders == null)
				return 0;
			return listOrders.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			// TODO Auto-generated method stub
			List<PalletBean> pallets = listOrders.get(groupPosition).code_nos;
			if (pallets == null)
				return 0;
			return pallets.size();
		}

		@Override
		public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			Holder_Child holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_scanload_pallet, null);
				holder = new Holder_Child();
				holder.item = (LinearLayout) convertView.findViewById(R.id.item);
				holder.tvPallet = (TextView) convertView.findViewById(R.id.tvPallet);
				holder.btnPalletType = (Button) convertView.findViewById(R.id.btnPalletType);
				// holder.btnRemove = (Button) convertView
				// .findViewById(R.id.btnRemove);

				convertView.setTag(holder);
			} else
				holder = (Holder_Child) convertView.getTag();

			// 刷新-视图
			final CheckInLoadOrderBean order = listOrders.get(groupPosition);
			final PalletBean b = order.code_nos.get(childPosition);
			holder.tvPallet.setText(b.palletNo);
			if (!b.hasType()) {
				holder.btnPalletType.setVisibility(View.INVISIBLE);
				// holder.btnRemove.setVisibility(View.INVISIBLE);
			} else {
				holder.btnPalletType.setVisibility(View.VISIBLE);
				holder.btnPalletType.setText(b.get_TypeId());

				// holder.btnRemove.setVisibility(View.VISIBLE);
			}

			// 监听事件
			holder.btnPalletType.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (b.isByOthers(complexSubLoads.getDlo_detail_id())) {
						UIHelper.showToast(mActivity, getString(R.string.sync_pallet_loadbytask));
						return;
					}

					flowStatus.saveState(listOrders.get(groupPosition), b);
					// 修改-palletType
					showDlg_selectPalletType(b, true);
				}
			});
			// holder.btnRemove.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// showDlg_removePallet(b, order.orderno);
			// }
			// });

			// 背景
			boolean isBottom = childPosition == order.getPalletCnt() - 1;
			int bg;
			if (isBottom)
				bg = b.isScaned ? R.drawable.sh_load_lv_bot_sel : R.drawable.sh_load_lv_bot_def;
			else
				bg = b.isScaned ? R.drawable.sh_load_lv_mid_sel : R.drawable.sh_load_lv_mid_def;
			holder.item.setBackgroundResource(bg);
			return convertView;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		// ==================自定义===============================

		// public final long Grp_BaseID=1000000; //用于长按时 区分grp、child

		public int getGrpPos(View convertView) {
			Object pos = convertView.getTag(R.id.expLv_grpPos_tag);
			return pos == null ? -1 : (Integer) pos;
		}

		public boolean isGrp(View convertView) {
			return getGrpPos(convertView) >= 0;
		}
	};

	class Holder_Grp {
		View item;
		// tvTotalPlates
		TextView tvOrder, tvPlatesScaned, tvStaging, tvNote;
		// ImageView imgPrint;
		// CheckBox cbPrint;
		// View cbPrint_layout;

		LinearLayout loProNo;
		TextView tvProNo;

		TextView tvCaseCnt;

		LinearLayout loadingSeqAllLy;
		LinearLayout loadingSequence;
		TextView tvLoadingSequence;
		LinearLayout poType;
		TextView tvPoType;
	}

	class Holder_Child {
		LinearLayout item;
		TextView tvPallet;
		Button btnPalletType;
		// btnRemove
	}

	// =================流程-状态口令=============================

	// 一定为单流程(不存在并行流程),故直接用"单例"
	private FlowStatus flowStatus = new FlowStatus();

	private class FlowStatus {
		// 扫描状态,防止重复运算 以提交效率
		// 注:1>扫描时刷新
		CheckInLoadOrderBean lastScaned_order; // 上1完成的order,若未完成 则
		PalletBean lastScaned_pallet;
		boolean isLastOrder_finished;
		boolean isLoadFinished;

		private boolean saved_isScaned_pallet; // 备份,可用于"通知服务器"失败时恢复
		// private CheckInLoadPalletTypeBean saved_palletType;
		private String saved_palletTypeId;
		private String saved_proNo;

		// 刷新-order/load.finish,需预先调saveState
		void refreshFinishState() {
			isLastOrder_finished = lastScaned_order.isAllPalletsScanned();
			isLoadFinished = getOrdersScaned() == listOrders.size();
		}

		// 保存状态,修改前设置(扫描/修改palletType/修改proNo)
		void saveState(CheckInLoadOrderBean order_scaned, PalletBean pallet_scaned) {
			lastScaned_order = order_scaned;
			lastScaned_pallet = pallet_scaned;

			if (lastScaned_pallet != null) {
				saved_isScaned_pallet = pallet_scaned.isScaned;
				saved_palletTypeId = pallet_scaned.get_TypeId();
			}
			if (lastScaned_order != null)
				saved_proNo = order_scaned.proNo;
		}

		// 恢复状态,pallet(scaned/type)、order(pro)
		void restoreState() {
			if (lastScaned_pallet != null) {
				lastScaned_pallet.isScaned = saved_isScaned_pallet;
				lastScaned_pallet.setTypeId(saved_palletTypeId);
			}
			if (lastScaned_order != null)
				lastScaned_order.proNo = saved_proNo;

			adpOrders.notifyDataSetChanged();
			TTS.getInstance().speakAll("fail");
		}
	}

	// 异步操作之后
	abstract class IOnNotify {
		// 成功时
		public abstract void onNotify_ok();

		// 失败是,failCode:错误码
		public void onNotify_fail(int failCode) {
		};
	}

}
