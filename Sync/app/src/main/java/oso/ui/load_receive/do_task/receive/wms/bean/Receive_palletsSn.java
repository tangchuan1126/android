package oso.ui.load_receive.do_task.receive.wms.bean;

import java.io.Serializable;
import java.util.List;

import utility.Utility;
import android.text.TextUtils;

/**
 * sn-bean
 * @author 朱成
 * @date 2015-3-18
 */
public class Receive_palletsSn implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1739837025713303869L;
	
//	public String at_lp_id;
//	public String pc_id;
	public String serial_number;
	public String serial_product_id;
//	public String supplier_id;
 
	public int is_repeat;
	public int is_damage;               //扫sn时,"好的plt"上的sn可临时标记为"坏的" 然后再挪到"坏托盘上"
	
	public boolean is_damage(){
		return is_damage==1;
	}
	
	public void setIs_damage(boolean value){
		is_damage=(value?1:0);
	}
	
	public boolean is_repeat(){
		return is_repeat==1;
	}
	
	public boolean isEqual(Receive_palletsSn sn){
		if(sn==null||sn.serial_number==null)
			return false;
		return sn.serial_number.equals(serial_number);
	}
	
	public boolean isEqual(String snNO){
		if(TextUtils.isEmpty(snNO))
			return false;
		return snNO.equals(serial_number);
	}
	
	public static boolean isServer(Receive_palletsSn bean) {
		return !TextUtils.isEmpty(bean.serial_product_id);
	}
	
	public boolean isServer() {
		return !TextUtils.isEmpty(serial_product_id);
	}
	
	//==================static===============================

	/**
	 * 移除重复标记,仅curPlt含两个snNO 且其他plt无snNO时 清空这两个sn的repeat标记
	 * @param listPlts
	 * @param curPlt
	 * @param snNO
	 */
	public static void removeRepeatMark(List<Rec_PalletBean> listPlts,Rec_PalletBean curPlt,String snNO){

		List<Receive_palletsSn> sns_curPlt=curPlt.getSNs(snNO);
		if(Utility.isEmpty(sns_curPlt))
			return;
		if(sns_curPlt.size()!=2)
			return;
		
		for (int i = 0; i < listPlts.size(); i++) {
			Rec_PalletBean tmpPlt=listPlts.get(i);
			//若其他plt有该sn,也不用清标志
			if(!tmpPlt.isEqual(curPlt)&&tmpPlt.hasSn(snNO))
				return;
		}
		
		//移除-重复标志
		for (int i = 0; i < sns_curPlt.size(); i++)
			sns_curPlt.get(i).is_repeat=0;
		
	}
	
	public static boolean isequalsLength(List<Receive_palletsSn> lists){
		if(lists == null || lists.size() ==0)
			return false;
		if(lists.size() == 1)
			return true;
		int length = lists.get(0).serial_number.length();
		for(Receive_palletsSn list : lists){
			if(list.serial_number.length() != length)
				return false;
		}
		return true;
	}
	
	
	
	
	
}
