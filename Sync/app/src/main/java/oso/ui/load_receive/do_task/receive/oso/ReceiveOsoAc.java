package oso.ui.load_receive.do_task.receive.oso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.SelectDoorActivity;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.load.scan.adapter.CheckInLoadPalletTypeBeanAdapter;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskExceptionActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskFinishActivity;
import oso.ui.load_receive.do_task.receive.oso.bean.ComplexReceiveOsoBean;
import oso.ui.load_receive.do_task.receive.oso.bean.ReceiveOsoBean;
import oso.ui.load_receive.do_task.receive.oso.util.DlgAddPallets;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.VpWithTab;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.loadbar.VSealBox;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.CheckInLoadPalletTypeBean;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.EntryDetailNumberTypeKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * oso版-收货
 * 
 * @author 朱成
 * @date 2014-12-26
 */
public class ReceiveOsoAc extends BaseActivity implements OnClickListener {

	private VpWithTab vp;
	private SingleSelectBar tab;
	private TabToPhoto ttp;

//	 tvEquipTitle, tvEquip, 
	private TextView tvResTitle, tvRes,tvPalletType;
	private Button btnDisableDef;

	private ListView lvPallets;
	private List<ReceiveOsoBean> listReceive;
	private TextView tvEmpty, tvPalletCnt;
	private SearchEditText etPallet;

	private ComplexReceiveOsoBean complexBean;
	private CheckInTaskBeanMain doorBean;
	private CheckInTaskItemBeanMain taskBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.receive_oso_main, 0);
		applyParams();
		setTitleString(taskBean.getTaskTypeName() + ": " + taskBean.number);
		showRightButton(R.drawable.menu_btn_style, "", this);

		// 取view
		vp = (VpWithTab) findViewById(R.id.vp);
		tab = (SingleSelectBar) findViewById(R.id.tab);
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		lvPallets = (ListView) findViewById(R.id.lvPallets);
		tvEmpty = (TextView) findViewById(R.id.tvEmpty);
		tvPalletCnt = (TextView) findViewById(R.id.tvPalletCnt);
		etPallet = (SearchEditText) findViewById(R.id.etPallet);
		// debug
		tvResTitle = (TextView) findViewById(R.id.tvResTitle);
		tvRes = (TextView) findViewById(R.id.tvRes);
//		tvEquipTitle = (TextView) findViewById(R.id.tvEquipTitle);
//		tvEquip = (TextView) findViewById(R.id.tvEquip);
		//debug
		tvPalletType = (TextView) findViewById(R.id.tvPalletType);
		btnDisableDef = (Button) findViewById(R.id.btnDisableDef);
		btnAddOrDel=(ImageButton)findViewById(R.id.btnAddOrDel);

		// 监听事件
		findViewById(R.id.imgAdd).setOnClickListener(this);
		tvPalletType.setOnClickListener(this);
		btnDisableDef.setOnClickListener(this);
		btnAddOrDel.setOnClickListener(this);

		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>("Receive", 0));
		list.add(new HoldDoubleValue<String, Integer>("Others", 1));
		tab.setUserDefineClickItems(list);
		// 初始化vp
		vp.init(tab);
		initPhotoTab();

		// 初始化lv
		lvPallets.setEmptyView(tvEmpty);
		lvPallets.setAdapter(adp);

		// 初始化et
		etPallet.setScanMode();
		etPallet.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
                //debug,防止-重复扫描,或扫描过快
                if(Utility.isFastClick())
                    return;
				doScan();
			}
		});

		refresh_Totals();

		// 初始化-基础数据
		// tvResTitle.setText(doorBean.resources_type_value+": ");
		// tvRes.setText(doorBean.resources_name);
		refreshRes();
//		tvEquipTitle.setText(doorBean.equipment_type_value + ": ");
//		tvEquip.setText(doorBean.equipment_number);
		setDefType("");
		
		//debug
//		taskBean.osoNote="Please be quick";
		if(taskBean.hasOsoNote()){
			tipOsoNote();
			findViewById(R.id.vRightHint).setVisibility(View.VISIBLE);
		}
	}
	
	private void tipOsoNote(){
		RewriteBuilderDialog.showSimpleDialog_Tip(this, "Note: "+taskBean.osoNote);
	}
	
	// ==================removePallet===============================

	private ImageButton btnAddOrDel;
	private RewriteBuilderDialog dlg_comfirmDel;

	private void setAddMode(boolean isAdd) {
		btnAddOrDel.setSelected(!isAdd);
	}

	/**
	 * 添加或移除托盘
	 * 
	 * @return
	 */
	private boolean isAdd() {
		return !btnAddOrDel.isSelected();
	}

	private void showDlg_confirmDel(final ReceiveOsoBean b) {
		TTS.getInstance().speakAll("Confirm Remove!");

		// view
		View vContent = getLayoutInflater().inflate(
				R.layout.dlg_load_comfirmdel, null);
		TextView tvPallet_dlg = (TextView) vContent
				.findViewById(R.id.tvPallet_dlg);
		final SearchEditText etPallet_dlg = (SearchEditText) vContent
				.findViewById(R.id.etPallet_dlg);

		// 初始化
		tvPallet_dlg.setText(b.palletNo);

		// 事件
		etPallet_dlg.setSeacherMethod(new SeacherMethod() {

			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				onComfirmDel(b, value);
				etPallet_dlg.setText("");
			}
		});

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("Scan Again To Remove");
		bd.setContentView(vContent);
		bd.setPositiveButtonOnClickDismiss(false);
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				onComfirmDel(b, etPallet_dlg.getEditableText().toString());
				etPallet_dlg.setText("");
			}
		});
		dlg_comfirmDel = bd.show();
	}

	private void onComfirmDel(ReceiveOsoBean b, String palletNo_confirm) {
		if (TextUtils.isEmpty(palletNo_confirm)) {
			TTS.getInstance().speakAll_withToast("Empty",true);
			return;
		}
		// 不匹配
		if (!palletNo_confirm.equals(b.palletNo)) {
			TTS.getInstance().speakAll_withToast("Not Match!",true);
			return;
		}
		dlg_comfirmDel.dismiss();
		reqRemovePallet(b);
	}

	private void reqRemovePallet(final ReceiveOsoBean b) {
		RequestParams p = new RequestParams();
//		p.add("Method", "commonLoadDelete");
//		p.add("wms_order_type_id", b.id_);
		p.add("Method", "CommonReceiveDelete");
		p.add("receive_pallet_id", b.id_);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// UIHelper.showToast(mActivity, "Success!");
				listReceive.remove(b);
				adp.notifyDataSetChanged();
				int total = refresh_Totals();
				TTS.getInstance().speakAll(
						"Removed" + TTS.comma() + "pallets " + total);
			}
		}
//		.doPost(HttpUrlPath.LoadPaperWorkAction, p, this);
		.doPost(HttpUrlPath.ReceiveWorkAction, p, this);
	}

	// =================================================

	// 刷新-资源
	private void refreshRes() {
		tvResTitle.setText(doorBean.getResTypeStr(mActivity) + ": ");
		tvRes.setText(doorBean.resources_name);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_CANCELED)
			return;

		ttp.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case SelectDoorActivity.SELECTDOOR:
			onChangeDoorReturn(data);
			break;

		default:
			break;
		}
	}

	// ==================photoTab===============================

	private void initPhotoTab() {

		// 取view
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		findViewById(R.id.loLoadbar_).setVisibility(View.GONE);
		VSealBox sealBox = (VSealBox) findViewById(R.id.sealBox);

		// 初始化ttp
//		ttp.initNoTitleBar(this, getTabParamList());
		TabParam param=ScanLoadActivity.getTab_load(ttp, doorBean.entry_id);
		ttp.initNoTitleBar(this,param);
		ttp.setTTPEnabled(doorBean);

		// 初始化seal
		sealBox.init(doorBean);
	}

	// ==========================================================

	private void onChangeDoorReturn(Intent data) {
		Bundle p = data.getExtras();
		IntentSelectDoorBean b = (IntentSelectDoorBean) p
				.getSerializable("intentSelectDoorBean");
		if (b == null) {
			UIHelper.showToast(this, "Error! Change door no return!");
			return;
		}
		CheckInTaskBeanMain task = doorBean;
		task.resources_id = b.resources_id;
		task.resources_type = b.resources_type;
		task.resources_name = b.resources_type_value;
		task.setResTypeValue_byTypeID(mActivity);

		// complexSubLoads.door_name=task.resources_name;
		// tvRes.setText(task.resources_name);
		// tvResTitle.setText(task.resources_type_value);
		refreshRes();

		CheckInTaskDoorItemActivity.sendBC_changeDoor(this, b);
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("Ready!");
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						ReceiveOsoAc.super.onBackBtnOrKey();
					}
				});
	}

	private DlgAddPallets dlgAddPallets;

	// 主onClick
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		//debug
		case R.id.btnAddOrDel: // 增减切换
			// showDlg_confirmDel();
			setAddMode(!isAdd());
			UIHelper.showToast(isAdd()?  getString(R.string.tms_r_addmodel) : getString(R.string.tms_r_removemodel));
			break;
		case R.id.btnRight: // 菜单
			showDlg_menu();
			break;
		case R.id.imgAdd: // 添加-无号pallet
			flowStatus.init(null);
			showDlg_addNoNOPallet();
			break;
		// case R.id.btnNext: // finish
		// tipFinish(ScanLoadActivity.CloseType_Normal);
		// break;

		case R.id.tvPalletType: // 默认palletType
			showDlg_selectPalletType_x();
			break;

		case R.id.btnDisableDef:// 移除默认pallet
			RewriteBuilderDialog.showSimpleDialog(this,
					"Disable default pallet type?",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							setDefType("");
						}
					});
			break;

		// ===addPallet_dlg================

		case R.id.tvType_dlg: // 选pallet
			showDlg_selectPalletType();
			break;
		case R.id.btnSubmit_dlg: // 添加/修改pallet
			ReceiveOsoBean toPallet = flowStatus.toPallet;
			toPallet.typeId = dlgAddPallets.getPalletTypeID();
			toPallet.cnt_noNoPallet = dlgAddPallets.getPalletCnt();
			
			//移除
//			if(toPallet.cnt_noNoPallet==0)
//				reqRemovePallet(flowStatus.curPallet);
//			else
			notifyServer(null);
			break;

		default:
			break;
		}
	}

	/**
	 * 添加/修改-无号pallet
	 * 
	 * @param beanToChange
	 *            无非null,则为修改
	 */
	private void showDlg_addNoNOPallet() {
		ReceiveOsoBean toPallet = flowStatus.toPallet;
		String id = "";
		int initCnt = 1;
		// 新增
		if (!flowStatus.isChange())
			id = complexBean.listPalletTypes.get(0).pallettypeid;
		// 修改
		else {
			id = toPallet.typeId;
			initCnt = toPallet.cnt_noNoPallet;
		}

		dlgAddPallets = new DlgAddPallets(this, this, id, initCnt);
		//debug
		dlgAddPallets.isChange = flowStatus.isChange();
		dlgAddPallets.show();
	}

	private void showDlg_menu() {

		// view
		View vDlg = getLayoutInflater().inflate(R.layout.dlg_receiveoso_menu,
				null);
		TextView tvDefType = (TextView) vDlg.findViewById(R.id.tvDefType);
		View loDefType = vDlg.findViewById(R.id.loDefType);
		TextView tvHelp = (TextView) vDlg.findViewById(R.id.tvHelp);
		ImageView imgHelp = (ImageView) vDlg.findViewById(R.id.imgHelp);
		//debug
		View loTaskNote=vDlg.findViewById(R.id.loTaskNote);
		TextView tvTaskNote=(TextView)vDlg.findViewById(R.id.tvTaskNote);
		// 初始化
		loDefType.setVisibility(false ? View.VISIBLE : View.GONE);
		tvDefType.setText(defPalletTypeID);
		//debug
		loTaskNote.setVisibility(taskBean.hasOsoNote()?View.VISIBLE:View.GONE);
		tvTaskNote.setText(taskBean.osoNote);

		String msg = !isRequestHelp ? "Requst Help" : "Finish Requesting Help";
		tvHelp.setText(msg);
		imgHelp.setSelected(isRequestHelp);

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));
		bd.hideCancelBtn();
		bd.setCanceledOnTouchOutside(true);

		bd.setContentView(vDlg);
		final RewriteBuilderDialog dlg = bd.show();

		// =================事件===============================

		OnClickListener onClick = new OnClickListener() {

			@Override
			public void onClick(View vv) {
				// TODO Auto-generated method stub
				switch (vv.getId()) {
				// 求助
				case R.id.loHelp:
					showDlg_help();
					break;
//				case R.id.loChangeDoor:
//					reqDoors();
//					break;
				case R.id.loDefType: // 移除默认
					defPalletTypeID = "";
					break;
				case R.id.loClose: // normalClose
//					tipFinish();
					//直接至-下一页面
					Intent in = new Intent(mActivity,
							NoListLoad_ComfirmAc.class);
					NoListLoad_ComfirmAc.initParams(in, complexBean);
					startActivity(in);
					//直接结束
					finish();
					break;
				case R.id.loExp: // 异常
					tipExp();
					break;
				case R.id.loTaskNote:	//任务注解
					tipOsoNote();
					break;

				default:
					break;
				}
				dlg.dismiss();
			}
		};

		// 监听事件
		loDefType.setOnClickListener(onClick);
		vDlg.findViewById(R.id.loExp).setOnClickListener(onClick);
		vDlg.findViewById(R.id.loClose).setOnClickListener(onClick);
		vDlg.findViewById(R.id.loHelp).setOnClickListener(onClick);
//		loChangeDoor.setOnClickListener(onClick);
		loTaskNote.setOnClickListener(onClick);
	}

	public void reqDoors() {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		// params.add("request_type", "1");
		params.add("request_type", "2"); // 1:可用res 2:所有
		params.add("occupy_type", OccupyTypeKey.DOOR + "");
		params.add("entry_id", doorBean.entry_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<ResourceInfo> listRes = DockHelpUtil.handjson_big(json);
				if (listRes == null || listRes.size() == 0) {
					UIHelper.showToast(mActivity, getString(R.string.sync_no_door));
					return;
				}
				jumpToSelectDoor(listRes);
			}
		}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, this);
	}

	private void jumpToSelectDoor(List<ResourceInfo> listRes) {
		CheckInTaskBeanMain task = doorBean;
		Intent intent = new Intent(mActivity, SelectDoorActivity.class);
		IntentSelectDoorBean bean = new IntentSelectDoorBean();
		bean.resources_id = task.resources_id;
		bean.resources_type = task.resources_type;
		bean.resources_type_value = task.resources_name;

		bean.equipment_id = task.equipment_id;
		bean.equipment_number = task.equipment_number;
		bean.equipment_type = task.equipment_type;
		bean.equipment_type_value = task.equipment_type_value;
		bean.check_in_entry_id = task.entry_id;

		bean.intentType = SelectDoorActivity.CheckInTaskDoorItemActivityFlag;

		intent.putExtra("intentSelectDoorBean", (Serializable) bean);
		intent.putExtra("resourceList", (Serializable) listRes);
		startActivityForResult(intent, SelectDoorActivity.SELECTDOOR);
	}

	private boolean isRequestHelp = false; // 正在请求帮助

	private void showDlg_help() {
		final boolean toRequest = !isRequestHelp;

		String msg = toRequest ? "Request?" : "Finish Requesting?";
		RewriteBuilderDialog.showSimpleDialog(this, msg,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						RequestParams params = new RequestParams();
						params.add("Method", "NeedHelp");
						params.add("dlo_detail_id", taskBean.dlo_detail_id);
						params.add("flag_help", toRequest ? "1" : "0");

						new SimpleJSONUtil() {

							@Override
							public void handReponseJson(JSONObject json) {
								// TODO Auto-generated method stub
								isRequestHelp = toRequest;
								UIHelper.showToast(mActivity, getString(R.string.sync_success));
							}

							@Override
							public void handFail() {
							}
						}.doGet(HttpUrlPath.LoadPaperWorkAction, params,
								mActivity);
					}
				});
	}

	private void tipExp() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exception_question),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						CheckInTaskBeanMain mainbean = complexBean.doorBean;
						CheckInTaskItemBeanMain item = complexBean.taskBean;

						Intent intent = new Intent(mActivity,
								CheckInTaskExceptionActivity.class);
						intent.putExtra("entry_id", mainbean.entry_id + "");
						intent.putExtra("detail_id", item.dlo_detail_id + "");
						intent.putExtra(
								"title",
								EntryDetailNumberTypeKey
										.getModuleName(item.number_type)
										+ ": "
										+ item.number + "");
						intent.putExtra("taskmainlist", (Serializable) mainbean);
						startActivity(intent);
						// 结束-当前页
						finish();
					}
				});
	}

	private void doScan() {
		String palletNo = etPallet.getText().toString();
		// 校验
		if (TextUtils.isEmpty(palletNo)) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet!",true);
			return;
		}
		
		// 添加,debug
		if (isAdd())
			scanPallet(palletNo);
		// 移除
		else {
			ReceiveOsoBean b = ReceiveOsoBean.getPalletByNo(listReceive,
					palletNo);
			if (b == null)
				TTS.getInstance().speakAll_withToast("Pallet Not Found!",true);
			else
				showDlg_confirmDel(b);
		}

		etPallet.setText("");
	}

	// 扫描target
	// 返回值:取ScanRst_x
	private void scanPallet(String target) {

		for (int i = 0; i < listReceive.size(); i++) {
			ReceiveOsoBean tmpPallet = listReceive.get(i);
			// 若匹配,则修改
			if (target.equals(tmpPallet.palletNo)) {
				flowStatus.init(tmpPallet);
				showDlg_selectPalletType();
				return;
			}
		}
		// 新增
		flowStatus.init(null);
		flowStatus.toPallet.palletNo = target;
		showDlg_selectPalletType();
	}

	private RewriteBuilderDialog dlgSelectPallet;
	private String defPalletTypeID; // 默认palletType

	private boolean hasDefPalletType() {
		return !TextUtils.isEmpty(defPalletTypeID);
	}

	// 入参:changeAddDlg(为添加无号pallet)
	private void showDlg_selectPalletType() {
		final ReceiveOsoBean toPallet = flowStatus.toPallet;

		// 有号pallet、新增、有默认palletType
		if (!flowStatus.isNoNoPallet() && !flowStatus.isChange()
				&& hasDefPalletType()) {
			onSearch_PalletType(defPalletTypeID, false);
			return;
		}

		// view
		View layout = getLayoutInflater().inflate(
				R.layout.dlg_receive_oso_selecttype, null);
		final SearchEditText etPalletType = (SearchEditText) layout
				.findViewById(R.id.etPallet);
		final CheckBox cbAsDefault = (CheckBox) layout
				.findViewById(R.id.cbAsDefault);

		// 有号、新增
		// boolean toShowDef = !flowStatus.isNoNoPallet()
		// && !flowStatus.isChange();
		boolean toShowDef = false;
		cbAsDefault.setVisibility(toShowDef ? View.VISIBLE : View.GONE);

		// tts
		//新增-才提示
		if(!flowStatus.isChange())
			TTS.getInstance().speakAll("select type");
		// 初始化dlg
		RewriteBuilderDialog.Builder buidler = new RewriteBuilderDialog.Builder(
				this);
		// 有号、新增时,不可取消
		if (!flowStatus.isNoNoPallet() && !flowStatus.isChange())
			buidler.setCancelable(false);
		buidler.setTitle("Select Type");
		buidler.isHideCancelBtn(true);
		buidler.setContentView(layout);
		// 初始化-列表
		ListView listview = (ListView) layout.findViewById(R.id.list_view);
		// BaseAdapter tmpAdp = new SimpleAdapter(this, getPalletTypes_toShow(),
		// R.layout.scan_load_dialog_listview_item, new String[] { "id",
		// "type" }, new int[] { R.id.pallettypeid,
		// R.id.pallettypename });
		// listview.setAdapter(tmpAdp);
		CheckInLoadPalletTypeBeanAdapter adp = new CheckInLoadPalletTypeBeanAdapter(
				mActivity, complexBean.listPalletTypes);
		if (toPallet.hasPalletType())
			adp.setCurPType(toPallet.typeId);
		listview.setAdapter(adp);

		dlgSelectPallet = buidler.show();
		// 初始化
		if (toPallet.hasPalletType())
			etPalletType.setHint(toPallet.typeId);
		etPalletType.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				onSearch_PalletType(value, cbAsDefault.isChecked());
			}
		});
		etPalletType.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (Utility.isEnterUp(event)) {
					onSearch_PalletType(etPalletType.getText().toString(),
							cbAsDefault.isChecked());
					return true;
				}
				return false;
			}
		});

		// 列表高度
		LayoutParams lp = listview.getLayoutParams();
		lp.width = LayoutParams.MATCH_PARENT;
		int height = (int) getResources().getDimension(
				R.dimen.dialog_listview_item_height);
		lp.height = (complexBean.listPalletTypes.size() > ReceiveOsoAc.MaxItems_PType) ? height
				* MaxItems_PType
				: lp.height;
		listview.setLayoutParams(lp);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final String palletTypeId = complexBean.listPalletTypes
						.get(position).pallettypeid;
				onSearch_PalletType(palletTypeId, cbAsDefault.isChecked());
			}
		});
	}

	public static int MaxItems_PType = 6;

	public static float getListHeight(int itemCnt, int itemH_px,
			int maxH_itemCnt, int minH_px) {
		return itemCnt > maxH_itemCnt ? itemH_px * maxH_itemCnt : minH_px;
	}

	// ==================改默认pallet===============================

	// 改默认pallet
	private void showDlg_selectPalletType_x() {

		// view
		View layout = getLayoutInflater().inflate(
				R.layout.dlg_receive_oso_selecttype, null);
		final SearchEditText etPalletType = (SearchEditText) layout
				.findViewById(R.id.etPallet);
		final CheckBox cbAsDefault = (CheckBox) layout
				.findViewById(R.id.cbAsDefault);
		cbAsDefault.setVisibility(View.GONE);

		// tts
		// TTS.getInstance().speakAll("select type");
		// 初始化dlg
		RewriteBuilderDialog.Builder buidler = new RewriteBuilderDialog.Builder(
				this);
		buidler.setTitle("Select Type");
		buidler.isHideCancelBtn(true);
		buidler.setContentView(layout);
		// 初始化-列表
		ListView listview = (ListView) layout.findViewById(R.id.list_view);
		CheckInLoadPalletTypeBeanAdapter adp = new CheckInLoadPalletTypeBeanAdapter(
				mActivity, complexBean.listPalletTypes);
		if (hasDefPalletType()) {
			adp.setCurPType(defPalletTypeID);
			etPalletType.setHint(defPalletTypeID);
		}
		listview.setAdapter(adp);

		dlgSelectPallet = buidler.show();
		// 初始化
		etPalletType.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				onSearch_setDefPType(value);
			}
		});
		etPalletType.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (Utility.isEnterUp(event)) {
					onSearch_setDefPType(etPalletType.getText().toString());
					return true;
				}
				return false;
			}
		});

		// 列表高度
		LayoutParams lp = listview.getLayoutParams();
		lp.width = LayoutParams.MATCH_PARENT;
		int height = (int) getResources().getDimension(
				R.dimen.dialog_listview_item_height);
		lp.height = (complexBean.listPalletTypes.size() > ReceiveOsoAc.MaxItems_PType) ? height
				* MaxItems_PType
				: lp.height;
		listview.setLayoutParams(lp);
		// ----
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final String palletTypeId = complexBean.listPalletTypes
						.get(position).pallettypeid;
				onSearch_setDefPType(palletTypeId);
			}
		});
	}

	// 默认palletType,为空时显示NA
	private void setDefType(String defType) {
		defPalletTypeID = defType;
		boolean isEmpty = TextUtils.isEmpty(defType);
		if (isEmpty)
			defType = "NA";
		tvPalletType.setText(defType);

		btnDisableDef.setVisibility(isEmpty ? View.INVISIBLE : View.VISIBLE);
	}

	// 设置-默认palletType
	private void onSearch_setDefPType(String value) {

		// ===========校验===============
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Empty!");
			return;
		}
		// debug
		CheckInLoadPalletTypeBean tmpType = CheckInLoadPalletTypeBean
				.getPalletType_byID(complexBean.listPalletTypes, value);
		if (tmpType == null) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet Type!");
			return;
		}
		// ===============================

		// 设置-默认值
		setDefType(tmpType.pallettypeid);
		dlgSelectPallet.dismiss();
	}

	// =================================================

	// 从"文本框"搜索palletType时
	// 入参:1>setAsDef:应设为"默认type"
	private void onSearch_PalletType(String value, boolean setAsDef) {
		ReceiveOsoBean toPallet = flowStatus.toPallet;

		// ===========校验===============
		if (TextUtils.isEmpty(value)) {
			TTS.getInstance().speakAll_withToast("Empty!");
			return;
		}
		// debug
		CheckInLoadPalletTypeBean tmpType = CheckInLoadPalletTypeBean
				.getPalletType_byID(complexBean.listPalletTypes, value);
		if (tmpType == null) {
			TTS.getInstance().speakAll_withToast("Invalid Pallet Type!");
			return;
		}
		// ===============================

		String palletTypeId = tmpType.pallettypeid;
		toPallet.typeId = palletTypeId;
		if (setAsDef)
			defPalletTypeID = palletTypeId;
		// 无号pallet
		if (flowStatus.isNoNoPallet()) {
			dlgAddPallets.setPalletType(palletTypeId);
			dlgSelectPallet.dismiss();
			return;
		}
		// 有号pallet
		notifyServer(null);
		dlgSelectPallet.dismiss();
	}

	// 添加pallet
	private void notifyServer(final IOnNotify onNotify) {
		CheckInTaskBeanMain doorBean = complexBean.doorBean;
		CheckInTaskItemBeanMain taskBean = complexBean.taskBean;
		ReceiveOsoBean toPallet = flowStatus.toPallet;
		int cnt = toPallet.cnt_noNoPallet;

		RequestParams p = new RequestParams();
		// p.add("Method", "CommonReceivePallet");
		p.add("system_type", complexBean.system_type + "");
		p.add("order_type", complexBean.order_type + "");
		p.add("detail_id", taskBean.dlo_detail_id);
		p.add("lr_id", taskBean.lr_id + "");
		p.add("order_level", complexBean.order_level + "");
		// debug
		p.add("resources_type", doorBean.resources_type + "");
		p.add("resources_id", doorBean.resources_id + "");

		// 新增
		if (!flowStatus.isChange()) {
			p.add("Method", "CommonReceivePallet");
			// 有号pallet
			if (!toPallet.is_noNoPallet()) {
				p.add("pallet_number", toPallet.palletNo);
				cnt = 1;
			}
			p.add("count", cnt + "");
			p.add("pallet_type", toPallet.typeId);
		}
		// 修改
		else {
			p.add("Method", "CommonReceiveModify");
			// 有号
			if (!toPallet.is_noNoPallet())
				cnt = 1;

			p.add("count", cnt + "");
			p.add("pallet_type", toPallet.typeId);
			p.add("receive_pallet_id", toPallet.id_);
		}

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// 若新增
				if (!flowStatus.isChange())
					flowStatus.toPallet.id_ = json
							.optString("receive_pallet_id");

				// 执行更改
				flowStatus.applyChange();
				if (onNotify != null)
					onNotify.onNotify_ok();
			}

			@Override
			public void handFail() {
				// 恢复-状态
				// flowStatus.restoreState();
				TTS.getInstance().speakAll("Fail");
				if (onNotify != null)
					onNotify.onNotify_fail(0);
			}
		}.setCancelable(false)
		.doPost(HttpUrlPath.ReceiveWorkAction, p, this);

	}

	// 刷新-统计信息
	private int refresh_Totals() {
		int cnt = ReceiveOsoBean.getPalletCnt(listReceive);
		tvPalletCnt.setText(cnt + "");
		return cnt;
	}

	private List<Map<String, String>> getPalletTypes_toShow() {
		List<Map<String, String>> tmpList = new ArrayList<Map<String, String>>();

		List<CheckInLoadPalletTypeBean> listPalletTypes = complexBean.listPalletTypes;
		for (int i = 0; i < listPalletTypes.size(); i++) {
			Map<String, String> map = new HashMap<String, String>();
			CheckInLoadPalletTypeBean tmpB = listPalletTypes.get(i);
			map.put("id", tmpB.pallettypeid);
			map.put("type", tmpB.pallettypename);
			tmpList.add(map);
		}
		return tmpList;
	}

//	private TabParam getTabParamList() {
//		String entry = complexBean.doorBean.entry_id;
//		String dlo = complexBean.taskBean.dlo_detail_id;
//
//		List<TabParam> params = new ArrayList<TabParam>();
//		String key0 = TTPKey.getReceiveKey_oSo(dlo, 0);
//		params.add(new TabParam("Receive", key0, new PhotoCheckable(0,
//				"Receive", ttp)));
//		params.get(0).setWebImgsParams(
//				FileWithCheckInClassKey.PhotoTaskProcessing + "",
//				FileWithTypeKey.OCCUPANCY_MAIN + "", entry, dlo);
//		return params.get(0);
//	}

	private void showDlg_removePallet(final ReceiveOsoBean pallet) {
		RewriteBuilderDialog.showSimpleDialog(this, pallet.getRemoveTip(),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						reqRemovePallet(pallet);
					}
				});
	}

	// ==================static===============================

	public static void toThis(final Activity ac,
			final CheckInTaskBeanMain doorBean,
			final CheckInTaskItemBeanMain taskBean,final boolean isSecend) {
		RequestParams params = new RequestParams();
		params.add("Method", "CommonReceiveRefresh");
		params.add("lr_id", taskBean.lr_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//debug
				if(!isSecend)
				{
					toThis(ac, doorBean, taskBean, true);
					return;
				}
				
				ComplexReceiveOsoBean b = new ComplexReceiveOsoBean();
				ComplexReceiveOsoBean.parseBean(json, b, false);
				b.taskBean = taskBean;
				b.doorBean = doorBean;

				Intent in = new Intent(ac, ReceiveOsoAc.class);
				ReceiveOsoAc.initParams(in, b);
				ac.startActivity(in);
			}
		}.doPost(HttpUrlPath.ReceiveWorkAction, params, ac);
	}

	// =========传参===============================

	public static void initParams(Intent in, ComplexReceiveOsoBean b) {
		in.putExtra("datas", (Serializable) b);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		complexBean = (ComplexReceiveOsoBean) params.getSerializable("datas");
		doorBean = complexBean.doorBean;
		taskBean = complexBean.taskBean;

		listReceive = complexBean.listPallets;
	}

	// ==================网络请求===============================

	/**
	 * normalClose
	 */
	private void tipFinish() {
		// String tip = closeType == ScanLoadActivity.CloseType_Normal ?
		// "Finish?"
		// : "Exception?";
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_close_question),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
//						reqFinish();
						Intent in=new Intent(mActivity,NoListLoad_ComfirmAc.class);
						NoListLoad_ComfirmAc.initParams(in,complexBean);
						startActivity(in);
						//直接结束
						finish();
					}
				});
	}

	// finish-请求
	private void reqFinish() {
		final CheckInTaskBeanMain mainbean = complexBean.doorBean;
		CheckInTaskItemBeanMain taskBean = complexBean.taskBean;

		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingCloseDetail");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("detail_id", taskBean.dlo_detail_id + "");
		params.add("number_status", ScanLoadActivity.CloseType_Normal + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		params.add("resources_id", mainbean.resources_id + "");
		// debug
		params.add("request_type", "notaskinfos");
		ttp.uploadZip(params, "TaskClose");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				ttp.clearData(true);
				// debug
				// if (!Utility.isNullForList(mainbean.complexDoorList)) {
				// mainbean.complexDoorList.clear();
				// }
				// CheckInTaskItemBeanMain.parseBean2(json,
				// mainbean.complexDoorList);
				// setData();

				int returnflag = json.optInt("returnflag");
				// 若为门下最后1个(1/2/4跳,3不跳),直接至finish页
				if (returnflag != CheckInTaskFinishActivity.LoadCloseNoifyNormal) {
					Intent in = new Intent(mActivity,
							CheckInTaskFinishActivity.class);
					CheckInTaskFinishActivity.initParams(in, mainbean);
					startActivity(in);
				}
				// 结束-当前页
				finish();
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listReceive.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_receiveo_oso_pallet, null);
				holder = new Holder();
				holder.item = (LinearLayout) convertView
						.findViewById(R.id.item);
				holder.tvPallet = (TextView) convertView
						.findViewById(R.id.tvPallet);
				holder.btnPalletType = (Button) convertView
						.findViewById(R.id.btnPalletType);
//				holder.btnRemove = (Button) convertView
//						.findViewById(R.id.btnRemove);

				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			final ReceiveOsoBean tmpPallet = listReceive.get(position);

			holder.btnPalletType.setText(tmpPallet.typeId);
			// 有号
			if (!tmpPallet.is_noNoPallet())
				holder.tvPallet.setText(tmpPallet.palletNo);
			// 无号
			else {
				Spannable sp = Utility.getCompoundText("Pallet x ",
						tmpPallet.cnt_noNoPallet + "", new ForegroundColorSpan(
								0xff0000ff));
				holder.tvPallet.setText(sp);
			}

			// 监听事件
			holder.btnPalletType.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// 修改palletType
					flowStatus.init(tmpPallet);
					if (!tmpPallet.is_noNoPallet())
						showDlg_selectPalletType();
					else
						showDlg_addNoNOPallet();
				}
			});
//			holder.btnRemove.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					showDlg_removePallet(tmpPallet);
//				}
//			});

			return convertView;
		}
	};

	class Holder {
		LinearLayout item;
		TextView tvPallet;
		Button btnPalletType;
//		btnRemove
	}

	// 异步操作之后
	abstract class IOnNotify {
		// 成功时
		public abstract void onNotify_ok();

		// 失败是,failCode:错误码
		public void onNotify_fail(int failCode) {
		};
	}

	// =================流程-状态口令=============================

	// 一定为单流程(不存在并行流程),故直接用"单例"
	private FlowStatus flowStatus = new FlowStatus();

	private class FlowStatus {

		boolean isChange = false;

		ReceiveOsoBean curPallet; // 若有值:为修改
		ReceiveOsoBean toPallet; // 新增、修改后的样子

		public void init(ReceiveOsoBean curPallet) {
			isChange = curPallet != null ? true : false;
			this.curPallet = curPallet;

			toPallet = new ReceiveOsoBean();
			if (isChange())
				curPallet.copyTo(toPallet);
		}

		public void applyChange() {
			// 新增
			if (!isChange())
				listReceive.add(0, toPallet);
			// 修改
			else{
				toPallet.copyTo(curPallet);
			
				//若为修改-无号,且没有 则移除,debug
				if(isNoNoPallet()&&curPallet.cnt_noNoPallet==0)
					listReceive.remove(curPallet);
			}
			adp.notifyDataSetChanged();

			int total = refresh_Totals();
			// tts
			String tts = "";
			if (!isChange())
//				tts = "Success" + TTS.comma() + "pallets "+total;
				tts ="pallets "+total;
//			else
//				tts = "Success";
			TTS.getInstance().speakAll(tts);
		}

		public boolean isChange() {
			return isChange;
		}

		// public boolean isAdd_noNo(){
		// return (!isChange)&&toPallet.is_noNoPallet();
		// }

		public boolean isNoNoPallet() {
			return toPallet.is_noNoPallet();
		}
	}

}
