package oso.ui.load_receive.do_task.receive.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.AppConfig;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.text.method.NumberKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 收货-扫描任务-扫breakfrom的pallets
 *
 * @author 朱成
 * @date 2015-3-11
 */
public class Rec_ScanTask_BreakAc extends BaseActivity implements OnClickListener {

    private ListView lvBreakpallets;

    private List<Rec_PalletBean> listTlps;          //所有tlp,plt同listBreaks中对象(直接addAll的)
    private List<Rec_PalletBean> listBreaks;        //待break的tlp
    
    private ATAdapter atAdapter;

    private TextView itemId;
    private TextView outputQTY;
    private TextView tvLotNo;
    
    private SearchEditText etBreakpallets;
    //    private Button btnSubmit;
    private TextView tvEmpty;
    private FrameLayout flEmpty;
    private Button btnBreakAll;

    private Rec_ItemBean itemBean;

    private int ccQty;                  //总break数
    private int breakQty;               //已break总数
    private View loSubmit;
    private Button btnDelAll;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_rec_scantask_breakac, 0);
        applyParams();
        initView();
        setData();
    }

    @Override
    protected void onFirstVisible() {
        super.onFirstVisible();
        TTS.getInstance().speakAll("Ready!");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                upLoadScanData(true);
                break;
            case R.id.btnDelAll:
                RewriteBuilderDialog.showSimpleDialog(mActivity,getString(R.string.sync_delete_all),new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reqDelAll();
                    }
                });
                break;
            case R.id.btnBreakAll:
            	for(Rec_PalletBean bean : listTlps)
            		doScan(bean.wms_container_id,false);
            	break;
        }
    }

    private void reqDelAll(){
        RequestParams p=new RequestParams();
        p.add("line_id", itemBean.receipt_line_id);
        new SimpleJSONUtil(){
            @Override
            public void handReponseJson(JSONObject json) {
                UIHelper.showToast(getString(R.string.sync_success));

                for (int i=0;i< listBreaks.size();i++) {
                    Rec_PalletBean tmp = listBreaks.get(i);
                    tmp.setBreakQty(0);
                }
                listBreaks.clear();
                atAdapter.notifyDataSetChanged();
                refSumUI();
            }
        }.doGet(HttpUrlPath.basePath+"_receive/android/deleteBreakPalletByLine",p,this);
    }

    private void initView() {
    	
        lvBreakpallets = (ListView) findViewById(R.id.lv_scaned_breaked_pallets);

        itemId = (TextView) findViewById(R.id.item_id);
        outputQTY = (TextView) findViewById(R.id.output_qty);
        etBreakpallets = (SearchEditText) findViewById(R.id.et_break_pallets);
//        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        tvEmpty = (TextView) findViewById(R.id.tvEmpty);
        loSubmit = findViewById(R.id.loSubmit);
        btnDelAll=(Button)findViewById(R.id.btnDelAll);
        tvLotNo= (TextView) findViewById(R.id.tvLotNo);
        
        flEmpty = (FrameLayout) findViewById(R.id.flEmpty);
        btnBreakAll = (Button) findViewById(R.id.btnBreakAll);
        btnBreakAll.setOnClickListener(this);
        
        //提交按钮
        findViewById(R.id.btnSubmit).setOnClickListener(this);
        btnDelAll.setOnClickListener(this);
    }

    private void setData() {
        //扫描框
        etBreakpallets.setScanMode();
        etBreakpallets.setSeacherMethod(new SeacherMethod() {

            @Override
            public void seacher(String value) {
                if (AppConfig.isDebug && TextUtils.isEmpty(value))
                    value = dbg_getNextPlt();

                doScan(value,true);
                etBreakpallets.setText("");
            }
        });

        //lv
        lvBreakpallets.setEmptyView(flEmpty);
        atAdapter = new ATAdapter();
        lvBreakpallets.setAdapter(atAdapter);
        
        lvBreakpallets.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showDlg_Select(listBreaks.get(position));
			}
		});
        
//        lvBreakpallets.setOnItemLongClickListener(new OnItemLongClickListener() {
//
//			@Override
//			public boolean onItemLongClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				if(is_start_scan == 1) return false;
//				showDlg_confirmQty(listBreaks.get(position));
//				return false;
//			}
//		});
        
        //若cc时 配置全改了,则可breakAll
        int totalGood = Rec_PalletBean.getTotalQty_break(listBreaks)+Rec_PalletBean.getTotalQty_Good(listTlps);
        isCCQty_eq_totalGood=ccQty ==totalGood;
//        if(ccQty == Rec_PalletBean.getTotalQty_Good(listTlps) || listBreaks == null){
//        	tvEmpty.setVisibility(View.GONE);
//        	btnBreakAll.setVisibility(View.VISIBLE);
//        }

        //初始化
        setTitleString("Receipt No: " + itemBean.receipt_no);
        itemId.setText(itemBean.item_id);
        if(TextUtils.isEmpty(itemBean.lot_no)){
        	tvLotNo.setText("");
        	tvLotNo.setHint("NA");
		}else{
			tvLotNo.setText(itemBean.lot_no);
		}
        refSumUI();
    }
    
    private boolean isCCQty_eq_totalGood=false;	//"ccQty"和"count时好的数量"相同,则支持breakAll

    private String dbg_getNextPlt() {
        for (int i = 0; i < listTlps.size(); i++) {
            Rec_PalletBean b = listTlps.get(i);
            Rec_PalletBean scaned = Rec_PalletBean.getPallet_byScanNO(listBreaks, b.wms_container_id);
            if (scaned == null && b.keep_inbound !=1)
                return b.wms_container_id;
        }
        return "";
    }

    //扫描结果处理
    //toSpeak:语音
    protected void doScan(String value,boolean toSpeak) {
    	
    	//已break完
        if (breakQty >= ccQty) {
            TTS.getInstance().speakAll_withToast("CC Quantity Done!", true);
            return;
        }
        Rec_PalletBean palletBean=Rec_PalletBean.getPallet_byScanNO(listBreaks, value);
        //若已扫
        if(palletBean!=null){
            //抬至第一个
            listBreaks.remove(palletBean);
            listBreaks.add(0,palletBean);
            atAdapter.notifyDataSetChanged();
            lvBreakpallets.setSelection(0);

            TTS.getInstance().speakAll_withToast("Breaked!", true);
            return;
        }
        
        palletBean = Rec_PalletBean.getPallet_byScanNO(listTlps, value);
        if (palletBean == null) {
            TTS.getInstance().speakAll_withToast("Invalid Pallet!", true);
            return;
        }
        if(palletBean.keep_inbound ==1){
        	TTS.getInstance().speakAll_withToast("Pallets Keep Inbound!", true);
        	return;
        }
        
        int countAll = breakQty + palletBean.normal_qty;
        //该plt的normalQty能全加上 不超出
        //待break的数量
        int piece;
        if (countAll <= ccQty) {
        	piece = palletBean.normal_qty;
        } else {
            piece = ccQty - breakQty;
        }
        
        if(piece <= 0){
        	UIHelper.showToast("Invalid number!");
        	return;
        }
        
//        //partialBreak
//        if(piece < palletBean.normal_qty){
//        	palletBean.setBreakQty(piece);
//            listBreaks.add(0,palletBean);
//            atAdapter.notifyDataSetChanged();
//            boolean isFinished=refSumUI();
//            TTS.getInstance().speakAll("Break Partial");
//            return;
//        }
        
        //正常break
        palletBean.setBreakQty(piece);
        listBreaks.add(0,palletBean);
        atAdapter.notifyDataSetChanged();
        boolean isFinished=refSumUI();
        
        String tts_str=isFinished?"Finish":(piece+" pieces!");
        if(toSpeak)
        	TTS.getInstance().speakAll(tts_str);
    }
    
    private PrintTool printTool = new PrintTool(this);
    private PrintTool.OnPrintLs printLs_pConfig = new PrintTool.OnPrintLs() {

		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintConfig(printServerID);
		}
	};
	
	private Rec_PalletBean config_toPrint; // 右键菜单-选中的bean
	
	private void reqPrintConfig(long printServerID) {

		final Rec_PalletBean tmpConfig = config_toPrint;
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs,  tmpConfig.con_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// debug
				tmpConfig.print_user_name = StoredData.getUsername();
				UIHelper.showToast(getString(R.string.sync_print_success));
				
				atAdapter.notifyDataSetChanged();
				
			}
		}.doGet(NetInterface.recPrintLP_url(), p, mActivity);
	}
    
    private void showDlg_Select(final Rec_PalletBean palletBean){
    	RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));

        String[] strs=null;
        
        if(palletBean.normal_qty == 0){
        	strs=new String[] { "Change","Delete"};
        }else{
        	strs=new String[] { "Change","Delete","Print"};
        }
        
		bd.setArrowItems(strs, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
				// TODO Auto-generated method stub
				switch (position_dlg) {
				case 0: // 查看pallets
					if(is_start_scan == 1){ 		//已开始扫描,不让修改
						//debug
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Has started scanning, can't change!");
						return;
					}
					showDlg_confirmQty(palletBean);
					break;
				case 1:
					showDlg_confirmDel(palletBean);
					break;
				case 2:
					if (palletBean.isPrinted()) {
						RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
						String msg = String.format("These Labels has been printed by %s. Reprint?", palletBean.print_user_name);
						builder.setMessage(msg);
						builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if(palletBean.isLocalScaned){
									upLoadScanData(palletBean);
								}else{
									config_toPrint=palletBean;
									printTool.showDlg_printers(printLs_pConfig);
								}
							}
						});
						builder.create().show();
					} else {
						if(palletBean.isLocalScaned){
							upLoadScanData(palletBean);
						}else{
							config_toPrint=palletBean;
							printTool.showDlg_printers(printLs_pConfig);
						}
					}
					break;
				default:
					break;
				}
			}
		});
		bd.show();
    	
    }

    private void showDlg_confirmQty(final Rec_PalletBean palletBean) {
        //v
        View layout = View.inflate(mActivity, R.layout.dlg_rec_scantask_breakac, null);
        TextView systemNum = (TextView) layout.findViewById(R.id.systemNum);
        TextView palletNo = (TextView) layout.findViewById(R.id.palletNo);
        final EditText et = (EditText) layout.findViewById(R.id.et);
        final int total = palletBean.normal_qty + palletBean.break_qty;
        systemNum.setText(total + "");
        palletNo.setText(palletBean.wms_container_id);
        et.setText(palletBean.break_qty+"");


        RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
        builder.setTitle("Confirm Pallet Pieces");
        builder.setView(layout);
        builder.setPositiveButton(getString(R.string.patrol_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int piece = Utility.parseInt(et.getText().toString());
                if(piece <=0 ){
                	UIHelper.showToast("Please enter a valid number !");
                	return;
                }
                //输入超出了normal
                if (piece > total) {
                    UIHelper.showToast("The input can't exceed the default maximum!").show();
                    return;
                }

              //超出了-clp总qty
                if ((piece + breakQty - palletBean.break_qty) > ccQty) {
                    int count = ccQty - breakQty - palletBean.break_qty;
                    et.setText(count + "");
                    UIHelper.showToast("Exceed CC requred Qty!");
                    return;
                }
                palletBean.setBreakQty(piece);
                atAdapter.notifyDataSetChanged();
                boolean isFinished=refSumUI();
                String tts_str=isFinished?"Finish":(piece+" pieces!");
                TTS.getInstance().speakAll(tts_str);
                
            }
        });
        builder.setNegativeButton(getString(R.string.sync_cancel), null);
        RewriteBuilderDialog rd = builder.create();
        rd.show();
    }

    private void showDlg_confirmDel(final Rec_PalletBean pltBean) {
        RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //若未上传
                if (pltBean.isLocalScaned) {
                    pltBean.setBreakQty(0);
                    listBreaks.remove(pltBean);
                    atAdapter.notifyDataSetChanged();
                    refSumUI();
                    return;
                }

                //若已上传
                final Rec_PalletBean scanBean = pltBean;
                RequestParams params = new RequestParams();
                params.add("cc_from_id", scanBean.cc_from_id + "");
                new SimpleJSONUtil() {

                    @Override
                    public void handReponseJson(JSONObject json) {
                        scanBean.setBreakQty(0);
                        listBreaks.remove(pltBean);
                        atAdapter.notifyDataSetChanged();
                        refSumUI();
                    }
                }.doGet(getDeleteUrl(), params, mActivity);
            }
        });
    }

    /*
    * @return 已扫完
    * */
    private boolean refSumUI() {
        int breakQty = Rec_PalletBean.getTotalQty_break(listBreaks);
        this.breakQty = breakQty;

        outputQTY.setText(breakQty + " / " + ccQty);

        boolean isFinished=(breakQty== ccQty || listBreaks.size() == listTlps.size());
        loSubmit.setVisibility(isFinished ? View.VISIBLE : View.GONE);
        btnDelAll.setVisibility(listBreaks.size()>0?View.VISIBLE:View.INVISIBLE);
        
//        //是否显示-breakAll,暂时不显示
//        if( Utility.isEmpty(listBreaks)){
//	        boolean showBreakAll=isCCQty_eq_totalGood;
//	        btnBreakAll.setVisibility(showBreakAll?View.VISIBLE:View.GONE);
//	        tvEmpty.setVisibility(showBreakAll?View.GONE:View.VISIBLE);
//        }
        
        return isFinished;
    }
    
    private void upLoadScanData(final Rec_PalletBean bean) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pallet_no", bean.con_id);
            jsonObject.put("qty", bean.break_qty);
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        if(is_start_scan == 1) {
        	return;
        }

        RequestParams params = new RequestParams();
        params.add("receipt_line_id", itemBean.receipt_line_id);
        params.add("pallet_qty", jsonArray.toString());
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
            	
            	bean.isLocalScaned = false;
            	
            	config_toPrint=bean;
				printTool.showDlg_printers(printLs_pConfig);
            }
        }.doPost(getLoadUrl(), params, mActivity);

    }
    

    /**
     * @param showSuccess 直接返回时 不提示success
     */
    private void upLoadScanData(final boolean showSuccess) {
        JSONArray jsonArray = new JSONArray();
        for (Rec_PalletBean scanBean : listBreaks) {
            if (scanBean.isLocalScaned) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("pallet_no", scanBean.con_id);
                    jsonObject.put("qty", scanBean.break_qty);
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        //若无提交的,直接退出
        if (Utility.isEmpty(jsonArray)) {
            toLastAc();
            return;
        }
        
        if(is_start_scan == 1) {
        	toLastAc();
        	return;
        }

        RequestParams params = new RequestParams();
        params.add("receipt_line_id", itemBean.receipt_line_id);
        params.add("pallet_qty", jsonArray.toString());
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                if(showSuccess)
                    UIHelper.showToast(getString(R.string.sync_success)).show();
                toLastAc();
            }
        }.doPost(getLoadUrl(), params, mActivity);

    }

    private void toLastAc() {
        Intent in = new Intent();
        in.putExtra("breakQty", breakQty);
        in.putExtra("steptwo", breakQty == ccQty);
        setResult(RESULT_OK, in);
        finish();
    }

    //返回处理
    @Override
    protected void onBackBtnOrKey() {
        upLoadScanData(false);
//        super.onBackBtnOrKey();
    }

    protected String getDeleteUrl() {
        return HttpUrlPath.basePath + "_receive/android/deleteBreakPallet";
    }

    protected String getLoadUrl() {
        return HttpUrlPath.basePath + "_receive/android/breakPallets";
    }

//    protected String getDemoUrl() {
//        return HttpUrlPath.basePath + "_receive/android/acquirePalletsInfoByLine";
//    }

    //===========================传参==========================================
    
    private int is_start_scan;	//已开始扫描

    public static void initParams(Intent in, Rec_ItemBean itemBean, List<Rec_PalletBean> listTlps,
                                  List<Rec_PalletBean> listBreakPlts,int is_start_scan) {
        in.putExtra("itemBean", (Serializable) itemBean);
        in.putExtra("listTlps", (Serializable) listTlps);
        in.putExtra("listBreakPlts", (Serializable) listBreakPlts);
        in.putExtra("is_start_scan", is_start_scan);
    }

    private void applyParams() {
        Bundle params = getIntent().getExtras();
        is_start_scan = params.getInt("is_start_scan", 0);
        itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
        listTlps = (List<Rec_PalletBean>) params.getSerializable("listTlps");
        listBreaks = (List<Rec_PalletBean>) params.getSerializable("listBreakPlts");
        //接口分开返回的
        listTlps.addAll(listBreaks);
        ccQty = itemBean.clp_expected_qty;
    }

    //    ======================================================================
    private final class ATAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listBreaks == null ? 0 : listBreaks.size();
        }

        @Override
        public Object getItem(int position) {
            return listBreaks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
//                item_rec_scantask_breakac
                convertView = View.inflate(mActivity, R.layout.item_break_plt, null);
                holder = new ViewHolder();
                holder.plt = (TextView) convertView.findViewById(R.id.palletNo);
                holder.qty = (TextView) convertView.findViewById(R.id.configQty);
                holder.tvPartial=(TextView) convertView.findViewById(R.id.tvPartial);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            //刷ui
            Rec_PalletBean b = listBreaks.get(position);
            holder.plt.setText(b.wms_container_id);
            int total = b.normal_qty + b.break_qty;
            holder.qty.setText(b.break_qty + " / " + total);
            holder.tvPartial.setVisibility(b.break_qty<total?View.VISIBLE:View.INVISIBLE);

//            holder.delete.setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    //确认提示框
//                    showDlg_confirmDel(position);
//                }
//            });
            return convertView;
        }

        class ViewHolder {
            TextView plt;
            TextView qty;
            TextView tvPartial;
        }

    }
}
