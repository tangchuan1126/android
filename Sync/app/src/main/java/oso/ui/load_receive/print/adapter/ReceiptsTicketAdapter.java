package oso.ui.load_receive.print.adapter;

import java.util.List;

import oso.ui.load_receive.print.bean.ReceiptsTicketPartenBase;
import oso.ui.load_receive.print.iface.ReceiptsTicketDataInterface;
import support.key.EntryDetailNumberTypeKey;
import utility.StringUtil;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * 
 * @ClassName: ReceiptsTicketBase 
 * @Description:
 * @author gcy 
 * @date 2014年7月17日 上午11:24:47 
 *
 */
public class ReceiptsTicketAdapter extends BaseAdapter {
	
	private  List<ReceiptsTicketPartenBase> arrayList; 
	private LayoutInflater inflater;
	private Context context;
	private ReceiptsTicketDataInterface receiptsTicketDataInterface;
	public ReceiptsTicketAdapter(Context context, List<ReceiptsTicketPartenBase> arrayList,ReceiptsTicketDataInterface receiptsTicketDataInterface) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		this.receiptsTicketDataInterface = receiptsTicketDataInterface;
		this.inflater  = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ReceiptsTicketPartenBase getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		 ReceiptsTicketHoder holder = null;
		 
		 if(convertView==null){
			holder = new ReceiptsTicketHoder();
			convertView = inflater.inflate(R.layout.printiconbar_receipts_ticket_partent_layout_listview_item,null);
			holder.receipts_ticket_type = (TextView) convertView.findViewById(R.id.receipts_ticket_type);
			holder.number_type = (TextView) convertView.findViewById(R.id.number_type);
			holder.receipts_ticket_loadno = (TextView) convertView.findViewById(R.id.receipts_ticket_loadno);
			holder.master_bol_layout = (View) convertView.findViewById(R.id.master_bol_layout);
			holder.receipts_ticket_masterbolno = (TextView) convertView.findViewById(R.id.receipts_ticket_masterbolno);
			holder.list_item = (LinearLayout) convertView.findViewById(R.id.list_item);
 			convertView.setTag(holder);
		}else{
			holder = (ReceiptsTicketHoder) convertView.getTag();
		}
		 
		 final ReceiptsTicketPartenBase temp  =  arrayList.get(position);
 
		 holder.receipts_ticket_type.setText(isNullOfStr(temp.getCustomerid()));
		 holder.number_type.setText(EntryDetailNumberTypeKey.getModuleName(temp.getNumber_type())+":");
		 holder.receipts_ticket_loadno.setText(isNullOfStr(temp.getLoad_no()));
		 
		 //控制是否显示masterbolno布局 如果为 true则显示  false 不显示
		 boolean show_master_bol_layout_flag = !StringUtil.isNullOfStr(temp.getMaster_bol_no());
		 holder.master_bol_layout.setVisibility(show_master_bol_layout_flag?View.VISIBLE:View.GONE);
		 if(show_master_bol_layout_flag){
			 holder.receipts_ticket_masterbolno.setText(isNullOfStr(temp.getMaster_bol_no()));
		 }
		 holder.list_item.removeAllViews();
		 for(int i=0;i<temp.getReceiptsTicketBaseList().size();i++){
			 View   item = inflater.inflate(R.layout.printiconbar_receipts_ticket_layout_listview_item, null);
			 CheckBox chose_print_checkbox = (CheckBox) item.findViewById(R.id.chose_print_checkbox);
			 TextView print_name = (TextView) item.findViewById(R.id.print_name);
			 TextView print_type = (TextView) item.findViewById(R.id.print_type);
			 Button preview_button = (Button) item.findViewById(R.id.preview_button);
//			 View item_layout
			 print_name.setText(isNullOfStr(temp.getReceiptsTicketBaseList().get(i).getName()));
			 print_type.setText(isNullOfStr(temp.getReceiptsTicketBaseList().get(i).getType()));
			 holder.list_item.addView(item);
			 chose_print_checkbox.setChecked(temp.getReceiptsTicketBaseList().get(i).isChecked());
			 final int flagnum = i;
			 chose_print_checkbox.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					temp.getReceiptsTicketBaseList().get(flagnum).setChecked(((CheckBox)v).isChecked());
				}
			});
			 item.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						temp.getReceiptsTicketBaseList().get(flagnum).setChecked(!temp.getReceiptsTicketBaseList().get(flagnum).isChecked());
						((CheckBox) v.findViewById(R.id.chose_print_checkbox)).setChecked(temp.getReceiptsTicketBaseList().get(flagnum).isChecked());
					}
			 });
			 preview_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					receiptsTicketDataInterface.preview(temp.getReceiptsTicketBaseList().get(flagnum));
				}
			});
		 }
		 
 		return convertView;
	}
	
	/**
	 * @Description:过滤数据避免返回NULL的情况
	 * @param str
	 * @return
	 */
	private String isNullOfStr(String str){
		return StringUtil.isNullOfStr(str)?"":(str+"");
	}
}
class ReceiptsTicketHoder {
	public TextView receipts_ticket_type;
	public TextView number_type;
	public TextView receipts_ticket_loadno;
	public TextView receipts_ticket_masterbolno;
	public LinearLayout list_item;
	public View master_bol_layout;
}
