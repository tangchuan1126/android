package oso.ui.load_receive.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.EntryBean;
import oso.ui.load_receive.window.bean.EquipmentBean;
import oso.ui.load_receive.window.bean.TmpEquipBean;
import oso.ui.load_receive.window.util.AdpEquipOccupy;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.selectoccupybutton.SelectOccupyBar;
import oso.widget.selectoccupybutton.SelectOccupyBar.OnSelectBarListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class UpdateEquipmentInfoActivity extends BaseActivity {

	private SingleSelectBar singleSelectBar;
	private EditText equipmentEt, inSealEt, outSealEt;
	private ImageView equipmentIv;

	private SelectOccupyBar soBar;

	private EntryBean mEntry;
	private EquipmentBean mEquipment;

	private String equipmentNoStr;
	private int purpose;

	private ResourceInfo selectDoorInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_update_equipment_info, 0);
		initData();
		initView();
		initSingleSelectBarDialog();
		setData();
	}

	private void initData() {
		mEntry = (EntryBean) getIntent().getSerializableExtra("entryBean");
		mEquipment = (EquipmentBean) getIntent().getSerializableExtra("equipmentBean");
		// setTitleString("Modify");
		setTitleString("E" + mEntry.getEntry_id());
	}

	private void initView() {
		equipmentIv = (ImageView) findViewById(R.id.equipmentIv);
		equipmentEt = (EditText) findViewById(R.id.equipmentEt);
		inSealEt = (EditText) findViewById(R.id.inSealEt);
		outSealEt = (EditText) findViewById(R.id.outSealEt);
		soBar = (SelectOccupyBar) findViewById(R.id.soBar);
		soBar.init(mActivity,mEntry.getEntry_id());
		equipmentEt.setTransformationMethod(new AllCapTransformationMethod());
		inSealEt.setTransformationMethod(new AllCapTransformationMethod());
		outSealEt.setTransformationMethod(new AllCapTransformationMethod());
	}

	private void setData() {
		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(mEquipment.getEquipment_type(), equipmentIv);

		equipmentEt.setText(mEquipment.getEquipment_number());
		boolean isInSeal = mEquipment.getSeal_delivery() == null || mEquipment.getSeal_delivery().equals("NA");
		boolean isOutSeal = mEquipment.getSeal_pick_up() == null || mEquipment.getSeal_pick_up().equals("NA");
		inSealEt.setText(isInSeal ? "" : mEquipment.getSeal_delivery());
		outSealEt.setText(isOutSeal ? "" : mEquipment.getSeal_pick_up());
		if (mEquipment.getResources_name() == null || mEquipment.getResources_type() == 0) {
			soBar.initState(mActivity);
		} else {
			soBar.setCurrentType(mActivity,mEquipment.getResources_type());
			soBar.setCurrentValue(mEquipment.getResources_name());
		}

		soBar.setOnSelectBarListener(new OnSelectBarListener() {
			@Override
			public void onClick(List<ResourceInfo> resourceList) {
				Intent intent = new Intent(mActivity, ChangeEquipmentDoorActivity.class);
				intent.putExtra("entryBean", mEntry);
				intent.putExtra("equipmentBean", mEquipment);
				intent.putExtra("doorlist", (Serializable) resourceList);
				intent.putExtra("type", soBar.getCurrentType());
				startActivityForResult(intent, ChangeEquipmentDoorActivity.Code);
			}
		});
		// 车头不显示type
		if (mEquipment.getEquipment_type() == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			findViewById(R.id.typeLay).setVisibility(View.GONE);
		}
	}

	public void submitOnClick(View v) {
		equipmentNoStr = equipmentEt.getText().toString().trim();
		if (TextUtils.isEmpty(equipmentNoStr)) {
			UIHelper.showToast(mActivity, getString(R.string.sync_input_equipmentno), Toast.LENGTH_SHORT).show();
			return;
		}
		reqUpdate(false);
	}

	private void initSingleSelectBarDialog() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_liveload), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_dropoff), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					purpose = 1;
					break;
				case 1:
				default:
					purpose = 2;
					break;
				}
				// UIHelper.showToast(mActivity,
				// CheckInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(purpose),
				// Toast.LENGTH_SHORT).show();
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(mEquipment.getEquipment_purpose() < 1 ? 0 : mEquipment.getEquipment_purpose() - 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == ChangeEquipmentDoorActivity.Code && resultCode == ChangeEquipmentDoorActivity.Code) {
			selectDoorInfo = (ResourceInfo) data.getSerializableExtra("selectDoorInfo");
			setDoorView();
		}
	}

	private void setDoorView() {
		if (selectDoorInfo.resource_type != 0 && selectDoorInfo.resource_name != null) {
			soBar.setCurrentType(mActivity,selectDoorInfo.resource_type);
			soBar.setCurrentValue(selectDoorInfo.resource_name);
		} else {
			soBar.initState(mActivity);
		}
	}

	private void reqUpdate(boolean reAssignEquip) {
		String inSealStr = inSealEt.getText().toString().trim();
		String outSealStr = outSealEt.getText().toString().trim();

		RequestParams params = new RequestParams();
		params.add("Method", "AddOrUpdateEquipment");
		params.add("equipment_id", mEquipment.getEquipment_id() + "");
		params.add("equipment_number", equipmentNoStr.toUpperCase());
		params.add("check_in_entry_id", mEquipment.getCheck_in_entry_id() + "");
		params.add("equipment_purpose", purpose + "");
		params.add("is_check_success", reAssignEquip ? "1" : "0");

		params.add("equipment_type", mEquipment.getEquipment_type() + "");
		params.add("seal_delivery", TextUtils.isEmpty(inSealStr) ? "NA" : inSealStr.toUpperCase());
		params.add("seal_pick_up", TextUtils.isEmpty(outSealStr) ? "NA" : outSealStr.toUpperCase());
		String occupy_type = "";
		String occupy_id = "";
		if (selectDoorInfo != null) {
			occupy_type = selectDoorInfo.resource_type + "";
			occupy_id = selectDoorInfo.resource_id + "";
		} else if (mEquipment != null) {
			occupy_type = mEquipment.getResources_type() + "";
			occupy_id = mEquipment.getResources_id() + "";
		}
		params.add("occupy_type", occupy_type);
		params.add("occupy_id", occupy_id);

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("WindowCheckInGetEquipmentInfos" + json.toString());
				int flag = json.optInt("flag");
				// 设备已存在
				if (flag == 2) {
					showDlg_quitEquip(json);
					return;
				}
				Utility.popTo(mActivity, TaskListActivity.class);
				mActivity.finish();
			}
		}.doPost(HttpUrlPath.AndroidWindowCheckInAction, params, mActivity);
	}

	// 提示-释放设备
	private void showDlg_quitEquip(JSONObject json) {
		List<TmpEquipBean> listEquips = null;
		try {
			listEquips = new Gson().fromJson(json.optJSONArray("equipment_not_entry").toString(), new TypeToken<List<TmpEquipBean>>() {
			}.getType());
		} catch (Exception e) {
			// TODO: handle exception
		}
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle(getString(R.string.checkout_equipment));
		builder.setAdapter(new AdpEquipOccupy(this, listEquips), null);
		builder.setPositiveButton(getString(R.string.yms_gate_checkout), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqUpdate(true);
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.show();
	}
}
