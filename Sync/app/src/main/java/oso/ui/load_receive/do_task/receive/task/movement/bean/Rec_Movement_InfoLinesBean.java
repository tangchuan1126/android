package oso.ui.load_receive.do_task.receive.task.movement.bean;

import java.io.Serializable;
import java.util.List;

public class Rec_Movement_InfoLinesBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4397849581724708614L;
	
	
	public String company_id;
	public String counted_user;
	//直接用pallets.size即可
//	public int pallet_size;
	public String item_id;
	
	public List<Rec_Movement_InfoPalletBean> pallets;

}
