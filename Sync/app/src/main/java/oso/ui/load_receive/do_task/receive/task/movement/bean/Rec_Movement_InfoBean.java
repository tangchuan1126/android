package oso.ui.load_receive.do_task.receive.task.movement.bean;

import java.io.Serializable;
import java.util.List;

public class Rec_Movement_InfoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5843603916652855895L;
	
	
	public String customer_id;
	public String supplier_id;
	public String receipt_no;
	public int receipt_id;
	
}
