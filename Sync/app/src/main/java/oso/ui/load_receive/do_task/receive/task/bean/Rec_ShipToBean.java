package oso.ui.load_receive.do_task.receive.task.bean;

import java.io.Serializable;

import android.text.TextUtils;

public class Rec_ShipToBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8998378502404577933L;
	public String ship_to_id;
	public String ship_to_name;
	
	//==================tmp===============================

	public boolean isSelect = false;
	
	public String upperCase(){
		return TextUtils.isEmpty(ship_to_name) ? "":ship_to_name.substring(0,1);
	}
}
