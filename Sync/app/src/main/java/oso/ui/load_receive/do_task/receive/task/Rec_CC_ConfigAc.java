package oso.ui.load_receive.do_task.receive.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.adapter.PalletAdapter;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_ShipToBean;
import oso.ui.load_receive.do_task.receive.task.util.QtyDialog_CC;
import oso.ui.load_receive.do_task.receive.wms.Receive_ConfigDetailAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.VpWithTab;
import oso.widget.VpWithTab.OnVpPageChangeListener;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteBuilderDialog.Builder;
import oso.widget.orderlistview.ChooseUsersActivity;
import oso.widget.orderlistview.CommonAdapter;
import oso.widget.rightmenu.ChickMoreInfo;
import oso.widget.rightmenu.RightButtonForMenu;
import oso.widget.singleSelect.SingleSelectBar;
import oso.widget.switchbtn.SwitchButton;
import support.AppConfig;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.print.PrintTool;
import support.common.print.PrintTool.OnPrintLs;
import support.dbhelper.StoredData;
import support.dbhelper.TmpDataUtil;
import support.dbhelper.TmpUtil;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import declare.com.vvme.R.color;

/**
 * @ClassName: Rec_CC_ConfigAc 
 * @Description: 用于显示一票货过来后的商品数量 该类商品都是相同的 并且放置在TLP上 
 * 				   需要经过CC人员创建托盘来重新打成CLP包装
 * 				   创建的过程需要根据商品的总数量来创建适合的托盘类型，并且所创建的托盘可装商品的总数不应该大于原有的商品数量只能存在（小于等于的关系）
 * 				   当创建托盘后 需要打印并指派相关人员去来完成这个任务
 * @author gcy
 * @date 2015-5-20 下午8:03:50
 */
public class Rec_CC_ConfigAc extends BaseActivity implements OnClickListener {

	private final int What_ChooseSupervisor = 1;

	private View loOperator, btnAssign,loEmpty_dmg,loRemain;
	private TextView tvOperator, tvItem, tvLotNO, tvRemain_title,tvCountQty,tvExpectedQty,tvCustomer,tvTitle;
	
	private TextView tvRemain;//根据所创建的托盘来计算剩余的商品数量
	
	private ListView lvCLP;
	
	private ListView lvInbound;
	
	
	private SingleSelectBar ssb;
	private VpWithTab vp;
	
	private PalletAdapter adp_in,adp_cc;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_cc_configpallets, 0);
		applyParams();
		setTitleString("Receipt: "+receipt_no);

		// 取v
		lvCLP = (ListView) findViewById(R.id.lvDmg);
		loOperator = findViewById(R.id.loOperator);
		loRemain = findViewById(R.id.loRemain);
		tvOperator = (TextView) findViewById(R.id.tvOperator);
		btnAssign = findViewById(R.id.btnAssign);
		tvItem = (TextView) findViewById(R.id.tvItem);
		tvLotNO = (TextView) findViewById(R.id.tvLotNO);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
		tvCountQty = (TextView) findViewById(R.id.tvCountQty);
		tvExpectedQty = (TextView) findViewById(R.id.tvExpectedQty);
		tvRemain_title = (TextView) findViewById(R.id.tvRemain_title);
		loEmpty_dmg=findViewById(R.id.loEmpty_dmg);
		
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		
		adp_in = new PalletAdapter(this,listConfigs);
		adp_cc = new PalletAdapter(this,listConfigs_cc);
		
		lvInbound = (ListView) findViewById(R.id.lvInbound);
		lvInbound.setAdapter(adp_in);
		lvInbound.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(!isStartScan) return;
				showMenu_Inbound(position);
			}
		});
		//
		vp = (VpWithTab) findViewById(R.id.vp);
		initSingleSelectBar();
		vp.init(ssb);
		// 事件
		loOperator.setOnClickListener(this);
		btnAssign.setOnClickListener(this);
		findViewById(R.id.btnAddDmgPlts).setOnClickListener(this);

		// lvDmg
		lvCLP.setEmptyView(loEmpty_dmg);
		lvCLP.setAdapter(adp_cc);
		lvCLP.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(!isStartScan) return;
				showMenu_item(position,true);
			}
		});

		// 初始化
//		loOperator.setVisibility(isFromSupvisor ? View.VISIBLE : View.GONE);
		btnAssign.setVisibility(isFromSupvisor ? View.VISIBLE : View.GONE);
		
		// linesBean = (Rec_ItemBean) getIntent()
		// .getSerializableExtra("personBean");
		tvItem.setText(itemBean.item_id);
		if(TextUtils.isEmpty(itemBean.lot_no)){
			tvLotNO.setText("");
			tvLotNO.setHint("NA");
		}else{
			tvLotNO.setText(itemBean.lot_no);
		}
		
		
		tvCustomer.setText(itemBean.customer);
		tvTitle.setText(itemBean.supplier);
		
		tvExpectedQty.setText(itemBean.expected_qty+"");
		tvCountQty.setText(itemBean.getQtyCout());
		
		int goodQty=Rec_PalletConfigBean.getTotalQty_good(listConfigs);
		refSumUI();
		
		
		showRightButton(R.drawable.menu_btn_style, null, new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!isStartScan) return;
				showMenu_AddConfig();
			}
		});
		
		vp.setOnVpPageChangeListener(new OnVpPageChangeListener() {
			
			@Override
			public void onPageChange(int index) {
				if(index ==0 ){
				}else{
					loEmpty_dmg.setVisibility(isStartScan ? View.VISIBLE : View.GONE);
					loRemain.setVisibility(isStartScan ? View.VISIBLE : View.GONE);
				}
			}
		});
	}

	private void initSingleSelectBar() {
		ssb = (SingleSelectBar) findViewById(R.id.ssb);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Inbound", 0));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("New Config.", 1));
		ssb.setUserDefineClickItems(clickItems);
		ssb.userDefineSelectIndexExcuteClick(0);
	}
	
	OnPrintLs lsAll = new OnPrintLs() {
		@Override
		public void onPrint(long printServerID) {
            
            String cfgIDs=getPltIDs(listConfigs_cc);
//			RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_LineID, itemBean.receipt_line_id);
            RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_ConfigIDs, cfgIDs);
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					// TODO Auto-generated method stub
					String name = StoredData.getUsername();
					for (Rec_PalletConfigBean bean : listConfigs_cc) {
						bean.print_user_name = name;
					}
					//debug
//					for (Rec_PalletBean pltDmg : listConfig) {
//						pltDmg.print_user_name=name;
//					}
//					adp.notifyDataSetChanged();
					adp_cc.notifyDataSetChanged();
					UIHelper.showToast(getString(R.string.sync_print_success));
				}
			}.doGet(NetInterface.recPrintLP_url(), p, mActivity);

		}
	};

    /**
     * 获取-选中的plts的ids,","分割
     * @return
     */
    public static String getPltIDs(List<Rec_PalletConfigBean> list){
        if(Utility.isEmpty(list))
            return "";

        String ret="";
        for (int i = 0; i < list.size(); i++) {
            Rec_PalletConfigBean b=list.get(i);
            ret=ret+b.container_config_id+",";
        }
        if(!TextUtils.isEmpty(ret)){
            //去掉尾部的","
            ret=ret.substring(0, ret.length()-1);
        }
        return ret;
    }

//	private void showDmgMenu(final int position) {
//		final Rec_PalletBean bean = (Rec_PalletBean) adp_cc.getItem(position);
//		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
//		bd.setTitle("Operation");
//		bd.setArrowItems(new String[] { "Print Pallets", "Delete" }, new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
//				// TODO Auto-generated method stub
//				switch (position_dlg) {
//				case 0: // 打印
//					tipPrintDmgPlt(bean);
//					break;
//				case 1: // 删除
//					RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							reqDelPlt(bean);
//						}
//					});
//					break;
//
//				default:
//					break;
//				}
//			}
//		});
//		bd.show();
//	}

	private void tipPrintDmgPlt(final Rec_PalletBean bean) {

		final PrintTool.OnPrintLs printLs = new PrintTool.OnPrintLs() {

			@Override
			public void onPrint(long printServerID) {
				// TODO Auto-generated method stub
				reqPrintPlts(printServerID, bean);
			}
		};

		if (bean.isPrinted()) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			String msg = String.format("These Labels has been printed by %s. Reprint?", bean.print_user_name);
			builder.setMessage(msg);
			builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// config_toPrint = listConfigs.get(position);
					printTool.showDlg_printers(printLs);
				}
			});
			builder.create().show();
		} else {
			// config_toPrint = listConfigs.get(position);
			printTool.showDlg_printers(printLs);
		}
	}

	private void reqPrintPlts(long printServerID, Rec_PalletBean dmgPlt) {
		// String ids = Rec_PalletBean.getPltIDs(tmpPlts);
		final Rec_PalletBean tmpPlt = dmgPlt;
		if (tmpPlt == null)
			return;
		
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, tmpPlt.con_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				String name = StoredData.getUsername();
				tmpPlt.print_user_name = name;
				// debug
				// adpDamagePallets.notifyDataSetChanged();
				// adpPallets.notifyDataSetChanged();
				adp_cc.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, this);

	}

	/**
	 * tab改变时
	 */
/*	private VpWithTab.OnVpPageChangeListener onVpPageChangeListener = new VpWithTab.OnVpPageChangeListener() {

		@Override
		public void onPageChange(int index) {
			// TODO Auto-generated method stub
            //clp才可添加
			imgAdd.setVisibility(!isTab_cntConfig() ? View.VISIBLE : View.INVISIBLE);
		}
	};

	private boolean isTab_cntConfig() {
		return ssb.getCurrentSelectItem().b == 0;
	}

	private void initSingleSelectBar() {
		ssb = (SingleSelectBar) findViewById(R.id.ssb);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Count Cfg.", 0));
		// debug
		clickItems.add(new HoldDoubleValue<String, Integer>("CC Cfg.", 1));
		ssb.setUserDefineClickItems(clickItems);
		// ssb.setUserDefineCallBack(new SelectBarItemClickCallBack() {
		// @Override
		// public void clickCallBack(HoldDoubleValue<String, Integer>
		// selectValue) {
		// // switch (selectValue.b) {
		// // case 0:
		// // break;
		// // case 1:
		// // break;
		// // }
		// }
		// });
		ssb.userDefineSelectIndexExcuteClick(0);
	}*/

	private String adid_notify; // 人
	
	public final static int Req_ViewPlts=1000;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		//--
		if(requestCode == Rec_CC_ConfigAc.Req_ViewPlts)
			refresh();
			
		if (resultCode == RESULT_CANCELED)
			return;
		
		
		if(dlg_addConfig!=null)
			dlg_addConfig.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case What_ChooseSupervisor: // 选管理员
			String svNames = data.getStringExtra("select_user_names");
			adid_notify = data.getStringExtra("select_user_adids");
			
			showConfirm_Choose(svNames);
			break;

		default:
			break;
		}
	}
	
	
	private void showConfirm_Choose(String person){
		
		
		final BottomDialog bd = new BottomDialog(mActivity);
		
		bd.setTitle("Assign Scan Task");
		
		View v = View.inflate(mActivity, R.layout.dialog_cc_configac_choose, null);
		
		bd.setContentView(v);
		
		TextView tv = (TextView) v.findViewById(R.id.notifyEt);
		tv.setText(person);
		
		tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toChooseUser();
				bd.dismiss();
			}
		});
		
		bd.setOnSubmitClickListener(getString(R.string.assign_task_assign), new BottomDialog.OnSubmitClickListener() {
			
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				reqAssign();
				dlg.dismiss();
			}
		});
		
		bd.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private void refresh() {
		RequestParams p = new RequestParams();
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", itemBean.receipt_line_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
//				JSONArray ja = json.optJSONArray("pallets");
//				List<Rec_PalletConfigBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
//				}.getType());

                json=json.optJSONObject("data");
                //---------System pallet*(TLP)
                JSONArray ja = json.optJSONArray("pallets");
                List<Rec_PalletConfigBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
                }.getType());
                //---------Create pallet*(CLP)
                JSONArray jaCfg_clp = json.optJSONArray("pallets_cc");
                List<Rec_PalletConfigBean> listClps = new ArrayList<Rec_PalletConfigBean>();
                if (!Utility.isEmpty(jaCfg_clp)) {
                    listClps = new Gson().fromJson(jaCfg_clp.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
                    }.getType());
                }
                
				listConfigs.clear();
				listConfigs.addAll(list);
//				adp.notifyDataSetChanged();

                listConfigs_cc.clear();
                listConfigs_cc.addAll(listClps);
                adp_cc.notifyDataSetChanged();
				refSumUI();

			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletConfigsByLine", p, mActivity);
//        findItemPalletConfig
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(Utility.isFastClick())
			return;
		switch (v.getId()) {
		case R.id.loOperator: // labor
//			int dep=BaseDataDepartmentKey.Scanner;
//			int pos= BaseDataSetUpStaffJobKey.Employee;
//			if (AppConfig.isDebug) {
//				dep = BaseDataDepartmentKey.Warehouse;
//				pos = BaseDataSetUpStaffJobKey.All;
//			}
//
//			Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
//			ChooseUsersActivity.initParams(intent, false, dep, pos, ChooseUsersActivity.KEY_REC_SCANLABOR);
//			startActivityForResult(intent, What_ChooseSupervisor);
			break;
		case R.id.btnAssign: // 分配
			adid_notify = "";
			toChooseUser();
			break;
		
//		case R.id.btnAddGoodPlts:	//添加好托盘
//			showDlg_addPallet(true);
//			break;
		case R.id.btnAddDmgPlts:	//添加-坏托盘
			showDlg_addPallet(false);
			break;

		default:
			break;
		}
	}
	
	private void toChooseUser(){
		int dep=BaseDataDepartmentKey.Scanner;
		int pos= BaseDataSetUpStaffJobKey.Employee;
        //debug
		if (AppConfig.isDebug) {
			dep = BaseDataDepartmentKey.Warehouse;
			pos = BaseDataSetUpStaffJobKey.All;
		}

		Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
		intent.putExtra("selected_adids", adid_notify);
		ChooseUsersActivity.initParams(intent, false, dep, pos, ChooseUsersActivity.KEY_REC_SCANLABOR);
		startActivityForResult(intent, What_ChooseSupervisor);
	}

	private void reqAssign() {

		if (TextUtils.isEmpty(adid_notify)) {
			UIHelper.showToast(getString(R.string.sync_select_labor));
			return;
		}
		
		RequestParams p = new RequestParams();
		p.add("receipt_id", itemBean.receipt_id);
		p.add("receipt_line_id", itemBean.receipt_line_id);
		p.add("operator", adid_notify);
		p.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				finish();
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/createScanTaskToScaner", p, this);
//		assignScanTask
	}

	int palletsType;

	private List<Rec_PalletConfigBean> listConfigs = new ArrayList<Rec_PalletConfigBean>();	//cnt.config
	private List<Rec_PalletConfigBean> listConfigs_cc = new ArrayList<Rec_PalletConfigBean>();	//cc.config
	
	//----------
	private List<Rec_ShipToBean> skipTo ;
	private List<Rec_CLPType>    clpType;

	private QtyDialog_CC dlg_addConfig;
	
	private void showDlg_addPallet(boolean def_good) {
//		int remain = itemBean.expected_qty - Rec_PalletConfigBean.getTotalQty(listConfigs);

        int goodQty=Rec_PalletConfigBean.getTotalQty_keep(listConfigs);
        int clpQty=Rec_PalletConfigBean.getTotalQty(listConfigs_cc);
        int remain=goodQty-clpQty;
		if (remain <= 0) {
			RewriteBuilderDialog.showSimpleDialog_Tip(this, "No piece remain!");
			return;
		}

		LocBean defLoc = TmpUtil.getRec_def_loc(receipt_no, listConfigs_cc);
		dlg_addConfig= new QtyDialog_CC(mActivity, remain,receipt_no,defLoc,skipTo,clpType);
		dlg_addConfig.setOnSubmitQtyListener(new QtyDialog_CC.OnSubmitQtyListener() {
			public void onSubmit() {
				reqAddConfig(dlg_addConfig);
			}
		});
		dlg_addConfig.show();
	}
	
	/**
	 * 当期tab
	 * 
	 * @param isTab_goodPlts
	 */
//	private void selGoodPltTab(boolean isTab_goodPlts) {
//		vp.setCurrentItem(isTab_goodPlts ? 0 : 1);
//	}

	/**
	 * 添加plts
	 * @param dlg
	 */
	private void reqAddConfig(final QtyDialog_CC dlg) {
		Rec_ItemBean bean=itemBean;
		RequestParams p=dlg.getNetParam(itemBean.receipt_line_id, bean.item_id, bean.lot_no,bean.detail_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//debug
				JSONArray joConfig=json.optJSONArray("container_config");
				if (Utility.isEmpty(joConfig)) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity,
							"Invalid add return!");
					return;
				}
				List<Rec_PalletConfigBean> tmpList = new Gson().fromJson(
						joConfig.toString(),
						new TypeToken<List<Rec_PalletConfigBean>>() {
						}.getType());
                listConfigs_cc.clear();
                listConfigs_cc.addAll(tmpList);
				adp_cc.notifyDataSetChanged();
				
				refSumUI();
				dlg.dismiss();
			}
		}.doGet(dlg.getReqUrl(), p, this);
	}

	private void tipPrint() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_print_label), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});
	}

	private PrintTool printTool = new PrintTool(this);
	private PrintTool.OnPrintLs printLs_pConfig = new PrintTool.OnPrintLs() {

		@Override
		public void onPrint(long printServerID) {
			// TODO Auto-generated method stub
			reqPrintConfig(printServerID);
		}
	};

	private void reqPrintConfig(long printServerID) {

		final Rec_PalletConfigBean tmpConfig = config_toPrint;
		RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_ConfigIDs,  tmpConfig.container_config_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// debug
				tmpConfig.print_user_name = StoredData.getUsername();
				// tmpConfig.print_date=new
				// SimpleDateFormat("yyyy-mm-dd ").format(new
				// Date().toString());
//				adp.notifyDataSetChanged();
				adp_cc.notifyDataSetChanged();
				UIHelper.showToast(getString(R.string.sync_print_success));
			}
		}.doGet(NetInterface.recPrintLP_url(), p, mActivity);
	}
	
	private void showMenu_AddConfig(){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_operation));
		
		String[] strs = null;
		strs = new String[]{"Add New Config"};
		
		builder.setArrowItems(strs, new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showDlg_addPallet(true);
			}
		});
		builder.create().show();
	}
	
	private void showMenu_Inbound(final int position){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_operation));
		
		final Rec_PalletConfigBean bean = listConfigs.get(position);
		String[] strs = null;
		if(bean.keep_inbound == 1){
			strs = new String[]{"Release Inbound"};
		}else{
			strs = new String[]{"Keep Inbound"};
		}
		
		builder.setArrowItems(strs, new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				reqKeepInbound(bean);
			}
		});
		builder.create().show();
	}
	
	private void reqKeepInbound(final Rec_PalletConfigBean bean){
		RequestParams params = new RequestParams();
		params.add("receipt_line_id", bean.receipt_line_id);
		params.add("config_id", bean.container_config_id);
		params.add("keep_inbound", bean.keep_inbound == 0 ? "1" : "0");
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				if(bean.keep_inbound == 0){
					bean.keep_inbound = 1;
				}else{
					bean.keep_inbound = 0;
				}
				adp_in.notifyDataSetChanged();
				refSumUI();
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/keepInbound", params, mActivity);
	}

	private Rec_PalletConfigBean config_toPrint; // 右键菜单-选中的bean

	private void showMenu_item(final int position,final boolean isClp) {
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle(getString(R.string.sync_operation));

        String[] strs=null;
        if(isClp)
           strs=new String[] { "View Pallets", "Print Pallets", "Delete" };
        else
            strs=new String[] { "View Pallets","Print Pallets"};

		bd.setArrowItems(strs, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position_dlg, long id) {
				// TODO Auto-generated method stub
				final Rec_PalletConfigBean bean = isClp?listConfigs_cc.get(position):listConfigs.get(position);
				switch (position_dlg) {
				case 0: // 查看pallets
					reqConfigPlts(position,isClp);
					break;
				case 1: // 打印
					if (bean.isPrinted()) {
						RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
						String msg = String.format("These Labels has been printed by %s. Reprint?", bean.print_user_name);
						builder.setMessage(msg);
						builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
//								config_toPrint = listConfigs_cc.get(position);
								config_toPrint=bean;
								printTool.showDlg_printers(printLs_pConfig);
							}
						});
						builder.create().show();
					} else {
//						config_toPrint = listConfigs_cc.get(position);
						config_toPrint=bean;
						printTool.showDlg_printers(printLs_pConfig);
					}
					break;
				case 2: // 删除
					RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							reqDelConfig_clp(position);
						}
					});

					break;

				default:
					break;
				}
			}
		});
		bd.show();

	}

	private void reqConfigPlts(int pos,final boolean isClp) {
		final Rec_PalletConfigBean b = isClp?listConfigs_cc.get(pos):listConfigs.get(pos);

		RequestParams p = new RequestParams();
		p.add("receipt_line_id", b.receipt_line_id);
		p.add("container_config_id", b.container_config_id);
		p.add("process", b.process+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");
				// Intent in = new Intent(mActivity,
				// Receive_ConfigDetailAc.class);
				// startActivity(in);
                String strJson=isClp?"pallets":"clppallets";
				JSONArray jaPlts = json.optJSONArray(strJson);
				if (jaPlts == null || jaPlts.length() == 0) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No Pallets!");
					return;
				}

				List<Rec_PalletBean> list = new Gson().fromJson(jaPlts.toString(), new TypeToken<List<Rec_PalletBean>>() {
				}.getType());

				Intent in = new Intent(mActivity, Receive_ConfigDetailAc.class);
				Receive_ConfigDetailAc.initParams(in, list, itemBean.receipt_line_id, b.getQtyPerPlt(), isStartScan);
				//startActivity(in);
				startActivityForResult(in, Rec_CC_ConfigAc.Req_ViewPlts);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletsInfoByLine", p, this);

	}

	private void reqDelConfig_clp(final int pos) {
		Rec_PalletConfigBean b = listConfigs_cc.get(pos);

		RequestParams p = new RequestParams();
		p.add("container_config_id", b.container_config_id + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listConfigs_cc.remove(pos);
				adp_cc.notifyDataSetChanged();
				refSumUI();
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/deleteClpPalletConfig", p, this);

//		deleteClpPallet?receipt_line_id=555&con_id=2001000&adid=100198
	}

//	private void reqDelPlt(final Rec_PalletBean pltBean) {
//		RequestParams p = new RequestParams();
//		p.add("con_ids", pltBean.con_id);
//		p.add("ps_id", StoredData.getPs_id());
//		p.add("receipt_line_id", itemBean.receipt_line_id);
//		new SimpleJSONUtil() {
//
//			@Override
//			public void handReponseJson(JSONObject json) {
//				// TODO Auto-generated method stub
//				// 防止-异步出错
//				// 坏的
//				if (pltBean.isDamagePlt() && listConfig.contains(pltBean)) {
//					listConfig.remove(pltBean);
//					adp_cc.notifyDataSetChanged();
//				}
//				// 正常的
//				else if (listConfig.contains(pltBean)) {
//					listConfig.remove(pltBean);
//					adp_cc.notifyDataSetChanged();
//				}
//				// refBtn_createCCTask();
//				UIHelper.showToast("Success!");
//				refSumUI();
//			}
//		}.doGet(HttpUrlPath.basePath + "_receive/android/deletePallet", p, this);
//	}

	private void refSumUI() {
//		int remain = itemBean.expected_qty - Rec_PalletConfigBean.getTotalQty(listConfigs);
//		if (remain >= 0) {
//			tvRemain_title.setText("Item Remain: ");
//			tvRemain.setText(remain + "");
//		} else {
//			tvRemain_title.setText("Item Overage: ");
//			tvRemain.setText((-remain) + "");
//		}

		int nokeepQty=Rec_PalletConfigBean.getTotalQty_keep(listConfigs);
		int clpQty=Rec_PalletConfigBean.getTotalQty(listConfigs_cc);
		clpQty = nokeepQty - clpQty;
        tvRemain.setText(clpQty+"");
		
		//刷新plts
		int gootPltCnt=Rec_PalletConfigBean.getTotalPlts(listConfigs);
		//debug
		int dmgPltCnt=Rec_PalletConfigBean.getTotalPlts(listConfigs_cc);
//		clp_count.setText(dmgPltCnt+"");
//		clp_count.setVisibility(dmgPltCnt>0?View.VISIBLE:View.INVISIBLE);
//		
//		tlp_count.setText(gootPltCnt+"");
//		tlp_count.setVisibility(gootPltCnt>0?View.VISIBLE:View.INVISIBLE);
	}
	
	// =========传参===============================

	private boolean isFromSupvisor;
	private boolean isStartScan;		//false:(草谁他妈定义反了)已开始扫描(含已开始break)
	private String detail_id;
	private String receipt_no;

	/**
	 * @param in
	 * @param isFromSupvisor
	 *            true:来自supervisor,false:来自receive第一步的配置
	 */
	public static void initParams(Intent in, Rec_ItemBean itemBean, List<Rec_PalletConfigBean> listConfigs,
			List<Rec_PalletConfigBean> listConfigs_clp,
			List<Rec_ShipToBean> skipTp,List<Rec_CLPType> clpType,
			String detail_id,String receipt_no, boolean isFromSupvisor,boolean isStartScan) {
		in.putExtra("isFromSupvisor", isFromSupvisor);
		in.putExtra("isStartScan", isStartScan);
		in.putExtra("listConfigs", (Serializable) listConfigs);
		in.putExtra("listConfigs_clp", (Serializable) listConfigs_clp);
//		in.putExtra("listDmg", (Serializable) listDmg);
		in.putExtra("itemBean", (Serializable) itemBean);
		in.putExtra("detail_id", detail_id);
		in.putExtra("receipt_no", receipt_no);
		//----------------------------------
		in.putExtra("skipTo", (Serializable)skipTp);
		in.putExtra("clpType", (Serializable)clpType);
	}

	private Rec_ItemBean itemBean;

//	private List<Rec_PalletConfigBean> listConfig;
	private List<Rec_CLPType> listClpTypes;

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		isFromSupvisor = params.getBoolean("isFromSupvisor", false);
		isStartScan = params.getBoolean("isStartScan",false);
		listConfigs = (List<Rec_PalletConfigBean>) params.getSerializable("listConfigs");
		listConfigs_cc = (List<Rec_PalletConfigBean>) params.getSerializable("listConfigs_clp");
		
		
		skipTo = (List<Rec_ShipToBean>) params.getSerializable("skipTo");
		clpType = (List<Rec_CLPType>) params.getSerializable("clpType");
		
		//debug
//		listDmg = (List<Rec_PalletConfigBean>) params.getSerializable("listDmg");
		itemBean = (Rec_ItemBean) params.getSerializable("itemBean");
		detail_id = params.getString("detail_id");
		receipt_no=params.getString("receipt_no");
	}

	public static void toThis(final Activity ct, final Rec_ItemBean itemBean, final boolean isFromSupvisor, final boolean isStartScan,final String detail_id,
			final String receipt_id,final String receipt_no) {
		RequestParams p = new RequestParams();
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", itemBean.receipt_line_id);
		p.add("need_pc_info", "1");				//需clpType
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				json=json.optJSONObject("data");
				

				//---------System pallet*(TLP)
				JSONArray ja = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
				}.getType());


				//---------Create pallet*(CLP)
				JSONArray jaCfg_clp = json.optJSONArray("pallets_cc");
				List<Rec_PalletConfigBean> listClps = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jaCfg_clp)) {
					listClps = new Gson().fromJson(jaCfg_clp.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				List<Rec_ShipToBean> listShipTo = new Gson().fromJson(json.optJSONArray("shipToRows").toString(), new TypeToken<List<Rec_ShipToBean>>(){}.getType());
				List<Rec_CLPType> listClpType = new Gson().fromJson(json.optJSONArray("configRows").toString(), new TypeToken<List<Rec_CLPType>>(){}.getType()); 
				 
				Intent in = new Intent(ct, Rec_CC_ConfigAc.class);
				itemBean.receipt_id = receipt_id;
				Rec_CC_ConfigAc.initParams(in, itemBean, list, listClps, listShipTo, listClpType, detail_id,receipt_no, isFromSupvisor,isStartScan);
				ct.startActivity(in);

			}
//			findItemPalletConfig
		}.doGet(HttpUrlPath.basePath + "_receive/android/acquirePalletConfigsByLine", p, ct);
	}

	// ==================nested===============================
}
