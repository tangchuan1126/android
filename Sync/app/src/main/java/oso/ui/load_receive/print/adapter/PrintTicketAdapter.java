package oso.ui.load_receive.print.adapter;

import java.util.List;

import oso.ui.load_receive.print.bean.PrintPackMainBeanModel;
import oso.ui.load_receive.print.bean.ReceiptBillBase;
import oso.ui.load_receive.print.iface.ReceiptsTicketDataInterface;
import support.key.EntryDetailNumberTypeKey;
import utility.StringUtil;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import declare.com.vvme.R;
 
public class PrintTicketAdapter extends BaseAdapter {
	
	private  List<ReceiptBillBase> arrayList; 
	private LayoutInflater inflater;
	private Context context;
	private PrintPackMainBeanModel receiptTicketModel;
	private ReceiptsTicketDataInterface receiptsTicketDataInterface;
	public PrintTicketAdapter(Context context, List<ReceiptBillBase> arrayList,ReceiptsTicketDataInterface receiptsTicketDataInterface,PrintPackMainBeanModel receiptTicketModel) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		this.receiptTicketModel = receiptTicketModel;
		this.receiptsTicketDataInterface = receiptsTicketDataInterface;
		this.inflater  = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public ReceiptBillBase getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		 PrintTicketHoder holder = null;
		 
		 if(convertView==null){
			holder = new PrintTicketHoder();
			convertView = inflater.inflate(R.layout.print_ticket_partent_layout_listview_item,null);
			holder.receipts_ticket_type = (TextView) convertView.findViewById(R.id.receipts_ticket_type);
			holder.number_type = (TextView) convertView.findViewById(R.id.number_type);
			holder.receipts_ticket_loadno = (TextView) convertView.findViewById(R.id.receipts_ticket_loadno);
			holder.master_bol_layout = (View) convertView.findViewById(R.id.master_bol_layout);
			holder.receipts_ticket_masterbolno = (TextView) convertView.findViewById(R.id.receipts_ticket_masterbolno);
			holder.masterbol_view = (View) convertView.findViewById(R.id.masterbol_view);
			holder.bol_view = (View) convertView.findViewById(R.id.bol_view);
			holder.master_btn = (Button) convertView.findViewById(R.id.preview_button_mbol);
			holder.bol_btn = (Button) convertView.findViewById(R.id.preview_button_bol);
			holder.master_box = (CheckBox) convertView.findViewById(R.id.master_box);
			holder.bol_box = (CheckBox) convertView.findViewById(R.id.bol_box);
  			convertView.setTag(holder);
		}else{
			holder = (PrintTicketHoder) convertView.getTag();
		}
		 
		 final ReceiptBillBase temp  =  arrayList.get(position);
 
		 holder.receipts_ticket_type.setText(isNullOfStr(receiptTicketModel.getCustomer_id()));
		 holder.number_type.setText(EntryDetailNumberTypeKey.getModuleName(temp.getNumber_type())+"");
		 holder.receipts_ticket_loadno.setText(isNullOfStr(temp.getLoad_no()));
		 
		 //控制是否显示masterbolno布局 如果为 true则显示  false 不显示
		 boolean show_master_bol_layout_flag = !StringUtil.isNullOfStr(temp.getMaster_bol_no());
		 holder.master_bol_layout.setVisibility(show_master_bol_layout_flag?View.VISIBLE:View.GONE);
		 
		 if(show_master_bol_layout_flag){
			 holder.receipts_ticket_masterbolno.setText(isNullOfStr(temp.getMaster_bol_no()));
		 }
		 if(temp.getMasterbolpath() != null && temp.getMasterbolpath().length() > 0){
			 holder.masterbol_view.setVisibility(View.VISIBLE);
		 }
		 if(temp.getPath() != null &&  temp.getPath().length() > 0){
			 holder.bol_view.setVisibility(View.VISIBLE);
		 }
		 holder.master_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				receiptsTicketDataInterface.preview(temp,1);
			}
		});
		 holder.bol_btn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					receiptsTicketDataInterface.preview(temp,2);
				}
			});
		 holder.master_box.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					temp.setChecked(((CheckBox)v).isChecked());
				}
			});
		 holder.bol_box.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				temp.setBolchecked(((CheckBox)arg0).isChecked());
			}
		});
 		return convertView;
	}
	
	/**
	 * @Description:过滤数据避免返回NULL的情况
	 * @param str
	 * @return
	 */
	private String isNullOfStr(String str){
		return StringUtil.isNullOfStr(str)?"":(str+"");
	}
}
class PrintTicketHoder {
	public TextView receipts_ticket_type;
	public TextView number_type;
	public TextView receipts_ticket_loadno;
	public TextView receipts_ticket_masterbolno;
	public View master_bol_layout;
	public View masterbol_view;
	public View bol_view;
	public Button master_btn;
	public Button bol_btn;
	public CheckBox master_box;
	public CheckBox bol_box;
}
