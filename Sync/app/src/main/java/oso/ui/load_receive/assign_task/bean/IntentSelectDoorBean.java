package oso.ui.load_receive.assign_task.bean;

import java.io.Serializable;
/**
 * @ClassName: IntentSelectDoorBean 
 * @Description: 传递到更改门的Activity所需要的参数
 * @author gcy
 * @date 2014-12-10 下午5:41:07
 */
public class IntentSelectDoorBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -6799310062343924597L;
	public int resources_id;//门ID
	public int resources_type;	//如:1
	public String resources_type_value;//门名称(同其他bean中resources_type_value不同),如:13

	public int equipment_id;// 1
	public int equipment_type;// 1,
	public String equipment_type_value;// "Tractor",
	public String equipment_number;// "20141125",
	public String check_in_entry_id;// 120847,
	
	public int intentType;

}
