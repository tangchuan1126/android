package oso.ui.load_receive.do_task.load.scan.adapter;

import java.util.List;

import support.common.bean.CheckInLoadPalletTypeBean;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckInLoadPalletTypeBeanAdapter extends BaseAdapter  {
	
	private List<CheckInLoadPalletTypeBean> arrayList;
	
	private LayoutInflater inflater;
	private String curPalletType;
	
	private Context context;
	public CheckInLoadPalletTypeBeanAdapter(Context context, List<CheckInLoadPalletTypeBean> arrayList ) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(context);
	}
	
	public void setCurPType(String palletType){
		this.curPalletType=palletType;
	}
	
	
	//======================================================
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public CheckInLoadPalletTypeBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		CheckInLoadPalletTypeBeanHoder holder = null;
		 if(convertView==null){
			holder = new CheckInLoadPalletTypeBeanHoder();
			convertView = inflater.inflate(R.layout.scan_load_dialog_listview_item,null);
			holder.pallettypeid = (TextView) convertView.findViewById(R.id.pallettypeid);
			holder.pallettypename = (TextView) convertView.findViewById(R.id.pallettypename);
 			convertView.setTag(holder);
		}else{
			holder = (CheckInLoadPalletTypeBeanHoder) convertView.getTag();
		}
		 
		final CheckInLoadPalletTypeBean temp  =  arrayList.get(position);
		holder.pallettypeid.setText(temp.pallettypeid);
		holder.pallettypename.setText(temp.pallettypename);
		
		boolean isCur=!TextUtils.isEmpty(curPalletType) && curPalletType.equals(temp.pallettypeid);
		holder.pallettypeid.setSelected(isCur);
 		return convertView;
	}
 

}
class CheckInLoadPalletTypeBeanHoder {
	public TextView pallettypeid;     
	public TextView pallettypename;
}