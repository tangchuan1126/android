package oso.ui.load_receive.do_task.receive.task;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_AssignTask_ItemBean;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_PalletType;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ItemBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 收货-分配任务-item列表,supervisor使用
 * @author 朱成
 * @date 2015-3-11
 */
public class Rec_ScanTask_ListAc extends BaseActivity {
	
	private ListView lv;
	private List<Rec_AssignTask_ItemBean> listLines=new ArrayList<Rec_AssignTask_ItemBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_rec_assigntasklistac, 0);
		initData();
		initView();
		setData();
	}
	
	private boolean isFirstResume=false;
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(isFirstResume)
			refresh();
		
		isFirstResume = true;
	}
	
	private void refresh(){
		
		RequestParams p=new RequestParams();
		p.add("adid", StoredData.getAdid());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray jaLines=json.optJSONArray("data");
				if(jaLines==null)
					return;
				List<Rec_AssignTask_ItemBean> tmpLines=new Gson().fromJson(
						jaLines.toString(),
						new TypeToken<List<Rec_AssignTask_ItemBean>>(){}.getType());
				listLines.clear();
				listLines.addAll(tmpLines);
				if(listLines.size() == 0){
					onBackBtnOrKey();
					return;
				}
				adp.notifyDataSetChanged();
				
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/findScanTaskList", p,this);
		
	}

	private void initData() {
//		setTitleString("Task Number: " + "100001");
		setTitleString("My CC/Scan Task");
	}

	private void initView() {
		lv = (ListView) findViewById(R.id.lv);
	}

	private void setData() {
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				Intent in = new Intent(mActivity, Rec_ScanTask_ItemAc.class);
//                Rec_ScanTask_ItemAc.initParams(in, listLines.get(position));
//				startActivity(in);
				Rec_AssignTask_ItemBean assignTask = listLines.get(position);
				
				
				if(assignTask.clp_expected_qty == 0 ){
					Rec_ItemBean itemBean = assignTask.to_Rec_ItemBean();
					
					if(!itemBean.hasStartScan()){
						reqLinePlts(true,itemBean);
					}else{
						reqLinePlts(false,itemBean);
					}
				}else{
					reqBreakPlts(assignTask);
				}
			}
		});
		
		refresh();
	}
	
	private void reqBreakPlts(Rec_AssignTask_ItemBean assignTask){
		RequestParams p = new RequestParams();
		p.add("line_id", assignTask.receipt_line_id);
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
				
				Rec_ItemBean itemBean = new Gson().fromJson(json.optJSONObject("data").toString(), Rec_ItemBean.class);
				//Rec_ItemBean itemBean = new Gson().fromJson(json.optJSONObject("data").toString(), new TypeToken<Rec_ItemBean>(){}.getType());
				
				//---------Create Pallet
				JSONArray jPallet = json.optJSONArray("pallets");
				List<Rec_PalletConfigBean> listPallet = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPallet)) {
					listPallet = new Gson().fromJson(jPallet.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//---------Create Pallet
				JSONArray jPalletCC = json.optJSONArray("pallets_cc");
				List<Rec_PalletConfigBean> listPalletCC = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listPalletCC = new Gson().fromJson(jPalletCC.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				//----------Break Partial-------
				JSONArray jBreakPartial = json.optJSONArray("break_partial");
				List<Rec_PalletConfigBean> listBreakPartial = new ArrayList<Rec_PalletConfigBean>();
				if (!Utility.isEmpty(jPalletCC)) {
					listBreakPartial = new Gson().fromJson(jBreakPartial.toString(), new TypeToken<List<Rec_PalletConfigBean>>() {
					}.getType());
				}
				
				Intent in = new Intent(mActivity,Rec_ScanTask_ItemAc.class);
				Rec_ScanTask_ItemAc.initParams(in, itemBean,true,listPallet,listPalletCC,listBreakPartial);
				startActivity(in);
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/acquireReceiptLinesByLineId",p,mActivity);
	}
	
	private void reqLinePlts(final boolean start, final Rec_ItemBean itemBean) {

	        final RequestParams params = new RequestParams();
	        params.add("receipt_line_id", itemBean.receipt_line_id);
	        // Container_config_id 为0时是获取所有的pallets 当值不为0时获取的是所有的pallets的列表
	        params.add("container_config_id", "0");
	        params.add("is_start_scan", start ? "1" : "0");
	        new SimpleJSONUtil() {

	            @Override
	            public void handReponseJson(JSONObject json) {
	                // 打开rn(如:不需扫sn的rn,start后 状态需变)
	                //注:1>rn关闭时 不能再进line(故此处可直接改为open)
	                //debug
//	                recBean.status = Rec_ReceiveBean.RNStatus_Open;
//	                refBtnFinish();

	                JSONArray jaPlts = json.optJSONArray("pallets");
	                // plts
	                String strPlts = jaPlts == null ? "[]" : jaPlts.toString();
	                List<Rec_PalletBean> listPlts = new Gson().fromJson(strPlts,
	                        new TypeToken<List<Rec_PalletBean>>() {
	                        }.getType());

	                // dmgPlts
	                JSONArray jaDmg = json.optJSONArray("damaged");
	                List<Rec_PalletBean> listDmg = null;
	                if (!Utility.isEmpty(jaDmg)) {
	                    listDmg = new Gson().fromJson(jaDmg.toString(),
	                            new TypeToken<List<Rec_PalletBean>>() {
	                            }.getType());
	                    listPlts.addAll(listDmg);
	                }
	                
	                List<Rec_PalletType> listTypes = new Gson().fromJson(json.optJSONArray("contypes").toString(), new TypeToken<List<Rec_PalletType>>(){}.getType());
					

	                // debug,刷新状态
	                // try {
	                // recBean.status =
	                // json.optJSONObject("datas").optJSONObject("receipt").optString("status");
	                // } catch (Exception e) {
	                // // TODO: handle exception
	                // e.printStackTrace();
	                // }

	                Intent in = new Intent(mActivity, Rec_ScanTask_PalletsAc.class);
	                Rec_ReceiveBean recBean=new Rec_ReceiveBean();
	                recBean.curItemBean=itemBean;
	                recBean.receipt_no=itemBean.receipt_no;
	                recBean.receipt_id=itemBean.receipt_id;
	                Rec_ScanTask_PalletsAc.initParams(in, listPlts, listTypes, recBean,true);
	                startActivity(in);
	            }

	        }.setCancelable(false).doGet(HttpUrlPath.acquirePalletsInfoByLine, params, this);
	    }

	//==================nested===============================
	
	private BaseAdapter adp=new BaseAdapter() {

		@Override
		public int getCount() {
			return listLines.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h;
			if (convertView == null) {
				convertView=getLayoutInflater().inflate(R.layout.item_rec_assigntasklistac, null);
				h = new Holder();
				h.tvCCTask=(TextView)convertView.findViewById(R.id.tvCCTask);
				h.tvReceiptID=(TextView)convertView.findViewById(R.id.tvReceiptID);
				h.tvItemID=(TextView)convertView.findViewById(R.id.tvItemID);
				h.tvLotNO=(TextView)convertView.findViewById(R.id.tvLot);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();
			
			//刷ui
			Rec_AssignTask_ItemBean b=listLines.get(position);
			h.tvCCTask.setText(b.receipt_line_id);
//			h.tvReceiptID.setText("Receipt"+b.receipt_id);
            h.tvReceiptID.setText(b.receipt_no);
			h.tvItemID.setText(b.item_id);
			if(TextUtils.isEmpty(b.lot_no)){
				h.tvLotNO.setText("");
				h.tvLotNO.setHint("NA");
			}else{
				h.tvLotNO.setText(b.lot_no);
			}
			
			return convertView;
		}
		
	};
	
	private class Holder{
		TextView tvCCTask,tvReceiptID,tvItemID,tvLotNO;
	}
}
