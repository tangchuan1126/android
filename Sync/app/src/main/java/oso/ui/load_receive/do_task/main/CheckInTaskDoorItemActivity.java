package oso.ui.load_receive.do_task.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.debug.product.SelectItemsActivity;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.load_receive.assign_task.SelectDoorActivity;
import oso.ui.load_receive.assign_task.bean.IntentSelectDoorBean;
import oso.ui.load_receive.do_task.load.samsung.SamsungLoadActivity;
import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import oso.ui.load_receive.do_task.load.scan.SelectSubLoadActivity;
import oso.ui.load_receive.do_task.load.smallparcel.SmallParcelAc;
import oso.ui.load_receive.do_task.load.smallparcel.bean.CarrierBean;
import oso.ui.load_receive.do_task.main.adapter.CheckInTaskItemlistAdapter;
import oso.ui.load_receive.do_task.main.iface.CheckInTaskInterface;
import oso.ui.load_receive.do_task.receive.nolist.LoadNListAc;
import oso.ui.load_receive.do_task.receive.oso.ReceiveOsoAc;
import oso.ui.load_receive.do_task.receive.samsung.ReceiveDetailActivity;
import oso.ui.load_receive.do_task.receive.samsung.bean.Ctnr;
import oso.ui.load_receive.do_task.receive.wms.Receive_ItemListAc;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_ReceiveBean;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.loadbar.LoadBarLayout;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.rightmenu.ChickMoreInfo;
import oso.widget.rightmenu.RightButtonForMenu;
import support.common.AlertUi;
import support.common.UIHelper;
import support.common.bean.CheckInLoadComplexDoor;
import support.common.bean.CheckInLoadLoadBean;
import support.common.bean.CheckInLoadPalletTypeBean;
import support.common.bean.CheckInLoad_ComplexSubLoads;
import support.common.bean.CheckInLoad_SubLoadBean;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.dbhelper.StoredData;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.OccupyTypeKey;
import support.key.TTPKey;
import support.network.HttpPostMethod;
import support.network.NetConnectionInterface;
import support.network.NetConnection_YMS;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;


public class CheckInTaskDoorItemActivity extends BaseActivity implements CheckInTaskInterface {

	// 搜索-类型
	private static final int TYPE_LOAD = 1;
	private static final int TYPE_MASTER_BOL = 2;
	private static final int TYPE_ORDER = 3;

	public static final String Action_ChangeDoor = "declare.com.vvme.Action_ChangeDoor";

	private ListView listView;
	private CheckInTaskBeanMain mainbean;
	private CheckInTaskItemlistAdapter mAdapter;
	private List<CheckInTaskItemBeanMain> submittasklist = new ArrayList<CheckInTaskItemBeanMain>();
	private List<Integer> listItemID = new ArrayList<Integer>();
	private RightButtonForMenu moreButton;
	public List<ResourceInfo> resourceList;

	private TextView small_door;
	public final static int EXCEPTIONNUM = 119;
	public final static int PHOTONUMBER = 49327427;
	public static Resources resources;

	private TabToPhoto ttp;

	public final static int LOAD = 12331;

	StringBuilder alltaskover = new StringBuilder();
	String allstr = null;
	int number = 0;

	LinearLayout finish_linear;
	LinearLayout tsc_linear;
	Button takeover_btn, start_btn, close_btn;
	Button finish_btn;
	LoadBarLayout loadbar_layout;

	ImageView all_checkbox;
	int type = 0;

	private Dialog dialog;
	String globaldetail1 = "";
	String globaldetail2 = "";
	int helpnumber = 0;
	private boolean isRequestHelp = false; // 正在请求帮助

	private String truck_size;// 车型

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dock_checkin_task_dooritem_layout, 0);
		resources = getResources();
		getFromActivityData();
		initView();
		setData(0);

		// 注册广播接收
		IntentFilter filter = new IntentFilter();
		filter.addAction(Action_ChangeDoor); // 只有持有相同的action的接受者才能接收此广播
		registerReceiver(rec_changeDoor, filter);
	}

	private BroadcastReceiver rec_changeDoor = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			IntentSelectDoorBean doorBean = (IntentSelectDoorBean) intent.getSerializableExtra("data");
			mainbean.resources_id = doorBean.resources_id;
			mainbean.resources_type = doorBean.resources_type;
			mainbean.resources_name = doorBean.resources_type_value;
			// 修改标题

			if (mainbean.resources_type == OccupyTypeKey.NULL) {
				small_door.setText(mainbean.resources_name + " ");
			} else {
				small_door.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity, mainbean.resources_type) + " : " + mainbean.resources_name + " ");
			}

		}
	};

	public static void sendBC_changeDoor(Activity ac, IntentSelectDoorBean doorBean) {
		// 通知"task列表"
		Intent in = new Intent(CheckInTaskDoorItemActivity.Action_ChangeDoor);
		in.putExtra("data", doorBean);
		ac.sendBroadcast(in);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(rec_changeDoor);
	}

	/**
	 * @param
	 * @Description:获取来自于上一个界面的数据
	 */
	public void getFromActivityData() {
		mainbean = (CheckInTaskBeanMain) getIntent().getSerializableExtra("taskmainlist");
		number = getIntent().getIntExtra("mainnumber", 0);
		truck_size = getIntent().getStringExtra("truck_size");
		if (StringUtil.isNullOfStr(truck_size)) {
			truck_size = "未知";
		}

		if (Utility.isNullForList(mainbean.complexDoorList)) {
			UIHelper.showToast(mActivity, getString(R.string.sync_data_error), Toast.LENGTH_SHORT).show();
			finish();
		}
		for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
			alltaskover.append(mainbean.complexDoorList.get(i).dlo_detail_id + ",");
		}
		allstr = alltaskover.toString();
		// 移除-最后的","
		if (!TextUtils.isEmpty(allstr)) {
			allstr = allstr.substring(0, allstr.length() - 1);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// 若从拍照过来 则不刷
		if (isReturnFromCam) {
			isReturnFromCam = false;
			return;
		}

		if (number == 0) {
			refreshway();
			// LoadBarLayout loadbar_layout = (LoadBarLayout)
			// findViewById(R.id.loadbar_layout);
			ttp = loadbar_layout.getTabToPhoto();
			loadbar_layout.initData(mainbean, getTabParam(), "");
		}
		number = 0;
	}

	@Override
	public void onRelogin() {
		// TODO Auto-generated method stub
		super.onRelogin();
		// 重新登录时,刷新列表 防止状态改变
		refreshway();
	}

	private TabParam getTabParam() {
		// 刷ttp
		// TabParam p = TabParam.new_NoTitle("Task_processing_"
		// + mainbean.entry_id + mainbean.resources_id, ttp);
		String key = TTPKey.getTaskProcessKey(mainbean.entry_id);
		TabParam p = TabParam.new_NoTitle(key, ttp);
		p.setWebImgsParams(String.valueOf(FileWithCheckInClassKey.PhotoTaskProcessing), String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN),
				mainbean.entry_id);
		return p;
	}

	/*private boolean isAllChecked() {
		return isAllChecked;
	}

	private void setAllChecked(boolean isChecked) {
		all_checkbox.setBackgroundResource(isChecked ? R.drawable.checkbox_pressed : R.drawable.checkbox_normal);
		isAllChecked = isChecked;
	}*/

	public void initView() {
		showRightButton(R.drawable.menu_btn_style, null, new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// -------------设置右上角更多功能的button
				moreButton = new RightButtonForMenu(mActivity, btnRight, makeMoreButtonData());
				if (moreButton != null && !moreButton.isShowing()) {
					moreButton.show();
				}
			}
		});
		// --------------设置标题信息
		setTitleString("E" + mainbean.entry_id + "");
		small_door = (TextView) findViewById(R.id.small_door);
		if (mainbean.resources_type == OccupyTypeKey.NULL) {
			small_door.setText(mainbean.resources_name + " ");
		} else {
			small_door.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity, mainbean.resources_type) + " : " + mainbean.resources_name + " ");
		}
		loadbar_layout = (LoadBarLayout) findViewById(R.id.loadbar_layout);

		listView = (ListView) findViewById(R.id.taskitemLv);
		finish_linear = (LinearLayout) findViewById(R.id.finish_linear);
		tsc_linear = (LinearLayout) findViewById(R.id.tsc_linear);
		takeover_btn = (Button) findViewById(R.id.takeover_btn);
		takeover_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				morepackinglist(2);
			}
		});
		start_btn = (Button) findViewById(R.id.start_btn);
		start_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				morepackinglist(3);
			}
		});
		close_btn = (Button) findViewById(R.id.close_btn);
		close_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				morepackinglist(1);
			}
		});
		finish_btn = (Button) findViewById(R.id.finish_btn);
		all_checkbox = (ImageView) findViewById(R.id.all_checkbox);
		all_checkbox.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
 
				if (type == 0) {
					for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
						if (mainbean.complexDoorList.get(i).number_status == 1 || mainbean.complexDoorList.get(i).number_status == 2
								|| mainbean.complexDoorList.get(i).takeoverflag == 1) {
							//mAdapter.mChecked.set(i, true);
							//mainbean.complexDoorList.get(i).checked = true;

							// add by xia 如果已经CLOSE的不改变状态
							int taskStatus = mainbean.complexDoorList.get(i).number_status;
							if(EntryDetailNumberStateKey.Close != taskStatus) {
								//mAdapter.mChecked.set(i, true);
								mainbean.complexDoorList.get(i).checked = true;
							}
							// add end
						}
					}
					mAdapter.notifyDataSetChanged();
					type = 1;
					setCheckboxStatus(Status.CHECKED);
					tsc_linear.setVisibility(View.VISIBLE);
				} else if (type == 1) {
					for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
						//mAdapter.mChecked.set(i, false);
						mainbean.complexDoorList.get(i).checked = false;
					}
					mAdapter.notifyDataSetChanged();
					type = 0;
					setCheckboxStatus(Status.UNCHECKED);
					tsc_linear.setVisibility(View.GONE);
				}

				showBottomBtn();
			}
		});
		small_door.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// 重置Selector
				/*all_checkbox.setBackgroundResource( all_checkbox.isChecked() ? R.drawable.checkbox_pressed : R.drawable.checkbox_normal);
				all_checkbox.setBackgroundResource(R.drawable.chickbox_style_forcheckin);*/

				if (type == 0) {
					for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
						if (mainbean.complexDoorList.get(i).number_status == 1 || mainbean.complexDoorList.get(i).number_status == 2
								|| mainbean.complexDoorList.get(i).takeoverflag == 1) {

							// add by xia 如果已经CLOSE的不改变状态
							int taskStatus = mainbean.complexDoorList.get(i).number_status;
							if(EntryDetailNumberStateKey.Close != taskStatus) {
								//mAdapter.mChecked.set(i, true);
								mainbean.complexDoorList.get(i).checked = true;
							}
							// add end

							//mAdapter.mChecked.set(i, true);
						}
					}
					mAdapter.notifyDataSetChanged();
					type = 1;
					setCheckboxStatus(Status.CHECKED);
					tsc_linear.setVisibility(View.VISIBLE);
				} else if (type == 1) {
					for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
						//mAdapter.mChecked.set(i, false);
						mainbean.complexDoorList.get(i).checked = false;
					}
					mAdapter.notifyDataSetChanged();
					type = 0;
					setCheckboxStatus(Status.UNCHECKED);
					tsc_linear.setVisibility(View.GONE);
				}

				showBottomBtn();
			}

		});
		finish_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				submitfinish();
			}
		});
		LoadBarLayout loadbar_layout = (LoadBarLayout) findViewById(R.id.loadbar_layout);
		ttp = loadbar_layout.getTabToPhoto();
		// 刷ttp
		loadbar_layout.initData(mainbean, getTabParam(), "");

		// 初始化
		finish_btn.setText(getString(R.string.tms_release) + mainbean.getResTypeStr(mActivity));
	}

	/**
	 * 设置Title “CheckBox”的背景
	 * @param status
	 */
	private void setCheckboxStatus(Status status) {
		if (Status.CHECKED == status) {
			all_checkbox.setBackgroundResource(R.drawable.checkbox_pressed);
			return;
		}

		if (Status.UNCHECKED == status) {
			all_checkbox.setBackgroundResource(R.drawable.checkbox_normal);
			return;
		}

		if (Status.CHECKEDHALF == status) {
			all_checkbox.setBackgroundResource(R.drawable.checkbox_enabled);
			return;
		}
	}

	private enum Status {
		CHECKED, UNCHECKED, CHECKEDHALF;
	}

	/**
	 * 底部操作按钮  add by xialimin
	 */
	private void showBottomBtn() {
		boolean showStartBtn 	= false;
		boolean showOperateBtn  = false;
		boolean showTakeOverBtn = false;

		if(mainbean == null || Utility.isNullForList(mainbean.complexDoorList)) {
			start_btn.setVisibility(showStartBtn ? View.VISIBLE : View.GONE);
			close_btn.setVisibility(showOperateBtn ? View.VISIBLE : View.GONE);
			takeover_btn.setVisibility(showTakeOverBtn ? View.VISIBLE : View.GONE);
		}

		for (int i = 0; i < mainbean.complexDoorList.size(); i ++) {

			CheckInTaskItemBeanMain itemBean = mainbean.complexDoorList.get(i);

			// 若未选中 则跳过
			if (!mainbean.complexDoorList.get(i).checked) {
				continue;
			}

			if (itemBean.is_forget_task == 0) {
				if (itemBean.takeoverflag == 1) {
					showTakeOverBtn = true;
				}
			}
			if (itemBean.number_status == EntryDetailNumberStateKey.Unprocess && itemBean.takeoverflag == 0) {
				showStartBtn = true;
			}
			if (itemBean.is_forget_task == 0) {
				if (itemBean.number_status == EntryDetailNumberStateKey.Processing && itemBean.takeoverflag == 0) {
					// 防止操作员 忘记做Count 直接就Close了，所以BOL/CTNR/Load/PONO/Order 任务不允许直接Close, delivery可以直接Close
					boolean isBolTask = EntryDetailNumberTypeKey.CHECK_IN_BOL == itemBean.number_type;
					boolean isCTNRTask = EntryDetailNumberTypeKey.CHECK_IN_CTN == itemBean.number_type;
					boolean isPONOTask = EntryDetailNumberTypeKey.CHECK_IN_PONO == itemBean.number_type;
					boolean isOrderTask = EntryDetailNumberTypeKey.CHECK_IN_ORDER == itemBean.number_type;
					boolean isLoadTask = EntryDetailNumberTypeKey.CHECK_IN_LOAD == itemBean.number_type;
					boolean isSmallParcel = itemBean.isSmallParcel();

					// 存在任一不可在外面Close的任务  则Close按钮不可出现
					showOperateBtn = !(isBolTask | isCTNRTask | isPONOTask | isOrderTask | isLoadTask | isSmallParcel);

					/*// Load 无 正常/异常 关闭的不可以关闭，有的可以关
					if (itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD || itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER
							|| itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO || itemBean.isSmallParcel()) {
						showOperateBtn = false;
					}*/

					// 如果被选的条目 其他条目能Close的 则显示Close
					if (mainbean.getOtherNeedClose(mainbean.complexDoorList, itemBean)) {
						showOperateBtn = true;
					}
				}
			} else {
				/*if (itemBean.number_status == EntryDetailNumberStateKey.Processing) {
					showOperateBtn = false;
				}*/

				if (itemBean.number_status == EntryDetailNumberStateKey.Processing /*&& itemBean.takeoverflag == 0*/) {
					// 防止操作员 忘记做Count 直接就Close了，所以BOL/CTNR/Load/PONO/Order 任务不允许直接Close, delivery可以直接Close
					boolean isBolTask = EntryDetailNumberTypeKey.CHECK_IN_BOL == itemBean.number_type;
					boolean isCTNRTask = EntryDetailNumberTypeKey.CHECK_IN_CTN == itemBean.number_type;
					boolean isPONOTask = EntryDetailNumberTypeKey.CHECK_IN_PONO == itemBean.number_type;
					boolean isOrderTask = EntryDetailNumberTypeKey.CHECK_IN_ORDER == itemBean.number_type;
					boolean isLoadTask = EntryDetailNumberTypeKey.CHECK_IN_LOAD == itemBean.number_type;
					boolean isSmallParcel = itemBean.isSmallParcel();

					// 存在任一不可在外面Close的任务  则Close按钮不可出现
					showOperateBtn = !(isBolTask | isCTNRTask | isPONOTask | isOrderTask | isLoadTask | isSmallParcel);

					/*// Load 无 正常/异常 关闭的不可以关闭，有的可以关
					if (itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD || itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER
							|| itemBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO || itemBean.isSmallParcel()) {
						showOperateBtn = false;
					}*/

					// 如果被选的条目 其他条目有不能Close的 BOL/CTNR任务，则不显示Close
					if (mainbean.getOtherNeedClose(mainbean.complexDoorList, itemBean)) {
						showOperateBtn = true;
					}
				}
			}
		}

		start_btn.setVisibility(showStartBtn ? View.VISIBLE : View.GONE);
		close_btn.setVisibility(showOperateBtn ? View.VISIBLE : View.GONE);
		takeover_btn.setVisibility(showTakeOverBtn ? View.VISIBLE : View.GONE);
	}

	public JSONArray url() {
		JSONArray jSONArray = new JSONArray();
		for (int i = 0; i < submittasklist.size(); i++) {
			try {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("dlo_detail_id", submittasklist.get(i).dlo_detail_id + "");
				jsonObject.put("number", submittasklist.get(i).number + "");
				if (submittasklist.get(i).number_status == 1) {
					jSONArray.put(jsonObject);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jSONArray;
	}

	public void submitStart() {
		JSONArray jSONArray = new JSONArray();
		jSONArray = url();
		if (jSONArray != null && jSONArray.length() > 0) {
			RequestParams params = new RequestParams();
			params.add("Method", "TaskProcessingStartDetails");
			params.add("entry_id", mainbean.entry_id + "");
			params.add("equipment_id", mainbean.equipment_id + "");
			params.add("resources_id", mainbean.resources_id + "");
			params.add("resources_type", mainbean.resources_type + "");
			params.add("values", jSONArray + "");
			// 改成LoadJson 数据的方式
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					if (!Utility.isNullForList(mainbean.complexDoorList)) {
						mainbean.complexDoorList.clear();
					}
					CheckInTaskItemBeanMain.parseBean2(json, mainbean.complexDoorList);
					setData(1);
				}
			}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
		}
	}

	public void submitfinish() {

		Intent in = new Intent(this, CheckInTaskFinishActivity.class);
		CheckInTaskFinishActivity.initParams(in, mainbean);
		startActivity(in);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		boolean refresh = intent.getBooleanExtra("refresh", false);
		boolean toLastAc = intent.getBooleanExtra("toLastAc", false);
		// 退出
		if (toLastAc) {
			// finish();
			onBackBtnOrKey();
			return;
		}

		if (refresh) {
			refreshway();
		}
	}

	// 至前一页面
	public static void toLastAc(Activity ac) {
		Intent in = new Intent(ac, CheckInTaskDoorItemActivity.class);
		in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		in.putExtra("toLastAc", true);
		ac.startActivity(in);
	}

	public void refreshway() {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingDetailByEntryEquipmentAndResouces");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_id", mainbean.resources_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				CheckInTaskBeanMain bean = new CheckInTaskBeanMain();
				CheckInTaskBeanMain.handJson(json, bean);
				if (!Utility.isNullForList(mainbean.complexDoorList)) {
					mainbean.complexDoorList.clear();
				}
				mainbean.complexDoorList = bean.complexDoorList;
				mainbean.in_seal = bean.in_seal;
				mainbean.out_seal = bean.out_seal;
				mainbean.load_barsList = bean.load_barsList;
				mainbean.load_UseList = bean.load_UseList;
				setData(0);
			}
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	private void setData(int type) {
		if (type == 0) {
			mAdapter = new CheckInTaskItemlistAdapter(mActivity, this, mainbean.complexDoorList);
			listView.setAdapter(mAdapter);
		} else {
			mAdapter.notifyDataSetChanged();
		}

		setCheckboxStatus(Status.UNCHECKED);
		type = 0;
		// 隐藏-下方"全部处理"-按钮
		tsc_linear.setVisibility(View.GONE);

		// debug
		loadbar_layout.initData(mainbean);

		if (mainbean.complexDoorList == null) {
			UIHelper.showToast(this, "No Task!");
			return;
		}
		setviewdate(mainbean);
	}

	public void listItemIDnumber() {
		listItemID.clear();
		submittasklist.clear();
		for (int i = 0; i < mainbean.complexDoorList.size(); i++) {
			if (mainbean.complexDoorList.get(i).checked && mainbean.complexDoorList.get(i).number_status != EntryDetailNumberStateKey.Close) {
				listItemID.add(i);
			}
		}
	}

	public void morepackinglist(int k) {
		// TODO Auto-generated method stub
		listItemIDnumber();
		StringBuilder sbC = new StringBuilder();
		StringBuilder sbS = new StringBuilder();
		StringBuilder sbtask = new StringBuilder();
		StringBuilder sbstart = new StringBuilder();
		StringBuilder sbclose = new StringBuilder();
		String retStrC = null;
		String retStrS = null;
		String retStrtask, retStrstart, retStrclose;
		// if (listItemID.size() == 0) {
		// timedata("No records selected!");
		// } else {
		for (int i = 0; i < listItemID.size(); i++) {
			if (mainbean.complexDoorList.get(listItemID.get(i)).is_forget_task == 0) {
				if (mainbean.complexDoorList.get(listItemID.get(i)).takeoverflag == 1) {
					sbS.append(mainbean.complexDoorList.get(listItemID.get(i).intValue()).dlo_detail_id + ",");
					sbtask.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
							+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");
				}
				if (mainbean.complexDoorList.get(listItemID.get(i)).number_status == 1
						&& mainbean.complexDoorList.get(listItemID.get(i)).takeoverflag != 1) {
					submittasklist.add(mainbean.complexDoorList.get(listItemID.get(i)));
					sbstart.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
							+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");
				}
				if (mainbean.complexDoorList.get(listItemID.get(i)).number_status == 2//-------
						&& mainbean.complexDoorList.get(listItemID.get(i)).takeoverflag != 1) {


					// modify by xia 除了Pickup Delivery 其他所有任务都不应该在外面close
					CheckInTaskItemBeanMain currCheckedBean = mainbean.complexDoorList.get(listItemID.get(i));
					if ( currCheckedBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS ||
							currCheckedBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS ) {

						//---old code start
						sbC.append(mainbean.complexDoorList.get(listItemID.get(i).intValue()).dlo_detail_id + ",");
						sbclose.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
								+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");
						//---old code end
					}


				}
			} else {
				if (mainbean.complexDoorList.get(listItemID.get(i)).number_status == 1) {
					submittasklist.add(mainbean.complexDoorList.get(listItemID.get(i)));
					sbstart.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
							+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");
				}
				if (mainbean.complexDoorList.get(listItemID.get(i)).number_status == 2
						&& mainbean.complexDoorList.get(listItemID.get(i)).number_type != EntryDetailNumberTypeKey.CHECK_IN_LOAD
						&& mainbean.complexDoorList.get(listItemID.get(i)).number_type != EntryDetailNumberTypeKey.CHECK_IN_ORDER
						&& mainbean.complexDoorList.get(listItemID.get(i)).number_type != EntryDetailNumberTypeKey.CHECK_IN_PONO) {

					/*sbC.append(mainbean.complexDoorList.get(listItemID.get(i).intValue()).dlo_detail_id + ",");
					sbclose.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
							+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");*/
					// modify by xia 除了Pickup Delivery 其他所有任务都不应该在外面close
					CheckInTaskItemBeanMain currCheckedBean = mainbean.complexDoorList.get(listItemID.get(i));
					if ( currCheckedBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS ||
							currCheckedBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS ) {

						//---old code start
						sbC.append(mainbean.complexDoorList.get(listItemID.get(i).intValue()).dlo_detail_id + ",");
						sbclose.append(EntryDetailNumberTypeKey.getModuleName(mainbean.complexDoorList.get(listItemID.get(i)).number_type) + ":"
								+ mainbean.complexDoorList.get(listItemID.get(i)).number + "\n");
						//---old code end
					}
				}
			}

		}
		retStrC = sbC.toString();
		retStrS = sbS.toString();
		retStrtask = sbtask.toString();
		retStrstart = sbstart.toString();
		retStrclose = sbclose.toString();
		// 移除-最后的","
		if (!TextUtils.isEmpty(retStrC)) {
			retStrC = retStrC.substring(0, retStrC.length() - 1);
		}
		if (!TextUtils.isEmpty(retStrS)) {
			retStrS = retStrS.substring(0, retStrS.length() - 1);
		}
		if (!TextUtils.isEmpty(retStrtask)) {
			retStrtask = retStrtask.substring(0, retStrtask.length() - 1);
		}
		if (!TextUtils.isEmpty(retStrstart)) {
			retStrstart = retStrstart.substring(0, retStrstart.length() - 1);
		}
		if (!TextUtils.isEmpty(retStrclose)) {
			retStrclose = retStrclose.substring(0, retStrclose.length() - 1);
		}
		// }
		if (k == 1) {
			closebuilder(retStrC, 3, retStrclose);
		}
		if (k == 2) {
			taskoverbuilder(retStrS, retStrtask);
		}
		if (k == 3) {
			startbuilder(retStrstart);
		}
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		// CheckInTaskDoorItemActivity.this,CheckInTaskMainActivity.class
		Intent intent = new Intent();
		setResult(CheckInTaskMainActivity.ITEMCLOSE, intent);
		CheckInTaskDoorItemActivity.super.onBackBtnOrKey();
	}

	private boolean isReturnFromCam = false;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (resultCode) {
		case SelectDoorActivity.SELECTDOOR: // 改门时
			IntentSelectDoorBean bean = (IntentSelectDoorBean) data.getSerializableExtra("intentSelectDoorBean");
			mainbean.resources_id = bean.resources_id;
			mainbean.resources_name = bean.resources_type_value;
			mainbean.resources_type = bean.resources_type;
			mainbean.resources_type_value = OccupyTypeKey.getOccupyTypeKeyName(mActivity, mainbean.resources_type);
			small_door.setText(mainbean.resources_type_value + " : " + mainbean.resources_name + " ");
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
		isReturnFromCam = ttp.isFromTTP(requestCode);

		if (requestCode == PHOTONUMBER) {
			number = 1;
		}
	}

	@Override
	public void selectItem(CheckInTaskItemBeanMain item, int position) {
		// TODO Auto-generated method stub
//		if (mAdapter.mChecked.get(position).booleanValue()) {
//			mAdapter.mChecked.set(position, false);
//		} else {
//			mAdapter.mChecked.set(position, true);
//		}

		mainbean.complexDoorList.get(position).checked = !mainbean.complexDoorList.get(position).checked;

		morepackinglist(0);
		//if (listItemID.size() > 0) {

			int allValidTaskSize = getValidTaskCnt(mainbean.complexDoorList);
		if (listItemID.size() == allValidTaskSize) {
				tsc_linear.setVisibility(View.VISIBLE);

			type = 1;

			// 全选
			setCheckboxStatus(Status.CHECKED);
		} else if (listItemID.size() > 0 && listItemID.size() < allValidTaskSize) {
			tsc_linear.setVisibility(View.VISIBLE);
			type = 0;

			// 半选
			setCheckboxStatus(Status.CHECKEDHALF);

		} else {
			tsc_linear.setVisibility(View.GONE);

			type = 0;

			// 未选
			setCheckboxStatus(Status.UNCHECKED);

		}
		mAdapter.notifyDataSetChanged();


		showBottomBtn();
	}

	/**
	 * 获得所有未Close的任务
	 * @param complexDoorList
	 * @return
	 */
	private int getValidTaskCnt(List<CheckInTaskItemBeanMain> complexDoorList) {
		int cnt = 0;
		for (CheckInTaskItemBeanMain bean : complexDoorList) {
			if (bean.number_status != EntryDetailNumberStateKey.Close) {
				cnt ++;
			}
		}
		return cnt;
	}


	public void url(CheckInTaskItemBeanMain item) {
		JSONArray jSONArray = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("dlo_detail_id", item.dlo_detail_id + "");
			jsonObject.put("number", item.number + "");
			if (item.number_status == 1) {
				jSONArray.put(jsonObject);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		submitStart(jSONArray);
	}

	public void submitStart(JSONArray jSONArray) {
		if (jSONArray != null && jSONArray.length() > 0) {
			RequestParams params = new RequestParams();
			params.add("Method", "TaskProcessingStartDetails");
			params.add("entry_id", mainbean.entry_id + "");
			params.add("equipment_id", mainbean.equipment_id + "");
			params.add("resources_id", mainbean.resources_id + "");
			params.add("resources_type", mainbean.resources_type + "");
			params.add("values", jSONArray + "");
			// 改成LoadJson 数据的方式
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					if (!Utility.isNullForList(mainbean.complexDoorList)) {
						mainbean.complexDoorList.clear();
					}
					CheckInTaskItemBeanMain.parseBean2(json, mainbean.complexDoorList);
					setData(1);
				}
			}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
		}
	}

	public void timedata(final String con) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		Resources resources = mActivity.getResources();
		builder.setCancelable(false);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		builder.setMessage(con);
		builder.setNegativeButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		builder.create().show();
	}

	// 点"Operate"时
	@Override
	public void selectItem(final CheckInTaskItemBeanMain item) {
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(mActivity);

		final View layout = LayoutInflater.from(this).inflate(R.layout.dock_close_item_dialog_second, null);
		LinearLayout load_linear = (LinearLayout) layout.findViewById(R.id.load_linear);
		LinearLayout ctnr_linear = (LinearLayout) layout.findViewById(R.id.ctnr_linear);
		LinearLayout partially_linear = (LinearLayout) layout.findViewById(R.id.partially_linear);
		LinearLayout close_linear = (LinearLayout) layout.findViewById(R.id.close_linear);
		LinearLayout exception_linear = (LinearLayout) layout.findViewById(R.id.exception_linear);
		LinearLayout dialog_exception = (LinearLayout) layout.findViewById(R.id.dialog_exception);

		// 设备未checkout
		if (item.is_forget_task == 0) {
			if (item.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS) {
				load_linear.setVisibility(View.VISIBLE);
			}
			if ((item.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS)) {
				ctnr_linear.setVisibility(View.VISIBLE);
			}
		}
		// 已checkout
		if (item.is_forget_task == 1) {
			load_linear.setVisibility(View.GONE);
			ctnr_linear.setVisibility(View.GONE);
			exception_linear.setVisibility(View.GONE);
		}
		// 有单据-load
		if (item.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD || item.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER
				|| item.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO   || item.isSmallParcel()) {
			load_linear.setVisibility(View.VISIBLE);
			// partially_linear.setVisibility(View.VISIBLE);
			dialog_exception.setVisibility(View.GONE);
			close_linear.setVisibility(View.GONE);
			exception_linear.setVisibility(View.GONE);
		}
		//receive
		if (   item.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL
				|| item.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN  || item.isSmallParcel()) {
			ctnr_linear.setVisibility(View.VISIBLE);
			// partially_linear.setVisibility(View.VISIBLE);
			dialog_exception.setVisibility(View.GONE);
			close_linear.setVisibility(View.GONE);
			exception_linear.setVisibility(View.GONE);
		}

		// 有单据receive,即使forget 也都支持(receive/normalClose/exceptionClose)
		/*if (item.isWmsReceive()) {
			ctnr_linear.setVisibility(View.VISIBLE);
			dialog_exception.setVisibility(View.VISIBLE);
			close_linear.setVisibility(View.VISIBLE);
			exception_linear.setVisibility(View.VISIBLE);
		}*/

		boolean showReceiptNO = item.isWmsReceive()
				&& !TextUtils.isEmpty(item.receipt_no);
		if(showReceiptNO && StoredData.getPs_id().equals("1000005")){
			dialog_exception.setVisibility(View.GONE);
			close_linear.setVisibility(View.GONE);
			exception_linear.setVisibility(View.GONE);
		}/*else{
			dialog_exception.setVisibility(View.VISIBLE);
			close_linear.setVisibility(View.VISIBLE);
			exception_linear.setVisibility(View.VISIBLE);
		}*/
		/****************************** TMS的收发货 ************************************/
		/***/
		if (item.isTmsLoad() || item.isTmsAnNengLoad()) {
			/***/
			load_linear.setVisibility(View.VISIBLE);
			/***/
			dialog_exception.setVisibility(View.GONE);
			/***/
			close_linear.setVisibility(View.GONE);
			/***/
			exception_linear.setVisibility(View.GONE);
			/***/
		}
		/***/
		if (item.isTmsReceive() || item.isTmsAnNengReceive()) {
			/***/
			ctnr_linear.setVisibility(View.VISIBLE);
			/***/
			dialog_exception.setVisibility(View.GONE);
			/***/
			close_linear.setVisibility(View.GONE);
			/***/
			exception_linear.setVisibility(View.GONE);
			/***/
		}
		/**************************************************************************/

		b.setContentView(layout);

		// operate菜单-事件
		OnClickListener mItemOnClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Utility.isFastClick())
					return;
				// 若未start
				if (item.number_status == 1) {
					dialog.dismiss();
					timedata("Please Start The Task!");
					return;
				}

				// if (item.number_status != 1) {
				switch (v.getId()) {
				case R.id.close_linear: // normal close
					leavebuilder(item.dlo_detail_id, 3, EntryDetailNumberTypeKey.getModuleName(item.number_type) + " " + item.number);
					break;
				case R.id.exception_linear: // exception
					jumpToExcetionAc(item, ScanLoadActivity.CloseType_Exception);
					break;
				case R.id.partially_linear: // partially
					jumpToExcetionAc(item, ScanLoadActivity.CloseType_Partially);
					break;
				case R.id.load_linear: // 装货
					// load
					CheckInLoadComplexDoor d = new CheckInLoadComplexDoor();
					d.entryID = mainbean.entry_id;
					d.resources_name = mainbean.resources_name + "";
					d.out_seal = mainbean.out_seal;

					CheckInLoadLoadBean load = item.toLoadBean();
					load.equipment_id = mainbean.equipment_id + "";
					load.resources_id = mainbean.resources_id + "";
					load.resources_type = mainbean.resources_type + "";
					// 三星load
					if (load.isSamsung()) {
						Intent intent1 = new Intent(mActivity, SamsungLoadActivity.class);
						intent1.putExtra("loadBean", (Serializable) load);
						intent1.putExtra("entryID", mainbean.entry_id);
						startActivityForResult(intent1, CheckInTaskDoorItemActivity.LOAD);
					}
					// // --------TMS的load
					else if (item.isTmsLoad()) {
						TMS_Load(item);
					}

//					// --------TMS的新版装货
//					else if (item.isTmsAnNengLoad()) {
//						TMS_AnNeng_Load(mainbean, item);
//					}

					// debug,smallParcel
					else if (item.isSmallParcel()) {
						reqCarrers_smallParcel(mainbean, item);
					}
					// 自己的load
					else {
						// 无单据load
						if (item.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS) {
							LoadNListAc.toThis(mActivity, mainbean, item);// 李君浩注释
						}
						// 有单据load
						else {
							load.entry_id = mainbean.entry_id;
							load.door = mainbean.resources_name + "";
							getCompList(mActivity, load, d, mainbean.equipment_id + "", mainbean, item);
						}
					}
					break;

				case R.id.ctnr_linear: // receive
					// 三星receive
					if (Utility.parseLong(item.ic_id) > 0) {
						Ctnr ctnr = item.toCtnrBean(mActivity);
						ctnr.setEquipment_id(mainbean.equipment_id + "");
						ctnr.setResources_id(mainbean.resources_id + "");
						ctnr.setResources_type(mainbean.resources_type + "");

						Intent intent = new Intent(mActivity, ReceiveDetailActivity.class);
						intent.putExtra("entry_id", mainbean.entry_id);
						intent.putExtra("ctnrs", ctnr);
						startActivity(intent);
					}
//					// 调试时 才能进,TMS收货
//					else if (item.isTmsReceive()) {
//						TMS_Receive(item);
//					}
//					// 调试时 才能进,TMS针对安能收货
//					else if (item.isTmsAnNengReceive()) {
//						TMS_AnNeng_Receive(mainbean, item);
//					}

					// 调试时 才能进,新版收货
					else if (item.isWmsReceive()) {
						reqWmsReceive(item.company_id, item.receipt_no, item.dlo_detail_id, item.osoNote);
					}
					// 无单据receive
					else {
						ReceiveOsoAc.toThis(mActivity, mainbean, item, true);// 李君浩注释
					}

					break;
				default:
					break;
				}
				dialog.dismiss();

				// } else {
				// dialog.dismiss();
				// timedata("Please Start The Task!");
				// }
			}
		};
		close_linear.setOnClickListener(mItemOnClick);
		exception_linear.setOnClickListener(mItemOnClick);
		partially_linear.setOnClickListener(mItemOnClick);
		load_linear.setOnClickListener(mItemOnClick);
		ctnr_linear.setOnClickListener(mItemOnClick);

		b.setContentView(layout);
		b.setTitle(getString(R.string.sync_operation));
		b.hideCancelBtn();
		b.setCanceledOnTouchOutside(true);
		dialog = b.create();
		// dialog.setCanceledOnTouchOutside(true);
		dialog.show();

		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = (int) (display.getWidth()); // 设置宽度
		dialog.getWindow().setAttributes(lp);

	}

	private void reqCarrers_smallParcel(final CheckInTaskBeanMain doorBean, final CheckInTaskItemBeanMain taskBean) {
		RequestParams p = new RequestParams();
		p.add("method", "getSmallParcelCarrier");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONArray ja = json.optJSONArray("data");
				List<CarrierBean> list = new Gson().fromJson(ja.toString(), new TypeToken<List<CarrierBean>>() {
				}.getType());
				if (Utility.isEmpty(list)) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No Carrier!");
					return;
				}

				Intent in = new Intent(mActivity, SmallParcelAc.class);
				SmallParcelAc.initParams(in, list, doorBean, taskBean);
				startActivity(in);
			}
		}.doGet(HttpUrlPath.androidSmallParcel, p, this);
	}

	private void reqWmsReceive(String companyID, final String recNo, final String detailID, final String osoNote) {

		NetConnection_YMS.getThis().reqAcquireRNLineInfo(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public void handReponseJson(JSONObject json) {

				JSONObject ja = json.optJSONObject("datas");
				if (ja == null) {
					RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "No Records!");
					return;
				}
				Rec_ReceiveBean bean = Rec_ReceiveBean.handJsonForBean(json);
				Intent in = new Intent(mActivity, Receive_ItemListAc.class);
				// 临时-数据
				bean.receipt_no = recNo;
				bean.dlo_detail_id = detailID;
				bean.doorBean = mainbean;
				bean.osoNote = osoNote;
				Receive_ItemListAc.initParams(in, bean);
				startActivity(in);// 李君浩注释
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json != null) { // 没有商品的情况
					try {
						JSONArray ja = json.getJSONArray("products");
						final List<ProductBean> products = new Gson().fromJson(ja.toString(), new TypeToken<List<ProductBean>>() {
						}.getType());

						// 提示没有发现商品，是否创建
						RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.product_not_found_create_new),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										reqGetContextData(products);
									}
								});
					} catch (JSONException e) {
						//TODO 没有 products 数据
						e.printStackTrace();
					}

				}
			}
		}, companyID, recNo, detailID, osoNote);
	}

	private void reqGetContextData(final List<ProductBean> products) {
		// 获取Context数据
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
					return;
				}
				ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
				}.getType());
				Intent intent = new Intent(mActivity, SelectItemsActivity.class);
				intent.putExtra("products", (Serializable) products);
				intent.putExtra("ContextData", data);
				startActivityForResult(intent, 0);
			}
		}.doGet(HttpUrlPath.product_context, new RequestParams(), mActivity);

	}

	/**
	 * 跳至-exception/partially
	 *
	 * @param item
	 */
	private void jumpToExcetionAc(CheckInTaskItemBeanMain item, int closeType) {
		// Intent intent = new
		// Intent(mActivity,CheckInTaskExceptionActivity.class);
		// intent.putExtra("entry_id", mainbean.entry_id + "");
		// intent.putExtra("detail_id", item.dlo_detail_id + "");
		// intent.putExtra("title",EntryDetailNumberTypeKey.getModuleName(item.number_type)+
		// ": "+ item.number + "");
		// intent.putExtra("taskmainlist", (Serializable) mainbean);
		// intent.putExtra("closeType",closeType);
		// startActivityForResult(intent, EXCEPTIONNUM);
		// startActivity(intent);
		Intent in = new Intent(this, CheckInTaskExceptionActivity.class);
		CheckInTaskExceptionActivity.initParams(in, mainbean, item, closeType);
		startActivity(in);
	}

	// "normal close"确认
	private void leavebuilder(final String detail_id, final int number_status, String title) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		// builder.setTitle(resources.getString(R.string.check_in_all_news));
		builder.setMessage(EntryDetailNumberStateKey.getType(mActivity, number_status) + " " + title + " ?");
		builder.setCancelable(false);
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				closeway(detail_id, number_status);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), null);
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private void builder() {
		AlertUi.showAlertBuilder(mActivity, "The date does not meet the requirements.");
	}

	// 多项close
	private void closebuilder(final String detail_id, final int number_status, String title) {
		if (title.length() > 0 && title != null) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(getString(R.string.sync_close_question));
			builder.setMessage(title);
			builder.setCancelable(false);
			builder.setPositiveButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					closeway(detail_id, number_status);
				}
			});
			builder.setNegativeButton(resources.getString(R.string.sync_no), null);
			Dialog dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		} else {
			builder();
		}
	}

	// 多项TakeOver
	private void taskoverbuilder(final String detail_id, String title) {
		if (title.length() > 0 && title != null) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle("Take Over");
			builder.setMessage(title);
			builder.setCancelable(false);
			builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					taskover(detail_id);
				}
			});
			builder.setNegativeButton(getString(R.string.sync_no), null);
			Dialog dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		} else {
			builder();
		}
	}

	// 多项start
	private void startbuilder(String title) {
		if (title.length() > 0 && title != null) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle("Start");
			builder.setMessage(title);
			builder.setCancelable(false);
			builder.setPositiveButton(resources.getString(R.string.check_in_yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					submitStart();
				}
			});
			builder.setNegativeButton(resources.getString(R.string.check_in_no), null);
			Dialog dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		} else {
			builder();
		}
	}

	// close-请求
	public void closeway(String detail_id, int number_status) {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingCloseDetail");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("detail_id", detail_id + "");
		params.add("number_status", number_status + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("resources_type", mainbean.resources_type + "");
		params.add("resources_id", mainbean.resources_id + "");
		ttp.uploadZip(params, "TaskClose");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				ttp.clearData(true);
				if (!Utility.isNullForList(mainbean.complexDoorList)) {
					mainbean.complexDoorList.clear();
				}
				CheckInTaskItemBeanMain.parseBean2(json, mainbean.complexDoorList);
				setData(1);

				int returnflag = json.optInt("returnflag");
				// 若为门下最后1个(1/2/4跳,3不跳),直接至finish页
				if (returnflag != CheckInTaskFinishActivity.LoadCloseNoifyNormal)
					submitfinish();
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	@Override
	public void all_btn(CheckInTaskItemBeanMain item, int btn_type) {
		if (btn_type == 1) { // takeOver
			taskover(item.dlo_detail_id);
		}
		if (btn_type == 2) { // start
			url(item);
		}
		if (btn_type == 3) { // operation
			selectItem(item);
		}
	}

	public void taskover(String item) {

		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingTakeOver");
		params.add("entry_id", mainbean.entry_id + "");
		params.add("detail_id", item + "");
		params.add("equipment_id", mainbean.equipment_id + "");
		params.add("equipment_type", mainbean.equipment_type + "");
		params.add("resources_type", mainbean.resources_type + "");
		params.add("resources_id", mainbean.resources_id + "");

		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				if (!Utility.isNullForList(mainbean.complexDoorList)) {
					mainbean.complexDoorList.clear();
				}
				CheckInTaskItemBeanMain.parseBean2(json, mainbean.complexDoorList);
				setData(1);
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	public List<ChickMoreInfo> makeMoreButtonData() {
		List<ChickMoreInfo> chickMoreInfoList = new ArrayList<ChickMoreInfo>();

		chickMoreInfoList.add(new ChickMoreInfo(getString(R.string.sync_help), -1, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (helpnumber != 0) {
					RightButtonForMenu.dismissPopupWindow(moreButton);
					showDlg_help();
					moreButton.dismiss();
				} else {
					UIHelper.showToast(mActivity, getString(R.string.sync_help_taskclose), Toast.LENGTH_SHORT).show();
					moreButton.dismiss();
				}

			}
		}));

		// debug
		// if (mainbean.canChangeDoor() && helpnumber != 0) {
		// chickMoreInfoList.add(new ChickMoreInfo("Change Door", -1,
		// new View.OnClickListener() {
		// public void onClick(View v) {
		// if(helpnumber != 0){
		// RightButtonForMenu.dismissPopupWindow(moreButton);
		// getDoorList();
		// }else{
		// UIHelper.showToast(mActivity, "All The Tasks Had Closed",
		// Toast.LENGTH_SHORT).show();
		// moreButton.dismiss();
		// }
		// };
		// }));
		// }
		return chickMoreInfoList;
	}

	private void showDlg_help() {
		final boolean toRequest = !isRequestHelp;

		String msg = toRequest ? getString(R.string.sync_help_request) : getString(R.string.sync_help_finishrequest);
		RewriteBuilderDialog.showSimpleDialog(this, msg, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				RequestParams params = new RequestParams();
				params.add("Method", "NeedHelp");
				params.add("dlo_detail_id", (globaldetail1 != null && globaldetail1.length() > 0) ? globaldetail1 : globaldetail2);
				params.add("flag_help", toRequest ? "1" : "0");

				new SimpleJSONUtil() {

					@Override
					public void handReponseJson(JSONObject json) {
						// TODO Auto-generated method stub
						isRequestHelp = toRequest;
						UIHelper.showToast(mActivity, getString(R.string.sync_success));
					}

					@Override
					public void handFail() {
					}
				}.doGet(HttpUrlPath.LoadPaperWorkAction, params, mActivity);
			}
		});
	}

	private void getDoorList() {
		if (Utility.isNullForList(resourceList)) {
			RequestParams params = new RequestParams();
			params.add("Method", HttpPostMethod.GetDoorAndSpot);
			params.add("request_type", "2"); // 1:可用res 2:所有
			params.add("occupy_type", OccupyTypeKey.DOOR + "");
			params.add("entry_id", mainbean.entry_id);
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					if (!Utility.isNullForList(resourceList)) {
						resourceList.clear();
					}
					resourceList = DockHelpUtil.handjson_big(json);
					jumpToSelectDoor();
				}
			}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, mActivity);
		} else {
			jumpToSelectDoor();
		}
	}

	private void jumpToSelectDoor() {
		Intent intent = new Intent(mActivity, CheckInTaskSelectDoorActivity.class);

		IntentSelectDoorBean bean = new IntentSelectDoorBean();
		bean.resources_id = mainbean.resources_id;
		bean.resources_type = mainbean.resources_type;
		bean.resources_type_value = mainbean.resources_name + "";

		bean.equipment_id = mainbean.equipment_id;
		bean.equipment_number = mainbean.equipment_number;
		bean.equipment_type = mainbean.equipment_type;
		bean.equipment_type_value = mainbean.equipment_type_value;
		bean.check_in_entry_id = mainbean.entry_id;

		bean.intentType = SelectDoorActivity.CheckInTaskDoorItemActivityFlag;

		intent.putExtra("intentSelectDoorBean", (Serializable) bean);
		intent.putExtra("resourceList", (Serializable) resourceList);
		startActivityForResult(intent, SelectDoorActivity.SELECTDOOR);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	@Override
	public void addLoadBatData(int load_bar_id, String total,boolean isChange) {
	}

	// ==================web===============================

	private void getCompList(final Activity ac, final CheckInLoadLoadBean loadBean, final CheckInLoadComplexDoor doorBean, final String equipment_id,
			final CheckInTaskBeanMain doorBean_x, final CheckInTaskItemBeanMain taskBean) {
		RequestParams params = new RequestParams();
		params.add("Method", "SearchOrderPalletByLoadNo");
		params.add("company_id", loadBean.company_id);
		params.add("customer_id", loadBean.customer_id);
		params.add("dlo_detail_id", loadBean.dlo_detail_id);
		// params.add("load_no", loadBean.loadnumber);
		// load取load、po/order取order
		boolean isLoad = loadBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD;
		params.add("order_type", (isLoad ? TYPE_LOAD : TYPE_ORDER) + "");
		// load或order
		params.add("load_no", loadBean.getMainNo());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				JSONObject jaData = json.optJSONObject("datas");
				if (jaData == null) {
					UIHelper.showToast(getString(R.string.sync_data_error));
					return;
				}

				// debug
				// boolean needScan=true; //有未扫的sn
				// needScan=jaData.optInt("is_all_picked")==2; //1:都扫了sn 2:有未扫的
				//
				// //若sn未扫
				// if(needScan){
				// tip_loadNotScanSN(parseNoScanOrders(jaData.optJSONArray("noscanorders")));
				// return;
				// }
				toNextAc_as1load(ac, json, doorBean, loadBean, equipment_id, doorBean_x, taskBean);
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.LoadPaperWorkAction, params, ac);
	}

	// private List<String> parseNoScanOrders(JSONArray jaOrders){
	// if(Utility.isEmpty(jaOrders))
	// return null;
	// List<String> list=new ArrayList<String>();
	// for (int i = 0; i < jaOrders.length(); i++) {
	// list.add("Order: "+jaOrders.optJSONObject(i).optString("ORDERNO"));
	// }
	// return list;
	// }

	/**
	 * 提示有没扫sn的order
	 */
	private void tip_loadNotScanSN(List<String> listOrders) {
		// db数据可能有问题,无数据仍提示
		if (listOrders == null)
			listOrders = new ArrayList<String>();

		// dlg
		RewriteBuilderDialog.Builder bd = new RewriteBuilderDialog.Builder(this);
		bd.setTitle("No Scanning");
		bd.setItems(listOrders.toArray(new String[] {}), null);
		bd.setNegativeButton(getString(R.string.sync_yes), null);
		bd.show();
	}

	// 仅一条load时的跳转
	private void toNextAc_as1load(Activity ac, JSONObject joDatas, CheckInLoadComplexDoor doorBean, CheckInLoadLoadBean loadBean,
			String equipment_id, CheckInTaskBeanMain taskBean, CheckInTaskItemBeanMain taskBean_x) {
		CheckInLoad_ComplexSubLoads complexSubLoads = new CheckInLoad_ComplexSubLoads();
		CheckInLoad_ComplexSubLoads.parseBean(joDatas, complexSubLoads);

		// palletTypes
		JSONArray jaPalletTypes = joDatas.optJSONArray("pallettypes");
		if (jaPalletTypes != null && jaPalletTypes.length() > 0) {
			complexSubLoads.listPalletTypes = new ArrayList<CheckInLoadPalletTypeBean>();
			CheckInLoadPalletTypeBean.parseBeans(jaPalletTypes, complexSubLoads.listPalletTypes);
		}

		// 添加-额外数据
		if (doorBean != null) {
			complexSubLoads.entry_id = doorBean.entryID;
			// complexSubLoads.door_name = doorBean.resources_name;
			complexSubLoads.out_seal = doorBean.out_seal;
		}
		if (loadBean != null) {
			complexSubLoads.number_type = loadBean.number_type;
			complexSubLoads.load = loadBean;
		}
		complexSubLoads.doorBean = taskBean;
		complexSubLoads.taskBean = taskBean_x;
		complexSubLoads.equipment_id = equipment_id;
		complexSubLoads.isFromLoad = true;

		if (complexSubLoads.listSubLoads == null) {
			UIHelper.showToast(ac, "No Records");
			return;
		}

		Intent in = null;
		// 仅1条,直接至"装载"
		if (complexSubLoads.listSubLoads.size() == 1) {
			CheckInLoad_SubLoadBean mbol = complexSubLoads.listSubLoads.get(0);
			// 有未扫的sn
			if (mbol.hasNOScanedSNs) {
				tip_loadNotScanSN(mbol.noscanorders);
				return;
			}

			in = new Intent(ac, ScanLoadActivity.class);
			complexSubLoads.curSubload = 0;
			ScanLoadActivity.initParams(in, complexSubLoads);
		}
		// 至-选"子单据"
		else {
			in = new Intent(ac, SelectSubLoadActivity.class);
			SelectSubLoadActivity.initParams(complexSubLoads);
		}
		ac.startActivity(in);
	}

	@Override
	public void DetailByEntry(CheckInTaskBeanMain bean) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setnewnumberstates(int i) {
		mainbean.complexDoorList.get(i).number_status = 2;
		mainbean.complexDoorList.get(i).takeoverflag = 0;
		setviewdate(mainbean);
	}

	public void setviewdate(CheckInTaskBeanMain main) {

		int j = 0;
		for (CheckInTaskItemBeanMain item : main.complexDoorList) {
			if (item.number_status != 3 && item.number_status != 4 && item.number_status != 5) {
				j++;
			}
		}
		helpnumber = j;
		// 全部-已操作,j:未操作数
		if (j == 0) {
			if ((main.equipment_status == CheckInMainDocumentsStatusTypeKey.UNPROCESS || main.equipment_status == CheckInMainDocumentsStatusTypeKey.PROCESSING)
					&& main.showfinish == 1) {
				finish_linear.setVisibility(View.VISIBLE);
			}
			all_checkbox.setVisibility(View.INVISIBLE);
		} else {
			finish_linear.setVisibility(View.GONE);
			all_checkbox.setVisibility(View.VISIBLE);
		}

		int i = 0;
		for (CheckInTaskItemBeanMain item : main.complexDoorList) {
			if (item.takeoverflag == 1 && item.is_forget_task == 0) {
				i++;
				globaldetail2 = item.dlo_detail_id;
			}
		}
		// i:可takeover数
		if (i == 0) {
			takeover_btn.setVisibility(View.GONE);
		} else {
			takeover_btn.setVisibility(View.VISIBLE);
		}

		int k = 0;
		for (CheckInTaskItemBeanMain item : main.complexDoorList) {
			if (item.number_status == 1 && item.is_forget_task == 0 && item.takeoverflag == 0) {
				k++;
				globaldetail2 = item.dlo_detail_id;
			}
		}
		// k:可start数
		if (k == 0) {
			start_btn.setVisibility(View.GONE);
		} else {
			start_btn.setVisibility(View.VISIBLE);
		}

		int p = 0;
		for (CheckInTaskItemBeanMain item : main.complexDoorList) {
			if (item.takeoverflag == 0 && item.is_forget_task == 0) {
				if (item.number_status == 2 && item.number_type != EntryDetailNumberTypeKey.CHECK_IN_LOAD
						&& item.number_type != EntryDetailNumberTypeKey.CHECK_IN_ORDER && item.number_type != EntryDetailNumberTypeKey.CHECK_IN_PONO) {
					p++;
					globaldetail1 = item.dlo_detail_id;
				}
			} else if (item.is_forget_task == 1) {
				if (item.number_status == 2 && item.number_type != EntryDetailNumberTypeKey.CHECK_IN_LOAD
						&& item.number_type != EntryDetailNumberTypeKey.CHECK_IN_ORDER && item.number_type != EntryDetailNumberTypeKey.CHECK_IN_PONO) {
					p++;
					globaldetail1 = item.dlo_detail_id;
				}
			}
		}
		// p:可close数
		if (p == 0) {
			close_btn.setVisibility(View.GONE);
		} else {
			// 注释掉  xia
			//close_btn.setVisibility(View.VISIBLE);
		}
	}

	// -----------------TMS----load receive---------
	private void TMS_Load(final CheckInTaskItemBeanMain item) {
		// if(AppConfig.Cycle_Count_Task){
		// JSONObject json = TMS_Load_InformationBean.getDebugStr();
		// TMS_allloaddata loaddata = TMS_allloaddata.getdata(json);
		// loaddata.loadpalletlist = TMS_Load_InformationBean.helpJson1(json,
		// item);
		// loaddata.loadtnlist= TMS_Load_InformationBean.tnhelpJson1(json,
		// item);
		// Intent i = new Intent(mActivity, TmsScanLoadmainActivity.class);
		// loaddata.entryID = mainbean.entry_id+"";
		// loaddata.dlo_detail_id = item.dlo_detail_id;
		// loaddata.door_name = mainbean.resources_name+"";
		// loaddata.bol_id = item.number+"";
		// i.putExtra("allload", (Serializable) loaddata);
		// startActivity(i);
		// }

		RequestParams params = new RequestParams();
		params.put("bol_id", item.number);
		params.put("entry_id", mainbean.entry_id + "");
		params.put("door_id", mainbean.resources_name + "");
		params.put("adid", StoredData.getAdid() + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// TMS_allloaddata loaddata = TMS_allloaddata.getdata(json);
				// loaddata.loadpalletlist = TMS_Load_InformationBean.helpJson(
				// json, item);
				// loaddata.loadtnlist =
				// TMS_Load_InformationBean.tnhelpJson(json,
				// item);
				// Intent i = new Intent(mActivity,
				// TmsScanLoadmainActivity.class);
				// loaddata.entryID = mainbean.entry_id + "";
				// loaddata.dlo_detail_id = item.dlo_detail_id;
				// loaddata.door_name = mainbean.resources_name + "";
				// loaddata.bol_id = item.number + "";
				// i.putExtra("allload", (Serializable) loaddata);
				// startActivity(i);李君浩注释

			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.GetLoadingTicket1, params, mActivity);
	}

//	private void TMS_AnNeng_Load(final CheckInTaskBeanMain doorBean, final CheckInTaskItemBeanMain item) {
//		RequestParams params = new RequestParams();
//		params.put("ps_id", StoredData.getPs_id());
//		// params.put("entry_id", mainbean.entry_id+"");
//		// params.put("door_id", mainbean.resources_name+"");
//		// params.put("adid", StoredData.getAdid()+"");
//		params.put("entryId", mainbean.entry_id);
//		params.put("loading_id", item.number);
//		params.put("language_type", StoredData.readLanguageID() + "");
//		new SimpleJSONUtil() {
//
//			@Override
//			public void handReponseJson(JSONObject json) {
//				// TODO Auto-generated method stub
//				Log.e("mylog", "json==========" + json);
//				JSONArray ja = json.optJSONArray("tms_waybill_list");
//				if (StringUtil.getJsonString(json, "err").equals("90")) {
//
//					TTS.getInstance().speakAll_withToast(json.optString("data").toString(), true);
//					return;
//				}
//				List<Tms_OrderBean> datas = new Gson().fromJson(ja.toString(), new TypeToken<ArrayList<Tms_OrderBean>>() {
//				}.getType());
//				Intent i = new Intent(mActivity, TmsLoadTN.class);
//				if (!json.toString().contains("error")) {
//
//					i.putExtra("noScanedWeight", json.optDouble("noScanedWeight"));
//					i.putExtra("scanedWeight", json.optDouble("scanedWeight"));
//					i.putExtra("noScanedWaybill", json.optString("noScanedWaybill").toString());
//					i.putExtra("scanedWaybill", json.optString("scanedWaybill").toString());
//
//					i.putExtra("carNo", json.optString("carNo").toString());
//					i.putExtra("shipto_name", json.optString("shiptoName").toString());
//					i.putExtra("truck_size", truck_size);
//
//					i.putExtra("shipto_id", json.optString("shipto_id").toString());
//					TmsLoadTN.initParams(i, datas, doorBean, item);
//					startActivity(i);
//				} else {
//					UIHelper.showToast(json.optString("error").toString());
//				}
//			}
//
//			@Override
//			public void handFail() {
//			}
//		}.doGet(HttpUrlPath.GetOrderAndShippto, params, mActivity);
//	}

	private void TMS_Receive(final CheckInTaskItemBeanMain item) {
		// if(AppConfig.Cycle_Count_Task){
		// JSONObject json = TMS_Listing_InformationBean.getDebugStr();
		// List<TMS_Listing_InformationBean> datalist =
		// TMS_Listing_InformationBean.helpJson(json,item);
		// List<TMS_Listing_InformationBean> tndatalist =
		// TMS_Listing_InformationBean.tnhelpJson(json,item);
		// if (!Utility.isNullForList(datalist) ||
		// !Utility.isNullForList(tndatalist)) {
		// Intent i = new Intent(mActivity,TmsScanLoadOtherActivity.class);
		// Bundle b=new Bundle();
		// b.putSerializable("otherinfo", (Serializable) datalist);
		// b.putSerializable("othertninfo", (Serializable) tndatalist);
		// i.putExtras(b);
		//
		// startActivity(i);
		// }else{
		// UIHelper.showToast("NO Data!");
		// }
		// return;
		// }

		RequestParams params = new RequestParams();
		params.put("loading_id", item.number);
		params.put("entry_id", mainbean.entry_id + "");
		params.put("door_id", mainbean.resources_name + "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				// TMS_allReceivedata receivedata = TMS_allReceivedata
				// .getdata(json);
				// receivedata.loadpalletlist = TMS_Listing_InformationBean
				// .helpJson(json, item);
				// receivedata.loadtnlist = TMS_Listing_InformationBean
				// .tnhelpJson(json, item);
				// Intent i = new Intent(mActivity,
				// TmsScanLoadOtherActivity.class);
				// receivedata.entryID = mainbean.entry_id + "";
				// receivedata.dlo_detail_id = item.dlo_detail_id;
				// receivedata.door_name = mainbean.resources_name + "";
				// receivedata.loading_id = item.number + "";
				// i.putExtra("allreceive", (Serializable) receivedata);
				// i.putExtra("intentnumber", 0);
				// startActivity(i);
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.GetPalletsOrPackages, params, mActivity);
	}

//	private void TMS_AnNeng_Receive(final CheckInTaskBeanMain doorBean, final CheckInTaskItemBeanMain taskBean) {
//
//		RequestParams params = new RequestParams();
//		params.put("loading_id", taskBean.number);
//		params.put("entry_id", mainbean.entry_id + "");
//		params.put("door_id", mainbean.resources_name + "");
//		params.put("ps_id", StoredData.getPs_id());
//		new SimpleJSONUtil() {
//
//			@Override
//			public void handReponseJson(JSONObject json) {
//				List<TMS_WayBill> waybill = new Gson().fromJson(json.optJSONArray("items").toString(), new TypeToken<List<TMS_WayBill>>() {
//				}.getType());
//
//				List<TMS_SortingArea> sortingArea = new Gson().fromJson(json.optJSONArray("forSorting").toString(),
//						new TypeToken<List<TMS_SortingArea>>() {
//						}.getType());
//
//				Intent intent = new Intent();
//				intent.setClass(mActivity, TMS_AnNengReceive.class);
//				intent.putExtra("waybill", (Serializable) waybill);
//				intent.putExtra("sortingArea", (Serializable) sortingArea);
//				intent.putExtra("doorBean", (Serializable) doorBean);
//				intent.putExtra("taskBean", (Serializable) taskBean);
//				startActivity(intent);
//				overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
//			}
//
//			@Override
//			public void handFail() {
//			}
//		}.doGet(HttpUrlPath.getAnnengLoadingOrders, params, mActivity);
//	}
}
