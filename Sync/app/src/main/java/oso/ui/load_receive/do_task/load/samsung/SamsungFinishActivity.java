package oso.ui.load_receive.do_task.load.samsung;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.bean.CheckInLoadLoadBean;
import support.common.tts.TTS;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SamsungFinishActivity extends BaseActivity {

	private TextView palletTotalTv, boxTotalTv, weightTotalTv;
	private TextView palletTv, boxTv, weightTv;
	private LinearLayout boxTotalLay, weightTotalLay;
	private TabToPhoto ttp;

	private CheckInLoadLoadBean loadBean;
	String palletStr, boxStr, weightStr, entry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_samsung_finish, 0);
		initData();
		initView();
		setData();
	}

	private void initData() {
		palletStr = getIntent().getStringExtra("pallet");
		boxStr = getIntent().getStringExtra("box");
		weightStr = getIntent().getStringExtra("weight");
		entry_id = getIntent().getStringExtra("entry_id");
		loadBean = (CheckInLoadLoadBean) getIntent().getSerializableExtra("loadBean");
		setTitleString("Load Finish");
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key0 = TTPKey.getSamsungLoadFinishKey(loadBean.dlo_detail_id, 0);
		params.add(new TabParam("Processing", key0, new PhotoCheckable(0, "Processing", ttp)));
		params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoTaskProcessing+"", FileWithTypeKey.OCCUPANCY_MAIN+"", entry_id);
		return params;
	}

	private void initView() {
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		boxTotalLay = (LinearLayout) findViewById(R.id.boxTotalLay);
		weightTotalLay = (LinearLayout) findViewById(R.id.weightTotalLay);
		palletTotalTv = (TextView) findViewById(R.id.palletTotalTv);
		boxTotalTv = (TextView) findViewById(R.id.boxTotalTv);
		weightTotalTv = (TextView) findViewById(R.id.weightTotalTv);
		palletTv = (TextView) findViewById(R.id.palletTv);
		boxTv = (TextView) findViewById(R.id.boxTv);
		weightTv = (TextView) findViewById(R.id.weightTv);
	}

	private void setData() {
		ttp.init(mActivity, getTabParamList());
		boxTotalLay.setVisibility(View.VISIBLE);
		weightTotalLay.setVisibility(View.VISIBLE);
		palletTotalTv.setText(palletStr);
		boxTotalTv.setText(boxStr);
		weightTotalTv.setText(weightStr);
		palletTv.setText(palletStr);
		boxTv.setText(boxStr);
		weightTv.setText(weightStr);
	}

	@Override
	protected void onResume() {
		super.onResume();
		ttp.onResume(getTabParamList());
	}

	public void finishOnClick(View v) {
		RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
		dialog.setTitle(getString(R.string.sync_notice));
		dialog.setMessage("Load Finish?");
		dialog.setCancelable(false);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				doDone();
			}
		});
		dialog.create().show();
	}

	private void doDone() {
		RequestParams params = new RequestParams();
		params.add("Method", "SumsangLoadFinish");
		params.add("dlo_detail_id", loadBean.dlo_detail_id);
		params.add("ic_id", loadBean.ic_id);
		params.add("entry_id", entry_id);
		params.add("close_type", "3");
		params.add("reason", "");
		params.add("equipment_id", loadBean.equipment_id);
		params.add("resources_id", loadBean.resources_id);
		params.add("resources_type", loadBean.resources_type);
		ttp.uploadZip(params, "SamsungFinishPhoto");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("Done= " + json.toString());
				TTS.getInstance().speakAll_withToast("Finish Success!");
				RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
				dialog.setTitle(getString(R.string.sync_notice));
				dialog.setMessage(getString(R.string.sync_success));
				dialog.isHideCancelBtn(true);
				dialog.setPositiveButton(getString(R.string.checkout_finish), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(mActivity, CheckInTaskDoorItemActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intent.putExtra("refresh", true);
						startActivity(intent);
					}
				});
				dialog.create().show();
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll_withToast("Finish Failed!");
			}
		}.doPost(HttpUrlPath.ReceiveWorkAction, params, this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onBackBtnOrKey() {
		ttp.clearCache();
		super.onBackBtnOrKey();
	}
}
