package oso.ui.load_receive.do_task.load.smallparcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.load.smallparcel.util.ClpTypeListView;
import oso.ui.load_receive.do_task.load.smallparcel.util.ClpTypeListView.OnItemOrderClickListener;
import oso.ui.load_receive.do_task.receive.task.bean.Rec_CLPType;
import utility.ActivityUtils;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class CC_ClpActivity extends BaseActivity {

	private ClpTypeListView lv;
	private EditText searchEt;
	private ImageView delBtn;
	
	
	private LinearLayout searchLayout;
	private ListView searchLv;
	
	private List<Rec_CLPType> searchClpType;
	
	private SearchAdapter searchAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_cc_carrier, 0);
		applyParams();
		setTitleString("CLP");
		initView();
		setData();
	}

	private void initView() {
		searchEt = (EditText) findViewById(R.id.searchEt);
		delBtn = (ImageView) findViewById(R.id.delBtn);
		lv = (ClpTypeListView) findViewById(R.id.lv);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
	}

	private void setData() {
		searchClpType = new ArrayList<Rec_CLPType>();
		searchAdapter = new SearchAdapter();
		searchLv.setAdapter(searchAdapter);
		
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s)) {
					delBtn.setVisibility(View.GONE);
				} else {
					delBtn.setVisibility(View.VISIBLE);
				}
			}
		});
		delBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchEt.setText("");
			}
		});
		// lv.setEmptyView(findViewById(R.id.nodataTv));
		lv.setAdapter(list);
		lv.setOnItemClickListener(new OnItemOrderClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id, Rec_CLPType letter) {
				Intent in=new Intent();
				in.putExtra("index", position);
				setResult(RESULT_OK,in);
                finish();
			}
		});
		
		searchLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent in = new Intent();
				in.putExtra("index",list.indexOf(searchClpType.get(position)));
				setResult(RESULT_OK,in);
				finish();
				
			}
		});
	}
	
	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();
			
			searchClpType.clear();
			for(Rec_CLPType clpType : list){
				if(clpType.lp_name.startsWith(searchStr.toLowerCase()) || clpType.lp_name.startsWith(searchStr.toUpperCase())){
					searchClpType.add(clpType);
				}
			}
			
			searchAdapter.notifyDataSetChanged();
			
		} else {
			hideSearch();
		}
	}
	
	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
		searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}
	
	
	//=========传参===============================
	
	private List<Rec_CLPType> list;
	private Rec_CLPType clpType;

	public static void initParams(Intent in,List<Rec_CLPType> list, Rec_CLPType clpType) {
		in.putExtra("list",(Serializable)list);
		in.putExtra("clptype", clpType);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		list=(List<Rec_CLPType>)params.getSerializable("list");
		clpType = (Rec_CLPType) params.getSerializable("clptype");
		
		if(clpType != null){
			for(Rec_CLPType data : list){
				if(data.lpt_id.equals(clpType.lpt_id)){
					data.isSelect = true;
					break;
				}
			}
		}
	}
	
	//---------------------------------------
	
	private class SearchAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return searchClpType == null ? 0 : searchClpType.size();
		}

		@Override
		public Object getItem(int position) {
			return searchClpType.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				convertView = View.inflate(mActivity, android.R.layout.simple_list_item_1, null);
			}
			((TextView) convertView).setTextColor(Color.BLACK);
			((TextView) convertView).setTextSize(12);
			((TextView) convertView).setText(searchClpType.get(position).lp_name);
			
			return convertView;
		}
		
	}
}
