package oso.ui.load_receive.do_task.main.iface;

import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;


/**adapter回调接口
 * @author ljh
 * @date 2014-12-16
 */
public interface CheckInTaskInterface {
	public void selectItem(CheckInTaskItemBeanMain item,int position);
	public void selectItem(CheckInTaskItemBeanMain item);
	public void addLoadBatData(int load_bar_id,String cnt,boolean isChange_loadbar);
	public void all_btn(CheckInTaskItemBeanMain item,int btn_type);
	public void DetailByEntry(final CheckInTaskBeanMain bean);
	public void setnewnumberstates(int i);
}
