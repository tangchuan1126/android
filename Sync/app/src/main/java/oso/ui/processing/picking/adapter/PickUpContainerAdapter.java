package oso.ui.processing.picking.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import oso.ui.processing.picking.bean.ContainerEquals;
import oso.ui.processing.picking.bean.ContainerSimple;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpContainer;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import support.key.ContainerIntTypeKey;
import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpContainerAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	
	private Context context;
	
	private List<PickUpBean>  arrayList ;
	
	private Resources resources ;
	
	private Long location_id;
	
	private long pickup_id ;
	
	private PickUpScanInfoDao pickUpScanInfoDao ;
	
	private int index;
	
	public PickUpContainerAdapter(Context context, List<PickUpBean> arrayList , Resources resources , LinkedList<PickUpContainer> listPickupContainer  ,
			long pickup_id , long location_id) {
		super();
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.arrayList = arrayList;
		this.resources = resources ;
		PickUpContainerAdapterHelper.resouresPickupContainerList  = listPickupContainer ;
 		this.pickup_id = pickup_id ;
 		this.location_id = location_id ;
 		this.pickUpScanInfoDao = new PickUpScanInfoDao(context);
	}

	public void setIndex(int selected) {  
        index = selected;  
    } 
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0 ;
	}

	@Override
	public PickUpBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PickUpBean temp = getItem(position);
 		PickUpHolder holder = null;
 		 
 		if (convertView == null) {
			holder = new PickUpHolder();
 
			convertView = inflater.inflate(R.layout.pickup_container_item, null);
			holder.pick_lp=(TextView)convertView.findViewById(R.id.pick_lp);
			holder.pickup_type = (TextView)convertView.findViewById(R.id.pickuptype);
			holder.fromContainer_id=(TextView)convertView.findViewById(R.id.fromContainer_id);
			holder.pc_name = (TextView)convertView.findViewById(R.id.pick_name);
 			holder.totalQty_num = (TextView)convertView.findViewById(R.id.totalQty_num);
 			holder.pick_up_l=(LinearLayout)convertView.findViewById(R.id.pick_up_linear);
 			holder.pick_up_type=(TextView)convertView.findViewById(R.id.pick_up_type);
			convertView.setTag(holder);
		} else {
			holder = (PickUpHolder) convertView.getTag();
		}
		if(index == position){  
            //此处就是设置textview为选中状态，方可以实现效果         
            convertView.findViewById(R.id.fromContainer_id) .setSelected(true);  
        }else{  
            //没选中的就不用设置了                             
             convertView.findViewById(R.id.fromContainer_id).setSelected(false);  
        }
 		//设置Header
		String notify = "" ;
		 
		holder.fromContainer_id.setText(notify);
//		if(temp.getPickUpType()==  PickUpBean.DirectPickUpContainerType){
//			
////			holder.fromContainer_id.setBackgroundResource(R.drawable.product_photo13);
//			notify = resources.getString(R.string.pickup_on_location_direct_container_type);
// 		
//		}else if(temp.getPickUpType() == PickUpBean.DirectPickUpContainerIdContainerType){
//			
// 			String containerName = ContainerIntTypeKey.getType(temp.getFromContainerType())  + temp.getFromContainerId() ;
// 			notify = String.format(resources.getString(R.string.pickup_on_container_direct_container_type), containerName);
////			holder.fromContainer_id.setBackgroundResource(R.drawable.product_photo16);
// 		
//		}else if(temp.getPickUpType() == PickUpBean.DirectPcikUpProduct){
//			
// 			String containerName = ContainerIntTypeKey.getType(temp.getFromContainerType())  + temp.getFromContainerId() ;
// 			notify = String.format(resources.getString(R.string.pickup_on_container_direct_product), containerName);
////			holder.fromContainer_id.setBackgroundResource(R.drawable.product_photo14);
//		
//		}else if(temp.getPickUpType() == PickUpBean.DirectPcikUpProductOnType){
//			
//  			notify = String.format(resources.getString(R.string.pickup_on_container_type_direct_product), ContainerIntTypeKey.getType(temp.getFromContainerType()) ,temp.getFromContainerTypeId()  );
////			holder.fromContainer_id.setBackgroundResource(R.drawable.product_photo14);
//			
//		}else if(temp.getPickUpType() == PickUpBean.DirectPcikUpProductNoType){
//			
//  			notify = String.format(resources.getString(R.string.pickup_on_container_no_type_direct_product), ContainerIntTypeKey.getType(temp.getFromContainerType())    );
////			holder.fromContainer_id.setBackgroundResource(R.drawable.product_photo14);
//			
//		}
		
		if(temp.getPickUpType() == PickUpBean.DirectPcikUpProductNoType || temp.getPickUpType() == PickUpBean.DirectPcikUpProductOnType){
			holder.fromContainer_id.setText(ContainerIntTypeKey.getType(temp.getFromContainerType())+"");
			holder.pick_up_type.setText("TYPE:"+temp.getFromContainerTypeId());
		}else{
			holder.fromContainer_id.setText(ContainerIntTypeKey.getType(temp.getPickUpContainerType())+"");
			holder.pick_up_type.setText("TYPE:"+temp.getPickUpContainerTypeId());
		}
//		
//			holder.fromContainer_id.setText(notify);
			//设置content
			holder.pick_up_l.removeAllViews();
			setContent(temp,holder.pick_up_l,inflater,pickUpScanInfoDao , pickup_id ,location_id);
			 
			//设置footer	
			holder.totalQty_num.setText(temp.getTotalQty()+"");
			return convertView;
	}
	
	
	
	
	private void setContent(PickUpBean pickUpBean , LinearLayout appendView ,  LayoutInflater inflater ,PickUpScanInfoDao pickUpScanInfoDao , long pickup_id ,long location_id){
		
		switch (pickUpBean.getPickUpType()) {
		
			case PickUpBean.DirectPickUpContainerType : 	 
				PickUpContainerAdapterHelper.setDirectPickUpContainerTypeBody(pickUpBean,appendView,inflater);
				break;
			case PickUpBean.DirectPickUpContainerIdContainerType :
				PickUpContainerAdapterHelper.setDirectPickUpContainerIdContainerTypeBody(pickUpBean,appendView,inflater);
				break ;
			case PickUpBean.DirectPcikUpProduct :
				PickUpContainerAdapterHelper.setDirectPcikUpProductBody(pickUpBean,appendView,inflater,pickUpScanInfoDao,pickup_id,location_id);
				break ;
			case PickUpBean.DirectPcikUpProductOnType :
				PickUpContainerAdapterHelper.setDirectPcikUpProductBody(pickUpBean,appendView,inflater,pickUpScanInfoDao,pickup_id,location_id);
				break ;
			case PickUpBean.DirectPcikUpProductNoType :
				PickUpContainerAdapterHelper.setDirectPcikUpProductBody(pickUpBean,appendView,inflater,pickUpScanInfoDao,pickup_id,location_id);
				break ;
		}
	
	}
	
}
class PickUpContainerAdapterHelper{
	 
	public static LinkedList<PickUpContainer> resouresPickupContainerList ;
 	 
 
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * 得到一个计算过后的集合
	 * 	1.要计算出来数据库已经拣了多少的货物了
	 * 	2.有可能是这个PickupBean 告诉你了。在这个位置拣Type 和 拣这个Type的具体的Container
	 * @param pickUpBean
	 * @return
	 */
	public static List<ContainerSimple> getContainerSimpleList(PickUpBean pickUpBean){
		
 		List<ContainerSimple> list = new ArrayList<ContainerSimple>();
		Map<ContainerEquals, ContainerSimple>  maps = PickUpBaseDataParse.getContainerSimpleList(pickUpBean);
		if(!maps.isEmpty()){
			Iterator<Map.Entry<ContainerEquals, ContainerSimple>> it = maps.entrySet().iterator();
			LinkedList<PickUpContainer> value = (LinkedList<PickUpContainer>)resouresPickupContainerList.clone() ;	//浅复制
			//先查询具体的ContainerId 是否有捡到
			while(it.hasNext()){ 
				Entry<ContainerEquals, ContainerSimple> entry = it.next();
				ContainerSimple simple = entry.getValue() ;
				if(simple.getContainerId() != 0l){
					//这里应该是要去查询数据库这个Container我是否已经拣货到了
					//这里为了节约时间我先一次查询出来所有的已经拣到的Container。每次都去遍历里面是否有了我的Container。找到一个就Remove一个
					
					if(isContainerPickup(simple.getContainerId(),value)){
						simple.setPickQty(1);
					}
					list.add(simple);
				}
			}
			Iterator<Map.Entry<ContainerEquals, ContainerSimple>> it_two = maps.entrySet().iterator();
			
			//不要把这个两个合并成一个。会有问题
			while(it_two.hasNext()){ 
				Entry<ContainerEquals, ContainerSimple> entry = it_two.next();
				ContainerSimple simple = entry.getValue() ;
				if(simple.getContainerId() == 0l){
 					simple.setPickQty( getTotalPickup(simple.getContainerType(), simple.getContainerTypeId(),value));
					list.add(simple);
				}
			}
		}
		return list ;
 	}
	
	/**
	 * 计算出来这种类型的ContainerType已经拣了多少货了
	 * @param containerType
	 * @param containerTypeId
	 * @return
	 */
	private static int getTotalPickup(int containerType , long containerTypeId ,LinkedList<PickUpContainer>	 value ){
		int returnCount = 0 ;
 		if(value != null &&  value.size() > 0 ){
			for(int index = 0 , count = value.size()  ; index < count ; index++ ){
				PickUpContainer pickupContainer = value.get(index);
				if(pickupContainer.getContainer_type() == containerType && pickupContainer.getContainer_type_id() == containerTypeId ){
					returnCount++ ;
					//value.remove(index);
				}
			}	
		}
		return returnCount ;

	}
	
	/**
	 * 返回请求的Container_id 是否我已经拣到了
	 * @param container_id
	 * @return
	 */
	private static boolean isContainerPickup(long container_id,LinkedList<PickUpContainer> value){
		boolean flag = false ;
		if(value != null &&  value.size() > 0 ){
			for(int index = 0 , count = value.size()  ; index < count ; index++ ){
				if(value.get(index).getContainer_id() == container_id){
					value.remove(index);
					flag = true ;
					break ;
				}
			}
			
		}
		return flag ;
	}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	
	/**
	 * 直接去这个位置拣货某种类型的Container
	 * 1.如果是有ContainerId 表示为指定的	数量为1
	 * 2.如果是只有Type的表示的是直接拣这种类型的Container 那么应该计算出几个来
	 * 
	 * @param pickUpBean
	 */
	public  static  void setDirectPickUpContainerTypeBody(PickUpBean pickUpBean,LinearLayout appendView, LayoutInflater inflater ){
			List<ContainerSimple> simpleList = getContainerSimpleList(pickUpBean) ;
			
			if(simpleList != null && simpleList.size() > 0 ){
				for(ContainerSimple simpe : simpleList){
					View   headerView = inflater.inflate(R.layout.get_pickup_container_item, null);
					//类型的PickupContainerType 
					TextView containerTypeTextView = (TextView)headerView.findViewById(R.id.pickup_container_type);
//					TextView qtyTextView = (TextView)headerView.findViewById(R.id.qty);
					TextView showContainerOrType = (TextView)headerView.findViewById(R.id.show_contaner_or_type);
//					TextView totalQty = (TextView)headerView.findViewById(R.id.totalqty);

	 				TextView qty_and_totalqty = (TextView)headerView.findViewById(R.id.qty_and_totalqty);
	 				
					String showValue = "Type:"+simpe.getContainerTypeId() +"" ;
					if(simpe.getContainerId() != 0l){
						showValue = ContainerIntTypeKey.getType(simpe.getContainerType())+simpe.getContainerId();
					} 
					showContainerOrType.setText(showValue);
					containerTypeTextView.setText(ContainerIntTypeKey.getType(simpe.getContainerType()));
//				 	qtyTextView.setText(simpe.getPickQty()+"");
//				 	totalQty.setText(simpe.getTotalQty() + "");
					
					qty_and_totalqty.setText(simpe.getPickQty()+" / "+simpe.getTotalQty());
					appendView.addView(headerView);
				}
			}else{
				View   headerView = inflater.inflate(R.layout.inventory_location_list_view_item, null);
				//类型的PickupContainerType 
				TextView productName = (TextView)headerView.findViewById(R.id.product_name);
				productName.setText("暂无数据");
				productName.setGravity(Gravity.CENTER);
				TextView locationProductQty = (TextView)headerView.findViewById(R.id.location_product_qty);
				locationProductQty.setVisibility(View.GONE);
				appendView.addView(headerView);
			}
			 
	}
	public  static  void setDirectPickUpContainerIdContainerTypeBody(PickUpBean pickUpBean,LinearLayout appendView, LayoutInflater inflater ){
			setDirectPickUpContainerTypeBody(pickUpBean, appendView, inflater);
	}
	
	public  static  void setDirectPcikUpProductBody(PickUpBean pickUpBean,LinearLayout appendView, LayoutInflater inflater , PickUpScanInfoDao pickUpScanInfoDao , long pickup_id , long location_id ){
		//分别显示出来。每个商品都需要多少。都拣货多少个了	利用数据库
  		List<ProductCompareSimple> datas =  pickUpScanInfoDao.getOnContainerTypePickupProductList(  pickup_id,   location_id,pickUpBean.getFromContainerType(),pickUpBean.getFromContainerTypeId() );
		if(datas != null && datas.size() > 0 ){
 			for(ProductCompareSimple simple : datas){
 				View   headerView = inflater.inflate(R.layout.get_pickup_container_item, null);
				//类型的PickupContainerType pickup_container_type
				TextView containerTypeTextView = (TextView)headerView.findViewById(R.id.pickup_container_type);
//				TextView qtyTextView = (TextView)headerView.findViewById(R.id.qty);
// 				TextView totalQty =  (TextView)headerView.findViewById(R.id.totalqty);
 				TextView qty_and_totalqty = (TextView)headerView.findViewById(R.id.qty_and_totalqty);
				String showValue = simple.getpName()  ;
				containerTypeTextView.setText(showValue);
// 			 	qtyTextView.setText(simple.getPickupQty()+"");
// 			 	totalQty.setText(simple.getTotalQty()+"");

				qty_and_totalqty.setText(simple.getPickupQty()+" / "+simple.getTotalQty());
				appendView.addView(headerView);
 			}
		}else{
			View   headerView = inflater.inflate(R.layout.inventory_location_list_view_item, null);
			//类型的PickupContainerType 
			TextView productName = (TextView)headerView.findViewById(R.id.product_name);
			productName.setText("暂无数据");
			productName.setGravity(Gravity.CENTER);
			TextView locationProductQty = (TextView)headerView.findViewById(R.id.location_product_qty);
			locationProductQty.setVisibility(View.GONE);
			appendView.addView(headerView);
		}
	}
	
	 
}

class PickUpHolder {
	public TextView pick_lp;
	public TextView pickup_type;
	public TextView fromContainer_id;
	public TextView pc_name ;
	public TextView totalQty_num;
	public LinearLayout pick_up_l;
	public TextView pick_up_type;
}
