package oso.ui.processing.picking.parse;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import oso.ui.processing.picking.bean.ContainerEquals;
import oso.ui.processing.picking.bean.ContainerSimple;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpLocation;
import oso.ui.processing.picking.bean.PickUpSerialProduct;
import oso.ui.processing.picking.bean.Product;
import oso.ui.processing.picking.bean.ProductCode;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.bean.ReturnBean;
import utility.StringUtil;

public class PickUpBaseDataParse {

	  
	
	
	
	public static ReturnBean<PickUpLoactionProduct> handNeedPickUpProduct(InputStream in , long  pickUpId){
		ReturnBean<PickUpLoactionProduct> returnBean =  new ReturnBean<PickUpLoactionProduct>() ;
		List<PickUpLoactionProduct> list = null;
		PickUpLoactionProduct needPickUpProduct = null ;
	 
		try {
			XmlPullParser parse = XmlPullParserFactory.newInstance().newPullParser();
			if(in!=null){
				parse.setInput(in, "UTF-8");
			}
			 
			int eventType = parse.getEventType();
		 
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) { 
				case XmlPullParser.START_DOCUMENT: 
					list = new ArrayList<PickUpLoactionProduct>();
					break;
				case XmlPullParser.START_TAG: 
					
					String name = parse.getName(); 
					if(name.equals("ret")){
						String nameValue = parse.nextText() ;
						returnBean.setRet(nameValue);
					}else if(name.equals("err")){
						String nameValue = parse.nextText() ;
						returnBean.setErr(nameValue);
					}else if(name.equals("Details")) {
						needPickUpProduct = new PickUpLoactionProduct();
						needPickUpProduct.setPickup_id(pickUpId);
   					}else if (name.equals("PcId")) {
						String nameValue = parse.nextText() ;
						needPickUpProduct.setPcid(StringUtil.convert2Long(nameValue)); 
  					}else if(name.equals("SlcId")) {
						String nameValue = parse.nextText() ;
						needPickUpProduct.setLocation_id(StringUtil.convert2Long(nameValue)); 
						needPickUpProduct.setSlcId(StringUtil.convert2Long(nameValue));
						
 					}else if (name.equals("NeedPickQuantity")) {
						String nameValue = parse.nextText() ;	
						needPickUpProduct.setQty(StringUtil.convert2Double(nameValue));
 					}else if(name.equals("From_Container_Type")) {
						String nameValue = parse.nextText();
						needPickUpProduct.setFrom_container_type(StringUtil.convert2Inter(nameValue));
					}else if(name.equals("From_Container_Type_id")) {
						String nameValue = parse.nextText();
						needPickUpProduct.setFrom_container_type_id(StringUtil.convert2Long(nameValue));						 
  					}else if(name.equals("From_Con_id")){
						String nameValue = parse.nextText();
						needPickUpProduct.setFrom_con_id(StringUtil.convert2Long(nameValue));
 					}else if(name.equals("Pick_Container_Type")){
						String nameValue = parse.nextText();
						needPickUpProduct.setPick_container_Type(StringUtil.convert2Inter(nameValue));
 					}else if(name.equals("Pick_Container_Type_id")){
						String nameValue = parse.nextText();
						needPickUpProduct.setPick_container_Type_id(StringUtil.convert2Long(nameValue)); 
 					}else if(name.equals("Pick_Con_id")){
						String nameValue = parse.nextText();
						needPickUpProduct.setPick_con_id(StringUtil.convert2Long(nameValue));
 					}else if(name.equals("SN")){
						String nameValue = parse.nextText();
						needPickUpProduct.setSn(StringUtil.isNullOfStr(nameValue) ? "" : nameValue);
  					}else if(name.equals("Location")){
						String nameValue = parse.nextText();
						needPickUpProduct.setLocation(nameValue);
  					}
  					else if(name.equals("Pick_Container_Qty")){
						String nameValue = parse.nextText();
						needPickUpProduct.setPickContainerQty(StringUtil.convert2Inter(nameValue));
  					}
					
					break;
				case XmlPullParser.END_TAG:
					if(parse.getName().equals("Details")){
						list.add(needPickUpProduct);
					}
					break;

				}
 
				eventType=parse.next();	
			}

		} catch (XmlPullParserException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
 					e.printStackTrace();
				}
			} 
		}
		returnBean.setArrayList(list);
		
		
		return returnBean ;
	}
	
	
	
	
	// product 
	public static ReturnBean<Product> handOutListProductXml(InputStream in){
 
		ReturnBean<Product> returnBean =  new ReturnBean<Product>() ;
		List<Product> list = null;
		Product product = null ;
		try {
			XmlPullParser parse = XmlPullParserFactory.newInstance().newPullParser();
			if(in!=null){
				parse.setInput(in, "UTF-8");
			}
			 
			int eventType = parse.getEventType();
		 
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) { 
				case XmlPullParser.START_DOCUMENT: 
					list = new ArrayList<Product>();
					break;
				case XmlPullParser.START_TAG: 
					String name = parse.getName(); 
					  if(name.equals("Details")) {
						product = new Product();
					}else if (name.equals("Name")) {
						String Name = parse.nextText() ;
						 
						product.setName(Name);
  					}else if(name.equals("PID")) {
						String PID = parse.nextText();
						product.setPid(StringUtil.convert2Long(PID));
						
 					}else if (name.equals("Length")) {
						String Length = parse.nextText() ;	
						product.setLength(StringUtil.convert2Double(Length));
					}else if(name.equals("Width")) {
						String Width = parse.nextText();
						product.setWidth(StringUtil.convert2Double(Width));
					}else if(name.equals("Heigth")) {
						String Heigth = parse.nextText();
						product.setHeigth(StringUtil.convert2Double(Heigth));
 					}else if(name.equals("Weight")){
 						String Weight = parse.nextText();
						product.setWeight(StringUtil.convert2Double(Weight));

 					}
					break;
				case XmlPullParser.END_TAG:
					if(parse.getName().equals("Details")){
						list.add(product);
					}
					break;

				}
 
				eventType=parse.next();	
			}

		} catch (XmlPullParserException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
 					e.printStackTrace();
				}
			} 
		}
		returnBean.setArrayList(list);
		return returnBean;
	
		
		
	}
	
	
	//pickupLocation ..... 
	public static ReturnBean<PickUpLocation> handPickUpLocationXml(InputStream in){
		ReturnBean<PickUpLocation> returnBean =  new ReturnBean<PickUpLocation>() ;
		List<PickUpLocation> list = null;
		PickUpLocation pickUpLocation = null ;
		try {
			XmlPullParser parse = XmlPullParserFactory.newInstance().newPullParser();
	 
			if(in!=null){
				parse.setInput(in, "UTF-8");
			}
			int eventType = parse.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) { 
				case XmlPullParser.START_DOCUMENT: 
					list = new ArrayList<PickUpLocation>();
					break;
				case XmlPullParser.START_TAG: 
					String name = parse.getName(); 
					  if(name.equals("Details")) {
						  pickUpLocation = new PickUpLocation();
					}else if (name.equals("Location")) {
						String nameValue = parse.nextText() ;
						pickUpLocation.setLocation(nameValue); 
  					}else if(name.equals("Sort")) {
 						String nameValue = parse.nextText() ;
 						pickUpLocation.setSort(StringUtil.convert2Inter(nameValue)); ;
 					}else if (name.equals("LocationId")) {
						String nameValue = parse.nextText() ;	
						pickUpLocation.setLocation_id(StringUtil.convert2Long(nameValue)); 
 					}else if(name.equals("Out")) {
						String nameValue = parse.nextText() ;	
						pickUpLocation.setPickup_id(StringUtil.convert2Long(nameValue));
					} else if(name.equals("LocationType")) {
						String nameValue = parse.nextText() ;	
						pickUpLocation.setLocation_type(StringUtil.convert2Inter(nameValue));
					}
					break;
				case XmlPullParser.END_TAG:
					if(parse.getName().equals("Details")){
						list.add(pickUpLocation);
					}
					break;
				}
				eventType=parse.next();	
			}
		} catch (XmlPullParserException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
 					e.printStackTrace();
				}
			} 
		}
		returnBean.setArrayList(list);
		return returnBean ;
 
	}
 
	
	//ProductCode..... 
	public static ReturnBean<ProductCode> handPickUpProductCodeXml(InputStream in){
		ReturnBean<ProductCode> returnBean =  new ReturnBean<ProductCode>() ;
		List<ProductCode> list = null;
		ProductCode productCode = null ;
	
		try {
			XmlPullParser parse = XmlPullParserFactory.newInstance().newPullParser();
	 
			if(in!=null){
				parse.setInput(in, "UTF-8");
			}
			 
			int eventType = parse.getEventType();
		 
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) { 
				case XmlPullParser.START_DOCUMENT: 
					list = new ArrayList<ProductCode>();
					break;
				case XmlPullParser.START_TAG: 
					String name = parse.getName(); 
					  if(name.equals("Details")) {
						  productCode = new ProductCode();
					}else if (name.equals("PCID")) {
						String nameValue = parse.nextText() ;
						productCode.setPcid(StringUtil.convert2Long(nameValue));
  					}else if(name.equals("Barcode")) {
 						String nameValue = parse.nextText() ;
 						productCode.setBarcode(nameValue);
 					}else if (name.equals("CodeType")) {
						String nameValue = parse.nextText() ;	
						productCode.setCodeType(nameValue);
					}else if(name.equals("PName")) {
						String nameValue = parse.nextText() ;	
						productCode.setP_name(nameValue);
					} 
					break;
				case XmlPullParser.END_TAG:
					if(parse.getName().equals("Details")){
						list.add(productCode);
					}
					break;
				}
				eventType=parse.next();	
			}
		} catch (XmlPullParserException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
 					e.printStackTrace();
				}
			} 
		}
 		returnBean.setArrayList(list);
		return returnBean ;
	}
	public static ReturnBean<PickUpSerialProduct> handPickUpSerialProductXml(InputStream in){
		ReturnBean<PickUpSerialProduct> returnBean =  new ReturnBean<PickUpSerialProduct>() ;
		List<PickUpSerialProduct> list = null;
		PickUpSerialProduct pickUpSerialProduct = null ;	

		try {
			XmlPullParser parse = XmlPullParserFactory.newInstance().newPullParser();
			if(in!=null){parse.setInput(in, "UTF-8");}		 
			int eventType = parse.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) { 
				case XmlPullParser.START_DOCUMENT: 
					list = new ArrayList<PickUpSerialProduct>();
					break;
				case XmlPullParser.START_TAG: 
					String name = parse.getName(); 
					  if(name.equals("Details")) {
						pickUpSerialProduct = new PickUpSerialProduct();
					}else if (name.equals("PCID")) {
						String nameValue = parse.nextText() ;
						pickUpSerialProduct.setPcid(StringUtil.convert2Long(nameValue));
  					}else if(name.equals("SN")) {
 						String nameValue = parse.nextText() ;
 						pickUpSerialProduct.setSn(nameValue);
 					} 
					break;
				case XmlPullParser.END_TAG:
					if(parse.getName().equals("Details")){
						list.add(pickUpSerialProduct);
					}
					break;
				}
				eventType=parse.next();	
			}
		} catch (XmlPullParserException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
 					e.printStackTrace();
				}
			} 
		}
 		returnBean.setArrayList(list);
		return returnBean ;
	}

	/**
	 * 根据PickupBean里面的arrayList 计算出来
	 * 	1.BLP1212121  0/1(这个具体的Container只需要一个)
	 *  2.BLP 121  	0/5	(这种类型的的要5个)
	 * @param pickUpBean
	 * @return
	 */
	public static Map<ContainerEquals, ContainerSimple>  getContainerSimpleList(PickUpBean pickUpBean){

		List<PickUpLoactionProduct> arrayList = pickUpBean.getArrayListPickupLocationProduct();
		Map<ContainerEquals, ContainerSimple> maps = new HashMap<ContainerEquals, ContainerSimple>();
		for(PickUpLoactionProduct temp : arrayList){
			ContainerEquals isEquals = new ContainerEquals(temp.getPick_con_id(), temp.getPick_container_Type_id(), temp.getPick_container_Type());
			ContainerSimple simple = maps.get(isEquals);
			if(simple== null){
			    simple = new ContainerSimple();
				simple.setContainerType(temp.getPick_container_Type());
				simple.setContainerTypeId(temp.getPick_container_Type_id());
				simple.setPickQty(0);
				
				simple.setTotalQty(temp.getPickContainerQty());
				simple.setPcid(temp.getPcid());
				simple.setpName(temp.getPname());
				simple.setProductQty(temp.getQty());
				simple.setContainerId(temp.getPick_con_id());
				maps.put(isEquals, simple);
			}else{
				 
				simple.addTotalQty(temp.getPickContainerQty()); 
			}
		}
		return maps;
	}
	/**
	 * 在PickupBean 里面获取如果是在具体的Container上面计算每种货物都需要多少
	 * @param pickUpBean
	 * @return
	 */
	public static Map<Long,ProductCompareSimple> getPickupContainerProduct(PickUpBean pickUpBean){
		Map<Long, ProductCompareSimple> maps = new HashMap<Long, ProductCompareSimple>();
		List<PickUpLoactionProduct>  arrayList =	pickUpBean.getArrayListPickupLocationProduct();
		if(arrayList != null && arrayList.size() > 0 ){
			for(PickUpLoactionProduct temp : arrayList){
				ProductCompareSimple simple = maps.get(temp.getPcid());
				if(simple == null){
					simple = new ProductCompareSimple();
					simple.setPcid(temp.getPcid());
					simple.setpName(temp.getPname());
					simple.setPickupQty(0);
					simple.setTotalQty(temp.getQty());
					maps.put(temp.getPcid(), simple);
				}else{
					simple.addTotalQty(temp.getQty());
				}
			}
		}
		return maps ;
	}
}
