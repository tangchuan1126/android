package oso.ui.processing.picking.bean;

public class PickUp {

	private long id ;
	private long pickup_id ;
	private int isOver = 0 ;
	private int type ; //1sn.2.Not SN
	private String doorName;
	private String locationname;
	
	
	
	public String getDoorName() {
		return doorName;
	}
	public void setDoorName(String doorName) {
		this.doorName = doorName;
	}
	public String getLocationname() {
		return locationname;
	}
	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPickup_id() {
		return pickup_id;
	}
	public void setPickup_id(long pickup_id) {
		this.pickup_id = pickup_id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getIsOver() {
		return isOver;
	}
	public void setIsOver(int isOver) {
		this.isOver = isOver;
	}
	
}
