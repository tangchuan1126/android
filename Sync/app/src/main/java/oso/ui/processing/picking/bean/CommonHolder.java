package oso.ui.processing.picking.bean;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class CommonHolder {

	public static TextView id;
	public TextView bill_id;          //单据id
	public TextView fromWarehouse;    //发货仓库
	public TextView toWarehouse;      //到货仓库
	public TextView area;
	public TextView state;
	public TextView door;              //门
	public TextView location;          //位置
	public TextView doorTitle;         //门标题
	public TextView locationTitle;     //位置标题
	public TextView repeateSn;
	public TextView selectId;
	public TextView selectSN;
	public TextView selectPname;
	public TextView selectPid;
	public TextView selectBarcode;
	public TextView selectQuantity;
	public TextView selectSKU;
	public TextView selectpalletNO;
	public TextView idEditText;       
	public TextView lpEditText;
	public TextView lpNameEditText;
	public TextView totalQuantityEditText;
	public TextView barcode;
	public TextView qty;
	public TextView sn;
	public TextView snLabel;
	public TextView pcid;
	public TextView pname;
	public ImageView differentType;
	public TextView machine;
	public TextView differentTypeText;
	public TextView to_id;
	public TextView sku;
	public TextView receiveQty;
	public TextView locationId;      //位置id
	public TextView scanedQty;
	public TextView submitQty;
	public TextView stockQty;
	public TextView sort;            //排序
	public static TextView union_pid;
	public static TextView union_catalog_id;
	public static TextView union_set_pid;
	public static TextView union_p_name;
	public static TextView union_quantity;
	public static TextView union_unit_name;
	public static TextView union_p_code;
	public static TextView union_catalog_text;
	public static TextView union_weight;
	public static TextView union_unit_price;
	public static TextView union_volume;
	public static TextView union_gross_profit;
	public static TextView get_p_code;           //条码
	public static TextView get_code_type;        //类型
	public TextView selected_fc_project_name;
	public TextView selected_fc_way;
	public TextView selected_fc_unit;
	public EditText selected_fc_unit_price;
	public Spinner selected_currency;
	public EditText selected_exchange_rate;
	public EditText selected_count;
	public Button btn_delete_freight;
	public TextView sku_label ; 
	public TextView barCodeType ;
	public TextView TotalQty ;
	public Button complete ;
	public Button clearDoor ;
	public Button photoCapter ;
	public Button transportDetailsFinish;
	public TextView titleId ;				//titleId;
	public TextView titleName ;				//titleName
	public TextView ContainerId ;
	
	//title
	public TextView title_id;
	public TextView title_name;
	public TextView pallets;
}
