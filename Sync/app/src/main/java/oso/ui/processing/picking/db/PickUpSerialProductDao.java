package oso.ui.processing.picking.db;

import java.util.List;

import oso.ui.processing.picking.bean.PickUpSerialProduct;
import support.dbhelper.DatabaseHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpSerialProductDao {
	
	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup_serial_product;
	
	public PickUpSerialProductDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public PickUpSerialProductDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	
	public void addPickUp(List<PickUpSerialProduct> arrayList){
		if(arrayList != null && arrayList.size() > 0){
			try{
				db = helper.getWritableDatabase();
				db.beginTransaction();
				for(PickUpSerialProduct temp : arrayList){
					ContentValues values = new ContentValues();
					values.put("pcid", temp.getPcid()+"");
					values.put("sn", temp.getSn());
 					db.insert(tableName, null, values );
				}
				db.setTransactionSuccessful();
				
			}catch (Exception e) {
	 		}finally{
	 			db.endTransaction();
	 			db.close();
	 		}
		}
	}
	public PickUpSerialProduct getPickUpSerialProductBySn(String sn){
		PickUpSerialProduct pickUpSerialProduct = null ;
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase();
			cursor =	db.rawQuery("select * from " +tableName + " where sn = ? ", new String[]{sn});	
			if(cursor !=null && cursor.getCount() > 0){
				cursor.moveToNext();
				pickUpSerialProduct = new PickUpSerialProduct();
				pickUpSerialProduct.setId(cursor.getLong(cursor.getColumnIndex("id")));
				pickUpSerialProduct.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
				pickUpSerialProduct.setSn(cursor.getString(cursor.getColumnIndex("sn")));
			}
		}catch(Exception e){}
		finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return pickUpSerialProduct ;
		 
	}
}
