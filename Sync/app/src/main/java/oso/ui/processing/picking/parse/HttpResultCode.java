package oso.ui.processing.picking.parse;

public class HttpResultCode {
	public static final int MSG_SUCCESS = 0;  		//ϵͳ��̨���سɹ�
	public static final int MSG_FAILURE = 1; 		//��̨ϵͳ����
	public static final int MSG_NET_ERROR = 2 ; 	//�������(��������������糬ʱ)
	public static final int MSG_NO_DATA = 3 ; 		//�޷��ص����
	public static final int MSG_SN_REPEAT = 4 ; 	//�����ջ�,outbound��ʱ���ʱ��SN ���ظ������
 	
	public static final int MSG_PICK_UP_QTY_OVER = 5 ; // pickup��ʱ��ĳ����Ʒ����Ҫ�ĳ��������ύ�����
	public static final int MSG_POSITION_RECEVIED_NOT_OVER = 6 ; //position ��ɵ�ʱ��,���recevieû�����
	public static final int MSG_INBOUND_NO_DATA =7 ;
	public static final int MSG_CODE_HAD_BE_EXIST = 8;
	public static final int CONTAINER_AFFIRM = 9;
	public static final int CONTAINER_WORK   = 10; 
	public static final int CONTAINER_ISFULL_AND_QTY_MATCH =11  ; 
	public static final int CONTAINER_UNAVAILABLE = 12 ;
	public static final int CONTAINER_UNHANDLE = 13; 	
	public static final int CONTAINER_ISNOTTHISTILTE = 14 ; //容器不属于这个TITLE
	public static final int CONTAINER_LOTNUMBER_ERROR = 15 ; // lotNumber 错误(扫描到的Container不是这个应该有的LotNumber)
	public static final int CONTAINER_INNER_LOTNUMBER_DEFF_WORKCONTAINER = 16 ;	//inner Container 的lotNumber != workcontainer
	public static final int CONTAINER_LOTNUMBER_ISNULL = 17	;		//收货提交的时候没有对应的Container lotNumber
	public static final int CONTAINER_PRODUCT_NOT_INBOUND = 18 ; //	这次收货,不包含这个商品
	public static final int CONTAINER_NOT_FOUND = 19 ;	 //	
	public static final int CONTAINER_EMPTY = 20;//创建交货明细时，托盘不能为空
}
