package oso.ui.processing.picking;

import support.common.UIHelper;
import support.dbhelper.ConfigDao;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;
import declare.com.vvme.R;

public class NetBroadcastReceiver extends BroadcastReceiver{

	private Context outContext ; 
	
	private Resources outResources ;
	
	private ConfigDao configDao  ;
	
	public NetBroadcastReceiver(Context outContext ) {
		super();
		this.outContext = outContext;
		this.outResources = outContext.getResources() ;
		configDao = new ConfigDao(outContext);
	}

 
	@Override
	public void onReceive(Context context, Intent intent) {
 		ConnectivityManager connectMgr = (ConnectivityManager)outContext.getSystemService(outContext.CONNECTIVITY_SERVICE); 
		NetworkInfo mobNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); 
		NetworkInfo wifiNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI); 
		String netValue  = configDao.getConfigValue("net_work");
		if ((null != mobNetInfo && !mobNetInfo.isConnected()) || (null != wifiNetInfo && !wifiNetInfo.isConnected())) { 
			 configDao.updateConfigDao("net_work", "0");
 		}else { 
			 configDao.updateConfigDao("net_work", "1");
 		} 
		String netNowValue =  configDao.getConfigValue("net_work");
		if(!netValue.equals(netNowValue)){
			if(netNowValue.equals("1")){
	 			 UIHelper.showToast(outContext, outResources.getString(R.string.sys_yes_net), Toast.LENGTH_SHORT).show();
			}else{
	 			 UIHelper.showToast(outContext, outResources.getString(R.string.sys_no_net), Toast.LENGTH_SHORT).show();
			}
		}
		
		
		
	}

}
