package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import support.dbhelper.DatabaseHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpLocationProductDao {

	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup_location_product;
	
	public PickUpLocationProductDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public PickUpLocationProductDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	
 
	
	 
	 
 
	
	
	
	 
	 
	 
	 
	 
	 
	

	public List<PickUpLoactionProduct> getAllPickUpLoactionProducts(long pickup_id , long locationId){
		List<PickUpLoactionProduct> arrayList = null ;
		Cursor cursor =	null ;
		try{
			db = helper.getReadableDatabase();
			cursor = db.rawQuery(" select pickup_location_product.* ,pickup_product_info.name from pickup_location_product left join pickup_product_info on pickup_product_info.pid  = pickup_location_product.pcid where pickup_id = ? and slcId=? ", new String[]{pickup_id+"",locationId+""});
			arrayList = handCursor(cursor);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(cursor != null && !cursor.isClosed()){cursor.close();}
			if(db != null && db.isOpen()){db.close();}
		}
		
		 
		return arrayList;
	}
 	 
	 
	 
	 
	 
	 
	 
	 
	
	/**
	 * 查询某个位置上面的 && 某个PCID 需要拣货的
	 * @param pcid
	 * @param location_id
	 * @param pickup_id
	 * @return
	 */
	public List<PickUpLoactionProduct> getNeedPickUpLocationProductBy(long pcid, long location_id , long pickup_id){
		List<PickUpLoactionProduct> array = null ;
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase();	
			cursor = db.rawQuery(" select * from "+tableName + " where pcid=? and location_id = ? and pickup_id = ? ", new String[]{pcid+"",location_id+"",pickup_id+""});
			array = handCursor(cursor);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(cursor != null && !cursor.isClosed()){cursor.close();}
			if(db != null && db.isOpen()){db.close();}
		}
		return array ;
	}
	
	
	public List<PickUpLoactionProduct> handCursor(Cursor cursor){
		List<PickUpLoactionProduct> array = new ArrayList<PickUpLoactionProduct>();
		if(cursor != null && cursor.getCount() > 0){
 
			while(!cursor.isLast()){
				cursor.moveToNext();
				PickUpLoactionProduct loactionProduct = new PickUpLoactionProduct();
				loactionProduct.setId(cursor.getLong(cursor.getColumnIndex("id")));
				loactionProduct.setFrom_con_id(cursor.getLong(cursor.getColumnIndex("from_con_id")));
				loactionProduct.setFrom_container_type(cursor.getInt(cursor.getColumnIndex("from_container_type")));
				loactionProduct.setFrom_container_type_id(cursor.getLong(cursor.getColumnIndex("from_container_type_id")));
				loactionProduct.setFrom_submit_qty(cursor.getDouble(cursor.getColumnIndex("from_submit_qty")));
				loactionProduct.setFromLocationId(cursor.getLong(cursor.getColumnIndex("from_location_id")));
				loactionProduct.setLocation(cursor.getString(cursor.getColumnIndex("location")));
				loactionProduct.setLocation_id(cursor.getLong(cursor.getColumnIndex("location_id")));
				loactionProduct.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
				loactionProduct.setPick_con_id(cursor.getLong(cursor.getColumnIndex("pick_con_id")));
				loactionProduct.setPick_container_Type(cursor.getInt(cursor.getColumnIndex("pick_container_Type")));
				loactionProduct.setPick_container_Type_id(cursor.getLong(cursor.getColumnIndex("pick_container_Type_id")));
				loactionProduct.setPickup_id(cursor.getLong(cursor.getColumnIndex("pickup_id")));
				loactionProduct.setQty(cursor.getDouble(cursor.getColumnIndex("qty")));
				loactionProduct.setSlcId(cursor.getLong(cursor.getColumnIndex("slcId")));
				loactionProduct.setPickContainerQty(cursor.getInt(cursor.getColumnIndex("pick_container_qty")));
				
				loactionProduct.setSn(cursor.getString(cursor.getColumnIndex("sn")));
				int nameIndex = cursor.getColumnIndex("name") ;
				if(nameIndex != -1){
					loactionProduct.setPname(cursor.getString(nameIndex));
				}
 				array.add(loactionProduct);
	  		}
		}
		return array ;
	}
}
