package oso.ui.processing.specialproject.view;

import oso.ui.processing.specialproject.bean.MainTaskBean;
import oso.ui.processing.specialproject.bean.SubTaskBean;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * Special Task HeaderViews
 * @author xialimin
 */
public class Special_TaskInfoHeaderView {
	
	public static View getHeaderViewLayout(Context ctx, MainTaskBean taskBean) {
		View view = View.inflate(ctx, R.layout.wms_specialtask_header_layout, null);
		initData(ctx, view, taskBean);
		return view;
	}
	
	private static void initData(Context context, View view, MainTaskBean taskBean) {
		
	}

	public static View getHeaderViewLayout_Project(Context mContext, MainTaskBean taskBean) {
		View view = View.inflate(mContext, R.layout.wms_specialtask_maintask_header_layout, null);

		if(taskBean == null) return view;
		((TextView)view.findViewById(R.id.tx_sp_projectno)).setText(taskBean.project_detail.schedule_id+"");
		((TextView)view.findViewById(R.id.tx_project_subject)).setText(taskBean.project_detail.subject+"");
		((TextView)view.findViewById(R.id.tx_project_startdate)).setText(taskBean.project_detail.start_time+"");
		((TextView)view.findViewById(R.id.tx_project_enddate)).setText(taskBean.project_detail.end_time+"");
		
		return view;
	}
	
	public static View getHeaderViewLayout_SubTask(Context mContext, SubTaskBean taskBean) {
		View view = View.inflate(mContext, R.layout.wms_specialtask_subtask_header_layout, null);

		if(taskBean == null) return view;
		// Main Task No
		((TextView) view.findViewById(R.id.tx_sp_mainno)).setText(taskBean.main_detail.task_no + "");
		// Task Detail
		((TextView)view.findViewById(R.id.tx_sp_subdes)).setText(taskBean.main_detail.description+"");
		// Start Date
		((TextView)view.findViewById(R.id.tx_subtask_startdate)).setText(taskBean.main_detail.start_time+"");
		// End Date
		((TextView)view.findViewById(R.id.tx_subtask_enddate)).setText(taskBean.main_detail.end_time+"");
		
		return view;
	}
}
