package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.bean.ProductCode;
import oso.ui.processing.picking.bean.SkuModelDriven;
import support.dbhelper.DatabaseHelper;
import support.exception.SystemException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ProductCodeDao {

	protected DatabaseHelper helper;
		
	private String tableName = DatabaseHelper.tbl_inbound_product_code; 
	
	protected SQLiteDatabase db;

	
	public ProductCodeDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public ProductCodeDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	public void appendProductCode(List<ProductCode> arrayList)throws Exception {
		if(arrayList != null && arrayList.size() > 0 ){
			db = helper.getWritableDatabase();
			try{
				db.beginTransaction();
 				for(ProductCode productCode : arrayList ){
					ContentValues contentValue = new ContentValues();
					contentValue.put("pcid", productCode.getPcid());
					contentValue.put("code_type", productCode.getCodeType());
					contentValue.put("barcode", productCode.getBarcode());
					contentValue.put("p_name", productCode.getP_name());
					db.insert(tableName, null, contentValue);
				}
				db.setTransactionSuccessful();
			}catch (Exception e) {
				throw new SystemException();
 			}finally{
 				db.endTransaction();
 				db.close();
 			}
		}
	}
	public void deleteAll() throws Exception {
		 
			db = helper.getWritableDatabase();
			db.execSQL("delete from " + tableName); 
			db.close();
		 
	}
	public void addProductCode(List<ProductCode> arrayList) throws Exception {
		if(arrayList != null && arrayList.size() > 0 ){
			db = helper.getWritableDatabase();
			try{
				db.beginTransaction();
				db.execSQL("delete from " + tableName);
				for(ProductCode productCode : arrayList ){
					ContentValues contentValue = new ContentValues();
					contentValue.put("pcid", productCode.getPcid());
					contentValue.put("code_type", productCode.getCodeType());
					contentValue.put("barcode", productCode.getBarcode());
					contentValue.put("p_name", productCode.getP_name());
					db.insert(tableName, null, contentValue);
				}
				db.setTransactionSuccessful();
			}catch (Exception e) {
				throw new SystemException();
 			}finally{
 				db.endTransaction();
 				db.close();
 			}
		}
	}
	public ProductCode getProductBySku(String sku) {
		ProductCode temp = null ;
		db = helper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from " + tableName + " where barcode = ?", new String[]{sku.toUpperCase()});
		if(cursor != null && cursor.getCount() > 0){
			cursor.moveToFirst();
			temp = new ProductCode();
			temp.setBarcode(cursor.getString(cursor.getColumnIndex("barcode")));
			temp.setId(cursor.getLong(cursor.getColumnIndex("id")));
			temp.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")) );
			temp.setCodeType(cursor.getString(cursor.getColumnIndex("code_type")));
			temp.setP_name(cursor.getString(cursor.getColumnIndex("p_name")));
		}
		cursor.close();
		db.close();
		return temp;
	} 
	//��ѯͳ�� �� barcode ��������� ReceiveInfo 
	public List<SkuModelDriven> getskuModelDriven(long transport_id ) {
		StringBuffer sql = new StringBuffer("select * from (select sku, sum(quantity) as sum_quantity from  receive_info where transport_id = "+transport_id+" group by sku) filter left join product_code on  product_code.pcid = filter.sku");
		Log.i("sql", sql.toString());
		db = helper.getReadableDatabase();
		List<SkuModelDriven> arrayList = new ArrayList<SkuModelDriven>();
		Cursor cursor =	db.rawQuery(sql.toString(),null);
		while(cursor.getCount()> 0 && !cursor.isLast()){
			cursor.moveToNext();
			SkuModelDriven skuTemp = new SkuModelDriven();
			skuTemp.setBarcode(cursor.getString(cursor.getColumnIndex("barcode")));
			skuTemp.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
			skuTemp.setQty(cursor.getInt(cursor.getColumnIndex("sum_quantity")));
			arrayList.add(skuTemp);
		}
		cursor.close();
		db.close();
		return arrayList ;
	}
	public ProductCode getMainCodeByPid(long pid){
		ProductCode code = new ProductCode() ;
		db = helper.getReadableDatabase() ;
		Cursor cursor = db.rawQuery("select * from " + tableName + " where pcid = ? and code_type = ? ", new String[]{pid+"",1+""});
		if(cursor != null && cursor.getCount() > 0){
			cursor.moveToFirst() ;
			code.setBarcode(cursor.getString(cursor.getColumnIndex("barcode")));
			code.setCodeType(cursor.getString(cursor.getColumnIndex("code_type")));
			code.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
			code.setP_name(cursor.getString(cursor.getColumnIndex("p_name")));
			code.setId(cursor.getLong(cursor.getColumnIndex("id")));
		}
		cursor.close();
		db.close();
		return code ;
	}
 
	
}
