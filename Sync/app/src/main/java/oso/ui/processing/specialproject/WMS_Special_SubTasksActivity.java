package oso.ui.processing.specialproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.processing.specialproject.adapter.SubTasksAdp;
import oso.ui.processing.specialproject.bean.SubTaskBean;
import oso.ui.processing.specialproject.bean.SubTaskBean.SubTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import oso.ui.processing.specialproject.view.Special_TaskInfoHeaderView;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import support.anim.AnimUtils;
import support.common.UIHelper;
import support.key.BCSKey;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * 
 * @description:Sepcial Project 主任务列表
 *
 * @author xialimin
 * @time : 2015-6-11 下午5:05:33
 */
public class WMS_Special_SubTasksActivity extends BaseActivity implements View.OnClickListener {

	public static final String SCHEDULE_ID = "schedule_id";

	public static final String MAIN_TASK_STATUS = "main_task_status";

	private static final int ASSIGN_TYPE_SINGLE = 0;
	
	private static final int ASSIGN_TYPE_ALL = 1;

	private Context mContext;

	private View mHeaderView, mTitleView;

	private ListView mTaskLv;

	private SubTasksAdp mTaskAdp;
	
	private SubTaskBean mTask;
	
	private Button mAssignBtn;

	private String scheduleId;

	private SubTask currSubTask; // 选人时, 被选的子任务临时对象

	private int mainTaskStatus, assignType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_special_project_subtask_list, 0);
		
		applyParams();
		
		initView();
		
		initData();
		
		initListener();

	}

	@Override
	protected void onResume() {
		super.onResume();

		if(currSubTask != null) {
			if(!TextUtils.isEmpty(currSubTask.assignLaborsAdids)) return;
			currSubTask.status = false;
			mTaskAdp.notifyDataSetChanged();
		}

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case ChooseUsersActivity.requestCode:
			handleData(data, assignType);
			break;
		}
	}
	
	
	/**
	 * 处理选人返回的数据
	 * @param data
	 */
	private void handleData(Intent data, int assignType) {
		// TODO
		if (data == null) return;
		String selectedNamesStr = data.getStringExtra("select_user_names");
		String selectedAdidsStr = data.getStringExtra("select_user_adids");
		
		if (ASSIGN_TYPE_SINGLE == assignType) {

			// 如果选人为空，并且def 默认是有人员的，则置为true
			if (TextUtils.isEmpty(selectedAdidsStr) && !(TextUtils.isEmpty(currSubTask.schedule_execute_id))) {
				currSubTask.isUserModified = true;
			}

			if (!TextUtils.isEmpty(selectedAdidsStr) && !(TextUtils.isEmpty(currSubTask.schedule_execute_id))) {
				currSubTask.isUserModified = mTask.isUserModified(selectedAdidsStr, currSubTask.schedule_execute_id);
			}

			currSubTask.assignLabors = selectedNamesStr;
			currSubTask.assignLaborsAdids = selectedAdidsStr;

			// 点击状态
			currSubTask.status = !TextUtils.isEmpty(selectedAdidsStr);
			mTaskAdp.notifyDataSetChanged();
			showAssignBtn();
			return;
		}
		
		if(ASSIGN_TYPE_ALL == assignType) {
			int size = mTask.main_detail.subtasks.size();
			for(int i = 0; i < size; i ++) {
				SubTask subTask = mTask.main_detail.subtasks.get(i);

				if(subTask.task_status != SpecialTaskKey.TYPE_OPEN) {
					continue;
				}

				subTask.assignLabors = selectedNamesStr;
				subTask.assignLaborsAdids = selectedAdidsStr;

				// 点击状态
				subTask.status = !TextUtils.isEmpty(selectedAdidsStr);
			}
			mTaskAdp.notifyDataSetChanged();
			showAssignBtn();
			return;
		}
		
		mTaskAdp.notifyDataSetChanged();

		showAssignBtn();
	}
	
	/**
	 * 显示分配任务按钮
	 */
	private void showAssignBtn() {
		
		int size = mTask.main_detail.subtasks.size();
		List<SubTask> subtasks = mTask.main_detail.subtasks;
		boolean isShouldShow = false;

		for(int i=0; i<size; i++) {

			if((!TextUtils.isEmpty(subtasks.get(i).assignLaborsAdids) &&
					(subtasks.get(i).task_status == SpecialTaskKey.TYPE_OPEN || subtasks.get(i).task_status == SpecialTaskKey.TYPE_INPROCESS))) {
				isShouldShow = true;
				break;
			}

			if((!TextUtils.isEmpty(subtasks.get(i).schedule_execute_id) &&
					(subtasks.get(i).task_status == SpecialTaskKey.TYPE_OPEN || subtasks.get(i).task_status == SpecialTaskKey.TYPE_INPROCESS))) {
				isShouldShow = true;
				break;
			}

		}

		boolean allClosed = true;
		for(int j=0; j<size; j++) {
			if(subtasks.get(j).task_status != SpecialTaskKey.TYPE_CLOSED) {
				allClosed = false;
			}
		}
		if(allClosed) {
			mAssignBtn.setVisibility(View.GONE);
			return;
		}
		mAssignBtn.setVisibility(isShouldShow ? View.VISIBLE : View.GONE);
		
	}


	private void initView() {
		
		mContext = this;
		
		setTitleString(getString(R.string.specialtask_subtask_title));
		
		mAssignBtn = (Button) findViewById(R.id.btnAssignLabors);
		mTaskLv = (ListView) findViewById(R.id.lv_task);
		TextView emptyView = (TextView) findViewById(R.id.tvTaskEmpty);
		mTaskLv.setEmptyView(emptyView);

		initEmptyTitleView();
		// TaskInfo Layout
		mHeaderView = Special_TaskInfoHeaderView.getHeaderViewLayout_SubTask(mContext, mTask);
		mTaskLv.addHeaderView(mHeaderView);
		setHeaderListener(mHeaderView);
		
		mTaskAdp = new SubTasksAdp(mContext, mTask);
		mTaskLv.setAdapter(mTaskAdp);

	}

	private void initEmptyTitleView() {
		mTitleView = findViewById(R.id.layout_header);
		mTitleView.setVisibility(View.GONE);
		mTitleView.findViewById(R.id.btnSpMoreTool).setOnClickListener(this);
		mTitleView.findViewById(R.id.layout_MoreTool).setOnClickListener(this);
	}

	/**
	 * HeaderView的点击事件
	 * @param headerView
	 */
	private void setHeaderListener(View headerView) {
		headerView.findViewById(R.id.btnSpMoreTool).setOnClickListener(this);
		headerView.findViewById(R.id.layout_MoreTool).setOnClickListener(this);
	}

	private void initData() {
		reqSubMainTask(scheduleId);
	}
	
	private void initListener() {
		
		// 刷新
		showRightButton(R.drawable.btn_ref_style, "", new OnRefreshListener());
		
		findViewById(R.id.topbar_title).setOnClickListener(this);
		mAssignBtn.setOnClickListener(this);
		mTaskLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				//----------已经是Pending的不支持操作-------
				int realPos = position - mTaskLv.getHeaderViewsCount();
				if(realPos < 0) return;

				if(mTask.main_detail.subtasks.get(realPos).task_status == SpecialTaskKey.TYPE_PENDING) {
					UIHelper.showToast(mContext, getString(R.string.specialtask_status_pending));
					return;
				}

                if(mTask.main_detail.subtasks.get(realPos).task_status == SpecialTaskKey.TYPE_CLOSED) {
                    UIHelper.showToast(mContext, getString(R.string.special_task_status_closed));
                    return;
                }

				if(mTask.main_detail.subtasks.get(realPos).task_status != SpecialTaskKey.TYPE_OPEN) {
					UIHelper.showToast(mContext, getString(R.string.specialtask_status_already_assigned));
					return;
				}

				currSubTask = (SubTask) parent.getAdapter().getItem(position);
				// 图标状态改变
				currSubTask.status = true;
				mTaskAdp.notifyDataSetChanged();

				assignLabors(currSubTask);
			}

		});
		
		mTaskLv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				int realPos = position - mTaskLv.getHeaderViewsCount();
				if (realPos < 0) return true;

				//----------已经是Pending的不支持操作-------
				// TODO
				if (mTask.main_detail.subtasks.get(realPos).task_status == SpecialTaskKey.TYPE_PENDING) {
					UIHelper.showToast(mContext, getString(R.string.specialtask_status_pending));
					return true;
				}

				if (mTask.main_detail.subtasks.get(realPos).task_status == SpecialTaskKey.TYPE_CLOSED) {
					UIHelper.showToast(mContext, getString(R.string.special_task_status_closed));
					return true;
				}

				if (mTask.main_detail.subtasks.get(realPos).task_status == SpecialTaskKey.TYPE_INPROCESS) {
					showSingleCloseMenuDialog(position);
					return true;
				}

				/*if (mTask.main_detail.subtasks.get(realPos).task_status != SpecialTaskKey.TYPE_OPEN) {
					UIHelper.showToast(mContext, getString(R.string.specialtask_status_already_assigned));
					return true;
				}*/

				showSlcDialog(position);
				return true;
			}
		});
	}
	
	/**
	 * 分配任务
	 * @param subTask
	 */
	private void assignLabors(SubTask subTask) {
		assignType = ASSIGN_TYPE_SINGLE;
		
		startChooseUserAct(subTask);
	}
	
	/**
	 * 所有子任务一次性分配
	 */
	private void assignAll() {
		assignType = ASSIGN_TYPE_ALL;
		
		startChooseUserAct(null);
		
	}
	
	/**
	 * @param subTask  如果传入null, 则默认给所有subTask 一同进行选人
	 */
	private void startChooseUserAct(SubTask subTask) {
		if(subTask == null) subTask = new SubTask();
		Intent intent = new Intent(mActivity, ChooseUsersActivity.class);
		
		// TODO
		if(ASSIGN_TYPE_ALL == assignType) {
			//intent.putExtra("selected_adids", mTask.main_detail.subtasks.get(0).assignLaborsAdids);
		} else {

			intent.putExtra("selected_adids", getChooseAdidsByTask(subTask));
		}
		
		intent.putExtra("role_ids", BaseDataDepartmentKey.Warehouse);
		intent.putExtra("single_select", false);
		intent.putExtra("select_key", ChooseUsersActivity.KEY_SPECIALTASK_ASSIGN);
		intent.putExtra("proJsId", BaseDataSetUpStaffJobKey.Employee);
		ChooseUsersActivity.setSubmitBtnAutoshow(false);
		startActivityForResult(intent, ChooseUsersActivity.requestCode);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	/**
	 * 选人时携带的人员的adids
	 * @param subTask
	 * @return
	 */
	private String getChooseAdidsByTask(SubTask subTask) {

		if (subTask.isUserModified) {
			return subTask.assignLaborsAdids;
		}

		if (!TextUtils.isEmpty(subTask.assignLaborsAdids)) {
			return subTask.assignLaborsAdids;
		}

		if (!TextUtils.isEmpty(subTask.schedule_execute_id)) {
			return subTask.schedule_execute_id;
		}

		return "";
	}

	/*public static void initParams(Intent in, SubTaskBean task) {
		in.putExtra("SubTask", task);
	}*/

	public static void initParams(Intent in, String schedule_id, int main_task_status) {
		in.putExtra(SCHEDULE_ID, schedule_id);
		in.putExtra(MAIN_TASK_STATUS, main_task_status);
	}
	
	private void applyParams() {
		scheduleId = getIntent().getStringExtra(SCHEDULE_ID);
		mainTaskStatus = getIntent().getIntExtra(MAIN_TASK_STATUS, 1);
	}
	
	/**
	 * 菜单Dialog 
	 * @param position
	 * 
	 * Edit 和 Delete
	 */
	private void showSlcDialog(final int position) {
		new RewriteBuilderDialog.Builder(WMS_Special_SubTasksActivity.this).setTitle(getString(R.string.sync_operation))
		.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).setArrowItems(new String[]{getString(R.string.specialtask_operate_edit), getString(R.string.specialtask_operate_delete), getString(R.string.specialtask_operate_close)},
				new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
						onSlcDialogItemClick(position, itemPosition);
					}
				}).show();
	}

	/**
	 * 菜单Dialog
	 * @param position
	 *
	 * 只有Close的Dialog
	 */
	private void showSingleCloseMenuDialog(final int position) {
		new RewriteBuilderDialog.Builder(WMS_Special_SubTasksActivity.this).setTitle(getString(R.string.sync_operation))
				.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setArrowItems(new String[]{getString(R.string.specialtask_operate_close)},
				new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
						showCloseTaskDlg(position);
					}
				}).show();
	}
	
	/**
	 * Operate Dialog 选择选项后
	 * @param position     数据源的position
	 * @param itemPosition
	 */
	private void onSlcDialogItemClick(int position, int itemPosition) {
		switch (itemPosition) {
		case SpecialTaskKey.OPERATE_EDIT:
			showEditDlg(position);
			break;
		case SpecialTaskKey.OPERATE_DELETE:
			showDelDlg(position);
			break;
		case SpecialTaskKey.OPERATE_CLOSE_TASK:
			showCloseTaskDlg(position);
			break;
		case SpecialTaskKey.OPERATE_REOPEN_TASK:
			showReopenDlg(position);
			break;
		}					
	}

	/**
	 * 重新打开已完毕的任务 确认Dialog
	 * @param position
	 */
	private void showReopenDlg(final int position) {
		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.specialtask_info_close), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int realPos = position - mTaskLv.getHeaderViewsCount();
				if(realPos < 0) return;

				//TODO 需要请求
				//---关闭----
				final SubTask task = mTask.main_detail.subtasks.get(realPos);
				reqReopenSubTask(task);
			}
		}).show();
	}

	/**
	 * 关闭子任务
	 * @param position
	 */
	private void showCloseTaskDlg(final int position) {
		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.specialtask_info_close), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int realPos = position - mTaskLv.getHeaderViewsCount();
				if(realPos < 0) return;

				//TODO 需要请求
				//---关闭----
				final SubTask task = mTask.main_detail.subtasks.get(realPos);
				closeSubTask(task);
			}
		}).show();
	}
	/**
	 * 删除子任务
	 * @param position
	 */
	private void showDelDlg(final int position) {

		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.specialtask_info_delete), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int realPos = position - mTaskLv.getHeaderViewsCount();
				if(realPos < 0) return;
				//---删除----
				final SubTask task = mTask.main_detail.subtasks.get(realPos);
				reqDelSubTask(task);
			}
		}).show();
	}

	/**
	 * 请求删除子任务
	 * @param task
	 */
	private void reqDelSubTask(final SubTask task) {
		RequestParams params = new RequestParams();
		params.put("key", "delSubTask");
		params.put("del", getReqDelSubtaskJson(task));

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				if (BCSKey.Err_WithMsg == json.optInt("err")) {
					task.task_status = json.optInt("task_status", task.task_status);
					if(task.task_status == SpecialTaskKey.TYPE_ASSIGNED) {
						UIHelper.showToast(mContext, getString(R.string.specialtask_info_task_assigned));
					} else {
						UIHelper.showToast(mContext, json.optString("data"));
					}
					mTaskAdp.notifyDataSetChanged();
					return;
				};

				mTask.main_detail.subtasks.remove(task);
				mTaskAdp.notifyDataSetChanged();
				mTitleView.setVisibility(Utility.isNullForList(mTask.main_detail.subtasks) ? View.VISIBLE : View.GONE);
				showAssignBtn();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

	}

	/**
	 * 添加任务的请求JSON
	 * @param task
	 * @return
	 */
	private String  getReqDelSubtaskJson(final SubTask task) {
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		try {
			obj.put("task_id", task.schedule_id);
			obj.put("task_detail", task.description);
			arr.put(obj);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return arr.toString();
	}

	/**
	 * 请求重新打开已经关闭的子任务
	 * @param task
	 */
	private void reqReopenSubTask(final SubTask task) {
		RequestParams params = new RequestParams();
		params.put("key", "reopenSubTask");
		params.put("task_id", task.schedule_id);
		params.put("task_status", task.task_status);

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				mTask.main_detail.subtasks.remove(task);
				mTaskAdp.notifyDataSetChanged();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

	}

	/**
	 * 请求删除子任务
	 * @param task
	 */
	private void reqUpdateSubTask(final SubTask task,final String newDes) {
		RequestParams params = new RequestParams();
		params.put("key", "updateSubTask");
		params.put("task_id", task.schedule_id);
		params.put("task_detail", newDes);

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				if (BCSKey.Err_WithMsg == json.optInt("err")) {
					UIHelper.showToast(mContext, json.optString("data"));
					return;
				};

				task.description = newDes;
				mTaskAdp.notifyDataSetChanged();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

	}

	/**
	 * 修改子任务
	 * @param position
	 */
	private void showEditDlg(int position) {
		int realPos = position - mTaskLv.getHeaderViewsCount();
		if(realPos < 0) return;
		
		//TODO
		final SubTask task = mTask.main_detail.subtasks.get(realPos);

		// 判断是否可修改
		if((task.task_status != SpecialTaskKey.TYPE_OPEN) && (task.task_status != SpecialTaskKey.TYPE_ASSIGNED)) {
			UIHelper.showToast(mContext, getString(R.string.specialtask_info_cannot_edit));
			return;
		}
		
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
//		final EditText etSubject = (EditText) view.findViewById(R.id.e_subject);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
//		etSubject.setText(task.subTaskName);
		etDescription.setText(task.description);
//		etCusorToLast(etSubject);
		etCusorToLast(etDescription);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle(getString(R.string.specialtask_update_task_title));
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
			String descriptionStr = etDescription.getText().toString();
			if(!isValid(task, etDescription)) return;

			dlg.dismiss();
			reqUpdateSubTask(task, descriptionStr);
		}

		}).show();
	}

	/**
	 * edittext 光标移动到最后
	 * @param et
	 */
	private void etCusorToLast(EditText et) {
		String text = et.getText().toString();
		if(TextUtils.isEmpty(text)) return;
		int cursorIndex = text.length();
		et.setSelection(cursorIndex);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btnSpMoreTool:
			showMoreToolsDlg();
            break;

		case R.id.layout_MoreTool:
			showMoreToolsDlg();
			break;

		case R.id.btnAssignLabors:
			verifyAssign();
			break;
			
		}
	}

	/**
	 * 验证Assign，符合条件则提交Assign数据
	 */
	private void verifyAssign() {

		// 列表为空
		if(StringUtil.isNullForJSONArray(mTask.getAssignSubmitJson())) {
			UIHelper.showToast(mContext, getString(R.string.sync_empty));
			return;
		}


		// Assign确认
		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.specialtask_info_assign), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				submitAssign();
			}
		}).show();
	}

	/**
	 * More Tool Dialog  包括Assign all 和 添加子任务
	 */
	private void showMoreToolsDlg() {
		boolean isDataEmpty = mTask.main_detail.subtasks.size() == 0;
		String [] menu = {getString(R.string.specialtask_operate_assign_all) , getString(R.string.specialtask_operate_close_all), getString(R.string.specialtask_operate_add_subtask)};
		String [] emptyMenu = {getString(R.string.specialtask_operate_add_subtask)};

		if(isDataEmpty) {
			new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_operation)).setNegativeButton(getString(R.string.sync_cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}

					}).setArrowItems(emptyMenu, new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
					switch (itemPosition) {
						case SpecialTaskKey.MENU_EMPTY_SUBTASK_ADD_SUBTASK:
							onAddSubtaskClick();
							break;
					}
				}

			}).show();
		} else {

			new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_operation)).setNegativeButton(getString(R.string.sync_cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}

					}).setArrowItems(menu, new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
					switch (itemPosition) {
						case SpecialTaskKey.MENU_SUBTASK_ASSIGN_ALL:
							onAssignAllClick();
							break;
						case SpecialTaskKey.MENU_SUBTASK_CLOSE_ALL:
							onCloseAllClick();
							break;
						case SpecialTaskKey.MENU_SUBTASK_ADD_SUBTASK:
							onAddSubtaskClick();
							break;
					}
				}

			}).show();

		}


	}

	/**
	 * menu assign all
	 */
	private void onAssignAllClick() {
		if (mTask.isAllClosed()) {
			UIHelper.showToast(mContext, "Already closed!");
		} else if (mTask.isAllAssigned()) {
			UIHelper.showToast(mContext, "Already assigned all!");
		} else {
			assignAll();
		}
	}

	/**
	 * menu close all
	 */
	private void onCloseAllClick() {
		if (mTask.isAllClosed()) {
			UIHelper.showToast(mContext, "Already closed!");
		} else {
			showCloseAllDlg();
		}
	}

	/**
	 * menu add subtask
	 */
	private void onAddSubtaskClick() {

		if (mainTaskStatus == SpecialTaskKey.TYPE_CLOSED) {
			UIHelper.showToast(mContext, getString(R.string.specialtask_info_maintask_already_closed));
			return;
		}

		if(mTask.isAllClosed()) {
			UIHelper.showToast(mContext, getString(R.string.specialtask_info_maintask_already_closed));
			return;
		}

		showAddSubTaskDlg();

	}

	private void showCloseAllDlg() {
		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.specialtask_info_close_all), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				closeAllSubtasks();
			}
		}).show();
	}


	/**
	 * 提交分配任务
	 */
	private void submitAssign() {
		RequestParams params = new RequestParams();
		params.put("key", "assignSubTask");
		params.put("main_task_status", mainTaskStatus); // 所属主任务请求之前的状态
		params.put("main_task_id", mTask.main_detail.task_no);
		params.put("start_time", mTask.main_detail.start_time);
		params.put("end_time", mTask.main_detail.end_time);
		params.put("assignJson", mTask.getAssignSubmitJson().toString());
		
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				// 已经分配的需要将type 置为 in-process
				mTask.updateAssignStatus();

				mTaskAdp.notifyDataSetChanged();

				UIHelper.showToast(mContext, mContext.getString(R.string.sync_success));

				mAssignBtn.setVisibility(mTask.hasChecked() ? View.VISIBLE : View.GONE);

			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);
		
	}

	/**
	 * 刷新监听器
	 */
	private class OnRefreshListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			String scheduleId = mTask.main_detail.task_no;
			//int taskStatus = mTask.main_detail.main_task_status;
			reqSubMainTask(scheduleId);
		}
	}

	/**
	 * 添加子任务
	 */
	private class OnAddSubBtnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			showAddSubTaskDlg();
		}

	}

	/**
	 * 添加子任务对话框
	 */
	private void showAddSubTaskDlg() {
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle(getString(R.string.specialtask_operate_add_subtask));
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
			String descriptionStr = etDescription.getText().toString();
			if(!isValid(null, etDescription)) return;
			reqAddSubTask(descriptionStr);
			
			dlg.dismiss();
		}

		}).show();

	}

	/**
	 * 根据schedule_id 请求SubTask
	 * @param schedule_id
	 */
	private void reqSubMainTask(String schedule_id) {
		RequestParams params = new RequestParams();
		params.put("key", "getSubTaskInformationByMainId");
		params.put("schedule_id", schedule_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				mTask = new Gson().fromJson(json.toString(), SubTaskBean.class);
				mTaskAdp = new SubTasksAdp(mContext, mTask);
				mTaskLv.setAdapter(mTaskAdp);

				if(Utility.isNullForList(mTask.main_detail.subtasks)) {
					mTitleView.setVisibility(View.VISIBLE);
				}

				// 重置headerView数据
				setHeaderViewData();

				showAssignBtn();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

	}

	/**
	 * 设置HeaderView数据
	 */
	private void setHeaderViewData() {
		if (mHeaderView == null) return;
		if (mTask == null && mTask.main_detail == null) return;
		// Main Task No
		((TextView)mHeaderView.findViewById(R.id.tx_sp_mainno)).setText(mTask.main_detail.task_no+"");
		// Task Detail
		((TextView)mHeaderView.findViewById(R.id.tx_sp_subdes)).setText(mTask.main_detail.description + "");
		// Start Date
		((TextView)mHeaderView.findViewById(R.id.tx_subtask_startdate)).setText(mTask.main_detail.start_time + "");
		// End Date
		((TextView)mHeaderView.findViewById(R.id.tx_subtask_enddate)).setText(mTask.main_detail.end_time + "");

		if (mTitleView == null) return;
		// Main Task No
		((TextView)mTitleView.findViewById(R.id.tx_sp_mainno)).setText(mTask.main_detail.task_no+"");
		// Task Detail
		((TextView)mTitleView.findViewById(R.id.tx_sp_subdes)).setText(mTask.main_detail.description + "");
		// Start Date
		((TextView)mTitleView.findViewById(R.id.tx_subtask_startdate)).setText(mTask.main_detail.start_time + "");
		// End Date
		((TextView)mTitleView.findViewById(R.id.tx_subtask_enddate)).setText(mTask.main_detail.end_time + "");

	}

	/**
	 * 关闭子任务
	 */
	private void closeSubTask(final SubTask subtask) {
		JSONArray arr = new JSONArray();
		arr.put(subtask.schedule_id);
		RequestParams params = new RequestParams();
		params.put("key", "closeSubTask");
		params.put("completeTaskIds", arr.toString());
		params.put("mainTaskStatus", mainTaskStatus+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				// 获得主任务的状态
				mainTaskStatus = json.optInt("maintaskstatus");
				subtask.task_status = SpecialTaskKey.TYPE_CLOSED;
				mTaskAdp.notifyDataSetChanged();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}

	/**
	 * 关闭全部子任务
	 */
	private void closeAllSubtasks() {
		JSONArray arr = new JSONArray();
		for(SubTask subTask : mTask.main_detail.subtasks) {
			if(subTask.task_status != SpecialTaskKey.TYPE_CLOSED) {
				arr.put(subTask.schedule_id);
			}
		}
		RequestParams params = new RequestParams();
		params.put("key", "closeSubTask");
		params.put("completeTaskIds", arr.toString());
		params.put("mainTaskStatus", mainTaskStatus+"");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				mainTaskStatus = json.optInt("maintaskstatus");
				for(SubTask subTask : mTask.main_detail.subtasks) {
					if(subTask.task_status != SpecialTaskKey.TYPE_CLOSED) {
						subTask.task_status = SpecialTaskKey.TYPE_CLOSED;
					}
				}
				mTaskAdp.notifyDataSetChanged();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}

	/**
	 * 请求添加SubTask
	 * @param descriptionStr
	 */
	private void reqAddSubTask(String descriptionStr) {

		RequestParams params = new RequestParams();
		params.put("key", "insertSubTask");
		params.put("main_task_id", mTask.main_detail.task_no);
		params.put("add", getReqAddSubtaskJson(descriptionStr));

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				if (BCSKey.Err_WithMsg == json.optInt("err", 0)) {
					UIHelper.showToast(mContext, json.optString("data"));
					return;
				};

				String jsonStr="";
				try {
					jsonStr = json.getJSONArray("sub_tasks").get(0).toString();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				SubTask subTask = new Gson().fromJson(jsonStr, SubTask.class);
				mTask.main_detail.subtasks.add(subTask);
				mTaskAdp.notifyDataSetChanged();
				mTaskLv.smoothScrollToPosition(mTask.main_detail.subtasks.size() + mTaskLv.getHeaderViewsCount());

				/**
				 * 子任务 0 --> add mTitleView需要隐藏
				 */
				mTitleView.setVisibility(Utility.isNullForList(mTask.main_detail.subtasks) ? View.VISIBLE : View.GONE);
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

	}

	/**
	 * 添加任务的请求JSON
	 * @param descriptionStr
	 * @return
	 */
	private String  getReqAddSubtaskJson(String descriptionStr) {
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		int cnt = mTask.getSubtaskCount();
		try {
			obj.put("task_detail", descriptionStr);
			obj.put("task_order_no" , ++ cnt);
			arr.put(obj);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return arr.toString();
	}

	/**
	 * 判断输入值是否非法
	 * @param tvDes  传入TextView 用于调用动画
	 * @return
	 */
	private boolean isValid(SubTask subTask, TextView tvDes) {
		boolean isValid = true;
		if(TextUtils.isEmpty(tvDes.getText().toString())) {
			AnimUtils.horizontalShake(mContext, tvDes);
			return false;
		}

		if(subTask == null) {
			return isValid;
		}

		if(tvDes.getText().toString().equals(subTask.description)) {
			AnimUtils.horizontalShake(mContext, tvDes);
			UIHelper.showToast(mContext, getString(R.string.specialtask_info_nochange));
			return false;
		}
		return isValid;
	}

}
