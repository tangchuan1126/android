package oso.ui.processing.picking.runnable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import oso.ui.processing.picking.bean.PickUpDifferentContainerEquals;
import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpSubmitLocationDifferent;
import oso.ui.processing.picking.bean.ReturnBean;
import oso.ui.processing.picking.db.PickUpLocationProductDao;
import oso.ui.processing.picking.parse.HttpResultCode;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import support.dbhelper.ConfigDao;
import support.network.HttpPostMethod;
import support.network.HttpTool;
import utility.FileUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class SubmitDifferentRunnable implements Runnable {
	 
	private Context context ;
	private long pcid ;
	private long location_id ;
	private long pickup_id ;
	private double totalQty ;
	private String location ;
	private PickUpLocationProductDao pickUpLocationProductDao ;
	private ConfigDao configDao ;
	private Handler handler ;
	
	public SubmitDifferentRunnable(Context context, long pcid,
			long location_id, long pickup_id , double totalQty , String location , Handler handler) {
		super();
		this.context = context;
		this.pcid = pcid;
		this.location_id = location_id;
		this.pickup_id = pickup_id;
		this.pickUpLocationProductDao = new PickUpLocationProductDao(context) ;
		this.configDao = new ConfigDao(context);
		this.totalQty = totalQty ;
		this.location = location ;
		this.handler = handler ;
	}

	@Override
	public void run() {
		int flag = HttpResultCode.MSG_FAILURE;
		List<PickUpSubmitLocationDifferent> arrayList = getPcidDifferent(location_id, pcid);
		StringBuffer detail = new StringBuffer();
		if(arrayList != null && arrayList.size() > 0){
			for(int index = 0 , count = arrayList.size() ; index < count ; index++ ){
				detail.append(handleItem(arrayList.get(index), index == (count-1)));
			}
		}
	 
 
		
		
		Map<String, String> param = new HashMap<String, String>();
		param.put("Out", pickup_id+"");
		param.put("LocationId", location_id+"");
		param.put("FOType",  "FIFO");
		param.put("Location",  location);

		param.put("Qty", totalQty+"");
		param.put("Pcid", pcid+"");
		String postXml = HttpTool.getPostXmlString(detail.toString(), HttpPostMethod.PickUpException, param );
		Log.i("post", postXml);
		try {
			InputStream inputStream = HttpTool.getStreamAndPostXML(HttpUrlPath.PickUp, postXml);
			FileUtil.saveFileNoSdCard("pickupDefferent.xml", inputStream, context);
			flag = 	handXml();	
		} catch (Exception e) {
 			e.printStackTrace();
		}
		handler.obtainMessage(flag).sendToTarget();
  	
 		
	}
	private int handXml() throws FileNotFoundException{
		int flag = HttpResultCode.MSG_FAILURE ;
		FileInputStream in  =	new FileInputStream(new File(context.getFilesDir()+File.separator+"pickupDefferent.xml"));
		ReturnBean<PickUpLoactionProduct> pickupBean = PickUpBaseDataParse.handNeedPickUpProduct(in, pickup_id);
		if(StringUtil.convert2Inter(pickupBean.getRet()) == 1){
			//说明提交数据成功,那么删除这个位置的数据(包含扫描的数据),因为已经返回了这个位置的数据
				configDao.afterSubmitPickupLocationScanInfo(location_id, pickup_id, pickupBean);
				flag = HttpResultCode.MSG_SUCCESS ;
		}
		return flag ;
	
	}
	//fromContainerType,fromContainerTypeId,fromContainerId,
	//pickupContainerType,pickupContainerTypeId,pickupContainerId
	//qty
	private String handleItem(PickUpSubmitLocationDifferent temp , boolean isLast){
		StringBuffer sb = new StringBuffer();
					sb.append(temp.getFromContainerType());
		sb.append(",").append(temp.getFromContainerTypeId());
		sb.append(",").append(temp.getFromContainerId());
		 
		sb.append(",").append(temp.getContainerType());
		sb.append(",").append(temp.getContainerTypeId());
		sb.append(",").append(temp.getContainerId());
		sb.append(",").append(temp.getContainerQty());
		
		if(!isLast){
			sb.append("|");
		}
		return sb.toString();
	}
	
	/**
	 * 因为提交差异之前肯定会提交已经拣货到的数据。然后会删除这个位置原来的数据,会插入新的数据
	 * @param location_id
	 * @param pcid
	 */
	private List<PickUpSubmitLocationDifferent> getPcidDifferent(long location_id , long pcid ){
		List<PickUpSubmitLocationDifferent> differentList = new ArrayList<PickUpSubmitLocationDifferent>();

		List<PickUpLoactionProduct>  arrayList = pickUpLocationProductDao.getNeedPickUpLocationProductBy(pcid, location_id, pickup_id);
		Map<PickUpDifferentContainerEquals, List<PickUpLoactionProduct>> category = new HashMap<PickUpDifferentContainerEquals, List<PickUpLoactionProduct>>();
		
		for(PickUpLoactionProduct loactionProduct : arrayList){
			PickUpDifferentContainerEquals equals = new PickUpDifferentContainerEquals(
					loactionProduct.getFrom_con_id(),
					loactionProduct.getFrom_container_type(),
					loactionProduct.getFrom_container_type_id(),
					
					loactionProduct.getPick_con_id(),
					loactionProduct.getPick_container_Type_id(),
					loactionProduct.getPick_container_Type()
					);	//
		    List<PickUpLoactionProduct> needPickUp =	category.get(equals);
			if(needPickUp == null){
				needPickUp = new ArrayList<PickUpLoactionProduct>();
			} 
			needPickUp.add(loactionProduct);
			category.put(equals, needPickUp);
		}
		
		
		if(!category.isEmpty()){
			 Iterator<Entry<PickUpDifferentContainerEquals, List<PickUpLoactionProduct>>> it = category.entrySet().iterator();
			 while(it.hasNext()){
				 PickUpSubmitLocationDifferent locationDifferent = new PickUpSubmitLocationDifferent();
				 Entry<PickUpDifferentContainerEquals, List<PickUpLoactionProduct>> entry = it.next();
				 List<PickUpLoactionProduct> values = entry.getValue() ;
				 PickUpDifferentContainerEquals key = entry.getKey();
				
				 locationDifferent.setContainerType(key.getPickupContainerType());		//containerType
				 locationDifferent.setContainerTypeId(key.getPickupContainerTypeId());	//containerTypeId
				 locationDifferent.setContainerId(key.getPickupContainerId());			//containerId
				 
				 locationDifferent.setFromContainerId(key.getFromContainerId()); 		//fromContainerId
				 locationDifferent.setFromContainerType(key.getFromContainerType());	//fromContainerType
				 locationDifferent.setFromContainerTypeId(key.getFromContainerTypeId());//fromContainerTypeId
				 
				 
				 
				 if(values != null && values.size() == 1 && values.get(0).getPick_con_id() != 0l ){
					 //直接是拣Container的情况
					 locationDifferent.setContainerId(values.get(0).getPick_con_id());
				 }
				 //总共所需的商品的数量
				 double totalQty = 0.0d ;
				 for(int index = 0 , count = values.size() ; index < count ; index++ ){
					 PickUpLoactionProduct locationProduct = values.get(index);
					 totalQty += locationProduct.getQty();
					 locationDifferent.setPcid(locationProduct.getPcid());
				 }
				 locationDifferent.setTotalQty(totalQty);
				 if(!isPickupProduct(key)){
					 locationDifferent.setContainerQty(values.size());
				 } else{
					 locationDifferent.setContainerQty((int)StringUtil.convertStrToDouble(totalQty+""));
				 }
				 differentList.add(locationDifferent);
			 }
		}
		return differentList;
		 
 	}
	private boolean isPickupProduct(PickUpDifferentContainerEquals containerEquals){
			return containerEquals.getPickupContainerType() == 0 && containerEquals.getPickupContainerTypeId() == 0l ;
		}
}
