package oso.ui.processing.specialproject;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.processing.specialproject.adapter.SpecialProjectAdp;
import oso.ui.processing.specialproject.bean.ProjectBean;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import support.anim.AnimUtils;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 
 * @description: Sepcial Project
 *
 * @author xialimin
 * @time : 2015-6-16 下午3:33:18
 */
public class WMS_Special_ProjectActivity extends BaseActivity {

	private Context mContext;

	private ListView mProjectLv;

	private SpecialProjectAdp mTaskAdp;

	private ProjectBean mTasks;

	private int mPosition, mTop; // ListView位置状态保存

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_special_project_list, 0);

		initView();

		initListener();

	}

	@Override
	protected void onResume() {
		super.onResume();
		initData();
	}

	private void initView() {

		mContext = this;
		setTitleString(getString(R.string.specialtask_project_title));

		mProjectLv = (ListView) findViewById(R.id.lv_project);
		TextView emptyView = (TextView) findViewById(R.id.tvProjectEmpty);
		mProjectLv.setEmptyView(emptyView);

		mTaskAdp = new SpecialProjectAdp(mContext, mTasks);
		mProjectLv.setAdapter(mTaskAdp);

	}

	/**
	 * 初始化数据
	 */
	private void initData() {

		saveCurrentPosition();

		RequestParams params = new RequestParams();
		params.put("key", "getAllProjectByAdid");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				mTasks = new ProjectBean(json.optJSONArray("projects"));
				mTaskAdp = new SpecialProjectAdp(mContext, mTasks);
				mProjectLv.setAdapter(mTaskAdp);
				restorePosition();
			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}

	private void initListener() {

		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				initData();
			}
		});

		mProjectLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				String projectId = mTasks.projectList.get(position).schedule_id;
				if (TextUtils.isEmpty(projectId)) {
					UIHelper.showToast(mContext, getString(R.string.sync_empty));
				}
				Intent in = new Intent(mContext, WMS_Special_TasksActivity.class);
				WMS_Special_TasksActivity.initParams(in, projectId);
				startActivity(in);
				overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}
		});

	}

	/**
	 * 菜单Dialog
	 * 
	 * @param position
	 * 
	 *            Edit 和 Delete
	 */
	private void showSlcDialog(int position) {
		new RewriteBuilderDialog.Builder(WMS_Special_ProjectActivity.this).setTitle(getString(R.string.sync_operation))
				.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setArrowItems(new String[] { "Edit", "Delete" }, new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
						onSlcDialogItemClick(itemPosition);
					}
				}).show();
	}

	/**
	 * Operate Dialog 选择选项后
	 * 
	 * @param itemPosition
	 */
	private void onSlcDialogItemClick(int itemPosition) {
		switch (itemPosition) {
		case SpecialTaskKey.OPERATE_EDIT:
			UIHelper.showToast(mContext, "Edit!");
			break;
		case SpecialTaskKey.OPERATE_DELETE:
			UIHelper.showToast(mContext, "Delete!");
			break;
		}
	}

	/**
	 * 打开添加任务Dialog
	 */
	private void showAddTaskDialog() {
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);

		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Add Task");
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				String descriptionStr = etDescription.getText().toString();
				if (!isValid(etDescription))
					return;
				submitAddTask(descriptionStr);
				dlg.dismiss();
			}

		}).show();
	}

	/**
	 * 判断输入值是否非法
	 * 
	 * @param tvDes
	 * @return
	 */
	private boolean isValid(TextView tvDes) {
		boolean isValid = true; // 有效的

		if (TextUtils.isEmpty(tvDes.getText().toString())) {
			AnimUtils.horizontalShake(mContext, tvDes);
			isValid = false;
		}
		return isValid;
	}

	/**
	 * 请求添加MainTask
	 * 
	 * @param descriptionStr
	 */
	private void submitAddTask(String descriptionStr) {
		/*
		 * MainTaskBean mainTask = new MainTaskBean(); mainTask.mainTaskName =
		 * subjectStr; mainTask.mainTaskDes = descriptionStr; mainTask.type =
		 * SpecialTaskKey.TYPE_PENDING; mTasks.add(mainTask);
		 */
		mTaskAdp.notifyDataSetChanged();
	}

	/**
	 * @description: 添加主任务
	 *
	 * @author xialimin
	 * @time : 2015-6-16 上午10:01:37
	 */
	private class OnAddBtnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			showAddTaskDialog();
		}

	}

	/**
	 * ListView位置状态保存
	 */
	private void saveCurrentPosition() {
		if (mProjectLv != null) {
			int position = mProjectLv.getFirstVisiblePosition();
			View v = mProjectLv.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();
			// 保存position和top位置
			mPosition = position;
			mTop = top;
		}
	}

	/**
	 * ListView位置恢复
	 */
	private void restorePosition() {
		if (mTasks != null && mProjectLv != null) {
			int position = mPosition;// 取出保存的数据
			int top = mTop;// 取出保存的数据
			mProjectLv.setSelectionFromTop(position, top);
		}
	}
}
