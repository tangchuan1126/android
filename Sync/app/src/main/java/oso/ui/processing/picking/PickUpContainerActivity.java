package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.ui.processing.picking.bean.LpInfo;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpContainer;
import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import oso.ui.processing.picking.db.PickUpContainerDao;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.runnable.ContainerParseJsonHelper;
import support.common.AlertUi;
import support.dbhelper.Goable;
import support.exception.SystemException;
import support.key.BCSKey;
import support.key.ContainerIntTypeKey;
import support.key.ContainerProductStateKey;
import support.network.HttpPostMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 扫描Container
 * 
 * @author zhangrui 直接拣容器的
 * 
 */
public class PickUpContainerActivity extends TTSFragment {
	private Context context;
	private Resources resources;
	private long pickup_id;
	private long location_id;
	private String location;
	private PickUpBean pickUpBean;

	private EditText containerText;

	private TextView toolContainer;
	private TextView toolScan;
	private TextView toolList;
	private LinearLayout containerBody;
	private int currentShowView;
	private String containerPrefix;

	private static AsyncHttpClient client = new AsyncHttpClient();

	private ProgressDialog baseDataProgressDialog = null;

	private PickUpScanInfoDao pickUpScanInfoDao;

	private PickUpContainerDao pickUpContainerDao;

	private HashSet<Long> needPickupContainerId = new HashSet<Long>(); // 进入这个页面的时候应该计算一下
																		// 还需要拣的ContainerId

	private int needPickupContainerTypeQty = 0; // 这种类型还需要拣货的数量(除去要拣货的ContainerId)

	private AlertDialog affirmDialog = null;

	private TextView title; // 显示的标题.1 直接在某一个位置上面拣货.2或者直接在某个Container上面拣货

	private ArrayList<PickUpBean> arrayListPickUpBeans;

	private int currentPickupContainerIndex = 0; // 拣ContainerId的时候当前的currentIndex

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pickup_container_layout);
		initField();
		initOpera();
		initNeedPickup();
		checkIsPickUpOver();
		initTab();
		findViewById(R.id.picup_black).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onInit(int status) {
		super.onInit(status);
		// 初始化这个界面的时候去读取值
		speakInit();
	}

	private Handler initSpeakHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			// pick up . BLP1000012.
			if (pickUpBean.getPickUpType() == PickUpBean.DirectPickUpContainerType) {
				String alertSpeakMessage = "";
				if (isPickupContainerId()) {
					// 拣货某个具体的Container的时候
					String containerType = ContainerIntTypeKey.getType(pickUpBean.getPickUpContainerType());
					alertSpeakMessage = "pick up . " + StringUtil.addSpace(containerType) + " "
							+ StringUtil.addSpace(String.valueOf(getPickupContainerId()));
				} else {

					String containerType = ContainerIntTypeKey.getType(pickUpBean.getPickUpContainerType());
					alertSpeakMessage = "pick up . " + StringUtil.addSpace(containerType) + ". Type. "
							+ StringUtil.addSpace(String.valueOf(pickUpBean.getPickUpContainerTypeId()));
				}
				alertMessage(false, null, 0, true, alertSpeakMessage);
			}
		};
	};

	private boolean isPickupContainerId() {
		return pickUpBean.getArrayListPickupLocationProduct().get(0).getPick_con_id() != 0l;
	}

	private void speakInit() {
		// 开启一个子线程去读取
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (pickUpBean == null) {
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
					}
				}
				initSpeakHandler.obtainMessage().sendToTarget();
			}
		});
		thread.start();
	}

	private long getPickupContainerId() {
		return pickUpBean.getArrayListPickupLocationProduct().get(currentPickupContainerIndex).getPick_con_id();
	}

	// 模拟添加数据
	public void scanContainer(View view) {

		if (isThisPickUpBeanOver()) {
			checkIsPickUpOver();
			return;
		}
		int randomInt = 0;
		if (!isPickupContainerId()) {
			randomInt = new Random().nextInt(1000);
		} else {
			randomInt = (int) getPickupContainerId();
		}
		String containerTypePre = ContainerIntTypeKey.getType(pickUpBean.getPickUpContainerType());
		PickUpContainer pickUpContainer = new PickUpContainer();
		pickUpContainer.setLocation_id(location_id);
		pickUpContainer.setContainer(containerTypePre + randomInt);
		pickUpContainer.setContainer_id(randomInt);
		pickUpContainer.setContainer_type(pickUpBean.getPickUpContainerType());
		pickUpContainer.setContainer_type_id(pickUpBean.getPickUpContainerTypeId());
		pickUpContainer.setFrom_container_id(pickUpBean.getFromContainerId());
		pickUpContainer.setFrom_container_type(pickUpBean.getFromContainerType());
		pickUpContainer.setPickup_id(pickup_id);

		List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>();

		PickUpScanInfo info = new PickUpScanInfo();
		info.setBarCode(pickUpBean.getPc_id() + "");
		info.setContainer(containerTypePre + randomInt);
		info.setFrom_container_id(pickUpBean.getFromContainerId());
		info.setFrom_container_type(pickUpBean.getFromContainerType());
		info.setFrom_container_type_id(pickUpBean.getFromContainerTypeId());
		info.setFrom_location_id(location_id);
		info.setLocation_id(location_id);
		info.setPickup_id(pickup_id);
		info.setSku(pickUpBean.getPc_id());
		info.setPickup_container_id(randomInt);
		info.setPickup_container_type(pickUpBean.getPickUpContainerType());
		info.setPickup_container_type_id(pickUpBean.getPickUpContainerTypeId());
		info.setPickup_id(pickup_id);
		info.setPname(pickUpBean.getpName());
		if (isPickupContainerId()) {
			info.setQty(pickUpBean.getTotalQty() / pickUpBean.getArrayListPickupLocationProduct().size());
		} else {
			info.setQty(pickUpBean.getTotalQty() / pickUpBean.getArrayListPickupLocationProduct().get(0).getPickContainerQty());

		}
		arrayList.add(info);
		handLoadLpInfoSuccess(pickUpContainer, arrayList);

	}

	private void checkIsPickUpOver() {
		if (isThisPickUpBeanOver()) {
			String alertSpeakMessage = "pick up over";
			alertMessage(true, null, 0, true, alertSpeakMessage);
			Builder overBuilder = new Builder(context);
			overBuilder.setTitle(resources.getString(R.string.sys_holdon));
			String notify = String.format(resources.getString(R.string.pickup_container_type_over), pickUpBean.getPickUpContainerTypeId() + "",
					ContainerIntTypeKey.getType(pickUpBean.getPickUpContainerType()));
			overBuilder.setMessage(notify);

			overBuilder.setPositiveButton(resources.getString(R.string.sys_button_pickup_goon), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// 继续拣货，下一种ContainerTyep （先简单考虑到时候应该是根据不同的跳转到不同的activity）
					goonPickUpContainer();
				}
			});
			overBuilder.setNegativeButton(resources.getString(R.string.sync_no), null);
			overBuilder.show();
		}
	}

	private void goonPickUpContainer() {
		int currentPickupIndex = 0;
		for (int index = 0, count = arrayListPickUpBeans.size(); index < count; index++) {
			if (arrayListPickUpBeans.get(index).equals(pickUpBean)) {
				currentPickupIndex = index;
			}
		}
		int nextPickupIndex = (currentPickupIndex + 1);
		if (nextPickupIndex >= arrayListPickUpBeans.size()) {
			// 直接跳到第一个界面，给他一个参数，这个位置已经拣货完成。让后直接走下一个Location
			Intent intent = new Intent(context, PickUpLocationActivity.class);
			intent.putExtra("refresh", "false");
			intent.putExtra("currentLocation", location_id);
			intent.putExtra("pickup_id", pickup_id + "");
			startActivity(intent);
			finish();
		} else {
			// finish.当前的页面，重新开启这个
			Intent intent = new Intent(context, PickUpContainerActivity.class);
			intent.putExtra("pickup_id", pickup_id);
			intent.putExtra("location_id", location_id);
			intent.putExtra("location", location);
			intent.putExtra("pickUpBean", arrayListPickUpBeans.get(nextPickupIndex));
			intent.putExtra("pickUpBeans", arrayListPickUpBeans);
			startActivity(intent);
			finish();

		}

	}

	private void initNeedPickup() {

		if (pickUpBean != null) {
			// 原本需要拣货的container
			List<PickUpLoactionProduct> listTemp = pickUpBean.getArrayListPickupLocationProduct();
			for (PickUpLoactionProduct tempProduct : listTemp) {
				if (tempProduct.getPick_con_id() != 0l) {
					needPickupContainerId.add(tempProduct.getPick_con_id());
				} else {
					needPickupContainerTypeQty += tempProduct.getPickContainerQty();
				}
			}
			// 需要拣货的数据 -- 查询数据库已经拣货到的数据 = 还需要拣货的数据
			List<Long> result = pickUpContainerDao.getPickUpContainerBy(pickUpBean.getFromContainerId(), pickUpBean.getFromContainerType(),
					pickUpBean.getFromContainerTypeId(), pickup_id, location_id, pickUpBean.getPickUpContainerType(),
					pickUpBean.getPickUpContainerTypeId());
			for (Long value : result) {
				if (needPickupContainerId.contains(value)) {
					needPickupContainerId.remove(value);
				} else {
					needPickupContainerTypeQty--;
				}
			}

		}

	}

	private void initOpera() {
		// 首先应该是根据需要的Container的Type去计算应该的Container的类型
		// 扫描Container,然后去后台下载
		containerText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {

				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					String value = containerText.getText().toString();
					if (value.trim().length() < 1) {
						alertMessage(true, resources.getString(R.string.scan_container), 0, true,
								resources.getString(R.string.speak_please_scan_container));
						clearInput();
						return true;
					}
					if (value.trim().toUpperCase().indexOf(containerPrefix) == -1) {
						alertMessage(true, resources.getString(R.string.scan_correct_container_type), 0, true,
								resources.getString(R.string.speak_scan_correct_container_type));
						clearInput();
						return true;
					}
					// 数据库是否已经有了这个ContainerId(加上判断)
					int count = pickUpContainerDao.countContainerByName(value);
					if (count < 1) {
						handleContainerValue(value);
					} else {
						alertMessage(true, resources.getString(R.string.lpName_is_exits), 0, false, null);
						clearInput();
					}

					return true;
				}
				return false;
			}
		});
	}

	// 下载Container的值
	private void handleContainerValue(String value) {
		Goable.initGoable(context);

		RequestParams params = new RequestParams();
		params.put("LoginAccount", Goable.LoginAccount);
		params.put("Password", Goable.Password);
		params.put("Machine", "");
		params.put("Container", value);
		params.put("Method", HttpPostMethod.InnerContainerLoad);
		client.get(HttpUrlPath.Container, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				if (!baseDataProgressDialog.isShowing()) {
					baseDataProgressDialog.show();
				}
			}

			@Override
			public void onFinish() {
				if (baseDataProgressDialog != null && baseDataProgressDialog.isShowing()) {
					baseDataProgressDialog.dismiss();
				}
			}

			@Override
			// public void onSuccess(int statusCode, JSONObject json) {
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (statusCode == 200) {
					handResult(json);
				} else {
					alertMessage(true, resources.getString(R.string.sys_error), 0, false, null);
					containerText.setText("");
					containerText.requestFocus();
				}
			}
		});
	}

	// 处理请求的结果
	private void handResult(JSONObject json) {
		// 对应的ContainerType要和pickup的一致,并且如果是有需要拣货具体的Container,那么一定是对应的
		if (StringUtil.getJsonInt(json, "ret") == 1) {
			LpInfo lpInfo = ContainerParseJsonHelper.getLpInfo(json);
			/**
			 * 0. container的类型必须正确 1. 判断这个Container是否在这个位置上(
			 * 有可能是他是去其他的位置扫描这个这种类型的Container) 2. 判断这个Container是否是满的(&&
			 * container 上面的商品和定义的一样) 3. 这种是直接在这个位置上的(也就是说ContainerParent为0)
			 */
			if (lpInfo == null || lpInfo.getLp() == 0l || !lpInfo.getContainerType().equalsIgnoreCase(containerPrefix)
					|| lpInfo.getContainerTypeId() != pickUpBean.getPickUpContainerTypeId()) {
				alertContainerTypeError(lpInfo.getContainerTypeId() + "");
				return;
			}
			// 如果是直接在某个确定的Container上面拣Container,那么请求的Container是没有位置的（因为在放货的时候是只有记了接地容器的和位置的关系）
			// 所以只有在位置上拣Container的时候才会加上这个判断.
			if (pickUpBean.getPickUpType() == PickUpBean.DirectPickUpContainerType && lpInfo.getLocationId() != location_id) {
				alertContainerNotOnThisLocation();
				return;
			}

			if (lpInfo.getParentContainerId() != pickUpBean.getFromContainerId()) {
				// 表示的意思是。你扫描的Container 是在其他的容器中 && 这个拣货要求缺不是这个FromContainerId
				alertContainerIsOnOtherContainer(lpInfo, pickUpBean);
				return;
			}
			Map<String, Object> values = getProducts(json, lpInfo);
			if (!isContainerProductFull(values, lpInfo)) {
				alertContainerProductQtyError();
				return;
			}
			List<PickUpScanInfo> arrayList = (ArrayList<PickUpScanInfo>) values.get("scanInfoList");

			PickUpContainer pickUpContainer = getPickUpContainer(lpInfo);
			// 首先检查是否能够添加
			if (isCanAdd(lpInfo)) {
				affrimPickUpContainer(pickUpContainer, arrayList);
			} else {
				alertMessage(true, null, 0, false, null);
				AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_container_not_allow));
				clearInput();
			}
		} else {
			// 系统错误等信息
			alertMessage(true, BCSKey.getReturnStatusById(context,StringUtil.getJsonInt(json, "err")), 0, false, null);
			containerText.setText("");
			containerText.requestFocus();
		}
	}

	// 提示是否拣货
	private void affrimPickUpContainer(final PickUpContainer pickupContainer, final List<PickUpScanInfo> arrayList) {

		final Builder builder = new Builder(context).setTitle(resources.getString(R.string.sys_holdon));

		View view = getLayoutInflater().inflate(R.layout.pickup_affrim_container_layout, null);

		final EditText containerEdit = (EditText) view.findViewById(R.id.container);
		containerEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// 验证扫描的是否就是和lpInfo 中container 编号是否相等.不相等
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {

					String value = containerEdit.getText() != null ? containerEdit.getText().toString() : "";
					if (!value.toUpperCase().equals(pickupContainer.getContainer().toUpperCase())) {
						containerEdit.setText("");
						alertMessage(true, resources.getString(R.string.container_not_match), 0, true,
								resources.getString(R.string.speak_container_not_matched));
						return true;
					} else {
						handLoadLpInfoSuccess(pickupContainer, arrayList);

					}
				}
				return false;
			}
		});
		builder.setView(view);
		builder.setPositiveButton(resources.getString(R.string.sys_button_add), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handLoadLpInfoSuccess(pickupContainer, arrayList);

			}
		});

		builder.setNegativeButton(resources.getString(R.string.sync_no), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				clearInput();
			}
		});
		affirmDialog = builder.show();

		alertMessage(false, null, 0, true, resources.getString(R.string.speak_make_sure_container));

	}

	// 检查是否可以添加
	private boolean isCanAdd(LpInfo lpInfo) {
		boolean flag = false;
		if (needPickupContainerId.contains(lpInfo.getLp())) {
			return true;
		} else {
			flag = needPickupContainerTypeQty > 0;
		}
		return flag;
	}

	private PickUpContainer getPickUpContainer(LpInfo lpInfo) {

		PickUpContainer pickUpContainer = new PickUpContainer();
		pickUpContainer.setContainer_id(lpInfo.getLp());
		pickUpContainer.setContainer(lpInfo.getLpName());
		pickUpContainer.setContainer_type(pickUpBean.getPickUpContainerType());
		pickUpContainer.setContainer_type_id(lpInfo.getContainerTypeId());

		pickUpContainer.setFrom_container_id(pickUpBean.getFromContainerId());
		pickUpContainer.setFrom_container_type(pickUpBean.getFromContainerType());
		pickUpContainer.setFrom_container_type_id(pickUpBean.getFromContainerTypeId());

		pickUpContainer.setLocation_id(location_id);
		pickUpContainer.setPickup_id(pickup_id);
		return pickUpContainer;
	}

	/**
	 * 前面的商品数量,容器类型,商品位置上 条件都满足了。
	 * 计算本地是否还需要这个Container(本来需要5个这种类型的Container其中包含一个具体的的ContainerID)
	 * 如果说他在拿到这种类型的Container && 并且已经超过了数量了。那么应该提示。他已经拿满了
	 * 如果是有具体的Container_id的那么每次都应该计算是否是这个ContainerId是否是要拣的
	 * 
	 * @param lpInfo
	 * @param arrayList
	 */
	private void handLoadLpInfoSuccess(PickUpContainer pickUpContainer, List<PickUpScanInfo> arrayList) {
		// 保存LpInfo到数据库中,保存PickupScanInfo到数据库中
		try {
			pickUpScanInfoDao.addPickupScanInfo(pickUpContainer, arrayList);
			// 添加成功
			if (needPickupContainerId.contains(pickUpContainer.getContainer_id())) {
				needPickupContainerId.remove(pickUpContainer.getContainer_id());
			} else {
				needPickupContainerTypeQty--;
			}
			// 提示
			if (!isPickupContainerId()) {
				alertMessageSuccess(true, null, 0, true, needPickupContainerTypeQty + " Left ", null);
			} else {
				if (currentPickupContainerIndex < pickUpBean.getArrayListPickupLocationProduct().size() - 1) {
					String alertSpeakMessage = "pick up . "
							+ StringUtil.addSpace(pickUpBean.getPickUpContainerType() + "")
							+ StringUtil.addSpace(String.valueOf(pickUpBean.getArrayListPickupLocationProduct().get(++currentPickupContainerIndex)
									.getPick_con_id()));
					;
					alertMessageSuccess(true, null, 0, true, alertSpeakMessage, null);
				}
			}

			if (affirmDialog != null && affirmDialog.isShowing()) {
				affirmDialog.dismiss();
			}
			// 检查是否都拣货完了
			checkIsPickUpOver();
			showTabView(currentShowView, false);
		} catch (SystemException e) {
			alertMessage(true, resources.getString(R.string.sys_database_error_add_failed), 0, false, null);
		}
		clearInput();
	}

	// 是否 已经这个页面的container 是否已经拣货完成
	private boolean isThisPickUpBeanOver() {
		return needPickupContainerId.size() == 0 && needPickupContainerTypeQty < 1;
	}

	private boolean isContainerProductFull(Map<String, Object> values, LpInfo lpInfo) {
		boolean isFull = false;

		double totalQty = StringUtil.convert2Double(values.get("totalQty") + "");
		if (lpInfo.getContainerPcCount() * 1.0d == totalQty && lpInfo.getIsFull() == ContainerProductStateKey.FULL) {
			isFull = true;
		}

		return isFull;

	}

	// {"pcid":"1000030","pname":"ads/as\"da/';';BALLAST","sn":"11111","qty":"1.0","lp":"100037","lpname":"CLP01"}
	private Map<String, Object> getProducts(JSONObject json, LpInfo lpInfo) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>();
		double totalQty = 0.0d;
		try {
			JSONArray products = json.getJSONArray("products");

			if (products != null && products.length() > 0) {
				for (int index = 0, count = products.length(); index < count; index++) {
					JSONObject product = products.getJSONObject(index);
					PickUpScanInfo receiveInfo = new PickUpScanInfo();

					receiveInfo.setQty(StringUtil.getJsonDouble(product, "qty")); // 数量
					receiveInfo.setIsSubmit(0);
					receiveInfo.setLocation_id(location_id);
					receiveInfo.setPickup_id(pickup_id);
					receiveInfo.setFrom_location_id(StringUtil.getJsonLong(product, "lp")); // container
																							// Id
					receiveInfo.setSn(StringUtil.getJsonString(product, "sn")); // sn
					receiveInfo.setSku(StringUtil.getJsonLong(product, "pcid")); // pcid
					receiveInfo.setBarCode(StringUtil.getJsonLong(product, "pcid") + ""); // sku
					receiveInfo.setPname(StringUtil.getJsonString(product, "pname")); // pname

					receiveInfo.setFrom_container_id(pickUpBean.getFromContainerId()); // fromContainerId
					receiveInfo.setFrom_container_type(pickUpBean.getFromContainerType()); // fromContainerType
					receiveInfo.setFrom_container_type_id(pickUpBean.getFromContainerTypeId()); // fromContainerTypeId

					receiveInfo.setPickup_container_id(lpInfo.getLp()); // pickupContainerId
					receiveInfo.setPickup_container_type(pickUpBean.getPickUpContainerType()); // pickupContainerType
					receiveInfo.setPickup_container_type_id(pickUpBean.getPickUpContainerTypeId()); // pickup_type_id;

					totalQty += StringUtil.getJsonDouble(product, "qty");
					arrayList.add(receiveInfo);
				}
			}
		} catch (Exception e) {
		}
		result.put("scanInfoList", arrayList);
		result.put("totalQty", totalQty);
		return result;
	}

	private void alertContainerTypeError(String lpInfoTypeId) {
		// LpInfo 不对
		String notify = String.format(resources.getString(R.string.container_type_not_matched), lpInfoTypeId, pickUpBean.getPickUpContainerTypeId()
				+ "");
		alertMessage(true, notify, 0, true, resources.getString(R.string.speak_container_type_wrong));
		clearInput();
	}

	private void clearInput() {
		containerText.setText("");
		containerText.requestFocus();
	}

	private void alertContainerProductQtyError() {
		alertMessage(true, null, 0, false, null);
		AlertUi.showAlertBuilder(context, resources.getString(R.string.container_product_qty_error));
		clearInput();
	}

	private void alertContainerNotOnThisLocation() {
		alertMessage(true, null, 0, false, null);
		AlertUi.showAlertBuilder(context, resources.getString(R.string.container_no_this_location));
		clearInput();
	}

	private void alertContainerIsOnOtherContainer(LpInfo lpInfo, PickUpBean pickUpBean) {
		alertMessage(true, null, 0, false, null);
		if (pickUpBean.getFromContainerId() != 0l) { // 我去这个具体的Container上面拣货
			// 表示这个Container不是在这个具体的Container上面
			// 提示系统记录的请求容器不和你的FromContainerId相等
			String notify = ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()) + pickUpBean.getFromContainerId();
			AlertUi.showAlertBuilder(context, resources.getString(R.string.container_not_on_from_container_id, notify));

		}
		if (pickUpBean.getFromContainerId() == 0l) { // 我去直接拣货 Container就在这个地板上面
			// 表示这个Container包含在其他的容器中不能直接拿出来
			AlertUi.showAlertBuilder(context, resources.getString(R.string.container_on_other_container));
		}

		clearInput();
	}

	private void initField() {
		context = PickUpContainerActivity.this;
		resources = context.getResources();
		pickUpScanInfoDao = new PickUpScanInfoDao(context);
		pickUpContainerDao = new PickUpContainerDao(context);
		// 初始化pickup_id,location_id,location
		pickup_id = getIntent().getLongExtra("pickup_id", 0l);
		location_id = getIntent().getLongExtra("location_id", 0l);
		location = getIntent().getStringExtra("location");
		containerText = (EditText) findViewById(R.id.container_text);

		toolScan = (TextView) findViewById(R.id.pickup_button_scan);
		toolList = (TextView) findViewById(R.id.pickup_button_list);
		toolContainer = (TextView) findViewById(R.id.pickup_button_container);
		containerBody = (LinearLayout) findViewById(R.id.containerBody);

		arrayListPickUpBeans = (ArrayList<PickUpBean>) getIntent().getSerializableExtra("pickUpBeans");

		pickUpBean = (PickUpBean) getIntent().getSerializableExtra("pickUpBean");
		containerPrefix = ContainerIntTypeKey.getType(pickUpBean.getPickUpContainerType()).toUpperCase();
		baseDataProgressDialog = new ProgressDialog(context);
		baseDataProgressDialog.setTitle(resources.getString(R.string.sys_holdon));
		baseDataProgressDialog.setMessage(resources.getString(R.string.sys_loadBase_data));

		title = (TextView) findViewById(R.id.title);
		if (pickUpBean.getFromContainerId() == 0l) {
			// 表示的是直接在某个位置上面去拣货
			// String notify = "On [" + location + "] PickUp Container.";
			title.setText("PickUp Container");
		} else {
			// 表示的是在某个位置上面的某个具体的Container上面拣Container
			// String notify = "On [" +
			// ContainerIntTypeKey.getType(pickUpBean.getFromContainerType())+pickUpBean.getFromContainerId()
			// + "] PickUp Container.";
			title.setText("PickUp Container");
		}
	}

	private void initTab() {
		addTab(toolScan, "Scan", 1);
		addTab(toolList, "List", 2);
		addTab(toolContainer, "Container", 3);
		showTabView(3, false);
	}

	private void addTab(final TextView textView, final String title_, final int currentIndex) {
		textView.setOnClickListener(new View.OnClickListener() {
			private int index = currentIndex;
			private String title = title_;

			@Override
			public void onClick(View v) {
				showTabView(index, true);
			}
		});
	}

	private void showTabView(int index, boolean isrefreshViewType) {
		if (index == 0) {
			index = currentShowView;
		}

		currentShowView = index;
		Fragment fragment = null;
		clearOtherTabColor();
		if (index == 1) {
			fragment = new PickUpProductScanActivity();
			toolScan.setBackgroundColor(Color.BLACK);
			toolScan.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_left_style_press));
			toolScan.setTextColor(resources.getColor(R.color.white));
		}
		if (index == 2) {
			fragment = new PickUpProductListActivity();
			toolList.setBackgroundColor(Color.BLACK);
			toolList.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_center_style_press));
			toolList.setTextColor(resources.getColor(R.color.white));
		}
		if (index == 3) {
			fragment = new PickUpProductContainerActivity();
			toolContainer.setBackgroundColor(Color.BLACK);
			toolContainer.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_right_style_press));
			toolContainer.setTextColor(resources.getColor(R.color.white));
		}

		Bundle bundle = new Bundle();
		bundle.putInt("from", 1);
		bundle.putLong("pickup_id", pickup_id);
		bundle.putLong("location_id", location_id);
		bundle.putString("location", location);
		bundle.putSerializable("pickUpBean", pickUpBean);
		bundle.putInt("needPickupContainerTypeQty", needPickupContainerTypeQty); // 这种类型需要的Container的数量()除去了直接拣某种容器的ID后的数量
		bundle.putSerializable("needPickupContainerId", needPickupContainerId); // 需要拣货的ContainerId(直接的指定了要拣货的ContainerId
																				// )
		fragment.setArguments(bundle);
		// 改成fragment replace
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.containerBody, fragment);
		transaction.commit();
	}

	private void clearOtherTabColor() {
		toolScan.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_left_style));
		toolScan.setTextColor(resources.getColor(R.color.black));
		toolList.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_center_style));
		toolList.setTextColor(resources.getColor(R.color.black));
		toolContainer.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_right_style));
		toolContainer.setTextColor(resources.getColor(R.color.black));
	}

	/**
	 * 
	 * @param isNeedAlertNoftify
	 *            是否是需要alert 声音 failed的声音
	 * @param alertMessage
	 *            //文字提示的
	 * @param speakModel
	 *            //speakModel 0.表示的是notify
	 * @param isNeedSpeak
	 *            //是否需要speak
	 * @param alertSpeakMessage
	 *            //alertSpeakMessage 说话的声音
	 */
	/*
	 * private void alertMessage(boolean isNeedAlertNoftify , String
	 * alertMessage, int speakModel ,boolean isNeedSpeak , String
	 * alertSpeakMessage){
	 * if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(alertPlayer);}
	 * if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length()
	 * > 0){ if(speakModel == 0){ speakNotify(alertSpeakMessage); } }
	 * if(alertMessage != null){ UIHelper.showToast(context, alertMessage,
	 * Toast.LENGTH_SHORT).show(); } } private void alertMessageSuccess(boolean
	 * isNeedAlertNoftify , String alertMessage, int speakModel ,boolean
	 * isNeedSpeak , String alertSpeakMessage , String alertValue){
	 * if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(successPlayer);}
	 * if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length()
	 * > 0){ if(speakModel == 0){ speakNotify(alertSpeakMessage); }
	 * if(speakModel == 1){ speakUnIntercept(alertSpeakMessage, alertValue); }
	 * if(speakModel == 2){ speakAll(alertSpeakMessage, alertValue); } }
	 * if(alertMessage != null) UIHelper.showToast(context, alertMessage,
	 * Toast.LENGTH_SHORT).show(); }
	 */
}
