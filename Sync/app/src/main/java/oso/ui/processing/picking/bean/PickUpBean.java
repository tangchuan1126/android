package oso.ui.processing.picking.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PickUpBean implements Serializable{
	
	public static final int DirectPickUpContainerType = 1 ;		//直接去 某个位置拣某种类型的Container (container 是直接放在地上的(包含了直接有container_id的))
 	public static final int DirectPickUpContainerIdContainerType = 2 ; // 直接去某个位置上某个确定的Container上拣某种container类型
	public static final int DirectPcikUpProduct	 = 3 ;					//在某个位置 具体的Container的上面拣货
	public static final int DirectPcikUpProductOnType = 4 ; 			//在2D位置的TLP上直接拣货
	public static final int DirectPcikUpProductNoType = 5 ; 			//直接告诉你去某种大类型的Container上面拣货，比如TLP
	private int pickUpType  ; 					//上面的1,2,3
	
	private int fromContainerType ;				//TLP,BLP,CLP
 	private long fromContainerTypeId ;			//具体的容器类型
 	private long fromContainerId ; 				//具体的容器ID
 	
 	private int pickUpContainerType ;			//TLP,BLP,CLP
	private long pickUpContainerTypeId ;		//ID
   	
 	private List<PickUpLoactionProduct> arrayListPickupLocationProduct = new ArrayList<PickUpLoactionProduct>() ;
 	
	private String pName ;				
	private long pc_id ;
 	private double totalQty ;							//总共要拣的商品数量
	
	
 	
 	
	public int getPickUpContainerType() {
		return pickUpContainerType;
	}

	public void setPickUpContainerType(int pickUpContainerType) {
		this.pickUpContainerType = pickUpContainerType;
	}

	public int getFromContainerType() {
		return fromContainerType;
	}

	public void setFromContainerType(int fromContainerType) {
		this.fromContainerType = fromContainerType;
	}

	public long getFromContainerTypeId() {
		return fromContainerTypeId;
	}

	public void setFromContainerTypeId(long fromContainerTypeId) {
		this.fromContainerTypeId = fromContainerTypeId;
	}

	public long getFromContainerId() {
		return fromContainerId;
	}

	public void setFromContainerId(long fromContainerId) {
		this.fromContainerId = fromContainerId;
	}

	public void addPickUpLocationProduct(PickUpLoactionProduct pickUpLoactionProduct){
		arrayListPickupLocationProduct.add(pickUpLoactionProduct);
	}
	
	public List<PickUpLoactionProduct> getArrayListPickupLocationProduct() {
		return arrayListPickupLocationProduct;
	}
	public void setArrayListPickupLocationProduct(
			List<PickUpLoactionProduct> arrayListPickupLocationProduct) {
		this.arrayListPickupLocationProduct = arrayListPickupLocationProduct;
	}
	public int getPickUpType() {
		return pickUpType;
	}
	public void setPickUpType(int pickUpType) {
		this.pickUpType = pickUpType;
	}
	public long getPickUpContainerTypeId() {
		return pickUpContainerTypeId;
	}
	public void setPickUpContainerTypeId(long pickUpContainerTypeId) {
		this.pickUpContainerTypeId = pickUpContainerTypeId;
	}
 
	 
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	 
 
	
 	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	
  
	//添加总共的商品一共是多少 
	public void addPickUpTotalQty(double qty){
		this.totalQty  +=  qty; 
	}
	@Override
	public boolean equals(Object o) {
 		if( o  instanceof PickUpBean){
 			PickUpBean other = (PickUpBean) o ;
 			return other.getPickUpContainerType() == this.getPickUpContainerType()
 					&& other.getPickUpContainerTypeId() == this.getPickUpContainerTypeId()
 					&& other.getFromContainerId() == this.getFromContainerId() 
 					&& other.getFromContainerType() == this.getFromContainerType()
 					&& other.getFromContainerTypeId() == this.getFromContainerTypeId() ;
 		}
 		return false ;
	}
	
}
