package oso.ui.processing.picking.adapter;

import java.util.List;

import oso.ui.processing.picking.bean.ContainerSimple;
import support.key.ContainerIntTypeKey;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpProductContainerAdapter extends BaseAdapter{

	private List<ContainerSimple> arrayList ;
	
	private LayoutInflater inflater;
	
	private Context context;
	
	
	
	public PickUpProductContainerAdapter(List<ContainerSimple> arrayList,
			Context context) {
		super();
		this.arrayList = arrayList;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0 ;
	}

	@Override
	public ContainerSimple getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Log.i("position", position+"");
		PickUpProductContainerAdapterHolder viewHolder = null ;
		if(convertView==null){
			viewHolder = new PickUpProductContainerAdapterHolder();
			convertView = inflater.inflate(R.layout.pickup_container_listview,null);
			viewHolder.pickTotalQty = (TextView)convertView.findViewById(R.id.pickup_total_qty);
			viewHolder.pickUpQty = (TextView)convertView.findViewById(R.id.pickup_qty);
			viewHolder.containerType = (TextView)convertView.findViewById(R.id.container_type);
			viewHolder.containerTypeId = (TextView)convertView.findViewById(R.id.container_type_id);
			viewHolder.containerProduct = (TextView)convertView.findViewById(R.id.container_product);
			viewHolder.containerId = (TextView)convertView.findViewById(R.id.container_id);
			viewHolder.containerIdIsShow = convertView.findViewById(R.id.container_id_isshow);
   			viewHolder.containerLimit = (TextView)convertView.findViewById(R.id.container_limit);
			viewHolder.mAll=(LinearLayout)convertView.findViewById(R.id.all);
			convertView.setTag(viewHolder);
		}else{
			viewHolder=(PickUpProductContainerAdapterHolder) convertView.getTag();
		}
		ContainerSimple  temp = getItem(position) ;
		viewHolder.pickTotalQty.setText(temp.getTotalQty()+""); 
		viewHolder.pickUpQty.setText(temp.getPickQty()+"") ;
		String type = ContainerIntTypeKey.getType(temp.getContainerType()) ;
		viewHolder.containerType.setText(type) ;
		viewHolder.containerTypeId.setText(temp.getContainerTypeId()+"");
		if(temp.getContainerId() == 0l){
			viewHolder.containerIdIsShow.setVisibility(View.GONE);
		}else{
			viewHolder.containerIdIsShow.setVisibility(View.VISIBLE);
			viewHolder.containerId.setText(temp.getContainerId()+"");
		}
		viewHolder.containerProduct.setText(temp.getpName());
		viewHolder.containerLimit.setText(temp.getProductQty()+"");
 		if(temp.getPickQty()==temp.getTotalQty()&&temp.getTotalQty()!=0){
			viewHolder.mAll.setBackgroundResource(R.drawable.tablepeizhi_hui);
 		}else{
			viewHolder.mAll.setBackgroundResource(R.drawable.inventory_layout_line);
		}
 		return convertView;
 	}

}
class PickUpProductContainerAdapterHolder{
	public TextView pickUpQty ;
	public TextView pickTotalQty ;
	public TextView containerType ;
	public TextView containerTypeId ;
	public TextView containerId ;
	public TextView containerProduct;
	public TextView containerLimit ;
	public View containerIdIsShow;
	public LinearLayout mAll;
 
	
}

