package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import oso.ui.processing.picking.bean.PickUpContainer;
import support.dbhelper.DatabaseHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpContainerDao {
	
	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup_container_info;
	
	public PickUpContainerDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	
	public int countContainerByName(String containerName){
		int count = 0;
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase() ;
			 cursor =	db.rawQuery("select count(*) as count_sum from " + tableName + " where container=?	", new String[]{containerName});
			if(cursor != null && cursor.getCount() > 0 ){
				cursor.moveToNext() ;
				count  = cursor.getInt(cursor.getColumnIndex("count_sum"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return count ;
 	}
	
	
	
	
	/**
	 * 
	 * @param from_container_id
	 * @param from_container_type
	 * @param from_container_type_id
	 * @param pickup_id
	 * @param location_id
	 * @param pickup_container_type
	 * @param pickup_container_type_id
	 * @return
	 * 查询已经拣货到的Container 在这个位置 && 拣货单上 && container_type
	 */
	public List<Long>  getPickUpContainerBy(long from_container_id , int from_container_type ,long from_container_type_id  , long pickup_id , long location_id , int pickup_container_type , long pickup_container_type_id ){
		StringBuffer sql = new StringBuffer("");
		sql.append(" select  container_id ")
		.append("	from 	"+ tableName )
		.append("	where ")
		.append(" container_type =? ")
		.append(" and container_type_id =? ")
		.append(" and from_container_id =? ")
		.append(" and from_container_type =? ")
		.append(" and from_container_type_id =? ")
		.append(" and location_id =? ")
		.append(" and pickup_id =?");
 		List<Long> result = new ArrayList<Long>();
		Cursor cursor  = null ;
		try{
			db = helper.getReadableDatabase() ;
			  cursor  = 	db.rawQuery(sql.toString(), new String[]{
					pickup_container_type+"",
					pickup_container_type_id+"",
					from_container_id+"",
					from_container_type+"",
					from_container_type_id+"",
					location_id+"",
					pickup_id+""
				});
			if(cursor != null && cursor.getCount() > 0 ){
				while(!cursor.isLast()){
					cursor.moveToNext() ;
 					long containerId = cursor.getLong(cursor.getColumnIndex("container_id"));
  					result.add(containerId);
				}
			}
		}catch(Exception e){}
		finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return result;
	}
	
	public LinkedList<PickUpContainer> getLocationPikcupContainerBy(long location_id , long pickup_id){
		LinkedList<PickUpContainer> returnList = new LinkedList<PickUpContainer>();
		StringBuffer sql = new StringBuffer("select * from ");
		sql.append(tableName).append(" where location_id= ? and pickup_id= ? ");
		 
		Cursor cursor  = null ;
		try{
			db = helper.getReadableDatabase() ;
			cursor = db.rawQuery(sql.toString(), new String[]{location_id+"",pickup_id+""}); 
			returnList = handCursor(cursor);
		}catch(Exception e){}
		finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return returnList;
	}

	private LinkedList<PickUpContainer> handCursor(Cursor cursor){
		LinkedList<PickUpContainer> returnList = new LinkedList<PickUpContainer>();
		if(cursor != null && cursor.getCount() > 0){
			while(!cursor.isLast()){
				cursor.moveToNext() ;
				PickUpContainer pickUpContainer = new PickUpContainer();
				
				pickUpContainer.setContainer(cursor.getString(cursor.getColumnIndex("container")));
				pickUpContainer.setContainer_id(cursor.getLong(cursor.getColumnIndex("container_id")));
				pickUpContainer.setContainer_type(cursor.getInt(cursor.getColumnIndex("container_type"))); 
				pickUpContainer.setContainer_type_id(cursor.getLong(cursor.getColumnIndex("container_type_id"))); 
				pickUpContainer.setFrom_container_id(cursor.getLong(cursor.getColumnIndex("from_container_id"))); 
				pickUpContainer.setFrom_container_type(cursor.getInt(cursor.getColumnIndex("from_container_type"))); 
				pickUpContainer.setFrom_container_type_id(cursor.getLong(cursor.getColumnIndex("from_container_type_id")));
				pickUpContainer.setLocation_id(cursor.getLong(cursor.getColumnIndex("location_id")));
				pickUpContainer.setPickup_id(cursor.getLong(cursor.getColumnIndex("pickup_id")));
				returnList.add(pickUpContainer);
			}
		}
		return returnList;
	}
}
