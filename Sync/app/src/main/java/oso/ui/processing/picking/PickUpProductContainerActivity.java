package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oso.ui.processing.picking.adapter.PickUpProductContainerAdapter;
import oso.ui.processing.picking.bean.ContainerEquals;
import oso.ui.processing.picking.bean.ContainerSimple;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import declare.com.vvme.R;

public class PickUpProductContainerActivity extends Fragment{

	
	private Context context ;
	private Resources resources;
	private long pickup_id ;
	private long location_id; 
 	private ListView listView;
 	private PickUpBean pickUpBean ;
	private List<ContainerSimple> showdatas = new ArrayList<ContainerSimple>() ;
	private int needPickupContainerTypeQty = 0;							//这种类型的ContainerType 还需要拣几个(除去了指定拣特定容器的)
	private Set<Long> needPickupContainerId = new HashSet<Long>();		//这种类型的ContainerId 还要拣那些（指定了特定的容器的ＩＤ）
	
	private Bundle bundle ; 
	private View view ;						//整个的布局文件.
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
 		    view = inflater.inflate(R.layout.pickup_common_layout, container, false);
 		
 			return view ;
 	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 		initField();
		showList();
	} 
	private void initField(){
		context = getActivity();
		bundle = getArguments() ;
 		pickUpBean = (PickUpBean)bundle.getSerializable("pickUpBean");
		pickUpBean.getArrayListPickupLocationProduct() ;
		listView = (ListView)view.findViewById(R.id.pickup_common_view_list);
		needPickupContainerTypeQty =  bundle.getInt("needPickupContainerTypeQty", 0);
		needPickupContainerId = (HashSet<Long>)bundle.getSerializable("needPickupContainerId");
	}
	private void showList(){
		initData();
		listView.setDivider(null);
		listView.setAdapter(new PickUpProductContainerAdapter(showdatas, context));
		 
	}
	 
	//组织数据
	private void initData(){
		
		if(pickUpBean != null){
 			Map<ContainerEquals, ContainerSimple>  maps = PickUpBaseDataParse.getContainerSimpleList(pickUpBean);
			if(!maps.isEmpty()){
				Iterator<Map.Entry<ContainerEquals, ContainerSimple>> it = maps.entrySet().iterator();
				while(it.hasNext()){
					ContainerSimple simple = it.next().getValue();
					//如果没有包含那么就表示:应该被拣走了
					//else 表示的是拣这些类型 (应该拣 - needPickupQty) = 已经拣货到的数量 
					if(simple.getContainerId() != 0l ){
						if(!needPickupContainerId.contains(simple.getContainerId())){
							simple.setPickQty(1);
						}
						
					}else{
						simple.setPickQty(simple.getTotalQty() - needPickupContainerTypeQty);
					}
					showdatas.add(simple);
				}
			}
		}
	
	}
}
