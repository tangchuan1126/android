package oso.ui.processing.picking.bean;

public class Product {
	
	private long id ;
	private long pid ;
	private String name ;
	private double length ;
	private double width ;
	private double heigth ;
	private double weight ;
  
	private int sn_length ;
 
	public Product() {
		super();
	}


	
 
	
	public Product(long id, long pid, String name, double length, double width,
			double heigth, double weight) {
		super();
		this.id = id;
		this.pid = pid;
		this.name = name;
		this.length = length;
		this.width = width;
		this.heigth = heigth;
		this.weight = weight;
	}




	public Product(long pid, String name, double length, double width,
			double heigth, double weight) {
		super();
		this.pid = pid;
		this.name = name;
		this.length = length;
		this.width = width;
		this.heigth = heigth;
		this.weight = weight;
	}




	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	 

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeigth() {
		return heigth;
	}

	public void setHeigth(double heigth) {
		this.heigth = heigth;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getSn_length() {
		return sn_length;
	}
	public void setSn_length(int sn_length) {
		this.sn_length = sn_length;
	}
  
}
