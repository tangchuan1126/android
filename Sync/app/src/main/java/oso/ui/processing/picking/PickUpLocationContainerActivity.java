package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import oso.ui.processing.picking.adapter.PickUpContainerAdapter;
import oso.ui.processing.picking.adapter.PickUpProductListAdapter;
import oso.ui.processing.picking.bean.ContainerEquals;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpContainer;
import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpContainerDao;
import oso.ui.processing.picking.db.PickUpLocationProductDao;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.runnable.SubmitLocationProductRunable;
import support.common.AlertUi;
import utility.StringUtil;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;



public class PickUpLocationContainerActivity  extends TTSFragment{

	private long location_id ;
	private long pickup_id ;
	private String location ; 
	private ListView listView ;
	private EditText container ;
	private Context context ;
	private Resources resources ;
	private PickUpLocationProductDao pickUpLocationProductDao ;
	private PickUpContainerDao pickUpContainerDao ;
	private PickUpScanInfoDao pickUpScanInfoDao ;
	private ArrayList<PickUpBean> pickUpDataList = new ArrayList<PickUpBean>();
	private PickUpContainerAdapter pickUpContainerAdapter ;
	private TextView title ;
	private Builder builder  ;
	private ProgressDialog progressDialog ; 
 	@Override
	protected void onCreate(Bundle savedInstanceState) {
 		super.onCreate(savedInstanceState);
 		setContentView(R.layout.pickup_location_container_layout);
 		initField();
  		organizingData();
  		//为了演示，我现在这个界面直接pass过去下一个界面了。（以后改好了。这个界面会没有...）
  		//startActivityScan();
  		findViewById(R.id.picup_black).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
 	@Override
 	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			finish();
			overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
		}
		return super.onKeyDown(keyCode, event);
 	}
 	 
	public void startNewActivity(View view){
		
		Intent intent = new Intent(context,PickUpContainerActivity.class);
		intent.putExtra("pickup_id", pickup_id);
		intent.putExtra("location_id", location_id);
		intent.putExtra("location", location);
		int index =StringUtil.convert2Inter(container.getText().toString()) ;
		intent.putExtra("pickUpBean", pickUpDataList.get(index));
		intent.putExtra("pickUpBeans", pickUpDataList);
 		startActivity(intent);
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		setListView();
		
 	}
	
	@Override
	public void onInit(int status) {
 		super.onInit(status);
 		alertMessage(false, null, 0, true, " Select Container Type ");

 		 
	}
	private void setListView(){
 
 		//查询这个位置上面已经拣货到的ContainerId
		LinkedList<PickUpContainer> linkedListPickUpContainer = pickUpContainerDao.getLocationPikcupContainerBy(location_id, pickup_id);
 		pickUpContainerAdapter = new PickUpContainerAdapter(context, pickUpDataList,resources,linkedListPickUpContainer,pickup_id,location_id); 
		listView.setDivider(null);
		listView.setAdapter(pickUpContainerAdapter);
 		 
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
 				//不同类型的pickUpBean 
  				Class clazz = PickUpContainerActivity.class ;
				PickUpBean pickupBean = pickUpDataList.get(position);
				if(pickupBean.getPickUpType() == PickUpBean.DirectPcikUpProduct || 
						pickupBean.getPickUpType() == PickUpBean.DirectPcikUpProductOnType ||
						pickupBean.getPickUpType() == PickUpBean.DirectPcikUpProductNoType){
					  clazz = PickUpContainerProductActivity.class;
				} 
			 
				
				Intent intent = new Intent(context,clazz);
				intent.putExtra("pickup_id", pickup_id);
				intent.putExtra("location_id", location_id);
				intent.putExtra("location", location);
 				intent.putExtra("pickUpBean", pickUpDataList.get(position));
 				intent.putExtra("pickUpBeans", pickUpDataList);

		 		startActivity(intent);
		 		
 			}
		});
	 
 	}
	//组织数据
	private void organizingData(){
		List<PickUpLoactionProduct> arrayList = pickUpLocationProductDao.getAllPickUpLoactionProducts(pickup_id, location_id);
		
		//直接去 某个位置拣某种类型的Container (container 是直接放在地上的 包括了 直接的拣有具体的Container 只要是这个Container_type)	group by container type
		List<PickUpLoactionProduct> directPickUpContainerType = new ArrayList<PickUpLoactionProduct>();
		
		 
		
		//直接去某个位置上的某个具体的Container上拣某种Container(待拣的不是直接放在地上的,而是在某个具体的container中) 和指定的ContainerID group by from_pick_location
		List<PickUpLoactionProduct> directPickUpContainerIdContainerType = new ArrayList<PickUpLoactionProduct>();
	
  		
		//直接去某个特定的container上面拣货(直接拿商品)
		List<PickUpLoactionProduct> directPickUpContainersProduct = new ArrayList<PickUpLoactionProduct>();
		
		//直接去某种类型的Container上拣货拣散货
		List<PickUpLoactionProduct> directPickUpOnContainerTypeProduct = new ArrayList<PickUpLoactionProduct>();

		//只告诉你。去TLP上拿货
		List<PickUpLoactionProduct> directPickUpNoTypeProduct = new ArrayList<PickUpLoactionProduct>();

		for(PickUpLoactionProduct temp : arrayList){
			
			if(temp.getFrom_con_id() == 0l   && temp.getPick_container_Type_id() !=0l){
				directPickUpContainerType.add(temp);
				continue ;
			}
			 
			if(temp.getFrom_con_id() != 0l && temp.getPick_container_Type() != 0l){
				directPickUpContainerIdContainerType.add(temp);
				continue ;
			}
			 
			if(temp.getFrom_con_id() !=0l && temp.getPick_con_id() == 0l && temp.getPick_container_Type() == 0l){
				directPickUpContainersProduct.add(temp);
				continue ;
			}
			if(temp.getFrom_con_id() == 0l && temp.getFrom_container_type_id() != 0l && temp.getPick_container_Type() == 0l){
				//在某种类型的TLP  上拣货
				directPickUpOnContainerTypeProduct.add(temp);
				continue ;
			}
			if(temp.getFrom_con_id() == 0l && temp.getFrom_container_type_id() == 0l && temp.getPick_container_Type() == 0l){
				//直接去TLP上拣货
				directPickUpNoTypeProduct.add(temp);
				continue ;
			}
		}
 
		setDirectPickUpContainerType(directPickUpContainerType);//直接去某个位置拣某种类型的Container
		
		setDirectContainerIdPickUpContainerType(directPickUpContainerIdContainerType);	//在指定container中拣指定的容器类型
 		
		setDirectContainerIdProduct(directPickUpContainersProduct);	//直接去特定的某个容器里面拣货
		
		setDirectContainerTypeProduct(directPickUpOnContainerTypeProduct);	//直接去某种类型 的Container上拣货
		
		setDirectPickUpNoTypeProduc(directPickUpNoTypeProduct);				//直接去告诉你直接去拣货比如TLP，没有类型的ID

	}
	private void initField(){
		
		context = PickUpLocationContainerActivity.this;
		pickUpLocationProductDao = new PickUpLocationProductDao(context);
		pickUpContainerDao = new PickUpContainerDao(context);
		pickUpScanInfoDao = new PickUpScanInfoDao(context);
		resources = getResources();
		Intent intent = getIntent() ;
		location_id = intent.getLongExtra("location_id", 0l);
		pickup_id = intent.getLongExtra("pickup_id", 0l);
		location = intent.getStringExtra("location");
 		listView = (ListView)findViewById(R.id.pickup_need_container);
		title = (TextView)findViewById(R.id.title);
//		title.setText(String.format("Work On Location [%1s]", location));
		title.setText(location+"");
		progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(resources.getString(R.string.sys_holdon));
		progressDialog.setMessage(resources.getString(R.string.sys_handle_data));
	}
	 
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	//组织数据  直接在某个位置上拣某种类型Container的(直接放在位置上的 (包含了有containerId))
	private void setDirectPickUpContainerType(List<PickUpLoactionProduct> directPickUpContainerTypes){
 			Map<ContainerEquals, PickUpBean> maps = new HashMap<ContainerEquals, PickUpBean>();
			
 			for(PickUpLoactionProduct temp :directPickUpContainerTypes ){
 				 ContainerEquals willAdded = new ContainerEquals(0l,temp.getPick_container_Type_id(), temp.getPick_container_Type());
				 PickUpBean bean = maps.get(willAdded) ;
				 if(bean == null){
					 bean = new PickUpBean();
					 bean.setPc_id(temp.getPcid());
					 bean.setpName(temp.getPname());
					 bean.setPickUpType(PickUpBean.DirectPickUpContainerType);
					 bean.setPickUpContainerType(temp.getPick_container_Type());
					 bean.setPickUpContainerTypeId(temp.getPick_container_Type_id());
					 bean.addPickUpLocationProduct(temp);
  					 bean.addPickUpTotalQty(temp.getQty());
					 maps.put(willAdded, bean);
				 }else{
					 bean.addPickUpLocationProduct(temp);
 					 bean.addPickUpTotalQty(temp.getQty());
				}
			}
 			addMapToPickUpBean(maps);
 	 
	}
	// 在某个位置上拣container group by from_con_id ;
	private void setDirectContainerIdPickUpContainerType(List<PickUpLoactionProduct> directPickUpContainerIdContainerType){
			Map<ContainerEquals, PickUpBean> maps = new HashMap<ContainerEquals, PickUpBean>();
			for(PickUpLoactionProduct temp :directPickUpContainerIdContainerType ){
				 ContainerEquals willAdded = new ContainerEquals(temp.getFrom_con_id() , 0l, temp.getFrom_container_type());
				 PickUpBean bean = maps.get(willAdded) ;
				 if(bean == null){
					 bean = new PickUpBean();
					 bean.setPc_id(temp.getPcid());
					 bean.setpName(temp.getPname());
					 
					 bean.setFromContainerId(temp.getFrom_con_id());
					 bean.setFromContainerType(temp.getFrom_container_type());
					 bean.setFromContainerTypeId(temp.getFrom_container_type_id());
					 
					 
					 bean.setPickUpContainerTypeId(temp.getPick_container_Type_id());
					 bean.setPickUpType(PickUpBean.DirectPickUpContainerIdContainerType);
					 bean.setPickUpContainerType(temp.getPick_container_Type());
					 bean.addPickUpLocationProduct(temp);
 					 bean.addPickUpTotalQty(temp.getQty());
 					 
					 maps.put(willAdded, bean);
				 }else{
					 bean.addPickUpLocationProduct(temp);
					 bean.addPickUpTotalQty(temp.getQty());
				}
			}
			addMapToPickUpBean(maps);
	}

	//在具体的Container上 拣货 group by container
	private void setDirectContainerIdProduct(List<PickUpLoactionProduct> directPickUpContainersProduct){

		Map<ContainerEquals, PickUpBean> maps = new HashMap<ContainerEquals, PickUpBean>();
		for(PickUpLoactionProduct temp : directPickUpContainersProduct ){
			 ContainerEquals willAdded = new ContainerEquals(temp.getFrom_con_id(),0l, temp.getPick_container_Type());
			 PickUpBean bean = maps.get(willAdded) ;
			 if(bean == null){
				 bean = new PickUpBean();
				 bean.setPc_id(temp.getPcid());
				 bean.setpName(temp.getPname());
				 bean.setPickUpType(PickUpBean.DirectPcikUpProduct);
				 bean.setFromContainerId(temp.getFrom_con_id());
				 bean.setFromContainerType(temp.getFrom_container_type());
				 bean.setFromContainerTypeId(temp.getFrom_container_type_id());
	 
  				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
					 
				 maps.put(willAdded, bean);
			 }else{
				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
			}
		}
		addMapToPickUpBean(maps);
		
	}
	//直接去某种类型的Container上面拣货
	private void setDirectContainerTypeProduct(List<PickUpLoactionProduct> directPickUpOnContainerTypeProduct){
		Map<ContainerEquals, PickUpBean> maps = new HashMap<ContainerEquals, PickUpBean>();
	
		for(PickUpLoactionProduct temp : directPickUpOnContainerTypeProduct ){
			 ContainerEquals willAdded = new ContainerEquals(temp.getFrom_con_id(),temp.getFrom_container_type_id(), temp.getFrom_container_type());
			 PickUpBean bean = maps.get(willAdded) ;
			 if(bean == null){
				 bean = new PickUpBean();
				 bean.setPc_id(temp.getPcid());
				 bean.setpName(temp.getPname());
				 bean.setPickUpType(PickUpBean.DirectPcikUpProductOnType);
				 bean.setFromContainerId(temp.getFrom_con_id());
				 bean.setFromContainerType(temp.getFrom_container_type());
				 bean.setFromContainerTypeId(temp.getFrom_container_type_id());
	 
  				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
					 
				 maps.put(willAdded, bean);
			 }else{
				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
			}
		}
		addMapToPickUpBean(maps);
		
	}
	
	private void setDirectPickUpNoTypeProduc(List<PickUpLoactionProduct> directPickUpOnContainerTypeProduct){
		Map<ContainerEquals, PickUpBean> maps = new HashMap<ContainerEquals, PickUpBean>();
	
		for(PickUpLoactionProduct temp : directPickUpOnContainerTypeProduct ){
			 ContainerEquals willAdded = new ContainerEquals(0l,0l, temp.getFrom_container_type());
			 PickUpBean bean = maps.get(willAdded) ;
			 if(bean == null){
				 bean = new PickUpBean();
				 bean.setPc_id(temp.getPcid());
				 bean.setpName(temp.getPname());
				 bean.setPickUpType(PickUpBean.DirectPcikUpProductNoType);
				 bean.setFromContainerId(temp.getFrom_con_id());
				 bean.setFromContainerType(temp.getFrom_container_type());
				 bean.setFromContainerTypeId(temp.getFrom_container_type_id());
	 
  				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
					 
				 maps.put(willAdded, bean);
			 }else{
				 bean.addPickUpLocationProduct(temp);
				 bean.addPickUpTotalQty(temp.getQty());
			}
		}
		addMapToPickUpBean(maps);
		
	}
	private void addMapToPickUpBean(Map<ContainerEquals,PickUpBean> maps){
		if(!maps.isEmpty()){
			Iterator<Entry<ContainerEquals, PickUpBean>> it = maps.entrySet().iterator();
			while(it.hasNext()){
				Entry<ContainerEquals,PickUpBean> entry = it.next();
				PickUpBean bean = entry.getValue();
				pickUpDataList.add(bean);
			}
		}
	}
	
	private Handler submitShowPickUpInfo =  new Handler(){
		
		public void handleMessage(android.os.Message msg) {
			if(progressDialog != null && progressDialog.isShowing()){progressDialog.dismiss();}
			if(builder != null){
				builder.create().show();
			}
		};
	};
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>提交数据<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	public void showPickUpInfo(View view){
		 
		progressDialog.show();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				builder = new Builder(context);
				 
				LinearLayout pickupLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.pickup_submit_container_info_layout, null);
				TextView innerTextView = (TextView)pickupLayout.findViewById(R.id.title);
				innerTextView.setText(String.format("Location [%1s] PickUp Info", location));
				ListView listView =(ListView) pickupLayout.findViewById(R.id.pickup_info) ;
				final List<ProductCompareSimple>   datas = pickUpScanInfoDao.getLocationPickupInfo(pickup_id, location_id);		
				listView.setAdapter(new PickUpProductListAdapter(datas, context));
				builder.setPositiveButton(resources.getString(R.string.sys_button_submit), new DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
					  boolean isCanSubmit = false ;	//没有数据的是不可以提交的
						if(datas != null){
							for(ProductCompareSimple simple : datas){
								if(simple.getPickupQty() > 0.0d){
									isCanSubmit = true ;
									break ;
								}
							}
						}
						submitPickupInfo(isCanSubmit);
					}
				});
				builder.setNegativeButton(resources.getString(R.string.sync_no), null);
				builder.setView(pickupLayout);
				submitShowPickUpInfo.obtainMessage().sendToTarget();
			}
		}).start();
		  
	}
	
	
	private Handler submitPickUpInfoHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(progressDialog != null && progressDialog.isShowing()){progressDialog.dismiss();}
			
		};
	};
	//读取这个位置没有提交的数据
	//提交目前收货到的记录
	private void submitPickupInfo(boolean isCanSubmit){
		if(!isCanSubmit){
			AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_no_scanInfo_cant_submit));
			return ; 
		}
		progressDialog.show();
		
		Thread thread = new Thread(new SubmitLocationProductRunable(context, location_id, pickup_id, location, submitPickUpInfoHandler)) ;
		thread.start();
	}
	
	
	
	
	
	
	
	
	
}
