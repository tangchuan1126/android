package oso.ui.processing.picking;

import java.util.List;

import oso.ui.processing.picking.adapter.PickUpLocationAdapter;
import oso.ui.processing.picking.adapter.PickUpProductListAdapter;
import oso.ui.processing.picking.bean.PickUpLocation;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpLocationDao;
import oso.ui.processing.picking.db.PickUpLocationProductDao;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.parse.HttpResultCode;
import oso.ui.processing.picking.runnable.LoadPickUpBaseDataRunnable;
import oso.ui.processing.picking.runnable.SubmitDifferentRunnable;
import oso.ui.processing.picking.runnable.SubmitLocationProductRunable;
import support.common.AlertUi;
import support.common.UIHelper;
import support.dbhelper.ConfigDao;
import utility.StringUtil;
import utility.Utility;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

/**
 * @author Administrator
 *
 */
//TTSFragment
public class PickUpLocationActivity extends  TTSFragment {
	
	private long pickUpId ;
	private Resources resources ;
	private Context context ;
	private ProgressDialog getPickUpDialog ; 
 	private EditText pickupLoactionText ;
 	private PickUpLocationDao pickUpLocationDao ;
 	private PickUpScanInfoDao pickUpScanInfoDao ;
 	private PickUpLocationProductDao pickUpLocationProductDao ;
 	private ListView listView ;
 	private ConfigDao configDao ;
	private NetBroadcastReceiver netBroadcastReceiver ;
	private int fixPosition = -1;
	private PickUpLocationAdapter pickUpLocationAdapter ;
	private boolean isFresh = true ;
	private Builder builder  ;
	private ProgressDialog progressDialog ; 
	private List<PickUpLocation> araryList  ;
	private AlertDialog alertDialog ; 
	private String currentLocation ;
	
	private AlertDialog builderDilog ; 
	private Button picup_black ;
	
	
	private long currentLocationId = 0l ; //从pickupContainer页面回来的时候获取的值
  	private Handler loadBaseInfoHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			getPickUpDialog.dismiss();
			switch (msg.what) {
				case HttpResultCode.MSG_SUCCESS: 
					loadData();
				break;
				case HttpResultCode.MSG_FAILURE:
					UIHelper.showToast(context, resources.getString(R.string.sys_error_get_base_info_failed), Toast.LENGTH_SHORT).show();
				break ;
			}
		};
	};
	 
	 
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
 		super.onCreate(savedInstanceState);
 		setContentView(R.layout.pickup_location_layout);
 		resources = getResources();
 		context = PickUpLocationActivity.this;
 		initValue();
   		initField();
   		loadBaseData();
   		configDao.updateConfigDao("picking_id", pickUpId+"");
   		netBroadcastReceiver = new NetBroadcastReceiver(context);
 	 
 		IntentFilter intentFilter = new IntentFilter(); 
 		intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION); 
 		registerReceiver(netBroadcastReceiver, intentFilter); 
   		 
	}
	private void initValue(){
 		pickUpId = StringUtil.convert2Long(getIntent().getExtras().getString("pickup_id"));
 		String isFreshString = getIntent().getExtras().getString("refresh");
 		if(isFreshString != null && isFreshString.equals("false")){
 			isFresh = false ;
 		}
 		currentLocationId = (long)getIntent().getExtras().getLong("currentLocation",0l) ;
  	}
 	private void initField(){
		
		configDao = new ConfigDao(context);
  		listView = (ListView)findViewById(R.id.pickup_need_location);
 		pickupLoactionText = (EditText)findViewById(R.id.pickup_location_input);
 		pickupLoactionText.setInputType(InputType.TYPE_NULL);
 		pickUpLocationDao = new PickUpLocationDao(context);
 		pickUpScanInfoDao = new PickUpScanInfoDao(context);
 		pickUpLocationProductDao = new PickUpLocationProductDao(context);
 		picup_black = (Button) findViewById(R.id.picup_black);
 		picup_black.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
 		
 		
 		progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(resources.getString(R.string.sys_holdon));
		progressDialog.setMessage(resources.getString(R.string.sys_handle_data));
		
		pickupLoactionText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					 String location = pickupLoactionText.getText().toString() ;
					 if(!StringUtil.isNullOfStr(location)){
						int index = getIndexOfLocation(location);
						if(index != -1){
							PickUpLocation pickupLocation =  getLocation(index);
							 
							startLocationProduct(pickupLocation.getLocation_id()+"",
									pickupLocation.getLocation(), pickupLocation.getPickup_id(), 0);
						}else{
							
							AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_location_not_on_this_pick));
						}
						pickupLoactionText.setText("");
						pickupLoactionText.requestFocus();
					 }
				}
 				return false;
			}
		});
	}
	private PickUpLocation getLocation(int index){
		return araryList.get(index);
	}
	private int getIndexOfLocation(String location){
		String fixLocation = location.replace(resources.getString(R.string.location_sign), "");
		int returnIndex = -1 ;
		if(araryList != null && araryList.size() > 0 ){
			for(int index = 0 , count = araryList.size() ; index < count ; index++ ){
				if(araryList.get(index).getLocation().equals(fixLocation)){
					returnIndex = index ;
					break ;
				}
			}
		}
		return returnIndex ;
	}
	private void loadBaseData(){
		if(Utility.isConnectNet(context) && isFresh){
			getPickUpDialog = new ProgressDialog(context);
	 		getPickUpDialog.setTitle(resources.getString(R.string.sys_holdon));
	 		getPickUpDialog.setMessage(resources.getString(R.string.sys_loadBase_data));
	 		getPickUpDialog.setCancelable(true);
	 		Thread thread  = new Thread(new LoadPickUpBaseDataRunnable(context, pickUpId, loadBaseInfoHandler));
			thread.start();
			getPickUpDialog.show();
		}else{
			loadData();
		}
	}
	@Override
	protected void onResume() {
		  
 		listView.requestFocus();
		pickupLoactionText.clearFocus();
		super.onResume();
	}
//	debug
	@Override
	public void onInit(int status) {
 		super.onInit(status);
 		notifyInitSpeak();
 	}
	/**
	 * 开一个子线程去检查是否有了数据有数据就返回，开始去读
	 * 
	 * @descrition
	 */
	 private Handler handler = new Handler(){
		 public void handleMessage(android.os.Message msg) {
			 if(msg.what == 1){
				 PickUpLocation pickUpLocation =(PickUpLocation)msg.obj;
				 spealLocation(pickUpLocation);
			 }
			 if(msg.what == 0){
				 alertMessage(false, null, 0, true, "pick up over");
			 }
		 };
	 };
	private void spealLocation(PickUpLocation pickUpLocation){
		String alertSpeakMessage = "please move to . " + pickUpLocation.getLocation_type()  + " D location . " + StringUtil.addSpace(pickUpLocation.getLocation());
		alertMessage(false, null, 0, true, alertSpeakMessage);
	}
	private void notifyInitSpeak(){
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
 				while(araryList == null){
 					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
 						e.printStackTrace();
					}
  				}
 				PickUpLocation location = findNextLocation();
 				if(location == null){
 					handler.obtainMessage(0).sendToTarget();
 				}else{
 					handler.obtainMessage(1,location).sendToTarget();
 				}
			}
		});
		thread.start();
	}
	private PickUpLocation findNextLocation(){
		if(currentLocationId != 0l){
			int currentIndex = 0 ;
			for(int index = 0 , count = araryList.size() ; index < count ; index++ ){
				if(araryList.get(index).getLocation_id() == currentLocationId){
					currentIndex  = index  ;
					break ;
				}
			}
			if(currentIndex + 1 >= araryList.size()){
				return null ;
			}
			return araryList.get(currentIndex + 1) ;
		}
		return  araryList.get(0) ;
	}
	private void loadData(){
		
		araryList =  pickUpLocationDao.getPickUpLoactionByPickUpId(pickUpId);
		
		pickUpLocationAdapter = new PickUpLocationAdapter(araryList, context);
		pickUpLocationAdapter.setSelectedIndex(fixPosition );		
		listView.setDivider(null);
		listView.setAdapter(pickUpLocationAdapter);	
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView locationId = (TextView)view.findViewById(R.id.pickup_location_id);
				TextView location =  (TextView)view.findViewById(R.id.pickup_location);
				TextView sortTextView =  (TextView)view.findViewById(R.id.pickup_location_sort);
				String sortStr = sortTextView.getText().toString().replace(". ", "");
				fixPosition = position;
				startLocationProduct( locationId.getText().toString(), location.getText().toString(),pickUpId, Integer.parseInt(sortStr));
 			}
		});
		 
	}
	 
	private void startLocationProduct(String locationId , String location , long pickupId , int sortTemp  ){
		Intent intent = new Intent(context, PickUpLocationContainerActivity.class);
	 
		intent.putExtra("location_id", StringUtil.convert2Long(locationId) );
		intent.putExtra("location", location);
  		intent.putExtra("pickup_id", pickupId);
  		startActivityForResult(intent, 2);
		 
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
			if(requestCode == 2){
 				loadData();
			}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			Intent intent = new Intent(this, PickUpSelectActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.push_from_right_out,R.anim.push_from_right_in);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onDestroy() {
		if(netBroadcastReceiver != null){
			unregisterReceiver(netBroadcastReceiver);
		}
		super.onDestroy();
	}
 private Handler submitShowPickUpInfo =  new Handler(){
		
		public void handleMessage(android.os.Message msg) {
			if(progressDialog != null && progressDialog.isShowing()){progressDialog.dismiss();}
			if(builder != null){
				builderDilog =  builder.show();
			}
		};
	};
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>提交数据<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		public void showPickUpInfo(View view){
			LinearLayout parentView =	(LinearLayout)view.getParent().getParent() ;
			final TextView locationView = (TextView)parentView.findViewById(R.id.pickup_location);
			final TextView locationIdView = (TextView)parentView.findViewById(R.id.pickup_location_id);

			final long location_id = StringUtil.convert2Long(locationIdView.getText().toString()) ;
			final String location = locationView.getText().toString();
			 
			progressDialog.show();
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					builder = new Builder(context);
					 
					LinearLayout pickupLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.pickup_submit_container_info_layout, null);
					TextView innerTextView = (TextView)pickupLayout.findViewById(R.id.title);
					innerTextView.setText(String.format("Location [%1s] PickUp Info", locationView.getText().toString().trim()));
					ListView listView =(ListView) pickupLayout.findViewById(R.id.pickup_info) ;
					final List<ProductCompareSimple>   datas = pickUpScanInfoDao.getLocationPickupInfo(pickUpId, location_id);
					
					listView.setAdapter(new PickUpProductListAdapter(datas, context));
					
					builder.setPositiveButton(resources.getString(R.string.sys_button_submit), new DialogInterface.OnClickListener(){

						@Override
						public void onClick(DialogInterface dialog, int which) {
						  boolean isCanSubmit = false ;	//没有数据的是不可以提交的
							if(datas != null){
								for(ProductCompareSimple simple : datas){
									if(simple.getPickupQty() > 0.0d){
										isCanSubmit = true ;
										break ;
									}
								}
							}
							submitPickupInfo(isCanSubmit,location_id,location);
						}
					});
					builder.setNegativeButton(resources.getString(R.string.sync_no), null);
					builder.setView(pickupLayout);
					submitShowPickUpInfo.obtainMessage().sendToTarget();
				}
			}).start();
			  
		}
		private Handler submitPickUpInfoHandler = new Handler(){
			public void handleMessage(android.os.Message msg) {
				if(progressDialog != null && progressDialog.isShowing()){progressDialog.dismiss();}
				if(builderDilog != null && builderDilog.isShowing()){builderDilog.dismiss();}
				if(msg.what == HttpResultCode.MSG_SUCCESS){
					//这里应该是调用readData();  
					loadBaseData();
				}else{
					AlertUi.showAlertBuilder(context, "Submit failed");
	
				}
			};
		};
		//读取这个位置没有提交的数据
		//提交目前拣货到的记录
		private void submitPickupInfo(boolean isCanSubmit , long location_id , String location ){
			if(!isCanSubmit){
				AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_no_scanInfo_cant_submit));
				return ; 
			}
			progressDialog.show();
			
			Thread thread = new Thread(new SubmitLocationProductRunable(context, location_id, pickUpId, location, submitPickUpInfoHandler)) ;
			thread.start();
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>提交拣货的数据结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		
		
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>提交位置差异<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		//提交位置差异
		//提交位置差异之前。要提示是否这个位置真的没有数据了
		public void beforeShowDifference(View view){
			//1.首先查询是否还有没有提交的数据
			//2.提交拣货的数据成功过后会下载最新的数据。所以要删除以前的数据
			LinearLayout parentView =	(LinearLayout)view.getParent().getParent() ;
			final TextView locationView = (TextView)parentView.findViewById(R.id.pickup_location);
			final TextView locationIdView = (TextView)parentView.findViewById(R.id.pickup_location_id);

			final long location_id = StringUtil.convert2Long(locationIdView.getText().toString()) ;
			final String location = locationView.getText().toString();
			
			int count = pickUpScanInfoDao.countPickupScanInfoNotSubmit(pickUpId, location_id);
			if(count > 0){
				AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_submit_scaninfo));
				return ;
			} 
			
			Builder builder =  new Builder(context);
			 
			LinearLayout pickupLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.pickup_confirm_exception_layout, null);
			builder.setTitle(resources.getString(R.string.sys_holdon));
			builder.setView(pickupLayout);
			
			builder.setNegativeButton(resources.getString(R.string.sync_no), null);
			builder.setPositiveButton(resources.getString(R.string.pickup_location_difference), new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
 					//计算位置差异
					showDifferent(location_id,location);
					if(alertDialog != null && alertDialog.isShowing()){alertDialog.dismiss();}
				}
				
			}); 
			alertDialog = builder.show();
		}
		
		private void showDifferent(final long location_id ,final String location){

			 
			currentLocation = location;
			progressDialog.show();
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					builder = new Builder(context);
					 
					LinearLayout pickupLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.pickup_submit_container_info_layout, null);
					TextView innerTextView = (TextView)pickupLayout.findViewById(R.id.title);
					innerTextView.setText(String.format("Location [%1s] PickUp Info", location));
					ListView listView =(ListView) pickupLayout.findViewById(R.id.pickup_info) ;
					final List<ProductCompareSimple>   datas = pickUpScanInfoDao.getLocationPickupInfo(pickUpId, location_id);
					
					listView.setAdapter(new PickUpProductListAdapter(datas, context));
					
					listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
						 
						@Override
						public void onItemClick(AdapterView<?> adpater, View view, int index, long id) {
							
 							TextView pcidView =(TextView)view.findViewById(R.id.pickup_location_pcid) ;
 							TextView qtyView =(TextView)view.findViewById(R.id.pcik_up_qty) ;
 							TextView totalView =(TextView)view.findViewById(R.id.pick_up_total_qty);
 							
 							
 							 
 							//要告诉zhanjie是PCID,ContainerType,ContianerTypeId,qty (containerType差几个)
 							//如果是这个地方是拣货商品的pcid,0,0,qty(商品的数量)
 							//查询数据库tbl_pickup_location_product.然后遍历出那些数据需要提交差异
 							
 							  
 							
 							submitDifferent(location_id,
 									StringUtil.convert2Long(pcidView.getText().toString()) ,
 									StringUtil.convert2Double(totalView.getText().toString()) 
 									 );
 							
						}
					});
				 
					builder.setNegativeButton(resources.getString(R.string.sync_no), null);
					builder.setView(pickupLayout);
					submitShowPickUpInfo.obtainMessage().sendToTarget();
				}
			}).start();
		}
		
		
		 private void submitDifferent(long location_id , long pcid , double totalQty  ){
			 progressDialog.show();
			 Thread thread = new Thread(new SubmitDifferentRunnable(context, pcid, location_id, pickUpId, totalQty,currentLocation,submitPickUpInfoHandler));
			 thread.start();
			 
		 }
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>提交位置结束<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}
