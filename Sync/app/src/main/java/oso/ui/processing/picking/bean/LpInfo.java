package oso.ui.processing.picking.bean;

import java.io.Serializable;

public class LpInfo implements Serializable {

	private long id; // android id
	private String typeName;
	private long lp; // 系统的LPId con_id
	private int isReceive; // 1.为receive green。2.为open new red 如果是open new
							// 那么就是需要提交的。
							// 如果是收托盘 为receive 。但是修改了里面的数据那么也是open new .
							// 这个时候要删除已经的数据在(web端)
	private String lpName; // LP编号
	private long bill_id; // 关联的ID android 端需要的数据

	private long containerTypeId; // 自身容器的类型ID
	private int containerHasSn; // 是否包含SN
	private int isFull; // 是否已经装满
	private String containerType; // TLP,BLP,CLP,ILP
	private int containerTypeInt; // 1,2,3,4,0
	private String subContainerType; // BLP,CLP,ILP,ALl
	private long subContainerTypeId; // -1 表示所有的都可以装 其他的情况就是 子容器
										// 类型的ID。到时候通过这个判断大的容器上能否放小的容器
	private int subCount; // 子容器可以放置的数量
	private long containerPcId; // 容器中的PCID;
	private String containerPcName; // 容器中的P_name
	private int containerPcCount; // 容器可以包含的商品数量的总和-1 表示没有限制

	private long parentContainerId; // 父容器的Container id ;

	private String locationName; // Container 所在的位置
	private long locationId; // container 所在的位置的Id

	private long titleId; // 所属人的TitleId
	private String title; // 所属人的名字
	private String lotNumber; // 批次号

	private int isAppendSuccess; // position 的时候添加的字段 1.表示成功. 2.0表示没有成功
	private int isServerCopy; // inventory 的时候添加的字段 （是否这个Container是去服务端Copy
								// 从Container copy到tempContainer）
								// 1表示是，0表示否

	private int isLocalCopy; // 在singleCreate的时候，如果在前面的页面已经添加了，在里面的其他的容器在放入原来添加的Container
								// 0,1

	public long getContainerTypeId() {
		return containerTypeId;
	}

	public void setContainerTypeId(long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}

	public LpInfo() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public long getLp() {
		return lp;
	}

	public void setLp(long lp) {
		this.lp = lp;
	}

	public int getIsReceive() {
		return isReceive;
	}

	public void setIsReceive(int isReceive) {
		this.isReceive = isReceive;
	}

	public String getLpName() {
		return lpName;
	}

	public void setLpName(String lpName) {
		this.lpName = lpName;
	}

	public long getBill_id() {
		return bill_id;
	}

	public void setBill_id(long bill_id) {
		this.bill_id = bill_id;
	}

	public int getContainerHasSn() {
		return containerHasSn;
	}

	public void setContainerHasSn(int containerHasSn) {
		this.containerHasSn = containerHasSn;
	}

	public int getIsFull() {
		return isFull;
	}

	public void setIsFull(int isFull) {
		this.isFull = isFull;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getSubContainerType() {
		return subContainerType;
	}

	public void setSubContainerType(String subContainerType) {
		this.subContainerType = subContainerType;
	}

	public long getSubContainerTypeId() {
		return subContainerTypeId;
	}

	public void setSubContainerTypeId(long subContainerTypeId) {
		this.subContainerTypeId = subContainerTypeId;
	}

	public int getSubCount() {
		return subCount;
	}

	public void setSubCount(int subCount) {
		this.subCount = subCount;
	}

	public long getContainerPcId() {
		return containerPcId;
	}

	public void setContainerPcId(long containerPcId) {
		this.containerPcId = containerPcId;
	}

	public String getContainerPcName() {
		return containerPcName;
	}

	public void setContainerPcName(String containerPcName) {
		this.containerPcName = containerPcName;
	}

	public int getContainerPcCount() {
		return containerPcCount;
	}

	public void setContainerPcCount(int containerPcCount) {
		this.containerPcCount = containerPcCount;
	}

	public long getParentContainerId() {
		return parentContainerId;
	}

	public void setParentContainerId(long parentContainerId) {
		this.parentContainerId = parentContainerId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public long getTitleId() {
		return titleId;
	}

	public void setTitleId(long titleId) {
		this.titleId = titleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	public int getIsAppendSuccess() {
		return isAppendSuccess;
	}

	public void setIsAppendSuccess(int isAppendSuccess) {
		this.isAppendSuccess = isAppendSuccess;
	}

	public int getIsServerCopy() {
		return isServerCopy;
	}

	public void setIsServerCopy(int isServerCopy) {
		this.isServerCopy = isServerCopy;
	}

	public int getContainerTypeInt() {
		return containerTypeInt;
	}

	public void setContainerTypeInt(int containerTypeInt) {
		this.containerTypeInt = containerTypeInt;
	}

	public int getIsLocalCopy() {
		return isLocalCopy;
	}

	public void setIsLocalCopy(int isLocalCopy) {
		this.isLocalCopy = isLocalCopy;
	}

}
