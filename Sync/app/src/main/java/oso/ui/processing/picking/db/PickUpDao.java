package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.bean.PickUp;
import support.dbhelper.DatabaseHelper;
import support.exception.SystemException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpDao {

	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup;
	
	public PickUpDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public PickUpDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	
	public void deleteAll(){
		db = helper.getWritableDatabase() ;
		db.delete(tableName, " 1=1 ", null);
		db.close();
	}
	
	public void addPickUp(List<PickUp> arrayList) throws Exception{
		if(arrayList != null && arrayList.size() > 0 ){
			
			db = helper.getWritableDatabase();
			try{
				db.beginTransaction();
				for(PickUp temp : arrayList){
					boolean flag = true ;
					Cursor cursor = db.rawQuery("select count(*) as count_sum from " + tableName + "  where pickup_id = ? " , new String[]{temp.getPickup_id()+""});
					if(cursor != null && cursor.getCount() > 0){
						cursor.moveToFirst();
						int count = cursor.getInt(cursor.getColumnIndex("count_sum"));
						if(count > 0){
							flag = false ;
						}
					}
					cursor.close();
					if(flag){
						ContentValues values = new ContentValues();
						values.put("pickup_id", temp.getPickup_id());
						values.put("type", temp.getType());
						values.put("isOver", temp.getIsOver());
						values.put("doorName", temp.getDoorName());
						values.put("locationName", temp.getLocationname());
						
						db.insert(tableName, null, values );
					}else{
						ContentValues values = new ContentValues();
					 
						values.put("doorName", temp.getDoorName());
						values.put("locationName", temp.getLocationname());
						db.update(tableName, values, " pickup_id = ? ", new String[]{temp.getPickup_id()+""});
					}
				}
				db.setTransactionSuccessful();
			}catch (Exception e) {
				e.printStackTrace();
				throw new SystemException("addPickUp " + e);
 			}finally{
 				db.endTransaction();
 				db.close();
 				
 			}
		}
	}
	public List<PickUp> queryAll(){
		db = helper.getReadableDatabase();
		List<PickUp> arrayList = new ArrayList<PickUp>();
		Cursor cursor = db.rawQuery("select * from " + tableName + " order by id desc ", null); 
		if(cursor != null && cursor.getCount() > 0){
			 while(!cursor.isLast()){
				 cursor.moveToNext();
				 PickUp temp = new PickUp();
				 temp.setId(cursor.getLong(cursor.getColumnIndex("id")));
				 temp.setPickup_id(cursor.getLong(cursor.getColumnIndex("pickup_id")));
				 temp.setType(cursor.getInt(cursor.getColumnIndex("type")));
				 temp.setIsOver(cursor.getInt(cursor.getColumnIndex("isOver")));
				 temp.setDoorName(cursor.getString(cursor.getColumnIndex("doorName")));
				 temp.setLocationname(cursor.getString(cursor.getColumnIndex("locationName")));
				 arrayList.add(temp);
			 }
		}
		cursor.close();
		db.close();
		return arrayList;
		
	}
 
	public PickUp getPickUpById(long pickup_id){
		db = helper.getReadableDatabase() ;
		PickUp pickUp = null ;
		Cursor cursor = db.rawQuery("select * from " + tableName + " where pickup_id = ? ", new String[]{pickup_id+""});
		if(cursor != null && cursor.getCount() > 0){
			cursor.moveToFirst();
			pickUp = new PickUp() ;
			pickUp.setId(cursor.getLong(cursor.getColumnIndex("id")));
			pickUp.setPickup_id(cursor.getLong(cursor.getColumnIndex("pickup_id")));
			pickUp.setType(cursor.getInt(cursor.getColumnIndex("type")));
			pickUp.setIsOver(cursor.getInt(cursor.getColumnIndex("isOver")));
		}
		cursor.close();
		db.close();
		return pickUp;
	}
	public void updatePickupIsOver(int isOver , long pickup_id ){
		db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("isOver", isOver);
		db.update(tableName, values , " pickup_id = ? ", new String[]{pickup_id+""});
		db.close();
		
	}
	
	
}
