package oso.ui.processing.picking.bean;

public class PickUpScanInfo {

	private int id ;
	private String sn ;
	private long sku ;	//当成Pcid 处理
	private double qty ;
	private long pickup_id ;
	private long location_id ;
	private int isSubmit = 0 ;
	private String pname ;
	private String barCode ;
	private String code_type ;				//barCodeTYpe;
	
	private long from_container_id ;		//具体的Container_id
	private int from_container_type ;		//具体的Container_type 1,2,3,4(BLP,CLP,ILP,TLP)
	private long from_container_type_id ;	//具体的Container_type_id	
	
	private long pickup_container_id ;		//已经拣到的Container_id;
	private int pickup_container_type ;		//已经捡到的Containe
	private long pickup_container_type_id ;	
	
	private String container	;			//容器的名字
	private long from_location_id ;	//这个是没有货的时候用的字段
	
	public int getIsSubmit() {
		return isSubmit;
	}
	public void setIsSubmit(int isSubmit) {
		this.isSubmit = isSubmit;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public long getSku() {
		return sku;
	}
	public void setSku(long sku) {
		this.sku = sku;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public long getPickup_id() {
		return pickup_id;
	}
	public void setPickup_id(long pickup_id) {
		this.pickup_id = pickup_id;
	}
	public long getLocation_id() {
		return location_id;
	}
	public void setLocation_id(long location_id) {
		this.location_id = location_id;
	}
	public long getFrom_location_id() {
		return from_location_id;
	}
	public void setFrom_location_id(long from_location_id) {
		this.from_location_id = from_location_id;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCode_type() {
		return code_type;
	}
	public void setCode_type(String code_type) {
		this.code_type = code_type;
	}
	public long getFrom_container_id() {
		return from_container_id;
	}
	public void setFrom_container_id(long from_container_id) {
		this.from_container_id = from_container_id;
	}
	public int getFrom_container_type() {
		return from_container_type;
	}
	public void setFrom_container_type(int from_container_type) {
		this.from_container_type = from_container_type;
	}
	public long getFrom_container_type_id() {
		return from_container_type_id;
	}
	public void setFrom_container_type_id(long from_container_type_id) {
		this.from_container_type_id = from_container_type_id;
	}
	public long getPickup_container_id() {
		return pickup_container_id;
	}
	public void setPickup_container_id(long pickup_container_id) {
		this.pickup_container_id = pickup_container_id;
	}
	public int getPickup_container_type() {
		return pickup_container_type;
	}
	public void setPickup_container_type(int pickup_container_type) {
		this.pickup_container_type = pickup_container_type;
	}
	public long getPickup_container_type_id() {
		return pickup_container_type_id;
	}
	public void setPickup_container_type_id(long pickup_container_type_id) {
		this.pickup_container_type_id = pickup_container_type_id;
	}
	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	
	
}
