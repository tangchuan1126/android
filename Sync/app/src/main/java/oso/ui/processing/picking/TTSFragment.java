package oso.ui.processing.picking;

import java.util.HashMap;
import java.util.Locale;

import support.common.UIHelper;
import support.dbhelper.ConfigDao;
import utility.MediaPlayerUtil;
import utility.StringUtil;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import declare.com.vvme.R;
public class TTSFragment extends FragmentActivity implements OnInitListener {
	/** Called when the activity is first created. */
	private static final int REQ_TTS_STATUS_CHECK = 0;
	private static final String TAG = "TTS Demo";
	private TextToSpeech mTts;
 	private ConfigDao cf;
 	private static HashMap<String, String> params = new HashMap<String, String>();
 	private boolean speakAvailable = false ;
 	
 	
 	protected MediaPlayer successPlayer = null;
 	
 	protected MediaPlayer alertPlayer = null;
	  
// 	static {
// 		params.put(Engine.KEY_PARAM_VOLUME, "0.4");
// 	}
 	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new ConfigDao(TTSFragment.this);
		// 检查TTS数据是否已经安装并且可用
		initPlayers();
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK);
		
	}

	private void initPlayers() {
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		successPlayer = MediaPlayerUtil.getInitMediaPlayer(R.raw.tinkerbell,  getResources() );
		alertPlayer = MediaPlayerUtil.getInitMediaPlayer(R.raw.error ,  getResources() );
	}
	private void speed() {
		// TODO Auto-generated method stub
		String speed = cf.getConfigValue("sp_speed");
		if(speed!=null&&speed.length()>0){
			mTts.setSpeechRate(Float.parseFloat(cf.getConfigValue("sp_speed")));
		}else{
			mTts.setSpeechRate(1.0f);
		}
	}


	/**
	 * 全部读取。不管系统的(读取语言截取的)配置
	 * 不加空格
	 * @param value
	 */
	public void speakNotify(String value){
		if(!speakAvailable){return ;}
		speed();
			
		mTts.speak(value.toString(),TextToSpeech.QUEUE_ADD, params);
 	}
	/**
	 * 前缀的全部读取 不加空格 
	 * value 根据系统配置的(读取语言截取的) 读取 
	 * 比如读取value 的前2位 。 后2 位
	 * @param prefix
	 * @param value
	 */
	public void speakAll(String prefix , String value ){
		if(!speakAvailable){return ;}
		String moren = cf.getConfigValue("sp_model");
		String sp_model_length = cf.getConfigValue("sp_model_length");
		String str = StringUtil.addSpace(value);
		speed();
		if (!moren.equals("")){
			if (moren.equals("1")) {
				if(str.length()<=Integer.parseInt(sp_model_length)*2){
					mTts.speak(prefix.toString() + " " + str.toString(),TextToSpeech.QUEUE_ADD, params);
				}else{
					mTts.speak(prefix.toString() + " " + str.substring(0, Integer.parseInt(sp_model_length)*2).toString(),TextToSpeech.QUEUE_ADD, params);
				}
			} 
			else if (moren.equals("2")) {
				if(str.length() <= Integer.parseInt(sp_model_length) * 2 ){
					mTts.speak(prefix.toString() + " " + str.toString(),TextToSpeech.QUEUE_ADD, params);
				}else{
					mTts.speak(prefix.toString() + " " + str.substring(str.length()-Integer.parseInt(sp_model_length)*2).toString(),TextToSpeech.QUEUE_ADD, params);
				}
			} else{
				mTts.speak(prefix.toString() + " " + str.toString(),TextToSpeech.QUEUE_ADD, params);
			}
		}
//			else if (moren.equals("2")) {
//				if (value.length() > 2) {
//					mTts.speak(prefix.toString() + " "
//							+ str.substring(0, 3).toString(),
//							TextToSpeech.QUEUE_ADD, params);
//				} else {
//					mTts.speak(prefix.toString() + " " + str.toString(),
//							TextToSpeech.QUEUE_ADD, params);
//				}
//			} else if (moren.equals("3")) {
//				mTts.speak(
//						prefix.toString()
//								+ str.substring(str.length() - 2, str.length())
//										.toString(), TextToSpeech.QUEUE_ADD,
//										params);
//	 
//			} else if (moren.equals("4")) {
//				if (value.length() > 2) {
//					mTts.speak(
//							prefix.toString()
//									+ " "
//									+ str.substring(str.length() - 4,
//											str.length()).toString(),
//							TextToSpeech.QUEUE_ADD, params);
//				} else {
//					mTts.speak(prefix.toString() + " " + str.toString(),
//							TextToSpeech.QUEUE_ADD, params);
//				}
//			}else if(moren.equals("5")){
//				mTts.speak(prefix.toString() + " " + str.toString(),
//						TextToSpeech.QUEUE_ADD, params);
//			}
	}
	/**
	 * 前缀的全部读取 不加空格 
	 * value 根据系统配置的(读取语言不截取的) 读取 
	 * 比如读取value 的前2位 。 后2 位
	 * @param prefix
	 * @param value
	 */
	public void speakUnIntercept(String prefix , String value){
		if(!speakAvailable){return ;}
		String str = StringUtil.addSpace(value);
		speed();
		
 		mTts.speak(prefix.toString() + " " + str.toString(),
						TextToSpeech.QUEUE_ADD, params);
	}
	/**
	 * 系统的设置的读取 比如891829
	 * 那么读取 29(如果系统设置的是后两位)
	 * 加空格
	 * @param value
	 */
	public void speakAllByConfig(String value){
		if(!speakAvailable){return ;}
		String moren = cf.getConfigValue("sp_model");
		String sp_model_length = cf.getConfigValue("sp_model_length");
		speed();
		String str = StringUtil.addSpace(value);
		
		if (!moren.equals("")){
			if (moren.equals("1")) {
				if(str.length()<=Integer.parseInt(sp_model_length)*2){
					mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
				}else{
					mTts.speak(str.substring(0, Integer.parseInt(sp_model_length)*2).toString(),TextToSpeech.QUEUE_ADD, params);
				}
			} 
			else if (moren.equals("2")) {
				if(str.length()<=Integer.parseInt(sp_model_length)*2){
					mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
				}else{
					mTts.speak(str.substring(str.length()-Integer.parseInt(sp_model_length)*2).toString(),TextToSpeech.QUEUE_ADD, params);
				}
			} else{
				mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
			}
		}
			
//			if (moren.equals("1")) {
//				    mTts.speak(str.substring(0, 1).toString(),TextToSpeech.QUEUE_ADD, params);
//			} else if (moren.equals("2")) {				
//				if (str.length() > 2) {mTts.speak(str.substring(0, 3).toString(),TextToSpeech.QUEUE_ADD, params);
//				} else {
//					mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
//				}
//			} else if (moren.equals("3")) {
//				mTts.speak(str.substring(str.length() - 2, str.length()).toString(), TextToSpeech.QUEUE_ADD,params);
//			} else if (moren.equals("4")) {
//				if (str.length() > 2) {
//					mTts.speak(str.substring(str.length() - 4,str.length()).toString(),TextToSpeech.QUEUE_ADD, params);
//				} else {
//					mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
//				}
//			}else if(moren.equals("5")){
//				mTts.speak(str.toString(),TextToSpeech.QUEUE_ADD, params);
//			}
	}


//	public void sp_slow(String ed) {
//		mTts.setSpeechRate(1.0f);
//		cf.updateConfigDao("sp_speed", 1);
//		// mTts.speak("how are you", TextToSpeech.QUEUE_ADD, null);
//		mTts.speak(ed.toString(), TextToSpeech.QUEUE_ADD, params);
//	}
//	public void sp_mode(String ed) {
//		mTts.setSpeechRate(1.5f);
//		cf.updateConfigDao("sp_speed", 2);
//		// mTts.speak("how are you", TextToSpeech.QUEUE_ADD, null);
//		mTts.speak(ed.toString(), TextToSpeech.QUEUE_ADD, params);
//	}
//
//	public void sp_quick(String ed) {
//		mTts.setSpeechRate(2.0f);
//		cf.updateConfigDao("sp_speed", 3);
//		// mTts.speak("how are you", TextToSpeech.QUEUE_ADD, null);
//		mTts.speak(ed.toString(), TextToSpeech.QUEUE_ADD, params);
//	}

	 
	// 实现TTS初始化接口
	@Override
	public void onInit(int status) {
 		// TTS Engine初始化完成
		if (status == TextToSpeech.SUCCESS) {
			int result = mTts.setLanguage(Locale.US);
			// 设置发音语言
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED)
			// 判断语言是否可用
			{
				Log.v(TAG, "Language is not available");
			} else {
				// mTts.speak("ha ha ha", TextToSpeech.QUEUE_ADD, null);
				speakAvailable = true ;
			}
		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQ_TTS_STATUS_CHECK) {
			switch (resultCode) {
			case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
			// 这个返回结果表明TTS Engine可以用
			{
				mTts = new TextToSpeech(this, this);
				Log.v(TAG, "TTS Engine is installed!");

			}

				break;
			case TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA:
				// 需要的语音数据已损坏
			case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA:
				// 缺少需要语言的语音数据
			case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME:
			// 缺少需要语言的发音数据
			{
				// 这三种情况都表明数据有错,重新下载安装需要的数据
				Log.v(TAG, "Need language stuff:" + resultCode);
				Intent dataIntent = new Intent();
				dataIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(dataIntent);

			}
				break;
			case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:
				// 检查失败
			default:
				Log.v(TAG, "Got a failure. TTS apparently not available");
				break;
			}
		} else {
			// 其他Intent返回的结果
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mTts != null)
		// activity暂停时也停止TTS
		{
			mTts.stop();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// 释放TTS的资源
		if(mTts != null){
			mTts.shutdown();
		}
	}
	
	 /**
	  * 
	  * @param value	传入的值
	  * @param subIndex	表示的是截取的几位
	  * @param oper	1.表示前面读取 。2表示的是从后面截取
	  * @return
	  */
	private String fixString(String value,int subIndex , int oper ){
		
		if(value != null){
			if(value.length() <= subIndex){
				return value ;
			}else{
				if(oper == 1){
					return value.substring(0, subIndex);
				}else{
					return value.substring(value.length() -subIndex, value.length());
				}
			}
		}
		return "" ;
	}
	
	 
		/**
		 * 
		 * @param isNeedAlertNoftify  是否是需要alert 声音 failed的声音
		 * @param alertMessage			//文字提示的
		 * @param speakModel			//speakModel		0.表示的是notify
		 * @param isNeedSpeak			//是否需要speak
		 * @param alertSpeakMessage		//alertSpeakMessage 说话的声音
		 */
		protected void alertMessage(boolean isNeedAlertNoftify , String alertMessage, int speakModel ,boolean isNeedSpeak ,  String alertSpeakMessage){
			if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(alertPlayer);}
			if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length() > 0){
				if(speakModel == 0){
					speakNotify(alertSpeakMessage);
				}
			}
			if(alertMessage != null){
				UIHelper.showToast(this, alertMessage, Toast.LENGTH_SHORT).show();
			}
		}
		/**
		 * 
		 * @param isNeedAlertNoftify
		 * @param alertMessage
		 * @param speakModel	0.是Notify 读取所有的英文 ,1.是前面是英文,后面是一个一个读取,2.是前面是英文,后面是按照系统的配置的读取
		 * @param isNeedSpeak	
		 * @param alertSpeakMessage 
		 * @param alertValue
		 */
		protected void alertMessageSuccess(boolean isNeedAlertNoftify , String alertMessage, int speakModel ,boolean isNeedSpeak ,  String alertSpeakMessage , String alertValue){
			if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(successPlayer);}
			if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length() > 0){
				if(speakModel == 0){
					speakNotify(alertSpeakMessage);
				}
				if(speakModel == 1){
	 				speakUnIntercept(alertSpeakMessage, alertValue);
				}
				if(speakModel == 2){
					speakAll(alertSpeakMessage, alertValue);
				}
			}
			if(alertMessage != null)
				UIHelper.showToast(this, alertMessage, Toast.LENGTH_SHORT).show();
		}
}