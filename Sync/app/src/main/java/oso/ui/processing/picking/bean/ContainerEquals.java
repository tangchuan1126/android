package oso.ui.processing.picking.bean;

 
/**
 * 用于判断Container是否是相等的
 * @author zhangrui
 *
 */
public class ContainerEquals{
	
	
	private Long containerId ; 
	
	private Long containerTypeId ;
	
	private Integer containerType ;
	
	public ContainerEquals(Long containerId , Long containerTypeId, Integer containerType) {
		super();
		this.containerId = containerId ;
		this.containerTypeId = containerTypeId;
		this.containerType = containerType;
	}
	
	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}

	public Long getContainerTypeId() {
		return containerTypeId;
	}

	public void setContainerTypeId(Long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}

	public Integer getContainerType() {
		return containerType;
	}

	public void setContainerType(Integer containerType) {
		this.containerType = containerType;
	}

	@Override
	public int hashCode() {
 		return  (containerTypeId+"").hashCode() + (containerType+"").hashCode() +( containerId + "").hashCode();
	}
	@Override
	public boolean equals(Object o) {
		ContainerEquals other = (ContainerEquals)o;
		boolean flag =( other.getContainerType().equals(this.containerType) 
				&& other.getContainerTypeId().equals(this.containerTypeId)) 
				&& other.getContainerId().equals(this.containerId);
		 
 		return flag ;
 	}
	
	 
}
