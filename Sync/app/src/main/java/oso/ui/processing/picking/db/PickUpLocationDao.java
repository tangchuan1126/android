package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.bean.PickUpLocation;
import support.dbhelper.DatabaseHelper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpLocationDao {
	
	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup_location;
	
	public PickUpLocationDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public PickUpLocationDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	 
 
	 
	 
	public synchronized List<PickUpLocation>   getPickUpLoactionByPickUpId(long pickup_id){
		 
		List<PickUpLocation> array =  new ArrayList<PickUpLocation>() ;
		db = helper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from  " + tableName + " where pickup_id=?  order by sort ", new String[]{pickup_id+""});
		if(cursor != null && cursor.getCount() > 0){
		 
			while(!cursor.isLast()){
				cursor.moveToNext();
				PickUpLocation location = new PickUpLocation();
				location = new PickUpLocation();
				location.setId(cursor.getLong(cursor.getColumnIndex("id")));
				location.setLocation(cursor.getString(cursor.getColumnIndex("location")));
				location.setPickup_id(pickup_id);
				location.setSort(cursor.getInt(cursor.getColumnIndex("sort")));
				location.setLocation_id(cursor.getLong(cursor.getColumnIndex("location_id")));
				location.setTotalQty(cursor.getDouble(cursor.getColumnIndex("totalQty")));
				location.setScanQty(cursor.getDouble(cursor.getColumnIndex("scanQty")));
				location.setIslodeProduct(cursor.getInt(cursor.getColumnIndex("islodeProduct")));
				location.setLocation_type(cursor.getInt(cursor.getColumnIndex("location_type")));
				array.add(location);
			}
			
		}
		cursor.close();
		db.close();
		return array ;
	}
	 
 
	 
  
}
