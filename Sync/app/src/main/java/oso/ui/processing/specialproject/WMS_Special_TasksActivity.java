package oso.ui.processing.specialproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.processing.specialproject.Interface.IMainTask;
import oso.ui.processing.specialproject.adapter.MainTaskAdp;
import oso.ui.processing.specialproject.bean.MainTaskBean;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import oso.ui.processing.specialproject.view.Special_TaskInfoHeaderView;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import support.anim.AnimUtils;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 
 * @description:Sepcial Project 主任务列表
 *
 * @author xialimin
 * @time : 2015-6-11 下午5:05:33
 */
public class WMS_Special_TasksActivity extends BaseActivity implements IMainTask {

	private static final String CONTENT_PROJECT_ID = "Content_Project_Id";

	private Context mContext;

	private View mHeaderView;

	private ExpandableListView mTaskLv;

	private MainTaskAdp mTaskAdp;
	
	private MainTaskBean mTask;

	private int mPosition, mTop;  // ListView位置状态保存
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_special_project_task_list, 0);
		
		applyParams();
		
		initView();
		
		initData();
		
		initListener();
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		String projectId = getIntent().getStringExtra(CONTENT_PROJECT_ID);
		reqMainTask(mTask == null ? projectId : mTask.project_detail.schedule_id);
	}

	public static void initParams(Intent in, String proId) {
		in.putExtra(CONTENT_PROJECT_ID, proId);
	}
	
	private void applyParams() {
	}
	
	private void initView() {
		
		mContext = this;
		setTitleString(getString(R.string.specialtask_maintask_title));
		
		mTaskLv = (ExpandableListView) findViewById(R.id.lv_main_task);
		TextView emptyView = (TextView) findViewById(R.id.tvMainTaskEmpty);
		mTaskLv.setEmptyView(emptyView);

		mHeaderView = Special_TaskInfoHeaderView.getHeaderViewLayout_Project(mContext, mTask);
		mHeaderView.findViewById(R.id.btnAddMainTask).setOnClickListener(new OnAddBtnClickListener());
		mTaskLv.addHeaderView(mHeaderView);
		
		mTaskAdp = new MainTaskAdp(mContext, mTask, this);
		mTaskLv.setAdapter(mTaskAdp);
		mTaskAdp.expandAll(mTaskLv);
		
	}
	
	private void initData() {
		
	}
	
	private void initListener() {
		
		// 添加主任务
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// 判断非空
				if (mTask == null) return;
				if (mTask.project_detail == null) return;
				if (TextUtils.isEmpty(mTask.project_detail.schedule_id)) return;

				// 请求主任务列表 重置适配器
				String projectId = mTask.project_detail.schedule_id;
				reqMainTask(projectId);
			}
		});
		

	}

    /**
     * 根据ProjectID 请求主Task
     * @param projectId
     */
    private void reqMainTask(String projectId) {

		saveCurrentPosition();

        RequestParams params = new RequestParams();
        params.put("key", "getMainTaskGroupByTime");
        params.put("schedule_id", projectId);
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {

                // 请求成功  重置适配器
                mTask = new Gson().fromJson(json.toString(), MainTaskBean.class);
                mTaskAdp = new MainTaskAdp(mContext, mTask, WMS_Special_TasksActivity.this);
                mTaskLv.setAdapter(mTaskAdp);
				setHeaderViewData();

				restorePosition();
                mTaskAdp.expandAll(mTaskLv);
            }
        }.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);

    }

	/**
	 * 设置HeaderView数据
	 */
	private void setHeaderViewData() {
		if(mTask != null && mTask.project_detail != null) {
			((TextView)mHeaderView.findViewById(R.id.tx_sp_warehouse)).setText(mTask.project_detail.warehouse + "");
			((TextView)mHeaderView.findViewById(R.id.tx_sp_projectno)).setText(mTask.project_detail.schedule_id + "");
			((TextView)mHeaderView.findViewById(R.id.tx_project_subject)).setText(mTask.project_detail.subject+"");
			((TextView)mHeaderView.findViewById(R.id.tx_project_startdate)).setText(mTask.project_detail.start_time+"");
			((TextView)mHeaderView.findViewById(R.id.tx_project_enddate)).setText(mTask.project_detail.end_time+"");
		}

	}


	/**
	 * 菜单Dialog 
	 * @param position
	 * 
	 * Edit 和 Delete
	 */
	private void showSlcDialog(final int position) {
		new RewriteBuilderDialog.Builder(WMS_Special_TasksActivity.this).setTitle(getString(R.string.sync_operation))
		.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,	int which) {
				dialog.dismiss();
			}
			}).setArrowItems(new String[] {"Edit", "Delete" },
			new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
					onSlcDialogItemClick(position, itemPosition);
				}
			}).show();
	}
	
	/**
	 * Operate Dialog 选择选项后
	 * @param position     数据源的position
	 * @param itemPosition 选项的position
	 */
	private void onSlcDialogItemClick(int position, int itemPosition) {
		switch (itemPosition) {
		case SpecialTaskKey.OPERATE_EDIT:
			
			
			
			showEditDlg(position);
			break;
		case SpecialTaskKey.OPERATE_DELETE:
			showDelDlg(position);
			break;
		}					
	}
	
	/**
	 * 删除主任务
	 * @param position
	 */
	private void showDelDlg(final int position) {
		RewriteBuilderDialog.newSimpleDialog(mContext, "Delete?", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				/*int realPos = position - mTaskLv.getHeaderViewsCount();
				if(realPos < 0) return;
				
				//---删除----
				final MainTaskBean task = mTask.get(realPos);
				task.type = SpecialTaskKey.TYPE_PENDING;
				mTaskAdp.notifyDataSetChanged();*/

			}
		}).show();
	}

	/**
	 * 修改主任务
	 * @param position
	 */
	private void showEditDlg(int position) {
		/*int realPos = position - mTaskLv.getHeaderViewsCount();
		if(realPos < 0) return;
		final MainTaskBean task = mTask.get(realPos);
		
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
//		final EditText etSubject = (EditText) view.findViewById(R.id.e_subject);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
//		etSubject.setText(task.mainTaskName);
		etDescription.setText(task.mainTaskDes);
//		etCusorToLast(etSubject);
		etCusorToLast(etDescription);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Update Task"); 
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
//			String subjectStr = etSubject.getText().toString();
			String descriptionStr = etDescription.getText().toString();
			if(!isValid(etDescription)) return;
			
			//---修改----
//			task.mainTaskName = subjectStr;
			task.mainTaskDes = descriptionStr;
			task.type = SpecialTaskKey.TYPE_PENDING;
			mTaskAdp.notifyDataSetChanged();
			
			dlg.dismiss();
		}

		}).show();*/
	}

	/**
	 * 打开添加任务Dialog
	 */
	private void showAddTaskDialog() {
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
//		final EditText etSubject = (EditText) view.findViewById(R.id.e_subject);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Add Task"); 
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
//			String subjectStr = etSubject.getText().toString();
				String descriptionStr = etDescription.getText().toString();
				if (!isValid(etDescription)) return;
				submitAddTask(descriptionStr);
				dlg.dismiss();
			}

		}).show();

		etDescription.requestFocus();

		Utility.showSoftkeyboard(mContext, etDescription);

	}
	
	/**
	 * 判断输入值是否非法
	 * @param tvDes
	 * @return
	 */
	private boolean isValid(TextView tvDes) {
		boolean isValid = true; // 有效的

		if(TextUtils.isEmpty(tvDes.getText().toString())) {
			AnimUtils.horizontalShake(mContext, tvDes);
			isValid = false;
		}
		return isValid;
	}
	
	/**
	 * 请求添加MainTask
	 * @param descriptionStr
	 */
	private void submitAddTask(String descriptionStr) {
		/*MainTaskBean mainTask = new MainTaskBean();
//		mainTask.mainTaskName = subjectStr;
		mainTask.mainTaskDes = descriptionStr;
		mainTask.type = SpecialTaskKey.TYPE_PENDING;
		mTask.add(mainTask);
		mTaskAdp.notifyDataSetChanged();
		int realPos = mTaskLv.getHeaderViewsCount() + mTask.size() - 1;
		if(realPos < 0) return;
		mTaskLv.smoothScrollToPosition(realPos);*/
	}
	
	/**
	 * @description: 添加主任务
	 *
	 * @author xialimin
	 * @time : 2015-6-16 上午10:01:37
	 */
	private class OnAddBtnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			showAddTaskDialog();
		}
		
	}


	@Override
	public void onChildClick(String schedule_id, int main_task_status) {
		//reqSubMainTask(schedule_id, main_task_status);
		Intent in = new Intent(WMS_Special_TasksActivity.this, WMS_Special_SubTasksActivity.class);
		WMS_Special_SubTasksActivity.initParams(in, schedule_id, main_task_status);
		startActivity(in);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	/**
	 * ListView位置状态保存
	 */
	private void saveCurrentPosition() {
		if (mTaskLv != null) {
			int position = mTaskLv.getFirstVisiblePosition();
			View v = mTaskLv.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();
			//保存position和top位置
			mPosition = position;
			mTop = top;
		}
	}

	/**
	 * ListView位置恢复
	 */
	private void restorePosition() {
		if (mTask != null && mTaskLv != null) {
			int position = mPosition;//取出保存的数据
			int top = mTop;//取出保存的数据
			mTaskLv.setSelectionFromTop(position, top);
		}
	}

}
