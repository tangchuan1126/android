package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oso.base.BaseActivity;
import oso.ui.processing.picking.bean.PickUp;
import oso.ui.processing.picking.db.PickUpDao;
import oso.ui.processing.picking.parse.HttpResultCode;
import oso.ui.processing.picking.runnable.GetPickUpListRunnable;
import support.common.UIHelper;
import support.dbhelper.ConfigDao;
import utility.Utility;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

public class PickUpSelectActivity extends BaseActivity {

	private Context context;

	private Resources resources;

	private ListView listView;

	private ProgressDialog getPickUpDialog;

	private PickUpDao pickUpDao;

	private ConfigDao configDao;

	// private Button btn_black,btn_ref;

	private AlertDialog.Builder pickupChangeBuilder;

	private TextView any_text;

	private Handler getPickUphandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			getPickUpDialog.dismiss();
			switch (msg.what) {
			case HttpResultCode.MSG_FAILURE:
				any_text.setVisibility(View.VISIBLE);
				// any_text.setBackgroundColor(Color.BLACK);
				// any_text.setTextColor(Color.YELLOW);
				UIHelper.showToast(context, resources.getString(R.string.sys_syserror_getData_failed), Toast.LENGTH_SHORT).show();
				break;
			case HttpResultCode.MSG_NO_DATA:
				any_text.setVisibility(View.VISIBLE);
				// any_text.setBackgroundColor(Color.BLACK);
				// any_text.setTextColor(Color.YELLOW);
				break;
			}
			getData();
		}
	};

	private void getData() {
		List<PickUp> arrayList = pickUpDao.queryAll();
		PickUp pickUp = new PickUp();
		pickUp.setPickup_id(2l);
		pickUp.setLocationname("LocationName");
		pickUp.setDoorName("DoorName");
		pickUp.setId(1l);
		arrayList.add(pickUp);
		if (arrayList.size() < 1) {
			return;
		}
		List<Map<String, String>> value = new ArrayList<Map<String, String>>();
		for (PickUp temp : arrayList) {
			Map<String, String> tempMap = new HashMap<String, String>();
			tempMap.put("pickup_id", temp.getPickup_id() + "");
			tempMap.put("door_name", temp.getDoorName());
			tempMap.put("location_name", temp.getLocationname());

			//debug
			if(TextUtils.isEmpty(temp.getDoorName()))
				continue;
			value.add(tempMap);
		}

		SimpleAdapter adapter = new SimpleAdapter(context, value, R.layout.pickup_select_layout, new String[] { "pickup_id", "door_name",
				"location_name" }, new int[] { R.id.pickup_select_pickup_id, R.id.pickup_select_pickup_doorName,
				R.id.pickup_select_pickup_locationName });
		listView.setDivider(null);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TextView pickup_id = (TextView) view.findViewById(R.id.pickup_select_pickup_id);
				String value = pickup_id.getText().toString();
				String oldValue = configDao.getConfigValue("picking_id");

				if (!oldValue.equals("0") && !oldValue.equals(value)) {
					//
					affarPickupChanged(value, oldValue);

				} else {
					Intent pickUpIntent = new Intent(PickUpSelectActivity.this, PickUpLocationActivity.class);
					pickUpIntent.putExtra("pickup_id", value);
					pickUpIntent.putExtra("refresh", "true");
					startActivity(pickUpIntent);
					overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
					finish();
				}
			}
		});

	}

	private void affarPickupChanged(final String pickup_id, final String oldPickupId) {
		pickupChangeBuilder = new Builder(context);
		pickupChangeBuilder.setTitle(resources.getString(R.string.sys_holdon));

		String notity = String.format(resources.getString(R.string.give_up_pick_up_location), oldPickupId + "", pickup_id + ""); // "确定放弃["+oldPickupId+"]拣货单,改拣货["+pickup_id+"]?";

		pickupChangeBuilder.setMessage(notity);
		pickupChangeBuilder.setCancelable(false);
		pickupChangeBuilder.setPositiveButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent pickUpIntent = new Intent(PickUpSelectActivity.this, PickUpLocationActivity.class);
				pickUpIntent.putExtra("pickup_id", pickup_id);
				/*
				 * if(!oldPickupId.equals("0") &&
				 * !oldPickupId.equals(pickup_id)){
				 * configDao.deletePickupData(Long.parseLong(oldPickupId)); }
				 */
				startActivity(pickUpIntent);
				overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
				finish();
			}
		});
		pickupChangeBuilder.setNegativeButton(resources.getString(R.string.sync_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		pickupChangeBuilder.create().show();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pickup_type_select_activity_layout, 0);
		setTitleString("Pick Up");
		listView = (ListView) findViewById(R.id.pickup_type_select_fix_fix);
		context = PickUpSelectActivity.this;
		resources = getResources();
		pickUpDao = new PickUpDao(context);
		configDao = new ConfigDao(context);
		loadPickList();
		setButton();
	}

	public void setButton() {
		any_text = (TextView) findViewById(R.id.any_data);
		any_text.setText(""); // 先不用韩总测试的时候先不用
		// btn_ref=(Button)findViewById(R.id.picup_ref);
		// btn_black=(Button)findViewById(R.id.picup_black);
		// btn_ref.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// loadPickList();
		// }
		// });
		showRightButton(R.drawable.btn_ref_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				loadPickList();
			}
		});
		any_text.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// loadPickList();
				// 先不用韩总测试的时候先不用
			}
		});
		// btn_black.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// closeThisActivity();
		// }
		// });
	}

	private void loadPickList() {
		if (Utility.isConnectNet(context)) {
			getPickUpDialog = new ProgressDialog(context);
			getPickUpDialog.setTitle(resources.getString(R.string.sys_holdon));
			getPickUpDialog.setMessage(resources.getString(R.string.get_pick_up_list));
			Thread thread = new Thread(new GetPickUpListRunnable(getPickUphandler, context));
			thread.start();
			getPickUpDialog.show();
		} else {
			UIHelper.showToast(context, resources.getString(R.string.no_net_and_refresh_data_failed), Toast.LENGTH_SHORT).show();
			getData();
		}
	}

}
