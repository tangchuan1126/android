package oso.ui.processing.picking.bean;

import java.io.Serializable;

public class PickUpLocation implements Serializable {
	
	private long id ;
	private long pickup_id ;
	private String location ;
	private int sort ;
	private long location_id ;
 	
	private double totalQty ;
	
	private double scanQty = 0.0d;
	
	private int islodeProduct = 0;	//0没有  1有
	
	private String pname ;
	
	private int location_type ; 	//2.表示的是2D位置.3.表示的是3D位置
	
	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	public double getScanQty() {
		return scanQty;
	}
	public void setScanQty(double scanQty) {
		this.scanQty = scanQty;
	}
	public long getLocation_id() {
		return location_id;
	}
	public void setLocation_id(long location_id) {
		this.location_id = location_id;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPickup_id() {
		return pickup_id;
	}
	public void setPickup_id(long pickup_id) {
		this.pickup_id = pickup_id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getIslodeProduct() {
		return islodeProduct;
	}
	public void setIslodeProduct(int islodeProduct) {
		this.islodeProduct = islodeProduct;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getLocation_type() {
		return location_type;
	}
	public void setLocation_type(int location_type) {
		this.location_type = location_type;
	}
	 
	
}
