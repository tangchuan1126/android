package oso.ui.processing.picking.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oso.ui.processing.picking.bean.CommonHolder;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import support.dbhelper.Goable;
import support.key.ContainerIntTypeKey;
import utility.StringUtil;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpProductScanInfoAdapter extends BaseAdapter {

	private List<PickUpScanInfo> arrayList;

	private LayoutInflater inflater;

	private Context context;
	private String s = "";
	private Resources resources;
	private boolean isPickupContainer;

	public PickUpProductScanInfoAdapter(List<PickUpScanInfo> arrayList, Context context, boolean isPickupContainer) {
		super();
		this.arrayList = arrayList;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		resources = context.getResources();
		s = resources.getString(R.string.sku_has_params);
		this.isPickupContainer = isPickupContainer;

	}

	@Override
	public int getCount() {
		return arrayList == null ? 0 : arrayList.size();
	}

	@Override
	public PickUpScanInfo getItem(int location) {
		return arrayList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		PickUpScanInfo temp = getItem(position);
		CommonHolder itemViewHolder = null;
		if (convertView == null) {
			itemViewHolder = new CommonHolder();
			convertView = inflater.inflate(R.layout.pickup_location_product_scan_info_list_view, null);
			itemViewHolder.sn = (TextView) convertView.findViewById(R.id.sn);
			itemViewHolder.sku = (TextView) convertView.findViewById(R.id.sku);
			itemViewHolder.qty = (TextView) convertView.findViewById(R.id.qty);
			itemViewHolder.pcid = (TextView) convertView.findViewById(R.id.pcid);
			itemViewHolder.pname = (TextView) convertView.findViewById(R.id.pname);
			itemViewHolder.bill_id = (TextView) convertView.findViewById(R.id.bill_id);
			itemViewHolder.sku_label = (TextView) convertView.findViewById(R.id.sku_label);
			itemViewHolder.ContainerId = (TextView) convertView.findViewById(R.id.container);
			convertView.setTag(itemViewHolder);
		} else {
			itemViewHolder = (CommonHolder) convertView.getTag();
		}
		if (temp.getSn() != null && temp.getSn().length() > 0 && !temp.getSn().trim().toUpperCase().equals("NULL")) {
			itemViewHolder.sn.setText(temp.getSn());
		}
		itemViewHolder.bill_id.setText(temp.getId() + "");
		String s = resources.getString(R.string.sku_has_params);
		String skuHasPram = String.format(s, getBarTypeCodeName(temp.getCode_type()));
		itemViewHolder.sku_label.setText(skuHasPram);
		itemViewHolder.sku.setText(temp.getBarCode());
		itemViewHolder.qty.setText(temp.getQty() + "");
		itemViewHolder.pcid.setText(temp.getSku() + "");
		itemViewHolder.pname.setText(temp.getPname());
		if (isPickupContainer) {
			itemViewHolder.ContainerId.setText(ContainerIntTypeKey.getType(temp.getPickup_container_type()) + temp.getPickup_container_id());

		} else {
			itemViewHolder.ContainerId.setText(ContainerIntTypeKey.getType(temp.getFrom_container_type()) + temp.getFrom_container_id());

		}
		itemViewHolder.sn.setText(temp.getSn());
		if (temp.getIsSubmit() == 0) {
			itemViewHolder.qty.setTextColor(Color.RED);
		} else {
			itemViewHolder.qty.setTextColor(Color.GREEN);
		}

		return convertView;
	}

	public static String getBarTypeCodeName(String value) {
		int codeType = 1;
		String codeTypeName = "";
		if (value != null && value.trim().length() > 0) {
			codeType = StringUtil.convert2Inter(value.trim());
		}
		codeTypeName = getPcodeTypeKey().get(codeType);
		return codeTypeName;
	}

	public static Map<Integer, String> getPcodeTypeKey() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(0, "ID");
		map.put(1, "Main");
		map.put(2, "UPC");
		map.put(3, "Amazon");
		map.put(4, "Old");
		return map;
	}

}
