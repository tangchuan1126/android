package oso.ui.processing.specialproject.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.processing.specialproject.WMS_Special_LaborTasksActivity;
import oso.ui.processing.specialproject.adapter.SpecialProLaborAdp;
import oso.ui.processing.specialproject.bean.SpecialLaborTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * Special Project Labor Tasks List(未完成状态的任务 TYPE == Active)
 * @author xialimin
 *
 */
public class LaborActiveFragment extends Fragment implements View.OnClickListener {
	
	private Context mContext;

	private View mRootView;

	private Button mCompleteBtn;

	private ListView mActiveLv;

	private SpecialProLaborAdp mActiveAdp;

	private int mPosition, mTop;  // ListView位置状态保存
	
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return mRootView = inflater.inflate(R.layout.fragment_special_labor_layout, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		initView(mRootView);

		initListener();

	}

	private void initView(View view) {
		mContext = getActivity();
		
		mActiveLv = (ListView) view.findViewById(R.id.lv_labor_active_task);
		//mActiveAdp = new SpecialProLaborAdp(mContext, getActiveData());
		mActiveLv.setAdapter(mActiveAdp);

		TextView emptyTv = (TextView) view.findViewById(R.id.tvltEmpty);
		mActiveLv.setEmptyView(emptyTv);

		mCompleteBtn = (Button) view.findViewById(R.id.btnCompleteTask);
	}

	private void initListener() {
		mCompleteBtn.setOnClickListener(this);
		mActiveLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (getActiveData().get(position).task_status == SpecialTaskKey.TYPE_PENDING) {
					UIHelper.showToast(mContext, "Pending!");
					return;
				}

				if (getActiveData().get(position).task_status == SpecialTaskKey.TYPE_CLOSED) {
					UIHelper.showToast(mContext, "Closed!");
					return;
				}

				SpecialLaborTask.LaborTask task = (SpecialLaborTask.LaborTask) mActiveLv.getAdapter().getItem(position);
				task.status = !task.status;
				mActiveAdp.notifyDataSetChanged();

				setCompletedBtnVisibility();
			}
		});
	}

	/**
	 * 设置提交按钮是否可见
	 */
	private void setCompletedBtnVisibility() {
		mCompleteBtn.setVisibility(shouldShowBtn() ? View.VISIBLE : View.GONE);
	}

	private List<SpecialLaborTask.LaborTask> getActiveData() {
		List<SpecialLaborTask.LaborTask> datas = ((WMS_Special_LaborTasksActivity) getActivity()).getData();
		List<SpecialLaborTask.LaborTask> activeTasks = new ArrayList<SpecialLaborTask.LaborTask>();

		if(Utility.isNullForList(datas)) {
			return activeTasks;
		}

		for (SpecialLaborTask.LaborTask laborTask : datas) {
			if (laborTask.task_status != SpecialTaskKey.TYPE_CLOSED) {
				activeTasks.add(laborTask);
			}
		}
		return activeTasks;
	}

	/**
	 * 是否应该显示Complete按钮
	 * @return
	 */
	private boolean shouldShowBtn() {
		List<SpecialLaborTask.LaborTask> activeData = getActiveData();
		for (SpecialLaborTask.LaborTask task : activeData) {
			if(task.status) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 刷新数据
	 */
	public void setData() {
		saveCurrentPosition();
		mActiveAdp = new SpecialProLaborAdp(mContext, getActiveData());
		mActiveLv.setAdapter(mActiveAdp);
		restorePosition();
		mCompleteBtn.setVisibility(hasChecked(getActiveData()) ? View.VISIBLE : View.GONE);
	}

	/**
	 * 是否有选中的数据
	 * @param activeData
	 * @return
	 */
	private boolean hasChecked(List<SpecialLaborTask.LaborTask> activeData) {
		if (Utility.isNullForList(activeData)) {
			return false;
		}
		for (int i=0; i<activeData.size(); i++) {
			if (activeData.get(i).status) {
				return true;
			}
		}
		return false;
	}

	/**
	 * ListView位置状态保存
	 */
	private void saveCurrentPosition() {
		if (mActiveLv != null) {
			int position = mActiveLv.getFirstVisiblePosition();
			View v = mActiveLv.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();
			//保存position和top位置
			mPosition = position;
			mTop = top;
		}
	}

	/**
	 * ListView位置恢复
	 */
	private void restorePosition() {
		if (getActiveData() != null && mActiveLv != null) {
			int position = mPosition;//取出保存的数据
			int top = mTop;//取出保存的数据
			mActiveLv.setSelectionFromTop(position, top);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnCompleteTask:
				SpecialLaborTask task = ((WMS_Special_LaborTasksActivity) getActivity()).getTaskBean();
				if(task!=null && task.verifyRequestData()) {
					showSubmitDlg();
				} else {
					UIHelper.showToast(mContext, "Submit data is empty!");
				}

				break;

			default:
				break;
		}
	}

	/**
	 * 提交二次确认对话框
	 */
	private void showSubmitDlg() {
		new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_notice)).setMessage("Are you sure to submit tasks?")
				.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				submitTask();
			}
		}).show();
	}

	/**
	 * 完成任务 提交任务
	 */
	private void submitTask() {
		final SpecialLaborTask mTasks = ((WMS_Special_LaborTasksActivity) getActivity()).getTaskBean();
		RequestParams params = new RequestParams();
		params.put("key", "closeSubTask");
		params.put("completeTaskIds", mTasks.getCompleteTaskIds().toString());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				mTasks.updateCompleteTask();

				saveCurrentPosition();

				mActiveAdp = new SpecialProLaborAdp(mContext, getActiveData());

				mActiveLv.setAdapter(mActiveAdp);

				restorePosition();

				mCompleteBtn.setVisibility(Utility.isNullForList(getActiveData()) ? View.GONE : View.VISIBLE);

				UIHelper.showToast(mContext, mContext.getString(R.string.sync_success));

			}
		}.doPost(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}
}
