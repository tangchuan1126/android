package oso.ui.processing.picking.bean;

import java.util.List;

public class ReturnBean<T> {

	private String ret ="" ;
	private String err ="";
	private List<T> arrayList ;
	private int processingCount = 0 ;
	
	private double totalQty ; //inventroy location need ;
	public ReturnBean() {
		super();
	}
	public ReturnBean(String ret, String err, List<T> arrayList) {
		super();
		this.ret = ret;
		this.err = err;
		this.arrayList = arrayList;
	}
	public String getRet() {
		return ret;
	}
	public void setRet(String ret) {
		this.ret = ret;
	}
	public String getErr() {
		return err;
	}
	public void setErr(String err) {
		this.err = err;
	}
	public List<T> getArrayList() {
		return arrayList;
	}
	public void setArrayList(List<T> arrayList) {
		this.arrayList = arrayList;
	}
	public int getProcessingCount() {
		return processingCount;
	}
	public void setProcessingCount(int processingCount) {
		this.processingCount = processingCount;
	}
	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	} 
	
	
}
