package oso.ui.processing.specialproject.bean;

import android.text.TextUtils;

import org.json.JSONArray;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oso.ui.processing.specialproject.key.SpecialTaskKey;
import utility.StringUtil;

/**
 * LaborTaskBean
 */
public class SpecialLaborTask implements Serializable {
	
	private static final long serialVersionUID = -6496490701697719507L;

	public List<LaborTask> notices = new ArrayList<LaborTask>();

	/**
	 * Labor Task提交任务数组
	 * @return
	 */
	public JSONArray getCompleteTaskIds() {
		JSONArray arr = new JSONArray();
		for (int i = 0; i < notices.size(); i++) {
			LaborTask task = notices.get(i);
			if((task.task_status == SpecialTaskKey.TYPE_OPEN || task.task_status == SpecialTaskKey.TYPE_INPROCESS
			 	|| task.task_status == SpecialTaskKey.TYPE_ASSIGNED || task.task_status == SpecialTaskKey.TYPE_ACCEPTED)
					&& task.status ) {
				arr.put(task.task_id);
			}
		}
		return arr;
	}

	/**
	 * 删除已完成的Labor任务
	 */
	public void updateCompleteTask() {
		for (int i = 0; i < notices.size(); i++) {
			LaborTask task = notices.get(i);
			if((task.task_status != SpecialTaskKey.TYPE_CLOSED && task.task_status != SpecialTaskKey.TYPE_PENDING) && task.status ) {

				task.task_status = SpecialTaskKey.TYPE_CLOSED;
			}
		}
		/*// 修改的个数
		int count = 0;
		for (int i = 0; i < notices.size(); i++) {
			LaborTask task = notices.get(i);
			if((task.task_status == SpecialTaskKey.TYPE_OPEN || task.task_status == SpecialTaskKey.TYPE_INPROCESS
					|| task.task_status == SpecialTaskKey.TYPE_ASSIGNED ) && task.status ) {
				count ++;
			}
		}

		for(int j = 0; j < count; j ++) {
			for (int i = 0; i < notices.size(); i++) {
				LaborTask task = notices.get(i);
				if((task.task_status == SpecialTaskKey.TYPE_OPEN || task.task_status == SpecialTaskKey.TYPE_INPROCESS
						|| task.task_status == SpecialTaskKey.TYPE_ASSIGNED) && task.status ) {
					// 完成状态 设置为 TYPE_CLOSED
					task.task_status = SpecialTaskKey.TYPE_CLOSED;

					// 删除
					//NOTICES.remove(i);
					break;
				}
			}
		}*/

	}

	/**
	 * 设置完成状态
	 */
	public void setCompleteStatus() {
		for (int i = 0; i < notices.size(); i++) {
			LaborTask task = notices.get(i);
			if((task.task_status == SpecialTaskKey.TYPE_OPEN || task.task_status == SpecialTaskKey.TYPE_INPROCESS)
					&& task.status ) {
				// 完成状态 设置为 TYPE_CLOSED
				task.task_status = SpecialTaskKey.TYPE_CLOSED;
			}
		}
	}

	/**
	 * 验证提交数据
	 * @return
	 */
	public boolean verifyRequestData() {
		/*boolean isValid = false;
		for (int i = 0; i < notices.size(); i++) {
			LaborTask task = notices.get(i);
			*//*if((task.task_status == SpecialTaskKey.TYPE_OPEN || task.task_status == SpecialTaskKey.TYPE_INPROCESS)
					&& task.status ) {
				isValid = true;
			}*//*
			if(task.status) {
				return true;
			}
		}
		return isValid;*/
		return !StringUtil.isNullForJSONArray(getCompleteTaskIds());
	}

	public class LaborTask implements Serializable {
		public int task_status;
		public String task_id;
		public String task_description;
		public String notice_time;
		public String start_time;
		public String end_time;


		//--------------------------
		public boolean status;  // true为选中  false为没选中

		/**
		 * pending ：     添加任务后需要Manager确认，期间应该为pending状态
		 * inprocess: 任务分配出去，正在执行
		 * closed:    任务完成
		 */
		//public int type;
	}

	public static String getFormatterTime(String timeStr) {
		int length = "2015-07-21 14:00:00".length();
		if(TextUtils.isEmpty(timeStr)) {
			return "";
		}
		if(timeStr.length() == length) {
			return timeStr.substring(0, length - 3);
		}
		return timeStr;
	}

	/**
	 * 2015-07-29 14:00:00  转为 07/29/15
	 * @param dataStr
	 * @return
	 */
	public static String getSpecialDate(String dataStr) {
		if (TextUtils.isEmpty(dataStr)) {
			return "";
		}
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dataStr);
			String time = new SimpleDateFormat("MM/dd/yy").format(date);
			return time;
		} catch (ParseException e) {
			e.printStackTrace();
			return dataStr;
		}
	}
	
}
