package oso.ui.processing.picking.runnable;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import oso.ui.processing.picking.bean.ReturnBean;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.parse.HttpResultCode;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import support.dbhelper.ConfigDao;
import support.dbhelper.Goable;
import support.network.HttpPostMethod;
import support.network.HttpTool;
import utility.FileUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.content.Context;
import android.os.Handler;

/**
 * 1.每次提交的时候都会下载 最新的记录
 * @author zhangrui
 *
 */
public class SubmitLocationProductRunable implements Runnable{
	
	private Context context ;
	
	private Long location_id ;
 	
	private Long pickup_id ;
	
	
	private String location ;
	
	private Handler handler ;
	
 	
	private PickUpScanInfoDao pickUpScanInfoDao ;
	
 	private ConfigDao configDao ;
 	
	private String fileName = "submitLocationProduct.xml";
	
 	
 	 
	public SubmitLocationProductRunable(Context context,long location_id , long pickup_id ,String location ,Handler handler  ) {
		super();
		this.context = context;
		this.location_id = location_id ;
		this.pickup_id = pickup_id ;
		this.location = location ;
 		this.handler = handler;
 		this.pickUpScanInfoDao = new PickUpScanInfoDao(context);
 		this.configDao = new ConfigDao(context);
   		Goable.initGoable(context);
		 
	}



	@Override
	public void run() {
	 
  			 int flag = sendXml();
 			 
			 handler.obtainMessage(flag).sendToTarget();
		}
		
	 
	private int sendXml( ){
		int flag  = HttpResultCode.MSG_FAILURE;
 	 
  		List<PickUpScanInfo> arrayList = pickUpScanInfoDao.queryPickUpScanInfo(pickup_id, location_id,0); 
		if(arrayList != null && arrayList.size() > 0){
			StringBuffer detail = new StringBuffer("<Out>"+pickup_id+"</Out>");
			detail.append("<Location>").append(location).append("</Location>");
			detail.append("<LocationId>").append(location_id).append("</LocationId>");
			detail.append("<FOType>").append("FIFO").append("</FOType>");
			detail.append("<RN>").append("1").append("</RN>");
			detail.append("<PickUpProduct>");
			for(int index = 0 , count = arrayList.size() ; index < count ; index++ ){
				detail.append(handleItem(arrayList.get(index), index == (count-1),1));
			}
			detail.append("</PickUpProduct>"); 
	
			String postXml = HttpTool.noDetailPostXml(detail.toString(), HttpPostMethod.PickUpPoroduct, null);
 			try{
	 			InputStream inputStream = HttpTool.getStreamAndPostXML(HttpUrlPath.PickUp, HttpTool.POST, postXml);
	 			FileUtil.saveFileNoSdCard(fileName, inputStream, context);
	 			inputStream.close();
	 			flag = handXml(pickup_id);
	 			 
	 		}catch (Exception e) {
	 			e.printStackTrace();
	 		} 
		}
 		return flag;
	}
	
	/**
	 * @param scanInfo
	 * pcid,qty,null,sn,pickup_from_type,pickup_from_type_id , pick_container_id , pickup_contiainer_type
	 * pickup_container_type_id , pickup_container_id,接地容器的ContainerID
	 * @param isLast
	 * @return
	 */
	private String handleItem(PickUpScanInfo scanInfo , boolean isLast , int pickupType){
		StringBuffer sb = new StringBuffer();
 		sb.append(scanInfo.getSku())
		.append(",").append(scanInfo.getQty())
		.append(",").append("NULL" )
		.append(",").append(StringUtil.isNullOfStr(scanInfo.getSn()) ? "NULL" : scanInfo.getSn()) 
		.append(",").append(scanInfo.getFrom_container_type())
		.append(",").append(scanInfo.getFrom_container_type_id());
		if(scanInfo.getFrom_container_type_id() != 0l){
			sb.append(",").append(scanInfo.getFrom_container_id()) ;
		}else{
			sb.append(",0");
		}
		
		sb.append(",").append(scanInfo.getPickup_container_type()) 
		.append(",").append(scanInfo.getPickup_container_type_id()) 
		.append(",").append(scanInfo.getPickup_container_id()) 
		.append(",").append(getFloorContainerId(scanInfo)) ;

  		
 		if(!isLast){
			sb.append("|");
		}
		return sb.toString();
	}
	//获取接地容器的ContainerID
	private long getFloorContainerId(PickUpScanInfo scanInfo){
		//container 是直接放在位置上的 floorContainerId 就是pickup_container_id
		//container 是放在其他的容器中的floorContainerId 就是From_container_id 
		return scanInfo.getFrom_container_id() != 0l ?scanInfo.getFrom_container_id() : scanInfo.getPickup_container_id() ;
		
	}
	/**
	 * @param location
	 * @param pickup_id
	 * @param location_id
	 */
	private int  handXml(long pickup_id){
		int flag = HttpResultCode.MSG_FAILURE ;
		try{
			FileInputStream in  =	new FileInputStream(new File(context.getFilesDir()+File.separator+fileName));
			ReturnBean<PickUpLoactionProduct> pickupBean = PickUpBaseDataParse.handNeedPickUpProduct(in, pickup_id);
			if(StringUtil.convert2Inter(pickupBean.getRet()) == 1){
				//说明提交数据成功,那么删除这个位置的数据(包含扫描的数据),因为已经返回了这个位置的数据
  				configDao.afterSubmitPickupLocationScanInfo(location_id, pickup_id, pickupBean);
 				flag = HttpResultCode.MSG_SUCCESS ;
			}
			
		}catch(Exception e){
		}
		return flag ;
	
	}
}
