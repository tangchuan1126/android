package oso.ui.processing.picking.db;

import support.dbhelper.DatabaseHelper;
import android.content.Context;

public class PickUpProductCodeDao extends ProductCodeDao {
	
	private static final String tableName = DatabaseHelper.tbl_pickup_product_code;
	
	public PickUpProductCodeDao(Context context) {
		super(context,tableName);
 	}

}
