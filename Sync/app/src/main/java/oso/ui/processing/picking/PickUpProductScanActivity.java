package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.adapter.PickUpProductScanInfoAdapter;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import declare.com.vvme.R;

public class PickUpProductScanActivity extends Fragment{

	public static int FROM_PICKUPCONTAINER = 1 ;	//直接拣货Container
	public static int FROM_PICKUPPRODUCT = 2 ;		//在某个具体的位置拣货的
	public static int FROM_CONTAINERTYPEPICKUPPRODUCT = 3 ;		//在某种具体的类型的Container上面拣货
	public static int FROM_CONTAINERONTYPEPCIKUPPRODUCT = 4 ; 	//在某种大类型的Container 上面拣货
	
	
	
 	private Context context ;
 	private long pickup_id ;
 	private long location_id ;
 	private PickUpBean pickUpBean ;
 	private Resources resources ;
 	private PickUpScanInfoDao pickUpScanInfoDao ;
 	private ListView listView ;
 	private int from ;				//表示的是来自页面的数据
 	
 	private View view ;
 	private Bundle bundle ;
 	
 	private Handler showListHandler = new Handler(){
  		
  	public void handleMessage(android.os.Message msg) {
  			List<PickUpScanInfo> arrayList = (List<PickUpScanInfo>)msg.obj;
  			showDatas(arrayList);
  		};
  	};
 	
  	public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
  		view = inflater.inflate(R.layout.pickup_common_layout, container, false);
  		return view ;
  	};
  	@Override
  	public void onActivityCreated(Bundle savedInstanceState) {
   		super.onActivityCreated(savedInstanceState);
   		initField();
  		initDatas();
  	}
	 
	private void initField(){
		context = getActivity();
		pickUpScanInfoDao = new PickUpScanInfoDao(context);
		resources = context.getResources();
		bundle = getArguments() ;
		pickup_id = bundle.getLong("pickup_id", 0l);
		location_id = bundle.getLong("location_id", 0l);
		pickUpBean = (PickUpBean)bundle.getSerializable("pickUpBean");
		from = bundle.getInt("from", 0);
		listView = (ListView)view.findViewById(R.id.pickup_common_view_list);
		 
 	}
	private void initDatas(){
		 new Thread(new Runnable() {
			@Override
			public void run() {
				List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>() ;
				if(from == FROM_PICKUPCONTAINER){
					 arrayList = pickUpScanInfoDao.getPickUpScanInfo(pickUpBean.getPickUpContainerType(), pickUpBean.getPickUpContainerTypeId(), pickup_id, location_id);
				}
				//直接拣Container_product 
				if(from == FROM_PICKUPPRODUCT){
					 arrayList = pickUpScanInfoDao.getContainerPickUpProductScanInfo(pickup_id, pickUpBean.getFromContainerId(), location_id);
				}
				if(from == FROM_CONTAINERTYPEPICKUPPRODUCT){
					 arrayList = pickUpScanInfoDao.getContainerTypePickUpProductScanInfo(pickup_id, pickUpBean.getFromContainerType(),pickUpBean.getFromContainerTypeId(), location_id);
				}
				if(from == FROM_CONTAINERONTYPEPCIKUPPRODUCT){
					 arrayList = pickUpScanInfoDao.getContainerTypePickUpProductScanInfo(pickup_id, pickUpBean.getFromContainerType(),pickUpBean.getFromContainerTypeId(), location_id);
				}
				
				showListHandler.obtainMessage(0, arrayList).sendToTarget();

			}
		}).start();
	}
	private void showDatas(List<PickUpScanInfo> arrayList){
		listView.setDivider(null);
		listView.setAdapter(new PickUpProductScanInfoAdapter(arrayList, context,FROM_PICKUPCONTAINER == from ?  true : false));
		
		LinearLayout.LayoutParams  lp5 =new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT); 
		listView.setLayoutParams(lp5);
	}
	
	
 
 
	 
}
