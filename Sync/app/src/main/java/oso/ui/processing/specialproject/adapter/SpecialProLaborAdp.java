package oso.ui.processing.specialproject.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import declare.com.vvme.R;
import oso.ui.processing.specialproject.bean.SpecialLaborTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;

public class SpecialProLaborAdp extends BaseAdapter {

	private Context mContext;
	
	private List<SpecialLaborTask.LaborTask> mData;
	
	public SpecialProLaborAdp(Context ctx, List<SpecialLaborTask.LaborTask> tasks) {
		mContext = ctx;
		mData = tasks;
	}

	@Override
	public int getCount() {
		if(mData==null) return 0;
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView  == null) {
			holder = new ViewHolder();
			convertView = initView(convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillData(position, holder);
		
		return convertView;
	}
	
	private View initView(View convertView, ViewHolder holder) {
		
		convertView = View.inflate(mContext, R.layout.item_specialtask_labortask, null);
		holder.ivStatus = (ImageView) convertView.findViewById(R.id.labortask_status);
		holder.tvType = (TextView) convertView.findViewById(R.id.labortask_type);
		holder.tvTaskId = (TextView) convertView.findViewById(R.id.labortask_id);
		holder.tvTaskDes = (TextView) convertView.findViewById(R.id.labortask_des);
		holder.tvStartTime = (TextView) convertView.findViewById(R.id.tv_labortask_startdate);
		holder.tvEndTime = (TextView) convertView.findViewById(R.id.tv_labortask_enddate);
		holder.layoutDate = (LinearLayout) convertView.findViewById(R.id.sp_labor_time);
		return convertView;
	}
	
	/**
	 * 填充数据
	 * @param position
	 * @param holder
	 */
	private void fillData(int position, ViewHolder holder) {
		SpecialLaborTask.LaborTask task = mData.get(position);

		if(task == null) return;

		boolean status = task.status;
		
		switch (mData.get(position).task_status) {
		case SpecialTaskKey.TYPE_PENDING:
			holder.ivStatus.setBackgroundResource(R.drawable.ic_cct_prompt);
			holder.tvType.setText("Pending");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_orange)); 
			break;

		case SpecialTaskKey.TYPE_OPEN:
			holder.ivStatus.setBackgroundResource(status ? R.drawable.ic_cct_scanned : R.drawable.ic_cct_unscanned);
			holder.tvType.setText("Open");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.green));
			break;
			
		case SpecialTaskKey.TYPE_INPROCESS:
		holder.ivStatus.setBackgroundResource(status ? R.drawable.ic_cct_scanned : R.drawable.ic_cct_unscanned);
				holder.tvType.setTextColor(mContext.getResources().getColor(R.color.green));

				holder.tvType.setText("In-Processing");
				holder.tvType.setTextColor(mContext.getResources().getColor(R.color.green));
				break;

		case SpecialTaskKey.TYPE_CLOSED:
				holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_CLOSED));
				holder.tvType.setText("Closed");
				holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_red));
				break;

        case SpecialTaskKey.TYPE_ASSIGNED:
            holder.ivStatus.setBackgroundResource(status ? R.drawable.ic_cct_scanned : R.drawable.ic_cct_unscanned);
            holder.tvType.setText("Assigned");
            holder.tvType.setTextColor(mContext.getResources().getColor(R.color.green));
            break;

		case SpecialTaskKey.TYPE_ACCEPTED:
			holder.ivStatus.setBackgroundResource(status ? R.drawable.ic_cct_scanned : R.drawable.ic_cct_unscanned);
			holder.tvType.setText("Accepted");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.green));
			break;
		}
		
		/*if(mData.get(position).task_status == SpecialTaskKey.TYPE_PENDING) {
			holder.ivStatus.setBackgroundResource(R.drawable.ic_cct_prompt);
			holder.tvType.setText("Pending");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_orange));
		}*/

		holder.tvTaskId.setText(task.task_id);
		holder.tvTaskDes.setText(task.task_description);


		holder.layoutDate.setVisibility((TextUtils.isEmpty(task.start_time) || TextUtils.isEmpty(task.end_time)) ?
				View.GONE : View.VISIBLE);
		holder.tvStartTime.setText(SpecialLaborTask.getSpecialDate(task.start_time));
		holder.tvEndTime.setText(SpecialLaborTask.getSpecialDate(task.end_time));

	}

	/**
	 *  Task No
	 *  Subject
	 *  Description
	 *  Start Time
	 *  End Time
	 *  Type [New, Closed]
	 */
	private static class ViewHolder {
		TextView tvLabors, tvHideLabors;
		TextView  tvTaskDes, tvType, tvTaskId, tvStartTime, tvEndTime;
		ImageView ivStatus;
		LinearLayout layoutDate;//layoutStartDate, layoutEndDate;
	}
}
