package oso.ui.processing.picking.adapter;

import java.util.List;

import oso.ui.processing.picking.bean.PickUpLocation;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpLocationAdapter extends BaseAdapter{

	private List<PickUpLocation> arrayList ;
	
	private LayoutInflater inflater;
	
	private Context context;
	
	private static PickUpScanInfoDao pickUpScanInfoDao ;
	
	private int selectedIndex = -1 ;
	
	public PickUpLocationAdapter(List<PickUpLocation>  arrayList,  Context context) {
		super();
		this.arrayList = arrayList;
 		this.context = context;
 		this.inflater = LayoutInflater.from(context);
 		this.pickUpScanInfoDao = new PickUpScanInfoDao(context);
	}


	@Override
	public int getCount() {
 		return arrayList == null ? 0 : arrayList.size();
	}


	@Override
	public PickUpLocation getItem(int location) {
 		return arrayList.get(location);
	}


	@Override
	public long getItemId(int location) {
 		return getItem(location).getId();
	}
	
	/** 
	 *  
	 * 
	 */

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
 		PickUpLocation temp = getItem(position);
		PickUpHolder viewHolder = null ;
 		if(convertView==null){
			
			convertView=inflater.inflate(R.layout.pickup_location_list_view,null);
			viewHolder=new PickUpHolder();
			viewHolder.sort = (TextView)convertView.findViewById(R.id.pickup_location_sort);
			viewHolder.Location = (TextView)convertView.findViewById(R.id.pickup_location);
			viewHolder.Location_id = (TextView)convertView.findViewById(R.id.pickup_location_id);
		
			viewHolder.linearLayout=(LinearLayout)convertView.findViewById(R.id.pickup_info_s);
		convertView.setTag(viewHolder);
		}else{
			viewHolder = (PickUpHolder) convertView.getTag();
		}

		viewHolder.sort.setText(temp.getSort()+". ");
		viewHolder.Location_id.setText(temp.getLocation_id()+"");
		viewHolder.Location.setText(temp.getLocation());
		
		setDirectPickUpContainerTypeBody(temp, viewHolder.linearLayout, inflater);
 		return convertView;
	}


	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	class PickUpHolder{
		public TextView sort;
		public TextView Location;
		public TextView Location_id;
		public LinearLayout linearLayout ;
 		 
	}
	public     void setDirectPickUpContainerTypeBody(PickUpLocation pickUpBean,LinearLayout appendView, LayoutInflater inflater ){
		List<ProductCompareSimple> datas = pickUpScanInfoDao.getLocationPickupInfo(pickUpBean.getPickup_id(), pickUpBean.getLocation_id());
		appendView.removeAllViews();
		if(datas != null && datas.size() > 0 ){
			for(ProductCompareSimple simpe : datas){
				View   headerView = inflater.inflate(R.layout.get_pickup_container_item, null);
 				TextView pName = (TextView)headerView.findViewById(R.id.pickup_container_type);
//				TextView totalQty = (TextView)headerView.findViewById(R.id.totalqty);
//				TextView pickUpQty = (TextView)headerView.findViewById(R.id.qty);
				
 				TextView qty_and_totalqty = (TextView)headerView.findViewById(R.id.qty_and_totalqty);
				 
				pName.setText(simpe.getpName());
//				totalQty.setText(simpe.getTotalQty()+"");
//				pickUpQty.setText(simpe.getPickupQty()+"");
				qty_and_totalqty.setText(simpe.getPickupQty()+" / "+simpe.getTotalQty());
				appendView.addView(headerView);
			}
		}else{
			View   headerView = inflater.inflate(R.layout.inventory_location_list_view_item, null);
			//类型的PickupContainerType 
			TextView productName = (TextView)headerView.findViewById(R.id.product_name);
			productName.setText("暂无数据");
			productName.setGravity(Gravity.CENTER);
			TextView locationProductQty = (TextView)headerView.findViewById(R.id.location_product_qty);
			locationProductQty.setVisibility(View.GONE);
			appendView.addView(headerView);
		}
		 
 }
	public void setDatas(List<PickUpLocation> arrayList){
		this.arrayList = arrayList ;
		notifyDataSetChanged();
	}
}
