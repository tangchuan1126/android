package oso.ui.processing.specialproject.adapter;

import oso.ui.processing.specialproject.bean.ProjectBean;
import oso.ui.processing.specialproject.bean.ProjectBean.Project;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;


public class SpecialProjectAdp extends BaseAdapter {

	private Context mContext;
	
	private ProjectBean mData;
	
	public SpecialProjectAdp(Context ctx, ProjectBean tasks) {
		mContext = ctx;
		mData = tasks;
	}

	@Override
	public int getCount() {
		if(mData==null) return 0;
		if(mData.projectList == null) return 0;
		return mData.projectList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView  == null) {
			holder = new ViewHolder();
			convertView = initView(convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillData(position, holder);
		
		return convertView;
	}
	
	private View initView(View convertView, ViewHolder holder) {
		
		convertView = View.inflate(mContext, R.layout.item_specialtask_project, null);
		holder.tvProjectNo = (TextView) convertView.findViewById(R.id.project_no);
		holder.tvWHName = (TextView) convertView.findViewById(R.id.project_warehouse);
		holder.tvSubject = (TextView) convertView.findViewById(R.id.project_subject);
		
		holder.tvManagerName = (TextView) convertView.findViewById(R.id.fromManager);
		holder.tvStartDate = (TextView) convertView.findViewById(R.id.project_startdate);
		holder.tvEndDate = (TextView) convertView.findViewById(R.id.project_enddate);
		
		holder.tvLRType = (TextView) convertView.findViewById(R.id.tvLRType);
		holder.tvLRNo = (TextView) convertView.findViewById(R.id.project_lrno);
		holder.tvDes = (TextView) convertView.findViewById(R.id.project_des);
		return convertView;
	}
	
	/**
	 * 填充数据
	 * @param position
	 * @param holder
	 */
	private void fillData(int position, ViewHolder holder) {
		Project project = mData.projectList.get(position);
		
		if(!TextUtils.isEmpty(project.schedule_id)) {
			holder.tvProjectNo.setText(project.schedule_id);
		}
		
		if(!TextUtils.isEmpty(project.warehouse)) {
			holder.tvWHName.setText(project.warehouse);
		}
		
		if(!TextUtils.isEmpty(project.subject)) {
			holder.tvSubject.setText(project.subject);
		}
		
		if(!TextUtils.isEmpty(project.fromname)) {
			holder.tvManagerName.setText(project.fromname);
		}
		
		if(!TextUtils.isEmpty(project.start_time)) {
			holder.tvStartDate.setText(project.start_time);
		}
		
		if(!TextUtils.isEmpty(project.end_time)) {
			holder.tvEndDate.setText(project.end_time);
		}
		
		if(project.LR_TYPE != 0) {
			holder.tvLRType.setText(SpecialTaskKey.lrTypeMap.get(project.LR_TYPE));
		}
		
		if(!TextUtils.isEmpty(project.LR_NO)) {
			holder.tvLRNo.setText(project.LR_NO);
		}
		
		if(!TextUtils.isEmpty(project.description)) {
			holder.tvDes.setText(project.description);
		}
	}

	/**
	 *  Task No
	 *  Subject
	 *  Description
	 *  Start Time
	 *  End Time
	 *  Type [New, Closed]
	 */
	private static class ViewHolder {
		TextView tvProjectNo, tvSubject, tvLRType, tvLRNo;
		TextView tvStartDate, tvEndDate, tvDes;
		TextView tvManagerName, tvWHName;
	}
}
