package oso.ui.processing.specialproject.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @description: Special Project 主任务Bean
 * ---包含Sub Tasks
 * ---包含Task Info
 *
 * @author xialimin
 * @time : 2015-6-18 下午5:35:24
 */
public class MainTaskBean implements Serializable {
	private static final long serialVersionUID = -2171751174261960207L;
	
	
	public ProjectDetail project_detail;
	
	public class ProjectDetail implements Serializable {
		private static final long serialVersionUID = 4645971655850210238L;
		public String end_time;
		public String schedule_id;
		public String start_time;
		public String subject;
		
		public String lr_no; // LR No
		public int lr_type;  // 8种LR No类型
		public String warehouse;
		
		public List<MainTask> main_tasks = new ArrayList<MainTask>();
		
	}
	
	public class MainTask implements Serializable  {
		private static final long serialVersionUID = 1840605653952925460L;
		public String start_time;
		public String end_time;
		public List<ChildTask> child_tasks = new ArrayList<ChildTask>();
		
		//----------ext-----------
		public String assignLabors;
		public String assignLaborsAdids;
	}
	
	public class ChildTask implements Serializable {
		private static final long serialVersionUID = -4132237719685729723L;
		public String schedule_id;
		public String description;
		public int task_status;
		public int closedcount;
		public int totalcount;
	}
	
	/*public String mainTaskName;
	public long mainTaskId;
	public String mainTaskDes;
	
	*//**
	 * pending ：     添加任务后需要Manager确认，期间应该为pending状态
	 * inprocess: 任务分配出去，正在执行
	 * closed:    任务完成
	 *//*
	public int type; 
	
	public List<SubTaskBean> subTask = new ArrayList<SubTaskBean>();

	*//**
	 * 获得已经关闭的子任务的个数
	 * @return closedCnt
	 *//*
	public int getSubCloseCnt() {
		int cnt = 0;
		for(int i=0; i<subTask.size(); i++) {
			if(SpecialTaskKey.TYPE_CLOSED == subTask.get(i).type) {
				cnt ++;
			}
		}
		return cnt;
	}*/
}
