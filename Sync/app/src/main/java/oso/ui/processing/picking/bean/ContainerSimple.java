package oso.ui.processing.picking.bean;

/**
 * 记录了每一种Container需要的个数,装的商品,商品的个数
 * @author zhangrui
 *
 */
public class ContainerSimple {

	private int totalQty; 						//这种类型的Container需要的数量
	private long pcid ;							//这种类型的Container是装的PCID
	private String pName ;						//这种类型的Container是装的商品
	private double productQty;					//这种类型的Container是装的商品数量
	private Integer pickQty ;					//用于拣货的时候捡了多少个的计算
	private Integer containerType ;				//BLP,ILP,CLP
	private Long containerTypeId ;				//类型的ID
	
	private Long containerId ;					//container 是否有ID
	
	
	public void addTotalQty(int qty){
		this.totalQty += qty ;
	}
	 
	public int getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(int totalQty) {
		this.totalQty = totalQty;
	}
	public void setPickQty(Integer pickQty) {
		this.pickQty = pickQty;
	}
	
	public Integer getPickQty() {
		return pickQty;
	}

	public long getPcid() {
		return pcid;
	}
	public void setPcid(long pcid) {
		this.pcid = pcid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public double getProductQty() {
		return productQty;
	}
	public void setProductQty(double productQty) {
		this.productQty = productQty;
	}
 
	public Integer getContainerType() {
		return containerType;
	}
	public void setContainerType(Integer containerType) {
		this.containerType = containerType;
	}
	public Long getContainerTypeId() {
		return containerTypeId;
	}
	public void setContainerTypeId(Long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}
	
	public Long getContainerId() {
		return containerId;
	}

	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}

	@Override
 	public boolean equals(Object o) {
		ContainerSimple other = (ContainerSimple)o;
		return other.getContainerType().equals(this.getContainerType()) 
				&& other.getContainerTypeId().equals(this.getContainerTypeId());
 	}
	
}
