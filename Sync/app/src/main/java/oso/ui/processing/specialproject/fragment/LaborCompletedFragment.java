package oso.ui.processing.specialproject.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.processing.specialproject.WMS_Special_LaborTasksActivity;
import oso.ui.processing.specialproject.adapter.SpecialProLaborAdp;
import oso.ui.processing.specialproject.bean.SpecialLaborTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import utility.Utility;

/**
 * Special Project Labor Tasks List(未完成状态的任务 TYPE == Completed)
 * @author xialimin
 *
 */
public class LaborCompletedFragment extends Fragment {

	private Context mContext;

	private View mRootView;

	private ListView mCompletedLv;

	private SpecialProLaborAdp mCompletedAdp;

	private int mPosition, mTop;  // ListView位置状态保存

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return mRootView = inflater.inflate(R.layout.fragment_special_labor_layout, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView(mRootView);
	}

	private void initView(View view) {
		mContext = getActivity();

		mCompletedLv = (ListView) view.findViewById(R.id.lv_labor_active_task);

		TextView emptyTv = (TextView) view.findViewById(R.id.tvltEmpty);
		mCompletedLv.setEmptyView(emptyTv);
	}

	private List<SpecialLaborTask.LaborTask> getCompletedData() {
		List<SpecialLaborTask.LaborTask> datas = ((WMS_Special_LaborTasksActivity) getActivity()).getData();
		List<SpecialLaborTask.LaborTask> completedTasks = new ArrayList<SpecialLaborTask.LaborTask>();

		if(Utility.isNullForList(datas)) {
			return completedTasks;
		}

		for(SpecialLaborTask.LaborTask laborTask : datas) {
			if(laborTask.task_status == SpecialTaskKey.TYPE_CLOSED) {
				completedTasks.add(laborTask);
			}
		}
		return completedTasks;
	}

	/**
	 * 刷新数据
	 */
	public void setData() {
		saveCurrentPosition();
		mCompletedAdp = new SpecialProLaborAdp(mContext, getCompletedData());
		mCompletedLv.setAdapter(mCompletedAdp);
		restorePosition();
	}

	/**
	 * ListView位置状态保存
	 */
	private void saveCurrentPosition() {
		if (mCompletedLv != null) {
			int position = mCompletedLv.getFirstVisiblePosition();
			View v = mCompletedLv.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();
			//保存position和top位置
			mPosition = position;
			mTop = top;
		}
	}

	/**
	 * ListView位置恢复
	 */
	private void restorePosition() {
		if (getCompletedData() != null && mCompletedLv != null) {
			int position = mPosition;//取出保存的数据
			int top = mTop;//取出保存的数据
			mCompletedLv.setSelectionFromTop(position, top);
		}
	}
}
