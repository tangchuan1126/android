package oso.ui.processing.picking.runnable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpLocation;
import oso.ui.processing.picking.bean.PickUpSerialProduct;
import oso.ui.processing.picking.bean.Product;
import oso.ui.processing.picking.bean.ProductCode;
import oso.ui.processing.picking.bean.ReturnBean;
import oso.ui.processing.picking.parse.HttpResultCode;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import support.dbhelper.ConfigDao;
import support.dbhelper.Goable;
import support.network.HttpTool;
import utility.FileUtil;
import utility.HttpUrlPath;
import android.content.Context;
import android.os.Handler;

/**
 * @author Administrator
 *
 */
public class LoadPickUpBaseDataRunnable implements Runnable {
	private Context context ;
 	private Handler handler ;
	private String zipFileName = "outPickUp.zip" ;
  	private ConfigDao configDao ;
	private long pickup_id ;
	
	public LoadPickUpBaseDataRunnable(Context context, long pickUpId, Handler handler ) {
		super();
		this.context = context;
 		this.handler = handler;
  		this.pickup_id = pickUpId ; 
 		configDao = new ConfigDao(context);
		Goable.initGoable(context);
	}



	@Override
	public void run() {
		int flag = HttpResultCode.MSG_FAILURE ;
 		String postXml = HttpTool.getPostXmlString("<Out>"+pickup_id+"</Out>", "", null);
 		try{
 			InputStream inputStream = HttpTool.getStreamAndPostXML(HttpUrlPath.PickUpBCSOutBill, HttpTool.POST, postXml);
 			FileUtil.saveFileNoSdCard(zipFileName, inputStream, context);
 			handZipFile();
 			flag = HttpResultCode.MSG_SUCCESS ;
 		}catch (Exception e) {
 			e.printStackTrace();
 		}
 		handler.obtainMessage(flag).sendToTarget();
	}
	private void handZipFile() throws Exception {
 		File file = new File(context.getFilesDir()+File.separator+zipFileName);
	 
		ZipFile zipFile = new ZipFile(file);
		ZipEntry entry = null; 
		InputStream stream = context.openFileInput(zipFileName);
		ZipInputStream zipInputStream = new ZipInputStream(stream);
		byte[] bufferd = new byte[1024 * 50];
		int length = -1 ;
		List<String> files  = new ArrayList<String>();
		while((entry = zipInputStream.getNextEntry()) != null){
			String name = entry.getName();
			
			File tempFile = new File(context.getFilesDir()+File.separator+name);
			tempFile.createNewFile();
			InputStream inputStream = zipFile.getInputStream(entry);
			OutputStream outputStream = new FileOutputStream(tempFile);
			
			while((length = inputStream.read(bufferd)) != -1){
				outputStream.write(bufferd, 0, length);
			}
			files.add(name);
			inputStream.close();
			outputStream.close();
		}
		stream.close();
		
		
		//返回数据然后在一个数据库的事务里面提交
 		if(files.size() > 0 ){
 			ReturnBean<PickUpLocation>  returnBeandPickUpLocation = null ;
 			ReturnBean<Product> returnBeanProduct = null ;
 			ReturnBean<ProductCode>  returnBeanProductCode = null ;
 			ReturnBean<PickUpSerialProduct> returnSnBean = null ;
 			ReturnBean<PickUpLoactionProduct> returnNeedProduct = null ;
			for(String name : files){
				if(name.equals("outlist.txt")){
					returnBeandPickUpLocation =	handOutListXml();//需要在那些位置上拣货
				} 
				if(name.equals("outlistProduct.txt")){
					returnBeanProduct = handOutListProduct();//在那些位置上的拣货(那些container上 或者是拣什么类型的container) 。。返回数据
				}
				if(name.equals("outlistProductCode.txt")){
					returnBeanProductCode = handleProductCode();//proudct Code
				}
				if(name.equals("outlistSerialProduct.txt")){
					returnSnBean = handleSn();	// sn
				}
				if(name.equals("outlistLocationProduct.txt")){
					returnNeedProduct = handleLocationNeedProduct(pickup_id);// 
				}
				
			}
			// 添加到数据库中
			configDao.addPickUpBaseData(returnBeandPickUpLocation, returnBeanProduct, returnBeanProductCode, returnSnBean, returnNeedProduct, pickup_id);
		}
	 
	}

	private ReturnBean<PickUpLocation> handOutListXml() throws Exception {
 		String xmlFileName = "outlist.txt" ;
 		ReturnBean<PickUpLocation>  returnResult =PickUpBaseDataParse.handPickUpLocationXml(new FileInputStream(new File(context.getFilesDir()+File.separator+xmlFileName)));
 		return returnResult ;
 	 
 	}
	private ReturnBean<Product> handOutListProduct() throws Exception {
		String name = "outlistProduct.txt";
		ReturnBean<Product> returnBeanProduct = PickUpBaseDataParse.handOutListProductXml(new FileInputStream(new File(context.getFilesDir()+File.separator+name)));
		return returnBeanProduct ;
	}
	 
	private ReturnBean<ProductCode>  handleProductCode() throws Exception {
		String name = "outlistProductCode.txt";
		ReturnBean<ProductCode>  returnBeanProductCode = PickUpBaseDataParse.handPickUpProductCodeXml(new FileInputStream(new File(context.getFilesDir()+File.separator+name)));
		return returnBeanProductCode ;
	}
	private ReturnBean<PickUpSerialProduct> handleSn() throws Exception {
		String name = "outlistSerialProduct.txt";
		InputStream in = new FileInputStream(new File(context.getFilesDir()+File.separator+name));
		ReturnBean<PickUpSerialProduct> returnBean = PickUpBaseDataParse.handPickUpSerialProductXml(in);
		return returnBean;
 
	}
	private ReturnBean<PickUpLoactionProduct> handleLocationNeedProduct(long pickup_id) throws Exception {

		String name = "outlistLocationProduct.txt";
		InputStream in = new FileInputStream(new File(context.getFilesDir()+File.separator+name)) ;
		ReturnBean<PickUpLoactionProduct> returnBean =  PickUpBaseDataParse.handNeedPickUpProduct(in,pickup_id);
		return returnBean;
		
	}
	
}


