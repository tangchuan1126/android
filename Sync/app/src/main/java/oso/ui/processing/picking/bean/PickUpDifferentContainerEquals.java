package oso.ui.processing.picking.bean;

/**
 * 提交差异的时候用来判断Container是否相等
 * 
 * @author zhangrui
 * 
 */
public class PickUpDifferentContainerEquals {

	private Long fromContainerId;

	private Integer fromContainerType;

	private Long fromContainerTypeId;

	private Long pickupContainerId;

	private Long pickupContainerTypeId;

	private Integer pickupContainerType;

	public PickUpDifferentContainerEquals(Long fromContainerId,
			Integer fromContainerType, Long fromContainerTypeId,
			Long pickupContainerId, Long pickupContainerTypeId,
			Integer pickupContainerType) {
		super();
		this.fromContainerId = fromContainerId;
		this.fromContainerType = fromContainerType;
		this.fromContainerTypeId = fromContainerTypeId;
		this.pickupContainerId = pickupContainerId;
		this.pickupContainerTypeId = pickupContainerTypeId;
		this.pickupContainerType = pickupContainerType;
	}

	public Long getFromContainerId() {
		return fromContainerId;
	}

	public void setFromContainerId(Long fromContainerId) {
		this.fromContainerId = fromContainerId;
	}

	public Integer getFromContainerType() {
		return fromContainerType;
	}

	public void setFromContainerType(Integer fromContainerType) {
		this.fromContainerType = fromContainerType;
	}

	public Long getFromContainerTypeId() {
		return fromContainerTypeId;
	}

	public void setFromContainerTypeId(Long fromContainerTypeId) {
		this.fromContainerTypeId = fromContainerTypeId;
	}

	public Long getPickupContainerId() {
		return pickupContainerId;
	}

	public void setPickupContainerId(Long pickupContainerId) {
		this.pickupContainerId = pickupContainerId;
	}

	public Long getPickupContainerTypeId() {
		return pickupContainerTypeId;
	}

	public void setPickupContainerTypeId(Long pickupContainerTypeId) {
		this.pickupContainerTypeId = pickupContainerTypeId;
	}

	public Integer getPickupContainerType() {
		return pickupContainerType;
	}

	public void setPickupContainerType(Integer pickupContainerType) {
		this.pickupContainerType = pickupContainerType;
	}

	@Override
	public int hashCode() {
		return (pickupContainerTypeId + "").hashCode()
				+ (pickupContainerType + "").hashCode()
				+ (pickupContainerId + "").hashCode()
				+ (fromContainerId + "").hashCode()
				+ (fromContainerType + "").hashCode()
				+ (fromContainerTypeId + "").hashCode();
	}

	@Override
	public boolean equals(Object o) {
		PickUpDifferentContainerEquals other = (PickUpDifferentContainerEquals) o;
		
		boolean flag =  
				other.getFromContainerId().equals(getFromContainerId())
			&&  other.getFromContainerType().equals(getFromContainerType())	
			&&	other.getFromContainerTypeId().equals(getFromContainerTypeId())
			&&  other.getPickupContainerId().equals(getPickupContainerId())
			&&  other.getPickupContainerType().equals(getPickupContainerType())
			&&  other.getPickupContainerTypeId().equals(getPickupContainerTypeId());
		return flag;
	}

}
