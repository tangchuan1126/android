package oso.ui.processing.specialproject.adapter;

import oso.ui.processing.specialproject.bean.SubTaskBean;
import oso.ui.processing.specialproject.bean.SubTaskBean.SubTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import utility.Utility;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import declare.com.vvme.R;

public class SubTasksAdp extends BaseAdapter {

	private Context mContext;
	
	private SubTaskBean mData;
	
	public SubTasksAdp(Context ctx, SubTaskBean tasks) {
		mContext = ctx;
		mData = tasks;
	}

	@Override
	public int getCount() {
		if(mData == null) return 0;
		if(Utility.isNullForList(mData.main_detail.subtasks)) return 0;
		return mData.main_detail.subtasks.size();
	}

	@Override
	public Object getItem(int position) {
		return mData.main_detail.subtasks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView  == null) {
			holder = new ViewHolder();
			convertView = initView(convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		fillData(position, holder);
		
		return convertView;
	}
	
	private View initView(View convertView, ViewHolder holder) {
		
		convertView = View.inflate(mContext, R.layout.item_specialtask_subtask_item, null);

		holder.tvTaskId = (TextView) convertView.findViewById(R.id.subtask_id);
		holder.ivStatus = (ImageView) convertView.findViewById(R.id.subtask_status);
		holder.tvTaskName = (TextView) convertView.findViewById(R.id.subtask_name);
		//holder.tvTaskDes = (TextView) convertView.findViewById(R.id.subtask_des);
		holder.tvLabors = (TextView) convertView.findViewById(R.id.subtask_labors);
		holder.tvType = (TextView) convertView.findViewById(R.id.subtask_type);
		holder.layoutLabors = convertView.findViewById(R.id.layout_subtask_labors);
		
		return convertView;
	}
	
	/**
	 * 填充数据
	 * @param position
	 * @param holder
	 */
	private void fillData(int position, ViewHolder holder) {
		SubTask subTask = mData.main_detail.subtasks.get(position);
		holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(subTask.task_status));

		holder.tvTaskId.setText(subTask.schedule_id);
		holder.tvTaskName.setText(subTask.description);

//		holder.tvTaskDes.setText(subTask.description);
		
		setLaborsData(holder, subTask);
		
		//---任务状态
		holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(subTask.task_status));

		switch (subTask.task_status) {
		case SpecialTaskKey.TYPE_OPEN:
			holder.tvType.setText("Open");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_green));
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_OPEN));
			break;
		case SpecialTaskKey.TYPE_ACCEPTED:   // Accept状态 对于SuperVisor 其实就是In-Processing的状态
		case SpecialTaskKey.TYPE_INPROCESS:
			holder.tvType.setText("In-Processing");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_green));
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_INPROCESS));
			break;
		case SpecialTaskKey.TYPE_PENDING:
			holder.tvType.setText("Pending");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_gray_text));
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_PENDING));
			break;
		case SpecialTaskKey.TYPE_CLOSED:
			holder.tvType.setText("Closed");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_red));
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_CLOSED));
			break;
		case SpecialTaskKey.TYPE_ASSIGNED:
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_green));
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_ASSIGNED));
			holder.tvType.setText("Assigned");
			break;
		}

		//----图标更改
		if (subTask.task_status == SpecialTaskKey.TYPE_OPEN) {
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(subTask.status ? SpecialTaskKey.TYPE_INPROCESS : SpecialTaskKey.TYPE_OPEN));
		}

		//----如果有默认选人，则默认图标变成绿色
		/*if (subTask.task_status == SpecialTaskKey.TYPE_OPEN && !TextUtils.isEmpty(subTask.schedule_execute_id)) {
			holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(SpecialTaskKey.TYPE_INPROCESS));
		}*/

	}

	/**
	 * 设置Labors数据  SuperVisor没有选人，如果Manager已经选人，则显示Manager选的人。
	 * 			   SuperVisor有选人，如果Manager已经选人，则显示SuperVisor选的人。
	 *             都没有选人则不显示
	 * @param holder
	 * @param subTask
	 */
	private void setLaborsData(ViewHolder holder, SubTask subTask) {

		// 如果是修改过的  则只显示assignLabor的数据
		if (subTask.isUserModified) {
			if (!TextUtils.isEmpty(subTask.assignLaborsAdids)) {
				//holder.tvLabors.setVisibility(View.VISIBLE);
				holder.layoutLabors.setVisibility(View.VISIBLE);
				holder.tvLabors.setText(subTask.assignLabors + "");
				return;
			} else {
				//holder.tvLabors.setVisibility(View.GONE);
				holder.layoutLabors.setVisibility(View.GONE);
			}
			return;
		}

		if (!TextUtils.isEmpty(subTask.assignLaborsAdids)) {
			//holder.tvLabors.setVisibility(View.VISIBLE);
			holder.layoutLabors.setVisibility(View.VISIBLE);
			holder.tvLabors.setText(subTask.assignLabors + "");
			return;
		} else {
			//holder.tvLabors.setVisibility(View.GONE);
			holder.layoutLabors.setVisibility(View.GONE);
		}
		
		if (!TextUtils.isEmpty(subTask.schedule_execute_id)) {
			//holder.tvLabors.setVisibility(View.VISIBLE);
			holder.layoutLabors.setVisibility(View.VISIBLE);
			holder.tvLabors.setText(subTask.exeemployname + "");
			return;
		} else {
			//holder.tvLabors.setVisibility(View.GONE);
			holder.layoutLabors.setVisibility(View.GONE);
		}

		/*if (subTask.isUserModified) {
			holder.tvLabors.setText(subTask.assignLabors);
			return;
		}

		if (!TextUtils.isEmpty(subTask.schedule_execute_id)) {
			holder.tvLabors.setText(subTask.exeemployname);
			return;
		}*/
	}

	/**
	 *  Task No
	 *  Subject
	 *  Description
	 *  Start Time
	 *  End Time
	 *  Type [New, Closed]
	 */
	private static class ViewHolder {
		TextView tvLabors;
		TextView tvTaskName, tvTaskId, tvTaskDes, tvType;
		ImageView ivStatus;
		View layoutLabors;
	}
}
