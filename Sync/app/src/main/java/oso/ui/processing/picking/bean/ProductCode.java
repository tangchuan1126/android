package oso.ui.processing.picking.bean;

public class ProductCode {

	private long id ;
	private long pcid ;
	private String barcode ;
	private String codeType ;
	private String p_name;
	
 
	public ProductCode() {
		super();
	}
	
	
	public ProductCode(long pcid, String barcode, String codeType, String p_name) {
		super();
		this.pcid = pcid;
		this.barcode = barcode;
		this.codeType = codeType;
		this.p_name = p_name;
	}


	public ProductCode(long pcid, String barcode, String codeType) {
		super();
		this.pcid = pcid;
		this.barcode = barcode;
		this.codeType = codeType;
	}


	public ProductCode(long id, long pcid, String barcode, String codeType) {
		super();
		this.id = id;
		this.pcid = pcid;
		this.barcode = barcode;
		this.codeType = codeType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPcid() {
		return pcid;
	}

	public void setPcid(long pcid) {
		this.pcid = pcid;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


	public String getCodeType() {
		return codeType;
	}


	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}


	public String getP_name() {
		return p_name;
	}


	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
 
	 
	
	
}
