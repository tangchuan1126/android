package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.ui.processing.picking.bean.LpInfo;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import oso.ui.processing.picking.bean.PickUpSerialProduct;
import oso.ui.processing.picking.bean.ProductCode;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpProductCodeDao;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.db.PickUpSerialProductDao;
import oso.ui.processing.picking.runnable.ContainerParseJsonHelper;
import support.common.AlertUi;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.BCSKey;
import support.key.ContainerIntTypeKey;
import support.network.HttpPostMethod;
import utility.HttpUrlPath;
import utility.MediaPlayerUtil;
import utility.StringUtil;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
/**
 * 在某一个具体的Container上面直接拣货
 * 有可能是在某种具体的TLP上面拣货
 * @author zhangrui
 *
 */
public class PickUpContainerProductActivity extends TTSFragment{
	
	private static final int SNSKUISCHECK = 3; // sn check && sku check
	private static final int SNCHECKSKUUNCHECKANDSKUNOVALUE = 4; // sn check && sku uncheck  && skuText is null;
						 							 
	private static final int SNCHECKSKUUNCHECKANDSKUVALUE = 5;// sn check && sku uncheck && skuText is not null;
	private static final int SKUQTY = 6; // sku 和 Qty情况的处理
	private static final int SKUQTYANDLOCKQTY = 7; // qty 锁住的情况。直接扫描sku
	
	
	private PickUpBean pickUpBean ;
	private Long pickup_id ;
	private Long location_id ;
	private Context context ;
	private Resources resources ;
	
	
	private LinearLayout containerBody ;
	private PickUpScanInfoDao pickUpScanInfoDao ;
	private PickUpSerialProductDao serialProductDao ;
	private PickUpProductCodeDao productCodeDao ;
 	
	
 	private EditText SnText ;		 //SnText,SkuText,QtyText
	private EditText SkuText ;
 	private CheckBox SkuCheckBox ;

	
 	private EditText SkuScanedSkuText; // 在ViewPager为2的时候页面的Sku
 	private CheckBox QtyCheckBox ;
	private EditText QtyText ;
	
	private Button toolScan = null ;
	private Button toolList = null ;
	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ViewPager<<<<<<<<<<<<<<<<<<<<<<
	private ViewPager viewPager; // 页卡内容 初始化ViewPager
	private ImageView cursorImage; // 动画图片
	private TextView snBar, skudBar ;
	private List<View> views; // Tab页面列表
	private int offset = 0; // 动画图片偏移量
	private int currIndex = 0 ;// 当前页卡编号
	private int fromIndex = 0 ; //表示从哪一个Index页面来的
	private int bmpW;// 动画图片宽度
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ViewPager<<<<<<<<<<<<<<<<<<<<<<
	
	private ProductCode lockBarCode;
	
	//player
	private MediaPlayer successPlayer = null ;
	private MediaPlayer alertPlayer = null ;
	
	 
	
	private int addType = 3 ;	//  
 	private int currentShowView = 1 ;	
	private double lockQtyLimit = 100.0d ; 	//如果是不小心扫描错了商品数量应该提示
	private String lockQtyNumber = "" ; 	//锁住商品数量的时候的
	
	
	private String pickUpPcIds = 	"" ;	//这个Container 能够拣货的的PCIDS
	private List<ProductCompareSimple> needPickUpProducts  = null ;
	
	private ProgressDialog progressDialog = null ;
	private ProgressDialog baseDataProgressDialog = null ;
	
	
	private TextView title ; 
	DisplayMetrics dm;
	private String containerPrefix ;
 	private static AsyncHttpClient client = new AsyncHttpClient();
 	private AlertDialog showSetWorkContainerDialog ; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
 		super.onCreate(savedInstanceState);
 		setContentView(R.layout.pickup_location_product_layout);
 		
 		initField();
 		
 		initViewPager();
		//初始化player
		initPlayers();
		
		initTab();
 		//initOper();
 		setButton();
 		
 		//初始化应该要拣的货
 		needPickUpInfo();
 		
 		setWorkContainer();
 		
	}
	
	public void setWorkContainer(){
		
		//1如果在某个具体的ContainerType上面拣货,那么必须先扫描一个ContainerId。
		//2.如果是TLP上面已经没有货了那么应该
		
		if(pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductOnType || pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductNoType ){
			Builder builder = new Builder(context);
			builder.setTitle(resources.getString(R.string.sys_holdon));
			View view = LayoutInflater.from(context).inflate(R.layout.pickup_product_on_containertype_affr_layout, null);
			final EditText container = (EditText)view.findViewById(R.id.container);
			container.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if(KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP ){
						String value = container.getText().toString();
						if(StringUtil.isNullOfStr(value)){
							alertMessage(true, resources.getString(R.string.scan_container), 0, false, null);
						}else{
							loadWorkContainer(value,container);
						}
						return true ;
					}
 					return false;
				}
			});
			//builder.setCancelable(cancelable)
 			builder.setView(view);
 			builder.setPositiveButton(resources.getString(R.string.sync_yes),null);
 
 			showSetWorkContainerDialog =   builder.show();
		}else{
			
		}
		
	}
	private void loadWorkContainer(final String container ,final EditText containerEdit){
		Goable.initGoable(context);

		RequestParams params = new RequestParams();
		params.put("LoginAccount", Goable.LoginAccount);
		params.put("Password", Goable.Password);
		params.put("Machine", "");
		params.put("Container", container);
		params.put("Method", HttpPostMethod.InnerContainerLoad);
		client.get(HttpUrlPath.Container, params, new JsonHttpResponseHandler("utf-8"){
			@Override
			public void onStart() {
				if(!baseDataProgressDialog.isShowing()){baseDataProgressDialog.show();}
			}
			@Override
			public void onFinish() {
				 if(baseDataProgressDialog != null && baseDataProgressDialog.isShowing()){baseDataProgressDialog.dismiss();}
			}
			@Override
//			public void onSuccess(int statusCode, JSONObject json) {
			public void onSuccess(int statusCode, Header[] headers,
					JSONObject json) {
				if(statusCode == 200){
					handResult(json,containerEdit);
				}else{
					alertMessage(true, resources.getString(R.string.sys_error), 0, false, null);
 				}
 			}
		});
 	}
	
	//处理请求的结果
		private void handResult(JSONObject json, EditText containerEdit){
			//对应的ContainerType要和pickup的一致,并且如果是有需要拣货具体的Container,那么一定是对应的
			 if(StringUtil.getJsonInt(json, "ret") == 1){
				 LpInfo lpInfo = ContainerParseJsonHelper.getLpInfo(json);
				 // 1.container 一定是这个位置, 一定是这个类型的 
				 if( lpInfo == null ||
						 lpInfo.getLp() == 0l || 
						 !lpInfo.getContainerType().equalsIgnoreCase(containerPrefix) || 
						 (lpInfo.getContainerTypeId() != pickUpBean.getFromContainerTypeId() && pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductOnType )){
					 	 //只有是在 直接某种类型的Container上拣货的时候才会提供Container type 是否相等
					 	 alertContainerTypeError(lpInfo.getContainerTypeId()+"");	
					 	 containerEdit.setText("");
					 	 containerEdit.requestFocus();
					 	 return ;
					 	
				 } 
				 //一定是在这个位置
				 if(lpInfo.getLocationId() != location_id){
					 alertContainerNotOnThisLocation();
					 containerEdit.setText("");
					 containerEdit.requestFocus();
					 return ;
				}
				 pickUpBean.setFromContainerId(lpInfo.getLp());
				 if(showSetWorkContainerDialog.isShowing()){
					 showSetWorkContainerDialog.dismiss();
				 }
				title.setText(String.format("On [%1s%2s] PickUp Product",  ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()),pickUpBean.getFromContainerId()+""));
	  		 }else{
				 //系统错误等信息
				 alertMessage(true, BCSKey.getReturnStatusById(context,StringUtil.getJsonInt(json, "err")) , 0, false, null);
				 
	 		 }
		}
		private void alertContainerTypeError(String lpInfoTypeId ){		 
			 //LpInfo 不对
			 String notify = String.format(resources.getString(R.string.container_type_not_matched), 
					 lpInfoTypeId,pickUpBean.getFromContainerTypeId()+"");
			 alertMessage(true, notify, 0, true, resources.getString(R.string.speak_container_type_wrong));
 		}
		private void alertContainerNotOnThisLocation(){
			alertMessage(true, null, 0, false, null);
			AlertUi.showAlertBuilder(context, resources.getString(R.string.container_no_this_location));
 		}
		 public void setButton(){
				
				toolList.setOnFocusChangeListener(new OnFocusChangeListener() {	
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
//						if(hasFocus){
//							toolList.setBackgroundResource(R.drawable.list);
//						}else{
//							toolList.setBackgroundResource(R.drawable.list_down);
//						}
					}
				});
				toolScan.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
//						if(hasFocus){
//							toolScan.setBackgroundResource(R.drawable.scan_down);
//						}else{
//							toolScan.setBackgroundResource(R.drawable.scan);
//						}
					}
				});
			 
		    }
	private void initField(){
		//init field
		pickUpBean = (PickUpBean)getIntent().getSerializableExtra("pickUpBean");
		pickup_id = getIntent().getLongExtra("pickup_id",0l);
		location_id = getIntent().getLongExtra("location_id", 0l);
		context =  this ;
		resources = context.getResources() ;
		
		//view
		snBar = (TextView) findViewById(R.id.snBar);
		skudBar = (TextView) findViewById(R.id.skuBar);
 		
 		snBar.setOnClickListener(new MyOnClickListener(0));
		skudBar.setOnClickListener(new MyOnClickListener(1));
		
		snBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_left_style_press));
		snBar.setTextColor(resources.getColor(R.color.white));
		skudBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_right_style));
		skudBar.setTextColor(resources.getColor(R.color.black));
		
		containerBody = (LinearLayout) findViewById(R.id.containerBody);

		//dao 
	    pickUpScanInfoDao = new  PickUpScanInfoDao(context);
	    serialProductDao = new PickUpSerialProductDao(context);
	    productCodeDao = new PickUpProductCodeDao(context);
 		
	    pickUpPcIds = getContainerPickUpPcids() ;
	    
	    progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(resources.getString(R.string.sys_holdon));
		progressDialog.setMessage(resources.getString(R.string.pickup_init_info));
		
		baseDataProgressDialog = new ProgressDialog(context);
		baseDataProgressDialog.setTitle(resources.getString(R.string.sys_holdon));
		baseDataProgressDialog.setMessage(resources.getString(R.string.sys_getData));
		
		containerPrefix = ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()) .toUpperCase();

		title = (TextView)findViewById(R.id.title);
		//显示ContainerType
		String notify  = "" ;
		if( pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductOnType ) {
		    notify = String.format(resources.getString(R.string.pickup_on_container_type_direct_product), ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()) ,pickUpBean.getFromContainerTypeId());
		}if(pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductNoType){
			notify = String.format(resources.getString(R.string.pickup_on_container_no_type_direct_product), ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()) );
		}else{
		    notify = String.format(resources.getString(R.string.pickup_on_container_direct_product),  ContainerIntTypeKey.getType(pickUpBean.getFromContainerType()) + pickUpBean.getFromContainerId());
		}
		title.setText(notify);
	}
	private String getContainerPickUpPcids(){
		String result = "" ;
		if(pickUpBean != null){
			List<PickUpLoactionProduct>  arrayList = pickUpBean.getArrayListPickupLocationProduct() ;
			for(PickUpLoactionProduct temp : arrayList){
				result += temp.getPcid()+",";
			}
		}
		return result; 
	}
	private void initTab(){
		
  		toolScan = (Button)findViewById(R.id.tool_scan);
 		toolList = (Button)findViewById(R.id.tool_list);
  		addTab(toolScan, PickUpProductScanActivity.class,"Scan",1);
 		addTab(toolList, PickUpProductListActivity.class, "List",2);
    	showTabView(1,false);	 
   }
	
	private void initPlayers() {
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		successPlayer = MediaPlayerUtil.getInitMediaPlayer(R.raw.tinkerbell, resources);
		alertPlayer = MediaPlayerUtil.getInitMediaPlayer(R.raw.error , resources);
	}
	private void initAddType() {

		if (currIndex == 0) {
			SkuCheckBox.setEnabled(true);
			SnText.setEnabled(true);
			SnText.setText("");
			SkuText.setEnabled(true);
			SkuText.setText("");
		}
		if (currIndex == 1) {
			QtyCheckBox.setEnabled(true);
			QtyCheckBox.setChecked(true);
			QtyText.setEnabled(true);
			QtyText.setText("");
		}
		if(addType == SNSKUISCHECK){
			SnText.requestFocus();
		}
		if(addType == SKUQTY){
			SkuScanedSkuText.requestFocus();
		}
		 
	
	}
	private boolean validateSku(){ 

		Editable skuTextStr = SkuText.getText();
		String scanedBarCode = "";
		if (skuTextStr == null || skuTextStr.toString().trim().length() < 1) {
			scanedBarCode = SkuText.getHint().toString();
		} else {
			scanedBarCode = skuTextStr.toString();
		}
		
		
		if (scanedBarCode.trim().length() < 1) {
			MediaPlayerUtil.playMedia(alertPlayer);
			return true;
		} else {
			ProductCode productCode = getProductCode(scanedBarCode);
			if (productCode != null && productCode.getId() != 0l) {
				//读取Snvalue 如果SNValue为null那么提示他是否想锁住当前的sku
				String snValue = SnText.getText().toString();
				if(StringUtil.isNullOfStr(snValue)){
					affrLockSku(productCode);
					return true ;
				}
				// 有可能是想锁住SKU的情况
				if(addType == SNSKUISCHECK || addType == SNCHECKSKUUNCHECKANDSKUNOVALUE || addType == SNCHECKSKUUNCHECKANDSKUVALUE ){
 					addScanInfo(SnText.getText().toString(), scanedBarCode,
							1.0d,  productCode.getPcid(),
							productCode.getP_name(), productCode.getCodeType(),
							addType);
	 
				}
				return true;
			} else {
				UIHelper.showToast(context,resources.getString(R.string.sys_sku_not_found),Toast.LENGTH_SHORT).show();
				SkuText.setText("");
				SkuText.setHint("");
				MediaPlayerUtil.playMedia(alertPlayer);
				return true;
			}
		}
	
	}
	private void addScanInfo(String sn , String scanedBarcode , double qty    ,long p_id, String p_name,String barCode_type ,int addType){
		//如果没有
		if(pickUpBean.getFromContainerId() == 0l){
			
			setWorkContainerAndNotify();
			return ;
		}
		
		PickUpScanInfo scanInfo = new PickUpScanInfo();
		scanInfo.setBarCode(scanedBarcode);
		scanInfo.setCode_type(barCode_type);
		scanInfo.setFrom_container_id(pickUpBean.getFromContainerId());
		scanInfo.setFrom_container_type(pickUpBean.getFromContainerType());
		scanInfo.setFrom_container_type_id(pickUpBean.getFromContainerTypeId());
 		scanInfo.setIsSubmit(0);
		scanInfo.setLocation_id(location_id);
		scanInfo.setPickup_id(pickup_id);
		scanInfo.setPname(p_name);
		scanInfo.setQty(qty);
		scanInfo.setSku(p_id);
		scanInfo.setSn(sn);
	 
		submitScanInfo(scanInfo,addType);
	}
	
	
	private Handler initPickUpHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			checkIsPickupOver();
			if(progressDialog.isShowing()){progressDialog.cancel();}
		};
	};
	 
	//init 初始要拣货的数据信息。和已经拣货多少了
	private void needPickUpInfo(){
		progressDialog.show();
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				if(pickUpBean.getPickUpType() == PickUpBean.DirectPcikUpProductNoType){
					needPickUpProducts = pickUpScanInfoDao.getContainerPickupProductListNoTypeId(pickup_id, location_id, pickUpBean.getFromContainerType());
				}else{
					needPickUpProducts = pickUpScanInfoDao.getContainerPickupProductList(pickup_id, location_id, pickUpBean.getFromContainerId());	
				}
				initPickUpHandler.obtainMessage().sendToTarget();
			}
			 
		});
		thread.start();
		
	}
	//是否已经 拣货完成了 
	private boolean isPickUpOver(){
		boolean flag = true ;
		if(needPickUpProducts != null && needPickUpProducts.size() > 0 ){
			for(ProductCompareSimple temp : needPickUpProducts){
				if(temp.getPickupQty() != temp.getTotalQty()){
					flag = false ;
					break ;
				}
			}
		}
		return flag ;
	}
	
	private void checkIsPickupOver(){
		if(isPickUpOver()){
			alertMessage(true, null, 0, true, resources.getString(R.string.speak_pickup_container_type_over));
 			Builder overBuilder = new Builder(context);
 			overBuilder.setTitle(resources.getString(R.string.sys_holdon));
 			
   			overBuilder.setMessage(resources.getString(R.string.pickup_container_product_is_pickup_over));
 		
 			overBuilder.setPositiveButton(resources.getString(R.string.sys_button_pickup_goon),new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
 					
				} 				
 			});
 			overBuilder.setNegativeButton(resources.getString(R.string.sync_no), null);
 			overBuilder.show();			
		}
	}
	//判断这个Container上面拣货的要求是否已经完成
	
	private boolean isCanAddPickupScanInfo(PickUpScanInfo scanInfo){
		boolean flag = false ;
		
		 
		for(ProductCompareSimple simple : needPickUpProducts){
			if(simple.getPcid() == scanInfo.getSku()){
				if(simple.getPickupQty() + scanInfo.getQty() <= simple.getTotalQty()){
					flag = true ;
					break ;
				}
			}
		}
		return flag ;
	}
	//往当前的需要拣货添加数据
	private void addNeedContainerProduct(PickUpScanInfo scanInfo){
 		for(ProductCompareSimple simple : needPickUpProducts){
			if(simple.getPcid() == scanInfo.getSku()){
				double old = simple.getPickupQty() ;
				simple.setPickupQty(old + scanInfo.getQty());
			}
		}
 	}
	
	/**
	 * @param scanInfo
	 * @param type  	
	 */
	private void submitScanInfo(PickUpScanInfo scanInfo , int type){
  		//添加到数据库中
		try{
			//首先检查这次添加的商品以及个数是否已经超过了需要的
			boolean isCanAdd =  isCanAddPickupScanInfo(scanInfo);
			if(isCanAdd){
	 			List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>();
	 			arrayList.add(scanInfo);
	 			//验证是否这个PCID已经按照要求的拣货满了
	 			//
	 			pickUpScanInfoDao.addPickupScanInfo(null , arrayList);
	 			MediaPlayerUtil.playMedia(successPlayer);
	 			addNeedContainerProduct(scanInfo);
	 			checkIsPickupOver();
			}else{
				alertMessage(true, null, 0, false, null);
				AlertUi.showAlertBuilder(context, resources.getString(R.string.pickup_this_product_is_over,scanInfo.getPname()));
			}
		}catch(Exception e){
			//系统错误
 			MediaPlayerUtil.playMedia(alertPlayer);
 			UIHelper.showToast(context, resources.getString(R.string.sys_error_add_scanInfo_error), Toast.LENGTH_SHORT).show();
		}finally{
			if (type == SNSKUISCHECK) {
				SnText.setText("");
				SkuText.setText("");
				SkuText.setHint("");
				SnText.requestFocus();
			}
			if (type == SNCHECKSKUUNCHECKANDSKUNOVALUE) {
				SnText.setText("");
				SkuText.setText("");
				SkuText.setEnabled(false);
				SnText.requestFocus();
			}
			if (type == SNCHECKSKUUNCHECKANDSKUVALUE) {
				SnText.setText("");
				SnText.requestFocus();
				SkuText.setText((lockBarCode != null && lockBarCode.getId() != 0l) ? lockBarCode.getBarcode() : "");
				SkuText.setEnabled(false);
			}
			// declare scaned 第二个页面
			if (type == SKUQTY) {
				SkuScanedSkuText.setText("");
				SkuScanedSkuText.requestFocus();
	 			QtyText.setText("");
				QtyText.setHint("");
			}
			if (type == SKUQTYANDLOCKQTY) {
				SkuScanedSkuText.setText("");
				SkuScanedSkuText.requestFocus();
			}
			showTabView(currentShowView, false);
		}
	  
 	}
	 private void showTabView(int index , boolean isrefreshViewType){
		  if(index == 0 ){
			  index = currentShowView ;
		  }
		 
		  currentShowView = index;
		  Fragment fragment = null ;	
		  clearOtherTabColor();
		  if(index == 1){fragment = new PickUpProductScanActivity(); toolScan.setBackgroundResource(R.drawable.inventory_tab_bg_text);toolScan.setTextColor(resources.getColor(R.color.pick_up_btn_color));}
		  if(index == 2){fragment = new PickUpProductListActivity(); toolList.setBackgroundResource(R.drawable.inventory_tab_bg_text);toolList.setTextColor(resources.getColor(R.color.pick_up_btn_color));}
  		  //添加数据
 		 Bundle bundle =  new Bundle();
 		 bundle.putLong("pickup_id", pickup_id);
 		 bundle.putLong("location_id", location_id);
 		 bundle.putInt("from", StringUtil.getPickupFromValue(pickUpBean) );
 		 bundle.putSerializable("pickUpBean", pickUpBean);
		 fragment.setArguments(bundle);
		 FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
	  	 transaction.replace(R.id.containerBody, fragment);
	  	 transaction.commit();
  		  
	  }
	
		private void addTab(final TextView textView,final java.lang.Class clas ,final String title_ , final int currentIndex){
			textView.setOnClickListener(new View.OnClickListener() {
				
				private int index = currentIndex;
				private String title = title_;
				@Override
				public void onClick(View v) {
					showTabView(index,true);
				}
			});
		}
	 private void clearOtherTabColor(){
 			toolScan.setBackgroundResource(R.drawable.pick_btn_text_style_two);
 			toolScan.setTextColor(resources.getColor(R.color.white));
 			toolList.setBackgroundResource(R.drawable.pick_btn_text_style_two);
 			toolList.setTextColor(resources.getColor(R.color.white));
 	}
	private void initViewPager() {
		 
		viewPager = (ViewPager) findViewById(R.id.scanedPager);
		cursorImage = (ImageView) findViewById(R.id.cursor);
		bmpW = BitmapFactory.decodeResource(getResources(), R.drawable.cursor).getWidth();// 获取图片宽度
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels;// 获取分辨率宽度
		offset = (screenW / 2- bmpW) / 2;// 计算偏移量
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		cursorImage.setImageMatrix(matrix);// 设置动画初始位置

		views = new ArrayList<View>();
		LayoutInflater inflater = getLayoutInflater();
		View snScanedView = inflater.inflate(R.layout.sn_scaned_layout, null);
		View skuScanedView = inflater.inflate(R.layout.sku_scaned_layout, null);
		View lpScanedView = inflater.inflate(R.layout.lp_scaned_layout, null);
		
		views.add(snScanedView);
		views.add(skuScanedView);
		views.add(lpScanedView);
		viewPager.setAdapter(new MyViewPagerAdapter(views));
		viewPager.setCurrentItem(0);
		viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
		
	 
	}
	private void initFieldSnScaned(View viewPage) {

		SnText = (EditText) viewPage.findViewById(R.id.sn_no_text);
		SkuText = (EditText) viewPage.findViewById(R.id.sku_no_text);
 		SkuCheckBox = (CheckBox) viewPage.findViewById(R.id.lab_layout_checkbox_skuNo);
 
		SnText.setHintTextColor(resources.getColor(R.color.text_hint));
		
		//张睿
		SkuText.setInputType(InputType.TYPE_NULL);
		SnText.setInputType(InputType.TYPE_NULL);
		initFieldSnOpera();
		initAddType();
		

	}
	private void handleSN(String snValue, PickUpSerialProduct serialProduct){

		// 直接添加 这里根据SKU (商品ID)去查询商品的主条码,p_name
		ProductCode mainCode = productCodeDao.getMainCodeByPid(serialProduct.getPcid());
		switch (addType) {
		 
		case SNSKUISCHECK: {
			// 默认sn查询出来的PCID显示他的MainCode信息 (主条码)
			String barCode = mainCode.getBarcode();
			SkuText.setHint(StringUtil.getSpannable(barCode));
			SkuText.requestFocus();
		}
		break;
		case SNCHECKSKUUNCHECKANDSKUNOVALUE: {
			// sn check && sku uncheck && sku no value
			// 识别SN后添加。如果不识别 SKU 获取焦点
			if (mainCode.getId() != 0l) {
				// 序列号添加。添加的是主条码,在扫描的界面
	  			addScanInfo(snValue, mainCode.getBarcode() + "", 1.0d, 
					 
						mainCode.getPcid(),
						mainCode.getP_name(),
						mainCode.getCodeType(),
						SNCHECKSKUUNCHECKANDSKUNOVALUE);
			} else {
				SkuText.setEnabled(true);
				SkuText.requestFocus();
			}
		}
			break;
		case SNCHECKSKUUNCHECKANDSKUVALUE: {
			// sku 锁住. 并且SN识别的情况
			if (lockBarCode != null && lockBarCode.getId() != 0l) {
				if (serialProduct.getPcid() == lockBarCode.getPcid()) {
					addScanInfo(snValue, mainCode.getBarcode() + "", 1.0d, 
							 
							serialProduct.getPcid(),
							mainCode.getP_name(),
							mainCode.getCodeType(),
							SNCHECKSKUUNCHECKANDSKUVALUE);
					SnText.requestFocus();
				} 
			}

		}
			break;
		default:
			break;
		}

		alertMessageSuccess(false, null, 2, true, " serial number ", snValue);

	}
	private void setWorkContainerAndNotify(){
		setWorkContainer();
		alertMessage(true, null	, 0	, false	, null);
 		alertMessage(true, resources.getString(R.string.pickup_product_on_containertype_notify_1), 0, false, null);
	}
	private void initFieldSnOpera(){
		 SnText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
	 				if(hasFocus){	
	 					SnText.setText("");
	 				}else{
	 					
	 				}
				}
			});
		 
		  SnText.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					boolean flag = false ;
					if(KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP ){

						if(pickUpBean.getFromContainerId() == 0l){
							setWorkContainerAndNotify();
							return true ;
						}
	 					String snValue = SnText.getText().toString();
 	 	 
	 					if(snValue.trim().length() > 0){
	 					 
	 						int count = pickUpScanInfoDao.countSnInScanInfo(snValue, pickup_id, location_id);
	 						if(count > 0){
	 							flag = true ;
								alertMessage(false, "该SN对应商品已经扫描", 0, true, resources.getString(R.string.speak_repeate_sn));

	 							SnText.setText("");
	 						}
	 						if(!flag){
		 						PickUpSerialProduct serialProduct = serialProductDao.getPickUpSerialProductBySn(snValue);
		 						if(serialProduct != null && serialProduct.getId() != 0l){
		 							handleSN(snValue, serialProduct);
		 							flag = true ;
			 					}else{
									// 不识别SN的情况 speak(sn,"1212");
									alertMessageSuccess(false, null, 2, true, " serial number ", snValue);
									showSnNotRe();
									if (addType == SNSKUISCHECK) {
										SkuText.requestFocus();
										SkuText.setText("");
										flag = false;
									}
									if (addType == SNCHECKSKUUNCHECKANDSKUNOVALUE) {
										SkuText.setEnabled(true);
										SkuText.requestFocus();
										SkuText.setText("");
										flag = false;
									}
									if (addType == SNCHECKSKUUNCHECKANDSKUVALUE) {
										SkuText.setEnabled(true);

										SkuText.requestFocus();
										SkuText.setText("");
										flag = false;
									}				
			 					}
	 						}
	 					}
	 				
					}
	 				return flag;
				}
			});
		  
		  SkuText.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if(KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP){
						return validateSku();
					}
	 				return false;
				}
			});	
		    
			//sku 
		  SkuText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(!SkuCheckBox.isChecked()){return ;}
	 				if(hasFocus){
	 					SkuText.setText("");
	 				}else{
	 			 
	 				}
				}
			});
		   //处理在SN Scan的时候锁住SkuEditText的情况
		  SkuCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				//如果是想锁住 barCode && barCode 可以查询对应的值
				if(isChecked){
					SkuText.setEnabled(true);
					SkuText.setHint("");
					SkuText.setText("");
					SkuText.requestFocus();
					lockBarCode = null ;
					addType = SNSKUISCHECK;
				}else{
					String barCode = SkuText.getText().toString() ;
					if(barCode != null && barCode.length() > 0 ){
						ProductCode temp  = getProductCode(SkuText.getText().toString());
						setLockSku(temp);
					}

					 
				}
			}
		});
	}
	private void showSnNotRe() {
		UIHelper.showToast(context,resources.getString(R.string.sn_unkown_need_scan_sku),Toast.LENGTH_SHORT).show();
	}
	private void affrLockSku(final ProductCode productCode){
		
		Builder builder = new Builder(context);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		
		View view =	LayoutInflater.from(context).inflate(R.layout.sn_scaned_affri_lock_sku_layout, null);
		TextView lockSkuAffrim =(TextView) view.findViewById(R.id.lock_sku_affrim);
		lockSkuAffrim.setText(resources.getString(R.string.lock_sku_affrim, productCode.getP_name()));
		
		TextView lockSkuAffrimLock =(TextView) view.findViewById(R.id.lock_sku_affrim_lock);
		lockSkuAffrimLock.setText(resources.getString(R.string.lock_sku_affrim_lock));
		builder.setView(view);
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SnText.requestFocus();
				setLockSku(productCode);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SkuText.setText("");
 				SnText.requestFocus();
			}
		});
		alertMessage(true, null, 0, true, resources.getString(R.string.speak_make_sure_is_lock));
		builder.create().show();
		
	}
	private void setLockSku(ProductCode productCode){
		if(productCode != null && productCode.getPcid() != 0l){
			addType = SNCHECKSKUUNCHECKANDSKUVALUE;
			lockBarCode = productCode ;
			SnText.requestFocus();
		}else{
			addType = SNCHECKSKUUNCHECKANDSKUNOVALUE;
		}
		SkuText.setEnabled(false);
		SnText.requestFocus();
	}
	
	// veiw pager == 1 sku扫描界面
	private void initFieldSkuScaned(View viewPage) {

		SkuScanedSkuText = (EditText) viewPage.findViewById(R.id.sku_no_text);
 		QtyText = (EditText) viewPage.findViewById(R.id.quantity_text);

		QtyCheckBox = (CheckBox) viewPage.findViewById(R.id.lab_layout_checkbox_qty);
  		//张睿
 
		QtyText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
				} else {
					QtyText.setText("");
				}
			}
		});
		SkuScanedSkuText.setInputType(InputType.TYPE_NULL);
		QtyText.setInputType(InputType.TYPE_NULL);
		initFieldSkuOpera();
	}
	
private void initFieldSkuOpera(){
		
		SkuScanedSkuText.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					String scanedBarCode = SkuScanedSkuText.getText().toString();
 					 
					if (scanedBarCode.trim().length() < 1) {
 						SkuScanedSkuText.setText("");
						alertMessage(true, resources.getString(R.string.sys_sku_not_found), 0, false, null);
 						return true;
					}

					ProductCode productCode = getProductCode(scanedBarCode);
					if (productCode == null || productCode.getId() == 0l) {
 						SkuScanedSkuText.setText("");
						alertMessage(true, resources.getString(R.string.sys_sku_not_found), 0, false, null);
 						return true;
					}
					// 这里判断当前是否是AddType为SKUQTYANDLOCKQTY的情况。如果是的那么就是 锁住了Qty
					// .直接添加数据的情况
					if (addType == SKUQTYANDLOCKQTY) {
 						addScanInfo("", scanedBarCode,Double.parseDouble(lockQtyNumber) , productCode.getPcid() , productCode.getP_name(),productCode.getCodeType(), SKUQTYANDLOCKQTY);
						return true;
					}
					if (addType == SKUQTY) {
						QtyText.setHint(StringUtil.getSpannable("1"));
					}
				}
				return false;
			}
		});
		
		QtyCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) { 
				String qtyValue = QtyText.getText().toString();

				if (!isChecked) {
					if (!StringUtil.isNullOfStr(qtyValue) && StringUtil.checkFloat(qtyValue, "+")) {
						double value = Double.parseDouble(qtyValue.trim());
						if(value > lockQtyLimit ){
							affraIsLockQtyIstrue(value);
						}else{
							lockQtyNumber = qtyValue.trim();
							//这里进行一个判断如果是输入的值大于了100那么应该提示他是否真的大于一百
							addType = SKUQTYANDLOCKQTY;
							SkuScanedSkuText.setText("");
							SkuScanedSkuText.requestFocus();
							QtyText.setEnabled(false);
							QtyText.setHint(lockQtyNumber);
						}
					}

				} else {
					addType = SKUQTY;
					lockQtyNumber = "0.0";
					QtyText.setEnabled(true);
					QtyText.setHint("");
					QtyText.setText("");
				}
			}
		}); 
		
		QtyText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					String scanedBarCode = SkuScanedSkuText.getText().toString();
					// declare
					if (SKUQTY == addType && scanedBarCode.length() > 0) {
						// 输入数量过后的添加
						// 查询p_name ,根据扫描的条码
						
						
						String qtyStringTemp = QtyText.getText().toString().trim();
						if (StringUtil.isNullOfStr(qtyStringTemp)) {
							qtyStringTemp = QtyText.getHint().toString();
						}
						if (!StringUtil.checkFloat(qtyStringTemp, "+")) {
							UIHelper.showToast(context,resources.getString(R.string.qty_not_matched),Toast.LENGTH_SHORT).show();
							return true;
						}
						double qtyNumberTemp = Double.parseDouble(qtyStringTemp);
						if(qtyNumberTemp > lockQtyLimit){
							//确定商品都是那么多,而不是扫描错了
							affraProductIsQty(qtyNumberTemp,scanedBarCode);
						}else{
							ProductCode productCode = getProductCode(scanedBarCode);
							if(productCode != null)
								addScanInfo("", scanedBarCode, qtyNumberTemp , productCode.getPcid(), productCode.getP_name(),productCode.getCodeType(), SKUQTY);
						}
					}
					SkuScanedSkuText.requestFocus();
					return true;
				}

				return false;
			}
		});
	}
	private void affraIsLockQtyIstrue(final double qty){
		Builder builder = new Builder(context);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		String notify = resources.getString(R.string.affri_qty);
		builder.setMessage(String.format(notify, qty+""));
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new OnClickListener() {		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				lockQtyNumber = qty+"" ;
				//这里进行一个判断如果是输入的值大于了100那么应该提示他是否真的大于一百
				addType = SKUQTYANDLOCKQTY;
				SkuScanedSkuText.setText("");
				SkuScanedSkuText.requestFocus();
				QtyText.setEnabled(false);
				QtyText.setHint(lockQtyNumber);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				addType = SKUQTY;
				lockQtyNumber = "0.0";
				QtyText.setEnabled(true);
				QtyText.setHint("");
				QtyText.setText("");
			}
		});
		alertMessage(true, null, 0, false, null);
		builder.create().show();
	}

	private void affraProductIsQty(final double qty , final String scanedBarCode){
		Builder builder = new Builder(context);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		String notify = resources.getString(R.string.affri_qty);
		builder.setMessage(String.format(notify, qty+""));
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new OnClickListener() {		
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ProductCode productCode =getProductCode(scanedBarCode);
				if(productCode != null)
					addScanInfo("", scanedBarCode, qty,productCode.getPcid(), productCode.getP_name(),productCode.getCodeType(), SKUQTY);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				QtyText.setText("");
				QtyText.requestFocus();
			}
		});
		alertMessage(true, null, 0, false, null);
		builder.create().show();
	
	}
	private ProductCode getProductCode(String barCode){
		ProductCode productCode = productCodeDao.getProductBySku(barCode);
 		if(productCode != null && pickUpPcIds.indexOf(productCode.getPcid()+"") != -1){
			return  productCode;
		}else{
			return null ;
		}
	 
	}
	/**
	 * 头标点击监听 3
	 */
	private class MyOnClickListener implements View.OnClickListener {
		private int index = 0;

		public MyOnClickListener(int i) {
			index = i;
		}
		public void onClick(View v) {
			viewPager.setCurrentItem(index);
		}
	}
	//view pager Adapter
		public class MyViewPagerAdapter extends PagerAdapter {

			private List<View> mListViews;

			public MyViewPagerAdapter(List<View> mListViews) {
				this.mListViews = mListViews;
			}

			@Override
			public void destroyItem(ViewGroup container, int position, Object object) {
				container.removeView(mListViews.get(position));
			}

			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				View viewPage = mListViews.get(position);
				if (position == 0) {
					//snSku 
					initFieldSnScaned(viewPage);
	 			}
				if (position == 1) {
					//sku qty
					initFieldSkuScaned(viewPage);
	 			}

				container.addView(viewPage, 0);
				return mListViews.get(position);
			}

			@Override
			public int getCount() {
				return mListViews.size();
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}
		}
		public class MyOnPageChangeListener implements OnPageChangeListener {

			int one = offset * 2 + bmpW;// 页卡1 -> 页卡2 偏移量
			int two = one * 2;// 页卡1 -> 页卡3 偏移量

			public void onPageScrollStateChanged(int arg0) {}

			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			public void onPageSelected(int index) {
				Animation animation = new TranslateAnimation(one * currIndex, one* index, 0, 0);
				fromIndex = currIndex ;
				currIndex = index;
				animation.setFillAfter(true);// True:图片停在动画结束位置
				animation.setDuration(300);
				cursorImage.startAnimation(animation);
			 
				if (index == 0) {
					addType = SNSKUISCHECK;
				}
				if (index == 1) {
					addType = SKUQTY;
				}
				 
				if(index==0){
	  				
					snBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_left_style_press));
					snBar.setTextColor(resources.getColor(R.color.white));
					skudBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_right_style));
					skudBar.setTextColor(resources.getColor(R.color.black));
					
				}else{
	 				
					snBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_left_style));
					snBar.setTextColor(resources.getColor(R.color.black));
					skudBar.setBackgroundDrawable(resources.getDrawable(R.drawable.tab_right_style_press));
					skudBar.setTextColor(resources.getColor(R.color.white));
				}
				
				initAddType();
			}

		}
		
		
		/**
		 * 
		 * @param isNeedAlertNoftify  是否是需要alert 声音 failed的声音
		 * @param alertMessage			//文字提示的
		 * @param speakModel			//speakModel		0.表示的是notify
		 * @param isNeedSpeak			//是否需要speak
		 * @param alertSpeakMessage		//alertSpeakMessage 说话的声音
		 *//*
		private void alertMessage(boolean isNeedAlertNoftify , String alertMessage, int speakModel ,boolean isNeedSpeak ,  String alertSpeakMessage){
			if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(alertPlayer);}
			if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length() > 0){
				if(speakModel == 0){
					speakNotify(alertSpeakMessage);
				}
			}
			if(alertMessage != null){
				UIHelper.showToast(context, alertMessage, Toast.LENGTH_SHORT).show();
			}
		}
		*//**
		 * 
		 * @param isNeedAlertNoftify
		 * @param alertMessage
		 * @param speakModel	0.是Notify 读取所有的英文 ,1.是前面是英文,后面是一个一个读取,2.是前面是英文,后面是按照系统的配置的读取
		 * @param isNeedSpeak	
		 * @param alertSpeakMessage 
		 * @param alertValue
		 *//*
		private void alertMessageSuccess(boolean isNeedAlertNoftify , String alertMessage, int speakModel ,boolean isNeedSpeak ,  String alertSpeakMessage , String alertValue){
			if(isNeedAlertNoftify){MediaPlayerUtil.playMedia(successPlayer);}
			if(isNeedSpeak && alertSpeakMessage != null && alertSpeakMessage.length() > 0){
				if(speakModel == 0){
					speakNotify(alertSpeakMessage);
				}
				if(speakModel == 1){
	 				speakUnIntercept(alertSpeakMessage, alertValue);
				}
				if(speakModel == 2){
					speakAll(alertSpeakMessage, alertValue);
				}
			}
			if(alertMessage != null)
				UIHelper.showToast(context, alertMessage, Toast.LENGTH_SHORT).show();
		}
		*/
}

