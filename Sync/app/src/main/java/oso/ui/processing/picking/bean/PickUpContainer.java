package oso.ui.processing.picking.bean;

public class PickUpContainer {
	
	private long id ;						//主键ID 
	private long location_id ;				//所在的位置
	private long pickup_id ;				//属于那个建货单

	private long container_id ;				//拣到的Container_id
	private int  container_type ;			//BLP,CLP,ILP,TLP
	private long container_type_id	;		//type_id
	
	private long from_container_id ;			//parent_container_id	
	private int  from_container_type ;			//BLP,CLP,ILP,TLP
	private long from_container_type_id	;		//type_id
	
	private String container ;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getContainer_id() {
		return container_id;
	}
	public void setContainer_id(long container_id) {
		this.container_id = container_id;
	}
	public long getLocation_id() {
		return location_id;
	}
	public void setLocation_id(long location_id) {
		this.location_id = location_id;
	}
	public long getPickup_id() {
		return pickup_id;
	}
	public void setPickup_id(long pickup_id) {
		this.pickup_id = pickup_id;
	}
	public int getContainer_type() {
		return container_type;
	}
	public void setContainer_type(int container_type) {
		this.container_type = container_type;
	}
	public long getContainer_type_id() {
		return container_type_id;
	}
	public void setContainer_type_id(long container_type_id) {
		this.container_type_id = container_type_id;
	}
	public long getFrom_container_id() {
		return from_container_id;
	}
	public void setFrom_container_id(long from_container_id) {
		this.from_container_id = from_container_id;
	}
	public int getFrom_container_type() {
		return from_container_type;
	}
	public void setFrom_container_type(int from_container_type) {
		this.from_container_type = from_container_type;
	}
	public long getFrom_container_type_id() {
		return from_container_type_id;
	}
	public void setFrom_container_type_id(long from_container_type_id) {
		this.from_container_type_id = from_container_type_id;
	}
	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	
	
	
	
}
