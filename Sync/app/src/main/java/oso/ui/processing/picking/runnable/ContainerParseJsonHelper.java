package oso.ui.processing.picking.runnable;

import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.processing.picking.bean.LpInfo;
import utility.StringUtil;

public class ContainerParseJsonHelper {

	
	
	public static LpInfo getLpInfoByJsonObject(JSONObject container , long bill_id){

		if (container == null) {
			return null;
		}
		LpInfo lpInfo = new LpInfo();
		
		lpInfo.setBill_id(bill_id);
		lpInfo.setIsReceive(1);
		lpInfo.setLpName(StringUtil.getJsonString(container, "container"));
		lpInfo.setLp(StringUtil.getJsonLong(container, "containerid"));
		lpInfo.setContainerHasSn(StringUtil.getJsonInt(container, "containerhassn"));
		lpInfo.setIsFull(StringUtil.getJsonInt(container, "is_full"));
		lpInfo.setContainerType(StringUtil.getJsonString(container, "containertype") );
		lpInfo.setSubContainerType(StringUtil.getJsonString(container, "subcontainertype"));
		lpInfo.setSubContainerTypeId(StringUtil.getJsonLong(container, "subcontainertypeid"));
		lpInfo.setSubCount(StringUtil.getJsonInt(container, "subcount"));
		lpInfo.setContainerPcId(StringUtil.getJsonLong(container, "containerpcid"));
		lpInfo.setContainerPcName(StringUtil.getJsonString(container, "containerpcname"));
		
		lpInfo.setContainerPcCount(StringUtil.getJsonInt(container,"containerpccount"));
		lpInfo.setContainerTypeId(StringUtil.getJsonLong(container,"type_id"));
		lpInfo.setLotNumber(StringUtil.getJsonString(container,"lot_number")); 
		lpInfo.setTitle(StringUtil.getJsonString(container,"title_name"));
		lpInfo.setTitleId(StringUtil.getJsonLong(container,"title_id"));
		lpInfo.setLocationId(StringUtil.getJsonLong(container,"location_id"));
		lpInfo.setLocationName(StringUtil.getJsonString(container,"location_name"));
		return lpInfo;
	}
	
	public static LpInfo getLpInfo(JSONObject json){

		if (json == null) {
			return null;
		}
		LpInfo lpInfo = new LpInfo();
		try {
			JSONObject lpJson =	json.getJSONArray("container").getJSONObject(0);
			lpInfo.setLpName( StringUtil.getJsonString(lpJson, "container"));
			lpInfo.setLp(StringUtil.getJsonLong(lpJson, "containerid"));
			lpInfo.setContainerHasSn( StringUtil.getJsonInt(lpJson, "containerhassn"));
			lpInfo.setIsFull( StringUtil.getJsonInt(lpJson, "is_full"));
			lpInfo.setContainerType( StringUtil.getJsonString(lpJson, "containertype"));
			lpInfo.setSubContainerType(StringUtil.getJsonString(lpJson, "subcontainertype"));
			lpInfo.setSubContainerTypeId( StringUtil.getJsonLong(lpJson, "subcontainertypeid"));
			lpInfo.setSubCount( StringUtil.getJsonInt(lpJson, "subcount"));
			lpInfo.setContainerPcId( StringUtil.getJsonLong(lpJson, "containerpcid"));
			lpInfo.setContainerPcName(StringUtil.getJsonString(lpJson, "containerpcname")); 
			lpInfo.setContainerPcCount(StringUtil.getJsonInt(lpJson, "containerpccount"));
			lpInfo.setContainerTypeId(StringUtil.getJsonLong(lpJson, "type_id"));
			lpInfo.setLotNumber(StringUtil.getJsonString(lpJson, "lot_number"));   
			lpInfo.setTitle(StringUtil.getJsonString(lpJson, "title_name"));  
			lpInfo.setTitleId(StringUtil.getJsonLong(lpJson, "title_id"));
			lpInfo.setLocationId(StringUtil.getJsonLong(lpJson, "location_id"));
			lpInfo.setParentContainerId(StringUtil.getJsonLong(lpJson, "parent_container"));
			lpInfo.setContainerTypeInt(StringUtil.getJsonInt(lpJson, "container_type"));
			lpInfo.setLocationName(StringUtil.getJsonString(lpJson,"location_name"));
		} catch (JSONException e) {
 			e.printStackTrace();
		}
 		
	
		return lpInfo;
	
	}
}
