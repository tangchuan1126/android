package oso.ui.processing.picking.db;

import java.util.ArrayList;
import java.util.List;

import oso.ui.processing.picking.bean.PickUpContainer;
import oso.ui.processing.picking.bean.PickUpScanInfo;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import support.dbhelper.DatabaseHelper;
import support.exception.SystemException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class PickUpScanInfoDao {

	private DatabaseHelper helper;
	
	private SQLiteDatabase db;
	
	private String tableName = DatabaseHelper.tbl_pickup_scan_info;
	
	
	public PickUpScanInfoDao(Context context) {
		helper = new DatabaseHelper(context);
	}
	public PickUpScanInfoDao(Context context , String tableName) {
		helper = new DatabaseHelper(context);
		this.tableName = tableName;
	}
	

	 
	 
	
	 
	 
	 
 
	public void deletePickedProduct(long location_id , long pickup_id){
		db = helper.getWritableDatabase();
		db.delete(tableName, " location_id=? and pickup_id=?  ", new String[]{location_id+"",pickup_id+""});
		db.close();
	}
	 
  
	 
	public double getDifferentFromOtherLocation(long pcid , long old_location_id ,long pickup_id ){
		db = helper.getReadableDatabase();
		double qty =  0.0d ;
		Cursor cursor = db.rawQuery("select sum(qty) as count_sum from " + tableName + " where from_location_id = ? and pickup_id = ? and sku = ? ",
				new String[]{old_location_id+"", pickup_id+"" , pcid+""});
		if(cursor != null && cursor.getCount() > 0){
			cursor.moveToNext();
			qty =	cursor.getDouble(cursor.getColumnIndex("count_sum"));
		}
		cursor.close();
		db.close() ;
 
		return qty ;		
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	/**
	 * 添加扫描的数据到数据库中
	 * @param arrayList
	 */
	public void addPickupScanInfo(PickUpContainer pickUpContainer ,List<PickUpScanInfo> arrayList){
		try{
			db = helper.getWritableDatabase() ;
			db.beginTransaction();
			if(arrayList != null && arrayList.size() > 0 ){
			
				for(PickUpScanInfo pickUpScanInfo : arrayList){
					
					
					ContentValues values = new ContentValues();
					values.put("sn", pickUpScanInfo.getSn());
					values.put("sku", pickUpScanInfo.getSku());
					values.put("qty", pickUpScanInfo.getQty());
					values.put("location_id", pickUpScanInfo.getLocation_id());
					values.put("pickup_id", pickUpScanInfo.getPickup_id());
					values.put("isSubmit", pickUpScanInfo.getIsSubmit());
					values.put("barCodeType", pickUpScanInfo.getCode_type());
					
					values.put("pname", pickUpScanInfo.getPname()); 
					values.put("barCode", pickUpScanInfo.getBarCode());
					values.put("from_container_id", pickUpScanInfo.getFrom_container_id());
					values.put("from_container_type", pickUpScanInfo.getFrom_container_type());
					values.put("from_container_type_id", pickUpScanInfo.getFrom_container_type_id());

					
					values.put("pickup_container_id", pickUpScanInfo.getPickup_container_id());
					values.put("pickup_container_type", pickUpScanInfo.getPickup_container_type());
					values.put("pickup_container_type_id", pickUpScanInfo.getPickup_container_type_id());
 					
					
					db.insert(tableName, null, values);
				}
				if(pickUpContainer != null && pickUpContainer.getContainer_id() != 0l){
					insertPickupContainer(pickUpContainer,db);
				}
				db.setTransactionSuccessful();
			}
		}catch(Exception e){
			throw new SystemException("pickupScanInfoAdd error");
		}finally{
			db.endTransaction();
			if(db != null && db.isOpen()){db.close();}
		}
	}
 
	private void insertPickupContainer(PickUpContainer pickUpContainer , SQLiteDatabase db ){
		
		ContentValues values = new ContentValues();
 
		
		
		values.put("location_id", pickUpContainer.getLocation_id());
		values.put("pickup_id", pickUpContainer.getPickup_id());
		values.put("container_id", pickUpContainer.getContainer_id());
		values.put("container_type", pickUpContainer.getContainer_type());
		values.put("container_type_id", pickUpContainer.getContainer_type_id());
		values.put("container", pickUpContainer.getContainer());
		
		values.put("from_container_id", pickUpContainer.getFrom_container_id());
		values.put("from_container_type", pickUpContainer.getFrom_container_type());
		values.put("from_container_type_id", pickUpContainer.getFrom_container_type_id());
	 
		db.insert(DatabaseHelper.tbl_pickup_container_info, null, values );
		
	}

	 
	public double getPickUpProductTotalQty(int container_type , long container_type_id , long pickup_id , long location_id ,long sku){
		double totalQty = 0.0d ;
		StringBuffer sql = new StringBuffer();
 		sql.append(" select sum(psi.qty) as total_sum   ")
		.append("  from pickup_scan_info psi ")
		.append(" left join pickup_container_info as pci on psi.pickup_container_id = pci.container_id ")
		.append(" where pci.container_type = ? ")
		.append("    and pci.container_type_id = ?")
		.append("	 and pci.pickup_id =? ")
		.append("    and pci.location_id=? ")
		.append("    and psi.sku = ? ");
 		Cursor cursor = null ;
 		try{
 			db = helper.getReadableDatabase() ;
 			cursor = db.rawQuery(sql.toString(), new String[]{
 					container_type+"",
 					container_type_id+"",
 					pickup_id+"",
 					location_id+"",
 					sku+""
 			});
 			if(cursor.getCount() > 0 ){
 				cursor.moveToNext() ;
 				totalQty =  cursor.getDouble(cursor.getColumnIndex("total_sum"));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
  		}finally{
 			if(db != null && db.isOpen()){db.close();}
 			if(cursor != null && !cursor.isClosed()){cursor.close();}
 		}
 		return totalQty;
	}
	public List<PickUpScanInfo> getPickUpScanInfo(int container_type , long container_type_id , long pickup_id , long location_id ){
 		StringBuffer sql = new StringBuffer();
 		sql.append(" select psi.* ,pci.container  ")
		.append("  from pickup_scan_info psi ")
		.append(" left join pickup_container_info as pci on psi.pickup_container_id = pci.container_id ")
		.append(" where pci.container_type = ? ")
		.append("    and pci.container_type_id = ?")
		.append("	 and pci.pickup_id =? ")
		.append("    and pci.location_id=? order by id desc ");
 
 		
		List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>();
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase() ;
			cursor = db.rawQuery(sql.toString(), new String[]{
					container_type+"",
					container_type_id+"",
					pickup_id+"",
					location_id+"",
			});
			arrayList = handCursor(cursor);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return arrayList;
	}
	private List<PickUpScanInfo> handCursor(Cursor cursor ){
		 List<PickUpScanInfo>  result = new ArrayList<PickUpScanInfo>();
		if(cursor != null && cursor.getCount() > 0){
			while(!cursor.isLast()){
				cursor.moveToNext();
				PickUpScanInfo pickUpScanInfo = new PickUpScanInfo();
				pickUpScanInfo.setBarCode(cursor.getString(cursor.getColumnIndex("barCode")));
				pickUpScanInfo.setCode_type(cursor.getString(cursor.getColumnIndex("barCodeType")));
 				pickUpScanInfo.setFrom_container_id(cursor.getLong(cursor.getColumnIndex("from_container_id")));
 				pickUpScanInfo.setFrom_container_type(cursor.getInt(cursor.getColumnIndex("from_container_type")));
 				pickUpScanInfo.setFrom_container_type_id(cursor.getLong(cursor.getColumnIndex("from_container_type_id")));
 				pickUpScanInfo.setFrom_location_id(cursor.getLong(cursor.getColumnIndex("from_location_id")));	//
 				pickUpScanInfo.setId(cursor.getInt(cursor.getColumnIndex("id")));
 				pickUpScanInfo.setIsSubmit(cursor.getInt(cursor.getColumnIndex("isSubmit")));
 				pickUpScanInfo.setLocation_id(cursor.getLong(cursor.getColumnIndex("location_id")));
 				pickUpScanInfo.setPickup_container_id(cursor.getLong(cursor.getColumnIndex("pickup_container_id")));
 				pickUpScanInfo.setPickup_container_type(cursor.getInt(cursor.getColumnIndex("pickup_container_type")));
 				pickUpScanInfo.setPickup_container_type_id(cursor.getLong(cursor.getColumnIndex("pickup_container_type_id")));
 				pickUpScanInfo.setPickup_id(cursor.getLong(cursor.getColumnIndex("pickup_id")));
 				pickUpScanInfo.setPname(cursor.getString(cursor.getColumnIndex("pname")));
 				pickUpScanInfo.setQty(cursor.getDouble(cursor.getColumnIndex("qty")));
 				pickUpScanInfo.setSku(cursor.getLong(cursor.getColumnIndex("sku")));
 				pickUpScanInfo.setSn(cursor.getString(cursor.getColumnIndex("sn")));
 				int containerIndex = cursor.getColumnIndex("container") ;
 				if( containerIndex > 0 ){
 					pickUpScanInfo.setContainer(cursor.getString(containerIndex)); 
 				}
 				result.add(pickUpScanInfo);
			}
		}
		return result;
	}
	public int countSnInScanInfo(String sn , long pickup_id , long location_id){
		int count = 0 ;
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase();
			cursor = db.rawQuery("select count(*) as count_sum from " + tableName + " where pickup_id= ? and location_id = ? and sn= ? ", new String[]{pickup_id+"",location_id+"",sn});
		}catch(Exception e){}
		finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return count ;
	}
	
	
	/**
	 * 
		SELECT total_need.* , pickup.* , pi.name FROM
		 (SELECT
			pcid,
			sum(qty) AS need_pickup
		FROM
			pickup_location_product
		WHERE
			location_id = 100126
		AND pickup_id = 2
		AND from_con_id = 10002
		AND from_container_type = 2
		AND pick_container_Type = 0
		AND pick_container_Type_id = 0
		GROUP BY 
			pcid
		) AS total_need LEFT JOIN 
		(SELECT
			sku,
			sum(qty) AS total_pickup
		FROM
			pickup_scan_info
		WHERE
			location_id = 100126
		AND pickup_id = 2
		AND from_container_id = 10002
		AND from_container_type = 2
		AND pickup_container_type = 0
		AND pickup_container_id = 0
		GROUP BY
		sku
		) AS pickup ON pickup.sku = total_need.pcid
		LEFT JOIN pickup_product_info AS pi ON pi.pid = pcid
 
	 * @param pickup_id
	 * @param location_id
	 * @param container_id
	 * @return
	 */
	public List<ProductCompareSimple> getContainerPickupProductList(long pickup_id , long location_id , long from_container_id  ){
		List<ProductCompareSimple> result = new ArrayList<ProductCompareSimple>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT total_need.* , pickup.* , pi.name FROM ");
		sql.append(" (SELECT ")
			.append("	pcid,")
			.append("	sum(qty) AS need_pickup ")
			.append(" FROM ")
			.append(" pickup_location_product")
			.append(" WHERE location_id =? ")
			.append(" AND pickup_id =? ")
			.append(" AND from_con_id = ? ")
 			.append(" AND pick_container_Type = 0 ")
			.append(" AND pick_container_Type_id = 0 ")
			.append(" GROUP BY pcid ) AS total_need ")
			.append(" LEFT JOIN  ")
			.append(" (SELECT sku,sum(qty) AS total_pickup")
			.append(" FROM pickup_scan_info WHERE ")
			.append(" location_id = ? ")
			.append(" AND pickup_id =? ")
			.append(" AND from_container_id = ? ")
			.append(" AND pickup_container_type = 0")
			.append(" AND pickup_container_id = 0")
			.append(" GROUP BY	sku ) AS pickup ON pickup.sku = total_need.pcid ")
			.append(" LEFT JOIN pickup_product_info AS pi ON pi.pid = pcid ") ;
 		
	 Cursor cursor   = null ;	
	 try{
		 db = helper.getReadableDatabase() ;
		 cursor = db.rawQuery(sql.toString(), new String[]{
				location_id+"",
				pickup_id+"",
				from_container_id+"",
				location_id+"",
				pickup_id+"" ,
				from_container_id+"",
			});
		 if(cursor != null && cursor.getCount() > 0 ){
			 while(!cursor.isLast()){
				 ProductCompareSimple needProductSimple = new ProductCompareSimple();
				 cursor.moveToNext() ;
				  
				 needProductSimple.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
				 needProductSimple.setPickupQty(cursor.getDouble(cursor.getColumnIndex("total_pickup")));
				 needProductSimple.setpName(cursor.getString(cursor.getColumnIndex("name")));
				 needProductSimple.setTotalQty(cursor.getDouble(cursor.getColumnIndex("need_pickup")));
				 result.add(needProductSimple);
			 }
		 }
	 }catch(Exception e){
		 e.printStackTrace();
	 }finally{
		 if(db != null && db.isOpen()){db.close();}
		 if(cursor != null && !cursor.isClosed()){cursor.close();}}
	 return result ;
	}
	public List<ProductCompareSimple> getOnContainerTypePickupProductList(long pickup_id,long  location_id,int from_container_type,long from_container_type_id  ){

		List<ProductCompareSimple> result = new ArrayList<ProductCompareSimple>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT total_need.* , pickup.* , pi.name FROM ");
		sql.append(" (SELECT ")
			.append("	pcid,")
			.append("	sum(qty) AS need_pickup ")
			.append(" FROM ")
			.append(" pickup_location_product")
			.append(" WHERE location_id =? ")
			.append(" AND pickup_id =? ")
			.append(" AND from_container_type = ? ")
			.append(" AND from_container_type_id = ? ")
 			.append(" AND pick_container_Type = 0 ")
			.append(" AND pick_container_Type_id = 0 ")
			.append(" GROUP BY pcid ) AS total_need ")
			.append(" LEFT JOIN  ")
			.append(" (SELECT sku,sum(qty) AS total_pickup")
			.append(" FROM pickup_scan_info WHERE ")
			.append(" location_id = ? ")
			.append(" AND pickup_id =? ")
			.append(" AND from_container_type = ? ")
			.append(" AND from_container_type_id = ? ")
			.append(" AND pickup_container_type = 0")
			.append(" AND pickup_container_id = 0")
			.append(" GROUP BY	sku ) AS pickup ON pickup.sku = total_need.pcid ")
			.append(" LEFT JOIN pickup_product_info AS pi ON pi.pid = pcid ") ;
 		
	 Cursor cursor   = null ;	
	 try{
		 db = helper.getReadableDatabase() ;
		 cursor = db.rawQuery(sql.toString(), new String[]{
				location_id+"",
				pickup_id+"",
				from_container_type+"",
				from_container_type_id+"",
				location_id+"",
				pickup_id+"",
				from_container_type+"",
				from_container_type_id+"",
			});
		 if(cursor != null && cursor.getCount() > 0 ){
			 while(!cursor.isLast()){
				 ProductCompareSimple needProductSimple = new ProductCompareSimple();
				 cursor.moveToNext() ;
				  
				 needProductSimple.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
				 needProductSimple.setPickupQty(cursor.getDouble(cursor.getColumnIndex("total_pickup")));
				 needProductSimple.setpName(cursor.getString(cursor.getColumnIndex("name")));
				 needProductSimple.setTotalQty(cursor.getDouble(cursor.getColumnIndex("need_pickup")));
				 result.add(needProductSimple);
			 }
		 }
	 }catch(Exception e){
		 e.printStackTrace();
	 }finally{
		 if(db != null && db.isOpen()){db.close();}
		 if(cursor != null && !cursor.isClosed()){cursor.close();}}
	 return result ;
	
	}
 	public List<PickUpScanInfo> getContainerPickUpProductScanInfo(long pickup_id , long from_container_id , long location_id ){
		Cursor cursor = null ;
		List<PickUpScanInfo>  result = null ;
		try{
			db = helper.getReadableDatabase() ;
			
			cursor = db.rawQuery("select * from pickup_scan_info where from_container_id = ? and pickup_container_type = 0  and pickup_id =? and location_id =? order by id desc ", new String[]{
						from_container_id+"",
						pickup_id+"",
						location_id+""
					});
			result = handCursor(cursor);
		}catch(Exception e){
			 e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return result ;
	}
	/**
	 * SELECT
		total.* , picked.* ,ppi.name
		FROM
			(
				SELECT
					pcid,
					sum(qty) AS total_qty
				FROM
					pickup_location_product
				WHERE
					location_id = 100126
				AND pickup_id = 2
				GROUP BY
					pcid
			) AS total
		LEFT JOIN (
			SELECT
				sku,
				sum(qty) AS pick_qty
			FROM
				pickup_scan_info
			WHERE
				pickup_id = 2
			AND location_id = 100126
			GROUP BY
				sku
		) AS picked ON picked.sku = total.pcid  
		LEFT JOIN pickup_product_info as ppi on ppi.pid = total.pcid
	 * @param pickup_id
	 * @param location_id
	 * @return
	 * 
	 * 查询一个Location上面的拣货的差异
	 */
	public List<ProductCompareSimple> getLocationPickupInfo(long pickup_id , long location_id){
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ")
		.append(" total.* , picked.* ,ppi.name ") 
		.append(" FROM")
		.append(" (")
		.append(" SELECT")
		.append(" pcid,")
		.append(" sum(qty) AS total_qty ")
		.append(" FROM ")
		.append(" pickup_location_product ")
		.append(" WHERE")
		.append(" location_id = ?")
		.append(" AND pickup_id =? ")
		.append(" GROUP BY pcid ) AS total ")	
		.append(" LEFT JOIN ")
		.append(" ( SELECT sku, sum(qty) AS pick_qty")
		.append(" FROM ")
		.append(" pickup_scan_info ")
		.append(" WHERE pickup_id =? AND location_id =? GROUP BY sku ")
		.append(" ) AS picked ON picked.sku = total.pcid  ")	
		.append(" LEFT JOIN pickup_product_info as ppi on ppi.pid = total.pcid");
		Cursor cursor = null ;
		List<ProductCompareSimple> result = new ArrayList<ProductCompareSimple>();
		try{
			db = helper.getReadableDatabase() ;
			cursor = db.rawQuery(sql.toString(), new String[]{location_id+"",pickup_id+"" , pickup_id+"" , location_id+""});
			if(cursor != null && cursor.getCount() > 0 ){
				 while(!cursor.isLast()){
					 ProductCompareSimple needProductSimple = new ProductCompareSimple();
					 cursor.moveToNext() ;
					  
					 needProductSimple.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
					 needProductSimple.setPickupQty(cursor.getDouble(cursor.getColumnIndex("pick_qty")));
					 needProductSimple.setpName(cursor.getString(cursor.getColumnIndex("name")));
					 needProductSimple.setTotalQty(cursor.getDouble(cursor.getColumnIndex("total_qty")));
					 result.add(needProductSimple);
				 }
			 }
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}					
		return result ;
	}
	/**
	 * 查询这个位置没有提交的数据的条数
	 * @param pickup_id
	 * @param location_id
	 * @return
	 */
	public int countPickupScanInfoNotSubmit(long pickup_id , long location_id){
		int count =  0 ;
		Cursor cursor = null ;
		try{
			db = helper.getReadableDatabase() ;
			String sql = "select count(*) as sum_count from " + tableName + " where pickup_id = ? and location_id = ? " ;
			cursor = db.rawQuery(sql, new String[]{pickup_id+"",location_id+""});
			if(cursor != null && cursor.getCount() > 0 ){
				cursor.moveToNext() ;
				count = cursor.getInt(cursor.getColumnIndex("sum_count"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return count ;
	}
	public List<PickUpScanInfo> queryPickUpScanInfo(long pickup_id , long location_id , int isSubmit ){
		List<PickUpScanInfo> arrayList = new ArrayList<PickUpScanInfo>();
		Cursor cursor  = null ;
		try{
		db = helper.getReadableDatabase() ;
	    cursor  = db.rawQuery("select * from " + tableName + " where pickup_id = ? and location_id=? and isSubmit = ? order by id desc ",
					new String[]{pickup_id+"",location_id+"",isSubmit+""});
			arrayList = handCursor(cursor) ;
		}catch(Exception e){
			
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		 
		return arrayList ;
	}
	public List<PickUpScanInfo> getContainerTypePickUpProductScanInfo(long pickup_id , int from_container_type ,long from_container_type_id, long location_id ){
		Cursor cursor = null ;
		List<PickUpScanInfo>  result = null ;
		try{
			db = helper.getReadableDatabase() ;
			
			cursor = db.rawQuery("select * from pickup_scan_info where from_container_type = ? and from_container_type_id =?  and pickup_id =? and location_id =? order by id desc ", new String[]{
					    from_container_type+"",
					    from_container_type_id+"",
					    pickup_id+"",
					    location_id+""
					});
			result = handCursor(cursor);
		}catch(Exception e){
			 e.printStackTrace();
		}finally{
			if(db != null && db.isOpen()){db.close();}
			if(cursor != null && !cursor.isClosed()){cursor.close();}
		}
		return result ;
	}
	//查询某种大类型的Container上面拣货了多少个
	public List<ProductCompareSimple> getContainerPickupProductListNoTypeId(long pickup_id , long location_id , long from_container_type  ){
		List<ProductCompareSimple> result = new ArrayList<ProductCompareSimple>();
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT total_need.* , pickup.* , pi.name FROM ");
		sql.append(" (SELECT ")
			.append("	pcid,")
			.append("	sum(qty) AS need_pickup ")
			.append(" FROM ")
			.append(" pickup_location_product")
			.append(" WHERE location_id =? ")
			.append(" AND pickup_id =? ")
 			.append(" AND pick_container_Type = 0 ")
			.append(" AND pick_container_Type_id = 0 ")
			.append(" GROUP BY pcid ) AS total_need ")
			.append(" LEFT JOIN  ")
			.append(" (SELECT sku,sum(qty) AS total_pickup")
			.append(" FROM pickup_scan_info WHERE ")
			.append(" location_id = ? ")
			.append(" AND pickup_id =? ")
			.append(" AND from_container_type = ? ")
			.append(" AND pickup_container_type = 0")
			.append(" AND pickup_container_id = 0 ")
			.append(" GROUP BY	sku ) AS pickup ON pickup.sku = total_need.pcid ")
			.append(" LEFT JOIN pickup_product_info AS pi ON pi.pid = pcid ") ;
 		
	 Cursor cursor   = null ;	
	 try{
		 db = helper.getReadableDatabase() ;
		 cursor = db.rawQuery(sql.toString(), new String[]{
				location_id+"",
				pickup_id+"",
 				location_id+"",
				pickup_id+"" ,
				from_container_type+"",
			});
		 if(cursor != null && cursor.getCount() > 0 ){
			 while(!cursor.isLast()){
				 ProductCompareSimple needProductSimple = new ProductCompareSimple();
				 cursor.moveToNext() ;
				  
				 needProductSimple.setPcid(cursor.getLong(cursor.getColumnIndex("pcid")));
				 needProductSimple.setPickupQty(cursor.getDouble(cursor.getColumnIndex("total_pickup")));
				 needProductSimple.setpName(cursor.getString(cursor.getColumnIndex("name")));
				 needProductSimple.setTotalQty(cursor.getDouble(cursor.getColumnIndex("need_pickup")));
				 result.add(needProductSimple);
			 }
		 }
	 }catch(Exception e){
		 e.printStackTrace();
	 }finally{
		 if(db != null && db.isOpen()){db.close();}
		 if(cursor != null && !cursor.isClosed()){cursor.close();}}
	 return result ;
	}
	
}
