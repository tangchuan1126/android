package oso.ui.processing;

import oso.base.BaseActivity;
import oso.ui.processing.specialproject.WMS_Special_ProjectActivity;
import oso.ui.processing.picking.PickUpSelectActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import declare.com.vvme.R;

public class ProcessActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_process_main, 0);
		setTitleString("Processing");
		initView();
	}

	private void initView() {
		findViewById(R.id.btn_picking).setOnClickListener(this);
		findViewById(R.id.btn_special_task).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_picking:		//picking
			popTo(PickUpSelectActivity.class);
			break;
		case R.id.btn_special_task: // Special Task
			popTo(WMS_Special_ProjectActivity.class);
			break;
		}
	}
	
	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
