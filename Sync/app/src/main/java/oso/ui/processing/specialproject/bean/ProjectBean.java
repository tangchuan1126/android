package oso.ui.processing.specialproject.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utility.StringUtil;

/**
 * WMS Special Project Project数据Bean
 * @author xialimin
 *
 */
public class ProjectBean implements Serializable {
	private static final long serialVersionUID = -2343407485814184047L;
	
	public List<Project> projectList = new ArrayList<Project>();
	
	public ProjectBean (JSONArray json) {
		if(!StringUtil.isNullForJSONArray(json)) {
			parseJson(json);
		}
	}
	private void parseJson(JSONArray json) {
		
		for(int i=0; i<json.length(); i++) {
			try {
				JSONObject jobj = json.getJSONObject(i);
				Project project = new Project();
				
				project.description = jobj.optString("description");
				project.start_time = jobj.optString("start_time");
				project.end_time = jobj.optString("end_time");
				
				project.subject = jobj.optString("subject");
				project.exeemployname = jobj.optString("exeemployname");
				project.fromname = jobj.optString("fromname");
				
				project.LR_NO = jobj.optString("lr_no");
				project.LR_TYPE = jobj.optInt("lr_type");
				project.schedule_execute_id = jobj.optString("schedule_execute_id");
				project.schedule_id = jobj.optString("schedule_id");

				project.warehouse = jobj.optString("warehouse");
				
				projectList.add(project);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public class Project implements Serializable {
		public String description; // description 描述
		public String start_time; // 开始时间
		public String end_time; // 结束时间
		
		public String subject; // 标题
		public String exeemployname; // 执行人
		public String fromname; // Manager Name
		
		public String LR_NO; // Load Receive ID
		public int LR_TYPE; // type
		public String schedule_execute_id; // 执行人ID

		public String schedule_id; // Project ID
		public String warehouse;
		public String ps_id;
	}
}
