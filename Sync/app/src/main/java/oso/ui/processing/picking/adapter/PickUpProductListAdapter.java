package oso.ui.processing.picking.adapter;

import java.util.List;

import oso.ui.processing.picking.bean.ProductCompareSimple;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpProductListAdapter extends  BaseAdapter{

	private List<ProductCompareSimple> arrayList ;
	
	private LayoutInflater inflater;
	
	private Context context;
	
	
	
	
	public PickUpProductListAdapter(List<ProductCompareSimple> arrayList,
			Context context) {
		super();
		this.arrayList = arrayList;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0 ;
	}

	@Override
	public ProductCompareSimple getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.i("pickup List position ", position+"");
		PickUpProductListAdapterHolder viewHolder = null ;
		if(convertView==null){
			viewHolder = new PickUpProductListAdapterHolder();
			convertView = inflater.inflate(R.layout.pickup_location_product_listview,null);
			viewHolder.pcid = (TextView)convertView.findViewById(R.id.pickup_location_pcid);
			viewHolder.pName = (TextView)convertView.findViewById(R.id.pickup_location_pname);
			viewHolder.totalQty = (TextView)convertView.findViewById(R.id.pick_up_total_qty);
			viewHolder.pickUpQty = (TextView)convertView.findViewById(R.id.pcik_up_qty);

   			convertView.setTag(viewHolder);
		}else{
			viewHolder=(PickUpProductListAdapterHolder) convertView.getTag();
		}
		ProductCompareSimple  temp = (ProductCompareSimple) getItem(position) ;
		viewHolder.pcid.setText(temp.getPcid()+"");
		viewHolder.pName.setText(temp.getpName());
		viewHolder.totalQty.setText(temp.getTotalQty()+"");
		viewHolder.pickUpQty.setText(temp.getPickupQty()+"");
		int iqty=(int) temp.getPickupQty();
		int itqty=(int) temp.getTotalQty();
		if(iqty==itqty&&itqty!=0){
			convertView.setBackgroundResource(R.drawable.tablepeizhi_hui);
		}
		 
 		return convertView;
	}

}
class PickUpProductListAdapterHolder{
	
	public TextView pcid;
	public TextView pName ;
	public TextView totalQty;
	public TextView pickUpQty ;
 	
}
