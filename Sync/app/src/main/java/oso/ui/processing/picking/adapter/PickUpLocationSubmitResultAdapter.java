package oso.ui.processing.picking.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class PickUpLocationSubmitResultAdapter extends BaseAdapter{

	private List<String> locations ;
	private List<String> results ;
	private LayoutInflater inflater;
	
	private Context context;
	
	public PickUpLocationSubmitResultAdapter(List<String>  locations,List<String> results ,Context context) {
		super();
		this.locations = locations;
		this.results = results;
 		this.context = context;
 		this.inflater = LayoutInflater.from(context);
 	}
	
	@Override
	public int getCount() {
 		return locations == null ? 0 : locations.size();
	}

	@Override
	public Object getItem(int position) {
 		return null;
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}
	//110904927

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		convertView=inflater.inflate(R.layout.pickup_location_submit_result_list_view,null);
		((TextView)convertView.findViewById(R.id.pickup_location_submit_fix)).setText(locations.get(position));
		((TextView)convertView.findViewById(R.id.pickup_location_submit_result_fix)).setText(results.get(position));
 		return convertView;
		 
	}

}
