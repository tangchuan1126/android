package oso.ui.processing.specialproject;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseFramentActivity;
import oso.ui.processing.specialproject.adapter.SpecialProLaborAdp;
import oso.ui.processing.specialproject.bean.SpecialLaborTask;
import oso.ui.processing.specialproject.fragment.LaborActiveFragment;
import oso.ui.processing.specialproject.fragment.LaborCompletedFragment;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import oso.widget.VpWithTab;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.anim.AnimUtils;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 
 * @description: Sepcial Project 
 *
 * @author xialimin
 * @time : 2015-6-16 下午3:33:18
 */
public class WMS_Special_LaborTasksActivity extends BaseFramentActivity implements View.OnClickListener {

	private static final int INDEX_ACTIVE = 1;

	private static final int INDEX_PENDING = 2;

	private static final int INDEX_COMPLETED = 3;

	private Context mContext;
	
	private ListView mTaskLv;

	private SpecialProLaborAdp mTaskAdp;
	
	private SpecialLaborTask mTasks;
	
	private Button mCompleteTaskBtn;

	private int mPosition, mTop;  // ListView位置状态保存

	private SingleSelectBar mTab;

	private VpWithTab mViewPager;

	private LaborActiveFragment mActiveFragment;

	private LaborCompletedFragment mCompletedFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_special_labortask_list, 0);
		
		initView();
		
		initData();
		
		initListener();
		
	}

	/*@Override
	public void onPush() {
		initData();
	}*/

	private void initView() {
		
		mContext = this;
		setTitleString(getString(R.string.specialtask_labortask_title));

		/*mCompleteTaskBtn = (Button) findViewById(R.id.btnCompleteTask);
		mTaskLv = (ListView) findViewById(R.id.lv_lttask);
		TextView emptyView = (TextView) findViewById(R.id.tvltEmpty);
		mTaskLv.setEmptyView(emptyView);*/


		mTab = (SingleSelectBar) findViewById(R.id.labortask_tab);
		mViewPager = (VpWithTab) findViewById(R.id.vpWithTab);

		mActiveFragment =  (LaborActiveFragment) getSupportFragmentManager().findFragmentById(R.id.frag_labor_active);
		mCompletedFragment =  (LaborCompletedFragment) getSupportFragmentManager().findFragmentById(R.id.frag_labor_completed);

		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>(getString(R.string.special_labor_active), 0));
		list.add(new HoldDoubleValue<String, Integer>(getString(R.string.special_labor_completed), 1));
		mTab.setUserDefineClickItems(list);

		// 初始化vp
		mViewPager.init(mTab);
		mViewPager.setOnVpPageChangeListener(new VpWithTab.OnVpPageChangeListener() {
			@Override
			public void onPageChange(int index) {
				onPageChanged(index);
			}

		});
		mTab.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				mViewPager.setCurrentItem(selectValue.b);
			}
		});
	}

	private void onPageChanged(int index) {
		switch (index) {
			case 0:
				mActiveFragment.setData();
				break;
			case 1:
				mCompletedFragment.setData();
				break;
		}
	}


	/**
	 * 请求数据
	 */
	private void initData() {

		RequestParams params = new RequestParams();
		params.put("key", "getALLSpecialTaskNotice");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				mTasks = new Gson().fromJson(json.toString(), SpecialLaborTask.class);

				if(mTasks == null) {
					return;
				}
				/*mTaskAdp = new SpecialProLaborAdp(mContext, mTasks.notices);
				mTaskLv.setAdapter(mTaskAdp);
				mCompleteTaskBtn.setVisibility(Utility.isNullForList(mTasks.notices) ? View.GONE : View.VISIBLE);*/

				/*if(mTasks == null) {
					mTasks = new Gson().fromJson(json.toString(), SpecialLaborTask.class);
				} else {
					mTasks.notices.clear();
					mTasks.notices.addAll(tasks.notices);
				}*/

				mCompletedFragment.setData();
				mActiveFragment.setData();

			}
		}.doGet(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}
	
	private void initListener() {

		// 刷新列表
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				initData();
			}
		});

		/*findViewById(R.id.btnCompleteTask).setOnClickListener(this);
		mTaskLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				if(mTasks.notices.get(position).task_status == SpecialTaskKey.TYPE_PENDING) {
					UIHelper.showToast(mContext, "Pending!");
					return;
				}

				if(mTasks.notices.get(position).task_status == SpecialTaskKey.TYPE_CLOSED) {
					UIHelper.showToast(mContext, "Closed!");
					return;
				}

				SpecialLaborTask.LaborTask task = (SpecialLaborTask.LaborTask) mTaskLv.getAdapter().getItem(position);
				task.status = !task.status;
				//task.type = (task.status ? SpecialTaskKey.TYPE_INPROCESS : SpecialTaskKey.TYPE_OPEN);
				mTaskAdp.notifyDataSetChanged();
			}
		});
		
		mTaskLv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if(mTasks.notices.get(position).task_status == SpecialTaskKey.TYPE_PENDING) {
					UIHelper.showToast(mContext, "Pending!");
					return false;
				}
				
				showSlcDlg(position);
				return false;
			}
			
		});*/
	}
	
	
	/**
	 * 菜单Dialog 
	 * @param position
	 * 
	 * Edit 和 Delete
	 */
	private void showSlcDlg(final int position) {
		new RewriteBuilderDialog.Builder(WMS_Special_LaborTasksActivity.this).setTitle(getString(R.string.sync_operation))
		.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog,	int which) {
				dialog.dismiss();
			}
			}).setArrowItems(new String[] {"Request Update", "Request Delete", "Request Insert" },
			new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
					onSlcDialogItemClick(position, itemPosition);
				}
			}).show();
	}
	
	/**
	 * Operate Dialog 选择选项后
	 * @param itemPosition
	 */
	private void onSlcDialogItemClick(int pos, int itemPosition) {
		switch (itemPosition) {
		case SpecialTaskKey.OPERATE_EDIT:
			TTS.getInstance().speakAll_withToast("Request Update!");
			showSubmitExceptionDlg(pos);
			break;
		case SpecialTaskKey.OPERATE_DELETE:
			TTS.getInstance().speakAll_withToast("Request Delete!");
			showSubmitExceptionDlg(pos);
			break;
		case SpecialTaskKey.OPERATE_INSERT:
			TTS.getInstance().speakAll_withToast("Request Insert!");
			showSubmitExceptionDlg(pos);
			break;
		
		}					
	}
	
	/**
	 * 提交有异常  做不了的任务 信息
	 * @param pos
	 */
	private void showSubmitExceptionDlg(final int pos) {
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
//		final EditText etSubject = (EditText) view.findViewById(R.id.e_subject);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Detail"); 
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
//			String subjectStr = etSubject.getText().toString();
				String descriptionStr = etDescription.getText().toString();
				if (!isValid(etDescription)) return;
//			
				dlg.dismiss();

				mTasks.notices.get(pos).task_status = SpecialTaskKey.TYPE_PENDING;
				mTaskAdp.notifyDataSetChanged();
				submitException(pos);
			}

		}).show();
	}

	/**
	 * 异常提交
	 * @param pos
	 */
	private void submitException(int pos) {
		
	}
	
	/**
	 * 打开添加任务Dialog
	 */
	private void showAddTaskDialog() {
		View view = View.inflate(mActivity, R.layout.dialog_specialtask_add_layout, null);
//		final EditText etSubject = (EditText) view.findViewById(R.id.e_subject);
		final EditText etDescription = (EditText) view.findViewById(R.id.e_description);
		
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Add Task"); 
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
//			String subjectStr = etSubject.getText().toString();
			String descriptionStr = etDescription.getText().toString();
			if(!isValid(etDescription)) return;
//			submitAddTask(subjectStr, descriptionStr);
			dlg.dismiss();
		}

		}).show();
	}
	
	/**
	 * 判断输入值是否非法
	 * @param tvDes
	 * @return
	 */
	private boolean isValid(TextView tvDes) {

		boolean isValid = true; // 有效的

		if(TextUtils.isEmpty(tvDes.getText().toString())) {
			AnimUtils.horizontalShake(mContext, tvDes);
			isValid = false;
		}

		return isValid;
	}
	
	/**
	 * 请求添加MainTask
	 *//*
	private void submitAddTask(String subjectStr, String descriptionStr) {
		MainTaskBean mainTask = new MainTaskBean();
		mainTask.mainTaskName = subjectStr;
		mainTask.mainTaskDes = descriptionStr;
		mainTask.type = SpecialTaskKey.TYPE_PENDING;
		mTasks.add(mainTask);
		mTaskAdp.notifyDataSetChanged();
	}*/
	

	@Override
	public void onClick(View v) {
		/*switch (v.getId()) {
		case R.id.btnCompleteTask:

			if(mTasks.verifyRequestData()) {
				submitTask();
			} else {
				UIHelper.showToast(mContext, "Submit data is empty!");
			}

			break;

		default:
			break;
		}*/
	}

	/**
	 * 完成任务 提交任务
	 */
	private void submitTask() {

		RequestParams params = new RequestParams();
		params.put("key", "closeSubTask");
		params.put("completeTaskIds", mTasks.getCompleteTaskIds().toString());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				//mTasks.setCompleteStatus();

				mTasks.updateCompleteTask();

				mTaskAdp.notifyDataSetChanged();

				mCompleteTaskBtn.setVisibility(Utility.isNullForList(mTasks.notices) ? View.GONE : View.VISIBLE);
			}
		}.doPost(HttpUrlPath.taskAndInvoiceAction, params, mContext);
	}


	/**
	 * ListView位置状态保存
	 */
	private void saveCurrentPosition() {
		if (mTaskLv != null) {
			int position = mTaskLv.getFirstVisiblePosition();
			View v = mTaskLv.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();
			//保存position和top位置
			mPosition = position;
			mTop = top;
		}
	}

	/**
	 * ListView位置恢复
	 */
	private void restorePosition() {
		if (mTasks != null && mTaskLv != null) {
			int position = mPosition;//取出保存的数据
			int top = mTop;//取出保存的数据
			mTaskLv.setSelectionFromTop(position, top);
		}
	}

	/**
	 * 子任务
	 * @return
	 */
	public List<SpecialLaborTask.LaborTask> getData() {
		if(mTasks == null) {
			return null;
		} else {
			return mTasks.notices;
		}
	}

	public SpecialLaborTask getTaskBean() {
		return mTasks;
	}
}
