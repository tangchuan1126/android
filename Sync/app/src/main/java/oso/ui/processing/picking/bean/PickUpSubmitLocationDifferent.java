package oso.ui.processing.picking.bean;

/**
 * 
 * @author zhangrui 提交差异的时候Bean
 */
public class PickUpSubmitLocationDifferent {

	private long containerTypeId; // containerTypeId ,
									// 所需ContainerTypeId,如果是直接拿货为0
	private int containerType; // containerType 所需ContainerType,如果是直接拿货为0
	private long containerId; // 如果告诉了具体拣货的那个Container，那么应该提交这个数据

	private int containerQty; // 这种Container提交差异的数量,也可能是商品的数量(如果是在直接拣商品的情况)
	private long pcid; // 拣货的PCID

	private int fromContainerType; // from container type
	private long fromContainerTypeId; // from container type id
	private long fromContainerId; // from container id

	private double totalQty; // 这个差异所需商品的总数

	public long getContainerTypeId() {
		return containerTypeId;
	}

	public void setContainerTypeId(long containerTypeId) {
		this.containerTypeId = containerTypeId;
	}

	public int getContainerType() {
		return containerType;
	}

	public void setContainerType(int containerType) {
		this.containerType = containerType;
	}

	public int getContainerQty() {
		return containerQty;
	}

	public void setContainerQty(int containerQty) {
		this.containerQty = containerQty;
	}

	public long getPcid() {
		return pcid;
	}

	public void setPcid(long pcid) {
		this.pcid = pcid;
	}

	public double getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}

	public long getContainerId() {
		return containerId;
	}

	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}

	public int getFromContainerType() {
		return fromContainerType;
	}

	public void setFromContainerType(int fromContainerType) {
		this.fromContainerType = fromContainerType;
	}

	public long getFromContainerTypeId() {
		return fromContainerTypeId;
	}

	public void setFromContainerTypeId(long fromContainerTypeId) {
		this.fromContainerTypeId = fromContainerTypeId;
	}

	public long getFromContainerId() {
		return fromContainerId;
	}

	public void setFromContainerId(long fromContainerId) {
		this.fromContainerId = fromContainerId;
	}
	
}
