package oso.ui.processing.specialproject.key;

import java.util.HashMap;
import java.util.Map;

import android.content.res.Resources;
import android.util.SparseArray;

import declare.com.vvme.R;

/**
 * Special Task Key
 * @author xia
 */
public class SpecialTaskKey {
	/**
	 * 编辑
	 */
	public static final int OPERATE_EDIT = 0;
	
	/**
	 * 删除
	 */
	public static final int OPERATE_DELETE = 1;
	
	/**
	 * 插入
	 */
	public static final int OPERATE_INSERT = 2;

	/**
	 * 关闭子任务
	 */
	public static final int OPERATE_CLOSE_TASK = 2;

	/**
	 * 重新打开已经关闭的子任务
	 */
	public static final int OPERATE_REOPEN_TASK = 3;

	/**
	 * Assign all
	 */
	public static final int MENU_SUBTASK_ASSIGN_ALL = 0;

	/**
	 * Close all
	 */
	public static final int MENU_SUBTASK_CLOSE_ALL = 1;

	/**
	 * add subtask
	 */
	public static final int MENU_SUBTASK_ADD_SUBTASK = 2;

	/**
	 * add subtask when subtasks is empty
	 */
	public static final int MENU_EMPTY_SUBTASK_ADD_SUBTASK = 0;

	/**
	 * Tag 
	 *//*
	public static final int TAG_TEXTVIEW_LABORS = 920610;

	public static final int TAG_TEXTVIEW_HIDE_LABORS = 920611;*/
	
	/**
	 * 任务的三种状态  
	 * Open[New]       : 新任务
	 * Pending   : 添加任务后 需要等待Manager进行确认
	 * Inprocess : 分配任务后，正在执行
	 * CLOSED    : 任务完成
	 *
	 * Assigned  : 任务已经分配(对于Supervisor来说)
	 * Accept    : 任务已接受  (对于Labor来说)
	 * 
	 */
	public static final int TYPE_INPROCESS = 1;

	public static final int TYPE_PENDING = 2;

	public static final int TYPE_CLOSED = 3;

	public static final int TYPE_OPEN = 4;

	public static final int TYPE_ASSIGNED = 5;

	public static final int TYPE_ACCEPTED = 6;
	
	public static final int REQ_MAIN_TO_SUB = 100123;

	public static final int REQ_PROJECT_TO_MAIN = 100124;
	
	final static Map<String, Integer> scannedStatus = new HashMap<String, Integer>();//任务处理级别
	
	static{

		scannedStatus.put(String.valueOf(TYPE_OPEN), R.drawable.ic_cct_unscanned);

		scannedStatus.put(String.valueOf(TYPE_PENDING), R.drawable.ic_cct_prompt);

		scannedStatus.put(String.valueOf(TYPE_INPROCESS), R.drawable.ic_cct_scanned);

		scannedStatus.put(String.valueOf(TYPE_CLOSED), R.drawable.checkbox_pressed);

		scannedStatus.put(String.valueOf(TYPE_ASSIGNED), R.drawable.ic_cct_scanned);

		scannedStatus.put(String.valueOf(TYPE_ACCEPTED), R.drawable.ic_cct_scanned);

	}
	
	public final static SparseArray<String> lrTypeMap = new SparseArray<String>();
	
	static {

		lrTypeMap.put(1, "DN");

		lrTypeMap.put(2, "RN");

		lrTypeMap.put(3, "RDN");

		lrTypeMap.put(4, "PO No.");

		lrTypeMap.put(5, "BOL No.");

		lrTypeMap.put(6, "Container No.");

		lrTypeMap.put(7, "Pro No.");

		lrTypeMap.put(8, "Load No.");

	}
	
	public final static SparseArray<String> processTypeArr = new SparseArray<String>();

	static {

		processTypeArr.put(TYPE_OPEN, "Open");

		processTypeArr.put(TYPE_PENDING, "Pending");

		processTypeArr.put(TYPE_INPROCESS, "In-Processing");

		processTypeArr.put(TYPE_CLOSED, "Closed");

		processTypeArr.put(TYPE_ASSIGNED, "Assigned");

	}

	public final static SparseArray<Integer> processTypeColorArr = new SparseArray<Integer>();

	static {

		processTypeColorArr.put(TYPE_OPEN, Resources.getSystem().getColor(android.R.color.holo_green_dark));

		processTypeColorArr.put(TYPE_PENDING, Resources.getSystem().getColor(android.R.color.holo_orange_light));

		processTypeColorArr.put(TYPE_INPROCESS, Resources.getSystem().getColor(android.R.color.holo_green_dark));

		processTypeColorArr.put(TYPE_CLOSED, Resources.getSystem().getColor(android.R.color.holo_red_dark));

		processTypeColorArr.put(TYPE_ASSIGNED, Resources.getSystem().getColor(android.R.color.holo_green_dark));

	}

	/**
	 * 根据任务类型[int] 获得对一个任务类型文字
	 * @param key
	 * @return
	 */
	public static String getProcessType(int key) {
		return processTypeArr.get(key);
	}

	/**
	 * 根据任务类型[int] 获得对一个任务类型文字颜色
	 * @param key
	 * @return
	 */
	public static int getProcessTypeColor(int key) {
		return processTypeColorArr.get(key);
	}
	
	/**
	 * 获得LR type对应数据
	 * @param key
	 * @return
	 */
	public static String getLRTypeValue(int key) {
		return lrTypeMap.get(key);
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getResource(int type)
	{
		return getResource(type + "");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getResource(String type)
	{
		return scannedStatus.get(type);
	}


}
