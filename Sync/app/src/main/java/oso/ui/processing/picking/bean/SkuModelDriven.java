package oso.ui.processing.picking.bean;

public class SkuModelDriven {
	
	private long pcid;
	private int qty ;
	private String barcode ;
	
	
	
	public SkuModelDriven() {
		super();
	}

	public SkuModelDriven(long pcid, int qty , String barcode) {
		super();
		this.pcid = pcid;
		this.qty = qty;
		this.barcode = barcode ;
	}

	public long getPcid() {
		return pcid;
	}

	public void setPcid(long pcid) {
		this.pcid = pcid;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	 
	
	
}
