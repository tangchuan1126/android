package oso.ui.processing.picking;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import oso.ui.processing.picking.adapter.PickUpProductListAdapter;
import oso.ui.processing.picking.bean.PickUpBean;
import oso.ui.processing.picking.bean.ProductCompareSimple;
import oso.ui.processing.picking.db.PickUpLocationProductDao;
import oso.ui.processing.picking.db.PickUpScanInfoDao;
import oso.ui.processing.picking.parse.PickUpBaseDataParse;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import declare.com.vvme.R;

public class PickUpProductListActivity extends Fragment{

	public static int FROM_PICKUPCONTAINER = 1 ;	//直接拣货Container
	public static int FROM_PICKUPPRODUCT = 2 ;		//在某个具体的位置拣货的
	public static int FROM_CONTAINERTYPEPICKUPPRODUCT = 3 ;		//在某种具体的类型的Container上面拣货
	public static int FROM_CONTAINERONTYPEPCIKUPPRODUCT = 4 ; 	//在某种大类型的Container 上面拣货
	
	
	private Context context ;
	private Resources resources;
	private long pickup_id ;
	private long location_id; 
	private PickUpLocationProductDao locationProductDao ;
	private PickUpScanInfoDao pickUpScanInfoDao ;
	private ListView listView;
 
	private PickUpBean pickUpBean ;
	private List<ProductCompareSimple> datas = new ArrayList<ProductCompareSimple>();
	
	private int from  = 0 ;
	private View view	;	
	private Bundle bundle ;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.pickup_common_layout, container, false);
		
		return view ;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 		initField();
 		showList();
	}
	 
	private void initField(){
		context = getActivity(); 
		resources = context.getResources();
		Bundle bundle = getArguments();
		pickUpBean = (PickUpBean)bundle.getSerializable("pickUpBean");
		pickUpScanInfoDao = new PickUpScanInfoDao(context);
		//遍历出PickupBean中的数据,然后显示出来 每一种商品的个数,有sn的也要显示出来
		listView = (ListView)view.findViewById(R.id.pickup_common_view_list);
		pickup_id = bundle.getLong("pickup_id", 0l);
		location_id = bundle.getLong("location_id", 0l);
		from =bundle.getInt("from", 0);
 	}
	
	private void showList(){
		initData();
		listView.setDivider(null);
		listView.setAdapter(new PickUpProductListAdapter(datas, context));
	/*	LinearLayout.LayoutParams  lp5 =new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT); 
		listView.setLayoutParams(lp5);*/
	}
	private void initData(){

		if(pickUpBean != null){
			if(from == FROM_PICKUPCONTAINER){
				initPickUpContainerData();
			}
			if(from == FROM_PICKUPPRODUCT){
				initPickUpProductData();
			}
			if(from == FROM_CONTAINERTYPEPICKUPPRODUCT){
				initPickUpProductOnTypeData();
			}
			if(from == FROM_CONTAINERONTYPEPCIKUPPRODUCT){
				initPickUpProductOnTypeData();
			}
 			
		}
 
	}
	
	private void initPickUpProductOnTypeData(){
		if(pickUpBean != null){
			//查询在ContainerType上面，一共需要的货物
			datas = pickUpScanInfoDao.getOnContainerTypePickupProductList(pickup_id, location_id, pickUpBean.getFromContainerType(),pickUpBean.getFromContainerTypeId());
		}
	}
	//直接在某个Container上面拣货的时候的数据
	private void initPickUpProductData(){
		//这种表示的是直接是在某个Container上面拣货,jion scanInfo 和 这个Container需要拣货的数据
		if(pickUpBean != null){
			datas = pickUpScanInfoDao.getContainerPickupProductList(pickup_id, location_id, pickUpBean.getFromContainerId());
		}
	}
	
	//直接拣Container的时候的数据
	private void initPickUpContainerData(){
		//这里我查询数据库计算出来,这个商品已经拣货多少个了(应该只有一条数据)
		
			Map<Long, ProductCompareSimple> maps =  PickUpBaseDataParse.getPickupContainerProduct(pickUpBean);
		
			Iterator<Map.Entry<Long, ProductCompareSimple>> it = maps.entrySet().iterator();
			while(it.hasNext()){
				Entry<Long, ProductCompareSimple> entry = it.next();
				ProductCompareSimple value = entry.getValue();
				
				value.setPickupQty(pickUpScanInfoDao.getPickUpProductTotalQty(
						pickUpBean.getPickUpContainerType(), 
						pickUpBean.getPickUpContainerTypeId(),
						pickup_id, 
						location_id, 
						entry.getKey()));
				datas.add(value);
			}
	}
	  
}
