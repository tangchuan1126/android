package oso.ui.processing.specialproject.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.processing.specialproject.key.SpecialTaskKey;
import utility.StringUtil;

import android.text.TextUtils;

public class SubTaskBean implements Serializable  {
	private static final long serialVersionUID = -1666684603532640924L;
	
	public MainDetail main_detail;

	/**
	 * 是否已经全部关闭
	 * @return
	 */
	public boolean isAllAssigned() {
		// 如果全部都是close 也隐藏
		boolean allClosed = true;
		for(int i = 0; i < main_detail.subtasks.size(); i++) {
			if(main_detail.subtasks.get(i).task_status != SpecialTaskKey.TYPE_CLOSED) {
				allClosed = false;
			}
		}
		if(allClosed) {
			return true;
		}

		boolean isAllAssigned = true;
		if(main_detail== null) return true;
		for(int i = 0; i < main_detail.subtasks.size(); i++) {
			if(main_detail.subtasks.get(i).task_status == SpecialTaskKey.TYPE_OPEN) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 子任务是否已经全部关闭
	 * @return
	 */
	public boolean isAllClosed() {
		// 如果全部都是close 也隐藏
		boolean allClosed = true;

		// 如果列表子任务个数为0个，则不为关闭
		if(main_detail.subtasks.size() == 0) {
			return false;
		}

		for(int i = 0; i < main_detail.subtasks.size(); i++) {
			if(main_detail.subtasks.get(i).task_status != SpecialTaskKey.TYPE_CLOSED) {
				allClosed = false;
			}
		}
		return allClosed;
	}

	/**
	 * 是否有状态为OPEN 且 选中的任务。
	 * @return
	 */
	public boolean hasChecked() {

		// 如果列表子任务个数为0个，则不为关闭
		if(main_detail.subtasks.size() == 0) {
			return false;
		}

		for(int i = 0; i < main_detail.subtasks.size(); i++) {
			SubTask subTask = main_detail.subtasks.get(i);
			if(subTask.task_status == SpecialTaskKey.TYPE_OPEN && subTask.status) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 子任务的个数
	 * @return
	 */
	public int getSubtaskCount() {
		if(main_detail == null) {
			return 0;
		}

		return main_detail.subtasks.size();
	}

	public class MainDetail implements Serializable {
		private static final long serialVersionUID = 8653446008559066372L;
		
		public String description;
		public String end_time;
		public String start_time;
		public String task_no;
		
		public List<SubTask> subtasks = new ArrayList<SubTask>();
		
		//------------自定义字段-------------
		public int main_task_status;
	}
	
	public static class SubTask implements Serializable {
		private static final long serialVersionUID = 8945574114627721657L;
		
		public String description;
		public String exeemployname;        // 如果有返回  则代表是由Manager已经进行分配了
		public String schedule_execute_id;  // 如果有返回  则代表是由Manager已经进行分配了
		public String schedule_id;
		public int task_status;
		
		
		//-------------自定义字段----------------
		public String assignLabors;
		public String assignLaborsAdids;
		public boolean status;
		public boolean isUserModified; // 判断默认人员是否修改过
	}
	
	
	
	
	//------------JSON----------------
	/**
	 * 获得Assign任务的JSON数据
	 * @return
	 */
	public JSONArray getAssignSubmitJson() {
		try {
			JSONArray arr = new JSONArray();
			
			List<SubTask> subtasks = main_detail.subtasks;
			int size = subtasks.size();
			for(int i=0; i<size; i++) {
				SubTask subTask = subtasks.get(i);

				if(subTask.isUserModified && TextUtils.isEmpty(subTask.assignLaborsAdids)) {
					continue;
				}

				if(subTask.task_status == SpecialTaskKey.TYPE_OPEN) {
					JSONObject obj = new JSONObject();
					obj.put("schedule_id", subTask.schedule_id);
					
					//------labors-------
					JSONArray laborsArr = new JSONArray();
					if(!TextUtils.isEmpty(subTask.assignLaborsAdids)) {
						String[] labors = subTask.assignLaborsAdids.split(",");
						for(int j=0; j<labors.length; j++) {
							laborsArr.put(labors[j]);
						}
						obj.put("exe_user_id", laborsArr);
					}


					if (TextUtils.isEmpty(subTask.assignLaborsAdids) && !subTask.isUserModified ) {
						if (!TextUtils.isEmpty(subTask.schedule_execute_id)) {
							String[] deflabors = subTask.schedule_execute_id.split(",");
							for(int k=0; k<deflabors.length; k++) {
								laborsArr.put(deflabors[k]);
							}
							obj.put("exe_user_id", laborsArr);
						}
					}
					
					//----默认值----
					JSONArray defLaborsArr = new JSONArray();
					if(!TextUtils.isEmpty(subTask.schedule_execute_id)) {
						String[] deflabors = subTask.schedule_execute_id.split(",");
						for(int k=0; k<deflabors.length; k++) {
							defLaborsArr.put(deflabors[k]);
						}
						obj.put("def_user_ids", defLaborsArr);
					}

					if(!StringUtil.isNullForJSONArray(laborsArr) || !StringUtil.isNullForJSONArray(defLaborsArr)) {
						arr.put(obj);
					}
				}
			}
			return arr;
		} catch (JSONException e) {
			e.printStackTrace();
			return new JSONArray();
		} 
	}
	
	/**
	 * 更新Assign的状态  (选择人员后Assign， 成功提交后需要将刚才已选的任务-人员 列表的状态 置为In-Progressing)
	 */
	public void updateAssignStatus() {
		List<SubTask> subtasks = main_detail.subtasks;
		int size = subtasks.size();
		for(int i=0; i<size; i++) {
			SubTask subTask = subtasks.get(i);
			if(subTask.task_status == SpecialTaskKey.TYPE_OPEN && (!TextUtils.isEmpty(subTask.schedule_execute_id)) || !TextUtils.isEmpty(subTask.assignLaborsAdids)) {
				subTask.task_status = SpecialTaskKey.TYPE_ASSIGNED;
			}
		}
	}

	/**
	 *
	 * @param adids    默认两个参数不为空
	 * @param defAdids
	 * @return
	 */
	public static boolean isUserModified (String adids, String defAdids) {
		String [] adidsArr = adids.split(",");
		String [] defAdidsArr = defAdids.split(",");

		boolean isUserModified = false;
		for (int i = 0; i < adidsArr.length; i++) {
			String adid = adidsArr[i];

			boolean hasEqual = false;
			for (int j = 0; j < defAdidsArr.length; j++) {
				String defAdid = defAdidsArr[j];
				hasEqual = adid.equals(defAdid);
				if (j == defAdidsArr.length - 1 && hasEqual) {
					isUserModified = true;
					return isUserModified;
				}
			}
		}
		return isUserModified;
	}
}
