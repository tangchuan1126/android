package oso.ui.processing.specialproject.adapter;

import java.util.List;

import oso.ui.processing.specialproject.Interface.IMainTask;
import oso.ui.processing.specialproject.bean.MainTaskBean;
import oso.ui.processing.specialproject.bean.MainTaskBean.ChildTask;
import oso.ui.processing.specialproject.bean.MainTaskBean.MainTask;
import oso.ui.processing.specialproject.key.SpecialTaskKey;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 基于时间段的 主任务列表 适配器
 * @author xialimin
 *
 */
public class MainTaskAdp extends BaseExpandableListAdapter {
	
	private Context mContext;
	
	private IMainTask iface;
	
	private MainTaskBean mTaskBean;
	
	public MainTaskAdp (Context ctx, MainTaskBean taskBean, IMainTask face) {
		mContext = ctx;
		iface = face;
		mTaskBean = taskBean;
	}
	
	@Override
	public int getGroupCount() {
		if(mTaskBean == null) return 0;
		if(mTaskBean.project_detail == null) return 0;
		return mTaskBean.project_detail.main_tasks.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		List<MainTask> mainTaskList = mTaskBean.project_detail.main_tasks;
		return mainTaskList.get(groupPosition).child_tasks.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initGroupView(groupPosition, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillGroupData(isExpanded, groupPosition, holder);
		setGroupListener(groupPosition, holder);
		return convertView;
	}
	
	private View initGroupView(int groupPosition, View convertView, ViewHolder holder) {
		convertView = View.inflate(mContext, R.layout.wms_special_project_maintask_group_item, null);
		
		holder.item = convertView.findViewById(R.id.view_special_grp_item);
		holder.tvGrpStartDate = (TextView) convertView.findViewById(R.id.tvMainTaskStartDate);
		holder.tvGrpEndDate = (TextView) convertView.findViewById(R.id.tvMainTaskEndDate);
		return convertView;
	}
	
	private void fillGroupData(boolean isExpanded, int groupPosition, ViewHolder holder) {
		List<MainTask> mainTasks = mTaskBean.project_detail.main_tasks;
		MainTask mainTask = mainTasks.get(groupPosition);
		
		holder.tvGrpStartDate.setText(mainTask.start_time);
		holder.tvGrpEndDate.setText(mainTask.end_time);
		
		// 关闭|无条目时,显示圆背景
		boolean showOne = !isExpanded || mainTask.child_tasks.size() == 0;
		holder.item.setBackgroundResource(showOne ? R.drawable.sh_load_lv_one : R.drawable.sh_load_lv_top);
	}

	private void setGroupListener(int groupPosition, ViewHolder holder) {
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initChildView(groupPosition, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillChildData(groupPosition, childPosition, holder);
		setChildListener(groupPosition, childPosition, holder);
		return convertView;
	}

	private void setChildListener(final int groupPosition, final int childPosition, ViewHolder holder) {
		holder.item.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainTask mainTask = mTaskBean.project_detail.main_tasks.get(groupPosition);
				String schedule_id = mainTask.child_tasks.get(childPosition).schedule_id;
				int main_task_status = mainTask.child_tasks.get(childPosition).task_status;
				
				iface.onChildClick(schedule_id, main_task_status);
			}
		});
	}

	private View initChildView(int groupPosition, View convertView, ViewHolder holder) {
		convertView = View.inflate(mContext, R.layout.wms_special_project_maintask_child_item, null);
		
		holder.item = convertView.findViewById(R.id.item_sp_child);
		holder.tvTaskNo = (TextView) convertView.findViewById(R.id.task_no);
		holder.tvType = (TextView) convertView.findViewById(R.id.maintask_type);
		holder.tvDes = (TextView) convertView.findViewById(R.id.task_description);

		holder.tvClosedCnt = (TextView) convertView.findViewById(R.id.task_close_cnt);
		holder.tvTotalCnt = (TextView) convertView.findViewById(R.id.task_total_cnt);
		holder.ivStatus = (ImageView) convertView.findViewById(R.id.main_task_status);
		
		holder.tvLrType = (TextView) convertView.findViewById(R.id.task_lr_type);
		holder.tvLrNo = (TextView) convertView.findViewById(R.id.task_lr_no);
		return convertView;
	}
	
	private void fillChildData(int groupPosition, int childPosition, ViewHolder holder) {
		List<MainTask> mainTasks = mTaskBean.project_detail.main_tasks;
		ChildTask childTask = mainTasks.get(groupPosition).child_tasks.get(childPosition);
		
		holder.tvTaskNo.setText(childTask.schedule_id+"");
		holder.tvType.setText(SpecialTaskKey.getProcessType(Integer.valueOf(childTask.task_status)));
		holder.tvType.setTextColor(SpecialTaskKey.getProcessTypeColor(Integer.valueOf(childTask.task_status)));
		holder.ivStatus.setImageResource(SpecialTaskKey.getResource(Integer.valueOf(childTask.task_status)));
		holder.tvDes.setText(childTask.description);
		
		if(TextUtils.isEmpty(SpecialTaskKey.getLRTypeValue(mTaskBean.project_detail.lr_type))) {
			holder.tvLrType.setVisibility(View.GONE);
		} else {
			holder.tvLrType.setVisibility(View.VISIBLE);
			holder.tvLrType.setText(SpecialTaskKey.getLRTypeValue(mTaskBean.project_detail.lr_type));
		}
		if(TextUtils.isEmpty(mTaskBean.project_detail.lr_no)) {
			holder.tvLrNo.setVisibility(View.GONE);
		} else {
			holder.tvLrNo.setVisibility(View.VISIBLE);
			holder.tvLrNo.setText(mTaskBean.project_detail.lr_no);
		}
		
		holder.tvClosedCnt.setText(childTask.closedcount+"");
		holder.tvTotalCnt.setText(" " + mContext.getString(R.string.tms_an_slash) + " " + childTask.totalcount + "");

		// 背景
		if (getChildrenCount(groupPosition) - 1 == childPosition) {
			holder.item.setBackgroundResource(R.drawable.sel_special_project_bottom);
		} else {
			holder.item.setBackgroundResource(R.drawable.sel_special_project_mid);
		}
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----打开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }

	
	private static class ViewHolder {
		
		View item;
		//-------Group--------
		TextView tvGrpStartDate, tvGrpEndDate;
		
		//-------Child--------
		
		TextView tvTaskNo, tvSubject, tvDes, tvStartTime, tvEndTime, tvType;
		TextView tvLrType, tvLrNo;
		TextView tvClosedCnt, tvTotalCnt;
		ImageView ivStatus;
	}
	
	/*private Context mContext;
	
	private List<MainTaskBean> mData;
	
	public MainTaskAdp(Context ctx, List<MainTaskBean> tasks) {
		mContext = ctx;
		mData = tasks;
	}

	@Override
	public int getCount() {
		return mData == null ? 0 : mData.size();
	}

	@Override
	public Object getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView  == null) {
			holder = new ViewHolder();
			convertView = initView(convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillData(position, holder);
		
		return convertView;
	}
	
	private View initView(View convertView, ViewHolder holder) {
		
		convertView = View.inflate(mContext, R.layout.special_project_task_layout_item, null);
		
		holder.tvTaskNo = (TextView) convertView.findViewById(R.id.task_no);
		holder.tvType = (TextView) convertView.findViewById(R.id.maintask_type);
//		holder.tvSubject = (TextView) convertView.findViewById(R.id.task_subject);
		
		holder.tvDes = (TextView) convertView.findViewById(R.id.task_description);
		holder.tvEndTime = (TextView) convertView.findViewById(R.id.task_end_time);
		holder.tvStartTime = (TextView) convertView.findViewById(R.id.task_start_time);
		
		holder.tvClosedCnt = (TextView) convertView.findViewById(R.id.task_close_cnt);
		holder.tvTotalTask = (TextView) convertView.findViewById(R.id.task_total_cnt);
		holder.ivStatus = (ImageView) convertView.findViewById(R.id.main_task_status);
		
		return convertView;
	}
	
	*//**
	 * 填充数据
	 * @param position
	 * @param holder
	 *//*
	private void fillData(int position, ViewHolder holder) {
		MainTaskBean task = mData.get(position);
		holder.tvTaskNo.setText(task.mainTaskId + "");
//		holder.tvSubject.setText(task.mainTaskName);
		holder.tvDes.setText(task.mainTaskDes);
		
		holder.tvClosedCnt.setText(task.getSubCloseCnt()+"");
		holder.tvTotalTask.setText(" / " + task.subTask.size());
		
		holder.ivStatus.setBackgroundResource(SpecialTaskKey.getResource(task.type));
		
		//---任务状态
		switch (task.type) {
		case SpecialTaskKey.TYPE_OPEN:
			holder.tvType.setText("Open");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_green));
			break;
		case SpecialTaskKey.TYPE_INPROCESS:
			holder.tvType.setText("In-Process");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_green));
			break;
		case SpecialTaskKey.TYPE_PENDING:
			holder.tvType.setText("Pending");
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_gray_text));
			break;
		case SpecialTaskKey.TYPE_CLOSED:
			holder.tvType.setTextColor(mContext.getResources().getColor(R.color.sync_red));
			holder.tvType.setText("Closed");
			break;
		}
	}

	*//**
	 *  Task No
	 *  Subject
	 *  Description
	 *  Start Time
	 *  End Time
	 *  Type [New, Closed]
	 *//*
	private static class ViewHolder {
		TextView tvTaskNo, tvSubject, tvDes, tvStartTime, tvEndTime, tvType;
		TextView tvClosedCnt, tvTotalTask;
		ImageView ivStatus;
	}*/
}
