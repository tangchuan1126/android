package oso.ui.processing.picking.bean;

import java.io.Serializable;

public class PickUpLoactionProduct implements Serializable{

	private long id ;						 //数据库自增ID	
	private long pcid ;						 //pcid
	private double qty ;					 //数量
	private String location ;				 //location 名字 
	private long slcId;						 //位置的编号slc_id	
	private long pickup_id ;				 // pickup_id  
	private long location_id ;				 //位置的编号slc_id	
	private String sn ; 					 //序列号	
	private String pname ;					 //商品名
	
	private int  from_container_type ;		 //1,2,3,4, BLP TLP ,CLP ,ILP 
	private long from_container_type_id ;	 // 某个容器的类型
	private long from_con_id ;				 //	某个具体的From容器
	
	private int  pick_container_Type ;		 //	1,2,3,4, BLP TLP ,CLP ,ILP 
	private long Pick_container_Type_id; 	 //	容器的类型ID	
	private long pick_con_id	;			 // 需要拣的容器ID
	
	private int pickContainerQty ;	 		//拣container的时候,应该要拣的个数
	
	
	private long fromLocationId ;			//		这两个字段应该是在这个位置上没有找到商品的情况处理的
	private double from_submit_qty ;		 // 
 	
	
	public long getLocation_id() {
		return location_id;
	}
	public void setLocation_id(long location_id) {
		this.location_id = location_id;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public long getPickup_id() {
		return pickup_id;
	}
	public void setPickup_id(long pickup_id) {
		this.pickup_id = pickup_id;
	}
	public long getSlcId() {
		return slcId;
	}
	public void setSlcId(long slcId) {
		this.slcId = slcId;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setPcid(long pcid) {
		this.pcid = pcid;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public long getId() {
		return id;
	}
	public long getPcid() {
		return pcid;
	}
	public double getQty() {
		return qty;
	}
	public String getLocation() {
		return location;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public long getFromLocationId() {
		return fromLocationId;
	}
	public void setFromLocationId(long fromLocationId) {
		this.fromLocationId = fromLocationId;
	}
	public double getFrom_submit_qty() {
		return from_submit_qty;
	}
	public void setFrom_submit_qty(double from_submit_qty) {
		this.from_submit_qty = from_submit_qty;
	}
	public int getFrom_container_type() {
		return from_container_type;
	}
	public void setFrom_container_type(int from_container_type) {
		this.from_container_type = from_container_type;
	}
	public long getFrom_container_type_id() {
		return from_container_type_id;
	}
	public void setFrom_container_type_id(long from_container_type_id) {
		this.from_container_type_id = from_container_type_id;
	}
	public long getFrom_con_id() {
		return from_con_id;
	}
	public void setFrom_con_id(long from_con_id) {
		this.from_con_id = from_con_id;
	}
	public int getPick_container_Type() {
		return pick_container_Type;
	}
	public void setPick_container_Type(int pick_container_Type) {
		this.pick_container_Type = pick_container_Type;
	}
	public long getPick_container_Type_id() {
		return Pick_container_Type_id;
	}
	public void setPick_container_Type_id(long pick_container_Type_id) {
		Pick_container_Type_id = pick_container_Type_id;
	}
	public long getPick_con_id() {
		return pick_con_id;
	}
	public void setPick_con_id(long pick_con_id) {
		this.pick_con_id = pick_con_id;
	}
	public int getPickContainerQty() {
		return pickContainerQty;
	}
	public void setPickContainerQty(int pickContainerQty) {
		this.pickContainerQty = pickContainerQty;
	}
	 
	
	
	 
	
	
}
