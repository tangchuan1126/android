package oso.ui.processing.picking.bean;

import java.io.Serializable;

/**
 * 用于显示每一个pickupBean中需要显示的商品的数量
 * @author zhangrui
 *PickUpNeedProductSimple
 */
public class ProductCompareSimple implements Serializable {
	
	private long pcid ;
	private String pName ;
	private double totalQty ;
	private double pickupQty ;
	private String sn ;
	public long getPcid() {
		return pcid;
	}
	public void setPcid(long pcid) {
		this.pcid = pcid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	public double getPickupQty() {
		return pickupQty;
	}
	public void setPickupQty(double pickupQty) {
		this.pickupQty = pickupQty;
	}
	public void addTotalQty(double addQty){
		this.totalQty += addQty;
	}
	
	

}
