package oso.ui.processing.picking.bean;

public class PickUpSerialProduct {

	private long id ;
	
	private long pcid ;
	
	private String sn ;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPcid() {
		return pcid;
	}
	public void setPcid(long pcid) {
		this.pcid = pcid;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	
	
}
