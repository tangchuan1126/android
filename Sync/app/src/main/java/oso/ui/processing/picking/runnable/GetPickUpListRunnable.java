package oso.ui.processing.picking.runnable;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import oso.ui.processing.picking.bean.PickUp;
import oso.ui.processing.picking.db.PickUpDao;
import oso.ui.processing.picking.parse.HttpResultCode;
import support.dbhelper.Goable;
import support.exception.SystemException;
import support.network.HttpPostMethod;
import support.network.HttpTool;
import utility.FileUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.content.Context;
import android.os.Handler;

/**
 * @author Administrator
 *
 */
public class GetPickUpListRunnable implements Runnable {
	
	private Handler handler ;
	
	private Context context ;
	
	private String fileName = "pickupList.xml";
	
	private PickUpDao pickUpDao ;
	
	
	public GetPickUpListRunnable(Handler handler, Context context) {
		super();
		this.handler = handler;
		this.context = context;
		pickUpDao = new PickUpDao(context);
		Goable.initGoable(context);
	}
	@Override
	public void run() {
		int flag = HttpResultCode.MSG_FAILURE ;
		String postXml = HttpTool.getPostXmlString("", HttpPostMethod.DownLoadOutBill, null);
		try{
			
			InputStream inputStream = HttpTool.getStreamAndPostXML(HttpUrlPath.PickUp, HttpTool.POST, postXml);
			FileUtil.saveFileNoSdCard(fileName, inputStream, context);
 
			flag = handXml();
		 
		}catch (Exception e) {
			e.printStackTrace();
 		}
		handler.obtainMessage(flag).sendToTarget();
		
	}
	private int handXml() throws Exception{
		int flag = HttpResultCode.MSG_FAILURE;
		DocumentBuilderFactory docBuilderFactory = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try{
 			docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.parse(new File(context.getFilesDir()+File.separator+fileName));
			Element root = doc.getDocumentElement();
			NodeList nodeList = root.getElementsByTagName("ret");
			
			long ret = StringUtil.getLongXmlElementLongValue(nodeList);
			if(ret == 1l){
				pickUpDao.deleteAll();//删除有可能服务器本来就没有数据回来
				NodeList Details = root.getElementsByTagName("Details");
  				
				List<PickUp> pickUpList = new ArrayList<PickUp>();
				for(int index = 0 , count = Details.getLength() ; index < count ; index++ ){
					Element temp =(Element)Details.item(index);
					PickUp pickUp = new PickUp();
					long out = StringUtil.getLongXmlElementLongValue(temp.getElementsByTagName("Out"));
					String doorName = StringUtil.getXmlElementStringValue(temp.getElementsByTagName("DoorName"));
					String locationname = StringUtil.getXmlElementStringValue(temp.getElementsByTagName("LocationName"));
				 
					pickUp.setPickup_id(out);
					pickUp.setDoorName(doorName);
					pickUp.setLocationname(locationname);
					pickUpList.add(pickUp);
					
					
 				}
				if(pickUpList.size() > 0 ){
				//	
					pickUpDao.addPickUp(pickUpList);
					flag = HttpResultCode.MSG_SUCCESS ;
				}else{flag = HttpResultCode.MSG_NO_DATA;}
				
			}
			return flag;
		}catch (Exception e) {
			flag = HttpResultCode.MSG_FAILURE;
			e.printStackTrace();
			throw new SystemException("GetPickUpListRunnable.handxml");
		}finally{
 			doc = null;
 			docBuilder = null;
 			docBuilderFactory = null;
 		}
		
	}

}
