package oso.ui.msg.bean;

import support.key.CheckInMainDocumentsRelTypeKey;
import android.content.Context;

/**执行任务bean
 * @author 朱成
 * @date 2014-12-8
 */
public class Task_ExecBean {
//	"entry_id": 120890,
//    "equipment_type_value": "Trailer",
//    "resources_id": 95,
//    "resources_type_value": "Spot",
//    "resources_type": 2,
//    "equipment_type": 2,
//    "task_total": 1,
//    "equipment_number": "CTNR2",
//    "resources_name": "105",
//    "equipment_id": 73
	
	public String entry_id;
	public String equipment_type_value;		//设备类型
	public String resources_id;
	public String resources_type_value;		//位置类型(如:door/spot)
	public String resources_type;
	
	public String equipment_type;
	public String task_total;
	public String equipment_number;			//设备值
	public String resources_name;			//位置值
	public String equipment_id;
	
	public int rel_type;	//in/out
	
	//=================================================
	
	public String getInOut(Context c){
		return CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(c,rel_type);
	}
	
}
