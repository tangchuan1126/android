package oso.ui.msg.bean;

import java.io.Serializable;

import support.key.CheckInMainDocumentsRelTypeKey;
import android.content.Context;

/**
 * @author 朱成
 * @date 2014-12-6
 */
public class Task_EntryBean implements Serializable{
	
//	"equipment_status": 1,
//    "equipment_type_value": "Trailer",
//    "equipment_number": "3SS",
//    "equipment_id": 60,
//    "equipment_type": 2,
//    "dlo_id": 120886,
//    "task_total": 2
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 6287091642538699236L;
	public int equipment_status;
	public String equipment_type_value;
	public String equipment_number;
	public String equipment_id;
	public int equipment_type;
	
	public String dlo_id;
	public int task_total;
	
	public int rel_type;		//in/out
	
	//=================================================
	
	public String getInOut(Context c){
		return CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(c,rel_type);
	}
}
