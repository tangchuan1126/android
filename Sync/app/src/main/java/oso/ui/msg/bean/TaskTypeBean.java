package oso.ui.msg.bean;

/**
 * 任务类型
 * @author 朱成
 * @date 2014-12-6
 */
public class TaskTypeBean {
	
	public final static int Type_Assign=52;
	public final static int Type_Execute=53;
	public final static int Type_Inventory=60;//库存 Inventory Task
	public final static int Type_Other=0;
//	public final static int Type_AssignCCTask=61;		//收货的-assign任务
	public final static int Type_Rec_CCTask=62;		//rec.assignTask
	public final static int Type_Rec_ScanSN=63;		//rec.scanTask
	public final static int Type_Rec_Movement=64;

	//Special Project
	public final static int Type_Special_Task = 70;

	public int total;			//数量、名称、类型id
	public String title;
	public int associate_process;

	
}
