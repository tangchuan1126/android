package oso.ui.msg;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.CCT_SlcArea;
import oso.ui.inventory.cyclecount.adapter.CCT_TaskTypeAdp;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_Task_Type_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.widget.BadgeView;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.Utility;

/**
 * @ClassName: Cycle_Count_Tasks_TypeActivity
 * @Description: 周期性盘点任务类型列表
 * @author xialimin
 */
public class Cycle_Count_Tasks_TypeActivity extends BaseActivity {

	private ListView lv;
	//private List<Cycle_Count_Tasks_Bean> list;
	private CCT_Task_Type_Bean mData;

	private NetConnection_CCT conn;

	private CCT_TaskTypeAdp mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_new_cycle_count_task_list, 0);

		conn = new NetConnection_CCT();
		setTitleString(getString(R.string.cct_inventory_task_types));
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						reqInventorys();
					}
				});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);

		// 初始化lv
		lv.setEmptyView(tvEmpty);


		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask = mData.allData.get(position);
				int taskType = CCT_Task_Type_Bean.getTaskType(cycleCountArrTask);


				jumpActivity(mData, taskType);
			}
		});

		//TTS.getInstance().speakAll(getString(R.string.cct_select_task));
	}
	/**
	 * @Description:跳转到指定页面
	 */
	private void jumpActivity(CCT_Task_Type_Bean data, int taskType){
		Intent in = new Intent();
		in.putExtra("Task_InventoryBean", data);
		in.putExtra("Task_Type", taskType);
		in.setClass(mActivity, Cycle_Count_Tasks_Single.class);
		startActivity(in);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		reqInventorys();
	}

	@Override
	public void onPush() {
		reqInventorys();
	}

	// Inventory-消息
	private void reqInventorys() {

		//请求获取任务数据
		conn.CCT_TaskInstanceListByUser(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == 1;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//list = new Gson().fromJson(json.optJSONArray("DATA").toString(), new TypeToken<List<Cycle_Count_Tasks_Bean>>() {
				//}.getType());

				try {
					mData = new Gson().fromJson(json.optJSONObject("DATA").toString(), CCT_Task_Type_Bean.class);
				} catch (Exception e) {
					e.printStackTrace();
					UIHelper.showToast(mActivity, "Request Failed!");
					return;
				}

				mData.initData();

				if (Utility.isNullForList(mData.allData))
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));

				lv.setAdapter(adpOthers);
				adpOthers.notifyDataSetChanged();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if (json == null) return;
				conn.handFail(json);
			}
		});
	}


	// ==================nested===============================

	private BaseAdapter adpOthers = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Other h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_tasktypes, null);
				h = new Holder_Other();
				h.task_name = (TextView) convertView.findViewById(R.id.tvName);

				convertView.setTag(h);

				// 初始化
				h.badge = new BadgeView(mActivity, h.task_name);
				h.badge.setBadgePosition(BadgeView.POSITION_RIGHT_VCENTER);
				h.badge.setBadgeMargin(Utility.dip2px(mActivity, 50));
				h.badge.show();
			} else
				h = (Holder_Other) convertView.getTag();

			// 刷新数据
			CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask = mData.allData.get(position);
			h.task_name.setText(CCT_Task_Type_Bean.getTaskTypeName(cycleCountArrTask));
			h.badge.setText(cycleCountArrTask.taskTypeArr.size() + "");
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public int getCount() {
			return mData.getValidTypeCnt();
		}

	};

	private class Holder_Other {
		TextView task_name;

		BadgeView badge;
	}
}
