package oso.ui.msg;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.assign_task.AssignTaskActivity;
import oso.ui.msg.bean.Task_EntryBean;
import oso.widget.BadgeView;
import support.common.UIHelper;
import support.common.bean.DockCheckInBase;
import support.common.bean.DockEquipmentBaseBean;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 分配任务-列表
 * 
 * @author 朱成
 * @date 2014-12-6
 */
public class TaskAssignAc extends BaseActivity {

	private ListView lv;
	private List<Task_EntryBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_assignlist, 0);
		setTitleString(getString(R.string.warehouse_linear_assign));
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						reqAssignTaskList();
					}
				});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);

		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Task_EntryBean bean = list.get(position);
				getEquipmentData(bean);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reqAssignTaskList();
	}

	@Override
	public void onPush() {
		// TODO Auto-generated method stub
		reqAssignTaskList();
	}

	/**
	 * @Description:获取tree的数据列表
	 * @param imgId
	 */
	private void getEquipmentData(final Task_EntryBean bean) {
		if (!StringUtil.isNullOfStr(bean.dlo_id)) {
			RequestParams params = new RequestParams();
			params.add("Method", "AssginTaskDetailByEntryAndEquipment");
			params.add("entry_id", bean.dlo_id);
			params.add("equipment_id", bean.equipment_id + "");
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					DockCheckInBase n = DockCheckInBase.parsingJSON(json,
							bean.dlo_id);
					jumpToAllocationActivity(n, bean.dlo_id, bean);
				}
			}.doPost(HttpUrlPath.AndroidAssignTaskAction, params, mActivity);
		}
	}

	/**
	 * @Description:根据条件跳转到entry所对应的类型
	 * @param flag
	 */
	protected void jumpToAllocationActivity(DockCheckInBase n,
			String checkin_id, Task_EntryBean bean) {
		if (n == null || Utility.isNullForList(n.getTreeList())) {
			UIHelper.showToast(mActivity, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
			return;
		}
		DockEquipmentBaseBean dBean = new DockEquipmentBaseBean();
		dBean.check_in_entry_id = bean.dlo_id;
		dBean.equipment_id = Integer.parseInt(bean.equipment_id);
		dBean.equipment_number = bean.equipment_number;
		dBean.equipment_type_value = bean.equipment_type_value;
		dBean.equipment_type = bean.equipment_type;

		Intent intent = new Intent(mActivity,
				AssignTaskActivity.class);
		intent.putExtra("dockCheckInBase", (Serializable) n);
		intent.putExtra("dlo_id", checkin_id);
		intent.putExtra("equipment", (Serializable) dBean);
		intent.putExtra("flag", AssignTaskActivity.ASSINGNTASK);
		startActivity(intent);
		// finish();
		overridePendingTransition(R.anim.push_from_left_in,
				R.anim.push_from_left_out);
	}

	private void reqAssignTaskList() {

		RequestParams p = new RequestParams();
		p.add("Method", "AssignTaskList");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");

				list=null;
				JSONArray ja = json.optJSONArray("datas");
				if (ja == null || ja.length() == 0) {
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				} else {
					list = new Gson().fromJson(ja.toString(),
							new TypeToken<List<Task_EntryBean>>() {
							}.getType());
				}
				adp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.GCMAction, p, this);

	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_assigntask, null);
				h = new Holder();
				h.lo = convertView.findViewById(R.id.lo);
				h.tvEntry = (TextView) convertView.findViewById(R.id.tvEntry);
				h.tvEquip = (TextView) convertView.findViewById(R.id.tvEquip);
				h.tvInOut = (TextView) convertView.findViewById(R.id.tvInOut);
				h.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
				convertView.setTag(h);

				// 初始化
				h.badge = new BadgeView(mActivity, h.lo);
				h.badge.setBadgePosition(BadgeView.POSITION_RIGHT_VCENTER);
				h.badge.setBadgeMargin(Utility.dip2px(mActivity, 50));
				h.badge.show();
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			Task_EntryBean b = list.get(position);
			h.tvEntry.setText("E"+b.dlo_id);
			h.tvEquip.setText(b.equipment_number);
			h.tvInOut.setText("("+b.getInOut(mActivity)+")");
			h.badge.setText(b.task_total + "");
			if(b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR){
				h.imageView1.setImageResource(R.drawable.ic_tractor);
				
			}
			if(b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER){
				h.imageView1.setImageResource(R.drawable.ic_trailer);
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list == null ? 0 : list.size();
		}
	};

	private class Holder {
		View lo;
		TextView tvEntry;
		TextView tvEquip,tvInOut;

		ImageView imageView1;
		BadgeView badge;
	}

}
