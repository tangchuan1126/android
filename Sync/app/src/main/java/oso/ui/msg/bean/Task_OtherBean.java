package oso.ui.msg.bean;

/**其他任务
 * @author 朱成
 * @date 2014-12-6
 */
public class Task_OtherBean {
	
	public String create_time;
	public String assign_user_name;
	public String schedule_detail;
	public String title;
	public String assign_user_id;
	public String header;
	
}
