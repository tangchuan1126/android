package oso.ui.msg;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.msg.bean.Task_EntryBean;
import oso.ui.msg.bean.Task_OtherBean;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 其他任务-列表
 * 
 * @author 朱成
 * @date 2014-12-6
 */
public class Task_OthersAc extends BaseActivity {

	private ListView lv;
	private List<Task_EntryBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_assignlist, 0);
		setTitleString("Other Tasks");
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						reqOthers();
					}
				});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);

		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adpOthers);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// Intent in = new Intent(this, .class);
				// startActivity(in);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reqOthers();
	}

	@Override
	public void onPush() {
		// TODO Auto-generated method stub
		reqOthers();
	}

	private List<Task_OtherBean> listOthers;

	// 其他-消息
	private void reqOthers() {

		RequestParams p = new RequestParams();
		p.add("Method", "ScheduleUnFinishAndNoAssociate");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");

				list=null;
				JSONArray ja = json.optJSONArray("datas");
				if (ja == null || ja.length() == 0)
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				else
					listOthers = new Gson().fromJson(ja.toString(),
							new TypeToken<List<Task_OtherBean>>() {
							}.getType());
				adpOthers.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.GCMAction, p, this);

	}

	// ==================nested===============================

	private BaseAdapter adpOthers = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Other h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_assigntask_other, null);
				h = new Holder_Other();
				h.tvAssigner = (TextView) convertView
						.findViewById(R.id.tvAssigner);
				h.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
				h.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
				convertView.setTag(h);
			} else
				h = (Holder_Other) convertView.getTag();

			// 刷新数据
			Task_OtherBean b = listOthers.get(position);
			h.tvAssigner.setText(b.assign_user_name);
			h.tvTime.setText(b.create_time);
			h.tvTitle.setText(b.title);

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listOthers == null ? 0 : listOthers.size();
		}
	};

	private class Holder_Other {
		TextView tvAssigner, tvTime, tvTitle;
	}

}
