package oso.ui.msg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskDoorItemActivity;
import oso.ui.load_receive.do_task.main.CheckInTaskMainActivity;
import oso.ui.msg.bean.Task_ExecBean;
import oso.widget.BadgeView;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.loadingview.LoadingView;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.photo.pager.ImagePagerAdapter;
import oso.widget.photo.pagerindicator.CirclePageIndicator;
import oso.widget.photo.pagerindicator.HackyViewPager;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 执行任务-列表
 * 
 * @author 朱成
 * @date 2014-12-6
 */
public class Task_ExecuteAc extends BaseActivity {

	private ListView lv;
	private List<Task_ExecBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_assignlist, 0);
		setTitleString(getString(R.string.tms_inouttask));
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reqExecTaskList();
			}
		});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);

		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (Utility.isFastClick())
					return;
				Task_ExecBean bean = list.get(position);
				showGateCheckInPhoto(bean);
			}
		});

		// reqExecTaskList();
	}

	private void showGateCheckInPhoto(final Task_ExecBean bean) {
		// final View contentView = View.inflate(mActivity,
		// R.layout.dlg_gatecheckin_photo, null);
		// final TabToPhoto ttp = (TabToPhoto)
		// contentView.findViewById(R.id.ttp);
		// List<TabParam> params = new ArrayList<TabParam>();
		// params.add(new TabParam("Gate Check In", null, new PhotoCheckable(0,
		// "Gate Check In", ttp)));
		// params.get(0).setWebImgsParams(FileWithCheckInClassKey.PhotoGateCheckIn
		// + "", FileWithTypeKey.OCCUPANCY_MAIN + "", bean.entry_id);
		// ttp.init(mActivity, params);
		// ttp.setAddBtnVisibility(false);
		// new BottomDialog(mActivity).setTitle("E" +
		// bean.entry_id).setView(contentView).setCanceledOnTouchOutside(true).setShowCancelBtn(true)
		// .setOnSubmitClickListener("Enter", new OnSubmitClickListener() {
		// @Override
		// public void onSubmitClick(BottomDialog dlg, String value) {
		// beginWork(bean);
		// dlg.dismiss();
		// }
		// }).show();

		// debug
		// v
		final View v = View.inflate(mActivity, R.layout.ac_image_pager_task, null);
		final HackyViewPager vp = (HackyViewPager) v.findViewById(R.id.pager);
		final CirclePageIndicator indicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
		final View loGrp_dlg = v.findViewById(R.id.loGrp_dlg);
		final LoadingView lv = (LoadingView) v.findViewById(R.id.lv);
		lv.setShowBorder(false);
		lv.setLoadingTextColor(Color.WHITE);
		int screenH = Utility.getScreenHeight(this);
		LayoutParams lp = loGrp_dlg.getLayoutParams();
		lp.height = screenH - Utility.dip2px(this, 140f);
		
		final View loLoading_dlg=v.findViewById(R.id.loLoading_dlg);

		// vp
		List<String> list = new ArrayList<String>();
		ImagePagerAdapter adp = new ImagePagerAdapter(list, this);
		vp.setAdapter(adp);
		indicator.setViewPager(vp);
		int blue = getResources().getColor(R.color.sync_blue);
		// indicator.setPageColor(blue);
		indicator.setStrokeColor(blue);
		indicator.setFillColor(blue);

		// dlg
		new BottomDialog(mActivity).setTitle("E" + bean.entry_id).setView(v).setCanceledOnTouchOutside(true).setShowCancelBtn(true)
				.setOnSubmitClickListener(getString(R.string.task_enter), new OnSubmitClickListener() {
					@Override
					public void onSubmitClick(BottomDialog dlg, String value) {
						beginWork(bean);
						dlg.dismiss();
					}
				}).show();
		reqServerUrls(list, adp, bean.entry_id,loLoading_dlg);
	}

	// 拉取-服务端图片
	public void reqServerUrls(final List<String> list, final PagerAdapter adp, String entry_id,final View loLoading_dlg) {
		RequestParams p = NetInterface
				.getImgs_p(FileWithCheckInClassKey.PhotoGateCheckIn + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry_id, "", "");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				//隐藏加载数据框
				loLoading_dlg.setVisibility(View.GONE);
				
				JSONArray jaDatas = json.optJSONArray("datas");
				if (jaDatas == null || jaDatas.length() == 0)
					return;
				List<TTPImgBean> tmpList = new Gson().fromJson(jaDatas.toString(), new TypeToken<List<TTPImgBean>>() {
				}.getType());

				System.out.println("");
				for (int i = 0; i < tmpList.size(); i++)
					list.add(HttpUrlPath.basePath + tmpList.get(i).uri);
				adp.notifyDataSetChanged();
			}

			public void handFail() {
				System.err.println("nimei>>pv.reqServerUrls.fail");
			};
		}.doPost(HttpUrlPath.FileUpZipAction, p, this, true);
	}

	private void beginWork(final Task_ExecBean temp) {
		final CheckInTaskBeanMain bean = new CheckInTaskBeanMain();
		bean.equipment_type_value = temp.equipment_type_value;
		bean.resources_id = Integer.parseInt(temp.resources_id);
		bean.resources_type_value = temp.resources_type_value;
		bean.resources_type = Integer.parseInt(temp.resources_type);
		bean.equipment_type = Integer.parseInt(temp.equipment_type);
		bean.total_task = Integer.parseInt(temp.task_total);
		bean.equipment_number = temp.equipment_number;
		bean.resources_name = temp.resources_name;
		bean.equipment_id = Integer.parseInt(temp.equipment_id);

		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingDetailByEntryEquipmentAndResouces");
		params.add("entry_id", temp.entry_id);
		params.add("equipment_id", temp.equipment_id + "");
		params.add("resources_id", temp.resources_id + "");
		params.add("resources_type", temp.resources_type + "");
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				CheckInTaskBeanMain.handJson(json, bean);
				if (!Utility.isNullForList(bean.complexDoorList)) {
					Intent intent = new Intent(mActivity, CheckInTaskDoorItemActivity.class);
					bean.entry_id = temp.entry_id;
					intent.putExtra("taskmainlist", (Serializable) bean);
					intent.putExtra("mainnumber", 1);
					startActivityForResult(intent, CheckInTaskMainActivity.ITEMCLOSE);
				}
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reqExecTaskList();
	}

	@Override
	public void onPush() {
		// TODO Auto-generated method stub
		reqExecTaskList();
	}

	private void reqExecTaskList() {

		RequestParams p = new RequestParams();
		p.add("Method", "LoaderExecuteTaskList");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");

				list = null;
				JSONArray ja = json.optJSONArray("datas");
				if (ja == null)
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				else
					list = new Gson().fromJson(ja.toString(), new TypeToken<List<Task_ExecBean>>() {
					}.getType());
				adp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.GCMAction, p, this);

	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_exectask, null);
				h = new Holder();
				h.lo = convertView.findViewById(R.id.lo);
				h.tvDoor_title = (TextView) convertView.findViewById(R.id.tvDoor_title);
				h.tvDoor = (TextView) convertView.findViewById(R.id.tvDoor);
				h.tvEntry = (TextView) convertView.findViewById(R.id.tvEntry);
				h.tvEquip = (TextView) convertView.findViewById(R.id.tvEquip);
				h.tvInOut = (TextView) convertView.findViewById(R.id.tvInOut);
				h.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
				convertView.setTag(h);

				// 初始化
				h.badge = new BadgeView(mActivity, h.lo);
				h.badge.setBadgePosition(BadgeView.POSITION_RIGHT_VCENTER);
				h.badge.setBadgeMargin(Utility.dip2px(mActivity, 50));
				h.badge.show();
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			Task_ExecBean b = list.get(position);
			h.tvDoor_title.setText(b.resources_type_value + ":");
			h.tvDoor.setText(b.resources_name);
			h.tvEntry.setText("E" + b.entry_id);
			h.tvEquip.setText(b.equipment_number);
			h.tvInOut.setText("(" + b.getInOut(mActivity) + ")");
			if (b.equipment_type.equals(CheckInTractorOrTrailerTypeKey.TRACTOR + "")) {
				h.imageView1.setImageResource(R.drawable.ic_tractor);

			}
			if (b.equipment_type.equals(CheckInTractorOrTrailerTypeKey.TRAILER + "")) {
				h.imageView1.setImageResource(R.drawable.ic_trailer);
			}
			h.badge.setText(b.task_total + "");

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list == null ? 0 : list.size();
		}
	};

	private class Holder {
		View lo;
		TextView tvDoor_title, tvDoor, tvEntry, tvEquip, tvInOut;
		ImageView imageView1;
		BadgeView badge;
	}

}
