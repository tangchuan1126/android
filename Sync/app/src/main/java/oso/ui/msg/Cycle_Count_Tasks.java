package oso.ui.msg;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.CCT_SlcArea;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.Utility;

/**
 * @ClassName: Cycle_Count_Tasks 
 * @Description: 周期性盘点任务列表
 * @author gcy
 * @date 2015-4-7 上午10:28:12
 */
public class Cycle_Count_Tasks extends BaseActivity {

	private ListView lv;
	private List<Cycle_Count_Tasks_Bean> list;
	private NetConnection_CCT conn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_cycle_count_task_list, 0);

		conn = new NetConnection_CCT();
		setTitleString(getString(R.string.cct));
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						reqInventorys();
					}
				});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);

		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adpOthers);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Cycle_Count_Tasks_Bean t = list.get(position);
				jumpActivity(t);
			}
		});
		TTS.getInstance().speakAll(getString(R.string.cct_select_task));
	}
	/**
	 * @Description:跳转到指定页面
	 */
	private void jumpActivity(Cycle_Count_Tasks_Bean t){
		Intent in = new Intent();
		in.putExtra("Task_InventoryBean", (Serializable)t);
		in.setClass(mActivity, CCT_SlcArea.class);
		startActivity(in);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		reqInventorys();
	}

	@Override
	public void onPush() {
		reqInventorys();
	}

	// Inventory-消息
	private void reqInventorys() {

		//请求获取任务数据
		conn.CCT_TaskInstanceListByUser(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == 1;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				list = new Gson().fromJson(json.optJSONArray("DATA").toString(), new TypeToken<List<Cycle_Count_Tasks_Bean>>() {
				}.getType());
				if (Utility.isNullForList(list))
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				adpOthers.notifyDataSetChanged();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if(json==null) return;
				conn.handFail(json);
			}
		});
	}

	// ==================nested===============================

	private BaseAdapter adpOthers = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Other h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.cycle_count_task_layout_item, null);
				h = new Holder_Other();
				h.task_name = (TextView) convertView.findViewById(R.id.task_name);
				h.t_priority = (TextView) convertView.findViewById(R.id.t_priority);
				
				h.task_type = (TextView) convertView.findViewById(R.id.task_type);
				h.task_status = (TextView) convertView.findViewById(R.id.task_status);
				h.task_start_time = (TextView) convertView.findViewById(R.id.task_start_time);
				h.task_end_time = (TextView) convertView.findViewById(R.id.task_end_time);
				
				convertView.setTag(h);
			} else
				h = (Holder_Other) convertView.getTag();

			// 刷新数据
			Cycle_Count_Tasks_Bean t = list.get(position);
			

			
			h.t_priority.setText((t.PRIORITY==0)?"":Cycle_Count_Task_Key.getPriorityValue(t.PRIORITY));
			h.t_priority.setTextColor(Cycle_Count_Task_Key.getPriorityColor(t.PRIORITY));
			h.task_name.setText(t.CODE);

			h.task_type.setText((t.TYPE==0)?"":Cycle_Count_Task_Key.getTaskTypeValue(t.TYPE));
			h.task_status.setText((t.STATUS==0)?"":Cycle_Count_Task_Key.getTaskStatusValue(t.STATUS));
			h.task_start_time.setText(t.START_DATE);
			h.task_end_time.setText(t.END_DATE);
			
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public int getCount() {
			return list == null ? 0 : list.size();
		}
	};

	private class Holder_Other {
		TextView task_name;
		TextView t_priority;
		TextView task_type;
		TextView task_status;
		TextView task_start_time;
		TextView task_end_time;
	}	
}
