package oso.ui.msg;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.processing.specialproject.WMS_Special_LaborTasksActivity;
import oso.ui.load_receive.do_task.receive.task.Rec_AssignTaskListAc;
import oso.ui.load_receive.do_task.receive.task.Rec_ScanTask_ListAc;
import oso.ui.load_receive.do_task.receive.task.movement.Rec_Movement_ListAc;
import oso.ui.msg.bean.TaskTypeBean;
import oso.widget.BadgeView;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 任务类型
 * 
 * @author 朱成
 * @date 2014-12-6
 */
public class TaskTypesAc extends BaseActivity {

	private ListView lv;
	private List<TaskTypeBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_tasktypes, 0);
		setTitleString(getString(R.string.main_msg_title));
		allowShowMsg = false;
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						reqTaskNums();
					}
				});

		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty=findViewById(R.id.tvEmpty);

		// 初始化lv
//		lv.setEmptyView(Utility.getEmptyView_lv(this, "No tasks!"));
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				int type = list.get(position).associate_process;
				
				switch (type) {
					case TaskTypeBean.Type_Assign:
						startActivity(new Intent(mActivity, TaskAssignAc.class));
						break;
					case TaskTypeBean.Type_Execute:
						startActivity(new Intent(mActivity, Task_ExecuteAc.class));
						break;
					case TaskTypeBean.Type_Inventory:
						startActivity(new Intent(mActivity, Cycle_Count_Tasks_TypeActivity.class));
						break;
					case TaskTypeBean.Type_Other:
						startActivity(new Intent(mActivity, Task_OthersAc.class));
						break;

					//=====Special Project=======
					case TaskTypeBean.Type_Special_Task:
						startActivity(new Intent(mActivity, WMS_Special_LaborTasksActivity.class));
						break;

					//=====收货===================
					case TaskTypeBean.Type_Rec_CCTask:
						startActivity(new Intent(mActivity,
								Rec_AssignTaskListAc.class));
						break;
					case TaskTypeBean.Type_Rec_ScanSN:
						startActivity(new Intent(mActivity,
								Rec_ScanTask_ListAc.class));
						break;
						
					case TaskTypeBean.Type_Rec_Movement:
						startActivity(new Intent(mActivity,
								Rec_Movement_ListAc.class));
						break;
					default:
						UIHelper.showToast(getString(R.string.sync_developing));
				}
				
				/*
				 * 注释日期:20150226
				 * 注释原因 :简洁
				 */
				/*if (type == TaskTypeBean.Type_Assign)
					startActivity(new Intent(mActivity, TaskAssignAc.class));
				else if (type == TaskTypeBean.Type_Execute)
					startActivity(new Intent(mActivity, Task_ExecuteAc.class));
				else if (type == TaskTypeBean.Type_Other)
					startActivity(new Intent(mActivity, Task_OthersAc.class));
				else if(type==TaskTypeBean.Type_AssignCCTask)
					startActivity(new Intent(mActivity, Rec_AssignTaskListAc.class));
				else if(type==TaskTypeBean.Type_Rec_CCTask)
					startActivity(new Intent(mActivity, Rec_ScanTask_ListAc.class));
				//没有实现
				else
					UIHelper.showToast(getString(R.string.sync_developing));*/
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reqTaskNums();
	}

	@Override
	public void onPush() {
		// TODO Auto-generated method stub
		reqTaskNums();
	}

	private void reqTaskNums() {

		RequestParams params = new RequestParams();
		params.add("Method", "ScheduleType");
		params.add("language", StoredData.readLanguageStr());
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				System.out.println("");

				list=null;
				JSONArray jaDatas = json.optJSONArray("datas");
				if (jaDatas == null || jaDatas.length() == 0)
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				else {
					list = new Gson().fromJson(jaDatas.toString(),
							new TypeToken<List<TaskTypeBean>>() {
							}.getType());
				}
			
				adp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.GCMAction, params, this);

	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_tasktypes, null);
				h = new Holder();

				h.tvName = (TextView) convertView.findViewById(R.id.tvName);
				convertView.setTag(h);

				// 初始化
				h.badge = new BadgeView(mActivity, h.tvName);
				h.badge.setBadgePosition(BadgeView.POSITION_RIGHT_VCENTER);
				h.badge.setBadgeMargin(Utility.dip2px(mActivity, 50));
				h.badge.show();
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			TaskTypeBean b = list.get(position);
			h.tvName.setText(b.title);
			h.badge.setText(b.total + "");
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list != null ? list.size() : 0;
		}
	};

	private class Holder {

		TextView tvName;

		BadgeView badge;
	}

}
