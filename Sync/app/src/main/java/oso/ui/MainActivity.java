package oso.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.Serializable;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.chat.util.XmppTool;
import oso.ui.debug.DebugActivity;
import oso.ui.inventory.InventoryMainActivity;
import oso.ui.load_receive.LoadReceiveMainActivity;
import oso.ui.main.AccountSettingsActivity;
import oso.ui.main.SetFunctionActivity;
import oso.ui.monitor.MonitorActivity;
import oso.ui.msg.TaskTypesAc;
import oso.ui.processing.ProcessActivity;
import oso.ui.yardcontrol.YardControlMainActivity;
import oso.widget.BadgeView;
import oso.widget.screennotice.SyncNotifyManager;
import support.AppConfig;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.dbhelper.DBManager;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import support.receiver.IncomingCallReceiver;
import utility.HttpUrlPath;

public class MainActivity extends BaseActivity implements OnClickListener {
	private static final int ITEM1 = Menu.FIRST;

	public static MainActivity instance;

	private SharedPreferences sp;

	private TextView user_name;// , ps_type;// 标题栏、用户名、类型

	private ImageView imgMsg;

	private ImageView imgChatMsg;

	private String userName = "";

	private String psName = "";

	private BadgeView badgeMsg; // 消息数

	private BadgeView badgeChatMsg; // 聊天未读消息数

	private NewMsgBroadcastReceiver chatMsgReceiver;

	private LockScreenReceiver lockReceiver;

	private IncomingCallReceiver callReceiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main, 0);
		instance = this;
		showBackButton(false);
		allowShowMsg = false;

		getLocalData();
		initView();
		// debug
		// TTS.getInstance().init(this);

		// Chat 部分
		registChatReceiver();
		registKeyguardReceiver();
		// registCallReceiver();
		
		setSwipeBackEnable(false);

	}


	/**
	 * @Description:获取本地数据
	 */
	private void getLocalData() {
		sp = getSharedPreferences("loginSave", MODE_PRIVATE);
		userName = sp.getString("username", "");
		psName = sp.getString("ps_name", "");
	}

	/**
	 * @Description::初始化主Ui的控件
	 */
	private void initView() {
		setTitleString(psName);

		imgMsg = (ImageView) findViewById(R.id.btnMsg);

		imgChatMsg = (ImageView) findViewById(R.id.img_chat_msg);

		user_name = (TextView) findViewById(R.id.user_name);
		// ps_name = (TextView) findViewById(R.id.ps_name);
		// ps_type = (TextView) findViewById(R.id.ps_type);

		/***************************************/
		imgChatMsg.setOnClickListener(this);
		/***************************************/

		imgMsg.setOnClickListener(this);
		findViewById(R.id.tvRouting).setOnClickListener(this);
		findViewById(R.id.main_YardControl).setOnClickListener(this);
		findViewById(R.id.main_InOut).setOnClickListener(this);
		findViewById(R.id.tvProcess).setOnClickListener(this);
		findViewById(R.id.tvAppointment).setOnClickListener(this);
		findViewById(R.id.tvPallitize).setOnClickListener(this);
		findViewById(R.id.tvItems).setOnClickListener(this);
		findViewById(R.id.tvInventory).setOnClickListener(this);
		findViewById(R.id.user_name).setOnClickListener(this);
		findViewById(R.id.ivUserIcon).setOnClickListener(this);
		/**************************************************/

		user_name.setText(userName);
		// ps_type.setText(psType);

		badgeMsg = new BadgeView(this, imgMsg);

		showRightButton(R.drawable.set_button_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, SetFunctionActivity.class);
				startActivity(intent);
			}
		});

		badgeChatMsg = new BadgeView(this, imgChatMsg);

	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshMsgCnt();
		refreshChatMsgCnt();
		refreshUserInfo();
	}

	/**
	 * 重新加载username
	 */
	private void refreshUserInfo() {
		user_name.setText(StoredData.getUsername());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		instance = null;
		// debug
		// TTS.getInstance().onTTSDestroy();

		if (chatMsgReceiver != null) {
			unregisterReceiver(chatMsgReceiver);
			chatMsgReceiver = null;
		}

		unregistKeyguardReceiver();
	}

	public static final int CODE_ACCOUNT_SETTING = 50;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// debug
		TTS.getInstance().onTTSActivityResult(requestCode, resultCode, data);
		// 修改人员图标的名字
		if (CODE_ACCOUNT_SETTING == resultCode) {
			user_name.setText(data.getStringExtra("username"));
			new Thread() {
				public void run() {
					// 并且需要重新登录openfire
					StoredData.logout(); // 登出
					XmppTool.getThis().changePwd_of();// 重新登录openfire
				};
			}.start();
		}
	}

	@Override
	public void onPush() {
		// 刷新-消息数
		refreshMsgCnt();

		// 刷新 聊天未读消息数
		refreshChatMsgCnt();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnMsg:
			 popTo(TaskTypesAc.class);
//			UIHelper.showToast("TaskTypesAc");
			// debug
			// if(AppConfig.isDebug)
			// popTo(TestAc.class);
			break;

		// Developing
		case R.id.tvRouting:
			UIHelper.showToast(getString(R.string.sync_developing));
			break;

		case R.id.main_YardControl:
			popTo(YardControlMainActivity.class);
			break;

		case R.id.main_InOut:
			// if (StoredData.is_Gate()) {// 如果是Gate人员则不能进入该功能
			// UIHelper.showToast("No Access!");
			// return;
			// }
			popTo(LoadReceiveMainActivity.class);
			break;

		// Developing
		case R.id.tvProcess:
			// debug,picking
			popTo(ProcessActivity.class);
			break;

		case R.id.tvAppointment:
			popTo(MonitorActivity.class);
			break;

		// Developing
		case R.id.tvPallitize:
			// popTo(PalletizeActivity.class);
			UIHelper.showToast(getString(R.string.sync_developing));
			break;

		case R.id.tvItems:
			if (AppConfig.isDebug)
				popTo(DebugActivity.class);

			// popTo(ItemsActivity.class);
			break;

		// Developing,debug
		case R.id.tvInventory:
			popTo(InventoryMainActivity.class);
			break;

		case R.id.img_chat_msg:
			popTo(ChatTabActivity.class);
			break;

		// // 进入account设置界面
		case R.id.user_name:
		case R.id.ivUserIcon:
				Intent intent = new Intent(mActivity, AccountSettingsActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("allpost", (Serializable) StoredData.getAllpost());
				intent.putExtras(b);
				startActivity(intent);
			break;

		}
	}

	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	// 刷新-消息数
	private void refreshMsgCnt() {

		RequestParams params = new RequestParams();
		params.put("Method", "MessageQueryCount");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {

				int msgCnt = json.optInt("countmessage");
				badgeMsg.setText(msgCnt + "");
				if (msgCnt == 0)
					badgeMsg.hide();
				else
					badgeMsg.show();

			}

			@Override
			public void handFail() {
				System.out.println("Sync>>msgCnt获取失败");
			};
		}.doPost(HttpUrlPath.GCMAction, params, this, true);

	}

	// public void openPush() {
	// Intent otherInent = new Intent(mActivity, LoadDevDemo.class);
	// startActivity(otherInent);
	// }

	@Override
	// 点击菜单按钮响应的事件
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(1, ITEM1, 0, R.string.main_menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ITEM1:
			Intent intent = new Intent(this, SetFunctionActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onBackBtnOrKey() {
		moveTaskToBack(true);
	}

	// 聊天部分新加
	/**
	 * 聊天账号登陆
	 */
	// public void chatUserLogin() {
	//
	// sp = getSharedPreferences("loginSave", MODE_PRIVATE);
	// String username = sp.getString("name", "");
	// String password = sp.getString("pwd", "");
	// ChatLoginTask loginTask = new ChatLoginTask(this);
	// loginTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
	// new LoginUserInfo(username, password));
	// new LoginUserInfo("test", "test"));
	//
	// }

	/**
	 * 初始化相关组件
	 */
	// private void initChatManager() {
	// new Thread() {
	// @Override
	// public void run() {
	// chatManager = XmppTool.getConnection().getChatManager();
	// chatManager = ChatManager.getInstanceFor(XmppTool
	// .getConnection());
	// handler.sendEmptyMessage(CHAT_MANAGER_SUCESS);
	// }
	// }.start();
	// }

	/**
	 * 刷新未读消息
	 */
	private void refreshChatMsgCnt() {
		String uname_me = StoredData.getName();
		int queryRecentMsgCount = DBManager.getThis().queryRecentMsgCount(uname_me);
		if (queryRecentMsgCount == 0) {
			badgeChatMsg.hide();
		} else if (queryRecentMsgCount > 99) {
			badgeChatMsg.show();
			badgeChatMsg.setText("99+");
		} else {
			badgeChatMsg.show();
			badgeChatMsg.setText("" + queryRecentMsgCount);
		}
	}

	/**
	 * 注册接收广播
	 */
	private void registChatReceiver() {
		// 消息广播
		IntentFilter intentFilter = new IntentFilter("com.vvme.chat.NEW_MESSAGE_ACTION");
		chatMsgReceiver = new NewMsgBroadcastReceiver();
		intentFilter.setPriority(100); // ChatActivity 聊天界面优先级最高
		registerReceiver(chatMsgReceiver, intentFilter);

	}

	private void registKeyguardReceiver() {
		if (lockReceiver == null) {
			lockReceiver = new LockScreenReceiver();
		}
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		filter.addAction(Intent.ACTION_SCREEN_ON);
		registerReceiver(lockReceiver, filter);
	}

	private void unregistKeyguardReceiver() {
		if(lockReceiver != null) {
			unregisterReceiver(lockReceiver);
			lockReceiver = null;
		}
	}

	/**
	 * 显示未读消息个数 BadgeView
	 * 
	 * @param recentMsgCount
	 */
	public void showChatMsgBdgView(final int recentMsgCount) {
		// 判断未读消息数量的个数
		if (0 == recentMsgCount) {
			badgeChatMsg.hide();
		} else {
			badgeChatMsg.show();
			badgeChatMsg.setText("" + recentMsgCount);
		}
	}

	/**
	 * 接收新消息广播
	 * 
	 * @author xialimin
	 * 
	 */
	private class NewMsgBroadcastReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			final ChatMessage message = (ChatMessage) intent.getExtras().get("NewMessage");
			String messageBody = message.getMessageBody();
			System.out.println("MainActivity 有新消息了: " + messageBody);
			new Thread() {
				@Override
				public void run() {
					// Tab显示数量
					String unameMe = StoredData.getName();
					DBManager db = DBManager.getThis();
					final int recentMsgCount = db.queryRecentMsgCount(unameMe);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showChatMsgBdgView(recentMsgCount);
						}
					});
				};
			}.start();

			abortBroadcast();

		}
	}

	private class LockScreenReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Intent.ACTION_SCREEN_ON)) {
			} else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
				SyncNotifyManager.isInKeyguard = true;
			}
		}
	}

	private void registCallReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.SipDemo.INCOMING_CALL");
		callReceiver = new IncomingCallReceiver();
		this.registerReceiver(callReceiver, filter);
	}
}
