package oso.ui.debug.product.adapter;

import java.util.List;

import oso.ui.debug.product.bean.ContextBean.CategoryBean;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class ChooseExAdapter extends BaseExpandableListAdapter {

	private List<CategoryBean> datas = null;
	private Context mContext;

	public ChooseExAdapter(Context c, List<CategoryBean> cb) {
		datas = cb;
		mContext = c;
	}

	@Override
	public int getGroupCount() {
		return datas == null ? 0 : datas.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return datas.get(groupPosition);
	}

	@Override
	public long getGroupId(int arg0) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Holder h;
		if (convertView == null) {
			convertView = View.inflate(mContext, R.layout.dlg_item_choose_category, null);
			h = new Holder();
			h.nameTv = (TextView) convertView.findViewById(R.id.nameTv);
			h.box = (CheckBox) convertView.findViewById(R.id.box);
			h.arrIv = (ImageView) convertView.findViewById(R.id.arrIv);
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();

		final CategoryBean bean = datas.get(groupPosition);
		h.nameTv.setText(bean.name);
		h.nameTv.setBackgroundResource(R.drawable.sync_white_to_gray);
		h.box.setChecked(bean.isCheck);
		h.box.setVisibility(bean.children == null ? View.VISIBLE : View.GONE);
		h.nameTv.setTextColor(bean.children == null ? mContext.getResources().getColor(R.color.sync_blue) : Color.BLACK);

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		CategoryBean parent = datas.get(groupPosition);
		List<CategoryBean> child = parent.children;
		return parent == null ? 0 : (child == null ? 0 : child.size());
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return datas.get(groupPosition).children.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		Holder h;
		if (convertView == null) {
			convertView = View.inflate(mContext, R.layout.dlg_item_choose_category, null);
			h = new Holder();
			h.nameTv = (TextView) convertView.findViewById(R.id.nameTv);
			h.box = (CheckBox) convertView.findViewById(R.id.box);
			h.arrIv = (ImageView) convertView.findViewById(R.id.arrIv);
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();

		final CategoryBean bean = datas.get(groupPosition).children.get(childPosition);
		h.nameTv.setText("\t\t" + bean.name);
		h.nameTv.setBackgroundResource(R.drawable.sync_gary_to_white);
		h.box.setChecked(bean.isCheck);
		h.box.setVisibility(bean.children == null ? View.VISIBLE : View.GONE);
		h.arrIv.setVisibility(bean.children != null ? View.VISIBLE : View.GONE);
		h.nameTv.setTextColor(bean.children == null ? mContext.getResources().getColor(R.color.sync_blue) : Color.BLACK);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

	class Holder {
		TextView nameTv;
		CheckBox box;
		ImageView arrIv;
	}

}
