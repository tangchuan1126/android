package oso.ui.debug.product;

import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.base.BaseFramentActivity;
import oso.ui.debug.product.bean.ContextBean.ContextParent;
import oso.ui.debug.product.bean.ProductBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.NetConnectionInterface.SyncJsonHandler;
import support.network.NetConnection_YMS;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import declare.com.vvme.R;

public class SimpleCreateProductActivity extends BaseFramentActivity implements OnClickListener {

	private View titleView, customerView, upcCodeView, weightView, priceView, serialView, lwhView;

	private TextView productNameTv, mainCodeTv, upcCodeTv, weightTv, priceTv, serialTv, lwhTv;

	private TextView titleTv, customerTv;

	private Button submitBtn;

	public ProductBean returnData = null;
	
	private boolean isBackWrite = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_create_product_simple, 0);
		initData();
		initView();
		setData();
	}

	private void initData() {
		setTitleString(R.string.pro_create_product);
		returnData = SelectItemsActivity.products.get(getIntent().getIntExtra("position", 0));
		isBackWrite = getIntent().getBooleanExtra("isBackWrite", true);
	}

	private void setData() {
		// 商品名称
		productNameTv.setText(returnData.productName);
		// 主条码
		mainCodeTv.setText(returnData.mainCode);
		// upc Code
		setView(upcCodeView, upcCodeTv, returnData.upcCode);
		// 重量
		String uom = getUom(SelectItemsActivity.contextData.weightUom, returnData.weightUom);
		setView(weightView, weightTv, returnData.weight, " " + uom);
		// 价格
		uom = getUom(SelectItemsActivity.contextData.priceUom, returnData.priceUom);
		setView(priceView, priceTv, returnData.price, " " + uom);
		// 长，宽，高
		uom = getUom(SelectItemsActivity.contextData.lengthUom, returnData.lengthUom);
		String lwhStr = returnData.length + " * " + returnData.width + " * " + returnData.height;
		setView(lwhView, lwhTv, lwhStr, " " + uom);
		// SN Length
		setView(serialView, serialTv, returnData.snLength);

		// title and customer
		if (returnData.tcArray != null && !returnData.tcArray.isEmpty()) {
			setView(titleView, titleTv, getUom(SelectItemsActivity.contextData.title, returnData.tcArray.get(0).title));
			setView(customerView, customerTv, getUom(SelectItemsActivity.contextData.customer, returnData.tcArray.get(0).customer));
		}

	}

	private String getUom(List<? extends ContextParent> list, String id) {
		for (ContextParent uom : list) {
			if (uom.id.equals(id))
				return uom.name;
		}
		return null;
	}

	private void setView(View layout, TextView tv, String data) {
		setView(layout, tv, data, "");
	}

	private void setView(View layout, TextView tv, String data, String append) {
		if (data != null && !data.isEmpty()) {
			tv.setText(data + append);

			layout.setVisibility(View.VISIBLE);
		} else {
			layout.setVisibility(View.GONE);
		}
	}

	private void initView() {
		productNameTv = (TextView) findViewById(R.id.productNameTv);
		mainCodeTv = (TextView) findViewById(R.id.mainCodeTv);
		upcCodeTv = (TextView) findViewById(R.id.upcCodeTv);
		priceTv = (TextView) findViewById(R.id.priceTv);
		serialTv = (TextView) findViewById(R.id.serialTv);
		weightTv = (TextView) findViewById(R.id.weightTv);
		lwhTv = (TextView) findViewById(R.id.lwhTv);
		titleTv = (TextView) findViewById(R.id.titleTv);
		customerTv = (TextView) findViewById(R.id.customerTv);

		// View
		titleView = findViewById(R.id.titleView);
		customerView = findViewById(R.id.customerView);
		upcCodeView = findViewById(R.id.upcCodeView);
		weightView = findViewById(R.id.weightView);
		priceView = findViewById(R.id.priceView);
		serialView = findViewById(R.id.serialView);
		lwhView = findViewById(R.id.lwhView);

		submitBtn = (Button) findViewById(R.id.submitBtn);
		submitBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submitBtn:
			RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_submit) + "?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					doSubmit();
				}
			});
			break;

		default:
			break;
		}
	}

	private void doSubmit() {
		NetConnection_YMS.getThis().reqProduct_add(new SyncJsonHandler(mActivity) {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("上传商品成功= " + json.toString());
				// 回写数据
				if (isBackWrite)
					NetConnection_YMS.getThis().reqProduct_write(json.optString("id"));

				RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
				dialog.setMessage(getString(R.string.sync_create_success));
				dialog.setCancelable(false);
				dialog.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						returnData.isCreateed = true;
						finish();
					}
				});
				dialog.hideCancelBtn();
				dialog.create().show();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
//				System.out.println("Create Failed" + json.toString());
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
				}
			}

		}, returnData.toJsonStr());
	}

}
