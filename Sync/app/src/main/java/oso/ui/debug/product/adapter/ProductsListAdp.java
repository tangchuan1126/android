package oso.ui.debug.product.adapter;

import java.util.List;

import oso.ui.debug.product.bean.ProductBean;
import oso.ui.debug.product.iface.IProduct;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;
/**
 * 显示Product列表 适配器
 * @author xialimin
 *
 */
public class ProductsListAdp extends BaseAdapter {
	
	private Context mContext;
	
	private List<ProductBean> mPdtList;
	
	private IProduct iProduct;
	
	public ProductsListAdp(Context ctx, List<ProductBean> productsList, IProduct iproduct) {  // 构造方法还需要数据Bean
		mContext = ctx;
		mPdtList = productsList;
		iProduct = iproduct;
	}
	@Override
	public int getCount() {
		return mPdtList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initView(position, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillData(position, holder);
		return convertView;
	}
	
	/**
	 * 初始化视图
	 * @param position
	 * @param convertView
	 * @param holder
	 * @return
	 */
	private View initView(int position, View convertView, ViewHolder holder) {
		convertView = View.inflate(mContext, R.layout.product_item_layout, null);
		
		holder.productIv = (ImageView) convertView.findViewById(R.id.product_image);
//		holder.field1Tv = (TextView) convertView.findViewById(R.id.product_tv_field1_value);
//		holder.field2Tv = (TextView) convertView.findViewById(R.id.product_tv_field2_value);
//		holder.field3Tv = (TextView) convertView.findViewById(R.id.product_tv_field3_value);
		return convertView;
	}

	/**
	 * 填充数据
	 * @param position
	 * @param holder
	 */
	private void fillData(int position, ViewHolder holder) {
//		ProductBean pdtBean = mPdtList.get(position);
		// debug
		if(iProduct.isShouldShow()) {
			String url = "http://www.dm456.com/uploads/image/20110608/103745343.jpg";
			ImageLoader.getInstance().displayImage(url, holder.productIv);
		}
//		holder.field1Tv.setText("字段1的值");
//		holder.field2Tv.setText("字段2的值");
//		holder.field3Tv.setText("字段3的值");
	}
	
	class ViewHolder {
		ImageView productIv;
		TextView field1Tv, field2Tv, field3Tv; // 模拟字段
	}
}
