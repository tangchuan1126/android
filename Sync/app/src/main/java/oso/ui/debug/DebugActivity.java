package oso.ui.debug;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.debug.product.CreateProductActivity;
import oso.ui.debug.product.ProductsListActivity;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.msg.Cycle_Count_Tasks;
import oso.ui.processing.specialproject.WMS_Special_ProjectActivity;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class DebugActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_debug, 0);
		setTitleString("Debug");
	}

	/* -------------------TEST BUTTON-------------------- */
	public void toPrePickingOnClick(View v) {
		/*
		 * jiang startActivity(new Intent(mActivity,
		 * SearchPrePickingActivity.class));
		 */
	}

	public void toConsolidateOnClick(View v) {
		/*
		 * jiang startActivity(new Intent(mActivity,
		 * SearchConsolidateActivity.class));
		 */
	}

	public void toSpecialProject(View v) {
		startActivity(new Intent(mActivity, WMS_Special_ProjectActivity.class));
	}

	public void toCCT(View v) {
		startActivity(new Intent(mActivity, Cycle_Count_Tasks.class));
	}

	public void toProductList(View v) {
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
					return;
				}
				ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
				}.getType());
				Intent intent = new Intent(mActivity, ProductsListActivity.class);
				intent.putExtra("ContextData", data);
				startActivity(intent);
			}
		}.doGet(HttpUrlPath.product_context, new RequestParams(), mActivity);

	}

	public void toCreateProductList(View v) {
		CreateProductActivity.toThis(mActivity, true);
	}

}
