package oso.ui.debug.product;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.debug.product.adapter.ChooseExAdapter;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.CategoryBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.widget.SyncSpinner;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import support.anim.AnimUtils;
import support.common.UIHelper;
import utility.ActivityUtils;
import utility.Utility;

public class BasicInfoFragment extends Fragment {

    private View view, bgLayout;

    private EditText productNameEt, mainCodeEt, upcCodeEt, weightEt, priceEt, serialEt;

    private SyncSpinner weightSp, priceSp;

    private Button lwhBtn, categoryBtn;

    String length, width, height, lengthUom, lengthUomType;

    int lengthPosition = 0;

    String weightUom, priceUom;

    CategoryBean mCurrentCategoryBean;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return view = inflater.inflate(R.layout.fragment_basic_info, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView(view);
        setData();
        setProduct();
    }

    private void initView(View v) {
        bgLayout = v.findViewById(R.id.bgLayout);
        productNameEt = (EditText) v.findViewById(R.id.productName);
        mainCodeEt = (EditText) v.findViewById(R.id.mainCodeEt);
        upcCodeEt = (EditText) v.findViewById(R.id.upcCodeEt);
        weightEt = (EditText) v.findViewById(R.id.weightEt);
        priceEt = (EditText) v.findViewById(R.id.priceEt);
        serialEt = (EditText) v.findViewById(R.id.serialEt);
        weightSp = (SyncSpinner) v.findViewById(R.id.weightSp);
        priceSp = (SyncSpinner) v.findViewById(R.id.priceSp);
        lwhBtn = (Button) v.findViewById(R.id.lwhSp);
        categoryBtn = (Button) v.findViewById(R.id.categorySp);
        /*
         * productNameEt.addTextChangedListener(watcher);
		 * mainCodeEt.addTextChangedListener(watcher);
		 * upcCodeEt.addTextChangedListener(watcher);
		 * weightEt.addTextChangedListener(watcher);
		 * priceEt.addTextChangedListener(watcher);
		 * serialEt.addTextChangedListener(watcher);
		 */
        bgLayout.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                colseInputMethod();
                return false;
            }
        });
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (isCheck()) {
                if (onCheckListener != null)
                    onCheckListener.onCheck();
            }
        }
    };

    private void setData() {
        setDefaultData();
//        weightSp.setAdapter(new ContextAdapter(CreateProductActivity.data.weightUom));
//        weightSp.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                weightUom = CreateProductActivity.data.weightUom.get(arg2).id;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });
        weightSp.setAdapterData(ContextBean.format(CreateProductActivity.data.weightUom), new SyncSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position) {
                weightUom = CreateProductActivity.data.weightUom.get(position).id;
            }
        }, 0);

//        priceSp.setAdapter(new ContextAdapter(CreateProductActivity.data.priceUom));
//        priceSp.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                priceUom = CreateProductActivity.data.priceUom.get(arg2).id;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });
        priceSp.setAdapterData(ContextBean.format(CreateProductActivity.data.priceUom), new SyncSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position) {
                priceUom = CreateProductActivity.data.priceUom.get(position).id;
            }
        }, 0);

        lwhBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showLWHDialog();
            }
        });

        categoryBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showChooseCategoryDialog();
            }
        });

    }

    private void setProduct() {
        ProductBean p = (ProductBean) getActivity().getIntent().getSerializableExtra("product");
        if (p == null)
            return;
        productNameEt.setText(formatValue(p.productname));
        serialEt.setText(formatValue(p.snlength));
        length = p.length;
        width = p.width;
        height = p.height;
        lengthUomType = CreateProductActivity.data.lengthUom.get(0).name;
        lwhBtn.setText(length + " * " + width + " * " + height + " " + lengthUomType);
        if (!TextUtils.isEmpty(p.productname)) {
            productNameEt.setBackgroundColor(Color.TRANSPARENT);
            productNameEt.setEnabled(false);
        }

    }

    /**
     * 设置默认值  Intent传参的数据
     * @author xia
     */
    private void setDefaultData() {
        if (getActivity() instanceof CreateProductActivity) {
            String productName = ((CreateProductActivity) getActivity()).mProductName;
            String mainCode = ((CreateProductActivity) getActivity()).mMainCode;
            productNameEt.setText(TextUtils.isEmpty(productName) ? "" : productName);
            mainCodeEt.setText(TextUtils.isEmpty(mainCode) ? "" : mainCode);
            Utility.etRequestFocus(productNameEt, true);
        }
    }

    private String formatValue(String value) {
        return value == null ? "" : value;
    }

    public void colseInputMethod() {
        /*
         * Utility.colseInputMethod(getActivity(), productNameEt);
		 * Utility.colseInputMethod(getActivity(), mainCodeEt);
		 * Utility.colseInputMethod(getActivity(), upcCodeEt);
		 * Utility.colseInputMethod(getActivity(), weightEt);
		 * Utility.colseInputMethod(getActivity(), priceEt);
		 * Utility.colseInputMethod(getActivity(), serialEt);
		 */
        ActivityUtils.hideSoftInput(getActivity());
    }

    private void showLWHDialog() {
        final View v = View.inflate(getActivity(), R.layout.dlg_input_whd, null);
        final EditText lEt = (EditText) v.findViewById(R.id.lEt);
        final EditText wEt = (EditText) v.findViewById(R.id.wEt);
        final EditText hEt = (EditText) v.findViewById(R.id.hEt);
        final Spinner lwhSp = (Spinner) v.findViewById(R.id.lwhSp);

        lwhSp.setAdapter(new ContextAdapter(CreateProductActivity.data.lengthUom));
        lwhSp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                lengthPosition = arg2;
                lengthUom = CreateProductActivity.data.lengthUom.get(arg2).id;
                lengthUomType = CreateProductActivity.data.lengthUom.get(arg2).name;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        lEt.setText(length == null ? "" : length);
        wEt.setText(width == null ? "" : width);
        hEt.setText(height == null ? "" : height);
        lwhSp.setSelection(lengthPosition);

        final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(getActivity());
        builder.setView(v);
        builder.setPositiveButtonOnClickDismiss(false);
        builder.setNegativeButton(R.string.sync_cancel, null);
        builder.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (checkLWH(lEt, wEt, hEt)) {
                    length = lEt.getEditableText().toString();
                    width = wEt.getEditableText().toString();
                    height = hEt.getEditableText().toString();
                    lwhBtn.setText(length + " * " + width + " * " + height + " " + lengthUomType);
                    builder.dismiss();
                }
            }
        });
        RewriteBuilderDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

    private BottomDialog categoryDialog;

    private void showChooseCategoryDialog() {
        final View dialogView = View.inflate(getActivity(), R.layout.dlg_choose_category, null);
        categoryDialog = new BottomDialog(getActivity());
        showExlist(dialogView);
        categoryDialog.setTitle("Category");
        categoryDialog.setContentView(dialogView);
        categoryDialog.setOnSubmitClickListener(getString(R.string.sync_ok), new OnSubmitClickListener() {
            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                if (mCurrentCategoryBean == null) {
                    categoryBtn.setText("");
                    UIHelper.showToast(getActivity(), "Please select category");
                    return;
                }
                categoryBtn.setText(mCurrentCategoryBean.name);
                dlg.dismiss();
            }
        });
        categoryDialog.showLeftBtn(false);
        categoryDialog.show();
    }

    private void showExlist(View dialogView) {
        final ExpandableListView exLv = (ExpandableListView) dialogView.findViewById(R.id.exLv);
        final ExpandableListView exLv2 = (ExpandableListView) dialogView.findViewById(R.id.exLv2);
        final ChooseExAdapter adapter = new ChooseExAdapter(getActivity(), CreateProductActivity.data.category);
        exLv.setAdapter(adapter);
        // 展开全部
        for (int i = 0; i < CreateProductActivity.data.category.size(); i++)
            exLv.expandGroup(i);
        exLv.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getGroup(groupPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(CreateProductActivity.data.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
        exLv.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getChild(groupPosition, childPosition);
                ContextBean.initCheckStatus(CreateProductActivity.data.category);
                if (bean.children == null) { // 最后一级
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                mCurrentCategoryBean = null;
                adapter.notifyDataSetChanged();
                exLv.setVisibility(View.GONE);
                categoryDialog.showLeftBtn(true);
                exLv2.setVisibility(View.VISIBLE);
                // 跳入下一个ListView
                showNextExlist(exLv2, bean);
                return false;
            }
        });
        categoryDialog.showTitleLeft(0, new OnClickListener() {
            @Override
            public void onClick(View v) {
                exLv.setVisibility(View.VISIBLE);
                exLv2.setVisibility(View.GONE);
                categoryDialog.showLeftBtn(false);
                mCurrentCategoryBean = null;
            }
        });
    }

    private void showNextExlist(ExpandableListView exLv2, CategoryBean bean) {
        List<CategoryBean> groupBeans = new ArrayList<ContextBean.CategoryBean>();
        groupBeans.add(bean);
        final ChooseExAdapter adapter = new ChooseExAdapter(getActivity(), groupBeans);
        exLv2.setAdapter(adapter);
        // 展开全部
        for (int i = 0; i < groupBeans.size(); i++)
            exLv2.expandGroup(i);
        exLv2.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getGroup(groupPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(CreateProductActivity.data.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
        exLv2.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getChild(groupPosition, childPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(CreateProductActivity.data.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
    }

    public ProductBean getData() {
        ProductBean bean = new ProductBean();
        bean.productName = productNameEt.getEditableText().toString();
        bean.price = priceEt.getEditableText().toString();
        bean.mainCode = mainCodeEt.getEditableText().toString();
        bean.weight = weightEt.getEditableText().toString();
        bean.upcCode = upcCodeEt.getEditableText().toString();
        bean.snLength = serialEt.getEditableText().toString();
        bean.length = length;
        bean.width = width;
        bean.height = height;
        bean.lengthUom = lengthUom;
        bean.weightUom = weightUom;
        bean.priceUom = priceUom;
        bean.device = "2";
        if (mCurrentCategoryBean != null)
            bean.categoryId = mCurrentCategoryBean.id;
        return bean;
    }

    /**
     * 验证7位整数与3位小数
     *
     * @param str
     * @return
     */
    public boolean checkLength(String str) {
        // 如果是整数
        if (Utility.isInt(str))
            return str.length() <= 7;
        String[] strs = str.split("\\.");
        if (strs.length == 2)
            return strs[0].length() <= 7 && strs[1].length() <= 3;
        return false;
    }

    public boolean checkLWH(EditText lEt, EditText wEt, EditText hEt) {
        if (TextUtils.isEmpty(lEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), lEt);
            UIHelper.showToast(getString(R.string.pro_length_empty));
            return false;
        }
        if (!checkLength(lEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), lEt);
            UIHelper.showToast(getActivity().getString(R.string.product_maximum));
            return false;
        }

        if (TextUtils.isEmpty(wEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), wEt);
            UIHelper.showToast(getString(R.string.pro_width_empty));
            return false;
        }
        if (!checkLength(wEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), wEt);
            UIHelper.showToast(getActivity().getString(R.string.product_maximum));
            return false;
        }

        if (TextUtils.isEmpty(hEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), hEt);
            UIHelper.showToast(getString(R.string.pro_height_empty));
            return false;
        }
        if (!checkLength(hEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), hEt);
            UIHelper.showToast(getActivity().getString(R.string.product_maximum));
            return false;
        }
        return true;
    }

    public boolean checkData() {
    	colseInputMethod();
        /* 非空验证 */
        if (TextUtils.isEmpty(productNameEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), productNameEt);
            UIHelper.showToast(getString(R.string.pro_pname_empty));
            return false;
        }
        if (productNameEt.getEditableText().toString().trim().length() > 100) {
            AnimUtils.horizontalShake(getActivity(), productNameEt);
            UIHelper.showToast(getString(R.string.pro_max_digits_three));
            return false;
        }

        if (TextUtils.isEmpty(mainCodeEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), mainCodeEt);
            UIHelper.showToast(getString(R.string.pro_mcode_empty));
            return false;
        }
        if (mainCodeEt.getEditableText().toString().trim().length() > 30) {
            AnimUtils.horizontalShake(getActivity(), mainCodeEt);
            UIHelper.showToast(getString(R.string.pro_max_digits_two));
            return false;
        }

        if (TextUtils.isEmpty(lwhBtn.getText().toString().trim())) {
            AnimUtils.horizontalShake(getActivity(), lwhBtn);
            UIHelper.showToast(getString(R.string.pro_lwh_empty));
            return false;
        }

        if (TextUtils.isEmpty(weightEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), weightEt);
            UIHelper.showToast(getString(R.string.pro_weight_empty));
            return false;
        }
        if (!checkLength(weightEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), weightEt);
            UIHelper.showToast(getActivity().getString(R.string.product_maximum));
            return false;
        }

        if (TextUtils.isEmpty(priceEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), priceEt);
            UIHelper.showToast(getString(R.string.pro_price_empty));
            return false;
        }
        if (!checkLength(priceEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), priceEt);
            UIHelper.showToast(getActivity().getString(R.string.product_maximum));
            return false;
        }

        if (TextUtils.isEmpty(serialEt.getEditableText().toString())) {
            AnimUtils.horizontalShake(getActivity(), serialEt);
            UIHelper.showToast(getString(R.string.pro_seriallong_empty));
            return false;
        }
        if (serialEt.getEditableText().toString().trim().length() > 10) {
            AnimUtils.horizontalShake(getActivity(), serialEt);
            UIHelper.showToast(getString(R.string.pro_max_digits_one));
            return false;
        }

        return true;
    }

    public boolean isCheck() {
        if (TextUtils.isEmpty(productNameEt.getEditableText().toString())) {
            return false;
        }

        if (TextUtils.isEmpty(mainCodeEt.getEditableText().toString())) {
            return false;
        }

        if (TextUtils.isEmpty(lwhBtn.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(weightEt.getEditableText().toString())) {
            return false;
        }

        if (TextUtils.isEmpty(priceEt.getEditableText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(serialEt.getEditableText().toString())) {
            return false;
        }
        return true;
    }

    public OnCheckListener getOnCheckListener() {
        return onCheckListener;
    }

    public void setOnCheckListener(OnCheckListener onCheckListener) {
        this.onCheckListener = onCheckListener;
    }

    private class ContextAdapter extends BaseAdapter {

        private List<? extends ContextBean.ContextParent> datas;

        public ContextAdapter(List<? extends ContextBean.ContextParent> data) {
            datas = data;
        }

        @Override
        public int getCount() {
            return datas == null ? 0 : datas.size();
        }

        @Override
        public Object getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView tv = (TextView) View.inflate(getActivity(), android.R.layout.simple_list_item_1, null);
            tv.setTextColor(Color.BLACK);
            tv.setTextSize(12);
            tv.setText(datas.get(position).name);
            return tv;
        }
    }

    private OnCheckListener onCheckListener;

    public interface OnCheckListener {
        void onCheck();
    }
}
