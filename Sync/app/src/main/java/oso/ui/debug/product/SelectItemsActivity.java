package oso.ui.debug.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.NetConnection_YMS;
import support.network.SimpleJSONUtil;
import support.network.NetConnectionInterface.SyncJsonHandler;
import utility.HttpUrlPath;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SelectItemsActivity extends BaseActivity {

	private ListView lv;
	private ItemAdapter adapter;

	private Button finishBtn, createAllBtn;

	public static List<ProductBean> products;
	public static ContextBean contextData;

	private int dataSize, dataCount = 0;
	
	private boolean isBackWrite = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_select_items, 0);
		initData();
		initView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adapter != null)
			adapter.notifyDataSetChanged();
		final List<ProductBean> datas = newDatas();
		if (datas.isEmpty()) {
			showFinishBtn();
		} else {
			showCreateAllBtn();
		}
	}

	private void initData() {
		setTitleString(getString(R.string.product_items));
		products = (List<ProductBean>) getIntent().getSerializableExtra("products");
		contextData = (ContextBean) getIntent().getSerializableExtra("ContextData");
		isBackWrite = getIntent().getBooleanExtra("isBackWrite", true);
	}

	private void initView() {
		finishBtn = (Button) findViewById(R.id.finishBtn);
		createAllBtn = (Button) findViewById(R.id.createAllBtn);
		lv = (ListView) findViewById(R.id.lv);
		adapter = new ItemAdapter(products);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				final ProductBean bean = products.get(position);
				if (!bean.isCreateed) {
					Intent intent = new Intent(mActivity, SimpleCreateProductActivity.class);
					intent.putExtra("position", position);
					intent.putExtra("isBackWrite", isBackWrite);
					startActivityForResult(intent, 0);
				}
			}
		});
	}

	/**
	 * 获取未创建的商品
	 * 
	 * @return
	 */
	private List<ProductBean> newDatas() {
		final List<ProductBean> datas = new ArrayList<ProductBean>();
		for (ProductBean bean : products) {
			if (!bean.isCreateed && bean.isCheck)
				datas.add(bean);
		}
		return datas;
	}

	public void createAllOnClick(View v) {
		// 获取未创建的商品
		final List<ProductBean> datas = newDatas();
		if (!datas.isEmpty()) {
			dataSize = datas.size();
			RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_submit) + "?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					doSubmitAll(datas);
				}
			});
		}

	}

	private void doSubmitAll(final List<ProductBean> datas) {
		final ProductBean data = datas.get(dataCount);
		NetConnection_YMS.getThis().reqProduct_add(new SyncJsonHandler(mActivity) {
			@Override
			public void handReponseJson(JSONObject json) {
				// 回写数据
				if (isBackWrite)
					NetConnection_YMS.getThis().reqProduct_write(json.optString("id"));
				data.isCreateed = true;
				adapter.notifyDataSetChanged();
				// 还有没传完的
				if (dataCount < dataSize - 1) {
					dataCount++;
					doSubmitAll(datas);
				}
				// 完成
				else {
					RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
					dialog.setMessage(getString(R.string.sync_create_success));
					dialog.setCancelable(false);
					dialog.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dataSize = 0;
							dataCount = 0;
							final List<ProductBean> datas = newDatas();
							if (datas.isEmpty()) {
								showFinishBtn();
							} else {
								showCreateAllBtn();
							}
						}
					});
					dialog.hideCancelBtn();
					dialog.create().show();
				}
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_create_fail));
				dataSize = 0;
				dataCount = 0;
			}

		}, data.toJsonStr());
	}

	public void finishOnClick(View v) {
		finish();
	}

	private void showCreateAllBtn() {
		createAllBtn.setVisibility(View.VISIBLE);
		finishBtn.setVisibility(View.GONE);
	}

	private void showFinishBtn() {
		finishBtn.setVisibility(View.VISIBLE);
		createAllBtn.setVisibility(View.GONE);
	}

	private class ItemAdapter extends BaseAdapter {

		List<ProductBean> datas;

		public ItemAdapter(List<ProductBean> datas) {
			this.datas = datas;
		}

		@Override
		public int getCount() {
			return datas == null ? 0 : datas.size();
		}

		@Override
		public Object getItem(int position) {
			return datas.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_items_list, null);
				holder = new Holder();
				holder.tv = (TextView) convertView.findViewById(R.id.tv);
				holder.box = (CheckBox) convertView.findViewById(R.id.box);
				holder.flagTv = (TextView) convertView.findViewById(R.id.flagTv);
				holder.arrowIv = (ImageView) convertView.findViewById(R.id.arrowIv);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}

			final ProductBean bean = datas.get(position);
			holder.tv.setText(bean.productName);
			if (bean.isCreateed) {
				holder.flagTv.setText("Created");
				holder.arrowIv.setVisibility(View.INVISIBLE);
				holder.box.setVisibility(View.INVISIBLE);
			} else {
				holder.flagTv.setText("");
				holder.arrowIv.setVisibility(View.VISIBLE);
				holder.box.setVisibility(View.VISIBLE);
				holder.box.setChecked(bean.isCheck);
				holder.box.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						bean.isCheck = !bean.isCheck;
						notifyDataSetChanged();
					}
				});
			}
			return convertView;
		}

		final class Holder {
			public TextView tv;
			public CheckBox box;
			public TextView flagTv;
			public ImageView arrowIv;
		}
	}

}
