package oso.ui.debug.product.ui;

import oso.ui.debug.product.adapter.SlcTitleCusAdp;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.ContextParent;
import oso.widget.orderlistview.SideBar;
import oso.widget.orderlistview.SideBar.OnTouchingLetterChangedListener;
import utility.Utility;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
/**
 * Product商品  Sidebar滑动变更selection列表
 * @author xialimin
 *
 */
public class TitleCusOrderListView extends FrameLayout implements OnTouchingLetterChangedListener, OnScrollListener, OnItemClickListener {
	private static int mType;
	
	private TextView letterTv; // 显示点击的字母
	private SideBar sideBar; // ListView右侧导航选项卡
	private ListView lv;
	private SlcTitleCusAdp adapter;

	private ContextBean data = null;

	public TitleCusOrderListView(Context context, int type) {
		super(context);
		mType = type;
		init(context);
	}

	public TitleCusOrderListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public TitleCusOrderListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		lv = new ListView(context);
		lv.setBackgroundColor(Color.TRANSPARENT);
		lv.setCacheColorHint(Color.TRANSPARENT);
		lv.setDivider(null);
//		lv.setSelector(null);
		// 右侧导航条初始化
		sideBar = new SideBar(context);
		FrameLayout.LayoutParams sbParams = new FrameLayout.LayoutParams(Utility.pxTodip(getContext(), 30), -1, Gravity.RIGHT);
		// 中间的字母控件初始化
		letterTv = new TextView(context);
		letterTv.setMinWidth(Utility.pxTodip(context, 90));
		letterTv.setMinHeight(Utility.pxTodip(context, 90));
		letterTv.setBackgroundColor(0x40000000);
		letterTv.setTextColor(Color.WHITE);
		letterTv.setTypeface(Typeface.MONOSPACE);
		letterTv.setTextSize(Utility.pxTosp(context, 24));
		letterTv.setVisibility(View.GONE);
		letterTv.setGravity(Gravity.CENTER);
		// 添加View
		FrameLayout.LayoutParams ltParams = new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER);
		addView(lv, -1, -1);
		addView(sideBar, sbParams);
		addView(letterTv, ltParams);
		// 设置监听事件
		lv.setOnScrollListener(this);
		lv.setOnItemClickListener(this);
		sideBar.setOnTouchingLetterChangedListener(this);// 右侧导航条监听
		// initDataHandler.sendEmptyMessage(0);
	}

	public void setAdapter(ContextBean dataAll, int type) {
		if (dataAll == null) {
			return;
		}
		setType(type);
		data = dataAll;
		initDataHandler.sendEmptyMessage(0);
	}

//	public String getSelectedNamesStr() {
//		if (data == null || data.isEmpty())
//			return "";
//		String str = "";
//		for (int i = 0; i < data.size(); i++) {
//			ContextBean bean = data.get(i);
//		if (user.isSelect) {
//				str += user.getEmploye_name() + ",";
//			}
//		}
//		return str.length() > 0 ? str.substring(0, str.length() - 1) : "";
//	}


/*	public List<String> getSelectedNames() {
		String str = getSelectedNamesStr();
		System.out.println("str= " + str);
		String[] datas = TextUtils.isEmpty(str.trim()) ? new String[] {} : str.split(",");
		List<String> list = Arrays.asList(datas);
		Collections.reverse(list);
		return list;
	}*/

	/*public int getSelectedCount() {
		return getSelectedNames().size();
	}
*/
	private Handler initDataHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (adapter == null) {
				adapter = new SlcTitleCusAdp(getContext(), data, mType);
				lv.setAdapter(adapter);
			}
			adapter.notifyDataSetChanged();
		}

	};

	public static void setType(int type) {
		mType = type;
	}
	private Handler _handler = new Handler();
	private Runnable letterThread = new Runnable() {
		public void run() {
			letterTv.setVisibility(View.GONE);
		}
	};

	@Override
	public void onTouchingLetterChanged(String s) {
		if (s.equals("A")) {
			lv.setSelection(0);
			showLetter(s);
		}
		/*if (users == null) {
			return;
		}*/
		if (alphaIndexer(s) > 0) {
			int position = alphaIndexer(s);
			lv.setSelection(position);
			showLetter(s);
		}
	}

	private void showLetter(String s) {
		letterTv.setText(s);
		letterTv.setVisibility(View.VISIBLE);
		_handler.removeCallbacks(letterThread);
		_handler.postDelayed(letterThread, 1000);
	}

	// 返回滑动条所指位置的索引
	public int alphaIndexer(String s) {
		int position = 0;
		
		if(mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
			for (int i = 0; i < data.title.size(); i++) {
				if (data.title.get(i).upperCase.equals(s)) {
					position = i;
					break;
				}
			}
		} else {
			for (int i = 0; i < data.customer.size(); i++) {
				if (data.customer.get(i).upperCase.equals(s)) {
					position = i;
					break;
				}
			}
		}
		return position;
	}

	public int getSelectedItemPosition() {
		return lv.getSelectedItemPosition();
	}

	public void setSelection(int position) {
		lv.setSelection(position);
	}

	public void setEmptyView(View emptyView) {
		lv.setEmptyView(emptyView);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (getOnItemClickListener() != null) {
			switch (mType) {
			case SlcTitleCusAdp.Type_Slc_Title:
				getOnItemClickListener().onItemClick(parent, view, position, id, data.title.get(position));
				break;
				
			case SlcTitleCusAdp.Type_Slc_Update_Title:
				getOnItemClickListener().onItemClick(parent, view, position, id, data.title.get(position));
				break;
			
			case SlcTitleCusAdp.Type_Slc_Customer:
				getOnItemClickListener().onItemClick(parent, view, position, id, data.customer.get(position));
				break;
			
			case SlcTitleCusAdp.Type_Slc_Update_Customer:
				getOnItemClickListener().onItemClick(parent, view, position, id, data.customer.get(position));
				break;
			}
			
		}
	}

	boolean isStateChanged = false;

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:// 空闲状态
			isStateChanged = false;
			break;
		case OnScrollListener.SCROLL_STATE_FLING:// 滚动状态
			isStateChanged = true;
			break;
		case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:// 触摸后滚动
			isStateChanged = true;
			break;
		}
	}

	@Override
	public void onScroll(AbsListView view, final int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				if (isStateChanged) {
					if(mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) showLetter(data.title.get(firstVisibleItem).upperCase);
					else showLetter(data.customer.get(firstVisibleItem).upperCase);
				}
			}
		});
	}

	public OnItemOrderClickListener getOnItemClickListener() {
		return onItemClickListener;
	}

	public void setOnItemClickListener(OnItemOrderClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	public SlcTitleCusAdp getAdapter() {
		if(adapter == null) {
			adapter = new SlcTitleCusAdp(getContext(), data, mType);
		}
		return this.adapter;
	}
	
	private OnItemOrderClickListener onItemClickListener;

	public interface OnItemOrderClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id, ContextParent letter);
	}
	
}
