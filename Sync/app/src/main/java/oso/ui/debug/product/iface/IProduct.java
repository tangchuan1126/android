package oso.ui.debug.product.iface;
/**
 * ProductList接口
 * @author xialimin
 *
 */
public interface IProduct {
	
	/**
	 * 是否应该加载图片。 
	 * 1. 当快速滑动ListView的时候不应该加载图片，提升性能
	 * @return
	 */
	public abstract boolean isShouldShow();
}
