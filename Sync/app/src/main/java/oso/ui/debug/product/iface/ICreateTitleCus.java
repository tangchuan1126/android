package oso.ui.debug.product.iface;

public interface ICreateTitleCus {
	/**
	 * 弹出编辑Dialog
	 */
	public abstract void showEditDlg(int position);
	
	/**
	 * 删除
	 * @param position
	 */
	public abstract void showRemoveDlg(int position);
}
