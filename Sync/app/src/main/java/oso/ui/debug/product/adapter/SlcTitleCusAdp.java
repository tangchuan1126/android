package oso.ui.debug.product.adapter;

import java.util.Locale;

import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.Customer;
import oso.ui.debug.product.bean.ContextBean.Title;
import utility.Utility;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * Product---选Title Customer 数据适配器
 * @author xialimin
 * 
 * @description 构造方法中第三个参数 type, 根据需求可传入Title类型,或者Customer类型
 *
 */
public class SlcTitleCusAdp extends BaseAdapter {
	public static final int Type_Slc_Title = 0;
	public static final int Type_Slc_Customer = 1;
	public static final int Type_Slc_Update_Title = 2;
	public static final int Type_Slc_Update_Customer = 3;
	
	private Context mContext;
	private ContextBean mData;
	private int mType;
	
	public SlcTitleCusAdp(Context ctx, ContextBean bean, int type) {
		mContext = ctx;
		mData = bean;
		mType = type;
		initUpperCaseData();
	}
	
	@Override
	public int getCount() {
		if(mType == Type_Slc_Title || mType == Type_Slc_Update_Title) {
			return mData==null?0:mData.title.size();
		} else {
			return mData==null?0:mData.customer.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = View.inflate(mContext, R.layout.item_product_slc_titlecus_layout, null);
			holder.cb = (CheckBox) convertView.findViewById(R.id.cbSlcTitleCus);
			holder.nameTv = (TextView) convertView.findViewById(R.id.tvSlcTitleCusName);
			holder.arrowIv = (ImageView) convertView.findViewById(R.id.ivSlcTitleCusArr);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if(mType == Type_Slc_Customer || mType == Type_Slc_Update_Customer) {
			holder.cb.setButtonDrawable(R.drawable.checkbox_style);
		}
		
		fillData(position, holder);
		
		return convertView;
	}
	
	/**
	 * 填充数据
	 * @param position
	 * @param holder
	 */
	private void fillData(int position, ViewHolder holder) {
		if(mType == Type_Slc_Title || mType == Type_Slc_Update_Title) {
			holder.nameTv.setText(mData.title.get(position).name);
			holder.cb.setChecked(mData.title.get(position).isSelected);
		} else {
			holder.nameTv.setText(mData.customer.get(position).name);
			holder.cb.setChecked(mData.customer.get(position).isSelected);
		}
	}

	/**
	 * 初始化数据 首字母索引
	 */
	private void initUpperCaseData() {
		if(mData == null) return;
		if(mType == Type_Slc_Title || mType == Type_Slc_Update_Title) {
			
			if(Utility.isNullForList(mData.title)) return;
			for(int i=0; i<mData.title.size(); i++) {
				Title title = mData.title.get(i);
				String name = title.name;
				title.upperCase = TextUtils.isEmpty(name) ? name : name.toUpperCase(Locale.ENGLISH).substring(0, 1);
			}
			
		} else {
			
			if(Utility.isNullForList(mData.customer)) return;
			for(int i=0; i<mData.customer.size(); i++) {
				Customer customer = mData.customer.get(i);
				String name = customer.name;
				customer.upperCase = TextUtils.isEmpty(name) ? name : name.toUpperCase(Locale.ENGLISH).substring(0, 1);
			}
			
		}
	}
	
	private class ViewHolder {
		CheckBox cb;
		TextView nameTv;
		ImageView arrowIv;
	}
	
	/**
	 * 清除Title的选中状态
	 */
	public void clearAllTitleStatus() {
		for(ContextBean.Title title : mData.title) {
			if(title.isSelected) {
				title.isSelected = false;
			}
		}
	}
}

