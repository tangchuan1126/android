package oso.ui.debug.product.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContextBean implements Serializable {

    private static final long serialVersionUID = 711776736147974504L;

    public List<LengthUom> lengthUom;
    public List<Title> title;
    public List<FileType> fileType;
    public List<WeightUom> weightUom;
    public List<Customer> customer;
    public List<PriceUom> priceUom;

    public List<CategoryBean> category;

    public static class CategoryBean extends ContextParent {
        private static final long serialVersionUID = 1L;
        public List<CategoryBean> children;
        public boolean isCheck;
    }

    public static void initCheckStatus(List<CategoryBean> beans) {
        if (beans == null)
            return;
        for (CategoryBean b : beans) {
            b.isCheck = false;
            initCheckStatus(b.children);
        }
    }

    public static String[] format(List<? extends ContextParent> beans) {
        if (beans == null)
            return new String[]{};
        String[] datas = new String[beans.size()];
        for (int i = 0; i < beans.size(); i++) {
            datas[i] = beans.get(i).name;
        }
        return datas;
    }

    public static class LengthUom extends ContextParent {
        private static final long serialVersionUID = 1L;
    }

    public static class Title extends ContextParent {
        private static final long serialVersionUID = 1L;
        public boolean isSelected;
        public String upperCase;
    }

    public static class FileType extends ContextParent {
        private static final long serialVersionUID = 1L;
    }

    public static class WeightUom extends ContextParent {
        private static final long serialVersionUID = 1L;
    }

    public static class Customer extends ContextParent {
        private static final long serialVersionUID = 1L;
        public boolean isSelected;
        public String upperCase;
    }

    public static class PriceUom extends ContextParent {
        private static final long serialVersionUID = 1L;
    }


    public static abstract class ContextParent implements Serializable {
        private static final long serialVersionUID = -6118335762977370056L;
        public String id;
        public String name;
        // category用到的
        public String sort;
        public String parent;
    }


    //-------------------------------------------------

    /**
     * 用于选Title和Customer的数据返回
     *
     * @author xialimin
     */
    public static class ResultBean implements Serializable {
        private static final long serialVersionUID = 124264576108386128L;
        public List<Title> slcTitleList = new ArrayList<Title>();
        public List<Customer> slcCusList = new ArrayList<Customer>();

        /**
         * 获得全部Customer的name  以逗号分隔
         *
         * @return
         */
        public String getCustomersString() {
            String str = "";
            for (int i = 0; i < slcCusList.size(); i++) {
                str += slcCusList.get(i).name;
                if (i != slcCusList.size() - 1) {
                    str += ",";
                }
            }
            return str;
        }
    }

    /**
     * 创建Title一对多 关系条目 数据bean
     *
     * @author xialimin
     */
    public static class RopeBean implements Serializable {
        private static final long serialVersionUID = -741612630293714967L;
        public Title title;
        public List<Customer> cusList = new ArrayList<Customer>();

        /**
         * 获得全部Customer的name  以逗号分隔
         *
         * @return
         */
        public String getCusNameString() {
            String str = "";
            for (int i = 0; i < cusList.size(); i++) {
                str += cusList.get(i).name;
                if (i != cusList.size() - 1) {
                    str += ",";
                }
            }
            return str;
        }

        /**
         * 返回Customer所有的id
         *
         * @return
         */
        public String getCusIdsString() {
            String ids = "";
            for (int i = 0; i < cusList.size(); i++) {
                ids += cusList.get(i).id;
                if (i != cusList.size() - 1) {
                    ids += ",";
                }
            }
            return ids;
        }
    }
}
