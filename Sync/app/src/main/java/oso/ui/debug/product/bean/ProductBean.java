package oso.ui.debug.product.bean;

import java.io.Serializable;
import java.util.List;

import com.google.gson.Gson;

public class ProductBean implements Serializable {

	private static final long serialVersionUID = -8447719623143332570L;

	public String productName = "";
	public String length = "";
	public String width = "";
	public String height = "";
	public String lengthUom = "0";
	public String weight = "";
	public String weightUom = "0";
	public String price = "";
	public String priceUom = "0";
	public String categoryId = "0";
	public String snLength = "";
	public String mainCode = "";
	public String upcCode = "";
	public String device = "2";

	// 等同于productName
	public String productname;
	// 等同于snLength
	public String snlength;

	public List<TitleAndCustomer> tcArray;
//	public Photo file;
	
	// 是否选择
	public boolean isCheck = true;
	
	public boolean isCreateed = false;
	
//	public int is_check_sn;

	public String toJsonStr() {
		String jsonStr = new Gson().toJson(this, ProductBean.class);
		System.out.println("jsonStr= " + jsonStr);
		return jsonStr;
	}

	/**
	 * title id and customer id
	 */
	public static class TitleAndCustomer implements Serializable {
		private static final long serialVersionUID = -3059280629586358923L;
		
		public String title;
		public String customer;
	}

	/**
	 * photo
	 */
	public static class Photo implements Serializable {
		private static final long serialVersionUID = -6299972093646632654L;
		
		public String id;
		public String type;
	}

}
