package oso.ui.debug.product;

import java.util.ArrayList;
import java.util.List;

import oso.ui.debug.product.adapter.SlcTitleCusAdp;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.ResultBean;
import oso.ui.debug.product.bean.ContextBean.RopeBean;
import oso.ui.debug.product.bean.ContextBean.Title;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.debug.product.bean.ProductBean.TitleAndCustomer;
import oso.ui.debug.product.iface.ICreateTitleCus;
import oso.widget.dialog.RewriteBuilderDialog;
import support.anim.AnimUtils;
import support.common.UIHelper;
import utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * Product 添加Title--Customer 界面
 * 
 * @author xialimin
 * 
 */
public class CreateTitleCusFragment extends Fragment implements View.OnClickListener, ICreateTitleCus {

	public static final int REQ_SLC_TITLE = 0;

	public static final int REQ_SLC_CUSTOMER = 1;

	public static final int REQ_SLC_UPDATE_TITLE = 2;

	public static final int REQ_SLC_UPDATE_CUSTOMER = 3;

	private Context mContext;

	private View mRootView;

	private TextView mTitleTv, mCustomerTv;

	private Button mAddBtn;

	private ListView mTitleLv;

	private TitleAdapter mTitleAdp;

	private ContextBean mData;

	public List<RopeBean> mRopeList;

	private RopeBean mRopeBean = new RopeBean();

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return mRootView = inflater.inflate(R.layout.fragment_title_customer, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mContext = getActivity();
		initView(mRootView);
		setData();
		setListener();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (data == null)
			return;
		ContextBean.ResultBean result = (ResultBean) data.getSerializableExtra("ResultBean");
		if (result == null)
			return;
		if (mRopeBean == null)
			mRopeBean = new RopeBean();
		if (dlgRopeBean == null)
			dlgRopeBean = new RopeBean();

		switch (resultCode) {
		case REQ_SLC_TITLE:
			if (Utility.isNullForList(result.slcTitleList))
				return;
			mRopeBean.title = result.slcTitleList.get(0);

			mTitleTv.setText(result.slcTitleList.get(0).name + "");
			break;

		case REQ_SLC_CUSTOMER:
			if (Utility.isNullForList(result.slcCusList)) {
				mRopeBean.cusList.clear();
			} else {
				mRopeBean.cusList.clear();
				mRopeBean.cusList.addAll(result.slcCusList);
			}
			mCustomerTv.setText(result.getCustomersString() + "");
			break;

		case REQ_SLC_UPDATE_TITLE:
			if (Utility.isNullForList(result.slcTitleList))
				return;
			dlgRopeBean.title = result.slcTitleList.get(0);
			isUpdate_Title = true;
			tvDlgTitle.setText(result.slcTitleList.get(0).name + "");
			break;

		case REQ_SLC_UPDATE_CUSTOMER:
			if (Utility.isNullForList(result.slcCusList)) {
				dlgRopeBean.cusList.clear();
			} else {
				dlgRopeBean.cusList.clear();
				dlgRopeBean.cusList.addAll(result.slcCusList);
			}
			isUpdate_Cus = true;
			tvDlgCustomer.setText(result.getCustomersString() + "");
			break;
		}
	}

	private void initView(View view) {

		mTitleTv = (TextView) view.findViewById(R.id.tvSlcTitle);
		mCustomerTv = (TextView) view.findViewById(R.id.tvSlcCustomer);
		mAddBtn = (Button) view.findViewById(R.id.btnAddTAndCustomer);
		mTitleLv = (ListView) view.findViewById(R.id.lvTandCustomer);
	}

	private void setData() {

		mRopeList = new ArrayList<RopeBean>();
		mData = CreateProductActivity.data;
		mTitleAdp = new TitleAdapter(this);
		mTitleLv.setAdapter(mTitleAdp);

	}

	private void setListener() {

		mTitleTv.setOnClickListener(this);
		mCustomerTv.setOnClickListener(this);

		mAddBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (addTitleCustomerInfo(mRopeBean)) {
					mTitleAdp.notifyDataSetChanged();
					mTitleLv.smoothScrollToPosition(mData.title.size() - 1);
				}
			}
		});

		mTitleLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// showEditDlg(position);
				showOperateDlg(position);
			}
		});
	}

	/**
	 * 添加到ListView
	 * 
	 * @param title
	 * @param customer2
	 */
	private boolean addTitleCustomerInfo(RopeBean ropeBean) {
		if (ropeBean == null) {
			UIHelper.showToast(getString(R.string.pro_add_title_customer));
			AnimUtils.horizontalShake(mContext, mTitleTv);
			AnimUtils.horizontalShake(mContext, mCustomerTv);
			return false;
		}
		if (ropeBean.title == null || ropeBean.title.id == null) {
			AnimUtils.horizontalShake(mContext, mTitleTv);
			UIHelper.showToast(getString(R.string.pro_title_empty));
			return false;
		}
		if (Utility.isNullForList(ropeBean.cusList)) {
			UIHelper.showToast(getString(R.string.pro_customer_empty));
			AnimUtils.horizontalShake(mContext, mCustomerTv);
			return false;
		}
		if (isExists(ropeBean)) {
			UIHelper.showToast(getString(R.string.sync_already_exists));
			return false;
		}

		mRopeList.add(ropeBean);
		mRopeBean = null;
		mTitleTv.setText("");
		mCustomerTv.setText("");
		return true;
	}

	/**
	 * 判断是否已经存在
	 * 
	 * @param mTitleStr
	 * @return 参数为null "" 返回true
	 */
	private boolean isExists(RopeBean ropeBean) {
		for (RopeBean rope : mRopeList) {
			if (rope.title.id.equals(ropeBean.title.id)) {
				return true;
			}
		}
		return false;
	}

	private class TitleAdapter extends BaseAdapter {
		private ICreateTitleCus iface;

		public TitleAdapter(ICreateTitleCus _iface) {
			iface = _iface;
		}

		@Override
		public int getCount() {
			return mRopeList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = initView(convertView, holder);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			fillData(position, holder);
			// setListener(position, holder);
			return convertView;
		}

		private View initView(View convertView, ViewHolder holder) {
			convertView = View.inflate(mContext, R.layout.product_title_customer_item_layout, null);
			holder.titleTv = (TextView) convertView.findViewById(R.id.tvProduct_Title);
			holder.customerTv = (TextView) convertView.findViewById(R.id.tvProduct_Customer);
			return convertView;
		}

		private void fillData(int position, ViewHolder holder) {
			holder.titleTv.setText(mRopeList.get(position).title.name);
			holder.customerTv.setText(mRopeList.get(position).getCusNameString());
		}

		class ViewHolder {
			TextView titleTv, customerTv;
		}

	}

	public List<TitleAndCustomer> getTcArray() {
		List<TitleAndCustomer> datas = new ArrayList<ProductBean.TitleAndCustomer>();
		for (int i = 0; i < mRopeList.size(); i++) {
			TitleAndCustomer tc = new TitleAndCustomer();
			tc.title = mRopeList.get(i).title.id;
			tc.customer = mRopeList.get(i).getCusIdsString();
			datas.add(tc);
		}
		return datas;
	}

	@Override
	public void onClick(View v) {
		Intent in = new Intent(mContext, SelectCustomerActivity.class);
		switch (v.getId()) {
		case R.id.tvSlcTitle:
			SelectCustomerActivity.initParams(in, mData, SlcTitleCusAdp.Type_Slc_Title, mRopeBean);
			startActivityForResult(in, REQ_SLC_TITLE);
			break;

		case R.id.tvSlcCustomer:
			SelectCustomerActivity.initParams(in, mData, SlcTitleCusAdp.Type_Slc_Customer, mRopeBean);
			startActivityForResult(in, REQ_SLC_CUSTOMER);
			break;
		}
		getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	public boolean checkData() {
		if (mRopeList.isEmpty() && TextUtils.isEmpty(mTitleTv.getText().toString().trim())) {
			AnimUtils.horizontalShake(getActivity(), mTitleTv);
			UIHelper.showToast(getString(R.string.pro_title_empty));
			return false;
		}
		if (mRopeList.isEmpty() && TextUtils.isEmpty(mCustomerTv.getText().toString().trim())) {
			AnimUtils.horizontalShake(getActivity(), mCustomerTv);
			UIHelper.showToast(getString(R.string.pro_customer_empty));
			return false;
		}
		/* 非空验证 */
		if (mRopeList.isEmpty()) {
			AnimUtils.horizontalShake(getActivity(), mAddBtn);
			UIHelper.showToast(getString(R.string.pro_add_title_customer));
			return false;
		}
		return true;
	}

	/**
	 * 修改后更新数据
	 * 
	 * @param position
	 * @param isUpdate_Title
	 * @param isUpdate_Cus
	 */
	private void updateData(int position, boolean isUpdate_Title, boolean isUpdate_Cus) {
		RopeBean ropeBean = mRopeList.get(position);
		if (dlgRopeBean.title != null) {
			ropeBean.title = dlgRopeBean.title;
		}

		mTitleAdp.notifyDataSetChanged();
	}

	/**
	 * 判断编辑后是否有重复
	 * 
	 * @param title
	 * @return
	 */
	private boolean isRepeat(Title title) {
		for (RopeBean rope : mRopeList) {
			if (rope.title.id.equals(title.id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 重置状态
	 */
	private void reset() {
		isUpdate_Title = false;
		isUpdate_Cus = false;
		dlgRopeBean = null;
	}

	private TextView tvDlgTitle, tvDlgCustomer;
	private boolean isUpdate_Title, isUpdate_Cus;
	private RopeBean dlgRopeBean;

	@Override
	public void showEditDlg(final int position) {
		dlgRopeBean = new RopeBean();
		dlgRopeBean.title = mRopeList.get(position).title;
		dlgRopeBean.cusList = mRopeList.get(position).cusList;

		final View view = View.inflate(getActivity(), R.layout.dlg_edit_title_cus, null);
		tvDlgTitle = (TextView) view.findViewById(R.id.tvDlgSlcTitle);
		tvDlgCustomer = (TextView) view.findViewById(R.id.tvDlgSlcCustomer);

		tvDlgTitle.setText(mRopeList.get(position).title.name);
		tvDlgCustomer.setText(mRopeList.get(position).getCusNameString());

		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(getActivity());
		builder.setView(view);
		builder.setNegativeButton(R.string.sync_cancel, null);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// 没做修改 直接关闭Dialog
				if (!isUpdate_Title && !isUpdate_Cus) {
					builder.dismiss();
					return;
				}

				// 有修改 但是重复 判断 编辑后的数据是否与原来的重复
				if (isRepeat(dlgRopeBean.title)) {
					AnimUtils.horizontalShake(getActivity(), tvDlgTitle);
					UIHelper.showToast(getString(R.string.sync_already_exist));
					return;
				}

				// 有修改 更新数据
				updateData(position, isUpdate_Title, isUpdate_Cus);
				UIHelper.showToast(getString(R.string.sync_success));
				reset();
				builder.dismiss();
			}

		});
		builder.show();

		tvDlgTitle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in = new Intent(mContext, SelectCustomerActivity.class);
				SelectCustomerActivity.initParams(in, mData, SlcTitleCusAdp.Type_Slc_Update_Title, dlgRopeBean);
				startActivityForResult(in, REQ_SLC_UPDATE_TITLE);
				getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}
		});

		tvDlgCustomer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in = new Intent(mContext, SelectCustomerActivity.class);
				SelectCustomerActivity.initParams(in, mData, SlcTitleCusAdp.Type_Slc_Update_Customer, dlgRopeBean);
				startActivityForResult(in, REQ_SLC_UPDATE_CUSTOMER);
				getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}
		});
	}

	private RewriteBuilderDialog opDlg;

	public void showOperateDlg(final int position) {
		opDlg = new RewriteBuilderDialog.Builder(mContext).setTitle("Operating")
				.setNegativeButton(getString(R.string.sync_cancel), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				}).setArrowItems(new String[] { "Edit", "Delete" }, new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
						switch (itemPosition) {
						case 0: // Edit
							opDlg.dismiss();
							showEditDlg(position);
							break;
						case 1: // Delete
							opDlg.dismiss();
							showRemoveDlg(position);
							break;
						}
					}

				}).show();
	}

	@Override
	public void showRemoveDlg(final int position) {
		RewriteBuilderDialog.showSimpleDialog(mContext, getString(R.string.sync_delete_notify), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				mRopeList.remove(position);
				mTitleAdp.notifyDataSetChanged();
			}
		});

	}

}
