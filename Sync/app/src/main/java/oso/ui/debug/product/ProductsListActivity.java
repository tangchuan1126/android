package oso.ui.debug.product;

import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.debug.product.adapter.ChooseExAdapter;
import oso.ui.debug.product.adapter.ProductsListAdp;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.CategoryBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.debug.product.iface.IProduct;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class ProductsListActivity extends BaseActivity implements OnClickListener, OnRefreshListener, OnScrollListener, OnTouchListener, IProduct {
	
	private Context mContext;
	
	private ListView mProductsLv;
	
	private SwipeRefreshLayout mSwipeRefreshLayout;
	
	private View mFooterView, mHeaderFloatView;

	private View mHeader;
	
	private TextView mFooterTv;
	
	private Button mCategoryBtn;
	
	private SearchEditText mMainCodeEt, mPnameEt;
	
	private ProductsListAdp mProductsAdp;
	
	private ContextBean mCtxBean;
	
	private CategoryBean mCurrentCategoryBean;
	
	private List<ProductBean> mProductsList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_products_list, 0);
		
		applyParams();
		
		initView();
		
		initListener();
		
		initData();
	}
	

	private void applyParams() {
		mCtxBean = (ContextBean) getIntent().getSerializableExtra("ContextData");
	}


	/**
	 * 初始化视图
	 */
	private void initView() {
		
		mContext = this;
		mProductsLv = (ListView) findViewById(R.id.lv_products_list);
		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srl_product);
		
		//------------footer view-----------
		mFooterView = View.inflate(mContext, R.layout.footer_product, null);
		mFooterTv = (TextView) mFooterView.findViewById(R.id.anFooterTv);
		mFooterView.setVisibility(View.GONE);
		mFooterTv.setText("Loading...");
		mProductsLv.addFooterView(mFooterView);
		
		//-----------header view  查询扫描----
		mHeaderFloatView = findViewById(R.id.ll_product_header_float);
		mMainCodeEt = (SearchEditText) findViewById(R.id.etScanMcode);
		mMainCodeEt.setIconMode(SearchEditText.IconMode_Scan);
		mPnameEt = (SearchEditText) findViewById(R.id.etScanPname);
		mPnameEt.setIconMode(SearchEditText.IconMode_Search);
		mCategoryBtn = (Button) findViewById(R.id.btnCategory);
		mMainCodeEt.requestFocus();
		
		// 获得高度
		ViewTreeObserver viewTreeObserver = mHeaderFloatView.getViewTreeObserver();
		viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		@SuppressWarnings("deprecation")
		@Override
		public void onGlobalLayout() {
			mHeaderFloatView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			mHeader = new View(mContext);
			int headerViewHeight = mHeaderFloatView.getHeight();
			mHeader.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, headerViewHeight));
			mHeader.setBackgroundColor(Color.parseColor("#00000000"));
		    mProductsLv.addHeaderView(mHeader);
		    
		    mHeaderFloatView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
		}
		});
		
		//-----------ListView适配器---------
		mProductsList = new ArrayList<ProductBean>();
		mProductsAdp = new ProductsListAdp(this, mProductsList, this);
		mProductsLv.setAdapter(mProductsAdp);
		
		//------------数值设置-------------
		mTouchSlop = (int) (ViewConfiguration.get(mContext).getScaledTouchSlop() * 0.5);//滚动过多少距离后才开始计算是否隐藏/显示头尾元素。这里用了默认touchslop的0.5倍。
		
		//------------titlebar-----------
		((TextView) vTopbar.findViewById(R.id.topbar_title)).setText("Products");
		
	}

	/**
	 * 初始化监听器
	 */
	private void initListener() {
		mSwipeRefreshLayout.setOnRefreshListener(this);
		mProductsLv.setOnTouchListener(this);
		mProductsLv.setOnScrollListener(this);
		mCategoryBtn.setOnClickListener(this);
	}
	
	/**
	 * 初始化数据
	 */
	private void initData() {
		// Category
		
		// Product列表   getIntent() 获取
	}

	
	/**
	 * 下拉刷新
	 */
	@Override
	public void onRefresh() {
		reqRefreshData();
	}
	
	/**
	 * 刷新新数据
	 */
	private void reqRefreshData() {
		/*RequestParams params = new RequestParams();
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
			}
		}.doGet(url, params, mContext);*/
		
		mSwipeRefreshLayout.setRefreshing(true);
		mProductsLv.getHandler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mProductsList.clear();
				for(int i=0; i<20; i++) {
					mProductsList.add(new ProductBean());
				}
				mProductsAdp.notifyDataSetChanged();
				mSwipeRefreshLayout.setRefreshing(false);
			}
		}, 2000);
	}


	/**
	 * 上拉到当前数据最后一条的时候，请求下一页数据
	 */
	private void reqNextPageData() {
		mSwipeRefreshLayout.setRefreshing(true);
		mProductsLv.getHandler().postDelayed(new Runnable() {
			@Override
			public void run() {
				for(int i=0; i<20; i++) {
					mProductsList.add(new ProductBean());
				}
				mProductsAdp.notifyDataSetChanged();
				mSwipeRefreshLayout.setRefreshing(false);
			}
		}, 2000);
		/*RequestParams params = new RequestParams();
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				
			}
		}.doGet(url, params, mContext);*/
	}


	private boolean isShouldShow = true;
	@Override
	public boolean isShouldShow() {
		return isShouldShow;
	}


	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if(OnScrollListener.SCROLL_STATE_FLING == scrollState) {
			isShouldShow = false;
			// 动画是否需要隐藏
			if(isShouldHide) {
				animateHide();
			}
		} else {
			isShouldShow = true;
		}		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		int lastItemPos = firstVisibleItem + visibleItemCount - 1;
		if (view.getChildCount() > 0 && view.getFirstVisiblePosition() == 0 && (view.getChildAt(0).getTop()) >= view.getPaddingTop()) {
			animateBack();
			mSwipeRefreshLayout.setEnabled(true);
		} else {
			mSwipeRefreshLayout.setEnabled(false);
		}
		
		if(lastItemPos == mProductsList.size()) {
			mFooterView.setVisibility(View.INVISIBLE);
			reqNextPageData();
		} 	
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCategory:
			// 选中打开分类Dlg
			showChooseCategoryDialog();
			break;

		default:
			break;
		}
	}
	
	private BottomDialog categoryDialog;

    private void showChooseCategoryDialog() {
        final View dialogView = View.inflate(mContext, R.layout.dlg_choose_category, null);
        categoryDialog = new BottomDialog(mContext);
        showExlist(dialogView);
        categoryDialog.setTitle("Category");
        categoryDialog.setContentView(dialogView);
        categoryDialog.setOnSubmitClickListener(getString(R.string.sync_ok), new OnSubmitClickListener() {
            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                if (mCurrentCategoryBean == null) {
                	mCategoryBtn.setText("");
                    UIHelper.showToast(mContext, "Please select category");
                    return;
                }
                mCategoryBtn.setText(mCurrentCategoryBean.name);
                dlg.dismiss();
            }
        });
        categoryDialog.showLeftBtn(false);
        categoryDialog.show();
    }
	
	private void showExlist(View dialogView) {
        final ExpandableListView exLv = (ExpandableListView) dialogView.findViewById(R.id.exLv);
        final ExpandableListView exLv2 = (ExpandableListView) dialogView.findViewById(R.id.exLv2);
        final ChooseExAdapter adapter = new ChooseExAdapter(mContext, mCtxBean.category);
        exLv.setAdapter(adapter);
        // 展开全部
        for (int i = 0; i < mCtxBean.category.size(); i++)
            exLv.expandGroup(i);
        exLv.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getGroup(groupPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(mCtxBean.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
        exLv.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getChild(groupPosition, childPosition);
                ContextBean.initCheckStatus(mCtxBean.category);
                if (bean.children == null) { // 最后一级
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                mCurrentCategoryBean = null;
                adapter.notifyDataSetChanged();
                exLv.setVisibility(View.GONE);
                categoryDialog.showLeftBtn(true);
                exLv2.setVisibility(View.VISIBLE);
                // 跳入下一个ListView
                showNextExlist(exLv2, bean);
                return false;
            }
        });
        categoryDialog.showTitleLeft(0, new OnClickListener() {
            @Override
            public void onClick(View v) {
                exLv.setVisibility(View.VISIBLE);
                exLv2.setVisibility(View.GONE);
                categoryDialog.showLeftBtn(false);
                mCurrentCategoryBean = null;
            }
        });
    }

    private void showNextExlist(ExpandableListView exLv2, CategoryBean bean) {
        List<CategoryBean> groupBeans = new ArrayList<ContextBean.CategoryBean>();
        groupBeans.add(bean);
        final ChooseExAdapter adapter = new ChooseExAdapter(mContext, groupBeans);
        exLv2.setAdapter(adapter);
        // 展开全部
        for (int i = 0; i < groupBeans.size(); i++)
            exLv2.expandGroup(i);
        exLv2.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getGroup(groupPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(mCtxBean.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
        exLv2.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CategoryBean bean = (CategoryBean) adapter.getChild(groupPosition, childPosition);
                if (bean.children == null) { // 最后一级
                    ContextBean.initCheckStatus(mCtxBean.category);
                    bean.isCheck = true;
                    adapter.notifyDataSetChanged();
                    mCurrentCategoryBean = bean;
                    return true;
                }
                return false;
            }
        });
    }
	

//-----------------下拉隐藏Header  上拉显示Header-------------------
	
	private int mTouchSlop;
	private float lastY = 0f;
	private float currentY = 0f;
    //下面两个表示滑动的方向，大于0表示向下滑动，小于0表示向上滑动，等于0表示未滑动
	private int lastDirection = 0;
	private int currentDirection = 0;
	private boolean isShouldHide = false;
    
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		 switch (event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	        	lastY = event.getY();
	         	currentY = event.getY();
	         	currentDirection = 0;
	         	lastDirection = 0;
	         	isShouldHide = false;
	          break;
	        case MotionEvent.ACTION_MOVE:
	        	if (mProductsLv.getFirstVisiblePosition() > 0) {
	            float tmpCurrentY = event.getY();
	            if (Math.abs(tmpCurrentY - lastY) > mTouchSlop) { //滑动距离大于touchslop时才进行判断
	            	currentY = tmpCurrentY;
	            	currentDirection = (int) (currentY - lastY);
	            	if (lastDirection != currentDirection) {
	                //如果与上次方向不同，则执行显/隐动画
		                if (currentDirection < 0) {
		                	isShouldHide = true;
		                	animateHide();
		                } else {
		                	animateBack();
		                }
	            	}
	            }
	          } else {
	        	  animateBack(); // getFirstVisiblePosition == 0 的时候 必须要显示查询框
	          }
	          break;
	        case MotionEvent.ACTION_CANCEL:
	      }
	      return false;
	}
	
	  AnimatorSet backAnimatorSet;//这是显示头元素使用的动画
	  
	  private void animateBack() {
	    //清除其他动画
	    if (hideAnimatorSet != null && hideAnimatorSet.isRunning()) {
	      hideAnimatorSet.cancel();
	    }
	    if (backAnimatorSet != null && backAnimatorSet.isRunning()) {
	    } else {
	      backAnimatorSet = new AnimatorSet();
	      ObjectAnimator headerAnimator_transY = ObjectAnimator.ofFloat(mHeaderFloatView, "translationY", mHeaderFloatView.getTranslationY(), 0f);
	      List<Animator> animators = new ArrayList<Animator>();
	      animators.add(headerAnimator_transY);
	      backAnimatorSet.setDuration(400);
	      backAnimatorSet.playTogether(animators);
	      backAnimatorSet.start();
	    }
	  }
	
	AnimatorSet hideAnimatorSet;//这是隐藏头元素使用的动画
	private void animateHide() {
	    //清除其他动画
	    if (backAnimatorSet != null && backAnimatorSet.isRunning()) {
	      backAnimatorSet.cancel();
	    }
	    if (hideAnimatorSet != null && hideAnimatorSet.isRunning()) {
	    } else {
	      hideAnimatorSet = new AnimatorSet();
	      ObjectAnimator headerAnimator_transY = ObjectAnimator.ofFloat(mHeaderFloatView, "translationY", mHeaderFloatView.getTranslationY(), -mHeaderFloatView.getHeight());
	      List<Animator> animators = new ArrayList<Animator>();
	      animators.add(headerAnimator_transY);
	      hideAnimatorSet.setDuration(400);
	      hideAnimatorSet.playTogether(animators);
	      hideAnimatorSet.start();
	    }
	  }

}
