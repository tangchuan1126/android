package oso.ui.debug.product;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.debug.product.adapter.ContextAdapter;
import oso.ui.debug.product.adapter.SlcTitleCusAdp;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ContextBean.ContextParent;
import oso.ui.debug.product.bean.ContextBean.Customer;
import oso.ui.debug.product.bean.ContextBean.ResultBean;
import oso.ui.debug.product.bean.ContextBean.RopeBean;
import oso.ui.debug.product.bean.ContextBean.Title;
import oso.ui.debug.product.ui.TitleCusOrderListView;
import oso.ui.debug.product.ui.TitleCusOrderListView.OnItemOrderClickListener;
import oso.widget.TagGroup;
import oso.widget.TagGroup.OnClickTagListener;
import oso.widget.TagGroup.TagView;
import oso.widget.dialog.RewriteBuilderDialog;
import support.anim.AnimUtils;
import support.common.UIHelper;
import utility.ActivityUtils;
import utility.AllCapTransformationMethod;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public class SelectCustomerActivity extends BaseActivity implements OnClickListener {
	private int mType;
	private ContextBean mData;

	private EditText searchEt;

	private LinearLayout searchLayout;
	private ListView searchLv;
	private ContextAdapter searchAdapter;
	private TagGroup tagGroup;

	private List<Customer> searchContent;
	private List<Title> searchContentT;
	private boolean isShowSearch;
	public boolean flag = true; // 控制搜索框的点击

	private List<Customer> selectedDatas;

	private Button mSubmitBtn;
	private TitleCusOrderListView mTitleCusLv;

	// private ArrayList<ContextParent> mSlcList = new
	// ArrayList<ContextParent>();
	private ResultBean mResultBean = new ResultBean();

	private RopeBean mRopeBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_select_customer, 0);
		applyParams();
		initView();
		initListener();
		setData();

		// ---处理数据回显---
		handleReshow(mRopeBean);
	}

	/**
	 * 初始化参数
	 */
	private void applyParams() {
		mData = (ContextBean) getIntent().getSerializableExtra("ContextBean");
		mType = getIntent().getIntExtra("Type", 0);
		mRopeBean = (RopeBean) getIntent().getSerializableExtra("RopeBean");
		setTitleString("Selector");
		selectedDatas = new ArrayList<Customer>();
	}

	/**
	 * 封装Intent数据
	 * 
	 * @param in
	 * @param bean
	 */
	public static void initParams(Intent in, ContextBean bean, int type, RopeBean ropeBean) {
		in.putExtra("ContextBean", bean);
		in.putExtra("Type", type);
		in.putExtra("RopeBean", ropeBean);
	}

	private void initListener() {
		mSubmitBtn.setOnClickListener(this);
		mTitleCusLv.setOnItemClickListener(new OnItemOrderClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id, ContextParent letter) {
				if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
					// -----只能单选 先清除状态--选中后直接跳到下一界面
					mTitleCusLv.getAdapter().clearAllTitleStatus();

					ContextBean.Title title = (Title) letter;
					boolean isSelected = title.isSelected;
					title.isSelected = !isSelected;

					mResultBean.slcTitleList.add(title);
					Intent in = new Intent();
					in.putExtra("ResultBean", mResultBean);

					if (mType == SlcTitleCusAdp.Type_Slc_Title)
						setResult(CreateTitleCusFragment.REQ_SLC_TITLE, in);
					else
						setResult(CreateTitleCusFragment.REQ_SLC_UPDATE_TITLE, in);
					SelectCustomerActivity.this.finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);

					/*
					 * if (title.isSelected) {
					 * mResultBean.slcTitleList.add(title); } else {
					 * mResultBean.slcTitleList.remove(title); }
					 */

				} else {

					ContextBean.Customer customer = (Customer) letter;
					boolean isSelected = customer.isSelected;
					customer.isSelected = !(isSelected);
					if (customer.isSelected) {
						selectedDatas.add(customer);
						tagGroup.setTags(getCustomrName(selectedDatas));
						mResultBean.slcCusList.add(customer);
					} else {
						removeTagGrpByCusId(customer.id);
						tagGroup.setTags(getCustomrName(selectedDatas));
						mResultBean.slcCusList.remove(customer);
					}

				}

				mTitleCusLv.getAdapter().notifyDataSetChanged();
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSubmitSlcData:
			submitSlcData();
			break;

		}
	}

	/**
	 * 结束选择 setResult返回上一界面
	 */
	private void submitSlcData() {
		Intent in = new Intent();
		in.putExtra("ResultBean", mResultBean);
		switch (mType) {
		case SlcTitleCusAdp.Type_Slc_Title:
			setResult(CreateTitleCusFragment.REQ_SLC_TITLE, in);
			break;

		case SlcTitleCusAdp.Type_Slc_Customer:
			mResultBean.slcCusList.clear();
			mResultBean.slcCusList.addAll(selectedDatas);
			setResult(CreateTitleCusFragment.REQ_SLC_CUSTOMER, in);
			break;

		case SlcTitleCusAdp.Type_Slc_Update_Title:
			/*
			 * //如果没选择Title 给出提示
			 * if(Utility.isNullForList(mResultBean.slcTitleList)) {
			 * mSubmitBtn.startAnimation(shakeAnimation(5));
			 * UIHelper.showToast("Empty!"); return; }
			 */
			setResult(CreateTitleCusFragment.REQ_SLC_UPDATE_TITLE, in);
			break;

		case SlcTitleCusAdp.Type_Slc_Update_Customer:
			mResultBean.slcCusList.clear();
			mResultBean.slcCusList.addAll(selectedDatas);

			// 如果没选择Customer 给出提示
			if (Utility.isNullForList(mResultBean.slcCusList)) {
				UIHelper.showToast(getString(R.string.pro_customer_empty));
				AnimUtils.horizontalShake(mActivity, mSubmitBtn);
				return;
			}
			setResult(CreateTitleCusFragment.REQ_SLC_UPDATE_CUSTOMER, in);
			break;
		}

		this.finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

	private void initView() {
		searchEt = (EditText) findViewById(R.id.et);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
		tagGroup = (TagGroup) findViewById(R.id.tagGroup);
		searchEt.setTransformationMethod(AllCapTransformationMethod.getThis());

		mSubmitBtn = (Button) findViewById(R.id.btnSubmitSlcData);
		mTitleCusLv = (TitleCusOrderListView) findViewById(R.id.lvSlcTitleCus);
		mTitleCusLv.setAdapter(mData, mType);

		if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
			mSubmitBtn.setVisibility(View.GONE);
			searchEt.setHint("Title");
		} else {
			mSubmitBtn.setVisibility(View.VISIBLE);
			searchEt.setHint("Customer");
		}
		((TextView) vTopbar.findViewById(R.id.topbar_title))
				.setText((mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) ? R.string.product_slc_title
						: R.string.product_slc_customer);
	}

	private void setData() {
		if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
			searchContentT = new LinkedList<Title>();
			searchAdapter = new ContextAdapter(mActivity, searchContentT);
		} else {
			searchContent = new LinkedList<Customer>();
			searchAdapter = new ContextAdapter(mActivity, searchContent);
		}
		searchLv.setAdapter(searchAdapter);
		searchEt.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return searchOnTouch(v, event);
			}
		});
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		searchLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

				// 点击Item 如果是Title 则直接选中返回上一界面[因为Title只能选一个]
				if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
					Title title = searchContentT.get(position);
					mResultBean.slcTitleList.add(title);
					Intent in = new Intent();
					in.putExtra("ResultBean", mResultBean);

					if (mType == SlcTitleCusAdp.Type_Slc_Title)
						setResult(CreateTitleCusFragment.REQ_SLC_TITLE, in);
					else
						setResult(CreateTitleCusFragment.REQ_SLC_UPDATE_TITLE, in);
					SelectCustomerActivity.this.finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);

					hideSearch();
					searchEt.setText("");
				}

				// --------Customer--------
				else {
					Customer customer = searchContent.get(position);
					if (isRepeat(selectedDatas, customer)) {
						UIHelper.showToast(getString(R.string.sync_repeat));
						return;
					}

					selectedDatas.add(customer);
					// 更新图标状态
					updateAddCus(customer.id);

					tagGroup.setTags(getCustomrName(selectedDatas));
					hideSearch();
					searchEt.setText("");
				}

			}

		});
		tagGroup.setOnClickTagListener(new OnClickTagListener() {
			@Override
			public void onClick(TagView tag, final int position) {
				RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_delete_notify) + tag.getText().toString().trim() + "?",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								updateRemoveCus(selectedDatas.get(position).id);
								selectedDatas.remove(position);
								tagGroup.setTags(getCustomrName(selectedDatas));
							}
						});
			}
		});

	}

	/**
	 * 处理数据回显
	 */
	private void handleReshow(RopeBean ropeBean) {
		if (ropeBean == null)
			return;

		if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
			if (ropeBean.title == null)
				return;

			reshowTitle(ropeBean.title);

		} else {

			if (Utility.isNullForList(ropeBean.cusList))
				return;

			reshowAddCus(ropeBean.cusList);
		}

		// mTitleCusLv.getAdapter().notifyDataSetChanged();
	}

	/**
	 * OrderListView中取消状态，TagGroup中也需要删除
	 * 
	 * @param cusId
	 */
	private void removeTagGrpByCusId(String cusId) {
		int index = -1;
		for (int i = 0; i < selectedDatas.size(); i++) {
			if (cusId.equals(selectedDatas.get(i).id)) {
				index = i;
				break;
			}
		}

		if (index >= 0) {
			selectedDatas.remove(index);
		}
	}

	/**
	 * Title数据回显
	 * 
	 * @param title
	 */
	private void reshowTitle(Title title) {
		for (int i = 0; i < mData.title.size(); i++) {
			if (title.id.equals(mData.title.get(i).id)) {
				mData.title.get(i).isSelected = true;
			}
		}
	}

	/**
	 * 数据回显 根据customerId 1.ListView
	 * 
	 * @param cusId
	 */
	private void reshowAddCus(List<Customer> cusList) {

		for (int i = 0; i < cusList.size(); i++) {
			for (int j = 0; j < mData.customer.size(); j++) {
				Customer currCus = mData.customer.get(j);
				if (cusList.get(i).id.equals(currCus.id)) {
					currCus.isSelected = true;
					selectedDatas.add(currCus);
					tagGroup.setTags(getCustomrName(selectedDatas));
					break;
				}
				currCus = null;
			}
		}

		mResultBean.slcCusList.clear();
		mResultBean.slcCusList.addAll(selectedDatas);

	}

	/**
	 * 添加后更新图标状态
	 * 
	 * @param customer
	 */
	private void updateAddCus(String cusId) {
		for (Customer cus : mData.customer) {
			if (cus.id.equals(cusId)) {
				cus.isSelected = true;
			}
		}
		mTitleCusLv.getAdapter().notifyDataSetChanged();
	}

	/**
	 * 删除后更新图标状态
	 * 
	 * @param customer
	 */
	private void updateRemoveCus(String cusId) {
		for (Customer cus : mData.customer) {
			if (cus.id.equals(cusId)) {
				cus.isSelected = false;
			}
		}
		mTitleCusLv.getAdapter().notifyDataSetChanged();
	}

	private boolean isRepeat(List<ContextBean.Customer> datas, Customer customer) {
		if (datas == null)
			return false;
		for (Customer bean : datas) {
			if (bean.name.equals(customer.name)) {
				return true;
			}
		}
		return false;
	}

	private String[] getCustomrName(List<Customer> list) {
		List<String> datas = new ArrayList<String>();
		for (Customer bean : list) {
			datas.add(bean.name);
		}
		return datas.toArray(new String[] {});
	}

	private boolean searchOnTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (v.getId() == R.id.searchEt) {
				if (flag) {
					listAnimation(true, searchLayout);
					flag = false;
				}
			}
			break;
		}
		return false;
	}

	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();

			if (mType == SlcTitleCusAdp.Type_Slc_Title || mType == SlcTitleCusAdp.Type_Slc_Update_Title) {
				searchContentT.clear();
				for (Title bean : CreateProductActivity.data.title) {
					if (bean.name.startsWith(searchStr.toLowerCase()) || bean.name.startsWith(searchStr.toUpperCase())) {
						searchContentT.add(bean);
					}
				}
			}

			else {
				searchContent.clear();
				for (Customer bean : CreateProductActivity.data.customer) {
					if (bean.name.startsWith(searchStr.toLowerCase()) || bean.name.startsWith(searchStr.toUpperCase())) {
						searchContent.add(bean);
					}
				}
			}

			searchAdapter.notifyDataSetChanged();
		} else {
			hideSearch();
		}
	}

	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
		isShowSearch = true;
		searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		isShowSearch = false;
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}

	public void listAnimation(boolean listFlag, View view) {
		AnimationSet mAnimationSet = new AnimationSet(true);
		if (listFlag) {
			TranslateAnimation translate = new TranslateAnimation(0, 0, ActivityUtils.getScreenHeight(mActivity), 0);
			translate.setFillAfter(true);
			translate.setDuration(400);
			mAnimationSet.addAnimation(translate);
			mAnimationSet.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					showSearch();
				}
			});

		} else {
			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, ActivityUtils.getScreenHeight(mActivity));
			translate.setFillAfter(true);
			translate.setDuration(150);
			mAnimationSet.addAnimation(translate);
			mAnimationSet.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					hideSearch();
				}
			});
		}
		view.startAnimation(mAnimationSet);
	}

}
