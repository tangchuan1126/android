package oso.ui.debug.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.base.BaseFramentActivity;
import oso.base.PhotoCheckable;
import oso.ui.debug.product.bean.ContextBean;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.VpWithTab;
import oso.widget.VpWithTab.OnVpPageChangeListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.key.TTPKey;
import support.network.NetConnectionInterface.SyncJsonHandler;
import support.network.NetConnection_YMS;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CreateProductActivity extends BaseFramentActivity implements OnClickListener {

	private SingleSelectBar tab;
	private VpWithTab vp;

	private FragmentManager fm;
	private BasicInfoFragment basicInfoFragment;
	private CreateTitleCusFragment titleAndCustomerFragment;
	private TabToPhoto ttp;
	private Button nextBtn, submitBtn;

	public static final int resultCode = 0xf0;

	public static ContextBean data = null;

	private String pc_id = null;
	private ProductBean returnData = null;

	private boolean isBackWrite = true;

	// Product Name     MainCode
	protected String mProductName;
	protected String mMainCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_create_product, 0);
		initData();
		initView();
	}

	private void initData() {
		setTitleString(R.string.pro_create_product);
		fm = getSupportFragmentManager();
		data = (ContextBean) getIntent().getSerializableExtra("ContextData");
		isBackWrite = getIntent().getBooleanExtra("isBackWrite", true);
		mProductName = getIntent().getStringExtra("ProductName");
		mMainCode = getIntent().getStringExtra("MainCode");
		returnData = new ProductBean();
	}

	private void initView() {
		tab = (SingleSelectBar) findViewById(R.id.tab);
		vp = (VpWithTab) findViewById(R.id.vp);

		basicInfoFragment = (BasicInfoFragment) fm.findFragmentById(R.id.basicInfoFragment);
		titleAndCustomerFragment = (CreateTitleCusFragment) fm.findFragmentById(R.id.titleAndCustomerFragment);
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		nextBtn = (Button) findViewById(R.id.nextBtn);
		submitBtn.setOnClickListener(this);
		nextBtn.setOnClickListener(this);
		// 初始化ttp
		ttp.init(mActivity, getTabParamList(), false);
		// 初始化tab
		List<HoldDoubleValue<String, Integer>> list = new ArrayList<HoldDoubleValue<String, Integer>>();
		list.add(new HoldDoubleValue<String, Integer>("Basic Info", 0));
		list.add(new HoldDoubleValue<String, Integer>("Title", 1));
		list.add(new HoldDoubleValue<String, Integer>("Photo", 2));
		tab.setUserDefineClickItems(list);
		// 初始化vp
		vp.init(tab);
		vp.setOnVpPageChangeListener(new OnVpPageChangeListener() {
			@Override
			public void onPageChange(int index) {
				isShowSubmitBtn(index == 2);
				basicInfoFragment.colseInputMethod();
				if (!basicInfoFragment.checkData()) {
					vp.setCurrentItem(0);
					isShowSubmitBtn(false);
					return;
				}
				if (!titleAndCustomerFragment.checkData()) {
					if (index == 2)
						vp.setCurrentItem(1);
					isShowSubmitBtn(false);
					return;
				}
			}
		});
		tab.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				if (!basicInfoFragment.checkData()) {
					tab.userDefineSelectIndex(0);
					vp.setCurrentItem(0);
				} else if (!titleAndCustomerFragment.checkData()) {
					if (selectValue.b == 2) {
						tab.userDefineSelectIndex(1);
						vp.setCurrentItem(1);
					} else if (selectValue.b < vp.getChildCount()) {
						vp.setCurrentItem(selectValue.b);
					}
				} else if (selectValue.b < vp.getChildCount()) {
					vp.setCurrentItem(selectValue.b);
				}
			}
		});
	}

	private void isShowSubmitBtn(boolean isShow) {
		submitBtn.setVisibility(isShow ? View.VISIBLE : View.GONE);
		nextBtn.setVisibility(isShow ? View.GONE : View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submitBtn:
			RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.sync_submit) + "?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					doSubmit();
				}
			});
			break;

		case R.id.nextBtn:
			if (vp.getCurrentItem() >= 2)
				break;
			if (!basicInfoFragment.checkData()) {
				vp.setCurrentItem(0);
				break;
			}
			if (!titleAndCustomerFragment.checkData()) {
				vp.setCurrentItem(1);
				break;
			}
			vp.setCurrentItem(vp.getCurrentItem() + 1);
			break;

		default:
			break;
		}
	}

	private void doSubmit() {
		if (!basicInfoFragment.checkData()) {
			return;
		}
		/* 取得数据 */
		returnData = basicInfoFragment.getData();
		returnData.tcArray = titleAndCustomerFragment.getTcArray();
		// returnData.snLength = photoFragment

		NetConnection_YMS.getThis().reqProduct_add(new SyncJsonHandler(mActivity) {
			@Override
			public void handReponseJson(JSONObject json) {

				pc_id = json.optString("id");

				// 回写数据
				if (isBackWrite)
					NetConnection_YMS.getThis().reqProduct_write(pc_id);

				RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
				dialog.setMessage(getString(R.string.sync_create_success));
				dialog.setCancelable(false);
				dialog.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent data = new Intent();
						if (!TextUtils.isEmpty(pc_id)) {
							try {
								data.putExtra("pc_id", Long.parseLong(pc_id));
							} catch (Exception e) {
							}
						}
						data.putExtra("product", returnData);
						setResult(resultCode, data);
						finish();
					}
				});
				dialog.hideCancelBtn();
				dialog.create().show();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (!TextUtils.isEmpty(json.optString(CCT_Util.ERR_DATA, ""))) {
					CCT_Util.showMessage(json, CCT_Util.ERR_DATA);
					return;
				}
				UIHelper.showToast(getString(R.string.sync_create_fail));
			}

		}, returnData.toJsonStr());

	}

	// 点返回按钮、返回键时
	protected void onBackBtnOrKey() {
		Intent data = new Intent();
		if (!TextUtils.isEmpty(pc_id)) {
			data.putExtra("pc_id", Long.parseLong(pc_id));
		}
		if(returnData!=null){
			data.putExtra("product", returnData);
		}
		setResult(resultCode, data);
		finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

	/**
	 * 切换页面的重载，优化了fragment的切换
	 */
	// public void switchFragment(Fragment from, Fragment to) { 1600*
	// if (from == null || to == null)
	// return;
	// FragmentTransaction transaction =
	// getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.tran_pre_in,
	// R.anim.tran_pre_out);
	// if (!to.isAdded()) {
	// // 隐藏当前的fragment，add下一个到Activity中
	// transaction.hide(from).add(R.id.fl_main, to).commit();
	// } else {
	// // 隐藏当前的fragment，显示下一个
	// transaction.hide(from).show(to).commit();
	// }
	// }
	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key0 = TTPKey.getCreateProductKey("SKU", 0);
		String key1 = TTPKey.getCreateProductKey("PKG", 1);
		String key2 = TTPKey.getCreateProductKey("BTM", 2);
		String key3 = TTPKey.getCreateProductKey("LBL", 3);
		String key4 = TTPKey.getCreateProductKey("WGT", 4);
		params.add(new TabParam("SKU", key0, new PhotoCheckable(0, "SKU", ttp)));
		params.add(new TabParam("PKG", key1, new PhotoCheckable(1, "PKG", ttp)));
		params.add(new TabParam("BTM", key2, new PhotoCheckable(2, "BTM", ttp)));
		params.add(new TabParam("LBL", key3, new PhotoCheckable(3, "LBL", ttp)));
		params.add(new TabParam("WGT", key4, new PhotoCheckable(4, "WGT", ttp)));
		// params.get(0).setWebImgsParams(file_with_class, file_with_type,
		// file_with_id)
		// params.get(0).setWebImgsParams("", "", "");
		// params.get(1).setWebImgsParams("", "", "");
		// params.get(2).setWebImgsParams("", "", "");
		// params.get(3).setWebImgsParams("", "", "");
		// params.get(4).setWebImgsParams("", "", "");
		return params;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp == null)
			return;
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 *
	 * @param c
	 * @param isBackWrite
	 *            是否向WMS回写数据
	 */
	public static void toThis(final Context c, final boolean isBackWrite) {
		toThis(c, isBackWrite, resultCode);
	}

	/**
	 * 
	 * @param c
	 * @param isBackWrite
	 *            是否向WMS回写数据
	 */
	public static void toThis(final Context c, final boolean isBackWrite,final int requestCode) {
		NetConnection_YMS.getThis().reqProduct_context(new SyncJsonHandler(c) {
			@Override
			public void handReponseJson(JSONObject json) {
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
					return;
				}
				ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
				}.getType());
				Intent intent = new Intent(c, CreateProductActivity.class);
				intent.putExtra("ContextData", data);
				intent.putExtra("isBackWrite", isBackWrite);
				((Activity) c).startActivityForResult(intent, requestCode);
			}
		});
	}

	/**
	 *
	 * @param c
	 * @param isBackWrite
	 *            是否向WMS回写数据
	 */
	public static void toThis(final Context c, final boolean isBackWrite,final int requestCode, final String productName, final String mainCode) {
		NetConnection_YMS.getThis().reqProduct_context(new SyncJsonHandler(c) {
			@Override
			public void handReponseJson(JSONObject json) {
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
					return;
				}
				ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
				}.getType());
				Intent intent = new Intent(c, CreateProductActivity.class);
				intent.putExtra("ContextData", data);
				intent.putExtra("isBackWrite", isBackWrite);
				intent.putExtra("ProductName", productName);
				intent.putExtra("MainCode", mainCode);
				((Activity) c).startActivityForResult(intent, requestCode);
			}
		});
	}

	/**
	 * 获取ContextBean，跳转到本页
	 *
	 * @param c
	 * @param product
	 */
	public static void toThis(final Context c, final ProductBean product) {
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (json.optInt("err") == 90) {
					UIHelper.showToast(json.optString("data"));
					return;
				}
				ContextBean data = new Gson().fromJson(json.toString(), new TypeToken<ContextBean>() {
				}.getType());
				Intent intent = new Intent(c, CreateProductActivity.class);
				intent.putExtra("ContextData", data);
				intent.putExtra("product", product);
				((Activity) c).startActivityForResult(intent, resultCode);
			}
		}.doGet(HttpUrlPath.product_context, new RequestParams(), c);
	}

}
