package oso.ui.chat.activity;

import java.util.ArrayList;
import java.util.List;

import oso.base.BaseActivity;
import oso.ui.chat.bean.ChatOtherBean;
import oso.ui.chat.bean.ChatOtherImageBean;
import oso.ui.chat.util.ChatUtil;
import oso.ui.main.bean.UserAvatar;
import oso.widget.DlgImg;
import oso.widget.HorizontalListView;
import oso.widget.photo.RecycleImageView;
import utility.DisplayPictureUtil;
import utility.HttpUrlPath;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

public class ChatAccountActivity extends BaseActivity {
//	private static final String LOCAL_IMG_BASEURL = "file://";
	private static final int AVATAR_DEFAULT = 1;    // 默认头像
	private static final int AVATAR_DEFAULT_CLEAR = 0; // 非默认头像
	private HorizontalListView lvPhoto;
	private AvatarAdapter avatarAdp = new AvatarAdapter();
	private List<UserAvatar> avatarUrlList = new ArrayList<UserAvatar>();
	private String name;
	String alias;
	private TextView no_photo_txt;
	private ChatOtherBean otherbean;
	private List<ChatOtherImageBean> listBean = new ArrayList<ChatOtherImageBean>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_account, 0);
		setTitleString(getString(R.string.account_setting_title_user_info));
		initData();
		initView();
		
	}
	
	private void initData() {
		otherbean = (ChatOtherBean) getIntent().getSerializableExtra(
				"chatotherbean");
		name = getIntent().getStringExtra("name");
		alias = ChatUtil.findAliasByUsername(name);
		no_photo_txt = (TextView) findViewById(R.id.no_photo_txt);
		setTitleString(alias);
		showAvatar();
	}

	private void initView() {
		
		lvPhoto = (HorizontalListView) findViewById(R.id.photoLv);
		lvPhoto.setAdapter(avatarAdp);
		lvPhoto.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String avatarUrl = HttpUrlPath.basePath+avatarUrlList.get(position - 1).getAvatarUrl();
				//String avatarId = avatarUrlList.get(position-1).getId();
				DlgImg.show(ChatAccountActivity.this,  avatarUrl);
			}

		});
		((TextView) findViewById(R.id.etFullname)).setText(alias);
		((TextView) findViewById(R.id.tvStorage)).setText(otherbean.warename);
		((TextView) findViewById(R.id.tvWarename)).setText(otherbean.warename+": ");
		((TextView) findViewById(R.id.tvRole)).setText(otherbean.postname);
		
	}
	
	/**
	 * 显示头像
	 */
	private void showAvatar() {
		listBean = otherbean.listBean;
		if(listBean != null && listBean.size() > 0){
		for(int i =0;i<listBean.size();i++){
			ChatOtherImageBean bean = listBean.get(i);
			if(bean.activity == 1){
				avatarUrlList.add(0, new UserAvatar(bean.id+"", bean.file_path, AVATAR_DEFAULT));
			}else{
				avatarUrlList.add(new UserAvatar(bean.id+"", bean.file_path, AVATAR_DEFAULT_CLEAR));
			}
		}
			no_photo_txt.setVisibility(View.GONE);
		}else{
			no_photo_txt.setVisibility(View.VISIBLE);
		}
		 
		avatarAdp.notifyDataSetChanged();
	}
	private class AvatarAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return avatarUrlList.size()+1;  // 第一个是加号
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolders holder = null;
			if(convertView == null) {
				holder = new ViewHolders();
				convertView = View.inflate(ChatAccountActivity.this, R.layout.account_set_avatar_item_layout, null);
				holder.ivAvatar = (RecycleImageView) convertView.findViewById(R.id.ivAvatar);
				holder.ivAddPhoto = (ImageView) convertView.findViewById(R.id.ivAddPhoto);
				holder.tvFlag = (TextView) convertView.findViewById(R.id.tvFlag);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolders) convertView.getTag();
			}
			
			holder.ivAddPhoto.setVisibility(View.GONE);
			if(position >= 1) {
				boolean isDefaultAvatar = (AVATAR_DEFAULT == avatarUrlList.get(position-1).activity);
				holder.tvFlag.setVisibility(isDefaultAvatar?View.VISIBLE:View.INVISIBLE);
				holder.ivAvatar.setVisibility(View.VISIBLE);
				ImageLoader.getInstance().displayImage(HttpUrlPath.basePath+avatarUrlList.get(position-1).getAvatarUrl(),
						holder.ivAvatar,
						DisplayPictureUtil.getAvatarDisplayImageOptions());
			} else {
				holder.tvFlag.setVisibility(View.GONE);
				holder.ivAvatar.setVisibility(View.GONE);
			}
			
			return convertView;
		}
	}
	class ViewHolders {
		public RecycleImageView ivAvatar;
		public TextView tvFlag;
		public ImageView ivAddPhoto;
	}

}
