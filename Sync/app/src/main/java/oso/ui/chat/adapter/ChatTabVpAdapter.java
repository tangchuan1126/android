package oso.ui.chat.adapter;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
/**
 * 
 * ChatTabViewPagerAdapter
 * @author xialimin
 *
 */
public class ChatTabVpAdapter extends FragmentPagerAdapter {
	ArrayList<Fragment> fragments = new ArrayList<Fragment>();
	
	public ChatTabVpAdapter(FragmentManager fm) {
		super(fm);
	}
	
	public ChatTabVpAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
		super(fm);
		this.fragments = fragments;
	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

}
