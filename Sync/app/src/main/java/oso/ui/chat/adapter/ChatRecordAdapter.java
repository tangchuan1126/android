package oso.ui.chat.adapter;

import java.util.List;

import org.jivesoftware.smack.util.StringUtils;

import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.chat.ext.sipdroid.sipua.ui.Receiver;
import oso.widget.DlgImg;
import oso.widget.photo.RecycleImageView;
import support.dbhelper.StoredData;
import utility.AudioMnger;
import utility.DisplayPictureUtil;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

public class ChatRecordAdapter extends BaseAdapter {
	private final int Index_Mine=0;
	private final int Index_Peer=1;
	private static int EVERY_PAGE_CNT = 0;
	private List<ChatMessage> chatRecordlist;
	private Context context;
	public String curPlayedAudio_msgId;			//正在播放的音频
	public static String avatarUrlMine = "";
	public static String avatarUrlPeer = "";
	//==================inner===============================

	public ChatRecordAdapter(Context context, List<ChatMessage> list, int evertPageCnt, String avatarUrlPeer) {
		this.chatRecordlist = list;
		this.context = context;
		EVERY_PAGE_CNT = evertPageCnt;
		avatarUrlMine = StoredData.getCurrUserAvatarUrl();
		ChatRecordAdapter.avatarUrlPeer = avatarUrlPeer;
	}

	@Override
	public int getCount() {
		return chatRecordlist.size();
	}
	
	/* 
	 * return Index_x
	 * (non-Javadoc)
	 * @see android.widget.BaseAdapter#getItemViewType(int)
	 */
	@Override
	public int getItemViewType(int position) {
		return chatRecordlist.get(position).isMine()?Index_Mine:Index_Peer;
	}
	
	@Override
	public int getViewTypeCount() {
		return  2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, getItemViewType(position)==Index_Peer?
					R.layout.chat_item_left_layout:R.layout.chat_item_right_layout,
					null);
			holder = new ViewHolder();
			holder.imgAvatar = (ImageView) convertView.findViewById(R.id.icon);
			holder.loContent = convertView
					.findViewById(R.id.loContent);
			holder.tvBody = (TextView) convertView
					.findViewById(R.id.chat_message_body_mine);
			holder.img = (RecycleImageView) convertView
					.findViewById(R.id.img);
			holder.imgAudio=(ImageView)convertView
					.findViewById(R.id.imgAudio);
			holder.tvAudioDuration=(TextView)convertView
					.findViewById(R.id.tvAudioDuration);
			
			holder.tvTime = (TextView) convertView.findViewById(R.id.datetime);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final ChatMessage chatMessage = chatRecordlist.get(position);
		// 如果当前聊天回合是自己/对方 第一次说话(或者是刷新时的第一条需要显示)，则显示时间框
		boolean showTime = getShowTimeBoolean(position);
		holder.tvTime.setVisibility(showTime?View.VISIBLE:View.GONE);
		
		//======================头像注释部分===========================
		// 头像显示 url
		if(chatRecordlist.get(position).isMine() && !TextUtils.isEmpty(avatarUrlMine)) {
			ImageLoader.getInstance().displayImage(HttpUrlPath.basePath+avatarUrlMine, 
					holder.imgAvatar, 
					DisplayPictureUtil.getAvatarDisplayImageOptions());
		} 
		
		if(!chatRecordlist.get(position).isMine() && !TextUtils.isEmpty(avatarUrlPeer)) {
			ImageLoader.getInstance().displayImage(HttpUrlPath.basePath+avatarUrlPeer, 
					holder.imgAvatar, 
					DisplayPictureUtil.getAvatarDisplayImageOptions());
		}
		//======================头像注释部分===========================
		
		holder.tvTime.setText(FormatterUtil.getFormatTime(chatMessage.getMessageTime()));
		
		showSpecificView(chatMessage, holder);
		
		//监听事件
		holder.loContent.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClickItem(chatMessage,holder);
			}
		});
		return convertView;
	}
	
	/**
	 * 当前Position是否应该显示时间
	 * @param position
	 * @return
	 */
	private boolean getShowTimeBoolean(int position) {
		int flag = chatRecordlist.size() % EVERY_PAGE_CNT;
		if(position == 0) {
			return true;
		}
		if(position % EVERY_PAGE_CNT == flag) {
			return true;
		}
		if(position % EVERY_PAGE_CNT == EVERY_PAGE_CNT - 1) {
			return true;
		}
		return false;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	//==================inner===============================
	
	/**
	 * 点击条目时
	 * @param msg
	 * @param holder
	 */
	private void onClickItem(ChatMessage msg,ViewHolder holder){
		//音频
		if (msg.isAudio()) {
			curPlayedAudio_msgId=msg.getId();
			notifyDataSetChanged();
			
			AudioMnger.getThis().playRec(msg.getMediaUrl(),onComp_audio,context);
		}
		//图片
		else if(msg.isImg())
			DlgImg.show(context,  msg.getMediaUrl());
		
		// 电话
		else if(msg.isCall()) 
			reCall();
	}
	
	private void reCall() {
		String peername = ChatActivity.chatUserName;
		Receiver.engine(context).call(StringUtils.parseName(peername), false);
	}

	private MediaPlayer.OnCompletionListener onComp_audio=new MediaPlayer.OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			curPlayedAudio_msgId="";
			notifyDataSetChanged();
		}
	};

	

	/**
	 * 据消息类型,显示给定view
	 */
	private void showSpecificView(ChatMessage msg,ViewHolder holder){
		holder.tvBody.setVisibility(View.GONE);
		holder.img.setVisibility(View.GONE);
		holder.imgAudio.setVisibility(View.GONE);
		holder.tvAudioDuration.setVisibility(View.GONE);
		
		//文本
		if(msg.isText()){
			holder.tvBody.setVisibility(View.VISIBLE);
			holder.tvBody.setText(msg.getMessageBody());
		}
		//图片
		else if(msg.isImg()){
			holder.img.setVisibility(View.VISIBLE);
			holder.img.setImageUri(msg.getMediaUrl());
		}
		//音频
		else if(msg.isAudio()){
			holder.imgAudio.setVisibility(View.VISIBLE);
			holder.tvAudioDuration.setVisibility(View.VISIBLE);
			
			holder.tvAudioDuration.setText(msg.getDuration() + "\"");
			LayoutParams lp =holder.imgAudio.getLayoutParams();
			int minLen=Utility.dip2px(context, 50);
			int maxLen=Utility.dip2px(context, 170);
			int durLen=maxLen-minLen;
			int len=LayoutParams.WRAP_CONTENT;
			if(msg.getDuration()>0)
				len=(int)(((float)durLen/60f)*msg.getDuration()+minLen);
			if(len>maxLen)
				len=maxLen;
				
			lp.width=len;
			//播放状态
			boolean isPlaying=msg.getId().equals(curPlayedAudio_msgId);
			holder.imgAudio.setSelected(isPlaying);
		} else if(msg.isCall()) {
			holder.tvBody.setVisibility(View.VISIBLE);
			String body = msg.getMessageBody();
			int callType = Integer.valueOf(body.substring(body.length()-1));
			holder.tvBody.setText(callType == 0 ? context.getResources().getString(R.string.call_missed) : msg.isMine() ? 
					context.getResources().getString(R.string.call_outgoing) : context.getResources().getString(R.string.call_incoming));
		}
		
	}
	
	//==================nested===============================

	private static class ViewHolder {
		ImageView imgAvatar;
		TextView tvBody; // 消息内容
		TextView tvTime; // 消息发送或接收时间
		RecycleImageView img;
		ImageView imgAudio;
		TextView tvAudioDuration;
		View loContent;
	}
}
