package oso.ui.chat.bean;

import java.io.Serializable;

import org.json.JSONObject;

import android.text.TextUtils;

/**
 * Chat  单条消息对象
 * @author xialimin
 */
public class ChatMessage implements Serializable {
	public static final String Type_Audio="[audio]";		//媒体类型
	public static final String Type_Img="[img]";		//媒体类型
	public static final String Type_Text="[text]";
	public static final String Type_Call="[call]";   // 网络电话
	
	private static final long serialVersionUID = 4603372667736463643L;
	private String id;
	private String messageTime;
	private String messageBody;
	private String unameMe;
	private String unamePeer;
	private String aliasPeer;   // 对方的别名
	private boolean isRead;
	private boolean isMine;
	
	private static ChatMessage tmpMsg=new ChatMessage();
	
	public static String getReadableStr(String msg){
		tmpMsg.setMessageBody(msg);
		return tmpMsg.getReadableStr();
	}
	
	/**
	 * 可读字符串,用于"最近回话列表"
	 * @return
	 */
	public String getReadableStr(){
		String ret=messageBody;
		if(isImg())
			ret="[Image]";
		else if(isAudio())
			ret="[Audio]";
		else if(isCall())
			ret = "[Call]";
		return ret;
	}
	
	/**
	 * 是否为-音频
	 * @return
	 */
	public boolean isAudio(){
		return Type_Audio.equals(type);
	}
	
	/**
	 * 是否为-图片
	 * @return
	 */
	public boolean isImg(){
		return Type_Img.equals(type);
	}
	
	/**为文本
	 * @return
	 */
	public boolean isText(){
		return Type_Text.equals(type);
	}
	
	/**
	 * 是否为  电话
	 * @return
	 */
	public boolean isCall() {
		return Type_Call.equals(type);
	}
	
	//==================debug===============================
	
	private String type=Type_Text;		//消息类型
	private String url="";		//多媒体url
	private int duration;		//音频长度
	
	/**
	 * 音频时长
	 * @return
	 */
	public int getDuration(){
		return duration;
	}
	
	/**
	 * 获取-多媒体内容url,如:音频
	 * @return
	 */
	public String getMediaUrl(){ 
		return url;
	}
	
	
	/**
	 * 解析"消息体"成"对象属性",如:音频
	 * @return
	 */
	private void parseBody(String msg){
		if(TextUtils.isEmpty(messageBody))
			return;
		
		//音频
		if(messageBody.indexOf(Type_Audio)==0)
		{
			type=Type_Audio;
			String json=messageBody.substring(messageBody.indexOf(']')+1);
			try {
				JSONObject jo=new JSONObject(json);
				url=jo.optString("url");
				duration=jo.optInt("dur");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		//图片
		else if(messageBody.indexOf(Type_Img)==0)
		{
			type=Type_Img;
			url=messageBody.substring(messageBody.indexOf(']')+1);
		}
		// 网络电话
		else if(messageBody.indexOf(Type_Call) == 0) {
			type = Type_Call;
		}

		//文字
		else {
			type=Type_Text;
		}

	}
	
	public static String newAudioBody(String url,int duration){
		JSONObject jo=new JSONObject();
		try {
			jo.put("url", url);
			jo.put("dur", duration);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return Type_Audio+jo.toString();
	}
	
	//==================inner===============================
	
	public ChatMessage() {
		super();
	}
	public ChatMessage(String messageBody ,boolean isMine) {
//		this.messageBody = messageBody;
		setMessageBody(messageBody);
		this.isMine = isMine;
	}
	public ChatMessage(String messageTime, String messageBody, String unameMe,
			String unamePeer, boolean isMine, boolean isRead) {
		super();
		this.messageTime = messageTime;
//		this.messageBody = messageBody;
		setMessageBody(messageBody);
		this.unameMe = unameMe;
		this.unamePeer = unamePeer;
		this.isMine = isMine;
		this.isRead = isRead;         // 创建新消息的时候isRead为false
	}
	
	public ChatMessage(String id, String messageTime, String messageBody, String unameMe,
			String unamePeer, boolean isMine) {
		super();
		this.id = id;
		this.messageTime = messageTime;
//		this.messageBody = messageBody;
		setMessageBody(messageBody);
		this.unameMe = unameMe;
		this.unamePeer = unamePeer;
		this.isMine = isMine;
		this.isRead = false;         // 创建新消息的时候isRead为false
	}
	
	public ChatMessage(String id, String messageTime, String messageBody, String unameMe,
			String unamePeer, boolean isMine, boolean isRead) {
		super();
		this.id = id;
		this.messageTime = messageTime;
//		this.messageBody = messageBody;
		setMessageBody(messageBody);
		this.unameMe = unameMe;
		this.unamePeer = unamePeer;
		this.isMine = isMine;
		this.isRead = isRead;         // 创建新消息的时候isRead为false
	}
	
	public String getMessageTime() {
		return messageTime;
	}
	public void setMessageTime(String messageTime) {
		this.messageTime = messageTime;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
		
		//解析-消息体
		parseBody(messageBody);
	}
	public String getUnameMe() {
		return unameMe;
	}
	public void setUnameMe(String unameMe) {
		this.unameMe = unameMe;
	}
	public String getUnamePeer() {
		int index = unamePeer.lastIndexOf("/");
		if(index > 0) {
			return unamePeer.substring(0, index);
		}
		return unamePeer;
	}
	public void setUnamePeer(String unamePeer) {
		this.unamePeer = unamePeer;
	}
	public boolean isMine() {
		return isMine;
	}
	public void setMine(boolean isMine) {
		this.isMine = isMine;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ChatMessage [messageTime=" + messageTime + ", messageBody="
				+ messageBody + ", unameMe=" + unameMe + ", unamePeer="
				+ unamePeer + ", isMine=" + isMine + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isRead() {
		return isRead;
	}
	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public String getAliasPeer() {
		return aliasPeer;
	}

	public void setAliasPeer(String aliasPeer) {
		this.aliasPeer = aliasPeer;
	}
	
}
