package oso.ui.chat.activity;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.ActivityStack;
import oso.base.BaseActivity;
import oso.ui.chat.adapter.ChatRecordAdapter;
import oso.ui.chat.bean.ChatHistoryRecord;
import oso.ui.chat.bean.ChatHistoryRecord.Records;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.chat.bean.ChatOtherBean;
import oso.ui.chat.ext.sipdroid.sipua.ui.Receiver;
import oso.ui.chat.util.ChatUtil;
import oso.ui.chat.util.XLog;
import oso.ui.chat.util.XmppTool;
import oso.widget.SimpleTextWatcher;
import oso.widget.dialog.RewriteBuilderDialog;
import support.AppConfig;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.dbhelper.DBManager;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.AudioMnger;
import utility.DisplayPictureUtil;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import utility.PhotoMnger;
import utility.Utility;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.ClipDrawable;
import android.net.sip.SipProfile;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import declare.com.vvme.R;

public class ChatActivity extends BaseActivity implements OnClickListener, SwipeRefreshLayout.OnRefreshListener {
	private static final String TAG = ChatActivity.class.getSimpleName();
	private static final int EVERY_PAGE_COUNT = 10; // 每次加载10条
	private static final int WHAT_CHAT_MSG_RECEIVE = 10;
	private static final int WHAT_CHAT_MSG_SEND = 20;
	private static final int WHAT_BTN_SEND_CLICKABLE = 30;
	private static final int WHAT_SHOW_CHAT_RECORD = 40;
	private static final int WHAT_INIT_CHAT_RECORD = 50;
	private static final int WHAT_GET_SERVER_RECORDS = 60;
	private static int REFRESH_DATA_SIZE = 0;
	private static int currentIndex = 0; // 当前聊天记录的偏移量
	private static String chatUserAlias = null;
	public  static String chatUserName = null;
	private static String avatarUrlPeer = null;
	private boolean isMoveToBottom = true;
	private String tempMsgId = null;
	private String chatUnamePeer;
	private String chatname;
	private Chat chat;
	private ChatManager chatManager;
	private Button btn_send;
	private EditText et_message_body;
	private ListView lv_chatRecord;
	private ImageView topbar_title_iv;
	private List<ChatMessage> chatRecordList; // 存储聊天记录
	private SwipeRefreshLayout swipeRefreshLayout;
	private ChatMessageListener chatMessageListener;
	private ImageView titleimage;
	private int online;
	/**
	 * 是否在同其聊天
	 * 
	 * @param peerName  带@
	 * @return
	 */
	public static boolean isChatingWith(String peerName) {
		if (TextUtils.isEmpty(peerName))
			return false;

		Activity ac = ActivityStack.getTopActInstance();
		if (ac instanceof ChatActivity) {
			ChatActivity chatAc = (ChatActivity) ac;
			return peerName.equals(chatAc.chatUnamePeer);
		}
		return false;
	}
	
	/**
	 * intent携带数据封装  
	 * @param intent
	 * @param uname    ChatUserName  : 如 admin   
	 * @param alias    ChatUserAlias : 如 alias
	 */
	public static void initParams(Intent intent, String uname, String alias) {
		intent.putExtra("ChatUserName", FormatterUtil.addDomain(uname));
		intent.putExtra("ChatUserAlias", alias);
	}
	
	// ===================头像================
	/*public static void initParams(Intent intent, String uname, String alias, String avatarUrlPeer) {
		intent.putExtra("ChatUserName", FormatterUtil.addDomain(uname));
		intent.putExtra("ChatUserAlias", alias);
		intent.putExtra("ChatUserAvatarUrl", avatarUrlPeer); // 对方的头像url
	}*/
	// ===================头像================
	private void applyParams() {	
		chatUserName = getIntent().getStringExtra("ChatUserName");
		chatUserAlias = getIntent().getStringExtra("ChatUserAlias");
		
		initAvatarUrl(FormatterUtil.getHalfname(chatUserName));
	}
	
	private void initAvatarUrl(String unamePeer) {
		// 查对方头像url
		@SuppressWarnings("unchecked")
		HashMap<String, Set<String>> extAccMap = (HashMap<String, Set<String>>) ( StoredData.getExtAccMap());
		Set<String> set = extAccMap.get(unamePeer);
		if(set == null) return;
		Iterator<String> it = set.iterator();
		
		while(it.hasNext()) {
			String result = it.next();
			if(!TextUtils.isEmpty(result) && result.startsWith("upload/account/")) {
				avatarUrlPeer = result;
				return;
			}
		}
	}
	// ==================inner===============================

	private NewMsgBroadcastReceiver chatMsgReceiver;
	private ChatRecordAdapter chatRecordAdapter;

	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			isMoveToBottom = true;
			
			switch (msg.what) {
			case WHAT_CHAT_MSG_RECEIVE: // 新消息接收
				org.jivesoftware.smack.packet.Message message = (org.jivesoftware.smack.packet.Message) msg.obj;
				addMessage(message, false);
				break;

			case WHAT_CHAT_MSG_SEND: // 新消息发送
				org.jivesoftware.smack.packet.Message message1 = (org.jivesoftware.smack.packet.Message) msg.obj;
				addMessage(message1, true);
				break;
				
			case WHAT_BTN_SEND_CLICKABLE: // 设置发送按钮是否可以点击
				btn_send.setClickable(true);
				break;
				
			case WHAT_INIT_CHAT_RECORD: // 初始化加载本地数据库聊天记录
				if (msg.obj != null) {
					chatRecordList.addAll(0, (List<ChatMessage>) msg.obj);
				}
				break;
			
			case WHAT_SHOW_CHAT_RECORD:  // UI线程刷新LV数据
				if (msg.obj != null) {
					chatRecordList.addAll(0, (List<ChatMessage>) msg.obj);
					
					REFRESH_DATA_SIZE = ((List<ChatMessage>) msg.obj).size();
				}
				isMoveToBottom = false;
				swipeRefreshLayout.setRefreshing(false);
				break;
			
			case WHAT_GET_SERVER_RECORDS: // 服务器上获取聊天记录
				updateCurrRecordsList((ChatHistoryRecord) msg.obj);
				isMoveToBottom = false;
				swipeRefreshLayout.setRefreshing(false);
				break;
			}
			
			notifyDataSetChanged(isMoveToBottom);
		}

	};

	/**
	 * 将获取的服务器消息显示
	 * 
	 * @param ChatHistoryRecord
	 */
	private void updateCurrRecordsList(ChatHistoryRecord chatHistoryRecord) {
		if (chatHistoryRecord == null || chatHistoryRecord.records == null
				|| chatHistoryRecord.records.size() == 0) {
			return;
		}
		REFRESH_DATA_SIZE = chatHistoryRecord.records.size();
		chatRecordAdapter.notifyDataSetChanged();
		int size = chatHistoryRecord.records.size();
		List<Records> list = chatHistoryRecord.records;
		for (int i = 0; i < size; i++) {
			String id = list.get(i).id;
			String messageTime = list.get(i).create_DATE;
			String messageBody = list.get(i).content;
			String unameMe = null;
			String unamePeer = null;
			boolean isMine = StoredData.getName().contains(list.get(i).sender_ID);
			// 判断是否是自己发送
			if (isMine) {
				unameMe = list.get(i).sender_ID;
				unamePeer = list.get(i).receiver_ID;
			} else {
				unameMe = list.get(i).receiver_ID;
				unamePeer = list.get(i).sender_ID;
			}
			ChatMessage chatMessage = new ChatMessage(id, messageTime, messageBody, unameMe, unamePeer, isMine, true);
			chatRecordList.add(0, chatMessage);
			// 偏移量改变
			currentIndex = chatRecordList.size();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_chat_mainchat_layout, 0);
		supportChatMenu=false;
		applyParams();  // 应用参数
		
		initView();
		
		setLVAdapter();
		
		createChat(); // 创建聊天对象
		
		registReceiver(); // 注册消息来了的广播接收者
		
		initData(); // 加载聊天历史记录
		
		initListener(); // ListView 下拉刷新监听器
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//取消notify
		Utility.cancelNotify(Utility.NotifyID_Chat);
	}

	/**
	 * 设置Adapter
	 */
	private void setLVAdapter() {
		chatRecordList = new ArrayList<ChatMessage>();
		
		if(TextUtils.isEmpty(avatarUrlPeer)) {
			avatarUrlPeer = "";
		}
		chatRecordAdapter = new ChatRecordAdapter(this, chatRecordList, EVERY_PAGE_COUNT, avatarUrlPeer);
		
		lv_chatRecord.setAdapter(chatRecordAdapter);
	}

	/**
	 * 下拉刷新监听器 回调onRefresh方法
	 */
	private void initListener() {
		swipeRefreshLayout.setOnRefreshListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		updataReadStatus();
	}

	/**
	 * 修改未读消息为已读状态
	 */
	private void updataReadStatus() {
		new Thread() {
			@Override
			public void run() {
				String unameMe = StoredData.getName();
				DBManager db = DBManager.getThis();
				db.updateReadStatusWithPeer(unameMe, StringUtils.parseName(chatUserName));
			}
		}.start();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (chatMsgReceiver != null) {
			unregisterReceiver(chatMsgReceiver);
			chatMsgReceiver = null;
		}
		//onRefreshListener = null;
		chatMessageListener = null;
		audioMnger.onDestroy();
		chatUserAlias = null;
		// 重置聊天记录偏移量
		currentIndex = 0;
		avatarUrlPeer = null;
	}

	/**
	 * 获取数据库中缓存的聊天记录
	 */
	private void initData() {
		new Thread() {
			@Override
			public void run() {
				long dataTimeStart = System.currentTimeMillis();
				DBManager db = DBManager.getThis();
				chatUnamePeer = StringUtils.parseName(chatUserName);
				chatname = StringUtils.parseName(chatUserName);
				String unameMe = StoredData.getName();
				// 查询聊天记录 一次查EVERY_PAGE_COUNT条
				List<ChatMessage> queryList = db.query(unameMe, chatUnamePeer, 
						EVERY_PAGE_COUNT, currentIndex);
				Collections.reverse(queryList); // 数据反了  反转
				if (queryList.size() > 0 ) {
					Message msg = Message.obtain();
					msg.what = WHAT_INIT_CHAT_RECORD;
					msg.obj = queryList;
					msg.arg1 = queryList.size(); // 记录条数，增加偏移量 offset。 通知主线程刷新UI
					handler.sendMessage(msg);
				} 
				XLog.out("initData() " + (System.currentTimeMillis() - dataTimeStart));
			};
		}.start();

	}

	private View loRecAudio, btnRecord;
	private Button btnVoiceOrKb, btnAttach, btnCall;
	
	private View frRecing,layerCancel_rec,layerRecing_rec;
	private ImageView imgVolume;

	private void initView() {
		
		// 取view
		btn_send = (Button) findViewById(R.id.btn_chat_send);
		btn_send.setOnClickListener(this);
		lv_chatRecord = (ListView) findViewById(R.id.lv_chat_record);
		loRecAudio = findViewById(R.id.loRecAudio);
		btnVoiceOrKb = (Button) findViewById(R.id.btnVoiceOrKb);
		btnRecord = (Button) findViewById(R.id.btnRecord);
		btnAttach = (Button) findViewById(R.id.btn_chat_attach);
		
		//debug
		frRecing=findViewById(R.id.frRecing);
		layerCancel_rec=findViewById(R.id.layerCancel_rec);
		layerRecing_rec=findViewById(R.id.layerRecing_rec);
		imgVolume=(ImageView)findViewById(R.id.imgVolume);

		chatRecordList = new ArrayList<ChatMessage>();
		et_message_body = (EditText) findViewById(R.id.chat_message_text);
		TextView topbar_title = (TextView) vTopbar.findViewById(R.id.topbar_title);
		topbar_title_iv = (ImageView) vTopbar.findViewById(R.id.topbar_title_iv);
		
		//lv
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
		lv_chatRecord.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_DISABLED);
		lv_chatRecord.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//隐藏-对话框、键盘
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					showUtilDlg(false);
					Utility.colseInputMethod(mActivity, vMiddle);
				}
				return false;
			}
		});
		
		String account = FormatterUtil.getHalfname(chatUserName);
		String warename = getWarename(account);
		if(TextUtils.isEmpty(warename)) {
			topbar_title.setText(chatUserAlias);
		} else {
			topbar_title.setText(chatUserAlias+" ["+warename+"]");
		}
		
		// 监听事件
		btnRecord.setOnTouchListener(onTouch_btnRecord);
		findViewById(R.id.btn_chat_attach).setOnClickListener(this);
		findViewById(R.id.btnCamera).setOnClickListener(this);
		findViewById(R.id.btnAlbum).setOnClickListener(this);
		btnCall = (Button) findViewById(R.id.btnCall);
		btnCall.setOnClickListener(this);
		btnCall.setVisibility(AppConfig.Debug_SipCall?View.VISIBLE:View.GONE);
		
		btnVoiceOrKb.setOnClickListener(this);
		et_message_body.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {

				updateRightBtn();
			}
		});
		et_message_body.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					showUtilDlg(false);
					updateLvPosition();
				}
				//lv_chatRecord.setSelection(chatRecordAdapter.getCount() - 1);
				return false;
			}
		});

		// 初始化
		setInputMode(true);
		showUtilDlg(false);
		updateRightBtn();
		updateCancelAudio(false, false);
		
		chatcountimage();
		
	}
	public void chatcountimage(){
		// 获得状态
		String namePeer = StringUtils.parseName(chatUserName);
//		findViewById(R.id.btnCall).setVisibility(AppConfig.isDebug?View.VISIBLE:View.GONE);
		
		setStatus(namePeer);
		titleimage = showRightImage(new ImageOnClickListener());
		if(TextUtils.isEmpty(avatarUrlPeer)){
			if(online == 0){
				titleimage.setImageDrawable(getResources().getDrawable(R.drawable.account_circle));
			}else{
				titleimage.setImageDrawable(getResources().getDrawable(R.drawable.account_offline_circle));
			}
		}else{
			if(online == 0){
				ImageLoader.getInstance().displayImage(HttpUrlPath.basePath+avatarUrlPeer, 
						titleimage,DisplayPictureUtil.getChatAvatarDisplayImageOptions_Online());
			}else{
				ImageLoader.getInstance().loadImage(HttpUrlPath.basePath+avatarUrlPeer, new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String arg0, View arg1) {
					}
					
					@Override
					public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					}
					
					@Override
					public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
//						titleimage.setImageDrawable(ChatUtil.changeIconStatus(String.valueOf(online), bitmap, true));
						titleimage.setImageBitmap(Utility.getGrayBitmap(bitmap));
					}
					@Override
					public void onLoadingCancelled(String arg0, View arg1) {
						
					}
				});
			}
				
				
		}
	}
	
	/**
	 * 获得该聊天对象的所属仓库名
	 * @param account    如: admin
	 * @return warename
	 */
	private String getWarename(String account) {
		Map<String, ?> extAccMap = (Map<String, ?>) StoredData.getExtAccMap();
		String warename = "";
		if(extAccMap.containsKey(account)) {
			@SuppressWarnings("unchecked")
			Set<String> set = (Set<String>) extAccMap.get(account);
			if(set.size()!=0 && set!=null) {
				Iterator<String> it = set.iterator();
				while(it.hasNext()) {
					String result = it.next();
					if(!it.hasNext()) return "";
					warename = it.next();
					if(!TextUtils.isEmpty(result)) {
						if(result.startsWith("upload/account/")) {
							//warename = it.next(); 头像注释
						} else {
							warename = result;
						}
					} 
				}
			}
		}
		return warename;
	}

	/**
	 * 设置-对方在线状态
	 */
	private void setStatus(String namePeer) {
		topbar_title_iv.setVisibility(View.VISIBLE);
		int status = ChatUtil.getThis(this).findStatusByUsername(namePeer);
		online = status;
		switch (status) {
		case ChatUtil.INT_STATUS_ONLINE:
			topbar_title_iv.setImageResource(R.drawable.ic_chat_online);
			break;

		case ChatUtil.INT_STATUS_OFFLINE:
			topbar_title_iv.setImageResource(R.drawable.ic_chat_offline);
			break;
		}
	}
	
	/**
	 * 修改在线状态
	 */
	public void setStatus(String namePeer, boolean isAvailable) {
		if(!namePeer.equals(chatUserName)) 
			return;
		topbar_title_iv.setImageResource(isAvailable ? R.drawable.ic_chat_online : R.drawable.ic_chat_offline);
		chatcountimage();
	}
	
	private AudioMnger.OnRecListener onRec_volume=new AudioMnger.OnRecListener() {
		
		@Override
		public void onRec(int volume) {
			
			//音量-动画
			ClipDrawable clVolume=(ClipDrawable)imgVolume.getDrawable();
			float maxVolume=15000f;
			int volCnt=8;
			int oneH=1250;
			
			int vol_=(int)(((float)volume/maxVolume)*(float)volCnt);	//段数
			if(vol_<1)
				vol_=1;
			clVolume.setLevel(vol_*oneH);
			
		}
	};

	/**
	 * 更新-发送/添加附件按钮-状态
	 */
	private void updateRightBtn() {
		//文本模式、且有文字时
		boolean showSend = !canRecord()
				&& et_message_body.getText().length() > 0;
		btn_send.setVisibility(showSend ? View.VISIBLE : View.GONE);
		btnAttach.setVisibility(showSend ? View.GONE : View.VISIBLE);
	}

	/**
	 * 切换-输入模式
	 * 
	 * @param isRecord
	 *            当前可录音
	 */
	private void setInputMode(boolean isRecord) {
		btnVoiceOrKb.setSelected(!isRecord);
		et_message_body.setVisibility(isRecord ? View.GONE : View.VISIBLE);
		btnRecord.setVisibility(isRecord ? View.VISIBLE : View.GONE);

		// 功能框
		loRecAudio.setVisibility(View.GONE);
		//右侧按钮
		updateRightBtn();
		
		// 录音时
		if (isRecord) {
			Utility.colseInputMethod(this, vMiddle);
		}
		// 码字时
		else {
			et_message_body.requestFocus();
			Utility.showSoftkeyboard(this, et_message_body);
			
			updateLvPosition();
		}
	}
	
	// 弹出键盘时，ListView重新设置position
	private void updateLvPosition() {
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				lv_chatRecord.setSelection(chatRecordAdapter.getCount() - 1);	
			}
		}, 200);		
	}

	/**
	 * 可录音
	 * 
	 * @return
	 */
	private boolean canRecord() {
		return !btnVoiceOrKb.isSelected();
	}

	/**
	 * 显示下方"工具箱"
	 */
	private void showUtilDlg(boolean toShow) {
		loRecAudio.setVisibility(toShow ? View.VISIBLE : View.GONE);
	}

	private boolean isUtilDlg_show() {
		return loRecAudio.getVisibility() == View.VISIBLE;
	}

	private AudioMnger audioMnger = AudioMnger.getThis();

	private long time_startRec; // 开始录音时间

	private OnTouchListener onTouch_btnRecord = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			int action = event.getAction();
			final boolean isInCancel = Utility.isPointInView(frRecing, event);
			
			//若没权限,直接跳过
//			if(!hasRecPermission())
//			{
//				if (action == MotionEvent.ACTION_DOWN) 
//					showDlg_tipRecPermission();
//				return false;
//			}
			
			// 按下时,开始录音
			if (action == MotionEvent.ACTION_DOWN) {
				time_startRec = System.currentTimeMillis();
				audioMnger.startRec(onRec_volume);
				// 显示-取消
				updateCancelAudio(true, isInCancel);
			} else if (action == MotionEvent.ACTION_MOVE) {
				updateCancelAudio(true, isInCancel);
			}
			// 松开时,停止录音
			else if (action == MotionEvent.ACTION_UP) {
				// 语音时长
				long duration = System.currentTimeMillis() - time_startRec;

				// 取消
				if (Utility.isPointInView(frRecing, event)){
					audioMnger.cancelRec();
					// 隐藏"取消"
					updateCancelAudio(false, isInCancel);
				}
				// 小于1s
				else if (duration < 1000) {
					UIHelper.showToast(getResources().getString(R.string.chat_record_too_short));
					audioMnger.cancelRec();
					// 隐藏"取消"
					updateCancelAudio(false, isInCancel);
				}
				// 上传
				else {
					//延迟上传,录音空间有点没采集到
					vMiddle.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							File f = audioMnger.stopRec();
							if(!Utility.isAudioPermissions(mActivity, f)) {
								RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, getString(R.string.str_audio_permissions_error));
								// 隐藏"取消"
								updateCancelAudio(false, isInCancel);
								return;
							}
							uploadAudio(f);
							
							// 隐藏"取消"
							updateCancelAudio(false, isInCancel);
						}
					}, 400);
				}
			}
			return false;
		}
	};
	
	/**检查语音权限
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean hasRecPermission(){
		return checkCallingOrSelfPermission("android.permission.RECORD_AUDIO")==
				PackageManager.PERMISSION_GRANTED;
	}
	
	/**
	 * 录音权限
	 */
	@SuppressWarnings("unused")
	private void showDlg_tipRecPermission(){
		RewriteBuilderDialog.showSimpleDialog(this, "Please enable the Recording Permission!", null);
	}

	/**
	 * @param toShow
	 * @param isPointIn
	 *            手指已移至cancel
	 */
	private void updateCancelAudio(boolean toShow, boolean isPointIn) {
		frRecing.setVisibility(toShow ? View.VISIBLE : View.GONE);
		// 图标
//		vCancelAudio.setSelected(isPointIn);
//		vCancelAudio.setText(isPointIn ? "Cancel" : "Recording");
		
		//图标
		layerCancel_rec.setVisibility(isPointIn ? View.VISIBLE : View.GONE);
		layerRecing_rec.setVisibility(isPointIn ? View.GONE : View.VISIBLE);
	}

	private void uploadAudio(final File fileAudio) {
		
		//连接
		if(!isConAvailable()){
			if(fileAudio!=null)
				fileAudio.delete();
			return;
		}
		
		RequestParams params = new RequestParams();
		try {
//			uploadfile
			params.put("file", fileAudio);
		} catch (Exception e) {
			e.printStackTrace();
		}

		new SimpleJSONUtil() {
			public void handReponseJson(JSONObject json) {
			};

			public void handFail() {
				//删掉
				if(fileAudio!=null)
					fileAudio.delete();
			};

			public boolean useTheCustom(JSONArray json) {
				//音频长度,下取
				int dur=audioMnger.getAudioDuration(fileAudio)/1000;
				
				//删掉
				if(fileAudio!=null)
					fileAudio.delete();
				
				JSONObject jo = json.optJSONObject(0);
				String fileID = jo.optString("file_id");
//				String body = ChatMessage.Type_Audio + HttpUrlPath.DownFile
//						+ fileID;
				//debug
				String body=ChatMessage.newAudioBody(HttpUrlPath.DownFile
						+ fileID, dur);
				org.jivesoftware.smack.packet.Message message = new org.jivesoftware.smack.packet.Message();
				message.setBody(body);
				addMessage(message, true);
				saveSended2DB(message);
				
				return true;
			};
		}.doPost(HttpUrlPath.UploadFile, params, this);

	}

	// 上传图片
	private void uploadImg(File img) {
		RequestParams params = new RequestParams();
		try {
			params.put("uploadfile", img);
		} catch (Exception e) {
			e.printStackTrace();
		}

		new SimpleJSONUtil() {
			public void handReponseJson(JSONObject json) {

			};

			public void handFail() {
			};

			public boolean useTheCustom(JSONArray json) {

				JSONObject jo = json.optJSONObject(0);
				String fileID = jo.optString("file_id");
				String url = ChatMessage.Type_Img + HttpUrlPath.DownFile + fileID;
				org.jivesoftware.smack.packet.Message message = new org.jivesoftware.smack.packet.Message();
				message.setBody(url);
				addMessage(message, true);
				saveSended2DB(message);
				return true;
			};
		}.doPost(HttpUrlPath.UploadFile, params, this);
	}
	
	/**
	 * url转html
	 * 
	 * @param url
	 * @return
	 */
	@SuppressWarnings("unused")
	private String toHtml_audioPath(String url) {
		return "<audio src=\"" + url + "\"/>";
	}

	/**
	 * 注册接收广播
	 */
	private void registReceiver() {
		IntentFilter intentFilter = new IntentFilter(
				"com.vvme.chat.NEW_MESSAGE_ACTION");
		chatMsgReceiver = new NewMsgBroadcastReceiver();
		intentFilter.setPriority(1000); // ChatActivity 聊天界面优先级最高
		registerReceiver(chatMsgReceiver, intentFilter);
	}

	/**
	 * ListView添加消息
	 * 
	 * @param msgBody
	 *            消息内容
	 * @param isMine
	 *            是否是自己发送的
	 */
	private void addMessage(org.jivesoftware.smack.packet.Message message,
			boolean isMine) {
		String unameMe = FormatterUtil.addDomain(StoredData.getName());
		ChatMessage chatMessage = new ChatMessage(message.getPacketID(),
				ChatUtil.getCurrentTime(), message.getBody(), unameMe,
				chatUserName, isMine, true);
		if (isMine == true) {
			try {
				//====================消息回执 debug===============
				DeliveryReceiptManager.addDeliveryReceiptRequest(message);
				chat.sendMessage(message);
				// 发送后查询的偏移量需要自增   
				//currentIndex ++;   
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// 发送后查询的偏移量需要自减  
		// currentIndex --;
		chatRecordList.add(chatRecordList.size(), chatMessage);
		notifyDataSetChanged(true);

	};

	/**
	 * ListView数据刷新 自动滚动到底部
	 */
	private void notifyDataSetChanged(boolean isMoveToBottom) {
		if (isMoveToBottom) {
			if(chatRecordAdapter == null) return;
			chatRecordAdapter.notifyDataSetChanged();
			scrollListViewToBottom();
		} else {
			if(chatRecordAdapter == null) return;
			chatRecordAdapter.notifyDataSetChanged();
			lv_chatRecord.setSelection(REFRESH_DATA_SIZE);
			
			lv_chatRecord.getHandler().postDelayed(new Runnable() {
				public void run() {
					lv_chatRecord.smoothScrollToPosition(REFRESH_DATA_SIZE - 1);
				}
			}, 50);
		}
		
		currentIndex = chatRecordAdapter.getCount();
	}
	

	private void scrollListViewToBottom() {
		lv_chatRecord.setSelection(chatRecordAdapter.getCount() - 1);
	}

	private void createChat() {
		long chatTimeStart = System.currentTimeMillis();
		chatManager = ChatManager.getInstanceFor(XmppTool.getConnection());
		if (chatMessageListener == null)
			chatMessageListener = new ChatMessageListener();
		String chatName = StringUtils.parseName(chatUserName) + ChatUtil.ChatServerDomain;
		chat = chatManager.createChat(chatName, chatMessageListener);
		XLog.out("createChat() " + (System.currentTimeMillis() - chatTimeStart));
	}

	private class ChatMessageListener implements MessageListener {

		@Override
		public void processMessage(Chat chat,
				org.jivesoftware.smack.packet.Message message) {
			XLog.d(TAG, message.getBody());
		}

	}

	/**
	 * 保存到数据库
	 * 
	 * @param message
	 */
	private void save2DB(final ChatMessage message) {
		new Thread() {
			@Override
			public void run() {
				DBManager db = DBManager.getThis();
				db.insert(message);
			};
		}.start();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnVoiceOrKb: // 语音/文本-切换
			setInputMode(!canRecord());
			break;

		case R.id.btn_chat_send:	//发送文本
			
			String messageBody = et_message_body.getText().toString();
			//空校验
			if(TextUtils.isEmpty(messageBody)){
				UIHelper.showToast(getResources().getString(R.string.chat_send_msg_empty));
				return;
			}
			//连接
			if(!isConAvailable())
				return;
			
			// 如果当前已经连接
			if(Utility.isConnectNet(this)) {
				org.jivesoftware.smack.packet.Message msgToSend = new org.jivesoftware.smack.packet.Message();
				msgToSend.setBody(et_message_body.getText().toString());
				//发消息
				Message msg = Message.obtain();
				msg.what = WHAT_CHAT_MSG_SEND;
				msg.obj = msgToSend;
				handler.sendMessage(msg);
				//存至db
				saveSended2DB(msgToSend);
				et_message_body.setText("");
			} else {
				UIHelper.showToast(getResources().getString(R.string.chat_network_exception));
			}
			break;

		case R.id.btn_chat_attach: // 多功能
			boolean toShowUtilDlg = !isUtilDlg_show();
			// 显示时,键盘下放
			if (toShowUtilDlg)
				Utility.colseInputMethod(this, vMiddle);
			// 隐藏时,键盘弹起
			else
				Utility.showSoftkeyboard(this, et_message_body);
			showUtilDlg(toShowUtilDlg);
			
			updateLvPosition();
			break;

		case R.id.btnCamera: // 相机
			Utility.colseInputMethod(this, vMiddle);
			
			if(!isConAvailable())
				return;
			PhotoMnger.getThis().openCamera(this);
			break;
		case R.id.btnAlbum: // 相册
			Utility.colseInputMethod(this, vMiddle);
			
			if(!isConAvailable())
				return;
			PhotoMnger.getThis().openAlbum(this);
			break;
			
		case R.id.btnCall:
			/*if(!getOnlineBoolean(chatUnamePeer)) {
				UIHelper.showToast(getString(R.string.chat_offline));
				return;
			}*/
//			Receiver.engine(ChatActivity.this).call(chatUnamePeer+"@"+HttpUrlPath.getSipServerUrl(), false);
			//debug
			String user="";
			if(StoredData.getUsername().equals("tms11"))
				user="200";
			else
				user="100";
			Receiver.engine(ChatActivity.this).call(user+"@"+HttpUrlPath.getSipServerUrl(), false); 
			break;
			
		}
	}
	
	/**
	 * 拨打网络电话，如果对方没在线，则不可点击
	 * @param chatUnamePeer
	 */
	@SuppressWarnings("unused")
	private void call(String chatUnamePeer) {
		if(!getOnlineBoolean(chatUnamePeer)) {
			UIHelper.showToast(getResources().getString(R.string.chat_offline));
			return;
		}
		SipProfile localProfile = ChatUtil.getLocalProfile();
		SipProfile peerProfile = ChatUtil.getPeerProfile(chatUnamePeer);
		Intent intent = new Intent(this, CallActivity.class);
		CallActivity.initParams(intent, localProfile, peerProfile, CallActivity.STATUS_CALL);
		startActivity(intent);
	}

	
	private boolean getOnlineBoolean(String chatUnamePeer) {
		int statusCode = ChatUtil.getThis(this).findStatusByUsername(chatUnamePeer);
		return statusCode == ChatUtil.INT_STATUS_ONLINE;
	}

	/**
	 * 连接可用
	 * @return
	 */
	private boolean isConAvailable(){
		//若未联网
		if(!Utility.isConnectNet(this))
		{
			UIHelper.showToast(getResources().getString(R.string.chat_network_exception));
			return false;
		}
		//of未连上
		if(!XmppTool.getConnection().isAuthenticated()){
			UIHelper.showToast(getResources().getString(R.string.chat_connecting));
			return false;
		}
		return true;
	}

	/**
	 * 保存-已发消息
	 * 
	 * @param msg
	 */
	private void saveSended2DB(org.jivesoftware.smack.packet.Message msg) {
		String unameMe = StoredData.getName();

		ChatMessage chatMsg = new ChatMessage(msg.getPacketID(),
				ChatUtil.getCurrentTime(), msg.getBody(), unameMe,
				chatUnamePeer, true, true);

		save2DB(chatMsg);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == TTS.REQ_TTS_STATUS_CHECK) return;
		File img = PhotoMnger.getThis().onActivityResult(requestCode, resultCode, data);
		if (img != null)
			uploadImg(img);
	}

	/**
	 * 接收新消息广播
	 * 
	 * @author xialimin
	 * 
	 */
	private class NewMsgBroadcastReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			final ChatMessage chatMessage = (ChatMessage) intent.getExtras().get("NewMessage");
			String messageBody = chatMessage.getMessageBody();
			if (chatUnamePeer.equals(chatMessage.getUnamePeer())) {
				Message msg = Message.obtain();
				msg.what = WHAT_CHAT_MSG_RECEIVE;
				org.jivesoftware.smack.packet.Message message = new org.jivesoftware.smack.packet.Message();
				message.setPacketID(chatMessage.getId());
				message.setBody(messageBody);
				msg.obj = message;
				handler.sendMessage(msg);
				
				// 消息标记为已读 存入数据库
				chatMessage.setRead(true);
				
				abortBroadcast();
			}
		}

	}

	/**
	 * 加载服务器历史聊天记录
	 */
	public void loadServerHistoryRecords() {
		// 服务器请求只需要账号前半部分
		String uname_me = StoredData.getName(); // 数据库中保存的账号都是@前面的账号信息 admin
		String uname_peer = StringUtils.parseName(chatUserName);
		String query_uname_me = uname_me;
		String query_uname_peer = uname_peer;
		// 防止重复查询
		final String lastMessageId = DBManager.getThis().queryLastId(query_uname_me,
				query_uname_peer);
		if (lastMessageId.equals(tempMsgId)) {
			// 完成刷新
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// 判断是否有网络
					judgeNetState();
					showToast();    // 聊天记录已经查找到结尾
					swipeRefreshLayout.setRefreshing(false);
				}

				private void showToast() {
					UIHelper.showToast(ChatActivity.this, getResources().getString(R.string.chat_get_record_nomoredata));
				}


			});
			return;
		} else {
			tempMsgId = lastMessageId;
		}
		
		String url = HttpUrlPath.chatRecordServerUrl();
		RequestParams params = new RequestParams();
		params.add("sender", uname_me);
		params.add("receiver", uname_peer); // 服务器上获取所有聊天记录 保存到数据库

		// 如果没有lastMessageId(""为没有查询到lastMessageId) 则默认取加载第一页
		if (!TextUtils.isEmpty(lastMessageId)) {
			params.add("lastMessageId", lastMessageId);
		} else {
			params.add("p", 1+""); // 本地没有 默认取服务器上拉取10条数据
		}
		
		swipeRefreshLayout.setEnabled(true);
		new SimpleJSONUtil() {

			private ChatHistoryRecord chatHistoryRecord;

			@Override
			public void handReponseJson(JSONObject json) {
				chatHistoryRecord = new Gson().fromJson(json.toString(), ChatHistoryRecord.class);

				// 过滤掉第一条(PS: 第一条为请求的本地最后一条ID的消息)
				if(chatHistoryRecord == null || chatHistoryRecord.records == null)
					return;
				if(chatHistoryRecord.records.size() > 1) {
					if(chatHistoryRecord.records.get(0).id.equals(lastMessageId)) {
							chatHistoryRecord.records.remove(0);
					}
					Message msg = Message.obtain();
					msg.obj = chatHistoryRecord;
					msg.what = WHAT_GET_SERVER_RECORDS;
					handler.sendMessage(msg);
					
					swipeRefreshLayout.setRefreshing(false);
					save2DB(chatHistoryRecord);
				} else {
					UIHelper.showToast(getResources().getString(R.string.chat_get_record_nomoredata));
					swipeRefreshLayout.setRefreshing(false);
				}
			}
			
			
			
			@Override
			public void handFail() {
				super.handFail();
				swipeRefreshLayout.setRefreshing(false);
			}



			private void save2DB(ChatHistoryRecord chatHistoryRecord2) {
				// 数据库中如果有了 就不进行插入保存了 没有 则进行保存
				DBManager db = DBManager.getThis();
				String lastMsgId = db.queryLastId(StoredData.getName(),
						StringUtils.parseName(chatUserName));
				if (!isContain(lastMsgId, chatHistoryRecord2)) {
					db.insert(chatHistoryRecord2);
				}
			}

			// 查询是否包含messageId (第一个会相同，除去。  原因：接口的问题)
			public boolean isContain(String lastMsgId, ChatHistoryRecord record) {
				for (int i = 0; i < record.records.size(); i++) {
					if (record.records.get(i).isContain(lastMsgId)) {
						return true;
					}
				}
				return false;
			}

		}
		//debug
		.enableCookie(false)
		.showLoadingDlg(false).doPost(url, params, ChatActivity.this);
	}

	/**
	 * 判断网络状态
	 */
	private boolean judgeNetState() {
		boolean isConnectNet = Utility.isConnectNet(ChatActivity.this);
		if (!isConnectNet) {
			UIHelper.showToast(getResources().getString(R.string.chat_network_exception));   // 网络有问题,请确保网络畅通 
		}
		return isConnectNet;
	}
	

	@Override
	public void onRefresh() {

		currentIndex = chatRecordAdapter.getCount();
		// 获取账号名 和 聊天对象的账号名 用于查询
		String uname_me = StoredData.getName();
		String uname_peer = StringUtils.parseName(chatUserName);

		// 查询本地的聊天记录缓存
		List<ChatMessage> currPageRecordList = DBManager.getThis().query(
				uname_me, uname_peer, EVERY_PAGE_COUNT, currentIndex);
		Collections.reverse(currPageRecordList); //数据反了 
		// 如果查询到的数据大于0 则进行显示
		if (currPageRecordList.size() > 0) {
			Message msg = Message.obtain();
			msg.what = WHAT_SHOW_CHAT_RECORD;
			msg.obj = currPageRecordList;
			msg.arg1 = currPageRecordList.size();
			handler.sendMessage(msg);
		}

		// 没查到数据 则进行网络请求 查询网络上的历史记录
		if (currPageRecordList.size() == 0) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if(!judgeNetState()) {
						// 网络没连接停止刷新
//						lv_chatRecord.stopRefresh();
						swipeRefreshLayout.setRefreshing(false);
						return;
					}
					loadServerHistoryRecords();
				}
			});
		}
	}
	private class ImageOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
//			Intent intent = new Intent(mActivity,AccountActivity.class);
//			intent.putExtra("name", chatname+"");
//			startActivity(intent);
			
			chatUserName = getIntent().getStringExtra("ChatUserName");
			
			DetailByEntry(FormatterUtil.getHalfname(chatUserName));
		}
		
	}
	public void DetailByEntry(String name) {
		RequestParams params = new RequestParams();
		params.add("account", name);
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				ChatOtherBean bean = new ChatOtherBean();
				ChatOtherBean.handJson(json, bean);
				Intent intent = new Intent(mActivity, ChatAccountActivity.class);
				intent.putExtra("name", chatname+"");
				intent.putExtra("chatotherbean", (Serializable) bean);
				startActivity(intent);
				overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.GetAccountInfoAction, params, mActivity);
//	}.doGet("http://192.168.1.182/Sync10/action/account/GetAccountInfoAction.action", params, mActivity);
		
	}


}
