package oso.ui.chat.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;

import oso.SyncApplication;
import oso.base.ActivityStack;
import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.chat.bean.ChatGroup;
import oso.ui.chat.bean.ChatGroupMember;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.main.WelcomeActivity;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import utility.FormatterUtil;
import utility.Utility;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import declare.com.vvme.R;

/**
 * 封装了用于聊天的相关api
 * 
 * @author xialimin
 * 
 */
public class ChatUtil {
	public static final int INT_STATUS_ONLINE = 0;
	public static final int INT_STATUS_OFFLINE = 1;
	public static String ChatServerDomain = null;
	public static final String LOCAL_CACHE_BASEURL = "file://";

	public static final String STATUS_ONLINE = "Online";
	public static final String STATUS_OFFLINE = "Offline";
	public static final String STATUS_UNKNOWN = "Unknown";
	public static final String MSGTYPE_CALL = "[Type_Msg_Call]";

	public static final String TYPE_WEAR_DEVICES = "My Device";
	public static final String TYPE_WEAR_GLASS = "My Glass";
	public static final String TYPE_WEAR_WATCH = "My Watch";

	private static Context context;
	private static SipManager sipManager;
	private static SipRegisterListener sipRegListener;
	private XMPPConnection connection;

	public ChatUtil(Context context) {
		ChatUtil.context = context;
	}

	public ChatUtil(XMPPConnection xmppConnection) {
		this.connection = xmppConnection;
	}

	public ChatUtil(Context context, XMPPConnection xmppConnection) {
		ChatUtil.context = context;
		this.connection = xmppConnection;
	}

//	/**
//	 * 登陆
//	 * 
//	 * @param username
//	 * @param password
//	 * @return -1 网络异常
//	 */
//	public int login(final String username, final String password) {
//		// 判断网络状态
//		if (!Utility.isConnectNet(context)) {
//			UIHelper.showToast(context, "网络异常");
//			return -1;
//		}
//		try {
//			connection.login(username, password);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//		return 1;
//	}

//	/**
//	 * 断开连接
//	 */
//	public static void disconnect() {
//		try {
//			if (XmppTool.getConnection().isConnected()) {
//				XmppTool.getConnection().disconnect();
//			}
//
//		} catch (NotConnectedException e) {
//			e.printStackTrace();
//		}
//
//	}

	/**
	 * 获得所有好友
	 */
	public List<RosterEntry> getAllFriends() {
		// 判断网络状态
		if (!Utility.isConnectNet(context)) {
			UIHelper.showToast(context, "网络异常");
			return null;
		}

		List<RosterEntry> entriesList = new ArrayList<RosterEntry>();
		Collection<RosterEntry> rosterEntry = getConnection().getRoster().getEntries();
		Iterator<RosterEntry> i = rosterEntry.iterator();
		while (i.hasNext()) {
			entriesList.add(i.next());
		}

		return entriesList;
	}

	/**
	 * 获得连接
	 * 
	 * @return
	 */
	public XMPPConnection getConnection() {
		return XmppTool.getConnection();
	}

	/**
	 * 获得群组List集合
	 * 
	 * @return
	 */
	public List<ChatGroup> getGroupsList() {
		Map<String, ?> extAccMap = (Map<String, ?>) StoredData.getExtAccMap();
		List<ChatGroup> groupsList = new ArrayList<ChatGroup>();
		Roster rs = XmppTool.getConnection().getRoster();
		Collection<RosterGroup> groups = rs.getGroups();
		int devicesIndex = -1; // 确定"我的设备"分组位置，用于交换分组到第一个

		Iterator<RosterGroup> iterator = groups.iterator();
		while (iterator.hasNext()) {
			// 有数据 则创建groupsList
			RosterGroup rosterGroup = iterator.next();
			String groupName = rosterGroup.getName();
			ChatGroup chatGroup = new ChatGroup();
			chatGroup.setGroupname(groupName);

			// 获得"我的设备"分组的索引  还未添加  不需要-1
			if(TYPE_WEAR_DEVICES.equals(groupName)) {
				devicesIndex = groupsList.size();
			}

			Collection<RosterEntry> entries = rosterGroup.getEntries(); // 获得分组内的所有成员
			Iterator<RosterEntry> iterator_member = entries.iterator();
			List<ChatGroupMember> memberList = null;

			int onlineCount = 0;
			while (iterator_member.hasNext()) {

				if (memberList == null) {
					memberList = new ArrayList<ChatGroupMember>();
				}
				RosterEntry rosterEntry = iterator_member.next();
				String username = rosterEntry.getUser();
				String alias = getName(rosterEntry);
				String warename = "";
				String avatarUrl = "";
				if(extAccMap.containsKey(FormatterUtil.getHalfname(username))) {
					@SuppressWarnings("unchecked")
					Set<String> set = (Set<String>) extAccMap.get(FormatterUtil.getHalfname(username));
					if(set.size()!=0 && set!=null) {
						Iterator<String> it = set.iterator();
						while(it.hasNext()) {
							String result = it.next();
							// 如果为Null 则没有图片
							if(TextUtils.isEmpty(result)) {
								avatarUrl = "";
							} else {
								if(result.startsWith("upload/account/")) {
									avatarUrl = result;
								} else {
									warename = result;
								}
							}
						}
					}
				}
				configChatServerDomain(username); 
				
				// 获得当前用户是否在线的状态
				Presence presence = rs.getPresence(username);
				String status = "";
				if (presence.getType() == Presence.Type.available) {	
					status = STATUS_ONLINE;
					onlineCount++; // 在线人数 + 1
				} else {
					status = STATUS_OFFLINE;
				}
				
				// 排序
				if (STATUS_ONLINE.equals(status)) {
					boolean isInserted = false;
					for (int index = 0; index < memberList.size(); index++) {
						if (STATUS_OFFLINE.equals(memberList.get(index).getStatus())) {
							memberList.add(index, new ChatGroupMember(null, avatarUrl, username, alias, status, warename, groupName));
							isInserted = true;
							break;
						}
					}

					// 如果没有插入 则在末尾进行添加
					if (!isInserted) {
						memberList.add(new ChatGroupMember(null, avatarUrl, username, alias, status, warename, groupName));
					}

				} else {
					memberList.add(new ChatGroupMember(null, avatarUrl, username, alias, status, warename, groupName));
				}

			}
			chatGroup.setMemberList(memberList);
			chatGroup.setOnlineNum(onlineCount); // 设置在线人数
			// 重置分组在线人数
			onlineCount = 0;

			groupsList.add(chatGroup);
		}

		// 将"我的设备"分组移至第一个
		if(devicesIndex!=-1) {
			groupsList.add(0, groupsList.get(devicesIndex));
			groupsList.remove(devicesIndex + 1);
		}

		// 缓存
		// WareHouseApplication.getThis().setContactsCacheList(groupsList);
		return groupsList;
	}

	/**
	 * 判断我的设备是否在线
	 * @param deviceName   XmppTool.TYPE_WEAR_GLASS  XmppTool.TYPE_WEAR_WATCH
	 * @return b
	 */
	public boolean isDevicesLoginIn(String deviceName) {
		if(TextUtils.isEmpty(deviceName)) return false;
		List<ChatGroup> groupsList = getGroupsList();
		for(ChatGroup chatGroup : groupsList) {
			if(TYPE_WEAR_DEVICES.equals(chatGroup.getGroupname())) {
				List<ChatGroupMember> memberList = chatGroup.getMemberList();
				for(ChatGroupMember member : memberList) {
					if(deviceName.equals(member.getAlias())) {
						return ChatUtil.STATUS_ONLINE.equals(member.getStatus());
					}
				}
				return false;
			}
 		}
		return false;
	}



	/**
	 * 缓存 服务器上的domain 例如账号 admin@192.168.1.15 则获取 @192.168.1.15
	 * 
	 * @param wholeUsername
	 *            admin@192.168.1.15
	 */
	private void configChatServerDomain(String wholeUsername) {
		int index = wholeUsername.indexOf("@");
		ChatServerDomain = wholeUsername.substring(index);
	}

	/**
	 * 获得所有分组          
	 */
	public Collection<RosterGroup> getGroups() {
		// 判断网络状态
		if (!Utility.isConnectNet(context)) {
			UIHelper.showToast(context, "网络异常");
			return null;
		}
		Collection<RosterGroup> groups = XmppTool.getConnection().getRoster().getGroups();

		Iterator<RosterGroup> iterator = groups.iterator();
		while (iterator.hasNext()) {
			RosterGroup rosterGroup = iterator.next();
			String groupName = rosterGroup.getName();
			Collection<RosterEntry> entries = rosterGroup.getEntries();
			Iterator<RosterEntry> iterator2 = entries.iterator();
			while (iterator2.hasNext()) {
				RosterEntry next = iterator2.next();
				String username = next.getUser();
				XLog.out("当前分组: " + groupName + "   该分组里的用户: " + username);
			}
		}
		return groups;
	}

	/**
	 * 所有所有好友
	 * 
	 * @return 所有好友的List
	 */
	public ArrayList<String> getFriends() {
		Roster roster = XmppTool.getConnection().getRoster();
		Collection<RosterEntry> entries = roster.getEntries();
		List<String> friendsList = new ArrayList<String>();

		for (RosterEntry rosterEntry : entries) {
			friendsList.add(rosterEntry.getUser());
		}
		return (ArrayList<String>) friendsList;
	}

	public static String getCurrentTime() {
		String FORMAT_TIME = "yyyy-MM-dd HH:mm:ss";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(FORMAT_TIME);
		return df.format(calendar.getTime());
	}

	public static String getFormatStatus(boolean isAvailable) {
		return isAvailable ? STATUS_ONLINE : STATUS_OFFLINE;
	}

	/**
	 * 返回昵称  (没有昵称则返回用户名)
	 * 
	 * @param rosterEntry
	 * @return alias
	 */
	public static String getName(RosterEntry rosterEntry) {
		String name = rosterEntry.getName();
		if (name != null && name.length() > 0) {
			return name;
		}
		name = StringUtils.parseName(rosterEntry.getUser());
		if (name.length() > 0) {
			return name;
		}
		return rosterEntry.getUser();
	}

	/**
	 * 根据Username获取别名Alias
	 * 
	 * @param username
	 *            如admin(不包括@ 和@以后的域)
	 * @return alias
	 */
	public static String findAliasByUsername(String username) {
		Roster rs = XmppTool.getConnection().getRoster();
		Collection<RosterGroup> groups = rs.getGroups();

		Iterator<RosterGroup> iterator = groups.iterator();
		while (iterator.hasNext()) {
			RosterGroup rosterGroup = iterator.next();
			Collection<RosterEntry> entries = rosterGroup.getEntries(); // 获得分组内的所有成员
			Iterator<RosterEntry> iterator_member = entries.iterator();

			while (iterator_member.hasNext()) {
				RosterEntry rosterEntry = iterator_member.next();
				String entryUsername = StringUtils.parseName(rosterEntry.getUser());
				if (username.equals(entryUsername)) {
					return getName(rosterEntry); // 返回alias
				}
			}
		}
		return STATUS_UNKNOWN;
	}
	
	/**
	 * 根据Username获取在线状态
	 * @param username  如  admin
	 * @return int     0: Online   1: Offline
	 */
	public int findStatusByUsername(String username) {
		if(TextUtils.isEmpty(username)) {
			return -1;
		}
		username = FormatterUtil.addDomain(username); 
		List<ChatGroup> groupsList = getGroupsList();
		
		if(groupsList == null) {
			return -1;
		}
		
		for(int i=0; i<groupsList.size(); i++) {
			ChatGroup chatGroup = groupsList.get(i);
			List<ChatGroupMember> memberList = chatGroup.getMemberList();
			if(memberList == null) continue;
			for(int m=0; m<memberList.size(); m++) {
				if(username.equals(memberList.get(m).getUsername())) {
					String status = memberList.get(m).getStatus();
					boolean isOnline = STATUS_ONLINE.equals(status);
					return isOnline ? INT_STATUS_ONLINE : INT_STATUS_OFFLINE;
				}
			}
		}
		return INT_STATUS_OFFLINE;
	}
	
	private static long Msg_Notification_Time = 0;
	private static SipProfile profMe;

	/**
	 * 状态栏弹出聊天消息 提示
	 * @param alias
	 * @param unamePeer
	 * @param msgBody
	 * @param msgTime
	 */
	public void showMsgNotification(String alias, String unamePeer, String msgBody, String msgTime) {
		int icon = R.drawable.icon;
		long when = System.currentTimeMillis();

		// 过滤图片和语音地址
		msgBody = ChatMessage.getReadableStr(msgBody);

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, msgBody, when);
		// 通知间隔小于1.5秒 则不进行声音提示
		notification.defaults = (when - Msg_Notification_Time > 1500)?Notification.DEFAULT_ALL:Notification.DEFAULT_LIGHTS;
		Msg_Notification_Time = when;

		Context app = SyncApplication.getThis();
		RemoteViews remoteView = new RemoteViews(app.getPackageName(),
				R.layout.chat_notify);
		remoteView.setImageViewResource(R.id.img, R.drawable.icon);
		// b.header
		remoteView.setTextViewText(R.id.tvTitle, "From: " + alias);
		remoteView.setTextViewText(R.id.tvMsg, msgBody);
		remoteView.setTextViewText(R.id.tvTime,
				FormatterUtil.getFormatTime(msgTime));
		notification.contentView = remoteView;

		Intent notificationIntent = new Intent(context, WelcomeActivity.class);
		WelcomeActivity.initParams(notificationIntent, WelcomeActivity.From_Msg_Notification);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notification.contentIntent = intent;
		
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(Utility.NotifyID_Chat, notification);

		/*//锁屏通知
		Intent in = new Intent(context, LockScreenNotifyActivity.class);
		LockScreenNotifyActivity.initParams(in, alias, msgTime, msgBody);
		context.startActivity(in);*/

	}

	/**
	 * 获得当前Activity任务栈最顶端的实例，如果和当前通知的聊天对象是同一人，则不进行状态栏提示
	 * @param unamePeer  例如 admin  (!!不包含@和后面的域)
	 * @return boolean 是否可以状态栏提示
	 */
	public boolean isShowNotification(String unamePeer) {
		
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);	
		
		// 若Sync处于后台进程，无论Sync中Act栈顶是哪个Act 都需要通知提醒
		if(!Utility.isRunningInForeground(context)) 
			return true;
		
		// 若当前为前台进程 并且当前正在ChatTabAct或者ChatAct时，不进行弹窗
		Activity topAc = ActivityStack.getTopActInstance();
		if(topAc != null)
			return true;
		if(topAc instanceof ChatTabActivity || topAc instanceof ChatActivity) {
			return false;
		}
		
		List<RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
		if (runningTaskInfos != null) {
			String topActName = runningTaskInfos.get(0).topActivity.toString();
			String targetActName = new ComponentName(context, ChatActivity.class).toString();
			String currChatPeername = ChatActivity.chatUserName;
			if (topActName.equals(targetActName) && !TextUtils.isEmpty(currChatPeername) && unamePeer.equals(StringUtils.parseName(currChatPeername))) {
				return false;
			}
		}
		return true;
	}

	public static ChatUtil getThis(Context context) {
		return new ChatUtil(context);
	}

	/**
	 * 根据Online / Offline 设置不同的头像
	 * @param hasDrawable
	 */
	public static Drawable changeIconStatus(String status, Drawable userIcon, boolean hasDrawable) {
		if (STATUS_ONLINE.equals(status)) {
			userIcon.setColorFilter(null);
			return userIcon;
		} else {
			userIcon.mutate();
			ColorMatrix cm = new ColorMatrix();
			cm.setSaturation(0);
			ColorMatrixColorFilter cf = new ColorMatrixColorFilter(cm);
			userIcon.setColorFilter(cf);
		}
		
		if(hasDrawable) {
			Bitmap bm = getRoundedCornerBitmap(drawableToBitmap(userIcon), 0.0f);
			return bitmapToDrawable(bm);
		} else {
			return userIcon;
		}
		
	}
	
	public static Drawable changeIconStatus(String status, Bitmap userIconBm, boolean hasDrawable) {
		Drawable userIcon;
		if(userIconBm == null) {
			userIcon = SyncApplication.getThis().getResources().getDrawable(R.drawable.account_circle);
		} else {
			userIcon = bitmapToDrawable(userIconBm);
		}
		if (STATUS_ONLINE.equals(status)) {
			userIcon.setColorFilter(null);
			//return userIcon;
			Bitmap bm = getRoundedCornerBitmap(drawableToBitmap(userIcon), 0.0f);
			return bitmapToDrawable(bm);
//			return userIcon;
		} else {
			userIcon.mutate();
			ColorMatrix cm = new ColorMatrix();
			cm.setSaturation(0);
			ColorMatrixColorFilter cf = new ColorMatrixColorFilter(cm);
			userIcon.setColorFilter(cf);
		}
		
		
//		return userIcon;
		if(hasDrawable) {
			Bitmap bm = getRoundedCornerBitmap(drawableToBitmap(userIcon), 0.0f);
			return bitmapToDrawable(bm);
		} else {
			return userIcon;
		}
	}
	
	public static Drawable bitmapToDrawable(Bitmap bm) {
		return new BitmapDrawable(SyncApplication.getThis().getResources(), bm);
	}
	
	/**
	 * Drawable 转 Bitmap
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmap(Drawable drawable) {  
		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(),
				drawable.getOpacity() != PixelFormat.OPAQUE ? 
						Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);  
				        Canvas canvas = new Canvas(bitmap);  
				        //canvas.setBitmap(bitmap);  
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());  
		drawable.draw(canvas);  
		return bitmap;  
	}  
	
	/**
	 * Bitmap圆角
	 * @param bitmap
	 * @param roundPx
	 * @return
	 */
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap,float roundPx){    
	           
	    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);    
	    Canvas canvas = new Canvas(output);    
	     
	    final int color = 0xff424242;    
	    final Paint paint = new Paint();    
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());    
	    final RectF rectF = new RectF(rect);    
	     
	    paint.setAntiAlias(true);    
	    canvas.drawARGB(0, 0, 0, 0);    
	    paint.setColor(color);    
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);    
	    paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));    
	    canvas.drawBitmap(bitmap, rect, rect, paint);    
		     
	    return output;    
	} 

	
	/**
	 * long类型的时间转换成  xx/xx xx:xx  如: 02/05 12:44
	 * @param millis
	 * @return
	 */
	public static String getDateStr(long millis) {
		SimpleDateFormat sdf= new SimpleDateFormat("MM/dd HH:mm");
		java.util.Date dt = new Date(millis);  
		String sDateTime = sdf.format(dt); 
		return sDateTime;
	}
	
	/**
	 * 是否和自己聊天
	 * @param unameMe
	 * @return
	 */
	public static boolean isChatWithSelf(String unameMe) {
		if(StoredData.getName().equals(unameMe))
			return true;
		return false;
	}
	
	
	public static boolean isSupportedSip() {
		if(ChatUtil.sipManager == null) {
			sipManager = SipManager.newInstance(SyncApplication.getThis());
		}
		return sipManager!=null;
	}
	
	/**
	 * 获得SipManager实例
	 * @return
	 */
	public static SipManager getSipManagerInstance() {
		if(ChatUtil.sipManager == null) {
			sipManager = SipManager.newInstance(SyncApplication.getThis());
		}
		return sipManager;
	}
	
	public static String sipServerDomain = "192.168.1.236:5060";
	/**
	 * 登陆 sip server
	 * @param account      1000153
	 * @param pwd          123456
	 * @param serverDomain 192.168.1.207:5060
	 */
	public static void login_sip(String account, String pwd, String serverDomain) {
		if(!isSupportedSip())  return; // 不支持就不登录
		try {
			SipProfile.Builder sipProfileBuilder = new SipProfile.Builder(account, serverDomain);
			sipProfileBuilder.setPassword(pwd);
			profMe = sipProfileBuilder.build();
			Intent i = new Intent();
            i.setAction("android.SipDemo.INCOMING_CALL");
            PendingIntent pi = PendingIntent.getBroadcast(SyncApplication.getThis(), 0, i, Intent.FILL_IN_DATA);
            sipManager.open(profMe, pi, null);
            
            if(sipRegListener == null) {
            	sipRegListener = new SipRegisterListener();
            }
            sipManager.setRegistrationListener(profMe.getUriString(), sipRegListener);
            System.out.println("");
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SipException e) {
			e.printStackTrace();
		}
		
	}
	
	private static class SipRegisterListener implements SipRegistrationListener {
		
		@Override
		public void onRegistrationFailed(String localProfileUri, int errorCode,
				String errorMessage) {
			System.out.println("");
		}
		
		@Override
		public void onRegistrationDone(String localProfileUri, long expiryTime) {
			System.out.println("");
		}
		
		@Override
		public void onRegistering(String localProfileUri) {
			System.out.println("");
		}
	}
	
	public static void unregisterSip() {
		if(profMe == null) return;
		try {
			getSipManagerInstance().unregister(profMe, null);
			getSipManagerInstance().close(profMe.getUriString());
			profMe = null;
		} catch (SipException e) {
			e.printStackTrace();
		}
	}
	
	// 获得登陆账号的Profile
		public static SipProfile getLocalProfile() {
			SipProfile.Builder builder = null;
			try {
				String account = StoredData.getName();
				String pwd = StoredData.getPwd();
//				String serverDomain = "192.168.1.19:5060";
				builder = new SipProfile.Builder(account, sipServerDomain).setPassword(pwd);
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return builder.build();
		}

		public static SipProfile getPeerProfile(String account) {
			SipProfile.Builder builder = null;
			try {
//				String serverDomain = "192.168.1.19:5060";
				builder = new SipProfile.Builder(account, sipServerDomain);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return builder.build();
		}
	
	public static void closeLocalProfile() {
        if (sipManager == null) {
            return;
        }
        try {
            if (profMe != null) {
            	sipManager.unregister(profMe, null);     // 反注册Profile , 服务器上会显示下线。
            	sipManager.close(profMe.getUriString());
            	profMe = null;
            }
        } catch (Exception ee) {
            Log.d("MainActivityActivity/onDestroy", "Failed to close local profile.", ee);
        }
        
        if(sipRegListener != null) {
        	sipRegListener = null;
        }
    }
	
	// Call ID 插入到数据库的主键
	public static String getCallId() {
		String timeString = String.valueOf(System.currentTimeMillis());
		return "Call"+timeString.substring(timeString.length() - 8);
	}
}
