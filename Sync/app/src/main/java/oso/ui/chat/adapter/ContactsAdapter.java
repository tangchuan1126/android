package oso.ui.chat.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.io.File;
import java.util.List;

import declare.com.vvme.R;
import oso.SyncApplication;
import oso.ui.chat.bean.ChatGroup;
import oso.ui.chat.bean.ChatGroupMember;
import oso.ui.chat.util.ChatUtil;
import utility.DisplayPictureUtil;
import utility.HttpUrlPath;
import utility.Utility;

public class ContactsAdapter extends BaseExpandableListAdapter {
	private Context context;
	public List<ChatGroup> groupsList;
	public ImageLoader imageLoader;
	public ContactsAdapter(Context context, List<ChatGroup> groupsList, ImageLoader imageLoader) {
		this.context = context;
		this.groupsList = groupsList;
		this.imageLoader = imageLoader;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ChildViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.chat_contacts_child_layout, null);
			holder = new ChildViewHolder();
			convertView.setTag(holder);
		} else {
			holder = (ChildViewHolder) convertView.getTag();
		}
		
		holder.ivAvatar = (ImageView) convertView.findViewById(R.id.chat_user_icon);
		holder.tvAlias = (TextView) convertView.findViewById(R.id.chat_contacts_childName);
		holder.tvWarename = (TextView) convertView.findViewById(R.id.chat_contacts_childWarename);


		ChatGroupMember member = groupsList.get(groupPosition).getMemberList().get(childPosition);
		String status = member.getStatus();
		String alias = member.getAlias();
		String warename = member.getWarename();
		if(!TextUtils.isEmpty(warename)) {
			holder.tvWarename.setVisibility(View.VISIBLE);
			holder.tvWarename.setText(warename+"");
		} else {
			holder.tvWarename.setVisibility(View.GONE);
		}
		String avatarUrl = HttpUrlPath.basePath + member.getAvatarURL();
		boolean hasAvatar = !TextUtils.isEmpty(member.getAvatarURL());
		setIvTag(hasAvatar, holder.ivAvatar, avatarUrl);
		
		// Offline头像置灰
		if(ChatUtil.STATUS_ONLINE.equals(status)) {
			holder.tvAlias.setTextColor(context.getResources().getColor(R.color.chat_contacts_online_color));
			if(hasAvatar) {
				imageLoader.displayImage(avatarUrl, holder.ivAvatar, DisplayPictureUtil.getChatAvatarDisplayImageOptions_Online());
			} else {
				holder.ivAvatar.setImageResource(R.drawable.account_circle);
			}
		} else {
			String cachePath = imageLoader.getDiscCache().get(avatarUrl).getPath();
			File tmpFile = new File(cachePath);
			boolean hasCache = tmpFile.exists();
			Drawable drawable = null;
			if(hasCache) {
				drawable = Drawable.createFromPath(cachePath);
			}
			boolean hasDrawable = (drawable != null);
			holder.tvAlias.setTextColor(context.getResources().getColor(R.color.chat_contacts_offline_color));
			if(hasDrawable) {
				holder.ivAvatar.setImageDrawable(ChatUtil.changeIconStatus(status, drawable, hasDrawable));
				drawable = null;
			} else {
				displayWithoutCache(holder, ChatUtil.STATUS_OFFLINE, avatarUrl, hasDrawable, DisplayPictureUtil.getChatAvatarDisplayImageOptions_Offline());
			}
		}
		
		holder.tvAlias.setText(alias);

		//---Devices----
		ChatGroup group = groupsList.get(groupPosition);
		boolean isDevicesGrp = ChatUtil.TYPE_WEAR_DEVICES.equals(group.getGroupname());
		boolean isGlass = ChatUtil.TYPE_WEAR_GLASS.equals(member.alias);
		boolean isOnline = ChatUtil.STATUS_ONLINE.equals(status);
		if(isDevicesGrp) {
			holder.tvWarename.setVisibility(View.GONE);
			Drawable drawable = context.getResources().getDrawable(isGlass ? R.drawable.ic_glass : R.drawable.ic_watch);
			Bitmap avatarBm = isOnline ? ChatUtil.drawableToBitmap(drawable) : Utility.getGrayBitmap(ChatUtil.drawableToBitmap(drawable));
			holder.ivAvatar.setImageBitmap(avatarBm);
		}

		return convertView;
	}

	/**
	 * 设置Tag 解决滑动闪烁
	 * @param hasAvatar
	 * @param ivAvatar
	 */
	private void setIvTag(boolean hasAvatar, ImageView ivAvatar, String avatarUrl) {
		if(hasAvatar) {
			ivAvatar.setTag(avatarUrl);
		}
	}

	public void displayWithoutCache(final ChildViewHolder holder, final String status, String url, boolean hasDrawable, DisplayImageOptions options) {
		if(url!=null) {
			// 服务器上有头像
			if(url.contains("upload/account/")) {
				loadImage(holder, status, url, options);
			} else { // 服务器没有头像  用R.drawable.account
				holder.ivAvatar.setImageDrawable(ChatUtil.changeIconStatus(status, SyncApplication.getThis().getResources().getDrawable(R.drawable.account_offline_circle), false));
			}
		}
	}
	
	public void loadImage(final ChildViewHolder holder, final String status,
			final String url, DisplayImageOptions options) {
		imageLoader.loadImage(url, options, new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				if(url.equals(holder.ivAvatar.getTag())) {
					holder.ivAvatar.setImageResource(ChatUtil.STATUS_ONLINE==status?R.drawable.account_circle:R.drawable.account_offline_circle);
				}
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				if(url.equals(holder.ivAvatar.getTag())) {
					holder.ivAvatar.setImageResource(ChatUtil.STATUS_ONLINE==status?R.drawable.account_circle:R.drawable.account_offline_circle);
				}
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap bm) {
				if(url.equals(holder.ivAvatar.getTag())) {
					holder.ivAvatar.setImageDrawable(ChatUtil.changeIconStatus(status, bm, true));
				}
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
			}
		});
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return groupsList.get(groupPosition).getMemberList().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		if (groupsList == null || groupsList.size() == 0) {
			return 0;
		} else {
			return groupsList.size();
		}

	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.chat_contacts_group_layout, null);
			holder = new GroupViewHolder();
			convertView.setTag(holder);
		} else {
			holder = (GroupViewHolder) convertView.getTag();
		}

		holder.tvGroupname = (TextView) convertView.findViewById(R.id.chat_contacts_groupname);
		holder.tvOnlinenum = (TextView) convertView.findViewById(R.id.chat_contacts_onlineNum);
		holder.tvTotalnum = (TextView) convertView.findViewById(R.id.chat_contacts_contactsNum);
		holder.tvGroupname.setText(groupsList.get(groupPosition).getGroupname());
		holder.tvOnlinenum.setText("" + groupsList.get(groupPosition).getOnlineNum()); 
		holder.tvTotalnum.setText("/" + groupsList.get(groupPosition).getTotalNum());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	class GroupViewHolder {
		TextView tvGroupname; // 组名
		TextView tvOnlinenum; // 在线人数
		TextView tvTotalnum; // 好友总人数
	}

	class ChildViewHolder {
		ImageView ivAvatar; // 用户头像
		TextView tvAlias; // 用户名的别名
		TextView tvWarename;
	}
}
