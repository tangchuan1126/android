package oso.ui.chat.bean;

import java.io.Serializable;

/**
 * Bean: ChatUserInfo 聊天用户的头像，用户名，状态，所属用户组信息
 * 
 * @author xialimin
 * 
 */
public class ChatOtherImageBean implements Serializable {
	private static final long serialVersionUID = 470422651618471229L;
	public int id;
	public String file_path; // 仓库名
	
	public int activity;
}
