package oso.ui.chat.bean;
/**
 * JavaBean 封装登录的用户信息，用于请求Chat服务器
 * @author xialimin
 *
 */
public class LoginUserInfo {
	private String username;
	private String password;
	public LoginUserInfo(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public LoginUserInfo() {
		super();
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "ChatUserInfo [username=" + username + ", password=" + password
				+ "]";
	}
	
	
}
