package oso.ui.chat.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.gson.Gson;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException.StreamErrorException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.StreamError;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import declare.com.vvme.R;
import oso.SyncApplication;
import oso.base.ActivityStack;
import oso.base.BaseActivity;
import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.chat.ext.MyOfflineMsgMnger;
import oso.ui.main.Login;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.screennotice.SyncNotifyManager;
import support.AppConfig;
import support.common.push.MsgBean;
import support.common.push.MyPushService;
import support.dbhelper.DBManager;
import support.dbhelper.StoredData;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import utility.Utility;



/**
 * @author 朱成
 * @date 2015-1-25
 */
public class XmppTool {
	private static final String TAG = XmppTool.class.getSimpleName();
	private static final String SYS_NOTIFY_FLAG = "[SystemNotifyMsgAppend]"; // 系统消息标识, 用于"广播任务"
//	private static final String SYS_WEAR_FLAG = "[WearMsgCommand]";  // 可穿戴设备命令消息标识

	private XMPPConnection conn = null;
	private static XmppTool instance;
	
	public static XmppTool getThis() {
		if (instance == null) 
			instance = new XmppTool();
		//可能切换用户,故每次getThis均需调
		instance.init();
		return instance;
	}
	
	private void init(){
		
		if (conn == null)
			resetConnection();
		
		//尝试登录
//		login_of();
	}
	
	/**
	 * 重置connection,以支持切换账号
	 */
	private void resetConnection(){
		//有网络访问
		if(conn!=null && conn.isConnected()){
			try {
				conn.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		ConnectionConfiguration connConfig = new ConnectionConfiguration(
				HttpUrlPath.chatServerUrl(), HttpUrlPath.Xmpp_Port);
		connConfig.setSecurityMode(SecurityMode.disabled);// 设置为disabled
//		connConfig.setSASLAuthenticationEnabled(false);//设置为false
		//坑爹啊,拉取离线消息 必须等超时
		SmackConfiguration.setDefaultPacketReplyTimeout(10000);
 		connConfig.setCompressionEnabled(true);
 		//自己处理重连,重连管理器-仅处理了closedOnError
// 		connConfig.setReconnectionAllowed(false);
// 	    SASLAuthentication.supportSASLMechanism("PLAIN");
 		
 		//debug
// 		connConfig.setDebuggerEnabled(true);
		//离线才能拿到-离线消息(认证后再上线)
		connConfig.setSendPresence(false);
		conn = new XMPPTCPConnection(connConfig);
		//自动消息回执
		DeliveryReceiptManager.getInstanceFor(conn).enableAutoReceipts();
	}

	/**
	 * 一定有值
	 * @return
	 */
	public static XMPPConnection getConnection() {
//		if (conn == null ) {
//			openConnection();
//		}
		return getThis().conn;
	}
	
	private ChatManager chatManager;
	private SyncApplication ct=SyncApplication.getThis();
	
	/**
	 * 防止重复登录
	 */
	private boolean isLogining=false;	
	
	/**
	 * 尝试登录openfire,可在ui线程调
	 */
	public void login_of() {
		
		XLog.out("nimei>>XmppTool.login_of");
		
		//debug
		if(AppConfig.Debug_NotLogin_Openfire)
			return;

		//若已认证
		if (conn != null && conn.isAuthenticated()){
			String username=FormatterUtil.getHalfname(conn.getUser());
			
			//若"of账号"同"oso"
			if(username.equals(StoredData.getName()))
				return;
			//若换了账号,则重连
			else
				resetConnection();
		}
		
		// 若oso未登录
		if (!StoredData.isLogin())
			return;
			
		if(isLogining)
			return;
		isLogining=true;
		new Thread() {
			public void run() {
				login_of_inner();
				isLogining=false;
			};
		}.start();
	}
	
//	/**
//	 * 尝试登录openfire,可在ui线程调
//	 */
//	public void login_of_with_pwdchanged() {
//		
//		XLog.out("nimei>>XmppTool.login_of");
//		
//		//debug
//		if(AppConfig.Debug_NotLogin_Openfire)
//			return;
//
//		//若已认证
//		if (conn != null && conn.isAuthenticated()){
//			String username=FormatterUtil.getHalfname(conn.getUser());
//			resetConnection();
//		}
//		
//		// 若oso未登录
//		if (!StoredData.isLogin())
//			return;
//			
//		if(isLogining)
//			return;
//		isLogining=true;
//		new Thread() {
//			public void run() {
//				login_of_inner();
//				isLogining=false;
//			};
//		}.start();
//	}
	
	/**
	 * 断开openFire,需在子线程调 有网络访问
	 */
	public void loginOut_of() {
		// 若未连接
		if (conn == null || !conn.isConnected()) {
			conn = null;
			return;
		}

		new Thread() {

			public void run() {
				if (conn == null)
					return;
				try {
					conn.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
				// 重置conn,防止不能换账号
				conn = null;
			};
		}.start();

	}
	
	/**
	 * 断开openFire,需在子线程调 有网络访问
	 */
	public void changePwd_of() {
		// 若未连接
		if (conn == null) {
			login_of();
			return;
		}

		new Thread() {

			public void run() {
				// 若未连接
//				if (conn == null) {
//					login_of();
//					return;
//				}
				
				if(conn!=null && conn.isConnected()){
					try {
						conn.disconnect();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				// 重置conn,防止不能换账号
				conn = null;
				
				login_of();
			};
		}.start();

	}
	
	private void login_of_inner(){
		
		//之前应已初始化,退出时 可能将其干掉
		if(conn==null)
			return;
		
		// 监听-连接状态
		conn.addConnectionListener(onConnectionChanged);
		
		//==================连接、登陆===============================
		
		tryConnect();
		tryAuth();
		
//		ChatUtil.storeWarehouseName();
		
		//仍未登上,延迟重连
		if(conn==null || !conn.isAuthenticated())
		{
			XLog.out("nimei>>XmppTool_tryConn_fail");
			handler.postDelayed(run_Reconnect, 10000);
			return;
		}
		
		//=================================================
		
		chatManager = ChatManager.getInstanceFor(conn);
		// 监听-消息
		chatManager.addChatListener(onReceiveMsg);
//		conn.getRoster().addRosterListener(onRosterChange);
		conn.getRoster();
	}
	
	/**
	 * 重连
	 */
	private Runnable run_Reconnect=new Runnable() {
		
		@Override
		public void run() {
			login_of();
		}
	};
	
	private boolean tryConnect(){
		if(conn==null)
			return false;
		if(conn.isConnected())
			return true;
		try {
			XLog.out("nimei>>XmppTool__tryConnect");
			conn.connect();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void tryAuth(){
		if(conn==null)
			return;
		//已登录 则跳出
		if (conn.isAuthenticated())
			return;

		// 登录
		try {
			XLog.out("nimei>>XmppTool__tryAuth");
//			if (StoredData.isLogin())
			conn.login(StoredData.getName(), StoredData.getPwd());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//==================private===============================
	
//	/**
//	 * 建立连接时需挂载其,否则状态可能不对
//	 */
//	private RosterListener onRosterChange=new RosterListener() {
//		
//		@Override
//		public void presenceChanged(Presence presence) {
//		}
//		
//		@Override
//		public void entriesUpdated(Collection<String> arg0) {
//		}
//		
//		@Override
//		public void entriesDeleted(Collection<String> arg0) {
//		}
//		
//		@Override
//		public void entriesAdded(Collection<String> arg0) {
//		}
//	};
	
	/**
	 * 连接状态-改变时
	 */
	private ConnectionListener onConnectionChanged=new ConnectionListener() {
		
		@Override
		public void reconnectionSuccessful() {
			XLog.out("nimei>>XmppTool_reconn_ok");
		}
		
		@Override
		public void reconnectionFailed(Exception arg0) {
			XLog.out("nimei>>XmppTool_reconn_fail");
		}
		
		@Override
		public void reconnectingIn(int arg0) {
			XLog.out("nimei>>XmppTool_recon_"+arg0);
		}
		
		@Override
		public void connectionClosedOnError(Exception arg0) {
			XLog.out("nimei>>XmppTool_closedOnError");
			//异地登录时,其他异常会触发"自动重连"
			if(isConflictErr(arg0)){
				ct.getHandler().post(new Runnable() {
					
					@Override
					public void run() {
						onLoginElse();
					}
				});
				
				StreamErrorException tmp=(StreamErrorException)arg0;
				tmp.printStackTrace();
			}
			
		}
		
		@Override
		public void connectionClosed() {
			XLog.out("nimei>>XmppTool_closed");
			login_of();
		}
		
		@Override
		public void connected(XMPPConnection arg0) {
			XLog.out("nimei>>XmppTool_connected");
		}
		
		//连接上时,一定进来
		@Override
		public void authenticated(final XMPPConnection con) {
			XLog.out("nimei>>XmppTool_auth");
			
			//若直接处理,可能卡住chatActivity
			new Timer().schedule(new TimerTask() {
				
				@Override
				public void run() {
					getOfflineMsgs(con);
				}
			},100);
		}
	};
	
	/**
	 * 是否为-异地登录
	 * @param e
	 * @return
	 */
	private boolean isConflictErr(Exception e){
		if (e instanceof StreamErrorException) {
            StreamErrorException xmppEx = (StreamErrorException) e;
            StreamError error = xmppEx.getStreamError();
            String reason = error.getCode();

            if ("conflict".equals(reason)) {
                return true;
            }
        }
		return false;
	}
	
	/**
	 * 获取-离线消息,注:此处con可能已被logout 此处会各种throw 但无影响
	 */
	private void getOfflineMsgs(XMPPConnection con){
		System.out.println("nimei>>XmppTool_offlineMsgCnt_startPull");
		
		//离线消息-处理
		long time0=System.currentTimeMillis();
		MyOfflineMsgMnger offMnger=new MyOfflineMsgMnger(con);
		List<Message> listMsgs = null;
		try {
			listMsgs=offMnger.getMessages();
			//若拉取部分后报错 仍不会删除"已拉去部分"
			offMnger.deleteMessages();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//上线
		try {
			con.sendPacket(new Presence(Presence.Type.available));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long dur=System.currentTimeMillis()-time0;
		int cnt=listMsgs==null?0:listMsgs.size();
		XLog.out("nimei>>XmppTool_offlineMsgCnt_"+cnt+",time:"+dur);
	}
	
	
	private static Handler handler=SyncApplication.getThis().getHandler();
	
	/**
	 * 异地登录
	 */
	private void onLoginElse(){
		final Activity ac=ActivityStack.getTopActInstance();
		if(ac==null)
			return;
		RewriteBuilderDialog.Builder builder=new RewriteBuilderDialog.Builder(ac);
		builder.setMessage(ac.getString(R.string.login_other_logined));
		builder.hideCancelBtn();
		builder.setCanceledOnTouchOutside(false);
		builder.setPositiveButton(ac.getString(R.string.login_relogin), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(ac instanceof BaseActivity)
					((BaseActivity) ac).onRelogin();
					
				//重新登录
				login_of();
			}
		});
		builder.setNegativeButton(ac.getString(R.string.login_logout), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Utility.popTo(ac, Login.class);
				//清空-登录状态
				StoredData.logout();
			}
		}).show();
	}

	/**
	 * 消息监听
	 */
	private ChatManagerListener onReceiveMsg = new ChatManagerListener() {

		@Override
		public void chatCreated(Chat chat, boolean b) {
			chat.addMessageListener(new MessageListener() {

				@Override
				public void processMessage(Chat chat, org.jivesoftware.smack.packet.Message msg) {
					onGetMsg(msg);
				}
			});
		}
	};
	
	/**
	 * 获取消息时,在线/离线-消息
	 */
	private void onGetMsg(Message msg) {
		
		//直接滤去-非聊天消息,如:Type.normal可能为"回执"
		if(msg.getType() != Message.Type.chat)
			return;
		
		//系统消息====================================================
		if(isSysNotify(msg.getBody())) {
			XLog.d(TAG, msg.getBody());
			processSysMsg(msg);
			return;
		}

		//可穿戴设备命令==============================================
		/*if(isWearCommand(msg.getBody())) {
			//TODO
			return;
		}*/

		//聊天消息====================================================
		
		//构造-消息
		final String unamePeer = StringUtils.parseName(msg.getFrom());
		String id = msg.getPacketID();
		String unameMe = StoredData.getName();
		final ChatMessage chatMessage = new ChatMessage(id, ChatUtil.getCurrentTime(), msg.getBody(), unameMe, unamePeer, false, false);
		// 发送通知前确保Roster里有数据
		if(XmppTool.getConnection().getRoster().getEntryCount() == 0) {
			new Thread() {
				@Override
				public void run() {
					XmppTool.getConnection().getRoster();
					processMsg(unamePeer, chatMessage);
				}
			}.start();
		} else {
			processMsg(unamePeer, chatMessage);
		}
	}

	public void processMsg(final String unamePeer, final ChatMessage chatMessage) {
		String alias = ChatUtil.findAliasByUsername(unamePeer);
		chatMessage.setAliasPeer(alias);
		
		//若正在聊(判断是否属于前台进程)
		updateWhenInBg();
		if(ChatActivity.isChatingWith(unamePeer))
			chatMessage.setRead(true);
		//插入db
		DBManager.getThis().insert(chatMessage);
		
		// 发送广播
		Intent intent = new Intent("com.vvme.chat.NEW_MESSAGE_ACTION");
		Bundle bundle = new Bundle();
		bundle.putSerializable("NewMessage", chatMessage);
		intent.putExtras(bundle);
		SyncApplication.getThis().sendOrderedBroadcast(intent, null);
		
		//debug,聊天页不显示
		Activity top=ActivityStack.getTopActInstance();
		boolean notToNotify = false;
		if (top instanceof ChatActivity || top instanceof ChatTabActivity)
			notToNotify = true;
		// 如看不见app,仍然响
		if (!Utility.isAppInView(ct))
			notToNotify = false;
		
		//抛通知
		if(!notToNotify)
			showMsgNotification(alias, unamePeer, chatMessage.getMessageBody(), chatMessage.getMessageTime());

		SyncNotifyManager.showNotify(false, alias, chatMessage.getMessageTime(), chatMessage.getMessageBody());
	}
	
	
	/**
	 * 处在后台进程时
	 * 针对的情况: 若ChatAct当前在栈顶，home键最小化。收消息点开通知PI ---> 进入ChatTabAct
	 * 为避免使用singleInstance。判断处于后台进程，接收到消息，通知栏通知，PI点击进入需要销毁ChatAct和ChatTabAct
	 */
	private void updateWhenInBg() {
		if(!Utility.isRunningInForeground(ct)) {
			Activity actChat = ActivityStack.getAct(ChatActivity.class.getName());
			Activity actChatTab = ActivityStack.getAct(ChatTabActivity.class.getName());
			if(actChat != null) {
				ActivityStack.removeAct(actChat);
				actChat.finish();
			}
			if(actChatTab != null) {
				ActivityStack.removeAct(actChatTab);
				actChatTab.finish();
			}
		}
	}
	
	// 状态栏通知
	public void showMsgNotification(String alias, String unamePeer, String messageBody, String messageTime) {
		if(ChatUtil.getThis(SyncApplication.getThis()).isShowNotification(unamePeer)) {
			ChatUtil.getThis(SyncApplication.getThis()).showMsgNotification(alias, unamePeer, messageBody, messageTime);
		}
	}

	//==================可穿戴设备消息处理==========================
	/*private boolean isWearCommand(String msgBody) {
		int index = msgBody.indexOf(SYS_WEAR_FLAG);
		return index == 0;
	}*/

	//==================系统消息处理===============================

	/**
	 * 为系统通知
	 * 
	 * @param msgBody body  消息体。  如果是系统通知，则会带前缀 [SystemNotifyMsgAppend]
	 * @return
	 */
	private boolean isSysNotify(String msgBody) {
		int index = msgBody.indexOf(SYS_NOTIFY_FLAG);
		return index == 0;
	}

	private long lastTime_sysNotify; // 上一次,系统通知时间
	private final int Interval_SysNotify = 1000 * 30;	//最小-抛出间隔
	private Message sysMsg_delayToNotify; // 延迟抛的消息

	/**
	 * 处理系统消息
	 */
	private void processSysMsg(Message msg) {
		
		// Json去掉系统消息的标示
		String json1 = msg.getBody().substring(SYS_NOTIFY_FLAG.length());
		MyPushService.broadcastPush(false, json1);

		MsgBean msgBean = new Gson().fromJson(json1, MsgBean.class);
		SyncNotifyManager.showNotify(true, msgBean.title, null, msgBean.content);

//		暂时不加延时
		if(true)
			return;

		long curTime = System.currentTimeMillis();
		// 若在间隔内,延迟抛出
		if (curTime - lastTime_sysNotify < Interval_SysNotify) {
			sysMsg_delayToNotify = msg;
			return;
		}
		lastTime_sysNotify = curTime;
		
		// Json去掉系统消息的标示
		String json = msg.getBody().substring(SYS_NOTIFY_FLAG.length());
		MyPushService.broadcastPush(false, json);
		// 延迟抛出-后续消息
		handler.postDelayed(run_delayNotify, Interval_SysNotify);
	}

	private Runnable run_delayNotify = new Runnable() {

		@Override
		public void run() {
			XLog.out("nimei>>run_delay");
			//若有"延迟消息",则跟新"抛出点"
			//没有"延迟消息","抛出点"间隔已过,可再次"及时抛"
			if (sysMsg_delayToNotify != null) {
				String json = sysMsg_delayToNotify.getBody().substring(SYS_NOTIFY_FLAG.length());
				MyPushService.broadcastPush(false, json);
				
				//更新-抛出点
				lastTime_sysNotify=System.currentTimeMillis();
				sysMsg_delayToNotify=null;
			}
		}
	};

}
