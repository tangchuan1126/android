package oso.ui.chat.ext.sipdroid.sipua.ui;

/*
 * Copyright (C) 2009 The Sipdroid Open Source Project
 * 
 * This file is part of Sipdroid (http://www.sipdroid.org)
 * 
 * Sipdroid is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import org.json.JSONObject;

import oso.ui.chat.bean.ChatOtherBean;
import oso.ui.chat.ext.sipdroid.media.RtpStreamReceiver;
import oso.ui.chat.ext.sipdroid.sipua.UserAgent;
import oso.ui.chat.ext.sipdroid.sipua.phone.Call;
import oso.ui.chat.ext.sipdroid.sipua.phone.Connection;
import oso.ui.chat.ext.sipdroid.sipua.phone.Phone;
import oso.ui.chat.util.ChatUtil;
import oso.widget.RoundImageView;
import support.network.SimpleJSONUtil;
import utility.DisplayPictureUtil;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

public class InCallScreen extends CallScreen implements View.OnClickListener, View.OnTouchListener, OnCheckedChangeListener, SensorEventListener {

	final int MSG_ANSWER = 1;
	final int MSG_ANSWER_SPEAKER = 2;
	final int MSG_BACK = 3;
	final int MSG_TICK = 4;
	final int MSG_POPUP = 5;
	final int MSG_ACCEPT = 6;
	
	final int SCREEN_OFF_TIMEOUT = 12000;
	private boolean Mode_Speaker = false; // 默认为听筒
	Phone ccPhone;
	int oldtimeout;
	SensorManager sensorManager;
    Sensor proximitySensor;
    boolean first;
	
    private RoundImageView ivDefaultAvatar;
    private CheckBox cbMic;
	private CheckBox cbSpeaker;
	private Button btnAnswer;
	private Button btnHungUp;
	private ImageView ivHungupIcon;
	private ImageView ivAnswerIcon;
	private TextView tvStatus;
	private TextView tvPeerName;
	private static FrameLayout flAnswerLayout;
	private Chronometer chTimer;
	public static Context mContext;
	
	private int Type_Call;
	public static int Type_Call_Outgoing = 0;
	public static int Type_Call_Incoming = 1;
	
	public static void initParams(Intent intent, int Call_Type) {
		intent.putExtra("call_type", Call_Type);
	}
	
	public void applyParams() {
		Type_Call = getIntent().getIntExtra("call_type", Type_Call_Incoming);
		mContext = this;
		vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	void screenOff(boolean off) {
        ContentResolver cr = getContentResolver();
        
        if (proximitySensor != null)
        	return;
        if (off) {
        	if (oldtimeout == 0) {
        		oldtimeout = android.provider.Settings.System.getInt(cr, android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 60000);
        		android.provider.Settings.System.putInt(cr, android.provider.Settings.System.SCREEN_OFF_TIMEOUT, SCREEN_OFF_TIMEOUT);
        	}
        } else {
        	if (oldtimeout == 0 && android.provider.Settings.System.getInt(cr, android.provider.Settings.System.SCREEN_OFF_TIMEOUT, 60000) == SCREEN_OFF_TIMEOUT)
        		oldtimeout = 60000;
        	if (oldtimeout != 0) {
        		android.provider.Settings.System.putInt(cr, android.provider.Settings.System.SCREEN_OFF_TIMEOUT, oldtimeout);
        		oldtimeout = 0;
        	}
        }
	}
	
	@Override
	public void onStop() {
		super.onStop();
		mHandler.removeMessages(MSG_BACK);
		mHandler.removeMessages(MSG_ACCEPT);
		mHandler.sendEmptyMessage(MSG_ACCEPT);
		if (Receiver.call_state == UserAgent.UA_STATE_IDLE)
			finish();
		sensorManager.unregisterListener(this);
		started = false;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		if (Receiver.call_state == UserAgent.UA_STATE_IDLE)
     		mHandler.sendEmptyMessageDelayed(MSG_BACK, Receiver.call_end_reason == -1?
    				2000:5000);
	    first = true;
	    pactive = false;
	    pactivetime = SystemClock.elapsedRealtime();
	    sensorManager.registerListener(this,proximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
	    started = true;
	}

	@Override
	public void onPause() {
		super.onPause();
    	if (!Sipdroid.release) Log.i("SipUA:","on pause");
    	switch (Receiver.call_state) {
    	case UserAgent.UA_STATE_INCOMING_CALL:
    		if (!RtpStreamReceiver.isBluetoothAvailable()) Receiver.moveTop(InCallScreen.Type_Call_Incoming);
    		break;
    	case UserAgent.UA_STATE_IDLE:
    		if (Receiver.ccCall != null)
     		mHandler.sendEmptyMessageDelayed(MSG_BACK, Receiver.call_end_reason == -1?
    				2000:5000);
    		break;
    	}
		if (t != null) {
			running = false;
			t.interrupt();
		}
		screenOff(false);
	}
	
	void moveBack() {
		/*if (Receiver.ccConn != null && !Receiver.ccConn.isIncoming()) {
	        startActivity(Receiver.createHomeIntent());
		}
		onStop();*/
		InCallScreen.this.finish();
	}
	
	//Context mContext = this;

	@Override
	public void onResume() {
		super.onResume();
    	if (!Sipdroid.release) Log.i("SipUA:","on resume");
		switch (Receiver.call_state) {
		case UserAgent.UA_STATE_INCOMING_CALL:
			if (Receiver.pstn_state == null || Receiver.pstn_state.equals("IDLE"))
				if (PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(Settings.PREF_AUTO_ON, Settings.DEFAULT_AUTO_ON) &&
						!mKeyguardManager.inKeyguardRestrictedInputMode())
					mHandler.sendEmptyMessageDelayed(MSG_ANSWER, 1000);
				else if ((PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(Settings.PREF_AUTO_ONDEMAND, Settings.DEFAULT_AUTO_ONDEMAND) &&
						PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(Settings.PREF_AUTO_DEMAND, Settings.DEFAULT_AUTO_DEMAND)) ||
						(PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(Settings.PREF_AUTO_HEADSET, Settings.DEFAULT_AUTO_HEADSET) &&
								Receiver.headset > 0))
					mHandler.sendEmptyMessageDelayed(MSG_ANSWER_SPEAKER, 10000);
			break;
		case UserAgent.UA_STATE_INCALL:
			if (Receiver.docked <= 0)
				screenOff(true);
			// 取消动画
			if(ivDefaultAvatar!=null) ivDefaultAvatar.clearAnimation();
			break;
		case UserAgent.UA_STATE_IDLE:
			if (!mHandler.hasMessages(MSG_BACK))
				moveBack();
			// 取消动画
			if(ivDefaultAvatar!=null) ivDefaultAvatar.clearAnimation();
			break;
		}
		if (Receiver.call_state != UserAgent.UA_STATE_INCALL) {
		}
		mHandler.sendEmptyMessage(MSG_TICK);
	}
	
    Handler mHandler = new Handler() {
    	public void handleMessage(Message msg) {
    		switch (msg.what) {
    		case MSG_ANSWER:
        		if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL)
        			answer();
        		break;
    		case MSG_ANSWER_SPEAKER:
        		if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL) {
        			answer();
    				Receiver.engine(mContext).speaker(AudioManager.MODE_NORMAL);
        		}
        		break;
    		case MSG_BACK:
    			moveBack();
    			break;
    		case MSG_TICK:
    			break;
    		case MSG_POPUP:
    			break;
    		case MSG_ACCEPT:
    	        setScreenBacklight((float) -1);
    	        getWindow().setFlags(0, 
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
				ContentResolver cr = getContentResolver();
    			if (hapticset) {
    				android.provider.Settings.System.putInt(cr, android.provider.Settings.System.HAPTIC_FEEDBACK_ENABLED, haptic);
    				hapticset = false;
    			}
    			break;
    		}
    	}
    };

    public void initInCallScreen() {
    	tvStatus = (TextView) findViewById(R.id.tvCallStatus);
		tvPeerName = (TextView) findViewById(R.id.tvPeerName);
		btnAnswer = (Button) findViewById(R.id.btnCallAnswer);
		btnHungUp = (Button) findViewById(R.id.btnCallHungUp);
		cbMic = (CheckBox) findViewById(R.id.cbMicMute);
		cbSpeaker = (CheckBox) findViewById(R.id.cbSpeaker);
		ivHungupIcon = (ImageView) findViewById(R.id.ivHungupIcon);
		ivAnswerIcon = (ImageView) findViewById(R.id.ivAnswerIcon);
		flAnswerLayout = (FrameLayout) findViewById(R.id.flAnswerLayout);
		ivDefaultAvatar = (RoundImageView) findViewById(R.id.ivDefaultAvatar);
		
		cbMic.setOnCheckedChangeListener(this);
		cbSpeaker.setOnCheckedChangeListener(this);
		btnAnswer.setOnClickListener(this);
		btnHungUp.setOnClickListener(this);
		ivHungupIcon.setOnClickListener(this);
		ivHungupIcon.setOnTouchListener(this);
		ivAnswerIcon.setOnClickListener(this);
		ivAnswerIcon.setOnTouchListener(this);
		
		flAnswerLayout.setVisibility(Type_Call == Type_Call_Incoming ? View.VISIBLE : View.GONE);
		ivAnswerIcon.setVisibility(View.VISIBLE);
		ivHungupIcon.setVisibility(View.VISIBLE);
		btnAnswer.setVisibility(View.VISIBLE);
		btnHungUp.setVisibility(View.VISIBLE);
		
		chTimer = (Chronometer) findViewById(R.id.chTimerCall);
		// 暂时隐藏选择项 debug
		findViewById(R.id.llOptionsLayout).setVisibility(View.INVISIBLE);
    }
    
	Thread t;
	boolean running;
	public static boolean started;

    @Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		applyParams();
		setContentView(R.layout.act_chat_call);
		
		initInCallScreen();
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        if(!android.os.Build.BRAND.equalsIgnoreCase("archos"))
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        
        showPeerInfo();
    }	
		
    // 显示对方名 和 头像
	private void showPeerInfo() {
		String address = Receiver.ccCall.getEarliestConnection().getAddress();
        tvPeerName.setText(getAliasName(address));
        detailByEntry(address);
	}

	private String getAliasName(String address) {
		if(TextUtils.isEmpty(address)) return "";
		address = FormatterUtil.getHalfname(address);
		return ChatUtil.findAliasByUsername(address);
	}

	public void reject() {
		if (Receiver.ccCall != null) {
			Receiver.stopRingtone();
			Receiver.ccCall.setState(Call.State.DISCONNECTED);
		}
		//debug
		if(chTimer != null) endChronometer();
		ivDefaultAvatar.clearAnimation();
		Connection.setRejected(true); // 是挂断的
		Receiver.engine(mContext).rejectcall();
    }
	
	public static void hideAnswerLayout() {
		if(flAnswerLayout != null)
			flAnswerLayout.setVisibility(View.GONE);
	}
	
	public void answer() {
		cbMic.setVisibility(View.VISIBLE);
		cbSpeaker.setVisibility(View.VISIBLE);
		flAnswerLayout.getHandler().postDelayed(new Runnable() {
			@Override
			public void run() {
				flAnswerLayout.setVisibility(View.GONE);
			}
		}, 400);
		if (Receiver.ccCall != null) {
			Receiver.ccCall.setState(Call.State.ACTIVE);
			Receiver.ccCall.base = SystemClock.elapsedRealtime();
		}
		Receiver.engine(mContext).answercall();
		
		// 取消动画
		if(ivDefaultAvatar!=null) ivDefaultAvatar.clearAnimation();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_MENU:
        	if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL /*&& mSlidingCardManager == null*/) {
        		answer();
				return true;
        	}
        	break;
        
        case KeyEvent.KEYCODE_CALL:
        	switch (Receiver.call_state) {
        	case UserAgent.UA_STATE_INCOMING_CALL:
        		answer();
        		break;
        	case UserAgent.UA_STATE_INCALL:
        	case UserAgent.UA_STATE_HOLD:
       			Receiver.engine(this).togglehold();
       			break;
        	}
            return true;

        case KeyEvent.KEYCODE_BACK:
            return true;

        case KeyEvent.KEYCODE_CAMERA:
        	return true;
        	
        case KeyEvent.KEYCODE_VOLUME_DOWN:
        case KeyEvent.KEYCODE_VOLUME_UP:
        	if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL) {
        		Receiver.stopRingtone();
        		return true;
        	}
        	RtpStreamReceiver.adjust(keyCode,true);
        	return true;
        }
        if (Receiver.call_state == UserAgent.UA_STATE_INCALL) {
	        char number = event.getNumber();
	        if (Character.isDigit(number) || number == '*' || number == '#') {
	        	return true;
	        }
        }
        return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);

		MenuItem m = menu.add(0, DTMF_MENU_ITEM, 0, R.string.menu_dtmf);
//		m.setIcon(R.drawable.ic_menu_dial_pad);
		return result;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean result = super.onPrepareOptionsMenu(menu);

		menu.findItem(DTMF_MENU_ITEM).setVisible(Receiver.call_state == UserAgent.UA_STATE_INCALL);
		return !(pactive || SystemClock.elapsedRealtime()-pactivetime < 1000);
	}
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case DTMF_MENU_ITEM:
			//mDialerDrawer.animateOpen();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
        case KeyEvent.KEYCODE_VOLUME_DOWN:
        case KeyEvent.KEYCODE_VOLUME_UP:
        	RtpStreamReceiver.adjust(keyCode,false);
        	return true;
        case KeyEvent.KEYCODE_ENDCALL:
        	if (Receiver.pstn_state == null ||
				(Receiver.pstn_state.equals("IDLE") && (SystemClock.elapsedRealtime()-Receiver.pstn_time) > 3000)) {
        			reject();      
        			return true;		
        	}
        	break;
		}
		Receiver.pstn_time = 0;
		return false;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	void setScreenBacklight(float a) {
        WindowManager.LayoutParams lp = getWindow().getAttributes(); 
        lp.screenBrightness = a; 
        getWindow().setAttributes(lp);		
	}

	static final float PROXIMITY_THRESHOLD = 5.0f;
	public static boolean pactive;
	public static long pactivetime;
	static int haptic;
	static boolean hapticset;
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		boolean keepon = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(Settings.PREF_KEEPON, Settings.DEFAULT_KEEPON);
		if (first) {
			first = false;
			return;
		}
		float distance = event.values[0];
        boolean active = (distance >= 0.0 && distance < PROXIMITY_THRESHOLD && distance < event.sensor.getMaximumRange());
		if (!keepon ||
				Receiver.call_state == UserAgent.UA_STATE_HOLD)
			active = false;
        pactive = active;
        pactivetime = SystemClock.elapsedRealtime();
        if (!active) {
     		mHandler.sendEmptyMessageDelayed(MSG_ACCEPT, 1000);
     		return;
        }
        mHandler.removeMessages(MSG_ACCEPT);
        setScreenBacklight((float) 0.1);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        closeOptionsMenu();
		/*mDialerDrawer.close();
		mDialerDrawer.setVisibility(View.GONE);*/
        ContentResolver cr = getContentResolver();
		if (!hapticset) {
			haptic = android.provider.Settings.System.getInt(cr, android.provider.Settings.System.HAPTIC_FEEDBACK_ENABLED, 1);
			hapticset = true;
		}
		android.provider.Settings.System.putInt(cr, android.provider.Settings.System.HAPTIC_FEEDBACK_ENABLED, 0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCallAnswer:
			answer();
			break;
		case R.id.btnCallHungUp:
			reject();
			break;
		}
	}
	

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: pressDown(v); break;
		case MotionEvent.ACTION_UP: pressUp(v); break;
		}
		return false;
	}
	
	private void pressDown(View v) {
		switch (v.getId()) {
		case R.id.ivHungupIcon: btnHungUp.setPressed(true); break;
		case R.id.ivAnswerIcon: btnAnswer.setPressed(true); break;
		}
	}
    
	private void pressUp(View v) {
		switch (v.getId()) {
		case R.id.ivHungupIcon:
			btnHungUp.setPressed(false);
			reject();
			break;

		case R.id.ivAnswerIcon:
			btnAnswer.setPressed(false);
			answer();
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.cbMicMute:
			//Receiver.engine(InCallScreen.this).togglemute();
			break;
		case R.id.cbSpeaker:
			Receiver.engine(InCallScreen.this).speaker(Mode_Speaker?0:1);
			Mode_Speaker = !Mode_Speaker;
			break;
		}
	}
	
	private void showAvatar(final String mainAvatarUrl) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(!TextUtils.isEmpty(mainAvatarUrl))
					ImageLoader.getInstance().displayImage(HttpUrlPath.basePath + mainAvatarUrl, ivDefaultAvatar, DisplayPictureUtil.getCallAvatarDisplayImageOptions());
				//translateAvatar(); //头像动画
			}
		});
	}
	
	
	
	// 根据account[hanfei]查询对应主头像的url
	public void detailByEntry(String name) {
		RequestParams params = new RequestParams();
		params.add("account", FormatterUtil.getHalfname(name));
		translateAvatar(); //头像动画
		// 改成LoadJson 数据的方式
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ChatOtherBean bean = new ChatOtherBean();
				ChatOtherBean.handJson(json, bean);
				showAvatar(bean.getMainAvatarUrl());
			}
			
			@Override
			public void handFinish() {
			}
			@Override
			public void handFail() {
			}
		}.doGetNotHaveDialog(HttpUrlPath.GetAccountInfoAction, params, InCallScreen.this);
	}


	public void updateCallStatus(final String status) {
		runOnUiThread(new Runnable() {
		@Override
		public void run() {
		if(tvStatus != null) {
			tvStatus.setVisibility(View.VISIBLE);
			tvStatus.setText(status);
		}}});
	}
	
	public void hideCallStatus() {
		runOnUiThread(new Runnable() {
		@Override
		public void run() {
		if(tvStatus != null) {
			tvStatus.setVisibility(View.INVISIBLE);
		}}});
	}
	
	private static Vibrator vib;
    /**
     * 震动
     * @param act
     * @param pattern
     * @param isRepeat
     */
    public static void vibrate(Context act, long[] pattern ,boolean isRepeat) {
	    vib = (Vibrator) act.getSystemService(Service.VIBRATOR_SERVICE);
	    vib.vibrate(pattern, isRepeat ? 1 : -1);
    }
    
    public static void vibrate(int duration) {
    	if(vib == null) return;
    	vib.vibrate(duration);
    }
    // 取消震动
    public static void cancelVibrate() {
    	if(vib != null) vib.cancel();
    }

	public void startChronometer() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
			chTimer.setBase(SystemClock.elapsedRealtime());
			chTimer.setVisibility(View.VISIBLE);
			chTimer.setTextColor(Color.WHITE);
			chTimer.setFormat("%s");
			chTimer.start();
		}});
	}
    
	public void endChronometer() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
			chTimer.setTextColor(Color.GRAY);
			chTimer.stop();
		}});
	}
	
	
	
	// 头像左右晃动
	public void translateAvatar() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				TranslateAnimation translate2Left = new TranslateAnimation(0, -15, 0, 0);
				translate2Left.setDuration(500);
				translate2Left.setRepeatCount(1);
				translate2Left.setFillAfter(true);
				ivDefaultAvatar.startAnimation(translate2Left);
			}
		}, 500);
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				TranslateAnimation translateAnimation = new TranslateAnimation(-15, 15, 0, 0);
				translateAnimation.setDuration(1000);
				translateAnimation.setRepeatCount(Animation.INFINITE);
				translateAnimation.setRepeatMode(Animation.REVERSE);
				ivDefaultAvatar.startAnimation(translateAnimation);
			}
		}, 1000);
	}
	
}
