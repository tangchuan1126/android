package oso.ui.chat.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

/**
 * Bean: ChatOtherBean 聊天对象的头像，用户名，状态，所属用户组信息
 * 
 */
public class ChatOtherBean implements Serializable {
	private static final long serialVersionUID = 470422651618471229L;
	public String postname;
	public String warename; // 仓库名

	public List<ChatOtherImageBean> listBean = new ArrayList<ChatOtherImageBean>();

	public static void handJson(JSONObject json, ChatOtherBean bean) {

		JSONObject jsonObject = json.optJSONObject("datas");
		
		JSONArray jsonArray1 = jsonObject.optJSONArray("departinfo");
		if(!StringUtil.isNullForJSONArray(jsonArray1)){
 				JSONObject itemjson = jsonArray1.optJSONObject(0);
				if(itemjson!=null){
					bean.postname = itemjson.optString("postname");
				}
		}
		JSONArray jsonArray2 = jsonObject.optJSONArray("storageinfo");
		if(!StringUtil.isNullForJSONArray(jsonArray2)){
 				JSONObject itemjson = jsonArray2.optJSONObject(0);
				if(itemjson!=null){
					bean.warename = itemjson.optString("warename");
				}
		}
		JSONArray jsonArray3 = jsonObject.optJSONArray("imginfo");
		if(!StringUtil.isNullForJSONArray(jsonArray3)){
			for (int i = 0; i < jsonArray3.length(); i++) {
				JSONObject itemjson = jsonArray3.optJSONObject(i);
				if(itemjson!=null){
					ChatOtherImageBean imagebean = new ChatOtherImageBean();
					imagebean.id = itemjson.optInt("id");
					imagebean.file_path = itemjson.optString("file_path");
					imagebean.activity = itemjson.optInt("activity");
					bean.listBean.add(imagebean);
				}
			}
		}
	}

	
	public String getMainAvatarUrl() {
		for(ChatOtherImageBean bean : listBean) {
			if(bean.activity == 1) return bean.file_path;
		}
		return "";
	}
}
