package oso.ui.chat.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.util.StringUtils;

import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.adapter.ContactsAdapter;
import oso.ui.chat.bean.ChatGroup;
import oso.ui.chat.bean.ChatGroupMember;
import oso.ui.chat.bean.ChatUserInfo;
import oso.ui.chat.util.ChatUtil;
import oso.ui.chat.util.XmppTool;
import oso.widget.dlgeditview.DlgSearch;
import utility.FormatterUtil;
import utility.Utility;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;

import declare.com.vvme.R;

public class ContactsFragment extends Fragment implements OnClickListener {
	private static final int WHAT_REFRESH_CONTACTS_DATA = 0;
	private static final int WHAT_REINITDATA = 10;
	private static ImageLoader imageLoader;
	private View rootView;
	private View searchView; // 查找框
	private ProgressBar pbEmpty;
	private ExpandableListView contactsListView;

	private ChatUtil chatUtil;
	private ContactsAdapter contactsAdapter;
	private List<ChatGroup> groupsList=new ArrayList<ChatGroup>();
	private List<ChatUserInfo> chatUserList = new ArrayList<ChatUserInfo>(); // 完整的用户名
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			
			switch (msg.what) {
			case WHAT_REFRESH_CONTACTS_DATA:		//刷新-联系人
				groupsList.clear();
				if(msg.obj != null){
					@SuppressWarnings("unchecked")
					ArrayList<ChatGroup> tmpList=(ArrayList<ChatGroup>)msg.obj;
					groupsList.addAll(tmpList);
				}
				contactsAdapter.notifyDataSetChanged();
				break;
			
			case WHAT_REINITDATA:
				initData();
				break;
			default:
				break;
			}
		};
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		chatUtil = new ChatUtil(getActivity(), XmppTool.getConnection());
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 if(rootView == null) {  
            rootView = inflater.inflate(R.layout.chat_fragment_contacts, null);  
            initView(rootView);
    		initData();
    		initListener(ContactsFragment.this.getActivity());
        }  
		//缓存的rootView需要判断是否已经被加过parent， 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。  
        ViewGroup parent = (ViewGroup) rootView.getParent();  
        if (parent != null) {  
            parent.removeView(rootView);  
        }   
        return rootView;  
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loSearch_hd:		//搜索
			showDlg_search();
			break;

		default:
			break;
		}
	}
	
	/**
	 * 搜索
	 */
	private void showDlg_search() {
		new DlgSearch(getActivity()) {
			protected void onSearch(String keyword) {

				List<ChatUserInfo> list = searchContacts(keyword);
				if (list != null) {
					this.setItems(list, true);
				}

			};

			protected void onItemSelected(ChatUserInfo bean) {
				Intent intent = new Intent(getActivity(), ChatActivity.class);

				for (int i = 0; i < chatUserList.size(); i++) {
					if (chatUserList.get(i).getAlias().equals(bean.getAlias())) {
						String username = StringUtils.parseName(chatUserList.get(i).getUsername());
						ChatActivity.initParams(intent, username, chatUserList.get(i).getAlias());
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
						break;
					}
				}
			};

			// 获得联系人
			private List<ChatUserInfo> searchContacts(String keyword) {
				List<ChatUserInfo> userList = new ArrayList<ChatUserInfo>();
				List<String> usernameList = new ArrayList<String>();
				chatUserList.clear();

				for (int i = 0; i < groupsList.size(); i++) {
					List<ChatGroupMember> memberList = groupsList.get(i).getMemberList();

					for (int index = 0; index < memberList.size(); index++) {
						String username = memberList.get(index).getUsername();
						String alias = memberList.get(index).getAlias();
						String avatarUrl = memberList.get(index).getAvatarURL();
						
						// 转小写 便于   忽略大小写查询
						Locale defloc = Locale.getDefault();
						String aliasBlur = alias.toLowerCase(defloc); 
						String keywordBlur = keyword.toLowerCase(defloc);
						if (aliasBlur.contains(keywordBlur)) {
							usernameList.add(alias);
							ChatUserInfo chatUserInfo = new ChatUserInfo();
							chatUserInfo.setAlias(memberList.get(index).getAlias());
							chatUserInfo.setUsername(memberList.get(index).getUsername());
							chatUserInfo.setAvatarURL(memberList.get(index).getAvatarURL());
							chatUserList.add(chatUserInfo);
	//-------------------------------------------------------------------------------------------------						
							userList.add(new ChatUserInfo(null, avatarUrl, FormatterUtil.getHalfname(username), alias, "WareName", ""));  
						}
					}
				}
				return userList;
			}

		}.show();
	}

	/**
	 * 加载数据
	 */
	public void initData() {
		
		new Thread(){
			
			public void run() {
				// 刷新ListView 数据
				if(chatUtil == null) chatUtil = ChatUtil.getThis(ContactsFragment.this.getActivity());
				List<ChatGroup> tempListGroup = chatUtil.getGroupsList();
				//无数据,延迟拉取
				if(tempListGroup==null||tempListGroup.size()==0)
					handler.sendEmptyMessageDelayed(WHAT_REINITDATA, 1000);
				else{
					Message msg = Message.obtain(handler, WHAT_REFRESH_CONTACTS_DATA, tempListGroup);
					handler.sendMessage(msg);
				}
			};
			
		}.start();
	}

	/**
	 * 初始化ExpandableListView的点击事件
	 */
	private void initListener(final Context context) {
		// 查找框点击后 startActivityForResult(); 搜索界面Activity

		searchView.setOnClickListener(this);

		// 点击联系人 跳转到聊天界面 传递联系人的真实账号 nickname@domain
		contactsListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				String username = groupsList.get(groupPosition).getMemberList().get(childPosition).getUsername();
				String alias = groupsList.get(groupPosition).getMemberList().get(childPosition).getAlias();
				username = StringUtils.parseName(username);  // 去后缀
				Intent intent = new Intent(getActivity(), ChatActivity.class);
				ChatActivity.initParams(intent, username, alias);
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
				return false;
			}

		});
		
		// 滑动不加载图片
		contactsListView.setOnScrollListener(new PauseOnScrollListener(imageLoader, true, false));
	}

	/**
	 * 初始化控件
	 * 
	 * @param container
	 */
	@SuppressLint("NewApi")
	private void initView(View view) {
		contactsListView = (ExpandableListView) view.findViewById(R.id.contact_listview);
		pbEmpty = (ProgressBar) view.findViewById(R.id.pbEmpty);

		// 设置箭头位置
		int indicatorR = Utility.dip2px(getActivity(), 30f);
		// 4.3时api改了
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2)
			contactsListView.setIndicatorBounds(0, indicatorR);
		else
			contactsListView.setIndicatorBoundsRelative(0, indicatorR);

		searchView = View.inflate(getActivity(), R.layout.chat_search_header_layout, null);

		contactsListView.addHeaderView(searchView);
		contactsListView.setEmptyView(pbEmpty);
		imageLoader = ImageLoader.getInstance();
		contactsAdapter = new ContactsAdapter(getActivity(), groupsList, imageLoader);
		contactsListView.setAdapter(contactsAdapter);
	}

	/**
	 * 用户状态发生改变
	 * @param changedUser
	 * @param isAvailable
	 */
	public void updateData(String changedUser, boolean isAvailable) {
		//debug
		if(contactsAdapter==null)
			return;
			
		List<ChatGroup> groups = contactsAdapter.groupsList;
		
		//--debug
		if(groups == null) {
			return;
		}
		
		String newStatus = null;
		if(isAvailable)
			newStatus = ChatUtil.STATUS_ONLINE;

		else
			newStatus = ChatUtil.STATUS_OFFLINE;
		
		// 修改后的在线人数
		
		if(TextUtils.isEmpty(changedUser)) {
			return;
		}
		
		// 查找修改
		for(int i=0; i<groups.size(); i++) {
			List<ChatGroupMember> memberList = groups.get(i).getMemberList();
			if(memberList.size() == 0 || memberList == null) {
				return;
			}
			
			for(int memberPosition=0; memberPosition<memberList.size(); memberPosition ++) {
				
				if(changedUser.equals(memberList.get(memberPosition).getUsername())) {
					memberList.get(memberPosition).setStatus(newStatus);
					groups.get(i).updateOnlineNum();
					
					// 排序
					groups.get(i).sortOnline();
					break;
				}
			}
		}
		
		
		contactsAdapter.notifyDataSetChanged();
	}
	

	
	
}