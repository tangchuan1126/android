package oso.ui.chat.bean;

import oso.ui.chat.util.ChatUtil;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;

/**
 * Bean: 分组中成员
 * @author xialimin
 *
 */
public class ChatGroupMember extends ChatUserInfo {
	private static final long serialVersionUID = -4266091603059210651L;
	private String groupname;  // 所属用户组

	public ChatGroupMember(Drawable avatar, String avatarUrl, String username, String alias, String status,
			String warename, String groupname) {
		super(avatar, avatarUrl, username, alias, warename, status);
		this.groupname = groupname;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	/**
	 * 根据Online / Offline 设置不同的头像
	 */
	public void changeIconStatus() {
		if (ChatUtil.STATUS_ONLINE.equals(this.getStatus())) {
			this.avatar.setColorFilter(null);
			return;
		} else {
			this.avatar.mutate();
			ColorMatrix cm = new ColorMatrix();
			cm.setSaturation(0);
			ColorMatrixColorFilter cf = new ColorMatrixColorFilter(cm);
			this.avatar.setColorFilter(cf);
		}
	}
	
	@Override
	public Drawable getAvatar() {
		return this.avatar;
	}
}
