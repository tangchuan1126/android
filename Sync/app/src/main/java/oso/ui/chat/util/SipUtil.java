package oso.ui.chat.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SipUtil {
	public static void initSipSetting(Context context, String uname, String pwd, String serverDomain) {
		SharedPreferences settings = context.getSharedPreferences("sipcall_pref", Context.MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString("username", uname);
		editor.putString("password", pwd);
		editor.putString("port", "5060");
		editor.putString("port1", "5060");
		editor.putString("pref", "SIP");
		editor.putString("pref1", "SIP");
		editor.putString("protocol", "udp");
		editor.putString("protocol1", "udp");
		editor.putString("server", serverDomain);
		editor.putString("server1", serverDomain);
		editor.commit();
		
		SharedPreferences sipdroidSettings = context.getSharedPreferences(context.getPackageName()+"_preferences", Context.MODE_PRIVATE);
		Editor e = sipdroidSettings.edit();
		e.putString("port", "5060");
		e.putString("speex_new", "never");
		e.putBoolean("wlan1", true);
		e.putString("password", uname);
		e.putString("username", pwd);
		e.putString("G722 HD Voice_new", "never");
		e.putString("vquality", "low");
		e.putString("eargain", "0.85");
		e.putString("heargain", "0.85");
		e.putString("micgain", "0.85");
		e.putString("protocol", "udp");
		e.putBoolean("3g", true);
		e.putBoolean("wlan", true);
		e.putBoolean("on", true);
		e.putBoolean("keepon", true);
		e.putBoolean("MWI_enabled", true);
		e.putString("server", serverDomain);
		e.putString("dns0", serverDomain);
		e.putString("dns1", "::1");
		e.putString("PCMU_new", "wlanor3g");
		e.commit();
	}
}
