package oso.ui.chat.bean;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * Bean: 分组   包含分组名, 分组内的用户成员 
 * @author xialimin
 *
 */
public class ChatGroup implements Comparator<ChatGroupMember> {
	private String groupname; // 组名
	private int onlineNum;    // 在线人数
	private int totalNum;  	  // 群组总人数
	private List<ChatGroupMember> memberList;
	
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public List<ChatGroupMember> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<ChatGroupMember> memberList) {
		this.memberList = memberList;
		sortOnline();
		this.setTotalNum(memberList.size());
	}
	public void setOnlineNum(int onlineNum) {
		this.onlineNum = onlineNum;
	}
	public int getOnlineNum() {
		return onlineNum; 
	}
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	
	/**
	 * 按Online/Offline排序
	 */
	public void sortOnline() {
		Collections.sort(this.memberList, this);
		for(int i=0; i<memberList.size()-1; i ++) {
			for(int j=0; j<memberList.size()-i-1; j++) {
				
				String status = memberList.get(j).getStatus();
				if("Offline".equals(status)) {
					swap(j, j+1);
				}
			}
		}
		
		// 将在线的账号 顺序
		int i=0;
		for(i=0; i<memberList.size(); i++) {
			if("Offline".equals(memberList.get(i).getStatus())) {
				break;
			}
		}
		
		List<ChatGroupMember> list = new ArrayList<ChatGroupMember>();
		if(i > 0 && i < list.size())
			list.addAll(memberList.subList(0, i));
		
		if(list.size() > 0) {
			this.memberList.removeAll(list);
			Collections.reverse(list);
			this.memberList.addAll(0, list);
		}
	}
	
	private void swap(int i, int j) {
		ChatGroupMember temp = memberList.get(i);
		memberList.set(i, memberList.get(j));
		memberList.set(j, temp);
		temp = null;
	}
	
	public void updateOnlineNum() {
		int count = 0;
		for(int i=0; i<this.memberList.size(); i++) {
			if("Online".equals(memberList.get(i).getStatus())) {
				count ++;
			}
		}
		this.setOnlineNum(count);
	}
	
	
	@Override
	public int compare(ChatGroupMember member1, ChatGroupMember member2) {
		//昵称-倒序
		Collator cmp = Collator.getInstance(java.util.Locale.CHINA);
		return cmp.compare(member2.getAlias(), member1.getAlias());
	}
	
}
