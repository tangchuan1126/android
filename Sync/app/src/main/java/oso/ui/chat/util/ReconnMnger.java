package oso.ui.chat.util;

import org.jivesoftware.smack.XMPPConnection;

import oso.SyncApplication;
import support.dbhelper.StoredData;
import utility.Utility;
import android.os.SystemClock;

/**
 * xmpp-重连管理器
 * 
 * @author 朱成
 * @date 2015-2-5
 */
public class ReconnMnger {

	private static ReconnMnger instance;

	public static ReconnMnger getThis() {
		if (instance == null)
			instance = new ReconnMnger();

		return instance;
	}
	
	private boolean isReconning=false;	//正在重连
	
	public boolean isReconning(){
		return isReconning;
	}
	
	private Thread thread;

	/**
	 * 启动-重连
	 */
	public void startReconnect() {
		
		if(!shouldReconn())
			return;

		new Thread() {
			public void run() {
				isReconning=true;

				while (shouldReconn()) {
					// 尝试-登录
					XmppTool.getThis().login_of();
					//延迟-7s
					SystemClock.sleep(7000);
				}
				
				isReconning=false;
			};
		}.start();

	}
	
	private boolean isRequest_Stop=false;
	
	/**
	 * 停止-重连
	 */
	public void stopReconn(){
		if(isReconning)
			isRequest_Stop=true;
	}
	

	/**
	 * 是否应连接,未登录oso、已连接of、未联网-则不连
	 * 
	 * @return
	 */
	public static boolean shouldReconn() {
		// 若未登录
		if (!StoredData.isLogin())
			return false;

		// 已登陆
		XMPPConnection conn = XmppTool.getConnection();
		if (conn != null && conn.isAuthenticated())
			return false;

		// 若未联网,联网时 会触发连接
		if (!Utility.isConnectNet(SyncApplication.getThis()))
			return false;
		
		return true;
	}

}
