package oso.ui.chat.bean;

import java.util.List;

/**
 * 
 * @author xialimin
 * 
 */
public class ChatHistoryRecord {
	public String ret;

	public PC pc;
	public List<Records> records;

	public class PC {
		public int allCount;
		public int pageCount;
		public int pageNo;
		public int pageSize;
		public int rowCount;
	}

	public class Records {
		public String content;
		public String create_DATE;
		public String id;
		public int is_DELETE;
		public int msg_TYPE;
		public String receiver_ID;
		public int receiver_ID_FLAG;
		public String sender_ID;
		public int state;
		public String xml_CONTENT;

		public boolean isContain(String msgId) {
			return msgId.equals(this.id);
		}
	}
	
}
