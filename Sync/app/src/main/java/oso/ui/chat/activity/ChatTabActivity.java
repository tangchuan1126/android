package oso.ui.chat.activity;

import java.util.ArrayList;
import java.util.Collection;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

import oso.base.ActivityStack;
import oso.base.IBaseActivity;
import oso.ui.chat.adapter.ChatTabVpAdapter;
import oso.ui.chat.fragment.ContactsFragment;
import oso.ui.chat.fragment.MessageFragment;
import oso.ui.chat.util.XLog;
import oso.ui.chat.util.XmppTool;
import support.dbhelper.DBManager;
import support.dbhelper.StoredData;
import utility.FormatterUtil;
import utility.Utility;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * @author xialimin
 */
public class ChatTabActivity extends FragmentActivity implements IBaseActivity,
		OnClickListener {
	private static final String TAG = ChatTabActivity.class.getSimpleName();
	private static final int NOTHING_UNREAD_MSG = 0;
	private static final int WHAT_INIT_ROSTER_LISTENER = 10;
	
	private RadioGroup tabGrp;
	private TextView tvTotalCnt; // 未读消息条目数量
	private ViewPager tabViewPager;
	
	private ChatTabVpAdapter tabVpAdapter;
	private ChatRosterListener chatRosterListener;
	private NewMsgBroadcastReceiver chatMsgReceiver;
	private ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();
	
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case WHAT_INIT_ROSTER_LISTENER:
				initRosterListener();
				break;
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addToStack();
		setContentView(R.layout.chat_main_tab_layout);
		
		initView();
		initAdapter();
		registReceiver();
		handler.sendEmptyMessageDelayed(WHAT_INIT_ROSTER_LISTENER, 200); // 进入后200毫秒注册Roster监听器
		
		
	}
	
	@Override
	public void addToStack() {
		ActivityStack.addAct(this);
	}
	
	@Override
	public void removeFromStack() {
		ActivityStack.removeAct(this);
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		if (chatMsgReceiver == null) {
			registReceiver();
		}

		initUnreadMsgCount();
		//取消notify
		Utility.cancelNotify(Utility.NotifyID_Chat);
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeFromStack();
		// 反注册广播
		if (chatMsgReceiver != null) {
			unregisterReceiver(chatMsgReceiver);
			chatMsgReceiver = null;
			XLog.d(TAG, "BroadcastReceiver unregister successful!!!");
		}

		chatRosterListener = null;
	}

	/**
	 * 注册接收广播
	 */
	private void registReceiver() {
		IntentFilter intentFilter = new IntentFilter("com.vvme.chat.NEW_MESSAGE_ACTION");
		if (chatMsgReceiver == null) {
			chatMsgReceiver = new NewMsgBroadcastReceiver();
			intentFilter.setPriority(500); // ChatActivity 聊天界面优先级最高
			registerReceiver(chatMsgReceiver, intentFilter);
		}

	}


	/**
	 * 初始化控件
	 */
	private void initView() {
		// 取view
		tabViewPager = (ViewPager) findViewById(R.id.tabViewPager);
		tabGrp = (RadioGroup) findViewById(R.id.tabGrp);
		tabGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				boolean ismsg = checkedId == R.id.radMsg;
				tabViewPager.setCurrentItem(ismsg ? 0 : 1);
			}
		});

		TextView topbar_title = (TextView) findViewById(R.id.topbar_title);

		tvTotalCnt = (TextView) findViewById(R.id.tvTotalCnt);

		// 初始化
		topbar_title.setText(getString(R.string.chat_title));

		// 监听事件
		View btnBack=findViewById(R.id.topbar_back);
		btnBack.setOnClickListener(this);
		Utility.addLs_backHome_onLongclick(this, btnBack);
		

		// 初始化未读消息条目数量
		initUnreadMsgCount();
		
		
		tabViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				switch (position) {
				case 0:
					((RadioButton)tabGrp.findViewById(R.id.radMsg)).setChecked(true);
					break;
				case 1:
					((RadioButton)tabGrp.findViewById(R.id.radContacts)).setChecked(true);
					break;
				default:
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	

	public void initAdapter() {
		fragmentList.add(new MessageFragment(this));
		fragmentList.add(new ContactsFragment());
		tabVpAdapter = new ChatTabVpAdapter(getSupportFragmentManager(), fragmentList);
		tabViewPager.setAdapter(tabVpAdapter);
	}
	
	/**
	 * 初始化未读消息数量
	 */
	public void initUnreadMsgCount() {
		new Thread() {
			@Override
			public void run() {
				String unameMe = StoredData.getName();
				final int recentMsgCount = DBManager.getThis().queryRecentMsgCount(unameMe);
				showRecentMsgCount(recentMsgCount);
			}

			private void showRecentMsgCount(final int recentMsgCount) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						tvTotalCnt.setVisibility(NOTHING_UNREAD_MSG == recentMsgCount ? View.GONE : View.VISIBLE);
						tvTotalCnt.setText("" + recentMsgCount);
					}

				});				
			};
		}.start();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.topbar_back: // 返回
			finish();
			break;

		default:
			break;
		}
	}

	/**
	 * 注册RosterListener
	 */
	private void initRosterListener() {
		if (chatRosterListener == null) {
			chatRosterListener = new ChatRosterListener();
		}
		
		// 添加RosterListener
		new Thread() {
			@Override
			public void run() {
				final Roster rs = XmppTool.getConnection().getRoster();
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						rs.addRosterListener(chatRosterListener);
					}
				});
			};
		}.start();
	}
	
	
	// 用户更新时(添加后会更新) 重新刷新联系人数据
	private void updateRoster() {
		ContactsFragment contactsFragment = (ContactsFragment) tabVpAdapter.getItem(1);
		if (contactsFragment != null) {
			contactsFragment.initData();
		}
	}
	
	// 用户状态发生改变时
	private void updateRosterGroups(String changedUser, boolean isAvailable) {
		ContactsFragment contactsFragment = (ContactsFragment) tabVpAdapter.getItem(1);
		if (contactsFragment != null) {
			contactsFragment.updateData(changedUser, isAvailable);
		}
		
		//如果当前正在ChatActivity中则修改在线标识
		Activity topAc=ActivityStack.getTopActInstance();
		if(topAc instanceof ChatActivity) {
			((ChatActivity) topAc).setStatus(changedUser, isAvailable);
		}
	}


	private class ChatRosterListener implements RosterListener {

		@Override
		public void presenceChanged(Presence presence) {
			final String changedUsername = FormatterUtil.getWholeName(presence.getFrom());
			final boolean isAvailable = presence.isAvailable();
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					updateRosterGroups(changedUsername, isAvailable);
				}
			});
		}

		@Override
		public void entriesUpdated(Collection<String> addresses) {
			updateRoster(); 
		}

		@Override
		public void entriesDeleted(Collection<String> addresses) {
			updateRoster();
		}

		@Override
		public void entriesAdded(Collection<String> addresses) {  // ttt_1@192.168.1.15
			updateRoster();
		}
		
	}

	
	/**
	 * 接收新消息广播
	 * 
	 * @author xialimin
	 * 
	 */
	private class NewMsgBroadcastReceiver extends BroadcastReceiver {
		
		public void onReceive(Context context, Intent intent) {
			new Thread() {
				@Override
				public void run() {
					// Tab显示未读消息数量
					String unameMe = StoredData.getName();
					DBManager db = DBManager.getThis();
					final int recentMsgCount = db.queryRecentMsgCount(unameMe);
					refreshTabMsgCount(recentMsgCount);
				}

				private void refreshTabMsgCount(final int recentMsgCount) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							tvTotalCnt.setText("" + recentMsgCount);
							tvTotalCnt.setVisibility(0 == recentMsgCount ? View.GONE : View.VISIBLE);
							MessageFragment messageFragment = (MessageFragment) tabVpAdapter.getItem(0);
							if (messageFragment != null) {
								messageFragment.initData();
							}
						}
					});
				};
			}.start();

		}

	}
}
