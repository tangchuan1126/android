package oso.ui.chat.util;

import android.util.Log;

/**
 * Log工具类，编译正式版本时，将XLog.DEBUG 置为false
 * @author xialimin
 *
 */
public class XLog {
	private static final boolean DEBUG = false;
	public static void i(String tag, String msg) {
		if(XLog.DEBUG) {
			Log.i(tag, msg);
		}
	}
	
	public static void d(String tag, String msg) {
		if(XLog.DEBUG) {
			Log.d(tag, msg);
		}
	}
	
	public static void e(String tag, String msg) {
		if(XLog.DEBUG) {
			Log.e(tag, msg);
		}
	}
	
	public static void v(String tag, String msg) {
		if(XLog.DEBUG) {
			Log.v(tag, msg);
		}
	}
	
	public static void w(String tag, String msg) {
		if(XLog.DEBUG) {
			Log.w(tag, msg);
		}
	}
	
	/**
	 * System.out.println
	 * @param msg
	 */
	public static void out(Object msg) {
		if(XLog.DEBUG) {
			System.out.println(msg);
		}
	}
	
}
