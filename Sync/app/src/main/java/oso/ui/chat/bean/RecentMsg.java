package oso.ui.chat.bean;

import java.util.List;

import android.graphics.drawable.Drawable;
/**
 * Bean: 最近消息条目
 * @author xialimin
 *
 */
public class RecentMsg {
	private Drawable icon;
	private String unamePeer;
	private String aliasPeer;
	private String messageBody;
	private String messageTime;
	private int unreadMsgCount;
	private List<String> messageIdList;
	
	public String getReadableStr() {
		return ChatMessage.getReadableStr(messageBody);
	}
	
	//==================inner===============================

	
	public RecentMsg(Drawable icon, String unamePeer, String aliasPeer, String messageBody,
			String messageTime, int unreadMsgCount) {
		super();
		this.icon = icon;
		this.unamePeer = unamePeer;
		this.setAliasPeer(aliasPeer);
		this.messageBody = messageBody;
		this.messageTime = messageTime;
		this.unreadMsgCount = unreadMsgCount;
	}
	
	public Drawable getIcon() {
		return icon;
	}
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}
	public String getUnamePeer() {
		return unamePeer;
	}
	public void setUnamePeer(String unamePeer) {
		this.unamePeer = unamePeer;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	public String getMessageTime() {
		return messageTime;
	}
	public void setMessageTime(String messageTime) {
		this.messageTime = messageTime;
	}
	public int getUnreadMsgCount() {
		return unreadMsgCount;
	}
	public void setUnreadMsgCount(int unreadMsgCount) {
		this.unreadMsgCount = unreadMsgCount;
	}

	public List<String> getMessageIdList() {
		return messageIdList;
	}

	public void setMessageIdList(List<String> messageIdList) {
		this.messageIdList = messageIdList;
	}

	public String getAliasPeer() {
		return aliasPeer;
	}

	public void setAliasPeer(String aliasPeer) {
		this.aliasPeer = aliasPeer;
	}
	
}
