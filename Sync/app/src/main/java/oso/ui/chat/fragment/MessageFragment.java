package oso.ui.chat.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.chat.bean.RecentMsg;
import oso.widget.dialog.RewriteBuilderDialog;
import support.dbhelper.DBManager;
import support.dbhelper.StoredData;
import utility.DisplayPictureUtil;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

/**
 * 最近消息
 * 
 * @author 朱成
 * @date 2015-1-15
 */
@SuppressLint("ValidFragment")
public class MessageFragment extends Fragment {
	private static final int RECENT_MSG_DATA_REFRESH = 10;
	private static final int RECENT_MSG_EMPTY = 20;
	private Map<String, Set<String>> extAccMap = new HashMap<String, Set<String>>();
	
	private Activity ac;
	private Context context;
	private ListView lv;
	private List<RecentMsg> dataList;

	private RecentMsgAdapter adapter;
	private TextView tvEmpty;
	private View rootView;
	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			if (RECENT_MSG_DATA_REFRESH == msg.what) {
				dataList = (List<RecentMsg>) msg.obj;
				if (dataList != null) {
					if(adapter == null) {
						adapter = new RecentMsgAdapter();
					} 
				}
			} else if(RECENT_MSG_EMPTY == msg.what) {
				// 没有未读消息了，清空集合
				dataList = new ArrayList<RecentMsg>();
				dataList.clear();
			}
			
			if(rootView == null) 
				 rootView = View.inflate(context, R.layout.frag_chat_message, null);
			if(adapter == null)
				adapter = new RecentMsgAdapter();
			((ListView)rootView.findViewById(R.id.lv_chat_recent)).setAdapter(adapter);
			
		};
	};
	
	
	public MessageFragment(Context context) {
		super();
		this.context = context;
	}

	
	public MessageFragment() {
		super();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ac = getActivity();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		initData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		 if(rootView == null){  
            rootView = inflater.inflate(R.layout.frag_chat_message, null); 
            lv = (ListView) rootView.findViewById(R.id.lv_chat_recent);
    		tvEmpty = (TextView) rootView.findViewById(R.id.tvEmpty);
    		lv.setEmptyView(tvEmpty);

    		initData();
    		
    		initListener();
	     }  
	 	//缓存的rootView需要判断是否已经被加过parent， 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。  
        ViewGroup parent = (ViewGroup) rootView.getParent();  
        if (parent != null) {  
            parent.removeView(rootView);  
        }   
        return rootView;  
		
	}


	private void initListener() {
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String uname_peer = dataList.get(position).getUnamePeer();
				String alias = dataList.get(position).getAliasPeer();
				Intent intent = new Intent(MessageFragment.this.getActivity(), ChatActivity.class);
				ChatActivity.initParams(intent, uname_peer, alias);
				startActivity(intent);
				MessageFragment.this.getActivity().overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}
		});
		
		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				showDlg_onLongPress(position);
				return true;
			}
		});
	}
	
	/**
	 * 长按 弹出框 选择是否删除该position对话
	 * @param position
	 */
	private void showDlg_onLongPress(final int deletePosition){
		RewriteBuilderDialog.Builder bd=new RewriteBuilderDialog.Builder(getActivity());
		bd.setItems(new String[]{getString(R.string.tms_delete)}, new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				deleteChatConversation(deletePosition);
			}

			private void deleteChatConversation(final int position) {
				final String unamePeer = dataList.get(position).getUnamePeer();
				// 删除和指定人聊天的集合   和 数据库中的聊天记录    
				
				new Thread(){
					@Override
					public void run() {
						if(DBManager.getThis().deleteAll(unamePeer) > 0) 
							refreshList();
					}

				private void refreshList() {
					MessageFragment.this.getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if(unamePeer.equals(dataList.get(position).getUnamePeer()) ) {
								dataList.remove(position);
								adapter.notifyDataSetChanged();
								// 刷新Tab的未读个数
								if(getActivity() instanceof ChatTabActivity) 
									((ChatTabActivity) getActivity()).initUnreadMsgCount();
							}
						}
					});
				};
				}.start();				
			}
			
		});
		bd.show();
		
	}

	/**
	 * 初始化数据
	 */
	public void initData() {

		new Thread() {
			@Override
			public void run() {
				DBManager db = DBManager.getThis();
				String unameMe = StoredData.getName();
				List<RecentMsg> queryRecentMsg = db.queryRecentMsgItem(unameMe);
				if (queryRecentMsg != null) {
					Message msg = Message.obtain();
					msg.obj = queryRecentMsg;
					msg.what = RECENT_MSG_DATA_REFRESH;
					handler.sendMessage(msg);
				} else {
					handler.sendEmptyMessage(RECENT_MSG_EMPTY);
				}
			};
		}.start();
	}

	// ==================nested===============================

	private class RecentMsgAdapter extends BaseAdapter {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = ac.getLayoutInflater().inflate(R.layout.frag_chat_lastmsgs_item, null);
				holder = new Holder();
				holder.tvAlias = (TextView) convertView.findViewById(R.id.chat_recent_name);
				holder.tvTime = (TextView) convertView.findViewById(R.id.chat_recent_time);
				holder.tvLastMsg = (TextView) convertView.findViewById(R.id.chat_recent_msg);
				holder.tvCount = (TextView) convertView.findViewById(R.id.chat_recent_count);
				holder.ivAvater = (ImageView) convertView.findViewById(R.id.ivAvatar);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.tvAlias.setText(dataList.get(position).getAliasPeer());
			holder.tvTime.setText(FormatterUtil.getFormatTime(dataList.get(position).getMessageTime()));
			holder.tvLastMsg.setText(dataList.get(position).getReadableStr());
			String unamePeer = dataList.get(position).getUnamePeer();  // hanfei
			loadAvatarImage(unamePeer, holder);
			// 未读消息数量
			int unreadMsgCount = dataList.get(position).getUnreadMsgCount();
			if(0 == unreadMsgCount) {
				holder.tvCount.setVisibility(View.INVISIBLE);
			} else {
				holder.tvCount.setText("" + dataList.get(position).getUnreadMsgCount());
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public int getCount() {
			return dataList == null ? 0:dataList.size();
		}
		
		@SuppressWarnings("unchecked")
		private void loadAvatarImage(String unamePeer, Holder holder) {
			// 查询头像
			if(extAccMap.size()==0) {
				extAccMap.putAll(((Map<String, Set<String>>) StoredData.getExtAccMap()));
			} 
			Set<String> set = extAccMap.get(unamePeer);
			if(set == null) return;
			Iterator<String> it = set.iterator();
			
			while(it.hasNext()) {
				String result = it.next();
				if(!TextUtils.isEmpty(result) && result.startsWith("upload/account/")) {
					result = HttpUrlPath.basePath + result;
					ImageLoader.getInstance().displayImage(result, holder.ivAvater, DisplayPictureUtil.getChatAvatarDisplayImageOptions_Online());
					return;
				}
			}
			
		}
	};

	class Holder {
		ImageView ivAvater;
		TextView tvAlias;
		TextView tvLastMsg;
		TextView tvTime;
		TextView tvCount;
	}

}
