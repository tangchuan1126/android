package oso.ui.chat.activity;

import oso.ui.chat.util.ChatUtil;

import com.google.gson.Gson;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipProfile;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class CallActivity extends Activity implements OnClickListener, OnTouchListener {
	public static int STATUS_CALL = 0;     // 拨出
	public static int STATUS_INCOMING = 1; // 接入
	private int typeCall; // 电话类型   拨出/接入
	
	private CheckBox cbMic;
	private CheckBox cbSpeaker;
	
	private Button btnAnswer;
	private Button btnHungUp;
	
	/*private ImageView ivHungupIcon;
	private ImageView ivAnswerIcon;*/
	
	private TextView tvStatus;
	private TextView tvPeerName;
	
	private SipProfile localProfile;
	private SipProfile peerProfile;
	
	private SipAudioCall incomingCall;
	private SipAudioCall makeAudioCall;
	
	private FrameLayout flAnswerLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_chat_call);
		applyParams();
		initView();
		initListener();
		
		setMaxVolume(this);
		Intent intent = getIntent().getParcelableExtra("intent");
		handleCall();
		handleIncoming(intent);
		
	}
	
	private void initView() {
		tvStatus = (TextView) findViewById(R.id.tvCallStatus);
		tvPeerName = (TextView) findViewById(R.id.tvPeerName);
		
		btnAnswer = (Button) findViewById(R.id.btnCallAnswer);
		btnHungUp = (Button) findViewById(R.id.btnCallHungUp);
		
		cbMic = (CheckBox) findViewById(R.id.cbMicMute);
		cbSpeaker = (CheckBox) findViewById(R.id.cbSpeaker);
		
		/*ivHungupIcon = (ImageView) findViewById(R.id.ivHungupIcon);
		ivAnswerIcon = (ImageView) findViewById(R.id.ivAnswerIcon);*/
	
		flAnswerLayout = (FrameLayout) findViewById(R.id.flAnswerLayout);
	
		btnAnswer.setOnClickListener(this);
		btnHungUp.setOnClickListener(this);
		/*ivHungupIcon.setOnClickListener(this);
		ivHungupIcon.setOnTouchListener(this);
		ivAnswerIcon.setOnClickListener(this);
		ivAnswerIcon.setOnTouchListener(this);*/
		
		setPeerName();
	}
	
	private void initListener() {
		// 处理Mic开关 静音/非静音
		cbMic.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			}
		});
		// 处理外放开关  扬声器/听筒播放
		cbSpeaker.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(typeCall == CallActivity.STATUS_CALL) {
					makeAudioCall.setSpeakerMode(isChecked);
				} else {
					incomingCall.setSpeakerMode(isChecked);
				}
			}
		});
	}


	/**
	 * 初始化参数
	 * @param intent
	 * @param localProfile
	 * @param peerProfile
	 * @param status        STATUS_CALL    STATUS_INCOMING
	 */
	public static void initParams(Intent intent, SipProfile localProfile, SipProfile peerProfile, int status) {
		intent.putExtra("localProfile", (Parcelable) localProfile);
		intent.putExtra("peerProfile", (Parcelable) peerProfile);
		intent.putExtra("status", status == STATUS_CALL ? STATUS_CALL : STATUS_INCOMING);
	}
	public void applyParams() {
		localProfile = getIntent().getParcelableExtra("localProfile");
		peerProfile = getIntent().getParcelableExtra("peerProfile");
		typeCall = getIntent().getIntExtra("status", -1);
	}
	
	private void handleCall() {
		if(typeCall != CallActivity.STATUS_CALL) return;
		btnHungUp.setVisibility(View.VISIBLE);
		//ivHungupIcon.setVisibility(View.VISIBLE);
		flAnswerLayout.setVisibility(View.GONE);
		try {
			makeAudioCall = ChatUtil.getSipManagerInstance().makeAudioCall(localProfile, peerProfile, callListener, 30);
		} catch (SipException e) {
			e.printStackTrace();
		}
	}
	
	private void handleIncoming(Intent incomingIntent) {
		// 只处理接入
		if(typeCall != CallActivity.STATUS_INCOMING) return;
		// 只显示接听按钮 和挂断按钮
		btnAnswer.setVisibility(View.VISIBLE);
		btnHungUp.setVisibility(View.VISIBLE);
		/*ivAnswerIcon.setVisibility(View.VISIBLE);
		ivHungupIcon.setVisibility(View.VISIBLE);*/
		try {
            SipAudioCall.Listener listener = new SipAudioCall.Listener() {

				@Override
				public void onReadyToCall(SipAudioCall call) {
					super.onReadyToCall(call);
				}

				@Override
				public void onCalling(SipAudioCall call) {
					super.onCalling(call);
				}

				@Override
				public void onRinging(SipAudioCall call, SipProfile caller) {
					setPeerName(call.getPeerProfile().getUserName());
					// 震动 
					vibrate(CallActivity.this, new long[]{800,500,800,500}, true);
				}

				@Override  // 通话建立
				public void onCallEstablished(SipAudioCall call) {
					super.onCallEstablished(call);
					tvStatus.setText(call.getPeerProfile().getUserName());
					// 取消震动
					cancelVibrate();
				}

				@Override  // 通话结束  延时1秒挂断电话
				public void onCallEnded(final SipAudioCall call) {
					super.onCallEnded(call);
					closeActDelayed(1000, "通话结束");
				}

				@Override
				public void onCallBusy(SipAudioCall call) {
					super.onCallBusy(call);
					closeActDelayed(1000, "繁忙挂断");
				}

				@Override
				public void onError(SipAudioCall call, int errorCode,
						String errorMessage) {
					super.onError(call, errorCode, errorMessage);
					closeActDelayed(1000, "异常关闭");
				}
				
				
            };
            
            incomingCall = ChatUtil.getSipManagerInstance().takeAudioCall(incomingIntent, listener);
            incomingCall.startAudio();
            incomingCall.setSpeakerMode(false);   // 听筒模式: false            扬声器模式: true 
            if(incomingCall.isMuted()) {
                incomingCall.toggleMute();
            }
        } catch (Exception e) {
            if (incomingCall != null) {
                incomingCall.close();
            }
        }
	}

	private void setPeerName() {
		if(peerProfile == null) return;
		tvPeerName.setText(ChatUtil.findAliasByUsername(peerProfile.getUserName()));
	}

	private void setPeerName(final String peername) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(peerProfile == null) return;
				if(!TextUtils.isEmpty(peername)) {
					tvPeerName.setText(peername);
					return;
				}
				String pname = ChatUtil.findAliasByUsername(peerProfile.getUserName());
				tvPeerName.setText(pname);
			}
		});
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnHungUp:
			hungUp();
			break;
		case R.id.ivHungupIcon:
			hungUp();
			break;
		case R.id.btnAnswer:
			answer();
		case R.id.ivAnswerIcon:
			answer();
			break;
		}
	}
	
	// 接听
	private void answer() {
		flAnswerLayout.setVisibility(View.GONE);
		btnAnswer.setVisibility(View.GONE);
//		ivAnswerIcon.setVisibility(View.GONE);
		cbMic.setVisibility(View.VISIBLE);
		cbSpeaker.setVisibility(View.VISIBLE);
		try {
			incomingCall.answerCall(30);
            incomingCall.startAudio();
            incomingCall.setSpeakerMode(false);   // 听筒模式: false            扬声器模式: true 
		} catch (SipException e) {
			e.printStackTrace();
		}
	}

	// 挂断电话
	private void hungUp() {
		closeActDelayed(1000, "结束通话");
		
		if(incomingCall != null) {
			try {
				incomingCall.endCall();
				incomingCall.close();
			} catch (SipException e) {
				e.printStackTrace();
			}	
		}
		
		if(makeAudioCall != null) {
			try {
				makeAudioCall.endCall();
				makeAudioCall.close();
			} catch (SipException e) {
				e.printStackTrace();
			}
		}
		
	}

	public void updateStatus(final String status) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				tvStatus.setText(status);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(incomingCall != null) {
			incomingCall.close();
			incomingCall = null;
		}
		
		if(callListener != null) {
			callListener = null;
		}
		
	}
	
	/**
	 * 设置最大音量
	 * @param context
	 */
	private void setMaxVolume(Context context) {
		AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);    
		int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL); 
		mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, maxVolume, 0);   
	}
	
	public void closeLocalProfile() {
        if (ChatUtil.getSipManagerInstance() == null) return;
        try {
            if (localProfile != null) {
            	ChatUtil.getSipManagerInstance().unregister(localProfile, null);     // 反注册Profile , 服务器上会显示下线。
            	ChatUtil.getSipManagerInstance().close(localProfile.getUriString());
            	localProfile = null;
            }
        } catch (Exception ee) {
            Log.d("MainActivityActivity/onDestroy", "Failed to close local profile.", ee);
        }
    }
	
	
	SipAudioCall.Listener callListener = new SipAudioCall.Listener() {
        @Override
        public void onCallEstablished(SipAudioCall call) {
            call.startAudio(); 
            call.setSpeakerMode(false);
            if(call.isMuted()) {
            	call.toggleMute();
            }
            updateStatus("通话建立.");
        }

        @Override
        public void onCallEnded(SipAudioCall call) {
            closeActDelayed(1000, "通话结束.Ready.");
        }

		@Override
		public void onCallBusy(SipAudioCall call) {
			closeActDelayed(1000, "对方正忙，挂断了");
		}
		
		public void onError(SipAudioCall call, int errorCode, String errorMessage) {
			closeActDelayed(1000, "异常关闭");
		};
    };
    
    /**
     * 延时delayMillis毫秒后关闭当前act, 可显示提示信息prompt
     * @param delayMillis
     * @param prompt
     */
    private void closeActDelayed(int delayMillis, final String prompt) {
    	if(!TextUtils.isEmpty(prompt)) {
    		runOnUiThread(new Runnable() {
				@Override
				public void run() {
					tvStatus.setText(prompt);
				}
			});
    	}
    	tvStatus.getHandler().postDelayed(new Runnable() {
			@Override
			public void run() {
				CallActivity.this.finish();
			}
		}, delayMillis);
    }
    
    // 挂断后需要关闭SipAudioCall, 否则又会注册不上
    @SuppressWarnings("unused")
	private void closeMakeAudioCall() {
    	if(makeAudioCall != null) {
			try {
				makeAudioCall.endCall();
				makeAudioCall.close();
			} catch (SipException e) {
				e.printStackTrace();
			}
		}
    }
    
	private static Vibrator vib;
    /**
     * 震动
     * @param act
     * @param pattern
     * @param isRepeat
     */
    private void vibrate(Activity act, long[] pattern ,boolean isRepeat) {
	    vib = (Vibrator) act.getSystemService(Service.VIBRATOR_SERVICE);
	    vib.vibrate(pattern, isRepeat ? 1 : -1);
    }
    
    /**
     *  取消震动
     */
    private void cancelVibrate() {
    	if(vib != null) {
    		vib.cancel();
    	}
    }


	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			pressDown(v);
			break;
			
		case MotionEvent.ACTION_UP:
			pressUp(v);
			break;
		}
		return false;
	}


	private void pressDown(View v) {
		switch (v.getId()) {
		case R.id.ivHungupIcon:
			btnHungUp.setPressed(true);
			break;

		case R.id.ivAnswerIcon:
			btnAnswer.setPressed(true);
			break;
		}
	}
    
	private void pressUp(View v) {
		switch (v.getId()) {
		case R.id.ivHungupIcon:
			btnHungUp.setPressed(false);
			hungUp();
			break;

		case R.id.ivAnswerIcon:
			btnAnswer.setPressed(false);
			answer();
			break;
		}
	}
}
