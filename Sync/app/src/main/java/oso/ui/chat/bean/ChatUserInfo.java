package oso.ui.chat.bean;

import java.io.Serializable;

import android.graphics.drawable.Drawable;

/**
 * Bean: ChatUserInfo 聊天用户的头像，用户名，状态，所属用户组信息
 * 
 * @author xialimin
 * 
 */
public class ChatUserInfo implements Serializable {
	private static final long serialVersionUID = 470422651618471229L;
	public Drawable avatar;
	public String avatarURL; // 用户头像
	public String username; // 用户名
	public String alias; // 用户别名
	public String status; // 状态 online offline
	private String warename; // 仓库名

	public ChatUserInfo(Drawable avatar, String avatarURL, String username, String alias,
			String warename, String status) {
		super();
		this.avatar = avatar;
		this.avatarURL = avatarURL;
		this.username = username;
		this.setAlias(alias);
		this.status = status;
		this.setWarename(warename);
	}

	public ChatUserInfo() {
		super();
	}

	public Drawable getAvatar() {
		return avatar;
	}
	
	public void setAvatar(Drawable avatar) {
		this.avatar = avatar;
	}
	
	public String getAvatarURL() {
		return avatarURL;
	}

	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getWarename() {
		return warename;
	}

	public void setWarename(String warename) {
		this.warename = warename;
	}

}
