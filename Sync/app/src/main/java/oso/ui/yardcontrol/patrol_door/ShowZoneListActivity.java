package oso.ui.yardcontrol.patrol_door;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.yardcontrol.patrol_spot.adapter.ResourceZoneAdapter;
import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import oso.ui.yardcontrol.patrol_spot.bean.ZoneBaseBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.lock.CheckPatrolTime;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 选择区域dockZone
 * @author zhangrui
 * @date 2014年5月26日 下午2:45:39
 * @description
 */
public class ShowZoneListActivity extends BaseActivity {
	
	
	private Context context ;
	private ListView listView ;
	private boolean flag = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_patrol_zone_list_layout,0);
		initView();
		setViewListener();
		loadBaseData();
	}
	
	/**
	 * @Description:初始化主UI控件
	 */
	private void initView(){
		setTitleString(getString(R.string.patrol_title));
		context = this;
 		listView = (ListView)findViewById(R.id.listview);
 		listView.setVisibility(View.GONE);
	}
	/**
	 * @Description:赋予主Ui控件监听事件
	 */
	private void setViewListener(){
		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadBaseData();
			}
		});
	}
	
	/**
	 * 下载登陆人所在仓库的Spot area 。
	 * 如果是数据库中已经有对应的表，和数据了。。那么就不用下载了。
	 * 在刷新的时候再去下载
	 * @descrition
	 */
	private void loadBaseData(){
		RequestParams params =	new RequestParams() ;
		params.add("Method", "LoadLoginUserDockZone");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<ZoneBaseBean> list = ZoneBaseBean.handJson(json);
				if(!Utility.isNullForList(list)){
					showValue(list);
				}else{
					UIHelper.showToast(context, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
				}

				if(json.optInt("needpatrolarea",0)!=0){
					RtAreaResouseBean.isZoneTypePatrolOver(list,true,mActivity);
				}
			}
			@Override
			public void handFail() {}
		}.doGet(HttpUrlPath.CheckInPatrol, params , context);
	}

//	private boolean handJson(JSONObject json){
//		boolean flag = false;
//		JSONArray jsonArray = 	StringUtil.getJsonArrayFromJson(json, "data");
//		if(jsonArray != null && jsonArray.length() > 0 ){
//			if(dockZoneList!=null&&dockZoneList.size()>0){
//				dockZoneList.clear();
//			}
//			for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
//				JSONObject jsonObject = StringUtil.getJsonObjectFromArray(jsonArray, index);
//				DockZone area = new DockZone();
//				
//				area.setDockZoneId(StringUtil.getJsonLong(jsonObject, "area_id"));
//				area.setDockZoneName(StringUtil.getJsonString(jsonObject, "area_name"));
//				area.setCount(StringUtil.getJsonInt(jsonObject, "occupycount"));
//				area.setMax(StringUtil.getJsonString(jsonObject, "max_number"));
//				area.setMin(StringUtil.getJsonString(jsonObject, "min_number"));
//				area.setCompareNum(StringUtil.getJsonString(jsonObject, "min_number"));
//				area.setPatrol_time(StringUtil.getJsonString(jsonObject, "patrol_time"));
//				if((area.getDockZoneName().toUpperCase()).equals("ALL")){
//					area.setCompareNum("0");
//				}
//				dockZoneList.add(area);
//			}
//		}
//		if(dockZoneList != null){
//			sortSpotArea();
//			flag = true;
//		}
//		return flag;
//	}
//	/**
//	 * @Description:排序
//	 */
//	private void sortSpotArea(){
//		for(int i=0;i<dockZoneList.size();i++){
//			for(int j = i+1;j<dockZoneList.size();j++){
//				int inum = Integer.parseInt(dockZoneList.get(i).getCompareNum());
//				int jnum = Integer.parseInt(dockZoneList.get(j).getCompareNum());
//				if(inum>jnum){
//					DockZone dockZone = dockZoneList.get(j) ;
//					dockZoneList.set(j, dockZoneList.get(i));
//					dockZoneList.set(i, dockZone);
//				}
//			}
//		}
//	}
	
	/**
	 * 显示数据
	 * @param result
	 * @descrition
	 */
	private void showValue(final List<ZoneBaseBean> result){
 		listView.setVisibility(View.VISIBLE);
		listView.setDivider(null);//earlybirdcurves
		listView.setAdapter(new ResourceZoneAdapter(result, context));
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view,int index, long id) {
				if(index < result.size()){
					getDocksInfo(result.get(index));
				}
			}
		});
	}
	/**
	 * 获取选择区域的Stops
	 * @descrition
	 */
	private void getDocksInfo(final ZoneBaseBean dockZone){
 		RequestParams params = new RequestParams();
		params.add("Method", "ZoneAndCheckInDetail");
		params.add("ZoneId", dockZone.area_id);
		params.add("Status","-1");	//2:No Ocuppied , 1:Ocuppied, -1 ALL

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				handDockAndCheckInfoJson(json,dockZone);
			}
			@Override
			public void handFail() {}
		}.doGet(HttpUrlPath.CheckInPatrol, params , context);
	}
	private void handDockAndCheckInfoJson(JSONObject json ,ZoneBaseBean dockZone){
		ArrayList<RtAreaResouseBean>  needPatrolList = RtAreaResouseBean.handJsonNP(json);
		ArrayList<RtAreaResouseBean>  finishPatrolList = RtAreaResouseBean.handJsonFP(json);
		if(Utility.isNullForList(needPatrolList)){
			UIHelper.showToast(context, "["+dockZone.area_name+"]" + getString(R.string.patrol_patrol_over_two), Toast.LENGTH_SHORT).show();
		}
		startShowDockAndCheckInfoActivity(needPatrolList,finishPatrolList,dockZone );
 	}
	/**
	 * 弹开显示占用spot 和 checkin info 的信息
	 * @param needPatrolList
	 * @descrition
	 */
	private void startShowDockAndCheckInfoActivity(ArrayList<RtAreaResouseBean> needPatrolList ,ArrayList<RtAreaResouseBean> finishPatrolList , ZoneBaseBean dockZone ){
		Intent  intent = new Intent(context,ShowDoorListActivity.class);
		intent.putExtra("needPatrolList", needPatrolList);
		intent.putExtra("finishPatrolList", finishPatrolList);
		intent.putExtra("zoneName", dockZone.area_name);
		intent.putExtra("zoneId", dockZone.area_id);
		startActivity(intent);
	}	

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(flag){
			loadBaseData();
		}
		flag = true;
		
		if (StoredData.readResetPatrolStatus()) {
			StoredData.saveResetPatrolStatus(false);

			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.hideCancelBtn();
			builder.setCancelable(false);
			builder.setMessage(CheckPatrolTime.ALARM_TEXT);
			builder.setPositiveButton("YES", null);
			builder.create().show();
		}
	}
	
}
