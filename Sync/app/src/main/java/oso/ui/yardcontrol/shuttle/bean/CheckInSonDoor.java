package oso.ui.yardcontrol.shuttle.bean;

/**
 * 门或位置的占用信息
 * 
 * @author Administrator
 * 
 */
public class CheckInSonDoor {

	private int gate_container_no;
	private String gate_liscense_plate;

	public int getGate_container_no() {
		return gate_container_no;
	}

	public void setGate_container_no(int gate_container_no) {
		this.gate_container_no = gate_container_no;
	}

	public String getGate_liscense_plate() {
		return gate_liscense_plate;
	}

	public void setGate_liscense_plate(String gate_liscense_plate) {
		this.gate_liscense_plate = gate_liscense_plate;
	}
}
