package oso.ui.yardcontrol.patrol_spot.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;


public class ZoneBaseBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 2656990032934182320L;
	public String area_id;//": "1000327",
	public String last_time;
	public int needpatrol;
	public int allcount;//": "13",
	public String area_name;//": "4-ST2"
	public String patrol_time;
	
	public static List<ZoneBaseBean> handJson(JSONObject json){
		List<ZoneBaseBean> list = null;
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "data");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			list = new ArrayList<ZoneBaseBean>();
			// 解析-doors
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				if(item!=null){
					ZoneBaseBean b = new ZoneBaseBean();					
					b.area_id = item.optString("area_id");
					b.last_time = item.optString("last_time");
					b.needpatrol = item.optInt("needpatrol");
					b.allcount = item.optInt("allcount");
					b.area_name = item.optString("area_name");
					b.patrol_time = item.optString("patrol_time");
					if(!StringUtil.isNullOfStr(b.area_name)&&"ALL".equals(b.area_name.toUpperCase())){
						list.add(0, b);
					}else{
						list.add(b);
					}
				}
			}
		}
		return list;
		
	}
}
