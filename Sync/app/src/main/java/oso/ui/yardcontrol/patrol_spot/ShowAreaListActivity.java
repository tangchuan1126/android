package oso.ui.yardcontrol.patrol_spot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.yardcontrol.patrol_spot.adapter.ResourceZoneAdapter;
import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import oso.ui.yardcontrol.patrol_spot.bean.ZoneBaseBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.lock.CheckPatrolTime;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 
 * @ClassName: PatrolSpotAreaShowActivity
 * @Description:
 * @author gcy
 * @date 2014年7月24日 下午4:09:33
 *
 */
public class ShowAreaListActivity extends BaseActivity {

	private Context context;
	private ListView listView;
	private boolean flag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_patrol_area_list_layout, 0);
		initView();
		setViewListener();
		loadBaseData();
	}

	/**
	 * @Description:初始化主UI控件
	 */
	private void initView() {
		setTitleString(getString(R.string.patrol_title));
		context = this;
		listView = (ListView) findViewById(R.id.listview);
		listView.setVisibility(View.GONE);
	}

	/**
	 * @Description:赋予主Ui控件监听事件
	 */
	private void setViewListener() {

		showRightButton(R.drawable.btn_ref_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadBaseData();
			}
		});
	}

	/**
	 * 下载登陆人所在仓库的Spot area 。 如果是数据库中已经有对应的表，和数据了。。那么就不用下载了。 在刷新的时候再去下载
	 * 
	 * @descrition
	 */
	private void loadBaseData() {
		RequestParams params = new RequestParams();
		params.add("Method", "LoadLoginUserSpotArea");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<ZoneBaseBean> list = ZoneBaseBean.handJson(json);
				if (!Utility.isNullForList(list)) {
					showValue(list);
				} else {
					UIHelper.showToast(context, getString(R.string.sync_nodatas), Toast.LENGTH_SHORT).show();
				}

				if(json.optInt("needpatrolarea",0)!=0){
					RtAreaResouseBean.isZoneTypePatrolOver(list, false, mActivity);
				}
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.CheckInPatrol, params, context);
	}

	/**
	 * 显示数据
	 * 
	 * @param result
	 * @descrition
	 */
	private void showValue(final List<ZoneBaseBean> result) {
		listView.setVisibility(View.VISIBLE);
		listView.setDivider(null);// earlybirdcurves
		listView.setAdapter(new ResourceZoneAdapter(result, context));
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int index, long id) {
				if (index < result.size()) {
					getStopsInfo(result.get(index));
				}
			}
		});
	}

	/**
	 * 获取选择区域的Stops
	 * @descrition
	 */
	private void getStopsInfo(final ZoneBaseBean spotArea) {
		RequestParams params = new RequestParams();
		params.add("Method", "SpotAndCheckInMain");
		params.add("AreaId", spotArea.area_id + "");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				handStopsAndCheckInfoJson(json, spotArea);
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.CheckInPatrol, params, context);
	}

	/**
	 * @Description:
	 * @param json
	 * @param spotArea
	 */
	private void handStopsAndCheckInfoJson(JSONObject json, ZoneBaseBean spotArea) {
		ArrayList<RtAreaResouseBean> needPatrolList = RtAreaResouseBean.handJsonNP(json);
		ArrayList<RtAreaResouseBean> finishPatrolList = RtAreaResouseBean.handJsonFP(json);
		if (Utility.isNullForList(needPatrolList)) {
			UIHelper.showToast(context, "[" + spotArea.area_name + "]" + getString(R.string.patrol_patrol_over_two), Toast.LENGTH_SHORT).show();
		}
		startShowSpotAndCheckInfoActivity(needPatrolList, finishPatrolList, spotArea);
	}

	/**
	 * 弹开显示占用spot 和 checkin info 的信息
	 * @descrition
	 */
	private void startShowSpotAndCheckInfoActivity(ArrayList<RtAreaResouseBean> needPatrolList, ArrayList<RtAreaResouseBean> finishPatrolList,
			ZoneBaseBean spotArea) {
		Intent intent = new Intent(context, ShowSpotListActivity.class);
		intent.putExtra("needPatrolList", needPatrolList);
		intent.putExtra("finishPatrolList", finishPatrolList);
		intent.putExtra("areaName", spotArea.area_name);
		intent.putExtra("areaId", spotArea.area_id);
		startActivity(intent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (flag) {
			loadBaseData();
		}
		flag = true;

		if (StoredData.readResetPatrolStatus()) {
			StoredData.saveResetPatrolStatus(false);

			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.hideCancelBtn();
			builder.setCancelable(false);
			builder.setMessage(CheckPatrolTime.ALARM_TEXT);
			builder.setPositiveButton("YES", null);
			builder.create().show();
		}
	}
}
