package oso.ui.yardcontrol.ctnr;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.ctnr.adapter.VerifySVAdapter;
import oso.ui.yardcontrol.ctnr.bean.VerifySVBean;
import oso.ui.yardcontrol.ctnr.iface.ViewDoubtIface;
import support.common.UIHelper;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 
 * @ClassName: 专为SuperVesor准备的 
 * @Description:
 * @author gcy 
 * @date 2014年7月14日 下午5:51:49 
 *
 */
public class ViewDoubtSVActivity extends BaseActivity implements ViewDoubtIface {
	
	private ListView listview;
	private VerifySVAdapter adapter;
	private List<VerifySVBean> baseList;
	private boolean isCheckOut;
	
	private View selectAll;
	private CheckBox checkBox;
	private Button checkout_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_patrol_view_doubtsv_activity_layout,0);
		initView();
	}
	
	/**
	 * @Description:初始化View
	 */
	protected void initView() {

		baseList = (List<VerifySVBean>)getIntent().getSerializableExtra("listData");
		isCheckOut = getIntent().getBooleanExtra("isCheckOut", false);
		
		setTitleString(getResources().getString(R.string.view_goes_ctnr));

		//------初始化View
		selectAll = (View) findViewById(R.id.selectAll);
		checkBox = (CheckBox) findViewById(R.id.checkBox);
		checkout_btn = (Button) findViewById(R.id.checkout_btn);
		
		listview = (ListView) findViewById(R.id.listview);
		listview.setEmptyView((ImageView) findViewById(R.id.any_data));
//		listview.setDivider(null);
		adapter = new VerifySVAdapter(mActivity, baseList,isCheckOut,checkBox);
		listview.setAdapter(adapter);
		
		//------设置 CheckBox的选中状态
		checkBox.setChecked(adapter!=null&&adapter.havaSelect());
		
		if(!isCheckOut){
			selectAll.setVisibility(View.GONE);
			checkout_btn.setVisibility(View.GONE);
			return;
		}
		
		if(Utility.isNullForList(baseList)){
			selectAll.setVisibility(View.INVISIBLE);
			checkout_btn.setVisibility(View.INVISIBLE);
			return;
		}
		selectAll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkBox.setChecked(!checkBox.isChecked());
				if(adapter!=null){
					adapter.selectAll(checkBox.isChecked());
				}
			}
		});
		
		checkout_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!judgeHaveSelect()){
					Animation anim = AnimationUtils.loadAnimation(mActivity, R.anim.shake_x);
					selectAll.startAnimation(anim);
					UIHelper.showToast(mActivity, getString(R.string.sync_select_data), Toast.LENGTH_LONG).show();
					return;
				}
				ReasonDialogWindow n = new ReasonDialogWindow(mActivity, baseList, ViewDoubtSVActivity.this);
				n.show();
			}
		});
	}
	
	/**
	 * @Description:判断是否有选中的数据 
	 */
	private boolean judgeHaveSelect(){
		int flagNum = 0;//判断是否有选中的数据
		for (int i = 0; i < baseList.size(); i++) {
			if (baseList.get(i).chose_checkbox) {
				flagNum++;
			}
		}
		return flagNum!=0;
	}
	
	/**
	 * @Description:根据返回的数据更新Ui
	 * @param @param json
	 */
	private void setView(JSONObject json){
		baseList = VerifySVBean.hendJson(json);
		if(Utility.isNullForList(baseList)){
			finish();
			return;
		}
		if(adapter==null){
			adapter = new VerifySVAdapter(mActivity, baseList,isCheckOut,checkBox);
		}else{
			adapter.setList(baseList);
		}
		checkBox.setChecked(adapter!=null&&adapter.havaSelect());
	}
	
	
	
	@Override
	/**
	 * 对单条数据进行CheckOut
	 */
	public void CheckOut(String note) {
		if(!StringUtil.isNullOfStr(note)){
			RequestParams params = new RequestParams();
			params.put("Method", "multipleCheckOut");
			params.put("datas",  getCheckOutData(note));
			
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_LONG).show();
					setView(json);
				}
			}.doPost(HttpUrlPath.CheckInPatrol, params, mActivity);
		}else{
			UIHelper.showToast(mActivity, getString(R.string.sync_reason_empty), Toast.LENGTH_LONG).show();
		}
	}
	

	
	
	/**
	 * @Description:获取需要提交的数据
	 * @return
	 */
	protected String getCheckOutData(String note) {
		JSONObject jObj = new JSONObject();
		try {
			JSONArray jArray = new JSONArray();
			for (int i = 0; i < baseList.size(); i++) {
				if (baseList.get(i).chose_checkbox) {
					JSONObject jItem = new JSONObject();
					jItem.put("equipment_id", baseList.get(i).equipment_id);
					jItem.put("note", StringUtil.isNullOfStr(note)?"NA":note);
					jArray.put(jItem);
				}
			}
			jObj.put("equipment_ids", jArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jObj.toString();
	}

}
