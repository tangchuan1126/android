package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;


/**
 * @author zhangrui
 * @date 2014年5月17日 下午12:03:23
 * @description
 */
public class CheckInDSonBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4015849318167188554L;
	private int sondoor;
	private int sondoorid;
	private int fatherdoorid;
	
	
	public int getFatherdoorid() {
		return fatherdoorid;
	}
	public void setFatherdoorid(int fatherdoorid) {
		this.fatherdoorid = fatherdoorid;
	}
	public int getSondoor() {
		return sondoor;
	}
	public void setSondoor(int sondoor) {
		this.sondoor = sondoor;
	}
	public int getSondoorid() {
		return sondoorid;
	}
	public void setSondoorid(int sondoorid) {
		this.sondoorid = sondoorid;
	}

	
	
}
