package oso.ui.yardcontrol;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.ctnr.ViewDoubtUtiles;
import oso.ui.yardcontrol.gate_checkin.GateCheckIn_SimpleAc;
import oso.ui.yardcontrol.gate_checkout.SearchGateCheckOutActivity;
import oso.ui.yardcontrol.gate_precheckin.GatePreCheckInAc;
import oso.ui.yardcontrol.patrol_spot.PatrolMainActivity;
import oso.ui.yardcontrol.photo.CheckInPhotoCaptureMainActivity;
import oso.ui.yardcontrol.shuttle.SearchShuttleActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import declare.com.vvme.R;

public class YardControlMainActivity extends BaseActivity implements OnClickListener {

	private TextView notifyTv; // photoCapture.entry数量

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_checkin_yard_main, 0);
		setTitleString(getString(R.string.main_yms));
		initView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 重新Load每天提交图片的数量
		initCountCheckInId();
	}

	private void initCountCheckInId() {
		int count = CheckInPhotoCaptureMainActivity.getEntryCnt();

		if (count > 0) {
			notifyTv.setText(String.valueOf(count));
			notifyTv.setVisibility(View.VISIBLE);
		} else {
			notifyTv.setVisibility(View.GONE);
		}
	}

	private void initView() {
		notifyTv = (TextView) findViewById(R.id.notifyTv);
		findViewById(R.id.btn_In).setOnClickListener(this);
		findViewById(R.id.btn_In2).setOnClickListener(this);
		findViewById(R.id.btn_Out).setOnClickListener(this);
		findViewById(R.id.btn_Photo).setOnClickListener(this);
		findViewById(R.id.rt_yard).setOnClickListener(this);
		findViewById(R.id.btn_Shuttle).setOnClickListener(this);
		findViewById(R.id.btn_View).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_In:
			popTo(GateCheckIn_SimpleAc.class);
			break;
		case R.id.btn_In2:
			popTo(GatePreCheckInAc.class);
			break;
		case R.id.btn_Out:
			popTo(SearchGateCheckOutActivity.class);
			break;
		case R.id.btn_Photo:
			popTo(CheckInPhotoCaptureMainActivity.class);
			break;
		case R.id.rt_yard:
			popTo(PatrolMainActivity.class);
			break;
		case R.id.btn_Shuttle:
			popTo(SearchShuttleActivity.class);
			break;
		case R.id.btn_View:
			ViewDoubtUtiles.getDataAndFlag(mActivity);
			break;
		}
	}

	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
