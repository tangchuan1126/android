package oso.ui.yardcontrol.gate_checkout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.gate_checkout.adapter.GCOAdapter;
import oso.ui.yardcontrol.gate_checkout.bean.GateCheckOutBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.DlgEditView.OnPromptListener;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.edittext.SimpleDlgAdapter;
import oso.widget.edittext.SimpleDlgAdapter.OnDlgItemListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class AddTrailerActivity extends BaseActivity {

	private EditText sv;
	private ListView gateLv;
	private GCOAdapter adapter;

	private List<GateCheckOutBean> addDatas;

	private String entry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_gatecheckout_add, 0);
		initData();
		initView();
		setData();
	}

	private void initData() {
		addDatas = new ArrayList<GateCheckOutBean>();
		entry_id = getIntent().getStringExtra("entry_id");
		setTitleString(getString(R.string.checkout_search_trailer));
	}
	
	private SingleSelectBar tab;

	private void initView() {
		tab=(SingleSelectBar)findViewById(R.id.tab);
		gateLv = (ListView) findViewById(R.id.gateLv);
		gateLv.setEmptyView(findViewById(R.id.nodataTv));
		sv = (EditText) findViewById(R.id.searchEt);
		sv.setTransformationMethod(new AllCapTransformationMethod());
		etEntry=(SearchEditText)findViewById(R.id.etEntry);
		
		//事件
		etEntry.setSeacherMethod(new SeacherMethod() {
			
			@Override
			public void seacher(String value) {
				// TODO Auto-generated method stub
				search(value);
			}
		});
	}
	
	private SearchEditText etEntry;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return sv.onKeyDown(keyCode, event) ? true : super.onKeyDown(keyCode, event);
	}

	private void setData() {
		adapter = new GCOAdapter(mActivity, addDatas);
		gateLv.setAdapter(adapter);
		gateLv.setFocusable(false);
		gateLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				GateCheckOutBean bean = (GateCheckOutBean) adapter.getItem(position);
				if (!adapter.isHideSeal(bean)) {
					showEditSealDialog(bean);
				}
			}
		});
		// allBox.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// for (GateCheckOut gco : showDatas) {
		// gco.setState(allBox.isChecked());
		// }
		// for (GateCheckOut gco : returnDatas) {
		// gco.setState(allBox.isChecked());
		// }
		// adapter.notifyDataSetChanged();
		// }
		// });
//		sv.setOnSearchListener(new OnSearchListener() {
//			@Override
//			public void onSearch(String value) {
//				search(value);
//			}
//		});
//		sv.getInputEt().setOnKeyListener(new View.OnKeyListener() {
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//				if (event.getAction() == KeyEvent.ACTION_UP) {
//					if (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_ENTER) {
//						String value = sv.getInputEt().getText().toString().trim();
//						search(value);
//						return true;
//					}
//				}
//				return false;
//			}
//		});
		final SimpleDlgAdapter adapter = new SimpleDlgAdapter(mActivity, sv);
		
		DlgEditView.attachOnFocus(sv, new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v, "Search")
						.setMethod("SearchTrailer").setHint("Search Trailer")
						.setDefKeyboard(true).setAdp(adapter)
						.setPromptOnClick(new OnPromptListener() {
							@Override
							public void onClick(String value) {
								search(value);
							}
						});
			}
		}, false);
		adapter.setOnDlgItemListener(new OnDlgItemListener() {
			@Override
			public void onClick(String value) {
				search(value);
			}
		});
		
		//tab
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.checkout_trailer), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.sync_entryid), 1));
		tab.setUserDefineClickItems(clickItems);
		tab.userDefineSelectIndex(0);
		tab.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				// TODO Auto-generated method stub
//				sv.setHint(getSearchHint());
				updateSearchEt();
			}
		});
	}
	
	private void updateSearchEt(){
		if(isSearchTrailer()){
			sv.setVisibility(View.VISIBLE);
			etEntry.setVisibility(View.GONE);
		}
		else{
			sv.setVisibility(View.GONE);
			etEntry.setVisibility(View.VISIBLE);
		}
	}
	
	private boolean isSearchTrailer(){
		return tab.getCurrentSelectItem().b==0;
	}
	
//	private String getSearchHint(){
//		return isSearchTrailer()?"Search Trailer":"Search Entry";
//	}

	private void showEditSealDialog(final GateCheckOutBean item) {
		final View view = getLayoutInflater().inflate(R.layout.dialog_edit_seal, null);
		final EditText sealEt = (EditText) view.findViewById(R.id.sealEt);
		final Button noSealBtn = (Button) view.findViewById(R.id.noSealBtn);
		sealEt.setTransformationMethod(new AllCapTransformationMethod());
		sealEt.setHint(getString(R.string.checkout_seal_null));
		Utility.endEtCursor(sealEt);
		noSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sealEt.setText("NA");
				Utility.endEtCursor(sealEt);
			}
		});
		final RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
		dialog.setTitle(getString(R.string.task_item_seal));
		dialog.setContentView(view);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String oldSeal = TextUtils.isEmpty(item.getNow_seal()) ? "NA" : item.getNow_seal();
				String newSeal = TextUtils.isEmpty(sealEt.getText().toString().trim()) ? "NA" : sealEt.getText().toString().trim();
				showSelectSealItemDialog(sealEt, item, oldSeal.toUpperCase(), newSeal.toUpperCase());
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void showSelectSealItemDialog(final EditText sealEt, final GateCheckOutBean item, final String oldSeal, final String newSeal) {
		if (oldSeal.equals(newSeal)) {
			item.setNewSeal(newSeal);
			adapter.notifyDataSetChanged();
			return;
		}
		final View view = getLayoutInflater().inflate(R.layout.dialog_select_seal, null);
		final LinearLayout oldSealBtn = (LinearLayout) view.findViewById(R.id.oldSealBtn);
		final LinearLayout newSealBtn = (LinearLayout) view.findViewById(R.id.newSealBtn);
		((TextView) view.findViewById(R.id.oldSealTv)).setText(oldSeal);
		((TextView) view.findViewById(R.id.newSealTv)).setText(newSeal);
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setCancelable(false);
		builder.setTitle(getString(R.string.checkout_select_seal));
		builder.setContentView(view);
		builder.hideCancelBtn();
		oldSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				item.setNewSeal(oldSeal);
				adapter.notifyDataSetChanged();
				builder.getDialog().dismiss();
			}
		});
		newSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				item.setNewSeal(newSeal);
				adapter.notifyDataSetChanged();
				builder.getDialog().dismiss();
			}
		});
		builder.show();
	}

	/**
	 * 可搜trailer、entry
	 * @param value
	 */
	private void search(String value) {
		if(TextUtils.isEmpty(value)) {
			UIHelper.showToast("Empty");
			return;
		}
		value = value.toUpperCase();
		final String values = value;
		Utility.colseInputMethod(mActivity, sv);
		RequestParams params = new RequestParams();
		
		if(isSearchTrailer()){
			params.put("Method", HttpPostMethod.GetCheckOutDataSearch);
			params.put("trailerNo", value);
			params.put("entry_id", entry_id);
		}
		else{
			params.put("Method", "GetCheckOutDataSearchByEntry");
			params.put("entry_id", value);
		}
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("GetCheckOutDataSearch= " + json.toString());
				Gson gson = new Gson();
				GateCheckOutBean out = gson.fromJson(StringUtil.getJsonString(json, "datas").toString(), GateCheckOutBean.class);
				addData(out,values);
			}

			@Override
			public void handFail() {
				UIHelper.showToast(mActivity, getString(R.string.sync_fail), Toast.LENGTH_SHORT).show();
			};
		}.doGet(HttpUrlPath.CheckInAction, params, mActivity);
	}

	private void addData(GateCheckOutBean out,String value) {
		if (out == null || out.getEquipment_number() == null) {
			UIHelper.showToast(mActivity, getString(R.string.checkout_equipment_error), Toast.LENGTH_SHORT).show();
			equipmentisnull(value);
			return;
		}
		if (isRepeat(out)) {
			UIHelper.showToast(mActivity, getString(R.string.checkout_equipment_repeat), Toast.LENGTH_SHORT).show();
			return;
		}
		if (out.getEquipment_type_msg().equals("Tractor")) {
			UIHelper.showToast(mActivity, getString(R.string.checkout_notadd_tractor), Toast.LENGTH_SHORT).show();
			return;
		}
		addDatas.add(out);
		adapter.notifyDataSetChanged();
	}

	public void equipmentisnull(final String value){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setTitle(getString(R.string.sync_notice));
		builder.setMessage(getString(R.string.sync_create_newequipment));
		builder.setPositiveButton(getString(R.string.sync_create),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						way(value);
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}
	public void way(String value){
		RequestParams params = new RequestParams();
		params.put("Method", "creatNewEquipmentOnCheckOut");
		params.put("equipment_type", 2+"");
		params.put("equipment_number", value+"");
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("GetCheckOutDataSearch= " + json.toString());
				Gson gson = new Gson();
				GateCheckOutBean out = gson.fromJson(StringUtil.getJsonString(json, "data").toString(), GateCheckOutBean.class);
				addDatas.add(out);
//				adapter.notifyDataSetChanged();
				Intent intent = new Intent(mActivity, GateCheckOutMainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("showDatas", (Serializable) addDatas);
				startActivity(intent);
			}
		}.doGet(HttpUrlPath.CheckInAction, params, mActivity);
	}
	public void okOnClick(View v) {
		if (!adapter.isAllHasSeal()) {
			adapter.startSealAnim();
			UIHelper.showToast(mActivity, getString(R.string.checkout_input_seal), Toast.LENGTH_SHORT).show();
			return;
		}
		// printer(showDatas);
		Intent intent = new Intent(mActivity, GateCheckOutMainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("showDatas", (Serializable) addDatas);
		startActivity(intent);
	}

	private boolean isRepeat(GateCheckOutBean gco) {
		for (GateCheckOutBean g : GateCheckOutMainActivity.allDatas) {
			if (g.getEquipment_id() == gco.getEquipment_id()) {
				return true;
			}
		}
		for (GateCheckOutBean g : addDatas) {
			if (g.getEquipment_id() == gco.getEquipment_id()) {
				return true;
			}
		}
		return false;
	}

}
