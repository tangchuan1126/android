package oso.ui.yardcontrol.patrol_spot.adapter;

import java.util.List;

import oso.ui.yardcontrol.patrol_spot.bean.ZoneBaseBean;
import utility.StringUtil;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class ResourceZoneAdapter extends BaseAdapter {

	private List<ZoneBaseBean> dockZones;
	private LayoutInflater inflater;
	private Context context;
	
	public ResourceZoneAdapter(List<ZoneBaseBean> dockZones,Context context) {
		super();
		this.dockZones = dockZones;
		this.inflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		return dockZones != null ? dockZones.size() : 0;
	}

	@Override
	public ZoneBaseBean getItem(int position) {
		return dockZones.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ResourceZoneHolder holder = null;
		if (convertView == null) {
			holder = new ResourceZoneHolder();
			convertView = inflater.inflate(R.layout.patrol_resource_zone_text_item, null);
			holder.zoneName = (TextView) convertView.findViewById(R.id.container_name);
			holder.more_layout = (View) convertView.findViewById(R.id.more_layout);
			holder.allcount = (TextView) convertView.findViewById(R.id.allcount);
			holder.needpatrol = (TextView) convertView.findViewById(R.id.needpatrol);
			holder.last_time = (TextView) convertView.findViewById(R.id.last_time);
			convertView.setTag(holder);
		} else {
			holder = (ResourceZoneHolder) convertView.getTag();
		}

		ZoneBaseBean bean = dockZones.get(position);
		holder.zoneName.setText(bean.area_name);
		holder.more_layout.setVisibility(View.VISIBLE);
		holder.allcount.setText(bean.allcount+"");
		holder.needpatrol.setText(bean.needpatrol+"");
		
//		holder.zoneName.setTextColor((StringUtil.isNullOfStr(bean.patrol_time))?Color.RED:Color.GREEN);
		holder.zoneName.setTextColor((StringUtil.isNullOfStr(bean.patrol_time))?Color.RED:(this.context.getResources().getColor(R.color.green)));
		
		holder.last_time.setText(bean.last_time);
		return convertView;
	}
}

class ResourceZoneHolder {
	TextView zoneName;
	View more_layout;
	TextView allcount;
	TextView needpatrol;
	TextView last_time;
}
