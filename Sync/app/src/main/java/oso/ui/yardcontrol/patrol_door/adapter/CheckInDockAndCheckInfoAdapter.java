package oso.ui.yardcontrol.patrol_door.adapter;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;

import oso.ui.yardcontrol.ctnr.EditEquipmentDialogWindow;
import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import oso.ui.yardcontrol.patrol_spot.iface.ResourceItemClickCallBack;
import oso.ui.yardcontrol.patrol_spot.linear.PatrolSpotOrDockSubItem;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.RtEquipment;
import support.key.OccupyStatusTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CheckInDockAndCheckInfoAdapter extends BaseAdapter {

	 

	public static int Ocuppied = 2 ;
	public static int UnOcuppied = 1 ;

	private ResourceItemClickCallBack dockClickCallBack ;
	private	List<RtAreaResouseBean> rtAreaSpotBaseBean ;
	private Context context ;
	private Resources resources;
	private LayoutInflater inflater;
	
	private boolean isNeedPatrol;//判断是否是 已经patrol的数据
	
	private CheckInDockAndCheckInfoAdapter oThis;
	
	
	public CheckInDockAndCheckInfoAdapter(List<RtAreaResouseBean> spotAndCheckInfos,Context context , ResourceItemClickCallBack dockClickCallBack,boolean isNeedPatrol) {
		super();
		this.rtAreaSpotBaseBean = spotAndCheckInfos;
		this.context = context;
		oThis = this;
		resources = this.context.getResources();
		this.inflater = LayoutInflater.from(context);
		this.dockClickCallBack = dockClickCallBack ;
		this.isNeedPatrol = isNeedPatrol;
 	}
	
	@Override
	public int getCount() {
		return rtAreaSpotBaseBean != null ?  rtAreaSpotBaseBean.size() : 0 ;
	}

	@Override
	public RtAreaResouseBean getItem(int position) {
		return rtAreaSpotBaseBean.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		CheckInDoorAndCheckInfoAdapterHolder holder = null;
		 if(convertView==null){
			holder = new CheckInDoorAndCheckInfoAdapterHolder();
			convertView = inflater.inflate(R.layout.patrol_rt_door_item,null);
			holder.spot_button = (View) convertView.findViewById(R.id.spot_button); 
			holder.type_value = (TextView) convertView.findViewById(R.id.type_value);
			
			holder.add_button = (ImageView) convertView.findViewById(R.id.add_button);
			holder.more_layout = (View) convertView.findViewById(R.id.more_layout);

			holder.equipment_layout = (LinearLayout) convertView.findViewById(R.id.equipment_layout);
			
			
			holder.tools_button = (View) convertView.findViewById(R.id.tools_button);
			holder.make_confim = (Button) convertView.findViewById(R.id.make_confim);

			convertView.setTag(holder);
		}else{
			holder = (CheckInDoorAndCheckInfoAdapterHolder) convertView.getTag();
		}
		
		final RtAreaResouseBean bean  =  rtAreaSpotBaseBean.get(position);
		//------------设置resouse信息 以及显示颜色
		holder.type_value.setText("Door " + bean.resource_name);
		holder.type_value.setTextColor((bean.occupy_status==OccupyStatusTypeKey.OCUPIED)?resources.getColor(R.color.checkin_patrol_red):resources.getColor(R.color.checkin_patrol_green));
		
		//------------判断设备是否为空 如果不为空则填充数据
		if(!Utility.isNullForMap(bean.equipmentMap)){
			holder.more_layout.setVisibility(View.VISIBLE);
			setResouses(bean, holder.equipment_layout);
		}else{
			holder.more_layout.setVisibility(View.GONE);
		}
		
		convertView.setOnClickListener(null);
		convertView.setEnabled(false);
		holder.add_button.setVisibility(View.VISIBLE);
		
		if(isNeedPatrol){
			
			holder.make_confim.setText(context.getString(R.string.patrol_confirm));
			holder.make_confim.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dockClickCallBack.confirmSubmit(bean,position);
				}
			});
			
			holder.spot_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!Utility.isFastClick(1000)){
						dockClickCallBack.modifyResource(bean , position);
					}
				} 
			});
			
		}else{
			holder.add_button.setVisibility(View.GONE);
			holder.make_confim.setText(context.getString(R.string.patrol_cancel));
			holder.make_confim.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dockClickCallBack.cancelPatrol(bean);
				}
			});
		}
		
		
		
 		return convertView;
	}
	
	/**
	 * @Description:遍历设备相关信息的显示列表
	 * @param @param bean 基础数据bean
	 * @param @param equipment_layout 需要填充的View控件
	 */
	private void setResouses(final RtAreaResouseBean bean,LinearLayout equipment_layout){
		//--------------移除当前索引下的所有View
		equipment_layout.removeAllViews();
		//遍历map数据
		Set<String> key = bean.equipmentMap.keySet();
		Iterator<String> it = key.iterator();
		int it_num = 0;
        while(it.hasNext()) {
            final String entry_id = (String) it.next();
            //引用外部UI填充主控件
            View view =((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.patrol_rt_spot_or_dock_sub_entry_item, null);  
            equipment_layout.addView(view);
            if(it_num==0){
            	((View) view.findViewById(R.id.line)).setVisibility(View.GONE);
            }
            //设置entryid值
            ((TextView)view.findViewById(R.id.entry_id)).setText("E"+entry_id);
            //根据当前key值获取map的value值
            final List<RtEquipment> list = bean.equipmentMap.get(entry_id);
            //循环显示数据
            for(int i=0;i<list.size();i++){
            	//引用外部方法获取view 显示设备填充View
            	final PatrolSpotOrDockSubItem item = new PatrolSpotOrDockSubItem(context);
        		item.setShowValues(list.get(i));
        		final int num = i ;
        		if(isNeedPatrol){
        			//控制View是否显示删除按钮以及添加删除事件
            		item.getDeleteView().setOnClickListener(new View.OnClickListener() {
        				@Override
        				public void onClick(View v) {
        					deleteDialog(bean,entry_id,num);
        				}
        			});
            		
        			item.getEditView().setOnClickListener(new View.OnClickListener() {
        				@Override
        				public void onClick(View v) {
        					EditEquipmentDialogWindow e = new EditEquipmentDialogWindow(context, list.get(num), oThis);
        					e.show();
        				}
        			});
        		}else{
        			item.getDeleteView().setVisibility(View.INVISIBLE);
        			item.getEditView().setVisibility(View.INVISIBLE);
        		}
                equipment_layout.addView(item);
            }
            it_num++;
        }
	}

	/**
	 * @Description:弹出dialog 用于删除设备相关信息
	 * @param @param bean 基础数据bean
	 * @param @param entry_id 
	 * @param @param i
	 */
	private void deleteDialog(final RtAreaResouseBean bean,final String entry_id,final int i){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(context.getString(R.string.sync_notice));
		final List<RtEquipment> list = bean.equipmentMap.get(entry_id);
		
		builder.setMessage(context.getString(R.string.tms_delete)+" "+list.get(i).equipment_number + " ?") ;
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				RequestParams params = new RequestParams();
				params.put("Method", "clearEquipment");
				params.put("equipment_id",list.get(i).equipment_id+"");
			
				new SimpleJSONUtil() {
					@Override
					public void handReponseJson(JSONObject json) {
						//成功过后如果还有车位显示则刷新数据
						if(json.optBoolean("data")){
							list.remove(i);
							if(Utility.isNullForList(bean.equipmentMap.get(entry_id))){
								bean.equipmentMap.remove(entry_id);
							}
							if(Utility.isNullForMap(bean.equipmentMap)){
								bean.occupy_status = OccupyStatusTypeKey.RELEASED;
							}
							UIHelper.showToast(context, context.getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
							oThis.notifyDataSetChanged();
						}else{
							UIHelper.showToast(context, context.getString(R.string.sync_fail_two), Toast.LENGTH_SHORT).show();
						}
					}
				}.doPost(HttpUrlPath.CheckInPatrol, params , context);
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), null);
		builder.create().show();
	}
	
}
class CheckInDoorAndCheckInfoAdapterHolder{
	public View spot_button;
	public TextView type_value;
	
	public ImageView add_button;
	
	public View more_layout;
	public LinearLayout equipment_layout;
	
	public View tools_button;
	public Button make_confim;
}
