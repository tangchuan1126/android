package oso.ui.yardcontrol.ctnr.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.dbhelper.StoredData;
import utility.StringUtil;

/**
 * o
 * @ClassName;// CheckInPatrolViewDoubtBase
 * @Description;//
 * @author gcy
 * @date 2014年7月15日 下午5;//01;//45
 * 
 */

public class VerifySVBean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -3754164185051806217L;
	
    public String equipment_purpose;// 1,
    public int pad_id;// 1,
    public String equipment_number;// "HDG",
    public String check_in_window_time;// "2015-01-19 08:26:54.0",
    public String seal_delivery;// "NA",
    public String check_in_time;// "2015-01-19 08:26:21.0",
    public int pa_id;// 1,
    public int equipment_id;// 1641,
    public int approve_status;// 2,
    public String check_out_time;// "2015-01-22 07:04:26.0",
    public String patrol_lost;// 1,
    public int equipment_type;// 1,
    public String ps_id;// 1000005,
    public int rel_type;// 4,
    public int equipment_status;// 5,
    public String check_in_entry_id;// 120121,
    public String note;// "fdfsdf",
    public String phone_equipment_number;// "434"
	public String location;
	
	public boolean chose_checkbox;
	/**
	 * @Description:辅助解析数据
	 * @param @param json
	 * @param @return
	 */
	public static List<VerifySVBean> hendJson(JSONObject json){
		JSONArray jsonArrays = StringUtil.getJsonArrayFromJson(json,"resources");
		List<VerifySVBean> baseList = new ArrayList<VerifySVBean>();
		if(!StringUtil.isNullForJSONArray(jsonArrays)){
			for(int i=0;i<jsonArrays.length();i++){
				JSONObject jsonItem = jsonArrays.optJSONObject(i);
				VerifySVBean bean = new VerifySVBean();
			    bean.equipment_purpose = jsonItem.optString("equipment_purpose");
			    bean.pad_id = jsonItem.optInt("pad_id");
			    bean.equipment_number = jsonItem.optString("equipment_number");
			    bean.check_in_window_time = jsonItem.optString("check_in_window_time");
			    bean.seal_delivery = jsonItem.optString("seal_delivery");
			    bean.check_in_time = jsonItem.optString("check_in_time");
			    bean.pa_id = jsonItem.optInt("pa_id");
			    bean.equipment_id = jsonItem.optInt("equipment_id");
			    bean.approve_status = jsonItem.optInt("approve_status");
			    bean.check_out_time = jsonItem.optString("check_out_time");
			    bean.patrol_lost = jsonItem.optString("patrol_lost");
			    bean.equipment_type = jsonItem.optInt("equipment_type");
			    bean.ps_id = jsonItem.optString("ps_id");
			    bean.rel_type = jsonItem.optInt("rel_type");
			    bean.equipment_status = jsonItem.optInt("equipment_status");
			    bean.check_in_entry_id = jsonItem.optString("check_in_entry_id");
			    bean.note = jsonItem.optString("note");
			    bean.phone_equipment_number = jsonItem.optString("phone_equipment_number");
			    bean.location = jsonItem.optString("location");
				bean.chose_checkbox = true;
				baseList.add(bean);
			}
		}
		return baseList;
	}
	
	/**
	 * @Description:如果全部Patrol 才可以进行patrol
	 * @param @param json
	 * @param @return
	 */
	public static boolean isCheckOut(JSONObject json){
		return (json.optInt("count",-1)==0&&StoredData.isSupervisor());
	}
}
