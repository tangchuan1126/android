package oso.ui.yardcontrol.photo.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import oso.ui.yardcontrol.photo.CheckInPhotoCaptureMainActivity;
import support.dbhelper.Goable;
import support.key.BCSKey;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.network.HttpTool;
import utility.FileUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

/**
 * 用于-photoCapture上传图片
 * 
 * @author 朱成
 * @date 2014-11-5
 */
public class Run_UpCheckoutPhoto implements Runnable {

	private ProgressDialog progressDialog;
	private Context context;
	private Handler handler;
	private String prefix; // 图片上传到服务器端的文件前缀名称

	private List<String> listEntrys;

	public Run_UpCheckoutPhoto(Context context,
			List<String> listEntrys, ProgressDialog progressDialog,
			Handler handler) {
		super();
		this.progressDialog = progressDialog;
		this.context = context;
		this.handler = handler;

		this.listEntrys = listEntrys;
	}

	@Override
	public void run() {
		Goable.initGoable(context);
		for (int index = 0; index < listEntrys.size(); index++) {
			submitZipFile(listEntrys.get(index));
			progressDialog.setProgress(index+1);
		}
		handler.obtainMessage(CheckInPhotoCaptureMainActivity.What_UploadOK).sendToTarget();
	}

	// 上传"单个entry"
	private void submitZipFile(String entry) {

		Map<String, String> params = new HashMap<String, String>();
		//debug
		params.put("Method", "UpFile");
		params.put("LoginAccount", Goable.LoginAccount);
		params.put("Password", Goable.Password);
		params.put("entry_id", entry);
		// 暂时固定
		// params.put("dir", holder.b.get(0).getFile_path());
		// params.put("file_with_class", holder.b.get(0).getFile_with_class());
		// params.put("prefix", prefix + holder.b.get(0).getFile_with_id() +
		// "_");
		// debug
		params.put("dir", "check_in");
		params.put("file_with_class", FileWithCheckInClassKey.PhotoGateCheckOut+"");
		params.put("file_with_id", entry);
		params.put("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN+"");

		params.put("prefix", "gate_check_out_" + entry);
		try {
			String returnValue = HttpTool.postFiles(
					HttpUrlPath.FileUpZipAction, params, initMapFile(entry));
			// 如果上传成功那么，在本地就应该删除
			JSONObject jsonObject = StringUtil.converntString(returnValue);
			if (StringUtil.getJsonInt(jsonObject, "ret") == BCSKey.SUCCESS) {
				//删除-图片
				CheckInPhotoCaptureMainActivity.delEntryImgs(entry);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 压缩文件为"zip",构造map.put("gate_out", zipFile);
	private Map<String, File> initMapFile(String entry) {
		List<File> files = CheckInPhotoCaptureMainActivity.getUploadImgs(entry);
		File zipFile = FileUtil.createZipFile("checkin_gateout_", files);
		Map<String, File> map = new HashMap<String, File>();
		map.put("gate_out", zipFile);
		return map;
	}

}
