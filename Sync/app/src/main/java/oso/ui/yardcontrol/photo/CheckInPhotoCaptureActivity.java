package oso.ui.yardcontrol.photo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CheckInPhotoCaptureActivity extends BaseActivity implements
		OnClickListener {

	private TabToPhoto linearLayoutPhoto;
	private SearchEditText etEntry;
	private View btnSubmit;

	private String entry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_photo_capture_layout_1, 0);
		setTitleString(getString(R.string.yms_gate_checkout_photo_title));

		applyParams();

		// 取view
		linearLayoutPhoto = (TabToPhoto) findViewById(R.id.linearLayoutPhoto);
		etEntry = (SearchEditText) findViewById(R.id.etEntry);
		btnSubmit = findViewById(R.id.submit);

		// 监听事件
		btnSubmit.setOnClickListener(this);
		if (!isModify()) {
			etEntry.setSeacherMethod(new SeacherMethod() {
				@Override
				public void seacher(String value) {
					searchEntry();
				}
			});
			etEntry.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					if (KeyEvent.KEYCODE_ENTER == keyCode
							&& event.getAction() == KeyEvent.ACTION_UP) {
						searchEntry();
						// 防止焦点下移
						return true;
					}
					return false;
				}
			});
		}

		// 初始化
		etEntry.setScanMode();
		if (isModify()) {
			etEntry.setText(entry_id);
			etEntry.setEnabled(false);
		}

		// 若新建,先清空目录
		if (!isModify()) {
			new Thread() {
				public void run() {
					File file0 = new File(Goable.getDir_takephoto(),
							TTPKey.getPhotoCapture_new(0));
					File file1 = new File(Goable.getDir_takephoto(),
							TTPKey.getPhotoCapture_new(1));
					Utility.delFolder(file0);
					Utility.delFolder(file1);

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							linearLayoutPhoto
									.init(mActivity, getTabParamList());
						}
					});
				}
			}.start();
		} else
			linearLayoutPhoto.init(mActivity, getTabParamList());

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		linearLayoutPhoto.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.submit:

			// 修改时,直接退出
			if (isModify()){
				finish();
				return;
			}

			String entry = etEntry.getText().toString();
			if(!TextUtils.isEmpty(entry)){
				notentry(entry);
				etEntry.setText("");
			}
			if (TextUtils.isEmpty(entry)) {
				UIHelper.showToast(this, getString(R.string.sync_entry_empty));
				return;
			}

			//之前已给该entry添加过
			if (CheckInPhotoCaptureMainActivity.isEntry_hasImgs(entry)) {
				UIHelper.showToast(this, getString(R.string.sync_entry_ever_photo));
				return;
			}
			
			break;

		default:
			break;
		}
	}
    public void notentry(final String entry){//asdf
    	RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingListByEntry");
		params.add("entry_id", entry + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// "临时目录"改为"有效缓存"
				File file0 = new File(Goable.getDir_takephoto(),
						TTPKey.getPhotoCapture_new(0));
				File file0_new = new File(Goable.getDir_takephoto(),
						TTPKey.getPhotoCaptureDir(entry, 0));
				Utility.renameFile(file0, file0_new);

				finish();
			}
		}.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, mActivity);
    }
	// 看entry是否已存在
	private void searchEntry() {
		String entry = etEntry.getText().toString();
		if (TextUtils.isEmpty(entry)) {
			UIHelper.showToast(this, getString(R.string.sync_entry_empty));
			return;
		}

		if (CheckInPhotoCaptureMainActivity.isEntry_hasImgs(entry))
			UIHelper.showToast(this, getString(R.string.sync_entry_ever_photo));

		// 下放键盘
		Utility.colseInputMethod(mActivity, etEntry);
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String tmp = !isModify() ? "new" : entry_id;
		params.add(new TabParam("Check Out", TTPKey.getPhotoCaptureDir(tmp, 0),
				new PhotoCheckable(0, "Check Out", linearLayoutPhoto)));
		return params;
	}

	private boolean isModify() {
		return !TextUtils.isEmpty(entry_id);
	}

	// =========传参===============================

	public static void initParams(Intent in, String entry_id) {
		in.putExtra("entry_id", entry_id);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		entry_id = params.getString("entry_id");
	}

}
