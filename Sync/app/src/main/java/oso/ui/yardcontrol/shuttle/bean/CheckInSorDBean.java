package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CheckInSorDBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<CheckInSBean> stop = new ArrayList<CheckInSBean>();		 

	private List<CheckInDBean> door = new ArrayList<CheckInDBean>();		
	
	private List<CheckInSBean> nostop = new ArrayList<CheckInSBean>();

	private List<CheckInDBean> nodoor = new ArrayList<CheckInDBean>();

	private List<String> stopstr = new ArrayList<String>();
	
	private List<String> doorstr = new ArrayList<String>();
	
	
	private List<CheckInSSonBean> stopson = new ArrayList<CheckInSSonBean>();

	private List<CheckInDSonBean> doorson = new ArrayList<CheckInDSonBean>();

	
	
	public List<CheckInSBean> getNostop() {
		return nostop;
	}

	public void setNostop(List<CheckInSBean> nostop) {
		this.nostop = nostop;
	}

	public List<CheckInDBean> getNodoor() {
		return nodoor;
	}

	public void setNodoor(List<CheckInDBean> nodoor) {
		this.nodoor = nodoor;
	}

	public List<CheckInDSonBean> getDoorson() {
		return doorson;
	}

	public void setDoorson(List<CheckInDSonBean> doorson) {
		this.doorson = doorson;
	}

	public List<CheckInSSonBean> getStopson() {
		return stopson;
	}

	public void setStopson(List<CheckInSSonBean> stopson) {
		this.stopson = stopson;
	}

	public List<String> getStopstr() {
		return stopstr;
	}

	public void setStopstr(List<String> stopstr) {
		this.stopstr = stopstr;
	}

	public List<String> getDoorstr() {
		return doorstr;
	}

	public void setDoorstr(List<String> doorstr) {
		this.doorstr = doorstr;
	}

	public List<CheckInSBean> getStop() {
		return stop;
	}

	public void setStop(List<CheckInSBean> stop) {
		this.stop = stop;
	}

	public List<CheckInDBean> getDoor() {
		return door;
	}

	public void setDoor(List<CheckInDBean> door) {
		this.door = door;
	}
	
	
}
