package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;


public class CheckInSSonBean implements Serializable {

	private int sonstop;
	private int sonstopid;
	private int fatherstopid;
	
	public int getFatherstopid() {
		return fatherstopid;
	}
	public void setFatherstopid(int fatherstopid) {
		this.fatherstopid = fatherstopid;
	}
	public int getSonstop() {
		return sonstop;
	}
	public void setSonstop(int sonstop) {
		this.sonstop = sonstop;
	}
	public int getSonstopid() {
		return sonstopid;
	}
	public void setSonstopid(int sonstopid) {
		this.sonstopid = sonstopid;
	}


	
	
}
