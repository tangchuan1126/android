package oso.ui.yardcontrol.patrol_spot.iface;

import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;

public interface ResourceItemClickCallBack {
	/**
	 * @Description:备份需要Confim的车位
	 * @param bean
	 * @param index
	 */
	public void confirmSubmit(RtAreaResouseBean bean,int index);
	
	public void cancelPatrol(RtAreaResouseBean bean);
	
	public void modifyResource(RtAreaResouseBean bean,int index);
}
