package oso.ui.yardcontrol.ctnr;

import org.json.JSONObject;

import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SealEditText;
import support.common.UIHelper;
import support.common.bean.RtEquipment;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
/**
 * @ClassName: EditEquipmentDialogWindow 
 * @Description: 修改设备信息
 * @author gcy
 * @date 2015-1-25 下午5:03:32
 */
public class EditEquipmentDialogWindow implements OnClickListener {

	private Context context;
	private Dialog dialog;

	int scWidth = 480;
	int scHeight = 800;

	private RtEquipment equipment;				//当前"修改"的类别(修改时才有)
	
	private SealEditText editpment;
	
	private BaseAdapter oThis;
	
	public EditEquipmentDialogWindow(Context context,RtEquipment verifySVBean,BaseAdapter oThis) {
		this.context = context;
		this.oThis = oThis;
		initDialog(context, verifySVBean);
	}
	
	

	private void initDialog(Context context, RtEquipment verifySVBean) {
		this.equipment = verifySVBean;

		scWidth = getScreenWidth(context);
		scHeight = getScreenHeight(context);
		dialog = new Dialog(context, R.style.signDialog);
		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		window.setWindowAnimations(R.style.signAnim); // 添加动画
		dialog.setContentView(R.layout.edit_equipment_view_dialog);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		editpment = (SealEditText) dialog.findViewById(R.id.editpment);
		
		editpment.initView("Number", equipment.equipment_number, "");

		editpment.endEtCursor();
		
		//初始化
		editpment.setEnable(true);
		
		(dialog.findViewById(R.id.submit)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			dialogForSeal();
			break;

		default:
			break;
		}
	}

	/**
	 * @Description:提交seal信息
	 * @param @param activity
	 * @param @param verifySVBean
	 */
	public void dialogForSeal(){
		//--------获取seal数据
		final String inSeal=editpment.getText();
		
		if(equipment.equipment_number.equals(inSeal)){
			close();
			return;
		}
		
		final View view = LayoutInflater.from(context).inflate(R.layout.dialog_select_equipment, null);
		final LinearLayout oldBtn = (LinearLayout) view.findViewById(R.id.oldBtn);
		final LinearLayout newBtn = (LinearLayout) view.findViewById(R.id.newBtn);
		((TextView) view.findViewById(R.id.old_equipment)).setText(equipment.equipment_number);
		((TextView) view.findViewById(R.id.new_equipment)).setText(((StringUtil.isNullOfStr(inSeal))?"NA":inSeal));
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setCancelable(false);
		builder.setTitle("Select Equipment");
		builder.setContentView(view);
		builder.hideCancelBtn();
		oldBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				builder.getDialog().dismiss();
				close();
			}
		});
		newBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//--------初始化提交数据
				builder.getDialog().dismiss();
				submitData();
			}
		});
		builder.create().show(); 
//		String message = "Old Equipment Number: "+equipment.equipment_number+"\n";
//		
//			   message += "Change to\n";
//			   
//			   message += "New Equipment Number: "+((StringUtil.isNullOfStr(inSeal))?"NA":inSeal);
//		
//		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
//		builder.setMessage(message);
//		builder.setCancelable(false);
//		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				//--------初始化提交数据
//				submitData();
//			}
//		});
//		builder.setNegativeButton(getString(R.string.sync_no), null);
//		builder.create().show();
	}
	
	public void submitData(){
		//--------获取seal数据
		final String equipment_name = StringUtil.isNullOfStr(editpment.getText())?"NA":editpment.getText();
		
		RequestParams params = new RequestParams();
		params.add("Method", "updateEquipment");
		params.add("equipment_id", equipment.equipment_id+"");
 		params.add("equipment_number", equipment_name);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				equipment.equipment_number=equipment_name;
				oThis.notifyDataSetChanged();
				UIHelper.showToast(context.getString(R.string.sync_success));
				close();
			}
		}.doPost(HttpUrlPath.CheckInPatrol, params, context);
	}

	public void show() {
		// isShowing = true;
		dialog.show();
	}

	public void close() {
		if (dialog.isShowing()) {
			// isShowing = false;
			dialog.dismiss();
		}
	}

	private static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	private static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm;
	}

}
