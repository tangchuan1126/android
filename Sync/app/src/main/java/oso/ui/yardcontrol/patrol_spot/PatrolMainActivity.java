package oso.ui.yardcontrol.patrol_spot;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.patrol_door.ShowZoneListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import declare.com.vvme.R;

public class PatrolMainActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_patrol_yard_main, 0);
		setTitleString(getString(R.string.yms_gate_patrol_yard));
		initView();
	}

	private void initView() {
		findViewById(R.id.rt_door).setOnClickListener(this);
		findViewById(R.id.rt_spot).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rt_spot:
			popTo(ShowAreaListActivity.class);
			break;

		case R.id.rt_door:
			popTo(ShowZoneListActivity.class);
		}
	}

	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
