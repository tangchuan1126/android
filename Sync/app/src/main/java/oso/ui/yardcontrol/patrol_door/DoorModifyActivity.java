package oso.ui.yardcontrol.patrol_door;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import oso.ui.yardcontrol.patrol_spot.linear.PatrolSpotOrDockModifyItem;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.DlgEditView.OnPromptListener;
import oso.widget.edittext.SimpleDlgAdapter;
import oso.widget.edittext.SimpleDlgAdapter.OnDlgItemListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.RtEquipment;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: SpotPatrolModifyActivity 
 * @Description: 
 * @author gcy
 * @date 2014-11-26 下午5:23:17
 */
public class DoorModifyActivity extends BaseActivity {

	private Context context;
	private Resources resources;
	private SingleSelectBar singleSelectBar;

	private RtAreaResouseBean doorBean;
	private int changedIndex;
	private int type; 

	private EditText tractor_sv;
	private View data_layout;
	private View array_layout;
	private String tractorValue = null;
	private boolean haveSubmit;//判断是否创建或者提交过数据 默认是false 没有操作过的
	private LinearLayout show_datalist;
	private Button create_button;
	
	String Method = "";
	String hintStr = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.patrol_rt_addlayout,0);
		getFromActivityData();
		initView();
		initSingleSelectBar();
		setListenerForView();
	}
 	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	protected void getFromActivityData() {
		doorBean = (RtAreaResouseBean) getIntent().getSerializableExtra("rtAreaDockBaseBean"); 
		changedIndex = getIntent().getIntExtra("changedIndex", 0);
	}
	/**
	 * @Description:初始化Ui主控件
	 */
	private void initView() {
		context = this;
		resources = getResources();
		tractor_sv = (EditText) findViewById(R.id.sv);

		data_layout = findViewById(R.id.data_layout);
		array_layout = findViewById(R.id.array_layout);
		create_button = (Button) findViewById(R.id.create_button);
		create_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				CreateEntryIdDialog();
			}
		});
		
		show_datalist = (LinearLayout) findViewById(R.id.show_datalist);
		setTitleString(getString(R.string.shuttle_door_text) + doorBean.resource_name);
	}
	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
//		clickItems.add(new HoldDoubleValue<String, Integer>("All", 2));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.patrol_tractor_title), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.patrol_trailer_title), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					type = CheckInTractorOrTrailerTypeKey.TRACTOR;
					hintStr = getString(R.string.checkout_search_tractor);
					Method = "SearchTractor";
					break;
				case 1:
					type = CheckInTractorOrTrailerTypeKey.TRAILER;
					hintStr = getString(R.string.checkout_search_trailer);
					Method = "SearchTrailer";
					break;
				}
				//默认三中搜索方式的布局均为隐藏
				array_layout.setVisibility(View.GONE);
				data_layout.setVisibility(View.GONE);
				tractor_sv.setHint(hintStr);
				tractor_sv.setInputType(InputType.TYPE_CLASS_TEXT);
				tractor_sv.setText("");
				tractor_sv.setSelected(false);
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}
	
	/**
	 * @Description:为Ui控件增加监听
	 */
	private void setListenerForView() {
		// 后退按钮
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				affrimGiveUp();
			}
		});
//		tractor_sv.setOnSearchListener(new OnSearchListener() {
//			@Override
//			public void onSearch(String value) {
//				if (isNullForSv()) {
//					return;
//				}
//				tractorValue = value;
//				searchData();
//			}
//		});
//
//		// sv搜索框右侧内的火车事件
//		tractor_sv.getInputEt().setOnKeyListener(new View.OnKeyListener() {
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//				if (event.getAction() == KeyEvent.ACTION_UP) {
//					if (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_ENTER) {
//						if (isNullForSv()) {
//							return false;
//						}
//						tractorValue = tractor_sv.getInputEt().getText().toString().trim();
//						searchData();
//						Utility.colseInputMethod(context, tractor_sv.getInputEt());
//						if (tractor_sv.getKeyboard().getVisibility() == View.VISIBLE)
//							tractor_sv.hideKbLayout();
//						return true;
//					}
//				}
//				return false;
//			}
//		});

		final SimpleDlgAdapter adapter = new SimpleDlgAdapter(mActivity, tractor_sv);

		DlgEditView.attachOnFocus(tractor_sv, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v, "Search").setMethod(Method).setHint(hintStr).setDefKeyboard(true).setAdp(adapter)
						.setPromptOnClick(new OnPromptListener() {
							@Override
							public void onClick(String value) {
								tractorValue = value;
								searchData();
							}
						});
			}
		}, false);
		adapter.setOnDlgItemListener(new OnDlgItemListener() {
			@Override
			public void onClick(String value) {
				if (isNullForSv()) {
					return;
				}
				tractorValue = value;
				searchData();
			}
		});
	}

	/**
	 * @Description:判断searchEditText是否为空
	 */
	private boolean isNullForSv() {
		if (StringUtil.isNullOfStr(tractor_sv.getText().toString())) {
			UIHelper.showToast(context, getString(R.string.sync_input_value), Toast.LENGTH_SHORT).show();
			return true;
		}
		return false;
	}
	
	/**
	 * @Description:根据searchEditText内值来搜索
	 */
	private void searchData() {
		RequestParams params = new RequestParams();
		params.add("Method", "SearchResourceByEquipment");
		params.add("Type", String.valueOf(type));
		params.add("searchValue", tractorValue);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				//如果是ALL的搜索方式的话那么默认解析机构为数组 否则为对象
					getEquipmentArrayList(json);
			}
		}.doGet(HttpUrlPath.CheckInPatrol, params, context);
	}
	
	/**
	 * @Description:解析查询方法所返回来的数据
	 * @param @param json
	 */
	private void getEquipmentArrayList(JSONObject json) {
		Map<String,List<RtEquipment>> map = RtEquipment.handJsonArray(json);
		if(Utility.isNullForMap(map)){
			array_layout.setVisibility(View.GONE);
			data_layout.setVisibility(View.VISIBLE);
		}else{
			array_layout.setVisibility(View.VISIBLE);
			data_layout.setVisibility(View.GONE);
			setResouses(map);
		}
	}
	
	/**
	 * @Description:解析ArrayJSON数组后 为UI添加布局 显示设备 并可对其进行删除
	 * @param @param rtEquipmentList
	 */
	private void setResouses(final Map<String,List<RtEquipment>> map){
		show_datalist.removeAllViews();		
		Set<String> key = map.keySet();
		Iterator<String> it = key.iterator();
		int it_num = 0;
        while(it.hasNext()) {
            final String entry_id = (String) it.next();
            //引用外部UI填充主控件
            View view =((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.patrol_rt_spot_or_dock_sub_entry_item, null);  
            show_datalist.addView(view);
            if(it_num==0){
            	((View) view.findViewById(R.id.line)).setVisibility(View.GONE);
            }
            //设置entryid值
            ((TextView)view.findViewById(R.id.entry_id)).setText("E"+entry_id);
            //根据当前key值获取map的value值
            List<RtEquipment> list = map.get(entry_id);
            //循环显示数据
            for(int i=0;i<list.size();i++){
            	//引用外部方法获取view 显示设备填充View
            	show_datalist.addView(getDetailView(map,entry_id,i));
            }
            it_num++;
        }
        
		Button add_button = (Button)findViewById(R.id.add_button);
		add_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submitArray(map);
			}
		});
	}
	/**
	 * @Description:显示tractors or trailers的列表 并且赋予点击事件 使其可以进行删除货柜
	 * @param @param bean 该停车位下的bean
	 * @param @param i
	 * @param @param istractors
	 * @param @param spotClickCallBack
	 * @param @return
	 */
	private LinearLayout getDetailView(final Map<String,List<RtEquipment>> map,final String entry_id,final int i){
		PatrolSpotOrDockModifyItem item = new PatrolSpotOrDockModifyItem(context);
		RtEquipment r = map.get(entry_id).get(i);
		item.setShowValueAndLocation(r);
		return item ;
	}
	

	
	/**
	 * @Description:弹出提交设备的对话框 是否确认操作
	 * @param
	 */
	private void CreateEntryIdDialog() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.patrol_create_equipment, null);

		TextView type_value = (TextView) layout.findViewById(R.id.type_value);
		
		final TextView equipment_type = (TextView) layout.findViewById(R.id.equipment_type);
		final EditText equipment_name = (EditText) layout.findViewById(R.id.equipment_name);
		
		
		final boolean tractorFlag = (type==CheckInTractorOrTrailerTypeKey.TRACTOR);
		equipment_type.setText(resources.getText(tractorFlag?R.string.patrol_tractor_title:R.string.patrol_trailer_title));
		equipment_name.setText(tractorValue);		
		type_value.setText(getString(R.string.shuttle_door_text)+" [" + doorBean.resource_name + "]");
		
		builder.setTitle(getString(R.string.window_linear_text));
		builder.setCancelable(true);
		builder.setPositiveButton(getString(R.string.sync_create_text), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				createEntry(equipment_name.getText().toString(),tractorFlag);
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.setContentView(layout);
		builder.create().show();
	}
	/**
	 * @Description:提交创建设备的请求
	 * @param @param value
	 * @param @param flag
	 */
	public void createEntry(String value,boolean flag) {
		RequestParams params = new RequestParams();
		params.put("Method", "createEquipment");
		params.add("resource_type", OccupyTypeKey.DOOR+"");
		params.add("resource_id", String.valueOf(doorBean.resource_id));
//		params.add("license_plate", flag?value:"");
//		params.add("ctnr", flag?"":value);
		params.add("equipment_type", String.valueOf((flag?CheckInTractorOrTrailerTypeKey.TRACTOR:CheckInTractorOrTrailerTypeKey.TRAILER)));
		params.add("equipment_number", value);
		
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				haveSubmit = true;
				UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				isLeaveing();
			}
		}.doPost(HttpUrlPath.CheckInPatrol, params, context);
	}

	/**
	 * @Description:提交需要添加的设备数组
	 * @param @param rtEquipmentList
	 */
	public void submitArray(Map<String,List<RtEquipment>> map) {
		RequestParams params = new RequestParams();
		params.add("Method", "BatchModifyResourceEquipment");
		JSONObject doorObject = new JSONObject();
		boolean haveSelectData = RtEquipment.getSelectJson(map, doorObject, doorBean);
		if(!haveSelectData){
			UIHelper.showToast(getString(R.string.sync_select_equipment));
			return;
		}
		
		params.add("datas", doorObject.toString());
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				isLeaveing();
			}
			@Override
			public void handFail() {}
		}.doPost(HttpUrlPath.CheckInPatrol, params , context);
	}
	
	
	/**
	 * @Description:关闭提示
	 */
	private void affrimGiveUp() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(resources.getString(R.string.sys_holdon));
		builder.setMessage(getString(R.string.sync_exit));
		builder.setPositiveButton(resources.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(haveSubmit){
					finishSuccessActivty(true);
				}else{
					finish();
				}
			}
		});
		builder.setNegativeButton(resources.getString(R.string.sync_no), null);
		builder.create().show();
	}
	
	/**
	 * @Description:关闭提示
	 */
	private void isLeaveing() {
//		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
//		builder.setTitle(resources.getString(R.string.sys_holdon));
//		builder.setMessage("Continue add?");
//		builder.setPositiveButton(resources.getString(R.string.sync_yes),  new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				tractor_sv.getInputEt().setText("");
//				tractor_sv.getInputEt().requestFocus();
//				data_layout.setVisibility(View.GONE);
//				array_layout.setVisibility(View.GONE);
//			}
//		});
//		builder.setNegativeButton(resources.getString(R.string.sync_no), new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
				finishSuccessActivty(true);
//			}
//		});
//		builder.create().show();
	}
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		affrimGiveUp();
	}
	
	/**
	 * 需要上一个页面刷新的finish
	 * 
	 * @Description:
	 */
	private void finishSuccessActivty(boolean flag) {
		Intent intent = new Intent(this, ShowDoorListActivity.class);
		intent.putExtra("changedIndex", changedIndex);
		intent.putExtra("flag", flag);
		setResult(RESULT_OK, intent);
		finish();
	}

}
