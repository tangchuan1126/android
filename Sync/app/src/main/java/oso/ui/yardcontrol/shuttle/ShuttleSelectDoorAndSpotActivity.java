package oso.ui.yardcontrol.shuttle;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.shuttle.bean.ShuttleBean;
import support.key.CheckInTractorOrTrailerTypeKey;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class ShuttleSelectDoorAndSpotActivity extends BaseActivity implements OnClickListener {

	private ShuttleBean shuttleBean; // 弹出窗口的选择，过后也回填到这个bean里面

	private TextView select_entry_id;
	private TextView select_ctnr;
	private ImageView imageView1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_shuttle_select_layout, 0);

		Intent intent = this.getIntent();
		shuttleBean = (ShuttleBean) intent.getSerializableExtra("Shuttle");
		setTitleString(getString(R.string.shuttle_title_small));
		// 监听事件
		findViewById(R.id.movetospot).setOnClickListener(this);
		findViewById(R.id.movetodoor).setOnClickListener(this);

		select_entry_id = (TextView) findViewById(R.id.show_entry_id);
		select_ctnr = (TextView) findViewById(R.id.release_name);
		select_entry_id.setText(shuttleBean.getCheck_in_entry_id() + "");
		select_ctnr.setText(shuttleBean.getEquipment_number() + "");
		imageView1 = (ImageView) findViewById(R.id.imageView1);
		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(shuttleBean.getEquipment_type(), imageView1);
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRACTOR)){
//			imageView1.setImageResource(R.drawable.ic_tractor);
//		}
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRAILER)){
//			imageView1.setImageResource(R.drawable.ic_trailer);
//		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.movetospot:
			Intent intent = new Intent(this, CheckInShuttleReleaseDoorActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable("Shuttle", shuttleBean);
			intent.putExtras(bundle);
			intent.putExtra("entry_id", select_entry_id.getText()+"");
			startActivity(intent);
			break;
		case R.id.movetodoor:
			Intent intent2 = new Intent(this, CheckInShuttleReleaseSpotActivity.class);
			Bundle bundle2 = new Bundle();
			bundle2.putSerializable("Shuttle", shuttleBean);
			intent2.putExtras(bundle2);
			startActivity(intent2);
			break;

		default:
			break;
		}
	};

}
