package oso.ui.yardcontrol.ctnr.adapter;

import java.util.List;

import oso.ui.yardcontrol.ctnr.bean.VerifySVBean;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class VerifySVAdapter extends BaseAdapter  {
	
	private List<VerifySVBean> arrayList;
	private LayoutInflater inflater;
	private Context context;
	private BaseAdapter oThis;
	private boolean isCheckOut;
	private CheckBox checkBox;
	
	public VerifySVAdapter(Context context,List<VerifySVBean> arrayList,boolean isCheckOut,CheckBox checkBox) {
		super();
 		this.context = context;
		this.arrayList = arrayList ;
		this.inflater  = LayoutInflater.from(this.context);
		this.oThis = this;
		this.isCheckOut = isCheckOut;
		this.checkBox = checkBox;
	}
	
	@Override
	public int getCount() {
 		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public VerifySVBean getItem(int position) {
 		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
 		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		 VerifySVHoder holder = null;
		 if(convertView==null){
			holder = new VerifySVHoder();
			convertView = inflater.inflate(R.layout.checkin_patrol_view_doubtsv_layout_item,null);

			holder.check_layout = (View)convertView.findViewById(R.id.check_layout);
			
			holder.chose_checkbox = (CheckBox)convertView.findViewById(R.id.chose_checkbox);
			holder.entry_id = (TextView)convertView.findViewById(R.id.entry_id);

			holder.equipment_type = (ImageView)convertView.findViewById(R.id.equipment_type);
			holder.equipment_number = (TextView)convertView.findViewById(R.id.equipment_number);	
			holder.type = (TextView)convertView.findViewById(R.id.type);
			holder.status = (TextView)convertView.findViewById(R.id.status);

			holder.old_lay = (View)convertView.findViewById(R.id.old_lay);
			holder.old_location = (TextView)convertView.findViewById(R.id.old_location);
			
 			convertView.setTag(holder);
		}else{
			holder = (VerifySVHoder) convertView.getTag();
		}
		 final VerifySVBean temp  =  arrayList.get(position);
		 holder.entry_id.setText("E"+temp.check_in_entry_id);
		 CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(temp.equipment_type, holder.equipment_type);
		 holder.equipment_number.setText(temp.equipment_number);
		 
		 holder.type.setText(CheckInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(context,temp.equipment_purpose));	
		 
		 if(temp.rel_type > 0 && !StringUtil.isNullOfStr(CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(context,temp.rel_type))){
			 holder.status.setVisibility(View.VISIBLE);
			 holder.status.setText(CheckInMainDocumentsRelTypeKey.getContainerTypeKeyValue(context,temp.rel_type)+" : "+CheckInMainDocumentsStatusTypeKey.getReturnStatusById(context,temp.equipment_status));	
		 }else{
			 holder.status.setVisibility(View.GONE);
		 }
		 
		 //--------------原来的位置
		 if(!StringUtil.isNullOfStr(temp.location)){
			 holder.old_lay.setVisibility(View.VISIBLE);
			 holder.old_location.setVisibility(View.VISIBLE); 
			 holder.old_location.setText(temp.location);
		 }else{
			 holder.old_lay.setVisibility(View.GONE);
			 holder.old_location.setVisibility(View.GONE); 
		 }

		 if(isCheckOut){
			 holder.chose_checkbox.setChecked(temp.chose_checkbox);
			 //--------------控制权选反选
			 holder.check_layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					temp.chose_checkbox = !temp.chose_checkbox;
					checkBox.setChecked(setAllSelect());
					oThis.notifyDataSetChanged();
				}
			 });
		 }else{
			 holder.chose_checkbox.setVisibility(View.INVISIBLE);
			 holder.check_layout.setBackgroundColor(Color.WHITE);
			 holder.equipment_number.setTextColor(Color.BLACK);
			 holder.entry_id.setTextColor(Color.BLACK);
			 holder.type.setTextColor(Color.BLACK);
		 }
		 
		 
		 convertView.setEnabled(false);
		 return convertView;
	}
	
	private boolean setAllSelect(){
		if(!Utility.isNullForList(arrayList)){
			for(int i=0;i<arrayList.size();i++){
				if(arrayList.get(i).chose_checkbox){
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * @Description:设置新数据 刷新列表
	 * @param @param arrayList
	 */
	public void setList(List<VerifySVBean> arrayList ){
		this.arrayList = arrayList ;
		oThis.notifyDataSetChanged();
	}
	
	/**
	 * @Description:设置当前数据集是全部选中还是 全部取消
	 * @param @param choseAll
	 */
	public void selectAll(boolean choseAll){
		for(int i=0;i<arrayList.size();i++){
			arrayList.get(i).chose_checkbox = choseAll;
		}
		oThis.notifyDataSetChanged();
	}
	/**
	 * @Description:判断当前数据集 是否有选中的数据
	 * @param @return
	 */
	public boolean havaSelect(){
		for(int i=0;i<arrayList.size();i++){
			if(arrayList.get(i).chose_checkbox){
				return true;
			}
		}
		return false;
	}
}
class VerifySVHoder {
	public View check_layout;
	
	public CheckBox chose_checkbox;
	public TextView entry_id;

	public ImageView equipment_type;
	public TextView equipment_number;	
	public TextView type;
	public TextView status;
	
	public View old_lay;
	public TextView old_location;
}