package oso.ui.yardcontrol.ctnr;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.yardcontrol.ctnr.bean.VerifySVBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
/**
 * @ClassName: ViewDoubtUtiles 
 * @Description: 工具类
 * @author gcy
 * @date 2015-1-23 上午10:46:06
 */
public class ViewDoubtUtiles{
	
	
	/**
	 * @Description:根据人员的权限 来判断是否显示该按钮
	 * 				如果该人员为Supervisor的话则可以显示 并且点击进去进行check out
	 */
	public static void showViewGoseCTNR(View view,final Activity activity) { //用逗号隔开的字符串
		view.setVisibility(StoredData.isSupervisor()?View.VISIBLE:View.GONE);

		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					getDataAndFlag(activity);
			}
		});
		
	}
	

	
	/**
	 * @Description:获取数据来判断是否跳到指定的activity
	 * @param @param activity
	 */
	public static void getDataAndFlag(final Activity activity) {
		
		RequestParams params = new RequestParams();
		params.put("Method", HttpPostMethod.patrolApproveList);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				goToViewDoubtSVActivity(activity, json);
			}
		}.doGet(HttpUrlPath.CheckInPatrol, params, activity);
	}
	
	/**
	 * @Description:跳转到SuperVesor页面如无数据 则不进行跳转并提示
	 * @param @param activity
	 * @param @param json
	 */
	private static void goToViewDoubtSVActivity(final Activity activity,final JSONObject json){
		final List<VerifySVBean> baseList = VerifySVBean.hendJson(json);
		if(Utility.isNullForList(baseList)){
//			UIHelper.showToast("No Doubt!");
			//debug
			UIHelper.showToast(activity.getString(R.string.bcs_key_norecordsexception));
			return;
		}


		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(activity);
		builder.setTitle(activity.getString(R.string.sync_notice));
		builder.setMessage("Patrol Finish");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(activity, ViewDoubtSVActivity.class);
				intent.putExtra("listData", (Serializable) baseList);
				intent.putExtra("isCheckOut", VerifySVBean.isCheckOut(json));
				activity.startActivity(intent);
				activity.overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
			}
		});
		builder.setNegativeButton("No",null);
		builder.show();
	}
}
