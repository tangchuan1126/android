package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;

public class ShuttleBean implements Serializable {

	private static final long serialVersionUID = -4462106743932266479L;

	public String getEquipment_number() {
		return equipment_number;
	}

	public void setEquipment_number(String equipment_number) {
		this.equipment_number = equipment_number;
	}

	public String getEquipment_id() {
		return equipment_id;
	}

	public void setEquipment_id(String equipment_id) {
		this.equipment_id = equipment_id;
	}

	public String getCheck_in_entry_id() {
		return check_in_entry_id;
	}

	public void setCheck_in_entry_id(String check_in_entry_id) {
		this.check_in_entry_id = check_in_entry_id;
	}

	public int getEquipment_type() {
		return equipment_type;
	}

	public void setEquipment_type(int equipment_type) {
		this.equipment_type = equipment_type;
	}
	
	private String equipment_number;
	private String equipment_id;
	private String check_in_entry_id;
	private int equipment_type;
}
