package oso.ui.yardcontrol.patrol_spot.bean;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import declare.com.vvme.R;
import oso.ui.yardcontrol.patrol_door.ShowZoneListActivity;
import oso.ui.yardcontrol.patrol_spot.ShowAreaListActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.bean.RtEquipment;
import support.key.CheckInTractorOrTrailerTypeKey;
import utility.StringUtil;

/**
 * @ClassName: RtAreaResouseBean
 * @Description:  区域下面的资源信息
 * @author gcy
 * @date 2014-11-25 下午2:58:48
 */

public class RtAreaResouseBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 3514187592256453231L;
	public int occupy_status;//": 2,
	public String resource_name;//": 2,
	public int resource_id;//: 50,
	public String resource_type;//": 2
	public String area_id;//": 2
	
	public boolean confimFlag;//判断是否需要confirm的数据
	public Map<String, List<RtEquipment>> equipmentMap;
	
	
    /*********************************************************************************************/
	
	
	/**
	 * @Description:解析需要patrol的JSON数据
	 * @param @param json
	 * @param @return
	 */
	public static ArrayList<RtAreaResouseBean> handJsonNP(JSONObject json){
		//---------解析需要Patrol的数据
		JSONArray needpatrol = json.optJSONArray("needpatrol");
		return resolveJson(needpatrol);
	}
	
	/**
	 * @Description:解析已经patrol的JSON数据
	 * @param @param json
	 * @param @return
	 */
	public static ArrayList<RtAreaResouseBean> handJsonFP(JSONObject json){
		//---------解析已经Patrol的数据
		JSONArray finishpatrol = json.optJSONArray("finishpatrol");
		return resolveJson(finishpatrol);
	}
	
	/**
	 * @Description:辅助解析
	 * @param @param jsonArray
	 * @param @return
	 */
	private static ArrayList<RtAreaResouseBean> resolveJson(JSONArray jsonArray){
		ArrayList<RtAreaResouseBean>  returnArray = new ArrayList<RtAreaResouseBean>();
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			for(int i=0;i<jsonArray.length();i++){
				JSONObject spotJson = jsonArray.optJSONObject(i);
				RtAreaResouseBean rt = new RtAreaResouseBean();
				rt.occupy_status = spotJson.optInt("occupy_status");
				rt.resource_name = spotJson.optString("resource_name");
				rt.resource_id = spotJson.optInt("resource_id");
				rt.resource_type = spotJson.optString("resource_type");
				rt.area_id = spotJson.optString("area_id","0");
				
				rt.equipmentMap = new HashMap<String, List<RtEquipment>>();
				
				JSONArray tractorsArray = spotJson.optJSONArray("tractors");
				if(!StringUtil.isNullForJSONArray(tractorsArray)){
					for(int j=0;j<tractorsArray.length();j++){
						JSONObject tractorsJson = tractorsArray.optJSONObject(j);
						String entryId = tractorsJson.optString("check_in_entry_id");
						if(rt.equipmentMap.get(entryId)==null){
							List<RtEquipment> list = new ArrayList<RtEquipment>();
							rt.equipmentMap.put(entryId, list);
						}
						
						RtEquipment r = new RtEquipment();
						r.equipment_id = tractorsJson.optString("equipment_id");
						r.equipment_number = tractorsJson.optString("equipment_number").toUpperCase(Locale.CANADA);
						r.equipment_type = CheckInTractorOrTrailerTypeKey.TRACTOR;
						rt.equipmentMap.get(entryId).add(r);
						
				
					}
				}
				//
				JSONArray trailersArray = spotJson.optJSONArray("trailers");
				if(!StringUtil.isNullForJSONArray(trailersArray)){
					for(int j=0;j<trailersArray.length();j++){
						JSONObject trailersJson = trailersArray.optJSONObject(j);
						
						String entryId = trailersJson.optString("check_in_entry_id");
						if(rt.equipmentMap.get(entryId)==null){
							List<RtEquipment> list = new ArrayList<RtEquipment>();
							rt.equipmentMap.put(entryId, list);
						}
						
						RtEquipment r = new RtEquipment();
						r.equipment_id = trailersJson.optString("equipment_id");
						r.equipment_number = trailersJson.optString("equipment_number").toUpperCase(Locale.CANADA);
						r.equipment_type = CheckInTractorOrTrailerTypeKey.TRAILER;
						rt.equipmentMap.get(entryId).add(r);
					}
				}
				returnArray.add(rt);
			}
		}
		
		return returnArray;
	}
	
	/**
	 * @Description:判断所有区域是否巡逻结束
	 * @param @param json
	 * @param @return
	 */
	public static boolean patrolOver(JSONObject json){
		return json.optInt("count",-1)==0;
	}

	/**
	 * @Description:判断门或者停车位的所有区域是否巡逻结束
	 * @param @param json
	 * @param @return
	 */
	public static void isZoneTypePatrolOver(List<ZoneBaseBean> list,boolean isDoor,Activity ac){
		int patrol_num = 0;

		for (int i = 0; i < list.size(); i++) {
			if(!TextUtils.isEmpty(list.get(i).patrol_time)){
				patrol_num++;
			}
		}
		if(patrol_num == list.size()){
			showJumpDialog(isDoor,ac);
		}
	}

	private static void showJumpDialog(boolean isDoor, final Activity ac){
		final Intent intent = new Intent();
		String massage = "";
		if(isDoor){
			intent.setClass(ac, ShowAreaListActivity.class);
			massage = "Patrol Spot?";
		}else{
			intent.setClass(ac, ShowZoneListActivity.class);
			massage = "Patrol Door?";
		}

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(ac);
		builder.setTitle(ac.getString(R.string.sync_notice));
		builder.setMessage(massage);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ac.startActivity(intent);
				ac.overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
				ac.finish();
			}
		});
		builder.setNegativeButton("No",null);
		builder.show();
	}
}
