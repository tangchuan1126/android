package oso.ui.yardcontrol.gate_checkin.adapter;

import java.util.List;

import oso.ui.yardcontrol.gate_checkin.bean.Gate_EquipBean;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpDlgCheckout extends BaseAdapter {
	
	private Context ct;
	private List<Gate_EquipBean> listEquips;
	
	public AdpDlgCheckout(Context ct,List<Gate_EquipBean> listEquips){
		this.ct=ct;
		this.listEquips=listEquips;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listEquips.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
//		R.layout.dlg_checkin_out_item;item_cheakin_main_layout
		Holder h;
		if (convertView == null) {
			convertView=LayoutInflater.from(ct).inflate(R.layout.dlg_checkin_out_item, null);
			h = new Holder();
			h.imageView1=(ImageView)convertView.findViewById(R.id.imageView1);
			h.tractor_txt=(TextView)convertView.findViewById(R.id.tractor_txt);
			h.door_txt=(TextView)convertView.findViewById(R.id.door_txt);
			h.equipment_txt=(TextView)convertView.findViewById(R.id.equipment_txt);
			h.loEntry_dlg=convertView.findViewById(R.id.loEntry_dlg);
			h.tvEntry_dlg=(TextView)convertView.findViewById(R.id.tvEntry_dlg);
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();
		
		//ic_tractor/ic_trailer
		
		//刷数据
		Gate_EquipBean b=listEquips.get(position);
		h.imageView1.setImageResource(b.isTractor()?R.drawable.ic_tractor:R.drawable.ic_trailer);
		h.tractor_txt.setText(b.EQUIPMENT_NUMBER);
		h.door_txt.setVisibility(b.hasRes()?View.VISIBLE:View.GONE);
		if(b.hasRes())
			h.door_txt.setText(ct.getResources().getString(R.string.tms_occupied)+b.getRes_str(ct)+": "+b.RESOURCES_NAME);
		h.loEntry_dlg.setVisibility(b.isFirstOne?View.VISIBLE:View.GONE);
		if(b.isFirstOne)
			h.tvEntry_dlg.setText("E"+b.ENTRY_ID);
		
		return convertView;
	}
	
	class Holder{
		ImageView imageView1;
		TextView tractor_txt,door_txt,equipment_txt,tvEntry_dlg;
		View loEntry_dlg;
	}

}
