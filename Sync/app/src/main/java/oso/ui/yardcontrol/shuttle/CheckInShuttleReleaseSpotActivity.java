package oso.ui.yardcontrol.shuttle;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.YardControlMainActivity;
import oso.ui.yardcontrol.shuttle.bean.CheckInDBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInDSonBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSorDBean;
import oso.ui.yardcontrol.shuttle.bean.CheckUtils;
import oso.ui.yardcontrol.shuttle.bean.ShuttleBean;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.ChooseUsersActivity;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.BaseDataDepartmentKey;
import support.key.BaseDataSetUpStaffJobKey;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: CheckInShuttleReleaseSpotActivity
 * @Description: 释放停车位
 * @author gcy
 * @date 2014-8-21 上午10:21:01
 */
public class CheckInShuttleReleaseSpotActivity extends BaseActivity {

	private static int selectDoorActivityFlag = 201;
	private SharedPreferences sp;
	private Context context;
	private Resources resources;
	private Button submitButton;
	private LinearLayout zoneLinearLayout;
	private LinearLayout doorLinearLayout;
	private TextView showDoor;
	private TextView showZone;
	private TextView showEntryId;
	private TextView releaseName;
	private EditText userIds;
	private TextView userNames;
	private ProgressDialog dialog;
	private List<CheckInDBean> fixZones;
	private ShuttleBean shuttleBean; // 弹出窗口的选择，过后也回填到这个bean里面
	private boolean isSubmitData = false;
	private int doorFather = -1 ;
	private int selectDoorName ; 		//选择Spot回来过后的填充
	private long selectDoorId ; 		//选择Spot回来过后的填充
	
	private List<ResourceInfo> doorlist = new ArrayList<DockHelpUtil.ResourceInfo>();
	private String[] freeZones;
	private String areaName;
	private String resourceName;
	
	ImageView imageView1;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.yard_shuttle_spot_activity_layout, 0);
		Intent intent = this.getIntent();
		setTitleString(getString(R.string.shuttle_moveto_door));
		shuttleBean = (ShuttleBean) intent.getSerializableExtra("Shuttle");
		context = CheckInShuttleReleaseSpotActivity.this;
		resources = getResources();
		initView();
		initOper();
	}

	private void initView() {
		showEntryId = (TextView) findViewById(R.id.show_entry_id);
		releaseName = (TextView) findViewById(R.id.release_name);
		submitButton = (Button) findViewById(R.id.submit_button);
		userIds = (EditText) findViewById(R.id.user_ids);
		userNames = (TextView) findViewById(R.id.informant_edittext_item);
//		userNames.setInputType(InputType.TYPE_NULL);// 关闭软键盘
//		userNames.setFocusable(false);
		

		showDoor = (TextView) findViewById(R.id.show_door);
		showZone = (TextView) findViewById(R.id.show_zone);
		zoneLinearLayout = (LinearLayout) findViewById(R.id.zone_linear_layout);
		doorLinearLayout = (LinearLayout) findViewById(R.id.door_linear_layout);

		showEntryId.setText(shuttleBean.getCheck_in_entry_id());
		releaseName.setText(shuttleBean.getEquipment_number() + "");

		dialog = new ProgressDialog(context);
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(resources.getString(R.string.sys_loadBase_data));
		dialog.setCancelable(true);
		// releaseName.requestFocus();
		
		imageView1 = (ImageView) findViewById(R.id.imageView1);

		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(shuttleBean.getEquipment_type(), imageView1);
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRACTOR)){
//			imageView1.setImageResource(R.drawable.ic_tractor);
//		}
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRAILER)){
//			imageView1.setImageResource(R.drawable.ic_trailer);
//		}
	}

	/**
	 * 第一个是人为添加的Spot area,所以应该去掉
	 * @descrition
	 */
	private List<CheckInDBean> fixCheckZoneBeans() {
		List<CheckInDBean> returnValue = new ArrayList<CheckInDBean>();
		CheckInSorDBean checkInSorDBean = CheckInGateJsonUtil.getStopordoor();
		if (checkInSorDBean != null && checkInSorDBean.getDoor() != null) {
			List<CheckInDBean> arrayList = checkInSorDBean.getDoor();
			if (arrayList.size() > 0) {
				for (CheckInDBean bean : arrayList) {
					if (!bean.getDoor().trim().equals("Zone")) {
						returnValue.add(bean);
					}
				}
			}
		}
		fixZones = returnValue;
		return returnValue;
	}

	private void checkReleaseSpot() {
		if (StringUtil.isNullOfStr(userIds.getText().toString())) {
			UIHelper.showToast(context, getString(R.string.sync_select_user), Toast.LENGTH_SHORT).show();
			return;
		}
		if (StringUtil.isNullOfStr(showDoor.getText().toString())) {
			UIHelper.showToast(context, getString(R.string.sync_select_door), Toast.LENGTH_SHORT).show();
			return;
		}
		doRelaseSpot();
	}

	private void doRelaseSpot(){

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(resources.getString(R.string.sync_notice));
		builder.setMessage(getString(R.string.shuttle_moveto_pop)+" "+getString(R.string.shuttle_door_text)+": "+showDoor.getText().toString()+" ?");
		builder.setCancelable(false);
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {	
			@Override
			public void onClick(DialogInterface dialog, int which) {
				doSubmitReleaseSpot();
			}
		} 
		);
		builder.create().show();
}
	private void doSubmitReleaseSpot() {
		RequestParams params = new RequestParams();
		params.put("entry_id", shuttleBean.getCheck_in_entry_id()+"");
		params.put("exeucut_use_ids", userIds.getText().toString()+"");
		params.put("resources_id",selectDoorId+""); 	
		params.put("equipment_id", shuttleBean.getEquipment_id()+"");
		params.put("Method", "MoveToDoor");

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(context, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				CheckInGateJsonUtil.clearData();
				isSubmitData = true;
				finishActivity();
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidShuttleAction, params, context);
	}

	private void openSelectDoor() {
		//李君浩注释
		Intent intent = new Intent(context, CheckInSelectDoorActivity.class);
		
		String areaNameText = showZone.getText().toString();
		String resourceNameText = showDoor.getText().toString();
		if(areaNameText != null && resourceNameText != null){
			areaName = areaNameText;
			resourceName = resourceNameText;
			intent.putExtra("doorId", selectDoorId); // long
			intent.putExtra("father", doorFather);// int
			intent.putExtra("areaName", areaName);
			intent.putExtra("resourceName", resourceName);
		}else{
			intent.putExtra("doorId", selectDoorId); // long
			intent.putExtra("father", doorFather);// int
		}
		startActivityForResult(intent, selectDoorActivityFlag);
//		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == selectDoorActivityFlag) {
			if (resultCode == Activity.RESULT_OK) {
				ResourceInfo item = (ResourceInfo) data.getSerializableExtra("Select");
				if (item != null) {
					areaName = item.area_name+"";
					resourceName = item.resource_name+"";
					selectDoorId = item.resource_id;
					showZone.setText(areaName);
					showDoor.setText(resourceName);
					storageInfo(areaName,resourceName);
					
				}
			}
		}
	}
	
	// 将数据存数到内存
	private void storageInfo(String areaName, String resourceName) {
		new CheckUtils().putString(mActivity, areaName, resourceName);
	}

	public void getchechinstopById(Context context, final String fromWhat) {
		Goable.initGoable(context);
		final RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params); 
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		params.add("request_type", 2 + "");
		params.add("occupy_type", OccupyTypeKey.DOOR + "");
		params.add("entry_id", shuttleBean.getCheck_in_entry_id() + "");
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				doorlist = DockHelpUtil.handjson_big(json);
				freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
				
				openZoneBuilder();
			}
		}.doPost(HttpUrlPath.AndroidDockCheckInAction, params, context);
	}

	private void openSelectZone() {
		if (!DockHelpUtil.isLoadData()) {
			getchechinstopById(context, "selectZone");
			return;
		}
		doorlist = DockHelpUtil.getStopordoor();
		freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
		// 弹开选择zone的
		openZoneBuilder();
	}

	/**
	 * 选择Zone的builder
	 * @descrition
	 */
	private void openZoneBuilder() {
		
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(mActivity);
		b.setTitle(getString(R.string.shuttle_select_zone));
		b.setItems(freeZones, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showZone.setText(freeZones[position]);
			}
		});
		b.show();
	}

	/**
	 * 计算最小的一个值（首先是选择奇数的，奇数没有的那么选择偶数的。两者没有return null）;
	 * @param bean
	 * @return
	 * @descrition
	 */
	private CheckInDSonBean calculateMinDoorInZone(CheckInDBean bean) {
		List<CheckInDSonBean> oddList = bean.getDoors();
		if (oddList != null && oddList.size() > 0) {
			return oddList.get(0);
		}
		List<CheckInDSonBean> evenList = bean.getDoorss();
		if (evenList != null && evenList.size() > 0) {
			return evenList.get(0);
		}
		return null;
	}

	private void initOper() {
		userNames.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//李君浩注释
				Intent intent = new Intent(context, ChooseUsersActivity.class);
				intent.putExtra("resoures_text_id", userNames.getId());
				intent.putExtra("resoures_hide_id", R.id.user_ids);
				intent.putExtra("selected_adids", userIds.getText().toString().trim());
				intent.putExtra("role_ids", BaseDataDepartmentKey.Warehouse);
				intent.putExtra("single_select", false);
				intent.putExtra("select_key", ChooseUsersActivity.KEY_SHUTTLE);
				intent.putExtra("proJsId", BaseDataSetUpStaffJobKey.LeadSuperVisor);
				startActivityForResult(intent, ChooseUsersActivity.requestCode);
			}
		});
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkReleaseSpot();
			}
		});

		/**
		 * 选择zone
		 */
		zoneLinearLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openSelectZone();
			}
		});

		/**
		 * 选择门
		 */
		doorLinearLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openSelectDoor();
			}
		});

		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finishActivity();
			}
		});
	}


	private void initDoorAndZoneValue(int doorFather) {
		if(fixZones == null){
			fixCheckZoneBeans() ;
		}
		for(CheckInDBean bean : fixZones ){
			if(bean.getDoorid() == doorFather){
				showZone.setText(bean.getDoor()+"");
			}
		} 
		
	}

	public void finishActivity() {
		if (isSubmitData) {
			CheckInGateJsonUtil.clearData();
			Intent intent = new Intent(this,YardControlMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);	
			finish();
			DockHelpUtil.clearData();
		} else {
			onBackBtnOrKey();
		}

	}

	@Override
	protected void onBackBtnOrKey() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				CheckInGateJsonUtil.clearData();
				CheckInShuttleReleaseSpotActivity.super.onBackBtnOrKey();
				DockHelpUtil.clearData();
			}
		});
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		finishActivity();
	}
}
