package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CheckInSBean implements Serializable{

	private String stop;
	private int stopid;
	private String License ;
	private String Trailer ;
	/**
	 * 默认是填写最小的奇数，如果没有奇数那么 填写最小的偶数。
	 * 都没有都不填
	 */
	private List<CheckInSSonBean> stops = new ArrayList<CheckInSSonBean>();		//这个是保存奇数的 在选择Area过后然后自动的填写一个值用
	private List<CheckInSSonBean> stopss = new ArrayList<CheckInSSonBean>();	//这个是保存偶数的  在选择Area过后然后自动的填写一个值用
 
	
	
	public String getLicense() {
		return License;
	}
	public void setLicense(String license) {
		License = license;
	}
	public String getTrailer() {
		return Trailer;
	}
	public void setTrailer(String trailer) {
		Trailer = trailer;
	}
	public String getStop() {
		return stop;
	}
	public void setStop(String stop) {
		this.stop = stop;
	}
	public int getStopid() {
		return stopid;
	}
	public void setStopid(int stopid) {
		this.stopid = stopid;
	}
	public List<CheckInSSonBean> getStops() {
		return stops;
	}
	public void setStops(List<CheckInSSonBean> stops) {
		this.stops = stops;
	}
	public List<CheckInSSonBean> getStopss() {
		return stopss;
	}
	public void setStopss(List<CheckInSSonBean> stopss) {
		this.stopss = stopss;
	}
	
}
