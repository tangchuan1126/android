package oso.ui.yardcontrol.shuttle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.shuttle.bean.ShuttleBean;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.DlgEditView.OnPromptListener;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.edittext.SimpleDlgAdapter;
import oso.widget.edittext.SimpleDlgAdapter.OnDlgItemListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SearchShuttleActivity extends BaseActivity {

	private SingleSelectBar singleSelectBar;
	private SearchEditText searchEt;
	private EditText sv;
	private ListView shuttleLv;
	private ShuttleAdapter adapter;

	private boolean flag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yard_shuttle_activity_layout, 0);
		setTitleString(getString(R.string.shuttle_title));
		initView();
		initSingleSelectBar();
		setData();
	}

	private void initView() {
		searchEt = (SearchEditText) findViewById(R.id.filter_edit);
		searchEt.setScanMode();
		shuttleLv = (ListView) findViewById(R.id.shuttleLv);
		shuttleLv.setEmptyView(findViewById(R.id.any_data));
		sv = (EditText) findViewById(R.id.ctnr_edit);
		sv.setTransformationMethod(new AllCapTransformationMethod());
	}

	private void setData() {
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				searchValue(value);
			}
		});
		searchEt.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				boolean flag = false;
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN || keyCode == KeyEvent.KEYCODE_ENTER) {
						String entryValue = searchEt.getText().toString();
						searchValue(entryValue);
						flag = true;
					}
				}
				return flag;
			}
		});
		// sv.setOnSearchListener(new OnSearchListener() {
		// @Override
		// public void onSearch(String value) {
		// searchValue(value);
		// }
		// });
		// sv.getInputEt().setOnKeyListener(new OnKeyListener() {
		// @Override
		// public boolean onKey(View v, int keyCode, KeyEvent event) {
		// boolean flag = false;
		// if (event.getAction() == KeyEvent.ACTION_DOWN) {
		// if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN || keyCode ==
		// KeyEvent.KEYCODE_ENTER) {
		// String entryValue = sv.getInputEt().getText().toString().trim();
		// searchValue(entryValue);
		// flag = true;
		// }
		// }
		// return flag;
		// }
		// });
		initDlgEditView();
		shuttleLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(mActivity, ShuttleSelectDoorAndSpotActivity.class);
				intent.putExtra("Shuttle", (Serializable) adapter.getItem(position));
				startActivity(intent);
			}
		});
	}

	private void initDlgEditView() {
		final SimpleDlgAdapter adapter = new SimpleDlgAdapter(mActivity, sv);

		DlgEditView.attachOnFocus(sv, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v, "Search").setMethod("SearchEquipment").setHint("Search Equipment").setDefKeyboard(true).setAdp(adapter)
						.setPromptOnClick(new OnPromptListener() {
							@Override
							public void onClick(String value) {
								searchValue(value);
							}
						});
			}
		}, false);
		adapter.setOnDlgItemListener(new OnDlgItemListener() {
			@Override
			public void onClick(String value) {
				searchValue(value);
			}
		});
	}

	/**
	 * 初始化SingleSelectBar
	 */
	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.entry_id), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.ctnr), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				switch (selectValue.b) {
				case 0:
					flag = true;
					searchEt.setHint(R.string.entry_id);
					searchEt.setVisibility(View.VISIBLE);
					sv.setVisibility(View.GONE);
					searchEt.setText("");
					((TextView) findViewById(R.id.any_data)).setText(R.string.check_in_scan_entry_id);
					break;
				case 1:
					flag = false;
					sv.setHint(R.string.ctnr);
					searchEt.setVisibility(View.GONE);
					sv.setVisibility(View.VISIBLE);
					sv.setText("");
					((TextView) findViewById(R.id.any_data)).setText(R.string.check_in_scan_ctnr);
					break;
				}
				searchEt.setText("");
				((View) findViewById(R.id.any_data)).startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_show_for_edit));
			}
		});
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	private void searchValue(final String entry_id_or_ctnr) {
		if (StringUtil.isNullOfStr(entry_id_or_ctnr)) {
			UIHelper.showToast(mActivity, getString(R.string.sync_input) + " " + (flag ? getString(R.string.sync_entryid) : getString(R.string.sync_ctnr)), Toast.LENGTH_SHORT).show();
			return;
		}
		RequestParams params = new RequestParams();
		params.add("Method", "GetDoorOrlocationByMainId");
		params.add(flag ? "dlo_id" : "ctnr", entry_id_or_ctnr);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				Gson gson = new Gson();
				JSONArray ja = StringUtil.getJsonArrayFromJson(json, "data");
				if (ja != null && !TextUtils.isEmpty(ja.toString()) && !ja.toString().equals("[]")) {
					List<ShuttleBean> list = gson.fromJson(ja.toString(), new TypeToken<List<ShuttleBean>>() {
					}.getType());
					adapter = new ShuttleAdapter(list);
					shuttleLv.setAdapter(adapter);
				} else {
					UIHelper.showToast(mActivity, getString(R.string.tms_data_empty), Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void handFinish() {
				searchEt.setText("");
				searchEt.requestFocus();
			};
		}.doGet(HttpUrlPath.CheckInAction, params, this);

	}

	class ShuttleAdapter extends BaseAdapter {

		private List<ShuttleBean> shuttleList;

		public ShuttleAdapter(List<ShuttleBean> list) {
			shuttleList = list;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public Object getItem(int arg0) {
			return shuttleList.get(arg0);
		}

		@Override
		public int getCount() {
			return shuttleList == null ? 0 : shuttleList.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			ShuttleHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(mActivity).inflate(R.layout.item_search_shuttle, null);
				holder = new ShuttleHolder();
				holder.equipment_number = (TextView) convertView.findViewById(R.id.equipment_number);
				holder.shuttle_enttry_id = (TextView) convertView.findViewById(R.id.shuttle_enttry_id);
				holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
				convertView.setTag(holder);
			} else
				holder = (ShuttleHolder) convertView.getTag();

			// 刷新-数据
			final ShuttleBean s = shuttleList.get(position);
			CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(s.getEquipment_type(), holder.imageView1);
//			if (s.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRACTOR)) {
//				holder.imageView1.setImageResource(R.drawable.ic_tractor);
//			}
//			if (s.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRAILER)) {
//				holder.imageView1.setImageResource(R.drawable.ic_trailer);
//			}
			// holder.equipment_name.setText(CheckInTractorOrTrailerTypeKey.getContainerTypeKeyValue(s.getEquipment_type())
			// + ": ");
			holder.equipment_number.setText(s.getEquipment_number() + "");
			holder.shuttle_enttry_id.setText("E" + s.getCheck_in_entry_id() + "");

			return convertView;
		}

		private class ShuttleHolder {
			TextView  equipment_number, shuttle_enttry_id;
			ImageView imageView1;
		}
	}

}
