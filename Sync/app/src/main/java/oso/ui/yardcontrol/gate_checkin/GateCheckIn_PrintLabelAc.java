package oso.ui.yardcontrol.gate_checkin;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.print.adapter.CheckinScanLoadSelectPrinterAdapter;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.CheckInBeanMain;
import support.common.bean.CheckInTaskBeanMain;
import support.common.print.OnInnerClickListener;
import support.dbhelper.StoredData;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyStatusTypeKey;
import support.key.OccupyTypeKey;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 大签
 * 
 * @author 朱成
 * @date 2015-1-4
 */
public class GateCheckIn_PrintLabelAc extends BaseActivity implements
		OnClickListener {

	private WebView web;
	private View loProgress, vDash;

	CheckInBeanMain bean = new CheckInBeanMain();

	LinearLayout addLinear;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		applyParams();
		setContentView(R.layout.lo_gatecheckin_gatelabel, 0);
		setTitleString(getString(R.string.checkin_gate_label_title));
		showRightButton(R.drawable.print_all_style, "", this);

		// 取view
		TextView tvEntry_dlg = (TextView) findViewById(R.id.tvEntry_dlg);
		TextView tvPrompt = (TextView) findViewById(R.id.tvPrompt);
		addLinear = (LinearLayout) findViewById(R.id.addLinear);
		View loPrompt = findViewById(R.id.loPrompt);
		web = (WebView) findViewById(R.id.web);
		loProgress = findViewById(R.id.loProgress);
		vDash = findViewById(R.id.vDash);
		
		//监听事件
		findViewById(R.id.btnFinish).setOnClickListener(this);

		// 初始化数据
		tvEntry_dlg.setText("E" + bean.entry_id + "");
		tvPrompt.setText(bean.prompt + "");

		loPrompt.setVisibility(TextUtils.isEmpty(bean.prompt) ? View.GONE
				: View.VISIBLE);

		addLinear();
		// 初始化web
		WebSettings ws = web.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setSupportZoom(true);
		ws.setBuiltInZoomControls(true);
		ws.setUseWideViewPort(true);
		ws.setLoadWithOverviewMode(true);
		web.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loProgress.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				UIHelper.showToast(mActivity, "Oh no! " + description,
						Toast.LENGTH_SHORT).show();
			}
		});
		String url = HttpUrlPath.basePath
				+ "check_in/check_in_open_print.html?main_id=" + bean.entry_id
				+ "&isprint=1&ps_id=" + StoredData.getPs_id() + "&adid="
				+ StoredData.getAdid();
		Utility.synCookies(mActivity, url, SimpleJSONUtil.getCookie());
		web.loadUrl(url);
		
		//debug
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.checkin_print_label), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqPrinters();
			}
		});
		
	}
	
	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		tipPrint_OnBack();
	}

	public void addLinear() {
		boolean noEquip = bean.complexDoorList == null
				|| bean.complexDoorList.size() == 0;
		// 无设备时,隐藏分割线
		vDash.setVisibility(noEquip ? View.GONE : View.VISIBLE);
		if (noEquip)
			return;
		for (int i = 0; i < bean.complexDoorList.size(); i++) {
			CheckInTaskBeanMain b = new CheckInTaskBeanMain();
			b = bean.complexDoorList.get(i);
			View view = ((LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_cheakin_main_layout, null);
			ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView1);
			TextView tractor_txt = (TextView) view.findViewById(R.id.tractor_txt);
			TextView door_txt = (TextView) view.findViewById(R.id.door_txt);

			tractor_txt.setText(b.equipment_number);
			door_txt.setText(OccupyStatusTypeKey.getContainerTypeKeyValue(b.occupy_status,mActivity) + " "+OccupyTypeKey
					.getOccupyTypeKeyName(mActivity,b.resources_type)+ ": "+ b.resources_name + "");
			if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
				imageView1.setImageResource(R.drawable.ic_tractor);
			}
			if (b.equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
				imageView1.setImageResource(R.drawable.ic_trailer);
			}
			boolean noRes = TextUtils.isEmpty(b.resources_name);
			door_txt.setVisibility(noRes ? View.GONE : View.VISIBLE);

			addLinear.addView(view);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRight: // 打印
			reqPrinters();
			break;
		case R.id.btnFinish:
			tipPrint_OnBack();
			break;

		default:
			break;
		}
	}
	
	/**
	 * 退出时提示打印
	 */
	private void tipPrint_OnBack(){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_notice));
		builder.setMessage("Have you printed Entry Label?");
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				GateCheckIn_PrintLabelAc.super.onBackBtnOrKey();
			}
		});
		builder.setNegativeButton(getString(R.string.tms_print_text), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				reqPrinters();
			}
		});
		builder.show();
	}

	private List<PrintServer> listPrinters1 = new ArrayList<PrintServer>();
	private long curPrinterId; // 当前printer

	// 请求printers
	private void reqPrinters() {
		// 若已加载,则直接显示
		if (listPrinters1.size() > 0) {
			showDlg_printers();
			return;
		}

		RequestParams params = new RequestParams();
		params.add("Method", "GetLetterPrintServer");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				listPrinters1.clear();

				// 解析
				JSONObject joData = json.optJSONObject("printserver");
				curPrinterId = joData.optLong("default");
				PrintServer.parseBeans(joData.optJSONArray("allprintserver"),
						listPrinters1);
				if (listPrinters1.size() == 0) {
					UIHelper.showToast(mActivity, getString(R.string.sync_no_printserver));
					return;
				}

				// 显示-列表
				showDlg_printers();
			}
		}.doGet(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	private CheckinScanLoadSelectPrinterAdapter adpPrinters = null;

	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			String id = listPrinters1.get(position).getPrinter_server_id();
			curPrinterId = Long.parseLong(id);
			adpPrinters.setCurItem(position);
		};
	};

	private void showDlg_printers() {
		List<PrintServer> printServerList = listPrinters1;
		if (printServerList == null || printServerList.size() == 0) {
			UIHelper.showToast(this, getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) mActivity
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver,
				null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new CheckinScanLoadSelectPrinterAdapter(mActivity,
				printServerList, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServerList.size() < 3) ? dm.heightPixels / 7
				* printServerList.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServerList.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getCurPrinter_index());
		// 初始-滑至可见
		listView.setSelection(getCurPrinter_index());
		/*****************************************************************/
 
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				this);
		builder.setTitle(getString(R.string.print_select_printer));
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(getString(R.string.tms_print_text),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (getCurPrinter_index() != -1) {
							doPrint();
							dialog.dismiss();
						} else
							UIHelper.showToast(getString(R.string.sync_select_printserver));
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	// 打印,同scanLoadActivity中打印不同
	private void doPrint() {
		RequestParams params = new RequestParams();
		params.add("Method", "GateLabelPrint");
		params.put("entry_id", bean.entry_id);
		params.put("print_server_id", curPrinterId + "");

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				UIHelper.showToast(mActivity, getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.AndroidPrintByWebAction, params, this);
	}

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getCurPrinter_index() {

		List<PrintServer> listPrinters = listPrinters1;
		if (listPrinters == null)
			return -1;

		for (int i = 0; i < listPrinters.size(); i++) {
			String tmpId = listPrinters.get(i).getPrinter_server_id();
			if ((curPrinterId + "").equals(tmpId))
				return i;
		}
		return -1;
	}

	// =========传参===============================
	public static void initParams(Intent in, String entry, String res,
			String resType, String prompt) {
		in.putExtra("entry", entry);
		in.putExtra("res", res);
		in.putExtra("resType", resType);
		in.putExtra("prompt", prompt);
		
	}

	private void applyParams() {
		bean = (CheckInBeanMain) getIntent().getSerializableExtra(
				"checkInBeanMain");
	}
}
