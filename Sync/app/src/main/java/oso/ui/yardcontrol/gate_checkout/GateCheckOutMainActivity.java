package oso.ui.yardcontrol.gate_checkout;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.yardcontrol.YardControlMainActivity;
import oso.ui.yardcontrol.gate_checkout.adapter.GCOAdapter;
import oso.ui.yardcontrol.gate_checkout.bean.GateCheckOutBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.common.UIHelper;
import support.key.FileWithCheckInClassKey;
import support.key.FileWithTypeKey;
import support.key.TTPKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class GateCheckOutMainActivity extends BaseActivity {

	private TabToPhoto ttp;
	private ListView gateLv;
	private GCOAdapter adapter;

	protected static List<GateCheckOutBean> allDatas;
	private String entry_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_gatecheckout, 0);
		initData();
		initView();
		setData();
	}

	private void initData() {
		setTitleString(getString(R.string.checkout_title));
		allDatas = (List<GateCheckOutBean>) getIntent().getSerializableExtra("scos");
		entry_id = getIntent().getStringExtra("entry_id");
	}

	private void initView() {
		gateLv = (ListView) findViewById(R.id.gateLv);
		ttp = (TabToPhoto) findViewById(R.id.ttp);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		allDatas.addAll((List<GateCheckOutBean>) intent.getSerializableExtra("showDatas"));
		adapter.notifyDataSetChanged();
	}

	private void setData() {
		setTitleString("E" + entry_id);
		adapter = new GCOAdapter(mActivity, allDatas);
		gateLv.setAdapter(adapter);
		gateLv.setEmptyView(findViewById(R.id.nodataTv));
		gateLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				GateCheckOutBean bean = (GateCheckOutBean) adapter.getItem(position);
				if (!adapter.isHideSeal(bean)) {
					showEditSealDialog(bean);
				}
			}
		});
		ttp.init(mActivity, getTabParamList());
		findViewById(R.id.okBtn).setFocusable(true);
		showRightButton(R.drawable.btn_add_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, AddTrailerActivity.class);
				intent.putExtra("entry_id", entry_id);
				startActivity(intent);
			}
		});
	}

	private void showEditSealDialog(final GateCheckOutBean item) {
		final View view = getLayoutInflater().inflate(R.layout.dialog_edit_seal, null);
		final EditText sealEt = (EditText) view.findViewById(R.id.sealEt);
		final Button noSealBtn = (Button) view.findViewById(R.id.noSealBtn);
		sealEt.setTransformationMethod(new AllCapTransformationMethod());
		sealEt.setHint(getString(R.string.checkout_seal_null));
		Utility.endEtCursor(sealEt);
		noSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sealEt.setText("NA");
				Utility.endEtCursor(sealEt);
			}
		});
		final RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
		dialog.setTitle(getString(R.string.task_item_seal));
		dialog.setContentView(view);
		dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String oldSeal = TextUtils.isEmpty(item.getNow_seal()) ? "NA" : item.getNow_seal();
				String newSeal = TextUtils.isEmpty(sealEt.getText().toString().trim()) ? "NA" : sealEt.getText().toString().trim();
				showSelectSealItemDialog(sealEt, item, oldSeal.toUpperCase(), newSeal.toUpperCase());
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void showSelectSealItemDialog(final EditText sealEt, final GateCheckOutBean item, final String oldSeal, final String newSeal) {
		if (oldSeal.equals(newSeal)) {
			item.setNewSeal(newSeal);
			adapter.notifyDataSetChanged();
			return;
		}
		final View view = getLayoutInflater().inflate(R.layout.dialog_select_seal, null);
		final LinearLayout oldSealBtn = (LinearLayout) view.findViewById(R.id.oldSealBtn);
		final LinearLayout newSealBtn = (LinearLayout) view.findViewById(R.id.newSealBtn);
		((TextView) view.findViewById(R.id.oldSealTv)).setText(oldSeal);
		((TextView) view.findViewById(R.id.newSealTv)).setText(newSeal);
		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setCancelable(false);
		builder.setTitle(getString(R.string.checkout_select_seal));
		builder.setContentView(view);
		builder.hideCancelBtn();
		oldSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				item.setNewSeal(oldSeal);
				adapter.notifyDataSetChanged();
				builder.getDialog().dismiss();
			}
		});
		newSealBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				item.setNewSeal(newSeal);
				adapter.notifyDataSetChanged();
				builder.getDialog().dismiss();
			}
		});
		builder.show();
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		String key = TTPKey.getGateCheckoutPhotoKey(entry_id, 0);
		TabParam param = new TabParam(getString(R.string.yms_gate_checkout), key, new PhotoCheckable(0, "Check Out", ttp));
		param.setWebImgsParams(FileWithCheckInClassKey.PhotoGateCheckOut + "", FileWithTypeKey.OCCUPANCY_MAIN + "", entry_id);
		params.add(param);
		return params;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data); // 在onActivityResult方法里调用
	}

	/**
	 * 提交按钮
	 */
	public void checkOutOnClick(View v) {
		// checkoutWay();
		RequestParams params = new RequestParams();
		params.put("Method", HttpPostMethod.CheckOut);
		params.put("jsonObject", changeDataToJson());
		ttp.uploadZip(params, "GateCheckOut");
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				try {
					JSONObject data = json.getJSONObject("data");
					if (data != null && data.length() > 0) {
						checkoutNotify(data);
					} else {
						checkoutWay();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handFail() {
				UIHelper.showToast(mActivity, getString(R.string.checkout_checkout_fail), Toast.LENGTH_SHORT).show();
			};
		}.doGet(HttpUrlPath.CheckInActionMutiRequest, params, mActivity);
	}

	public void checkoutWay() {
		if (!adapter.isAllHasSeal()) {
			adapter.startSealAnim();
			UIHelper.showToast(mActivity, getString(R.string.checkout_input_seal), Toast.LENGTH_SHORT).show();
			return;
		}
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle(getString(R.string.sync_notice));
		builder.setMessage(getString(R.string.checkout_finish_confirm));
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				System.out.println(changeDataToJson());
				getHttpData();
			}
		});
		builder.setNegativeButton(getString(R.string.sync_no), null);
		builder.create().show();
	}

	public void checkoutNotify(JSONObject json) {
		if (StringUtil.getJsonInt(json, "result") == 0) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(getString(R.string.sync_notice));
			builder.setMessage("Please Call: " + StringUtil.getJsonString(json, "phone_number"));
			builder.setPositiveButton(getString(R.string.yms_gate_checkout), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.out.println(changeDataToJson());
					getHttpData();
				}
			});
			builder.setNegativeButton(getString(R.string.sync_cancel), null);
			builder.create().show();
		}
		if (StringUtil.getJsonInt(json, "result") == 1) {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(getString(R.string.sync_notice));
			builder.setMessage("Driver Name: " + StringUtil.getJsonString(json, "driver_name") + "\n" + "Dispatch Date: "
					+ StringUtil.getJsonString(json, "dispatch_date") + "\n" + "Completed Date: " + StringUtil.getJsonString(json, "completed_date")
					+ "\n" + "From Wareshouse: " + StringUtil.getJsonString(json, "from_wareshouse") + "\n" + "To Warehouse: "
					+ StringUtil.getJsonString(json, "to_warehouse"));
			builder.setPositiveButton(getString(R.string.yms_gate_checkout), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					System.out.println(changeDataToJson());
					getHttpData();
				}
			});
			builder.setNegativeButton(getString(R.string.sync_cancel), null);
			builder.create().show();
		}
	}

	// private String getConfirmInfo() {
	// String str = "";
	// if (!showDatas.isEmpty()) {
	// for (GateCheckOut g : showDatas) {
	// if (!g.getIs_disabled().equals("true"))
	// str += g.getEquipment_number() + "\t→\t" + g.getNow_seal() + "\n";
	// }
	// str += "Check Out Finish?";
	// }
	// return str;
	// }

	private void getHttpData() {
		RequestParams params = new RequestParams();
		params.put("Method", HttpPostMethod.CheckOut);
		params.put("jsonObject", changeDataToJson());
		ttp.uploadZip(params, "GateCheckOut");
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
				dialog.setTitle(getString(R.string.sync_notice));
				dialog.setMessage(getString(R.string.checkout_checkout_success));
				dialog.setCancelable(false);
				dialog.hideCancelBtn();
				dialog.setPositiveButton(getString(R.string.checkout_finish), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Utility.popTo(mActivity, YardControlMainActivity.class);
					}
				});
				dialog.create().show();
			}

			@Override
			public void handFail() {
				UIHelper.showToast(mActivity, getString(R.string.checkout_checkout_fail), Toast.LENGTH_SHORT).show();
			};
		}.doGet(HttpUrlPath.CheckInActionMutiRequest, params, mActivity);
	}

	public String changeDataToJson() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("entry_id", entry_id);
			jsonObject.put("filePath", "");
			JSONArray array = new JSONArray();
			for (int i = 0, length = allDatas.size(); i < length; i++) {
				GateCheckOutBean bean = allDatas.get(i);
				JSONObject beanObject = new JSONObject();
				beanObject.put("equipment_type", bean.getEquipment_type());
				beanObject.put("equipment_id", bean.getEquipment_id());
				beanObject.put("resources_id", bean.getResources_id());
				beanObject.put("equipment_no", bean.getEquipment_number());
				beanObject.put("resources_type", bean.getResources_type());
				beanObject.put("is_android", 1);
				beanObject.put("location", bean.getLocation() + "");
				beanObject.put("isSearch", bean.getIssearch() + "");
				beanObject.put("check_in_entry_id", bean.getCheck_in_entry_id() + "");
				beanObject.put("seal", bean.getNewSeal());
				array.put(beanObject);
			}
			jsonObject.put("jsonArray", array);
			return jsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onBackBtnOrKey() {
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ttp.clearCache();
				GateCheckOutMainActivity.super.onBackBtnOrKey();
			}
		});
	}

}
