package oso.ui.yardcontrol.ctnr;

import java.util.List;

import oso.ui.yardcontrol.ctnr.bean.VerifySVBean;
import oso.ui.yardcontrol.ctnr.iface.ViewDoubtIface;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SealEditText;
import utility.StringUtil;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import declare.com.vvme.R;
/**
 * @ClassName: ReasonDialogWindow 
 * @Description: 填写原因
 * @author gcy
 * @date 2015-1-25 下午5:03:11
 */
public class ReasonDialogWindow implements OnClickListener {

	private Context context;
	private Dialog dialog;

	int scWidth = 480;
	int scHeight = 800;

//	private VerifySVBean verifySVBean;				//当前"修改"的类别(修改时才有)
	private List<VerifySVBean> baseList;
	private SealEditText note;
	
//	private BaseAdapter oThis;
	
	private ViewDoubtIface iface;
	
	public ReasonDialogWindow(Context context,List<VerifySVBean> baseList,ViewDoubtIface iface) {
		this.context = context;
//		this.oThis = oThis;
		this.iface = iface;
		initDialog(context);
	}
	
	

	private void initDialog(Context context) {
//		this.verifySVBean = verifySVBean;

		scWidth = getScreenWidth(context);
		scHeight = getScreenHeight(context);
		dialog = new Dialog(context, R.style.signDialog);
		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		window.setWindowAnimations(R.style.signAnim); // 添加动画
		dialog.setContentView(R.layout.edit_note_view_dialog);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		note = (SealEditText) dialog.findViewById(R.id.note);
		
		note.initView("Reason", "", "");

		note.endEtCursor();
		
		//初始化
		note.setEnable(true);
		
		(dialog.findViewById(R.id.submit)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			dialogForSeal();
			break;

		default:
			break;
		}
	}

	/**
	 * @Description:提交seal信息
	 * @param @param activity
	 * @param @param verifySVBean
	 */
	public void dialogForSeal(){
		//--------获取seal数据
		final String note_str = StringUtil.isNullOfStr(note.getText())?"NA":note.getText();
		
		String message = "Reason: "+note_str+"?";
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(context.getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				iface.CheckOut(note_str);
				close();
			}
		});
		builder.setNegativeButton(context.getString(R.string.sync_no), null);
		builder.create().show();
	}
	
	
	public void show() {
		// isShowing = true;
		dialog.show();
	}

	public void close() {
		if (dialog.isShowing()) {
			// isShowing = false;
			dialog.dismiss();
		}
	}

	private static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	private static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm;
	}
}
