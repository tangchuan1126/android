package oso.ui.yardcontrol.gate_checkin;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.base.PhotoCheckable;
import oso.ui.yardcontrol.YardControlMainActivity;
import oso.ui.yardcontrol.gate_checkin.adapter.AdpDlgCheckout;
import oso.ui.yardcontrol.gate_checkin.bean.Gate_EquipBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.adapter.AdpCarrier;
import oso.widget.dlgeditview.adapter.AdpDriver;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.bean.CheckInBeanMain;
import support.common.datas.HoldDoubleValue;
import support.key.CheckInLiveLoadOrDropOffKey;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * GateCheckIn简单处理页
 * 
 * @author 朱成
 * @date 2014-11-7
 */
public class GateCheckIn_SimpleAc extends BaseActivity implements
		OnClickListener {

	private TabToPhoto ttp;
	private EditText etDL, etDriverName, etLP, etCTNR, etCarrier, etMcDot,
			etDetail;
	private TextView tvCarrier_title, tvDrLic_title, drName_title,
			tvDetail_title;
	private View tabDeliveryOrPickup,vFake;
	private RadioGroup tabOther;

	private SingleSelectBar singleSelectBar;// 选项卡

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_gatecheckin_simple, 0);
		setTitleString(getString(R.string.yms_gate_checkin_title));

		// 取view
		ttp = (TabToPhoto) findViewById(R.id.ttp);
		etDL = (EditText) findViewById(R.id.etDL);
		etDriverName = (EditText) findViewById(R.id.etDriverName);
		etCarrier = (EditText) findViewById(R.id.etCarrier);
		etMcDot = (EditText) findViewById(R.id.etMcDot);

		etLP = (EditText) findViewById(R.id.etLP);
		etCTNR = (EditText) findViewById(R.id.etCTNR);
		etDetail = (EditText) findViewById(R.id.etDetail);

		tvDetail_title = (TextView) findViewById(R.id.tvDetail_title);
		tvCarrier_title = (TextView) findViewById(R.id.tvCarrier_title);
		tvDrLic_title = (TextView) findViewById(R.id.tvDrLic_title);
		drName_title = (TextView) findViewById(R.id.drName_title);

		tabDeliveryOrPickup = findViewById(R.id.tabDeliveryOrPickup);
		tabOther = (RadioGroup) findViewById(R.id.tabOther);
		vFake=findViewById(R.id.vFake);

		// 监听事件
		findViewById(R.id.btnSubmit).setOnClickListener(this);
//		etDetail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//			
//			@Override
//			public void onFocusChange(View v, boolean hasFocus) {
//				// TODO Auto-generated method stub
//				if(!hasFocus)
//					etDetail.clearFocus();
//			}
//		});

		// 初始化ttp
		ttp.init(this, getTabParamList(), false);
		initSingleSelectBar();
		updateDetail();

		tvDrLic_title.setText(Utility.getCompoundText("", getString(R.string.checkin_driver_license),
				new ForegroundColorSpan(0xff000000)));
		drName_title.setText(Utility.getCompoundText("", getString(R.string.checkin_driver_name),
				new ForegroundColorSpan(0xff000000)));
		tvCarrier_title.setText(Utility.getCompoundText("", getString(R.string.checkin_carrier),
				new ForegroundColorSpan(0xff000000)));

		// 全大写
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		etDL.setTransformationMethod(trans);
		etDriverName.setTransformationMethod(trans);
		etCarrier.setTransformationMethod(trans);
		etMcDot.setTransformationMethod(trans);
		etLP.setTransformationMethod(trans);
		etCTNR.setTransformationMethod(trans);
		etDetail.setTransformationMethod(trans);

		// etCTNR 与CTNR / BOL的联动
//		etCTNR.addTextChangedListener(new TextWatcher() {
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				// 若为dropOff,则同步
//				if (getTabIndex() == 0)
//					etDetail.setText(s.toString());
//			}
//		});

		// debug
		initCarrierTip();
		initDriverTip();
	}

	// ==================nested===============================

	private AdpDriver adpDriver;

	private void initDriverTip() {
		adpDriver = new AdpDriver(etDL, etDriverName);

		// debug
		DlgEditView.attachOnFocus(etDL, new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DlgEditView.show(mActivity, (TextView) v)
						.setMethod("SearchGateDriverLiscense").setHint("Driver License")
						.setDefKeyboard(true).setAdp(adpDriver);
			}
		});
	}
	
	private AdpCarrier adpCarrier;

	private void initCarrierTip() {
		adpCarrier = new AdpCarrier(etCarrier, etMcDot);

		// debug
		DlgEditView.attachOnFocus(etMcDot, new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				adpCarrier.isChange_carrier = false;
				DlgEditView.show(mActivity, (TextView) v)
						.setMethod("SearchMcDot").setHint("MC / DOT")
						.setAdp(adpCarrier);
			}
		});
		// debug
		DlgEditView.attachOnFocus(etCarrier, new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				adpCarrier.isChange_carrier = true;
				DlgEditView.show(mActivity, (TextView) v)
						.setMethod("SearchCarrier").setHint("Carrier")
						.setAdp(adpCarrier);
			}
		});

	}

	// =================================================

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		ttp.onActivityResult(requestCode, resultCode, data);
	}

	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar) mActivity.findViewById(R.id.bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_delivery), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_pickup), 1));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.window_type_other), 2));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.userDefineSelectIndexExcuteClick(0);

		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {

			@Override
			public void clickCallBack(
					HoldDoubleValue<String, Integer> selectValue) {
				// TODO Auto-generated method stub
				updateDetail();
			}
		});
	}

	private void updateDetail() {
		int tab = getTabIndex();
		tvDetail_title.setText(tab == 0 ? "CTNR / BOL" : "LOAD  PO  ORDER");
		// debug
		// etDetail.setText("");
//		etDetail.setText(tab == 0 ? etCTNR.getEditableText().toString() : "");

//		tabOther.check(R.id.rbDropoffs);
		tabOther.check(R.id.rbSmallParcel);
		//other
		if (tab == 2) {
			//debug
			vFake.requestFocus();
			Utility.colseInputMethod(this,vMiddle);
			
			tabOther.setVisibility(View.VISIBLE);
			tabDeliveryOrPickup.setVisibility(View.GONE);
		} else {
			tabDeliveryOrPickup.setVisibility(View.VISIBLE);
			tabOther.setVisibility(View.GONE);
		}
	}

	private int getTabIndex() {
		return singleSelectBar.getCurrentSelectItem().b;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSubmit:
			doSubmit(true,"",-1);
			
			//debug
//			showDlg_checkOutEquip();
			
			break;

		default:
			break;
		}
	};
	
	private void showDlg_checkOutEquip(final List<Gate_EquipBean> listEquips){
		
		//view
		View vContent=getLayoutInflater().inflate(R.layout.dlg_checkin_out,null);
		ListView lv_dlg=(ListView)vContent.findViewById(R.id.lv_dlg);
		lv_dlg.setAdapter(new AdpDlgCheckout(this,listEquips));
		
		int itemH=Utility.dip2px(this, 40);
		//最多显示5条
		if(listEquips.size()>5){
			LayoutParams lp=lv_dlg.getLayoutParams();
			lp.height=5*itemH;
		}
		
		//dlg
		RewriteBuilderDialog.Builder bd=new RewriteBuilderDialog.Builder(this);
		bd.setContentView(vContent);
		bd.setTitle("Check Out Existing Equips");
		bd.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				doSubmit(false,Gate_EquipBean.getEquipIds(listEquips),1);
			}
		});
		bd.setNegativeButton(getString(R.string.sync_no),  new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				doSubmit(false, "",1);
			}
		});
		bd.show();
		
	}

	private List<TabParam> getTabParamList() {
		List<TabParam> params = new ArrayList<TabParam>();
		params.add(new TabParam(getString(R.string.checkin_photo_title), "GCheckIn_0", new PhotoCheckable(0,
				"Driver", ttp)));
		return params;
	}

	private boolean checkForm() {
		if (ttp.isPhotoEmpty(true)) {
			UIHelper.showToast(this, getString(R.string.checkin_photo_null));
			return false;
		}

		String drLicense = etDL.getText().toString();
		String drName = etDriverName.getText().toString();
		String carrier = etCarrier.getText().toString();
		String lp = etLP.getText().toString();

		if (TextUtils.isEmpty(lp)) {
			UIHelper.showToast(this, getString(R.string.checkin_lp_null));
			return false;
		}
		if (TextUtils.isEmpty(drLicense)) {
			UIHelper.showToast(this, getString(R.string.checkin_license_null));
			return false;
		}
		if (TextUtils.isEmpty(drName)) {
			UIHelper.showToast(this, getString(R.string.checkin_name_null));
			return false;
		}
		if (TextUtils.isEmpty(carrier)) {
			UIHelper.showToast(this, getString(R.string.checkin_carrier_null));
			return false;
		}
		// if (TextUtils.isEmpty(carrier)) {
		// UIHelper.showToast(this, "Carrier is Empty!");
		// return false;
		// }
		return true;
	}

	private void doSubmit(boolean needvalidate,String equipIds,int creatFlag) {

		if (!checkForm())
			return;

		int bigType = getBigType();
		RequestParams params = new RequestParams();
		params.add("Method", "GateCheckIn");
		params.add("driver_name", etDriverName.getText().toString()
				.toUpperCase());
		params.add("driver_liscense", etDL.getText().toString().toUpperCase());

		params.add("company_name", etCarrier.getText().toString().toUpperCase());
		params.add("mc_dot", etMcDot.getText().toString().toUpperCase());
		// params.add("company_name",
		// window_carrier.getText().toString().toUpperCase());
		// params.add("mc_dot",
		// window_mc_dot.getText().toString().toUpperCase());
		params.add("liscense_plate", etLP.getText().toString().toUpperCase());
		params.add("gate_container_no", etCTNR.getText().toString()
				.toUpperCase());
		//最后一个不传
		if(getTabIndex()!=2)
			params.add("add_checkin_number", etDetail.getText().toString()
				.toUpperCase());
		params.add("rel_type", bigType + ""); // pickup等
		params.add("creatFlag", creatFlag+"");
		// other时,不传live 
		// if (singleSelectBar.getCurrentSelectItem().b != 2)
		// params.add("live", CheckInLiveLoadOrDropOffKey.LIVE + "");

		params.add("live", getLiveMode() + ""); // liveLoad等
		ttp.uploadZip(params, "LoadToPrintPhoto");
		
		//debug
		params.add("needvalidate",needvalidate?"1":"0");
		if(!TextUtils.isEmpty(equipIds))
			params.add("equipmentids",equipIds);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				String err_info=json.optString("err_info");
				if(err_info!=null&&err_info.length()>0){err_info(err_info);}else{
					int flag=json.optInt("flag");
					//需checkOut
					if(flag==1)
					{
						List<Gate_EquipBean> listEquips=new ArrayList<Gate_EquipBean>();
						Gate_EquipBean.parseBeans(listEquips, json.optJSONArray("datas"));
						showDlg_checkOutEquip(listEquips);
					}
					//直接过
					else{
						UIHelper.showToast(mActivity, getString(R.string.sync_success));
						Intent in = new Intent(mActivity,
								GateCheckIn_PrintLabelAc.class);
						in.putExtra("checkInBeanMain", CheckInBeanMain.handJsonForList(json));
						startActivity(in);
						finish();
						ttp.clearData();
					}
				}
			}
		}.doPost(HttpUrlPath.CheckInActionMutiRequest, params, this);

	}
	public void err_info(String err_info){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(
				mActivity);
		builder.setTitle(getString(R.string.sync_notice));
		builder.setMessage(err_info);
		builder.setPositiveButton(getString(R.string.yms_gate_checkin_text),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						doSubmit(true,"",1);
					}
				});
		builder.setNegativeButton(getString(R.string.sync_cancel), null);
		builder.create().show();
	
	}
	private int getBigType() {
		int ret = -1;
		int tab = singleSelectBar.getCurrentSelectItem().b;
		if (tab == 0)
			ret = CheckInMainDocumentsRelTypeKey.DELIVERY;
		else if (tab == 1)
			ret = CheckInMainDocumentsRelTypeKey.PICK_UP;
		else if (tab == 2) {
			int rbOther = tabOther.getCheckedRadioButtonId();
			if (rbOther == R.id.rbSmallParcel)
				ret = CheckInMainDocumentsRelTypeKey.SMALL_PARCEL;
			else if (rbOther == R.id.rbDropoffs)
				ret = CheckInMainDocumentsRelTypeKey.CTNR;
			else if (rbOther == R.id.rbVisitors)
				ret = CheckInMainDocumentsRelTypeKey.VISITOR;
			else if (rbOther == R.id.rbUnknown)
				ret = CheckInMainDocumentsRelTypeKey.NONE;
		}
		return ret;
	}

	private int getLiveMode() {

		// if(singleSelectBar.getCurrentSelectItem().b != 2)
		// return CheckInLiveLoadOrDropOffKey.LIVE;

		int rbOther = tabOther.getCheckedRadioButtonId();
		// 若为"dropOff等",则返回dropOff
		if (singleSelectBar.getCurrentSelectItem().b == 2
				&& rbOther == R.id.rbDropoffs) {
			// return CheckInLiveLoadOrDropOffKey.DROP;
			// 若无,则为pickUpCtnr
			if (TextUtils.isEmpty(etCTNR.getText()))
				return CheckInLiveLoadOrDropOffKey.PICK_UP;
			// 若有ctnr 则为dropOff
			else
				return CheckInLiveLoadOrDropOffKey.DROP;

		}
		return CheckInLiveLoadOrDropOffKey.LIVE;
	}

	// 注:1>spotName:可为Spot/Door
	private void tipEntry1(String entry, String spot, String spotName,
			String prompt) {

		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		View vContent = getLayoutInflater().inflate(
				R.layout.dlg_gatecheckin_tipentry, null);
		b.setContentView(vContent);

		// 取view
		// tvEntry_dlg tvResType_dlg tvResValue_dlg tvPrompt
		// loPrompt loRes
		TextView tvEntry_dlg = (TextView) vContent
				.findViewById(R.id.tvEntry_dlg);
		TextView tvResType_dlg = (TextView) vContent
				.findViewById(R.id.tvResType_dlg);
		TextView tvResValue_dlg = (TextView) vContent
				.findViewById(R.id.tvResValue_dlg);
		TextView tvPrompt = (TextView) vContent.findViewById(R.id.tvPrompt);
		View loPrompt = vContent.findViewById(R.id.loPrompt);
		View loRes = vContent.findViewById(R.id.loRes);

		// 初始化数据
		tvEntry_dlg.setText(entry);
		tvResType_dlg.setText(spotName + ": ");
		tvResValue_dlg.setText(spot);
		tvPrompt.setText(prompt);

		loRes.setVisibility(TextUtils.isEmpty(spot) ? View.GONE : View.VISIBLE);
		loPrompt.setVisibility(TextUtils.isEmpty(prompt) ? View.GONE
				: View.VISIBLE);

		// dlg
		b.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Utility.popTo(mActivity, YardControlMainActivity.class);
				finish();
			}
		}).hideCancelBtn().setCancelable(false).show();

	}
	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						GateCheckIn_SimpleAc.super.onBackBtnOrKey();
					}
				});
	}
}
