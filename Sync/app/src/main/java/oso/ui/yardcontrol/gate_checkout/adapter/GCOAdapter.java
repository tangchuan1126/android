package oso.ui.yardcontrol.gate_checkout.adapter;

import java.util.List;

import oso.ui.yardcontrol.gate_checkout.bean.GateCheckOutBean;
import oso.widget.dialog.RewriteBuilderDialog;
import support.key.CheckInMainDocumentsRelTypeKey;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class GCOAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<GateCheckOutBean> scos;
	private Context mContext;
	private boolean isStartAnim;

	public GCOAdapter(Context c, List<GateCheckOutBean> scos) {
		inflater = LayoutInflater.from(c);
		mContext = c;
		this.scos = scos;
	}

	@Override
	public int getCount() {
		return scos == null ? 0 : scos.size();
	}

	@Override
	public Object getItem(int position) {
		return scos.get(position);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	public void startSealAnim() {
		isStartAnim = true;
		notifyDataSetChanged();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				isStartAnim = false;
			}
		}, 500);
	}

	public boolean isAllHasSeal() {
		for (GateCheckOutBean g : scos) {
			// 不是隐藏的并且是选中的为空的Seal
			if (!isHideSeal(g) && TextUtils.isEmpty(g.getNewSeal())) {
				return false;
			}
		}
		return true;
	}

	public boolean isHideSeal(GateCheckOutBean gco) {
		return gco.getIs_disabled().equals("true") || gco.getRel_type() == CheckInMainDocumentsRelTypeKey.DELIVERY;
	}

	GateHolder holder = null;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			holder = new GateHolder();
			convertView = inflater.inflate(R.layout.item_gatecheckout_add, null);
			holder.item_main = convertView.findViewById(R.id.item_main);
			holder.equipmentTv = (TextView) convertView.findViewById(R.id.equipmentTv);
			holder.resTv = (TextView) convertView.findViewById(R.id.resTv);
			holder.delBtn = (ImageView) convertView.findViewById(R.id.delBtn);
			holder.sealTv = (TextView) convertView.findViewById(R.id.sealTv);
			holder.typeIv = (ImageView) convertView.findViewById(R.id.typeIv);
			holder.sealLayout = (LinearLayout) convertView.findViewById(R.id.sealLayout);
			holder.carrierTv = (TextView) convertView.findViewById(R.id.carrierTv);
			convertView.setTag(holder);
		} else {
			holder = (GateHolder) convertView.getTag();
		}

		final GateCheckOutBean bean = scos.get(position);

		// 背景颜色
		int bgRes = R.drawable.v5_group_single_item_bg;
		if (getCount() >= 2) {
			if (position == 0) {
				bgRes = R.drawable.v5_group_first_item_bg;
				// holder.d.setVisibility(View.VISIBLE);
			} else if (position == getCount() - 1) {
				bgRes = R.drawable.v5_group_last_item_bg;
				// holder.d.setVisibility(View.GONE);
			} else {
				bgRes = R.drawable.v5_group_middle_item_bg;
				// holder.d.setVisibility(View.VISIBLE);
			}
		} else {
			// 只有一条数据并且是车头的情况
			if (bean.getEquipment_type_msg().equals("Tractor")) {
				// bgRes = R.drawable.v5_group_single_item_bg_light;
				// holder.d.setVisibility(View.GONE);
			}
		}
		// 设备号
		holder.equipmentTv.setText(bean.getEquipment_number());
		// 资源
		holder.resTv.setText(bean.getLocation() + "");
		// carrier
		holder.carrierTv.setText(bean.getCarrier() + "");
		// Seal
		holder.sealLayout.setVisibility(isHideSeal(bean) ? View.GONE : View.VISIBLE);
		holder.sealTv.setText(bean.getNewSeal().toUpperCase());
		// 车头车尾
		if (bean.getEquipment_type_msg().equals("Tractor")) {
			holder.typeIv.setImageResource(R.drawable.ic_tractor);
			// 删除按钮
			holder.delBtn.setVisibility(View.INVISIBLE);
		} else {
			holder.typeIv.setImageResource(R.drawable.ic_trailer);
			// 删除按钮
			holder.delBtn.setVisibility(View.VISIBLE);
			holder.delBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.dialog_scale_in));
					RewriteBuilderDialog.newSimpleDialog(mContext, mContext.getString(R.string.sync_delete_notify),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									scos.remove(bean);
									notifyDataSetChanged();
								}
							}).show();
				}
			});
		}
		// 删除线
		// holder.equipmentTv.getPaint().setFlags(bean.isState() ?
		// Paint.DEV_KERN_TEXT_FLAG : Paint.STRIKE_THRU_TEXT_FLAG);

		// seal为空的控件动画
		if (holder.sealLayout.getVisibility() == View.VISIBLE && TextUtils.isEmpty(holder.sealTv.getText()) && isStartAnim) {
			Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.shake_x);
			holder.sealTv.startAnimation(anim);
		}

		holder.item_main.setBackgroundResource(bgRes);

		return convertView;
	}

	class GateHolder {
		public View item_main;
		public TextView equipmentTv;
		public TextView resTv;
		public TextView sealTv;
		public TextView carrierTv;
		public ImageView typeIv;
		public LinearLayout sealLayout;
		public ImageView delBtn;
//		public LinearLayout delLay;
		// public CheckBox box;
	}
}