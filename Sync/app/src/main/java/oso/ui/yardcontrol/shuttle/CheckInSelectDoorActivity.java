package oso.ui.yardcontrol.shuttle;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.window.bean.WindowTaskBean;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.FragSelRes;
import support.common.UIHelper;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class CheckInSelectDoorActivity extends BaseActivity {

	private List<ResourceInfo> doorlist = new ArrayList<DockHelpUtil.ResourceInfo>();
	private WindowTaskBean lastCloseUserTask;
	private FragSelRes fragRes;
	private ResourceInfo selectDoorInfo;
	public static final int REQUEST_CODE = 10000;
	public static boolean isRefreshDatas = false;
	//private ShuttleBean shuttleBean = null;
//	private int dor_tag = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_add_door_spot, 0);
		
		// 取view
		fragRes = (FragSelRes) getFragmentManager().findFragmentById(R.id.fragRes);
		doorlist = (List<ResourceInfo>) getIntent().getSerializableExtra("doorlist");
		lastCloseUserTask = (WindowTaskBean) getIntent().getSerializableExtra("LastCloseUserTask");
		Button btn_door = (Button) findViewById(R.id.btn_door);
		btn_door.setText(getString(R.string.sync_submit));
		setTitleString(getString(R.string.shuttle_select_door));
		
		String areaName = getIntent().getStringExtra("areaName");
		String resourceName = getIntent().getStringExtra("resourceName");
		// 1-A1,1-A2,1-A3,1-A4,1-LTL,1-ST1,2-ST2,3-ST3,4-ST1,4-ST2
//		if(areaName.equals("1-A1")){
//			dor_tag = 1;
//		}else if(areaName.equals("1-A2")){
//			dor_tag = 2;
//		}else if(areaName.equals("1-A3")){
//			dor_tag = 3;
//		}else if(areaName.equals("1-A4")){
//			dor_tag = 4;
//		}else if(areaName.equals("1-LTL")){
//			dor_tag = 5;
//		}else if(areaName.equals("1-ST1")){
//			dor_tag = 6;
//		}else if(areaName.equals("2-ST2")){
//			dor_tag = 7;
//		}else if(areaName.equals("3-ST")){
//			dor_tag = 8;
//		}else if(areaName.equals("4-ST1")){
//			dor_tag = 9;
//		}else if(areaName.equals("4-ST2")){
//			dor_tag = 10;
//		}
		fragRes.initData(areaName, resourceName);
		
		commondata();
	}
	private void commondata() {
		// 初始化fragRes
		if (!DockHelpUtil.isLoadData()) {
			refreshDoorList(getIntent().getStringExtra("entry_id"));
			return;
		}
				
		fragRes.init(DockHelpUtil.getStopordoor(),(lastCloseUserTask!=null)?lastCloseUserTask.resources_id:0);
	}
	public void addTaskOnClick(View v) {
		selectDoorInfo = fragRes.getSelectRes();
		if (selectDoorInfo == null) {
			UIHelper.showToast(OccupyTypeKey.getPrompt(mActivity,OccupyTypeKey.DOOR));
			return;
		}
		Intent intent = new Intent(mActivity,CheckInShuttleReleaseSpotActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("Select", selectDoorInfo);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
 	}
	
	public void refreshDoorList(String entry_id) {
		
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		params.add("request_type", "2"); // 1:可用res 2:所有
		params.add("occupy_type", OccupyTypeKey.DOOR + "");
		params.add("entry_id", entry_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (!Utility.isNullForList(doorlist)) {
					doorlist.clear();
				}
				doorlist = DockHelpUtil.handjson_big(json);
				fragRes.init(doorlist,(lastCloseUserTask!=null)?lastCloseUserTask.resources_id:0);
			}
		}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, mActivity);
	}
	@Override
	protected void onBackBtnOrKey() {
		CheckInSelectDoorActivity.super.onBackBtnOrKey();
		finish();
	}
}