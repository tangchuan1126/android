package oso.ui.yardcontrol.patrol_spot.linear;

import support.common.bean.RtEquipment;
import support.key.CheckInTractorOrTrailerTypeKey;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * @ClassName: PatrolSpotOrDockSubItem 
 * @Description: 初始化equipment 的UI界面
 * @author gcy
 * @date 2015-1-26 上午11:41:58
 */
public class PatrolSpotOrDockSubItem extends LinearLayout{
	
	 private TextView equipment_number,location ;
	 private ImageView edit_equiptment,equiptment_type;
	 private View delete_button,location_layout;
	 
	 public PatrolSpotOrDockSubItem(Context context) {  
         super(context);  
		 initDockDetail(context);
	 }  
     public PatrolSpotOrDockSubItem(Context context, AttributeSet attrs) {  
    	 super(context, attrs);  
		 initDockDetail(context);
     }
     
     private void initDockDetail(Context context){
		  LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		  View view = inflater.inflate(R.layout.patrol_rt_spot_or_dock_subitem, this);  
		  delete_button = (View) view.findViewById(R.id.delete_button);		  
		  edit_equiptment = (ImageView) view.findViewById(R.id.edit_equiptment);
		  equipment_number = (TextView) view.findViewById(R.id.equipment_number);
		  equiptment_type = (ImageView) view.findViewById(R.id.equiptment_type);
		  location_layout = (View) view.findViewById(R.id.location_layout);
		  location = (TextView) view.findViewById(R.id.location);
     }

     public void setShowValueAndLocation(RtEquipment rt){
    	 location_layout.setVisibility(View.VISIBLE);
    	 CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(rt.equipment_type, equiptment_type);
    	 equipment_number.setText(rt.equipment_number);
    	 location.setText(rt.location);
     }
     
     public void setShowValues(RtEquipment rt){
    	 CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(rt.equipment_type, equiptment_type);
    	 equipment_number.setText(rt.equipment_number);
     }
     
     public TextView getEquipmentView(){
    	 return equipment_number;
     }
     
     public View getEditView(){
    	 return edit_equiptment;
     }
     
     public View getDeleteView(){
    	 return delete_button;
     }
}
