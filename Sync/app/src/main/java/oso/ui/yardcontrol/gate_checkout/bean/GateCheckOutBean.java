package oso.ui.yardcontrol.gate_checkout.bean;

import java.io.Serializable;

public class GateCheckOutBean implements Serializable {

	private static final long serialVersionUID = 1441803125750863825L;

	public int getEquipment_id() {
		return equipment_id;
	}

	public void setEquipment_id(int equipment_id) {
		this.equipment_id = equipment_id;
	}

	public int getEquipment_type() {
		return equipment_type;
	}

	public void setEquipment_type(int equipment_type) {
		this.equipment_type = equipment_type;
	}

	public String getEquipment_number() {
		return equipment_number;
	}

	public void setEquipment_number(String equipment_number) {
		this.equipment_number = equipment_number;
	}

	public int getResources_type() {
		return resources_type;
	}

	public void setResources_type(int resources_type) {
		this.resources_type = resources_type;
	}

	public int getResources_id() {
		return resources_id;
	}

	public void setResources_id(int resources_id) {
		this.resources_id = resources_id;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public String getEquipment_type_msg() {
		return equipment_type_msg;
	}

	public void setEquipment_type_msg(String equipment_type_msg) {
		this.equipment_type_msg = equipment_type_msg;
	}

	// public String getEquipment_detail_msg() {
	// return equipment_detail_msg;
	// }
	//
	// public void setEquipment_detail_msg(String equipment_detail_msg) {
	// this.equipment_detail_msg = equipment_detail_msg;
	// }

	// public boolean isState() {
	// return isState;
	// }
	//
	// public void setState(boolean isState) {
	// this.isState = isState;
	// }

	// public static List<GateCheckOut> getAddData(JSONObject json) {
	// List<GateCheckOut> gcos = new ArrayList<GateCheckOut>();
	// JSONObject detailJo = StringUtil.getJsonObjectFromJSONObject(json,
	// "datas");
	// JSONArray ja = StringUtil.getJsonArrayFromJson(detailJo, "detail");
	// for (int i = 0, length = ja.length(); i < length; i++) {
	// JSONObject jo = StringUtil.getJsonObjectFromArray(ja, i);
	// GateCheckOut gco = new GateCheckOut();
	// gco.setCheck_in_entry_id(StringUtil.getJsonInt(jo, "check_in_entry_id"));
	// gco.setEquipment_id(StringUtil.getJsonInt(jo, "equipment_id"));
	// gco.setEquipment_number(StringUtil.getJsonString(jo,
	// "equipment_number"));
	// gco.setEquipment_type(StringUtil.getJsonInt(jo, "equipment_type"));
	// gco.setIs_disabled(StringUtil.getJsonString(jo, "is_disabled"));
	// gco.setEquipment_detail_msg(formatLocation(StringUtil.getJsonString(jo,
	// "location")));
	// gco.setResources_id(StringUtil.getJsonInt(jo, "resources_id"));
	// gco.setResources_type(StringUtil.getJsonInt(jo, "resources_type"));
	// gco.setEquipment_type_msg("Trailer");
	// gco.setIssearch(0);
	// gcos.add(gco);
	// }
	// return gcos;
	// }
	//
	// private static String formatLocation(String str) {
	// return str.length() > 4 ? str.substring(0, 4) + ":" + str.substring(4,
	// str.length()) : str;
	// }

	public int getCheck_in_entry_id() {
		return check_in_entry_id;
	}

	public void setCheck_in_entry_id(int check_in_entry_id) {
		this.check_in_entry_id = check_in_entry_id;
	}

	public int getIssearch() {
		return issearch;
	}

	public void setIssearch(int issearch) {
		this.issearch = issearch;
	}

	public String getIs_disabled() {
		return is_disabled;
	}

	public void setIs_disabled(String is_disabled) {
		this.is_disabled = is_disabled;
	}

	public String getNow_seal() {
		return now_seal;
	}

	public void setNow_seal(String now_seal) {
		this.now_seal = now_seal;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	// public static void convertLocation(List<GateCheckOutBean> list) {
	// for (GateCheckOutBean out : list) {
	// out.setLocation(out.getEquipment_detail_msg());
	// }
	// }

	public int getRel_type() {
		return rel_type;
	}

	public void setRel_type(int rel_type) {
		this.rel_type = rel_type;
	}

	public String getNewSeal() {
		return newSeal;
	}

	public void setNewSeal(String newSeal) {
		this.newSeal = newSeal;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	private int check_in_entry_id; // 101010
	// private String equipment_detail_msg; // SPOT
	private String location = "";
	private int equipment_id;// 95
	private String equipment_number;// LLLL-Y01
	private int equipment_type;// 1
	private String equipment_type_msg;// Tractor
	private int issearch;// 1
	private String is_disabled;// 1
	private int module_id;// 101010
	// private int ps_id;//100000
	private int rel_type;// 1
	private int resources_id;// 883
	private int resources_type;// 2

	private String now_seal = "";
	private String newSeal = "";
	private String carrier = "NA";
	// private boolean isState = true; //是否选中
}
