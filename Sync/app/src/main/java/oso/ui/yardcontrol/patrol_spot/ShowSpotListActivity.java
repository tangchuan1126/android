package oso.ui.yardcontrol.patrol_spot;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.yardcontrol.ctnr.ViewDoubtUtiles;
import oso.ui.yardcontrol.patrol_spot.adapter.CheckInSpotAndCheckInfoAdapter;
import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import oso.ui.yardcontrol.patrol_spot.iface.ResourceItemClickCallBack;
import oso.widget.lock.CheckPatrolTime;
import oso.widget.lock.CheckPatrolTime.OnAlarmListener;
import oso.widget.rightmenu.ChickMoreInfo;
import oso.widget.rightmenu.RightButtonForMenu;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.key.OccupyTypeKey;
import support.network.NetConnectionInterface.SyncJsonHandler;
import support.network.NetConnection_YMS;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * 显示停车位和占用信息的页面 1.每一个停车位都需要自己去查看一下。 2.如果有发现停错位置，那么点击一个停车位进入 进行修改（添加）Entry
 * 3.如果这个Spot没有Entry 但是系统记录了有Entry，那么长按一条记录 选择Yes NO Entry 4.修改或者添加完成过后，不刷新数据了。
 * 只有在切换上面的导航的时候在刷新数据。每次有数据改变都是notifyChange
 * 
 * @author zhangrui
 * @date 2014年5月26日 下午2:45:39
 * @description
 */
public class ShowSpotListActivity extends BaseActivity implements ResourceItemClickCallBack {
	private static int OPENMODIFY = 1;

	private String areaId;
	private String areaName;
	private ListView listView;

	private RightButtonForMenu moreButton;// 页面顶端 more按钮

	private CheckInSpotAndCheckInfoAdapter andCheckInfoAdapter;
	private int changedIndex = 0; // 用于刷新的时候，listView滚动到那一条记录
	private ArrayList<RtAreaResouseBean> needPartolList;
	private ArrayList<RtAreaResouseBean> haveFinishList;

	private View any_data;

	private TextView need_partol;
	private TextView have_finish;

	private final int needPartol_data = 1;
	private final int haveFinish_data = 2;
	private int showCurrentIndex = needPartol_data;

	private TextView need_finish_num;
	private TextView have_finish_num;

	private CheckPatrolTime check = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_patrol_spot_or_door_list_layout, 0);
		getFromActivityData();
		initView();
		initTab();

		check = CheckPatrolTime.getThis();
		CheckPatrolTime.setOnAlarmListener(new OnAlarmListener() {
			@Override
			public void onDone() {
				showResetDialog();
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		check.patrolFinish();
	}
	
	private void showResetDialog() {
		NetConnection_YMS.getThis().reqResetPatrol(new SyncJsonHandler(mActivity, false) {
			@Override
			public void handReponseJson(JSONObject json) {
				helpJson(json);
			}
		});
	}

	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	@SuppressWarnings("unchecked")
	protected void getFromActivityData() {
		Intent intent = this.getIntent();
		areaId = intent.getStringExtra("areaId");
		areaName = intent.getStringExtra("areaName");
		needPartolList = (ArrayList<RtAreaResouseBean>) intent.getSerializableExtra("needPatrolList");
		haveFinishList = (ArrayList<RtAreaResouseBean>) intent.getSerializableExtra("finishPatrolList");

		showCurrentIndex = judge();
	}

	/**
	 * @Description:初始化Ui主控件
	 */
	private void initView() {
		setTitleString(areaName);
		// 页面顶部更多按钮
		moreButton = new RightButtonForMenu(mActivity, btnRight, makeMoreButtonData());

		showRightButton(R.drawable.menu_btn_style, null, new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (moreButton != null && !moreButton.isShowing()) {
					moreButton.show();
				}
			}
		});

		need_partol = (TextView) findViewById(R.id.need_partol);
		have_finish = (TextView) findViewById(R.id.have_finish);

		need_finish_num = (TextView) findViewById(R.id.need_finish_num);
		have_finish_num = (TextView) findViewById(R.id.have_finish_num);

		any_data = (View) findViewById(R.id.any_data);
		listView = (ListView) findViewById(R.id.listview);
		listView.setDivider(null);
		listView.setEmptyView(any_data);

	}

	/**
	 * @Description:初始化导航按钮
	 */
	private void initTab() {
		addTab(need_partol, needPartol_data);
		addTab(have_finish, haveFinish_data);
		showTabView(showCurrentIndex);
	}

	/**
	 * @Description:增加选项并控制导航事件
	 */
	private void addTab(TextView textView, final int currentIndex) {
		textView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showTabView(currentIndex);
			}
		});
	}

	/**
	 * @Description:导航事件
	 * @param index
	 */
	private void showTabView(int index) {
		clearOtherTabColor();
		if (index == needPartol_data) {
			need_partol.setBackgroundResource(R.drawable.tab_left_style_press);
			need_partol.setTextColor(getResources().getColor(R.color.white));
		}
		if (index == haveFinish_data) {
			have_finish.setBackgroundResource(R.drawable.tab_right_style_press);
			have_finish.setTextColor(getResources().getColor(R.color.white));
		}
		showCurrentIndex = index;
		setViewData(showCurrentIndex);
		if (!Utility.isNullForList(haveFinishList)) {
			have_finish_num.setVisibility(View.VISIBLE);
			have_finish_num.setText((haveFinishList.size()) + "");
		} else {
			have_finish_num.setVisibility(View.GONE);
		}

		if (!Utility.isNullForList(needPartolList)) {
			need_finish_num.setVisibility(View.VISIBLE);
			need_finish_num.setText((needPartolList.size()) + "");
		} else {
			need_finish_num.setVisibility(View.GONE);
		}
	}

	/**
	 * @Description:清空当前导航状态
	 */
	private void clearOtherTabColor() {
		need_partol.setBackgroundResource(R.drawable.tab_left_style);
		need_partol.setTextColor(getResources().getColor(R.color.black));
		have_finish.setBackgroundResource(R.drawable.tab_right_style);
		have_finish.setTextColor(getResources().getColor(R.color.black));
	}

	private void setViewData(int flag) {
		switch (flag) {
		case needPartol_data:
			andCheckInfoAdapter = new CheckInSpotAndCheckInfoAdapter(needPartolList, mActivity, this, true);
			break;
		case haveFinish_data:
			andCheckInfoAdapter = new CheckInSpotAndCheckInfoAdapter(haveFinishList, mActivity, this, false);
			break;
		}
		listView.setAdapter(andCheckInfoAdapter);

		if (andCheckInfoAdapter == null || andCheckInfoAdapter.getCount() == 0) {
			return;
		}
		if (changedIndex > 0 && needPartolList != null && changedIndex < needPartolList.size()) {
			listView.setSelection(changedIndex);
		}
	}

	public List<ChickMoreInfo> makeMoreButtonData() {
		List<ChickMoreInfo> chickMoreInfoList = new ArrayList<ChickMoreInfo>();
		chickMoreInfoList.add(new ChickMoreInfo(getString(R.string.patrol_again), -1, new View.OnClickListener() {
			public void onClick(View v) {
				RightButtonForMenu.dismissPopupWindow(moreButton);
				againPatrolSpot();
			};
		}));

		return chickMoreInfoList;
	}

	/**
	 * @Description:处理下一个Activity关闭时的操作
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == OPENMODIFY) {
			if (resultCode == RESULT_OK) {
				changedIndex = data.getIntExtra("changedIndex", 0);
				boolean flag = data.getBooleanExtra("flag", false);
				if (flag) {
					getStopsInfo();
				}
				listView.setSelection(changedIndex);
			}
		}
	}

	@Override
	public void confirmSubmit(final RtAreaResouseBean bean, final int index) {
		if (StoredData.readResetPatrolStatus()) {
			showResetDialog();
			return;
		}
		// //--------弹出提示框 防止误操作
		// RewriteBuilderDialog.Builder builder = new
		// RewriteBuilderDialog.Builder(mActivity);
		// builder.setTitle("Prompt");
		// String str =
		// "Confirm "+OccupyTypeKey.getOccupyTypeKeyName(bean.resource_type)+" "+bean.resource_name+"?";
		// builder.setMessage(str) ;
		// builder.setPositiveButton(mActivity.getResources().getString(R.string.sync_yes),
		// new DialogInterface.OnClickListener(){
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		submitEquipment(bean, index);
		// }
		// });
		// builder.setNegativeButton(mActivity.getResources().getString(R.string.sync_no),
		// null);
		// builder.create().show();
	}

	/**
	 * @Description:提交设备号
	 * @param @param bean
	 * @param @param index
	 */
	private void submitEquipment(RtAreaResouseBean bean, int index) {
		changedIndex = index;
		RequestParams params = new RequestParams();
		params.put("Method", "confirmPatrolResource");
		params.put("patrol_type", OccupyTypeKey.SPOT + "");
		params.put("resource_id", bean.resource_id + "");
		params.put("resource_type", bean.resource_type + "");
		params.put("area_id", areaId + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// 成功过后如果还有车位显示则刷新数据
				helpJson(json);
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.CheckInPatrol, params, mActivity);
	}

	@Override
	public void cancelPatrol(final RtAreaResouseBean bean) {
		// //--------弹出提示框 防止误操作
		// RewriteBuilderDialog.Builder builder = new
		// RewriteBuilderDialog.Builder(mActivity);
		// builder.setTitle("Prompt");
		// String str =
		// "Cancel "+OccupyTypeKey.getOccupyTypeKeyName(bean.resource_type)+" "+bean.resource_name+"?";
		// builder.setMessage(str) ;
		// builder.setPositiveButton(mActivity.getResources().getString(R.string.sync_yes),
		// new DialogInterface.OnClickListener(){
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		cancelEquipment(bean);
		// }
		// });
		// builder.setNegativeButton(mActivity.getResources().getString(R.string.sync_no),
		// null);
		// builder.create().show();
	}

	/**
	 * @Description:提交设备号
	 * @param @param bean
	 * @param @param index
	 */
	private void cancelEquipment(RtAreaResouseBean bean) {
		changedIndex = 0;
		RequestParams params = new RequestParams();
		params.put("Method", "againPatrolResource");
		params.put("resource_id", bean.resource_id + "");
		params.put("resource_type", bean.resource_type + "");
		params.put("area_id", areaId + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// 成功过后如果还有车位显示则刷新数据
				helpJson(json);
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.CheckInPatrol, params, mActivity);
	}

	@Override
	public void modifyResource(RtAreaResouseBean bean, int index) {
		changedIndex = index;
		Intent intent = new Intent(this, SpotModifyActivity.class);
		intent.putExtra("rtAreaSpotBaseBean", bean);
		intent.putExtra("changedIndex", changedIndex);
		intent.putExtra("areaId", areaId);
		startActivityForResult(intent, OPENMODIFY);
	}

	/********************************************************************************************************************************************/
	private void againPatrolSpot() {
		RequestParams params = new RequestParams();
		params.put("Method", "againPatrolSpot");
		params.add("areaId", areaId + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				helpJson(json);
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.CheckInPatrol, params, mActivity);
	}

	private void helpJson(JSONObject json) {

		needPartolList = RtAreaResouseBean.handJsonNP(json);
		haveFinishList = RtAreaResouseBean.handJsonFP(json);

		showCurrentIndex = judge();

		// -------------判断所有区域是否巡逻结束
		if (RtAreaResouseBean.patrolOver(json)) {
			showTabView(showCurrentIndex);
			ViewDoubtUtiles.getDataAndFlag(mActivity);
			// 5分钟检查
			check.checkPatrol(mActivity, true);
			return;
		}

		// -------------判断返回数据是否为空
		if (Utility.isNullForList(needPartolList)) {
			UIHelper.showToast(getString(R.string.patrol_patrol_over));
			finish();
			return;
		}

		// 5分钟检查
		check.checkPatrol(mActivity, false);

		showTabView(showCurrentIndex);
	}

	private int judge() {
		if (Utility.isNullForList(needPartolList)) {
			showCurrentIndex = haveFinish_data;
		}
		if (Utility.isNullForList(haveFinishList)) {
			showCurrentIndex = needPartol_data;
		}
		return showCurrentIndex;
	}

	/**
	 * 获取选择区域的Stops
	 * @descrition
	 */
	private void getStopsInfo() {
		RequestParams params = new RequestParams();
		params.add("Method", "SpotAndCheckInMain");
		params.add("AreaId", areaId + "");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				helpJson(json);
			}

			@Override
			public void handFail() {
			}
		}.doGet(HttpUrlPath.CheckInPatrol, params, mActivity);
	}
}
