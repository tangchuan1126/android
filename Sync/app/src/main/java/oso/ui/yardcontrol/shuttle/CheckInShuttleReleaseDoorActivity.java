package oso.ui.yardcontrol.shuttle;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.YardControlMainActivity;
import oso.ui.yardcontrol.shuttle.bean.CheckInSBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSSonBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSorDBean;
import oso.ui.yardcontrol.shuttle.bean.CheckUtils;
import oso.ui.yardcontrol.shuttle.bean.ShuttleBean;
import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.bean.CheckInTaskBeanMain;
import support.dbhelper.Goable;
import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyTypeKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @ClassName: CheckInShuttleReleaseDoorActivity
 * @Description: 释放门
 * @author gcy
 * @date 2014-8-21 上午10:19:10
 */
public class CheckInShuttleReleaseDoorActivity extends BaseActivity {

	private static final int SelectSpotActivity = 102;
	private long selectSpotId; // 选择Spot回来过后的填充 子类id
	private int spotFather = -1;//父类id
	private CheckInTaskBeanMain mainbean;

	public Context context;
	private Resources resources;
	private LinearLayout selectSpotArea;
	private LinearLayout selectSpot;
	private TextView showSpot; //子类name
	private TextView showSpotArea; //父类name

	private Button submitButton;
	private TextView showEntryId;
	private TextView releaseName;
	// private int submitType ; //1需要选择spot ， 2.直接提交
	private ProgressDialog dialog;
	private List<CheckInSBean> selectSpotAreas; // 用于Builder 弹出SpotArea 的时候保存的数据
	private ShuttleBean shuttleBean = null;
	private boolean isSubmitData = false;
	private TextView equipment_txt;

	private List<ResourceInfo> doorlist = new ArrayList<DockHelpUtil.ResourceInfo>();
	private String[] freeZones;
	private ResourceInfo item;
	
	public static final String DOOR = "door";
	private CheckUtils checkUtils;
	private String areaName;
	private String resourceName;
	
	private ImageView imageView1;
	
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.yard_shuttle_door_activity_layout, 0);
		setTitleString(getString(R.string.shuttle_moveto_spot));
		this.context = this;
		Intent intent = this.getIntent();
		shuttleBean = (ShuttleBean) intent.getSerializableExtra("Shuttle");
		checkUtils = new CheckUtils();
		initView();
		initOper();
	}

	private void initView() {
		resources = mActivity.getResources();
		releaseName = (TextView) findViewById(R.id.release_name);
		showEntryId = (TextView) findViewById(R.id.show_entry_id);
		showEntryId.setEnabled(true);
		showEntryId.setFocusableInTouchMode(true);
		selectSpotArea = (LinearLayout) findViewById(R.id.select_spot_area);
		selectSpot = (LinearLayout) findViewById(R.id.select_spot);
		submitButton = (Button) findViewById(R.id.submit_button);
		showSpot = (TextView) findViewById(R.id.show_spot);
		showSpotArea = (TextView) findViewById(R.id.show_spot_area);
		showEntryId.setText(shuttleBean.getCheck_in_entry_id());
		releaseName.setText(shuttleBean.getEquipment_number() + "");
		dialog = new ProgressDialog(mActivity);
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(resources.getString(R.string.sys_loadBase_data));
		dialog.setCancelable(true);
		
		imageView1 = (ImageView) findViewById(R.id.imageView1);

		CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(shuttleBean.getEquipment_type(), imageView1);
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRACTOR)){
//			imageView1.setImageResource(R.drawable.ic_tractor);
//		}
//		if(shuttleBean.getEquipment_type().equals(CheckInTractorOrTrailerTypeKey.TRAILER)){
//			imageView1.setImageResource(R.drawable.ic_trailer);
//		}
	}

	private void initOper() {
		selectSpotArea.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openSelectSpotArea();
			}
		});
		selectSpot.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openSelectSpot();
			}
		});
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkRelaseDoor();
			}
		});
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finishActivity();
			}
		});
	}

	
	private void checkRelaseDoor(){
		if( StringUtil.convert2Inter(showSpot.getText().toString()) == 0l){
			UIHelper.showToast(mActivity, "Please , Select Spot ", Toast.LENGTH_SHORT).show();
			return ;
		}
		doRelaseDoor();
	}

	private void doRelaseDoor() {
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(resources.getString(R.string.sync_notice));
			builder.setMessage(getString(R.string.shuttle_moveto_pop)+" "+getString(R.string.shuttle_spot_text)+": "+showSpot.getText().toString()+" ?");
			builder.setCancelable(false);
			builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {	
				@Override
				public void onClick(DialogInterface dialog, int which) {
					submit();
				}
			} 
			);
			builder.create().show();
	}

	public void submit() {
		RequestParams params = new RequestParams();
		params.put("entry_id", shuttleBean.getCheck_in_entry_id() + "");
		params.put("resources_id", selectSpotId + "");
		params.put("equipment_id", shuttleBean.getEquipment_id() + "");
		params.put("Method", "MoveToSpot");
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(mActivity, getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
				CheckInGateJsonUtil.clearData();
				isSubmitData = true;
				finishActivity();
			}
		}.doPost(HttpUrlPath.AndroidShuttleAction, params, mActivity);
	}

	/**
	 * 选择停车位区域
	 * @descrition
	 */
	private void openSelectSpotArea() {
		if (!DockHelpUtil.isLoadData()) {
			getchechinstopById(mActivity, "selectSpotArea");
			return;
		}
		// 弹开一个Builder 里面是显示list集合的
		doorlist = DockHelpUtil.getStopordoor();
		freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
		openSpotAreaBuilder();
	}

	/**
	 * 第一个是人为添加的Spot area,所以应该去掉
	 * 
	 * @descrition
	 */
	private List<CheckInSBean> fixCheckSpotBeans() {
		List<CheckInSBean> returnValue = new ArrayList<CheckInSBean>();
		CheckInSorDBean checkInSorDBean = CheckInGateJsonUtil.getStopordoor();
		if (checkInSorDBean != null && checkInSorDBean.getStop() != null) {
			List<CheckInSBean> arrayList = checkInSorDBean.getStop();
			if (arrayList.size() > 0) {
				for (CheckInSBean bean : arrayList) {
					if (!bean.getStop().equals("Spot Area")) {
						returnValue.add(bean);
					}
				}
			}
		}
		selectSpotAreas = returnValue;
		return returnValue;
	}

	/**
	 * 弹出Spot area 选择
	 * @descrition
	 */
	private void openSpotAreaBuilder() {
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(mActivity);
		b.setTitle(getString(R.string.shuttle_select_area));
		b.setItems(freeZones, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showSpotArea.setText(freeZones[position]);
				
			}
		});
		b.show();
		
	}
	
	/**
	 * 计算最小的一个值（首先是选择奇数的，奇数没有的那么选择偶数的。两者没有return null）;
	 * @param bean
	 * @return
	 * @descrition
	 */
	private CheckInSSonBean calculateMinSpotInArea(CheckInSBean bean) {
		List<CheckInSSonBean> oddList = bean.getStops();
		if (oddList != null && oddList.size() > 0) {
			return oddList.get(0);
		}
		List<CheckInSSonBean> evenList = bean.getStopss();
		if (evenList != null && evenList.size() > 0) {
			return evenList.get(0);
		}
		return null;
	}

	private String getSpotAreaNameById(int id) {
		if (selectSpotAreas == null) {
			fixCheckSpotBeans();
		}
		for (CheckInSBean bean : selectSpotAreas) {
			if (bean.getStopid() == id) {
				return bean.getStop();
			}
		}
		return "";
	}
	
	/**
	 * 选择停车位
	 * @descrition
	 */
	private void openSelectSpot() {
		String areaNameText = showSpotArea.getText().toString();
		String resourceNameText = showSpot.getText().toString();
		
		Intent intent = new Intent(mActivity, CheckInSelectParkActivity.class);
		if(areaNameText != null && resourceNameText != null){
			areaName = areaNameText;
			resourceName = resourceNameText;
			intent.putExtra("entry_id", getIntent().getStringExtra("entry_id"));
			intent.putExtra("areaName", areaName);
			intent.putExtra("resourceName", resourceName);
		}else{
			intent.putExtra("entry_id", getIntent().getStringExtra("entry_id"));
		}
		startActivityForResult(intent, SelectSpotActivity);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SelectSpotActivity) {
			if (resultCode == Activity.RESULT_OK) {
				item = (ResourceInfo) data.getSerializableExtra("Select");
				if (item != null) {
					// 父类
					areaName = item.area_name+"";
					// 子类
					resourceName = item.resource_name+"";
					selectSpotId = item.resource_id;
					showSpotArea.setText(areaName);
					showSpot.setText(resourceName);
					storageInfo(areaName,resourceName);
					
				}
			}
		}
	}
	// 将数据添加到内存
	private void storageInfo(String areaName,String resourceName) {
		checkUtils.putString(mActivity, areaName, resourceName);
	}

	public void getchechinstopById(Context context, final String selectWhat) {
		Goable.initGoable(context);
		final RequestParams params = new RequestParams();
		StringUtil.setLoginInfoParams(params); 
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		params.add("request_type", 2 + ""); // 1:可用res 2:所有
		params.add("occupy_type", OccupyTypeKey.SPOT + "");
		params.add("entry_id", shuttleBean.getCheck_in_entry_id() + "");
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				doorlist = DockHelpUtil.handjson_big(json);
				freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
				openSpotAreaBuilder();
			}
		}.doPost(HttpUrlPath.AndroidDockCheckInAction, params, context);
		
	}

	public void finishActivity() {
		if (isSubmitData) {
			CheckInGateJsonUtil.clearData();
			Intent intent = new Intent(this, YardControlMainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			DockHelpUtil.clearData();
		} else {
			onBackBtnOrKey();
		}
	}

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit),new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						CheckInGateJsonUtil.clearData();
						CheckInShuttleReleaseDoorActivity.super.onBackBtnOrKey();
						DockHelpUtil.clearData();
					}
				});
	}

	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		finishActivity();
	}
}
