package oso.ui.yardcontrol.gate_checkout;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.yardcontrol.gate_checkout.bean.GateCheckOutBean;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SearchGateCheckOutActivity extends BaseActivity {

	private SearchEditText searchEt;// 条目输入框

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gate_checkout_layout, 0);
		initData();
		initView();
	}

	private void initData() {
		setTitleString(getString(R.string.checkout_title));
	}

	private void initView() {
		searchEt = (SearchEditText) findViewById(R.id.filter_edit);
		searchEt.setScanMode();
		// 文本框回车事件
		searchEt.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					if (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_ENTER) {
						getHttpData(searchEt.getText().toString());
						return true;
					}
				}
				return false;
			}
		});
		// 文本框右侧点击事件
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				getHttpData(value);
			}
		});
	}

	private void getHttpData(final String value) {
		Utility.colseInputMethod(mActivity, searchEt);
		searchEt.setText("");
		searchEt.requestFocus();
		if (TextUtils.isEmpty(value)) {
			UIHelper.showToast(mActivity, getString(R.string.check_in_scan_entry_id), Toast.LENGTH_SHORT).show();
			return;
		}
		RequestParams params = new RequestParams();
		params.put("Method", HttpPostMethod.GetCheckOutData);
		params.put("entry_id", value);
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("GetCheckOutData= " + json.toString());
				Gson gson = new Gson();
				JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "datas");
				List<GateCheckOutBean> scos = gson.fromJson(dataArray.toString(), new TypeToken<List<GateCheckOutBean>>() {
				}.getType());
//				GateCheckOutBean.convertLocation(scos);
				Intent intent = new Intent(mActivity, GateCheckOutMainActivity.class);
				intent.putExtra("scos", (Serializable) scos);
				intent.putExtra("entry_id", value);
				startActivity(intent);
			}
		}.doGet(HttpUrlPath.CheckInAction, params, mActivity);
	}
}
