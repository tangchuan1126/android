package oso.ui.yardcontrol.gate_checkin.bean;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.key.CheckInTractorOrTrailerTypeKey;
import support.key.OccupyTypeKey;
import android.content.Context;
import android.text.TextUtils;

public class Gate_EquipBean {
	
//	ENTRY_ID EQUIPMENT_NUMBER/EQUIPMENT_TYPE RESOURCES_TYPE/RESOURCES_NAME/RESOURCES_ID
	
	public String ENTRY_ID;
	public int EQUIPMENT_ID;
	public String EQUIPMENT_NUMBER;
	public int EQUIPMENT_TYPE;
	public int RESOURCES_TYPE;
	public String RESOURCES_NAME;
	
	public String RESOURCES_ID;
	
	//---临时数据----
	public boolean isFirstOne;
	
	public boolean isTractor(){
		return CheckInTractorOrTrailerTypeKey.TRACTOR==EQUIPMENT_TYPE;
	}
	
	public String getRes_str(Context c) {
		return OccupyTypeKey.getOccupyTypeKeyName(c,RESOURCES_TYPE);
	}
	
	public boolean hasRes(){
		return !TextUtils.isEmpty(RESOURCES_NAME);
	}
	
	
	//==================static===============================
	
	/**
	 * 获取"设备ids",逗号分割
	 * @param list
	 * @return
	 */
	public static String getEquipIds(List<Gate_EquipBean> list){
		if(list==null)
			return "";
		
		StringBuilder ret = new StringBuilder();
		int len = list.size();
		for (int i = 0; i < len; i++)
			ret.append(list.get(i).EQUIPMENT_ID + ",");

		String retStr = ret.toString();
		// 移除-最后的","
		if (!TextUtils.isEmpty(retStr))
			retStr = retStr.substring(0, retStr.length() - 1);
		return retStr;
	}
	

	public static void parseBeans(List<Gate_EquipBean> list,JSONArray ja){
		if(ja==null)
			return;
		
		//解析-entry
		for (int i = 0; i < ja.length(); i++) {
			JSONArray jaEquips=ja.optJSONObject(i).optJSONArray("values");
			if(jaEquips==null)
				return;
			//解析-equip
			for (int j = 0; j <jaEquips.length(); j++) {
				Gate_EquipBean equipB=new Gate_EquipBean();
				JSONObject joEquip=jaEquips.optJSONObject(j);
				equipB.ENTRY_ID=joEquip.optString("ENTRY_ID");
				equipB.EQUIPMENT_ID=joEquip.optInt("EQUIPMENT_ID");
				equipB.EQUIPMENT_NUMBER=joEquip.optString("EQUIPMENT_NUMBER");
				equipB.EQUIPMENT_TYPE=joEquip.optInt("EQUIPMENT_TYPE");
				equipB.RESOURCES_TYPE=joEquip.optInt("RESOURCES_TYPE");
				equipB.RESOURCES_NAME=joEquip.optString("RESOURCES_NAME");
				
				equipB.RESOURCES_ID=joEquip.optString("RESOURCES_ID");
				
				//首个
				equipB.isFirstOne=j==0;
				list.add(equipB);
			}
		}
	}
	
	
}
