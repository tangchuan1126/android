package oso.ui.yardcontrol.shuttle.bean;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 将数据存储到内存中
 * @author Administrator
 *
 */
public class CheckUtils {

	public static final String MOVE_TO_DOOR = "moveToDoor";
	private static SharedPreferences mSharedPreferences;
	
	/**
	 * 将数据存储到内存中
	 * @param context	上下文
	 * @param key	存储数据的标识
	 * @param value	存储的数据
	 */
	public static void putString(Context context, String key, String value){
		if(mSharedPreferences == null){
			mSharedPreferences = context.getSharedPreferences(MOVE_TO_DOOR, Context.MODE_PRIVATE);
		}
		mSharedPreferences.edit().putString(key, value).commit();
	}
	
	/**
	 * 获取内存中的数据
	 * @param context	上下文
	 * @param key	获取数据的标识
	 * @param value	获取数据不存在时，默认获取的数据
	 */
	public static String getString(Context context, String key, String defValue){
		if(mSharedPreferences == null){
			mSharedPreferences = context.getSharedPreferences(MOVE_TO_DOOR, Context.MODE_PRIVATE);
		}
		return mSharedPreferences.getString(key, defValue);
	}
	
	
	/**
	 * 判断是否是第一次进入，存储boolean类型的数据
	 * @param context	上下文
	 * @param key	存储数据的标识
	 * @param defValue	存储的数据
	 * @return
	 */
	public static void putBoolean(Context context, String key, boolean value){
		if(mSharedPreferences == null){
			mSharedPreferences = context.getSharedPreferences(MOVE_TO_DOOR, Context.MODE_PRIVATE);
		}
		mSharedPreferences.edit().putBoolean(key, value).commit();
	}
	
	/**
	 * 判断是否是第一次进入，获取boolean类型的数据
	 * @param context 	上下文
	 * @param key	获取数据的标识
	 * @param defValue	获取数据不存在时，默认获取的数据
	 * @return
	 */
	public static boolean getBoolean(Context context, String key, boolean defValue){
		if(mSharedPreferences == null){
			mSharedPreferences = context.getSharedPreferences(MOVE_TO_DOOR, Context.MODE_PRIVATE);
		}
		return mSharedPreferences.getBoolean(key, defValue);
	}
}
