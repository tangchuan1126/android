package oso.ui.yardcontrol.shuttle;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.yardcontrol.shuttle.bean.CheckInDBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInDSonBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSSonBean;
import oso.ui.yardcontrol.shuttle.bean.CheckInSorDBean;
import utility.StringUtil;
import android.content.Context;

public class CheckInGateJsonUtil {
	Context context;
//	private static AsyncHttpClient client = new AsyncHttpClient();
	private static List<CheckInSBean> liststrstop;//可用停车位
//	private static List<CheckInSBean> liststrstopno;//占用停车位
	String stopname;
	static int stopnameid;
	private static List<CheckInDBean> liststrdoor;//可用门
//	private static List<CheckInDBean> liststrdoorno;//占用门
	String doorname;
	static int doornameid;
	private static List<CheckInDSonBean> doorodd;//可用门奇数信息
	private static List<CheckInDSonBean> dooreven;//可用门偶数信息
	private static List<CheckInDSonBean> alldoors;//所有可用的门
	private static List<CheckInDSonBean> allliststrdoorss;//zone下所有可用的门
	private static List<CheckInSSonBean> allliststrparks;//其余所有可用停车位
	private static List<CheckInSSonBean> stopodd;//可用停车位奇数信息
	private static List<CheckInSSonBean> stopeven;//可用停车位偶数信息
	private static List<CheckInSSonBean> allstops;//所有可用停车位
	private static CheckInSorDBean stopordoor;
 
	public static List<CheckInSBean> getListstrstop() {
		return liststrstop;
	}
	public static List<CheckInSSonBean> getAllliststrstops() {
		return allstops;
	}
	public static List<CheckInDBean> getListstrdoor() {
		return liststrdoor;
	}
	public static List<CheckInDSonBean> getAllliststrdoors() {
		return alldoors;
	}
	public static CheckInSorDBean getStopordoor() {
		return stopordoor;
	}
	public  static CheckInSorDBean stopjson(JSONObject result) throws JSONException {
		
		if (result != null) {
			stopordoor = new CheckInSorDBean();
			liststrstop = new ArrayList<CheckInSBean>();
			liststrdoor = new ArrayList<CheckInDBean>();
			allstops = new ArrayList<CheckInSSonBean>();
			alldoors = new ArrayList<CheckInDSonBean>();

				JSONArray jsonstop = StringUtil.getJsonArrayFromJson(result, "spot");
				for (int i = 0; jsonstop != null && i < jsonstop.length(); i++) {
					JSONObject jsonobjectstop = (JSONObject) jsonstop.get(i);
					CheckInSBean sbean = new CheckInSBean();
					sbean.setStop(StringUtil.getJsonString(jsonobjectstop,"spotzonename"));
					sbean.setStopid(StringUtil.getJsonInt(jsonobjectstop,"spotzoneid"));

					JSONArray jsonstops = StringUtil.getJsonArrayFromJson(jsonobjectstop, "spots");

					stopodd = new ArrayList<CheckInSSonBean>();
					stopeven = new ArrayList<CheckInSSonBean>();
					allliststrparks = new ArrayList<CheckInSSonBean>();
					if (jsonobjectstop.getString("spots") != null&& (jsonobjectstop.getString("spots")).indexOf("null") < 0){
						for (int is = 0; is < jsonstops.length(); is++) {
							JSONObject jsonobjectstops = (JSONObject) jsonstops.get(is);
							CheckInSSonBean ssonbean = new CheckInSSonBean();

							ssonbean.setSonstop(StringUtil.getJsonInt(jsonobjectstops, "spotname"));
							ssonbean.setSonstopid(StringUtil.getJsonInt(jsonobjectstops, "spotid"));
							ssonbean.setFatherstopid(StringUtil.getJsonInt(jsonobjectstop,"spotzoneid"));
							if (StringUtil.getJsonInt(jsonobjectstops, "spotname") % 2 != 0) {
								stopodd.add(ssonbean);
							} else if (StringUtil.getJsonInt(jsonobjectstops,"spotname") % 2 == 0) {
								stopeven.add(ssonbean);
							}
							allliststrparks.add(ssonbean);
							allstops.add(ssonbean);
						}
						
					}
					sbean.setStopss(allliststrparks);
					if (stopodd.size() > 0) {
						sbean.setStops(stopodd);
					} else {
						sbean.setStops(stopeven);
					}
					liststrstop.add(sbean);
				}
				CheckInSBean addcheckstop = new CheckInSBean();
				addcheckstop.setStop("Spot Area");
				liststrstop.add(0,addcheckstop);
				stopordoor.setStop(liststrstop);
				stopordoor.setStopson(allstops);
				JSONArray jsondoor = StringUtil.getJsonArrayFromJson(result, "door");
				for (int i = 0; jsondoor != null && i < jsondoor.length(); i++) {
					JSONObject jsonobjectdoor = (JSONObject) jsondoor.get(i);
					CheckInDBean dbean = new CheckInDBean();
					dbean.setDoor(StringUtil.getJsonString(jsonobjectdoor,"doorzonename"));
					dbean.setDoorid(StringUtil.getJsonInt(jsonobjectdoor,"doorzoneid"));

					JSONArray jsondoors = StringUtil.getJsonArrayFromJson(jsonobjectdoor, "doors");
					doorodd = new ArrayList<CheckInDSonBean>();
					dooreven = new ArrayList<CheckInDSonBean>();
					allliststrdoorss = new ArrayList<CheckInDSonBean>();
					if (jsondoors != null&& jsondoors.length() > 0) {
						for (int is = 0; is < jsondoors.length(); is++) {
							JSONObject jsonobjectdoors = (JSONObject) jsondoors.get(is);
							CheckInDSonBean dsonbean = new CheckInDSonBean();

							dsonbean.setSondoor(StringUtil.getJsonInt(jsonobjectdoors, "doorname"));
							dsonbean.setSondoorid(StringUtil.getJsonInt(jsonobjectdoors, "doorid"));
							dsonbean.setFatherdoorid(StringUtil.getJsonInt(jsonobjectdoor,"doorzoneid"));
							if (StringUtil.getJsonInt(jsonobjectdoors,"doorname") % 2 != 0) {
								doorodd.add(dsonbean);
							} else if (StringUtil.getJsonInt(jsonobjectdoors,"doorname") % 2 == 0) {
								dooreven.add(dsonbean);
							}
							allliststrdoorss.add(dsonbean);
							alldoors.add(dsonbean);
						}
					}else{
						liststrdoor.add(dbean);
						continue;
					}
					dbean.setDoorss(allliststrdoorss);
					if (doorodd.size() > 0) {
						dbean.setDoors(doorodd);
					} else {
						dbean.setDoors(dooreven);
					}
					liststrdoor.add(dbean);
				}
				
				CheckInDBean addcheckdoor = new CheckInDBean();
				addcheckdoor.setDoor("ALL Zone");
				liststrdoor.add(0, addcheckdoor);
				stopordoor.setDoor(liststrdoor);
				stopordoor.setDoorson(alldoors);

		}
		return stopordoor;

	}
	public static void clearData(){
		stopordoor = null ;
	}
	/**
	 * ture 表示下载成功
	 * false 表示下载失败
	 * @return
	 */
	public static boolean isLoadData(){
		return stopordoor != null ;
	}
}
