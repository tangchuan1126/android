package oso.ui.yardcontrol.shuttle.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * zone
 * @author zhangrui
 * @date 2014年5月21日 下午4:01:56
 * @description
 */
public class CheckInDBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1884691079243132205L;
	private String door;
	private int doorid;
	private String License ;
	private String Trailer ;
	private List<CheckInDSonBean> doors = new ArrayList<CheckInDSonBean>();	//door
	private List<CheckInDSonBean> doorss = new ArrayList<CheckInDSonBean>();

	
	public String getLicense() {
		return License;
	}
	public void setLicense(String license) {
		License = license;
	}
	public String getTrailer() {
		return Trailer;
	}
	public void setTrailer(String trailer) {
		Trailer = trailer;
	}
	public String getDoor() {
		return door;
	}
	public void setDoor(String door) {
		this.door = door;
	}
	public int getDoorid() {
		return doorid;
	}
	public void setDoorid(int doorid) {
		this.doorid = doorid;
	}
	public List<CheckInDSonBean> getDoors() {
		return doors;
	}
	public void setDoors(List<CheckInDSonBean> doors) {
		this.doors = doors;
	}
	public List<CheckInDSonBean> getDoorss() {
		return doorss;
	}
	public void setDoorss(List<CheckInDSonBean> doorss) {
		this.doorss = doorss;
	}

	
}
