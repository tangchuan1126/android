package oso.ui.yardcontrol.patrol_spot.linear;

import support.common.bean.RtEquipment;
import support.key.CheckInTractorOrTrailerTypeKey;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class PatrolSpotOrDockModifyItem extends LinearLayout{
	
	 private TextView equipment_number,location ;
	 private ImageView equiptment_type;
	 private View delete_button,location_layout;
	 private View check_layout;
	 private CheckBox chose_checkbox;
	 
	 
	 public PatrolSpotOrDockModifyItem(Context context) {  
         super(context);  
		 initDockDetail(context);
	 }  
     public PatrolSpotOrDockModifyItem(Context context, AttributeSet attrs) {  
    	 super(context, attrs);  
		 initDockDetail(context);
     }
     
     private void initDockDetail(Context context){
		  LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		  View view = inflater.inflate(R.layout.patrol_rt_spot_or_dock_modify_item, this);  
		  delete_button = (View) view.findViewById(R.id.delete_button);		  
		  equipment_number = (TextView) view.findViewById(R.id.equipment_number);
		  equiptment_type = (ImageView) view.findViewById(R.id.equiptment_type);
		  location_layout = (View) view.findViewById(R.id.location_layout);
		  location = (TextView) view.findViewById(R.id.location);
		  chose_checkbox = (CheckBox) view.findViewById(R.id.chose_checkbox);
		  check_layout = (View) view.findViewById(R.id.check_layout);
     }

     public void setShowValueAndLocation(final RtEquipment rt){
    	 location_layout.setVisibility(View.VISIBLE);
    	 CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(rt.equipment_type, equiptment_type);
    	 equipment_number.setText(rt.equipment_number);
    	 location.setText(rt.location);
    	 chose_checkbox.setChecked(rt.checkflag);
    	 check_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rt.checkflag = !rt.checkflag;
				chose_checkbox.setChecked(rt.checkflag);
			}
		});
     }
     
     public void setShowValues(RtEquipment rt){
    	 CheckInTractorOrTrailerTypeKey.setImageViewBgdBig(rt.equipment_type, equiptment_type);
    	 equipment_number.setText(rt.equipment_number);
     }
     
     public View getView(){
    	 return delete_button;
     }
}
