package oso.ui.inventory.cyclecount.model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.widget.photo.bean.TTPImgBean;
import utility.Utility;

/**
 * @ClassName: CCT_ProductBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-8 下午10:07:01
 */
public class CCT_ProductBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 2095244998052836114L;

	public String P_NAME;
	public String P_CODE;
//	public String LOT_NUMBER;//": "12522265",
	public int TITLE_ID;
	public long PC_ID;//": 1092966,
	public int UNION_FLAG;
	public int LOCKED_QUANTITY;
	public String PRODUCT_LINE;
	public int TOTAL_QUANTITY;
	public int TOTAL_LOCKED_QUANTITY;
	
	public int QUANTITY;//": 5,
	public List<String> CATALOGS;
	public List<String> SN = new ArrayList<String>();
	
	public int IS_HAS_SN;//CCT_P_HAS_SN = 1;  CCT_P_NO_SN = 2;
	
	public boolean IS_NEW = false;
	
	public Map<String, Integer> SN_Map = new HashMap<String, Integer>();
	public Map<String, Boolean> NEW_SN_Map = new HashMap<String, Boolean>();//新的sn 非单据内SN
	public Map<String, Boolean> SN_Damaged_Map = new HashMap<String, Boolean>();
	//---------针对有SN的进行拍照
	public Map<String, List<TTPImgBean>> SN_photos = new HashMap<String, List<TTPImgBean>>();

	//---------没有SN的需要拍照
	public List<TTPImgBean> photos = new ArrayList<TTPImgBean>();
	
	public int statusLP = CCT_Container_StatusKey.UNSCANNED;//判断托盘的状态   
	
	public boolean is_opreation = false;//判断是否操作过
	
	// 盲盘部分的上传图片
	public List<TTPImgBean> blindPhotos = new ArrayList<TTPImgBean>();
	public String modelNo;
	public String lotNo;
	public String comparedId;  // 用于比较的IDpc_name +TLP.con_id;
	
//	
//	//猜想
//	public int TITLE_ID;
//	public String UNION_FLAG;
//	public int LOCKED_QUANTITY;
//	public int PRODUCT_LINE;
//	public int TOTAL_LOCKED_QUANTITY;
//	
	// blind scan debug start
	public boolean isDamaged = false;
	public String SN_BLIND_SINGLE;
	public int TOTAL;
	public int DAMAGEDCNT;
	public boolean itemIsSeleted; // 扫描如果发现有重复，则item显示为选中状态，默认为false
	// end
	
	/**
	 * 判断当前product是否是SN类型的，false则为model/Lot No 类型的
	 * @return
	 */
	public boolean isSNPdt() {
		return !TextUtils.isEmpty(SN_BLIND_SINGLE);
	}

	public static void helpJson(JSONObject json, List<CCT_ProductBean> list){
		JSONArray jArray = json.optJSONArray("PRODUCTS");
		CCT_ProductBean.resolveJson(jArray, list);
	}

	public static void resolveJson(JSONArray jArray,List<CCT_ProductBean> list){
		
		list= new Gson().fromJson(jArray.toString(), new TypeToken<List<CCT_ProductBean>>() {}.getType());
		
	}
	
	/**
	 * @Description:获取
	 * @param @return
	 */
	public static JSONObject getDebugStr(){
		JSONObject json = new JSONObject();
		
		Random r = new Random();
		try {
			int product = r.nextInt(10)+1;
			JSONArray jProductArray = new JSONArray();
			for (int k = 0; k < product; k++) {
				JSONObject jProduct_Type = new JSONObject();
//				jProduct_Type.put("PNAME",k+"00100286/TXCHB2"+k);
				jProduct_Type.put("SKU",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				jProduct_Type.put("LOT_NUMBER",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				jProduct_Type.put("PC_ID",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				int products = r.nextInt(10)+5;
				jProduct_Type.put("QUANTITY", products);
				
				List<String> sn = new ArrayList<String>();
				sn.add(r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				sn.add(r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				JSONArray mArray = new JSONArray(sn);
				jProduct_Type.put("SN",mArray);
				
				jProductArray.put(jProduct_Type);
			}
			json.put("PRODUCTS", jProductArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * 清除信息
	 */
	public void clearInfo() {
		this.DAMAGEDCNT = 0;
		this.TOTAL = 0;
		this.blindPhotos.clear();
		this.isDamaged = false;
	}

	/**
	 * 获得Product已经上传过的图片的file_ids
	 * @return
	 */
	public String getReqDelPhotosIds() {
		StringBuilder sb = new StringBuilder();
		List<TTPImgBean> photosList = blindPhotos;
		for(int i=0; i<photosList.size(); i++) {
			if(i==photosList.size()-1) {
				sb.append(photosList.get(i).file_id);
			} else {
				sb.append(photosList.get(i).file_id+",");
			}
		}
		return sb.toString();
	}

	/**
	 * 获得当前CLP TYPE下的所有CLP是否都是新数据[New]
	 * @description 若CLP TYPE下无数据，则返回false
	 * @return
	 */
	public boolean isAllNew() {
		if (Utility.isNullForList(SN)||Utility.isNullForMap(NEW_SN_Map)) {
			return false;
		}

		if(SN.size()!=NEW_SN_Map.size()){
			return false;
		}

		int new_sn_num = 0;
		for (String sn : SN) {
			if (NEW_SN_Map.get(sn)!=null&&NEW_SN_Map.get(sn)) {
				new_sn_num++;
			}
		}
		return new_sn_num==SN.size();
	}
}
