package oso.ui.inventory.cyclecount;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_GetCreateTLP_Info;
import oso.ui.inventory.cyclecount.model.CCT_PackagingType;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.ui.inventory.cyclecount.util.SyncTextDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.datas.HoldThreeValue;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.AllCapTransformationMethod;

/**
 * @author gcy
 * @ClassName: CCT_VerifyTask
 * @Description: 对照盘点
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_CreateTLP extends BaseActivity {

    private EditText et_rfid, et_lot;
    private SyncTextDialog btn_packaging_type, btn_customer, btn_title;
    private Button btn_submit, btn_print;
    private ImageButton btn_add;

    private CCT_GetCreateTLP_Info bean;
    private String create_code;

    private NetConnection_CCT conn;
    public static final int resultCode = 49;

    private CCT_TLP_Bean tlp_bean;
    private boolean isDamaged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cct_create_tlp, 0);
        getFromActivityData(getIntent());
    }

    /**
     * @Description:接收来自于上一个activity所传递过来的数据
     */
    private void getFromActivityData(Intent intent) {
        bean = (CCT_GetCreateTLP_Info) intent.getSerializableExtra("CCT_GetCreateTLP_Info");
        create_code = intent.getStringExtra("create_code");

        initView();
    }

    /**
     * @Description:初始化View
     */
    private void initView() {
        conn = NetConnection_CCT.getThis();
        setTitleString(getString(R.string.cct_create_tlp));

        //初始化表单信息
        et_rfid = (EditText) findViewById(R.id.et_rfid);
        et_lot = (EditText) findViewById(R.id.et_lot);
        btn_packaging_type = (SyncTextDialog) findViewById(R.id.btn_packaging_type);
        btn_title = (SyncTextDialog) findViewById(R.id.btn_title);
        btn_customer = (SyncTextDialog) findViewById(R.id.btn_customer);

        // 全大写
        TransformationMethod trans = AllCapTransformationMethod.getThis();
        et_rfid.setTransformationMethod(trans);
        et_lot.setTransformationMethod(trans);

        //设置TAG方便做校验
        et_rfid.setTag(getString(R.string.cct_packaging_btn_packaging_type));
        et_lot.setTag(getString(R.string.cct_packaging_et_rfid));
        btn_packaging_type.setTag(getString(R.string.cct_packaging_btn_packaging_type));


        btn_title.setTag(getString(R.string.cct_packaging_btn_title));
        btn_title.setShowSearchView(true);

        btn_customer.setTag(getString(R.string.cct_packaging_btn_customer));
        btn_customer.setShowSearchView(true);
        //提交按钮

        setSelPackagingType(-1);
        setSelBtnTitle();
        setBtnCustomer();


        btn_add = (ImageButton) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CCT_CreatePackagingType.toThis(mActivity);
            }
        });

        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(CCT_Util.checkTextViewValue(mActivity,btn_packaging_type, btn_title)){
                submitData();
            }
            }
        });

        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPrint();
            }
        });
    }

    private void setSelPackagingType(int index) {
        List<HoldThreeValue<String, String, Object>> titleList = new ArrayList<HoldThreeValue<String, String, Object>>();//title
        for (int i = 0; i < bean.packaging_types.size(); i++) {
            titleList.add(new HoldThreeValue<String, String, Object>(bean.packaging_types.get(i).type_id + "", bean.packaging_types.get(i).type_name, bean.packaging_types.get(i)));
        }
        btn_packaging_type.setData(titleList, index, getString(R.string.cct_packaging_btn_packaging_type));
    }

    private void setSelBtnTitle() {
        List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
        for (int i = 0; i < bean.titles.size(); i++) {
            titleList.add(SyncTextDialog.getHoldDoubleValue(bean.titles.get(i)));
        }
        btn_title.setDoubleData(titleList, getString(R.string.cct_packaging_btn_title));
    }

    private void setBtnCustomer() {
        List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
        for (int i = 0; i < bean.customers.size(); i++) {
            titleList.add(SyncTextDialog.getHoldDoubleValue(bean.customers.get(i)));
        }
        btn_customer.setDoubleData(titleList, getString(R.string.cct_packaging_btn_customer));
    }

    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey() {
        RewriteBuilderDialog.newSimpleDialog(mActivity, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("tlp", tlp_bean);
                setResult(resultCode, data);
                finish();
                overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            }
        }).show();
    }

    public void toGoActivity(Context packageContext, Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(packageContext, cls);
        startActivity(intent);
        overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_in);
    }

    /**
     * 进入创建tlp的页面前 需要请求下来一些基础数据
     * @param c
     * @param create_code
     */
    public static void toThis(final Context c, final String create_code) {
        NetConnection_CCT conn = new NetConnection_CCT();
        conn.CCT_GetCreateTLPInfo(new NetConnectionInterface.SyncJsonHandler(c) {
            @Override
            public void handReponseJson(JSONObject json) {
                CCT_GetCreateTLP_Info bean = new Gson().fromJson(json.toString(), new TypeToken<CCT_GetCreateTLP_Info>() {
                }.getType());

                Intent intent = new Intent(c, CCT_CreateTLP.class);
                intent.putExtra("create_code", create_code);
                intent.putExtra("CCT_GetCreateTLP_Info",  bean);
                ((Activity) c).startActivityForResult(intent, resultCode);
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json);
            }
        });
    }

    /**
     * 提交方法
     */
    private void submitData() {
        String string = getJsonData();
        if (TextUtils.isEmpty(string)) {
            UIHelper.showToast(getString(R.string.sync_data_error));
            return;
        }
        conn.CCT_Addtlpcontext(new NetConnectionInterface.SyncJsonHandler(mActivity) {
            @Override
            public void handReponseJson(JSONObject json) {
                UIHelper.showToast(getString(R.string.sync_success));
                parsingData(json.optJSONObject("lp"));
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json, CCT_Util.ERR_MESSAGE);
            }
        }, string);
    }


    /**
     * 创建TLP后 保存tlp json  并且隐藏提交按钮 显示打印按钮
     * @param json
     */
    private void parsingData(JSONObject json){
        tlp_bean = new CCT_TLP_Bean();
        tlp_bean.CON_ID = json.optLong("con_id");
        tlp_bean.CONTAINER = json.optString("container");
        tlp_bean.REAL_NUMBER = CCT_Util.fixLP_Number(json.optString("container"));
        tlp_bean.CONTAINER_TYPE = json.optInt("container_type");
        tlp_bean.IS_FULL = json.optInt("is_full");
        tlp_bean.LOT_NUMBER = json.optString("lot_number");
        tlp_bean.TYPE_ID = json.optInt("type_id");
        tlp_bean.TITLE_ID = json.optInt("title_id");

        btn_submit.setVisibility(View.GONE);
        btn_print.setVisibility(View.VISIBLE);

        RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
        dialog.setMessage(getString(R.string.sync_create_success) + "," + getString(R.string.sync_print) + "?");
        dialog.setCancelable(false);
        dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doPrint();
            }
        });
        dialog.setNegativeButton(getString(R.string.cancle_alert_dialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("tlp", tlp_bean);
                setResult(resultCode, data);
                finish();
                overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            }
        });
        dialog.create().show();

    }

    /**
     * 获取需要提交的数据
     *
     * @return
     */
    private String getJsonData() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("container", TextUtils.isEmpty(create_code) ? "" : create_code.toUpperCase(Locale.ENGLISH));
            jsonObject.put("container_type", Cycle_Count_Task_Key.CCT_TLP);
            jsonObject.put("type_id", btn_packaging_type.key);
            jsonObject.put("lot_number", TextUtils.isEmpty(et_lot.getText().toString()) ? "" : et_lot.getText().toString());
            jsonObject.put("customer_id", TextUtils.isEmpty(btn_customer.key)?0:btn_customer.key);
            jsonObject.put("title_id", btn_title.key);
            jsonObject.put("hardwareId", TextUtils.isEmpty(et_rfid.getText().toString()) ? "" : et_rfid.getText().toString());
            jsonObject.put("is_full", "2");//1  容器上没有商品 2容器上商品不满 3容器上商品已满
            jsonObject.put("status", "2");//1:open 2closed  表示的如果为1那么 创建的Clp是可以修改的
            jsonObject.put("ps_id", StoredData.getPs_id());
//            jsonObject.put("location_name",sBean.slc_id);
//            jsonObject.put("location_type",sBean.slc_type);
//            jsonObject.put("is_damage",isDamaged?1:0); //0:normal 1:damage
        } catch (Exception e) {
            e.printStackTrace();
            UIHelper.showToast(getString(R.string.sync_data_error));
            return null;
        }
        return jsonObject.toString();
    }

    private PrintTool printTool = new PrintTool(this);
    PrintTool.OnPrintLs lsAll = new PrintTool.OnPrintLs() {
        @Override
        public void onPrint(long printServerID) {
            String cfgIDs = tlp_bean.CON_ID+"";
            if(TextUtils.isEmpty(cfgIDs)){
                UIHelper.showToast(getString(R.string.sync_success));
                return;
            }

            conn.recPrintLP_p(new NetConnectionInterface.SyncJsonHandler(mActivity) {
                @Override
                public void handReponseJson(JSONObject json) {
                    UIHelper.showToast(getString(R.string.sync_success));
                    Intent data = new Intent();
                    data.putExtra("tlp", tlp_bean);
                    setResult(resultCode, data);
                    finish();
                    overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
                }
            }, printServerID, Cycle_Count_Task_Key.CCT_CLP, tlp_bean.TYPE_ID, cfgIDs);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CCT_CreatePackagingType.resultCode&&data!=null) {
            CCT_PackagingType packaging_type = (CCT_PackagingType) data.getSerializableExtra("packaging_type");
            if (packaging_type != null) {
                bean.packaging_types.add(0, packaging_type);
                setSelPackagingType(0);
            }
        }

    }

    /**
     * 获取打印提示
     */
    private void doPrint(){
        printTool.setIface(new PrintTool.UseMySelfWay() {
            @Override
            public void getPrintServer() {
                conn.getPrintServerList(new NetConnectionInterface.SyncJsonHandler(mActivity) {
                    @Override
                    public void handReponseJson(JSONObject json) {
                        printTool.jsonToShow(json, lsAll, "");
                    }
                });
            }
        });
        printTool.showDlg_printers(lsAll);
    }
}
