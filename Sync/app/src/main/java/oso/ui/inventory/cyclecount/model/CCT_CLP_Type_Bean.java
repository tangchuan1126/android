package oso.ui.inventory.cyclecount.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import utility.Utility;
/**
 * @ClassName: CCT_CLP_Type_Bean 
 * @Description: 
 * @author gcy
 * @date 2015-4-8 下午10:04:36
 */
public class CCT_CLP_Type_Bean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 6787000398549138710L;
	

	public long CONTAINER_TYPE_ID;//": 1254,  该字段 盲盘在使用
	
	public String LP_TYPE_ID;//": "3x5x2",
	public String LP_TYPE_NAME;//": "3x5x2",
//	public String LOT_NUMBER;//": "789456",
	public String MODEL_NUMBER;//": "123658",  SKU
	public String P_NAME;//": "123658",  SKU
	public int TOTAL_CONTAINERS;
	public List<CCT_CLP_Bean> CONTAINERS = new ArrayList<CCT_CLP_Bean>();//": [
	
	
	
	
	// Blind
	public int containerType;
	// Blind end


	/**
	 * 获得当前CLP TYPE下的所有CLP是否都是新数据[New]
	 * @description 若CLP TYPE下无数据，则返回false
	 * @return
	 */
	public boolean isAllNew() {
		if (Utility.isNullForList(CONTAINERS)) {
			return false;
		}

		boolean isAllNew = true;
		for (CCT_CLP_Bean clpBean : CONTAINERS) {
			if (!clpBean.isNewData) {
				isAllNew = false;
				return isAllNew;
			}
		}
		return isAllNew;
	}

	public static void helpJson(JSONObject json, List<CCT_CLP_Type_Bean> list){
		JSONArray jArray = json.optJSONArray("CONTAINER_TYPES");
		CCT_CLP_Type_Bean.resolveJson(jArray,list);
	}

	public static void resolveJson(JSONArray jArray,List<CCT_CLP_Type_Bean> list){
		
		list= new Gson().fromJson(jArray.toString(), new TypeToken<List<CCT_CLP_Type_Bean>>() {}.getType());
		
	}
	
	/**
	 * @Description:获取
	 * @param @return
	 */
	public static JSONObject getDebugStr(){
		JSONObject json = new JSONObject();
		
		Random r = new Random();
		try {
			int clp_types = r.nextInt(10)+1;			
			JSONArray jCLP_TypesArray = new JSONArray();
			for (int k = 0; k < clp_types; k++) {
				JSONObject jCLP_Type = new JSONObject();
				jCLP_Type.put("CONTAINER_TYPE_ID","1254");
				jCLP_Type.put("DIMENSIONS",r.nextInt(10)+"x"+r.nextInt(10)+"x"+r.nextInt(10));
				jCLP_Type.put("LOT_NUMBER", r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				jCLP_Type.put("SKU", r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
				int clp = r.nextInt(10)+1;
				jCLP_Type.put("TOTAL_CONTAINERS", clp);
				JSONArray jCLPArray = new JSONArray();
				for (int i = 0; i < clp; i++) {
					JSONObject jCLP = new JSONObject();
					jCLP.put("CONTAINER_TYPE",Cycle_Count_Task_Key.CCT_CLP);
					String clpStr = "1"+i+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10);
					jCLP.put("CONTAINER_ID",clpStr);
					jCLP.put("CONTAINER", "CLP"+clpStr);
					jCLPArray.put(jCLP);
				}
				jCLP_Type.put("CONTAINERS", jCLPArray);
				jCLP_TypesArray.put(jCLP_Type);
			}
			json.put("CONTAINER_TYPES", jCLP_TypesArray);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
}
