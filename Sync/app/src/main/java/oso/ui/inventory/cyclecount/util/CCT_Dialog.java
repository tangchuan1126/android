package oso.ui.inventory.cyclecount.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.ui.debug.product.CreateProductActivity;
import oso.ui.inventory.cyclecount.CCT_CreateCLP;
import oso.ui.inventory.cyclecount.CCT_CreateTLP;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_GetCreateTLP_Info;
import oso.ui.inventory.cyclecount.model.CCT_Product;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.AllCapTransformationMethod;
import utility.StringUtil;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: CCT_Dialog
 * @Description:盘点的各种弹出工具
 * @date 2015-4-28 下午8:45:42
 */
public class CCT_Dialog {


    public static final int requestCode_Sku = 0xf11112;//用于扫描sku但是没有发现的时候创建的产品
    public static final int requestCode_CreateProduct = 0xf11113;//用于单纯的右上角点击创建产品的值
    public static final int requestCode_CreateCLP= 0xf11114;//用于单纯的右上角点击创建产品的值

    /**
     * @Description:右上角的操作弹出框
     */
    public static void showRightDialog(final Activity activity,final ScanLocationDataBean sBean,final CCT_TLP_Bean tlp) {
        final RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(activity);
        String prompt[] = new String[]{"	Create CLP", "	Create Product"};

        b.setNegativeButton(activity.getString(R.string.sync_cancel), null);
        b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0://该容器类别下更改为已扫描状态
//                        selectLP_Type(activity,"",sBean,lp_id,false,false);
                        scanProduct(activity, "", sBean, tlp);
                        break;
                    case 1://该容器类别下更改为已扫描状态
                        CreateProductActivity.toThis(activity, false,requestCode_CreateProduct);
                        break;
                    default:
                        break;
                }
                b.dismiss();
            }
        });
        b.show();
    }

    /**
     * location中弹出创建TLP 或者 CLP的弹出框默认没有提示语
     * @param activity 当前的activiy
     * @param code 需要创建tlp/clp/product的code
     * @param sBean local的基础信息bean
     */
    public static void selectLP_Type(final Activity activity,String code,final ScanLocationDataBean sBean) {
        selectLP_Type(activity, code, sBean, null, true, false);
    }

    /**
     * tlp中弹出创建TLP 或者 CLP的弹出框默认没有提示语
     * @param activity 当前的activiy
     * @param code 需要创建tlp/clp/product的code
     * @param tlp 如果是location中则默认传空也就是0  如果是在tlp中则需要把tlp的id传进来
     * @param sBean local的基础信息bean
     */
    public static void selectLP_Type(final Activity activity,String code,final ScanLocationDataBean sBean,CCT_TLP_Bean tlp) {
        selectLP_Type(activity, code, sBean,tlp, true, false);
    }

    /**
     * 弹出创建TLP 或者 CLP的弹出框
     * @param activity 当前的activiy
     * @param code 需要创建tlp/clp/product的code
     * @param tlp 如果是location中则默认传空也就是0  如果是在tlp中则需要把tlp的id传进来
     * @param sBean local的基础信息bean
     * @param showTLP 是否显示创建tlp
     * @param isShowNotFoundMsg 是否显示未找到的提示信息
     */
    public static void selectLP_Type(final Activity activity,String code,final ScanLocationDataBean sBean,final CCT_TLP_Bean tlp,boolean showTLP,boolean isShowNotFoundMsg) {

        if(TextUtils.isEmpty(code)){
            code = "";
        }

        final String value = code;

        final RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(activity);
        if(isShowNotFoundMsg){
            b.setMessage("LP is not found in system, do you want to create it?");
        }
        //定义提示语
        String c_tlp = "	Create TLP";
        final String c_clp = "	Create CLP";

        String str[] = null;
        //判断是否显示创建tlp
        if(showTLP){
            str = new String[]{c_tlp,c_clp};
        }else{
            str = new String[]{c_clp};
        }
        final String prompt[] = str;

        b.setNegativeButton(activity.getString(R.string.sync_cancel), null);
        b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //如果创建clp 则应该先扫描 pro
                if (prompt[position].equals(c_clp)) {
                    scanProduct(activity, value, sBean, tlp);
                } else {
                    CCT_CreateTLP.toThis(activity, value);
                }
                b.dismiss();
            }
        });
        b.show();
    }

    private static void scanProduct(final Activity activity,final String code, final ScanLocationDataBean sBean, final CCT_TLP_Bean tlp) {

        final BottomDialog bottomDialog = new BottomDialog(activity);
//		// view
        View v = activity.getLayoutInflater().inflate(R.layout.cct_scan_pro, null);

        final View sku_layout = v.findViewById(R.id.sku_layout);
        final SearchEditText search_model_edit = (SearchEditText) v.findViewById(R.id.search_model_edit);
        final SearchEditText search_lot_edit = (SearchEditText) v.findViewById(R.id.search_lot_edit);

        search_model_edit.setIconMode(SearchEditText.IconMode_Scan);
        search_lot_edit.setIconMode(SearchEditText.IconMode_Scan);

        // 全大写
        TransformationMethod trans = AllCapTransformationMethod.getThis();
        search_model_edit.setTransformationMethod(trans);
        search_lot_edit.setTransformationMethod(trans);

        //----设置key事件防止会车的时候触发Submit方法
        search_model_edit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
                    //-----如果为空则锁定当前焦点 如果不为空则焦点下移
                    String value = search_model_edit.getText().toString();
                    //----------判断是否是procode
                    if (!StringUtil.isNullOfStr(value)) {
                        search_model_edit.requestFocus();
                    }
                    Utility.colseInputMethod(activity, search_model_edit);
                }
                return false;
            }
        });

        search_model_edit.setSeacherMethod(new SeacherMethod() {
            @Override
            public void seacher(String value) {
                if (StringUtil.isNullOfStr(value)) {
                    UIHelper.showToast(activity.getString(R.string.cct_prompt_modelno));
                    return;
                }
                getSKU_And_Submit(activity, value, search_lot_edit.getText().toString(), code, sBean, tlp);
                bottomDialog.dismiss();
                //--------清空并锁定焦点
                search_model_edit.setText("");
                search_lot_edit.setText("");
                search_model_edit.requestFocus();
                Utility.colseInputMethod(activity, search_model_edit);
            }
        });
        search_lot_edit.setSeacherMethod(new SeacherMethod() {
            @Override
            public void seacher(String value) {
//                if (StringUtil.isNullOfStr(search_lot_edit.getText().toString())) {
//                    UIHelper.showToast(activity.getString(R.string.cct_prompt_lotno));
//                    return;
//                }
                if (StringUtil.isNullOfStr(search_model_edit.getText().toString())) {
                    UIHelper.showToast(activity.getString(R.string.cct_prompt_modelno));
                    search_lot_edit.clearFocus();
                    search_model_edit.requestFocus();
                    return;
                }
                getSKU_And_Submit(activity, search_model_edit.getText().toString(), value, code, sBean, tlp);
                bottomDialog.dismiss();
                //--------清空并锁定焦点
                search_model_edit.setText("");
                search_lot_edit.setText("");
                search_lot_edit.requestFocus();
                Utility.colseInputMethod(activity, search_lot_edit);
            }
        });

        bottomDialog.setTitle("Please Scan Label")
                .setView(v)
                .setCanceledOnTouchOutside(false)
                .setShowCancelBtn(true)
                .setOnSubmitClickListener(new BottomDialog.OnSubmitClickListener() {
                    @Override
                    public void onSubmitClick(BottomDialog dlg, String value) {
                        String modelStr = search_model_edit.getText().toString();
                        if (StringUtil.isNullOfStr(modelStr)) {
                            UIHelper.showToast(activity.getString(R.string.cct_prompt_modelno));
                            Utility.colseInputMethod(activity, search_model_edit);
                            return;
                        }
                        String lotStr = search_lot_edit.getText().toString();
                        getSKU_And_Submit(activity, modelStr, lotStr, code, sBean,tlp);
                        dlg.dismiss();
                    }
                })
                .show();

    }

   /*
    * @Description:通过model no 和 lot no来获取产品信息
    * @param @param modelNo
    * @param @param lotNo
    */
    private static void getSKU_And_Submit(Activity activity,String modelNo,String lotNo,String code,ScanLocationDataBean sBean,CCT_TLP_Bean tlp) {
        if(StringUtil.isNullOfStr(modelNo)){
            modelNo = "";
        }

        String procode = modelNo;
        if(!StringUtil.isNullOfStr(lotNo)){
            procode = modelNo +"/"+ lotNo;
        }

        submitSKU(activity, procode, code, sBean, tlp);
    }

    private static void submitSKU(final Activity activity,final String procode,final String code,final ScanLocationDataBean sBean, final CCT_TLP_Bean tlp) {
        submitSKU(activity, procode.toUpperCase(Locale.ENGLISH), code.toUpperCase(Locale.ENGLISH), sBean, tlp, "", null);
    }

    /**
     * @Description:通过产品的code 来获取产品的相关信息
     */
    private static void submitSKU(final Activity activity,final String procode,final String code,final ScanLocationDataBean sBean, final CCT_TLP_Bean tlp,final String title_id,final CCT_DialogInterface iface) {
        final NetConnection_CCT conn = NetConnection_CCT.getThis();
        long lp_id = 0;
        if(tlp!=null){
            lp_id = tlp.CON_ID;
        }

        conn.CCT_GetProductByCode(new NetConnectionInterface.SyncJsonHandler(activity) {
            @Override
            public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
                return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
            }

            @Override
            public void handReponseJson(JSONObject json) {
                JSONObject object = json.optJSONObject("DATA");
                if (object != null) {
                    CCT_Product product = new CCT_Product();
                    product.p_name = object.optString("P_NAME");
                    product.main_code = object.optString("P_CODE");
                    product.pc_id = object.optLong("PC_ID", 0);
                    if (iface != null) {
                        iface.doSomething(json);
                    } else {
                        CCT_CreateCLP.toThis(activity, product, code, sBean, tlp);
                    }
                } else {
                    UIHelper.showToast(activity.getString(R.string.sync_data_error));
                }
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {
                if (json != null) {
                    int msgErr = json.optInt("status", 0);
                    if (msgErr == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
                        UIHelper.showToast(activity.getString(R.string.sync_not_found));
                        RewriteBuilderDialog.showSimpleDialog(activity, "Create Product?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                CreateProductActivity.toThis(activity, false, requestCode_CreateCLP, procode, CCT_Util.getModelNo(procode));
                            }
                        });
                    } else {
                        JSONArray jArray = json.optJSONArray("errors");
                        if (!StringUtil.isNullForJSONArray(jArray)) {
                            JSONObject jObj = jArray.optJSONObject(0);
                            //特定错误 如果为37则提示选择title 也就是重新弹出界面选择title
                            int SELECT_TITLE = 37;
                            if (jObj.optInt("errorCode", 0) == SELECT_TITLE) {
                                selTitleDialog(activity, procode, code, sBean, tlp, null);
//                            UIHelper.showToast("Developing");
                            } else {
                                conn.handFail(json);
                            }
                        }
                    }
                }
            }
        }, procode, lp_id, title_id);
    }

    /**
     * 选择title 的dialog
     */
    public static void selTitleDialog(final Activity activity,final String procode,final String code,final ScanLocationDataBean sBean, final CCT_TLP_Bean tlp, final CCT_DialogInterface iface){
        NetConnection_CCT conn = new NetConnection_CCT();
        conn.CCT_GetCreateTLPInfo(new NetConnectionInterface.SyncJsonHandler(activity) {
            @Override
            public void handReponseJson(JSONObject json) {
                CCT_GetCreateTLP_Info bean = new Gson().fromJson(json.toString(), new TypeToken<CCT_GetCreateTLP_Info>() {
                }.getType());

                final SyncTextDialog s = new SyncTextDialog(activity);
                s.setTag(activity.getString(R.string.cct_packaging_btn_title));
                s.setShowSearchView(true);

                List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
                for (int i = 0; i < bean.titles.size(); i++) {
                    titleList.add(SyncTextDialog.getHoldDoubleValue(bean.titles.get(i)));
                }
                s.setDoubleData(titleList, activity.getString(R.string.cct_packaging_btn_title));
                s.showSingleDialog();
                s.setIface(new SyncTextDialog.SyncTextDialogInterface() {
                    @Override
                    public void doSomething() {
                        if(!TextUtils.isEmpty(s.key)){
                            submitSKU(activity, procode, code, sBean, tlp, s.key,iface);
                        }
                    }
                });
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json);
            }
        });



//        LinearLayout layout = new LinearLayout(activity);
//        layout.setOrientation(LinearLayout.HORIZONTAL);
//        layout.setGravity(Gravity.CENTER_VERTICAL);
//        TextView textView = new TextView(activity);
//        textView.setText("Title: ");
//
//        SyncTextDialog textDialog = new SyncTextDialog(activity);
//        textDialog.setTag(activity.getString(R.string.cct_packaging_btn_title));
//        textDialog.setShowSearchView(true);
////        List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
////        for (int i = 0; i < bean.titles.size(); i++) {
////            titleList.add(SyncTextDialog.getHoldDoubleValue(bean.titles.get(i)));
////        }
////        btn_title.setDoubleData(titleList, getString(R.string.cct_packaging_btn_title));
//        layout.addView(textView);
//        layout.addView(textDialog);

    }

    /**
     * 选择title 的dialog
     */
    public static void selTLP_TitleDialog(final Activity activity, final CCT_TLP_DialogInterface iface){
        NetConnection_CCT conn = new NetConnection_CCT();
        conn.CCT_GetCreateTLPInfo(new NetConnectionInterface.SyncJsonHandler(activity) {
            @Override
            public void handReponseJson(JSONObject json) {
                CCT_GetCreateTLP_Info bean = new Gson().fromJson(json.toString(), new TypeToken<CCT_GetCreateTLP_Info>() {
                }.getType());

                final SyncTextDialog s = new SyncTextDialog(activity);
                s.setTag(activity.getString(R.string.cct_packaging_btn_title));
                s.setShowSearchView(true);

                List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
                for (int i = 0; i < bean.titles.size(); i++) {
                    titleList.add(SyncTextDialog.getHoldDoubleValue(bean.titles.get(i)));
                }
                s.setDoubleData(titleList, activity.getString(R.string.cct_packaging_btn_title));
                s.showSingleDialog();
                s.setIface(new SyncTextDialog.SyncTextDialogInterface() {
                    @Override
                    public void doSomething() {
                        if(!TextUtils.isEmpty(s.key)){
                            iface.doSomething(s.key);
                        }
                    }
                });
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json);
            }
        });
    }

    public interface CCT_DialogInterface {
        void doSomething(JSONObject json);
    }
    public interface CCT_TLP_DialogInterface {
        void doSomething(String key);
    }
}
