package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_GetCreateLPTypeInfo implements Serializable{

    private static final long serialVersionUID = 5653781591193539272L;

    public CCT_Product product;
    public List<CCT_PackagingType> packaging_types;
    public List<CCT_InnerClp> inner_clps;

}
