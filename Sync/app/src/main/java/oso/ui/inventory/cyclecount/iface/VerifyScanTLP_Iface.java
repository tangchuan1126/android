package oso.ui.inventory.cyclecount.iface;

import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;


public interface VerifyScanTLP_Iface {
	/**
	 * @Description:用于弹出提示框选择操作
	 * @param @param is 只能传2个参数 第一个是父级别的索引,第二个是子级别的索引
	 */
	public void dialogCondition(int...is);
	public void editTotal(int groupPosition,String sku);
	
	public void changeCLP_Scanned(int groupPosition,int childPosition);
	public void changeSN_Scanned(int groupPosition,int childPosition);
	
	public void showCLP_Dlg_photos(String sku,CCT_CLP_Bean clp);
	public void showProNoSn_Dlg_photos(String sku,CCT_ProductBean pro);
	public void showProSn_Dlg_photos(String sku,CCT_ProductBean pro,String SN);
}
