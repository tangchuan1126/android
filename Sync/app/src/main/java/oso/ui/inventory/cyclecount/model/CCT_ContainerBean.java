package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.util.CCT_JudgeIcon;
import utility.Utility;
/**
 * @ClassName: CCT_TLP_Bean 
 * @Description: 用于得到各个类型托盘的集合
 * @author gcy
 * @date 2015-4-8 下午10:04:49
 */
public class CCT_ContainerBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -6943001302118586493L;
		
	//------托盘类型 值为 Cycle_Count_Task_Key.TLP/ Cycle_Count_Task_Key.CLP/ Cycle_Count_Task_Key.CCT_PRODUCT
	public int containerType;
	public int statusLP = CCT_Container_StatusKey.UNSCANNED;//判断托盘的状态   
	
	//------根据containerType 类型来判断是 CLP的类别集合 还是 TLP托盘
	public CCT_TLP_Bean tlp;
	public CCT_CLP_Type_Bean clp_type;
	public CCT_ProductBean product_type;
	
	public static List<CCT_ContainerBean> getContainerList(ScanLocationDataBean sBean){
		List<CCT_ContainerBean> list = new ArrayList<CCT_ContainerBean>();
		//------判断是TLP
		if(!Utility.isNullForList(sBean.tlp_containers)){
			for (int i = 0; i < sBean.tlp_containers.size(); i++) {
				CCT_ContainerBean c = new CCT_ContainerBean();
				c.containerType = Cycle_Count_Task_Key.CCT_TLP;
				c.tlp = sBean.tlp_containers.get(i);
				list.add(c);
			}
		}
		//------判断是CLP_TYPE
		if(!Utility.isNullForList(sBean.container_types)){
			for (int i = 0; i < sBean.container_types.size(); i++) {
				CCT_ContainerBean c = new CCT_ContainerBean();
				c.containerType = Cycle_Count_Task_Key.CCT_CLP;
				c.clp_type = sBean.container_types.get(i);
				list.add(c);
			}
		}
		return list;
	}
	
	public static List<CCT_ContainerBean> getContainerList(CCT_TLP_Bean	 tlp){
		List<CCT_ContainerBean> list = new ArrayList<CCT_ContainerBean>();

		//------判断是CLP_TYPE
		if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
			for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
				CCT_ContainerBean c = new CCT_ContainerBean();
				c.containerType = Cycle_Count_Task_Key.CCT_CLP;
				c.clp_type = tlp.CONTAINER_TYPES.get(i);
				CCT_JudgeIcon.judgeIcon(c);
//				if(!Utility.isNullForList(tlp.CONTAINER_TYPES.get(i).CONTAINERS)){
//					int scanNum = 0;
//					int missNum = 0;
//					for (int j = 0; j < tlp.CONTAINER_TYPES.get(i).CONTAINERS.size(); j++) {
//						if(tlp.CONTAINER_TYPES.get(i).CONTAINERS.get(j).statusLP!=CCT_Container_StatusKey.UNSCANNED){
//							scanNum++;
//						}
//						if(tlp.CONTAINER_TYPES.get(i).CONTAINERS.get(j).statusLP==CCT_Container_StatusKey.MISSING){
//							missNum++;
//						}
//					}
//					if(scanNum == tlp.CONTAINER_TYPES.get(i).CONTAINERS.size()){
//						c.statusLP = CCT_Container_StatusKey.SCANNED;
//					}
//					if(missNum>0){
//						c.statusLP = CCT_Container_StatusKey.MISSING;
//					}
//				}
				
				list.add(c);
			}
		}
		
		//------判断是商品
		if(!Utility.isNullForList(tlp.PRODUCTS)){
			for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
				CCT_ContainerBean c = new CCT_ContainerBean();
				c.containerType = Cycle_Count_Task_Key.CCT_PRODUCT;
				c.product_type = tlp.PRODUCTS.get(i);
				CCT_JudgeIcon.judgeIcon(c);
			
//				if(!Utility.isNullForList(tlp.PRODUCTS.get(i).SN)){
//					int scanNum = 0;
//					int missNum = 0;
//					for (int j = 0; j < tlp.PRODUCTS.get(i).SN.size(); j++) {
//						String sn = tlp.PRODUCTS.get(i).SN.get(j);
//						if(tlp.PRODUCTS.get(i).SN_Map.get(sn)!=null&&tlp.PRODUCTS.get(i).SN_Map.get(sn)!=CCT_Container_StatusKey.UNSCANNED){
//							scanNum++;
//						}
//						if(tlp.PRODUCTS.get(i).SN_Map.get(sn)!=null&&tlp.PRODUCTS.get(i).SN_Map.get(sn)==CCT_Container_StatusKey.MISSING){
//							missNum++;
//						}
//					}
//					if(scanNum == tlp.PRODUCTS.get(i).SN.size()){
//						c.statusLP = CCT_Container_StatusKey.SCANNED;
//					}
//					if(missNum>0){
//						c.statusLP = CCT_Container_StatusKey.MISSING;
//					}
//				}else{
//					if(tlp.PRODUCTS.get(i).TOTAL!=0){
//						c.statusLP = CCT_Container_StatusKey.SCANNED;
//					}
//				}
				
				list.add(c);
			}
		}
		return list;
	}
	
	public static void removeProduct(List<CCT_ContainerBean> tlpBean_CPlist,long pc_id){
		if(Utility.isNullForList(tlpBean_CPlist)){
			return;
		}
		for (int i = 0; i < tlpBean_CPlist.size(); i++) {
			if(tlpBean_CPlist.get(i).containerType == Cycle_Count_Task_Key.CCT_PRODUCT&&tlpBean_CPlist.get(i).product_type.PC_ID==pc_id){
				tlpBean_CPlist.remove(i);
				break;
			}
		}
	}
}
