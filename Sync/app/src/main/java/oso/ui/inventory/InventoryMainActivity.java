package oso.ui.inventory;

import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import oso.base.BaseActivity;
import oso.ui.inventory.putaway.PutAwayActivity;
import oso.ui.inventory.putaway.bean.CompanyBean;
import oso.ui.msg.Cycle_Count_Tasks;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import declare.com.vvme.R;
import oso.ui.msg.Cycle_Count_Tasks_TypeActivity;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;

public class InventoryMainActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_inventory_main, 0);
		setTitleString("Inventory");
		initView();
	}

	private void initView() {
		findViewById(R.id.btn_MovePallets).setOnClickListener(this);
		findViewById(R.id.btn_cct).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_MovePallets:
			reqPalletType();
			break;
		case R.id.btn_cct:
			popTo(Cycle_Count_Tasks_TypeActivity.class);
			break;
		default:
			break;
		}
	}
	
	private void reqPalletType(){
		RequestParams params = new RequestParams();
		params.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				List<CompanyBean> listTypes = new Gson().fromJson(json.optJSONArray("data").toString(), new TypeToken<List<CompanyBean>>(){}.getType());
				
				Intent in = new Intent(mActivity,PutAwayActivity.class);
				PutAwayActivity.toThis(in, listTypes);
				startActivity(in);
			}
		}.doGet(HttpUrlPath.basePath+"_receive/android/getCompanyByPsId", params, mActivity);
	}

	public void popTo(Class<?> main) {
		Intent intent = new Intent(mActivity, main);
		startActivity(intent);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
}
