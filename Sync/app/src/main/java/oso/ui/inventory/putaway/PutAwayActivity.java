package oso.ui.inventory.putaway;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.inventory.putaway.bean.CompanyBean;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.location.ChooseLocationActivity;
import oso.widget.location.bean.LocationBean;
import support.common.UIHelper;
import support.common.print.PrintTool;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.network.NetInterface;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class PutAwayActivity extends BaseActivity implements OnClickListener{

	private SearchEditText searchEt;
	private Button putwayBtn, printBtn;
	private View qtyLayout;
	private TextView qtyTv;

	private ListView lv;
	private List<PutAwayBean> listPallets;
	
	private TextView tvLocation;
	
	private Button tvCompany;
	
	
	public final static int Location_Back = 0x1200;

	
	private List<CompanyBean> listCompanys;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_putway, 0);
		applyParams();
		setTitleString("Move Pallets");
		initData();
		initView();
		setData();
		
		if(listCompanys.size() == 1){
			tvCompany.setText(listCompanys.get(0).company_id);
		}else{
			showDialog_PalletType();
		}
		
	}

	private void initData() {
		listPallets = new ArrayList<PutAwayBean>();
	}

	private void initView() {
		searchEt = (SearchEditText) findViewById(R.id.etSearch);
		lv = (ListView) findViewById(R.id.lvPallets);
		putwayBtn = (Button) findViewById(R.id.btnMove);
		qtyLayout = findViewById(R.id.qtyLayout);
		qtyTv = (TextView) findViewById(R.id.qtyTv);
		printBtn = (Button) findViewById(R.id.btnPrint);
		
		tvCompany = (Button) findViewById(R.id.tvCompany);
		tvCompany.setOnClickListener(this);
	}

	private void setData() {
		// 初始化lv
		lv.setEmptyView(findViewById(R.id.tvEmpty));
		lv.setAdapter(adapter);
		// 监听事件
		searchEt.setScanMode();
		searchEt.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				isPrint = false;
				reqPallet();
			}
		});
		putwayBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final View contentView = View.inflate(mActivity, R.layout.dlg_putaway_select_loc, null);
				tvLocation = (TextView) contentView.findViewById(R.id.tvLocation);
				
				tvLocation.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent in = new Intent(mActivity,ChooseLocationActivity.class);
						in.putExtra("single_select", true);
						startActivityForResult(in, Location_Back);
					}
				});
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
				builder.setTitle("Input Location");
				builder.setView(contentView);
				builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						final String locationStr = tvLocation.getText().toString();
						if (TextUtils.isEmpty(locationStr)) {
							// UIHelper.showToast("Please input Location!");
							TTS.getInstance().speakAll_withToast("Please input Location!");
							return;
						}
						initPlts();
						if (!TextUtils.isEmpty(pallets))
							doSubmit(pallets, 1, locationStr.toUpperCase());
						else
							TTS.getInstance().speakAll_withToast("Pallets Error!");
					}
				});
				builder.show();

			}
		});
		printBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 打印
				new PrintTool(mActivity).showDlg_printers(ls);
			}
		});
		showRightButton(R.drawable.menu_btn_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listPallets.isEmpty()) {
					UIHelper.showToast(getString(R.string.sync_scan_first));
					return;
				}
				RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
				builder.setTitle("Options");
				builder.setArrowItems(new String[] { "Clear All", "Print All" }, new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						if (position == 0) {
							clearAll();
						}
						if (position == 1) {
							initPlts();
							new PrintTool(mActivity).showDlg_printers(ls);
						}
					}
				});
				builder.show();
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode != RESULT_OK)
			return;
		
		if(requestCode == Location_Back){
			List<LocationBean> lists = (List<LocationBean>) data.getSerializableExtra("SelectLocation");
			if(!Utility.isEmpty(lists))tvLocation.setText(lists.get(0).location_name);
		}
	}

	private void initPlts() {
		String plts = "";
		for (PutAwayBean bean : listPallets) {
			plts += bean.con_id + ",";
		}
		pallets = plts.length() > 0 ? plts.substring(0, plts.length() - 1) : plts;
	}

	private void clearAll() {
		RewriteBuilderDialog.showSimpleDialog(mActivity, "Clear All?", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listPallets.clear();
				isPrint = false;
				refresh_BtnMove();
			}
		});
	}

	/**
	 * 检查是否是重复的plt
	 */
	private boolean isCheckRepeatPallet(String palletNo) {
		if (TextUtils.isEmpty(palletNo))
			return false;
		String pn = palletNo;
		// 去掉打头的“0”
		if (StringUtil.isInteger(pn))
			pn = Integer.parseInt(pn) + "";
		for (PutAwayBean bean : listPallets) {
			if (bean.plate_no.equals(pn) || getFullPltNO_wms_94x(bean.plate_no, false).equals(pn))
				return true;
		}
		return false;
	}

	@Override
	protected void onFirstVisible() {
		// TODO Auto-generated method stub
		super.onFirstVisible();
		TTS.getInstance().speakAll("ready");
	}

	private void reqPallet() {
		final String pallet = searchEt.getEditableText().toString();
		searchEt.setText("");
		
		final String company = tvCompany.getText().toString();
		// 过滤
		if (TextUtils.isEmpty(pallet)) {
			TTS.getInstance().speakAll_withToast("Empty!");
			return;
		}
		if (isCheckRepeatPallet(pallet)) {
			TTS.getInstance().speakAll_withToast("Repeat!");
			return;
		}

		RequestParams params = new RequestParams();
		params.add("plate_no", pallet);
		params.add("ps_id", StoredData.getPs_id());
		params.add("company_id", company);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (StringUtil.getJsonInt(json, "err") == 90) {
					TTS.getInstance().speakAll_withToast(StringUtil.getJsonString(json, "data"));
					return;
				}
				PutAwayBean bean = new Gson().fromJson(json.optJSONObject("data").toString(), PutAwayBean.class);
				if (isCheckRepeatPallet(bean.plate_no)) {
					TTS.getInstance().speakAll_withToast("Repeat!");
					return;
				}
				listPallets.add(0, bean);
				refresh_BtnMove();

				// tts
				TTS.getInstance().speakAll("Pallet " + listPallets.size());
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll("Pallet Not Found!");
			}
		}.doGet(HttpUrlPath.acquireMovementByPalletId, params, this);
	}

	boolean isPrint = false;

	private void refresh_BtnMove() {
		printBtn.setVisibility(!listPallets.isEmpty() && isPrint ? View.VISIBLE : View.GONE);
		putwayBtn.setVisibility(listPallets.isEmpty() || isPrint ? View.GONE : View.VISIBLE);
		qtyLayout.setVisibility(listPallets.isEmpty() ? View.INVISIBLE : View.VISIBLE);
		qtyTv.setText(listPallets.size() + "");
		adapter.notifyDataSetChanged();
	}

	private String pallets = "";

	private void doSubmit(String pallets, final int type, final String location) {
		this.pallets = pallets;
		System.out.println("pallets= " + pallets);
		RequestParams params = new RequestParams();
		params.add("plate_nos", pallets);
		params.add("location_type", type + "");
		params.add("location_name", location);
		params.add("adid", StoredData.getAdid());
		params.add("ps_id", StoredData.getPs_id());
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (StringUtil.getJsonInt(json, "err") == 90) {
					// UIHelper.showToast(StringUtil.getJsonString(json,
					// "data"));
					TTS.getInstance().speakAll_withToast(StringUtil.getJsonString(json, "data"));
					return;
				}
				for (PutAwayBean b : listPallets) {
					b.slc_name = location;
					b.slc_type = type;
				}
				refresh_BtnMove();
				TTS.getInstance().speakAll_withToast("Move Pallets Success!");
				printBtn.setVisibility(View.VISIBLE);
				putwayBtn.setVisibility(View.GONE);
				isPrint = true;
				RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(mActivity);
				b.setMessage("Move pallets success, reprint pallet lables? ");
				b.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 打印
						new PrintTool(mActivity).showDlg_printers(ls);
					}
				});
				b.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mActivity.finish();
					}
				});
				b.show();
			}

			@Override
			public void handFail() {
				TTS.getInstance().speakAll("Error!");
			}
		}.doGet(HttpUrlPath.logMovement, params, this);
	}

	PrintTool.OnPrintLs ls = new PrintTool.OnPrintLs() {
		@Override
		public void onPrint(long printServerID) {
			RequestParams p=NetInterface.recPrintLP_p(printServerID, NetInterface.PrintLP_PltIDs, pallets);
			new SimpleJSONUtil() {
				@Override
				public void handReponseJson(JSONObject json) {
					// TODO Auto-generated method stub
					listPallets.clear();
					refresh_BtnMove();
					TTS.getInstance().speakAll_withToast(getString(R.string.sync_print_success));
				}
			}.doGet(NetInterface.recPrintLP_url(), p, mActivity);
		}
	};

	@Override
	protected void onBackBtnOrKey() {
		// TODO Auto-generated method stub
		if (listPallets.size() == 0) {
			super.onBackBtnOrKey();
			return;
		}
		RewriteBuilderDialog.showSimpleDialog(this, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				PutAwayActivity.super.onBackBtnOrKey();
			}
		});
	}

	// ==================nested===============================

	private BaseAdapter adapter = new BaseAdapter() {
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			Holder holder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.item_movepallets, null);
				holder = new Holder();
				holder.errorBtn = (ImageView) convertView.findViewById(R.id.deleteBtn);
				holder.pnTv = (TextView) convertView.findViewById(R.id.pnTv);
				holder.areaTypeTv = (TextView) convertView.findViewById(R.id.areaTypeTv);
				holder.areaTv = (TextView) convertView.findViewById(R.id.areaTv);
				holder.itemTv = (TextView) convertView.findViewById(R.id.itemTv);
				convertView.setTag(holder);
			} else
				holder = (Holder) convertView.getTag();

			// 刷新数据
			final PutAwayBean b = listPallets.get(position);
			holder.pnTv.setText(b.plate_no);
			holder.areaTv.setText(b.slc_name);
			holder.areaTypeTv.setText(b.slc_type == 1 ? "Location:" : "Staging:");
			holder.itemTv.setText(b.item_id);

			// 监听事件
			holder.errorBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
					dialog.setMessage("Cancel Move " + b.plate_no + "?");
					dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// datas.remove(position);
							listPallets.remove(position);
							refresh_BtnMove();
						}
					});
					dialog.create().show();
				}
			});

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return listPallets.get(position);
		}

		@Override
		public int getCount() {
			return listPallets == null ? 0 : listPallets.size();
		}
	};

	private class Holder {
		public TextView pnTv, areaTv, areaTypeTv, itemTv;
		public ImageView errorBtn;
	}

	/**
	 * 获取-完整的plt号,94000001234567,注:无括号
	 * 
	 * @param pltID
	 * @return
	 */
	public String getFullPltNO_wms_94x(String pltNo, boolean with94Brackets) {
		if (TextUtils.isEmpty(pltNo))
			return "";
		int len = pltNo.length();
		int zeroLen = 12 - len;
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < zeroLen; i++)
			sb.append("0");
		if (!with94Brackets)
			return "94" + sb.toString() + pltNo;
		else
			return "(94)" + sb.toString() + pltNo;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.tvCompany:
			showDialog_PalletType();
			break;
		}
	}
	
	private void showDialog_PalletType(){
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setTitle("Select Company");
		final String[] str = CompanyBean.getTypes(listCompanys);
		builder.setArrowItems(str, new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				tvCompany.setText(str[position]);
			}
		});
		builder.setCancelable(false);
		builder.setCanceledOnTouchOutside(false);
		builder.isHideCancelBtn(true);
		builder.show();
	}
	
	//------------------
	private void applyParams() {
		Intent in = getIntent();
		listCompanys = (List<CompanyBean>) in.getSerializableExtra("listCompanys");
	}
	
	public static void toThis(Intent in,List<CompanyBean> lists) {
		in.putExtra("listCompanys", (Serializable)lists);
	}

}
