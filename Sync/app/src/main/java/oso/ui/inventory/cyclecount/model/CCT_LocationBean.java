package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * @ClassName: CCT_LocationBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-7 上午11:33:56
 */
public class CCT_LocationBean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 5710603364691252221L;
	public long LOCATION_ID;//": 1006261,
	public String LOCATION_FULL_NAME;//": "Valley1-B3B051",
	public String LOCATION_NAME;//": "B051",
	public int LOCATION_TYPE;
	public String SCANNED_BY_ID;
	public String SCANNED_BY_NAME;
	public int STATUS;//": 1
	
	//-----status
	public static final int UNTREATED = 1;//未处理
	public static final int DONE = 3;//处理中
	
	
//	public static void helpJson(JSONObject json, List<CCT_LocationBean> list){
//		JSONArray jArray = json.optJSONArray("LOCATIONS");
//		CCT_LocationBean.resolveJson(jArray,list);
//	}
//
//	public static void resolveJson(JSONArray jArray,List<CCT_LocationBean> list){
//		
//		if(!StringUtil.isNullForJSONArray(jArray)){
//			for(int i=0;i<jArray.length();i++){
//				JSONObject jObj = jArray.optJSONObject(i);
//				CCT_LocationBean t = new CCT_LocationBean();
//				t.LOCATION_ID = jObj.optString("LOCATION_ID");
//				t.LOCATION_FULL_NAME = jObj.optString("LOCATION_FULL_NAME");
//				t.LOCATION_NAME = jObj.optString("LOCATION_NAME");
//				t.SCANNED_BY_NAME = jObj.optString("CLOSE_USER");
//				t.STATUS = jObj.optInt("STATUS");
//				list.add(t);
//			}
//		}
//	}
	
//	/**
//	 * @Description:获取
//	 * @param @return
//	 */
//	public static JSONObject getDebugStr(){
//		JSONObject json = new JSONObject();
//		Random r = new Random();
//		int locationNum = r.nextInt(10)+1;
//		int closeLocationNum = r.nextInt(5)+1;
//		int increaseNum = 0;//增加量
//		JSONArray jLocationArray = new JSONArray();
//		try {
//			for (int k = 0; k < locationNum; k++) {
//				JSONObject jLocation = new JSONObject();
//				increaseNum ++;
//				jLocation.put("LOCATION_FULL_NAME", "Valley1-B3B051"+(k+1));
//				jLocation.put("LOCATION_NAME", "B3B051"+(k+1));
//				jLocation.put("LOCATION_ID", "1006261");
//				int location_status =(increaseNum>=closeLocationNum)?Cycle_Count_Task_Key.Location_NEW:Cycle_Count_Task_Key.Location_SCANNED;
//				jLocation.put("STATUS", location_status);
//				if(location_status == Cycle_Count_Task_Key.Location_SCANNED){
//					jLocation.put("CLOSE_USER","Tony Berserker");
//				}else{
//					jLocation.put("CLOSE_USER","");
//				}
//				jLocationArray.put(jLocation);
//			}
//			json.put("DATA", jLocationArray);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		return json;
//	}

}
