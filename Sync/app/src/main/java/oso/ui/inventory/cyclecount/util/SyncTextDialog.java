package oso.ui.inventory.cyclecount.util;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.adapter.CCT_Sel_Adapter;
import oso.ui.inventory.cyclecount.model.CCT_TitleAndCustomerInfo;
import oso.widget.dialog.BottomDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.datas.HoldThreeValue;
import utility.AllCapTransformationMethod;
import utility.Utility;

/**
 * 重构的点击下拉控件 只方便与CCT
 * Created by gcy on 15/6/29.
 */
public class SyncTextDialog extends Button {

    private List<HoldThreeValue<String,String,Object>> list;//基础数据bean
    private List<HoldThreeValue<String,String,Object>> showData = new ArrayList<HoldThreeValue<String,String,Object>>();//用于显示的bean
    private int index = -1;//当前选中项 默认-1
    private String title;//标题
    private SyncTextDialogInterface iface;
    public boolean isWrapContent = false;//控制下拉弹出窗的格式是否随动自增高
    public String key;//所选取值的key
    public HoldThreeValue<String,String,Object> cutObj ;

    private boolean showSearchView = false;//控制是否显示搜索控件
    private boolean isCanOnClick = true;



    public SyncTextDialog(Context context) {
        super(context);
    }

    public SyncTextDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        setTextSize(13);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
        setPadding(Utility.dip2px(getContext(), 10), 0, 0, 0);
    }

    /**
     * 初始化控件 索引默认为-1
     * @param doublelist 基础数据
     * @param title 弹出标题
     */
    public void setDoubleData(List<HoldDoubleValue<String,String>> doublelist,String title){
        this.setEmpty();
        setDoubleData(doublelist, title, isWrapContent);
    }


    /**
     * 初始化控件 索引默认为-1
     * @param doublelist 基础数据
     * @param title 弹出标题
     */
    public void setDoubleData(List<HoldDoubleValue<String,String>> doublelist,int index,String title,boolean isWrapContent){
        this.setEmpty();
        List<HoldThreeValue<String,String,Object>> list = new ArrayList<HoldThreeValue<String,String,Object>>();
        for (int i = 0; i < doublelist.size(); i++) {
            list.add(new HoldThreeValue<String, String, Object>(doublelist.get(i).a,doublelist.get(i).b,null));
        }
        setData(list, index, title, isWrapContent);
    }

    /**
     * 初始化控件 索引默认为-1
     * @param doublelist 基础数据
     * @param title 弹出标题
     */
    public void setDoubleData(List<HoldDoubleValue<String,String>> doublelist,String title,boolean isWrapContent){
        this.setEmpty();
        List<HoldThreeValue<String,String,Object>> list = new ArrayList<HoldThreeValue<String,String,Object>>();
        for (int i = 0; i < doublelist.size(); i++) {
            list.add(new HoldThreeValue<String, String, Object>(doublelist.get(i).a,doublelist.get(i).b,null));
        }
        setData(list, index, title, isWrapContent);
    }

    /**
     * 初始化控件 索引默认为-1
     * @param list 基础数据
     * @param title 弹出标题
     */
    public void setData(List<HoldThreeValue<String,String,Object>> list,String title){
        this.setEmpty();
        setData(list, index, title, isWrapContent);
    }

    /**
     * 初始化控件 索引默认为-1
     * @param list 基础数据
     * @param title 弹出标题
     */
    public void setData(List<HoldThreeValue<String,String,Object>> list,String title,boolean isWrapContent){
        this.setEmpty();
        setData(list, index, title, isWrapContent);
    }

    /**
     * 初始化控件 索引默认为-1
     * @param list 基础数据
     * @param title 弹出标题
     */
    public void setData(List<HoldThreeValue<String,String,Object>> list,int index,String title){
        this.setEmpty();
        setData(list, index, title, isWrapContent);
    }

    /**
     * 初始化控件 附带索引
     * @param list
     * @param index
     */
    public void setData(List<HoldThreeValue<String,String,Object>> list,int index,String title,boolean isWrapContent){
        this.list = list;
        this.index = index;
        this.title = title;
        this.isWrapContent = isWrapContent;

        if(this.index>=0&&!Utility.isNullForList(list)&&list.size()>=this.index){
            String string = this.list.get(this.index).b.toString();
            setText(TextUtils.isEmpty(string)?"":string);
            this.key = this.list.get(this.index).a;
            this.cutObj = this.list.get(this.index);
        }

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCanOnClick){
                    showSingleDialog();
                }
            }
        });
    }

    /**
     * 弹出的dialog 默认向下弹出
     */
    public void showSingleDialog(){
        View view = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.commen_listview_layout, null);
        ListView listView = (ListView) view.findViewById(R.id.listview);
        this.showData.clear();
        this.showData.addAll(list);
        final CCT_Sel_Adapter adp = new CCT_Sel_Adapter(getContext(),showData,index,isWrapContent);
        // 高度
        int heightLimitCount = 6;

        int cnt_visible = list.size() > heightLimitCount ? heightLimitCount
                : list.size();

        //------由于是随动行高 可能行高过窄不方便显示
        if(isWrapContent&&cnt_visible<6){
            cnt_visible = 4;
        }

        int height = Utility.pxTodip(getContext(), 40);
        // dividerHeight
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = cnt_visible * height;

        listView.setLayoutParams(params);

        listView.setAdapter(adp);
        if(index>=0){
            listView.setSelection(index);
        }

        final BottomDialog dialog = new BottomDialog(getContext());
        dialog.setTitle(title);
        dialog.hideSubmitButton();
        dialog.setView(view);
        dialog.show();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(Utility.isNullForList(showData)&&showData.size()<=position){
                    UIHelper.showToast(getContext().getString(R.string.sync_data_error));
                    return;
                }

                String string = showData.get(position).b;
                for(int i = 0;i<list.size();i++){
                    if(string.equals(list.get(i).b)){
                        index = i;
                    }
                }
                adp.setIndex(index);
                key = list.get(index).a;
                cutObj = list.get(index);
                setText(list.get(index).b);
                toDo();
                dialog.dismiss();
            }
        });

        //------如果不显示则隐藏
        View search_layout = view.findViewById(R.id.search_layout);
        search_layout.setVisibility(showSearchView ? View.VISIBLE : View.GONE);

        if(showSearchView){
            final SearchEditText search_edit = (SearchEditText) view.findViewById(R.id.search_edit);
            search_edit.setHint(title);
            search_edit.setIconMode(SearchEditText.IconMode_Scan);
            search_edit.requestFocus();

            // 全大写
            TransformationMethod trans = AllCapTransformationMethod.getThis();
            search_edit.setTransformationMethod(trans);

            search_edit.setSeacherMethod(new SeacherMethod() {
                @Override
                public void seacher(String value) {
                    adp.setShowSel(false);
                    searchData(value, search_edit, adp);
                    search_edit.requestFocus();
                }
            });

            search_edit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
                @Override
                public void afterTextChanged(Editable s) {
                    String string = search_edit.getText().toString();
                    if(TextUtils.isEmpty(string)){
                        string = "";
                    }
                    adp.setShowSel(false);
                    searchData(string, search_edit, adp);
                    search_edit.requestFocus();
                }
            });
        }
    }

    public void toDo(){
        if(iface!=null){
            iface.doSomething();
        }
    }

    /**
     * 查询接口
     * @param value
     * @param adp
     */
    private void searchData(String value,SearchEditText search_edit,CCT_Sel_Adapter adp){
        index = -1;
        value = value.toUpperCase(Locale.US);
        if(Utility.isNullForList(list)){
            return;
        }
        showData.clear();
        int selectNum = 0;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).b.toUpperCase(Locale.US).indexOf(value)>=0){
                selectNum++;
                showData.add(list.get(i));
            }
        }
        if(selectNum>0){
            adp.notifyDataSetChanged();
//            //-------如果只有一个并且名字相同则 跳到当前的位置
//            if(showData.size()==1&&showData.get(0).b.toUpperCase(Locale.US).equals(value)){
//                getData(locations.get(0));
//                return;
//            }
        }else{
            //search_edit.setShakeAnimation();//晃动动画
            search_edit.setText("");
            search_edit.requestFocus();
            showData.addAll(list);
            adp.notifyDataSetChanged();
//            String message = getContext().getString(R.string.sync_not_found);
//            TTS.getInstance().speakAll(message);
//            RewriteBuilderDialog.showSimpleDialog_Tip(getContext(), message);
        }
    }

    public void setIface(SyncTextDialogInterface iface){
        this.iface = iface;
    }

    public interface SyncTextDialogInterface {
        void doSomething();
    }

    /**
     * 获取keyValue
     * @param bean
     * @return
     */
    public static HoldDoubleValue<String,String> getHoldDoubleValue(CCT_TitleAndCustomerInfo.CCT_BaseData_Info bean){
        return new HoldDoubleValue<String,String>(bean.id,bean.name);
    }

    private void setEmpty(){
        this.index = -1;
        this.key = "";
        this.isWrapContent = false;
        this.cutObj = null;
        this.setText("");
    }

    /**
     * 设置检索控件是否初始化显示
     * @param isShow
     */
    public void setShowSearchView(boolean isShow){
        showSearchView = isShow;
    }

    public boolean check_C_IsNull(){
        if(cutObj!=null&&cutObj.c!=null){
            return false;
        }
        return true;
    }

    /**
     * 设置是否可以执行点击时间
     * @param isCanOnClick
     */
    public void setCanOnClick(boolean isCanOnClick){
        this.isCanOnClick = isCanOnClick;
    }

}