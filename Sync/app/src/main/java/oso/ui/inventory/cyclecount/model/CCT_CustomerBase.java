package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_CustomerBase implements Serializable {

    private static final long serialVersionUID = -5097520428570525310L;

    public int customer_key;
    public String customer_id;
    public String customer_name;
}
