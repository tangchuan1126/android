package oso.ui.inventory.cyclecount.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import utility.StringUtil;
import utility.Utility;
/**
 * @ClassName: ScanLocationDataBean 
 * @Description: 
 * @author gcy
 * @date 2015-4-8 下午9:59:52
 */
public class ScanLocationDataBean implements Serializable{
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 804882576379134115L;
	
	public String slc_area;//": 1000265,
	public String slc_position;//": "B057",
	public String slc_id;//": 1006265,
	public String slc_type;
	public String ps_id;//": 1000005,
	public String is_three_dimensional;
	public List<CCT_TLP_Bean> tlp_containers = new ArrayList<CCT_TLP_Bean>();//list of container on this location
	public List<CCT_CLP_Type_Bean> container_types = new ArrayList<CCT_CLP_Type_Bean>();
	
//	CONTAINER_TYPES
	
	
	public static ScanLocationDataBean helpJsonVerification(JSONObject json){
		ScanLocationDataBean s = new ScanLocationDataBean();
		JSONObject jObj = json.optJSONObject("DATA");
		s.slc_id = jObj.optString("SLC_ID");
		s.slc_area = jObj.optString("SLC_AREA");
		s.slc_type = jObj.optString("SLC_TYPE");
		s.slc_position = jObj.optString("SLC_POSITION");
		JSONObject cObj = jObj.optJSONObject("CONTAINER");
		if(cObj.has("CONTAINER_TYPE")&&cObj.optInt("CONTAINER_TYPE")==Cycle_Count_Task_Key.CCT_TLP){
			CCT_TLP_Bean tlp = new CCT_TLP_Bean();
			tlp.CON_ID = cObj.optLong("CON_ID");
			tlp.CONTAINER_TYPE = cObj.optInt("CONTAINER_TYPE");
			tlp.CONTAINER = cObj.optString("CONTAINER");
			tlp.TITLE_ID = cObj.optInt("TITLE_ID");
			s.tlp_containers.add(tlp);
		}else{
			CCT_CLP_Bean clp = new CCT_CLP_Bean();
			clp.CON_ID = cObj.optLong("CON_ID");
			clp.CONTAINER_TYPE = cObj.optInt("CONTAINER_TYPE");
			clp.CONTAINER = cObj.optString("CONTAINER");
			clp.TITLE_ID = cObj.optInt("TITLE_ID");
			CCT_CLP_Type_Bean c_type = new CCT_CLP_Type_Bean();
			
			c_type.CONTAINERS.add(clp);
			
			c_type.LP_TYPE_ID = cObj.optString("TYPE_ID");
			c_type.LP_TYPE_NAME = cObj.optString("LP_TYPE_NAME");
			
			s.container_types.add(c_type);
		}
		return s;
	}

	public static ScanLocationDataBean helpJsonFindTask(JSONObject json){
		ScanLocationDataBean s = new ScanLocationDataBean();
		JSONObject jObj = json.optJSONObject("DATA");
		s.slc_id = jObj.optString("SLC_ID");
		s.slc_type = jObj.optString("SLC_TYPE");
		s.slc_area = jObj.optString("SLC_AREA");
		s.slc_position = jObj.optString("SLC_POSITION");

//		{
//			"status": 1,
//					"DATA": {
//					"SLC_TYPE": 1,
//					"SLC_POSITION": "loctest",
//					"SLC_ID": 1014038,
//					"CONTAINERS": [{
//						"CONTAINER_TYPE": 1,
//						"CON_ID": 2001015,
//						"CONTAINER": "2001015",
//						"TYPE_ID": 75,
//						"LP_TYPE_NAME": "CLP2*2*2[CLP1]"
//					}],
//					"SLC_AREA": 1000221
//		}
//		}
		JSONArray cArray = jObj.optJSONArray("CONTAINERS");
		if(StringUtil.isNullForJSONArray(cArray)){
			return null;
		}
		for (int i = 0; i < cArray.length(); i++) {
			JSONObject cObj = cArray.optJSONObject(i);
			if(cObj.optInt("CONTAINER_TYPE")==Cycle_Count_Task_Key.CCT_TLP){
				CCT_TLP_Bean tlp = new CCT_TLP_Bean();
				tlp.CON_ID = cObj.optLong("CON_ID");
				tlp.CONTAINER_TYPE = cObj.optInt("CONTAINER_TYPE");
				tlp.CONTAINER = cObj.optString("CONTAINER");
				s.tlp_containers.add(tlp);
			}else{
				CCT_CLP_Bean clp = new CCT_CLP_Bean();
				clp.CON_ID = cObj.optLong("CON_ID");
				clp.CONTAINER_TYPE = cObj.optInt("CONTAINER_TYPE");
				clp.CONTAINER = cObj.optString("CONTAINER");
				CCT_CLP_Type_Bean c_type = new CCT_CLP_Type_Bean();

				c_type.CONTAINERS.add(clp);

				c_type.LP_TYPE_ID = cObj.optString("TYPE_ID");
				c_type.LP_TYPE_NAME = cObj.optString("LP_TYPE_NAME");

				s.container_types.add(c_type);
			}
		}

		return s;
	}

	//============================ Blind Scan ======================================
	public static int Type_TLP = 0;
	public static int Type_CLP = 1;
	
	/**
	 * 获得指定Position的对应的数据类型 TLP / CLP
	 * @param position
	 * @return type
	 */
	public int getType(int position) {
		if(position <= tlp_containers.size() - 1) return Type_TLP;
		return Type_CLP;
	}
	
	/**
	 * 获得指定Position和Type的对应的数据类型 TLP / CLP
	 * @param position
	 * @param type
	 * @return Object
	 */
	public Object getBeanByPosition(int type, int position) {
		if(Type_TLP == type) return tlp_containers.get(position);
		return getClpBean(position);
	}
	
	/**
	 * 获得指定Position对应Clp List中的对应数据
	 * @param position
	 * @return CCT_CLP_Bean
	 */
	public CCT_CLP_Bean getClpBean(int position) {
		int d = position - tlp_containers.size() + 1;
		for(CCT_CLP_Type_Bean clpTypeBean : container_types) {
			int currSize = clpTypeBean.CONTAINERS.size();
			if(d < currSize) return clpTypeBean.CONTAINERS.get(d);
			else d -= clpTypeBean.CONTAINERS.size();
		}
		return new CCT_CLP_Bean(); 
	}

	/**
	 * 获得总数据的个数
	 * @return allSize
	 */
	public int getAllSize() {
		int cnt = 0;
		if(Utility.isNullForList(container_types)) return 0;
		for(CCT_CLP_Type_Bean clpTypeBean : container_types) {
			cnt += clpTypeBean.CONTAINERS.size();
		}
		return cnt;
	}
	
	/**
	 * 不算CLP里所有Product的数量。 
	 * @return TLP和CLP的数量和
	 */
	public int getAllSizeByCLP2One() {
		return (tlp_containers.size() + container_types.size());
	}
	
	/**
	 * 通过position 查询 CONTAINER_TYPES 中对应Position的CCT_CLP_Type_Bean中的CONTAINERS的ArrayList长度
	 * @param position
	 * @return CONTAINERS.size()
	 */
	public int getClpListSizeByPosition(int position) {
		int childPosition = position - (tlp_containers.size() + 1);
		if(childPosition <= 0) return 0;
		if(Utility.isNullForList(container_types)) return 0;
		CCT_CLP_Type_Bean cct_CLP_Type_Bean = container_types.get(childPosition);
		if(cct_CLP_Type_Bean==null) return 0;
		if(Utility.isNullForList(cct_CLP_Type_Bean.CONTAINERS)) return 0;
		return cct_CLP_Type_Bean.CONTAINERS.size();
	}
	
	/**
	 * 减去tlplist.size()
	 * @param grpPosition
	 * @return CCT_CLP_Type_Bean
	 */
	public CCT_CLP_Type_Bean getCLPTypeBeanByGrpPosition(int grpPosition) {
		int childPosition = grpPosition - tlp_containers.size();
		return container_types.get(childPosition);
	}
}
