package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import utility.Utility;

/**
 * Created by xialimin on 15/8/4.
 * My Task界面进入CycleCount 按任务类型进行分组  FIND_TASKS/VERIFICATION_TASKS/CYCLE_COUNT_TASKS
 */
public class CCT_Task_Type_Bean implements Serializable {

    public List<CycleCountArrTask> allData = new ArrayList<CycleCountArrTask>();

    public List<Cycle_Count_Tasks_Bean> CYCLE_COUNT_TASKS = new ArrayList<Cycle_Count_Tasks_Bean>();
    public List<Cycle_Count_Tasks_Bean> FIND_TASKS = new ArrayList<Cycle_Count_Tasks_Bean>();
    public List<Cycle_Count_Tasks_Bean> VERIFICATION_TASKS = new ArrayList<Cycle_Count_Tasks_Bean>();

    public class CycleCountArrTask implements Serializable {
        public List<Cycle_Count_Tasks_Bean> taskTypeArr = new ArrayList<Cycle_Count_Tasks_Bean>();
    }

    /**
     * 初始化数据 包含CYCLE_COUNT_TASKS/FIND_TASKS/VERIFICATION_TASKS 三种数据，根据type来进行区分
     * 并按List有数据的在前排序
     * 0 : CYCLE_COUNT_TASKS
     * 1 : FIND_TASKS
     * 2 : VERIFICATION_TASKS
     */
    public void initData() {
        List<CycleCountArrTask> all = new ArrayList<CycleCountArrTask>();

        for (int i = 0; i < 3; i ++) {
            CycleCountArrTask cycleCountArrTask = new CycleCountArrTask();
            if (Cycle_Count_Task_Key.INDEX_CYCLE_COUNT_TASK == i) {
                cycleCountArrTask.taskTypeArr.addAll(CYCLE_COUNT_TASKS);
            }

            if (Cycle_Count_Task_Key.INDEX_VERIFICATION_TASK == i) {
                cycleCountArrTask.taskTypeArr.addAll(VERIFICATION_TASKS);
            }

            if (Cycle_Count_Task_Key.INDEX_FIND_TASK == i) {
                cycleCountArrTask.taskTypeArr.addAll(FIND_TASKS);
            }
            all.add(cycleCountArrTask);
        }

        for (int i = 0; i< 2; i ++) {
            for (int j = 0; j< 3; j++) {

                // 先添加有数据的
                if ( 0 == i && !Utility.isNullForList(all.get(j).taskTypeArr)) {
                    allData.add(all.get(j));
                }

                if ( 1 == i && Utility.isNullForList(all.get(j).taskTypeArr)) {
                    allData.add(all.get(j));
                }

            }
        }

    }

    /**
     * 获得对应GroupName
     * @param cycleCountArrTask
     * @return
     */
    public static String getTaskTypeName(CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask) {
        Cycle_Count_Tasks_Bean cycleCountTask = null;

        if (!Utility.isNullForList(cycleCountArrTask.taskTypeArr)) {
            cycleCountTask = cycleCountArrTask.taskTypeArr.get(0);
        }
        if (cycleCountTask == null) {
            return "";
        }

        if (cycleCountTask.TYPE == Cycle_Count_Task_Key.VERIFY_TASK || cycleCountTask.TYPE == Cycle_Count_Task_Key.BLIND_TASK) {
            return Cycle_Count_Task_Key.TITLE_CYCLE_COUNT_TASK;
        }

        if (cycleCountTask.TYPE == Cycle_Count_Task_Key.VERIFICATION_TASK) {
            return Cycle_Count_Task_Key.TITLE_VERIFICATION_TASK;
        }

        if (cycleCountTask.TYPE == Cycle_Count_Task_Key.FIND_TASK) {
            return Cycle_Count_Task_Key.TITLE_FIND_TASK;
        }
        return Cycle_Count_Task_Key.getTaskTypeValue(cycleCountTask.TYPE);
    }

    /**
     * 获得对应GroupName
     * @param cycleCountArrTask
     * @return
     */
    public static int getTaskType(CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask) {
        Cycle_Count_Tasks_Bean cycleCountTask = cycleCountArrTask.taskTypeArr.get(0);
        return cycleCountTask.TYPE;
    }

    /**
     * 根据任务类型获得相应数据
     * @param taskType
     * @return
     */
    public List<Cycle_Count_Tasks_Bean> getListByTaskType(int taskType) {
        List<Cycle_Count_Tasks_Bean> taskList = new ArrayList<Cycle_Count_Tasks_Bean>();
        if (taskType == Cycle_Count_Task_Key.BLIND_TASK || taskType == Cycle_Count_Task_Key.VERIFY_TASK) {
            taskList.addAll(CYCLE_COUNT_TASKS);
        }

        if (taskType == Cycle_Count_Task_Key.FIND_TASK) {
            taskList.addAll(FIND_TASKS);
        }

        if (taskType == Cycle_Count_Task_Key.VERIFICATION_TASK) {
            taskList.addAll(VERIFICATION_TASKS);
        }
        return taskList;
    }

    /**
     * 获得有任务的类型任务   FIND/VERIFICATION/CYCLE COUNT TASK
     * @return
     */
    public int getValidTypeCnt () {
        int cnt = 0;
        if (!Utility.isNullForList(VERIFICATION_TASKS)) {
            cnt ++;
        }
        if (!Utility.isNullForList(FIND_TASKS)) {
            cnt ++;
        }
        if (!Utility.isNullForList(CYCLE_COUNT_TASKS)) {
            cnt ++;
        }
        return cnt;
    }
}
