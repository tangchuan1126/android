package oso.ui.inventory.cyclecount.util;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.widget.photo.bean.TTPImgBean;
import support.anim.AnimUtils;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: CCT_Utile 
 * @Description: 包含盘点任务中常用的工具
 * @author gcy
 * @date 2015-4-28 下午8:45:42
 */
public class CCT_Util {

	public static String ERR_DATA = "data";
	public static String ERR_MESSAGE = "message";


	public static String Scanned_Str = "	Scanned";
	public static String Missing_Str = "	Missing";
	public static String Cancel_Str =  "	Cancel";


	/**
	 * 根据Value 获取当前选择的状态
	 * @param value
	 * @return
	 */
	public static int getScanStatus(String value){
		if(Scanned_Str.equals(value)){
			return CCT_Container_StatusKey.SCANNED;
		}else if(Missing_Str.equals(value)){
			return CCT_Container_StatusKey.MISSING;
		}else if(Cancel_Str.equals(value)){
			return CCT_Container_StatusKey.UNSCANNED;
		}else{
			return CCT_Container_StatusKey.PROBLEM;
		}
	}

	/**用于主页面
	 * @Description:制作索引 方便查找tlp外的包裹以及商品信息 以至于可以去掉重复的数据 来让操作更具准确性
	 * 				索引规则 最外层只有clp 并且无父级tlp的id 所以 用“-”来表示没有父级结构(LP加id)
	 * 				如果是tlp中的clp 则 父级id 引用tlp的id  		("LP"加id)
	 * 				如果是tlp中的产品属性id 则父级id 引用tlp的id		("PID"加id)
	 * 				如果是tlp中的产品中的sn 则父级别id 引用产品属性id	("SN"加sn)
	 * @param @param listBean 基础信息集合
	 * @param @param t 不包含索引在内的tlp信息
	 * @param @return
	 */
	public static Map<String, String> getIndexMap(List<CCT_ContainerBean> listBean){
		Map<String, String> map = new HashMap<String, String>();
		if(!Utility.isNullForList(listBean)){
			//-------循环大列表
			for (int i = 0; i < listBean.size(); i++) {
				//---------判断是tlp
				if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP){
					//------取到索引的tlp
					CCT_TLP_Bean tlp = listBean.get(i).tlp;
					if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
						for (int j = 0; j < tlp.CONTAINER_TYPES.size(); j++) {
							if(!Utility.isNullForList(tlp.CONTAINER_TYPES.get(j).CONTAINERS)){
								CCT_CLP_Type_Bean clp_type =  tlp.CONTAINER_TYPES.get(j);
								for (int j2 = 0; j2 < clp_type.CONTAINERS.size(); j2++) {
									//-----添加tlp的中的clp数据
									if(clp_type.CONTAINERS.get(j2).statusLP!=CCT_Container_StatusKey.MISSING){
										map.put("LP"+clp_type.CONTAINERS.get(j2).CON_ID, tlp.CON_ID+"");
									}
								}
							}
						}
					}
					
//					if(!Utility.isNullForList(tlp.PRODUCTS)){
//						for (int j = 0; j < tlp.PRODUCTS.size(); j++) {
//							//-----添加tlp的中的商品类型id
//							map.put("PID"+ tlp.PRODUCTS.get(j).PC_ID, tlp.CON_ID);
//							if(!Utility.isNullForList(tlp.PRODUCTS.get(j).SN)){
//								for (int j2 = 0; j2 < tlp.PRODUCTS.get(j).SN.size(); j2++) {
//									String sn = tlp.PRODUCTS.get(j).SN.get(j2);
//									if(tlp.PRODUCTS.get(j).SN_Map.get(sn)==null||tlp.PRODUCTS.get(j).SN_Map.get(sn) != CCT_Container_StatusKey.MISSING){
//										//-----添加tlp的中的sn数据
//										map.put("SN"+ tlp.PRODUCTS.get(j).SN.get(j2), tlp.PRODUCTS.get(j).PC_ID);
//									}
//								}
//							}
//						}
//					}
				}
			}
		}
		return map;
	}
	/**
	 * @Description:制作索引 方便查找tlp外的包裹以及商品信息 以至于可以去掉重复的数据 来让操作更具准确性
	 * 				索引规则 最外层只有clp 并且无父级tlp的id 所以 用“-”来表示没有父级结构(LP加id)
	 * 				如果是tlp中的clp 则 父级id 引用tlp的id  		("LP"加id)
	 * 				如果是tlp中的产品属性id 则父级id 引用tlp的id		("PID"加id)
	 * 				如果是tlp中的产品中的sn 则父级别id 引用产品属性id	("SN"加sn)
	 * @param @param listBean 基础信息集合
	 * @param @param t 不包含索引在内的tlp信息
	 * @param @return
	 */
	public static Map<String, String> getIndexMap(List<CCT_ContainerBean> listBean,CCT_TLP_Bean t){
		Map<String, String> map = new HashMap<String, String>();
		//-----最外层的CLP
		String noParent = "-";
		if(!Utility.isNullForList(listBean)){
			//-------循环大列表
			for (int i = 0; i < listBean.size(); i++) {
				//---------判断是tlp
				if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP&&listBean.get(i).tlp.CON_ID!=t.CON_ID){
					//------取到索引的tlp
					CCT_TLP_Bean tlp = listBean.get(i).tlp;
					if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
						for (int j = 0; j < tlp.CONTAINER_TYPES.size(); j++) {
							if(!Utility.isNullForList(tlp.CONTAINER_TYPES.get(j).CONTAINERS)){
								CCT_CLP_Type_Bean clp_type =  tlp.CONTAINER_TYPES.get(j);
								for (int j2 = 0; j2 < clp_type.CONTAINERS.size(); j2++) {
									//-----添加tlp的中的clp数据
									if(clp_type.CONTAINERS.get(j2).statusLP!=CCT_Container_StatusKey.MISSING){
										map.put("LP"+clp_type.CONTAINERS.get(j2).CON_ID, tlp.CON_ID+"");
									}
								}
							}
						}
					}
					
					if(!Utility.isNullForList(tlp.PRODUCTS)){
						for (int j = 0; j < tlp.PRODUCTS.size(); j++) {
							//-----添加tlp的中的商品类型id
							map.put("PID"+ tlp.PRODUCTS.get(j).PC_ID, tlp.CON_ID+"");
							if(!Utility.isNullForList(tlp.PRODUCTS.get(j).SN)){
								for (int j2 = 0; j2 < tlp.PRODUCTS.get(j).SN.size(); j2++) {
									String sn = tlp.PRODUCTS.get(j).SN.get(j2);
									if(tlp.PRODUCTS.get(j).SN_Map.get(sn)==null||tlp.PRODUCTS.get(j).SN_Map.get(sn) != CCT_Container_StatusKey.MISSING){
										//-----添加tlp的中的sn数据
										map.put("SN"+ tlp.PRODUCTS.get(j).SN.get(j2), tlp.PRODUCTS.get(j).PC_ID+"/"+tlp.CON_ID);
									}
								}
							}
						}
					}
				}
				//--------判断是clp
				if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_CLP){
					//------取到索引的clp_type
					CCT_CLP_Type_Bean clp_type =  listBean.get(i).clp_type;
					for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
						//-----添加最外层的lp数据
						if(clp_type.CONTAINERS.get(j).statusLP!=CCT_Container_StatusKey.MISSING){
							map.put("LP"+clp_type.CONTAINERS.get(j).CON_ID, noParent);
						}
					}
				}
			}
		}
		return map;
	}
	
	/**
	 * @Description:修复数据中所有LP的真实内容
	 */
	public static void fixScanLocationDataBean(ScanLocationDataBean location){
		if(location==null){
			return;
		}
		//---------修复TLP
		if(!Utility.isNullForList(location.tlp_containers)){
			for (int i = 0; i < location.tlp_containers.size(); i++) {
				fixTLP(location.tlp_containers.get(i));
			}
		}
		//---------修复CLP
		if(!Utility.isNullForList(location.container_types)){
			for (int i = 0; i < location.container_types.size(); i++) {				
				if(!Utility.isNullForList(location.container_types.get(i).CONTAINERS)){
					for (int j = 0; j < location.container_types.get(i).CONTAINERS.size(); j++) {
						fixCLP(location.container_types.get(i).CONTAINERS.get(j));
					}
				}
			}
		}
	}
	
	/**
	 * @Description:修复TLP
	 */
	public static void fixTLP(CCT_TLP_Bean tlp){
		tlp.REAL_NUMBER = fixLP_Number(tlp.CONTAINER);
		//-----修复产品
		fixProductBean(tlp);
		if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
			for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
				if(!Utility.isNullForList(tlp.CONTAINER_TYPES.get(i).CONTAINERS)){
					for (int j = 0; j < tlp.CONTAINER_TYPES.get(i).CONTAINERS.size(); j++) {
						fixCLP(tlp.CONTAINER_TYPES.get(i).CONTAINERS.get(j));
					}
				}
			}
		}
	}
	
	/**
	 * @Description:修复数据中所有商品中的真实内容
	 */
	public static void fixProductBean(CCT_TLP_Bean tlp){
		if(tlp==null||Utility.isNullForList(tlp.PRODUCTS)){
			return;
		}
		for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
			if(Utility.isNullForList(tlp.PRODUCTS.get(i).SN)){
				tlp.PRODUCTS.get(i).IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_NO_SN;
			}else{
				tlp.PRODUCTS.get(i).IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_HAS_SN;
			}
		}
	}
	
	/**
	 * @Description:修复CLP
	 */
	public static void fixCLP(CCT_CLP_Bean clp){
		clp.REAL_NUMBER = fixLP_Number(clp.CONTAINER);
	}
	
	
	
	/**
	 * @Description:修复lp的字符串长度
	 */
	public static String fixLP_Number(String value){

		//-----规定长度
		int format = 14;
		//-----字符长度
		int length = value.length();
		
		if(length>=format){
			return value;
		}

		String str = "94";
		
		//-----还差的字符长度
		int difference = format - str.length() - length;
		for (int i = 0; i < difference; i++) {
			str+="0";
		}
		str = str + value;
		
		if(difference < 0) {
			return value;
		}
		
		return str.toUpperCase();
	}
	
	/**
	 * LP是否经过修复
	 * @param value
	 * @return
	 */
	public static boolean isLPFixed(String value) {
		int format = 12;
		int length = value.length();
		int difference = format - length;
		String str = "94";
		for (int i = 0; i < difference; i++) {
			str+="0";
		}
		
		if(TextUtils.isEmpty(value)) return false;
		return value.startsWith(str);
	}
	
	/**
	 * @Description:校验是否所有货物均被扫描 true全部扫描    false未扫描
	 * @param
	 */
	public static boolean checkVerifyData(List<CCT_ContainerBean> listBean){
//		boolean flag = false;
		if(Utility.isNullForList(listBean)){
			return true;
		}
		//-----判断是否都已经扫过的数量  如果与当前lp数量相同 则认为已经全部扫描过了
//		int a = 0;
		for (int i = 0; i < listBean.size(); i++) {
			
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_TLP
					//-----------先判断tlp是否扫描过 如果未扫描 则返回false
					&&(listBean.get(i).statusLP == CCT_Container_StatusKey.UNSCANNED 
					//-----------判断tlp里面的内容
					|| (listBean.get(i).statusLP != CCT_Container_StatusKey.MISSING&&!checkTLPData(listBean.get(i).tlp)))){
				return false;
			}
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP&&!checkCLPData(listBean.get(i).clp_type)){
				return false;
			}
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_PRODUCT&&!checkProData(listBean.get(i).product_type)){
				return false;
			}
			
//			a++;
		}
		
		return true;
	}
	
	/**
	 * @Description:判断TLP是否全部扫描，如果全部扫描则返回true 否则返回false
	 */
	private static boolean checkTLPData(CCT_TLP_Bean tlp){		
		//------如果都为空 则表示TLP上面没有东西
		if(Utility.isNullForList(tlp.CONTAINER_TYPES)&&Utility.isNullForList(tlp.PRODUCTS)){
			return true;
		}
		
		for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
			if(!checkCLPData(tlp.CONTAINER_TYPES.get(i))){
				return false;
			}
		}
		
		for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
			if(!checkProData(tlp.PRODUCTS.get(i))){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * @Description:判断CLP同种类型的是否全部扫描，如果全部扫描则返回true 否则返回false
	 */
	private static boolean checkCLPData(CCT_CLP_Type_Bean clp_type){
		for (int i = 0; i < clp_type.CONTAINERS.size(); i++) {
			CCT_CLP_Bean clp = clp_type.CONTAINERS.get(i);
			if(clp.statusLP==CCT_Container_StatusKey.UNSCANNED){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @Description:判断pro同种类型的是否全部扫描，如果全部扫描则返回true 否则返回false
	 */
	private static boolean checkProData(CCT_ProductBean pro){
		if(pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_HAS_SN){
			for (int i = 0; i < pro.SN.size(); i++) {
				String sn = pro.SN.get(i);
				if(pro.SN_Map.get(sn)==null||pro.SN_Map.get(sn)==CCT_Container_StatusKey.UNSCANNED){
					return false;
				}
			}
			
		}else{
			if(pro.statusLP==CCT_Container_StatusKey.UNSCANNED){
				return false;
			}
		}
		return true;
	}
	
	
	public static String getDelUploadPicsIds(List<CCT_ContainerBean> listBean) {
		StringBuilder builder = new StringBuilder();
		for(CCT_ContainerBean containerBean : listBean) {
			if(containerBean.containerType ==  Cycle_Count_Task_Key.CCT_TLP) {
				// 当前是TLP
				
				//TLP中的CLP
				for(CCT_CLP_Type_Bean clpTypeBean : containerBean.tlp.CONTAINER_TYPES) {
					for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
						List<TTPImgBean> photos = clpBean.photos;
						for(int i=0; i<photos.size(); i++) {
							builder.append(photos.get(i).file_id+",");
						}
					}
				}
				
				//TLP中的Product
				for(CCT_ProductBean pdtBean : containerBean.tlp.PRODUCTS) {
					// 没有SN 遍历photos
					if(pdtBean.IS_HAS_SN == Cycle_Count_Task_Key.CCT_P_NO_SN) {
						List<TTPImgBean> photos = pdtBean.photos;
						for(int i=0; i<photos.size(); i++) {
							builder.append(photos.get(i).file_id+",");
						}
					}
					
					
					// 有SN 遍历map
					else {
						Map<String, List<TTPImgBean>> sN_photos = pdtBean.SN_photos;
						Set<Entry<String, List<TTPImgBean>>> entrySet = sN_photos.entrySet();
						for(Entry<String, List<TTPImgBean>> entry : entrySet) {
							List<TTPImgBean> photoss = entry.getValue();
							for(int j=0; j<photoss.size(); j++) {
								builder.append(photoss.get(j).file_id+",");
							}
						}
					}
					
				}
				
			}
			
			else if(containerBean.containerType ==  Cycle_Count_Task_Key.CCT_CLP) {
				for(CCT_CLP_Bean clpBean : containerBean.clp_type.CONTAINERS) {
					List<TTPImgBean> photos = clpBean.photos;
					for(int i=0; i<photos.size(); i++) {
						builder.append(photos.get(i).file_id+",");
					}
				}
			}
			
			else if(containerBean.containerType ==  Cycle_Count_Task_Key.CCT_PRODUCT) {
				CCT_ProductBean product_type = containerBean.product_type;
				//没有SN的 遍历photos
				if(product_type.IS_HAS_SN == Cycle_Count_Task_Key.CCT_P_NO_SN) {
					List<TTPImgBean> pdtPhotos = containerBean.product_type.photos;
					
					for(int i=0; i<pdtPhotos.size(); i++) {
						builder.append(pdtPhotos.get(i).file_id + ",");
					}
				} 
				
				
				//有SN的 遍历SN_photos
				else {
					Map<String, List<TTPImgBean>> pdtSNPhotosMap = containerBean.product_type.SN_photos;
					Set<Entry<String, List<TTPImgBean>>> entrySet = pdtSNPhotosMap.entrySet();
					for(Entry<String, List<TTPImgBean>> entry : entrySet) {
						List<TTPImgBean> photoss = entry.getValue();
						for(int j=0; j<photoss.size(); j++) {
							builder.append(photoss.get(j).file_id+",");
						}
					}
				}
			}
		}
		
		String result =  builder.toString();
		int index = result.lastIndexOf(",");
		if(index > 0) {
			result = result.substring(0, index);
		}
		return result;
	}
	
	
	/**
	 * @Description:获取需要提交的JSON格式
	 * @param @param tBean 任务对象
	 * @param @param sBean 当前location的任务数据
	 * @param @param listBean 经过整理后的LP数组
	 * @param @return
	 */
	public static JSONObject getSubmitData(Cycle_Count_Tasks_Bean tBean,ScanLocationDataBean sBean,List<CCT_ContainerBean> listBean){
		JSONObject json = new JSONObject();
		try {
			json.put("task_id", tBean.ID);
			json.put("task_type", tBean.TYPE);
			json.put("slc_id", sBean.slc_id);
			json.put("slc_type", sBean.slc_type);
			json.put("user_id", StoredData.getAdid());
			//--------clp
			JSONObject containerJson = new JSONObject();
			//--------clp Damage数组
			JSONArray ctnrDamageArray = new JSONArray();
			//--------clp 没有损坏的id集合
			List<Long> ids = new ArrayList<Long>();

			//--------tlp数组
			JSONArray tlpArray = new JSONArray();
			
			for (int i = 0; i < listBean.size(); i++) {
				//----CLP
				if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_CLP
						&&listBean.get(i).statusLP!=CCT_Container_StatusKey.MISSING
						&&listBean.get(i).statusLP!=CCT_Container_StatusKey.UNSCANNED){
					CCT_CLP_Type_Bean clp_type = listBean.get(i).clp_type;
					for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
						CCT_CLP_Bean clp = clp_type.CONTAINERS.get(j);
						if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
							ids.add(clp.CON_ID);
						}
						
						if(clp.isDamaged){
							JSONObject ctnrDamageObj = new JSONObject();
							ctnrDamageObj.put("id", clp.CON_ID);
							if(!Utility.isNullForList(clp.photos)){
								List<String> images = new ArrayList<String>();
								for (int k = 0; k < clp.photos.size(); k++) {
									images.add(clp.photos.get(k).uri);
								}
								ctnrDamageObj.put("images", new JSONArray(images));
							}
							ctnrDamageArray.put(ctnrDamageObj);
						}
					}
					if(!Utility.isNullForList(ids)){
						containerJson.put("ids", new JSONArray(ids));
					}
					if(!StringUtil.isNullForJSONArray(ctnrDamageArray)){
						containerJson.put("damaged", ctnrDamageArray);
					}
				}
				//----TLP
				else if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP
						&&listBean.get(i).statusLP!=CCT_Container_StatusKey.MISSING
						&&listBean.get(i).statusLP!=CCT_Container_StatusKey.UNSCANNED){
					JSONObject tlpJson = new JSONObject();
					CCT_TLP_Bean tlp = listBean.get(i).tlp;
					tlpJson.put("con_id", tlp.CON_ID);
					if(tlp.isDamaged){
						JSONObject tlp_DamageObj = new JSONObject();
						if(!Utility.isNullForList(tlp.photos)){
							List<String> images = new ArrayList<String>();
							for (int k = 0; k < tlp.photos.size(); k++) {
								images.add(tlp.photos.get(k).uri);
							}

							tlp_DamageObj.put("id", tlp.CON_ID);
							if(!Utility.isNullForList(images)){
								tlp_DamageObj.put("images", new JSONArray(images));
							}
							tlpJson.put("damaged", tlp_DamageObj);
						}
					}
					
					
					if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
						JSONObject tlp_ctnr_Json = new JSONObject();
						//--------clp Damage数组
						JSONArray tlp_ctnr_DamageArray = new JSONArray();
						//--------clp 没有损坏的id集合
						List<Long> clp_ids = new ArrayList<Long>();
						
						for (int a = 0; a < tlp.CONTAINER_TYPES.size(); a++) {
							CCT_CLP_Type_Bean clp_type = tlp.CONTAINER_TYPES.get(a);
							for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
								CCT_CLP_Bean clp = clp_type.CONTAINERS.get(j);
								if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
									clp_ids.add(clp.CON_ID);
								}
								
								if(clp.isDamaged){
									JSONObject tlp_ctnrDamageObj = new JSONObject();
									tlp_ctnrDamageObj.put("id", clp.CON_ID);
									if(!Utility.isNullForList(clp.photos)){
										List<String> images = new ArrayList<String>();
										for (int k = 0; k < clp.photos.size(); k++) {
											images.add(clp.photos.get(k).uri);
										}
										if(!Utility.isNullForList(images)){
											tlp_ctnrDamageObj.put("images", new JSONArray(images));
										}
									}
									tlp_ctnr_DamageArray.put(tlp_ctnrDamageObj);
								}
							}
							if(!Utility.isNullForList(clp_ids)){
								tlp_ctnr_Json.put("ids",  new JSONArray(clp_ids));
							}
							
							if(!StringUtil.isNullForJSONArray(tlp_ctnr_DamageArray)){
								tlp_ctnr_Json.put("damaged", tlp_ctnr_DamageArray);
							}
						}

						if(tlp_ctnr_Json!=null){
							tlpJson.put("containers", tlp_ctnr_Json);
						}
					}
					if(!Utility.isNullForList(tlp.PRODUCTS)){
						//--------商品集合
						JSONArray proJsonArray = new JSONArray();
						for (int a = 0; a < tlp.PRODUCTS.size(); a++) {
							
							JSONObject proJsonItem = new JSONObject();
							//--------clp Damage数组
							JSONArray pro_DamageArray = new JSONArray();
							
							CCT_ProductBean pro = tlp.PRODUCTS.get(a);
//							if(pro.statusLP == CCT_Container_StatusKey.SCANNED){
							proJsonItem.put("pc_id", pro.PC_ID);
							
							//------如果有sn
							if(!Utility.isNullForList(pro.SN)){
								//--------pro 没有损坏的id集合
								List<String> sns = new ArrayList<String>();
								
								for (int j = 0; j < pro.SN.size(); j++) {
									String sn = pro.SN.get(j);
									if(pro.SN_Map.get(sn)!=null&&pro.SN_Map.get(sn)==CCT_Container_StatusKey.SCANNED){
										sns.add(sn);
									}
									if(pro.SN_Damaged_Map.get(sn)!=null&&pro.SN_Damaged_Map.get(sn)){
										JSONObject pro_damage_json = new JSONObject();
										pro_damage_json.put("sn", sn);
										pro_damage_json.put("id", pro.PC_ID);
										
										if(!Utility.isNullForList(pro.SN_photos.get(sn))){
											List<String> images = new ArrayList<String>();
											for (int k = 0; k < pro.SN_photos.get(sn).size(); k++) {
												images.add(pro.SN_photos.get(sn).get(k).uri);
											}
											if(!Utility.isNullForList(images)){
												pro_damage_json.put("images", new JSONArray(images));
											}
										}
										pro_DamageArray.put(pro_damage_json);
									}
								}
								if(!Utility.isNullForList(sns)){
									proJsonItem.put("sn", new JSONArray(sns));
								}
								if(!StringUtil.isNullForJSONArray(pro_DamageArray)){
									proJsonItem.put("damaged", pro_DamageArray);
								}
							}
							//------没有SN
							else{

								//Modify by xialimin 如果有问题 statusLP == 4 添加不了
								//if(pro.statusLP == CCT_Container_StatusKey.SCANNED){
									if(pro.isDamaged){
										JSONObject pro_damage_json = new JSONObject();
										pro_damage_json.put("id",pro.PC_ID);
										pro_damage_json.put("quantity", pro.DAMAGEDCNT);
										if(!Utility.isNullForList(pro.photos)){
											

											List<String> images = new ArrayList<String>();
											for (int k = 0; k < pro.photos.size(); k++) {
												images.add(pro.photos.get(k).uri);
											}
											if(!Utility.isNullForList(images)){
												pro_damage_json.put("images", new JSONArray(images));
											}
										}
										pro_DamageArray.put(pro_damage_json);
									}
									proJsonItem.put("quantity", pro.TOTAL);
									if(!StringUtil.isNullForJSONArray(pro_DamageArray)){
										proJsonItem.put("damaged", pro_DamageArray);
									}
								//}
								
								
							}

							proJsonArray.put(proJsonItem);
						}
						if(!StringUtil.isNullForJSONArray(proJsonArray)){
							tlpJson.put("products", proJsonArray);
						}
					}
					tlpArray.put(tlpJson);
				}
			}
			
			//----添加containers
			if(!containerJson.isNull("ids")||!containerJson.isNull("damaged")){
				json.put("containers", containerJson);
			}
			
			//----添加containers
			if(!StringUtil.isNullForJSONArray(tlpArray)){
				//----添加TLP
				json.put("tlps", tlpArray);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
	/**
	 * @Description:将String类型更改成long类型
	 */
	public static long changeToLong(String value){
		long a = 0;
		try {
			a = Long.parseLong(value);
		} catch (Exception e) {
			
		}
		return a;
	}
	
	
	/*****************************************************************************************************/

	/**
	 * 方便跳入其他的页面
	 * @param ac  当前activity
	 * @param cls 即将要去的activity
	 */
	public static void simpleToGoAc(Activity ac, Class<?> cls){
		Intent intent = new Intent();
		intent.setClass(ac, cls);
		ac.startActivity(intent);
		ac.overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_in);
	}

	/**
	 * 检查是否有没填的数据 有的则返回false 没有则返true
	 * @param view
	 * @return
	 */
	public static boolean checkTextViewValue(Activity activity,TextView... view){
		boolean flag = (view!=null&&view.length>0);
		if(!flag){
			return false;
		}
		for (int i=0;i<view.length;i++) {
			if(TextUtils.isEmpty(view[i].getText().toString())){
				TTS.getInstance().speakAll_withToast((String)(view[i].getTag()), true);
				AnimUtils.horizontalShake(activity, view[i]);
				view[i].requestFocus();
				return false;
			}
		}
		return true;
	}

	/**
	 * 检查所填写的如果有效数点的话则提示 只能保留两位小数 没有的话不提示
	 * @param activity
	 * @param digit 索引只保留几位小数
	 * @param view
	 * @return
	 */
	public static void checkTextViewTwoPointValue(final Activity activity,final int digit ,final EditText... view){
		boolean flag = (view!=null&&view.length>0);
		if(!flag){
			return;
		}
		for (int i=0;i<view.length;i++) {
			final int index = i;
			view[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					String string = view[index].getText().toString();
					if (!hasFocus && !TextUtils.isEmpty(string) && string.indexOf(".")>0&& (string.length() - 1 -string.indexOf(".")) > digit) {//获得焦点
						TTS.getInstance().speakAll_withToast((String)(view[index].getTag()), true);
						AnimUtils.horizontalShake(activity, view[index]);
					}
				}
			});
		}
	}

	/**
	 * 显示消息
	 * @param json
	 */
	public static void showMessage(JSONObject json){
		showMessageAndSpeak(json,false,null);
	}

	/**
	 * 显示消息
	 * @param json
	 */
	public static void showMessage(JSONObject json,String err_code){
		showMessageAndSpeak(json,false,err_code);
	}

	/**
	 * 显示消息并语音提示
	 * @param json
	 */
	public static void showMessageAndSpeak(JSONObject json,boolean isSpeak,String err_code){
		String err = err_code;
		if(TextUtils.isEmpty(err)){
			err = ERR_DATA;
		}
		if (json != null) {
			if(isSpeak){
				TTS.getInstance().speakAll_withToast(json.optString(err), true);
			}else{
				UIHelper.showToast(json.optString(err));
			}
		}
	}

	/**
	 * 获取对应value的key值
	 * @param value
	 * @param names
	 * @return
	 */
	public static int getKeyValue(String value,List<HoldDoubleValue<String,String>> names) {
		int reInt = 0;
		if (!TextUtils.isEmpty(value)) {
			for (HoldDoubleValue<String,String> h : names) {
				int key = Integer.parseInt(h.a);
				String values = h.b;
				if (value.toUpperCase().equals(values.toUpperCase())) {
					reInt = key;
					break;
				}
			}
		}
		return reInt;
	}

	/**
	 * 处理容器与商品之间的json
	 * @param json json数据
	 * @param procode sku
	 * @return
	 */
	public static CCT_ProductBean dealWithConPro(JSONObject json,String procode){

		CCT_ProductBean pro = new CCT_ProductBean();
		try {
			pro.P_NAME = json.optString("P_NAME");
			pro.PC_ID = json.optLong("PC_ID", 0);
			pro.P_CODE = json.optString("P_CODE");
			/*
			 * 由于 procode 的拼接规则为 modelnumber
			 * 加上“/”再加上lotnumber来拼接所组合的 因此用split("/")会得到两个结构所以lotnumber 在第二个索引中
			 * 当然有特殊情况 比如 没有lotnumber的情况 则 procode 只有modelnumber 而没有“/”
			 */
			if(procode.indexOf("/")>=0){
				pro.modelNo = procode.split("/")[0];
				pro.lotNo = procode.split("/")[1];
			}else{
				pro.modelNo = procode;
			}

			JSONArray jsonArray = json.optJSONArray("SN_LIST");
			if(!json.has("SN_LIST")||StringUtil.isNullForJSONArray(jsonArray)){
				pro.IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_NO_SN;
			}else{
				pro.IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_HAS_SN;
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jObj = jsonArray.optJSONObject(i);
					pro.SN.add(jObj.optString("SERIAL_NUMBER"));
//					//因为是编辑页面所以 默认为已经扫描 已经存在的
//					pro.SN_Map.put(jObj.optString("SERIAL_NUMBER"),CCT_Container_StatusKey.SCANNED);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			return pro;
		}


		return pro;
	}

	/**
	 * 根据ModelNo/LotNo 获得LotNo
	 * @param procode
	 * @return
	 */
	public static String getLotNo(String procode) {
		if (TextUtils.isEmpty(procode)) {
			return "";
		}

		if (procode.indexOf("/") > 0 && procode.indexOf("/") < procode.length()) {
			return procode.substring(procode.indexOf("/") + 1, procode.length());
		}

		return "";
	}

	/**
	 * 根据ModelNo/LotNo 获得LotNo
	 * @param procode
	 * @return
	 */
	public static String getModelNo(String procode) {
		if (TextUtils.isEmpty(procode)) {
			return "";
		}

		if (procode.indexOf("/") > 0 && procode.indexOf("/") > 0) {
			return procode.substring(0, procode.indexOf("/"));
		}

		return procode;
	}

	/**
	 * 获得时间间隔
	 * @param start_date
	 * @param end_date
	 * @return
	 */
	public static long getDuration(String start_date, String end_date) {

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date beginDate = format.parse(start_date);
			Date endDate= format.parse(end_date);
			long day = (endDate.getTime() - beginDate.getTime()) / (24*60*60*1000) + 1;
			return day;
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}

}
