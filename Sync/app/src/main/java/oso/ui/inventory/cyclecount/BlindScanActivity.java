package oso.ui.inventory.cyclecount;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.debug.product.CreateProductActivity;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.inventory.cyclecount.adapter.BlindScanAdp;
import oso.ui.inventory.cyclecount.iface.IBlind;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.BlindCLPResultBean;
import oso.ui.inventory.cyclecount.model.BlindScanDataBean;
import oso.ui.inventory.cyclecount.model.BlindScanTLPDetailBean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_Product;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Dialog;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
/**
 * 
 * @author xialimin
 * @date 2015-4-10 上午10:48:50
 * 
 * @description 盲盘主界面  可扫TLP / CLP
 */
public class BlindScanActivity extends BaseActivity implements OnClickListener, IBlind {
	
	public static final int REQ_DETAIL = 0;
	public static final String INTENT_EXTRA_TLP_DETAIL = "TLP_DETAIL_BEAN";
	public static final String INTENT_EXTRA_POSITION = "POSITION";
	public static final String INTENT_EXTRA_TLPNUMBER = "TLPNUMBER";
	public static final String INTENT_EXTRA_TLP_CON_ID = "TLP_CON_ID";
	public static final String INTENT_EXTRA_DATA = "ALLDATA";
	public static final String INTENT_EXTRA_LOCATION = "LOCATION";
	public static final String INTENT_EXTRA_TASK = "TASK";
	
	public static final int POS_GROUP = 0;
	public static final int POS_CHILD = 1;
	
	private ExpandableListView mLv; 
	private TabToPhoto ttp;
	private SearchEditText mEtScanNumber;
	
	private BlindScanAdp mScanAdp;
	private CCT_LocationBean mLocBean;
	private ScanLocationDataBean sBean = new ScanLocationDataBean();
	private BlindScanDataBean mData;
	private Cycle_Count_Tasks_Bean mTasksBean;
	
	private Context mContext;

	private NetConnection_CCT conn;
	private String interimStr; //用于保存临时的code
	
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_cycle_count_blind_scan, 0);
		applyParams();
		initView();
		initData();
		initListener();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(REQ_DETAIL == resultCode) {
			if(data == null) return;
			int currPosition = data.getIntExtra(INTENT_EXTRA_POSITION, 0);
			BlindScanTLPDetailBean tlpDetailBean = (BlindScanTLPDetailBean)data.getSerializableExtra(INTENT_EXTRA_TLP_DETAIL);
			saveTlpDetail(tlpDetailBean, currPosition);
		}
		if (ttp != null) {
			ttp.onActivityResult(requestCode, resultCode, data);
		}

		if(requestCode == CCT_CreateTLP.resultCode){
			if(data.getSerializableExtra("tlp")==null){
				return;
			}
			CCT_TLP_Bean tlp_bean  = (CCT_TLP_Bean) data.getSerializableExtra("tlp");
			submitLP(tlp_bean.CONTAINER);
		}
		if(requestCode == CCT_CreateCLP.resultCode){
			if(data.getSerializableExtra("clp")==null){
				return;
			}
			CCT_CLP_Bean clp_bean  = (CCT_CLP_Bean) data.getSerializableExtra("clp");
			submitLP(clp_bean.CONTAINER);
		}

		if(requestCode == CreateProductActivity.resultCode){
			if(data.getSerializableExtra("product")==null||data.getLongExtra("pc_id",0)==0){
				return;
			}
			ProductBean returnData = (ProductBean)data.getSerializableExtra("product");
			String pc_id = data.getStringExtra("pc_id");


			CCT_Product product = new CCT_Product();
			product.p_name = returnData.productName;
			product.main_code = returnData.mainCode;
			product.pc_id = data.getLongExtra("pc_id", 0);

//			CCT_CreateCLP.toThis(mActivity, product, interimStr, sBean);

		}
	}
	
	@Override
	public void onBackPressed() {
		showBackDlg();
	}
	
	private void applyParams() {
		mLocBean = (CCT_LocationBean) getIntent().getSerializableExtra("LocationInfo");	
		mTasksBean = (Cycle_Count_Tasks_Bean) getIntent().getSerializableExtra("taskBean");
		mData = new BlindScanDataBean();
		try{
			sBean.slc_id = mLocBean.LOCATION_ID+"";
			sBean.slc_type = mLocBean.LOCATION_TYPE+"";
		}catch (Exception e){
			UIHelper.showToast(getString(R.string.sync_data_error));
			return;
		}
	}


	private void initView() {
		mContext = BlindScanActivity.this;
		conn = new NetConnection_CCT();
		mLv = (ExpandableListView) findViewById(R.id.lvBlindScan);
		mScanAdp = new BlindScanAdp(this, mData, this);
		mLv.setAdapter(mScanAdp);
		mScanAdp.expandAll(mLv);
		
		mEtScanNumber = (SearchEditText) findViewById(R.id.etScanNumber);
		mEtScanNumber.setIconMode(SearchEditText.IconMode_Scan);
		
		findViewById(R.id.btnBlindSubmit).setOnClickListener(this);
		TextView emptyView = (TextView) findViewById(R.id.tvBlindEmpty);
		
		mLv.setEmptyView(emptyView);
		((TextView) vTopbar.findViewById(R.id.topbar_title)).setText(mLocBean==null?"":mLocBean.LOCATION_NAME+"");
		showBackButton(true);
	}

	private void initData() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("TLP", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("CLP", 1));
	}
	
	
	
	private void initListener() {
		findViewById(R.id.topbar_back).setOnClickListener(this);
		mEtScanNumber.setSeacherMethod(new SeacherMethod() {
		@Override
		public void seacher(String value) {
			submitLP(value);
			mEtScanNumber.setText("");
			Utility.colseInputMethod(mActivity, mEtScanNumber);
		}
		});
		
		//------------ 设置listview item的点击事件  点到TLP上 跳转到TLP详情界面
		mLv.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				if(mData.getType(groupPosition) == Cycle_Count_Task_Key.CCT_TLP) {
					enterNextAct(groupPosition, type_new);
				}
				return false;
			}
		});
		
		/*mLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				enterNextAct(position, type_new);
			}
		});*/
		
		/*mLv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				int groupPosition = (Integer) view.getTag(R.layout.cct_blindscan_item_tlp_layout); // 参数值是在setTag时使用的对应资源id号
				int childPosition = (Integer) view.getTag(R.layout.cct_blindscan_item_group_clp_layout);
				showDlg_onLongPress(mData.getType(groupPosition), groupPosition, childPosition);
				return true;
			}
		});*/
		
	}

	
	private void submitLP(final String value) {
		if(TextUtils.isEmpty(value)) return;
		
		boolean isRepeated = checkLPRepeated(value);
		if(isRepeated) return;

		interimStr = ""; //清空

		conn.CCT_GetTLP_Or_CLP_ByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				handleLP(value, json);
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					interimStr = value;
					CCT_Dialog.selectLP_Type(mActivity, value, sBean);
				} else {
					conn.handFail(json);
				}

			}
		}, value);
	}

	/**
	 * 减产LP是否重复添加
	 * @param value
	 */
	private boolean checkLPRepeated(final String value) {
		//-------校验是否重复
		if(isContain(Cycle_Count_Task_Key.CCT_TLP, value)) {  
			mEtScanNumber.setText("");
			
			int grpPosition = mData.getLPRepeatOldPostion(value)[POS_GROUP];
			enterNextAct(grpPosition, type_old); // 扫描发现TLP重复  则直接进入TLP 详情界面  需要跳转进详情界面
			mScanAdp.notifyDataSetChanged();
			return true;
		}
		
		//-------判断去重 CLP
		if(isContain(Cycle_Count_Task_Key.CCT_CLP, value)) {
			TTS.getInstance().speakAll_withToast("Already exists!");
			mEtScanNumber.setText("");
			
			int pos[] = mData.getLPRepeatOldPostion(value);
			mData.setSelectedByPosition(pos[POS_GROUP], pos[POS_CHILD]);
			mScanAdp.notifyDataSetChanged();
			mLv.setSelection(pos[POS_GROUP]);
			return true;
		}
		
		// 清除selected 状态
		mScanAdp.clearSeletedStatus();
		return false;
	}
	
	/**
	 * LP请求完成后 对有正确返回数据的LP 进行后续处理(1.添加操作   2.替换操作)
	 * @param value
	 * @param json
	 */
	private void handleLP(final String value, JSONObject json) {
		BlindCLPResultBean resultBean = new Gson().fromJson(json.toString(), BlindCLPResultBean.class);
		if(resultBean.DATA==null) {
			TTS.getInstance().speakAll_withToast("Data Not Found!");
			return;
		}
		
		
		//--------------------------是TLP-----------------------------------------
		
		if(resultBean.DATA.CONTAINER_TYPE == Cycle_Count_Task_Key.CCT_TLP) {
			CCT_TLP_Bean tlpBean = new CCT_TLP_Bean();
			tlpBean.CONTAINER = value; 
			tlpBean.CONTAINER_TYPE = Integer.valueOf(resultBean.DATA.CONTAINER_TYPE);
			tlpBean.CON_ID = resultBean.DATA.CON_ID;
			tlpBean.IS_FULL = resultBean.DATA.IS_FULL;
			tlpBean.IS_HAS_SN = resultBean.DATA.IS_HAS_SN;
			tlpBean.LOT_NUMBER = resultBean.DATA.LOT_NUMBER;
			tlpBean.TYPE_ID = resultBean.DATA.TYPE_ID;
			tlpBean.REAL_NUMBER = CCT_Util.fixLP_Number(resultBean.DATA.CONTAINER);
			
			//----------------添加TLP
			mScanAdp.clearSeletedStatus();
			int selection = addTLP(tlpBean);
			mScanAdp.expandAll(mLv);
			mScanAdp.notifyDataSetChanged();
			mLv.setSelection(selection);
			mEtScanNumber.setText("");
			
			enterNextAct(mData.tlpList.size()-1, type_new);
			
		} 
		
		//--------------------------是CLP-----------------------------------------
		
		else if(resultBean.DATA.CONTAINER_TYPE == Cycle_Count_Task_Key.CCT_CLP) {
			CCT_CLP_Bean clpBean = new CCT_CLP_Bean();
			clpBean.CONTAINER = value;
			clpBean.CONTAINER_TYPE = resultBean.DATA.CONTAINER_TYPE;
			clpBean.CON_ID = resultBean.DATA.CON_ID;
			clpBean.IS_FULL = resultBean.DATA.IS_FULL;
			clpBean.IS_HAS_SN = resultBean.DATA.IS_HAS_SN;
			clpBean.LOT_NUMBER = resultBean.DATA.LOT_NUMBER;
			clpBean.TYPE_ID = resultBean.DATA.TYPE_ID;
			clpBean.TYPE_NAME = resultBean.DATA.LP_TYPE_NAME;
			clpBean.REAL_NUMBER = CCT_Util.fixLP_Number(resultBean.DATA.CONTAINER);
			
			// 获得重复的CLP对象
			CCT_CLP_Bean clp = (CCT_CLP_Bean) mData.getRepeatObj(BlindScanTLPDetailBean.Type_CLP, clpBean.REAL_NUMBER);
			if(clp!=null) {
				// 获得重复的CLP的parent(TLP) 对象
				CCT_TLP_Bean parent = mData.getParent(clp.REAL_NUMBER);
				//------------ 一级界面界面 发生替换的情景，必定会有parent
				if(parent!=null) {
					showReplaceDlg(parent, clp);
					return;
				} 
			}
			
			// 清除状态
			mScanAdp.clearSeletedStatus();
			int selection = addCLP(clpBean);
			mScanAdp.notifyDataSetChanged();
			mScanAdp.expandAll(mLv);
			TTS.getInstance().speakAll_withToast("Scan CLP");
			mScanAdp.notifyDataSetChanged();
			if(selection > -1) {
				mLv.setSelection(selection);
			}
		}
	}
	
	private int type_new = 0; // 新的             0           tip:进入TLP里时 判断是新添加TLP后进入的,还是Already exists后进入的
	private int type_old = 1; // 已经存在的  1
	
	/**
	 * 跳转进TLP详情界面
	 * @param grpPosition
	 * @param type
	 */
	private void enterNextAct(int grpPosition , int type) {
		Intent in = new Intent(BlindScanActivity.this, BlindTLPDetailActivity.class);
		in.putExtra(INTENT_EXTRA_POSITION, grpPosition);
		in.putExtra(INTENT_EXTRA_TLP_DETAIL, (CCT_TLP_Bean)mData.getGrpBeanByPosition(grpPosition));
		in.putExtra(INTENT_EXTRA_TLPNUMBER, ((CCT_TLP_Bean)mData.getGrpBeanByPosition(grpPosition)).REAL_NUMBER);
		in.putExtra(INTENT_EXTRA_TLP_CON_ID, ((CCT_TLP_Bean)mData.getGrpBeanByPosition(grpPosition)).CON_ID+"");
		in.putExtra(INTENT_EXTRA_DATA, mData);
		in.putExtra(INTENT_EXTRA_TASK, mTasksBean);
		in.putExtra(INTENT_EXTRA_LOCATION, mLocBean);
		
		if(type == type_new) {
//			TTS.getInstance().speakAll_withToast("Scan TLP");
			TTS.getInstance().speakAll("Scan TLP");
			UIHelper.showToast("Scan TLP "+ ((CCT_TLP_Bean)mData.getGrpBeanByPosition(grpPosition)).REAL_NUMBER);
		} else {
			TTS.getInstance().speakAll_withToast("Already exists!");
		}
		
		BlindTLPDetailActivity.initParams(in, mData);
		startActivityForResult(in, REQ_DETAIL);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	/**
	 * 判断是否重复输入
	 * @param b
	 * @param scanNumber
	 * @return
	 */
	private boolean isContain(Integer b, String scanNumber) {
		if(b==Cycle_Count_Task_Key.CCT_TLP) return mData.isContainTLPByNo(scanNumber);
		if(b==Cycle_Count_Task_Key.CCT_CLP) return mData.isContainCLPByNo(scanNumber);
		return false;
	}

	/**
	 * 保存从Tlp详情界面返回的数据
	 * @param tlpDetailBean
	 */
	private void saveTlpDetail(BlindScanTLPDetailBean tlpDetailBean, int currPosition) {
		if(tlpDetailBean == null) return;
		CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) mData.getGrpBeanByPosition(currPosition, Cycle_Count_Task_Key.CCT_TLP);
		tlpBean.clpTypeList.clear();
		tlpBean.pdtTypeList.clear();
		tlpBean.pdtNoSnList.clear();
		mScanAdp.notifyDataSetChanged();
		
		tlpBean.clpTypeList.addAll(tlpDetailBean.clpTypeList); 
		tlpBean.pdtTypeList.addAll(tlpDetailBean.pdtTypeList);
		tlpBean.pdtNoSnList.addAll(tlpDetailBean.pdtNoSnList);
		
		mScanAdp = new BlindScanAdp(mContext, mData, BlindScanActivity.this);
		mLv.setAdapter(mScanAdp);
		mScanAdp.expandAll(mLv);
		mScanAdp.notifyDataSetChanged();
		mLv.setSelection(getEndPosition());
	}
	
	/**
	 * 获得最后一个位置的position
	 * 如果没有数据则返回 -1
	 * @return
	 */
	private int getEndPosition() {
		int index = -1;
		int tlpListSize = mData.tlpList.size();
		int clpTypeListSize = mData.clpTypeList.size();
		
		if(tlpListSize ==0 && clpTypeListSize == 0) return index;
		if(tlpListSize > 0 && clpTypeListSize == 0) return tlpListSize-1;
		if(tlpListSize >0 && clpTypeListSize > 0) return tlpListSize + clpTypeListSize -2;
		return index;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBlindSubmit:
			doSubmit();
			break;

		case R.id.topbar_back:
			showBackDlg();
			break;
		}
	}

	/**
	 * 退出的弹出框
	 */
	private void showBackDlg() {
		RewriteBuilderDialog.newSimpleDialog(mContext, "Done?", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			String fileIds = mData.getPicsFileIds();
			if(fileIds.length() > 0) {
				reqDeletePicsByIds(fileIds);
			}
			BlindScanActivity.this.finish();
			overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
		}
		}).show();
	}

	/**
	 * 删除服务端无效图片[提交后了图片，但是没有submit Location, 需要删除服务端上已经上传过的图片]
	 */
	private void reqDeletePicsByIds(String ids) {
		RequestParams p = new RequestParams();
		p.put("file_ids", ids);
		
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
			}
		}.doPost(HttpUrlPath.CCT_delUploadPhotos, p, mContext);
	}
	
	/**
	 * 提交
	 */
	private void doSubmit() {
		
		final JSONObject json = mData.getSubmitJson(mTasksBean, mLocBean);
		if(TextUtils.isEmpty(json.toString())) {
			UIHelper.showToast(mContext, "Create json exception!");
			return;
		}
		
		// 无数据的提交，弹出Dialog 提示是否提交空的location
		if(mData.getAllSize() == 0) {
			showSubmitDlgWithNoData(json);
			return;
		} 
		
		// 有数据的提交
		else {
			showSubmitJsonDlg(json);
		}
	}

	/**
	 * 无数据提交, 展示对话框, 提示是否提交空location
	 * @param json
	 */
	private void showSubmitDlgWithNoData(final JSONObject json) {
		new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_notice)).setMessage("Are you sure to submit an empty loaction?")
		.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				submitJson(json);
			}
		}).show();
	}

	/**
	 * 提交的时候弹出dlg 需要确认是否提交
	 * @param json
	 */
	private void showSubmitJsonDlg(final JSONObject json) {
		RewriteBuilderDialog.newSimpleDialog(mContext, getString(R.string.sync_submit)+"?", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			submitJson(json);
		}
		}).show();
	}

	/**
	 * 提交JSON，提交后退回到上一界面
	 * @param json
	 */
	private void submitJson(JSONObject json) {
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				BlindScanActivity.this.finish();
				overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
			}
		}.doPostForYasir(HttpUrlPath.SubmitLocationScan, json, mContext);
	}
	

	private void initTTP(TabToPhoto ttp_dlg,final CCT_CLP_Bean clp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Blind_Key(null, clp.CON_ID+"");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(clp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				List<TTPImgBean> imgs = clp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					mScanAdp.notifyDataSetChanged();
				}
			}
		});
	}
	
	private void initTTP(TabToPhoto ttp_dlg,final CCT_TLP_Bean tlp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Blind_Key(null, tlp.CON_ID+"");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(tlp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				List<TTPImgBean> imgs = tlp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					mScanAdp.notifyDataSetChanged();
				}
			}
		});
	}
	
	/**
	 * 上传图片 
	 * @param b
	 * @param ttp
	 * @param type  Cycle_Count_Task_Key.CCT_TLP   Cycle_Count_Task_Key.CCT_CLP
	 */
	private void reqUploadPhotos(final Object b, final TabToPhoto ttp, final int type) {
		RequestParams p = new RequestParams();
		switch (type) {
		case Cycle_Count_Task_Key.CCT_TLP:
			String fileName_t = mTasksBean.ID +"_" + mLocBean.LOCATION_ID + "_" + ((CCT_TLP_Bean)b).CON_ID;
			p.add("file_name", fileName_t);
			ttp.uploadZip(p, "receive");
			break;

		case Cycle_Count_Task_Key.CCT_CLP:
			String fileName_c = mTasksBean.ID +"_" + mLocBean.LOCATION_ID + "_" + ((CCT_CLP_Bean)b).CON_ID;
			p.add("file_name", fileName_c);
			ttp.uploadZip(p, "receive");
			break;
		}
		
		
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				
				if(type==Cycle_Count_Task_Key.CCT_TLP) {
					((CCT_TLP_Bean)b).photos = listPhotos;
				} else {
					((CCT_CLP_Bean)b).photos = listPhotos;
				}
				mScanAdp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
	@Override
	public void showDlg_photos_clp(final CCT_CLP_Bean clpBean) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, clpBean);
		
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0) reqUploadPhotos(clpBean, ttp, Cycle_Count_Task_Key.CCT_CLP);
				dlg.dismiss();
			}
		});
	}

	@Override
	public void showDlg_photos_pdt(int pdtType, CCT_ProductBean pdtBean) {
	}
	
	@Override
	public void showDlg_photos_tlp(final CCT_TLP_Bean tlpBean) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, tlpBean);
		
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0) reqUploadPhotos(tlpBean, ttp, Cycle_Count_Task_Key.CCT_TLP);
				dlg.dismiss();
			}
		});
	}
	
	/**
	 * 扫描到重复的CLP 提示进行替换
	 */
	private void showReplaceDlg(final CCT_TLP_Bean parent, final CCT_CLP_Bean clpBean) {
		
		String realNumber="";
		if(!TextUtils.isEmpty(parent.REAL_NUMBER)) realNumber = parent.REAL_NUMBER;
		new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_notice)).setMessage("It's already scanned at:\nTLP: "+ realNumber +"\nAre you sure to re-scan?")
		.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 取消  不做操作
				// 清空EditText
				mEtScanNumber.setText("");
				dialog.dismiss();
			}

		}).setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				//TODO 以后需要加上
				/*// 需要先删除被替换的CLP 已经存在服务端上的图片
				if(clpBean.getReqDelPhotosIds().length() > 0) {
					RequestParams p = new RequestParams();
					p.put("file_ids", clpBean.getReqDelPhotosIds());
					new SimpleJSONUtil() {
						@Override
						public void handReponseJson(JSONObject json) {
						}
					}.doPost(HttpUrlPath.CCT_delUploadPhotos, p, mContext);
				}*/
				
				
				// 替换 
				mData.removeBeanByKeywords(BlindTLPDetailActivity.Type_CLP, clpBean.REAL_NUMBER);
				// 再添加
				clpBean.clearInfo();
				mScanAdp.clearSeletedStatus();
				int selection = addCLP(clpBean);
				TTS.getInstance().speakAll_withToast("Scan CLP");
				mScanAdp.expandAll(mLv);
				mScanAdp.notifyDataSetChanged();
				if(selection != -1) {
					mLv.setSelection(selection);
				}
				mEtScanNumber.setText("");
				
				dialog.dismiss();
				
			}
		})
		.show();
	}
	
	
	
	
	/**
	 * 添加TLP  TLP不需要分组，所以只需要进行过当前界面的去重即可
	 * @param tlpBean
	 */
	public int addTLP(CCT_TLP_Bean tlpBean) {
		mData.tlpList.add(tlpBean);
		return mData.tlpList.size() - 1;
	}
	
	/**
	 * 添加CLP  CLP需要分组  根据请求返回的type_id进行分组
	 * @param clpBean
	 * return 插入的position
	 */
	public int addCLP(CCT_CLP_Bean clpBean) {
		// 获得CLP的  typeId
		int type_id = clpBean.TYPE_ID;
		
		int insertGrpPos = -1;
		// 是否插入成功
		boolean isInserted = false;
		for (CCT_CLP_Type_Bean clpTypeBean : mData.clpTypeList) {
			if (clpTypeBean.CONTAINER_TYPE_ID == type_id ) {
				clpTypeBean.CONTAINERS.add(clpBean);
				isInserted = true;
				break;
			}
			
			// 记录插入的位置
			if(!isInserted) {
				insertGrpPos++;
			}
		}
		
		
		// 判断是否已经根据type_id插入到CLP CONTAINERS中， 如果没有则需要添加一个新的
		if(!isInserted) {
			CCT_CLP_Type_Bean newClpType = new CCT_CLP_Type_Bean();
			newClpType.CONTAINER_TYPE_ID = type_id;
			newClpType.LP_TYPE_NAME = clpBean.TYPE_NAME;
			newClpType.containerType = Cycle_Count_Task_Key.CCT_CLP;
			newClpType.CONTAINERS.add(clpBean);
			mData.clpTypeList.add(newClpType);
			return getEndPosition();
		} else {
			if(mData.tlpList.size() > 0) {
				return  mData.tlpList.size() -1 + insertGrpPos;
			} else {
				return insertGrpPos;
			}
		}
	}


	@Override
	public void showEditDlg(int grpPosition, int pdtType, CCT_ProductBean pdtBean) {
	}

	@Override
	public void showDlg_onClickStatusIcon(int type, final int groupPosition, final int childPosition) {
		switch (type) {
		case Cycle_Count_Task_Key.CCT_TLP:
			
			RewriteBuilderDialog.newSimpleDialog(mContext, "Are you sure to delete it?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mData.removeBeanByPosition(groupPosition, childPosition);
					mScanAdp = new BlindScanAdp(mContext, mData, BlindScanActivity.this);
					mLv.setAdapter(mScanAdp);
					mScanAdp.expandAll(mLv);
					mScanAdp.notifyDataSetChanged();
				}
			}).show();
			
			break;

		case Cycle_Count_Task_Key.CCT_CLP:
			
			RewriteBuilderDialog.newSimpleDialog(mContext, "Are you sure to delete it?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// = -1 删除组， >0 删除child
					if(childPosition == -1) {
						mData.removeBeanByPosition(groupPosition, -1);
					} else if(childPosition >=0) {
						mData.removeBeanByPosition(groupPosition, childPosition);
					}
					mScanAdp = new BlindScanAdp(mContext, mData, BlindScanActivity.this);
					mLv.setAdapter(mScanAdp);
					mScanAdp.expandAll(mLv);
					mScanAdp.notifyDataSetChanged();
				}
			}).show();
			break;
		}
		
	}

}
