package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Task_Inventory_AreasBean 
 * @Description: 
 * @author gcy
 * @date 2015-2-27 下午6:52:25
 */
public class CCT_AreasBean implements Serializable{
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -8664995537964155702L;
	public List<CCT_LocationBean> LOCATIONS = new ArrayList<CCT_LocationBean>();
	public String AREA_NAME;//": "1-B3",
	public long AREA_ID;//": 1000265,
	public int TOTAL_LOCATIONS;
	public int SCANNED_LOCATIONS;
	public String CLOSE_USER;
	public int AREA_TYPE;//": 1
	public int STATUS;//": 1

	
//	public static void helpJson(JSONObject json, List<CCT_AreasBean> list){
//		JSONArray jArray = json.optJSONArray("AREAS");
//		CCT_AreasBean.resolveJson(jArray,list);
//	}
//	
//	public static void resolveJson(JSONArray jArray,List<CCT_AreasBean> list){
//		if(!StringUtil.isNullForJSONArray(jArray)){
//			for(int i=0;i<jArray.length();i++){
//				JSONObject jObj = jArray.optJSONObject(i);
//				CCT_AreasBean t = new CCT_AreasBean();
//				t.AREA_NAME = jObj.optString("AREA_NAME");
//				t.AREA_ID = jObj.optString("AREA_ID");
//				t.TOTAL_LOCATIONS = jObj.optInt("LOCATION_COUNT");
//				t.SCANNED_LOCATIONS = jObj.optInt("CLOSE_LOCATION_COUNT");
//				t.CLOSE_USER = jObj.optString("CLOSE_USER");
//				t.STATUS = jObj.optInt("STATUS");
//				CCT_LocationBean.helpJson(jObj, t.LOCATIONS);
//				list.add(t);
//			}
//		}
//	}
//	
//	/**
//	 * @Description:获取
//	 * @param @return
//	 */
//	public static JSONObject getDebugStr(){
//		JSONObject json = new JSONObject();
//		Random r = new Random();
//		int areaNum = r.nextInt(10) + 2;
//		JSONArray jAreaArray = new JSONArray();
//		try {
//			for (int j = 0; j < areaNum; j++) {
//				JSONObject jArea = new JSONObject();
//				jArea.put("AREA_NAME", "1-B"+(j+1));
//				jArea.put("AREA_ID", "1000265");
//				int area_status = r.nextInt(3)+1;
//				jArea.put("STATUS", area_status);
//				int locationNum = r.nextInt(10)+1;
//				int closeLocationNum = r.nextInt(5)+1;
//				jArea.put("LOCATION_COUNT",locationNum);
//				jArea.put("CLOSE_LOCATION_COUNT",closeLocationNum);
//				if(area_status == Cycle_Count_Task_Key.Area_SCANNED){
//					jArea.put("CLOSE_USER","Tony Berserker");
//				}else{
//					jArea.put("CLOSE_USER","");
//				}
//				
//				int increaseNum = 0;//增加量
//				JSONArray jLocationArray = new JSONArray();
//				for (int k = 0; k < locationNum; k++) {
//					JSONObject jLocation = new JSONObject();
//					increaseNum ++;
//					jLocation.put("LOCATION_FULL_NAME", "Valley1-B3B051"+(k+1));
//					jLocation.put("LOCATION_NAME", "B3B051"+(k+1));
//					jLocation.put("LOCATION_ID", "1006261");
//					jLocation.put("STATUS", (increaseNum>=closeLocationNum)?1:3);
//					jLocationArray.put(jLocation);
//				}
//				jArea.put("LOCATIONS", jLocationArray);
//				jAreaArray.put(jArea);
//			}
//			json.put("DATA", jAreaArray);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		return json;
//	}
}
