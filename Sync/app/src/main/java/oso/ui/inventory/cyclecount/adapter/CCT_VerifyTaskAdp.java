package oso.ui.inventory.cyclecount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.iface.VerifyIface;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import support.common.UIHelper;
import utility.StringUtil;
import utility.Utility;

public class CCT_VerifyTaskAdp extends BaseExpandableListAdapter {

	private CCT_VerifyTaskAdp oThis;

	private Context content;
	private List<CCT_ContainerBean> listBean;
	private LayoutInflater inflater;
	
	private final int TLP = 0;
	private final int CLP = 1;
	
	private VerifyIface iface;
	
	@Override //不能超过Count数量
	public int getGroupType(int groupPosition) {
		// TODO Auto-generated method stub
		return listBean.get(groupPosition).containerType==Cycle_Count_Task_Key.CCT_TLP?TLP:CLP;
	}

	@Override
	public int getGroupTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	/**
	 * @Description:构造方法
	 */
	public CCT_VerifyTaskAdp(Context context, List<CCT_ContainerBean> listBean,VerifyIface iface) {
		this.content = context;
		this.listBean = listBean;
		this.inflater = LayoutInflater.from(content);
		this.iface = iface;
		oThis = this;
	}

	public void setDataList(List<CCT_ContainerBean> listBean){
		this.listBean = listBean;
		oThis.notifyDataSetChanged();
	}
	
	// ====================



	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		Holder_Grp holder;
		if (convertView == null) {
			holder = new Holder_Grp();
			convertView = initGroupView(groupPosition, holder);
			convertView.setTag(holder);
		} else{
			holder = (Holder_Grp) convertView.getTag();
		}
		fillGroupViewData(isExpanded, groupPosition, holder);

		return convertView;
	}

	/**
	 * @Description:加载父级控件
	 */
	private View initGroupView(int groupPosition, Holder_Grp holder) {
		View convertView = null;
		switch (getGroupType(groupPosition)) {
		case TLP:
			convertView = inflater.inflate(R.layout.cct_tlp_item, null);
			
			holder.tlp_layout = (View) convertView.findViewById(R.id.tlp_layout);
			holder.tlp_number = (TextView) convertView.findViewById(R.id.tlp_number);
			holder.is_new = (View) convertView.findViewById(R.id.is_new);
			
			holder.is_damage_btn = (Button) convertView.findViewById(R.id.is_damage_btn);
			holder.image_btn = (View) convertView.findViewById(R.id.image_btn);
			holder.iv = (ImageView) convertView.findViewById(R.id.iv);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
			
			break;
		case CLP:
			convertView = inflater.inflate(R.layout.cct_clp_item, null);

//			holder.clp_type = (TextView) convertView.findViewById(R.id.clp_type);
			holder.model_number = (View) convertView.findViewById(R.id.model_number);
			holder.clp_sku = (TextView) convertView.findViewById(R.id.clp_sku);
			holder.clp_type = (TextView) convertView.findViewById(R.id.clp_dimension);
			holder.t_process = (TextView) convertView.findViewById(R.id.t_process);
			holder.t_count = (TextView) convertView.findViewById(R.id.t_count);
			break;
		}

		holder.item = convertView.findViewById(R.id.item);
		holder.select_condition = convertView.findViewById(R.id.select_condition);
		holder.arrow_icon = (ImageView) convertView.findViewById(R.id.arrow_icon);
		holder.lp_status_icon = (ImageView) convertView.findViewById(R.id.lp_status_icon);
		
		return convertView;
	}
	
	/**
	 * @Description:为父级空间添加数据
	 */
	private void fillGroupViewData(boolean isExpanded, final int groupPosition, Holder_Grp holder) {
		// 刷新-数据
		final CCT_ContainerBean c = listBean.get(groupPosition);
		boolean isTLP = (c.containerType == Cycle_Count_Task_Key.CCT_TLP);

		boolean hasChild = false;// 判断是否有子结构
		if (isTLP) {
			final CCT_TLP_Bean tlp = c.tlp;
			holder.tlp_number.setText(Cycle_Count_Task_Key.getContainerTypeValue(tlp.CONTAINER_TYPE) + ": " + tlp.REAL_NUMBER);
			//-------如果丢失则无法继续扫描TLP
			if(c.statusLP == CCT_Container_StatusKey.MISSING){
				holder.item.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(!Utility.isFastClick(500)){
							UIHelper.showToast("TLP is missing");
						}
					}
				});
				holder.is_damage_btn.setVisibility(View.INVISIBLE);
				holder.image_btn.setVisibility(View.INVISIBLE);				
				
			}else{
				holder.item.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(!Utility.isFastClick(500)){
							iface.scanTLP_Info(groupPosition);
						}
					}
				});
				
				holder.is_damage_btn.setVisibility(View.VISIBLE);
				//------判断Damage按钮的背景显示
				holder.is_damage_btn.setBackgroundResource(tlp.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
				holder.is_damage_btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
							tlp.isDamaged = !tlp.isDamaged;

							//xia TLP点击Damaged  不应改变成SCANNED图标，维持原样unscan的图标
						    //c.statusLP = CCT_Container_StatusKey.SCANNED;
							oThis.notifyDataSetChanged();
							if(tlp.isDamaged){
								iface.showTLP_Dlg_photos("",tlp);
							}
					}
				});
				
				
				if(tlp.isDamaged){
					holder.image_btn.setVisibility(View.VISIBLE);
					//------对于Damage的lp进行拍照
					boolean hasPhoto = !Utility.isNullForList(tlp.photos);
				    holder.iv.setSelected(hasPhoto);
				    holder.tvImgCnt.setText(hasPhoto?(tlp.photos.size()+""):"0");
				    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
					holder.image_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!Utility.isFastClick(500)){
								iface.showTLP_Dlg_photos("",tlp);
							}
						}
					});
				}else{
					holder.image_btn.setVisibility(View.INVISIBLE);
				}
				
			}
			
			holder.is_new.setVisibility(tlp.isNewData ? View.VISIBLE : View.GONE);
			
			// -----更改小图标背景
			holder.lp_status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(c.statusLP));
			boolean tlp_disabled = (c.statusLP == CCT_Container_StatusKey.SCANNED||c.statusLP == CCT_Container_StatusKey.PROBLEM);
			holder.select_condition.setOnClickListener(tlp_disabled?null:new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!Utility.isFastClick(500)){
						iface.dialogCondition(groupPosition);
					}
				}
			});
		} else {
			CCT_CLP_Type_Bean clp_type = c.clp_type;
			//-------判断当前model No.是否为空 如果为空则不显示
			if(!StringUtil.isNullOfStr(clp_type.P_NAME)){
				holder.model_number.setVisibility(View.VISIBLE);
				holder.clp_sku.setText(clp_type.P_NAME);
			}else{
				holder.model_number.setVisibility(View.GONE);
			}
			
			holder.clp_type.setText(clp_type.LP_TYPE_NAME);
			// -----更改小图标背景
			holder.lp_status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(c.statusLP));
			
			hasChild = !Utility.isNullForList(clp_type.CONTAINERS);
			
//			holder.select_condition.setVisibility(View.VISIBLE);
			holder.select_condition.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!Utility.isFastClick(500)){
						iface.dialogCondition(groupPosition);
					}
				}
			});

			holder.t_process.setText(jumpScanSize(clp_type.CONTAINERS)+"");
			holder.t_count.setText(" / "+clp_type.CONTAINERS.size());
			
			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || !hasChild;
			holder.item.setBackgroundResource(showOne ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
			// 更改箭头方向
			holder.arrow_icon.setBackgroundResource(showOne ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
		}
	}


	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGroupCount() {
		return Utility.isNullForList(listBean) ? 0 : listBean.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	/**********************************************************************************************************************/
	
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if (listBean.get(groupPosition).containerType == Cycle_Count_Task_Key.CCT_TLP) {
			return 0;
		} else {
			CCT_CLP_Type_Bean c = (listBean.get(groupPosition).clp_type);
			return Utility.isNullForList(c.CONTAINERS) ? 0 : c.CONTAINERS
					.size();
		}
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		Holder_Child holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.cct_verify_clp_item, null);
			holder = new Holder_Child();
			holder.item = convertView.findViewById(R.id.item);
			
			holder.image_btn = (View) convertView.findViewById(R.id.image_btn);
			holder.iv = (ImageView) convertView.findViewById(R.id.iv);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
			
			holder.select_clp_condition = convertView
					.findViewById(R.id.select_clp_condition);
			holder.clp_status_icon = (ImageView) convertView
					.findViewById(R.id.clp_status_icon);
			holder.clp_number = (TextView) convertView
					.findViewById(R.id.clp_number);
			holder.is_damage_btn = (Button) convertView
					.findViewById(R.id.is_damage_btn);

			holder.is_new = (View) convertView.findViewById(R.id.is_new);
			
			convertView.setTag(holder);
		} else
			holder = (Holder_Child) convertView.getTag();

		// 刷新-视图
		final CCT_CLP_Type_Bean c = listBean.get(groupPosition).clp_type;
		final CCT_CLP_Bean clp = c.CONTAINERS.get(childPosition);

		//------点击CLP并弹出CLP的操作
		holder.select_clp_condition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!Utility.isFastClick(500)){
					iface.dialogCondition(groupPosition, childPosition);
				}
			}
		});
		
		if(clp.statusLP==CCT_Container_StatusKey.MISSING){
			holder.is_damage_btn.setVisibility(View.INVISIBLE);
			holder.image_btn.setVisibility(View.INVISIBLE);
		}else{
			holder.is_damage_btn.setVisibility(View.VISIBLE);
			//------判断Damage按钮的背景显示
			holder.is_damage_btn.setBackgroundResource(clp.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
			holder.is_damage_btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
						clp.isDamaged = !clp.isDamaged;
						oThis.notifyDataSetChanged();
						iface.changeScanned(groupPosition, childPosition);
						if(clp.isDamaged){
							iface.showCLP_Dlg_photos(c.MODEL_NUMBER,clp);
						}
				}
			});
			
			
			if(clp.isDamaged){
				holder.image_btn.setVisibility(View.VISIBLE);
				//------对于Damage的lp进行拍照
				boolean hasPhoto = !Utility.isNullForList(clp.photos);
			    holder.iv.setSelected(hasPhoto);
			    holder.tvImgCnt.setText(hasPhoto?(clp.photos.size()+""):"0");
			    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
				holder.image_btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(!Utility.isFastClick(500)){
							iface.showCLP_Dlg_photos(c.MODEL_NUMBER,clp);
						}
					}
				});
			}else{
				holder.image_btn.setVisibility(View.INVISIBLE);
			}
		}
		
		

		holder.is_new.setVisibility(clp.isNewData?View.VISIBLE:View.GONE);
		
		
		// -----更改小图标背景
		holder.clp_status_icon
				.setBackgroundResource(clp.statusLP == CCT_Container_StatusKey.UNSCANNED ? R.drawable.ic_cct_unscanned
						: (clp.statusLP == CCT_Container_StatusKey.SCANNED ? R.drawable.ic_cct_scanned
								: R.drawable.ic_cct_missing));
		holder.clp_number.setText(Cycle_Count_Task_Key.getContainerTypeValue(clp.CONTAINER_TYPE)+": "+clp.REAL_NUMBER);

		// 背景
		if (getChildrenCount(groupPosition) - 1 == childPosition) {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
		} else {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
		}

		return convertView;
	}



	
	
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}
	
	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----打开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }
	 
	 public int jumpScanSize(List<CCT_CLP_Bean> list){
		 if(Utility.isNullForList(list)){
			 return 0;
		 }
		 int a = 0;
		 for (int i = 0; i < list.size(); i++) {
			if(list.get(i).statusLP!=CCT_Container_StatusKey.UNSCANNED){
				a++;
			}
		 }
		 return a;
	 }
	
};

class Holder_Grp {
	View item;
	View select_condition;
	ImageView arrow_icon;
	ImageView lp_status_icon;
	
	// -------TLP
	TextView tlp_number;
	View tlp_layout;
	View is_new;
	
	Button is_damage_btn;
	View image_btn;
	ImageView iv;
	TextView tvImgCnt;

	// -------CLP
	View model_number;
//	TextView clp_type;
	TextView clp_sku;
	TextView clp_type;
	TextView t_process;
	TextView t_count;

}

class Holder_Child {
	View item;
	View select_clp_condition;
	ImageView clp_status_icon;
	TextView clp_number;
	
	Button is_damage_btn;
	View image_btn;
	ImageView iv;
	TextView tvImgCnt;

	View is_new;

}