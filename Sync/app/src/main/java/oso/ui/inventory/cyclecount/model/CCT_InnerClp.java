package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * @author gcy
 * @ClassName: CCT_TLP_Bean
 * @Description:
 * @date 2015-4-8 下午10:04:49
 */
public class CCT_InnerClp implements Serializable {

    private static final long serialVersionUID = -1689045914265457557L;

    public int basic_height;// 2,
    public int sort;// 4,
    public double total_weight;// 3244,
    public int ibt_weight;// 20,
    public int pc_id;// 1002445,
    public int ibt_length;// 48,
    public int stack_length_qty;// 1,
    public int level;// 3,
    public int basic_weight_uom;// 2,
    public int ibt_width;// 40,
    public int length_uom;// 4,
    public String lp_name;// "CLP1*1*1[48X40][CLP1*2*1[CLP0909][CLP1*2*8[72x45]]]",
    public int inner_total_pc;// 32,
    public int container_type;// 1,
    public int basic_length;// 48,
    public int basic_length_uom;// 4,
    public int stack_width_qty;// 1,
    public int basic_width;// 40,
    public int is_has_sn;// 1,
    public int inner_total_lp;// 1,
    public int basic_weight;// 20,
    public int inner_pc_or_lp;// 10000,
    public int stack_height_qty;// 1,
    public int weight_uom;// 2,
    public int basic_type_id;// 2,
    public int active;// 1,
    public int ibt_height;// 2,
    public int lpt_id;// 10001

}
