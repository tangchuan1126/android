package oso.ui.inventory.cyclecount;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.debug.product.CreateProductActivity;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.inventory.cyclecount.adapter.CCT_ScanTLP_Adp;
import oso.ui.inventory.cyclecount.iface.VerifyScanTLP_Iface;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_Product;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Dialog;
import oso.ui.inventory.cyclecount.util.CCT_JudgeIcon;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: CCT_Verify_Scan_TLP 
 * @Description: 扫描TLP
 * @author gcy
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_Verify_Scan_TLP extends BaseActivity implements VerifyScanTLP_Iface{
	
	//---------来自与上一页面 的数据
	public static List<CCT_ContainerBean> listBean;
	private int selectGroupNum;
	private ScanLocationDataBean sBean;
	private boolean isVerify;
	
	//---------本页面的数据
	public static CCT_TLP_Bean tlp;
	public static List<CCT_ContainerBean> tlpBean_CPlist;
	
	private SingleSelectBar singleSelectBar;// 选项卡

	private SearchEditText search_view;//搜索框
	private View sku_layout;
	private SearchEditText search_model_edit;
	private SearchEditText search_lot_edit;
	
	private ExpandableListView listview;
	private CCT_ScanTLP_Adp adps;
	
	//-----------导航默认值
	private final int CLP = 0;
	private final int SKU = 1;
	private int selectNum = CLP;

	private TabToPhoto ttp;

	private Button submit_btn;
	
	//----用于弹出窗体
	private Dialog showDialog;

	public static Map<String, String> indexMap;

	private CCT_LocationBean mLocation;
	private Cycle_Count_Tasks_Bean mTaskBean;

	private NetConnection_CCT conn;
	private String interimStr; //用于保存临时的code

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cct_verify_scan_tlp, 0);
		getFromActivityData(getIntent());
	}
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	@SuppressWarnings("unchecked")
	private void getFromActivityData(Intent intent) {
		listBean = (List<CCT_ContainerBean>) intent.getSerializableExtra("listBean");
		sBean = (ScanLocationDataBean) intent.getSerializableExtra("ScanLocationDataBean");
		selectGroupNum = intent.getIntExtra("selectGroupNum", 0);
		isVerify = intent.getBooleanExtra("isVerify", true);

		mLocation = (CCT_LocationBean) intent.getSerializableExtra("LocationBean");
		mTaskBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("TaskBean");

		tlp = listBean.get(selectGroupNum).tlp;

		//-------拆分tlp转换数据结构 改装成CTNR集合
		tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);

		indexMap = CCT_Util.getIndexMap(listBean,tlp);

		setTitleString(Cycle_Count_Task_Key.getContainerTypeValue(tlp.CONTAINER_TYPE) + ": " + tlp.REAL_NUMBER);
		initView();
		initListener();
	}



	/**
	 * @Description:初始化UI
	 */
	private void initView(){
		conn = new NetConnection_CCT();

		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDialog();
			}
		});

		// 事件
		showRightButton(R.drawable.menu_btn_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CCT_Dialog.showRightDialog(mActivity,sBean,tlp);//因为不是扫描所以默认传空
			}
		});

		search_view = (SearchEditText) findViewById(R.id.search_edit);
		
		sku_layout = (View) findViewById(R.id.sku_layout);
		search_model_edit = (SearchEditText) findViewById(R.id.search_model_edit);
		search_lot_edit = (SearchEditText) findViewById(R.id.search_lot_edit);
		
		search_view.setIconMode(SearchEditText.IconMode_Scan);
		search_view.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				if(StringUtil.isNullOfStr(value)){
					return;
				}

				doScanCLP(value);
				search_view.setText("");
				search_view.requestFocus();
				Utility.colseInputMethod(mActivity, search_view);
			}
		});
		search_model_edit.setIconMode(SearchEditText.IconMode_Scan);
		search_lot_edit.setIconMode(SearchEditText.IconMode_Scan);
		
		//----设置key事件防止会车的时候触发Submit方法
		search_model_edit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					//-----如果为空则锁定当前焦点 如果不为空则焦点下移
					String value = search_model_edit.getText().toString();
					//----------判断是否是procode
					if(!StringUtil.isNullOfStr(value)){
						search_model_edit.requestFocus();
					}
					Utility.colseInputMethod(mActivity, search_model_edit);
				}
				return false;
			}
		});
		
		search_model_edit.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				if(StringUtil.isNullOfStr(value)){
					UIHelper.showToast(getString(R.string.cct_prompt_modelno));
					return;
				}

				getSKU_And_Submit(value, search_lot_edit.getText().toString());
				//--------清空并锁定焦点
				setEmpty(search_model_edit);
				Utility.colseInputMethod(mActivity, search_model_edit);
			}
		});
		search_lot_edit.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
//
				if (StringUtil.isNullOfStr(search_model_edit.getText().toString())) {
					UIHelper.showToast(getString(R.string.cct_prompt_modelno));
					search_lot_edit.clearFocus();
					search_model_edit.requestFocus();
					return;
				}

				getSKU_And_Submit(search_model_edit.getText().toString(), value);
				//--------清空并锁定焦点
				setEmpty(search_lot_edit);
				Utility.colseInputMethod(mActivity, search_lot_edit);
			}
		});
		
		// 全大写
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		search_view.setTransformationMethod(trans);
		search_model_edit.setTransformationMethod(trans);
		search_lot_edit.setTransformationMethod(trans);

		listview = (ExpandableListView) findViewById(R.id.listview);
//		
//		//实例化adp
		adps = new CCT_ScanTLP_Adp(mActivity, tlpBean_CPlist, this,isVerify);
		listview.setAdapter(adps);
//		
//		//张开全部
		adps.expandAll(listview);
		initSingleSelectBar();
		
		//--------提交
		submit_btn = (Button) findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UIHelper.showToast(getString(R.string.sync_success));
				finishThisAct();
			}
		});
		showSubmitButton();
	}

	private void initListener() {
		/**
		 * group为有SN的product  则跳到 有SN/无SN的界面
		 */
		listview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				CCT_ContainerBean cct_containerBean = tlpBean_CPlist.get(groupPosition);
				CCT_ProductBean product_type = cct_containerBean.product_type;

				if (product_type == null) {
					return false;
				}

//				if (!Utility.isNullForList(product_type.SN)) {
				Intent intent = new Intent(mActivity, CCT_Container_Product.class);
				intent.putExtra("pro_index", groupPosition);
				intent.putExtra("LocationBean", mLocation);
				intent.putExtra("TaskBean", mTaskBean);
				startActivityForResult(intent, CCT_Container_Product.resultCode);
//				} else {
//					return false;
//				}
				return true;
			}
		});
	}
	
	/**
	 * @Description:清空文本框并根据所传的View 来判焦点锁定在哪里
	 */
	private void setEmpty(SearchEditText view){
		search_model_edit.setText("");
		search_lot_edit.setText("");
		view.requestFocus();		
	}
	
	/**
	 * @Description:初始化singleBarSelect
	 */

	private void initSingleSelectBar() {
		singleSelectBar = (SingleSelectBar)mActivity.findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("CLP", CLP));
		clickItems.add(new HoldDoubleValue<String, Integer>("SKU", SKU));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(
					HoldDoubleValue<String, Integer> selectValue) {
				selectNum = selectValue.b;
				boolean isShowSkuView = (selectNum == SKU);

				if (isShowSkuView) {
					search_view.setVisibility(View.GONE);
					sku_layout.setVisibility(View.VISIBLE);
					setEmpty(search_model_edit);
				} else {
					sku_layout.setVisibility(View.GONE);
					search_view.setHint("Scan " + selectValue.a);
					search_view.setVisibility(View.VISIBLE);
					search_view.requestFocus();
				}
			}
		});
		
		singleSelectBar.userDefineSelectIndexExcuteClick(selectNum);
	}
	
	/**
	 * @Description:通过model no 和 lot no来获取产品信息
	 * @param @param modelNo
	 * @param @param lotNo
	 */
	private void getSKU_And_Submit(String modelNo,String lotNo) {
		if(StringUtil.isNullOfStr(modelNo)){
			modelNo = "";
		}
		
		String procode = modelNo;
		if(!StringUtil.isNullOfStr(lotNo)){
			procode = modelNo +"/"+ lotNo;
		}
		
		submitSKU(procode.toUpperCase(Locale.ENGLISH));
	}
	
	/**
	 * @Description:通过产品的code 来获取产品的相关信息
	 */
	private void submitSKU(final String procode) {
		conn.CCT_GetProductByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				CCT_ProductBean pro = CCT_Util.dealWithConPro(json.optJSONObject("DATA"), procode);
				pro.IS_NEW = true;
				addProductCode(pro, procode);
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json == null) return;
				int msgErr = json.optInt("status", 0);
				if (msgErr == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Create Product?", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							CreateProductActivity.toThis(mActivity, false, CCT_Dialog.requestCode_Sku, procode, CCT_Util.getModelNo(procode));
						}
					});
				} else {
					JSONArray jArray = json.optJSONArray("errors");
					if (!StringUtil.isNullForJSONArray(jArray)) {
						JSONObject jObj = jArray.optJSONObject(0);
						//特定错误 如果为37则提示选择title 也就是重新弹出界面选择title
						int SELECT_TITLE = 37;
						if (jObj.optInt("errorCode", 0) == SELECT_TITLE) {
							CCT_Dialog.selTitleDialog(mActivity, procode, "", sBean, tlp, new CCT_Dialog.CCT_DialogInterface() {
								@Override
								public void doSomething(JSONObject json) {
									CCT_ProductBean pro = CCT_Util.dealWithConPro(json.optJSONObject("DATA"), procode);
									addProductCode(pro, procode);
								}
							});
						} else {
							conn.handFail(json);
						}
					}
				}
			}
		}, procode, tlp.CON_ID);
	}
	
	/**
	 * @Description:添加商品
	 * @param @param pro
	 * @param @param procode
	 */
	private void addProductCode(CCT_ProductBean pro,String procode){
		//------校验是否已经存在该商品
		boolean isFound = false;
		int pro_index = -1;
		for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
			if(tlp.PRODUCTS.get(i).PC_ID!=0&&tlp.PRODUCTS.get(i).PC_ID==pro.PC_ID){
				isFound = true;
				pro_index = i;
				break;
			}
		}
		if(!isFound){
			pro.IS_NEW = true;
			tlp.PRODUCTS.add(0,pro);
			tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
			adps.setDataList(tlpBean_CPlist);
		}else{
			CCT_ProductBean oldPro = tlp.PRODUCTS.get(pro_index);
			if(Utility.isNullForList(pro.SN)){
				pro.SN.addAll(oldPro.SN);
				if(!Utility.isNullForMap(pro.SN_Map)&&!Utility.isNullForMap(oldPro.SN_Map)){
					pro.SN_Map.putAll(pro.SN_Map);
				}
				if(!Utility.isNullForMap(pro.NEW_SN_Map)&&!Utility.isNullForMap(oldPro.NEW_SN_Map)){
					pro.NEW_SN_Map.putAll(pro.NEW_SN_Map);
				}
				if(!Utility.isNullForMap(pro.SN_Damaged_Map)&&!Utility.isNullForMap(oldPro.SN_Damaged_Map)){
					pro.SN_Damaged_Map.putAll(pro.SN_Damaged_Map);
				}

				if(!Utility.isNullForMap(pro.SN_photos)&&!Utility.isNullForMap(oldPro.SN_photos)){
					pro.SN_photos.putAll(pro.SN_photos);
				}
			}else{
				String newSN_Str = "";
				for (int i = 0; i < pro.SN.size(); i++) {
					newSN_Str+=("["+pro.SN+"]");
				}
				for (int i = 0; i < oldPro.SN.size(); i++) {
					if(newSN_Str.indexOf(oldPro.SN.get(i))<0){
						String sn = oldPro.SN.get(i);
						pro.SN.add(sn);

						if(!Utility.isNullForMap(oldPro.SN_Map)&&oldPro.SN_Map.get(sn)!=null){
							pro.SN_Map.put(sn, oldPro.SN_Map.get(sn));
						}
						if(!Utility.isNullForMap(oldPro.NEW_SN_Map)&&oldPro.NEW_SN_Map.get(sn)!=null){
							pro.NEW_SN_Map.put(sn, oldPro.NEW_SN_Map.get(sn));
						}
						if(!Utility.isNullForMap(oldPro.SN_Damaged_Map)&&oldPro.SN_Damaged_Map.get(sn)!=null){
							pro.SN_Damaged_Map.put(sn, pro.SN_Damaged_Map.get(sn));
						}

						if(!Utility.isNullForMap(oldPro.SN_photos)&&oldPro.SN_photos.get(sn)!=null){
							pro.SN_photos.put(sn,pro.SN_photos.get(sn));
						}
					}
				}
			}
		}
		
		//张开全部
		adps.expandAll(listview);
		
		int index = -1;
		for (int i = 0; i < tlpBean_CPlist.size(); i++) {
			if(tlpBean_CPlist.get(i).containerType == Cycle_Count_Task_Key.CCT_PRODUCT&&tlpBean_CPlist.get(i).product_type.PC_ID==pro.PC_ID){
				index = i;
				break;
			}
		}
		listview.setSelection(index);

		Intent intent = new Intent(mActivity, CCT_Container_Product.class);
		intent.putExtra("pro_index", index);
		intent.putExtra("LocationBean", mLocation);
		intent.putExtra("TaskBean", mTaskBean);
		startActivityForResult(intent, CCT_Container_Product.resultCode);
	}
	
	/**
	 * @Description:根据输入值扫描TLP 或者 CLP
	 */
	private void doScanCLP(String value){
		if(Utility.isNullForList(tlpBean_CPlist)){
			getLP_Data(value);//网络请求
			return;
		}
		
		//-------判断是否查询到 如果查询不到则请求网络 获取该Container的 详细信息 并作为盈亏处理
		boolean isFound = false;
		int selectGroupNum = 0;
		int selectChildNum = 0;
		
		for (int i = 0; i < tlpBean_CPlist.size(); i++) {
			CCT_ContainerBean c = tlpBean_CPlist.get(i);

			//-------循环判断clp
			if(c.containerType == Cycle_Count_Task_Key.CCT_CLP){
				CCT_CLP_Type_Bean clp_type = c.clp_type;
				if(!Utility.isNullForList(clp_type.CONTAINERS)){
					for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
						CCT_CLP_Bean clp = clp_type.CONTAINERS.get(j);
						if(clp.REAL_NUMBER.equals(value)||clp.CONTAINER.equals(value)){
							isFound = true;
							selectGroupNum = i;
							selectChildNum = j;
							break;
						}
					}
				}
			}
		}
		
		if(!isFound){
			getLP_Data(value);//网络请求
		}else{
			scanCLP(selectGroupNum, selectChildNum,false);
		}
		
	}
	
	/**
	 * @Description:通过网络请求获取LP基础数据信息
	 */
	private void getLP_Data(final String value){

		interimStr = ""; //清空

		conn.CCT_GetTLP_Or_CLP_ByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				JSONObject jObj = json.optJSONObject("DATA");
				if (!jObj.has("CONTAINER_TYPE")) {
					TTS.getInstance().speakAll_withToast(getString(R.string.cct_prompt_contain));
					return;
				}

				if (jObj.optInt("CONTAINER_TYPE") == Cycle_Count_Task_Key.CCT_TLP) {
					TTS.getInstance().speakAll_withToast(getString(R.string.cct_primpt_contain_tlp));
					return;
				}
				if (jObj.optInt("CONTAINER_TYPE") == Cycle_Count_Task_Key.CCT_CLP) {
					//----------解析数据
					final CCT_CLP_Bean clp = new Gson().fromJson(jObj.toString(), new TypeToken<CCT_CLP_Bean>() {
					}.getType());
					clp.REAL_NUMBER = CCT_Util.fixLP_Number(clp.CONTAINER);
					clp.isNewData = true;
					//--新建container类型的基础bean 显示到当前的界面中
					final CCT_ContainerBean c = new CCT_ContainerBean();
					c.containerType = Cycle_Count_Task_Key.CCT_CLP;
					c.clp_type = new CCT_CLP_Type_Bean();
					c.clp_type.LP_TYPE_ID = jObj.optString("TYPE_ID");
					c.clp_type.LP_TYPE_NAME = jObj.optString("LP_TYPE_NAME");
					c.clp_type.P_NAME = jObj.optString("P_NAME");
					c.clp_type.CONTAINERS.add(clp);

					//----添加
					boolean isAdd = true;
					//----提示音
					boolean isBeep = false;
					//----检索clp
					indexCLP(clp, c, isAdd, isBeep);
				}
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json == null) return;
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					interimStr = value;
					CCT_Dialog.selectLP_Type(mActivity, value, sBean, tlp, false, true);
				} else {
					conn.handFail(json);
				}

			}
		}, value);
	}
	
	/**
	 * @Description:检索CLP 是否存在外面的列表中
	 * @param @param jObj
	 */
	private void indexCLP(final CCT_CLP_Bean clp,final CCT_ContainerBean c,final boolean isAdd,final boolean isBeep){

		//---------检索数据
		String indexValue = indexData(clp.CON_ID+"");
		//---------如果都没有则直接添加
		if(!StringUtil.isNullOfStr(indexValue)){
			//------如果查询到并且无父级结构 则直接确认clp真实位置 是否在外面列表中 如果在则不更改 如果不再则使外面的状态变成丢失状态，而里面的则变成新增状态
			if(indexValue.equals("-")){
				boolean isFound = false;
				int clp_type_Index = -1;
				int clp_Index = -1;
				for (int i = 0; i < listBean.size(); i++) {
					if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_CLP){
						for (int j = 0; j < listBean.get(i).clp_type.CONTAINERS.size(); j++) {
							if(listBean.get(i).clp_type.CONTAINERS.get(j).CON_ID==clp.CON_ID){
								isFound = true;
								clp_type_Index = i;
								clp_Index = j;
								break;
							}
						}
					}
				}
				final int indexTypeNum = clp_type_Index;
				final int indexNum = clp_Index;
				//--------如果索引到了 并且已经发现了 
				if(isFound&&indexNum!=-1&&indexTypeNum!=-1){
					final CCT_CLP_Type_Bean type_old = listBean.get(indexTypeNum).clp_type;
					final CCT_CLP_Bean clp_old = type_old.CONTAINERS.get(indexNum);
					//-----如果未扫描 则不给提示,直接更改
					if(clp_old.statusLP==CCT_Container_StatusKey.UNSCANNED){
						listBean.get(indexTypeNum).statusLP = CCT_Container_StatusKey.MISSING;
						clp_old.statusLP = CCT_Container_StatusKey.MISSING;
						compareCLP(clp, c, isAdd, isBeep);
					}
					//-----如果已扫描 则给提示
					else{
						if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
							compareCLP(clp, c, isAdd, isBeep);
							return;
						}
						
						RewriteBuilderDialog.showSimpleDialog(mActivity, "It's already scanned at: \n Loc.:"+sBean.slc_position+"\nAre you sure to re-scan?", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//----如果是新添加的数据不是系统给出的数据则删除  否则标记为丢失
								if(clp_old.isNewData){
									type_old.CONTAINERS.remove(indexNum);
									if(Utility.isNullForList(type_old.CONTAINERS)){
										listBean.remove(indexTypeNum);
									}
									
								}else{

									clp_old.statusLP = CCT_Container_StatusKey.MISSING;
									clp_old.isDamaged = false;
									//-------给父级别增加丢失状态
									if(listBean.get(indexTypeNum).containerType == Cycle_Count_Task_Key.CCT_TLP){
										listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeTLP_TypeIcon(listBean.get(indexTypeNum).tlp);
									}else{
										listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeCLP_TypeIcon(listBean.get(indexTypeNum).clp_type);
									}
								}
								compareCLP(clp, c, isAdd, isBeep);
							}
						});
					}
				}
				//--------如果索引到了 但是没有发现
				else{
					compareCLP(clp, c, isAdd, isBeep);
				}
			}
			//------如果查询到并且有父级结构 则直接确认clp真实位置 是否在外面列表中的TLP上  如果在则不更改 如果不再则使外面的状态变成丢失状态，而里面的则变成新增状态
			else{
				boolean isFound = false;
				CCT_TLP_Bean tlp = null;
				for (int i = 0; i < listBean.size(); i++) {
					if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP&&(listBean.get(i).tlp.CON_ID+"").equals(indexValue)){
						isFound = true;
						tlp = listBean.get(i).tlp;
						break;
					}
				}
				if(isFound){
					final CCT_TLP_Bean oldTlp = tlp;
					//---------clp_type索引
					int clp_type_Index = -1;
					//---------clp索引
					int clp_Index = -1;
					
					for (int i = 0; i < oldTlp.CONTAINER_TYPES.size(); i++) {
						if(!Utility.isNullForList(oldTlp.CONTAINER_TYPES.get(i).CONTAINERS)){
							for (int j = 0; j < oldTlp.CONTAINER_TYPES.get(i).CONTAINERS.size(); j++) {
								if(oldTlp.CONTAINER_TYPES.get(i).CONTAINERS.get(j).CON_ID==clp.CON_ID){
									clp_type_Index = i;
									clp_Index = j;
									break;
								}
							}
						}
					}
					
					//----如果索引均不为-1则表示存在该clp
					if(clp_type_Index!=-1&&clp_Index!=-1){						
						//-----由于回调函数只能应用静态变量 所以需要中转一下该变量 声明一个clp的索引
						final int indexTypeNum = clp_type_Index;
						final int indexNum = clp_Index;
						//-------获取clp_type
						final CCT_CLP_Type_Bean c_type_old = oldTlp.CONTAINER_TYPES.get(indexTypeNum);
						//-------获取clp
						final CCT_CLP_Bean clp_old = c_type_old.CONTAINERS.get(indexNum);
						//------判断是否扫描过该clp 如果未扫描则改变状态
						if(clp_old.statusLP == CCT_Container_StatusKey.UNSCANNED){
							clp_old.statusLP = CCT_Container_StatusKey.MISSING;
							compareCLP(clp, c, isAdd, isBeep);
						}
						//------如果已经扫描过该clp则弹出提示是否确认该clp的真实位置
						else{	
							if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
								compareCLP(clp, c, isAdd, isBeep);
								return;
							}
							RewriteBuilderDialog.showSimpleDialog(mActivity, "It's already scanned at: \n TLP:"+oldTlp.REAL_NUMBER+"\nAre you sure to re-scan?", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									//--------如果是新建数据则直接删除
									if(clp_old.isNewData){
										c_type_old.CONTAINERS.remove(indexNum);
										if(Utility.isNullForList(c_type_old.CONTAINERS)){
											oldTlp.CONTAINER_TYPES.remove(indexTypeNum);
										}
									}
									//--------如果不是新建数据则更改为丢失状态
									else{

										clp_old.statusLP = CCT_Container_StatusKey.MISSING;
										clp_old.isDamaged = false;
										
										//-------给父级别增加丢失状态
										if(listBean.get(indexTypeNum).containerType == Cycle_Count_Task_Key.CCT_TLP){
											listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeTLP_TypeIcon(listBean.get(indexTypeNum).tlp);
										}else{
											listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeCLP_TypeIcon(listBean.get(indexTypeNum).clp_type);
										}
									}
									compareCLP(clp, c, isAdd, isBeep);
								}
							});
						}
					}
					//---------如果不存在则直接添加该clp
					else{
						compareCLP(clp, c, isAdd, isBeep);
					}
					
				}
			}
			
			return;
		}
		
		//-----------没有索引到则直接添加
		compareCLP(clp, c, isAdd, isBeep);
	}

	private void compareCLP(final CCT_CLP_Bean clp, final CCT_ContainerBean c, final boolean isAdd, final boolean isBeep){
		if(tlp.TITLE_ID!=clp.TITLE_ID){
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
			builder.setTitle(getString(R.string.sync_notice));
			builder.setMessage("This CLP has different title with the parent TLP, are you sure to add it?");
			builder.setPositiveButton(getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					clp.TITLE_ID = tlp.TITLE_ID;
					addCLP(clp, c, isAdd, isBeep);
				}
			});
			builder.setNegativeButton(R.string.sync_cancel, null);
			builder.show();
		}else {
			addCLP(clp, c, isAdd, isBeep);
		}
	}

	/**
	 * @Description:添加CLP 到当前列表
	 */
	private void addCLP(CCT_CLP_Bean clp, CCT_ContainerBean c, boolean isAdd, boolean isBeep){
		if(!isAdd){
			if(isBeep){
				if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
					TTS.getInstance().speakAll_withToast("You have already scanned this container.");
				}else{
					TTS.getInstance().speakAll("Scanned");
				}
			}
			
			clp.statusLP = CCT_Container_StatusKey.SCANNED;
			CCT_JudgeIcon.judgeIcon(c);
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
			
			int selectGroupNum = -1;
			int selectChildNum = -1;
			for (int i = 0; i < tlpBean_CPlist.size(); i++) {
				if(tlpBean_CPlist.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP&&tlpBean_CPlist.get(i).clp_type.LP_TYPE_ID.equals(c.clp_type.LP_TYPE_ID)){
					selectGroupNum = i;
					for (int j = 0; j < tlpBean_CPlist.get(i).clp_type.CONTAINERS.size(); j++) {
						if(tlpBean_CPlist.get(i).clp_type.CONTAINERS.get(j).CON_ID==clp.CON_ID){
							selectChildNum = j;
							break;
						}
					}
				}
			}
			
			if(isBeep&&selectGroupNum!=-1&&selectChildNum!=-1){
				if(selectChildNum<5){
					listview.setSelectedGroup(selectGroupNum);
				}else{
					listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
				}	
			}
			return;
		}

		clp.statusLP = CCT_Container_StatusKey.SCANNED;
		
		//----先判断是否有相同的lp_type_id 如果有则直接添加并显示
		for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
			if(tlp.CONTAINER_TYPES.get(i).LP_TYPE_ID.equals(c.clp_type.LP_TYPE_ID)){
				tlp.CONTAINER_TYPES.get(i).CONTAINERS.add(0,clp);
				//-------拆分tlp转换数据结构 改装成CTNR集合
				tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
				adps.setDataList(tlpBean_CPlist);
				//张开全部
				adps.expandAll(listview);
				listview.setSelection(0);
				return;
			}
		}
		
		//----如果没有相同的tlp_type则只能添加到type列表
		tlp.CONTAINER_TYPES.add(0,c.clp_type);
		//-------拆分tlp转换数据结构 改装成CTNR集合
		tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
		adps.setDataList(tlpBean_CPlist);
		//张开全部
		adps.expandAll(listview);
		listview.setSelection(0);
	}
	
	/**
	 * @Description:通过网络请求获取包含该SN的商品的基础数据信息
	 */
	private void getSN_Data(final String value){

		conn.CCT_GetProductBySN(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				JSONObject jobj = json.optJSONObject("DATA");

				CCT_ProductBean pro = new CCT_ProductBean();
				pro.PC_ID = jobj.optLong("PC_ID");
				pro.P_NAME = jobj.optString("P_NAME");
				pro.IS_HAS_SN = jobj.optInt("IS_HAS_SN");
				pro.SN.add(0, value);
				pro.QUANTITY = 1;

				//----添加
				boolean isAdd = true;
				//----提示音
				boolean isBeep = false;
				indexSN(pro, value, isAdd, isBeep);
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if (json == null) return;
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					UIHelper.showToast(mActivity.getString(R.string.sync_not_found));
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Create Product?", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							CreateProductActivity.toThis(mActivity, false);
						}
					});
				} else {
					conn.handFail(json);
				}
			}
		}, value);
	}
	/**
	 * @Description:检索SN
	 * @param @param jobj
	 * @param @param value
	 */
	private void indexSN(final CCT_ProductBean pro, final String value,final boolean isAdd,final boolean isBeep){
		//---------检索数据
		String indexValue = indexMap.get("SN" + value);
		if(!StringUtil.isNullOfStr(indexValue)){
			if(indexValue.indexOf("/")<0){
				return;
			}
			
			String tlp_id = indexValue.split("/")[1];
			if(!StringUtil.isNullOfStr(tlp_id)){
				boolean isFound = false;
				CCT_TLP_Bean tlp = null;
				for (int i = 0; i < listBean.size(); i++) {
					if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP&&(listBean.get(i).tlp.CON_ID+"").equals(tlp_id)){
						isFound = true;
						tlp = listBean.get(i).tlp;
						break;
					}
				}
				
				if(isFound){
					final CCT_TLP_Bean oldTlp = tlp;
					//---------clp_type索引
					int clp_type_Index = -1;
					//---------clp索引
					int clp_Index = -1;
					if(!Utility.isNullForList(oldTlp.PRODUCTS)){
						for (int j = 0; j < oldTlp.PRODUCTS.size(); j++) {
							if(!Utility.isNullForList(oldTlp.PRODUCTS.get(j).SN)){
								for (int i = 0; i < oldTlp.PRODUCTS.get(j).SN.size(); i++) {
									if(oldTlp.PRODUCTS.get(j).SN.get(i).equals(value)){
										clp_type_Index = j;
										clp_Index = i;
										break;
									}
								}
							}else{
								oldTlp.PRODUCTS.get(j).SN_Map.put(value, CCT_Container_StatusKey.MISSING);
							}
							break;
						}
					}
					//----如果索引均不为-1则表示存在该clp
					if(clp_type_Index!=-1&&clp_Index!=-1){
						//-----由于回调函数只能应用静态变量 所以需要中转一下该变量 声明一个clp的索引
						final int indexTypeNum = clp_type_Index;
						final int indexNum = clp_Index;
						
						final CCT_ProductBean p = oldTlp.PRODUCTS.get(indexTypeNum);
						String sn = p.SN.get(indexNum);
						if(p.SN_Map.get(sn)==null||p.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED){
							p.SN_Map.put(sn, CCT_Container_StatusKey.MISSING);
							addSN(pro, value,isAdd,isBeep);
						}else{
							if(pro.SN_Map.get(value)!=null&&pro.SN_Map.get(value)==CCT_Container_StatusKey.SCANNED){
								addSN(pro, value,isAdd,isBeep);
								return;
							}
							RewriteBuilderDialog.showSimpleDialog(mActivity, "It's already scanned at: \n TLP:"+oldTlp.REAL_NUMBER+"\nAre you sure to re-scan?", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									if(p.NEW_SN_Map.get(value)!=null&& p.NEW_SN_Map.get(value)){
										p.NEW_SN_Map.remove(value);
										
										if( p.SN_Map.get(value)!=null) 
											 p.SN_Map.remove(value);
										
										if( p.SN_Damaged_Map.get(value)!=null) 
											 p.SN_Damaged_Map.remove(value);
										
										if( p.SN_photos.get(value)!=null) 
											 p.SN_photos.remove(value);
										
										 p.SN.remove(value);
										
										if(Utility.isNullForList(p.SN)){
											oldTlp.PRODUCTS.remove(indexTypeNum);
										}
									}else{
										p.SN_Map.put(value, CCT_Container_StatusKey.MISSING);
									}
									addSN(pro, value,isAdd,isBeep);
								}
							});
						}
						
					}//---------如果不存在则直接添加该clp
					else{
						addSN(pro, value,isAdd,isBeep);
					}
				}
				return;
			}
			return;
		}
		addSN(pro, value, isAdd, isBeep);
	}
	
	/**
	 * @Description:添加SN
	 * @param @param jobj
	 * @param @param value
	 */
	private void addSN(final CCT_ProductBean pro, String value,boolean isAdd,boolean isBeep){
		if(!isAdd){
			if(isBeep){
				if(pro.SN_Map.get(value)!=null&&pro.SN_Map.get(value)==CCT_Container_StatusKey.SCANNED){
					TTS.getInstance().speakAll_withToast("You have already scanned this container.");
				}else{
					TTS.getInstance().speakAll("Scanned");
				}
			}
			
			int selectGroupNum = -1;
			int selectChildNum = -1;
			for (int i = 0; i < tlpBean_CPlist.size(); i++) {
				if(tlpBean_CPlist.get(i).containerType==Cycle_Count_Task_Key.CCT_PRODUCT&&tlpBean_CPlist.get(i).product_type.PC_ID==pro.PC_ID){
					if(!Utility.isNullForList(pro.SN)){
						for (int j = 0; j < pro.SN.size(); j++) {
							if(pro.SN.get(j).equals(value)){
								selectGroupNum = i;
								selectChildNum = j;
								break;
							}
						}
					}
				}
			}
			
			pro.SN_Map.put(value,CCT_Container_StatusKey.SCANNED);
			CCT_JudgeIcon.judgeIcon(tlpBean_CPlist.get(selectGroupNum));
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
			
			
			if(isBeep&&selectGroupNum!=-1&&selectChildNum!=-1){
				if(selectChildNum<5){
					listview.setSelectedGroup(selectGroupNum);
				}else{
					listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
				}	
			}
			return;
		}
		
		pro.SN_Map.put(value,CCT_Container_StatusKey.SCANNED);
		
		//--------首先判断当前的产品列表中是否有相同的model_number 如果有相同的则添加到当前产品列表中显示
		for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
			if(tlp.PRODUCTS.get(i).PC_ID!=0&&tlp.PRODUCTS.get(i).PC_ID==pro.PC_ID){
				tlp.PRODUCTS.get(i).SN.add(0,value);
				tlp.PRODUCTS.get(i).QUANTITY +=1;
				tlp.PRODUCTS.get(i).SN_Map.put(value,CCT_Container_StatusKey.SCANNED);
				tlp.PRODUCTS.get(i).NEW_SN_Map.put(value,true);
				
				tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
				adps.setDataList(tlpBean_CPlist);
				//张开全部
				adps.expandAll(listview);
				return;
			}
		}
		//--------如果不相同则新建pro种类 添加进行显示

		pro.SN_Map.put(value,CCT_Container_StatusKey.SCANNED);
		pro.NEW_SN_Map.put(value,true);
		
		tlp.PRODUCTS.add(0,pro);
		
		tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
		adps.setDataList(tlpBean_CPlist);
		//张开全部
		adps.expandAll(listview);
		
		int selectGroupNum = -1;
		int selectChildNum = -1;
		for (int i = 0; i < tlpBean_CPlist.size(); i++) {
			if(tlpBean_CPlist.get(i).containerType==Cycle_Count_Task_Key.CCT_PRODUCT&&tlpBean_CPlist.get(i).product_type.PC_ID==pro.PC_ID){
				if(!Utility.isNullForList(pro.SN)){
					for (int j = 0; j < pro.SN.size(); j++) {
						if(pro.SN.get(j).equals(value)){
							selectGroupNum = i;
							selectChildNum = j;
							break;
						}
					}
				}
			}
		}
		
		if(selectGroupNum!=-1&&selectChildNum!=-1){
			if(selectChildNum<5){
				listview.setSelectedGroup(selectGroupNum);
			}else{
				listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
			}	
		}
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 * @param @param selectGroupNum 父级索引
	 * @param @param selectChildNum 子级索引
	 * @param @param scannedStatus 扫描需要更改的状态
	 * @param @param isShowDialog 是否弹出dialog 来操作
	 */
	private void scanCLP(final int selectGroupNum,final int selectChildNum,boolean isShowDialog){
		CCT_CLP_Type_Bean clp_type = tlpBean_CPlist.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		
		//如果是正常扫描则不弹出dialog操作 直接更改状态刷新UI
		if(!isShowDialog){
			changeCLP_Status(selectGroupNum, selectChildNum,CCT_Container_StatusKey.SCANNED,true);
			return;
		}
		
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		b.setMessage(clp.REAL_NUMBER);

		b.setNegativeButton(getString(R.string.sync_cancel), null);
		if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
			b.setArrowItems(
					new String[]{"	Missing"},
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
												int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
								case 0://该容器类别下更改为已扫描状态
									scanStatus = CCT_Container_StatusKey.MISSING;
									break;
								default:
									break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus, false);
						}
					});
		}else if(clp.statusLP==CCT_Container_StatusKey.MISSING){
			b.setArrowItems(
					new String[] { "	Scanned" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}else{
			b.setArrowItems(
					new String[] { "	Scanned","	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							case 1://该容器类别下更改为丢失状态
								scanStatus = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}
		
		b.show();
	}
	
	/**
	 * @Description:改变clp的状态
	 * @param @param selectGroupNum
	 * @param @param selectChildNum
	 * @param @param clp_type
	 * @param @param isBeep 提示音
	 */
	private void changeCLP_Status(int selectGroupNum,int selectChildNum,int scanStatus,boolean isBeep){
		final CCT_CLP_Type_Bean clp_type = tlpBean_CPlist.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		//-------如果是丢失状态则不索引
		if(scanStatus == CCT_Container_StatusKey.MISSING){
			clp.statusLP = scanStatus;
			if(clp.isNewData){
				clp_type.CONTAINERS.remove(selectChildNum);
				if(Utility.isNullForList(clp_type.CONTAINERS)){
					for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
						if(tlp.CONTAINER_TYPES.get(i).LP_TYPE_ID.equals(clp_type.LP_TYPE_ID)){
							tlp.CONTAINER_TYPES.remove(i);
							break;
						}
					}
					tlpBean_CPlist = CCT_ContainerBean.getContainerList(tlp);
					//张开全部
					adps = new CCT_ScanTLP_Adp(mActivity, tlpBean_CPlist, this,isVerify);
					listview.setAdapter(adps);
					adps.expandAll(listview);
					return;
				}
			}else{
				CCT_JudgeIcon.judgeIcon(tlpBean_CPlist.get(selectGroupNum));
			}
			
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);

		}else{
			indexCLP(clp, tlpBean_CPlist.get(selectGroupNum), false, isBeep);
		}
	}
	
	/**
	 * @Description:改变clp的状态
	 * @param @param selectGroupNum
	 * @param @param selectChildNum
	 * @param @param clp_type
	 * @param @param isBeep 提示音
	 */
	private void changeSN_Status(int selectGroupNum,int selectChildNum,int scanStatus,boolean isBeep){
	 	final CCT_ProductBean pro_type = tlpBean_CPlist.get(selectGroupNum).product_type;
		final String sn = pro_type.SN.get(selectChildNum);
		//-------如果是丢失状态则不索引
		if(scanStatus == CCT_Container_StatusKey.MISSING){		
			pro_type.SN_Map.put(sn, scanStatus);
			
			if(pro_type.NEW_SN_Map.get(sn)!=null&&pro_type.NEW_SN_Map.get(sn)){
				pro_type.SN.remove(sn);
				pro_type.NEW_SN_Map.remove(sn);
				pro_type.SN_Map.remove(sn);
				
				if(pro_type.SN_Damaged_Map.get(sn)!=null)
					pro_type.SN_Damaged_Map.remove(sn);
				if(pro_type.SN_photos.get(sn)!=null)
					pro_type.SN_photos.remove(sn);
				
				
				if(Utility.isNullForList(pro_type.SN)){
					for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
						if(tlp.PRODUCTS.get(i).PC_ID==pro_type.PC_ID){
							tlp.PRODUCTS.remove(i);
							break;
						}
					}
					CCT_ContainerBean.removeProduct(tlpBean_CPlist, pro_type.PC_ID);

					//张开全部
					adps = new CCT_ScanTLP_Adp(mActivity, tlpBean_CPlist, this,isVerify);
					listview.setAdapter(adps);
					adps.expandAll(listview);
					return;
				}
			}else{
				CCT_JudgeIcon.judgeIcon(tlpBean_CPlist.get(selectGroupNum));
			}
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
//			//张开全部
			adps.expandAll(listview);
		}else{
			indexSN(pro_type, sn, false, isBeep);
		}
		
	}

	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 * @param @param selectGroupNum 父级索引
	 * @param @param selectChildNum 子级索引
	 * @param @param scannedStatus 扫描需要更改的状态
	 * @param @param isShowDialog 是否弹出dialog 来操作
	 */
	private void scanPro_SN(final int selectGroupNum,final int selectChildNum,boolean isShowDialog){
	 	final CCT_ProductBean pro_type = tlpBean_CPlist.get(selectGroupNum).product_type;
		final String sn = pro_type.SN.get(selectChildNum);
		
		//如果是正常扫描则不弹出dialog操作 直接更改状态刷新UI
		if(!isShowDialog){
			changeSN_Status(selectGroupNum, selectChildNum, CCT_Container_StatusKey.SCANNED, true);
			return;
		}
		
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		b.setMessage("SN: "+sn);

		b.setNegativeButton(getString(R.string.sync_cancel), null);
		if(pro_type.SN_Map.get(sn)!=null&&pro_type.SN_Map.get(sn)==CCT_Container_StatusKey.SCANNED){
			b.setArrowItems(
					new String[]{"	Missing"},
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
												int position, long id) {
							int status = (pro_type.SN_Map.get(sn) == null || pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED) ? CCT_Container_StatusKey.UNSCANNED : pro_type.SN_Map.get(sn);
							switch (position) {
								case 0://该容器类别下更改为已扫描状态
									status = CCT_Container_StatusKey.MISSING;
									break;
								default:
									break;
							}
							changeSN_Status(selectGroupNum, selectChildNum, status, false);
						}
					});
		}else if(pro_type.SN_Map.get(sn)!=null&&pro_type.SN_Map.get(sn)==CCT_Container_StatusKey.MISSING){
			b.setArrowItems(
					new String[] { "	Scanned" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int status = (pro_type.SN_Map.get(sn)==null||pro_type.SN_Map.get(sn)==CCT_Container_StatusKey.UNSCANNED)?CCT_Container_StatusKey.UNSCANNED:pro_type.SN_Map.get(sn);
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								status = CCT_Container_StatusKey.SCANNED;
								break;
							default:
								break;
							}
							changeSN_Status(selectGroupNum, selectChildNum, status, false);
						}
					});
		}else{
			b.setArrowItems(
					new String[] { "	Scanned","	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int status = (pro_type.SN_Map.get(sn)==null||pro_type.SN_Map.get(sn)==CCT_Container_StatusKey.UNSCANNED)?CCT_Container_StatusKey.UNSCANNED:pro_type.SN_Map.get(sn);
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								status = CCT_Container_StatusKey.SCANNED;
								break;
							case 1://该容器类别下更改为丢失状态
								status = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeSN_Status(selectGroupNum, selectChildNum, status, false);
						}
					});
		}
		b.show();
	}
	
	@Override
	public void dialogCondition(int...is) {
		boolean flag = (is!=null&&is.length>0&&!Utility.isNullForList(tlpBean_CPlist));
		if(!flag){
			return;
		}
		//如果长度只有1 则扫描的是CLP或者Product的SKU
		if(is.length==1){
			CTNR_Dialog(tlpBean_CPlist.get(is[0]));
		}
		//如果长度只有2 则扫描的是CLP
		else if(is.length==2){
			if(tlpBean_CPlist.get(is[0]).containerType==Cycle_Count_Task_Key.CCT_PRODUCT){
				scanPro_SN(is[0], is[1],true);
			}else{
				scanCLP(is[0], is[1],true);
			}
		}
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 */
	private void CTNR_Dialog(final CCT_ContainerBean c){
		final RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		final boolean isProdouct = c.containerType==Cycle_Count_Task_Key.CCT_PRODUCT;

		if(isProdouct){
//			String massage = "Model No.: "+c.product_type.P_NAME; // TODO 修改过的
			String massage = "SKU: "+c.product_type.P_NAME;
			if(!Utility.isNullForList(c.product_type.SN)){
				massage+="\nAre all "+c.product_type.SN.size()+" SN there?";
			}
			b.setMessage(massage);
		}else{
			String massage = "LP Type: "+c.clp_type.LP_TYPE_NAME;
			if(!Utility.isNullForList(c.clp_type.CONTAINERS)){
				massage+="\nAre all "+c.clp_type.CONTAINERS.size()+" containers there?";
			}			
			b.setMessage(massage);
		}

		b.setNegativeButton(getString(R.string.sync_cancel), null);
		if(isProdouct&&c.product_type.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_NO_SN){
				String prompt[] = null;
				if(c.statusLP == CCT_Container_StatusKey.MISSING){
					prompt = new String[] { "	Cancel" };
				}else{
					prompt = new String[] { "	Missing" };
				}
				
				b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
											int position, long id) {
						int status = c.statusLP;
						switch (position) {
							case 0://该容器类别下更改为已扫描状态
								if (status == CCT_Container_StatusKey.MISSING) {
									status = CCT_Container_StatusKey.UNSCANNED;
									c.product_type.is_opreation = false;
								} else {
									status = CCT_Container_StatusKey.MISSING;
									c.product_type.TOTAL = 0;
									c.product_type.DAMAGEDCNT = 0;
								}
								break;
							default:
								break;
						}

						b.dismiss();

						c.statusLP = status;
						c.product_type.statusLP = status;

						//----校验是否 显示提交按钮
						showSubmitButton();
						adps.notifyDataSetChanged();
						//张开全部
						adps.expandAll(listview);
					}
				});
		}else{
			if(isProdouct){
				Change_Product_Type_Status(b,c);
			}else {
				Change_CLP_Type_Status(b,c);
			}
		}
	}

	private void Change_Product_Type_Status(RewriteBuilderDialog.Builder b,final CCT_ContainerBean c){
		String str[] = null;
		if(c.statusLP == CCT_Container_StatusKey.UNSCANNED||c.statusLP == CCT_Container_StatusKey.PROBLEM){
			str = new String[] { CCT_Util.Scanned_Str , CCT_Util.Missing_Str};
		}else if(c.statusLP == CCT_Container_StatusKey.SCANNED){
			str = new String[] { CCT_Util.Missing_Str };
		}else{
			str = new String[] { CCT_Util.Scanned_Str };
		}
		final String prompt[] = str;

		b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int status = CCT_Util.getScanStatus(prompt[position]);

				boolean isFound = false;
				if(status==CCT_Container_StatusKey.SCANNED){
					for (int i = 0; i < c.product_type.SN.size(); i++) {
						if(!StringUtil.isNullOfStr(indexMap.get("SN"+c.product_type.SN.get(i)))){
							isFound = true;
							break;
						}
					}

					//------如果发现有存在其他容器内的clp 则提示不继续执行下面的方法
					if(isFound){
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "At least 1 SN under this SKU was already scanned on same location, please scan each SN individually.");
						return;
					}

					for (int i = 0; i < c.product_type.SN.size(); i++) {
						c.product_type.SN_Map.put(c.product_type.SN.get(i),status);
					}
					c.statusLP = status;
				}else if(status==CCT_Container_StatusKey.MISSING){
					if (c.product_type.isAllNew()) {
						tlpBean_CPlist.remove(c);
						tlp.PRODUCTS.remove(c.product_type);
					}else{
						List<String> remove_sns = new ArrayList<String>();
						for (int i = 0; i < c.product_type.SN.size(); i++) {
							String sn = c.product_type.SN.get(i);
							if(c.product_type.NEW_SN_Map.get(sn)!=null&&c.product_type.NEW_SN_Map.get(sn)){
								remove_sns.add(c.product_type.SN.get(i));
							}
						}
						if(!Utility.isNullForList(remove_sns)){
							for (int i = 0; i < remove_sns.size(); i++) {
								c.product_type.SN.remove(remove_sns.get(i));
								c.product_type.NEW_SN_Map.remove(remove_sns.get(i));
								c.product_type.SN_Map.remove(remove_sns.get(i));
								c.product_type.SN_Damaged_Map.remove(remove_sns.get(i));
							}
						}
						for (int i = 0; i < c.product_type.SN.size(); i++) {
							c.product_type.SN_Map.put(c.product_type.SN.get(i),status);
						}
						c.statusLP = status;
					}
				}else{
					for (int i = 0; i < c.product_type.SN.size(); i++) {
						c.product_type.SN_Map.put(c.product_type.SN.get(i),status);
					}
					c.statusLP = status;
				}

				//----校验是否 显示提交按钮
				showSubmitButton();
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);

			}
		});



		b.show();

	}

	/**
	 * 更改CLP的状态
	 */
	private void Change_CLP_Type_Status(RewriteBuilderDialog.Builder b,final CCT_ContainerBean c){
		String str[] = null;
		if(c.statusLP == CCT_Container_StatusKey.UNSCANNED||c.statusLP == CCT_Container_StatusKey.PROBLEM){
			str = new String[] { CCT_Util.Scanned_Str , CCT_Util.Missing_Str};
		}else if(c.statusLP == CCT_Container_StatusKey.SCANNED){
			str = new String[] { CCT_Util.Missing_Str };
		}else{
			str = new String[] { CCT_Util.Scanned_Str };
		}
		final String prompt[] = str;
		b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int status = CCT_Util.getScanStatus(prompt[position]);

				boolean isFound = false;
				if(status==CCT_Container_StatusKey.SCANNED){
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						if(!StringUtil.isNullOfStr(indexMap.get("LP"+c.clp_type.CONTAINERS.get(i).CON_ID))){
							isFound = true;
							break;
						}
					}

					//------如果发现有存在其他容器内的clp 则提示不继续执行下面的方法
					if(isFound){
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "At least 1 CLP under this LP type was already scanned on same location, please scan each LP individually.");
						return;
					}
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						c.clp_type.CONTAINERS.get(i).statusLP = status;
					}
					c.statusLP = status;
				}else if(status==CCT_Container_StatusKey.MISSING){
					if (c.clp_type.isAllNew()) {
//						c.clp_type.CONTAINERS.clear();
						tlpBean_CPlist.remove(c);
						tlp.CONTAINER_TYPES.remove(c.clp_type);
					}else{
						List<CCT_CLP_Bean> remove_cons = new ArrayList<CCT_CLP_Bean>();
						for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
							if(c.clp_type.CONTAINERS.get(i).isNewData){
								remove_cons.add(c.clp_type.CONTAINERS.get(i));
							}
						}
						if(!Utility.isNullForList(remove_cons)){
							for (int i = 0; i < remove_cons.size(); i++) {
								c.clp_type.CONTAINERS.remove(remove_cons.get(i));
							}
						}
						for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
							c.clp_type.CONTAINERS.get(i).statusLP = status;
						}
						c.statusLP = status;
					}
				}else{
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						c.clp_type.CONTAINERS.get(i).statusLP = status;
					}
					c.statusLP = status;
				}

				//----校验是否 显示提交按钮
				showSubmitButton();
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);

			}
		});
		b.show();
	}

	@Override
	public void editTotal(final int groupPosition,String sku) {
		View view = View.inflate(mActivity, R.layout.cct_edit_product_total, null);
		final EditText e_total = (EditText) view.findViewById(R.id.e_total);
		final EditText e_damage = (EditText) view.findViewById(R.id.e_damage);
		final View photo_view = (View) view.findViewById(R.id.photo_view);
		
		final CCT_ProductBean pro = tlpBean_CPlist.get(groupPosition).product_type;
		
		if(pro!=null&&pro.TOTAL!=0){
			e_total.setText(pro.TOTAL+"");
			e_total.setSelection((pro.TOTAL+"").length());
		}
		if(pro!=null&&pro.DAMAGEDCNT!=0){
			e_damage.setText(pro.DAMAGEDCNT+"");
			e_damage.setSelection((pro.DAMAGEDCNT+"").length());
			photo_view.setVisibility(View.VISIBLE);
		}
		
		e_total.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					//-----如果为空则锁定当前焦点 如果不为空则焦点下移
					if(!StringUtil.isNullOfStr(e_total.getText().toString())){
						e_damage.requestFocus();
					}
					Utility.colseInputMethod(mActivity, e_total);
				}
				return false;
			}
		});
		
		e_damage.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					//-----如果为空则锁定当前焦点 如果不为空则焦点下移
					submitTotalAndDamage(groupPosition, pro, e_total, e_damage);
					Utility.colseInputMethod(mActivity, e_damage);
					return true;
				}
				return false;
			}
		});
		
		//---------监听文本控制拍照控件是否显示
		e_damage.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {	
				photo_view.setVisibility(StringUtil.isNullOfStr(e_damage.getText().toString())||"0".equals(e_damage.getText().toString())?View.GONE:View.VISIBLE);
			}
		});
		
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initProNoSn_TTP(ttp, sku, pro);
		
//		showDialog = BottomDialog.showSimpleDialog(mActivity, "Model No.: "+pro.P_NAME, view, false, new OnSubmitClickListener() { // TODO 修改过的
		showDialog = BottomDialog.showSimpleDialog(mActivity, "SKU: "+pro.P_NAME, view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				submitTotalAndDamage(groupPosition, pro, e_total, e_damage);
			}
		});	
		showDialog.setCanceledOnTouchOutside(false);
	}
	/**
	 * @Description:修改没有SN商品数量的方法
	 * @param @param groupPosition
	 * @param @param pro
	 * @param @param e_total
	 * @param @param e_damage
	 */
	private void submitTotalAndDamage(final int groupPosition,final CCT_ProductBean pro,EditText e_total,EditText e_damage){
		Utility.colseInputMethod(mActivity, e_total);
		Utility.colseInputMethod(mActivity, e_damage);
		
		String total = StringUtil.isNullOfStr(e_total.getText().toString())?"0":e_total.getText().toString();
		String damage = e_damage.getText().toString();
		
//		if(!pro.IS_NEW&&"0".equals(total)){
//			UIHelper.showToast("Input total!");
//			e_total.requestFocus();
//			return;
//		}
		if(StringUtil.isNullOfStr(damage)){
			damage = "0";
//			UIHelper.showToast("Input amount of damage!");
//			e_damage.requestFocus();
//			return;
		}
		
		int totalNum = Utility.parseInt(total);
		int damageNum = Utility.parseInt(damage);
		
		if(damageNum>totalNum){
			UIHelper.showToast("Amount of damage cannot exceed the total amount!");
			e_damage.setText("");
			e_damage.requestFocus();
			return;
		}
		
		pro.TOTAL = totalNum;
		pro.DAMAGEDCNT = damageNum;
		
		pro.is_opreation = true;
		
		
//		if(totalNum!=0){
//			tlpBean_CPlist.get(groupPosition).statusLP = CCT_Container_StatusKey.SCANNED;
//			tlpBean_CPlist.get(groupPosition).product_type.statusLP = CCT_Container_StatusKey.SCANNED;
//		}else{
//			tlpBean_CPlist.get(groupPosition).statusLP = CCT_Container_StatusKey.UNSCANNED;
//			tlpBean_CPlist.get(groupPosition).product_type.statusLP = CCT_Container_StatusKey.UNSCANNED;
//		}
		
		tlpBean_CPlist.get(groupPosition).product_type.statusLP = CCT_JudgeIcon.judgePro_TypeIcon(tlpBean_CPlist.get(groupPosition).product_type);
		tlpBean_CPlist.get(groupPosition).statusLP = tlpBean_CPlist.get(groupPosition).product_type.statusLP;
		
		if(damageNum!=0){
			tlpBean_CPlist.get(groupPosition).product_type.isDamaged = true;
		}else{
			tlpBean_CPlist.get(groupPosition).product_type.isDamaged = false;
		}

		dismiss();
		//----校验是否 显示提交按钮
		showSubmitButton();
		adps.notifyDataSetChanged();
		//张开全部
		adps.expandAll(listview);
		
		if (ttp.getPhotoCnt(false) > 0)
			reqUploadProNoSn_Photos(pro, ttp);
	}
	
	@Override
	public void onBackPressed() {
		closeDialog();
	}
	
	/**
	 * @Description:关闭当前页面
	 */
	private void closeDialog(){
		RewriteBuilderDialog.newSimpleDialog(mActivity, "Done?", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finishThisAct();
			}
		}).show();
	}
	
	/**
	 * @Description:关闭当前页面
	 */
	private void finishThisAct(){
		final Intent intent = new Intent();
		
		if(isVerify){
			intent.setClass(mActivity, CCT_VerifyTask.class);
		}else{
			intent.setClass(mActivity, VerificationTaskActivity.class);
		}
		
		if(CCT_Util.checkVerifyData(tlpBean_CPlist)){
			listBean.get(selectGroupNum).statusLP = CCT_Container_StatusKey.SCANNED;
		}
		
//		int scanA =  CCT_JudgeIcon.judgeTLP_TypeIcon(tlp);
		//如果TLP里面的东西是空的 则TLP的存在是没有意义的所以需要抹杀
		if(Utility.isNullForList(listBean.get(selectGroupNum).tlp.CONTAINER_TYPES)&&Utility.isNullForList(listBean.get(selectGroupNum).tlp.PRODUCTS)){
			listBean.get(selectGroupNum).statusLP = CCT_Container_StatusKey.MISSING;
		}else{
			listBean.get(selectGroupNum).statusLP = CCT_JudgeIcon.judgeTLP_TypeIcon(tlp);
		}

		if(Utility.isNullForList(tlpBean_CPlist)){

			RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "Empty new TLP will be removed from location.", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listBean.remove(selectGroupNum);
					intent.putExtra("dataList", (Serializable)listBean);
					intent.putExtra("tlp_id", tlp.CON_ID);
					setResult(RESULT_OK, intent);
					finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
				}
			}).show();

			return;
		}

		if(listBean.get(selectGroupNum).statusLP == CCT_Container_StatusKey.SCANNED&&CCT_JudgeIcon.foundNewFromTLP(listBean.get(selectGroupNum).tlp)){
			listBean.get(selectGroupNum).statusLP = CCT_Container_StatusKey.PROBLEM;
		}

		intent.putExtra("dataList", (Serializable)listBean);
		intent.putExtra("tlp_id", tlp.CON_ID);
		//清空数据
//		listBean.clear();
//		tlp=null;
//		tlpBean_CPlist.clear();
//		indexMap.clear();

		setResult(RESULT_OK, intent);	
		finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}
	
	/**
	 * @Description:判断是否显示提交按钮  每次扫描都需要校验
	 */
	private void showSubmitButton(){
		boolean showBtn = CCT_Util.checkVerifyData(tlpBean_CPlist);
		submit_btn.setVisibility(showBtn?View.VISIBLE:View.GONE);
	}

	/**********************************以下均为拍照***********************************************/
	
	@Override
	public void showCLP_Dlg_photos(String sku, final CCT_CLP_Bean clp) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initCLP_TTP(ttp, sku, clp);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadCLP_Photos(clp, ttp);
				dlg.dismiss();
			}
		});
	}
	private void initCLP_TTP(TabToPhoto ttp_dlg,final String sku, final CCT_CLP_Bean clp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+tlp.CON_ID +"_"+clp.CON_ID+"");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(clp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = clp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadCLP_Photos(final CCT_CLP_Bean b, final TabToPhoto ttp) {
		RequestParams p = new RequestParams();
//		p.add("fileName", b.CON_ID+"");
		String fileName = mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+tlp.CON_ID +"_"+ b.CON_ID;
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}

	@Override
	public void showProNoSn_Dlg_photos(String sku, final CCT_ProductBean pro) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initProNoSn_TTP(ttp, sku, pro);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadProNoSn_Photos(pro, ttp);
				dlg.dismiss();
			}
		});
	}
	
	private void initProNoSn_TTP(TabToPhoto ttp_dlg,final String sku, final CCT_ProductBean pro) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+ tlp.CON_ID+"_"+pro.PC_ID);
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(pro.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = pro.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadProNoSn_Photos(final CCT_ProductBean b, final TabToPhoto ttp) {
		RequestParams p = new RequestParams();
		String fileName = mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+ tlp.CON_ID+"_"+b.PC_ID;
//		p.add("fileName", tlp.CON_ID+"_"+b.PC_ID);
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}

	@Override
	public void showProSn_Dlg_photos(String sku, final CCT_ProductBean pro, final String SN) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initProSn_TTP(ttp, sku, pro,SN);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadProSn_Photos(pro, ttp,SN);
				dlg.dismiss();
			}
		});
		
	}
	
	private void initProSn_TTP(TabToPhoto ttp_dlg,final String sku, final CCT_ProductBean pro,final String SN) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku,mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+ tlp.CON_ID+"_"+SN);
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(pro.SN_photos.get(SN));
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = pro.SN_photos.get(SN);
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadProSn_Photos(final CCT_ProductBean b, final TabToPhoto ttp,final String SN) {
		RequestParams p = new RequestParams();
//		p.add("fileName", SN);
		String fileName = mTaskBean.ID + "_"+mLocation.LOCATION_ID+"_"+ tlp.CON_ID+"_"+SN;
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.SN_photos.put(SN, listPhotos);
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
	/**
	 * @Description:查询数据是否存在外面列表为空则没有
	 * @param @param value
	 */
	private String indexData(String value){
		if(selectNum==CLP){
			return indexMap.get("LP"+value);
		}
		
		if(selectNum==SKU){
			return indexMap.get("PID"+value);
		}
		
//		if(selectNum==SN){
//			return indexMap.get("SN"+value);
//		}
		return null;
	}
	
	/**
	 * @Description:关闭添加Model and lotnum 弹出的dialog
	 */
	private void dismiss(){
		if(showDialog!=null&&showDialog.isShowing()){
			showDialog.dismiss();
		}
	}

	@Override
	public void changeCLP_Scanned(int groupPosition, int childPosition) {
		changeCLP_Status(groupPosition, childPosition, CCT_Container_StatusKey.SCANNED, false);
	}

	@Override
	public void changeSN_Scanned(int groupPosition, int childPosition) {
		changeSN_Status(groupPosition, childPosition, CCT_Container_StatusKey.SCANNED, false);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp != null)
			ttp.onActivityResult(requestCode, resultCode, data);

		if(requestCode == CCT_CreateCLP.resultCode){
			if(data.getSerializableExtra("clp")==null){
				return;
			}
			CCT_CLP_Bean clp_bean  = (CCT_CLP_Bean) data.getSerializableExtra("clp");
			clp_bean.REAL_NUMBER = CCT_Util.fixLP_Number(clp_bean.CONTAINER);
			clp_bean.isNewData = true;
			//--新建container类型的基础bean 显示到当前的界面中
			final CCT_ContainerBean c = new CCT_ContainerBean();
			c.containerType = Cycle_Count_Task_Key.CCT_CLP;
			c.clp_type = new CCT_CLP_Type_Bean();
			c.clp_type.LP_TYPE_ID = clp_bean.TYPE_ID+"";
			c.clp_type.LP_TYPE_NAME = clp_bean.TYPE_NAME;
			c.clp_type.P_NAME = clp_bean.P_NAME;
			c.clp_type.CONTAINERS.add(clp_bean);

			//----添加
			boolean isAdd = true;
			//----提示音
			boolean isBeep = false;
			//----检索clp
			indexCLP(clp_bean, c, isAdd, isBeep);
			showSubmitButton();
		}

		if(requestCode == CCT_Container_Product.resultCode){
//			adps = new CCT_ScanTLP_Adp(mActivity, tlpBean_CPlist, this,isVerify);
			listview.setAdapter(adps);
			//张开全部
			adps.expandAll(listview);
//			adps.notifyDataSetChanged();
			if(data!=null){
				listview.setSelection(data.getIntExtra("pro_index", 0));
			}else{
				listview.setSelection(0);
			}
			showSubmitButton();
		}

		if(requestCode == CCT_Dialog.requestCode_CreateProduct||requestCode == CCT_Dialog.requestCode_Sku){
			if(data.getSerializableExtra("product")==null||data.getLongExtra("pc_id",0)==0){
				return;
			}
			ProductBean returnData = (ProductBean)data.getSerializableExtra("product");
			submitSKU(returnData.productName);
			showSubmitButton();
		}
		if(requestCode == CCT_Dialog.requestCode_CreateCLP){
			if(data.getSerializableExtra("product")==null||data.getLongExtra("pc_id",0)==0){
				return;
			}
			ProductBean returnData = (ProductBean)data.getSerializableExtra("product");

			CCT_Product product = new CCT_Product();
			product.p_name = returnData.productName;
			product.main_code = returnData.mainCode;
			product.pc_id = data.getLongExtra("pc_id", 0);

			CCT_CreateCLP.toThis(mActivity, product, interimStr, sBean,tlp);

			showSubmitButton();
		}

	}
}
