package oso.ui.inventory.cyclecount;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.adapter.CCT_VerifyTaskAdp;
import oso.ui.inventory.cyclecount.iface.VerifyIface;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_JudgeIcon;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
/**
 * @ClassName: VerificationTaskActivity 
 * @Description: 
 * @author gcy
 * @date 2015-5-11 上午11:40:40
 */
public class VerificationTaskActivity extends BaseActivity implements VerifyIface{
	private Cycle_Count_Tasks_Bean tBean;
	private ScanLocationDataBean sBean;
	private List<CCT_ContainerBean> listBean;

	private SearchEditText search_view;//搜索框
	
	private ExpandableListView listview;
	private CCT_VerifyTaskAdp adps;

	private TabToPhoto ttp;
	
	public static int thisActivityFlag = 321;//标记

	private Button submit_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cct_verify_task, 0);
		getFromActivityData(getIntent());
	}
	
	
	private CCT_LocationBean mLocationBean;
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	private void getFromActivityData(Intent intent) {
		tBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("tBean");
		mLocationBean = (CCT_LocationBean) intent.getSerializableExtra("LocationBean");
		sBean = (ScanLocationDataBean) intent.getSerializableExtra("ScanLocationDataBean");
		if(sBean==null||tBean==null){
			finish();
		}
		//-------转换数据结构 改装成CTNR集合
		listBean = CCT_ContainerBean.getContainerList(sBean);
		setTitleString(sBean.slc_position);
		initView();
	}
	
	/**
	 * @Description:初始化View
	 */
	private void initView(){
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finishThisActivity();
			}
		});

		search_view = (SearchEditText) findViewById(R.id.search_edit);
		search_view.setIconMode(SearchEditText.IconMode_Scan);
		search_view.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				if(StringUtil.isNullOfStr(value)){
					UIHelper.showToast("Enter or scan "+search_view.getHint());
					return;
				}
				
				doScanLP(value);
				search_view.setText("");
				search_view.requestFocus();
//				UIHelper.showToast(value);
				Utility.colseInputMethod(mActivity, search_view);
			}
		});
		
		// 全大写
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		search_view.setTransformationMethod(trans);
		
		listview = (ExpandableListView) findViewById(R.id.listview);
		
		//实例化adp
		adps = new CCT_VerifyTaskAdp(mActivity, listBean,this);
		listview.setAdapter(adps);
		
		//张开全部
		adps.expandAll(listview);
		
		//--------提交
		submit_btn = (Button) findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submitData();
			}
		});

	}
	
	/**
	 * @Description:根据输入值扫描TLP 或者 CLP
	 */
	private void doScanLP(String value){
		//------如果为空则直接请求获取数据
		if(Utility.isNullForList(listBean)){
			UIHelper.showToast("It's not the expected container!");
			return;
		}
		
		//-------判断是否查询到 如果查询不到则请求网络 获取该Container的 详细信息 并作为盈亏处理
		boolean isFound = false;
		boolean isTLP = false;
		int selectGroupNum = 0;
		int selectChildNum = 0;
		
		for (int i = 0; i < listBean.size(); i++) {
			CCT_ContainerBean c = listBean.get(i);
			//-------循环判断tlp
			if(c.containerType == Cycle_Count_Task_Key.CCT_TLP){
				if((c.tlp.REAL_NUMBER).equals(value)||(c.tlp.CONTAINER).equals(value)){
//					c.statusLP = CCT_Container_StatusKey.SCANNED;
					isFound = true;
					isTLP = true;
					selectGroupNum = i;
					break;
				}
			}
			//-------循环判断clp
			else{
				CCT_CLP_Type_Bean clp_type = c.clp_type;
				if(!Utility.isNullForList(clp_type.CONTAINERS)){
					for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
						CCT_CLP_Bean clp = clp_type.CONTAINERS.get(j);
						if(clp.REAL_NUMBER.equals(value)||(clp.CONTAINER).equals(value)){
//							clp_type.CONTAINERS.get(j).statusLP = CCT_Container_StatusKey.SCANNED;
							isFound = true;
							isTLP = false;
							selectGroupNum = i;
							selectChildNum = j;
							break;
						}
					}
				}
			}
		}
		
		if(!isFound){
			UIHelper.showToast("It's not the expected container!");
		}else{
			if(isTLP){
				scanTLP(selectGroupNum);
			}else{
				scanCLP(selectGroupNum, selectChildNum,false);
			}
		}
		
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 */
	private void scanTLP(final int selectGroupNum){
		CCT_TLP_Bean tlp = listBean.get(selectGroupNum).tlp;
		
		if(listBean.get(selectGroupNum).statusLP == CCT_Container_StatusKey.MISSING){
			UIHelper.showToast("TLP is missing");
			return;
		}
		
		TTS.getInstance().speakAll( "Scan TLP");
		UIHelper.showToast("Scan " + tlp.REAL_NUMBER);
		
		boolean isVerify = false;
		
		Intent intent = new Intent();
		intent.putExtra("selectGroupNum", selectGroupNum);
		intent.putExtra("listBean", (Serializable)listBean);	
		intent.putExtra("isVerify", isVerify);	
		intent.putExtra("ScanLocationDataBean", (Serializable)sBean);	
		intent.putExtra("TaskBean", (Serializable)tBean);	
		intent.putExtra("LocationBean", (Serializable)mLocationBean);
		
		intent.setClass(mActivity, CCT_Verify_Scan_TLP.class);
		startActivityForResult(intent, thisActivityFlag);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 */
	private void LP_Dialog(final int selectGroupNum,int containerType){
		final RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		
		final CCT_ContainerBean c = listBean.get(selectGroupNum);
		
		String prompt[] = null;
		final boolean isTLP = c.containerType == Cycle_Count_Task_Key.CCT_TLP;
		if(c.containerType == Cycle_Count_Task_Key.CCT_TLP){
			b.setMessage("TLP: "+c.tlp.REAL_NUMBER+"?");
			if(c.statusLP == CCT_Container_StatusKey.MISSING){
				prompt = new String[] { "	Cancel" };
			}else{
				prompt = new String[] { "	Missing" };
			}
		}else{
			b.setMessage("LP Type: "+c.clp_type.LP_TYPE_NAME+"?");
			prompt = new String[] { "	Scanned","	Missing" };
		}
		b.setNegativeButton(getString(R.string.sync_cancel), null);
		b.setArrowItems(prompt,new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						
						
						if(isTLP){
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								if(c.statusLP == CCT_Container_StatusKey.MISSING){
									c.statusLP = CCT_Container_StatusKey.UNSCANNED;
								}else{
									c.statusLP = CCT_Container_StatusKey.MISSING;
									if(c.tlp.isNewData){
										listBean.remove(selectGroupNum);
									}
								}
								break;
							default:
								break;
							}

							b.dismiss();
						}else{
							int status = c.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								status = CCT_Container_StatusKey.SCANNED;
								break;
							case 1://该容器类别下更改为丢失状态
								status = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}

							b.dismiss();
							
							for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
								c.clp_type.CONTAINERS.get(i).statusLP = status;
							}							

							c.statusLP = status;
						}
						
						
						//----校验是否 显示提交按钮
						showSubmitButton();
						adps.notifyDataSetChanged();
						//张开全部
						adps.expandAll(listview);
					}
				});
		b.show();
	}

	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 * @param @param selectGroupNum 父级索引
	 * @param @param selectChildNum 子级索引
	 * @param @param scannedStatus 扫描需要更改的状态
	 * @param @param isShowDialog 是否弹出dialog 来操作
	 */
	private void scanCLP(final int selectGroupNum,final int selectChildNum,boolean isShowDialog){
		final CCT_CLP_Type_Bean clp_type = listBean.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		
		//如果是正常扫描则不弹出dialog操作 直接更改状态刷新UI
		if(!isShowDialog){
			changeCLP_Status(selectGroupNum, selectChildNum,CCT_Container_StatusKey.SCANNED,true);
			return;
		}
		//------------弹出操作提示
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		b.setMessage("CLP: "+clp.REAL_NUMBER);
		b.setNegativeButton(getString(R.string.sync_cancel), null);
		if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
			b.setArrowItems(
					new String[] { "	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}else if(clp.statusLP==CCT_Container_StatusKey.MISSING){
			b.setArrowItems(
					new String[] { "	Scanned" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}else{
			b.setArrowItems(
					new String[] { "	Scanned","	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							case 1://该容器类别下更改为丢失状态
								scanStatus = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}
		
		
		b.show();
	}
	
	/**
	 * @Description:改变clp的状态
	 * @param @param selectGroupNum
	 * @param @param selectChildNum
	 * @param @param clp_type
	 * @param @param isBeep 提示音
	 */
	private void changeCLP_Status(int selectGroupNum,int selectChildNum,int scanStatus,boolean isBeep){
		final CCT_CLP_Type_Bean clp_type = listBean.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		//-------如果是丢失状态则不索引
		if(scanStatus == CCT_Container_StatusKey.MISSING){
			clp.statusLP = scanStatus;
			if(clp.isNewData){
				clp_type.CONTAINERS.remove(selectChildNum);
				if(Utility.isNullForList(clp_type.CONTAINERS)){
					listBean.remove(selectGroupNum);
				}
			}else{
				CCT_JudgeIcon.judgeIcon(listBean.get(selectGroupNum));
			}
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
//			if(selectChildNum<5){
//				listview.setSelectedGroup(selectGroupNum);
//			}else{
//				listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
//			}
		}else{
			addCLP(clp, listBean.get(selectGroupNum),isBeep);
		}
	}
	
	/**
	 * @Description:添加CLP 到当前列表
	 * @param @param clp
	 * @param @param c
	 * @param @param isAdd 是否添加 就是是否执行该方法 true执行 false 不执行
	 */
	private void addCLP(CCT_CLP_Bean clp,CCT_ContainerBean c,boolean isBeep){
			if(isBeep){
				if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
					TTS.getInstance().speakAll_withToast("You have already scanned this container.");
				}else{
					TTS.getInstance().speakAll("Scanned");
				}
			}
			
			clp.statusLP = CCT_Container_StatusKey.SCANNED;
			CCT_JudgeIcon.judgeIcon(c);
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
			
			int selectGroupNum = -1;
			int selectChildNum = -1;
			for (int i = 0; i < listBean.size(); i++) {
				if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP&&listBean.get(i).clp_type.LP_TYPE_ID.equals(c.clp_type.LP_TYPE_ID)){
					selectGroupNum = i;
					for (int j = 0; j < listBean.get(i).clp_type.CONTAINERS.size(); j++) {
						if(listBean.get(i).clp_type.CONTAINERS.get(j).CON_ID==clp.CON_ID){
							selectChildNum = j;
							break;
						}
					}
				}
			}
			if(isBeep&&selectGroupNum!=-1&&selectChildNum!=-1){
				if(selectChildNum<5){
					listview.setSelectedGroup(selectGroupNum);
				}else{
					listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
				}	
			}
			return;
	}
	
	@Override
	public void dialogCondition(int... is) {
		boolean flag = (is!=null&&is.length>0&&!Utility.isNullForList(listBean));
		if(!flag){
			return;
		}
		//如果长度只有1 则扫描的是TLP 或者SKU
		if(is.length==1){
			LP_Dialog(is[0], listBean.get(is[0]).containerType);
		}
		//如果长度只有2 则扫描的是CLP
		else if(is.length==2){
			scanCLP(is[0], is[1], true);
		}
	}

	@Override
	public void scanTLP_Info(int groupPosition) {
		scanTLP(groupPosition);
	}

	/**
	 * 用于弹出拍照的相关控件
	 */
	@Override
	public void showCLP_Dlg_photos(String sku, final CCT_CLP_Bean clp) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, sku, clp);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadPhotos(clp, ttp);
				dlg.dismiss();
			}
		});
	}
	
	private void initTTP(TabToPhoto ttp_dlg,final String sku, final CCT_CLP_Bean clp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, tBean.ID+"_"+mLocationBean.LOCATION_ID+"_"+clp.CON_ID+"");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(clp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = clp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadPhotos(final CCT_CLP_Bean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		String fileName = tBean.ID+"_"+mLocationBean.LOCATION_ID+"_"+ b.CON_ID;
//		p.add("id", b.CON_ID+"");
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				// UIHelper.showToast("Success!");
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
	/**
	 * @Description:判断是否显示提交按钮  每次扫描都需要校验
	 */
	private void showSubmitButton(){
		boolean showBtn = CCT_Util.checkVerifyData(listBean);
		submit_btn.setVisibility(showBtn?View.VISIBLE:View.GONE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp != null)
			ttp.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == thisActivityFlag){
 			if(resultCode == RESULT_OK){
 				listBean = (List<CCT_ContainerBean>)data.getSerializableExtra("dataList");
 				adps.setDataList(listBean);
 				showSubmitButton();
 			}
 		}
		
	}
	
	@Override
	public void onBackPressed() {
		finishThisActivity();
	}
	
	/**
	 * 如果没有submit location就进行了后退操作，需要请求删除所有已经上传过的图片
	 * @param ids
	 */
	private void reqDelPicsByFileIds(String ids) {
		RequestParams p = new RequestParams();
		p.put("file_ids", ids);
		
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				mActivity.finish();
				overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
			}
		}.doPost(HttpUrlPath.CCT_delUploadPhotos, p, mActivity);
	}

	/**
	 * @Description:关闭当前页面
	 */
	private void finishThisActivity(){
		RewriteBuilderDialog.newSimpleDialog(mActivity, "You will lose scanning data on current location.", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 后退需要判断是否有已经上传的图片，有则进行删除
				String ids = CCT_Util.getDelUploadPicsIds(listBean);
				if(ids.length()>0) {
					reqDelPicsByFileIds(ids);
				} 
				
				else {
					mActivity.finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
				}
				
			}
		}).show();	
	}

	private void submitData(){
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_success));
				finish();
			}
		}.doPostForYasir(HttpUrlPath.SubmitLocationScan, CCT_Util.getSubmitData(tBean, sBean, listBean), mActivity);
	}

	
	@Override
	public void changeScanned(int groupPosition, int childPosition) {
		changeCLP_Status(groupPosition, childPosition, CCT_Container_StatusKey.SCANNED, false);
	}

	@Override
	public void showTLP_Dlg_photos(String sku, final CCT_TLP_Bean tlp) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP_TLP(ttp, sku, tlp);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadPhotosTLP(tlp, ttp);
				dlg.dismiss();
			}
		});
	}

	private void initTTP_TLP(TabToPhoto ttp_dlg,final String sku, final CCT_TLP_Bean tlp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, tBean.ID + "_" + mLocationBean.LOCATION_ID+"_"+tlp.CON_ID+"");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(tlp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = tlp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadPhotosTLP(final CCT_TLP_Bean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		p.add("file_name", tBean.ID + "_" + mLocationBean.LOCATION_ID+"_"+ b.CON_ID+"");
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
}
