package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_TitleBase implements Serializable {

    private static final long serialVersionUID = 556933840719960122L;

    public String title_id;
    public String title_name;

}
