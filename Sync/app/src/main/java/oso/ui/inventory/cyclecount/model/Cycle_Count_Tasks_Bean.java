package oso.ui.inventory.cyclecount.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import utility.Utility;

/**
 * @ClassName: Task_InventoryBean 
 * @Description: 用于获取
 * @author gcy
 * @date 2015-2-27 下午6:52:25
 */
public class Cycle_Count_Tasks_Bean implements Serializable{
	
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -4681405134269281878L;
	
	public String START_DATE;//": "2015-02-28",
	public List<TitleBean> TITLES = new ArrayList<TitleBean>();//": 0, 如果TITLE_ID 为0时 则默认为ALL
	public List<String> MODEL_NUMBERS = new ArrayList<String>();
	public List<String> LOT_NUMBERS = new ArrayList<String>();
	public int PRODUCT_LINE_ID;//": 3,
	public String PRODUCT_LINE_NAME;//": "Electronics",
	public int PRODUCT_CATEGORY_ID;//": 6,
	public String PRODUCT_CATEGORY_NAME;//": "TVs",
	public int LP_TYPE_ID;//": 45,
	public String LP_TYPE_NAME;//": "4*9*4",
	public String CODE;//": "SONY-QUARTER-REPORT",
	public int PRIORITY;//": "High",
//	public List<CCT_AreasBean> AREAS = new ArrayList<CCT_AreasBean>();
	public String USER_ID;//": 100198,
	public String WAREHOUSE_NAME;//": "Valley",
	public String WAREHOUSE_ID;//": 1000005,
	public String END_DATE;//": "2015-03-02",
	public int ID;//": 33,  taskId
	public int STATUS;//": 1,  
	public int TYPE;//": 2   taskType
	public String CUSTOMER_NAME;
	public String CUSTOMER_ID;
	
	
//	/**
//	 * @Description:辅助解析数据
//	 * @param @param json
//	 */
//	public static List<Cycle_Count_Tasks_Bean> helpJson(JSONObject json){
//		
//		List<Cycle_Count_Tasks_Bean> list = new ArrayList<Cycle_Count_Tasks_Bean>();
//		JSONArray jArray = json.optJSONArray("DATA");
//		
//		if(!StringUtil.isNullForJSONArray(jArray)){
//			for(int i=0;i<jArray.length();i++){
//				JSONObject jObj = jArray.optJSONObject(i);
//				Cycle_Count_Tasks_Bean t = new Cycle_Count_Tasks_Bean();
//				t.START_DATE = jObj.optString("START_DATE");
//				t.END_DATE = jObj.optString("END_DATE");
//				
//				JSONArray tArray = jObj.optJSONArray("TITLES");
//				for (int j = 0; j < tArray.length(); j++) {
//					JSONObject jt = tArray.optJSONObject(j);
//					t.TITLES.add(new TitleBean(jt.optInt("TITLE_ID"),jt.optString("TITLE_NAME")));
//				}
//				
//				JSONArray mArray = jObj.optJSONArray("MODEL_NUMBERS");
//				for (int j = 0; j < mArray.length(); j++) {
//					t.MODEL_NUMBERS.add(mArray.optString(j));
//				}
//				
//				JSONArray lArray = jObj.optJSONArray("LOT_NUMBERS");
//				for (int j = 0; j < lArray.length(); j++) {
//					t.LOT_NUMBERS.add(lArray.optString(j));
//				}
//				t.PRODUCT_LINE_ID = jObj.optInt("PRODUCT_LINE_ID");
//				t.PRODUCT_LINE = jObj.optString("PRODUCT_LINE");
//				t.PRODUCT_CATEGORY_ID = jObj.optInt("PRODUCT_CATEGORY_ID");
//				t.PRODUCT_CATEGORY = jObj.optString("PRODUCT_CATEGORY");
//				t.CODE = jObj.optString("TASK_NAME");
//				t.PRIORITY = jObj.optInt("TASK_PRIORITY");
//				t.LP_TYPE_ID = jObj.optInt("LP_TYPE_ID");//": 45,
//				t.LP_TYPE_NAME = jObj.optString("LP_TYPE_NAME");//": "4*9*4",
//				
//				
//				t.USER_ID = jObj.optString("USER_ID");
//				t.WAREHOUSE_NAME = jObj.optString("WAREHOUSE_NAME");
//				t.WAREHOUSE_ID = jObj.optString("WAREHOUSE_ID");
//				t.ID = jObj.optInt("ID");
//				t.STATUS = jObj.optInt("STATUS");
//				t.TYPE = jObj.optInt("TYPE");
////				t.PRIORITY = jObj.optInt("PRIORITY");
//				CCT_AreasBean.helpJson(jObj, t.AREAS);
//				list.add(t);
//			}
//		}
//		return list;
//	}

	protected Cycle_Count_Tasks_Bean(Parcel in) {
		START_DATE = in.readString();
		MODEL_NUMBERS = in.createStringArrayList();
		LOT_NUMBERS = in.createStringArrayList();
		PRODUCT_LINE_ID = in.readInt();
		PRODUCT_LINE_NAME = in.readString();
		PRODUCT_CATEGORY_ID = in.readInt();
		PRODUCT_CATEGORY_NAME = in.readString();
		LP_TYPE_ID = in.readInt();
		LP_TYPE_NAME = in.readString();
		CODE = in.readString();
		PRIORITY = in.readInt();
		USER_ID = in.readString();
		WAREHOUSE_NAME = in.readString();
		WAREHOUSE_ID = in.readString();
		END_DATE = in.readString();
		ID = in.readInt();
		STATUS = in.readInt();
		TYPE = in.readInt();
		CUSTOMER_NAME = in.readString();
		CUSTOMER_ID = in.readString();
	}


	/**
	 * @Description:获取title的字符串
	 */
	public static String getTitleStr(List<TitleBean> tlist){
		String titleStr = "";
		if(!Utility.isNullForList(tlist)){
			for (int i = 0; i < tlist.size(); i++) {
				if(i!=0){
					titleStr +=", ";
				}
				titleStr += (tlist.get(i).TITLE_ID==0?"All":tlist.get(i).TITLE_NAME);
			}
		}
		return titleStr;
	}
	
	/**
	 * @Description:获取title的字符串
	 */
	public static String getListString(List<String> list){
		String str = "";
		if(!Utility.isNullForList(list)){
			for (int i = 0; i < list.size(); i++) {
				if(i!=0){
					str +=", ";
				}
				str += list.get(i);
			}
		}
		return str;
	}

//	/**
//	 * @Description:获取
//	 * @param @return
//	 */
//	public static JSONObject getDebugStr(){
//		JSONObject json = new JSONObject();
//		Random r = new Random();
//		int taskNum = r.nextInt(10) + 11;
//		JSONArray jArray = new JSONArray();
//		try {
//			for (int i = 0; i < taskNum; i++) {
//				JSONObject jTask = new JSONObject();
//				jTask.put("START_DATE", ("2015-02-0"+(r.nextInt(9)+1)));
//				int titleNum = r.nextInt(3)+1;
//				JSONArray tArray = new JSONArray();
//				for (int j = 0; j < titleNum; j++) {
//					JSONObject tTask = new JSONObject();
//					tTask.put("TITLE_ID", j);
//					tTask.put("TITLE_NAME", "VIZIO"+j);
//					tArray.put(tTask);
//				}
//				jTask.put("TITLES", tArray);
//				
//				List<String> mList = new ArrayList<String>();
//				mList.add("35896");
//				mList.add("7325746");
//				JSONArray mArray = new JSONArray(mList);
//				jTask.put("MODEL_NUMBERS",mArray);
//				
//				List<String> lList = new ArrayList<String>();
//				lList.add("1382694");
//				lList.add("170257413");
//				JSONArray lArray = new JSONArray(lList);
//				jTask.put("LOT_NUMBERS", lArray);
//				
//				
//				jTask.put("PRODUCT_LINE","Electronics");
//				jTask.put("PRODUCT_CATEGORY","TVs");
//				jTask.put("MODEL_NUMBER","254136");
//				jTask.put("LOT_NUMBER","254136");
//				jTask.put("TASK_NAME","SONY-QUARTER-REPORT"+i);
//				jTask.put("TASK_PRIORITY",r.nextInt(3)+1);
//				
//				jTask.put("USER_ID", "100198");
//				jTask.put("WAREHOUSE_NAME", "Valley");
//				jTask.put("END_DATE",  ("2015-02-1"+(r.nextInt(9)+1)));
//				jTask.put("ID", "3"+i);
//				jTask.put("STATUS", r.nextInt(2)+1);
//				jTask.put("TYPE", r.nextInt(3)+1);
////				jTask.put("PRIORITY", r.nextInt(3)+1);
//				JSONArray jAreaArray = new JSONArray();
//				int areaNum = r.nextInt(10) + 2;
//				for (int j = 0; j < areaNum; j++) {
//					JSONObject jArea = new JSONObject();
//					jArea.put("AREA_NAME", "1-B"+(j+1));
//					jArea.put("AREA_ID", "1000265");
//					int area_status = r.nextInt(3)+1;
//					jArea.put("STATUS", area_status);
//					int locationNum = r.nextInt(10)+1;
//					int closeLocationNum = r.nextInt(5)+1;
//					jArea.put("LOCATION_COUNT",locationNum);
//					jArea.put("CLOSE_LOCATION_COUNT",closeLocationNum);
//					if(area_status == Cycle_Count_Task_Key.Area_SCANNED){
//						jArea.put("CLOSE_USER","Tony Berserker");
//					}else{
//						jArea.put("CLOSE_USER","");
//					}
//					
//					
//					int increaseNum = 0;//增加量
//					JSONArray jLocationArray = new JSONArray();
//					for (int k = 0; k < locationNum; k++) {
//						JSONObject jLocation = new JSONObject();
//						increaseNum ++;
//						jLocation.put("LOCATION_FULL_NAME", "Valley1-B3B051"+(k+1));
//						jLocation.put("LOCATION_NAME", "B3B051"+(k+1));
//						jLocation.put("LOCATION_ID", "1006261");
//						int location_status =(increaseNum>=closeLocationNum)?Cycle_Count_Task_Key.Location_NEW:Cycle_Count_Task_Key.Location_SCANNED;
//						jLocation.put("STATUS", location_status);
//						if(location_status == Cycle_Count_Task_Key.Location_SCANNED){
//							jLocation.put("CLOSE_USER","Tony Berserker");
//						}else{
//							jLocation.put("CLOSE_USER","");
//						}
//						jLocationArray.put(jLocation);
//					}
//					jArea.put("LOCATIONS", jLocationArray);
//					jAreaArray.put(jArea);
//				}
//				jTask.put("AREAS", jAreaArray);
//				jArray.put(jTask);
//			}
//			json.put("DATA", jArray);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		return json;
//	}
}


/*  测试数据
 *  {
	  "WAREHOUSE_NAME": "Valley",
	  "USER_ID": "100198",
	  "END_DATE": "2015-02-17",
	  "PRODUCT_LINE": "Electronics",
	  "MODEL_NUMBERS": [
	    "35896",
	    "7325746"
	  ],
	  "TITLES": [
	    {
	      "TITLE_ID": 0,
	      "TITLE_NAME": "VIZIO0"
	    }
	  ],
	  "STATUS": 1,
	  "TASK_PRIORITY": "Low",
	  "START_DATE": "2015-02-05",
	  "AREAS": [
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0512",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0512",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0513",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0513",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0514",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0514",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0515",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0515",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0516",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0516",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0517",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0517",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 4,
	      "CLOSE_USER": "Tony Berserker",
	      "AREA_ID": "1000265",
	      "STATUS": 3,
	      "LOCATION_COUNT": 7,
	      "AREA_NAME": "1-B1"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0512",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0512",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0513",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0513",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0514",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0514",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0515",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0515",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0516",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0516",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0517",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0517",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0518",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0518",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 2,
	      "CLOSE_USER": "",
	      "AREA_ID": "1000265",
	      "STATUS": 2,
	      "LOCATION_COUNT": 8,
	      "AREA_NAME": "1-B2"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 4,
	      "CLOSE_USER": "",
	      "AREA_ID": "1000265",
	      "STATUS": 1,
	      "LOCATION_COUNT": 1,
	      "AREA_NAME": "1-B3"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 4,
	      "CLOSE_USER": "",
	      "AREA_ID": "1000265",
	      "STATUS": 1,
	      "LOCATION_COUNT": 1,
	      "AREA_NAME": "1-B4"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 1,
	      "CLOSE_USER": "Tony Berserker",
	      "AREA_ID": "1000265",
	      "STATUS": 3,
	      "LOCATION_COUNT": 1,
	      "AREA_NAME": "1-B5"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0512",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0512",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0513",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0513",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 3,
	      "CLOSE_USER": "",
	      "AREA_ID": "1000265",
	      "STATUS": 2,
	      "LOCATION_COUNT": 3,
	      "AREA_NAME": "1-B6"
	    },
	    {
	      "LOCATIONS": [
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0511",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0511",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0512",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0512",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0513",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0513",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0514",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0514",
	          "CLOSE_USER": "Tony Berserker",
	          "STATUS": 3
	        },
	        {
	          "LOCATION_FULL_NAME": "Valley1-B3B0515",
	          "LOCATION_ID": "1006261",
	          "LOCATION_NAME": "B3B0515",
	          "CLOSE_USER": "",
	          "STATUS": 1
	        }
	      ],
	      "CLOSE_LOCATION_COUNT": 5,
	      "CLOSE_USER": "",
	      "AREA_ID": "1000265",
	      "STATUS": 1,
	      "LOCATION_COUNT": 5,
	      "AREA_NAME": "1-B7"
	    }
	  ],
	  "LOT_NUMBERS": [
	    "1382694",
	    "170257413"
	  ],
	  "TASK_NAME": "SONY-QUARTER-REPORT0",
	  "ID": "30",
	  "MODEL_NUMBER": "254136",
	  "TYPE": 1,
	  "PRODUCT_CATEGORY": "TVs",
	  "LOT_NUMBER": "254136"
	}*/