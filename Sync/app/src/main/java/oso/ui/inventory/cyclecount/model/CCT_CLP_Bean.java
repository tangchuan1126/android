package oso.ui.inventory.cyclecount.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.widget.photo.bean.TTPImgBean;
/**
 * @ClassName: CCT_CLP_Bean 
 * @Description: 
 * @author gcy
 * @date 2015-4-8 下午10:04:36
 */
public class CCT_CLP_Bean implements Serializable{

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 7726324908436197869L;
	
	public int TITLE_ID;
	public long CON_ID;//": 12354,
	public int CONTAINER_TYPE;//": 1,
	public String CONTAINER;//": "CLP12354"
	public String REAL_NUMBER;//真实的lp number
	
	public int IS_HAS_SN;
	public int TYPE_ID;
	public int IS_FULL;
	public int STATUS;//目前没用
	public String LOT_NUMBER;

	//--------判断当前的CLP是不是新的托盘
	public boolean isNewData = false;
	public String P_NAME;//用于创建CLP之后 的传递参数
	
	public int statusLP = CCT_Container_StatusKey.UNSCANNED;//判断托盘的状态   
	
	public List<TTPImgBean> photos = new ArrayList<TTPImgBean>();
	
	// xlm
	public boolean isDamaged = false; // 是否损毁
	public boolean itemIsSeleted; // 扫描如果发现有重复，则item显示为选中状态，默认为false
	public String TYPE_NAME;  // 盘点添加  CLP3*2*1[CLP1]
	
	public static void helpJson(JSONObject json, List<CCT_CLP_Bean> list){
		JSONArray jArray = json.optJSONArray("CONTAINERS");
		CCT_CLP_Bean.resolveJson(jArray,list);
	}

	public static void resolveJson(JSONArray jArray,List<CCT_CLP_Bean> list){
		
		list= new Gson().fromJson(jArray.toString(), new TypeToken<List<CCT_CLP_Bean>>() {}.getType());
		
	}
	
	/**
	 * @Description:获取
	 * @param @return
	 */
	public static JSONObject getDebugStr(){
		JSONObject json = new JSONObject();
		
		Random r = new Random();
		try {
			int clp = r.nextInt(10)+1;
			JSONArray jCLPArray = new JSONArray();
			for (int i = 0; i < clp; i++) {
				JSONObject jCLP = new JSONObject();
				jCLP.put("CONTAINER_TYPE",Cycle_Count_Task_Key.CCT_CLP);
				String clpStr = "1"+i+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10);
				jCLP.put("CONTAINER_ID",clpStr);
				jCLP.put("CONTAINER", "CLP"+clpStr);
				jCLPArray.put(jCLP);
			}
			json.put("CONTAINERS",jCLPArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * 清除图片 和 damage状态
	 */
	public void clearInfo() {
		this.isDamaged = false;
		this.photos.clear();
	}
	
	/**
	 * 获得当前CLP已经在服务端上的图片的ids
	 * @return
	 */
	public String getReqDelPhotosIds() {
		StringBuilder sb = new StringBuilder();
		List<TTPImgBean> photosList = photos;
		for(int i=0; i<photosList.size(); i++) {
			if(i==photosList.size()-1) {
				sb.append(photosList.get(i).file_id);
			} else {
				sb.append(photosList.get(i).file_id + ",");
			}
		}
		return sb.toString();
	}
}
