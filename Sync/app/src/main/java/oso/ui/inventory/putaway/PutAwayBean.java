package oso.ui.inventory.putaway;

import java.io.Serializable;

public class PutAwayBean implements Serializable {

	private static final long serialVersionUID = 6344988819121268129L;
	
	public String company_id;
	public String date_created;
	public String item_id = "";
	public int movement_no;
	public int movement_printed;
	public String plate_no;
	public int plate_printed;
	public String slc_id;
	public String slc_name;
	public int slc_type; //类型：0为Location， 1为Staging
	public int status;
	public String user_created;
	
	public String con_id; //需要回传的plate_no
	
	
	
//	{
//	    "data": {
//	        "company_id": "",
//	        "date_created": "2015-04-11 13:30:46.0",
//	        "movement_no": 5,
//	        "movement_printed": 0,
//	        "plate_no": 3010990,
//	        "plate_printed": 0,
//	        "slc_id": "1",
//	        "slc_name": "A004",
//	        "slc_type\r\nslc_type\r\nslc_type": 0,
//	        "status": 0,
//	        "user_created": "100198"
//	    },
//	    "err": 0,
//	    "exception": 0,
//	    "plate_no": 3011243,
//	    "ret": 1
//	}

}
