package oso.ui.inventory.cyclecount;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.LengthUOMKey;
import oso.ui.inventory.cyclecount.key.WeightUOMKey;
import oso.ui.inventory.cyclecount.model.CCT_PackagingType;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.ui.inventory.cyclecount.util.SyncTextDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: CCT_VerifyTask
 * @Description: 对照盘点
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_CreatePackagingType extends BaseActivity implements View.OnClickListener{

    private EditText et_packaging_type, et_length, et_width, et_height, et_weight, et_max_capacity, et_max_height;
    private SyncTextDialog btn_lengh_uom, btn_weight_uom;
    private Button btn_submit;
    public static final int resultCode = 51;
    private NetConnection_CCT conn;
    private CCT_PackagingType bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cct_create_packaging_type, 0);
        initView();
    }

    /**
     * @Description:初始化View
     */
    private void initView() {
        conn = NetConnection_CCT.getThis();
        setTitleString(getString(R.string.cct_createpackagingtype));

        //初始化表单信息
        et_packaging_type = (EditText) findViewById(R.id.et_packaging_type);
        et_length = (EditText) findViewById(R.id.et_length);
        et_width = (EditText) findViewById(R.id.et_width);
        et_height = (EditText) findViewById(R.id.et_height);
        et_weight = (EditText) findViewById(R.id.et_weight);
        et_max_capacity = (EditText) findViewById(R.id.et_max_capacity);
        et_max_height = (EditText) findViewById(R.id.et_max_height);
        btn_lengh_uom = (SyncTextDialog) findViewById(R.id.btn_lengh_uom);
        btn_weight_uom = (SyncTextDialog) findViewById(R.id.btn_weight_uom);

        // 全大写
        TransformationMethod trans = AllCapTransformationMethod.getThis();
        et_packaging_type.setTransformationMethod(trans);


        //设置TAG方便做校验
        et_packaging_type.setTag(getString(R.string.cct_packaging_et_packaging_type));

        et_length.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_length.setFilters(Utility.setNumberDecimal(10, 1));

        et_width.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_width.setFilters(Utility.setNumberDecimal(10, 1));

        et_height.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_height.setFilters(Utility.setNumberDecimal(10, 1));

        et_weight.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_weight.setFilters(Utility.setNumberDecimal(10, 2));

        et_max_capacity.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_max_capacity.setFilters(Utility.setNumberDecimal(10, 2));

        et_max_height.setTag(getString(R.string.cct_packaging_prompt_two_point));
        et_max_height.setFilters(Utility.setNumberDecimal(10, 1));

        btn_lengh_uom.setTag(getString(R.string.cct_packaging_btn_lengh_uom));
        btn_weight_uom.setTag(getString(R.string.cct_packaging_btn_weight_uom));
        btn_lengh_uom.setDoubleData(LengthUOMKey.lengthUOMList, getString(R.string.cct_packaging_btn_lengh_uom));
        btn_weight_uom.setDoubleData(WeightUOMKey.weightUOMList,getString(R.string.cct_packaging_btn_weight_uom));

        CCT_Util.checkTextViewTwoPointValue(mActivity, 2, et_length, et_width, et_height, et_weight, et_max_capacity, et_max_height);
        //提交按钮
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                boolean isCheck = CCT_Util.checkTextViewValue(mActivity, et_packaging_type, btn_lengh_uom, btn_weight_uom);
                if(isCheck){
                    submitData();
                }
                break;
            default:
                break;
        }
    }

    /**
     * @Description:关闭当前页面
     */
    private void finishThisActivity() {
        if(bean!=null){
            Intent data = new Intent();
            data.putExtra("packaging_type", bean);
            setResult(resultCode, data);
            finish();
            overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            return;
        }
        RewriteBuilderDialog.newSimpleDialog(mActivity, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("packaging_type", bean);
                setResult(resultCode, data);
                finish();
                overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            }
        }).show();
    }

    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey() {
        finishThisActivity();
    }


    public void toGoActivity(Context packageContext, Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(packageContext, cls);
        startActivity(intent);
        overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_in);
    }

    public static void toThis(final Context c) {
        Intent intent = new Intent(c, CCT_CreatePackagingType.class);
        ((Activity) c).startActivityForResult(intent, CCT_CreatePackagingType.resultCode);

    }

    /**
     * 获取需要提交的数据
     * @return
     */
    private String getJsonData(){
        bean = new CCT_PackagingType();
        try {
            bean.type_name = et_packaging_type.getText().toString().toUpperCase(Locale.ENGLISH);
            bean.length = getwoPointNum(et_length);
            bean.width = getwoPointNum(et_width);
            bean.height = getwoPointNum(et_height);
            bean.weight = getwoPointNum(et_weight);
            bean.max_load = getwoPointNum(et_max_capacity);
            bean.max_height = getwoPointNum(et_max_height);
            bean.length_uom = CCT_Util.getKeyValue(btn_lengh_uom.getText().toString(), LengthUOMKey.lengthUOMList);//":"1",
            bean.weight_uom = CCT_Util.getKeyValue(btn_weight_uom.getText().toString(), WeightUOMKey.weightUOMList);//":"1",
        } catch (Exception e) {
            e.printStackTrace();
            UIHelper.showToast(getString(R.string.sync_data_error));
            return null;
        }
        return bean.toJsonStr();
    }

    /**
     * 获取小数点后两位
     * @param v
     * @return
     */
    private double getwoPointNum(TextView v){
        String string = v.getText().toString();
        if(TextUtils.isEmpty(string)){
            return 0;
        }
        return ((int)(Double.parseDouble(v.getText().toString())*100))/100;
    }

    private void submitData(){
        String string = getJsonData();
        if(TextUtils.isEmpty(string)){
            UIHelper.showToast(getString(R.string.sync_data_error));
            return;
        }

        RequestParams params = new RequestParams();
        params.put("json", string);
        new SimpleJSONUtil() {
            @Override
            public void handReponseJson(JSONObject json) {
                UIHelper.showToast(getString(R.string.sync_success));
                int type_id = json.optInt("type_id", 0);
                bean.type_id = type_id;
                finishThisActivity();
            }
            public void handFail() {
                UIHelper.showToast(getString(R.string.sync_create_fail));
            }
        }.doPost(HttpUrlPath.CCT_CreatePackaging, params, mActivity);
    }

}
