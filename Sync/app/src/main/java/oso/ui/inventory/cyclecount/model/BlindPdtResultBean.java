package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 盘点Product请求验证 json
 * @author xialimin
 *
 */
public class BlindPdtResultBean implements Serializable {
	private static final long serialVersionUID = 1508070539800438935L;
	public List<Data> DATA = new ArrayList<Data>();
	public int status;
	
	public class Data {
		public long PC_ID;
		public String P_NAME;
		public int IS_HAS_SN;//CCT_P_HAS_SN = 1;  CCT_P_NO_SN = 2;
	}
}
