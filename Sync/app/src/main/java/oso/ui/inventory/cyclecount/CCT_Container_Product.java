package oso.ui.inventory.cyclecount;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.util.CCT_JudgeIcon;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: CCT_Container_Product
 * @Description: 用于处理容易与商品之间的关系
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_Container_Product extends BaseActivity {

    private NetConnection_CCT conn;
    public static final int resultCode = 411;

    private CCT_ProductBean pro;
    private CCT_LocationBean mLocation;
    private Cycle_Count_Tasks_Bean mTaskBean;
    private int pro_index;

    private SingleSelectBar singleSelectBar;
    private final String S_HAS_SN = "SN";
    private final String S_NO_SN = "No SN";

    private View show_sn_list_layout;
    private View show_no_sn_layout;
    private EditText e_total, e_damage;
    private View photo_view;
    private SearchEditText search_sn;
    private ListView listview;
    private Button btn_submit;

    private TabToPhoto ttp;

    private final int HAS_SN = 0;
    private final int NO_SN = 1;
    private int selectNum = HAS_SN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cct_container_product, 0);
        getFromActivityData(getIntent());
    }

    /**
     * @Description:接收来自于上一个activity所传递过来的数据
     */
    private void getFromActivityData(Intent intent) {
        pro_index = intent.getIntExtra("pro_index", 0);
        pro = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type;
        mLocation = (CCT_LocationBean) intent.getSerializableExtra("LocationBean");
        mTaskBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("TaskBean");
        if (pro == null) {
            UIHelper.showToast(getString(R.string.sync_data_error));
            finish();
            return;
        }

        initView();
    }


    /**
     * @Description:初始化View
     */
    private void initView() {
        conn = new NetConnection_CCT();
        setTitleString(pro.P_NAME);

        show_sn_list_layout = findViewById(R.id.show_sn_list_layout);
        show_no_sn_layout = findViewById(R.id.show_no_sn_layout);
        search_sn = (SearchEditText) findViewById(R.id.search_sn);
        e_total = (EditText) findViewById(R.id.e_total);
        e_damage = (EditText) findViewById(R.id.e_damage);
        photo_view = findViewById(R.id.photo_view);

        search_sn.setIconMode(SearchEditText.IconMode_Scan);
        search_sn.setSeacherMethod(new SeacherMethod() {
            @Override
            public void seacher(String value) {
                if (StringUtil.isNullOfStr(value)) {
                    return;
                }

                doScanSN(value, false);
                search_sn.setText("");
                search_sn.requestFocus();
                Utility.colseInputMethod(mActivity, search_sn);
            }
        });
        search_sn.setIconMode(SearchEditText.IconMode_Scan);
        // 全大写
        TransformationMethod trans = AllCapTransformationMethod.getThis();
        search_sn.setTransformationMethod(trans);


        listview = (ListView) findViewById(R.id.listview);
        listview.setAdapter(adpOthers);

        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSubmit();
            }
        });

        initSingleSelectBar(pro.IS_HAS_SN == Cycle_Count_Task_Key.CCT_P_HAS_SN);
    }

    /**
     * @Description:初始化singleBarSelect
     */
    private void initSingleSelectBar(final boolean isHasSN) {
        singleSelectBar = (SingleSelectBar) mActivity.findViewById(R.id.single_select_bar);
        List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
        clickItems.add(new HoldDoubleValue<String, Integer>(S_HAS_SN, HAS_SN));
        clickItems.add(new HoldDoubleValue<String, Integer>(S_NO_SN, NO_SN));
        singleSelectBar.setUserDefineClickItems(clickItems);
        singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
            @Override
            public void clickCallBack(
                    HoldDoubleValue<String, Integer> selectValue) {
                selectNum = selectValue.b;
                boolean isOnClickHasSN = (selectNum == HAS_SN);
                if (isHasSN) {
                    if (!isOnClickHasSN) {
                        UIHelper.showToast("Please scan SN");
                        selectNum = HAS_SN;
                        singleSelectBar.userDefineSelectIndexExcuteClick(selectNum);
                    } else {
                        show_sn_list_layout.setVisibility(View.VISIBLE);
                        show_no_sn_layout.setVisibility(View.GONE);
                        pro.IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_HAS_SN;
                    }
                } else {
                    if (isOnClickHasSN) {
                        show_sn_list_layout.setVisibility(View.VISIBLE);
                        show_no_sn_layout.setVisibility(View.GONE);
                        pro.IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_HAS_SN;
                    } else {
                        show_sn_list_layout.setVisibility(View.GONE);
                        show_no_sn_layout.setVisibility(View.VISIBLE);
                        pro.IS_HAS_SN = Cycle_Count_Task_Key.CCT_P_NO_SN;
                        e_total.requestFocus();
                        toNoSN();
                    }
                }
            }
        });

        if (pro.IS_HAS_SN == Cycle_Count_Task_Key.CCT_P_HAS_SN) {
            selectNum = HAS_SN;
        } else {
            selectNum = NO_SN;
        }
        singleSelectBar.userDefineSelectIndexExcuteClick(selectNum);

    }

    private void toNoSN() {
        if (pro != null && pro.TOTAL != 0) {
            e_total.setText(pro.TOTAL + "");
            e_total.setSelection((pro.TOTAL + "").length());
        }
        if (pro != null && pro.DAMAGEDCNT != 0) {
            e_damage.setText(pro.DAMAGEDCNT + "");
            e_damage.setSelection((pro.DAMAGEDCNT + "").length());
            photo_view.setVisibility(View.VISIBLE);
        }

        e_total.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
                    //-----如果为空则锁定当前焦点 如果不为空则焦点下移
                    if (!StringUtil.isNullOfStr(e_total.getText().toString())) {
                        e_damage.requestFocus();
                    }
                    Utility.colseInputMethod(mActivity, e_total);
                }
                return false;
            }
        });

        //---------监听文本控制拍照控件是否显示
        e_damage.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                photo_view.setVisibility(StringUtil.isNullOfStr(e_damage.getText().toString()) || "0".equals(e_damage.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        ttp = (TabToPhoto) findViewById(R.id.ttp);
        initProNoSn_TTP(ttp, pro.P_NAME, pro);
    }

    private void initProNoSn_TTP(TabToPhoto ttp_dlg, final String sku, final CCT_ProductBean pro) {
        ttp = ttp_dlg;
        String key = TTPKey.getCCT_Key(sku, mTaskBean.ID + "_" + mLocation.LOCATION_ID + "_" + CCT_Verify_Scan_TLP.tlp.CON_ID + "_" + pro.PC_ID);
        ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
        ttp_dlg.pvs[0].addWebPhotoToIv(pro.photos);
        ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

            @Override
            public void onDeleteComplete(int tabIndex, TTPImgBean b) {
                // TODO Auto-generated method stub
                List<TTPImgBean> imgs = pro.photos;
                if (imgs != null && imgs.contains(b)) {
                    imgs.remove(b);
                    adpOthers.notifyDataSetChanged();
                }
            }
        });
    }

    private void reqUploadProNoSn_Photos(final CCT_ProductBean b, final TabToPhoto ttp) {
        RequestParams p = new RequestParams();
        String fileName = mTaskBean.ID + "_" + mLocation.LOCATION_ID + "_" + CCT_Verify_Scan_TLP.tlp.CON_ID + "_" + b.PC_ID;
//		p.add("fileName", tlp.CON_ID+"_"+b.PC_ID);
        p.add("file_name", fileName);
        ttp.uploadZip(p, "receive");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
                ttp.clearData();
                // UIHelper.showToast("Success!");
                JSONArray ja = json.optJSONArray("PHOTO");
                if (!Utility.isEmpty(ja)) {
                    List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
                    b.photos = listPhotos;
                    adpOthers.notifyDataSetChanged();
                }

                UIHelper.showToast(getString(R.string.sync_success));

                onBackBtnOrKey(false);

            }
        }.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

    }

    /**
     * @Description:根据输入值扫描TLP 或者 CLP
     */
    private void doScanSN(String value, boolean isShowDialog) {
        if (Utility.isNullForList(CCT_Verify_Scan_TLP.tlpBean_CPlist)) {
            addNewSN(value);
            return;
        }

        //-------判断是否查询到 如果查询不到则请求网络 获取该Container的 详细信息 并作为盈亏处理
        boolean isFound = false;
        int selectGroupNum = 0;
        int selectChildNum = 0;

        for (int i = 0; i < CCT_Verify_Scan_TLP.tlpBean_CPlist.size(); i++) {
            CCT_ContainerBean c = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(i);

            //-------循环判断pro
            if (c.containerType == Cycle_Count_Task_Key.CCT_PRODUCT) {
                CCT_ProductBean pro_type = c.product_type;
                if (!Utility.isNullForList(pro_type.SN)) {
                    for (int j = 0; j < pro_type.SN.size(); j++) {
                        String sn = pro_type.SN.get(j);
                        if (sn.equals(value)) {
                            isFound = true;
                            selectGroupNum = i;
                            selectChildNum = j;
                            break;
                        }
                    }
                }
            }
        }

        if (!isFound) {
            addNewSN(value);
        } else {
            scanPro_SN(selectGroupNum, selectChildNum, isShowDialog);
        }

    }

    private void addNewSN(String value) {
        pro.SN.add(0, value);
        pro.NEW_SN_Map.put(value, true);
        pro.SN_Map.put(value, CCT_Container_StatusKey.SCANNED);
        CCT_JudgeIcon.judgeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index));
        adpOthers.notifyDataSetChanged();
    }

    /**
     * @param @param selectGroupNum 父级索引
     * @param @param selectChildNum 子级索引
     * @param @param scannedStatus 扫描需要更改的状态
     * @param @param isShowDialog 是否弹出dialog 来操作
     * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
     */
    private void scanPro_SN(final int selectGroupNum, final int selectChildNum, boolean isShowDialog) {
        final CCT_ProductBean pro_type = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(selectGroupNum).product_type;
        final String sn = pro_type.SN.get(selectChildNum);

        //如果是正常扫描则不弹出dialog操作 直接更改状态刷新UI
        if (!isShowDialog) {
            changeSN_Status(selectGroupNum, selectChildNum, CCT_Container_StatusKey.SCANNED, true);
            return;
        }

        RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
        b.setMessage("SN: " + sn);

        b.setNegativeButton(getString(R.string.sync_cancel), null);
        if (pro_type.SN_Map.get(sn) != null && pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.SCANNED) {
            b.setArrowItems(
                    new String[]{"	Missing"},
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            int status = (pro_type.SN_Map.get(sn) == null || pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED) ? CCT_Container_StatusKey.UNSCANNED : pro_type.SN_Map.get(sn);
                            switch (position) {
                                case 0://该容器类别下更改为已扫描状态
                                    status = CCT_Container_StatusKey.MISSING;
                                    break;
                                default:
                                    break;
                            }
                            changeSN_Status(selectGroupNum, selectChildNum, status, false);
                        }
                    });
        } else if (pro_type.SN_Map.get(sn) != null && pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.MISSING) {
            b.setArrowItems(
                    new String[]{"	Scanned"},
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            int status = (pro_type.SN_Map.get(sn) == null || pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED) ? CCT_Container_StatusKey.UNSCANNED : pro_type.SN_Map.get(sn);
                            switch (position) {
                                case 0://该容器类别下更改为已扫描状态
                                    status = CCT_Container_StatusKey.SCANNED;
                                    break;
                                default:
                                    break;
                            }
                            changeSN_Status(selectGroupNum, selectChildNum, status, false);
                        }
                    });
        } else {
            b.setArrowItems(
                    new String[]{"	Scanned", "	Missing"},
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            int status = (pro_type.SN_Map.get(sn) == null || pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED) ? CCT_Container_StatusKey.UNSCANNED : pro_type.SN_Map.get(sn);
                            switch (position) {
                                case 0://该容器类别下更改为已扫描状态
                                    status = CCT_Container_StatusKey.SCANNED;
                                    break;
                                case 1://该容器类别下更改为丢失状态
                                    status = CCT_Container_StatusKey.MISSING;
                                    break;
                                default:
                                    break;
                            }
                            changeSN_Status(selectGroupNum, selectChildNum, status, false);
                        }
                    });
        }
        b.show();
    }

    public void dialogCondition(final int position, boolean isShowDialog) {
        String sn = pro.SN.get(position);
        doScanSN(sn, true);
    }

    /**
     * @param @param selectGroupNum
     * @param @param selectChildNum
     * @param @param clp_type
     * @param @param isBeep 提示音
     * @Description:改变clp的状态
     */
    private void changeSN_Status(int selectGroupNum, int selectChildNum, int scanStatus, boolean isBeep) {
        final CCT_ProductBean pro_type = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(selectGroupNum).product_type;
        final String sn = pro_type.SN.get(selectChildNum);
        //-------如果是丢失状态则不索引
        if (scanStatus == CCT_Container_StatusKey.MISSING) {
            pro_type.SN_Map.put(sn, scanStatus);

            if (pro_type.NEW_SN_Map.get(sn) != null && pro_type.NEW_SN_Map.get(sn)) {
                pro_type.SN.remove(sn);
                pro_type.NEW_SN_Map.remove(sn);
                pro_type.SN_Map.remove(sn);

                if (pro_type.SN_Damaged_Map.get(sn) != null)
                    pro_type.SN_Damaged_Map.remove(sn);
                if (pro_type.SN_photos.get(sn) != null)
                    pro_type.SN_photos.remove(sn);


                if (Utility.isNullForList(pro_type.SN)) {
//                    for (int i = 0; i < CCT_Verify_Scan_TLP.tlp.PRODUCTS.size(); i++) {
//                        if (CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).PC_ID == pro_type.PC_ID) {
//                            CCT_Verify_Scan_TLP.tlp.PRODUCTS.remove(i);
//                            break;
//                        }
//                    }
//                    CCT_ContainerBean.removeProduct(CCT_Verify_Scan_TLP.tlpBean_CPlist, pro_type.PC_ID);
//                    onBackBtnOrKey(false);
                    doSubmit();
                }
            } else {
                CCT_JudgeIcon.judgeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(selectGroupNum));
            }
            //----校验是否 显示提交按钮
            adpOthers.notifyDataSetChanged();
//			if(selectChildNum<5){
//				listview.setSelectedGroup(selectGroupNum);
//			}else{
//				listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
//			}

        } else {
            indexSN(pro_type, sn, false, isBeep);
        }

    }

    /**
     * @param @param jobj
     * @param @param value
     * @Description:检索SN
     */
    private void indexSN(final CCT_ProductBean pro, final String value, final boolean isAdd, final boolean isBeep) {
        //---------检索数据
        String indexValue = indexData(value);
        if (!StringUtil.isNullOfStr(indexValue)) {
            if (indexValue.indexOf("/") < 0) {
                return;
            }

            String tlp_id = indexValue.split("/")[1];
            if (!StringUtil.isNullOfStr(tlp_id)) {
                boolean isFound = false;
                CCT_TLP_Bean tlp = null;
                for (int i = 0; i < CCT_Verify_Scan_TLP.listBean.size(); i++) {
                    if (CCT_Verify_Scan_TLP.listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP && (CCT_Verify_Scan_TLP.listBean.get(i).tlp.CON_ID + "").equals(tlp_id)) {
                        isFound = true;
                        tlp = CCT_Verify_Scan_TLP.listBean.get(i).tlp;
                        break;
                    }
                }

                if (isFound) {
                    final CCT_TLP_Bean oldTlp = tlp;
                    //---------clp_type索引
                    int clp_type_Index = -1;
                    //---------clp索引
                    int clp_Index = -1;
                    if (!Utility.isNullForList(oldTlp.PRODUCTS)) {
                        for (int j = 0; j < oldTlp.PRODUCTS.size(); j++) {
                            if (!Utility.isNullForList(oldTlp.PRODUCTS.get(j).SN)) {
                                for (int i = 0; i < oldTlp.PRODUCTS.get(j).SN.size(); i++) {
                                    if (oldTlp.PRODUCTS.get(j).SN.get(i).equals(value)) {
                                        clp_type_Index = j;
                                        clp_Index = i;
                                        break;
                                    }
                                }
                            } else {
                                oldTlp.PRODUCTS.get(j).SN_Map.put(value, CCT_Container_StatusKey.MISSING);
                            }
                            break;
                        }
                    }
                    //----如果索引均不为-1则表示存在该clp
                    if (clp_type_Index != -1 && clp_Index != -1) {
                        //-----由于回调函数只能应用静态变量 所以需要中转一下该变量 声明一个clp的索引
                        final int indexTypeNum = clp_type_Index;
                        final int indexNum = clp_Index;

                        final CCT_ProductBean p = oldTlp.PRODUCTS.get(indexTypeNum);
                        String sn = p.SN.get(indexNum);
                        if (p.SN_Map.get(sn) == null || p.SN_Map.get(sn) == CCT_Container_StatusKey.UNSCANNED) {
                            p.SN_Map.put(sn, CCT_Container_StatusKey.MISSING);
                            addSN(pro, value, isAdd, isBeep);
                        } else {
                            if (pro.SN_Map.get(value) != null && pro.SN_Map.get(value) == CCT_Container_StatusKey.SCANNED) {
                                addSN(pro, value, isAdd, isBeep);
                                return;
                            }
                            RewriteBuilderDialog.showSimpleDialog(mActivity, "It's already scanned at: \n TLP:" + oldTlp.REAL_NUMBER + "\nAre you sure to re-scan?", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (p.NEW_SN_Map.get(value) != null && p.NEW_SN_Map.get(value)) {
                                        p.NEW_SN_Map.remove(value);

                                        if (p.SN_Map.get(value) != null)
                                            p.SN_Map.remove(value);

                                        if (p.SN_Damaged_Map.get(value) != null)
                                            p.SN_Damaged_Map.remove(value);

                                        if (p.SN_photos.get(value) != null)
                                            p.SN_photos.remove(value);

                                        p.SN.remove(value);

                                        if (Utility.isNullForList(p.SN)) {
                                            oldTlp.PRODUCTS.remove(indexTypeNum);
                                        }
                                    } else {
                                        p.SN_Map.put(value, CCT_Container_StatusKey.MISSING);
                                    }
                                    addSN(pro, value, isAdd, isBeep);
                                }
                            });
                        }

                    }//---------如果不存在则直接添加该clp
                    else {
                        addSN(pro, value, isAdd, isBeep);
                    }
                }
                return;
            }
            return;
        }
        addSN(pro, value, isAdd, isBeep);
    }

    /**
     * @param @param value
     * @Description:查询数据是否存在外面列表为空则没有
     */
    private String indexData(String value) {
        return CCT_Verify_Scan_TLP.indexMap.get("SN" + value);
    }

    /**
     * @param @param jobj
     * @param @param value
     * @Description:添加SN
     */
    private void addSN(final CCT_ProductBean pro, String value, boolean isAdd, boolean isBeep) {
        if (!isAdd) {
            if (isBeep) {
                if (pro.SN_Map.get(value) != null && pro.SN_Map.get(value) == CCT_Container_StatusKey.SCANNED) {
                    TTS.getInstance().speakAll_withToast("You have already scanned this container.");
                } else {
                    TTS.getInstance().speakAll("Scanned");
                }
            }

            int selectGroupNum = -1;
            for (int i = 0; i < CCT_Verify_Scan_TLP.tlpBean_CPlist.size(); i++) {
                if (CCT_Verify_Scan_TLP.tlpBean_CPlist.get(i).containerType == Cycle_Count_Task_Key.CCT_PRODUCT && CCT_Verify_Scan_TLP.tlpBean_CPlist.get(i).product_type.PC_ID == pro.PC_ID) {
                    if (!Utility.isNullForList(pro.SN)) {
                        for (int j = 0; j < pro.SN.size(); j++) {
                            if (pro.SN.get(j).equals(value)) {
                                selectGroupNum = i;
                                break;
                            }
                        }
                    }
                }
            }

            pro.SN_Map.put(value, CCT_Container_StatusKey.SCANNED);
            CCT_JudgeIcon.judgeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(selectGroupNum));
            adpOthers.notifyDataSetChanged();
            return;
        }

        pro.SN_Map.put(value, CCT_Container_StatusKey.SCANNED);

        //--------首先判断当前的产品列表中是否有相同的model_number 如果有相同的则添加到当前产品列表中显示
        for (int i = 0; i < CCT_Verify_Scan_TLP.tlp.PRODUCTS.size(); i++) {
            if (CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).PC_ID != 0 && CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).PC_ID == pro.PC_ID) {
                CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).SN.add(0, value);
                CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).QUANTITY += 1;
                CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).SN_Map.put(value, CCT_Container_StatusKey.SCANNED);
                CCT_Verify_Scan_TLP.tlp.PRODUCTS.get(i).NEW_SN_Map.put(value, true);

                CCT_Verify_Scan_TLP.tlpBean_CPlist = CCT_ContainerBean.getContainerList(CCT_Verify_Scan_TLP.tlp);
                adpOthers.notifyDataSetChanged();
                return;
            }
        }
        //--------如果不相同则新建pro种类 添加进行显示

        pro.SN_Map.put(value, CCT_Container_StatusKey.SCANNED);
        pro.NEW_SN_Map.put(value, true);

        CCT_Verify_Scan_TLP.tlp.PRODUCTS.add(0, pro);

        CCT_Verify_Scan_TLP.tlpBean_CPlist = CCT_ContainerBean.getContainerList(CCT_Verify_Scan_TLP.tlp);
        adpOthers.notifyDataSetChanged();
    }

    public void showProSn_Dlg_photos(String sku, final CCT_ProductBean pro, final String SN) {
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
        ttp = (TabToPhoto) view.findViewById(R.id.ttp);
        initProSn_TTP(ttp, sku, pro, SN);

        // dlg
        BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new BottomDialog.OnSubmitClickListener() {
            @Override
            public void onSubmitClick(BottomDialog dlg, String value) {
                if (ttp.getPhotoCnt(false) > 0)
                    reqUploadProSn_Photos(pro, ttp, SN);
                dlg.dismiss();
            }
        });

    }

    private void initProSn_TTP(TabToPhoto ttp_dlg, final String sku, final CCT_ProductBean pro, final String SN) {
        ttp = ttp_dlg;
        String key = TTPKey.getCCT_Key(sku, mTaskBean.ID + "_" + mLocation.LOCATION_ID + "_" + CCT_Verify_Scan_TLP.tlp.CON_ID + "_" + SN);
        ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
        ttp_dlg.pvs[0].addWebPhotoToIv(pro.SN_photos.get(SN));
        ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

            @Override
            public void onDeleteComplete(int tabIndex, TTPImgBean b) {
                // TODO Auto-generated method stub
                List<TTPImgBean> imgs = pro.SN_photos.get(SN);
                if (imgs != null && imgs.contains(b)) {
                    imgs.remove(b);
                    adpOthers.notifyDataSetChanged();
                }
            }
        });
    }

    private void reqUploadProSn_Photos(final CCT_ProductBean b, final TabToPhoto ttp, final String SN) {
        RequestParams p = new RequestParams();
        String fileName = mTaskBean.ID + "_" + mLocation.LOCATION_ID + "_" + CCT_Verify_Scan_TLP.tlp.CON_ID + "_" + SN;
        p.add("file_name", fileName);
        ttp.uploadZip(p, "receive");
        new SimpleJSONUtil() {

            @Override
            public void handReponseJson(JSONObject json) {
                // TODO Auto-generated method stub
                ttp.clearData();
                // UIHelper.showToast("Success!");
                JSONArray ja = json.optJSONArray("PHOTO");
                if (Utility.isEmpty(ja))
                    return;
                List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
                b.SN_photos.put(SN, listPhotos);
                adpOthers.notifyDataSetChanged();
                UIHelper.showToast(getString(R.string.sync_success));
            }
        }.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

    }

    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey() {
        onBackBtnOrKey(true);
    }

    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey(boolean isExit) {
        if (isExit) {
            RewriteBuilderDialog.newSimpleDialog(mActivity, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    doSubmit();
                }
            }).show();
            return;
        }
        Intent intent = new Intent(mActivity, CCT_Verify_Scan_TLP.class);
        intent.putExtra("pro_index", pro_index);
        setResult(resultCode, intent);
        finish();
        overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
    }

    /**
     * 提交方法
     */
    private void doSubmit() {
        pro.IS_HAS_SN = (selectNum == HAS_SN) ? Cycle_Count_Task_Key.CCT_P_HAS_SN : Cycle_Count_Task_Key.CCT_P_NO_SN;
        if (selectNum == HAS_SN) {
            doHasSN();
        } else {
            doNoSN();
        }
    }

    /**
     * 处理有SN的提交选项
     */
    private void doHasSN() {

        if (Utility.isNullForList(pro.SN)) {
            if(pro.IS_NEW){
                RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "New SKU with quantity 0 will be removed.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CCT_Verify_Scan_TLP.tlpBean_CPlist.remove(pro_index);
                        CCT_Verify_Scan_TLP.tlp.PRODUCTS.remove(pro);
                        onBackBtnOrKey(false);
                    }
                }).show();
            }else{
                //使产品的状态变为已经操作
                pro.is_opreation = true;
                //判断商品本身的图标
                CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP = CCT_JudgeIcon.judgePro_TypeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type);
                //给予显示的列表
                CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).statusLP = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP;
                onBackBtnOrKey(false);
            }
            return;
        }

        if (!isAllSnScanned()) {
            snPromptDialog();
        } else {
            submitHasSN();
        }
    }

    /**
     * 提交有SN的方法
     */
    private void submitHasSN() {
        pro.TOTAL = Utility.isNullForList(pro.SN) ? 0 : pro.SN.size();
        pro.DAMAGEDCNT = Utility.isNullForMap(pro.SN_Damaged_Map) ? 0 : pro.SN_Damaged_Map.size();
        String sn_list = "";
        for (int i = 0; i < pro.SN.size(); i++) {
            if (i == 0) {
                sn_list += pro.SN.get(i).toUpperCase(Locale.ENGLISH);
            } else {
                sn_list += ("," + pro.SN.get(i).toUpperCase(Locale.ENGLISH));
            }
        }

        //判断商品本身的图标
        CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP = CCT_JudgeIcon.judgePro_TypeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type);
        //给予显示的列表
        CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).statusLP = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP;

        conn.CCT_CheckSnList(new NetConnectionInterface.SyncJsonHandler(mActivity) {
            @Override
            public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
                return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
            }

            @Override
            public void handReponseJson(JSONObject json) {
                onBackBtnOrKey(false);
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {
                if (json == null) return;
                conn.handFail(json);
            }
        }, pro.PC_ID, sn_list);
    }

    /**
     * 处理没有SN的提交选项
     */
    private void doNoSN() {
        Utility.colseInputMethod(mActivity, e_total);
        Utility.colseInputMethod(mActivity, e_damage);

        String total = StringUtil.isNullOfStr(e_total.getText().toString()) ? "0" : e_total.getText().toString();
        int totalNum = Utility.parseInt(total);
        if (totalNum == 0&&pro.IS_NEW) {
            RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "New SKU with quantity 0 will be removed.", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CCT_Verify_Scan_TLP.tlpBean_CPlist.remove(pro_index);
                    CCT_Verify_Scan_TLP.tlp.PRODUCTS.remove(pro);
                    onBackBtnOrKey(false);
                }
            }).show();
            return;
        }

        String damage = e_damage.getText().toString();
        if (StringUtil.isNullOfStr(damage)) {
            damage = "0";
        }
        int damageNum = Utility.parseInt(damage);

        if (damageNum > totalNum) {
            UIHelper.showToast("Amount of damage cannot exceed the total amount!");
            e_damage.setText("");
            e_damage.requestFocus();
            return;
        }
        pro.TOTAL = totalNum;
        pro.DAMAGEDCNT = damageNum;

        pro.is_opreation = true;

        //判断商品本身的图标
        CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP = CCT_JudgeIcon.judgePro_TypeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type);
        //给予显示的列表
        CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).statusLP = CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.statusLP;

        //清空关于SN的一切操作
        pro.SN.clear();
        pro.SN_Map.clear();
        pro.NEW_SN_Map.clear();
        pro.SN_Damaged_Map.clear();

        if (damageNum != 0) {
            CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.isDamaged = true;
        } else {
            CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index).product_type.isDamaged = false;
        }

        adpOthers.notifyDataSetChanged();

        //判断是否有坏的并且是否有需要上传的图片 如果有需要上传的图片则上传
        if (damageNum > 0 && ttp.getPhotoCnt(false) > 0)
            reqUploadProNoSn_Photos(pro, ttp);
        else {
            onBackBtnOrKey(false);
        }
    }

    /**
     * 如果有SN则检验SN状态 在提交 如果没有SN 直接提交
     */
    private void checkSnScan() {
        if (!isAllSnScanned()) {
            snPromptDialog();
        } else {
            doSubmit();
        }
    }

    /**
     * 弹出提示窗口 是否将未扫描的SN 置为丢失状态
     */
    private void snPromptDialog() {
        RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "All unchecked SN will be marked as missing.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < pro.SN.size(); i++) {
                    if (Utility.isNullForMap(pro.SN_Map) || pro.SN_Map.get(pro.SN.get(i)) == null) {
                        pro.SN_Map.put(pro.SN.get(i), CCT_Container_StatusKey.MISSING);
                    }
                }
                CCT_JudgeIcon.judgeIcon(CCT_Verify_Scan_TLP.tlpBean_CPlist.get(pro_index));
                submitHasSN();
            }
        }).show();
    }

    /**
     * 检查SN是否都被扫描 false 没有 |  true 有
     *
     * @return
     */
    private boolean isAllSnScanned() {
        if (selectNum == HAS_SN) {
            if (!Utility.isNullForList(pro.SN)) {
                for (int i = 0; i < pro.SN.size(); i++) {
                    if (Utility.isNullForMap(pro.SN_Map) || pro.SN_Map.get(pro.SN.get(i)) == null) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (ttp != null)
            ttp.onActivityResult(requestCode, resultCode, data);
    }

    // ==================nested===============================

    private BaseAdapter adpOthers = new BaseAdapter() {

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            HolderConPro holder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.cct_verify_pro_item, null);
                holder = new HolderConPro();

                holder.sn_number = (TextView) convertView.findViewById(R.id.sn_number);
                holder.item = convertView.findViewById(R.id.item);
                holder.select_condition = convertView.findViewById(R.id.select_clp_condition);
                holder.status_icon = (ImageView) convertView.findViewById(R.id.clp_status_icon);
                holder.image_btn = convertView.findViewById(R.id.image_btn);
                holder.iv = (ImageView) convertView.findViewById(R.id.iv);
                holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
                holder.is_damage_btn = (Button) convertView.findViewById(R.id.is_damage_btn);
                holder.is_new = convertView.findViewById(R.id.is_new);
                ;

                convertView.setTag(holder);
            } else {
                holder = (HolderConPro) convertView.getTag();
            }


            final String pro_sn = pro.SN.get(position);
            holder.sn_number.setText("S/N: " + pro_sn);

            int status = CCT_Container_StatusKey.UNSCANNED;// 初始化左侧图标的默认值 默认为未扫描
            //如果已经扫描则 获取当前SN的状态
            if (pro.SN_Map.get(pro_sn) != null) {
                status = pro.SN_Map.get(pro_sn);
            }
            //判断当前的SN状态 如果已经扫描 并且为丢失状态则隐藏 图片图标 和 damege 按钮
            if (pro.SN_Map.get(pro_sn) != null && pro.SN_Map.get(pro_sn) == CCT_Container_StatusKey.MISSING) {
                holder.is_damage_btn.setVisibility(View.INVISIBLE);
                holder.image_btn.setVisibility(View.INVISIBLE);
            } else {//否则 显示按钮 并且赋予事件
                holder.is_damage_btn.setVisibility(View.VISIBLE);
                //------判断Damage按钮的背景显示
                final boolean isDamaged = (pro.SN_Damaged_Map.get(pro_sn) == null ? false : pro.SN_Damaged_Map.get(pro_sn));
                holder.is_damage_btn.setBackgroundResource(isDamaged ? R.drawable.cct_damage_btn_red : R.drawable.cct_damage_btn_gray);
                holder.is_damage_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pro.SN_Damaged_Map.put(pro_sn, !isDamaged);
                        doScanSN(pro_sn, false);
                        adpOthers.notifyDataSetChanged();
                        if (pro.SN_Damaged_Map.get(pro_sn)) {
                            showProSn_Dlg_photos(pro.P_NAME, pro, pro_sn);
                        }
                    }
                });

                if (isDamaged) {
                    holder.image_btn.setVisibility(View.VISIBLE);
                    //------对于Damage的lp进行拍照
                    List<TTPImgBean> sn_photos = pro.SN_photos.get(pro.SN.get(position));
                    boolean hasPhoto = !Utility.isNullForList(sn_photos);
                    holder.iv.setSelected(hasPhoto);
                    holder.tvImgCnt.setText(hasPhoto ? (sn_photos.size() + "") : "0");
                    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
                    holder.image_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!Utility.isFastClick(500)) {
                                showProSn_Dlg_photos(pro.P_NAME, pro, pro_sn);
                            }
                        }
                    });
                } else {
                    holder.image_btn.setVisibility(View.INVISIBLE);
                }
            }

            holder.is_new.setVisibility(pro.NEW_SN_Map.get(pro_sn) != null && pro.NEW_SN_Map.get(pro_sn) ? View.VISIBLE : View.GONE);

            holder.select_condition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Utility.isFastClick(500)) {
                        dialogCondition(position, true);
                    }
                }
            });


            // -----更改小图标背景
            holder.status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(status));
            // 背景
            if (position != 0 && getCount() - 1 == position) {
                holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
            } else if (position == 0) {
                if (position == (getCount() - 1)) {
                    holder.item.setBackgroundResource(R.drawable.cct_lv_item);
                } else {
                    holder.item.setBackgroundResource(R.drawable.cct_lv_item_top_del);
                }
            } else {
                holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
            }
            return convertView;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return pro.SN == null ? 0 : pro.SN.size();
        }
    };

    class HolderConPro {
        View item;
        View select_condition;
        ImageView status_icon;
        TextView sn_number;

        Button is_damage_btn;
        View image_btn;
        ImageView iv;
        TextView tvImgCnt;

        View is_new;
    }
}
