package oso.ui.inventory.cyclecount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.iface.VerifyScanTLP_Iface;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.widget.photo.bean.TTPImgBean;
import utility.StringUtil;
import utility.Utility;

public class CCT_ScanTLP_Adp extends BaseExpandableListAdapter {

	private CCT_ScanTLP_Adp oThis;

	private Context content;
	private List<CCT_ContainerBean> listBean;
	private LayoutInflater inflater;
	
	private final int PRODUCT = 0;
	private final int CLP = 1;
	
	private VerifyScanTLP_Iface iface;
	
	private boolean isVerify;
	
	@Override //不能超过Count数量
	public int getGroupType(int groupPosition) {
		// TODO Auto-generated method stub
		return listBean.get(groupPosition).containerType==Cycle_Count_Task_Key.CCT_PRODUCT?PRODUCT:CLP;
	}

	@Override
	public int getGroupTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	
	/**
	 * @Description:构造方法
	 */
	public CCT_ScanTLP_Adp(Context context, List<CCT_ContainerBean> listBean,VerifyScanTLP_Iface iface,boolean isVerify) {
		this.content = context;
		this.listBean = listBean;
		this.inflater = LayoutInflater.from(content);
		this.iface = iface;
		this.isVerify = isVerify;
		oThis = this;
	}


	public void setDataList(List<CCT_ContainerBean> listBean){
		this.listBean = listBean;
		oThis.notifyDataSetChanged();
	}
	
	// ====================



	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		Holder_TLP_Grp holder;
		if (convertView == null) {
			holder = new Holder_TLP_Grp();
			convertView = initGroupView(groupPosition, holder);
			convertView.setTag(holder);
		} else{
			holder = (Holder_TLP_Grp) convertView.getTag();
		}
		fillGroupViewData(isExpanded, groupPosition, holder);

		return convertView;
	}

	/**
	 * @Description:加载父级控件
	 */
	private View initGroupView(int groupPosition, Holder_TLP_Grp holder) {
		View convertView = null;
		switch (getGroupType(groupPosition)) {
		case PRODUCT:
			convertView = inflater.inflate(R.layout.cct_pro_item, null);
			
			holder.edit_total = (View) convertView.findViewById(R.id.edit_total);
			holder.quantity_lay = (View) convertView.findViewById(R.id.quantity_lay);
			holder.quantity = (TextView) convertView.findViewById(R.id.quantity);
			holder.p_sku = (TextView) convertView.findViewById(R.id.p_sku);
			holder.isShowTotal = (View) convertView.findViewById(R.id.no_sn);
		
			holder.total_layout = (View) convertView.findViewById(R.id.total_layout);
			holder.damage_layout = (View) convertView.findViewById(R.id.damage_layout);
			holder.p_total = (TextView) convertView.findViewById(R.id.p_total);
			holder.p_damage = (TextView) convertView.findViewById(R.id.p_damage);
			
			holder.image_btn = (View) convertView.findViewById(R.id.image_btn);
			holder.iv = (ImageView) convertView.findViewById(R.id.iv);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
			
			holder.t_prompt = (TextView) convertView.findViewById(R.id.t_prompt);
			
			holder.is_new  = (View) convertView.findViewById(R.id.is_new);
			holder.process_layout = (View) convertView.findViewById(R.id.process_layout);
			break;
		case CLP:
			convertView = inflater.inflate(R.layout.cct_clp_item, null);
			
//			holder.clp_type = (TextView) convertView.findViewById(R.id.clp_type);
			holder.clp_sku = (TextView) convertView.findViewById(R.id.clp_sku);
			holder.clp_dimension = (TextView) convertView.findViewById(R.id.clp_dimension);
			break;
		}

		holder.item = convertView.findViewById(R.id.item);
		holder.arrow_icon = (ImageView) convertView.findViewById(R.id.arrow_icon);
		holder.lp_status_icon = (ImageView) convertView.findViewById(R.id.lp_status_icon);
		holder.select_condition = convertView.findViewById(R.id.select_condition);
		holder.model_number  = (View) convertView.findViewById(R.id.model_number);

		holder.t_process = (TextView) convertView.findViewById(R.id.t_process);
		holder.t_count = (TextView) convertView.findViewById(R.id.t_count);
		
		return convertView;
	}
	
	/**
	 * @Description:为父级空间添加数据
	 */
	private void fillGroupViewData(boolean isExpanded, final int groupPosition, Holder_TLP_Grp holder) {
		// 刷新-数据
		final CCT_ContainerBean c = listBean.get(groupPosition);
		boolean isProduct = (c.containerType == Cycle_Count_Task_Key.CCT_PRODUCT);

		boolean hasChild = false;// 判断是否有子结构
		if (isProduct) {
			final CCT_ProductBean pro = c.product_type;
			
			// -----更改小图标背景
			holder.lp_status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(c.statusLP));

			/*hasChild = !Utility.isNullForList(pro.SN)*//*pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_HAS_SN*/;
			
			holder.isShowTotal.setVisibility((!hasChild&&pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_NO_SN)?View.VISIBLE:View.GONE);
			holder.is_new.setVisibility((/*!hasChild&&pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_NO_SN&&*/pro.IS_NEW&&isVerify)?View.VISIBLE:View.GONE);
			
			
			/*holder.t_prompt.setVisibility((pro.QUANTITY==0&&pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_HAS_SN)?View.VISIBLE:View.GONE);*/
			
			//-----判断是否有model_number
			if(!StringUtil.isNullOfStr(pro.P_NAME)){
				holder.model_number.setVisibility(View.VISIBLE);
				holder.p_sku.setText(pro.P_NAME);
			}else{
				holder.model_number.setVisibility(View.GONE);
			}
			
			//-----判断是否有值如果没有则不显示
			if(pro.QUANTITY==0){
				holder.quantity_lay.setVisibility(View.GONE);
			}else{
				holder.quantity_lay.setVisibility(View.VISIBLE);
				holder.quantity.setText(pro.QUANTITY+"");
			}
			

			boolean isShowTotal = pro.TOTAL!=0;
			boolean isShowDamage = pro.DAMAGEDCNT!=0;
			holder.total_layout.setVisibility(isShowTotal?View.VISIBLE:View.GONE);
			holder.damage_layout.setVisibility(isShowDamage?View.VISIBLE:View.GONE);
			
			holder.p_total.setText(hasChild?"":pro.TOTAL+"");
			holder.p_damage.setText(hasChild?"":pro.DAMAGEDCNT+"");

//			boolean isNoSN = (getChildrenCount(groupPosition)==0);
			
			
			
			if (!hasChild&&pro.IS_HAS_SN==Cycle_Count_Task_Key.CCT_P_NO_SN) {
				holder.process_layout.setVisibility(View.GONE);
				if(pro.isDamaged){
					holder.image_btn.setVisibility(View.VISIBLE);
					//------对于Damage的lp进行拍照
					boolean hasPhoto = !Utility.isNullForList(pro.photos);
				    holder.iv.setSelected(hasPhoto);
				    holder.tvImgCnt.setText(hasPhoto?(pro.photos.size()+""):"0");
				    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
					holder.image_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!Utility.isFastClick(500)){
								iface.showProNoSn_Dlg_photos(pro.P_NAME, pro);
							}
						}
					});
				}else{
					holder.image_btn.setVisibility(View.INVISIBLE);
				}
				holder.item.setEnabled(false);

//				holder.edit_total.setVisibility(View.VISIBLE);
			}else{
				
				
				holder.process_layout.setVisibility(Utility.isNullForList(pro.SN)?View.GONE:View.VISIBLE);
				if(!Utility.isNullForList(pro.SN)){
					holder.t_process.setText(jumpScanSize(pro)+"");
					holder.t_count.setText(" / "+pro.SN.size());
				}
				
				holder.image_btn.setVisibility(View.GONE);
				
//				holder.edit_total.setVisibility(View.GONE);

				holder.item.setEnabled(false);//setOnClickListener(null);
			}
			
			holder.select_condition.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!Utility.isFastClick(500)){
						iface.dialogCondition(groupPosition);
					}
				}
			});
			
			
		} else {
			CCT_CLP_Type_Bean clp_type = c.clp_type;

//			holder.clp_type.setText(clp_type.LP_TYPE_NAME);
			if(!StringUtil.isNullOfStr(clp_type.P_NAME)){
				holder.model_number.setVisibility(View.VISIBLE);
				holder.clp_sku.setText(clp_type.P_NAME);
			}else{
				holder.model_number.setVisibility(View.GONE);
			}
//			holder.clp_sku.setText(clp_type.MODEL_NUMBER);
//			holder.clp_lot.setText(clp_type.LOT_NUMBER);
			holder.clp_dimension.setText(clp_type.LP_TYPE_NAME);
			
			holder.t_process.setText(jumpScanSize(clp_type.CONTAINERS)+"");
			holder.t_count.setText(" / "+clp_type.CONTAINERS.size());
			
			
			// -----更改小图标背景
			holder.lp_status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(c.statusLP));

			hasChild = !Utility.isNullForList(clp_type.CONTAINERS);
			

			holder.select_condition.setOnClickListener(getChildrenCount(groupPosition)==0?null:new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(!Utility.isFastClick(500)){
						iface.dialogCondition(groupPosition);
					}
				}
			});
		}
		
		
		
		// 关闭|无条目时,显示圆背景
		boolean showOne = !isExpanded || !hasChild;
		holder.item.setBackgroundResource(showOne ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
		// 更改箭头方向
		holder.arrow_icon.setBackgroundResource(showOne ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
	}


	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGroupCount() {
		return Utility.isNullForList(listBean) ? 0 : listBean.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int jumpScanSize(CCT_ProductBean pro){
		 if(Utility.isNullForList(pro.SN)){
			 return 0;
		 }
		 int a = 0;
		 for (int i = 0; i < pro.SN.size(); i++) {
			if(pro.SN_Map.get(pro.SN.get(i))!=null&&pro.SN_Map.get(pro.SN.get(i))!=CCT_Container_StatusKey.UNSCANNED){
				a++;
			}
		 }
		 return a;
	 }
	
	public int jumpScanSize(List<CCT_CLP_Bean> list){
		 if(Utility.isNullForList(list)){
			 return 0;
		 }
		 int a = 0;
		 for (int i = 0; i < list.size(); i++) {
			if(list.get(i).statusLP!=CCT_Container_StatusKey.UNSCANNED){
				a++;
			}
		 }
		 return a;
	 }

	/**********************************************************************************************************************/
	
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if (listBean.get(groupPosition).containerType == Cycle_Count_Task_Key.CCT_PRODUCT) {
			CCT_ProductBean pro = (listBean.get(groupPosition).product_type);
			return /*Utility.isNullForList(pro.SN) ?*/ 0/* : pro.SN.size()*/;
		} else {
			CCT_CLP_Type_Bean c = (listBean.get(groupPosition).clp_type);
			return Utility.isNullForList(c.CONTAINERS) ? 0 : c.CONTAINERS
					.size();
		}
	}

	@Override
	public int getChildType(int groupPosition, int childPosition) {
		return getGroupType(groupPosition);
	}

	@Override
	public int getChildTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	
	@Override
	public View getChildView(final int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		Holder_TLP_Child holder;
		if (convertView == null) {
			holder = new Holder_TLP_Child();
			convertView = initChildView(groupPosition,childPosition, holder);
			convertView.setTag(holder);
		} else
			holder = (Holder_TLP_Child) convertView.getTag();

		fillChildViewData( groupPosition, childPosition, holder);

		return convertView;
	}

	/**
	 * @Description:加载父级控件
	 */
	private View initChildView(int groupPosition,int childPosition, Holder_TLP_Child holder) {
		View convertView = null;
		switch (getChildType(groupPosition,childPosition)) {
		case PRODUCT:
			convertView = inflater.inflate(R.layout.cct_verify_pro_item, null);
			holder.sn_number = (TextView) convertView.findViewById(R.id.sn_number);
			break;
		case CLP:
			convertView = inflater.inflate(R.layout.cct_verify_clp_item, null);
			
			holder.clp_number = (TextView) convertView.findViewById(R.id.clp_number);
	
			break;
		}

		holder.item = convertView.findViewById(R.id.item);
		holder.select_condition = convertView.findViewById(R.id.select_clp_condition);
		holder.status_icon = (ImageView) convertView.findViewById(R.id.clp_status_icon);
		
		holder.image_btn = (View) convertView.findViewById(R.id.image_btn);
		holder.iv = (ImageView) convertView.findViewById(R.id.iv);
		holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgCnt);
		
		holder.is_damage_btn = (Button) convertView
				.findViewById(R.id.is_damage_btn);
		

		holder.is_new = (View) convertView.findViewById(R.id.is_new);
		
		return convertView;
	}
	
	/**
	 * @Description:为父级空间添加数据
	 */
	private void fillChildViewData(final int groupPosition,final int childPosition, Holder_TLP_Child holder) {
		// 刷新-数据
		final CCT_ContainerBean c = listBean.get(groupPosition);
		boolean isProduct = (c.containerType == Cycle_Count_Task_Key.CCT_PRODUCT);

		int status = CCT_Container_StatusKey.UNSCANNED;
		
		if (isProduct) {
			// 刷新-视图
			final CCT_ProductBean pro = c.product_type;
			
			
			final String pro_sn = pro.SN.get(childPosition);

			
			if(pro.SN_Map.get(pro_sn)!=null){
				status = pro.SN_Map.get(pro_sn);
			}

			holder.sn_number.setText("S/N: "+pro_sn);
			
			
			if(pro.SN_Map.get(pro_sn)!=null&&pro.SN_Map.get(pro_sn) == CCT_Container_StatusKey.MISSING){
				holder.is_damage_btn.setVisibility(View.INVISIBLE);
				holder.image_btn.setVisibility(View.INVISIBLE);
			}else{
				holder.is_damage_btn.setVisibility(View.VISIBLE);
				//------判断Damage按钮的背景显示
				final boolean isDamaged = (pro.SN_Damaged_Map.get(pro_sn)==null?false:pro.SN_Damaged_Map.get(pro_sn));
				holder.is_damage_btn.setBackgroundResource(isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
				holder.is_damage_btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
							pro.SN_Damaged_Map.put(pro_sn, !isDamaged);
							oThis.notifyDataSetChanged();
							iface.changeSN_Scanned(groupPosition, childPosition);
							if(pro.SN_Damaged_Map.get(pro_sn)){
								iface.showProSn_Dlg_photos(pro.P_NAME, pro,pro_sn);
							}
					}
				});
				
				if(isDamaged){
					holder.image_btn.setVisibility(View.VISIBLE);
					//------对于Damage的lp进行拍照
					List<TTPImgBean> sn_photos = pro.SN_photos.get(pro.SN.get(childPosition));
					boolean hasPhoto = !Utility.isNullForList(sn_photos);
				    holder.iv.setSelected(hasPhoto);
				    holder.tvImgCnt.setText(hasPhoto?(sn_photos.size()+""):"0");
				    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
					holder.image_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!Utility.isFastClick(500)){
								iface.showProSn_Dlg_photos(pro.P_NAME, pro,pro_sn);
							}
						}
					});
				}else{
					holder.image_btn.setVisibility(View.INVISIBLE);
				}
			}
			
			holder.is_new.setVisibility(pro.NEW_SN_Map.get(pro_sn)!=null&&pro.NEW_SN_Map.get(pro_sn)&&isVerify?View.VISIBLE:View.GONE);
			
		} else {
			// 刷新-视图
			final CCT_CLP_Type_Bean clp_type = c.clp_type;
			final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(childPosition);

			
			status = clp.statusLP;
			
			holder.clp_number.setText(Cycle_Count_Task_Key.getContainerTypeValue(clp.CONTAINER_TYPE)+": "+clp.REAL_NUMBER);


			if(clp.statusLP==CCT_Container_StatusKey.MISSING){
				holder.is_damage_btn.setVisibility(View.INVISIBLE);
				holder.image_btn.setVisibility(View.INVISIBLE);
			}else{
				holder.is_damage_btn.setVisibility(View.VISIBLE);
				//------判断Damage按钮的背景显示
				holder.is_damage_btn.setBackgroundResource(clp.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
				holder.is_damage_btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(!Utility.isFastClick(500)){
							clp.isDamaged = !clp.isDamaged;
							oThis.notifyDataSetChanged();
							iface.changeCLP_Scanned(groupPosition, childPosition);
							if(clp.isDamaged){
								iface.showCLP_Dlg_photos(clp_type.MODEL_NUMBER, clp);
							}
							
						}
					}
				});
				if(clp.isDamaged){
					holder.image_btn.setVisibility(View.VISIBLE);
					//------对于Damage的lp进行拍照
					boolean hasPhoto = !Utility.isNullForList(clp.photos);
				    holder.iv.setSelected(hasPhoto);
				    holder.tvImgCnt.setText(hasPhoto?(clp.photos.size()+""):"0");
				    holder.tvImgCnt.setVisibility(hasPhoto ? View.VISIBLE : View.GONE);
					holder.image_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!Utility.isFastClick(500)){
								iface.showCLP_Dlg_photos(clp_type.MODEL_NUMBER, clp);
							}
						}
					});
				}else{
					holder.image_btn.setVisibility(View.INVISIBLE);
				}
			}
			holder.is_new.setVisibility(clp.isNewData&&isVerify?View.VISIBLE:View.GONE);
		}
		
		
		
		holder.select_condition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!Utility.isFastClick(500)){
					iface.dialogCondition(groupPosition,childPosition);
				}
			}
		});

		
		// -----更改小图标背景
		holder.status_icon.setBackgroundResource(CCT_Container_StatusKey.getResource(status));
		// 背景
		if (getChildrenCount(groupPosition) - 1 == childPosition) {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
		} else {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
		}
	}

	
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}
	
	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----打开全部  modify by xialimin
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++) {
//			 if (listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP) {
				 listview.expandGroup(i);
//			 }else {
//				 listview.collapseGroup(i);
//			 }
		 }
	 }


	/* //-----打开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }*/
	
};

class Holder_TLP_Grp {
	View item;
	ImageView arrow_icon;
	View select_condition;
	ImageView lp_status_icon;
	View model_number;
	
	// -------Product
	View edit_total;
	View quantity_lay;
	TextView quantity;
	TextView p_sku;
	View isShowTotal;
	View no_sn;
	
	View total_layout;
	View damage_layout;
	TextView p_total;
	TextView p_damage;
	View image_btn;
	ImageView iv;
	TextView tvImgCnt;
	TextView t_prompt;
	View is_new;
	View process_layout;
	
	// -------CLP
//	TextView clp_type;
	TextView clp_sku;
	TextView clp_dimension;
	TextView t_process;
	TextView t_count;

}

class Holder_TLP_Child {
	View item;
	View select_condition;
	ImageView status_icon;
	TextView clp_number;
	TextView sn_number;
	
	Button is_damage_btn;
	View image_btn;
	ImageView iv;
	TextView tvImgCnt;

	View is_new;
}