package oso.ui.inventory.cyclecount.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import declare.com.vvme.R;
import support.common.datas.HoldDoubleValue;
import support.common.datas.HoldThreeValue;

public class CCT_Sel_Adapter extends BaseAdapter {

	private Context ct;
	private List<HoldThreeValue<String,String,Object>> list;
	public int curIndex = -1;
	private boolean isWrapContent;//控制是否显示随动行高 默认为false 不随动
	private boolean isShowSel = true;//控制是否显示选中效果 默认为true 选中

	public CCT_Sel_Adapter(Context ct, List<HoldThreeValue<String,String,Object>> list, int defIndex, boolean isWrapContent) {
		this.ct = ct;
		this.list = list;
		this.curIndex = defIndex;
		this.isWrapContent = isWrapContent;
	}



	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.size();
	}

	@Override
	public HoldDoubleValue<String,String>  getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		Holder h;
		if (convertView == null) {
			convertView = LayoutInflater.from(ct).inflate(
					R.layout.tms_simple_list_single_sel, null);
			h = new Holder();
			h.line_gray = (View) convertView.findViewById(R.id.line_gray);
			h.text1 = (TextView) convertView.findViewById(R.id.text1);
			h.vSel = convertView.findViewById(R.id.vSel);
			h.cb_dlg = (RadioButton) convertView.findViewById(R.id.cb_dlg);

			if(isWrapContent){
				h.text1.setSingleLine(false);
				ViewGroup.LayoutParams lp = h.text1.getLayoutParams();
				lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
				h.text1.setLayoutParams(lp);
			}else{
				h.text1.setSingleLine(true);
			}

			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();



		// 刷ui
		h.text1.setText(list.get(position).b);

		//选中-样式
		boolean isSelected = ((position == curIndex)&&isShowSel);

		h.vSel.setVisibility(isSelected ? View.VISIBLE : View.INVISIBLE);
		h.cb_dlg.setVisibility(/*isSelected ? View.VISIBLE : */View.INVISIBLE);
//		h.cb_dlg.setChecked(isSelected);

		h.line_gray.setVisibility(position==0?View.GONE:View.VISIBLE);

		return convertView;
	}


	/**
	 * 设置当前选中项
	 * @param index
	 */
	public void setIndex(int index){
		this.curIndex = index;
		this.notifyDataSetChanged();
	}

	/**
	 * 控制是否显示选中效果
	 * @param isShowSel
	 */
	public void setShowSel(boolean isShowSel){
		this.isShowSel = isShowSel;
	}

	class Holder {
		View line_gray;
		TextView text1;
		View vSel;
		RadioButton cb_dlg;
	}
}