package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_GetCreateTLP_Info implements Serializable{

    private static final long serialVersionUID = 3925954911733927182L;

    public List<CCT_PackagingType> packaging_types;
    public List<CCT_TitleAndCustomerInfo.Title> titles;
    public List<CCT_TitleAndCustomerInfo.Customer> customers;

}
