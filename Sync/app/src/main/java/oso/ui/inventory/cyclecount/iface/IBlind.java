package oso.ui.inventory.cyclecount.iface;

import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;

/**
 * 
 * @author xialimin
 * @date 2015-4-23 上午9:57:27
 * 
 * @description 盲盘图片上传
 */
public interface IBlind {
	public void showDlg_photos_tlp(CCT_TLP_Bean tlpBean);
	public void showDlg_photos_clp(CCT_CLP_Bean clpBean);
	public void showDlg_photos_pdt(int pdtType, CCT_ProductBean pdtBean);
	public void showEditDlg(int grpPosition, int pdtType, CCT_ProductBean pdtBean);
	
	/**
	 * 点击status icon弹出删除dialog
	 * @param type
	 * @param groupPosition
	 * @param childPosition  -1表示 没有子结构
	 */
	public void showDlg_onClickStatusIcon(int type, int groupPosition, int childPosition);
}
