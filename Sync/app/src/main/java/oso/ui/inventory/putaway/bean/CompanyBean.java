package oso.ui.inventory.putaway.bean;

import java.io.Serializable;
import java.util.List;

public class CompanyBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1092447093815157970L;

	public String company_id;

	public static String[] getTypes(List<CompanyBean> listTypes) {
		
		if(listTypes == null)
			return null;
		String[] str = new String[listTypes.size()];
		for(int i=0; i< listTypes.size(); i++){
			str[i] = listTypes.get(i).company_id;
		}
		return str;
	}
}
