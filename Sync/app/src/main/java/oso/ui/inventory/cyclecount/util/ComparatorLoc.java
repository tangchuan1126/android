package oso.ui.inventory.cyclecount.util;

import java.util.Comparator;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;

public class ComparatorLoc implements Comparator<CCT_LocationBean>{

	@Override
	public int compare(CCT_LocationBean lhs, CCT_LocationBean rhs) {
		//---------判断是否完成 
		if(lhs.STATUS<Cycle_Count_Task_Key.Location_SCANNED&&rhs.STATUS<Cycle_Count_Task_Key.Location_SCANNED){
			String str1 = lhs.LOCATION_NAME.replaceAll("-","");
			String str2 = rhs.LOCATION_NAME.replaceAll("-","");
			//字母部分
			String alphabet1 = str1.replaceAll("\\d+$", "");
			String alphabet2 = str2.replaceAll("\\d+$", "");
			// 如果不想区分大小写，否则compareTo
			int cmpAlphabet = alphabet1.compareToIgnoreCase(alphabet2);
			if (cmpAlphabet != 0) {
				return cmpAlphabet;
			}
			// 数字部分
			String numeric1 = str1.replaceAll("[a-zA-Z]+", "");
			String numeric2 = str2.replaceAll("[a-zA-Z]+", "");
			if ("".equals(numeric1)) {
				// 即使numeric2也是空串也无所谓，当然，如果比较的不是String（或其他immutable对象）则另当别论
				return -1;
			}
			if ("".equals(numeric2)) {
				return 1;
			}
			int num1 = 0;
			try {
				num1 = Integer.parseInt(numeric1);
			}catch (Exception e){}
			int num2 = 0;
			try {
				num2 = Integer.parseInt(numeric2);
			}catch (Exception e){}
			return num1 - num2;
		}else{
		 	int m1=lhs.STATUS;
            int m2=rhs.STATUS;
            int result=0;
            if(m1>m2)
            {
                result=1;
            }
            if(m1<m2)
            {
                result=-1;
            }
            return result;
		}
		
	}
}
