package oso.ui.inventory.cyclecount.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * @author gcy
 * @ClassName: CCT_TLP_Bean
 * @Description:
 * @date 2015-4-8 下午10:04:49
 */
public class CCT_PackagingType implements Serializable {

    private static final long serialVersionUID = -8718188312837628296L;

    public String type_name;//":"chep",
    public double length;//":"60",
    public double width;//":"34",
    public double height;//":"21",
    public double weight;//":"23",
    public double max_load;//":"34",
    public double max_height;//":"43",
    public int length_uom;//":"1",
    public int weight_uom;//":"1",

    public int container_type;//": 1,
    public int type_id;//": 3


    /**
     * 获取json的字符串
     * @return
     */
    public String toJsonStr() {
//        String jsonStr = new Gson().toJson(this, CCT_PackagingType.class);
        JSONObject jsonStr = new JSONObject();
        try{
            jsonStr.put("type_name",this.type_name);
            jsonStr.put("length",this.length);
            jsonStr.put("width",this.width);
            jsonStr.put("height",this.height);
            jsonStr.put("weight",this.weight);
            jsonStr.put("max_load",this.max_load);
            jsonStr.put("max_height",this.max_height);
            jsonStr.put("length_uom",this.length_uom);
            jsonStr.put("weight_uom",this.weight_uom);
        }catch (Exception e){
            return null;
        }


        System.out.println("jsonStr= " + jsonStr.toString());
        return jsonStr.toString();
    }

}
