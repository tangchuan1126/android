package oso.ui.inventory.cyclecount.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.widget.photo.bean.TTPImgBean;
import utility.Utility;

/**
 * @ClassName: CCT_TLP_Bean 
 * @Description: 
 * @author gcy
 * @date 2015-4-8 下午10:04:49
 */
public class CCT_TLP_Bean implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -2084310784445922844L;
	
	public int TITLE_ID;
	public long CON_ID;//": 125486,
	public int CONTAINER_TYPE;
	public String CONTAINER;//": "TLP125486",
	public String REAL_NUMBER;//真实的lp number
	
	public int IS_HAS_SN;
	public int TYPE_ID;
	public int IS_FULL;
	public String LOT_NUMBER;
	
	//--------判断当前的TLP是不是新的托盘
	public boolean isNewData = false;
	
	public List<CCT_CLP_Type_Bean> CONTAINER_TYPES = new ArrayList<CCT_CLP_Type_Bean>();//":
	public List<CCT_ProductBean> PRODUCTS = new ArrayList<CCT_ProductBean>();
	
	//-------拍照图片
	public List<TTPImgBean> photos = new ArrayList<TTPImgBean>();
		
	// debug start
	public boolean isDamaged = false;
	public List<CCT_CLP_Type_Bean> clpTypeList = new ArrayList<CCT_CLP_Type_Bean>();
	public List<CCT_ProductTypeBean> pdtTypeList = new ArrayList<CCT_ProductTypeBean>();
	public List<CCT_ProductBean> pdtNoSnList = new ArrayList<CCT_ProductBean>();
	public boolean itemIsSeleted; // 扫描如果发现有重复，则item显示为选中状态，默认为false
	// debug end

	public boolean isEmpty(){
		if(Utility.isNullForList(clpTypeList)&&Utility.isNullForList(pdtTypeList)&&Utility.isNullForList(pdtNoSnList)){
			return true;
		}
		return false;
	}

	/**
	 * 获得Products中SN类型的个数
	 * @return
	 */
	public int getPdtSnCnt() {
		int cnt = 0;
		for(CCT_ProductBean pdtBean : PRODUCTS) {
			if(pdtBean.isSNPdt()) cnt++;
		}
		return cnt;
	}
	
	/**
	 * 获得Products中Model/Lot类型的个数
	 * @return
	 */
	public int getPdtModelLotCnt() {
		int cnt = 0;
		for(CCT_ProductBean pdtBean : PRODUCTS) {
			if(!pdtBean.isSNPdt()) cnt++;
		}
		return cnt;
	}
	
	/**
	 * 获得该TLP中所有Damaged的CLPs
	 * @return
	 */
	public List<CCT_CLP_Bean> getDamagedCLPs() {
		List<CCT_CLP_Bean> dClps = new ArrayList<CCT_CLP_Bean>();
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.isDamaged) dClps.add(clpBean);
			}
		}
//		for(CCT_CLP_Bean clp_Bean : clpList) {
//			if(clp_Bean.isDamaged) ids.add(clp_Bean.CON_ID);
//		}
		return dClps;
	}
	
	/**
	 * 获得所有CLP的id
	 * @return
	 */
	public List<Long> getAllCLPIds() {
		List<Long> ids = new ArrayList<Long>();
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				ids.add(clpBean.CON_ID);
			}
		}
//		for(CCT_CLP_Bean clpBean:clpList) {
//			ids.add(clpBean.CON_ID);
//		}
		return ids;
	}
	
	/**
	 * 获得该TLP中所有Damaged的Products的id
	 * @return
	 */
	public List<Long> getDamagedPdtIds() {
		List<Long> ids = new ArrayList<Long>();
		for(CCT_ProductBean pdtBean:PRODUCTS) {
			if(pdtBean.isDamaged) ids.add(pdtBean.PC_ID);
		}
		return ids;
	}
	
	/**
	 * 获得该TLP中所有Products的id
	 * @return
	 */
	public List<Long> getAllPdtsIds() {
		List<Long> ids = new ArrayList<Long>();
		for(CCT_ProductBean pdtBean:PRODUCTS) {
			ids.add(pdtBean.PC_ID);
		}
		return ids;
	}
	
	public static void helpJson(JSONObject json, List<CCT_TLP_Bean> list){
		JSONArray jArray = json.optJSONArray("TLP_CONTAINERS");
		CCT_TLP_Bean.resolveJson(jArray,list);
	}

	public static void resolveJson(JSONArray jArray,List<CCT_TLP_Bean> list){
		
		list= new Gson().fromJson(jArray.toString(), new TypeToken<List<CCT_TLP_Bean>>() {}.getType());
		
	}
	
	/**
	 * @Description:获取
	 * @param @return
	 */
	public static JSONObject getDebugStr(){
		JSONObject json = new JSONObject();
		
		Random r = new Random();
		try {
		    int tlp = r.nextInt(4)+1;
			
			JSONArray jTLPArray = new JSONArray();
			for (int i = 0; i < tlp; i++) {
				JSONObject jTLP = new JSONObject();
				String tlpStr = "2"+i+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10);
				jTLP.put("CONTAINER_ID", tlpStr);
				jTLP.put("CONTAINER", "TLP"+tlpStr);
				
				int t_clp_types = r.nextInt(10)+1;
//				
				JSONArray jT_CLP_TypesArray = new JSONArray();
				for (int k = 0; k < t_clp_types; k++) {
					JSONObject jCLP_Type = new JSONObject();
					jCLP_Type.put("CONTAINER_TYPE_ID","1254");
					jCLP_Type.put("DIMENSIONS",r.nextInt(10)+"x"+r.nextInt(10)+"x"+r.nextInt(10));
					jCLP_Type.put("LOT_NUMBER", r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					jCLP_Type.put("SKU", r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					int clp = r.nextInt(10)+1;
					jCLP_Type.put("TOTAL_CONTAINERS", clp);
					JSONArray jCLPArray = new JSONArray();
					for (int j = 0; j < clp; j++) {
						JSONObject jCLP = new JSONObject();
						jCLP.put("CONTAINER_TYPE",Cycle_Count_Task_Key.CCT_CLP);
						String clpStr = r.nextInt(10)+""+j+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10);
						jCLP.put("CONTAINER_ID",clpStr);
						jCLP.put("CONTAINER", "CLP"+clpStr);
						jCLPArray.put(jCLP);
					}
					jCLP_Type.put("CONTAINERS", jCLPArray);
					jT_CLP_TypesArray.put(jCLP_Type);
				}
				jTLP.put("CONTAINER_TYPES", jT_CLP_TypesArray);
				
				int product = r.nextInt(10)+1;
				JSONArray jProductArray = new JSONArray();
				for (int k = 0; k < product; k++) {
					JSONObject jProduct_Type = new JSONObject();
//					jProduct_Type.put("PNAME",k+"00100286/TXCHB2"+k);
					jProduct_Type.put("SKU",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					jProduct_Type.put("LOT_NUMBER",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					jProduct_Type.put("PC_ID",r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					int products = r.nextInt(10)+5;
					jProduct_Type.put("QUANTITY", products);
					
					List<String> sn = new ArrayList<String>();
					sn.add(r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					sn.add(r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10)+""+r.nextInt(10));
					JSONArray mArray = new JSONArray(sn);
					jProduct_Type.put("SN",mArray);
					
					jProductArray.put(jProduct_Type);
				}
				jTLP.put("PRODUCTS", jProductArray);
				
				jTLPArray.put(jTLP);
			}
			json.put("TLP_CONTAINERS", jTLPArray);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	
	/**
	 * 获得所有Product类型的数据集合, 包括SN的Product和SKU类型的Product
	 * @return
	 */
	public List<CCT_ProductBean> getAllPdts() {
		List<CCT_ProductBean> allPdtList = new ArrayList<CCT_ProductBean>();
		// 添加no sn 的pdtList
		allPdtList.addAll(pdtNoSnList);
		
		// 添加有sn的pdtList
		for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
			for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
				allPdtList.add(pdtBean);
			}
		}
		return allPdtList;
	}
	
	/**
	 * 获得SKU类型的Product所有Damaged的集合
	 * @return
	 */
	public List<CCT_ProductBean> getDamagedSKUPdtList() {
		List<CCT_ProductBean> list = new ArrayList<CCT_ProductBean>();
		for(CCT_ProductBean pdt : pdtNoSnList) {
			if(pdt.isDamaged) {
				list.add(pdt);
			}
		}
		return list;
	}
	
}
