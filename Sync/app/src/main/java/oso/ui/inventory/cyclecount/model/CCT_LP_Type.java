package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_LP_Type implements Serializable{

    private static final long serialVersionUID = -100903731061328576L;

    public int sort;// 1,
    public double total_weight;// 1610.0,
    public double ibt_weight;// 10.0,
    public String pc_id;// 1002445,
    public int stack_width_qty;// 2,
    public String title_ids;// "2,2,2,5,8",
    public double ibt_length;// 72,
    public int is_has_sn;// 2,//SN. 1.包含SN .2.不包含SN
    public int stack_length_qty;// 1,
    public int inner_total_lp;// 16,
    public int inner_pc_or_lp;// 0,
    public int level;// 1,
    public int stack_height_qty;// 8,
    public int ibt_width;// 45,
    public int weight_uom;// 2,
    public String customer_ids;// "1405,0,1405,1405,1405",
    public String type_name;// "72x45",
    public int basic_type_id;// 1,
    public String ship_to_ids;// "1,0,0,1,1",
    public int active;// 1,
    public int ibt_height;// 2,
    public int length_uom;// 4,
    public String lp_name;// "CLP1*2*8[72x45]",
    public int inner_total_pc;// 16,
    public int container_type;// 1,
    public int lpt_id;// 15
}
