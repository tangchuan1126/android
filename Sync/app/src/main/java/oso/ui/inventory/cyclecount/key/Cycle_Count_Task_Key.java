package oso.ui.inventory.cyclecount.key;

import android.R;
import android.content.res.Resources;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: Cycle_Count_Task_Key 
 * @Description: 该key类主要应用的盘点的任务的类型判断及显示
 * @author gcy
 * @date 2015-4-3 下午4:13:54
 */
public class Cycle_Count_Task_Key {

	public static final String TITLE_CYCLE_COUNT_TASK = "Cycle Count Tasks";
	public static final String TITLE_VERIFICATION_TASK = "Verification Tasks";
	public static final String TITLE_FIND_TASK = "Find Tasks";

	//---------CCT MyTask 三种类型的顺序
	public static final int INDEX_CYCLE_COUNT_TASK = 0;
	public static final int INDEX_VERIFICATION_TASK = 1;
	public static final int INDEX_FIND_TASK = 2;

	//---------CCT中网络请求后 常用的回执状态
	public static final int STATUS_SUCCESS = 1;
	public static final int STATUS_ERR = 2;
	public static final int STATUS_CREATE_INFO = 4;//学习型盘中需要应用的状态key 应用场景 一般为需要创建lp或者商品的时候

	//---------任务、区域、位置的status 一般都是1.new  2.in progress  3.scanned
	
	public static final int BLIND_TASK = 1;//盲扫
	public static final int VERIFY_TASK = 2;//审核盘点
	public static final int VERIFICATION_TASK = 3;//差异盘点 主要针对审核盘点中差异较大的任务 创建的只针对差异的盘点任务
	public static final int FIND_TASK = 4;//寻找任务
	
	
	//-----status 盘点任务只有两种状态 处理中和未处理     因为 已处理的任务不需要显示 表示经完成
	public static final int NEW = 1;//未处理
	public static final int IN_PROGRESS = 2;//处理中
	
	//-----priority status 盘点任务的优先级
	public static final int MEDIUM = 2;//中
	public static final int LOW = 1;//底
	public static final int HIGH = 3;//高
		
	
	//-----Area status 区域有三种状态 已处理 未处理 处理中
	public static final int Area_NEW = 1;//未处理
	public static final int Area_IN_PROGRESS = 2;//处理中
	public static final int Area_SCANNED = 3;//已处理
	
	
	//-----Location status 位置有两种状态 只有 未处理   和  已处理 因为盘点任务是不能被中断的
	public static final int Location_NEW = 1;//未处理   
	//-----大于>=3的为已经处理
	public static final int Location_SCANNED = 3;//已处理
	
	
	//-----Container的 类型
	public static final int CCT_CLP = 1;
	public static final int CCT_TLP = 3;
	public static final int CCT_PRODUCT = 5;	
	
	
	//-----是否有SN 
	public static final int CCT_P_HAS_SN = 1;
	public static final int CCT_P_NO_SN = 2;

	//-----loc 的type  分为正常的位置 和 staging的位置
	public static final int CCT_AL_NORMAL = 1;
	public static final int CCT_AL_STAGING = 2;

	final static Map<String, String> taskType = new HashMap<String, String>();//任务种类
	final static Map<String, String> taskStatus = new HashMap<String, String>();//任务状态
	final static Map<String, String> priorityType = new HashMap<String, String>();//任务处理级别
	final static Map<String, Integer> priorityColor = new HashMap<String, Integer>();//任务处理级别

	final static Map<String, String> areaStatus = new HashMap<String, String>();//区域状态
	
	final static Map<String, String> containerType = new HashMap<String, String>();//container的类型
	
	static{

		taskType.put(String.valueOf(BLIND_TASK), "Blind");	
		taskType.put(String.valueOf(VERIFY_TASK), "Verify");
 		taskType.put(String.valueOf(VERIFICATION_TASK), "Verification");
		taskType.put(String.valueOf(FIND_TASK), "Find");
 		

 		taskStatus.put(String.valueOf(NEW), "New");	
 		taskStatus.put(String.valueOf(IN_PROGRESS), "In-Progress");
		
 		priorityType.put(String.valueOf(HIGH), "High");
 		priorityType.put(String.valueOf(MEDIUM), "Medium");
 		priorityType.put(String.valueOf(LOW), "Low");
 		
 		priorityColor.put(String.valueOf(HIGH),  Resources.getSystem().getColor(R.color.holo_red_light));
 		priorityColor.put(String.valueOf(MEDIUM),  Resources.getSystem().getColor(R.color.holo_orange_dark));
 		priorityColor.put(String.valueOf(LOW),  Resources.getSystem().getColor(R.color.holo_green_dark)); 		
 		
 		areaStatus.put(String.valueOf(Area_NEW), "New");
 		areaStatus.put(String.valueOf(Area_IN_PROGRESS), "In-Progress");
// 		areaStatus.put(String.valueOf(Area_SCANNED), "Scanned");
 		
 		containerType.put(String.valueOf(CCT_CLP), "CLP");
 		containerType.put(String.valueOf(CCT_TLP), "TLP");
	}

	
	/**
	 * 通过类型获取任务类型
	 */
	public static String getTaskTypeValue(int type)
	{
		return getTaskTypeValue(type+"");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static String getTaskTypeValue(String type)
	{
		return String.valueOf(taskType.get(type));
	}

	/**
	 * 通过类物类型获取任务分类名称
	 * @param type
	 * @return
	 */
	public static String getTaskTypeName(int type) {
		switch (type) {
			case BLIND_TASK:
			case VERIFY_TASK:
				return TITLE_CYCLE_COUNT_TASK;
			case VERIFICATION_TASK:
				return TITLE_VERIFICATION_TASK;
			case FIND_TASK:
				return TITLE_FIND_TASK;
			default:
				return "";
		}
	}
	//------------------------------------------------------------------------
	
	/**
	 * 通过类型获取任务状态
	 */
	public static String getTaskStatusValue(int type)
	{
		return getTaskStatusValue(type+"");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static String getTaskStatusValue(String type)
	{
		return String.valueOf(taskStatus.get(type));
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * 通过类型获取任务类型
	 */
	public static String getPriorityValue(int type)
	{
		return getPriorityValue(type+"");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static String getPriorityValue(String type)
	{
		return String.valueOf(priorityType.get(type));
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getPriorityColor(int type)
	{
		return getPriorityColor(type+"");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getPriorityColor(String type)
	{
		return priorityColor.get(type);
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * 通过类型获取区域状态
	 */
	public static String getAreaStatusValue(int type)
	{
		return getAreaStatusValue(type+"");
	}
	
	/**
	 * 通过类型获取区域状态
	 */
	public static String getAreaStatusValue(String type)
	{
		return String.valueOf(areaStatus.get(type));
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * 获取container的类型
	 */
	public static String getContainerTypeValue(int type)
	{
		return getContainerTypeValue(type+"");
	}
	
	/**
	 * 获取container的类型
	 */
	public static String getContainerTypeValue(String type)
	{
		return String.valueOf(containerType.get(type));
	}
}
