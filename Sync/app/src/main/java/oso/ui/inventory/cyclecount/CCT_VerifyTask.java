package oso.ui.inventory.cyclecount;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.inventory.cyclecount.adapter.CCT_VerifyTaskAdp;
import oso.ui.inventory.cyclecount.iface.VerifyIface;
import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_Product;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Dialog;
import oso.ui.inventory.cyclecount.util.CCT_JudgeIcon;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: CCT_VerifyTask 
 * @Description: 对照盘点
 * @author gcy
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_VerifyTask extends BaseActivity implements VerifyIface{
	
	private Cycle_Count_Tasks_Bean tBean;//任务信息
	private ScanLocationDataBean sBean;//该位置上系统存在的信息
	private List<CCT_ContainerBean> listBean;//重新解析系统存在的信息用于显示在当前页面中

	private SearchEditText search_view;//搜索框
	
	private ExpandableListView listview;
	private CCT_VerifyTaskAdp adps;
	
	private TabToPhoto ttp;
	
	public static int thisActivityFlag = 321;//标记
	
	//-----查询索引
	private Map<String, String> indexMap;

	private Button submit_btn;

	private NetConnection_CCT conn;

	private String interimStr; //用于保存临时的code

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cct_verify_task, 0);
		getFromActivityData(getIntent());
	}
	
	private CCT_LocationBean mLocationBean;
	
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	private void getFromActivityData(Intent intent) {
		tBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("tBean");
		sBean = (ScanLocationDataBean) intent.getSerializableExtra("ScanLocationDataBean");
		mLocationBean = (CCT_LocationBean) intent.getSerializableExtra("LocationBean");
		if(sBean==null||tBean==null){
			finish();
		}
		//-------转换数据结构 改装成CTNR集合
		listBean = CCT_ContainerBean.getContainerList(sBean);
		setTitleString(sBean.slc_position);
		initView();
	}
	
	/**
	 * @Description:初始化View
	 */
	private void initView(){
		conn = new NetConnection_CCT();
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finishThisActivity();
			}
		});

		// 事件
		showRightButton(R.drawable.menu_btn_style, "", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CCT_Dialog.selectLP_Type(mActivity, "",sBean);//因为不是扫描所以默认传空
			}
		});

		search_view = (SearchEditText) findViewById(R.id.search_edit);
		search_view.setIconMode(SearchEditText.IconMode_Scan);
		search_view.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				if(StringUtil.isNullOfStr(value)){
					UIHelper.showToast("Enter or scan "+search_view.getHint());
					return;
				}
				
				doScanLP(value);
				search_view.setText("");
				search_view.requestFocus();
				Utility.colseInputMethod(mActivity, search_view);
			}
		});
		
		// 全大写
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		search_view.setTransformationMethod(trans);
		
		listview = (ExpandableListView) findViewById(R.id.listview);
		
		//实例化adp
		adps = new CCT_VerifyTaskAdp(mActivity, listBean,this);
		listview.setAdapter(adps);
		
		//张开全部
		adps.expandAll(listview);
		
		//--------提交
		submit_btn = (Button) findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Utility.isNullForList(listBean)){
					RewriteBuilderDialog.newSimpleDialog(mActivity, "Do you want to submit an empty location?", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							submitData();
						}
					}).show();
				}else{
					submitData();
				}
			}
		});
		
		//------创建索引
		if(!Utility.isNullForMap(indexMap)){
			indexMap.clear();
		}
		indexMap = CCT_Util.getIndexMap(listBean);
		showSubmitButton();
	}
	
	/**
	 * @Description:根据输入值扫描TLP 或者 CLP
	 */
	private void doScanLP(String value){
		//------如果为空则直接请求获取数据
		if(Utility.isNullForList(listBean)){
			getLP_Data(value,"");
			return;
		}
		
		//-------判断是否查询到 如果查询不到则请求网络 获取该Container的 详细信息 并作为盈亏处理
		boolean isFound = false;
		boolean isTLP = false;
		int selectGroupNum = 0;
		int selectChildNum = 0;
		
		for (int i = 0; i < listBean.size(); i++) {
			CCT_ContainerBean c = listBean.get(i);
			//-------循环判断tlp
			if(c.containerType == Cycle_Count_Task_Key.CCT_TLP){
				if((c.tlp.REAL_NUMBER).equals(value)||(c.tlp.CONTAINER).equals(value)){
					isFound = true;
					isTLP = true;
					selectGroupNum = i;
					break;
				}
			}
			//-------循环判断clp
			else{
				CCT_CLP_Type_Bean clp_type = c.clp_type;
				if(!Utility.isNullForList(clp_type.CONTAINERS)){
					for (int j = 0; j < clp_type.CONTAINERS.size(); j++) {
						CCT_CLP_Bean clp = clp_type.CONTAINERS.get(j);
						if(clp.REAL_NUMBER.equals(value)||(clp.CONTAINER).equals(value)){
							isFound = true;
							isTLP = false;
							selectGroupNum = i;
							selectChildNum = j;
							break;
						}
					}
				}
			}
		}
		
		if(!isFound){
			//----网络请求
			getLP_Data(value, "");
		}else{
			if(isTLP){
				scanTLP(selectGroupNum);
			}else{
				scanCLP(selectGroupNum, selectChildNum,false);
			}
		}
		
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 */
	private void scanTLP(final int selectGroupNum){
		CCT_TLP_Bean tlp = listBean.get(selectGroupNum).tlp;
		
//		if(listBean.get(selectGroupNum).statusLP == CCT_Container_StatusKey.MISSING){
//			UIHelper.showToast("TLP is missing");
//			return;
//		}
		
		TTS.getInstance().speakAll( "Scan TLP");
		UIHelper.showToast("Scan " + tlp.REAL_NUMBER);
		
		boolean isVerify = true;
		
		Intent intent = new Intent();
		intent.putExtra("selectGroupNum", selectGroupNum);
		intent.putExtra("listBean", (Serializable)listBean);	
		intent.putExtra("isVerify", isVerify);	
		intent.putExtra("ScanLocationDataBean", (Serializable)sBean);
		intent.putExtra("LocationBean", (Serializable)mLocationBean);
		intent.putExtra("TaskBean", (Serializable)tBean);
		intent.setClass(mActivity, CCT_Verify_Scan_TLP.class);
		startActivityForResult(intent, thisActivityFlag);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 */
	private void LP_Dialog(final int selectGroupNum,int containerType){
		final RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		
		final CCT_ContainerBean c = listBean.get(selectGroupNum);
		
		String prompt[] = null;
		final boolean isTLP = c.containerType == Cycle_Count_Task_Key.CCT_TLP;
		if(c.containerType == Cycle_Count_Task_Key.CCT_TLP){
			b.setMessage("TLP: "+c.tlp.REAL_NUMBER+"?");
			Change_TLP_Type_Status(b,c);
//			if(c.statusLP == CCT_Container_StatusKey.MISSING){
//				prompt = new String[] { "	Cancel" };
//			}else{
//				prompt = new String[] { "	Missing" };
//			}
//			b.setNegativeButton(getString(R.string.sync_cancel), null);
//			b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
//				@Override
//				public void onItemClick(AdapterView<?> parent, View view,
//										int position, long id) {
//
//
////				if (isTLP) {
//					switch (position) {
//						case 0://该容器类别下更改为已扫描状态
//							if (c.statusLP == CCT_Container_StatusKey.MISSING) {
//								c.statusLP = CCT_Container_StatusKey.UNSCANNED;
//							} else {
//								c.statusLP = CCT_Container_StatusKey.MISSING;
//								if (c.tlp.isNewData) {
//									listBean.remove(selectGroupNum);
//								}
//							}
//							break;
//						default:
//							break;
//					}
//					//----校验是否 显示提交按钮
//					showSubmitButton();
//					adps.notifyDataSetChanged();
//					//张开全部
//					adps.expandAll(listview);
//
//					b.dismiss();
//				}
//			});
		}else{
			
			String massage = "LP Type: "+c.clp_type.LP_TYPE_NAME;
			if(!Utility.isNullForList(c.clp_type.CONTAINERS)){
				massage+="\nAre all "+c.clp_type.CONTAINERS.size()+" containers there?";
			}	
			
			b.setMessage(massage);
			Change_CLP_Type_Status(b,c);
		}

//				} else {
//					int status = c.statusLP;
//					switch (position) {
//						case 0://该容器类别下更改为已扫描状态
//							status = CCT_Container_StatusKey.SCANNED;
//							break;
//						case 1://该容器类别下更改为丢失状态
//							status = CCT_Container_StatusKey.MISSING;
//							break;
//						default:
//							break;
//					}
//
//					b.dismiss();
//
//					boolean isFound = false;
//					if (status == CCT_Container_StatusKey.SCANNED) {
//						for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
//							if (!StringUtil.isNullOfStr(indexMap.get("LP" + c.clp_type.CONTAINERS.get(i).CON_ID))) {
//								isFound = true;
//								break;
//							}
//						}
//					}
//					//------如果发现有存在其他容器内的clp 则提示不继续执行下面的方法
//					if (isFound) {
//						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "There CLP already on the other TLP, please separate scanning CLP.");
//						b.dismiss();
//						return;
//					}
//
//					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
//						if (c.statusLP == CCT_Container_StatusKey.MISSING && c.clp_type.CONTAINERS.get(i).isNewData) {
//							RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "Have non-system data, please separate scanning CLP.");
//							return;
//						}
//						c.clp_type.CONTAINERS.get(i).statusLP = status;
//					}
//
//					c.statusLP = status;
//				}


//				//----校验是否 显示提交按钮
//				showSubmitButton();
//				adps.notifyDataSetChanged();
//				//张开全部
//				adps.expandAll(listview);
//			}
//		});
//		b.show();
	}

	/**
	 * 更改CLP的状态
	 */
	private void Change_TLP_Type_Status(RewriteBuilderDialog.Builder b,final CCT_ContainerBean c){
		String str[] = null;
		if(c.statusLP == CCT_Container_StatusKey.MISSING){
			str = new String[] { CCT_Util.Cancel_Str };
		}else{
			str = new String[] { CCT_Util.Missing_Str  };
		}
		final String prompt[] = str;
		b.setNegativeButton(getString(R.string.sync_cancel), null);
		b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {


//				if (isTLP) {
				switch (position) {
					case 0://该容器类别下更改为已扫描状态
						if (c.statusLP == CCT_Container_StatusKey.MISSING) {
							c.statusLP = CCT_Container_StatusKey.UNSCANNED;
						} else {
							c.statusLP = CCT_Container_StatusKey.MISSING;
							if (c.tlp.isNewData) {
								listBean.remove(c);
							}
						}
						break;
					default:
						break;
				}
				//----校验是否 显示提交按钮
				showSubmitButton();
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
			}
		});
		b.show();
	}

	/**
	 * 更改CLP的状态
	 */
	private void Change_CLP_Type_Status(RewriteBuilderDialog.Builder b,final CCT_ContainerBean c){
		String str[] = null;
		if(c.statusLP == CCT_Container_StatusKey.UNSCANNED||c.statusLP == CCT_Container_StatusKey.PROBLEM){
			str = new String[] { CCT_Util.Scanned_Str , CCT_Util.Missing_Str};
		}else if(c.statusLP == CCT_Container_StatusKey.SCANNED){
			str = new String[] { CCT_Util.Missing_Str };
		}else{
			str = new String[] { CCT_Util.Scanned_Str };
		}
		final String prompt[] = str;
		b.setArrowItems(prompt, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int status = CCT_Util.getScanStatus(prompt[position]);

				boolean isFound = false;
				if(status==CCT_Container_StatusKey.SCANNED){
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						if(!StringUtil.isNullOfStr(indexMap.get("LP"+c.clp_type.CONTAINERS.get(i).CON_ID))){
							isFound = true;
							break;
						}
					}

					//------如果发现有存在其他容器内的clp 则提示不继续执行下面的方法
					if(isFound){
						RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, "At least 1 CLP under this LP type was already scanned on same location, please scan each LP individually.");
						return;
					}
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						c.clp_type.CONTAINERS.get(i).statusLP = status;
					}
					c.statusLP = status;
				}else if(status==CCT_Container_StatusKey.MISSING){
					if (c.clp_type.isAllNew()) {
//						c.clp_type.CONTAINERS.clear();
						listBean.remove(c);
					}else{
						List<CCT_CLP_Bean> remove_cons = new ArrayList<CCT_CLP_Bean>();
						for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
							if(c.clp_type.CONTAINERS.get(i).isNewData){
								remove_cons.add(c.clp_type.CONTAINERS.get(i));
							}
						}
						if(!Utility.isNullForList(remove_cons)){
							for (int i = 0; i < remove_cons.size(); i++) {
								c.clp_type.CONTAINERS.remove(remove_cons.get(i));
							}
						}
						for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
							c.clp_type.CONTAINERS.get(i).statusLP = status;
						}
						c.statusLP = status;
					}
				}else{
					for (int i = 0; i < c.clp_type.CONTAINERS.size(); i++) {
						c.clp_type.CONTAINERS.get(i).statusLP = status;
					}
					c.statusLP = status;
				}

				//----校验是否 显示提交按钮
				showSubmitButton();
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);

			}
		});
		b.show();
	}



	/**
	 * @Description:弹出选择框让用户选择 是否发现该SKU下 所有的CLP
	 * @param @param selectGroupNum 父级索引
	 * @param @param selectChildNum 子级索引
	 * @param @param scannedStatus 扫描需要更改的状态
	 * @param @param isShowDialog 是否弹出dialog 来操作
	 */
	private void scanCLP(final int selectGroupNum,final int selectChildNum,boolean isShowDialog){
		final CCT_CLP_Type_Bean clp_type = listBean.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		
		//如果是正常扫描则不弹出dialog操作 直接更改状态刷新UI
		if(!isShowDialog){
			changeCLP_Status(selectGroupNum, selectChildNum,CCT_Container_StatusKey.SCANNED,true);
			return;
		}
		//------------弹出操作提示
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(this);
		b.setMessage("CLP: " + clp.REAL_NUMBER);
		b.setNegativeButton(getString(R.string.sync_cancel), null);
		if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
			b.setArrowItems(
					new String[] { "	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}else if(clp.statusLP==CCT_Container_StatusKey.MISSING){
			b.setArrowItems(
					new String[] { "	Scanned" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}else{
			b.setArrowItems(
					new String[] { "	Scanned","	Missing" },
					new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							int scanStatus = clp.statusLP;
							switch (position) {
							case 0://该容器类别下更改为已扫描状态
								scanStatus = CCT_Container_StatusKey.SCANNED;
								break;
							case 1://该容器类别下更改为丢失状态
								scanStatus = CCT_Container_StatusKey.MISSING;
								break;
							default:
								break;
							}
							changeCLP_Status(selectGroupNum, selectChildNum, scanStatus,false);
						}
					});
		}
		
		
		b.show();
	}
	
	/**
	 * @Description:改变clp的状态
	 * @param @param selectGroupNum
	 * @param @param selectChildNum
	 * @param @param clp_type
	 * @param @param isBeep 提示音
	 */
	private void changeCLP_Status(int selectGroupNum,int selectChildNum,int scanStatus,boolean isBeep){
		final CCT_CLP_Type_Bean clp_type = listBean.get(selectGroupNum).clp_type;
		final CCT_CLP_Bean clp = clp_type.CONTAINERS.get(selectChildNum);
		//-------如果是丢失状态则不索引
		if(scanStatus == CCT_Container_StatusKey.MISSING){
			clp.statusLP = scanStatus;
			if(clp.isNewData){
				clp_type.CONTAINERS.remove(selectChildNum);
				if(Utility.isNullForList(clp_type.CONTAINERS)){
					listBean.remove(selectGroupNum);
				}
			}else{
				CCT_JudgeIcon.judgeIcon(listBean.get(selectGroupNum));
			}
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
//			if(selectChildNum<5){
//				listview.setSelectedGroup(selectGroupNum);
//			}else{
//				listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
//			}
		}else{
			indexCLP(clp, listBean.get(selectGroupNum),false,isBeep);
		}
	}
	
	/**
	 * @Description:通过网络请求获取LP基础数据信息
	 */
	private void getLP_Data(final String value, final String title){

		interimStr = ""; //清空

		conn.CCT_GetTLP_Or_CLP_ByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				JSONObject objJSON = json.optJSONObject("DATA");
				if (!objJSON.has("CONTAINER_TYPE")) {
					TTS.getInstance().speakAll_withToast(getString(R.string.cct_prompt_contain));
					return;
				}
				if (objJSON.optInt("CONTAINER_TYPE") == Cycle_Count_Task_Key.CCT_TLP) {
					jsonToTLP(json);
				}
				if (objJSON.optInt("CONTAINER_TYPE") == Cycle_Count_Task_Key.CCT_CLP) {
					final CCT_CLP_Bean clp = new Gson().fromJson(objJSON.toString(), new TypeToken<CCT_CLP_Bean>() {
					}.getType());
					clp.REAL_NUMBER = CCT_Util.fixLP_Number(clp.CONTAINER);
					clp.isNewData = true;
					//--新建container类型的基础bean 显示到当前的界面中
					final CCT_ContainerBean c = new CCT_ContainerBean();
					c.containerType = Cycle_Count_Task_Key.CCT_CLP;
					c.clp_type = new CCT_CLP_Type_Bean();
					c.clp_type.LP_TYPE_ID = objJSON.optString("TYPE_ID");
					c.clp_type.LP_TYPE_NAME = objJSON.optString("LP_TYPE_NAME");
					c.clp_type.P_NAME = objJSON.optString("P_NAME");
					c.clp_type.CONTAINERS.add(clp);

					//----添加
					boolean isAdd = true;
					//----提示音
					boolean isBeep = false;
					//----检索clp
					indexCLP(clp, c, isAdd, isBeep);
				}
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if(json==null) return;
				int msgErr = json.optInt("status", 0);
				if (msgErr == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					interimStr = value;
					CCT_Dialog.selectLP_Type(mActivity,value,sBean,null,true,true);
				} else {
					JSONArray jArray = json.optJSONArray("errors");
					if (!StringUtil.isNullForJSONArray(jArray)) {
						JSONObject jObj = jArray.optJSONObject(0);
						//特定错误 如果为37则提示选择title 也就是重新弹出界面选择title
						int SELECT_TITLE = 39;
						if (jObj.optInt("errorCode", 0) == SELECT_TITLE) {
							CCT_Dialog.selTLP_TitleDialog(mActivity, new CCT_Dialog.CCT_TLP_DialogInterface() {
								@Override
								public void doSomething(String key) {
									getLP_Data(value,key);
								}
							});
						} else {
							conn.handFail(json);
						}
					}
				}
			}
		}, value, title);
	}

	/**
	 * @Description:添加TLP 到当前列表
	 */
	private void jsonToTLP(JSONObject json){
		CCT_TLP_Bean tlp = new Gson().fromJson(json.optJSONObject("DATA").toString(), new TypeToken<CCT_TLP_Bean>() {}.getType());
		addTLP(tlp);
	}

	private void addTLP(CCT_TLP_Bean tlp){
		tlp.isNewData = true;
		tlp.REAL_NUMBER = CCT_Util.fixLP_Number(tlp.CONTAINER);
		//--新建container类型的基础bean 显示到当前的界面中
		CCT_ContainerBean c = new CCT_ContainerBean();
		c.tlp = tlp;
		c.containerType = Cycle_Count_Task_Key.CCT_TLP;

		//------验证当前添加的数据是否在当前列表中 如果在 则不添加 如果不在则作为扫描处理
		for (int i = 0; i < listBean.size(); i++) {
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_TLP
					&&listBean.get(i).tlp!=null
					&&listBean.get(i).tlp.CON_ID==tlp.CON_ID){
				doScanLP(tlp.CONTAINER);
				return;
			}
		}

		listBean.add(0,c);
		TTS.getInstance().speakAll_withToast("Discover a new TLP!");
		adps.notifyDataSetChanged();
		//张开全部
		adps.expandAll(listview);
		listview.setSelection(0);
		doScanLP(tlp.CONTAINER);
	}

	/**
	 * @Description:检索CLP 是否存在外面的列表中
	 * @param @param jObj
	 */
	private void indexCLP(final CCT_CLP_Bean clp,final CCT_ContainerBean c,final boolean isAdd,final boolean isBeep){		
		//---------检索数据
		String indexValue = indexMap.get("LP"+clp.CON_ID);
		if(!StringUtil.isNullOfStr(indexValue)){
			boolean isFound = false;
			CCT_TLP_Bean tlp = null;
			for (int i = 0; i < listBean.size(); i++) {
				if(listBean.get(i).containerType == Cycle_Count_Task_Key.CCT_TLP&&(listBean.get(i).tlp.CON_ID+"").equals(indexValue)){
					isFound = true;
					tlp = listBean.get(i).tlp;
					break;
				}
			}
			if(isFound){
				final CCT_TLP_Bean oldTlp = tlp;				
				//---------clp_type索引
				int clp_type_Index = -1;
				//---------clp索引
				int clp_Index = -1;
				for (int i = 0; i < oldTlp.CONTAINER_TYPES.size(); i++) {
					if(!Utility.isNullForList(oldTlp.CONTAINER_TYPES.get(i).CONTAINERS)){
						for (int j = 0; j < oldTlp.CONTAINER_TYPES.get(i).CONTAINERS.size(); j++) {
							if(oldTlp.CONTAINER_TYPES.get(i).CONTAINERS.get(j).CON_ID==clp.CON_ID){
								clp_type_Index = i;
								clp_Index = j;
								break;
							}
						}
					}
				}
				//----如果索引均不为-1则表示存在该clp
				if(clp_type_Index!=-1&&clp_Index!=-1){
					//-----由于回调函数只能应用静态变量 所以需要中转一下该变量 声明一个clp的索引
					final int indexTypeNum = clp_type_Index;
					final int indexNum = clp_Index;
					//-------获取clp_type
					final CCT_CLP_Type_Bean c_type_old = oldTlp.CONTAINER_TYPES.get(indexTypeNum);
					//-------获取clp
					final CCT_CLP_Bean clp_old = c_type_old.CONTAINERS.get(indexNum);
					//------判断是否扫描过该clp 如果未扫描则改变状态
					if(clp_old.statusLP == CCT_Container_StatusKey.UNSCANNED){
						clp_old.statusLP = CCT_Container_StatusKey.MISSING;
						addCLP(clp, c,isAdd,isBeep);
					}
					//------如果已经扫描过该clp则弹出提示是否确认该clp的真实位置
					else{
						if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
							addCLP(clp, c,isAdd,isBeep);
							return;
						}
						
						RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "It's already scanned at: \n TLP:" + oldTlp.REAL_NUMBER + "\nAre you sure to re-scan?", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//--------如果是新建数据则直接删除
								if (clp_old.isNewData) {
									c_type_old.CONTAINERS.remove(indexNum);
									if (Utility.isNullForList(c_type_old.CONTAINERS)) {
										oldTlp.CONTAINER_TYPES.remove(indexTypeNum);
									}
								}
								//--------如果不是新建数据则更改为丢失状态
								else {

									clp_old.statusLP = CCT_Container_StatusKey.MISSING;
									clp_old.isDamaged = false;

									//-------给父级别增加丢失状态
									if (listBean.get(indexTypeNum).containerType == Cycle_Count_Task_Key.CCT_TLP) {
										listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeTLP_TypeIcon(listBean.get(indexTypeNum).tlp);
									} else {
										listBean.get(indexTypeNum).statusLP = CCT_JudgeIcon.judgeCLP_TypeIcon(listBean.get(indexTypeNum).clp_type);
									}
								}
								addCLP(clp, c, isAdd, isBeep);
							}
						}).show();
					}
				}
				//---------如果不存在则直接添加该clp
				else{
					addCLP(clp, c,isAdd,isBeep);
				}
			}
			return;
		}
		addCLP(clp, c,isAdd,isBeep);
	}
	/**
	 * @Description:添加CLP 到当前列表
	 * @param @param clp
	 * @param @param c
	 * @param @param isAdd 是否添加 就是是否执行该方法 true执行 false 不执行
	 */
	private void addCLP(CCT_CLP_Bean clp,CCT_ContainerBean c,boolean isAdd,boolean isBeep){
		if(!isAdd){
			if(isBeep){
				if(clp.statusLP==CCT_Container_StatusKey.SCANNED){
					TTS.getInstance().speakAll_withToast("You have already scanned this container.");
				}else{
					TTS.getInstance().speakAll("Scanned");
				}
			}
			
			clp.statusLP = CCT_Container_StatusKey.SCANNED;
			CCT_JudgeIcon.judgeIcon(c);
			//----校验是否 显示提交按钮
			showSubmitButton();
			adps.notifyDataSetChanged();
			//张开全部
			adps.expandAll(listview);
			
			int selectGroupNum = -1;
			int selectChildNum = -1;
			for (int i = 0; i < listBean.size(); i++) {
				if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP&&listBean.get(i).clp_type.LP_TYPE_ID.equals(c.clp_type.LP_TYPE_ID)){
					selectGroupNum = i;
					for (int j = 0; j < listBean.get(i).clp_type.CONTAINERS.size(); j++) {
						if(listBean.get(i).clp_type.CONTAINERS.get(j).CON_ID==clp.CON_ID){
							selectChildNum = j;
							break;
						}
					}
				}
			}
			if(isBeep&&selectGroupNum!=-1&&selectChildNum!=-1){
				if(selectChildNum<5){
					listview.setSelectedGroup(selectGroupNum);
				}else{
					listview.setSelectedChild(selectGroupNum, selectChildNum-1, true);
				}	
			}
			return;
		}
		

		clp.statusLP = CCT_Container_StatusKey.SCANNED;
		
		//----将数据添加进去 先判断clp_type如果相同 则添加到当前clp_type内 进行显示
		for (int i = 0; i < listBean.size(); i++) {
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP){
				//---如果相同则直接添加进行显示
				if(listBean.get(i).clp_type.LP_TYPE_ID.equals(c.clp_type.LP_TYPE_ID)){
					for (int j = 0; j < listBean.get(i).clp_type.CONTAINERS.size(); j++) {
						if(listBean.get(i).clp_type.CONTAINERS.get(j).CON_ID==clp.CON_ID){
							doScanLP(listBean.get(i).clp_type.CONTAINERS.get(j).CONTAINER);
							return;
						}
					}
					
					listBean.get(i).clp_type.CONTAINERS.add(0,clp);
					CCT_JudgeIcon.judgeIcon(listBean.get(i));
					TTS.getInstance().speakAll_withToast("Discover a new CLP!");
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
					listview.setSelection(i);
					return;
				}
			}
		}
		
		//----将Clp_type 添加进去进行显示数据添加进去 进行显示
		for (int i = 0; i < listBean.size(); i++) {
			if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_CLP){
				CCT_JudgeIcon.judgeIcon(c);
				listBean.add(i,c);
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				listview.setSelection(i);
				return;
			}
		}
		
		CCT_JudgeIcon.judgeIcon(c);
		listBean.add(c);
		adps.notifyDataSetChanged();
		//张开全部
		adps.expandAll(listview);
		listview.setSelection(listBean.size() - 1);
	}
	
	@Override
	public void dialogCondition(int... is) {
		boolean flag = (is!=null&&is.length>0&&!Utility.isNullForList(listBean));
		if(!flag){
			return;
		}
		//如果长度只有1 则扫描的是TLP 或者SKU
		if(is.length==1){
			LP_Dialog(is[0], listBean.get(is[0]).containerType);
		}
		//如果长度只有2 则扫描的是CLP
		else if(is.length==2){
			scanCLP(is[0], is[1], true);
		}
	}

	@Override
	public void scanTLP_Info(int groupPosition) {
		scanTLP(groupPosition);
	}
	
	@Override
	public void showTLP_Dlg_photos(String sku, final CCT_TLP_Bean tlp) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP_TLP(ttp, sku, tlp);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadPhotosTLP(tlp, ttp);
				dlg.dismiss();
			}
		});
	}

	private void initTTP_TLP(TabToPhoto ttp_dlg,final String sku, final CCT_TLP_Bean tlp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, tBean.ID + "_" + mLocationBean.LOCATION_ID + "_" + tlp.CON_ID + "");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(tlp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = tlp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadPhotosTLP(final CCT_TLP_Bean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		p.add("file_name", tBean.ID + "_" + mLocationBean.LOCATION_ID + "_" + b.CON_ID + "");
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
	/**
	 * 用于弹出拍照的相关控件
	 */
	@Override
	public void showCLP_Dlg_photos(String sku, final CCT_CLP_Bean clp) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, sku, clp);

		// dlg
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0)
					reqUploadPhotos(clp, ttp);
				dlg.dismiss();
			}
		});
	}
	
	
	
	
	private void initTTP(TabToPhoto ttp_dlg,final String sku, final CCT_CLP_Bean clp) {
		ttp = ttp_dlg;
		String key = TTPKey.getCCT_Key(sku, tBean.ID + "_" + mLocationBean.LOCATION_ID + "_" + clp.CON_ID + "");
		ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
		ttp_dlg.pvs[0].addWebPhotoToIv(clp.photos);
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

			@Override
			public void onDeleteComplete(int tabIndex, TTPImgBean b) {
				// TODO Auto-generated method stub
				List<TTPImgBean> imgs = clp.photos;
				if (imgs != null && imgs.contains(b)) {
					imgs.remove(b);
					adps.notifyDataSetChanged();
					//张开全部
					adps.expandAll(listview);
				}
			}
		});
	}
	
	private void reqUploadPhotos(final CCT_CLP_Bean b, final TabToPhoto ttp) {

		RequestParams p = new RequestParams();
		p.add("file_name", tBean.ID + "_" + mLocationBean.LOCATION_ID + "_" + b.CON_ID + "");
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				adps.notifyDataSetChanged();
				//张开全部
				adps.expandAll(listview);
				UIHelper.showToast(getString(R.string.sync_success));
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);

	}
	
	/**
	 * @Description:判断是否显示提交按钮  每次扫描都需要校验
	 */
	private void showSubmitButton(){
		boolean showBtn = CCT_Util.checkVerifyData(listBean);
		submit_btn.setVisibility(showBtn ? View.VISIBLE : View.GONE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp != null)
			ttp.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == thisActivityFlag){
 			if(resultCode == RESULT_OK){
 				listBean = (List<CCT_ContainerBean>)data.getSerializableExtra("dataList");
				long tlp_id = data.getLongExtra("tlp_id", 0);
				//实例化adp
				adps = new CCT_VerifyTaskAdp(mActivity, listBean,this);
				listview.setAdapter(adps);
 				
 				//张开全部
 				adps.expandAll(listview);
 				

 				showSubmitButton();
 				
 				if(!Utility.isNullForMap(indexMap)){
 					indexMap.clear();
 				}
 				indexMap = CCT_Util.getIndexMap(listBean);
 				
 				if(tlp_id!=0&&!Utility.isNullForList(listBean)){
 					for (int i = 0; i < listBean.size(); i++) {
						//----判断是否相同
						if(listBean.get(i).containerType==Cycle_Count_Task_Key.CCT_TLP&&tlp_id==listBean.get(i).tlp.CON_ID){
							listview.setSelection(i);
							return;
						}
					}
 				}
 				

 			}
 		}

		if(requestCode == CCT_CreateTLP.resultCode){
			if(data.getSerializableExtra("tlp")==null){
				return;
			}
			CCT_TLP_Bean tlp_bean  = (CCT_TLP_Bean) data.getSerializableExtra("tlp");
			addTLP(tlp_bean);
		}
		if(requestCode == CCT_CreateCLP.resultCode){
			if(data.getSerializableExtra("clp")==null){
				return;
			}
			CCT_CLP_Bean clp_bean  = (CCT_CLP_Bean) data.getSerializableExtra("clp");
			clp_bean.REAL_NUMBER = CCT_Util.fixLP_Number(clp_bean.CONTAINER);
			clp_bean.isNewData = true;
			//--新建container类型的基础bean 显示到当前的界面中
			final CCT_ContainerBean c = new CCT_ContainerBean();
			c.containerType = Cycle_Count_Task_Key.CCT_CLP;
			c.clp_type = new CCT_CLP_Type_Bean();
			c.clp_type.LP_TYPE_ID = clp_bean.TYPE_ID+"";
			c.clp_type.LP_TYPE_NAME = clp_bean.TYPE_NAME;
			c.clp_type.P_NAME = clp_bean.P_NAME;
			c.clp_type.CONTAINERS.add(clp_bean);

			//----添加
			boolean isAdd = true;
			//----提示音
			boolean isBeep = false;
			//----检索clp
			indexCLP(clp_bean, c, isAdd, isBeep);
		}

		if(requestCode == CCT_Dialog.requestCode_CreateCLP){
			if(data.getSerializableExtra("product")==null||data.getLongExtra("pc_id",0)==0){
				return;
			}
			ProductBean returnData = (ProductBean)data.getSerializableExtra("product");
			String pc_id = data.getStringExtra("pc_id");


			CCT_Product product = new CCT_Product();
			product.p_name = returnData.productName;
			product.main_code = returnData.mainCode;
			product.pc_id = data.getLongExtra("pc_id", 0);

			CCT_CreateCLP.toThis(mActivity, product, interimStr, sBean,null);

		}

	}
	
	@Override
	public void onBackPressed() {
		
		finishThisActivity();
	}
	
	/**
	 * 如果没有submit location就进行了后退操作，需要请求删除所有已经上传过的图片
	 * @param ids
	 */
	private void reqDelPicsByFileIds(String ids) {
		RequestParams p = new RequestParams();
		p.put("file_ids", ids);
		
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
			}
		}.doPost(HttpUrlPath.CCT_delUploadPhotos, p, mActivity);
	}
	
	/**
	 * @Description:关闭当前页面
	 */
	private void finishThisActivity(){
		RewriteBuilderDialog.newSimpleDialogImperatives(mActivity, "You will lose scanning data on current location.", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				// 后退需要判断是否有已经上传的图片，有则进行删除
				String ids = CCT_Util.getDelUploadPicsIds(listBean);
				if(ids.length()>0) {
					reqDelPicsByFileIds(ids);
				} 
				
//				else {
					mActivity.finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
//				}
			}
		}).show();	
	}

	private void submitData(){
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				UIHelper.showToast(getString(R.string.sync_success));
				finish();
			}
		}.doPostForYasir(HttpUrlPath.SubmitLocationScan, CCT_Util.getSubmitData(tBean, sBean, listBean), mActivity);
	}

	@Override
	public void changeScanned(int groupPosition, int childPosition) {
		changeCLP_Status(groupPosition, childPosition, CCT_Container_StatusKey.SCANNED, false);
	}


	
}
