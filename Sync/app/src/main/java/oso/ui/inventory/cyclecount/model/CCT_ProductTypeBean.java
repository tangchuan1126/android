package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 盲盘---- ProductType类型
 * @author xialimin
 *
 */
public class CCT_ProductTypeBean implements Serializable {
	private static final long serialVersionUID = -7947297348541584629L;
	public long TYPE_ID;  // Type_ID 就是PC_ID
	public String P_Name;
	public List<CCT_ProductBean> pdtHasSNList = new ArrayList<CCT_ProductBean>();
	
	/**
	 * 获得SN类型的Damaged的Product
	 * @return
	 */
	public List<CCT_ProductBean> getDamagedSNPdtList() {
		List<CCT_ProductBean> list = new ArrayList<CCT_ProductBean>();
		for(CCT_ProductBean pdtBean : pdtHasSNList) {
			if(pdtBean.isDamaged) {
				list.add(pdtBean);
			}
		}
		return list;
	}
	
}
