package oso.ui.inventory.cyclecount.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.widget.LabelView;
import utility.StringUtil;
import utility.Utility;
/**
 * @ClassName: TaskInfoLayout 
 * @Description: 用于显示任务详情信息
 * @author gcy
 * @date 2015-4-10 下午3:35:33
 */
public class TaskInfoForListViewHead{
	
	/**
	 * @Description:用于获取任务的布局文件 并填充数据加以显示
	 * @param @param context  
	 * @param @param tBean  数据bean
	 * @param @return 返回填充数据后的View
	 */
	public static View getLayout(Context context,Cycle_Count_Tasks_Bean tBean){
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.lo_cct_task_info_layout, null);
		initData(context, view, tBean);
		return view;
	}
	
	/**
	 * @Description:初始化数据
	 */
	private static void initData(Context context,View view,Cycle_Count_Tasks_Bean tBean) {
		LabelView l = new LabelView(context);
		l.setText((tBean.PRIORITY==0)?"":Cycle_Count_Task_Key.getPriorityValue(tBean.PRIORITY));
		l.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9);
		l.setBackgroundColor(Cycle_Count_Task_Key.getPriorityColor(tBean.PRIORITY));
		l.setTargetView(((LinearLayout)view.findViewById(R.id.task_info_layout)), context.getResources().getDimensionPixelOffset(R.dimen.cct_head_light_prompt), LabelView.Gravity.RIGHT_TOP);
		
		// 获得对子控件的引用
		((TextView)view.findViewById(R.id.tx_warehouse)).setText(tBean.WAREHOUSE_NAME);
		((TextView)view.findViewById(R.id.tx_title)).setText(Cycle_Count_Tasks_Bean.getTitleStr(tBean.TITLES));
		((TextView)view.findViewById(R.id.tx_type)).setText((tBean.TYPE==0)?"":Cycle_Count_Task_Key.getTaskTypeValue(tBean.TYPE));
		((TextView)view.findViewById(R.id.tx_date)).setText(tBean.START_DATE+" — "+tBean.END_DATE);

		//Duration
		long duration = CCT_Util.getDuration(tBean.START_DATE, tBean.END_DATE);
		((TextView)view.findViewById(R.id.tx_duration)).setText(duration + ((duration == 1) ? " Day" : " Days"));

		//--------判断是否显示Title
		if(!Utility.isNullForList(tBean.TITLES)){
			((View)view.findViewById(R.id.title)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.tx_title)).setText(Cycle_Count_Tasks_Bean.getTitleStr(tBean.TITLES));
		}
		
		//--------判断是否显示Customer
		if(!TextUtils.isEmpty(tBean.CUSTOMER_NAME)){
			((View)view.findViewById(R.id.customer)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.tx_customer)).setText(tBean.CUSTOMER_NAME);
		}

		String model_numberStr = Cycle_Count_Tasks_Bean.getListString(tBean.MODEL_NUMBERS);
		if(!StringUtil.isNullOfStr(model_numberStr)){
			((View)view.findViewById(R.id.l_model_numbers)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.model_numbers)).setText(model_numberStr);
		}
		
		//--------判断是否显示型号
		String lot_numbersStr = Cycle_Count_Tasks_Bean.getListString(tBean.LOT_NUMBERS);
		if(!StringUtil.isNullOfStr(model_numberStr)){
			((View)view.findViewById(R.id.l_lot_numbers)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.lot_numbers)).setText(lot_numbersStr);
		}
		//--------判断是否显示生产线
		if(!StringUtil.isNullOfStr(tBean.PRODUCT_LINE_NAME)){
			((View)view.findViewById(R.id.l_product_line)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.product_line)).setText(tBean.PRODUCT_LINE_NAME);
		}
		//--------判断是否显示产品分类
		if(!StringUtil.isNullOfStr(tBean.PRODUCT_CATEGORY_NAME)){
			((View)view.findViewById(R.id.l_product_category)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.product_category)).setText(tBean.PRODUCT_CATEGORY_NAME);
		}
		//--------判断是否显示CLP类型  (TLP 目前是没有类型的)
		if(!StringUtil.isNullOfStr(tBean.LP_TYPE_NAME)){
			((View)view.findViewById(R.id.l_lp_type_name)).setVisibility(View.VISIBLE);
			((TextView)view.findViewById(R.id.lp_type_name)).setText(tBean.LP_TYPE_NAME);
		}	
	}
	
}
