package oso.ui.inventory.cyclecount.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import support.common.datas.HoldDoubleValue;

public class WeightUOMKey {

	public static final int KG = 1;
	public static final int LBS = 2;


	public final static Map<String, String> weightUOMType = new HashMap<String, String>();//任务种类
	public final static List<HoldDoubleValue<String,String>> weightUOMList = new ArrayList<HoldDoubleValue<String,String>>();//任务种类

	static {
		weightUOMType.put(String.valueOf(WeightUOMKey.LBS), "LBS");
		weightUOMType.put(String.valueOf(WeightUOMKey.KG), "Kg");

		weightUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(WeightUOMKey.LBS), "LBS"));
		weightUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(WeightUOMKey.KG), "Kg"));
	}

	public String getWeightUOMKey(int id) {
		return (weightUOMType.get(String.valueOf(id)));
	}

}
