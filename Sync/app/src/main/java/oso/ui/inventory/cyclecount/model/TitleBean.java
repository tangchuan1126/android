package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

public class TitleBean implements Serializable {
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -8257009910873626157L;
	
	public int TITLE_ID;//": 3,  如果tilte 为0 则 显示All
	public String TITLE_NAME;//": "SONY"
	
	
	public TitleBean() {
		super();
	}
	public TitleBean(int tITLE_ID, String tITLE_NAME) {
		super();
		TITLE_ID = tITLE_ID;
		TITLE_NAME = tITLE_NAME;
	}
	
	
}
