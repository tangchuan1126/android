package oso.ui.inventory.cyclecount.adapter;

import java.util.Locale;

import oso.ui.inventory.cyclecount.BlindTLPDetailActivity;
import oso.ui.inventory.cyclecount.iface.IBlind;
import oso.ui.inventory.cyclecount.model.BlindScanTLPDetailBean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductTypeBean;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * 
 * @author xialimin
 * @date 2015-4-14 上午8:54:56
 * 
 * @description 盲盘TLP中的CLP和有无SN的Products的集合
 */
public class BlindScanTLPDetailAdp extends BaseExpandableListAdapter {
	
	private static final int CLP = 0;
	private static final int PDT_HAS_SN = 1;
	private static final int PDT_NO_SN = 2;
	
	private Context mContext;
	private BlindScanTLPDetailBean mData;
	private IBlind iBlind;
	public BlindScanTLPDetailAdp(Context _ctx, BlindScanTLPDetailBean _dataBean, IBlind _iBlind) {
		iBlind = _iBlind;
		mContext = _ctx;
		mData = _dataBean;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initGroupView(holder, groupPosition, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		convertView.setTag(R.layout.cct_blindscan_item_group_clp_layout, groupPosition);  
		convertView.setTag(R.layout.cct_blindscan_item_group_pdt_layout, -1);
		convertView.setTag(R.layout.cct_blindscan_item_product_sku_layout, -1);
		
		fillGroupData(isExpanded, holder, groupPosition, convertView);
		//===============================================
		
		setGroupListener(holder, groupPosition);
		
		
		return convertView;
	}
	

	/**
	 * 初始化Group item
	 * @param groupPosition
	 * @param convertView
	 * @return
	 */
	private View initGroupView(ViewHolder holder, int groupPosition, View convertView) {
		switch (getGroupType(groupPosition)) {
		
		case CLP:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_group_clp_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blind_clp_grp_item);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindCLPType_grp);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindCLPTypeId);
			
			holder.ivArrow = (ImageView) convertView.findViewById(R.id.iv_clptype_arrow);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_d_clp_icon);
			break;
		
		case PDT_HAS_SN:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_group_pdt_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blind_pdt_grp_item);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindPdtType_grp);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindPdtTypeId);
			
			holder.ivArrow = (ImageView) convertView.findViewById(R.id.iv_blind_sku_arrow);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_d_p_icon);
			break;
		
		case PDT_NO_SN:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_product_sku_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blindscan_detail_sku);
			holder.tvModelNo = (TextView) convertView.findViewById(R.id.tvBlindSKU);
			holder.tvTotal = (TextView) convertView.findViewById(R.id.tvBlindTotal);
			holder.ivDamaged = (ImageView) convertView.findViewById(R.id.ivSku);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgSkuCnt);
			holder.tvDamagedNumber = (TextView) convertView.findViewById(R.id.tvBlindDamagedNumber);
			holder.flTakePhoto =  (FrameLayout) convertView.findViewById(R.id.image_btn_takephoto_pdt_sku);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_d_psku_icon);
			break;
		}
		
		return convertView;
	}

	/**
	 * 填充Group数据
	 * @param holder
	 * @param groupPosition
	 * @param convertView
	 */
	private void fillGroupData(boolean isExpanded, ViewHolder holder, int groupPosition, View convertView) {
		switch (getGroupType(groupPosition)) {
		case CLP:
			CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) mData.getBeanByGrpPosition(groupPosition);
			holder.tvNumber.setText(clpTypeBean.LP_TYPE_NAME+"");
			
			boolean hasChild = clpTypeBean.CONTAINERS.size() > 0;
			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || !hasChild;
			holder.item.setBackgroundResource(showOne ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
			
			// 更改箭头方向
			holder.ivArrow.setBackgroundResource(showOne ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
			break;
		
		case PDT_HAS_SN:
			CCT_ProductTypeBean pdtTypeBean = (CCT_ProductTypeBean) mData.getBeanByGrpPosition(groupPosition);
			holder.tvType.setText("Model NO.: "); 
			holder.tvNumber.setText(pdtTypeBean.P_Name+""); // [P_name ]
			
			boolean hasChild1 = pdtTypeBean.pdtHasSNList.size() > 0;
			// 关闭|无条目时,显示圆背景
			boolean showOne1 = !isExpanded || !hasChild1;
			holder.item.setBackgroundResource(showOne1 ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
			
			// 更改箭头方向
			holder.ivArrow.setBackgroundResource(showOne1 ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
			break;
		
		case PDT_NO_SN:
			CCT_ProductBean pdtNoSnBean = (CCT_ProductBean) mData.getBeanByGrpPosition(groupPosition);
//			holder.item.setSelected(pdtNoSnBean.itemIsSeleted);
			boolean hasModelNo = !TextUtils.isEmpty(pdtNoSnBean.modelNo);
//			holder.tvModelNo.setText(hasModelNo?pdtNoSnBean.modelNo.toUpperCase(Locale.ENGLISH)+"":pdtNoSnBean.P_NAME.toUpperCase(Locale.ENGLISH)+"");//TODO 修改过的
			holder.tvModelNo.setText(pdtNoSnBean.P_NAME.toUpperCase(Locale.ENGLISH)+"");
			/*String sku = (hasModelNo?(pdtNoSnBean.modelNo.toUpperCase(Locale.ENGLISH) + (pdtNoSnBean.lotNo==null?"": " / "+pdtNoSnBean.lotNo.toUpperCase(Locale.ENGLISH)))
					: pdtNoSnBean.P_NAME.toUpperCase(Locale.ENGLISH)+"");
			holder.tvModelNo.setText(sku);*/
			holder.tvTotal.setText(pdtNoSnBean.TOTAL+"");
			holder.tvDamagedNumber.setText(pdtNoSnBean.DAMAGEDCNT+"");
			holder.tvImgCnt.setText(pdtNoSnBean.blindPhotos.size()+"");
			holder.tvImgCnt.setVisibility(pdtNoSnBean.blindPhotos.size()==0?View.INVISIBLE:View.VISIBLE);
			holder.ivDamaged.setSelected(pdtNoSnBean.blindPhotos.size()!=0);
			holder.flTakePhoto.setVisibility(pdtNoSnBean.DAMAGEDCNT==0?View.INVISIBLE:View.VISIBLE);
			break;
		}
	}
	
	/**
	 * 设置组的监听器
	 * @param holder
	 * @param groupPosition
	 */
	private void setGroupListener(ViewHolder holder, final int groupPosition) {
		switch (getGroupType(groupPosition)) {
		case BlindScanTLPDetailBean.Type_PDT_NO_SN: 
			final CCT_ProductBean pdtBeanSku = (CCT_ProductBean) mData.getBeanByGrpPosition(groupPosition);
			holder.flTakePhoto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				iBlind.showDlg_photos_pdt(BlindTLPDetailActivity.Type_PDT_SKU, pdtBeanSku);
			}
			});
			
			/**
			 * 长按删除
			 *//*
			holder.item.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					iBlind.showDlg_onLongPress(BlindScanTLPDetailBean.Type_PDT_NO_SN, groupPosition, -1);
					return true;
				}
			});*/
			
			holder.ivStatus.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iBlind.showDlg_onClickStatusIcon(BlindScanTLPDetailBean.Type_PDT_NO_SN, groupPosition, -1);
				}
			});
			
			holder.item.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iBlind.showEditDlg(groupPosition, BlindScanTLPDetailBean.Type_PDT_NO_SN, pdtBeanSku);
				}
			});
			break;

			
			
	//---------------------------CLP----------------------------------------------------
		case BlindScanTLPDetailBean.Type_CLP:
//			holder.item.setOnLongClickListener(new OnLongClickListener() {
//				@Override
//				public boolean onLongClick(View v) {
//					
//					iBlind.showDlg_onLongPress(BlindScanTLPDetailBean.Type_CLP, groupPosition, -1);
//					return true;
//				}
//			});
			
			holder.ivStatus.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iBlind.showDlg_onClickStatusIcon(BlindScanTLPDetailBean.Type_CLP, groupPosition, -1);
				}
			});
			break;
			
			
			
	//---------------------------PDT HAS SN----------------------------------------------
		case BlindScanTLPDetailBean.Type_PDT_HAS_SN:
//			holder.item.setOnLongClickListener(new OnLongClickListener() {
//				
//				@Override
//				public boolean onLongClick(View v) {
//					
//					iBlind.showDlg_onLongPress(BlindScanTLPDetailBean.Type_PDT_HAS_SN, groupPosition, -1);
//					return true;
//				}
//			});
			
			holder.ivStatus.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iBlind.showDlg_onClickStatusIcon(BlindScanTLPDetailBean.Type_PDT_HAS_SN, groupPosition, -1);
				}
			});
			break;
		}
	}
	
	@Override
	public int getGroupCount() {
		return mData.getGroupsSize();
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		return mData.getChildrenCount(groupPosition);
	}
	
	@Override
	public int getGroupType(int groupPosition) {
		return mData.getType(groupPosition);
	}
	@Override
	public int getGroupTypeCount() {
		return 3;
	}
	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initChildView(holder, groupPosition, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillChildData(holder, groupPosition, childPosition, convertView);
		setChildListener(holder, groupPosition, childPosition);
		
		
		
		return convertView;
	}

	/**
	 * 初始化Child View
	 * @param holder
	 * @param groupPosition
	 * @param convertView
	 * @return
	 */
	private View initChildView(ViewHolder holder, int groupPosition, View convertView) {
		//----------------根据Group的Type来进行初始化子View  因为一个组内只会有同种类型的layout
		switch (getGroupType(groupPosition)) {
		case CLP:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_clp_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blindscan_clp);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindCLPType);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindCLPNumber);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvBlindClpImgCnt);
			holder.btnDamage = (Button) convertView.findViewById(R.id.btnBlindClpDamage);
			holder.ivDamaged = (ImageView) convertView.findViewById(R.id.ivDamaged);
			holder.flTakePhoto =  (FrameLayout) convertView.findViewById(R.id.image_btn_takephoto_clp);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_clp_status_icon);
			break;

		case PDT_HAS_SN:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_product_sn_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blindscan_pdt_sn);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindProductSnType);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindProductSnNumber);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvImgSnCnt);
			holder.btnDamage = (Button) convertView.findViewById(R.id.btnBlindProductSnDamage);
			holder.ivDamaged = (ImageView) convertView.findViewById(R.id.iv);
			holder.flTakePhoto =  (FrameLayout) convertView.findViewById(R.id.image_btn_takephoto_pdt);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_d_p_child_status);
			break;
		}
		return convertView;
	}
	

	/**
	 * 设置ChildView的监听器
	 * @param holder
	 * @param groupPosition
	 * @param childPosition
	 */
	private void setChildListener(final ViewHolder holder, final int groupPosition, final int childPosition) {
		final int type = mData.getType(groupPosition);
		switch (mData.getType(groupPosition)) {
		case BlindScanTLPDetailBean.Type_CLP:
			final CCT_CLP_Bean clpBean = (CCT_CLP_Bean) mData.getChildBeanByPosition(groupPosition, type); mData.getChildBeanByPosition(groupPosition, childPosition);
				holder.flTakePhoto.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					iBlind.showDlg_photos_clp(clpBean);
				}
				});
				
				holder.btnDamage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mData.isUpdated=true;
					boolean isDamaged = (Boolean) ((type==BlindScanTLPDetailBean.Type_CLP)
							?((CCT_CLP_Bean)mData.getChildBeanByPosition(groupPosition, childPosition)).isDamaged
							:((CCT_ProductBean)mData.getChildBeanByPosition(groupPosition, childPosition)).isDamaged);
					CCT_CLP_Bean clpBean = ((CCT_CLP_Bean)mData.getChildBeanByPosition(groupPosition, childPosition));
					clpBean.isDamaged = !isDamaged;
					BlindScanTLPDetailAdp.this.notifyDataSetChanged();
					if(clpBean.isDamaged) {
						iBlind.showDlg_photos_clp(clpBean);
					}
					
				}
				});
				
				/*//--------------------------长按删除事件
				holder.item.setOnLongClickListener(new OnLongClickListener() {
					
					@Override
					public boolean onLongClick(View v) {
						iBlind.showDlg_onLongPress(type, groupPosition, childPosition);
						return true;
					}
				});*/
				
				holder.ivStatus.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						iBlind.showDlg_onClickStatusIcon(type, groupPosition, childPosition);
					}
				});
				break;
				
		case BlindScanTLPDetailBean.Type_PDT_HAS_SN:
			final CCT_ProductBean pdtBean = (CCT_ProductBean) mData.getChildBeanByPosition(groupPosition, childPosition);
			holder.flTakePhoto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				iBlind.showDlg_photos_pdt(type, pdtBean);
			}
			});
			holder.btnDamage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mData.isUpdated=true;
				boolean isDamaged = (type==BlindScanTLPDetailBean.Type_CLP)
						?((CCT_CLP_Bean)mData.getChildBeanByPosition(groupPosition, childPosition)).isDamaged
						:((CCT_ProductBean)mData.getChildBeanByPosition(groupPosition, childPosition)).isDamaged;
				isDamaged = mData.setDamagedByPosition(type, !isDamaged, groupPosition, childPosition);
				CCT_ProductBean pdtBean = ((CCT_ProductBean)mData.getChildBeanByPosition(groupPosition, childPosition));
				BlindScanTLPDetailAdp.this.notifyDataSetChanged();
				if(pdtBean.isDamaged) {
					iBlind.showDlg_photos_pdt(type, pdtBean);
				}
			}
			});
			
			//--------------------------长按删除事件
			/*holder.item.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					iBlind.showDlg_onLongPress(type, groupPosition, childPosition);
					return true;
				}
			});*/
			
			holder.ivStatus.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iBlind.showDlg_onClickStatusIcon(type, groupPosition, childPosition);
				}
			});
			break;
		
		}
	}
	
	
	@Override
	public int getChildType(int groupPosition, int childPosition) {
		if(CLP == getGroupType(groupPosition)) {
			return 0;
		} else {
			return 1;
		}
	}

	@Override
	public int getChildTypeCount() {
		return 2;
	}

	/**
	 * 填充子条目数据
	 * @param holder
	 * @param groupPosition
	 * @param convertView
	 */
	private void fillChildData(ViewHolder holder, int groupPosition, int childPosition, View convertView) {
		//----------------根据Group的Type来进行初始化子View  因为一个组内只会有同种类型的layout
		switch (getGroupType(groupPosition)) {
		case CLP:
			CCT_CLP_Bean clpBean = (CCT_CLP_Bean)mData.getChildBeanByPosition(groupPosition, childPosition);
//			holder.item.setSelected(clpBean.itemIsSeleted);
			holder.tvType.setText("CLP ");
			holder.tvNumber.setText(clpBean.REAL_NUMBER);
			holder.tvImgCnt.setText(clpBean.photos.size()+"");
			holder.tvImgCnt.setVisibility(clpBean.photos.size()==0?View.INVISIBLE:View.VISIBLE);
//			holder.ivDamaged.setSelected(clpBean.photos.size()!=0);
//			isDamaged = mData.getDamagedByPosition(itemViewType, position);
//			isDamaged = clpBean.isDamaged;
//			holder.btnDamage.setBackgroundResource(isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
//			holder.btnDamage.setSelected(clpBean.isDamaged);
			holder.btnDamage.setBackgroundResource(clpBean.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
			holder.ivDamaged.setSelected(clpBean.photos.size()!=0);
			holder.flTakePhoto.setVisibility(clpBean.isDamaged?View.VISIBLE:View.INVISIBLE);
			
			// 背景
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
			} else {
				holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
			}
			break;
		
		case PDT_HAS_SN:
			CCT_ProductBean pdtBean_sn = (CCT_ProductBean)mData.getChildBeanByPosition(groupPosition, childPosition);
//			holder.item.setSelected(pdtBean_sn.itemIsSeleted);
			holder.tvType.setText("S/N ");
			holder.tvNumber.setText(pdtBean_sn.SN_BLIND_SINGLE);
			holder.tvImgCnt.setText(pdtBean_sn.blindPhotos.size()+"");
			holder.tvImgCnt.setVisibility(pdtBean_sn.blindPhotos.size()==0?View.INVISIBLE:View.VISIBLE);
//			holder.ivDamaged.setSelected(pdtBean_sn.blindPhotos.size()!=0);
//			isDamaged = mData.getDamagedByPosition(itemViewType, position);
//			isDamaged = pdtBean_sn.isDamaged;
//			holder.btnDamage.setBackgroundResource(isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
//			holder.btnDamage.setSelected(pdtBean_sn.isDamaged);
			holder.ivDamaged.setSelected(pdtBean_sn.blindPhotos.size()!=0);
			holder.btnDamage.setBackgroundResource(pdtBean_sn.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
			holder.flTakePhoto.setVisibility(pdtBean_sn.isDamaged?View.VISIBLE:View.INVISIBLE);
			
			// 背景
			if (getChildrenCount(groupPosition) - 1 == childPosition) {
				holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
			} else {
				holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
			}
			break;
			
		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}
	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}
	@Override
	public boolean hasStableIds() {
		return false;
	}
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----打开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }
	
	static class ViewHolder {
		
		LinearLayout item;
		TextView tvType, tvNumber, tvImgCnt;
		Button btnDamage;
		TextView tvTotal, tvDamagedNumber, tvModelNo;
		ImageView ivDamaged;
		FrameLayout flTakePhoto;
		
		// 箭头
		ImageView ivArrow;
		ImageView ivStatus;
	}

	
	
	
	/**
	 * 清除selected状态
	 */
	public void clearSeletedStatus() {
		/////---------------------------Product Type
		int typePos = 0;
		int index = 0;
		boolean isFound = false;
		for(typePos=0; typePos<mData.clpTypeList.size(); typePos++) {
			CCT_CLP_Type_Bean clpTypeBean = mData.clpTypeList.get(typePos);
			for(index=0; index<clpTypeBean.CONTAINERS.size(); index++) {
				CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(index);
				if(clpBean.itemIsSeleted) {
					isFound = true;
					break;
				}
			}
			
			if(isFound) break;
		}
		
		if(isFound) {
			mData.clpTypeList.get(typePos).CONTAINERS.get(index).itemIsSeleted = false;
		}
		
		//-----------------------------Product no sn
		for(CCT_ProductBean pdtBean : mData.pdtNoSnList) {
			if(pdtBean.itemIsSeleted) {
				pdtBean.itemIsSeleted = false;
			}
		}
		
		//----------------------------CLP
		for(CCT_CLP_Type_Bean clpTypeBean : mData.clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.itemIsSeleted) {
					clpBean.itemIsSeleted = false;
				}
			}
		}
		
		BlindScanTLPDetailAdp.this.notifyDataSetChanged();
	}
	
	

}
