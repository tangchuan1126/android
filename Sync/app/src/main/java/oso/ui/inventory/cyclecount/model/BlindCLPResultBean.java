package oso.ui.inventory.cyclecount.model;


/**
 * CLP提交后返回的数据bean
 * @author xialimin
 *
 */
public class BlindCLPResultBean {
	public Data DATA;
	public int status;
	
	public class Data {
		public String CONTAINER;
		public int CONTAINER_TYPE;
		public int CON_ID;
		public int IS_FULL;
		public String LOT_NUMBER;
		public int IS_HAS_SN;
		public int STATUS;
		public int TYPE_ID;
		public String LP_TYPE_NAME;
	}
}
