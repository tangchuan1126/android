package oso.ui.inventory.cyclecount.util;

import oso.ui.inventory.cyclecount.key.CCT_Container_StatusKey;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_ContainerBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import utility.Utility;

/**
 * @ClassName: CCT_Utile 
 * @Description: 包含盘点任务中常用的工具
 * @author gcy
 * @date 2015-4-28 下午8:45:42
 */
public class CCT_JudgeIcon {
	
	
	
	/**
	 * @Description:判断相同model number下的clp父级图标
	 */
	public static void judgeIcon(CCT_ContainerBean c){
		c.statusLP = getIcon(c);
	}
	
	public static int getIcon(CCT_ContainerBean c){
		int icon = CCT_Container_StatusKey.UNSCANNED;
		
		if(c.containerType == Cycle_Count_Task_Key.CCT_TLP){
			return judgeTLP_TypeIcon(c.tlp);
		}
		
		if(c.containerType == Cycle_Count_Task_Key.CCT_CLP){
			return judgeCLP_TypeIcon(c.clp_type);
		}
		
		if(c.containerType == Cycle_Count_Task_Key.CCT_PRODUCT){
			return judgePro_TypeIcon(c.product_type);
		}
		return icon;
	}
	
	/**
	 * @Description:判断相同model number下的clp父级图标
	 */
	public static int judgeTLP_TypeIcon(CCT_TLP_Bean tlp){
		int icon = CCT_Container_StatusKey.UNSCANNED;
		
		int clpStatus = icon;
		int proStatus = icon;
		
		if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
			int missNum = 0;
			int scanNum = 0;
			int unscanNum = 0;
			for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
				int status = judgeCLP_TypeIcon(tlp.CONTAINER_TYPES.get(i));
				if(status==CCT_Container_StatusKey.PROBLEM){
					return CCT_Container_StatusKey.PROBLEM;
				}
				if(status==CCT_Container_StatusKey.MISSING){
					missNum++;
				}
				if(status==CCT_Container_StatusKey.SCANNED){
					scanNum++;
				}
				if(status==CCT_Container_StatusKey.UNSCANNED){
					unscanNum++;
				}
			}
			
			if(missNum==tlp.CONTAINER_TYPES.size()){
				clpStatus = CCT_Container_StatusKey.MISSING;
			}
			
			if(scanNum==tlp.CONTAINER_TYPES.size()){
				clpStatus = CCT_Container_StatusKey.SCANNED;
			}
			
			if(unscanNum==tlp.CONTAINER_TYPES.size()){
				clpStatus = CCT_Container_StatusKey.UNSCANNED;
			}
			if(clpStatus!=CCT_Container_StatusKey.MISSING&&missNum>0&&scanNum>=0&&(missNum+scanNum)==tlp.CONTAINER_TYPES.size()){
				clpStatus = CCT_Container_StatusKey.PROBLEM;
			}
		}
		if(!Utility.isNullForList(tlp.PRODUCTS)){
			int missNum = 0;
			int scanNum = 0;
			int unscanNum = 0;
			int problemNum = 0;
			for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
				int status = judgePro_TypeIcon(tlp.PRODUCTS.get(i));
				if(status==CCT_Container_StatusKey.PROBLEM){
					problemNum++;
				}
				if(status==CCT_Container_StatusKey.MISSING){
					missNum++;
				}
				if(status==CCT_Container_StatusKey.SCANNED){
					scanNum++;
				}
				if(status==CCT_Container_StatusKey.UNSCANNED){
					unscanNum++;
				}
			}
			
			proStatus = CCT_Container_StatusKey.UNSCANNED;
			
			if(missNum==tlp.PRODUCTS.size()){
				proStatus = CCT_Container_StatusKey.MISSING;
			}
			
			if(scanNum==tlp.PRODUCTS.size()){
				proStatus = CCT_Container_StatusKey.SCANNED;
			}
			
			if(unscanNum==tlp.PRODUCTS.size()){
				proStatus = CCT_Container_StatusKey.UNSCANNED;
			}
			if(problemNum==tlp.PRODUCTS.size()){
				proStatus = CCT_Container_StatusKey.PROBLEM;
			}

			if(proStatus != CCT_Container_StatusKey.PROBLEM&&proStatus!=CCT_Container_StatusKey.MISSING){
				boolean problemFlag = (problemNum>0||missNum>0);
				if(problemFlag&&scanNum>=0&&(problemNum+missNum+scanNum)==tlp.PRODUCTS.size()){
					proStatus = CCT_Container_StatusKey.PROBLEM;
				}
			}
			
		}
		
		if(!Utility.isNullForList(tlp.CONTAINER_TYPES)&&!Utility.isNullForList(tlp.PRODUCTS)){
			if(clpStatus==CCT_Container_StatusKey.MISSING&&proStatus==CCT_Container_StatusKey.MISSING){
				return CCT_Container_StatusKey.MISSING;
			}
			
			if(clpStatus==CCT_Container_StatusKey.SCANNED&&proStatus==CCT_Container_StatusKey.SCANNED){
				return CCT_Container_StatusKey.SCANNED;
			}

			if(clpStatus!=CCT_Container_StatusKey.UNSCANNED&&proStatus!=CCT_Container_StatusKey.UNSCANNED){
				return CCT_Container_StatusKey.PROBLEM;
			}
			
		}
		
		if(Utility.isNullForList(tlp.PRODUCTS)&&!Utility.isNullForList(tlp.CONTAINER_TYPES)){
			return clpStatus;
		}
		
		if(!Utility.isNullForList(tlp.PRODUCTS)&&Utility.isNullForList(tlp.CONTAINER_TYPES)){
			return proStatus;
		}
		
		if(Utility.isNullForList(tlp.PRODUCTS)&&Utility.isNullForList(tlp.CONTAINER_TYPES)){
			return icon;
		}
		
		return icon;
	}
	
	/**
	 * @Description:判断相同model number下的clp父级图标
	 */
	public static int judgeCLP_TypeIcon(CCT_CLP_Type_Bean clp_type){
		int icon = CCT_Container_StatusKey.UNSCANNED;
		int selectNum = 0;
		int missNum = 0;
//		int damageNum = 0;
		
		for (int i = 0; i < clp_type.CONTAINERS.size(); i++) {
			if(clp_type.CONTAINERS.get(i).statusLP == CCT_Container_StatusKey.MISSING){
				missNum++;
			}
			
//			if(clp_type.CONTAINERS.get(i).isDamaged){
//				damageNum++;
//			}
			
			if(clp_type.CONTAINERS.get(i).statusLP == CCT_Container_StatusKey.SCANNED){
				selectNum++;
			}
		}
		
		if(missNum==clp_type.CONTAINERS.size()){
			return CCT_Container_StatusKey.MISSING;
		}
		
		if(selectNum==clp_type.CONTAINERS.size()/*&&damageNum==0*/){
			return CCT_Container_StatusKey.SCANNED;
		}
		
		if(missNum>0/*||damageNum>0*/){
			return CCT_Container_StatusKey.PROBLEM;
		}
		
		return icon;
	}
	
	/**
	 * @Description:判断相同model number下的pro父级图标
	 */
	public static int judgePro_TypeIcon(CCT_ProductBean pro_type){
		int icon = CCT_Container_StatusKey.UNSCANNED;
		if(pro_type.IS_HAS_SN == Cycle_Count_Task_Key.CCT_P_HAS_SN){
			int selectNum = 0;
			int missNum = 0;
//			int damageNum = 0;
			
			for (int i = 0; i < pro_type.SN.size(); i++) {
				String sn = pro_type.SN.get(i);
				if(pro_type.SN_Map.get(sn)!=null&&pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.MISSING){
					missNum++;
				}
				
				/*if(pro_type.SN_Damaged_Map.get(sn)!=null&&pro_type.SN_Damaged_Map.get(sn)){
					damageNum++;
				}*/
				
				if(pro_type.SN_Map.get(sn)!=null&&pro_type.SN_Map.get(sn) == CCT_Container_StatusKey.SCANNED){
					selectNum++;
				}
			}
			//有SN的时候有两种情况 一种是当前tlp已给的数据 一种是后扫描进来的 如果后扫描进来的数据 则没有qty
			if(pro_type.QUANTITY==0){
				if(missNum== pro_type.SN.size()){
					return CCT_Container_StatusKey.MISSING;
				}

				if(selectNum== pro_type.SN.size()/*&&damageNum==0*/){
					return CCT_Container_StatusKey.SCANNED;
				}

				if(missNum>0&&(missNum+selectNum)==pro_type.SN.size()/*||damageNum>0*/){
					return CCT_Container_StatusKey.PROBLEM;
				}
			}else{

				if(pro_type.QUANTITY>=pro_type.SN.size()){
					if(missNum== pro_type.QUANTITY){
						return CCT_Container_StatusKey.MISSING;
					}

					if(selectNum== pro_type.QUANTITY/*&&damageNum==0*/){
						return CCT_Container_StatusKey.SCANNED;
					}

					if((missNum+selectNum)>0/*||damageNum>0*/){
						return CCT_Container_StatusKey.PROBLEM;
					}
					if(pro_type.is_opreation&&(missNum+selectNum)==0){
						return CCT_Container_StatusKey.MISSING;
					}
				}else{
					if((selectNum+missNum)==pro_type.SN.size()){
						return CCT_Container_StatusKey.PROBLEM;
					}
				}
			}

		}else{
			icon = CCT_Container_StatusKey.UNSCANNED;
			
			if(!pro_type.is_opreation&&pro_type.statusLP != CCT_Container_StatusKey.UNSCANNED){
				return pro_type.statusLP;
			}
			
			if(!pro_type.IS_NEW&&pro_type.TOTAL == pro_type.QUANTITY){
				icon = CCT_Container_StatusKey.SCANNED;
			}
			
			if(!pro_type.IS_NEW&&pro_type.TOTAL > pro_type.QUANTITY){
				icon = CCT_Container_StatusKey.PROBLEM;
			}
			
			if(pro_type.IS_NEW&&pro_type.is_opreation){
				if(pro_type.TOTAL==0){
					icon = CCT_Container_StatusKey.MISSING;
				}else{
					icon = CCT_Container_StatusKey.SCANNED;
				}
			}
			
			if(pro_type.is_opreation&&pro_type.QUANTITY > pro_type.TOTAL){
				if(pro_type.TOTAL==0){
					icon = CCT_Container_StatusKey.MISSING;
				}else{
					icon = CCT_Container_StatusKey.PROBLEM;
				}
			}
			
			return icon;
		}
		
		
		return icon;
	}

	/**
	 * @Description:从tlp中查询是否有新的数据填充进来
	 */
	public static boolean foundNewFromTLP(CCT_TLP_Bean tlp){
		if(!Utility.isNullForList(tlp.CONTAINER_TYPES)){
			for (int i = 0; i < tlp.CONTAINER_TYPES.size(); i++) {
				CCT_CLP_Type_Bean clp_type_bean = tlp.CONTAINER_TYPES.get(i);
				if(!Utility.isNullForList(clp_type_bean.CONTAINERS)){
					for(int j = 0; j<clp_type_bean.CONTAINERS.size();j++){
						if(clp_type_bean.CONTAINERS.get(j).isNewData){
							return true;
						}
					}
				}
			}
		}
		if(!Utility.isNullForList(tlp.PRODUCTS)){
			for (int i = 0; i < tlp.PRODUCTS.size(); i++) {
				CCT_ProductBean productBean = tlp.PRODUCTS.get(i);
				if (Utility.isNullForList(productBean.SN)) {
					if (productBean.IS_NEW) {
						return true;
					}
				} else {
					for (int j = 0; j < productBean.SN.size(); j++) {
						if (productBean.NEW_SN_Map.get(productBean.SN.get(j)) != null
								&& productBean.NEW_SN_Map.get(productBean.SN.get(j))) {
							return true;
						}
					}
				}

			}
		}

		return false;
	}

}
