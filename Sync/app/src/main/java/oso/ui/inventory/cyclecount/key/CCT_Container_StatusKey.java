package oso.ui.inventory.cyclecount.key;

import java.util.HashMap;
import java.util.Map;

import declare.com.vvme.R;



/**
 * @ClassName: Cycle_Count_Task_Key 
 * @Description: 托盘的状态
 * @author gcy
 * @date 2015-4-3 下午4:13:54
 */
public class CCT_Container_StatusKey {
	
	//---------托盘的状态
	public static final int UNSCANNED = 0;
	public static final int SCANNED = 1;
	public static final int MISSING = 2;
	public static final int DAMAGE = 3;
	public static final int PROBLEM = 4;//有问题的
	
	
	final static Map<String, Integer> scannedStatus = new HashMap<String, Integer>();//任务处理级别
	
	static{
		scannedStatus.put(String.valueOf(UNSCANNED), R.drawable.ic_cct_unscanned);	
		scannedStatus.put(String.valueOf(SCANNED), R.drawable.ic_cct_scanned);
		scannedStatus.put(String.valueOf(MISSING), R.drawable.ic_cct_missing);
		scannedStatus.put(String.valueOf(PROBLEM), R.drawable.ic_cct_prompt);
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getResource(int type)
	{
		return getResource(type+"");
	}
	
	/**
	 * 通过类型获取任务类型
	 */
	public static int getResource(String type)
	{
		return scannedStatus.get(type);
	}
	
}
