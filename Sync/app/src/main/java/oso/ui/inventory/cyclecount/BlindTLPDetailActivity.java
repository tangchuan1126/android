package oso.ui.inventory.cyclecount;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.debug.product.CreateProductActivity;
import oso.ui.debug.product.bean.ProductBean;
import oso.ui.inventory.cyclecount.adapter.BlindScanTLPDetailAdp;
import oso.ui.inventory.cyclecount.iface.IBlind;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.BlindCLPResultBean;
import oso.ui.inventory.cyclecount.model.BlindPdtResultBean;
import oso.ui.inventory.cyclecount.model.BlindScanDataBean;
import oso.ui.inventory.cyclecount.model.BlindScanTLPDetailBean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.CCT_ProductBean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Dialog;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.BottomDialog.OnSubmitClickListener;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.tts.TTS;
import support.key.TTPKey;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import support.network.SimpleJSONUtil;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
/**
 * 
 * @author xialimin
 * @date 2015-4-14 下午5:40:06
 * 
 * @description 盲盘扫TLP里的Clp或者有无SN的Product
 */
public class BlindTLPDetailActivity extends BaseActivity implements OnClickListener, IBlind {
	public static final int Type_CLP = 0;
	public static final int Type_PDT_SN = 1;
	public static final int Type_PDT_SKU = 2;
	public static final int Type_PDT_Product_Code = 3;
	
	
	private Context mContext;
	private ExpandableListView mLv;
	//-----CLP -------- Product_Code --------- SN 使用的扫描框  
	private SearchEditText mEtScanNumber;
	
	//-----SKU 的扫描框
	private SearchEditText mEtScanModelNumber , mEtScanLotNo, mEtScanProCode;
	
	private SingleSelectBar mSelectBar;
	private EditText mEtTotal, mEtDamage, mEtModel, mEtLotNo; // modelNo dialog里的total和damage
	
	private TabToPhoto ttp;
	private BlindScanTLPDetailAdp mAdp;
	private BlindScanTLPDetailBean mData = new BlindScanTLPDetailBean();


	private NetConnection_CCT conn;
	private String interimStr; //用于保存临时的code

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_cycle_count_blind_scan_detail, 0);
		applyParams();
		initView();
		initListener();
		initData();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (ttp != null) {
			ttp.onActivityResult(requestCode, resultCode, data);
		}

		if(requestCode == CCT_CreateCLP.resultCode){
			if(data.getSerializableExtra("clp")==null){
				return;
			}
			CCT_CLP_Bean clp_bean  = (CCT_CLP_Bean) data.getSerializableExtra("clp");
			requestCLP(clp_bean.CONTAINER);
		}

		if(requestCode == CreateProductActivity.resultCode){
			if(data.getSerializableExtra("product")==null||data.getLongExtra("pc_id",0)==0){
				return;
			}
			ProductBean returnData = (ProductBean)data.getSerializableExtra("product");
			String pc_id = data.getStringExtra("pc_id");
			requestProductCode(returnData.productName);
		}
	}
	
	private static BlindScanDataBean mData_bridge; 
	
	public static void initParams(Intent in,BlindScanDataBean data){
		mData_bridge=data;
	}
	
	
	private int currPosition;
	private String currTLPNumber="";
	private String currTLPID = "";
	private CCT_TLP_Bean tlpDetailBean;
	private BlindScanDataBean outBean;
	private CCT_LocationBean locationBean;
	private Cycle_Count_Tasks_Bean taskBean;
	private ScanLocationDataBean sBean = new ScanLocationDataBean();
	
	private void applyParams() {
		currPosition = getIntent().getIntExtra(BlindScanActivity.INTENT_EXTRA_POSITION, -1);
		currTLPNumber = getIntent().getStringExtra(BlindScanActivity.INTENT_EXTRA_TLPNUMBER);
		currTLPID = getIntent().getStringExtra(BlindScanActivity.INTENT_EXTRA_TLP_CON_ID);
		tlpDetailBean = (CCT_TLP_Bean) getIntent().getSerializableExtra(BlindScanActivity.INTENT_EXTRA_TLP_DETAIL);
		locationBean = (CCT_LocationBean) getIntent().getSerializableExtra(BlindScanActivity.INTENT_EXTRA_LOCATION);
		taskBean = (Cycle_Count_Tasks_Bean) getIntent().getSerializableExtra(BlindScanActivity.INTENT_EXTRA_TASK);
		try{
			sBean.slc_id = locationBean.LOCATION_ID+"";
			sBean.slc_type = locationBean.LOCATION_TYPE+"";
		}catch (Exception e){
			UIHelper.showToast(getString(R.string.sync_data_error));
			return;
		}
		//------------location下的数据bean
		outBean=mData_bridge;
		if(outBean!=null) {
			if(outBean.getAllSize()!=0) {outBean.clearAllSeletedStatus();};
		}
		mData_bridge=null;
	}

	private void initView() {
		mContext = BlindTLPDetailActivity.this;
		conn = new NetConnection_CCT();

		mLv = (ExpandableListView) findViewById(R.id.lvBlindScanTLPDetail);
		mAdp = new BlindScanTLPDetailAdp(this, mData, this);
		mLv.setAdapter(mAdp);
		mAdp.expandAll(mLv);
		
		TextView tvEmpty = (TextView) findViewById(R.id.tvBlindDetailEmpty);
		mLv.setEmptyView(tvEmpty);
		
		mSelectBar = (SingleSelectBar) findViewById(R.id.blindTLPDetailSlcBar);
		mEtScanNumber = (SearchEditText) findViewById(R.id.etScanDetailNumber);
		mEtScanModelNumber = (SearchEditText) findViewById(R.id.etScanModelNumber);
		mEtScanLotNo = (SearchEditText) findViewById(R.id.etScanLotNumber);
		mEtScanProCode = (SearchEditText) findViewById(R.id.etScanProCode);
		
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		mEtScanNumber.setTransformationMethod(trans);
		mEtScanNumber.setIconMode(SearchEditText.IconMode_Scan);
		mEtScanModelNumber.setTransformationMethod(trans);
		mEtScanModelNumber.setIconMode(SearchEditText.IconMode_Scan);
		mEtScanLotNo.setTransformationMethod(trans);
		mEtScanLotNo.setIconMode(SearchEditText.IconMode_Scan);
		mEtScanProCode.setTransformationMethod(trans);
		mEtScanProCode.setIconMode(SearchEditText.IconMode_Scan);
		
		((TextView) vTopbar.findViewById(R.id.topbar_title)).setText("TLP: " + currTLPNumber);
		showBackButton(true);
		initSelectBar();
	}
	
	/**
	 * 有数据 则显示(之前该TLP已经被编辑过)
	 */
	private void initData() {
		if(tlpDetailBean == null) return;
		mData.clpTypeList.addAll(tlpDetailBean.clpTypeList);
		mData.pdtTypeList.addAll(tlpDetailBean.pdtTypeList);
		mData.pdtNoSnList.addAll(tlpDetailBean.pdtNoSnList);
		mAdp.expandAll(mLv);
		mAdp.notifyDataSetChanged();
	}
	
	private void add2Data(String value, HoldDoubleValue<String, Integer> currentSelectItem) {
		switch (currentSelectItem.b) {
		case SelectBar_Index_CLP: 
			requestCLP(value);
			break;
		case SelectBar_Index_SN:
			requestPdtWithSn(value);
			break;
		case SelectBar_Index_SKU:
			requestProductCode(value);
			break;
		}
		mAdp.notifyDataSetChanged();
		
	}
	
	private void initListener() {
		findViewById(R.id.topbar_back).setOnClickListener(this);
		findViewById(R.id.btnBlindTLPDetailSubmit).setOnClickListener(this);
		mEtScanNumber.setSeacherMethod(new SeacherMethod() {
		@Override
		public void seacher(String value) {
			if(TextUtils.isEmpty(value)) return;
			mEtScanNumber.requestFocus();
			add2Data(value, mSelectBar.getCurrentSelectItem());
			mEtScanNumber.setText("");
			Utility.colseInputMethod(mActivity, mEtScanNumber);
		}
		});
		
		mEtScanModelNumber.setSeacherMethod(new SeacherMethod() {
		@Override
		public void seacher(String modelNo) {
			reqModelLotNo(modelNo, "", Type_Scan_ModelNO);
		}
		});
		
		mEtScanLotNo.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String lotNo) {
				String modelNo = mEtScanModelNumber.getText().toString();
				reqModelLotNo(modelNo, lotNo, Type_Scan_LotNO);
				
			}
		});
		
		mEtScanProCode.setSeacherMethod(new SeacherMethod() {
			
			@Override
			public void seacher(String value) {
				requestProductCode(value);
				mEtScanProCode.setText("");
			}
		});
		
		mEtScanProCode.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					String val_proCode = mEtScanProCode.getText().toString();
					
					requestProductCode(val_proCode);
					mEtScanProCode.requestFocus();
					mEtScanProCode.setText("");
					Utility.colseInputMethod(mActivity, mEtScanLotNo);
				}
				return false;
			}
		});
		
	}
	
	private static final int Type_Scan_ModelNO = 0;
	private static final int Type_Scan_LotNO = 1;
	/**
	 * 请求Model Lot No.
	 * @param modelNo
	 * @param lotNo
	 */
	private void reqModelLotNo(String modelNo, String lotNo, int currType) {
		if(Type_Scan_ModelNO == currType) {
			if(StringUtil.isNullOfStr(modelNo)){
				UIHelper.showToast("Model NO. not be empty");
				mEtScanModelNumber.requestFocus();
				return;
			}
		}
		
		if(Type_Scan_LotNO == currType) {
			if(StringUtil.isNullOfStr(lotNo)){
				UIHelper.showToast("Lot NO. not be empty");
				mEtScanLotNo.requestFocus();
				return;
			}
		}
		
		requestSku(modelNo, lotNo);
//		requestProductCode(modelNo+"/"+lotNo);
//		add2Data(modelNo+"/"+lotNo, mSelectBar.getCurrentSelectItem());
		mEtScanModelNumber.setText("");
		mEtScanLotNo.setText("");
		mEtScanProCode.setText("");
		Utility.colseInputMethod(mActivity, mEtScanNumber);
	}
	
	
	private void requestSku(String model, String lot) {
		requestPdtModelLot(null, model, lot);
	}

	private void endDo(final int selection) {
		runOnUiThread(new Runnable() {
		@Override
		public void run() {
			mData.isUpdated=true;
			mEtScanNumber.setText("");
			mEtScanLotNo.setText("");
			mAdp.notifyDataSetChanged();
			if(selection >= 0) {
				mLv.setSelection(selection);
			}
		}
		});
	}
	
	
	/**
	 * 扫描到Product Code  == Model No./Lot No.
	 * @param proCode
	 */
	private void requestProductCode(final String proCode) {
		if(TextUtils.isEmpty(proCode)) {
			UIHelper.showToast("Pro. Code is empty!");
			mEtScanProCode.requestFocus();
			return;
		} else {
			mEtScanProCode.setText("");
		}

		conn.CCT_GetProductByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				// 清除之前的选中状态
				mAdp.clearSeletedStatus();
				mEtScanProCode.requestFocus();
				try {
					BlindPdtResultBean resultBean = new Gson().fromJson(json.toString(), BlindPdtResultBean.class);
					CCT_ProductBean pdtBean = new CCT_ProductBean();
					pdtBean.modelNo = proCode;

					// 如果有SN
					if (resultBean == null) return;
					// 个数
					if (resultBean.DATA.size() == 1) {

						//----------根据返回的信息创建Product_Bean
						pdtBean.PC_ID = resultBean.DATA.get(0).PC_ID;
						pdtBean.IS_HAS_SN = resultBean.DATA.get(0).IS_HAS_SN;
						pdtBean.comparedId = tlpDetailBean.CON_ID + "/" + pdtBean.PC_ID;

						//----------判断当前TLP是否有重复
						if (isContain(BlindScanTLPDetailBean.Type_PDT_NO_SN, pdtBean.PC_ID + "")) {
							CCT_ProductBean pdt = mData.getRepeatPdtBean(proCode);
							if (pdt.TOTAL > 0) {
								pdt.itemIsSeleted = true;
								int position = mData.getPdtRepeatOldPosition(proCode);
								if (position == -1) {
									UIHelper.showToast(getString(R.string.sync_position_error));
									return;
								}
								CCT_ProductBean pdtRepeat = mData.getRepeatPdtBean(proCode);
								mLv.setSelection(position);
								mAdp.notifyDataSetChanged();
								TTS.getInstance().speakAll_withToast("Already exists");
								mEtScanNumber.setText("");
								mEtScanLotNo.setText("");
								mEtScanProCode.setText("");

								//---弹出框
								showEditStagingDialog(true, position, pdtRepeat.P_NAME, pdtRepeat.TOTAL, pdtRepeat.DAMAGEDCNT, pdtRepeat);
								mAdp.notifyDataSetChanged();
								return;

							}
						}

						// 有SN  需要去扫描SN
						if (resultBean.DATA.get(0).IS_HAS_SN == 1) {
							mSelectBar.userDefineSelectIndexExcuteClick(SelectBar_Index_SKU); // SN
							mEtScanNumber.setText("");
							mEtScanLotNo.setText("");
							mEtScanProCode.setText("");
							TTS.getInstance().speakAll_withToast("Please scan SN");
							return;
						} else {
							// IS_HAS_SN == 2 的时候  代表可直接添加damage的个数和total的个数
							pdtBean.PC_ID = resultBean.DATA.get(0).PC_ID;
							pdtBean.IS_HAS_SN = resultBean.DATA.get(0).IS_HAS_SN;
							openDlg(null, pdtBean);
							return;
						}

					} else {
						showPdtsListDlg(proCode, null, resultBean);
					}

					showEditStagingDialog(false, 0, proCode, 0, 0, pdtBean);

				} catch (Exception e) {
					System.out.println(e);
				}
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					UIHelper.showToast(getString(R.string.sync_not_found));
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Create Product?", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							CreateProductActivity.toThis(mActivity, false);
						}
					});
				} else {
					conn.handFail(json);
				}

			}
		}, proCode,tlpDetailBean.CON_ID);
	}
	
	// 请求ProductSKU
	private void requestPdtModelLot(final BottomDialog oldDlg, final String modelNo, final String lotNo) {
		
		RequestParams params = new RequestParams();
		final String sku = modelNo + (TextUtils.isEmpty(lotNo)?"":"/"+lotNo);
		params.put("sku", sku);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				// 清除之前的选中状态
				mAdp.clearSeletedStatus();
				
				CCT_ProductBean pdtBean = new CCT_ProductBean();
				pdtBean.modelNo = modelNo; 
				pdtBean.lotNo = TextUtils.isEmpty(lotNo)?"":lotNo;
				BlindPdtResultBean resultBean = new Gson().fromJson(json.toString(), BlindPdtResultBean.class);
				
				if(resultBean.DATA==null) return;
				if(resultBean.DATA.get(0) == null) return;
				
				
				// 有SN  需要去扫描SN
				if(resultBean.DATA.get(0).IS_HAS_SN == 1) {
					mSelectBar.userDefineSelectIndexExcuteClick(SelectBar_Index_SKU); // SN
					mEtScanModelNumber.setText("");
					mEtScanLotNo.setText("");
					mEtScanProCode.setText("");
					TTS.getInstance().speakAll_withToast("Please scan SN");
					return;
				} else {
					// IS_HAS_SN == 2 的时候  代表可直接添加damage的个数和total的个数
//					pdtBean.PC_ID = resultBean.DATA.get(0).PC_ID;
//					pdtBean.IS_HAS_SN = resultBean.DATA.get(0).IS_HAS_SN;
//					openDlg(null, pdtBean);
					if(resultBean.DATA.size() == 1) {
						// 有一个
						pdtBean.PC_ID = resultBean.DATA.get(0).PC_ID;
						pdtBean.P_NAME = resultBean.DATA.get(0).P_NAME;	
						pdtBean.IS_HAS_SN = resultBean.DATA.get(0).IS_HAS_SN;
						
						//----------判断当前TLP是否有重复
						if(isContain(BlindScanTLPDetailBean.Type_PDT_NO_SN, pdtBean.PC_ID+"")) { 
							CCT_ProductBean pdt = mData.getRepeatPdtBean(sku);
							if(pdt.TOTAL > 0) {
								pdt.itemIsSeleted = true;
								int position = mData.getPdtRepeatOldPosition(sku);
								if(position == -1) {UIHelper.showToast(getString(R.string.sync_position_error)); return;}
								CCT_ProductBean pdtRepeat = mData.getRepeatPdtBean(sku);
								mLv.setSelection(position);
								mAdp.notifyDataSetChanged();
								TTS.getInstance().speakAll_withToast("Already exists");
								mEtScanNumber.setText("");
								mEtScanLotNo.setText("");
								mEtScanProCode.setText("");
								
								//---弹出框
								showEditStagingDialog(true, position, pdtRepeat.P_NAME, pdtRepeat.TOTAL, pdtRepeat.DAMAGEDCNT, pdtRepeat);
								mAdp.notifyDataSetChanged();
								return;
								
							}
						}
						
						// 有SN  需要去扫描SN
						if(resultBean.DATA.get(0).IS_HAS_SN == 1) {
							mSelectBar.userDefineSelectIndexExcuteClick(SelectBar_Index_SKU); // SN
							mEtScanNumber.setText("");
							mEtScanLotNo.setText("");
							mEtScanProCode.setText("");
							TTS.getInstance().speakAll_withToast("Please scan SN");
							return;
						} else {
							// IS_HAS_SN == 2 的时候  代表可直接添加damage的个数和total的个数
							pdtBean.PC_ID = resultBean.DATA.get(0).PC_ID;
							pdtBean.IS_HAS_SN = resultBean.DATA.get(0).IS_HAS_SN;
							openDlg(null, pdtBean);
							return;
						}
						
						
					} else {
						// 有多个的时候需要弹出框来进行选择
						showPdtsListDlg(sku, oldDlg, resultBean);
						return;
					}
				}
				
				
//				int selection = mData.clpTypeList.size();
//				if(oldDlg!=null) openDlg(oldDlg, pdtBean);
//				endDo(selection);
			}
			
		}.doGetForYasir(HttpUrlPath.CCT_RequestSKU, params, mContext);
	}
	
	
	private BottomDialog pdtsDialog;
	
	/**
	 * 显示products 列表
	 * @param resultBean
	 */
	private void showPdtsListDlg(final String sku, final BottomDialog oldDialog, BlindPdtResultBean resultBean) {
		if(oldDialog!=null) oldDialog.dismiss();
		final List<BlindPdtResultBean.Data> dataList = resultBean.DATA;
		int dataListSize = dataList.size();
		String [] arr = new String[dataList.size()];
		for(int i=0; i<dataListSize; i++) {
			arr[i] = dataList.get(i).P_NAME;
		}
		
		
		final View view = View.inflate(mActivity, R.layout.dlg_add_blind_pdt_select_layout, null);
		ListView lv = (ListView) view.findViewById(R.id.lvSelectPdt);
		ArrayAdapter<String> adp = new ArrayAdapter<String>(mContext, R.layout.simple_list_item_1_arraw, arr);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				BlindPdtResultBean.Data currData = dataList.get(position);
				CCT_ProductBean pdtBean = new CCT_ProductBean();
				pdtBean.P_NAME = currData.P_NAME;
				pdtBean.PC_ID = currData.PC_ID;
				pdtBean.IS_HAS_SN = currData.IS_HAS_SN;
				
				if(pdtBean.IS_HAS_SN == 1) {
					mSelectBar.userDefineSelectIndexExcuteClick(SelectBar_Index_SN);
					UIHelper.showToast(mContext, "Please scan SN");
					return;
				}
				
				if(isContain(BlindScanTLPDetailBean.Type_PDT_NO_SN, pdtBean.PC_ID+"")) { 
					CCT_ProductBean pdt = mData.getRepeatPdtBean(sku);
					if(pdt.TOTAL > 0) {
						pdt.itemIsSeleted = true;
						int pos = mData.getPdtRepeatOldPosition(sku);
						if(pos == -1) {UIHelper.showToast(getString(R.string.sync_position_error)); return;}
						CCT_ProductBean pdtRepeat = mData.getRepeatPdtBean(sku);
						mLv.setSelection(pos);
						mAdp.notifyDataSetChanged();
						TTS.getInstance().speakAll_withToast("Already exists");
						mEtScanNumber.setText("");
						mEtScanLotNo.setText("");
						mEtScanProCode.setText("");
						
						//---弹出框
						if(pdtsDialog!=null) pdtsDialog.dismiss(); // 关闭之前的dialog
						showEditStagingDialog(true, pos, pdtRepeat.P_NAME, pdtRepeat.TOTAL, pdtRepeat.DAMAGEDCNT, pdtRepeat);
						mAdp.notifyDataSetChanged();
						return;
						
					}
				}else {
					openDlg(pdtsDialog, pdtBean);
				}
			}
		});
		
		pdtsDialog = new BottomDialog(mActivity);
		pdtsDialog.setContentView(view);
		pdtsDialog.setTitle("Select Product");
		pdtsDialog.setCanceledOnTouchOutside(true);
		pdtsDialog.setShowCancelBtn(true);
		pdtsDialog.setDefEtToUpperCase(true);
		pdtsDialog.show();
		
	}
	
	private void openDlg(final BottomDialog oldDlg, final CCT_ProductBean pdtBean) {
		runOnUiThread(new Runnable() {
		@Override
		public void run() {
			if(oldDlg!=null) oldDlg.dismiss(); // 关闭之前的dialog
			String modelNo = pdtBean.modelNo;
			modelNo = TextUtils.isEmpty(modelNo)?"":modelNo.toUpperCase(Locale.ENGLISH);
			if(TextUtils.isEmpty(modelNo)) {
				showEditStagingDialog(false, 0, pdtBean.P_NAME, 0, 0, pdtBean);
			} else {
				showEditStagingDialog(false, 0, modelNo, 0, 0, pdtBean);
			}
		}
		});
	}
	
	
	/**
	 * 请求扫描SN的Product
	 * @param pdtWithSnNo
	 */
	private void requestPdtWithSn(final String pdtWithSnNo) {
		// 校验是否重复
		if(isContain(BlindScanTLPDetailBean.Type_PDT_HAS_SN, pdtWithSnNo)) { 
			// 清除选中
			mAdp.clearSeletedStatus();
			
			UIHelper.showToast(mContext, "Already exists!");
			TTS.getInstance().speakAll_withToast("Already exists");
			mEtScanNumber.setText("");
			mEtScanLotNo.setText("");
			mEtScanProCode.setText("");
			CCT_ProductBean pdtBean = mData.getRepeatPdtSnBean(pdtWithSnNo);
			pdtBean.itemIsSeleted = true;
			mAdp.notifyDataSetChanged();
			return;
		}


		conn.CCT_GetProductBySN(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				// 清除之前的选中状态
				mAdp.clearSeletedStatus();

				CCT_ProductBean pdtBean = new CCT_ProductBean();
				JSONObject jobj = json.optJSONObject("DATA");
				pdtBean.PC_ID = jobj.optInt("PC_ID");
				pdtBean.P_NAME = jobj.optString("P_NAME");
				pdtBean.SN_BLIND_SINGLE = pdtWithSnNo;
				pdtBean.IS_HAS_SN = jobj.optInt("IS_HAS_SN");

				pdtBean.comparedId = tlpDetailBean.CON_ID + "/" + pdtBean.PC_ID;

				// 检验外部是否重复
				CCT_TLP_Bean parent = outBean.getParent(pdtBean.SN_BLIND_SINGLE);
				CCT_ProductBean pdt = (CCT_ProductBean) outBean.getRepeatObj(BlindScanDataBean.Type_PDT_HAS_SN, pdtBean.SN_BLIND_SINGLE);
				if (pdt != null) { // 有重复
					showReplaceDlg(parent, pdt, BlindScanDataBean.Type_PDT_HAS_SN);
					return;
				}

				int selection = mData.addProduct(pdtBean, tlpDetailBean.CON_ID + "");
				mAdp.expandAll(mLv);
				TTS.getInstance().speakAll_withToast("Scan product");
				endDo(selection);
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					UIHelper.showToast(mActivity.getString(R.string.sync_not_found));
					RewriteBuilderDialog.showSimpleDialog(mActivity, "Create Product?", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							CreateProductActivity.toThis(mActivity, false);
						}
					});
				} else {
					conn.handFail(json);
				}
			}
		}, pdtWithSnNo);
	}
	
	/**
	 * 请求CLP
	 * @param clpNo
	 */
	private void requestCLP(final String clpNo) {

		interimStr = ""; //清空

		// 校验是否重复
		if(isContain(BlindScanTLPDetailBean.Type_CLP, clpNo)) {
			// 重复则选中
//			mData.clpList.get(mData.getCLPRepeatOldPostion(clpNo)).itemIsSeleted = true; 
			mAdp.notifyDataSetChanged();
			UIHelper.showToast(mContext, "Already exists!");
			TTS.getInstance().speakAll_withToast("Already exists");
			mEtScanNumber.setText("");
			mEtScanLotNo.setText("");
			mEtScanProCode.setText("");
			return;
		}

		conn.CCT_GetTLP_Or_CLP_ByCode(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_SUCCESS;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				// 清除之前的选中状态
				mAdp.clearSeletedStatus();
				BlindCLPResultBean resultBean = new Gson().fromJson(json.toString(), BlindCLPResultBean.class);

				// 判断类型  如果是TLP 则提示NOT FOUND
				if (resultBean.DATA.CONTAINER_TYPE == Cycle_Count_Task_Key.CCT_TLP) {
					TTS.getInstance().speakAll_withToast("Container not found!");
					return;
				}


				CCT_CLP_Bean clpBean = new CCT_CLP_Bean();
				clpBean.CONTAINER = clpNo;
				clpBean.CONTAINER_TYPE = resultBean.DATA.CONTAINER_TYPE;
				clpBean.CON_ID = resultBean.DATA.CON_ID;
				clpBean.IS_FULL = resultBean.DATA.IS_FULL;
				clpBean.IS_HAS_SN = resultBean.DATA.IS_HAS_SN;
				clpBean.LOT_NUMBER = resultBean.DATA.LOT_NUMBER;
				clpBean.TYPE_ID = resultBean.DATA.TYPE_ID;
				clpBean.REAL_NUMBER = CCT_Util.fixLP_Number(clpNo);
				clpBean.TYPE_NAME = resultBean.DATA.LP_TYPE_NAME;

				// 验证外部是否重复
				CCT_CLP_Bean replaceCLP = (CCT_CLP_Bean) outBean.getRepeatObj(BlindScanDataBean.Type_CLP, clpBean.REAL_NUMBER);
				// 清除信息
				if (replaceCLP != null) {
					replaceCLP.clearInfo();
					CCT_TLP_Bean parent = outBean.getParent(CCT_Util.fixLP_Number(resultBean.DATA.CONTAINER));
					showReplaceDlg(parent, replaceCLP, BlindScanDataBean.Type_CLP);
					return;
				}

				int selection = mData.addCLP(clpBean);
				mAdp.expandAll(mLv);
				TTS.getInstance().speakAll("Scan CLP");
				endDo(selection);
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {
				if (json.optInt("status", 0) == Cycle_Count_Task_Key.STATUS_CREATE_INFO) {
					interimStr = clpNo;
					CCT_Dialog.selectLP_Type(mActivity, clpNo, sBean);
				} else {
					conn.handFail(json);
				}

			}
		}, clpNo);
	}
	
	/**
	 * 判断是否重复添加
	 * @param b
	 * @param value
	 * @return
	 */
	private boolean isContain(Integer b, String value) {
		if(b==BlindScanTLPDetailBean.Type_CLP) return mData.isContainCLPById(value);
		if(b==BlindScanTLPDetailBean.Type_PDT_HAS_SN) return mData.isContainPdtBySn(value);
		if(b==BlindScanTLPDetailBean.Type_PDT_NO_SN) return mData.isContainPdtByModelNo(value);
		return false;
	}

	public static final int SelectBar_Index_CLP = 0;
	public static final int SelectBar_Index_SN = 1;
	public static final int SelectBar_Index_SKU = 2;
	public static final int SelectBar_Index_Product_Code = 3;
	
	private void initSelectBar() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("CLP", SelectBar_Index_CLP));
		clickItems.add(new HoldDoubleValue<String, Integer>("SKU", SelectBar_Index_SKU));
		clickItems.add(new HoldDoubleValue<String, Integer>("SN", SelectBar_Index_SN));
		mSelectBar.setUserDefineClickItems(clickItems);
		mSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
		@Override
		public void clickCallBack(final HoldDoubleValue<String, Integer> selectValue) {
			StringBuilder hint = new StringBuilder("Scan ");
			mEtScanNumber.setVisibility(View.VISIBLE);
			mEtScanLotNo.setVisibility(View.GONE);
			mEtScanModelNumber.setVisibility(View.GONE);
			mEtScanProCode.setVisibility(View.GONE);
			
			//------------如果是SKU类型 则需要显示两个EditText 分别输入Model No.和Lot No.
			switch (selectValue.b) {
			case SelectBar_Index_SKU:
				// 显示EditText
				mEtScanNumber.setText("");
				mEtScanNumber.setVisibility(View.GONE);
				mEtScanLotNo.setVisibility(View.VISIBLE); 
				mEtScanModelNumber.setVisibility(View.VISIBLE);
//				mEtScanProCode.setVisibility(View.VISIBLE);
				
				mEtScanLotNo.setHint(hint + "Lot NO.");
				mEtScanModelNumber.setHint(hint + "Model NO.");
				mEtScanProCode.setHint(hint + "Pro. Code");
				
				mEtScanModelNumber.requestFocus();
				
				/*mEtScanLotNo.setOnKeyListener(new View.OnKeyListener() {
					
					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
							String val_modelNo = mEtScanModelNumber.getText().toString();
							String val_lotNo = mEtScanLotNo.getText().toString();
							
							//-----如果为空则锁定当前焦点 如果不为空则焦点下移
							if(!StringUtil.isNullOfStr(val_modelNo) && !StringUtil.isNullOfStr(val_lotNo) ){
								// 请求ModelNo  和 Lot No
								String modelNo = mEtScanModelNumber.getText().toString();
								String lotNo = mEtScanLotNo.getText().toString();
								reqModelLotNo(modelNo, lotNo, Type_Scan_LotNO);
							} else {
								// 光标跳到Pro.Code上
								mEtScanProCode.requestFocus();
								Utility.colseInputMethod(mActivity, mEtScanLotNo);
							}
							Utility.colseInputMethod(mActivity, mEtScanModelNumber);
						}
						return false;
					}
				});*/
				
				
				//----设置key事件防止会车的时候触发Submit方法
				mEtScanModelNumber.setOnKeyListener(new View.OnKeyListener() {
					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
					if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
						mEtScanLotNo.requestFocus();
						Utility.colseInputMethod(mActivity, mEtScanModelNumber);
					}
						return false;
					}
				});
				break;
			}
			
			mEtScanNumber.requestFocus();
			mEtScanNumber.setHint(hint.append(selectValue.a).toString());
		}
		});
		

		mSelectBar.userDefineSelectIndexExcuteClick(SelectBar_Index_CLP);
	}
	
	/**
	 * 扫描Model No., Lot No.
	 */
	private void showScanModelLotNoDlg() {
		final View view = View.inflate(mActivity, R.layout.dlg_add_blind_model_lot, null);
		mEtModel = (EditText) view.findViewById(R.id.etBlindModelNo);
		mEtLotNo = (EditText) view.findViewById(R.id.etBlindLotNo);
		
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		mEtModel.setTransformationMethod(trans);
		mEtLotNo.setTransformationMethod(trans);
		
		BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		dialog.setTitle("Scan Model NO. / Lot NO.");
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(true);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
			String model = mEtModel.getText().toString();
			String lot = mEtLotNo.getText().toString();
			
			if(!TextUtils.isEmpty(model)) {
				requestPdtModelLot(dlg, model, lot);
			} 
			
			// total和damage都为"" 
			else if(TextUtils.isEmpty(model) && TextUtils.isEmpty(lot)) {
//				mEtTotal.startAnimation(shakeAnimation(5));
//				mEtDamage.startAnimation(shakeAnimation(5));
				UIHelper.showToast(mContext, "Empty!");
			} 
			
			// model 为空
			else if(TextUtils.isEmpty(model)) {
//				mEtTotal.startAnimation(shakeAnimation(5));
				UIHelper.showToast(mContext, "Model NO. is empty!");
			} 
		}
		});
		dialog.show();
	}
	
	/**
	 * 弹出框 填写damage total 和拍照
	 * @param isUpdate 是否是更新数据的。新添加的置为false
	 * @param position listview对应的position
	 * @param modelNo  modelNo
	 * 
	 * @param totalNum 总数
	 * @param damageNum 损坏的数量
	 * @param pdtBean  
	 */
	private void showEditStagingDialog(final boolean isUpdate, final int position, final String modelNo, int totalNum, int damageNum, final CCT_ProductBean pdtBean) {
		if(Utility.isFastClick(1000)) return;
		
		View view = View.inflate(mActivity, R.layout.cct_edit_product_total, null);
		mEtTotal = (EditText) view.findViewById(R.id.e_total);
		mEtDamage = (EditText) view.findViewById(R.id.e_damage);
		final View photo_view = (View) view.findViewById(R.id.photo_view);
		
		if(totalNum!=0) {
			mEtTotal.setText(totalNum+"");
			mEtDamage.setText(damageNum == 0 ? "" : damageNum+"");
			//默认获得焦点
			int cursorIndex = mEtTotal.getText().toString().length();
			mEtTotal.setSelection(cursorIndex);
			int cursorIndex_d = mEtDamage.getText().toString().length();
			mEtDamage.setSelection(cursorIndex_d);
			photo_view.setVisibility(View.VISIBLE);
		}
		final BottomDialog dialog = new BottomDialog(mActivity);
		dialog.setContentView(view);
		String title = ((TextUtils.isEmpty(pdtBean.modelNo)?"":pdtBean.modelNo.toUpperCase(Locale.ENGLISH)) + ((TextUtils.isEmpty(pdtBean.lotNo))?"": " / "+pdtBean.lotNo.toUpperCase(Locale.ENGLISH)));
//		dialog.setTitle(TextUtils.isEmpty(title)? "Select Product" : "Model NO.: "+title );  // 也有可能是Product Code // TODO 修改过的
		if(TextUtils.isEmpty(title)) {title = ""+modelNo;};
		dialog.setTitle(TextUtils.isEmpty(title)? "Select Product" : "SKU: "+title ); 
		dialog.setCanceledOnTouchOutside(true);
		dialog.setShowCancelBtn(false);
		dialog.setDefEtToUpperCase(true);
		dialog.setOnSubmitClickListener(new OnSubmitClickListener() {
		@Override
		public void onSubmitClick(BottomDialog dlg, String value) {
			updatePdtTotalDamage(isUpdate, position, pdtBean, dlg);
		}

		});
		
		//---------total 输入完毕后  damage的edittext设置selection
		mEtTotal.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					//-----如果为空则锁定当前焦点 如果不为空则焦点下移
					mEtDamage.requestFocus();
					int cursorIndex = mEtDamage.getText().toString().length();
					mEtDamage.setSelection(cursorIndex);
					return true;
				}
				return false;
			}
		});
		
		//---------监听文本控制拍照控件是否显示
		mEtDamage.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {	
				photo_view.setVisibility(StringUtil.isNullOfStr(mEtDamage.getText().toString())||"0".equals(mEtDamage.getText().toString())?View.GONE:View.VISIBLE);
			}
		});
		mEtDamage.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP) {
					//-----如果为空则锁定当前焦点 如果不为空则焦点下移
					updatePdtTotalDamage(isUpdate, position, pdtBean, dialog);
					mEtDamage.requestFocus();
					Utility.colseInputMethod(mActivity, mEtScanNumber);
					return true;
				}
				return false;
			}
		});
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, pdtBean, Type_PDT_SKU);
		dialog.show();
	}
	
	/**
	 * 修改Product的total damage的个数
	 * @param isUpdate
	 * @param position
	 * @param pdtBean
	 * @param dlg
	 */
	private void updatePdtTotalDamage(final boolean isUpdate, final int position, final CCT_ProductBean pdtBean, BottomDialog dlg) {
		String total = mEtTotal.getText().toString();
		String damagedNumber = mEtDamage.getText().toString();
		
		int totalInt = Utility.parseInt(total);
		int damagedNumInt = Utility.parseInt(damagedNumber);
		
//		if(!TextUtils.isEmpty(total) && !TextUtils.isEmpty(damagedNumber)) {
		if(!TextUtils.isEmpty(total)) {
			if(TextUtils.isEmpty(damagedNumber)) {
				damagedNumber = "0";
			}
			if((totalInt< damagedNumInt) || (totalInt==0 && totalInt==damagedNumInt)) {
//				mEtTotal.startAnimation(shakeAnimation(5));
//				mEtDamage.startAnimation(shakeAnimation(5));
				mEtDamage.getHandler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						mEtDamage.requestFocus();
					}
				}, 200);
				UIHelper.showToast(mContext, "Amount of damage cannot exceed the total amount!");
				mEtDamage.getHandler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mEtDamage.requestFocus();
					}
				}, 200);
				return;
			} 
			
			// -------是修改的  显示total 和 damage的个数
			if(isUpdate) {
				CCT_ProductBean pdtBean_update = (CCT_ProductBean) mData.getBeanByGrpPosition(position);
				pdtBean_update.TOTAL = Utility.parseInt(total);
				pdtBean_update.DAMAGEDCNT = TextUtils.isEmpty(damagedNumber)? 0 : Utility.parseInt(damagedNumber);
				if(Utility.parseInt(damagedNumber) > 0) {
					pdtBean_update.isDamaged = true;
				} else {pdtBean_update.isDamaged = false;}
				mData.isUpdated=true;
			} else {
				
				int selection=0;
				if(pdtBean!=null) {
					pdtBean.clearInfo();
					pdtBean.TOTAL = Utility.parseInt(total);
					pdtBean.DAMAGEDCNT = TextUtils.isEmpty(damagedNumber)? 0 : Utility.parseInt(damagedNumber);
					if(Utility.parseInt(damagedNumber) > 0) {
						pdtBean.isDamaged = true;
					} else {pdtBean.isDamaged = false;}
					selection = mData.addProduct(pdtBean, tlpDetailBean.CON_ID+"");
					mAdp.expandAll(mLv);
					TTS.getInstance().speakAll("Scan product");
				}
				
				mAdp.notifyDataSetChanged();
				mEtScanNumber.setText("");
				if(selection != -1) mLv.setSelection(selection); 
			}
			
			// 如果有未提交的图片, 则需要请求提交
			if (ttp.getPhotoCnt(false) > 0) {
				reqUploadPhotos_Pdt(pdtBean, ttp);
			}
			
			dlg.dismiss();
		} 
		
		// total和damage都为"" 
		else if(TextUtils.isEmpty(total) && TextUtils.isEmpty(damagedNumber)) {
//			mEtTotal.startAnimation(shakeAnimation(5));
//			mEtDamage.startAnimation(shakeAnimation(5));
			UIHelper.showToast(mContext, "Empty!");
			mEtTotal.getHandler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mEtTotal.requestFocus();
				}
			}, 200);
		} 
		
		// 总数为空
		else if(TextUtils.isEmpty(total)) {
//			mEtTotal.startAnimation(shakeAnimation(5));
			UIHelper.showToast(mContext, "Input Amount of total!");
			mEtTotal.getHandler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mEtTotal.requestFocus();
				}
			}, 200);
		} 
		
		/*// damage数为空  默认就是0
		else if(TextUtils.isEmpty(damagedNumber)) {
			mEtDamage.startAnimation(shakeAnimation(5));
			UIHelper.showToast(mContext, "Input Amount of damage!");
		}*/
		mEtScanModelNumber.requestFocus();
		mAdp.notifyDataSetChanged();
		mLv.setSelection(mData.getEndPosition());
	}
	
	public static Animation shakeAnimation(int counts) {
		Animation translateAnimation = new TranslateAnimation(0, 8, 0, 0);
		translateAnimation.setInterpolator(new CycleInterpolator(counts));
		translateAnimation.setDuration(1000);
		return translateAnimation;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBlindTLPDetailSubmit:
			doSubmit();
			break;
		case R.id.topbar_back:
			showBackDlg();
			break;
		}
	}

	@Override
	public void onBackPressed() {
		showBackDlg();
	};
	
	
	
	/**
	 * 左上角返回 或者 onBackPress时 如果有编辑过的数据 则应该提示是否确定返回
	 */
	private void showBackDlg() {
		
		RewriteBuilderDialog.newSimpleDialog(mContext, "Done?", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			doSubmit(); // 也需要保存
			BlindTLPDetailActivity.this.finish();
			overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
			
		}
		}).show();
	}
	
	/**
	 * 提交并返回到上一界面
	 */
	private void doSubmit() {
		Intent data = new Intent();
		data.putExtra(BlindScanActivity.INTENT_EXTRA_POSITION, currPosition);
		data.putExtra(BlindScanActivity.INTENT_EXTRA_TLP_DETAIL, mData);
		setResult(BlindScanActivity.REQ_DETAIL, data);
		this.finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}

	private void initTTP(TabToPhoto ttp_dlg, final Object obj, final int type) {
		ttp = ttp_dlg;
		String key = "";
		switch (type) {
		case Type_CLP:
			CCT_CLP_Bean clpBean = (CCT_CLP_Bean) obj;
			key = TTPKey.getCCT_Blind_Key(null, clpBean.CON_ID+"");
			ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
			ttp_dlg.pvs[0].addWebPhotoToIv(clpBean.photos);
			break;
		case Type_PDT_SN:
			CCT_ProductBean pdtBean = (CCT_ProductBean) obj;
			key = TTPKey.getCCT_Blind_Key(pdtBean.SN_BLIND_SINGLE, null);
			ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
			ttp_dlg.pvs[0].addWebPhotoToIv(pdtBean.blindPhotos);
			break;
		case Type_PDT_SKU:
			CCT_ProductBean pdtBeanSku = (CCT_ProductBean) obj;
			key = TTPKey.getCCT_Blind_Key(pdtBeanSku.P_NAME, null);
			ttp_dlg.initNoTitleBar(mActivity, TabParam.new_NoTitle(key, ttp));
			ttp_dlg.pvs[0].addWebPhotoToIv(pdtBeanSku.blindPhotos);
			break;
		}
		
		
		ttp_dlg.setOnTTPDeleteListener(new TabToPhoto.OnTTPDeleteListener() {

		@Override
		public void onDeleteComplete(int tabIndex, TTPImgBean b) {
			List<TTPImgBean> imgs = (type==Type_CLP?((CCT_CLP_Bean) obj).photos:((CCT_ProductBean) obj).blindPhotos);
			if (imgs != null && imgs.contains(b)) {
				imgs.remove(b);
				mAdp.notifyDataSetChanged();
			}
		}
		});
	}
	
	/**
	 * 上传图片
	 * @param b
	 * @param ttp
	 */
	private void reqUploadPhotos_Clp(final CCT_CLP_Bean b, final TabToPhoto ttp) {
		if(b==null) return;
		RequestParams p = new RequestParams();
		String fileName = taskBean.ID +"_" + locationBean.LOCATION_ID + "_" + currTLPID +"_"+ b.CON_ID;
//		p.add("fileName", b.CON_ID+"");
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				b.photos = listPhotos;
				mAdp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);
	}
	
	private void reqUploadPhotos_Pdt(final CCT_ProductBean pdt, final TabToPhoto ttp) {
		if(pdt==null) return;
		RequestParams p = new RequestParams();
		String fileName = taskBean.ID +"_" + locationBean.LOCATION_ID + "_" + (TextUtils.isEmpty(pdt.SN_BLIND_SINGLE)?currTLPID+"_"+pdt.PC_ID+"": currTLPID+"_"+pdt.SN_BLIND_SINGLE);
//		p.add("fileName", TextUtils.isEmpty(pdt.SN_BLIND_SINGLE)?currTLPID+"_"+pdt.PC_ID+"":pdt.SN_BLIND_SINGLE);
		p.add("file_name", fileName);
		ttp.uploadZip(p, "receive");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				ttp.clearData();
				JSONArray ja = json.optJSONArray("PHOTO");
				if (Utility.isEmpty(ja))
					return;
				List<TTPImgBean> listPhotos = TTPImgBean.getCCT_PhotoList(ja);
				pdt.blindPhotos = listPhotos;
				mAdp.notifyDataSetChanged();
			}
		}.doPost(HttpUrlPath.CCT_uploadPhoto, p, mActivity);
	}
	
	@Override
	public void showDlg_photos_clp(final CCT_CLP_Bean clpBean) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, clpBean, Type_CLP);
		
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0) reqUploadPhotos_Clp(clpBean, ttp);
				dlg.dismiss();
			}
		});
	}

	
	/**
	 * 替换的Dialog
	 * @param outTLP  可能为NULL  null: 最外层的CLP
	 * @param obj
	 * @param type
	 */
	private void showReplaceDlg(final CCT_TLP_Bean outTLP, final Object obj, final int type) {
		String tlpNumber="";
		if(outTLP == null) tlpNumber = "Loc.: "+ locationBean.LOCATION_NAME ;
		else if(TextUtils.isEmpty(outTLP.REAL_NUMBER)) tlpNumber = "Other TLP ";
		else tlpNumber = "TLP: "+outTLP.REAL_NUMBER;
		
		String message = "It's already scanned at: \n"+ tlpNumber +"\nAre you sure to re-scan?";
		new RewriteBuilderDialog.Builder(mContext).setTitle(getString(R.string.sync_notice)).setMessage(message)
		.setNegativeButton(getString(R.string.sync_no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 取消 do nothing
				mEtScanNumber.setText("");
				mEtScanLotNo.setText("");
				mEtScanProCode.setText("");
				dialog.dismiss();
			}
			//---------------下一版本删除下面代码-------start---------
		}).setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
 
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 替换
//				outBean
				int selection = -1;
				switch (type) {
				case BlindScanDataBean.Type_CLP:
					CCT_CLP_Bean clpBean = (CCT_CLP_Bean) obj;
					outBean.removeBeanByKeywords(type, clpBean.REAL_NUMBER);
					clpBean.clearInfo();
					selection = mData.addCLP(clpBean);
					mAdp.expandAll(mLv);
					TTS.getInstance().speakAll("Scan CLP");
					break;
				case BlindScanDataBean.Type_PDT_HAS_SN:
					CCT_ProductBean pdtBean_SN = (CCT_ProductBean) obj;
					outBean.removeBeanByKeywords(type, pdtBean_SN.SN_BLIND_SINGLE);
					pdtBean_SN.clearInfo();
					selection = mData.addProduct(pdtBean_SN, tlpDetailBean.CON_ID+"");
					mAdp.expandAll(mLv);
					TTS.getInstance().speakAll("Scan product");
					break;
				case BlindTLPDetailActivity.Type_PDT_SKU:
					CCT_ProductBean pdtBean_SKU = (CCT_ProductBean) obj;
					outBean.removeBeanByKeywords(type, pdtBean_SKU.P_NAME);
					openDlg(null, pdtBean_SKU);
					
					break;
				case BlindTLPDetailActivity.Type_PDT_Product_Code :
					CCT_ProductBean pdtBean_procode = (CCT_ProductBean) obj;
					outBean.removeBeanByKeywords(type, pdtBean_procode.P_NAME);
					openDlg(null, pdtBean_procode);
					break;
				}
				
				endDo(selection);
				mEtScanNumber.setText("");
				mEtScanLotNo.setText("");
				mEtScanProCode.setText("");
				dialog.dismiss();
			}
		})
		.show();
			
		//---------------下一版本删除上面代码------end----------
		
		
			// TODO 下一版本需要替换上面的代码
		/*}).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 替换
				int selection = -1;
				switch (type) {
				case BlindScanDataBean.Type_CLP:
					CCT_CLP_Bean clpBean = (CCT_CLP_Bean) obj;
					
					// 删除已经在服务端上的图片 ----------------
					reqDelCLPPhotos(clpBean);
					//---------------------------------
					
					outBean.removeBeanByKeywords(type, clpBean.REAL_NUMBER);
					clpBean.clearInfo();
					selection = mData.addCLP(clpBean);
					mAdp.expandAll(mLv);
					TTS.getInstance().speakAll("Scan CLP");
					break;
				case BlindScanDataBean.Type_PDT_HAS_SN:
					CCT_ProductBean pdtBean_SN = (CCT_ProductBean) obj;
					
					// 删除已经在服务端上的图片 ----------------
					reqDelPdtPhotos(pdtBean_SN);
					//---------------------------------
					
					outBean.removeBeanByKeywords(type, pdtBean_SN.SN_BLIND_SINGLE);
					pdtBean_SN.clearInfo();
					selection = mData.addProduct(pdtBean_SN, tlpDetailBean.CON_ID+"");
					mAdp.expandAll(mLv);
					TTS.getInstance().speakAll("Scan product");
					break;
				case BlindTLPDetailActivity.Type_PDT_SKU:
					CCT_ProductBean pdtBean_SKU = (CCT_ProductBean) obj;
					
					// 删除已经在服务端上的图片 ----------------
					reqDelPdtPhotos(pdtBean_SKU);
					//---------------------------------
					
					outBean.removeBeanByKeywords(type, pdtBean_SKU.P_NAME);
					openDlg(null, pdtBean_SKU);
					
					break;
				case BlindTLPDetailActivity.Type_PDT_Product_Code :
					CCT_ProductBean pdtBean_procode = (CCT_ProductBean) obj;
					
					// 删除已经在服务端上的图片 ----------------
					reqDelPdtPhotos(pdtBean_procode);
					//---------------------------------
					
					outBean.removeBeanByKeywords(type, pdtBean_procode.P_NAME);
					openDlg(null, pdtBean_procode);
					break;
				}
				
				endDo(selection);
				mEtScanNumber.setText("");
				mEtScanLotNo.setText("");
				mEtScanProCode.setText("");
				dialog.dismiss();
			}

		})
		.show();*/
	}
	
	
	private void reqDelPicsByIds(String ids) {
		RequestParams p = new RequestParams();
		p.put("file_ids", ids);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
			}
		}.doPost(HttpUrlPath.CCT_delUploadPhotos, p, mContext);
	}
	
//	/**
//	 * 删除重复的Product已经在服务端上的图片
//	 * @param clpBean
//	 */
//	private void reqDelPdtPhotos(CCT_ProductBean pdtBean) {
//		if(pdtBean.getReqDelPhotosIds().length() > 0) {
//			reqDelPicsByIds(pdtBean.getReqDelPhotosIds());
//		}
//	}
//	
//	/**
//	 * 删除重复的CLP已经在服务端上的图片
//	 * @param clpBean
//	 */
//	private void reqDelCLPPhotos(CCT_CLP_Bean clpBean) {
//		if(clpBean.getReqDelPhotosIds().length() > 0) {
//			reqDelPicsByIds(clpBean.getReqDelPhotosIds());
//		}
//	}
	
	@Override
	public void showDlg_photos_pdt(int pdtType, final CCT_ProductBean pdtBean) {
		final View view = LayoutInflater.from(mActivity).inflate(R.layout.dlg_photo, null);
		ttp = (TabToPhoto) view.findViewById(R.id.ttp);		
		initTTP(ttp, pdtBean, pdtType);
		
		BottomDialog.showCustomViewDialog(mActivity, "Damage Photos", view, false, new OnSubmitClickListener() {
			@Override
			public void onSubmitClick(BottomDialog dlg, String value) {
				if (ttp.getPhotoCnt(false) > 0) reqUploadPhotos_Pdt(pdtBean, ttp);
				dlg.dismiss();
			}
		});
	}

	@Override
	public void showEditDlg(int grpPosition, int pdtType, CCT_ProductBean pdtBean) {
		mAdp.clearSeletedStatus();
		int type = mData.getType(grpPosition);
		if(type != BlindScanTLPDetailBean.Type_PDT_NO_SN) return;
		if(type == BlindScanTLPDetailBean.Type_PDT_NO_SN) {
			showEditStagingDialog(true, grpPosition, pdtBean.P_NAME, pdtBean.TOTAL, pdtBean.DAMAGEDCNT, pdtBean);
		}
	}

	@Override
	public void showDlg_onClickStatusIcon(int type, final int groupPosition, final int childPosition) {
		switch (type) {
		case BlindScanTLPDetailBean.Type_PDT_NO_SN:
			
			RewriteBuilderDialog.newSimpleDialog(mContext, "Are you sure to delete it?", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(childPosition == -1) {
						mData.removeBeanByPosition(groupPosition, -1); 
					} else {
						mData.removeBeanByPosition(groupPosition, childPosition);
					}
					mAdp = new BlindScanTLPDetailAdp(mContext, mData, BlindTLPDetailActivity.this);
					mLv.setAdapter(mAdp);
					mAdp.expandAll(mLv);
					mAdp.notifyDataSetChanged();
				}
			}).show();
			
			break;

		case BlindScanTLPDetailBean.Type_PDT_HAS_SN:
			
			RewriteBuilderDialog.newSimpleDialog(mContext, "Are you sure to delete it?", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(childPosition == -1) {
						mData.removeBeanByPosition(groupPosition, -1); 
					} else {
						mData.removeBeanByPosition(groupPosition, childPosition); 
					}
					mAdp = new BlindScanTLPDetailAdp(mContext, mData, BlindTLPDetailActivity.this);
					mLv.setAdapter(mAdp);
					mAdp.expandAll(mLv);
					mAdp.notifyDataSetChanged();
				}
			}).show();
			
			break;
			
		case BlindScanTLPDetailBean.Type_CLP:
			
			RewriteBuilderDialog.newSimpleDialog(mContext, "Are you sure to delete it?", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if(childPosition == -1) {
						mData.removeBeanByPosition(groupPosition, -1); 
					} else {
						mData.removeBeanByPosition(groupPosition, childPosition); 
					}
					mAdp = new BlindScanTLPDetailAdp(mContext, mData, BlindTLPDetailActivity.this);
					mLv.setAdapter(mAdp);
					mAdp.expandAll(mLv);
					mAdp.notifyDataSetChanged();
				}
			}).show();
			
			break;
		}
	}

	@Override
	public void showDlg_photos_tlp(CCT_TLP_Bean tlpBean) {
		// TODO Auto-generated method stub
		
	}
	
	
}
