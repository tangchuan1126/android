package oso.ui.inventory.cyclecount.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import support.common.datas.HoldDoubleValue;

public class LengthUOMKey {

	public static final int MM = 1;
	public static final int CM = 2;
	public static final int DM = 3;
	public static final int INCH = 4;

	public final static Map<String, String> lengthUOMType = new HashMap<String, String>();//任务种类
	public final static List<HoldDoubleValue<String,String>> lengthUOMList = new ArrayList<HoldDoubleValue<String,String>>();//任务种类

	static {
		lengthUOMType.put(String.valueOf(LengthUOMKey.INCH), "inch");
		lengthUOMType.put(String.valueOf(LengthUOMKey.MM), "mm");
		lengthUOMType.put(String.valueOf(LengthUOMKey.CM), "cm");
		lengthUOMType.put(String.valueOf(LengthUOMKey.DM), "dm");

		lengthUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(LengthUOMKey.INCH), "inch"));
		lengthUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(LengthUOMKey.MM), "mm"));
		lengthUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(LengthUOMKey.CM), "cm"));
		lengthUOMList.add(new HoldDoubleValue<String, String>(String.valueOf(LengthUOMKey.DM), "dm"));
	}

	public static String getLengthUOMKey(int id) {
		return (lengthUOMType.get(String.valueOf(id)));
	}

}
