package oso.ui.inventory.cyclecount;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_AreasBean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.util.TaskInfoForListViewHead;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.Utility;

/**
 * @ClassName: IvtSlcArea 
 * @Description: 
 * @author gcy
 * @date 2015-4-7 上午10:28:12
 */
public class CCT_SlcArea extends BaseActivity {

	private Cycle_Count_Tasks_Bean tBean;
	private ListView lv;
	private List<CCT_AreasBean> areas = new ArrayList<CCT_AreasBean>();
	private Button btn_submit;

	private NetConnection_CCT conn;

	public static final int IvtSlcAreaActivity = 321;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_cct_slc_area, 0);
		getFromActivityData(getIntent());
		initView();
	}
	
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	private void getFromActivityData(Intent intent) {
		tBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("Task_InventoryBean");
		setTitleString(tBean.CODE);
	}
	
	private void initView(){
		conn = new NetConnection_CCT();
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						reqInventorys();
					}
				});
		
		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);
		
		//获取ListView的头
		View head = TaskInfoForListViewHead.getLayout(mActivity, tBean);
		if(head!=null)	lv.addHeaderView(head,null,false);//添加头
		
		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adpOthers);
		TTS.getInstance().speakAll(getString(R.string.cct_select_area));
		btn_submit = (Button) findViewById(R.id.btn_submit);
		btn_submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				conn.CCT_StartTask(new NetConnectionInterface.SyncJsonHandler(mActivity) {
					@Override
					public boolean isChecked(int statusCode, Header[] headers, JSONObject json){
						return json.optInt("status",0) == 1;
					}
					@Override
					public void handReponseJson(JSONObject json) {
						tBean.STATUS = Cycle_Count_Task_Key.IN_PROGRESS;
						UIHelper.showToast(getString(R.string.sync_success));
						btn_submit.setVisibility(View.GONE);
					}
					@Override
					public void handFail(int statusCode, Header[] headers, JSONObject json) {

						if(json==null) return;
						conn.handFail(json);
					}
				},tBean);
			}
		});
		showStartBtn();
	}

	/**
	 * 判断是否显示开始任务按钮
	 */
	private void showStartBtn(){
		boolean isStartTask = (tBean.STATUS == Cycle_Count_Task_Key.IN_PROGRESS);
		btn_submit.setVisibility(isStartTask?View.GONE:View.VISIBLE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		reqInventorys();
	}
	
	// Inventory-消息
	private void reqInventorys() {
		//请求获取任务下的区域列表
		conn.CCT_TaskInstanceAreas(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == 1;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				try {
					areas = new Gson().fromJson(json.optJSONArray("DATA").toString(), new TypeToken<List<CCT_AreasBean>>() {
					}.getType());
				} catch (Exception e) {
					UIHelper.showToast(getString(R.string.sync_data_error));
					return;
				}

				sortAreaId(areas);
				Collections.sort(areas, new ComparatorArea());

				if (Utility.isNullForList(areas))
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				adpOthers.notifyDataSetChanged();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if(json==null) return;
				conn.handFail(json);
			}
		},tBean);
	}

	/**
	 * 跳转方法
	 */
	private void jumpNextAcitiviy(CCT_AreasBean t){
		if(t.STATUS>=Cycle_Count_Task_Key.Area_SCANNED){
			TTS.getInstance().speakAll_withToast(getString(R.string.cct_prompt_area_has_bean_scanned));
			return;
		}
		Intent in = new Intent();
		in.putExtra("Task_InventoryBean", (Serializable)tBean);
		in.putExtra("Area", (Serializable) t);
		in.setClass(mActivity, CCT_SlcLocation.class);
		startActivityForResult(in, IvtSlcAreaActivity);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == CCT_SlcArea.IvtSlcAreaActivity){
			tBean = (Cycle_Count_Tasks_Bean) data.getSerializableExtra("Task_InventoryBean");
			showStartBtn();
		}

	}

	// ==================nested===============================

	private BaseAdapter adpOthers = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder_Other h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.lo_cct_slc_area_item, null);
				h = new Holder_Other();
				h.area_name = (TextView) convertView.findViewById(R.id.area_name);
				h.t_processing = (TextView) convertView.findViewById(R.id.t_processing);
				h.t_total = (TextView) convertView.findViewById(R.id.t_total);
				h.t_area_status = (TextView) convertView.findViewById(R.id.t_area_status);
				h.list_icon = (View) convertView.findViewById(R.id.list_icon);
				
				convertView.setTag(h);
			} else
				h = (Holder_Other) convertView.getTag();

			// 刷新数据
			final CCT_AreasBean t = areas.get(position);
			h.area_name.setText(t.AREA_NAME);
			h.t_processing.setText(t.SCANNED_LOCATIONS+"");
			h.t_total.setText(" / "+t.TOTAL_LOCATIONS);
			boolean status = (t.STATUS>=Cycle_Count_Task_Key.Area_SCANNED);//判断是否关闭 true 为关闭

			h.list_icon.setVisibility(status?View.INVISIBLE:View.VISIBLE);
			h.t_area_status.setText(status?"Scanned":Cycle_Count_Task_Key.getAreaStatusValue(t.STATUS));
			h.t_area_status.setTextColor(mActivity.getResources().getColor(status?R.color.text_font_red:R.color.text_font_gray_light));
			
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					jumpNextAcitiviy(t);
				}
			});
			
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return areas == null ? 0 : areas.size();
		}
	};

	private class Holder_Other {
		TextView area_name;
		TextView t_processing;
		TextView t_total;
		TextView t_area_status;
		View list_icon;
	}
	
	class ComparatorArea implements Comparator<CCT_AreasBean>{

        @Override
        public int compare(CCT_AreasBean object1, CCT_AreasBean object2) {
			int m1 = 0;
			int m2 = 0;
			try{m1=object1.STATUS;}catch (Exception e){m1 = 0;}
			try{m2=object2.STATUS;}catch (Exception e){m1 = 0;}
            int result=0;
            if(m1>m2)
            {
                result=1;
            }
            if(m1<m2)
            {
                result=-1;
            }
            return result;
        }      
	}

	/**
	 * 用于区分Staging
	 * @param list
	 */
	private void sortAreaId(List<CCT_AreasBean> list){
		if(Utility.isNullForList(list)){
			return;
		}

		List<CCT_AreasBean> id0 = new ArrayList<CCT_AreasBean>();//id == 0 的集合
		List<CCT_AreasBean> idList = new ArrayList<CCT_AreasBean>();//id 》0的集合
		for(int i = 0; i< list.size();i++){
			if(list.get(i).AREA_ID==0){
				id0.add(list.get(i));
			}else{
				idList.add(list.get(i));
			}
		}
		list.clear();
		list.addAll(id0);
		list.addAll(idList);
	}
}
