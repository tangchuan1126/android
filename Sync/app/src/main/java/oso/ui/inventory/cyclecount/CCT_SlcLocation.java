package oso.ui.inventory.cyclecount;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.TransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_AreasBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.ui.inventory.cyclecount.util.ComparatorLoc;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SeacherMethod;
import oso.widget.edittext.SearchEditText;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.AllCapTransformationMethod;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: Task_InventoryAc 
 * @Description: 选择区域
 * @author gcy
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_SlcLocation extends BaseActivity {

	private Cycle_Count_Tasks_Bean tBean;
	private CCT_AreasBean area;

	private SearchEditText search_edit;
	private ListView lv;
	private List<CCT_LocationBean> locations = new ArrayList<CCT_LocationBean>();
	private List<CCT_LocationBean> locationsBackup = new ArrayList<CCT_LocationBean>();//原数据的备份
	private Button btn_submit;
	private boolean isStartTask = false;//判断是否已经开始任务了

	private NetConnection_CCT conn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lo_cct_slc_location, 0);
		getFromActivityData(getIntent());
		initView();
	}
	
	/**
	 * @Description:接收来自于上一个activity所传递过来的数据
	 */
	private void getFromActivityData(Intent intent) {
		tBean = (Cycle_Count_Tasks_Bean) intent.getSerializableExtra("Task_InventoryBean");
		area = (CCT_AreasBean) intent.getSerializableExtra("Area");
		isStartTask = (tBean.STATUS == Cycle_Count_Task_Key.IN_PROGRESS);//如果任务已经开始则为true
		setTitleString("Area:" + area.AREA_NAME);
	}
	
	private void initView(){
		conn = new NetConnection_CCT();
		showRightButton(R.drawable.btn_ref_style, "",
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						reqInventorys();
					}
				});
		
		search_edit = (SearchEditText) findViewById(R.id.search_edit);
		search_edit.setIconMode(SearchEditText.IconMode_Scan);
		
		// 全大写
		TransformationMethod trans = AllCapTransformationMethod.getThis();
		search_edit.setTransformationMethod(trans);
		
		search_edit.requestFocus();

		search_edit.setSeacherMethod(new SeacherMethod() {
			@Override
			public void seacher(String value) {
				searchData(value);
				search_edit.setText("");
				search_edit.requestFocus();
			}
		});
		
		
		// 取view
		lv = (ListView) findViewById(R.id.lv);
		View tvEmpty = findViewById(R.id.tvEmpty);
		
		// 初始化lv
		lv.setEmptyView(tvEmpty);
		lv.setAdapter(adpOthers);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				CCT_LocationBean t = locations.get(position);
				if(!isStartTask){
					UIHelper.showToast(getString(R.string.prompt_start_task));
					return;
				}
				if(t.STATUS>=Cycle_Count_Task_Key.Location_SCANNED){
					String message = "This location has been scanned.";
					TTS.getInstance().speakAll_withToast(message);
					return;
				}
				getData(t);		
			}
		});
	
		imgBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeThisActivity();
			}
		});
		TTS.getInstance().speakAll("Scan or select a location!");

		btn_submit = (Button) findViewById(R.id.btn_submit);
		btn_submit.setVisibility(isStartTask ? View.GONE : View.VISIBLE);
		btn_submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				conn.CCT_StartTask(new NetConnectionInterface.SyncJsonHandler(mActivity) {
					@Override
					public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
						return json.optInt("status", 0) == 1;
					}

					@Override
					public void handReponseJson(JSONObject json) {
						isStartTask = true;
						tBean.STATUS = Cycle_Count_Task_Key.IN_PROGRESS;
						UIHelper.showToast(getString(R.string.sync_success));
						btn_submit.setVisibility(View.GONE);
					}

					@Override
					public void handFail(int statusCode, Header[] headers, JSONObject json) {

						if(json==null) return;
						conn.handFail(json);
					}
				}, tBean);
			}
		});
	}
	/**
	 * @Description:模糊匹配搜素数据
	 */
	public void searchData(String value){
		value = value.toUpperCase(Locale.US);
		
		if(Utility.isNullForList(locationsBackup)){
			return;
		}
		locations.clear();
		int selectNum = 0;
		for (int i = 0; i < locationsBackup.size(); i++) {
			if(locationsBackup.get(i).LOCATION_NAME.indexOf(value)>=0){
				selectNum++;
				locations.add(locationsBackup.get(i));
			}
		}
		if(selectNum>0){
			adpOthers.notifyDataSetChanged();
			//-------如果只有一个并且名字相同则 跳到当前的位置
			if(locations.size()==1&&locations.get(0).LOCATION_NAME.toUpperCase(Locale.US).equals(value)){
				getData(locations.get(0));
				return;
			}
		}else{
			//search_edit.setShakeAnimation();//晃动动画
			search_edit.setText("");
			search_edit.requestFocus();
			locations.addAll(locationsBackup);
			adpOthers.notifyDataSetChanged();
			String message = "The location is not found in the list.";
			TTS.getInstance().speakAll(message);
			RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, message);
		}
	}
	
	/**
	 * @Description:跳转到指定页面
	 */
	private void getData(CCT_LocationBean t){
		if(tBean.TYPE==Cycle_Count_Task_Key.BLIND_TASK){
			doBlindTask(t);
		}else{
			doTask(t,tBean.TYPE);
		}
		search_edit.setText("");
		search_edit.requestFocus();
	}
	
	/**
	 * @Description:对照盘点
	 */
	private void doTask(final CCT_LocationBean t,int task_type){
		//请求获取区域下的位置列表
		conn.CCT_LocationData(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == 1;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				switch (tBean.TYPE) {
					case Cycle_Count_Task_Key.VERIFY_TASK://审核盘点
						doVerifyTask(t,json);
						break;
					case Cycle_Count_Task_Key.VERIFICATION_TASK://差异盘点
						doVerificationTaskOrFindTask(t,json, false);
						break;
					case Cycle_Count_Task_Key.FIND_TASK://差异盘点
						doVerificationTaskOrFindTask(t,json, true);
						break;
					default:
						break;
				}
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if(json==null) return;
				conn.handFail(json);
			}
		}, tBean, t);
	}

	/**
	 * 与系统数据进行对照盘点
	 * @param json
	 */
	private void doVerifyTask(CCT_LocationBean t,JSONObject json){
		ScanLocationDataBean location = new Gson().fromJson(json.optJSONObject("DATA").toString(), new TypeToken<ScanLocationDataBean>() {}.getType());
		//----修复数据中所有LP的真实内容
		CCT_Util.fixScanLocationDataBean(location);

		if(location!=null){
			Intent in = new Intent();
			in.putExtra("ScanLocationDataBean", (Serializable)location);
			in.putExtra("tBean", (Serializable)tBean);
			in.putExtra("LocationBean", (Serializable)t);
			in.setClass(mActivity, CCT_VerifyTask.class);
			startActivity(in);
			overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
		}
	}

	/**
	 * 差异盘点
	 * @param json
	 * @param isFindTask true为find task
	 */
	private void doVerificationTaskOrFindTask(CCT_LocationBean t,JSONObject json,boolean isFindTask){
		ScanLocationDataBean location = null;
		if(isFindTask){
			location = ScanLocationDataBean.helpJsonFindTask(json);
		}else{
			location = ScanLocationDataBean.helpJsonVerification(json);
		}

		if(location==null){
			UIHelper.showToast(getString(R.string.sync_data_error));
			return;
		}

		//----修复数据中所有LP的真实内容
		CCT_Util.fixScanLocationDataBean(location);

		if(location!=null){
			Intent in = new Intent();
			in.putExtra("ScanLocationDataBean", (Serializable)location);
			in.putExtra("tBean", (Serializable)tBean);
			in.putExtra("LocationBean", (Serializable)t);
			in.setClass(mActivity, VerificationTaskActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
		}
	}


	/**
	 * @Description:盲盘
	 */
	private void doBlindTask(CCT_LocationBean t){
		ScanLocationDataBean location = new ScanLocationDataBean();
		location.slc_area = area.AREA_ID+"";
		location.slc_position = t.LOCATION_NAME;
		location.slc_type = t.LOCATION_TYPE+"";
		location.slc_id = t.LOCATION_ID+"";
		location.ps_id = StoredData.getPs_id();

		Intent in = new Intent();
		in.putExtra("ScanLocationDataBean", (Serializable)location);
		in.putExtra("tBean", (Serializable)tBean);
		in.putExtra("LocationBean", (Serializable)t);
		in.setClass(mActivity, CCT_VerifyTask.class);
		startActivity(in);
		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
//		Intent in = new Intent();
//		in.putExtra("LocationInfo", t);
//		in.putExtra("taskBean", tBean);
//		in.setClass(mActivity, BlindScanActivity.class);
//		startActivity(in);
//		overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_out);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reqInventorys();
	}
	
	// Inventory-消息
	private void reqInventorys() {
		//请求获取区域下的位置列表
		conn.CCT_TaskInstanceAreaLocations(new NetConnectionInterface.SyncJsonHandler(mActivity) {
			@Override
			public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
				return json.optInt("status", 0) == 1;
			}

			@Override
			public void handReponseJson(JSONObject json) {
				try {
					locations = new Gson().fromJson(json.optJSONArray("DATA").toString(), new TypeToken<List<CCT_LocationBean>>() {
					}.getType());
				} catch (Exception e) {
					UIHelper.showToast(getString(R.string.sync_data_error));
					return;
				}
				locationsBackup.clear();
				locationsBackup.addAll(locations);
				Collections.sort(locations, new ComparatorLoc());
				sortLocationId(locations);

				if (Utility.isNullForList(locations))
					UIHelper.showToast(mActivity, getString(R.string.sync_nodatas));
				adpOthers.notifyDataSetChanged();
			}

			@Override
			public void handFail(int statusCode, Header[] headers, JSONObject json) {

				if(json==null) return;
				conn.handFail(json);
			}
		}, tBean, area);
	}
	
	/**
	 * @Description:重写后退方法
	 */
	@Override
	public void onBackPressed() {
		closeThisActivity();
	}
	/**
	 * @Description:关闭当前activity
	 */
	protected void closeThisActivity() {
		Intent data = new Intent();
		data.putExtra("Task_InventoryBean", (Serializable)tBean);
		setResult(CCT_SlcArea.IvtSlcAreaActivity, data);
		finish();
		overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
	}
	
	// ==================nested===============================

	private BaseAdapter adpOthers = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder_Other h;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.lo_cct_slc_location_item, null);
				h = new Holder_Other();
				h.location_name = (TextView) convertView.findViewById(R.id.location_name);
				h.status = (TextView) convertView.findViewById(R.id.t_status);
				h.list_icon = (View) convertView.findViewById(R.id.list_icon);
				
				h.close_by = (TextView) convertView.findViewById(R.id.close_by);
				h.close_user = (TextView) convertView.findViewById(R.id.close_user);
				
				convertView.setTag(h);
			} else
				h = (Holder_Other) convertView.getTag();

			// 刷新数据
			CCT_LocationBean t = locations.get(position);
			
			boolean status = (t.STATUS>=Cycle_Count_Task_Key.Location_SCANNED);//判断是否关闭 true 为关闭
			
			h.list_icon.setVisibility(status?View.INVISIBLE:View.VISIBLE);
			
			h.status.setText(status?"Scanned":"New");
			h.status.setTextColor(mActivity.getResources().getColor(status?R.color.text_font_red:R.color.text_font_gray_light));
			
			if(status&&!StringUtil.isNullOfStr(t.SCANNED_BY_NAME)){
				h.close_user.setText(t.SCANNED_BY_NAME);
				h.status.setVisibility(View.GONE);
				h.close_by.setVisibility(View.VISIBLE);
				h.close_user.setVisibility(View.VISIBLE);
			}else{
				h.status.setVisibility(View.VISIBLE);
				h.close_by.setVisibility(View.GONE);
				h.close_user.setVisibility(View.GONE);
			}
			
			
			h.location_name.setText(t.LOCATION_NAME);

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stubo
			return locations == null ? 0 : locations.size();
		}
	};

	private class Holder_Other {
		TextView location_name;
		TextView status;
		TextView close_by;
		TextView close_user;
		
		View list_icon;
	}

	/**
	 * 用于区分Staging
	 * @param list
	 */
	private void sortLocationId(List<CCT_LocationBean> list){
		if(Utility.isNullForList(list)){
			return;
		}

		List<CCT_LocationBean> id0 = new ArrayList<CCT_LocationBean>();//id == 0 的集合
		List<CCT_LocationBean> idList = new ArrayList<CCT_LocationBean>();//id 》0的集合
		for(int i = 0; i< list.size();i++){
			if(list.get(i).LOCATION_ID==0&&list.get(i).STATUS==Cycle_Count_Task_Key.Location_NEW){
				id0.add(list.get(i));
			}else{
				idList.add(list.get(i));
			}
		}
		list.clear();
		list.addAll(id0);
		list.addAll(idList);
	}

}
