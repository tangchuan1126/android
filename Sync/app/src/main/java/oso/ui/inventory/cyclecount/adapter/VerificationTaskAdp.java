package oso.ui.inventory.cyclecount.adapter;

import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 
 * @author xialimin
 * @date 2015-4-14 下午5:39:27
 * 
 * @description 审核盘点数据适配器
 */
public class VerificationTaskAdp extends BaseExpandableListAdapter {
	private Context mContext;
	private ScanLocationDataBean mData;
	
	public VerificationTaskAdp(Context _context, ScanLocationDataBean _data) {
		mContext = _context;
		mData = _data;
	}
	@Override
	public int getGroupCount() {
		return mData.getAllSizeByCLP2One();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int type = mData.getType(groupPosition);
		if(type == ScanLocationDataBean.Type_TLP) return 0;
		else if(type == ScanLocationDataBean.Type_CLP) {
			int cnt = mData.getCLPTypeBeanByGrpPosition(groupPosition).CONTAINERS.size();
			return cnt;
		}
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}
	
	/**
	 * 加载Group布局
	 * @param convertView
	 * @param holder
	 * @param grpPosition
	 * @return View
	 */
	public View inflateGrpLayout(View convertView, ViewHolder_Grp holder, int grpPosition) {
		int type = mData.getType(grpPosition);
		convertView = View.inflate(mContext, R.layout.cct_verification_grp_item, null);
		switch (type) {
		case 0: // TLP
			holder.item = convertView.findViewById(R.id.item_verification);
			holder.tlp_layout = convertView.findViewById(R.id.verification_tlp_layout);
			holder.select_tlp_condition = convertView.findViewById(R.id.verification_select_tlp_condition);
			holder.tlp_number = (TextView) convertView.findViewById(R.id.tv_verification_tlp_number);
			holder.tlp_status_icon = (ImageView) convertView.findViewById(R.id.iv_verification_tlp_status_icon);
			break;
		case 1: // CLP
			holder.clp_layout = convertView.findViewById(R.id.verification_clp_layout);
			holder.select_clp_condition = convertView.findViewById(R.id.verification_select_clp_condition);
			holder.clp_type = (TextView) convertView.findViewById(R.id.tv_verification_clp_numer);
			/*holder.clp_sku = (TextView) convertView.findViewById(R.id.tv_verification_sku);
			holder.clp_lot = (TextView) convertView.findViewById(R.id.tv_verification_lotnumber);
			holder.clp_dimension = (TextView) convertView.findViewById(R.id.tv_verification_dimension);*/
			holder.clp_status_icon = (ImageView) convertView.findViewById(R.id.iv_verification_clp_status_icon);
			holder.arrow_icon = (ImageView) convertView.findViewById(R.id.iv_verification_arrow);
			
			holder.clp_layout.findViewById(R.id.ll_verfication_options).setVisibility(View.GONE);
			break;
		}
		

		
		
		return convertView;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		ViewHolder_Grp holder;
		if (convertView == null) {
			holder = new ViewHolder_Grp();
			convertView = inflateGrpLayout(convertView, holder, groupPosition);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder_Grp) convertView.getTag();
		}
		
		fillGrpData(holder, groupPosition, isExpanded);
		return convertView;
	}

	private void fillGrpData(ViewHolder_Grp holder, int groupPosition, boolean isExpanded) {
		int type = mData.getType(groupPosition);
		boolean hasChild = false;// 判断是否有子结构
		switch (type) {
		case 0:
			CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) mData.getBeanByPosition(type, groupPosition);
			holder.tlp_number.setText(tlpBean.CONTAINER);
			holder.tlp_status_icon.setBackgroundResource(tlpBean.isDamaged?R.drawable.checkbox_pressed:R.drawable.checkbox_normal);
			holder.tlp_layout.setVisibility(type==ScanLocationDataBean.Type_TLP ? View.VISIBLE:View.GONE);
			break;
		case 1:
			CCT_CLP_Bean clpBean = (CCT_CLP_Bean) mData.getBeanByPosition(type, groupPosition);
			CCT_CLP_Type_Bean clp_type_bean = mData.getCLPTypeBeanByGrpPosition(groupPosition);
//			holder.clp_type.setText(clpBean.CONTAINER_ID);
			/*holder.clp_lot.setText(clp_type_bean.LOT_NUMBER);
			holder.clp_sku.setText(clp_type_bean.SKU);
			holder.clp_dimension.setText(clp_type_bean.DIMENSIONS);*/
//			holder.clp_status_icon.setBackgroundResource(clpBean.isDamaged?R.drawable.checkbox_pressed:R.drawable.checkbox_normal);
//			hasChild = !Utility.isNullForList(clp_type_bean.CONTAINERS);
//			holder.clp_layout.setVisibility(type==ScanLocationDataBean.Type_CLP ? View.VISIBLE:View.GONE);
			break;
		}
		
//		boolean showOne = !isExpanded || !hasChild;
//		holder.item.setBackgroundResource(showOne ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
//		holder.arrow_icon.setBackgroundResource(showOne ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
	}
	
	public View inflateChildLayout(View convertView, ViewHolder_Child holder, int grpPosition, int childPosition) {
		convertView = View.inflate(mContext, R.layout.cct_verification_clp_item, null);
		holder.item = convertView.findViewById(R.id.item);
		holder.select_clp_condition = convertView.findViewById(R.id.verification_select_clp_condition);
		holder.clp_status_icon = (ImageView) convertView.findViewById(R.id.iv_verification_clp_status_icon);
		holder.clp_number = (TextView) convertView.findViewById(R.id.tv_verification_clp_number);
		return convertView;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder_Child holder;
		if (convertView == null) {
			holder = new ViewHolder_Child();
			convertView = inflateChildLayout(convertView, holder, groupPosition, childPosition);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder_Child) convertView.getTag();
		}
		fillChildData(holder, groupPosition, childPosition);
		return convertView;
	}

	private void fillChildData(ViewHolder_Child holder, int groupPosition,int childPosition) {
		//if(ScanLocationDataBean.Type_TLP != mData.getType(groupPosition)) return;
		CCT_CLP_Type_Bean clpTypeBean = mData.container_types.get(groupPosition-mData.tlp_containers.size());
		CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(childPosition);
		holder.clp_status_icon.setBackgroundResource(clpBean.isDamaged?R.drawable.checkbox_pressed:R.drawable.checkbox_normal);
		holder.clp_number.setText(clpBean.CONTAINER);
 		boolean b = (getChildrenCount(groupPosition) - 1 == childPosition);
		holder.item.setBackgroundResource(b?R.drawable.cct_lv_item_bot_def:R.drawable.cct_lv_item_mid_def);
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	class ViewHolder_Grp {
		View item;
		// -------TLP
		View tlp_layout;
		View select_tlp_condition;
		TextView tlp_number;
		ImageView tlp_status_icon;

		// -------CLP
		View clp_layout;
		View select_clp_condition;
		TextView clp_type;
		TextView clp_sku;
		TextView clp_lot;
		TextView clp_dimension;
		ImageView clp_status_icon;
		ImageView arrow_icon;
	}
	
	class ViewHolder_Child {
		View item;
		View select_clp_condition;
		ImageView clp_status_icon;
		TextView clp_number;
		Button btnFound, btnNotFound, btnDamage;
	}
	
}

