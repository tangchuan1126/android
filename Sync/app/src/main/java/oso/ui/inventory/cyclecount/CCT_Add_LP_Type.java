package oso.ui.inventory.cyclecount;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.model.CCT_GetCreateLPTypeInfo;
import oso.ui.inventory.cyclecount.model.CCT_InnerClp;
import oso.ui.inventory.cyclecount.model.CCT_LP_Type;
import oso.ui.inventory.cyclecount.model.CCT_PackagingType;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.ui.inventory.cyclecount.util.SyncTextDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.datas.HoldThreeValue;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: CCT_VerifyTask
 * @Description: 对照盘点
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_Add_LP_Type extends BaseActivity implements View.OnClickListener {

    private TextView product_name;
    private EditText et_length_qty, et_width_qty,et_height_qty,et_lp_type_name;
    private SyncTextDialog btn_sel_packaging_tyoe, btn_sel_inner_type, btn_sel_serialized;
    private TextView t_inner_one,t_inner_two,t_note_box_number,t_note_boxtype,et_weight;
    private View layout_weight;
    private NetConnection_CCT conn;

    private CCT_GetCreateLPTypeInfo bean;
    private List<CCT_LP_Type> lp_list = new ArrayList<CCT_LP_Type>();

    public static final int resultCode = 46;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cct_add_lp_type, 0);
        getFromActivityData(getIntent());
    }

    /**
     * @Description:接收来自于上一个activity所传递过来的数据
     */
    private void getFromActivityData(Intent intent) {
        bean = (CCT_GetCreateLPTypeInfo) intent.getSerializableExtra("CCT_GetCreateLPTypeInfo");

        if (bean==null) {
            UIHelper.showToast(getString(R.string.sync_data_error));
            finish();
            return;
        }

        initView();
    }

    /**
     * @Description:初始化View
     */
    private void initView() {
        conn = NetConnection_CCT.getThis();
        setTitleString(getString(R.string.cct_create_lp_type));

//		//初始化表单信息

        product_name = (TextView) findViewById(R.id.product_name);

        et_length_qty = (EditText) findViewById(R.id.et_length_qty);
        et_width_qty = (EditText) findViewById(R.id.et_width_qty);
        et_height_qty = (EditText) findViewById(R.id.et_height_qty);
        et_weight = (TextView) findViewById(R.id.et_weight);
        layout_weight = findViewById(R.id.layout_weight);

        et_lp_type_name = (EditText) findViewById(R.id.et_lp_type_name);

        btn_sel_packaging_tyoe = (SyncTextDialog) findViewById(R.id.btn_sel_packaging_tyoe);
        btn_sel_inner_type = (SyncTextDialog) findViewById(R.id.btn_sel_inner_type);
        btn_sel_serialized = (SyncTextDialog) findViewById(R.id.btn_sel_serialized);

        et_length_qty.setTag(getString(R.string.cct_prompt_length_qty));
        et_width_qty.setTag(getString(R.string.cct_prompt_width_qty));
        et_height_qty.setTag(getString(R.string.cct_prompt_height_qty));
        et_weight.setTag(getString(R.string.cct_prompt_weight));

        btn_sel_packaging_tyoe.setTag(getString(R.string.cct_packaging_btn_packaging_type));
        btn_sel_inner_type.setTag(getString(R.string.cct_prompt_inner_type));
        btn_sel_serialized.setTag(getString(R.string.cct_prompt_serialized));

        product_name.setText(bean.product.p_name);
        setPackagingTyoe(-1);
        setInnerType();
        setSelSerialized();

        t_inner_one = (TextView) findViewById(R.id.t_inner_one);
        t_inner_two = (TextView) findViewById(R.id.t_inner_two);
        t_note_box_number = (TextView) findViewById(R.id.t_note_box_number);
        t_note_boxtype = (TextView) findViewById(R.id.t_note_boxtype);

        (findViewById(R.id.btn_submit)).setOnClickListener(this);
        (findViewById(R.id.btn_add_packaging_type)).setOnClickListener(this);

        btn_sel_packaging_tyoe.addTextChangedListener(watcher);
        btn_sel_inner_type.addTextChangedListener(watcher);
        et_length_qty.addTextChangedListener(watcher);
        et_width_qty.addTextChangedListener(watcher);
        et_height_qty.addTextChangedListener(watcher);
    }

    /**
     * 初始化lp控件
     */
    private void setPackagingTyoe(int index) {
        if (Utility.isNullForList(bean.packaging_types)) {
            return;
        }
        List<HoldThreeValue<String, String,Object>> lpList = new ArrayList<HoldThreeValue<String, String,Object>>();//title
        for (int i = 0; i < bean.packaging_types.size(); i++) {
            lpList.add(new HoldThreeValue<String, String,Object>(String.valueOf(bean.packaging_types.get(i).type_id), bean.packaging_types.get(i).type_name,bean.packaging_types.get(i)));
        }
        btn_sel_packaging_tyoe.setData(lpList, index, getString(R.string.cct_packaging_btn_packaging_type), true);
    }

    /**
     * 初始化Title控件
     */
    private void setInnerType () {

        List<HoldThreeValue<String, String,Object>> lpList = new ArrayList<HoldThreeValue<String, String,Object>>();//title

        //------因为首项需要有一个选择产品的条目 所以需要手动配置添加
        lpList.add(0,new HoldThreeValue<String, String, Object>("0","Product",null));

        if (!Utility.isNullForList(bean.inner_clps)) {
            for (int i = 0; i < bean.inner_clps.size(); i++) {
                lpList.add(new HoldThreeValue<String, String,Object>(String.valueOf(bean.inner_clps.get(i).lpt_id), bean.inner_clps.get(i).lp_name,bean.inner_clps.get(i)));
            }

        }

        btn_sel_inner_type.setData(lpList, 0, getString(R.string.cct_prompt_inner_type), true);
        btn_sel_inner_type.setIface(new SyncTextDialog.SyncTextDialogInterface() {
            @Override
            public void doSomething() {
                boolean isProduct = (btn_sel_inner_type.cutObj != null && (btn_sel_inner_type.cutObj.b).equals("Product"));
                layout_weight.setVisibility(isProduct?View.GONE:View.VISIBLE);
                t_inner_one.setText(isProduct ? "Product" : "CLP Type");
                t_inner_two.setText(isProduct ? "Product" : "CLP Type");
            }
        });
    }

    /**
     * 初始化Title控件
     */
    private void setSelSerialized () {
        List<HoldThreeValue<String, String,Object>> lpList = new ArrayList<HoldThreeValue<String, String,Object>>();//title
        lpList.add(new HoldThreeValue<String, String,Object>(String.valueOf(1), "Yes",null));
        lpList.add(new HoldThreeValue<String, String, Object>(String.valueOf(2), "No", null));
        btn_sel_serialized.setData(lpList, 0, getString(R.string.cct_prompt_serialized), true);
    }


    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey() {
        RewriteBuilderDialog.newSimpleDialog(mActivity, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("clp_types", (Serializable)lp_list);
                data.putExtra("curr_lpt_id", 0);
                setResult(resultCode, data);
                finish();
            }
        }).show();
    }

    public void toGoActivity(Context packageContext, Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(packageContext, cls);
        startActivity(intent);
        overridePendingTransition(R.anim.push_from_left_in, R.anim.push_from_left_in);
    }


    /**
     * 通用的回调方法
     * @param c
     */
    public static void toThis(final Context c,String product_id) {

        NetConnection_CCT conn = NetConnection_CCT.getThis();
        conn.CCT_GetCreateLPInfo(new NetConnectionInterface.SyncJsonHandler(c) {
            @Override
            public void handReponseJson(JSONObject json) {
                CCT_GetCreateLPTypeInfo bean = new Gson().fromJson(json.toString(), new TypeToken<CCT_GetCreateLPTypeInfo>() {
                }.getType());

                Intent intent = new Intent(c, CCT_Add_LP_Type.class);
                intent.putExtra("CCT_GetCreateLPTypeInfo", (Serializable) bean);

                ((Activity) c).startActivityForResult(intent, resultCode);
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json);
            }
        }, product_id);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_submit:
                boolean isCheck = CCT_Util.checkTextViewValue(mActivity, btn_sel_packaging_tyoe,btn_sel_inner_type,btn_sel_serialized,et_length_qty, et_width_qty, et_height_qty,et_lp_type_name);
                if(isCheck){
                    submitData(false);
                }
                break;
            case R.id.btn_add_packaging_type:
                CCT_CreatePackagingType.toThis(mActivity);
                break;

            default:
                break;
        }
    }

    /**
     * 提交方法
     */
    private void submitData(final boolean isSubmit){
        lp_list.clear();
        String string = getJsonData(isSubmit);
        if(TextUtils.isEmpty(string)){
            UIHelper.showToast(getString(R.string.sync_data_error));
            return;
        }

        conn.CCT_CLP_TypeAdd(new NetConnectionInterface.SyncJsonHandler(mActivity) {
            @Override
            public void handReponseJson(JSONObject json) {
                lp_list = new Gson().fromJson(json.optJSONArray("clp_types").toString(), new TypeToken<List<CCT_LP_Type>>() {}.getType());
                int curr_lpt_id = json.optInt("curr_lpt_id");
                Intent data = new Intent();
                data.putExtra("clp_types", (Serializable)lp_list);
                data.putExtra("curr_lpt_id", curr_lpt_id);
                setResult(resultCode, data);
                finish();
                UIHelper.showToast(getString(R.string.sync_success));
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                if(isSubmit){
                    CCT_Util.showMessageAndSpeak(json, true,CCT_Util.ERR_MESSAGE);
                }else {
                    // add by xia
                    CCT_Util.showMessageAndSpeak(json, true, CCT_Util.ERR_DATA);
                    ProcessingError(json);
                }
            }
        }, string);
    }

    private String getJsonData(boolean isSubmit){
        JSONObject json = new JSONObject();
        try{
            json.put("sku_lp_pc_id",bean.product.pc_id);//pc_id
            json.put("is_has_sn",btn_sel_serialized.key);//下拉选Yes or No
            json.put("lp_name",et_lp_type_name.getText().toString().toUpperCase(Locale.ENGLISH));//LP Type Name  ???

            json.put("sku_lp_box_type",btn_sel_inner_type.key);	//盒子类型 Inner Type的ID

            json.put("sku_lp_box_length",et_length_qty.getText().toString()); // 容器的摆放形式 输入框输入
            json.put("sku_lp_box_width",et_width_qty.getText().toString());	// 输入框输入
            json.put("sku_lp_box_height",et_height_qty.getText().toString()); // 输入框输入

            CCT_PackagingType packagingType = (CCT_PackagingType)btn_sel_packaging_tyoe.cutObj.c;
            CCT_InnerClp inner_clp = (CCT_InnerClp)btn_sel_inner_type.cutObj.c;
            if(inner_clp==null){
                inner_clp = new CCT_InnerClp();
            }
            json.put("lp_type_id",packagingType.type_id);
            json.put("length_uom",packagingType.length_uom); //Packaging Type的length_uom 选择packaging type时 动态获取
            json.put("weight_uom",packagingType.weight_uom); //Packaging Type的weight_uom 选择packaging type时 动态获取
            json.put("box_length",packagingType.length); //Packaging Type中的length
            json.put("box_width",packagingType.width); //Packaging Type中的width
            json.put("box_height",packagingType.height); //Packaging Type中的height
            json.put("box_weight",packagingType.weight); //Packaging Type中的weight
            json.put("basic_length",inner_clp.basic_length); //innertype中的 basic_length
            json.put("basic_width",inner_clp.basic_width);//innertype中的 basic_width
            json.put("basic_height",inner_clp.basic_height);//innertype中的 basic_height
            json.put("basic_weight",inner_clp.basic_weight);//innertype中的 basic_weight
            json.put("basic_length_uom",inner_clp.basic_length_uom);//innertype中的 basic_length_uom
            json.put("basic_weight_uom",inner_clp.basic_weight_uom);//innertype中的 basic_weight_uom

            json.put("pc_length",bean.product.length); //商品中的 length
            json.put("pc_width",bean.product.width);//商品中的 length
            json.put("pc_heigth",bean.product.heigth);//商品中的 length
            json.put("pc_weight",bean.product.weight);//商品中的 length

            json.put("pc_weight_uom",bean.product.weight_uom);//商品中的 length
            json.put("pc_length_uom",bean.product.length_uom);//商品中的 length

            json.put("weight",et_weight.getText().toString());//商品中的 length  ???

            json.put("isSubmit",isSubmit);

        }catch (Exception e){
            System.out.print(e.toString());
            return null;
        }
        return  json.toString();
    }

    /**
     * 处理错误消息
     */
    private void ProcessingError(JSONObject json){

        if (json == null) {
            return;
        }

        StringBuffer errStr = new StringBuffer();//非常严重的错误数据

        int num = 0;

        if(!TextUtils.isEmpty(json.optString("err_msg_inactive"))){
            num ++;
            errStr.append(num + ". "+ json.optString("err_msg_inactive") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("err_msg_match"))){
            num ++;
            errStr.append(num + ". "+ json.optString("err_msg_match") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("err_msg_box"))){
            num ++;
            errStr.append(num + ". "+ json.optString("err_msg_box") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("err_msg_product"))){
            num ++;
            errStr.append(num + ". "+ json.optString("err_msg_product") + "\n");
        }

        StringBuffer normalErrStr = new StringBuffer();

        if(!TextUtils.isEmpty(json.optString("data_length"))){
            num ++;
            normalErrStr.append(num + ". "+ json.optString("data_length") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("data_width"))){
            num ++;
            normalErrStr.append(num + ". "+ json.optString("data_width") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("data_heigth"))){
            num ++;
            normalErrStr.append(num + ". "+ json.optString("data_heigth") + "\n");
        }
        if(!TextUtils.isEmpty(json.optString("data_weight"))){
            num ++;
            normalErrStr.append(num + ". "+ json.optString("data_weight")+"\n");
        }

        if(num==0){
            return;
        }

        LinearLayout ly = new LinearLayout(mActivity);
        ViewGroup.LayoutParams lyParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        ly.setLayoutParams(lyParams);

        ly.setPadding(25, 5, 5, 5);
        ly.setOrientation(LinearLayout.VERTICAL);

        boolean isCanSubmit = true;
        if(!TextUtils.isEmpty(errStr)){
            TextView err = new TextView(mActivity);

            String string = "";
            try {
                string = TextUtils.isEmpty(normalErrStr)?errStr.toString().substring(0, errStr.toString().length()-2):errStr.toString();
            }catch (Exception e){
                string = errStr.toString();
            }

            err.setText(string);
            err.setTextColor(getResources().getColor(R.color.sync_red));
            ly.addView(err);
            isCanSubmit = false;
        }

        if(!TextUtils.isEmpty(normalErrStr)){
            TextView normal = new TextView(mActivity);
            String string = "";
            try {
                string = normalErrStr.toString().substring(0, normalErrStr.toString().length()-2);
            }catch (Exception e){
                string = normalErrStr.toString();
            }
            normal.setText(string);
            normal.setTextColor(getResources().getColor(R.color.black));
            ly.addView(normal);
        }

        RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.sync_notice));
        builder.setView(ly);
        if(isCanSubmit){
            builder.setPositiveButton(getString(R.string.sync_submit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    submitData(true);
                }
            });
        }else{
            builder.setNegativeButton(getString(R.string.sync_ok), null);
        }
        builder.setNegativeButton(R.string.sync_cancel, null);
        builder.create().show();

    }

    private void changeListener(){
//        String sku_lp_box_type = btn_sel_inner_type.key;

        int boxType = 0;
        try{
            boxType = Integer.parseInt(btn_sel_inner_type.cutObj.a);
        }catch (Exception e){}

        CCT_InnerClp inner_clp = null;
        boolean isProduct = btn_sel_inner_type.check_C_IsNull();
        if(isProduct){
            inner_clp = new CCT_InnerClp();
        }else{
            inner_clp = (CCT_InnerClp)btn_sel_inner_type.cutObj.c;
        }

        int box_total_sku = inner_clp.inner_total_pc;
        double innerClpWeight = inner_clp.total_weight;
        int innerClpWeightUom = inner_clp.weight_uom;
        int box_type_name =  inner_clp.inner_total_pc;

        CCT_PackagingType packagingType = null;
        try{
            packagingType = (CCT_PackagingType)btn_sel_packaging_tyoe.cutObj.c;
        }catch (Exception e){
            packagingType = new CCT_PackagingType();
        }

        if(btn_sel_packaging_tyoe.check_C_IsNull()){
            packagingType = new CCT_PackagingType();
        }else{
            packagingType = (CCT_PackagingType)btn_sel_packaging_tyoe.cutObj.c;
        }

        String lp_type_id =  packagingType.type_name;
        boolean isSel_lp = !TextUtils.isEmpty(lp_type_id);
        if(!isSel_lp){
            lp_type_id = btn_sel_packaging_tyoe.getHint().toString();
        }

        int sku_lp_box_length = getIntData(et_length_qty);
        int sku_lp_box_width = getIntData(et_width_qty);
        int sku_lp_box_height = getIntData(et_height_qty);

        int sl = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height;
        String lpname = "CLP"+sku_lp_box_length+"*"+sku_lp_box_width+"*"+sku_lp_box_height+"["+lp_type_id+"]";
        
        int weight_uom = packagingType.weight_uom;
        int length_uom = packagingType.length_uom;
        int pc_weight_uom = bean.product.weight_uom;
        int pc_length_uom =bean.product.length_uom;
        double pc_weight = bean.product.weight;
        double box_weight = packagingType.weight;
        double box_weighn_no = box_weight;
        int uom_pcOrClp = 0;
        double weight_pcOrClp = 1;

        if( boxType == 0 ){
            t_note_boxtype.setText(lpname+" Contains Product Qty : "+sl);
            t_note_box_number.setVisibility(View.GONE);
            uom_pcOrClp = pc_weight_uom;
            weight_pcOrClp = pc_weight;
        }else{
            t_note_box_number.setVisibility(isProduct?View.GONE:View.VISIBLE);
            lpname += "["+box_type_name+"]";
            t_note_boxtype.setText(lpname+" Contains Product Qty : "+box_total_sku*sl);
            t_note_box_number.setText(box_type_name+" Contains Product Qty : "+box_total_sku);
            uom_pcOrClp = innerClpWeightUom;
            weight_pcOrClp = innerClpWeight;

        }

        et_lp_type_name.setText(lpname);
        et_lp_type_name.setSelection(lpname.length());

        if(isSel_lp&&!isProduct){
            //weight 计算
            double weight = 0;

            if(weight_uom==1 && uom_pcOrClp==1){
                weight = weight_pcOrClp * sl + box_weighn_no;
            }
            if(weight_uom==2 && uom_pcOrClp==1){
                double	pc_weight_lbs = weight_pcOrClp * 2.2046;
                weight = pc_weight_lbs * sl+ box_weighn_no;
            }
            if(weight_uom==1 && uom_pcOrClp==2){
                double	pc_weight_kg = weight_pcOrClp  * 0.4536 ;
                weight = pc_weight_kg * sl + box_weighn_no;
            }
            if(weight_uom==2 && uom_pcOrClp==2){
                weight = weight_pcOrClp * sl + box_weighn_no/*.toFixed(3)*/;
            }
            double r = 1000; //乘积
            weight = ((int)(weight * r))/r;
            et_weight.setText(weight+"");
        }else{
            et_weight.setText("");
        }

    }

    private int getIntData(TextView et){
        int data = 0;
        try{
            data = Integer.parseInt(et.getText().toString());
        }catch (Exception e){
            return 0;
        }
        return data;
    }

    private void setIntData(EditText... et){
        if(et==null||et.length>0){
            return;
        }
        for (int i = 0; i <et.length; i++) {
            et[i].addTextChangedListener(watcher);
        }
    }

    private TextWatcher watcher = new TextWatcher(){

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,int count) {
            changeListener();
        }

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CCT_CreatePackagingType.resultCode){
            CCT_PackagingType packaging_type  = (CCT_PackagingType) data.getSerializableExtra("packaging_type");
            if(packaging_type!=null){
                bean.packaging_types.add(0,packaging_type);
                setPackagingTyoe(0);
            }
        }

    }

}
