package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import android.text.TextUtils;
/**
 * 
 * @author xialimin
 * @date 2015-4-10 下午5:15:34
 * 
 * @description 扫描TLP内的CLP或Product
 */
public class BlindScanTLPDetailBean implements Serializable {
	private static final long serialVersionUID = -2514342856373130113L;
	
	public static final int Type_CLP = 0;
	public static final int Type_PDT_HAS_SN = 1;  // is_has_sn = 1;
	public static final int Type_PDT_NO_SN = 2; // is_has_sn = 2;
	
	public List<CCT_CLP_Type_Bean> clpTypeList = new ArrayList<CCT_CLP_Type_Bean>();
	public List<CCT_ProductBean> pdtNoSnList = new ArrayList<CCT_ProductBean>();
	public List<CCT_ProductTypeBean> pdtTypeList = new ArrayList<CCT_ProductTypeBean>();
	
	/**
	 * 数据是否做过修改 没有做过修改则左上角可以直接返回。否则需要提醒
	 */
	public boolean isUpdated;
	
	/**
	 * 添加Product  返回添加的position
	 * @param pdtBean
	 * @param tlpConId
	 * @return
	 */
	public int addProduct(CCT_ProductBean pdtBean, String tlpConId) { 
		if(pdtBean == null) return -1;
		if(!TextUtils.isEmpty(tlpConId)) {
			pdtBean.comparedId = tlpConId + "/" + pdtBean.PC_ID;
		}
		
		// 根据Is_Has_Sn字段 区分是在分组的Product里还是 noSn的ProductList中    is_has_sn = 1 存在SN  应该在分组中
		if(pdtBean.IS_HAS_SN == 1) {

			//===========需要按照PC_ID 进行分组
			long pc_id = pdtBean.PC_ID;
			// 是否插入成功
			boolean isInserted = false;
			int typePosition = 0;
			int insertedPos = 0; //插入的位置
			for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
				for(CCT_ProductBean currPdtBean : pdtTypeBean.pdtHasSNList) {
					if(pc_id==currPdtBean.PC_ID) {
						isInserted = true;
						break;
					}
					
					if(!isInserted) {
						insertedPos ++;
					}
				}
				
				if(isInserted) break;
				else typePosition++;
			}
			
			if(isInserted) {
				pdtTypeList.get(typePosition).pdtHasSNList.add(pdtBean);
				// 返回当前插入的position
				if(clpTypeList.size()>0) {
					return clpTypeList.size() - 1 + insertedPos;
				} else {
					return insertedPos;
				}
			}
			
			// 判断是否已经根据type_id插入到PDT pdtHasSNList中， 如果没有则需要添加一个新的
			if(!isInserted) {
				CCT_ProductTypeBean newPdtType = new CCT_ProductTypeBean();
				newPdtType.TYPE_ID = pc_id;
				newPdtType.P_Name = pdtBean.P_NAME;
				newPdtType.pdtHasSNList.add(pdtBean);
				pdtTypeList.add(newPdtType);
			}
			
			// 新的pdtType  则返回当前pdtType的位置
			if(clpTypeList.size() == 0) {
				return pdtTypeList.size() - 1;
			} else {
				return clpTypeList.size() + pdtTypeList.size() -1;
			}
		}
		
		// IS_HAS_SN == 2的 存入到NoSNProductList中去
		pdtNoSnList.add(pdtBean);
		return getEndPosition();
	}
	
	
	/**
	 * 获得最后的position
	 * @return
	 */
	public int getEndPosition() {
		int index = -1;
		int clpTypeSize = clpTypeList.size();
		int pdtTypeSize = pdtTypeList.size();
		int pdtNoSNSize = pdtNoSnList.size();
		
		if(clpTypeSize>0 && pdtTypeSize>0 && pdtNoSNSize>0) {
			return clpTypeSize + pdtTypeSize + pdtNoSNSize -1;
		}
		if(clpTypeSize==0 && pdtTypeSize==0 && pdtNoSNSize==0) {
			return -1;
		}
		if(clpTypeSize==0 && pdtTypeSize==0 && pdtNoSNSize>0) {
			return pdtNoSNSize - 1;
		}
		if(clpTypeSize==0 && pdtTypeSize>0 && pdtNoSNSize==0) {
			return pdtNoSNSize - 1;
		}
		if(clpTypeSize==0 && pdtTypeSize>0 && pdtNoSNSize>0) {
			return pdtTypeSize + pdtNoSNSize - 1;
		}
		if(clpTypeSize>0 && pdtTypeSize==0 && pdtNoSNSize==0) {
			return clpTypeSize -1;
		}
		if(clpTypeSize>0 && pdtTypeSize==0 && pdtNoSNSize>0) {
			return clpTypeSize + pdtNoSNSize -1;
		}
		if(clpTypeSize>0 && pdtTypeSize>0 && pdtNoSNSize>0) {
			return clpTypeSize + pdtTypeSize -1;
		}
		return index;
	}

	/**
	 * 添加TLP !!! 必须要有clpBean.TYPE_ID!!
	 * @param clpBean
	 * @return 当前插入对应二级列表的Group position
	 */
	public int addCLP(CCT_CLP_Bean clpBean) {
		// 获得CLP的  typeId
		int type_id = clpBean.TYPE_ID;
		if(type_id==0) return 0;
		//---------------------------------------------------------
		
		// 是否插入成功
		boolean isInserted = false;
		int typePosition = 0;
		for (CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			if (clpTypeBean.CONTAINER_TYPE_ID == type_id ) {
				isInserted = true;
				break;
			}
			
			if(isInserted) break;
			else {
				typePosition++;
			}
		}
		
		if(isInserted) {
			clpTypeList.get(typePosition).CONTAINERS.add(clpBean);
			//-----插入成功
			return typePosition;
		}
		
		// 判断是否已经根据type_id插入到CLP CONTAINERS中， 如果没有则需要添加一个新的
		if(!isInserted) {
			CCT_CLP_Type_Bean newClpType = new CCT_CLP_Type_Bean();
			newClpType.CONTAINER_TYPE_ID = type_id;
			newClpType.LP_TYPE_NAME = clpBean.TYPE_NAME;
			newClpType.containerType = Cycle_Count_Task_Key.CCT_CLP;
			newClpType.CONTAINERS.add(clpBean);
			clpTypeList.add(newClpType);
			//-----插入成功
			return clpTypeList.size() - 1;
		}
		return -1;
	}
	
	
	
	/**
	 * 根据grpPosition 和 childPosition设置对应条目的选中状态
	 * @param grpPos
	 * @param childPos
	 */
	public void setSelectedByPosition(int grpPos, int childPos) {
		switch (getType(grpPos)) {
		case Type_CLP:
			CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) getBeanByGrpPosition(grpPos);
			clpTypeBean.CONTAINERS.get(childPos).itemIsSeleted = true;
			break;

		case Type_PDT_HAS_SN:
			CCT_ProductTypeBean pdtTypeBean = (CCT_ProductTypeBean) getBeanByGrpPosition(grpPos);
			pdtTypeBean.pdtHasSNList.get(childPos).itemIsSeleted = true;
			break;
			
		case Type_PDT_NO_SN:
			CCT_ProductBean pdtBean = (CCT_ProductBean) getBeanByGrpPosition(grpPos);
			pdtBean.itemIsSeleted = true;
			break;
		}
	}
	
	
	public boolean removeBean(Object key) {
		if(key == null) return false;
		
		// 情况1 删除CLPType组
		// 情况2 删除CLP
		//-----------情况1  clp组---------------
		if(key instanceof CCT_CLP_Type_Bean) {
			key = (CCT_CLP_Type_Bean) key;
			
			//---删除
			clpTypeList.remove(key);
		}
		//-----------情况2  clp 子---------------
		if(key instanceof CCT_CLP_Bean) {
			boolean isFound = false;
			int index_i = 0;
			int index_j = 0;
			key = (CCT_CLP_Bean) key;
			
			for(int i=0; i<clpTypeList.size(); i++) {
				CCT_CLP_Type_Bean clpTypeBean = clpTypeList.get(i);
				for(int j=0; j<clpTypeBean.CONTAINERS.size(); j++) {
					if(((CCT_CLP_Bean) key).REAL_NUMBER.equals(clpTypeBean.CONTAINERS.get(j).REAL_NUMBER)) {
						isFound = true;
						index_j = j;
						index_i = i;
						break;
					}
				}
			}
			
			if(isFound) {
				clpTypeList.get(index_i).CONTAINERS.remove(index_j);
				if(clpTypeList.get(index_i).CONTAINERS.size() == 0) {
					clpTypeList.remove(index_i);
					return isFound;
				}
			}
		}
		
		
		// 情况3 删除pdtType组
		if(key instanceof CCT_ProductTypeBean) {
			key = (CCT_ProductTypeBean) key;
			
			//---删除
			pdtTypeList.remove(key);
		}
		
		// 情况4 删除pdtHasSN
		if(key instanceof CCT_ProductBean) {
			key = (CCT_ProductBean) key;
			boolean isFound = false;
			int index_i = 0;
			int index_j = 0;
			
			for(int i = 0; i<pdtTypeList.size(); i++) {
				CCT_ProductTypeBean pdtTypeBean = pdtTypeList.get(i);
				for(int j=0; j<pdtTypeBean.pdtHasSNList.size(); j++) {
					String currSn = pdtTypeBean.pdtHasSNList.get(j).SN_BLIND_SINGLE;
					if(TextUtils.isEmpty(((CCT_ProductBean) key).SN_BLIND_SINGLE)) continue;
					if(((CCT_ProductBean) key).SN_BLIND_SINGLE.equals(currSn)) {
						isFound = true;
						index_i = i;
						index_j = j;
					}
				}
			}
			
			
			if(isFound) {
				pdtTypeList.get(index_i).pdtHasSNList.remove(index_j);
				// 如果当前pdtType组长度为0 则删除组
				if(pdtTypeList.get(index_i).pdtHasSNList.size() == 0) {
					pdtTypeList.remove(index_i);
				}
			}
		}
		
		
		// 情况5 删除pdtNoSn
		if(key instanceof CCT_ProductBean) {
			String sn = ((CCT_ProductBean) key).SN_BLIND_SINGLE;
			if(TextUtils.isEmpty(sn)) {
				return pdtNoSnList.remove(key);
			}
		}
		
		
		return false;
	}
	
	
	/**
	 * 获得重复的Product position
	 * @param modelLot  DWM55F2Y1/lot123456
	 * @return
	 */
	public int getPdtRepeatOldPosition(String modelLot) {
		if(TextUtils.isEmpty(modelLot)) return -1;
		for(int index=0; index<pdtNoSnList.size(); index++) {
			if(modelLot.equalsIgnoreCase(pdtNoSnList.get(index).modelNo+"/"+pdtNoSnList.get(index).lotNo)  
					|| modelLot.equalsIgnoreCase(pdtNoSnList.get(index).P_NAME ) || modelLot.equalsIgnoreCase(pdtNoSnList.get(index).modelNo)) {
				return clpTypeList.size()+ pdtTypeList.size() + index;
			}
		}
		return -1;
	}
	
	
	/**
	 * 获得重复的Product bean
	 * @param value
	 * @return
	 */
	public CCT_ProductBean getRepeatPdtBean(String value) {
		if(TextUtils.isEmpty(value)) return new CCT_ProductBean();
		for(int index=0; index<pdtNoSnList.size(); index++) {
			if(value.equalsIgnoreCase(pdtNoSnList.get(index).modelNo)  || value.equalsIgnoreCase(pdtNoSnList.get(index).P_NAME) ) return pdtNoSnList.get(index);
		}
		return new CCT_ProductBean();
	}
	
	/**
	 * 获得PdtWithSn Bean
	 * @param sn
	 * @return
	 */
	public CCT_ProductBean getRepeatPdtSnBean(String sn) {
		if(TextUtils.isEmpty(sn)) return new CCT_ProductBean();
		
		for(CCT_ProductTypeBean pdtType : pdtTypeList) {
			for(CCT_ProductBean pdtBean : pdtType.pdtHasSNList) {
				if(sn.equals(pdtBean.SN_BLIND_SINGLE)) return pdtBean;
			}
		}
		
		return new CCT_ProductBean();
	}
	
	
	/**
	 * 获得当前grpPosition的bean类型
	 * @param position
	 * @return type
	 */
	public int getType(int grpPosition) {
		
		//-------------是Group CLP
		if(grpPosition < clpTypeList.size()) {
			return Type_CLP;
		} 
		
		//-------------是Group PdtType
		else if(grpPosition < clpTypeList.size() + pdtTypeList.size()) {
			return Type_PDT_HAS_SN;
		} 
		
		//-------------是Group PdtNo
		else {
			return Type_PDT_NO_SN;
		}
		
	}
	
	/**
	 * @return 获得一级列表的数据个数
	 */
	public int getGroupsSize() {
		return clpTypeList.size() + pdtNoSnList.size() + pdtTypeList.size();
	}
	
	/**
	 * 返回有SN的product个数, 即通过扫描SN录入的product
	 * @return count
	 */
	public int getProductWithSnCount() {
		int cnt = 0;
		for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
			for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
				if(TextUtils.isEmpty(pdtBean.SN_BLIND_SINGLE)) cnt++;
			}
		}
		return cnt;
	}
	
	/**
	 * 返回有SN的product个数, 即product没有SN号，而通过SKU号录入的
	 * @return count
	 */
	public int getProductWithSkuCount() {
		int cnt = 0;
		for(CCT_ProductBean pdtBean : pdtNoSnList) {
			if(TextUtils.isEmpty(pdtBean.P_NAME)) cnt++;
		}
		return cnt;
	}
	
	
	
	/**
	 * 获得子条目的数据bean
	 * @param grpPosition
	 * @param childPosition
	 * @return
	 */
	public Object getChildBeanByPosition(int grpPosition, int childPosition) {
		//-------------是Group CLP
		if(grpPosition < clpTypeList.size()) {
			return clpTypeList.get(grpPosition).CONTAINERS.get(childPosition);
		} 
		
		//-------------是Group PdtType
		else {
			int pos = grpPosition - clpTypeList.size();
			return pdtTypeList.get(pos).pdtHasSNList.get(childPosition);
		}
		
	}
	
	/**
	 * 通过groupPosition 返回Group级的指定GroupPosition的对应数据
	 * @param grpPosition
	 * @return
	 */
	public Object getBeanByGrpPosition(int grpPosition) {
		//-------------是Group CLP
		if(grpPosition < clpTypeList.size()) {
			return clpTypeList.get(grpPosition);
		} 
		
		//-------------是Group PdtType
		else if(grpPosition < clpTypeList.size() + pdtTypeList.size()) {
			int pos = grpPosition - clpTypeList.size();
			return pdtTypeList.get(pos);
		} 
		
		//-------------是Group PdtNo
		else {
			int pos = grpPosition - clpTypeList.size() - pdtTypeList.size();
			return pdtNoSnList.get(pos);
		}
	}
	/**
	 * 删除
	 * @param deletePosition
	 */
	public void removeBeanByPosition(int grpPosition, int deleteChildPosition) {
		removeBean(getBeanByPosition(grpPosition, deleteChildPosition));
	}
	
	public Object getBeanByPosition(int grpPosition, int deleteChildPosition) {
		boolean isSearchGroup = (deleteChildPosition == -1);
		switch (getType(grpPosition)) {
		case Type_CLP:
			if(isSearchGroup) {
				return clpTypeList.get(grpPosition);
			} else {
				return clpTypeList.get(grpPosition).CONTAINERS.get(deleteChildPosition);
			}
			
		case Type_PDT_HAS_SN:
			if(isSearchGroup) {
				return pdtTypeList.get(grpPosition - clpTypeList.size());
			} else {
				return pdtTypeList.get(grpPosition - clpTypeList.size()).pdtHasSNList.get(deleteChildPosition);
			}
			
		case Type_PDT_NO_SN:
			return pdtNoSnList.get(grpPosition - clpTypeList.size() - pdtTypeList.size());
		}
		return null;
	}
	
	/**
	 * 设置指定Position的CLP的damage的状态
	 * @param isDamaged
	 * @param position
	 * @return boolean
	 */
	public boolean setDamagedByPosition(int type, boolean isDamaged, int grpPosition, int childPosition) {
		switch (getType(grpPosition)) {
		case Type_CLP:
			clpTypeList.get(grpPosition).CONTAINERS.get(childPosition).isDamaged = isDamaged;
			break;

		case Type_PDT_HAS_SN:
			pdtTypeList.get(grpPosition - clpTypeList.size()).pdtHasSNList.get(childPosition).isDamaged = isDamaged;
			break;
			
		case Type_PDT_NO_SN:
			int diffSize = clpTypeList.size() + pdtTypeList.size();
			pdtNoSnList.get(grpPosition - diffSize).isDamaged = isDamaged; 
			break;
		}
		
		return isDamaged;
	}
	
	
	/**
	 * 根据container_id判断是否重复
	 * @param realContainerNo
	 * @return
	 */
	public boolean isContainCLPById(String realContainerNo) {
		if(TextUtils.isEmpty(realContainerNo)) return true;
		if(!CCT_Util.isLPFixed(realContainerNo)) {
			realContainerNo = CCT_Util.fixLP_Number(realContainerNo);
		}
		
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(realContainerNo.equals(clpBean.REAL_NUMBER)) return true;
			}
		}
		return false;
	}
	
	/**
	 * 根据sn判断是否重复
	 * @param sn
	 * @return
	 */
	public boolean isContainPdtBySn(String sn) {
		if(TextUtils.isEmpty(sn)) return true;
		
		for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
			for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
				if(sn.equals(pdtBean.SN_BLIND_SINGLE)) return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 根据sku判断是否重复
	 * @param modelLot
	 * @return
	 */
	public boolean isContainPdtByModelNo(String pc_id) {
		if(TextUtils.isEmpty(pc_id)) return true;
		
		for(CCT_ProductBean pdtBean : pdtNoSnList) {
			String curr_pc_id = pdtBean.PC_ID+"";
			if(pc_id.equals(curr_pc_id)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获得重复的Product Bean
	 * @param modelLotNo
	 * @return
	 */
	public CCT_ProductBean getContainPdtWithSKU(String modelLotNo) {
		if(TextUtils.isEmpty(modelLotNo)) return new CCT_ProductBean();  
		
		for(CCT_ProductBean pdtBean : pdtNoSnList) {
			String modelAndLotNo = pdtBean.modelNo+"/"+pdtBean.lotNo;
			if(modelLotNo.equals(modelAndLotNo)) return pdtBean;
		}
		return new CCT_ProductBean(); 
	}

	/**
	 * 获得当前groupPosition的子条目个数
	 * @param groupPosition
	 * @return
	 */
	public int getChildrenCount(int groupPosition) {
		//-------------是Group CLP
		if(groupPosition < clpTypeList.size()) {
			return clpTypeList.get(groupPosition).CONTAINERS.size();
		} 
		
		else if(groupPosition < clpTypeList.size() + pdtTypeList.size()) {
			int position = groupPosition - clpTypeList.size();
			return pdtTypeList.get(position).pdtHasSNList.size();
		} 
		
		else if(groupPosition < clpTypeList.size() + pdtTypeList.size() + pdtNoSnList.size()) {
			return 0;
		}
		
		return 0;
	}
	
}
