package oso.ui.inventory.cyclecount.adapter;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import declare.com.vvme.R;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_Task_Type_Bean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;

/**
 * CycleCount MyTask任务列表适配器  分组
 * @author xialimin
 *
 */
public class CCT_TaskTypeAdp extends BaseExpandableListAdapter {

	private static final int INDEX_CYCLE_COUNT_TASKS = 0;

	private static final int INDEX_VERIFICATION_TASKS = 1;

	private static final int INDEX_FIND = 2;

	private Context mContext;

	private CCT_Task_Type_Bean mTypeBean;

	public CCT_TaskTypeAdp(Context ctx, CCT_Task_Type_Bean taskTypeBean) {
		mContext = ctx;
		mTypeBean = taskTypeBean;
	}
	
	@Override
	public int getGroupCount() {
		if (mTypeBean == null) return 0;
		return mTypeBean.allData.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (mTypeBean == null) return 0;
		CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask = mTypeBean.allData.get(groupPosition);

		return cycleCountArrTask.taskTypeArr.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initGroupView(groupPosition, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillGroupData(isExpanded, groupPosition, holder);
		setGroupListener(groupPosition, holder);
		return convertView;
	}
	
	private View initGroupView(int groupPosition, View convertView, ViewHolder holder) {
		convertView = View.inflate(mContext, R.layout.lo_cct_mytask_group, null);

		holder.item = convertView.findViewById(R.id.item_cct_task_group);
		holder.tvGrpName = (TextView) convertView.findViewById(R.id.tv_cct_mytask_grpname);
		return convertView;
	}
	
	private void fillGroupData(boolean isExpanded, int groupPosition, ViewHolder holder) {
		CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask = mTypeBean.allData.get(groupPosition);

		holder.tvGrpName.setText(getGroupName(cycleCountArrTask));

		// 关闭|无条目时,显示圆背景
		boolean showOne = !isExpanded || cycleCountArrTask.taskTypeArr.size() == 0;
		holder.item.setBackgroundResource(showOne ? R.drawable.sh_load_lv_one : R.drawable.sh_load_lv_top);
	}


	private void setGroupListener(int groupPosition, ViewHolder holder) {
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = View.inflate(mContext, R.layout.cycle_count_task_layout_item, null);
			holder = new ViewHolder();

			holder.item = convertView.findViewById(R.id.item_mytask_child);
			holder.task_name = (TextView) convertView.findViewById(R.id.task_name);
			holder.t_priority = (TextView) convertView.findViewById(R.id.t_priority);

			holder.task_type = (TextView) convertView.findViewById(R.id.task_type);
			holder.task_status = (TextView) convertView.findViewById(R.id.task_status);
			holder.task_start_time = (TextView) convertView.findViewById(R.id.task_start_time);
			holder.task_end_time = (TextView) convertView.findViewById(R.id.task_end_time);

			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();


		fillChildData(groupPosition, childPosition, holder);
		setChildListener(groupPosition, childPosition, holder);
		return convertView;
	}

	private void setChildListener(final int groupPosition, final int childPosition, ViewHolder holder) {
	}

	private View initChildView(int groupPosition, View convertView, ViewHolder holder) {
		convertView = View.inflate(mContext, R.layout.wms_special_project_maintask_child_item, null);
		holder.item = convertView.findViewById(R.id.item_sp_child);
		return convertView;
	}
	
	private void fillChildData(int groupPosition, int childPosition, ViewHolder holder) {

		// 刷新数据
		CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask = mTypeBean.allData.get(groupPosition);
		Cycle_Count_Tasks_Bean t = cycleCountArrTask.taskTypeArr.get(childPosition);


		holder.t_priority.setText((t.PRIORITY==0)?"":Cycle_Count_Task_Key.getPriorityValue(t.PRIORITY));
		holder.t_priority.setTextColor(Cycle_Count_Task_Key.getPriorityColor(t.PRIORITY));
		holder.task_name.setText(t.CODE);

		holder.task_type.setText((t.TYPE==0)?"":Cycle_Count_Task_Key.getTaskTypeValue(t.TYPE));
		holder.task_status.setText((t.STATUS==0)?"":Cycle_Count_Task_Key.getTaskStatusValue(t.STATUS));
		holder.task_start_time.setText(t.START_DATE);
		holder.task_end_time.setText(t.END_DATE);

		// 背景
		if (getChildrenCount(groupPosition) - 1 == childPosition) {
			holder.item.setBackgroundResource(R.drawable.sel_special_project_bottom);
		} else {
			holder.item.setBackgroundResource(R.drawable.sel_special_project_mid);
		}
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----打开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }


	/**
	 * 获得对应GroupName
	 * @param cycleCountArrTask
	 * @return
	 */
	private String getGroupName(CCT_Task_Type_Bean.CycleCountArrTask cycleCountArrTask) {
		Cycle_Count_Tasks_Bean cycleCountTask = cycleCountArrTask.taskTypeArr.get(0);
		if (cycleCountTask.TYPE == 1 || cycleCountTask.TYPE == 2) {
			return Cycle_Count_Task_Key.TITLE_CYCLE_COUNT_TASK;
		}
		return Cycle_Count_Task_Key.getTaskTypeValue(cycleCountTask.TYPE);
	}

	private static class ViewHolder {
		
		View item;
		//-------Group--------
		TextView tvGrpName;
		
		//-------Child--------

		TextView task_name;
		TextView t_priority;
		TextView task_type;
		TextView task_status;
		TextView task_start_time;
		TextView task_end_time;
	}
	
}
