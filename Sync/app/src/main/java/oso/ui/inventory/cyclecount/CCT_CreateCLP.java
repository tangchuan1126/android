package oso.ui.inventory.cyclecount;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import declare.com.vvme.R;
import oso.base.BaseActivity;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_GetCreateCLP_Info;
import oso.ui.inventory.cyclecount.model.CCT_LP_Type;
import oso.ui.inventory.cyclecount.model.CCT_Product;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_TitleAndCustomerInfo;
import oso.ui.inventory.cyclecount.model.ScanLocationDataBean;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.ui.inventory.cyclecount.util.SyncTextDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.common.datas.HoldThreeValue;
import support.common.print.PrintTool;
import support.dbhelper.StoredData;
import support.network.NetConnectionInterface;
import support.network.NetConnection_CCT;
import utility.AllCapTransformationMethod;
import utility.StringUtil;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: CCT_VerifyTask
 * @Description: 对照盘点
 * @date 2015-2-27 下午5:23:45
 */
public class CCT_CreateCLP extends BaseActivity implements View.OnClickListener,View.OnFocusChangeListener {

    private EditText et_lot, et_rfid;
    private SyncTextDialog btn_sel_lp_tyoe, btn_sel_title, btn_sel_customer;
    private Button btn_submit, btn_print;
    private LinearLayout scan_sn_layout;
    private List<EditText> listView;//放置SN控件的序列

    // 全大写
    private TransformationMethod trans = AllCapTransformationMethod.getThis();

    private View show_layout,no_sn_layout,has_sn_layout;
    private TextView et_total,t_scan_num,t_total;

    Handler handler = new Handler();

    private NetConnection_CCT conn;
    private CCT_GetCreateCLP_Info bean;

    private CCT_Product product;
    private ScanLocationDataBean sBean;
    private String create_code;
    private CCT_TLP_Bean tlp;

    private CCT_CLP_Bean clp_bean;

    public static final int resultCode = 44;

    private int cutIsHasSn = Cycle_Count_Task_Key.CCT_P_HAS_SN;
    private boolean isDamaged = false;

    private int total_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cct_create_clp, 0);
        getFromActivityData(getIntent());
    }

    /**
     * @Description:接收来自于上一个activity所传递过来的数据
     */
    private void getFromActivityData(Intent intent) {
        bean = (CCT_GetCreateCLP_Info)intent.getSerializableExtra("CCT_GetCreateCLP_Info");
        sBean = (ScanLocationDataBean) intent.getSerializableExtra("ScanLocationDataBean");
        product = (CCT_Product)intent.getSerializableExtra("product");
        tlp = (CCT_TLP_Bean)intent.getSerializableExtra("CCT_TLP_Bean");
        create_code = intent.getStringExtra("create_code");
        setTitleString(getString(R.string.cct_create_clp));
        initView();
    }

    /**
     * @Description:初始化View
     */
    private void initView() {
        conn = NetConnection_CCT.getThis();

        // 初始化表单信息
        et_lot = (EditText) findViewById(R.id.et_lot);
        et_rfid = (EditText) findViewById(R.id.et_rfid);

        scan_sn_layout = (LinearLayout)findViewById(R.id.scan_sn_layout);

        show_layout = findViewById(R.id.show_layout);
        no_sn_layout = findViewById(R.id.no_sn_layout);
        has_sn_layout = findViewById(R.id.has_sn_layout);
        et_total = (TextView)findViewById(R.id.et_total);
        t_scan_num = (TextView)findViewById(R.id.t_scan_num);
        t_total = (TextView)findViewById(R.id.t_total);

        btn_sel_lp_tyoe = (SyncTextDialog) findViewById(R.id.btn_sel_lp_tyoe);
        btn_sel_title = (SyncTextDialog) findViewById(R.id.btn_sel_title);
        btn_sel_customer = (SyncTextDialog) findViewById(R.id.btn_sel_customer);


        et_rfid.setTransformationMethod(trans);
        et_lot.setTransformationMethod(trans);

        btn_sel_lp_tyoe.setTag(getString(R.string.cct_packaging_btn_lp_type));
        btn_sel_title.setTag(getString(R.string.cct_packaging_btn_title));
        btn_sel_title.setShowSearchView(true);
        btn_sel_customer.setTag(getString(R.string.cct_packaging_btn_customer));
        btn_sel_customer.setShowSearchView(true);

        setSelLpTyoe(-1);
        setSelTitle();
        getCustomerList();

        (findViewById(R.id.btn_add_lp)).setOnClickListener(this);
        (findViewById(R.id.btn_add_sn)).setOnClickListener(this);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(this);

        setAddSnLayout(false);
        int a = ((ScrollView) findViewById(R.id.scrollview)).getScrollY();
        ((ScrollView) findViewById(R.id.scrollview)).fullScroll(View.FOCUS_DOWN);
    }


    /**
     * 初始化lp控件
     */
    private void setSelLpTyoe(int index) {
        if (Utility.isNullForList(bean.clp_types)) {
            return;
        }
        List<HoldThreeValue<String, String, Object>> lpList = new ArrayList<HoldThreeValue<String, String, Object>>();//title
        for (int i = 0; i < bean.clp_types.size(); i++) {
            lpList.add(new HoldThreeValue<String, String, Object>(String.valueOf(bean.clp_types.get(i).lpt_id), bean.clp_types.get(i).lp_name,bean.clp_types.get(i)));
        }
        btn_sel_lp_tyoe.setData(lpList, index, getString(R.string.cct_packaging_btn_lp_type), true);
        btn_sel_lp_tyoe.setIface(new SyncTextDialog.SyncTextDialogInterface() {
            @Override
            public void doSomething() {
                if (btn_sel_lp_tyoe.cutObj != null && ((CCT_LP_Type) btn_sel_lp_tyoe.cutObj.c) != null) {
                    CCT_LP_Type cct_lp_type = (CCT_LP_Type)btn_sel_lp_tyoe.cutObj.c;
                    cutIsHasSn = cct_lp_type.is_has_sn;
                    //当前CLP可以装多少个商品
                    total_num = cct_lp_type.stack_width_qty * cct_lp_type.stack_height_qty * cct_lp_type.stack_length_qty;

                    boolean isHasSn = (cutIsHasSn == Cycle_Count_Task_Key.CCT_P_HAS_SN);
                    //判断是显示View
                    show_layout.setVisibility(View.VISIBLE);
                    has_sn_layout.setVisibility(isHasSn ? View.VISIBLE : View.GONE);
                    no_sn_layout.setVisibility(!isHasSn ? View.VISIBLE : View.GONE);
                    //判断有没有sn的操作
                    if (!isHasSn) {
                        et_total.setText(total_num+"");
                    }else{
                        scan_sn_layout.removeAllViews();//清空显示的View
                        listView.clear();
                        t_scan_num.setText(getScanSN_Num()+"");
                        t_total.setText(total_num+"");
                        setAddSnLayout(false);
                    }
                }
            }
        });
    }

    /**
     * 初始化Title控件
     */
    private void setSelTitle() {
        CCT_TitleAndCustomerInfo.Title title = null;
        if(tlp!=null&&tlp.TITLE_ID!=0){
            for (int i = 0; i < bean.titles.size(); i++) {
                if((tlp.TITLE_ID+"").equals(bean.titles.get(i).id)){
                    title = bean.titles.get(i);
                }
            }
        }

        List<HoldDoubleValue<String, String>> titleList = new ArrayList<HoldDoubleValue<String, String>>();//title
        if(title!=null){
            titleList.add(SyncTextDialog.getHoldDoubleValue(title));
            btn_sel_title.setDoubleData(titleList, 0, getString(R.string.cct_packaging_btn_title), false);
            btn_sel_title.setOnClickListener(null);
        }
        else{
            for (int i = 0; i < bean.titles.size(); i++) {
                titleList.add(SyncTextDialog.getHoldDoubleValue(bean.titles.get(i)));
            }
            btn_sel_title.setDoubleData(titleList, getString(R.string.cct_packaging_btn_title));
        }
    }


    /**
     * 初始化Customer控件
     */
    private void getCustomerList() {
        List<HoldDoubleValue<String, String>> customerList = new ArrayList<HoldDoubleValue<String, String>>();//customer
        for (int i = 0; i < bean.customers.size(); i++) {
            customerList.add(SyncTextDialog.getHoldDoubleValue(bean.customers.get(i)));
        }
        btn_sel_customer.setDoubleData(customerList, getString(R.string.cct_packaging_btn_customer));
    }


    /**
     * @Description:关闭当前页面
     */
    protected void onBackBtnOrKey() {
        RewriteBuilderDialog.newSimpleDialog(mActivity, getString(R.string.sync_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("clp", clp_bean);
                setResult(resultCode, data);
                mActivity.finish();
                overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            }
        }).show();
    }

    /**
     * 通用的回调方法
     * @param c
     * @param product
     */
    public static void toThis(final Context c, final CCT_Product product,final String create_code,final ScanLocationDataBean sBean,final CCT_TLP_Bean tlp) {

        NetConnection_CCT conn = NetConnection_CCT.getThis();
        conn.CCT_GetCreateCLPInfo(new NetConnectionInterface.SyncJsonHandler(c) {
            @Override
            public void handReponseJson(JSONObject json) {
                CCT_GetCreateCLP_Info bean = null;
                try {
                    bean = new Gson().fromJson(json.toString(), new TypeToken<CCT_GetCreateCLP_Info>() {
                    }.getType());
                } catch (Exception e) {
                    UIHelper.showToast(c.getString(R.string.sync_data_error));
                    return;
                }

                Intent intent = new Intent(c, CCT_CreateCLP.class);
                intent.putExtra("create_code", create_code);
                intent.putExtra("CCT_GetCreateCLP_Info", bean);
                intent.putExtra("ScanLocationDataBean",  sBean);
                intent.putExtra("CCT_TLP_Bean",tlp);
                intent.putExtra("product", product);

                ((Activity) c).startActivityForResult(intent, resultCode);
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json);
            }
        }, product.pc_id + "");
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_add_lp:
                CCT_Add_LP_Type.toThis(mActivity,product.pc_id+"");
                break;
            case R.id.btn_add_sn:
                setAddSnLayout(true);
                break;
            case R.id.btn_submit:


                submitData();

                break;
            case R.id.btn_print:
                doPrint();
                break;
            default:
                break;
        }
    }

    private PrintTool printTool = new PrintTool(this);
    PrintTool.OnPrintLs lsAll = new PrintTool.OnPrintLs() {
        @Override
        public void onPrint(long printServerID) {
            String cfgIDs = clp_bean.CON_ID+"";
            if(TextUtils.isEmpty(cfgIDs)){
                UIHelper.showToast(getString(R.string.sync_success));
                return;
            }

            conn.recPrintLP_p(new NetConnectionInterface.SyncJsonHandler(mActivity) {
                @Override
                public void handReponseJson(JSONObject json) {
                    UIHelper.showToast(getString(R.string.sync_success));
                    Intent data = new Intent();
                    data.putExtra("clp", clp_bean);
                    setResult(resultCode, data);
                    mActivity.finish();
                    overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
                }
            }, printServerID, Cycle_Count_Task_Key.CCT_CLP, clp_bean.TYPE_ID, cfgIDs);
        }
    };

    /**
     * 监视器用于循环守卫线程是否正常运行
     */
    private Runnable slc_to_down = new Runnable() {
        public void run() {
            ((ScrollView) findViewById(R.id.scrollview)).fullScroll(View.FOCUS_DOWN);
            if(!Utility.isNullForList(listView)){
                listView.get(listView.size()-1).requestFocus();
            }
        }
    };

    /**
     * 获得有效的SN个数
     * @param list
     * @return
     */
    private int getValidSNCount(List<EditText> list) {
        int cnt = 0;
        if (Utility.isNullForList(list)) {
            return cnt;
        }
        for (EditText et : list) {
            if (!TextUtils.isEmpty(et.getText().toString())) {
                cnt ++;
            }
        }
        return cnt;
    }

    /**
     * 添加SN View
     * isSelect true 选中底部 false不选
     */
    private void setAddSnLayout(boolean isSelect){

        if(btn_sel_lp_tyoe.cutObj!=null){
            CCT_LP_Type cct_lp_type = (CCT_LP_Type)btn_sel_lp_tyoe.cutObj.c;
            int num = cct_lp_type.stack_width_qty * cct_lp_type.stack_height_qty * cct_lp_type.stack_length_qty;
            if(!Utility.isNullForList(listView) && num <= listView.size()){
                //UIHelper.showToast("Just only add " + num + " sn");
                if (listView.get(listView.size() - 1).isFocused()) {
                    UIHelper.showToast("All SN have been added.");
                }
                // 添加到最后一个 需要隐藏加号按钮
                findViewById(R.id.btn_add_sn).setVisibility(View.GONE);
                return;
            }
        }

        //判断是否可以继续添加
        if(isSelect && !checkSNLayoutCanAdd()){
            UIHelper.showToast("Please scan sn");
            return;
        }

        if(isSameValue(null)){
            return;
        }

        if(listView == null){
            listView = new ArrayList<EditText>();
        }
        final View view = getLayoutInflater().inflate(R.layout.cct_create_clp_sn_item, null);
        ImageButton btn_del = (ImageButton)view.findViewById(R.id.btn_del);
        final EditText et_sn = (EditText)view.findViewById(R.id.et_sn);

        //add by xia
        final int snTotal = Integer.valueOf(t_total.getText().toString());
        findViewById(R.id.btn_add_sn).setVisibility(snTotal <= 1 ? View.GONE : View.VISIBLE);
        // 添加后 如果有效 重置 value / total SN个数
        t_scan_num.setText(getValidSNCount(listView)+"");

        et_sn.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_ENTER == keyCode && event.getAction() == KeyEvent.ACTION_UP && !isSameValue(et_sn)) {
                    t_scan_num.setText(getScanSN_Num()+"");
                    Utility.colseInputMethod(mActivity, et_sn);
                    setAddSnLayout(true);
                }
                return false;
            }
        });

        et_sn.setTransformationMethod(trans);

        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listView.size() > 1) {
                    scan_sn_layout.removeView(view);
                    listView.remove(et_sn);
                } else {
                    UIHelper.showToast(mActivity, "Please scan sn");
                }

                //add by xia
                t_scan_num.setText(listView.size()+"");
                if (listView.size() < snTotal) {
                    findViewById(R.id.btn_add_sn).setVisibility(View.VISIBLE);
                }
            }
        });
        scan_sn_layout.addView(view);
        listView.add(et_sn);
        if(isSelect) {
            handler.postDelayed(slc_to_down, 10);
        }
    }

    /**
     * 提交方法
     */
    private void submitData(){
        t_scan_num.setText(getScanSN_Num() + "");

        boolean isCheck = CCT_Util.checkTextViewValue(mActivity, btn_sel_lp_tyoe, btn_sel_title, btn_sel_customer);
        if(!isCheck){return;}

        final JSONObject json = getJsonData();
        if(TextUtils.isEmpty(json.toString())){
            UIHelper.showToast(getString(R.string.sync_data_error));
            return;
        }

        boolean isHasSn = (cutIsHasSn == Cycle_Count_Task_Key.CCT_P_HAS_SN);
        if(isHasSn){
            if(!json.has("sn_list")){
                UIHelper.showToast("Please scan SN!");
                return;
            }else{
                int sn_num = json.optJSONArray("sn_list").length();
                if(total_num-sn_num>0){
                    RewriteBuilderDialog.showSimpleDialog_Tip(mActivity, (total_num-sn_num) + " SN to be scanned for current LP type");
                }else{
                    toDoSubmit(json);
                }
            }
        }else{
            toDoSubmit(json);
        }
    }

    /**
     * 提交数据
     * @param json
     */
    private void toDoSubmit(JSONObject json){
        conn.CCT_Addtlpcontext(new NetConnectionInterface.SyncJsonHandler(mActivity) {
            @Override
            public void handReponseJson(JSONObject json) {
                String type_name = btn_sel_lp_tyoe.getText().toString();
                parsingData(json,type_name);
            }

            @Override
            public void handFail(int statusCode, Header[] headers, JSONObject json) {

                if(json==null) return;
                CCT_Util.showMessage(json, CCT_Util.ERR_MESSAGE);
            }
        }, json.toString());
    }

    /**
     * 创建TLP后 保存tlp json  并且隐藏提交按钮 显示打印按钮
     * @param json
     */
    private void parsingData(JSONObject json,String type_name){
        JSONObject objJson = json.optJSONObject("lp");
        clp_bean = new CCT_CLP_Bean();
        clp_bean.CON_ID = objJson.optLong("con_id");
        clp_bean.CONTAINER = objJson.optString("container");
        clp_bean.REAL_NUMBER = objJson.optString("container");
        clp_bean.CONTAINER_TYPE = objJson.optInt("container_type");
        clp_bean.IS_FULL = objJson.optInt("is_full");
        clp_bean.LOT_NUMBER = objJson.optString("lot_number");
        clp_bean.TYPE_ID = objJson.optInt("type_id");
        clp_bean.TITLE_ID = objJson.optInt("title_id");
        clp_bean.P_NAME = json.optString("p_name");

        clp_bean.TYPE_NAME = type_name;//因为系统没有返回type name 所以需要根据创建的条件来定义当前的lp_name
        btn_submit.setVisibility(View.GONE);
        btn_print.setVisibility(View.VISIBLE);

        RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mActivity);
        dialog.setMessage(getString(R.string.sync_create_success) + "," + getString(R.string.sync_print) + "?");
        dialog.setCancelable(false);
        dialog.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doPrint();
            }
        });
        dialog.setNegativeButton(getString(R.string.cancle_alert_dialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent data = new Intent();
                data.putExtra("clp", clp_bean);
                setResult(resultCode, data);
                mActivity.finish();
                overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
            }
        });
        dialog.create().show();

    }




    /**
     * 检查SN控件是否可以继续添加
     * @return
     */
    private boolean checkSNLayoutCanAdd(){
        int totalSN = Integer.parseInt(t_total.getText().toString());
        if(Utility.isNullForList(listView) && totalSN > 0){
            return true;
        }
        for (int i = 0; i < listView.size(); i++) {
            if(TextUtils.isEmpty(listView.get(i).getText().toString())){
                listView.get(i).requestFocus();
                return false;
            }
        }
        return true;
    }

    /**
     * 检查SN是否有重复的 true 重复的
     * @param et
     */
    private boolean isSameValue(EditText et){
        if(Utility.isNullForList(listView)){
            return false;
        }

        for (int i = 0; i < listView.size(); i++) {
            String s = listView.get(i).getText().toString();
            int sameNum = 0;
            for (int j = 0; j < listView.size(); j++) {
                if(s.equals(listView.get(j).getText().toString())){
                    sameNum++;
                }
            }
            if(sameNum>=2){
                if(et!=null){
                    et.requestFocus();
                    et.setText("");
                }
                UIHelper.showToast("Have found the same sn");
                return true;
            }
        }
        return false;
    }

    /**
     * 获取需要提交的数据
     * @return
     */
    private JSONObject getJsonData(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pc_id", product.pc_id);
            jsonObject.put("item_id", product.main_code);
            jsonObject.put("container",TextUtils.isEmpty(create_code)?"":create_code.toUpperCase(Locale.ENGLISH));
            jsonObject.put("container_type", Cycle_Count_Task_Key.CCT_CLP);
            jsonObject.put("type_id",btn_sel_lp_tyoe.key);
            jsonObject.put("lot_number",TextUtils.isEmpty(et_lot.getText().toString())?"":et_lot.getText().toString());
            jsonObject.put("customer_id",btn_sel_customer.key);
            jsonObject.put("title_id",btn_sel_title.key);
            jsonObject.put("hardwareId",TextUtils.isEmpty(et_rfid.getText().toString())?"":et_rfid.getText().toString());
            jsonObject.put("status","2");//1:open 2closed  表示的如果为1那么 创建的Clp是可以修改的
            jsonObject.put("ps_id", StoredData.getPs_id());
            jsonObject.put("location_name",sBean.slc_id);
            jsonObject.put("location_type",sBean.slc_type);


//            jsonObject.put("is_damage",isDamaged?1:0); //0:normal 1:damage

            boolean isHasSn = (cutIsHasSn == Cycle_Count_Task_Key.CCT_P_HAS_SN);
            //容器内的包装是否满了
            final int Full = 3;
            final int Not_Full = 2;
            final int Empty = 1;

            if(isHasSn){
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i <listView.size() ; i++) {
                    JSONObject j = new JSONObject();
                    String sn = listView.get(i).getText().toString();
                    if(!TextUtils.isEmpty(sn)){
                        j.put("sn",sn.toUpperCase(Locale.ENGLISH));
                        jsonArray.put(j);
                    }
                }
                if(!StringUtil.isNullForJSONArray(jsonArray)){
                    jsonObject.put("sn_list",jsonArray);
                }

                CCT_LP_Type cct_lp_type = (CCT_LP_Type)btn_sel_lp_tyoe.cutObj.c;
                //当前CLP可以装多少个商品
                int num = cct_lp_type.stack_width_qty * cct_lp_type.stack_height_qty * cct_lp_type.stack_length_qty;

                int full_type = Empty;
                if(jsonArray.length()==0){
                    full_type = Empty;
                }else if(jsonArray.length()>0&&jsonArray.length()<num){
                    full_type = Not_Full;
                }else{
                    full_type = Full;
                }
                jsonObject.put("is_full",full_type);//1  容器上没有商品 2容器上商品不满 3容器上商品已满
            }else{
                jsonObject.put("pc_qty",et_total.getText().toString()); //0:normal 1:damage
                if("0".equals(et_total.getText().toString())){
                    jsonObject.put("is_full",Empty);
                }else{
                    jsonObject.put("is_full",Full);
                }
            }

            jsonObject.put("is_has_snlist",isHasSn);

        } catch (Exception e) {
            e.printStackTrace();
            UIHelper.showToast(getString(R.string.sync_data_error));
            return null;
        }
        return jsonObject;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CCT_Add_LP_Type.resultCode){
            List<CCT_LP_Type> clp_types  = (List<CCT_LP_Type>) data.getSerializableExtra("clp_types");
            int curr_lpt_id = data.getIntExtra("curr_lpt_id", 0);
            if(clp_types==null||curr_lpt_id==0){
                return;
            }
            int index = -1;
            if(!Utility.isNullForList(clp_types)&&curr_lpt_id!=0){
                for (int i = 0; i < clp_types.size(); i++) {
                    if(clp_types.get(i).lpt_id == curr_lpt_id){
                        index = i;
                        break;
                    }
                }
            }

            bean.clp_types.clear();
            bean.clp_types.addAll(clp_types);
            setSelLpTyoe(index);
            if(index>=0&&index<clp_types.size()){
                btn_sel_lp_tyoe.toDo();
            }
        }
    }

    /**
     * 获取打印提示
     */
    private void doPrint(){
        printTool.setIface(new PrintTool.UseMySelfWay() {
            @Override
            public void getPrintServer() {
                conn.getPrintServerList(new NetConnectionInterface.SyncJsonHandler(mActivity) {
                    @Override
                    public void handReponseJson(JSONObject json) {
                        printTool.jsonToShow(json, lsAll, "");
                    }
                });
            }
        });
        printTool.showDlg_printers(lsAll);
    }

    /**
     * 获取扫描的sn的数量
     */
    private int getScanSN_Num(){
        int scan_num = 0;
        if(Utility.isNullForList(listView)){
            return scan_num;
        }
        for (int i = 0; i < listView.size(); i++) {
            if(!TextUtils.isEmpty(listView.get(i).getText().toString())){
                scan_num++;
            }
        }
        return scan_num;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        t_scan_num.setText(getScanSN_Num()+"");
    }
}
