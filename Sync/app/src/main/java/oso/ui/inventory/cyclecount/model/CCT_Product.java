package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;

/**
 * @ClassName: CCT_CLP_Bean 
 * @Description: 用于
 * @author gcy
 * @date 2015-4-8 下午10:04:36
 */
public class CCT_Product implements Serializable{

	private static final long serialVersionUID = -7622521064486859320L;
	public String p_name;// "010B3-LEOPARD",
	public double weight;// 100,
	public double heigth;// 18,
	public double width;// 18,
	public long pc_id;// 1002445,
	public String length_uom_name;// "inch",
	public String weight_uom_name;// "LBS",
	public String category_name;// "AMPLIFIER 1CH",
	public int weight_uom;// 2,
	public double volume;// 6151.723,
	public double length;// 18,
	public int length_uom;// 4,
	public String main_code;// "010B3-LEOPARDD010B3-LEOPARDD",
	public int catalog_id;// 11

}
