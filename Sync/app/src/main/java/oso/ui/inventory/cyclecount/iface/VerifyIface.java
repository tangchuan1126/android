package oso.ui.inventory.cyclecount.iface;

import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;


public interface VerifyIface {
	/**
	 * @Description:用于弹出提示框选择操作
	 * @param @param is 只能传2个参数 第一个是父级别的索引,第二个是子级别的索引
	 */
	public void dialogCondition(int...is);
	
	public void changeScanned(int groupPosition,int childPosition);
	
	public void scanTLP_Info(int groupPosition);

	public void showCLP_Dlg_photos(String sku,CCT_CLP_Bean clp);
	
	public void showTLP_Dlg_photos(String sku,CCT_TLP_Bean tlp);
}
