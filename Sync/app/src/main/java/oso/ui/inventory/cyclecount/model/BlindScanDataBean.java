package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.util.CCT_Util;
import oso.widget.photo.bean.TTPImgBean;
import support.dbhelper.StoredData;
import utility.StringUtil;
import android.text.TextUtils;
/**
 * 
 * @author xialimin
 * @date 2015-4-9 下午5:56:27
 * 
 * @description 已扫描待提交列表  有顺序要求  上-->下 依次为TLP CLP 
 */
public class BlindScanDataBean implements Serializable {
	private static final long serialVersionUID = 6829067176262914675L;
	
	public String slc_id;
	public String task_id;
	public String user_id;
	public int task_type; //1.Blind, 2.Verify 3.Verification
	
	public static final int Type_CLP = 0;
	public static final int Type_PDT_HAS_SN = 1;  // is_has_sn = 1;
	public static final int Type_PDT_NO_SN = 2; // is_has_sn = 2;
	
	
	public List<CCT_TLP_Bean> tlpList = new ArrayList<CCT_TLP_Bean>();
	public List<CCT_CLP_Type_Bean> clpTypeList = new ArrayList<CCT_CLP_Type_Bean>();
	
	/**
	 * 获得总共的一级个数[Group]。
	 * @return
	 */
	public int getGroupCount() {
		return tlpList.size() + clpTypeList.size();
	}

	/**
	 * 根据Lv的position来获得当前position的数据bean
	 * @param groupPosition
	 * @return
	 */
	public Object getGrpBeanByPosition(int groupPosition) {
		int type = getType(groupPosition);
		return getGrpBeanByPosition(groupPosition, type);
	}


	/**
	 * 根据Lv的position来获得当前position的数据bean
	 * @param position
	 * @param type
	 * @return
	 */
	public Object getGrpBeanByPosition(int position, int type) {
		if(Cycle_Count_Task_Key.CCT_TLP == type) return tlpList.get(position);
		else return clpTypeList.get(position - tlpList.size()); 
	}
	
	/**
	 * 获得item group类型
	 * @param position
	 * @return
	 */
	public int getType(int groupPosition) {
		if(groupPosition <= tlpList.size() - 1) return Cycle_Count_Task_Key.CCT_TLP;
		return Cycle_Count_Task_Key.CCT_CLP;
	}
	
	
	/**
	 * 获得重复的ProductSnBean
	 * @param sn
	 * @return
	 */
	public CCT_ProductBean getRepeatPdtSnBean(String sn) {
		if(TextUtils.isEmpty(sn)) return null;
		// 只有在TLP中才会有
		for(CCT_TLP_Bean tlpBean : tlpList) {
		for(CCT_ProductTypeBean pdtTypeBean : tlpBean.pdtTypeList) {
			for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
				if(sn.equals(pdtBean.SN_BLIND_SINGLE)) {
					return pdtBean;
				}
			}
		}
		}
		return null;
	}
	
	/**
	 * 获得damaged clp的数量
	 * @return
	 */
	public int getDamagedClpsCnt() {
		int cnt=0;
		for(CCT_CLP_Type_Bean clpTypeBean:clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.isDamaged) cnt++;
			}
		}
		return cnt;
	}
	
	/**
	 * 获得所有Damaged的CLP集合
	 * @return
	 */
	public List<CCT_CLP_Bean> getAllAdamagedCLPList() {
		List<CCT_CLP_Bean> list = new ArrayList<CCT_CLP_Bean>();
		for(CCT_CLP_Type_Bean clpTypeBean:clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.isDamaged) {
					list.add(clpBean);
				}
			}
		}
		return list;
	}
	
	/**
	 * 根据关键字段    pc_id  或者 real_number来进行删除
	 * @param type
	 * @param number
	 * @return
	 */
	public boolean removeBeanByKeywords(int type, String number) {
		if(TextUtils.isEmpty(number)) return false;
		boolean isDeled = false;
		int currIndex = -1;
		switch (type) {
		case BlindScanDataBean.Type_CLP:
			// 外层的clp list
			for(int t=0; t<clpTypeList.size(); t++) {
				CCT_CLP_Type_Bean clpTypeBean = clpTypeList.get(t);
				for(int out_clp_index=0; out_clp_index<clpTypeBean.CONTAINERS.size(); out_clp_index++) {
					CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(out_clp_index);
					if(number.equals(clpBean.REAL_NUMBER)) {
						clpTypeBean.CONTAINERS.remove(out_clp_index);
						isDeled = true;
						currIndex = t;
						break;
					}
				}
				
			}
			
			// 如果该分组已经删没了，则连分组也删掉
			if(isDeled && clpTypeList.get(currIndex).CONTAINERS.size() == 0 && currIndex >=0) {
				clpTypeList.remove(currIndex);
				return isDeled;
			}
			
			
			
			// ---------------------------------------------------------------------------------
			isDeled = false;
			currIndex = -1;
			// 里层CLP
			for(int i=0; i<tlpList.size(); i++) {
				// tlp中的clp list
				CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) tlpList.get(i);
				
				for(int t=0; t<tlpBean.clpTypeList.size(); t++) {
					CCT_CLP_Type_Bean clpTypeBean = tlpBean.clpTypeList.get(t);
					
					for(int out_clp_index=0; out_clp_index<clpTypeBean.CONTAINERS.size(); out_clp_index++) {
						CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(out_clp_index);
						if(number.equals(clpBean.REAL_NUMBER)) {
							clpTypeBean.CONTAINERS.remove(out_clp_index);
							isDeled = true;
							currIndex = t;
						}
					}
					
				}
				
				// 如果该分组已经删没了，则连分组也删掉
				if(isDeled) {
					if(tlpBean.clpTypeList.get(currIndex).CONTAINERS.size()== 0 && currIndex >=0) {
						tlpBean.clpTypeList.remove(currIndex);
					}
					return isDeled;
				}
			}
			
			break;
		case BlindScanDataBean.Type_PDT_NO_SN:
			for(int i=0; i<tlpList.size(); i++) {
				// tlp中的product list
				List<CCT_ProductBean> pdtList = tlpList.get(i).pdtNoSnList;
				for(int j=0; j<pdtList.size(); j++) {
					CCT_ProductBean pdtBean = pdtList.get(j);
					if(number.equals(pdtBean.modelNo+"/"+pdtBean.lotNo) || number.equals(pdtBean.P_NAME)) {
						tlpList.get(i).pdtNoSnList.remove(j);
						return true;
					}
				}
			}
			break;
			
		case BlindScanDataBean.Type_PDT_HAS_SN:
			
			for(int i=0; i<tlpList.size(); i++) {
				// tlp中的clp list
				CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) tlpList.get(i);
				
				for(int t=0; t<tlpBean.pdtTypeList.size(); t++) {
					CCT_ProductTypeBean pdtTypeBean = tlpBean.pdtTypeList.get(t);
					for(int out_pdt_index=0; out_pdt_index<pdtTypeBean.pdtHasSNList.size(); out_pdt_index++) {
						CCT_ProductBean pdtBean = pdtTypeBean.pdtHasSNList.get(out_pdt_index);
						if(number.equals(pdtBean.SN_BLIND_SINGLE)) {
							isDeled = true;
							currIndex = out_pdt_index;
						}
					}
					
					// 如果该分组已经删没了，则连分组也删掉
					if(isDeled) {
						pdtTypeBean.pdtHasSNList.remove(currIndex);
						if(pdtTypeBean.pdtHasSNList.size() == 0) {
							tlpBean.pdtTypeList.remove(t);
							return isDeled;
						}
						return isDeled;
					}
				}
			}
			
			break;
			
		}
		
		return false;
	}
	

	/**
	 * 删除
	 * @param deletePosition
	 */
	public void removeBeanByPosition(int grpPosition, int deletePosition) {
		removeBean(getBeanByPosition_Level1(grpPosition, deletePosition));
	}
	
	/**
	 * 获得外层界面的 指定位置的数据bean
	 * @param grpPosition
	 * @param childPosition
	 * @return
	 */
	public Object getBeanByPosition_Level1(int grpPosition, int childPosition) {
		switch (getType(grpPosition)) {
		case Cycle_Count_Task_Key.CCT_TLP: return tlpList.get(grpPosition);

		case Cycle_Count_Task_Key.CCT_CLP:
			
			//-----------情况1    组---------------
			if(childPosition == -1) {
				return clpTypeList.get(grpPosition - tlpList.size());
			}
			//-----------情况2   子---------------
			if(childPosition >= 0) {
				CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) getGrpBeanByPosition(grpPosition);
				return clpTypeBean.CONTAINERS.get(childPosition);
			}
			
		}
		
		return null;
	}
	
	/**
	 * 删除Bean
	 * @param key
	 * @return
	 */
	public boolean removeBean(Object key) {
		if(key == null) return false;
		
		//-------------------------------------------如果是删除TLP组的操作  删除组
		if(key instanceof CCT_TLP_Bean) {
			return tlpList.remove(key);
		}
		
		//-------------------------------------------如果是删除CLP组的操作  删除组
		if(key instanceof CCT_CLP_Type_Bean) {
			boolean isFound = false;
			int currIndex = 0;
			key = (CCT_CLP_Type_Bean) key;
			for(int i=0; i<clpTypeList.size(); i++) {
				if(((CCT_CLP_Type_Bean) key).CONTAINER_TYPE_ID == clpTypeList.get(i).CONTAINER_TYPE_ID) {
					currIndex = i;
					isFound = true;
					break;
				}
			}
			
			if(isFound) {
				clpTypeList.remove(currIndex);
			}
		}
		
		//-------------------------------------------如果是删除CLP的操作  删除CLP
		if(key instanceof CCT_CLP_Bean) {
			boolean isFound = false;
			int currPosJ = 0, currPosI = 0;
			for(int i=0; i<clpTypeList.size(); i++) {
				CCT_CLP_Type_Bean clpTypeBean = clpTypeList.get(i);
				for(int j=0; j<clpTypeBean.CONTAINERS.size(); j++) {
					CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(j);
					
					key = (CCT_CLP_Bean)key;
					if(clpBean == key) {
						isFound = true;
						currPosI = i;
						currPosJ = j;
					}
				}
			}
			
			if(isFound) {
				clpTypeList.get(currPosI).CONTAINERS.remove(currPosJ);
				
				// 判断当前clpType是否还有子数据，如果没有  则删除整个clpType
				if(clpTypeList.get(currPosI).CONTAINERS.size() == 0) {
					clpTypeList.remove(currPosI);
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 设置指定Position的CLP的damage的状态
	 * @param isDamaged
	 * @param position
	 * @return
	 */
	public boolean setCLPDamagedByPosition(boolean isDamaged, int groupPosition, int childPosition) {
		CCT_CLP_Type_Bean cct_CLP_Type_Bean = clpTypeList.get(groupPosition - tlpList.size());
		CCT_CLP_Bean cct_CLP_Bean = cct_CLP_Type_Bean.CONTAINERS.get(childPosition);
		cct_CLP_Bean.isDamaged = isDamaged;
		return isDamaged;
	}
	
	/**
	 * 设置指定Position的TLP的damage的状态
	 * @param b
	 * @param groupPosition
	 */
	public boolean setTLPDamagedByPosition(boolean isDamaged, int groupPosition) {
		CCT_TLP_Bean tlpBean = tlpList.get(groupPosition);
		tlpBean.isDamaged = isDamaged;
		return isDamaged;
	}
	
	/**
	 * 根据container_id 查找是否有重复的TLP
	 * @param container_id
	 * @return 是否重复
	 */
	public boolean isContainTLPByNo(String realContainerNo) {
		if(!CCT_Util.isLPFixed(realContainerNo)) {
			realContainerNo = CCT_Util.fixLP_Number(realContainerNo);
		}
		if(TextUtils.isEmpty(realContainerNo)) return true;
		for(CCT_TLP_Bean tlpBean : tlpList) {
			if(tlpBean.REAL_NUMBER.equals(realContainerNo)) return true;
		}
		return false;
	}
	
	/**	
	 * 获得重复的CLP，之前CLP的Position  // TODO 这个方法貌似有问题
	 * @param container_id
	 * @return int[] {groupPosition, childPosition}
	 */
	public int[] getLPRepeatOldPostion(String realContainerNo) {
		// 获得当前TLP的长度，0:0   length>0: length-1;
		int pos[] = new int[2];
		if(TextUtils.isEmpty(realContainerNo)) {
			int cnt = 0;
			if(tlpList.size() > 0) {
				cnt += tlpList.size() - 1;
			}
			if(clpTypeList.size() > 0) {
				cnt += clpTypeList.size() - 1;
			}
			
			pos[0] = cnt;
			pos[1] = -1;
			return pos;
		}
		
		if(!CCT_Util.isLPFixed(realContainerNo)) {
			realContainerNo = CCT_Util.fixLP_Number(realContainerNo);
		}
		
		//--------------tlp
		for(int index = 0; index < tlpList.size(); index ++) {
			if(realContainerNo.equals(tlpList.get(index).REAL_NUMBER)) {
				pos[0] = index;
				pos[1] = -1;
				return pos;
			}
		}
		
		//--------------clp
		for(int typeIndex=0; typeIndex < clpTypeList.size(); typeIndex ++) {
			CCT_CLP_Type_Bean clpTypeBean = clpTypeList.get(typeIndex);
			for(int index=0; index<clpTypeBean.CONTAINERS.size(); index++) {
				CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(index);
				if(realContainerNo.equals(clpBean.REAL_NUMBER)) {
					pos[0] = tlpList.size() + typeIndex;
					pos[1] = index;
					return pos;
				}
			}
		}
		
		int cnt = 0;
		if(tlpList.size() > 0) {
			cnt += tlpList.size() - 1;
		}
		if(clpTypeList.size() > 0) {
			cnt += clpTypeList.size();
		}
		pos[0] = cnt;
		pos[1] = -1;
		return pos;
	}
	
	/**
	 * 根据container_id 查找是否有重复的CLP
	 * @param container_id
	 * @return 是否重复
	 */
	public boolean isContainCLPByNo(String realContainerNo) {
		if(TextUtils.isEmpty(realContainerNo)) return true;
		if(!CCT_Util.isLPFixed(realContainerNo)) {
			realContainerNo = CCT_Util.fixLP_Number(realContainerNo);
		}
		
		
		//---------验证一级界面的CLP是否重复。  [外层的clpList]
		for (CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for (CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.REAL_NUMBER.equals(realContainerNo)) return true;
			}
		}
		
		return false;
	}

	/**
	 * 根据grpPosition 和 childPosition设置对应条目的选中状态
	 * @param grpPos
	 * @param childPos
	 */
	public void setSelectedByPosition(int grpPos, int childPos) {
		switch (getType(grpPos)) {
		case Cycle_Count_Task_Key.CCT_TLP:
			CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) getGrpBeanByPosition(grpPos);
			tlpBean.itemIsSeleted = true;
			break;

		case Cycle_Count_Task_Key.CCT_CLP:
			CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) getGrpBeanByPosition(grpPos);
			clpTypeBean.CONTAINERS.get(childPos).itemIsSeleted = true;
			break;
		}
	}
	
	
	/**
	 * 查询当前
	 * @param type
	 * @param key
	 * @return  !!! 可能为null  
	 */
	public Object getRepeatObj(int type, String key) {
		// 只会查CLP 和Product
		if(type == Type_CLP) {
			
			// 先查外层是否存在重复的
			for (CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
				for (CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
					if(clpBean.REAL_NUMBER.equals(key)) return clpBean;
				}
			}
			
			// 查内层是否存在重复的
			for(CCT_TLP_Bean tlpBean : tlpList) {
				for(CCT_CLP_Type_Bean clpTypeBean : tlpBean.clpTypeList) {
					for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
						if(clpBean.REAL_NUMBER.equals(key)) return clpBean;
					}
				}
			}
		} else if(type == Type_PDT_HAS_SN) {
			for(CCT_TLP_Bean tlpBean : tlpList) {
				
				//查pdtType
				List<CCT_ProductTypeBean> pdtTypeList = tlpBean.pdtTypeList;
				for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
					
					for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
						if(pdtBean.SN_BLIND_SINGLE.equals(key)) {
							return pdtBean;
						}
					}
					
				}
			}
		} else if(type == Type_PDT_NO_SN) {
			for(CCT_TLP_Bean tlpBean : tlpList) {
				List<CCT_ProductBean> pdtNoSnList = tlpBean.pdtNoSnList;
				for(CCT_ProductBean pdtBean : pdtNoSnList) {
					if(pdtBean.comparedId.equals(key)) {
						return pdtBean;
					}
				}
			}
				
		}
		
		return null;
	}
	
	
	/**
	 * 查询CLP或者PDT所属的TLP bean
	 * @param clpRealNoOrPdtPcId
	 * @return !!! 可能为null
	 */
	public CCT_TLP_Bean getParent(String clpRealNoOrPdtCmpId) {
		if(TextUtils.isEmpty(clpRealNoOrPdtCmpId)) return null;
		
		// 先判断外层的CLP
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.REAL_NUMBER.equals(clpRealNoOrPdtCmpId)) {
					return null;  // 如果外层CLP存在  则返回NULL  因为外层的CLP 不属于TLP，只属于当前Location
				}
			}
		}
		
		
		for(CCT_TLP_Bean tlpBean : tlpList) {
			List<CCT_ProductBean> pdtList = tlpBean.pdtNoSnList;
			for(CCT_ProductBean pdtBean_inner : pdtList) {
				
				// 校验其他类型的Product
				String comparedId = "";
				if(clpRealNoOrPdtCmpId.indexOf("/") < 0) {
					comparedId = tlpBean.CON_ID + "/" + clpRealNoOrPdtCmpId;
				} else if(clpRealNoOrPdtCmpId.indexOf("/") == 0) {
					comparedId = tlpBean.CON_ID + clpRealNoOrPdtCmpId;
				}
				if(pdtBean_inner.comparedId.equals(comparedId)) {
					return tlpBean;
				}
				
			}
			
			for(CCT_CLP_Type_Bean clpTypeBean : tlpBean.clpTypeList) {
				for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
					if(clpBean.REAL_NUMBER.equals(clpRealNoOrPdtCmpId)) {
						return tlpBean;  // 如果外层CLP存在  则返回NULL  因为外层的CLP 不属于TLP，只属于当前Location
					}
				}
			}
		}
		
		//校验SN类型  获得parent TLP。 此处clpRealNoOrPdtCmpId 为 SN 
		for(CCT_TLP_Bean tlpBean : tlpList) {
			for(CCT_ProductTypeBean pdtTypeBean : tlpBean.pdtTypeList) {
				for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
					// 校验SN类型  获得parent TLP。 此处clpRealNoOrPdtCmpId 为 SN
					if(clpRealNoOrPdtCmpId.equals(pdtBean.SN_BLIND_SINGLE)) {
						return tlpBean;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 清除所有选中状态
	 */
	public void clearAllSeletedStatus() {
		//--外层
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.itemIsSeleted) {
					clpBean.itemIsSeleted = false;
				}
			}
		}
		
		//---内层clptype
		for(CCT_TLP_Bean tlpBean : tlpList) {
			List<CCT_CLP_Type_Bean> clpTypeBeanList = tlpBean.clpTypeList;
			for(CCT_CLP_Type_Bean clpTypeBean : clpTypeBeanList) {
				for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				if(clpBean.itemIsSeleted) {
					clpBean.itemIsSeleted = false;
				}
				}
			}
		}
		
		//---内层pdtType
		for(CCT_TLP_Bean tlpBean : tlpList) {
			for(CCT_ProductTypeBean pdtTypeBean : tlpBean.pdtTypeList) {
				for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
				if(pdtBean.itemIsSeleted) {
					tlpBean.itemIsSeleted = false;
				}
				}
			}
		}
		
		//---内层pdtNoSn
		for(CCT_TLP_Bean tlpBean : tlpList) {
			for(CCT_ProductBean pdtBean : tlpBean.pdtNoSnList) {
				if(pdtBean.itemIsSeleted) {
					pdtBean.itemIsSeleted = false;
				}
			}
		}
	}

	
	/**
	 * 拍照上传后，没有submit提交location，获得所有已经提交过的图片的file_id，请求服务端，进行批量删除图片
	 */
	public String getPicsFileIds() {
		ArrayList<Long> idsList = new ArrayList<Long>();
		// 外层TLP
		for(CCT_TLP_Bean tlp : tlpList) {
			List<TTPImgBean> photos = tlp.photos;
			for(int i=0; i<photos.size(); i++) {
				idsList.add(Long.parseLong(photos.get(i).file_id));
			}
		}
		
		// 外层CLP
		for(CCT_CLP_Type_Bean clpTypeBean : clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				List<TTPImgBean> photos = clpBean.photos;
				for(int i=0; i<photos.size(); i++) {
					idsList.add(Long.parseLong(photos.get(i).file_id));
				}
			}
		}
		
		for(CCT_TLP_Bean tlpBean : tlpList) {
			// 内层CLP
			for(CCT_CLP_Type_Bean clpTypeBean : tlpBean.clpTypeList) {
				for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
					List<TTPImgBean> photos = clpBean.photos;
					for(int i=0; i<photos.size(); i++) {
						idsList.add(Long.parseLong(photos.get(i).file_id));
					}
				}
			}
			// 内层SKU
			for(CCT_ProductTypeBean pdtTypeBean : tlpBean.pdtTypeList) {
				
				for(CCT_ProductBean pdtBean : pdtTypeBean.pdtHasSNList) {
					List<TTPImgBean> photos = pdtBean.blindPhotos;
					for(int i=0; i<photos.size(); i++) {
						idsList.add(Long.parseLong(photos.get(i).file_id));
					}
				}
			}
			
			// 内层无SN
			for(CCT_ProductBean pdtBean : tlpBean.pdtNoSnList) {
				List<TTPImgBean> blindPhotos = pdtBean.blindPhotos;
				for(int i=0; i<blindPhotos.size(); i++) {
					idsList.add(Long.parseLong(blindPhotos.get(i).file_id));
				}
			}
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<idsList.size(); i++) {
			sb.append(idsList.get(i)+"");
			if(i!=idsList.size()-1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	/**
	 * 生成提交的JSON数据
	 * @return
	 */
	public JSONObject getSubmitJson(Cycle_Count_Tasks_Bean tasksBean, CCT_LocationBean mLocBean) {
		try {
		JSONObject jobj = new JSONObject();
		JSONObject containers = new JSONObject();
		JSONArray damaged = new JSONArray();
		int clpDamagedCnt = getDamagedClpsCnt();
		List<CCT_CLP_Bean> damagedClpList = getAllAdamagedCLPList();
		for(int i=0; i<clpDamagedCnt; i++) {
			JSONObject damagedObj = new JSONObject();
			JSONArray images = new JSONArray();
			
			//=====photos=====
			List<TTPImgBean> damagedCLPphotos = damagedClpList.get(i).photos;
			for (int j=0; j<damagedCLPphotos.size(); j++) {  // images 
				images.put(damagedCLPphotos.get(j).uri); // 之前是file_id
			}
			
			if(images.length() > 0) {
				damagedObj.put("images", images);
			}
			damagedObj.put("id", damagedClpList.get(i).CON_ID);  // containers ---damaged -- id
			damaged.put(damagedObj);
		}
		
		
		JSONArray ids = new JSONArray();
		for(CCT_CLP_Type_Bean clpTypeBean:clpTypeList) {
			for(CCT_CLP_Bean clpBean : clpTypeBean.CONTAINERS) {
				ids.put(clpBean.CON_ID);
			}
		}
		
		if(!StringUtil.isNullForJSONArray(damaged)) {
			containers.put("damaged", damaged);
		}
		
		if(!StringUtil.isNullForJSONArray(ids)) {
			containers.put("ids", ids);
		}
		
		//===========TLPS======
		JSONArray tlps = new JSONArray();
		
		for(int i=0; i<tlpList.size();i++) {
			CCT_TLP_Bean tlp = tlpList.get(i);
			
			JSONObject tlpObj = new JSONObject();
			long con_id = tlp.CON_ID; // ---tlps 
			
			tlpObj.put("con_id", con_id);
			
			
			
			//--tlps --damaged     tlp也有damaged
			if(tlp.isDamaged) {
				JSONObject tlpDamagedObj = new JSONObject();
				tlpDamagedObj.put("id", tlp.CON_ID);
				
				JSONArray tlpDamageImgsArr = new JSONArray();
				for(TTPImgBean imgBean : tlp.photos) {
					tlpDamageImgsArr.put(imgBean.uri);
				}
				tlpDamagedObj.put("id", tlp.CON_ID);
				
				if(!StringUtil.isNullForJSONArray(tlpDamageImgsArr)) {
					tlpDamagedObj.put("images", tlpDamageImgsArr);
				}
				
				tlpObj.put("damaged", tlpDamagedObj);
			}
			
			
			
			// tlps--0--containers
			JSONObject containersObj = new JSONObject();
			JSONArray clpDamaged = new JSONArray();            // tlps --- 0 ---containers -- damaged
			
			
			List<CCT_CLP_Bean> dCLPs = tlp.getDamagedCLPs();
			for(int j=0;j<dCLPs.size();j++) {  // damaged size
				JSONObject damagedIdObj = new JSONObject();
				damagedIdObj.put("id", dCLPs.get(j).CON_ID);
				//需要提交Images
				JSONArray images = new JSONArray();
				List<TTPImgBean> clpPhotos = dCLPs.get(j).photos;
				for(TTPImgBean imgBean : clpPhotos) {
					images.put(imgBean.uri);   // 之前是file_id
				}
				
				if(images.length() > 0) {
					damagedIdObj.put("images", images);
				}
				clpDamaged.put(damagedIdObj);
			}
			
			if(!StringUtil.isNullForJSONArray(clpDamaged)) {
				containersObj.put("damaged", clpDamaged);
			}
			
			JSONArray clpIds = new JSONArray();    // tlps --- 0 ---containers -- ids
			List<Long> allCLPIds = tlp.getAllCLPIds();
			for(int k=0; k<allCLPIds.size();k++) {
				clpIds.put(allCLPIds.get(k));
			}
			
			if(!StringUtil.isNullForJSONArray(clpIds)) {
				containersObj.put("ids", clpIds);
			}
			
			
			
			// tlps---- products ---------------------------------------------------------
			JSONArray productsArr = new JSONArray();
			// tlps-products-添加pdtType的product
			List<CCT_ProductTypeBean> pdtTypeList = tlp.pdtTypeList;
			for(CCT_ProductTypeBean pdtTypeBean : pdtTypeList) {
				JSONObject pdtObj = new JSONObject();
				//----pc_id
				pdtObj.put("pc_id", pdtTypeBean.TYPE_ID);
				
				//----sn 数组
				JSONArray snArr = new JSONArray();
				for(CCT_ProductBean pdtHasSN : pdtTypeBean.pdtHasSNList) {
					snArr.put(pdtHasSN.SN_BLIND_SINGLE);
				}
				
				if(!StringUtil.isNullForJSONArray(snArr)) {
					pdtObj.put("sn", snArr);
				}
				
				//----damage
				JSONArray pdtSNDamageArr = new JSONArray();
				List<CCT_ProductBean> damagedSNPdtList = pdtTypeBean.getDamagedSNPdtList();
				for(CCT_ProductBean damagedSNPdt : damagedSNPdtList) {
					JSONObject damagedObj = new JSONObject();
					damagedObj.put("id", damagedSNPdt.PC_ID);
					damagedObj.put("sn", damagedSNPdt.SN_BLIND_SINGLE);
					
					// images
					JSONArray imagesSNArr = new JSONArray();
					for(TTPImgBean imgBean : damagedSNPdt.blindPhotos) {
						imagesSNArr.put(imgBean.uri);
					}
					
					if(!StringUtil.isNullForJSONArray(imagesSNArr)) {
						damagedObj.put("images", imagesSNArr);
					}
					pdtSNDamageArr.put(damagedObj);
				}
				
				if(pdtSNDamageArr.length() > 0) {
					pdtObj.put("damaged", pdtSNDamageArr);
				}
				productsArr.put(pdtObj);
			}
			
			// tlps-products-添加pdtSKU的product
			for(CCT_ProductBean pdtNoSN : tlp.pdtNoSnList) {
				JSONObject pdtObj = new JSONObject();
				//----pc_id
				pdtObj.put("pc_id", pdtNoSN.PC_ID);
				pdtObj.put("quantity", pdtNoSN.TOTAL);
				
				//----damage
				JSONArray pdtSKUDamageArr = new JSONArray();
				List<CCT_ProductBean> damagedSKUPdtList = tlp.getDamagedSKUPdtList();
				for(CCT_ProductBean damagedSKUPdt : damagedSKUPdtList) {
					
					if(pdtNoSN.PC_ID != damagedSKUPdt.PC_ID) {
						continue;
					}
					
					JSONObject damagedObj = new JSONObject();
					damagedObj.put("id", damagedSKUPdt.PC_ID);
					damagedObj.put("quantity", damagedSKUPdt.DAMAGEDCNT);
					
					// images
					JSONArray imagesArr = new JSONArray();
					for(TTPImgBean imgBean : damagedSKUPdt.blindPhotos) {
						imagesArr.put(imgBean.uri);
					}
					if(!StringUtil.isNullForJSONArray(imagesArr)) {
						damagedObj.put("images", imagesArr);
					}
					pdtSKUDamageArr.put(damagedObj);
				}
				
				if(!StringUtil.isNullForJSONArray(pdtSKUDamageArr)) {
					pdtObj.put("damaged", pdtSKUDamageArr);
				}
				productsArr.put(pdtObj);
			}
			
			if(!StringUtil.isNullForJSONArray(productsArr)) {
				tlpObj.put("products", productsArr);
			}
			
			
			
			
			tlpObj.put("containers", containersObj);
			
			tlps.put(tlpObj);
		}
		//===========TLPS END========
		if(!containers.isNull("ids")||!containers.isNull("damaged")){
			jobj.put("containers", containers);
		}
		
		if(!StringUtil.isNullForJSONArray(tlps)) {
			jobj.put("tlps", tlps);
		}
		jobj.put("slc_id", mLocBean==null?0:mLocBean.LOCATION_ID);
		jobj.put("slc_type", mLocBean==null?0:CCT_Util.changeToLong(mLocBean.LOCATION_TYPE+""));

		jobj.put("task_id", tasksBean==null?0:tasksBean.ID);
		jobj.put("task_type", tasksBean==null?"":tasksBean.TYPE);
		jobj.put("user_id", tasksBean==null?0:CCT_Util.changeToLong(StoredData.getAdid()));
		return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new JSONObject();
	}

	public int getAllSize() {
		return tlpList.size() + clpTypeList.size();
	}

}
