package oso.ui.inventory.cyclecount.adapter;

import oso.ui.inventory.cyclecount.iface.IBlind;
import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.BlindScanDataBean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Bean;
import oso.ui.inventory.cyclecount.model.CCT_CLP_Type_Bean;
import oso.ui.inventory.cyclecount.model.CCT_TLP_Bean;
import utility.Utility;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;
/**
 * 
 * @author xialimin
 * @date 2015-4-14 上午8:54:23
 * 
 * @description 盲盘数据适配器  数据包括TLC和CLP的集合
 */
public class BlindScanAdp extends BaseExpandableListAdapter {
	
	private Context mContext;
	private BlindScanDataBean mData;
	private IBlind iBlind;
	
	public static final int TLP = 0;
	public static final int CLP = 1;
	
	public BlindScanAdp(Context context, BlindScanDataBean mData, IBlind iblind) {
		this.mContext = context;
		this.mData = mData;
		this.iBlind = iblind;
	}
	
	@Override
	public int getGroupCount() {
		return mData.getGroupCount();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if(mData.getType(groupPosition) == Cycle_Count_Task_Key.CCT_CLP) {
			CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_CLP);
			return Utility.isNullForList(clpTypeBean.CONTAINERS)?0:clpTypeBean.CONTAINERS.size();
		}
		return 0;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initGroupView(groupPosition, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		convertView.setTag(R.layout.cct_blindscan_item_tlp_layout, groupPosition);  
		convertView.setTag(R.layout.cct_blindscan_item_group_clp_layout, -1);  
		
		fillGroupData(isExpanded, groupPosition, holder);
		setGroupListener(groupPosition, holder);
		return convertView;
	}



	/**
	 * 设置组的 按钮的点击事件
	 * @param groupPosition
	 * @param holder
	 */
	private void setGroupListener(final int groupPosition, ViewHolder holder) {
		final int type = getGroupType(groupPosition);
		
		holder.ivStatus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				iBlind.showDlg_onClickStatusIcon(type==TLP?Cycle_Count_Task_Key.CCT_TLP:Cycle_Count_Task_Key.CCT_CLP, groupPosition, -1);
			}
			});
		
		if(mData.getType(groupPosition) == Cycle_Count_Task_Key.CCT_CLP) return;
		
		final CCT_TLP_Bean tlpBean = ((CCT_TLP_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_TLP));
		holder.btnDamage.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			boolean isDamaged = tlpBean.isDamaged;
			mData.setTLPDamagedByPosition(!isDamaged, groupPosition);
			tlpBean.isDamaged = !isDamaged;
			BlindScanAdp.this.notifyDataSetChanged();
			if(tlpBean.isDamaged) {
				iBlind.showDlg_photos_tlp(tlpBean);
			}
		}
		});
		
		holder.flTakePhoto.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			iBlind.showDlg_photos_tlp(tlpBean);
		}
		});
		
	}

	/**
	 * 1. TLP 按顺序添加   2.CLP 按照type_id 进行分组  type_id 相同的归为一组
	 * @param groupPosition
	 * @param convertView
	 * @param holder
	 * @return convertView
	 */
	private View initGroupView(int groupPosition, View convertView, ViewHolder holder) {
		
		// -------根据containerType进行判断
		switch (getGroupType(groupPosition)) {
		case TLP:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_tlp_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blind_tlp_item);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindTLPType);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindTLPNumber);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_tlp_status_icon);
			holder.btnDamage = (Button) convertView.findViewById(R.id.btnBlindTlpDamage);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvBlindTlpImgCnt);
			holder.ivDamaged = (ImageView) convertView.findViewById(R.id.ivDamaged);
			holder.flTakePhoto =  (FrameLayout) convertView.findViewById(R.id.image_btn_takephoto_tlp);
			break;
		case CLP:
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_group_clp_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blind_clp_grp_item);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindCLPType_grp);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindCLPTypeId);
			
			holder.ivArrow = (ImageView) convertView.findViewById(R.id.iv_clptype_arrow);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_d_clp_icon);
			break;
		}
		return convertView;
	}

	/**
	 * 填充Group的数据
	 * @param groupPosition
	 * @param convertView
	 * @param holder
	 */
	private void fillGroupData(boolean isExpanded, int groupPosition, ViewHolder holder) {
		
		switch (getGroupType(groupPosition)) {
		case TLP:
			//---数据
			CCT_TLP_Bean tlpBean = (CCT_TLP_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_TLP);
			holder.tvNumber.setText(tlpBean.REAL_NUMBER);
			
			holder.item.setSelected(tlpBean.itemIsSeleted);
			holder.tvImgCnt.setText(tlpBean.photos.size()==0?0+"":tlpBean.photos.size()+"");
			holder.tvImgCnt.setVisibility(tlpBean.photos.size()==0?View.INVISIBLE:View.VISIBLE);
			holder.btnDamage.setBackgroundResource(tlpBean.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
			holder.ivDamaged.setSelected(tlpBean.photos.size()!=0);
			holder.flTakePhoto.setVisibility(tlpBean.isDamaged?View.VISIBLE:View.INVISIBLE);
			break;
			
		case CLP:
			CCT_CLP_Type_Bean clpTypeBean = (CCT_CLP_Type_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_CLP);
			holder.tvNumber.setText(clpTypeBean.LP_TYPE_NAME+"");   // CLP的 type_id
			
			boolean hasChild = clpTypeBean.CONTAINERS.size() > 0;
			// 关闭|无条目时,显示圆背景
			boolean showOne = !isExpanded || !hasChild;
			holder.item.setBackgroundResource(showOne ? R.drawable.cct_lv_item: R.drawable.cct_lv_item_top);
			// 更改箭头方向
			holder.ivArrow.setBackgroundResource(showOne ? R.drawable.mm_submenu_normal: R.drawable.mm_submenu_down);
			break;
		}
	}
	
	@Override
	public int getGroupType(int groupPosition) {
		return mData.getType(groupPosition)==Cycle_Count_Task_Key.CCT_TLP?TLP:CLP;
	}
	
	@Override
	public int getGroupTypeCount() {
		return 2;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = initChildView(groupPosition, childPosition, convertView, holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		fillChildData(groupPosition, childPosition, isLastChild, holder);
		setChildListener(holder, groupPosition, childPosition);
		return convertView;
	}


	/**
	 * 初始化ChildView
	 * @param groupPosition
	 * @param childPosition
	 * @param convertView
	 * @param holder
	 * @return
	 */
	private View initChildView(int groupPosition, int childPosition, View convertView, ViewHolder holder) {
		if(convertView == null) {
			convertView = View.inflate(mContext, R.layout.cct_blindscan_item_clp_layout, null);
			holder.item = (LinearLayout) convertView.findViewById(R.id.ll_blindscan_clp);
			holder.tvType = (TextView) convertView.findViewById(R.id.tvBlindCLPType);
			holder.tvNumber = (TextView) convertView.findViewById(R.id.tvBlindCLPNumber);
			holder.btnDamage = (Button) convertView.findViewById(R.id.btnBlindClpDamage);
			holder.tvImgCnt = (TextView) convertView.findViewById(R.id.tvBlindClpImgCnt);
			holder.ivDamaged = (ImageView) convertView.findViewById(R.id.ivDamaged);
			holder.flTakePhoto =  (FrameLayout) convertView.findViewById(R.id.image_btn_takephoto_clp);
			holder.ivStatus = (ImageView) convertView.findViewById(R.id.blind_clp_status_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	/**
	 * 填充Child数据  只有CLP的才会有子级结构
	 * @param groupPosition
	 * @param childPosition
	 * @param isLastChild
	 * @param holder
	 */
	private void fillChildData(int groupPosition, int childPosition, boolean isLastChild, ViewHolder holder) {
		CCT_CLP_Type_Bean clp_type = (CCT_CLP_Type_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_CLP);
		CCT_CLP_Bean clpBean = clp_type.CONTAINERS.get(childPosition);
		holder.item.setSelected(clpBean.itemIsSeleted);
		holder.tvType.setText("CLP: ");
		holder.tvNumber.setText(clpBean.REAL_NUMBER + "");
		holder.tvImgCnt.setText(clpBean.photos.size()==0?0+"":clpBean.photos.size()+"");
		holder.tvImgCnt.setVisibility(clpBean.photos.size()==0?View.INVISIBLE:View.VISIBLE);
		holder.btnDamage.setBackgroundResource(clpBean.isDamaged?R.drawable.cct_damage_btn_red:R.drawable.cct_damage_btn_gray);
		holder.ivDamaged.setSelected(clpBean.photos.size()!=0);
		holder.flTakePhoto.setVisibility(clpBean.isDamaged?View.VISIBLE:View.INVISIBLE);

		
		// 背景
		if (getChildrenCount(groupPosition) - 1 == childPosition) {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_bot_def);
		} else {
			holder.item.setBackgroundResource(R.drawable.cct_lv_item_mid_def);
		}
	}
	
	/**
	 * 设置子条目[CLP] 的点击事件
	 * @param holder
	 * @param groupPosition
	 * @param childPosition
	 */
	private void setChildListener(ViewHolder holder, final int groupPosition, final int childPosition) {
		if(mData.getType(groupPosition) == Cycle_Count_Task_Key.CCT_TLP) return;
		
		final CCT_CLP_Bean clpBean = ((CCT_CLP_Type_Bean) mData.getGrpBeanByPosition(groupPosition, Cycle_Count_Task_Key.CCT_CLP)).CONTAINERS.get(childPosition);
		holder.btnDamage.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			boolean isDamaged = clpBean.isDamaged;
			mData.setCLPDamagedByPosition(!isDamaged, groupPosition, childPosition);
			clpBean.isDamaged = !isDamaged;
			BlindScanAdp.this.notifyDataSetChanged();
			if(clpBean.isDamaged) {
				iBlind.showDlg_photos_clp(clpBean);
			}
		}
		});
		
		holder.flTakePhoto.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			iBlind.showDlg_photos_clp(clpBean);
		}
		});
		
		//------------------------长按删除条目----------------
		/*holder.item.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				iBlind.showDlg_onLongPress(CLP, groupPosition, childPosition);
				return true;
			}
		});*/
		
		holder.ivStatus.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			iBlind.showDlg_onClickStatusIcon(Cycle_Count_Task_Key.CCT_CLP, groupPosition, childPosition);
		}
		});
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	 //-----关闭全部
	 public void collapseAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.collapseGroup(i);
		 }
	 }
	 //-----展开全部
	 public void expandAll(ExpandableListView listview) {
		 if(listview==null){
			 return;
		 }
		 for (int i = 0; i < getGroupCount(); i++){
			 listview.expandGroup(i);
		 }
	 }
	
	class ViewHolder {
		LinearLayout item;
		TextView tvType, tvNumber, tvImgCnt;   // CLP group  显示Type_Name
		Button btnDamage;
		ImageView ivDamaged;
		FrameLayout flTakePhoto;
		
		// group箭头
		ImageView ivArrow;
		ImageView ivStatus;
	}
	
	
	
	/**
	 * 清除selected状态
	 */
	public void clearSeletedStatus() {
		int typePos = 0;
		int index = 0;
		boolean isFound = false;
		
		for(typePos=0; typePos<mData.clpTypeList.size(); typePos++) {
			CCT_CLP_Type_Bean clpTypeBean = mData.clpTypeList.get(typePos);
			for(index=0; index<clpTypeBean.CONTAINERS.size(); index++) {
				CCT_CLP_Bean clpBean = clpTypeBean.CONTAINERS.get(index);
				if(clpBean.itemIsSeleted) {
					isFound = true;
					break;
				}
			}
			
			if(isFound) break;
		}
		
		if(isFound) {
			mData.clpTypeList.get(typePos).CONTAINERS.get(index).itemIsSeleted = false;
		}
		
		BlindScanAdp.this.notifyDataSetChanged();
	}
}
