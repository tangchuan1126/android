package oso.ui.inventory.cyclecount.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Haven on 15/7/1.
 */
public class CCT_TitleAndCustomerInfo implements Serializable {

    private static final long serialVersionUID = 9213260801245622556L;

    public Title title;
    public List<Customer> customers;
    public String productId;


    public static class CCT_BaseData_Info implements Serializable{

        private static final long serialVersionUID = -6854242700047578212L;
        public String id;
        public String name;

    }

    public static class Title extends CCT_BaseData_Info {
        private static final long serialVersionUID = 1L;
    }


    public static class Customer extends CCT_BaseData_Info {
        private static final long serialVersionUID = 1L;
    }

}
