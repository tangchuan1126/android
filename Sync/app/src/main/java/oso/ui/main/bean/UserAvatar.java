package oso.ui.main.bean;

import java.io.Serializable;

public class UserAvatar implements Serializable {
	private static final long serialVersionUID = -5328914839007085417L;
	public String imgId;
	public String avatarUrl;
	public int activity; // 是否是主头像
	public UserAvatar(String id, String avatarUrl, int activity) {
		this.imgId = id;
		this.avatarUrl = avatarUrl;
		this.activity = activity;
	}
	public String getId() {
		return imgId;
	}
	public void setId(String id) {
		this.imgId = id;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	/**
	 * 头像标记
	 * @return  1：默认头像     2：普通头像
	 */
	public int getActivity() {
		return activity;
	}
	
	/**
	 * 设置是否是默认头像  
	 * @param activity   1：默认头像     2：普通头像
	 */
	public void setActivity(int activity) {
		this.activity = activity;
	}
}
