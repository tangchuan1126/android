package oso.ui.main;

import java.io.File;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.load_receive.photo.PhotoMainAc_LR;
import oso.ui.yardcontrol.photo.CheckInPhotoCaptureMainActivity;
import oso.widget.dialog.BottomDialog;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteDialog;
import oso.widget.switchbtn.SwitchButton;
import support.common.DownloadApk;
import support.common.UIHelper;
import support.dbhelper.DBHelper;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SetFunctionActivity extends BaseActivity {

	LinearLayout clear_image;
	LinearLayout version_Update;
	LinearLayout log_out;
	LinearLayout saveinfo;
	TextView cache_size;
	TextView version;
    DBHelper dbHelper;
    SwitchButton wiperSwitch;
    Switch switchbtn;
    TextView saveuserinfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setfunction_layout, 0);
		setTitleString(getString(R.string.set_title));
		init();
		refreshImageCache();
	}

	public void init() {
		dbHelper = new DBHelper(this);
		clear_image = (LinearLayout) findViewById(R.id.clear_image);
		version_Update = (LinearLayout) findViewById(R.id.version_Update);
		log_out = (LinearLayout) findViewById(R.id.log_out);
		cache_size = (TextView) findViewById(R.id.cache_size);
		version =(TextView) findViewById(R.id.version);
		clear_image.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				deleteTmpImgs();
			}
		});
		version_Update.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				RewriteBuilderDialog.showSimpleDialog(mActivity, getString(R.string.set_version_update)+"?",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								checkVersion();	
							}
						});
			}
		});
		log_out.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!StoredData.saveinfo){
					dbHelper.deleteALLUser();
				}
				Intent intent = new Intent(mActivity, Login.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("type", 1);
				startActivity(intent);
				
				//断开-连接状态
				StoredData.logout();
			}
		});
		saveinfo = (LinearLayout) findViewById(R.id.saveinfo);
		saveuserinfo = (TextView) findViewById(R.id.saveuserinfo);
		saveuserinfo.setText(StoredData.saveinfo ? getString(R.string.set_save_info_y) : getString(R.string.set_save_info_n));
		saveinfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final BottomDialog dialog = new BottomDialog(mActivity);
		        dialog.setTitle(getString(R.string.set_save_info));
		        dialog.hideButton();
		        dialog.setAdapterData(new String[]{getString(R.string.set_save_info_y),getString(R.string.set_save_info_n)}, new AdapterView.OnItemClickListener() {
		            @Override
		            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		            	if(position == 0){
		            		Utility.updateSaveInfo(true); 
		            	}else{
		            		Utility.updateSaveInfo(false); 
		            	}
		            	saveuserinfo.setText(StoredData.saveinfo ? getString(R.string.set_save_info_y) : getString(R.string.set_save_info_n));
		            	dialog.dismiss();
		            }
		        }, (StoredData.saveinfo ? 0 : 1));
		        dialog.show();
			}
		});
	}

	public void refreshImageCache() {
//		String cacheSize = Utility.getFileSize_M(new File(
//				Goable.file_image_path)) + "M";
		String cacheSize = Utility.getFileSize_M(new File(
				Goable.file_image_path));
		Spannable sp=new SpannableString(cacheSize+" M");
//		0xff0084b4
	    Utility.addSpan(sp, "",cacheSize, new ForegroundColorSpan(0xff0000ff));
		cache_size.setText(sp);
		
		version.setText("V"+getVersionName());
       
	}
	// 删除-临时图像
	private void deleteTmpImgs() {
		// debug,判断photoCapture
		int count = CheckInPhotoCaptureMainActivity.getEntryCnt();
		count=count+PhotoMainAc_LR.getEntryCnt();
		if (count > 0) {
			RewriteBuilderDialog
					.showSimpleDialog_Tip(this,
							"There're photos in \"Photo Capture\". Process them first !");
			return;
		}

		// dlg
		final RewriteDialog dlg = RewriteDialog.createDialog(this, 1);
		dlg.show();
		// 仍删除所有图片
		final File imgFolder = new File(Goable.file_image_path);
		new Thread() {
			public void run() {
				Utility.delFolder(imgFolder);
				// 恢复-图片目录
				initFolders();

				vMiddle.post(new Runnable() {

					@Override
					public void run() {
						dlg.dismiss();
//						cache_size.setText("0M");
						refreshImageCache();
						UIHelper.showToast(SetFunctionActivity.this, getString(R.string.sync_success));
					}
				});
			};
		}.start();
	}

	private void initFolders() {
		File thumbnail = new File(Goable.file_image_path_thumbnail);
		if (!thumbnail.exists()) {
			thumbnail.mkdirs();
		}
		File filePath = new File(Goable.file_path);
		if (!filePath.exists()) {
			filePath.mkdirs();
		}
		File upFile = new File(Goable.file_image_path_up);
		if (!upFile.exists()) {
			upFile.mkdirs();
		}
	}
	private void checkVersion() {
		RequestParams params = new RequestParams();
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
//				String version = getVersionName();
//				if (!StringUtil.isNullOfStr(version) && !version.equalsIgnoreCase(StringUtil.getJsonString(json, "version"))) {
					DownloadApk.loadApk(mActivity);
//				}else{
//					AlertUi.showAlertBuilder(mActivity, "Is the latest version.");
//				}
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}

		}.doGet(HttpUrlPath.LoadApkVersion, params, mActivity);
	}
	private String getVersionName() {
		String version = "";
		try {
			PackageManager packageManager = getPackageManager();
			android.content.pm.PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(getPackageName(), 0);
			version = packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}
//	@Override
//	protected void onBackBtnOrKey() {
//		if(!StoredData.saveinfo){
//			for(int i = 0;i<dbHelper.queryAllUserName().length;i++){
//				dbHelper.delete(dbHelper.queryAllUserName()[i]);
//			}
//		}
//		SetFunctionActivity.super.onBackBtnOrKey();
//	}
}
