package oso.ui.main;

import oso.base.BaseActivity;
import oso.ui.MainActivity;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.msg.TaskTypesAc;
import support.common.push.MyPushService;
import utility.Utility;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class WelcomeActivity extends Activity {

	public final static int From_None = 0; // 该页面调用位置
	public final static int From_Task_Notification = 1;
	public final static int From_Msg_Notification = 2;

	private int from = From_None;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		applyParams();

		// debug
		System.out.println("nimei>>WelcomeActivity.onCreate");

		Class target;
		if (from == From_Task_Notification) {
			// 移除-页面内提示
			BaseActivity.last_msg = "";
			// 若已登录,则至"任务列表"
			if (MainActivity.instance != null)
				target = TaskTypesAc.class;
			else
				target = Login.class;
		} else if (from == From_Msg_Notification) {
			// 若已登录,则至"消息列表"
			if (MainActivity.instance != null) {
				target = ChatTabActivity.class;
			} else
				target = Login.class;
		} else
			target = Login.class;

		// 修改语言配置
		Utility.updateConfigurationLanguage(getApplicationContext());

		Intent intent = new Intent(this, target);
		startActivity(intent);
		finish();
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

		// debug
		Intent in = new Intent(MyPushService.Action_MyPushService);
		in.setPackage(getPackageName());
		startService(in);
	}

	// =========传参===============================

	// 入参:from:页面来源,取From_x
	public static void initParams(Intent in, int from) {
		in.putExtra("from", from);
	}

	private void applyParams() {
		if (getIntent() == null || getIntent().getExtras() == null)
			return;
		Bundle params = getIntent().getExtras();
		from = params.getInt("from", From_None);
	}
}
