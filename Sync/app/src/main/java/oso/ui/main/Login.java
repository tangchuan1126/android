package oso.ui.main;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.SyncApplication;
import oso.ui.MainActivity;
import oso.ui.chat.ext.sipdroid.sipua.ui.Receiver;
import oso.ui.chat.util.SipUtil;
import oso.ui.chat.util.XmppTool;
import oso.ui.main.bean.LoginResultBean;
import oso.ui.main.bean.LoginResultBean.Perportraits;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteDialog;
import oso.widget.floatinglabelview.FloatingLabelView;
import oso.widget.permission.PermissionBean;
import support.AppConfig;
import support.anim.AnimUtils;
import support.common.AlertUi;
import support.common.UIHelper;
import support.common.bean.LoginPostname;
import support.dbhelper.DBHelper;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.key.BCSKey;
import support.key.LanguageKey;
import support.network.SimpleJSONUtil;
import utility.DisplayPictureUtil;
import utility.FormatterUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

/**
 * @author gcy
 * @ClassName: Login
 * @Description:用户登录
 * @date 2014年5月20日 上午9:53:23
 */
public class Login extends Activity {
	ArrayAdapter<String> adapter;
	private Button btn_yes;
	private ImageView loginscan;
	private EditText username, password;
	// private Spinner spinner;
	private String spValue;
	private SharedPreferences sp;
	private Context context;
//	private ConfigDao configDao;
	private Configuration config;
	private Resources resources;
	private DBHelper dbHelper;
	private PopupWindow popView;
	private MyAdapter dropDownAdapter;

	private View changeLanguageBtn;
	private TextView changeServerBtn;
	private ImageView langIv;
	private TextView langTv;

	private final String longuageDB[] = { "US", "CHINSES", "Japanese", "Russian", "French" };
    private List<String> machinelists;
    
    private List<LoginPostname> allpost = new ArrayList<LoginPostname>();

	private TextView last_version;
	private static String avatarUrlPeer = null;

	/**
	 * 1.进入这个界面的时候每次都去服务器上更新最新的版本 2.在有网络的情况下
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		initData();
		adddate();// 添加数据
		setValue(); // 获取控件
		setEvent(); // 绑定事件
		// setSpinner(); // 添加下拉列表
		getMemory(); // 返填用户名密码
		checkVersion();
	}

	private void initData() {
		context = Login.this;
		resources = getResources();
		config = resources.getConfiguration();
//		configDao = new ConfigDao(context);
		initLanguageBtn();
		initServerUrlBtn();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		initCursor();
	}

	private void initCursor() {
		if (TextUtils.isEmpty(username.getText().toString())) {
			username.requestFocus();
		} else {
			password.requestFocus();
		}
	}

	/**
	 * @Description:检测当前语言配置 国际化界面
	 */
//	private void updateLanguage() {
//		DisplayMetrics dm = new DisplayMetrics();
//		// pname_username = (TextView) findViewById(R.id.pname_username);
//		// pname_password = (TextView) findViewById(R.id.pname_password);
//		String sp_la = configDao.getConfigValue("sp_la");
//		for (int i = 0; i < longuageDB.length; i++) {
//			if (longuageDB[i].equals(sp_la)) {
//				getWindowManager().getDefaultDisplay().getMetrics(dm);
//				switch (i) {
//				case 0:
//					config.locale = Locale.ENGLISH;
//					resources.updateConfiguration(config, dm);
//					break;
//				case 1:
//					config.locale = Locale.SIMPLIFIED_CHINESE;
//					resources.updateConfiguration(config, dm);
//					break;
//				default:
//					break;
//				}
//				btn_yes.setText(resources.getText(R.string.login_login));
//				break;
//			}
//		}
//	}

	public void adddate() {
		machinelists = new ArrayList<String>();
		machinelists.add("Please select the machine number");
		machinelists.add("BJ-01");
		machinelists.add("BJ-02");
		machinelists.add("GZ-01");
		machinelists.add("GZ-02");
		machinelists.add("LA-01");
		machinelists.add("LA-02");
		machinelists.add("other");
	}

	// private void inittitleimage(String name){
	// // 头像显示 url
	// initAvatarUrl(FormatterUtil.getHalfname(name));
	// if(TextUtils.isEmpty(avatarUrlPeer)){
	// login_titleimage.setImageResource(R.drawable.icon);
	// }else{
	// ImageLoader.getInstance().displayImage(HttpUrlPath.basePath+avatarUrlPeer,
	// login_titleimage,
	// DisplayPictureUtil.getAvatarDisplayImageOptions());
	//
	// }
	// }
	private void initAvatarUrl(String unamePeer) {
		// 查对方头像url
		@SuppressWarnings("unchecked")
		HashMap<String, Set<String>> extAccMap = (HashMap<String, Set<String>>) (StoredData.getExtAccMap());
		Set<String> set = extAccMap.get(unamePeer);
		if (set == null)
			return;
		Iterator<String> it = set.iterator();

		while (it.hasNext()) {
			String result = it.next();
			if (!TextUtils.isEmpty(result) && result.startsWith("upload/account/")) {
				avatarUrlPeer = result;
				return;
			} else {
				avatarUrlPeer = "";
			}
		}
	}

	// 返填用户名密码
	public void getMemory() {
		// SimpleBroadcastReceiver.setOnPackageListener(new OnPackageListener()
		// {
		// @Override
		// public void onPackchange() {
		// cleanSP();
		// }
		// });
		sp = getSharedPreferences("loginSave", MODE_PRIVATE);
		if(StoredData.saveinfo){
			
			String sp_name = sp.getString("name", "");
			String sp_pwd = sp.getString("pwd", "");
			// String machine = sp.getString("machine", "");
			username.setText(sp_name);
		}
	
		// password.setText(sp_pwd);
		// if (machine != null && machine.length() > 0) {
		// for (int i = 0; i < machinelists.size(); i++) {
		// if (machine.equals(machinelists.get(i).toString())) {
		// spinner.setSelection(i);
		// break;
		// }
		// }
		// }
		// inittitleimage(sp_name);
	}

	// 加载 按钮事件
	public void setEvent() {
		btn_yes = (Button) findViewById(R.id.btn_yes);
		btn_yes.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				login();
			}
		});
		password.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP) {
					if (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_ENTER) {
						login();
						return true;
					}
				}
				return false;
			}
		});
		loginscan.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
				if (imm.hideSoftInputFromWindow(username.getWindowToken(), 0)) {
					imm.showSoftInput(username, 0);
					bohaos();
					// 软键盘已弹出
				} else {
					// 软键盘未弹出
					// 如果有已经登录过账号
					if(StoredData.saveinfo){
						if (dbHelper.queryAllUserName().length > 0) {
							initPopView(dbHelper.queryAllUserName());
							if (!popView.isShowing()) {
								loginscan.setImageResource(R.drawable.title_btn_data_up);
								// debug,-10,-7
								// int offset=Utility.dip2px(Login.this, -7);
								// popView.showAsDropDown(username,offset,offset);
								showPopView_users();

								// popView.showAtLocation(username,
								// Gravity.NO_GRAVITY, 0,0);
							} else {
								loginscan.setImageResource(R.drawable.title_btn_data);
								popView.dismiss();
							}
						}
					}
					
				}
			}
		});
	}

	private void showPopView_users() {
		int offset = Utility.dip2px(Login.this, -7);
		popView.showAsDropDown(username, offset, offset);
	}

	private void login() {
		String name = username.getText().toString();
		String pwd = password.getText().toString();
		if (StringUtil.isNullOfStr(name) || StringUtil.isNullOfStr(pwd)) { // 判断
			UIHelper.showToast(Login.this, resources.getString(R.string.login_kong)).show();
			return;
		}
		// if
		// (spValue.equals(resources.getString(R.string.login_moren))) {
		// UIHelper.showToast(Login.this,
		// resources.getString(R.string.login_moren)+"",
		// Toast.LENGTH_LONG)
		// .show();
		// return;
		// }
		if (isNetworkConnected(context)) {
			SubmitVerification(name, pwd, true);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 处理扫描结果（在界面上显示）
		if (requestCode == 3) {
			if (resultCode == RESULT_OK) {
				Bundle bundle = data.getExtras();
				String scanResult = bundle.getString("result");
				username.setText(scanResult + "\n");
			}
		}
	}

	private String getVersionName() {
		String version = "";
		try {
			PackageManager packageManager = getPackageManager();
			android.content.pm.PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(getPackageName(), 0);
			version = packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}

	private void SubmitVerification(final String name, final String pwd, boolean checkOnline) {

		// 先清空cookie,防止"不同服务器的cookie"影响
		PersistentCookieStore cookie = new PersistentCookieStore(Login.this);
		cookie.clear();

		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		dialog.setTitle(resources.getString(R.string.sys_holdon));
		dialog.setMessage(getString(R.string.login_inthelogin));
		dialog.setCancelable(true);
		Goable.initGoable(context);
		final RequestParams params = new RequestParams();
		params.add("Method", "Login");
		params.add("LoginAccount", name);
		params.add("Password", pwd);
		params.add("version", getVersionName());
		params.add("ischeckonline", checkOnline ? "1" : "0");

		new SimpleJSONUtil() {
			/**
			 * 重写方法设置不填登陆信息
			 */
			@Override
			public boolean setLoginInfoParams() {
				return false;
			}

			@Override
			public boolean useTheCustom(JSONObject result) {
				try {
					// 登录成功时 // portraits
					if (result.getInt("ret") == 1) {
						settingPermissions(result);
						settingUserInfo(result);
						StoredData.isLogin_inRam = true;
						login_of_delay();

						// sip登陆
						login_sip(Login.this, StoredData.getName(), StoredData.getPwd());
						loginjson(result, name, pwd);
					} else if (StringUtil.getJsonInt(result, "err") == BCSKey.AppVersionException) {
						AlertUi.showVersionError(context);
					} else if (StringUtil.getJsonInt(result, "err") == BCSKey.UserIsOnLineCantLoginException) {
						tipLoginElse();
					} else {
						UIHelper.showToast(Login.this, resources.getString(R.string.login_err), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return true;
			}

			;

			@Override
			public void handReponseJson(JSONObject json) {
			}
		}.doPost(HttpUrlPath.seachLoginUser, params, context);
	}

	// -------debug 输出json
	public static void writeTxtFile(String strcontent, String strFilePath) {
		// 每次写入时，都换行写
		String strContent = strcontent + "\n";
		try {
			File file = new File(strFilePath);
			if (!file.exists()) {
				Log.d("TestFile", "Create the file:" + strFilePath);
				file.createNewFile();
			}
			RandomAccessFile raf = new RandomAccessFile(file, "rw");
			raf.seek(file.length());
			raf.write(strContent.getBytes());
			raf.close();
		} catch (Exception e) {
			Log.e("TestFile", "Error on write File.");
		}
	}

	private void settingUserInfo(JSONObject result) {
		final LoginResultBean resultBean = new Gson().fromJson(result.toString(), LoginResultBean.class);
		String allpostname = "";
		if(allpost != null && allpost.size() > 0){
			allpost.clear();
		}
        for(int i=0;i<resultBean.data.get(0).departments.size();i++){
        	String itempostname = resultBean.data.get(0).departments.get(i).postname+",";
        	allpostname = allpostname+itempostname;
        	LoginPostname item = new LoginPostname();
        	item.Managername = resultBean.data.get(0).departments.get(i).deptname;
        	item.allpostname = resultBean.data.get(0).departments.get(i).postname;
        	item.postid = resultBean.data.get(0).departments.get(i).postid;
        	allpost.add(item);
        }
        allpostname = allpostname.substring(0, (allpostname.length()-1)).toString();
//        String postname = resultBean.data.get(0).departments.get(0).postname;
//        String postid = resultBean.data.get(0).departments.get(0).postid;
        StoredData.setAllpost(allpost);
        StoredData.setPostname(allpostname);
        String postid = resultBean.data.get(0).departments.get(0).postid;
        StoredData.setPostId(postid);
		LoginResultBean.Data data = resultBean.data.get(0);
		int size = data.portraits == null ? 0 : data.portraits.size();
		StoredData.clearExtAcc();
		StoredData.clearAvatar();
		// 设置新的头像和默认头像信息进sp
		for (int i = 0; i < size; i++) {
			LoginResultBean.Portraits portraits = data.portraits.get(i);
			if (portraits.warehouses == null || portraits.warehouses.size() == 0)
				continue; // 过滤账号
			Set<String> strSet = new HashSet<String>();
			strSet.add(portraits.file_path);
			strSet.add(portraits.warehouses.get(0).warename);
			StoredData.saveExtAcc(portraits.account, strSet);
		}

		final int perSize = data.perportraits == null ? 0 : data.perportraits.size();
		if (perSize > 0) {
			new Thread() {
				public void run() {
					for (int i = 0; i < perSize; i++) {
						Perportraits perportraits = resultBean.data.get(0).perportraits.get(i);
						String activity = perportraits.activity; // 是否是默认头像
						String imgId = perportraits.id;
						String avatarUrl = perportraits.file_path;
						if (Integer.valueOf(activity) == 1)
							StoredData.setDefaultAvatarUrl(imgId, avatarUrl);
						StoredData.setAvatarUrl(imgId, avatarUrl);
					}
				}

				;
			}.start();

		}
	}

	private void settingPermissions(JSONObject result) {
		JSONArray ja = StringUtil.getJsonArrayFromJson(result, "permissions");
		if (ja != null) {
			Log.i("login= ", ja.toString());
			// if (PermissionBean.permissions != null) {
			// PermissionBean.permissions.clear();
			// PermissionBean.permissions = PermissionBean.readPermissions();
			// }
			List<PermissionBean> permissions = new Gson().fromJson(ja.toString(), new TypeToken<List<PermissionBean>>() {
			}.getType());
			PermissionBean.savePermissions(permissions);
		}
		// PermissionBean.savePermissions(PermissionBean.getDeBugPermission(context));
	}

	private void tipLoginElse() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setMessage(getString(R.string.enforcement_login));
		builder.setCancelable(false);
		builder.setCanceledOnTouchOutside(false);
		builder.setPositiveButton(getString(R.string.login_login), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 重新登录
				String name = username.getText().toString();
				String pwd = password.getText().toString();
				SubmitVerification(name, pwd, false);
			}
		});
		builder.setNegativeButton(getString(R.string.sync_cancel), null).show();
	}

	/**
	 * 登录openfire
	 */
	private void login_of_delay() {
		btn_yes.postDelayed(new Runnable() {

			@Override
			public void run() {
				XmppTool.getThis().login_of();
			}
		}, 1000);
	}

	/**
	 * 登陆sip server 用于拨打网络电话
	 */
	private void login_sip(Context context, String account, String pwd) {
		if (!AppConfig.Debug_SipCall)
			return;
		String sipServer = HttpUrlPath.getSipServerUrl();
		SipUtil.initSipSetting(context, account, pwd, sipServer);
		Receiver.engine(context).StartEngine();
		Receiver.engine(context).register();
	}

	/**
	 * 1.这个以后做.在进入这个界面的时候去查询是不是更新的条目。(但是必须是英文)
	 *
	 * @descrition
	 */
	private void checkVersion() {
		RequestParams params = new RequestParams();
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				String version = getVersionName();
				if (!StringUtil.isNullOfStr(version) && version.compareToIgnoreCase(StringUtil.getJsonString(json, "version")) < 0) {
					last_version.setText(getString(R.string.login_new_version) + StringUtil.getJsonString(json, "version"));
					AlertUi.showVersionError(context);
				}
			}

			@Override
			public void handFinish() {
			}

			@Override
			public void handFail() {
			}

		}.doGet(HttpUrlPath.LoadApkVersion, params, context);
	}

	// public void cleanSP() {
	// Editor editor = sp.edit(); // 选中了缓存帐号密码
	// editor.putBoolean("choose", false);
	// editor.putString("name", "");
	// editor.putString("pwd", "");
	// editor.putString("machine", "");
	// editor.putString("ps_name", "");
	// editor.putString("ps_type", "");
	// editor.putString("adgid", "");
	// editor.putString("adid", "");
	// editor.putString("username", "");
	// editor.putString("ps_id", "");
	// editor.putString("projsid", "");//人员权限
	// editor.commit();
	// username.setText("");
	// password.setText("");
	// }

	// 缓存-用户信息
	private void loginjson(JSONObject result, final String name, final String pwd) throws JSONException {

		JSONArray jsonArray = result.getJSONArray("data");
		JSONObject object = (JSONObject) jsonArray.get(0);
		String adid = StringUtil.getJsonString(object, "userid");

		String lastAdid = StoredData.getAdid();
		// 若账号变更
		if (adid != null && !adid.equals(lastAdid))
			StoredData.onUserChange();

		Editor editor = sp.edit(); // 选中了缓存帐号密码
		editor.putBoolean("choose", true);
		editor.putString("name", name);
		editor.putString("pwd", pwd);
		editor.putString("machine", spValue);
		// debug
		// editor.putString("ps_id", StringUtil.getJsonString(object, "ps_id"));
		// editor.putString("ps_name", StringUtil.getJsonString(object,
		// "psname"));
		// editor.putString("ps_type", StringUtil.getJsonString(object,
		// "pstype"));
		// editor.putString("adgid", StringUtil.getJsonString(object, "adgid"));
		// editor.putString("projsid", object.optString("projsid"));//人员权限

		// debug
		JSONArray jaDep = object.optJSONArray("departments");
		if (!StringUtil.isNullForJSONArray(jaDep)) {
			String projsid = "";
			String adgid = "";
			for (int i = 0; i < jaDep.length(); i++) {
				JSONObject joDep = jaDep.optJSONObject(i);
				if (joDep != null) {
					projsid += joDep.optString("postid") + ",";
					adgid += joDep.optString("deptid") + ",";
				}
			}
			editor.putString("projsid", projsid);
			editor.putString("adgid", adgid);
		}
		JSONArray jaWh = object.optJSONArray("warehouses");
		if (jaWh != null && jaWh.length() > 0) {
			JSONObject joWh = jaWh.optJSONObject(0);
			if (joWh != null) {
				editor.putString("ps_id", joWh.optString("wareid"));
				editor.putString("ps_name", joWh.optString("warename"));
			}
		}

		editor.putString("adid", adid);
		editor.putString("username", StringUtil.getJsonString(object, "username"));
		editor.commit();

//		configDao.updateConfigDao("login_adid", name);
//		configDao.updateConfigDao("login_psd", pwd);
		StoredData.saveUser(name, pwd);;
		Goable.LoginAccount = "";
		Goable.Password = "";
		Goable.Machine = "";
		dbHelper.insertOrUpdate(name, pwd, 0);

		startActivity(new Intent(Login.this, MainActivity.class));


		username.setText("");
		username.requestFocus();
		password.setText("");
		// spinner.setSelection(0);
		// login_titleimage.setImageResource(R.drawable.icon);

		// push绑定,debug
		/* GCMIntentService.bindGCM(); */
	}

	// 添加机器号到下拉列表
	// public void setSpinner() {
	// // 将可选内容与ArrayAdapter连接起来
	// adapter = new ArrayAdapter<String>(this, R.layout.n_login_spinner_style,
	// machinelists);
	// // adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
	// // adapter = ArrayAdapter.createFromResource(this, machinelist,
	// // android.R.layout.simple_spinner_item);
	// // 设置下拉列表的风格
	// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	// // 将adapter 添加到spinner中
	// spinner.setAdapter(adapter);
	// // 添加事件Spinner事件监听
	// spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	//
	// @Override
	// public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long
	// arg3) {
	// // TODO Auto-generated method stub
	// spValue = adapter.getItem(arg2).toString();
	// }
	//
	// @Override
	// public void onNothingSelected(AdapterView<?> arg0) {
	// // TODO Auto-generated method stub
	//
	// }
	// });
	// // 设置默认值
	// spinner.setVisibility(View.GONE);
	// }

	// 监听获取选择的下拉列表的值
	public class SpinnerSelectedListener implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			spValue = adapter.getItem(arg2).toString();

		}

		public void onNothingSelected(AdapterView<?> arg0) {

		}
	}

	private TextView tvCurVer;

	public void setValue() {
		dbHelper = new DBHelper(this);
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		// login_titleimage = (ImageView) findViewById(R.id.login_titleimage);
		// spinner = (Spinner) findViewById(R.id.my_spinner);
		loginscan = (ImageView) findViewById(R.id.loginbtn);

		tvCurVer = (TextView) findViewById(R.id.tvCurVer);
		last_version = (TextView) findViewById(R.id.last_version);

		initLoginUserName();

		// 初始化
		tvCurVer.setText(getString(R.string.login_version) + Utility.getVersionName(this));
	}

	private void initServerUrlBtn() {
		changeServerBtn = (TextView) findViewById(R.id.changeServerBtn);
		changeServerBtn.setText(StoredData.readBaseServerName());
		changeServerBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPasswordDialog();
			}
		});
	}

	private void initLanguageBtn() {
		changeLanguageBtn = findViewById(R.id.changeLanguageBtn);
		langIv = (ImageView) findViewById(R.id.langIv);
		langTv = (TextView) findViewById(R.id.langTv);
		boolean isChr = StoredData.readLanguageID() == 1;
		langIv.setImageResource(isChr ? R.drawable.ic_cn : R.drawable.ic_en);
		langTv.setText(LanguageKey.getLanguageKey(StoredData.readLanguageID()));
		changeLanguageBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showLanguageDialog();
			}
		});
	}

	private void showLanguageDialog() {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(this);
		builder.setTitle(getString(R.string.login_dlg_language));
		builder.setItems_singleChoice(LanguageKey.languages, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				StoredData.saveLanguageName(position);
				StoredData.saveIsFirstSelectLanguage(false);
				if (position == 0) {
					config.locale = Locale.ENGLISH;
				}
				if (position == 1) {
					config.locale = Locale.SIMPLIFIED_CHINESE;
				}
				final RewriteDialog progressDialog = RewriteDialog.createDialog(context, 1);
				progressDialog.show();
				SyncApplication.getThis().getHandler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (progressDialog != null && progressDialog.isShowing())
							progressDialog.dismiss();
						resources.updateConfiguration(config, null);
						Login.this.finish();
						overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
						Utility.popTo(Login.this, Login.class);
						overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
					}
				}, 1000);
			}
		}, StoredData.readLanguageID());
		builder.show();
	}

	private void showPasswordDialog() {
		final EditText et = new EditText(context);
		// debug
		et.setBackgroundResource(R.drawable.search_bar_edit_selector);
		int padding = Utility.pxTodip(context, 10);
		et.setPadding(padding, padding, padding, padding);
		et.setHint(R.string.login_password);
		// et.setInputType(0x81);
		et.setSingleLine(true);
		et.setTransformationMethod(PasswordTransformationMethod.getInstance());

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(R.string.login_dlg_select_server);
		builder.setContentView(et);
		// 若密码错误,则不dismiss
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setNegativeButton(R.string.sync_cancel, null);
		builder.setPositiveButton(R.string.sync_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Utility.colseInputMethod(context, et);
				// if (et.getText().toString().trim().equals("vvme.com")) {
				// showSelectServerUrlDialog();
				// } else {
				// UIHelper.showToast(context, "Password Error!",
				// Toast.LENGTH_SHORT).show();
				// }
				// debug
				showSelectServerUrlDialog(et, (RewriteBuilderDialog) dialog);
			}
		});

		final RewriteBuilderDialog dlg = builder.show();
		// 监听事件
		et.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (Utility.isEnterUp(event)) {
					showSelectServerUrlDialog(et, dlg);
					return true;
				}
				return false;
			}
		});

	}

	private void showSelectServerUrlDialog(EditText et, RewriteBuilderDialog dlg) {

		// 若密码不同
		if (!et.getText().toString().trim().equals("vvme.com")) {
			UIHelper.showToast(context, getString(R.string.login_err_pass), Toast.LENGTH_SHORT).show();
			return;
		}
		// 断开openfire,防止切换服务器
		new Thread() {
			public void run() {
				XmppTool.getThis().loginOut_of();
			}
		}.start();

		// 相同时,才dismiss
		Utility.colseInputMethod(context, et);
		dlg.dismiss();

		final RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(R.string.login_dlg_select_server);
		builder.setRightButtonEnable(true, 0, new OnClickListener() {
			@Override
			public void onClick(View v) {
				builder.getDialog().dismiss();
				showAddServerDlg();
			}
		});
		final String[] datas = HttpUrlPath.getServerNames();
		builder.setNegativeButton(R.string.sync_cancel, null);
		builder.setAdapter(new ArrayAdapter<String>(context, R.layout.simple_sp_item_1, datas), new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				StoredData.saveBaseServerName(datas[position]);
				updateServerUrl(Login.this);
				changeServerBtn.setText(StoredData.readBaseServerName());
			}
		});
		builder.show();
	}

	/**
	 * @description 添加Server Dialog
	 */
	private void showAddServerDlg() {
		View view = View.inflate(context, R.layout.dialog_add_server_layout, null);
		final FloatingLabelView flvName = (FloatingLabelView) view.findViewById(R.id.etAddServer_name);
		final FloatingLabelView flvServerUrl = (FloatingLabelView) view.findViewById(R.id.etAddServer_url);

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setTitle(getString(R.string.sync_add_server));
		builder.setContentView(view);
		// 若密码错误,则不dismiss
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(R.string.sync_yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (!isValid(flvName.getEditText(), flvServerUrl.getEditText())) return;

				// 存入StoredData
				StoredData.saveNewServer(flvName.getText(), HttpUrlPath.formatUrl(flvServerUrl.getText()));
				StoredData.saveBaseServerName(flvName.getText());

				updateServerUrl(Login.this);
				changeServerBtn.setText(StoredData.readBaseServerName());
				dialog.dismiss();
			}
		});
		builder.show();
	}


	/**
	 * @description 添加Server  判断是否有效
	 * @param etName
	 * @param etServerUrl
	 * @return
	 */
	private boolean isValid(EditText etName, EditText etServerUrl) {
		boolean isValid = true;
		String name = etName.getText().toString();
		String server = etServerUrl.getText().toString();

		// 校验重复name
		String[] serverNames = HttpUrlPath.getServerNames();
		for(int index = 0; index < serverNames.length; index ++) {
			if(serverNames[index].equals(name)) {
				AnimUtils.shake(context, etName, true);
				Utility.etRequestFocus(etName, true);
				UIHelper.showToast(context, getString(R.string.sync_repeat));
				isValid = false;
				return isValid;
			}
		}

		if(TextUtils.isEmpty(name)) {
			AnimUtils.shake(context, etName, true);
			Utility.etRequestFocus(etName, true);
			isValid = false;
			return isValid;
		}

		if(TextUtils.isEmpty(server)) {
			AnimUtils.shake(context, etServerUrl, true);
			Utility.etRequestFocus(etServerUrl, true);
			isValid = false;
			return isValid;
		} else {
			// 校验IP地址是否正确
			String rule = "((?:(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d))))";
			Matcher matcher = Pattern.compile(rule).matcher(server);
			if(!matcher.matches()) {
				AnimUtils.shake(context, etServerUrl, true);
				Utility.etRequestFocus(etServerUrl, true);
				// 光标默认移动到最后
				UIHelper.showToast(context, getString(R.string.sync_wrong_ip));
				isValid = false;
				return isValid;
			}
		}

		return isValid;
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Login.updateServerUrl(this);
	}

	public static void updateServerUrl(Context c) {
		HttpUrlPath.initBaseUrls();
		// 向gcm发送广播
		Intent intent = new Intent("android.intent.action.INIT_URL");
		intent.putExtra("basePath", HttpUrlPath.basePath);
		c.sendBroadcast(intent);

	}

	private void initLoginUserName() {
		String[] usernames = dbHelper.queryAllUserName();
		if (usernames.length > 0&&StoredData.saveinfo) {
			String tempName = usernames[usernames.length - 1];
			username.setText(tempName);
			username.setSelection(tempName.length());
			String tempPwd = dbHelper.queryPasswordByName(tempName);
			password.setText(tempPwd);
		}
		username.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				password.setText("");
				// inittitleimage(s.toString());
				// login_titleimage.setImageResource(R.drawable.icon);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	public void bohaos() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(Login.this.findViewById(R.id.login_main).getWindowToken(), 0);

	}

	public boolean isNetworkConnected(Context context) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
			if (mNetworkInfo != null) {
				return mNetworkInfo.isAvailable();
			} else {
				UIHelper.showToast(context, resources.getString(R.string.login_network), Toast.LENGTH_SHORT).show();
			}
		}
		return false;
	}

	private void initPopView(String[] usernames) {
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < usernames.length; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("name", usernames[i]);
			map.put("drawable", R.drawable.emotionstore_progresscancelbtn);
			list.add(map);
		}
		dropDownAdapter = new MyAdapter(this, list, R.layout.dropdown_item, new String[] { "name", "drawable" }, new int[] { R.id.textview,
				R.id.delete });
		ListView lv = new ListView(this);
		lv.setAdapter(dropDownAdapter);

		// debug,背景偏执
		int offset = Utility.dip2px(this, 13);
		popView = new PopupWindow(lv, username.getWidth() + offset, ViewGroup.LayoutParams.WRAP_CONTENT);
		popView.setFocusable(true);
		popView.setOutsideTouchable(true);
		popView.setBackgroundDrawable(getResources().getDrawable(R.drawable.dialog_full_more));
		// popView.showAtLocation(username, Gravity.LEFT | Gravity.BOTTOM, 0,0);
		popView.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				loginscan.setImageResource(R.drawable.title_btn_data);
			}
		});
	}

	class MyAdapter extends SimpleAdapter {

		private List<HashMap<String, Object>> data;

		public MyAdapter(Context context, List<HashMap<String, Object>> data, int resource, String[] from, int[] to) {
			super(context, data, resource, from, to);
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(Login.this).inflate(R.layout.dropdown_item, null);
				holder.btn = (ImageButton) convertView.findViewById(R.id.delete);
				holder.tv = (TextView) convertView.findViewById(R.id.textview);
				holder.photo_image = (ImageView) convertView.findViewById(R.id.photo_image);
				holder.layout = convertView.findViewById(R.id.layout);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.tv.setText(data.get(position).get("name").toString());
			initAvatarUrl(FormatterUtil.getHalfname(data.get(position).get("name").toString()));
			if (TextUtils.isEmpty(avatarUrlPeer)) {
				holder.photo_image.setImageResource(R.drawable.account_circle);
			} else {
				ImageLoader.getInstance().displayImage(HttpUrlPath.basePath + avatarUrlPeer, holder.photo_image,
						DisplayPictureUtil.getChatAvatarDisplayImageOptions_Online());

			}
			holder.layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String[] usernames = dbHelper.queryAllUserName();
					username.setText(usernames[position]);
					password.setText(dbHelper.queryPasswordByName(usernames[position]));
					// inittitleimage(usernames[position]);
					popView.dismiss();
				}
			});
			holder.btn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					String[] usernames = dbHelper.queryAllUserName();
					if (usernames.length > 0) {
						dbHelper.delete(usernames[position]);
					}
					String[] newusernames = dbHelper.queryAllUserName();
					// if (newusernames.length > 1) {
					// initPopView(newusernames);
					// popView.showAsDropDown(username,-10,-10);
					// // popView.showAtLocation(username,Gravity.BOTTOM, 0,0);
					// } else {
					// popView.dismiss();
					// popView = null;
					// }

					// debug,还有账户 则刷新
					if (newusernames.length > 0) {
						// 先移除
						// initPopView(newusernames);
						// showPopView_users();
						// popView.showAsDropDown(username,-10,-10);
						// popView.showAtLocation(username,Gravity.BOTTOM, 0,0);

						// 移除-该条
						data.remove(position);
						notifyDataSetChanged();

					}
					// 移除
					else {
						popView.dismiss();
						popView = null;
					}

					password.setText("");
				}
			});
			return convertView;
		}
	}

	class ViewHolder {
		private TextView tv;
		private ImageButton btn;
		private ImageView photo_image;
		public View layout;
	}
}
