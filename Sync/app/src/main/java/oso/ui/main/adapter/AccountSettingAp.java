package oso.ui.main.adapter;

import java.util.List;

import support.common.bean.LoginPostname;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class AccountSettingAp extends BaseAdapter {

	private List<LoginPostname> arrayList;

	private LayoutInflater inflater;

	private Context context;

	public AccountSettingAp(Context context,List<LoginPostname> arrayList) {
		super();
		this.context = context;
		this.arrayList = arrayList;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return arrayList != null ? arrayList.size() : 0;
	}

	@Override
	public LoginPostname getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DoorHoder holder = null;
		if (convertView == null) {
			holder = new DoorHoder();
			convertView = inflater.inflate(R.layout.account_setting_post_item, null);
			holder.tvWarename = (TextView) convertView.findViewById(R.id.tvWarename);
			holder.tvRole = (TextView) convertView.findViewById(R.id.tvRole);
			convertView.setTag(holder);
		} else {
			holder = (DoorHoder) convertView.getTag();
		}
		LoginPostname temp = arrayList.get(position);
		holder.tvWarename.setText(temp.Managername+": ");
		holder.tvRole.setText(temp.allpostname.substring(0, temp.allpostname.length()-1).toString());
		return convertView;
	}

}

class DoorHoder {

	public TextView tvWarename;
	public TextView tvRole;
	

}
