package oso.ui.main;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.ui.MainActivity;
import oso.ui.main.adapter.AccountSettingAp;
import oso.ui.main.bean.UserAvatar;
import oso.widget.DlgImg;
import oso.widget.HorizontalListView;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.RecycleImageView;
import support.common.UIHelper;
import support.common.bean.LoginPostname;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.DisplayPictureUtil;
import utility.FileUtil;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

public class AccountSettingsActivity extends BaseActivity {
	public static final int MODIFYPWD = 123;
	private static final int AVATAR_DEFAULT = 1; // 默认头像
	private static final int AVATAR_DEFAULT_CLEAR = 0; // 非默认头像
	private static final int WHICH_SET_NEW_AVATAR = 0;
	private static final int WHICH_DELETE_AVATAR = 1;
	private static final int WHICH_OPEN_GALLERY = 0;
	private static final int WHICH_TAKE_PHOTO = 1;

	private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
	private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
	private static final int PHOTO_REQUEST_CROP = 3;// 结果

	private CheckBox cbMonitor;
	private EditText etFullname;
	private EditText etPassword;
	private HorizontalListView lvPhoto;
	private AvatarAdapter avatarAdp = new AvatarAdapter();
	private List<UserAvatar> avatarUrlList = new ArrayList<UserAvatar>();
	private List<LoginPostname> allpost;
	private List<LoginPostname> allposttwo;
	/* 头像名称 */
	private static final String PHOTO_FILE_NAME = new Date().getTime()
			+ "_avatar.jpg";
	private static final String PHOTO_FILE_PATH = getPath(Goable
			.getDir_TmpImg().getAbsolutePath());
	private File tempFile, tmpSrcFile;
	private ListView accountlist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_account_setting, 0);

		initView();

		initData();

		initListener();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {

		case PHOTO_REQUEST_GALLERY: // 相册选取
			handleGallery(data);
			break;

		case PHOTO_REQUEST_CAMERA: // 照相机选取
			if (resultCode == Activity.RESULT_OK) {
				handleCam();
			}
			break;

		case PHOTO_REQUEST_CROP: // 返回截取的图片
			handleCrop(data);
			break;

		case MODIFYPWD: // 修改密码
			etPassword.setText(data.getStringExtra("newpwd"));
			etPassword.setTextColor(Color.RED);
			break;

		}

	}

	private void initData() {
		showAvatar();
	}

	private void initListener() {

		tempFile = getFile(PHOTO_FILE_PATH + "/" + PHOTO_FILE_NAME);

		final String[] menu = {
				getString(R.string.account_setting_set_portrait),
				getString(R.string.sync_delete) };
		lvPhoto.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					final int position, long id) {
				if (0 == position)
					return true; // 0为添加头像Button
				new RewriteBuilderDialog.Builder(AccountSettingsActivity.this)
						.setTitle(getString(R.string.sync_operation))
						.setNegativeButton(getString(R.string.sync_cancel),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}

								})
						.setArrowItems(menu,
								new AdapterView.OnItemClickListener() {

									@Override
									public void onItemClick(
											AdapterView<?> parent, View view,
											int itemPosition, long id) {
										switch (itemPosition) {
										case WHICH_SET_NEW_AVATAR:
											// 设置为主头像
											setDefaultAvatar(position - 1);
											break;
										case WHICH_DELETE_AVATAR:
											// 删除头像
											delAvatar(
													position - 1,
													avatarUrlList
															.get(position - 1).imgId);
											break;
										}
									}

								}).show();

				return true;
			}
		});
		lvPhoto.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0) {
					openSelectDialog();
					return;
				} else {
					String avatarUrl = HttpUrlPath.basePath
							+ avatarUrlList.get(position - 1).getAvatarUrl();
					DlgImg.show(AccountSettingsActivity.this, avatarUrl);
				}
			}

		});
	}

	// 可以选择从相册 也可以直接拍照
	private void openSelectDialog() {
		new RewriteBuilderDialog.Builder(AccountSettingsActivity.this)
				.setTitle(getString(R.string.sync_operation))
				.setNegativeButton(getString(R.string.sync_cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}

						})
				.setArrowItems(
						new String[] {
								getString(R.string.account_setting_choose_ingallery),
								getString(R.string.account_setting_take_photo) },
						new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int itemPosition, long id) {
								switch (itemPosition) {
								case WHICH_OPEN_GALLERY:
									openGallery();
									break;
								case WHICH_TAKE_PHOTO:
									openCamera();
									break;
								}
							}

						}).show();
	}

	/**
	 * 初始化视图
	 */
	private void initView() {
		allpost = new ArrayList<LoginPostname>();
		allpost = (List<LoginPostname>) getIntent().getSerializableExtra(
				"allpost");

		setTitleString(getString(R.string.account_setting_title_user_info));

		showRightButton(R.drawable.submit_button, "",
				new SubmitOnClickListener());

		lvPhoto = (HorizontalListView) findViewById(R.id.photoLv);
		lvPhoto.setAdapter(avatarAdp);
		cbMonitor = (CheckBox) findViewById(R.id.cbMonitor);
		cbMonitor.setOnCheckedChangeListener(new PwdEyeListener());
		etFullname = (EditText) findViewById(R.id.etFullname);
		etFullname.setText(StoredData.getUsername());
		etPassword = (EditText) findViewById(R.id.etPassword);
		etPassword.setText(StoredData.getPwd());
		accountlist = (ListView) findViewById(R.id.accountlist);
		((TextView) findViewById(R.id.tvStorage)).setText(StoredData
				.getPs_name());

		showRoles();

	}

	/**
	 * 显示头像
	 */
	private void showAvatar() {
		Set<String> avatarUrlSet = StoredData.getAvatarUrlSet();
		String defaultAvatarUrl = StoredData.getDefaultAvatarUrl();
		if (defaultAvatarUrl.indexOf(",") > 0) {
			defaultAvatarUrl = defaultAvatarUrl.split(",")[0]; // 此处已赋值为imgId
		}
		Iterator<String> it = avatarUrlSet.iterator();
		while (it.hasNext()) {
			String[] arr = it.next().split(",");
			String imgId = arr[0];
			String avatarUrl = arr[1];
			boolean isActivity = defaultAvatarUrl.equals(imgId);
			if (isActivity) {
				avatarUrlList.add(0, new UserAvatar(imgId, avatarUrl,
						AVATAR_DEFAULT));
			} else {
				avatarUrlList.add(new UserAvatar(imgId, avatarUrl,
						AVATAR_DEFAULT_CLEAR));
			}
		}
		avatarAdp.notifyDataSetChanged();
	}

	/**
	 * 登陆后返回个人信息 展示Role
	 */
	private void showRoles() {
		// ((TextView)
		// findViewById(R.id.tvWarename)).setText(StoredData.getPs_name() +
		// ": ");
		// ((TextView)
		// findViewById(R.id.tvRole)).setText(StoredData.getPostname());
		if (Utility.isNullForList(allpost)) {
			return;
		}
		allposttwo = new ArrayList<LoginPostname>();
		allposttwo.add(allpost.get(0));
		if (allposttwo.size() > 0 && allposttwo != null) {
			allposttwo.get(0).allpostname = allposttwo.get(0).allpostname + ",";
			for (int i = 1; i < allpost.size(); i++) {
				int k = 0;
				for (int j = 0; j < allposttwo.size(); j++) {
					if (allpost.get(i).Managername
							.equals(allposttwo.get(j).Managername)) {
						allposttwo.get(j).allpostname = allposttwo.get(j).allpostname
								+ allpost.get(i).allpostname + ",";
					} else {
						k++;
					}
				}
				if (k == allposttwo.size()) {
					LoginPostname additem = allpost.get(i);
					additem.allpostname = additem.allpostname + ",";
					allposttwo.add(additem);
				}
			}
			accountlist.setAdapter(new AccountSettingAp(mActivity, allposttwo));
		}

	}

	/**
	 * 更新昵称和密码
	 * 
	 * @param fullName
	 * @param newPwd
	 */
	private void updateUserInfo(final String fullName, final String newPwd) {
		RequestParams params = new RequestParams();
		params.put("flag", "android");
		params.put("adid", StoredData.getAdid());
		params.put("name", etFullname.getText().toString());
		params.put("pwd", etPassword.getText().toString());
		params.put("android", "PUT");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				if (1 == json.optInt("ret")) {
					UIHelper.showToast(getString(R.string.set_update_success));
					Intent data = new Intent().putExtra("username", fullName);
					setResult(MainActivity.CODE_ACCOUNT_SETTING, data);
					StoredData.setUsername(fullName);
					StoredData.setPwd(newPwd);
					AccountSettingsActivity.this.finish();
					overridePendingTransition(R.anim.push_from_right_out,
							R.anim.push_from_right_in);
				}
			}

		}.doPost(HttpUrlPath.accountUpdateInfoUrl, params, this);
	}

	/**
	 * 删除头像
	 * 
	 * @param position
	 * @param imgId
	 */
	private void delAvatar(final int position, final String imgId) {
		RequestParams params = new RequestParams();
		params.put("id", imgId);
		params.put("android", "DELETE");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				if (1 == json.optInt("ret")) {
					refreshAvatar();
					UIHelper.showToast(AccountSettingsActivity.this,
							getString(R.string.sync_delete));
				}
			}

			private void refreshAvatar() {
				StoredData.delAvatarUrl(imgId);
				avatarUrlList.remove(position);
				avatarAdp.notifyDataSetChanged();
				String currDftAvatarImgId = StoredData.getCurrUserAvatarImgId();
				if (imgId == null)
					return;
				if (imgId.equals(currDftAvatarImgId)) {
					StoredData.clearDefaultAvatarUrl();
				}
			}
		}.doPost(HttpUrlPath.accountDeleteAvatarUrl, params, this);
	}

	/**
	 * 设置主头像
	 * 
	 * @param position
	 */
	private void setDefaultAvatar(int position) {
		String imgId = avatarUrlList.get(position).imgId;
		String avatarUrl = avatarUrlList.get(position).avatarUrl;
		setDefaultAvatar(imgId, avatarUrl, AVATAR_DEFAULT, position); // 设置为默认头像
	}

	/**
	 * 设置图片为默认图片
	 * 
	 * @param imgId
	 * @param status
	 *            默认=1，清除默认=0
	 */
	private void setDefaultAvatar(final String imgId, final String avatarUrl,
			final int status, final int position) {
		RequestParams params = new RequestParams();
		params.put("id", imgId);
		params.put("adid", StoredData.getAdid());
		params.put("file_flag", "portrait");
		params.put("activity", status + "");
		params.put("android", "PUT");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				if (!json.optBoolean("success")) {
					// error == 1 表示服务器上这张图片已经被删除 理论上这种问题不应该在正式环境上发生
					if (1 == json.optInt("error")) {
						UIHelper.showToast(AccountSettingsActivity.this,
								getString(R.string.sync_failed));
						return;
					}
					UIHelper.showToast(AccountSettingsActivity.this,
							getString(R.string.sync_failed));
					return;
				}

				StoredData.setDefaultAvatarUrl(imgId, avatarUrl);
				updateData(imgId, status, position);
				avatarAdp.notifyDataSetChanged();
				UIHelper.showToast(AccountSettingsActivity.this,
						getString(R.string.account_setting_set_portrait));
			}

			private void updateData(String imgId, int status, int position) {
				switch (status) {
				case AVATAR_DEFAULT:
					// 设置为默认，排序到第一条
					clearStatus();
					avatarUrlList.get(position).setActivity(AVATAR_DEFAULT);
					Collections.swap(avatarUrlList, 0, position);
					System.out.println(avatarUrlList);
					break;
				case AVATAR_DEFAULT_CLEAR:
					avatarUrlList.get(position).setActivity(
							AVATAR_DEFAULT_CLEAR);
					break;
				}
			}

			private void clearStatus() {
				int size = avatarUrlList.size();
				for (int i = 0; i < size; i++) {
					avatarUrlList.get(i).setActivity(AVATAR_DEFAULT_CLEAR);
				}
			}
		}.doGet(HttpUrlPath.accountUpdateAvatarMainUrl, params, this);
	}

	/**
	 * 上传新头像
	 * 
	 * @param newAvatarFile
	 */
	private void uploadNewAvatar(File newAvatarFile) {
		try {
			if (newAvatarFile == null)
				return;
			if (newAvatarFile.length() == 0)
				return;

			List<File> fileList = new ArrayList<File>();
			fileList.add(newAvatarFile);
			final File zipFile = FileUtil.createZipFile("avatar", fileList);
			RequestParams params = new RequestParams();
			params.put("adid", StoredData.getAdid());
			params.put("file_flag", "portrait");
			params.put("imgfile", zipFile);
			params.put("android", "POST");
			new SimpleJSONUtil() {

				@Override
				public void handReponseJson(JSONObject json) {
					String avatarId = json.optString("id");
					String avatarUrl = json.optString("url");
					Utility.delFolder(zipFile);
					System.gc();
					refreshAvatar(avatarId, avatarUrl);
					saveAvatar(avatarId, avatarUrl);
				}

				private void saveAvatar(String avatarId, String avatarUrl) {
					StoredData.setAvatarUrl(avatarId, avatarUrl);
					if (avatarUrlList.size() == 1) { // 有且只有一张头像, 默认为主头像
						StoredData.setDefaultAvatarUrl(avatarId, avatarUrl);
					}
				}

				private void refreshAvatar(final String avatarId,
						final String avatarUrl) {
					avatarUrlList.add(new UserAvatar(avatarId, avatarUrl,
							avatarUrlList.size() == 0 ? AVATAR_DEFAULT
									: AVATAR_DEFAULT_CLEAR));
					avatarAdp.notifyDataSetChanged();
				}

			}.doPost(HttpUrlPath.accountAddAvatarUrl, params, this);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private class PwdEyeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			int cursorIndex = etPassword.getSelectionStart();
			etPassword
					.setTransformationMethod(isChecked ? HideReturnsTransformationMethod
							.getInstance() : PasswordTransformationMethod
							.getInstance());
			etPassword.setSelection(cursorIndex);
		}

	}

	private class SubmitOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			String fullname = etFullname.getText().toString();
			String pwd = etPassword.getText().toString();
			if (TextUtils.isEmpty(fullname)) {
				UIHelper.showToast(getString(R.string.account_setting_fullname_empty));
				return;
			}
			if (TextUtils.isEmpty(pwd)) {
				UIHelper.showToast(getString(R.string.account_setting_pwd_empty));
				return;
			}
			if (fullname.equals(StoredData.getUsername())
					&& pwd.equals(StoredData.getPwd())) {
				AccountSettingsActivity.this.finish();
				overridePendingTransition(R.anim.push_from_right_out,
						R.anim.push_from_right_in);
				return;
			}
			updateUserInfo(fullname, pwd);
		}

	}

	private class AvatarAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return avatarUrlList.size() + 1; // 第一个是加号
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(AccountSettingsActivity.this,
						R.layout.account_set_avatar_item_layout, null);
				holder.ivAvatar = (RecycleImageView) convertView
						.findViewById(R.id.ivAvatar);
				holder.ivAddPhoto = (ImageView) convertView
						.findViewById(R.id.ivAddPhoto);
				holder.tvFlag = (TextView) convertView
						.findViewById(R.id.tvFlag);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.ivAddPhoto.setVisibility(position == 0 ? View.VISIBLE
					: View.GONE);
			if (position >= 1) {
				boolean isDefaultAvatar = (AVATAR_DEFAULT == avatarUrlList
						.get(position - 1).activity);
				holder.tvFlag.setVisibility(isDefaultAvatar ? View.VISIBLE
						: View.INVISIBLE);
				holder.ivAvatar.setVisibility(View.VISIBLE);
				ImageLoader.getInstance().displayImage(
						HttpUrlPath.basePath
								+ avatarUrlList.get(position - 1)
										.getAvatarUrl(), holder.ivAvatar,
						DisplayPictureUtil.getAvatarDisplayImageOptions());
			} else {
				holder.tvFlag.setVisibility(View.GONE);
				holder.ivAvatar.setVisibility(View.GONE);
			}

			return convertView;
		}

	}

	class ViewHolder {
		public RecycleImageView ivAvatar;
		public TextView tvFlag;
		public ImageView ivAddPhoto;
	}

	/**
	 * 从相册获取
	 * 
	 */
	public void openGallery() {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");
		startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
	}

	/**
	 * 从相机获取
	 * 
	 */
	public void openCamera() {
		// 判断存储卡是否可以用，可用进行存储
		if (Utility.isSDExits()) {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);// 调用照相机
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
			startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
		}
	}

	/**
	 * 处理从图库选择图片
	 * 
	 * @param data
	 */
	private void handleGallery(Intent data) {
		if (data != null) {
			crop(data.getData(), Uri.fromFile(tempFile));
		}
	}

	/**
	 * 拍照完成的处理 拷贝拍照原图，如果遇到截图失败的情况
	 * 则上传原图/storage/emulated/0/vvme/image/tmp/real1433485427266_avatar.jpg
	 */
	private void handleCam() {
		if (tempFile == null)
			return;
		Utility.copyFile(tempFile.getAbsolutePath(),
				getFile(PHOTO_FILE_PATH + "/" + "real" + PHOTO_FILE_NAME)
						.getAbsolutePath(), true);
		tmpSrcFile = getFile(PHOTO_FILE_PATH + "/" + "real" + PHOTO_FILE_NAME);
		crop(Uri.fromFile(tempFile), Uri.fromFile(tempFile));
	}

	/**
	 * 截图完成后 上传头像
	 * 
	 * @param data
	 */
	private void handleCrop(Intent data) {
		if (data != null) {
			// 裁切大图使用Uri
			// Bitmap bitmap = decodeUriAsBitmap(Uri.fromFile(tempFile));
			Bitmap bitmap = decodeUriAsBitmapComp(tempFile);
			File ret;
			if (bitmap != null) {
				// ret = Utility.saveTmpImgNoCompress(bitmap);
				ret = Utility.saveTmpImage(bitmap);
				bitmap.recycle();
			} else {
				ret = tmpSrcFile;
			}
			uploadNewAvatar(ret);
		}
	}

	/**
	 * 裁切图片 uri为选中图片返回的Uri cutImgUri为把截取图片写入sd卡的Uri
	 */
	private void crop(Uri uri, Uri cutImgUri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// 裁剪后输出图片的尺寸大小
		intent.putExtra("outputX", 500);
		intent.putExtra("outputY", 500);
		// 图片格式
		intent.putExtra("outputFormat", "JPEG");
		intent.putExtra("noFaceDetection", true);// 取消人脸识别
		intent.putExtra("return-data", false);// true:不返回uri，false：返回uri
		intent.putExtra(MediaStore.EXTRA_OUTPUT, cutImgUri);// 写入截取的图片
		startActivityForResult(intent, PHOTO_REQUEST_CROP);
	}

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 * @return
	 */
	private static String getPath(String path) {
		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 * @return
	 */
	private File getFile(String path) {
		File f = new File(path);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}

	/**
	 * uri转换为bitmap
	 * 
	 * @param uri
	 * @return
	 */
	public Bitmap decodeUriAsBitmap(Uri uri) {
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeStream(this.getContentResolver()
					.openInputStream(uri));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	public Bitmap decodeUriAsBitmapComp(File file) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		newOpts.inJustDecodeBounds = true;// 只读边,不读内容
		Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),
				newOpts);

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		float hh = 500f;//
		float ww = 500f;//
		int be = 1;
		if (w > h && w > ww) {
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置采样率

		newOpts.inPreferredConfig = Config.RGB_565;
		newOpts.inPurgeable = true;// 同时设置才会有效
		newOpts.inInputShareable = true;// 。当系统内存不够时候图片自动被回收

		bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), newOpts);

		if (bitmap != null)
			return bitmap;
		else {
			try {
				byte[] data = getBytes(new FileInputStream(file));
				Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);
				return bm;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return bitmap;
	}

	// 定义一个根据图片url获取InputStream的方法
	public static byte[] getBytes(InputStream is) throws IOException {
		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024]; // 用数据装
		int len = -1;
		while ((len = is.read(buffer)) != -1) {
			outstream.write(buffer, 0, len);
		}
		outstream.close();
		// 关闭流一定要记得。
		return outstream.toByteArray();
	}

	// 然后使用方法decodeByteArray（）方法解析编码，生成Bitmap对象。
	/*
	 * byte[] data = getBytesFromInputStream(new URL(imgUrl).openStream());
	 * Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);
	 */
}
