package oso.ui.main.bean;

import java.util.List;

/**
 * Bean: 登陆返回结果,包含登陆account信息,所有列表人的adid,仓库信息,头像url。  Permissions
 * @author xialimin
 *
 */

public class LoginResultBean {
	public List<Data> data;
	public List<Permissions> permissions;
	public String err;
	public String ret;
	
	public class Data {
		public String userid;
		public String username;
		public List<Departments> departments;
		public List<Perportraits> perportraits;
		public List<Portraits> portraits;
		public List<Warehouses> warehouses;
	}
	
	public class Perportraits {
		public String activity;
		public String file_path;
		public String id;
	}
	
	public class Departments {
		public String deptid;
		public String deptname;
		public String postid;
		public String postname;
	}

	public class Portraits {
		public List<Warehouses> warehouses;
		public String adid;
		public String account;
		public String file_path;
	}

	public class Warehouses {
		public String areaid;
		public String wareid;
		public String warename;
	}

	public class Permissions {
		public String id;
		public String parentid;
		public String title;
	}
}


