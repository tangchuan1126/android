package oso.widget.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import declare.com.vvme.R;

public class AdpDlgSingleSel extends BaseAdapter {

	private Context ct;
	private CharSequence[] list;
	public int curIndex=-1;

	private boolean switchInner = false; // 可内部点击,但setOnItemClick会失效
	
	public boolean isStyle_rightRButton=false;	//选中样式,true:右边cb,false:左侧"竖线"

	public AdpDlgSingleSel(Context ct, CharSequence[] list, int defIndex) {
		// TODO Auto-generated constructor stub
		this.ct = ct;
		this.list = list;
		this.curIndex = defIndex;
	}

	public AdpDlgSingleSel(Context ct, CharSequence[] list, int defIndex,
			boolean switchInner) {
		// TODO Auto-generated constructor stub
		this.ct = ct;
		this.list = list;
		this.curIndex = defIndex;
		this.switchInner = switchInner;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		Holder h;
		if (convertView == null) {
			convertView = LayoutInflater.from(ct).inflate(
					R.layout.simple_list_single_sel, null);
			h = new Holder();
			h.text1 = (TextView) convertView.findViewById(R.id.text1);
			h.vSel = convertView.findViewById(R.id.vSel);
			h.cb_dlg= (RadioButton) convertView.findViewById(R.id.cb_dlg);
			
			//初始化-选中样式
			if(isStyle_rightRButton){
				h.cb_dlg.setVisibility(View.VISIBLE);
				h.vSel.setVisibility(View.INVISIBLE);
			}
			else{
				h.vSel.setVisibility(View.VISIBLE);
				h.cb_dlg.setVisibility(View.INVISIBLE);
			}
			
			convertView.setTag(h);
		} else
			h = (Holder) convertView.getTag();

		// 刷ui
		h.text1.setText(list[position]);
		
		//选中-样式
		boolean isSelected=position == curIndex ;
		if(isStyle_rightRButton)
			h.cb_dlg.setChecked(isSelected);
		else
			h.vSel.setVisibility(isSelected? View.VISIBLE
					: View.INVISIBLE);

		// debug
		if (switchInner)
			convertView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					curIndex = position;
					notifyDataSetChanged();
				}
			});

		return convertView;
	}

	class Holder {
		TextView text1;
		View vSel;
		RadioButton cb_dlg;
	}

}
