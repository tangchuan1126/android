package oso.widget.edittext;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.widget.autosearch.MCBean;
import oso.widget.dlgeditview.AdpDlgEt;
import oso.widget.dlgeditview.DlgEditView;
import utility.StringUtil;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class SimpleDlgAdapter extends AdpDlgEt {

	private Context mContext;

	private TextView inputEt;

	private List<MCBean> mclist = new ArrayList<MCBean>();

	String[] ds;

	public SimpleDlgAdapter(Context mContext, TextView inputEt) {
		this.inputEt = inputEt;
		this.mContext = mContext;
	}

	@Override
	public boolean onGetData(DlgEditView dlg, JSONObject json,String keyword) {
		System.out.println("返回数据= " + json.toString());
		String type = StringUtil.getJsonString(json, "type"); // 判断是字符串还是json
		ds = type.equals("string") ? praseString(dlg, json) : praseJson(dlg, json);
		if (ds.length != 0 && !ds[0].equals("")) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.simple_list_item_1, ds);
			dlg.getListView().setAdapter(adapter);
		} else {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.simple_list_item_1, new String[] {});
			dlg.getListView().setAdapter(adapter);
		}
		return true;
	}

	@Override
	public String onItemSelected(int position) {
		inputEt.setText(ds[position]);
		if (onDlgItemListener != null)
			onDlgItemListener.onClick(ds[position]);
		return ds[position];
	}

	private String[] praseString(DlgEditView dlg, JSONObject json) {
		String datas = StringUtil.getJsonString(json, "datas");
		return datas.split(",");
	}

	private String[] praseJson(DlgEditView dlg, JSONObject json) {
		mclist = MCBean.handJsonForList(json);
		if (!StringUtil.isNullOfStr(inputEt.getText().toString().trim()) && mclist != null && mclist.size() > 0) {
			return getStringlist();
		}
		return new String[] {};
	}

	public String[] getStringlist() {
		String[] sb = new String[mclist.size()];
		if (mclist != null && mclist.size() > 0) {
			for (int i = 0; i < mclist.size(); i++) {
				sb[i] = mclist.get(i).carrier + "," + mclist.get(i).mc_dot;
			}
		}
		return sb;
	}

	public OnDlgItemListener getOnDlgItemListener() {
		return onDlgItemListener;
	}

	public void setOnDlgItemListener(OnDlgItemListener onDlgItemListener) {
		this.onDlgItemListener = onDlgItemListener;
	}

	private OnDlgItemListener onDlgItemListener;

	public interface OnDlgItemListener {
		void onClick(String value);
	}
}
