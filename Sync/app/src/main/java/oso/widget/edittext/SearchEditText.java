package oso.widget.edittext;

import utility.Utility;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import declare.com.vvme.R;

/**
 * 单行-搜索edit
 * 
 * @author 朱成
 * @date 2015-1-12
 */
public class SearchEditText extends EditText implements TextWatcher {

	public final static int IconMode_None = 0; // 图标模式,无/放大镜/扫描/删除
	public final static int IconMode_Search = 1;
	public final static int IconMode_Scan = 2;
	public final static int IconMode_Del = 3;

	/**
	 * 删除按钮的引用
	 */
	private Drawable mSearcherDrawable;

	private SeacherMethod seacherMethod;

	private String searchValue;

	private long clickTime;

	private Context context;

	private int iconMode = IconMode_Search; // 图标-模式

	public SearchEditText(Context context) {
		this(context, null);
		this.context = context;
	}

	public SearchEditText(Context context, AttributeSet attrs) {
		// 这里构造方法也很重要，不加这个很多属性不能再XML里面定义
		this(context, attrs, android.R.attr.editTextStyle);
		this.context = context;
	}

	public SearchEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}

	private void init() {
		// if(getBackground()==null)
		// setBackgroundResource(R.drawable.search_edittext_style_for_checkin);

		// 获取EditText的DrawableRight,假如没有设置我们就使用默认的图片
		mSearcherDrawable = getCompoundDrawables()[2];
		if (mSearcherDrawable == null) {
			mSearcherDrawable = getResources().getDrawable(R.drawable.ic_edittext_search);
		}
		// mSearcherDrawable.getIntrinsicHeight(),debug
		mSearcherDrawable.setBounds(0, 0, mSearcherDrawable.getIntrinsicWidth(), getHeight());
		// updateRight();
		// setOnFocusChangeListener(this);
		addTextChangedListener(this);

		// debug,默认回车时-触发搜索,若外部setOnKeyListener 会覆盖其
		setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (Utility.isEnterUp(event)) {
					// 触发-搜索
					if (seacherMethod != null)
						seacherMethod.seacher(getEditableText().toString());
					// 防止-焦点下移
					return true;
				}

				return false;
			}
		});
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				return true;
			}
		});
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		// TODO Auto-generated method stub
		super.onLayout(changed, left, top, right, bottom);

		if (!changed && getHeight() > 0)
			return;

		// 调整drawableRight高度
		Drawable drR = getCompoundDrawables()[2];
		if (drR != null)
			// drR.setBounds(0, 0, drR.getIntrinsicWidth(),
			// getHeight());
			updateRightIcon();
	}

	/**
	 * 因为我们不能直接给EditText设置点击事件，所以我们用记住我们按下的位置来模拟点击事件 当我们按下的位置 在 EditText的宽度 -
	 * 图标到控件右边的间距 - 图标的宽度 和 EditText的宽度 - 图标到控件右边的间距之间我们就算点击了图标，竖直方向没有考虑
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// 若点中"右侧-按钮" 触发事件,禁止输入时 不能点
		if (isEnabled() && getCompoundDrawables()[2] != null) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				boolean touchable = event.getX() > (getWidth() - getPaddingRight() - mSearcherDrawable.getIntrinsicWidth())
						&& (event.getX() < ((getWidth() - getPaddingRight())));
				String value = this.getText().toString();
				// if (touchable && !StringUtil.isNullOfStr(value)
				// && canSearch(value)) {
				// !StringUtil.isNullOfStr(value)&&
				if (touchable && canSearch(value)) {
					// if (seacherMethod != null)
					// seacherMethod.seacher(value);
					// debug
					doAction();
					return false;
				}
			}
		}

		return super.onTouchEvent(event);
	}

	/**
	 * 比较前后两次搜索的值， 只要是不相等，那么就可以搜索 如果是两次点击的时间相差两秒也是可以再次查询的
	 * 
	 * @param value
	 * @return
	 * @descrition
	 */
	private boolean canSearch(String value) {
		if (searchValue == null || !searchValue.equals(value)) {
			clickTime = System.currentTimeMillis();
			searchValue = value;
			return true;
		}
		if (System.currentTimeMillis() - clickTime > 2000) {
			return true;
		}
		return false;
	}

	/**
	 * 当ClearEditText焦点发生变化的时候，判断里面字符串长度设置清除图标的显示与隐藏
	 */
	// @Override
	// public void onFocusChange(View v, boolean hasFocus) {
	// if (hasFocus) {
	// setRightIconVisible(getText().length() > 0);
	// } else {
	// setRightIconVisible(false);
	// }
	// }

	/**
	 * 设置清除图标的显示与隐藏，调用setCompoundDrawables为EditText绘制上去
	 * 
	 * @param visible
	 */
	protected void updateRightIcon() {
		// Drawable right = visible ? mSearcherDrawable : null;
		// setCompoundDrawables(getCompoundDrawables()[0],
		// getCompoundDrawables()[1], right, getCompoundDrawables()[3]);

		// debug
		// int len = getText().length() % 4;
		// if (len == 1)
		// iconMode = IconMode_Search;
		// else if (len == 2)
		// iconMode = IconMode_Scan;
		// else if (len == 3)
		// iconMode = IconMode_Del;

		boolean toShow = getText().length() > 0;
		int resIcon = 0;
		if (iconMode == IconMode_Search)
			resIcon = R.drawable.ic_edittext_search;
		else if (iconMode == IconMode_Scan) {
			resIcon = R.drawable.ic_edittext_scan;
			toShow = true;
		} else if (iconMode == IconMode_Del)
			resIcon = R.drawable.ic_edittext_delect;

		// 非空时才显示
		Drawable right = (toShow && resIcon != 0) ? getResources().getDrawable(resIcon) : null;
		// debug
		// right.getIntrinsicHeight()
		// 素材有点偏移,补偿一点,+1
		if (right != null)
			right.setBounds(0, 0, right.getIntrinsicWidth(), getHeight());
		// right.setBounds(3, 0, right.getIntrinsicWidth()-5,
		// getHeight()-3);
		Drawable[] ds = getCompoundDrawables();
		setCompoundDrawables(ds[0], ds[1], right, ds[3]);
	}

	// 点击-右侧图标时
	private void doAction() {

		// 清空
		if (iconMode == IconMode_Del) {
			setText("");
			return;
		}

		if (seacherMethod != null)
			seacherMethod.seacher(getEditableText().toString());
	}

	/**
	 * 当输入框里面内容发生变化的时候回调的方法
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int count, int after) {
		updateRightIcon();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		/*
		 * if(s.length() < 1){ setShakeAnimation(); }
		 */
	}

	/**
	 * 设置晃动动画
	 */
	public void setShakeAnimation() {
		this.setAnimation(shakeAnimation(5));
	}

	/**
	 * 晃动动画
	 * 
	 * @param counts
	 *            1秒钟晃动多少下
	 * @return
	 */
	public static Animation shakeAnimation(int counts) {
		Animation translateAnimation = new TranslateAnimation(0, 8, 0, 0);
		translateAnimation.setInterpolator(new CycleInterpolator(counts));
		translateAnimation.setDuration(1000);
		return translateAnimation;
	}

	// 点击图标-事件
	public void setSeacherMethod(SeacherMethod seacherMethod) {
		this.seacherMethod = seacherMethod;
	}

	public int getIconMode() {
		return iconMode;
	}

	// 取IconMode_x
	public void setIconMode(int iconMode) {
		this.iconMode = iconMode;
		updateRightIcon();
	}

	public void setScanMode() {
		setIconMode(SearchEditText.IconMode_Scan);
	}

}
