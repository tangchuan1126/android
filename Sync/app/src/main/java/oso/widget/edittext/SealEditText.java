package oso.widget.edittext;

import utility.AllCapTransformationMethod;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

public class SealEditText extends LinearLayout {
	private TextView textview;
	private EditText edittext;
//	private String text;
	
	public SealEditText(Context context) {
		super(context);
	}
	
	public SealEditText(Context context, AttributeSet attrs) {
		super(context, attrs);  
		
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
        View linearLayout =	li.inflate(R.layout.seal_et_layout, this, true);
        //-----------获得对子控件的引用   
        textview = (TextView) linearLayout.findViewById(R.id.textview);
        edittext = (EditText) linearLayout.findViewById(R.id.edittext);
        
        //初始化
        edittext.setTransformationMethod(AllCapTransformationMethod.getThis());
        
        //-----------设置动态监听文字转大写
//        edittext.addTextChangedListener(new TextWatcher() {  
//  
//            @Override  
//            public void onTextChanged(CharSequence s, int start, int before, int count) {  
//                // TODO Auto-generated method stub  
//            	edittext.removeTextChangedListener(this);//解除文字改变事件  
//            	edittext.setText(s.toString().toUpperCase());//转换  
//            	edittext.setSelection(s.toString().length());//重新设置光标位置  
//            	edittext.addTextChangedListener(this);//重新绑  
//            	text = edittext.getText().toString().trim();  
//            }  
//  
//            @Override  
//            public void beforeTextChanged(CharSequence s, int start, int count,  
//                    int after) {  
//                // TODO Auto-generated method stub  
//  
//            }  
//  
//            @Override  
//            public void afterTextChanged(Editable s) {  
//                // TODO Auto-generated method stub  
//            }  
//        }); 
	}
	
	/**
	 * @Description:初始化控件基本信息
	 * @param @param title 输入类型
	 * @param @param value 输入内容
	 * @param @param hint  hint值
	 */
	public void initView(String title,String value,String hint){
		textview.setText(StringUtil.isNullOfStr(title)?"":title);
		edittext.setText(StringUtil.isNullOfStr(value)?"":value.toUpperCase());
		edittext.setHint(StringUtil.isNullOfStr(hint)?"NA":hint);
	}

	/**
	 * @Description:获取文本
	 */
	public String getText() {
//    	String ret= edittext.getText().toString().trim();
		String ret= edittext.getText().toString();
    	return ret.toUpperCase();
	}
	
	public void setText(String value) {
		edittext.setText(StringUtil.isNullOfStr(value)?"":value);
	}
	
	public void endEtCursor(){
		Utility.endEtCursor(edittext);
	}
		
	/**
	 * @Description:设置文本是否可以进行编辑 
	 * @param @param flag  如果为true 则可以进行编辑 否则则不可以进行编辑
	 */
	public void setEnable(boolean flag) {
		edittext.setEnabled(flag);
	}
	
}