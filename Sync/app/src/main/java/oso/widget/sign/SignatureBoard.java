package oso.widget.sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;

import org.json.JSONObject;

import oso.widget.BadgeView;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

public class SignatureBoard implements OnClickListener {

	private Context mContext;
	private Dialog dialog;
	private LinearLayout signLayout, historyLayout;
	private View lineView;
	private ImageView closeBtn, clearBtn, backBtn, fullBtn, saveBtn;
	private FrameLayout hisBtnLayout;
	private TextView typeTv;
	private SignatureView sv;
	private BadgeView badge;
	private RequestParams requestParams;

	boolean isFullScreen = false;
	boolean isShowing = false;
	int scWidth = 480;
	int scHeight = 800;
	int scHalfHeight = 400;

	boolean flag = false, isHistory = false;

	public static final String img_sign = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/sign/";

	String mRelativeValue = "defValue", mKey = "defValue", mType = "1";
	int direction = 0; // 签名版方向

	public SignatureBoard(Context c) {
		mContext = c;
	}

	public void init(String relativeValue, String key, String type) {
		requestParams = new RequestParams();
		requestParams.add("Method", HttpPostMethod.BillOfLoadingSignature);
		requestParams.add("relative_value", relativeValue);
		mRelativeValue = relativeValue;
		mKey = key;
		mType = type;
		requestParams.add("relative_type", type);
		scWidth = getScreenWidth(mContext);
		scHeight = getScreenHeight(mContext);
		scHalfHeight = getScreenHeight(mContext) / 2;
		if (direction == 0) {
			initDialog();
		} else {
			initDialog2();
		}
		initDialogView(false);
	}

	public void init(boolean isFull) {
		scWidth = getScreenWidth(mContext);
		scHeight = getScreenHeight(mContext);
		scHalfHeight = getScreenHeight(mContext) / 2;
		if (direction == 0 || isFull) {
			initDialog();
		} else {
			initDialog2();
		}
		initDialogView(isFull);
	}

	private void initDialog() {
		dialog = new Dialog(mContext, R.style.signDialog);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = scWidth; // 设置宽度
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setWindowAnimations(R.style.signAnim); // 添加动画
		dialog.setContentView(R.layout.weiget_signboard);
		// setOnKeyListener();
	}

	private void initDialog2() {
		dialog = new Dialog(mContext, R.style.signDialog);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = scWidth; // 设置宽度
		window.setGravity(Gravity.TOP); // 此处可以设置dialog显示的位置
		window.setWindowAnimations(R.style.signAnim2); // 添加动画
		dialog.setContentView(R.layout.weiget_signboard2);
		// setOnKeyListener();
	}

	/*
	 * public void setOnKeyListener() { dialog.setOnKeyListener(new
	 * OnKeyListener() {
	 * 
	 * @Override public boolean onKey(DialogInterface arg0, int keyCode,
	 * KeyEvent arg2) { switch (keyCode) { // 音量减小 case
	 * KeyEvent.KEYCODE_VOLUME_DOWN: changeDialogState(); return true; // 音量增大
	 * case KeyEvent.KEYCODE_VOLUME_UP: changeDialogState(); return true; case
	 * KeyEvent.KEYCODE_BACK: flag = false; return false; } return false; } });
	 * }
	 * 
	 * private void changeDialogState() { if (flag) { dialog.dismiss(); flag =
	 * false; } else { dialog.show(); flag = true; } }
	 */

	private void initDialogView(boolean isFull) {
		isFullScreen = isFull;
		signLayout = (LinearLayout) dialog.findViewById(R.id.signLayout);
		hisBtnLayout = (FrameLayout) dialog.findViewById(R.id.backBtnLayout);
		historyLayout = (LinearLayout) dialog.findViewById(R.id.historyLayout);
		lineView = dialog.findViewById(R.id.lineView);
		typeTv = (TextView) dialog.findViewById(R.id.typeTv);
		closeBtn = (ImageView) dialog.findViewById(R.id.closeBtn);
		clearBtn = (ImageView) dialog.findViewById(R.id.clearBtn);
		backBtn = (ImageView) dialog.findViewById(R.id.backBtn);
		fullBtn = (ImageView) dialog.findViewById(R.id.fullBtn);
		saveBtn = (ImageView) dialog.findViewById(R.id.saveBtn);
		sv = (SignatureView) dialog.findViewById(R.id.sv);

		setSignType(mType);

		badge = new BadgeView(mContext, hisBtnLayout);
		badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
		int s = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, mContext.getResources().getDisplayMetrics());
		badge.setTextSize(s);
		setHistoryNum();

		if (isFull) {
			fullBtn.setImageResource(R.drawable.fullscreen_return);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(scWidth, scHeight);
			signLayout.setLayoutParams(params);
			sv.initCanvas(scWidth, scHeight);
			lineView.setVisibility(View.GONE);
		} else {
			fullBtn.setImageResource(R.drawable.fullscreen);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(scWidth, scHalfHeight);
			signLayout.setLayoutParams(params);
			sv.initCanvas(scWidth, scHalfHeight);
			if (direction != 0) {
				RotateAnimation rot = new RotateAnimation(0, 180, 1, 0.5f, 1, 0.5f);
				rot.setDuration(20);
				rot.setFillAfter(true);
				typeTv.startAnimation(rot);
			}
		}
		initHistory();

		closeBtn.setOnClickListener(this);
		clearBtn.setOnClickListener(this);
		backBtn.setOnClickListener(this);
		fullBtn.setOnClickListener(this);
		saveBtn.setOnClickListener(this);

		dialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);// 竖屏
				isHistory = false;
			}
		});
	}

	private void initHistory() {
		File[] s = new File(img_sign + mKey).listFiles();
		if (s != null && s.length != 0) {
			Arrays.sort(s, Collections.reverseOrder());
			for (int i = 0; i < s.length; i++) {
				ImageView iv = new ImageView(mContext);
				Bitmap bm = BitmapFactory.decodeFile(s[i].getPath());
				iv.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.slt_as_ios7_other_bt_single));
				iv.setScaleType(ScaleType.CENTER_CROP);
				iv.setImageBitmap(bm);
				int w = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, mContext.getResources().getDisplayMetrics());
				int h = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, mContext.getResources().getDisplayMetrics());
				int m = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, mContext.getResources().getDisplayMetrics());
				historyLayout.addView(iv, w, h);
				historyLayout.addView(new TextView(mContext), m, h);

				final String path = s[i].getPath();
				iv.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						showOKDialog(path);
					}
				});
			}
		}
	}

	/**
	 * History上的红色徽章
	 * 
	 * @param value
	 */
	private void setHistoryNum() {
		File[] s = new File(img_sign + mKey).listFiles();
		if (s != null && s.length != 0) {
			badge.setText(s.length + "");
			badge.show();
		} else {
			badge.hide();
		}
	}

	public void show() {
		isShowing = true;
		dialog.show();
	}

	public void close() {
		if (dialog.isShowing()) {
			isShowing = false;
			((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);// 竖屏
			sv.redo();
			dialog.dismiss();
		}
		isHistory = false;
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.closeBtn:
			flag = false;
			close();
			break;

		case R.id.clearBtn:
			sv.redo();
			break;

		case R.id.backBtn:// 撤销功能变历史记录
			// sv.undo();
			File[] s = new File(img_sign + mKey).listFiles();
			if (s != null && s.length != 0) {
				if (isHistory) {
					historyLayout.setVisibility(View.GONE);
					lineView.setVisibility(View.GONE);
				} else {

					historyLayout.setVisibility(View.VISIBLE);
					lineView.setVisibility(View.VISIBLE);
				}
				isHistory = !isHistory;
			} else {
				historyLayout.setVisibility(View.GONE);
				lineView.setVisibility(View.GONE);
				isHistory = false;
				UIHelper.showToast(mContext.getString(R.string.bcs_key_norecordsexception));
			}
			break;

		case R.id.fullBtn:
			if (isFullScreen) {
				((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);// 竖屏
				dialog.dismiss();
				init(false);
				dialog.show();
			} else {
				((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);// 横屏
				dialog.dismiss();
				init(true);
				dialog.show();
			}
			isHistory = false;
			break;

		case R.id.saveBtn:
			if (!sv.isBitmapEmpty()) {
				File mkFile = new File(img_sign + mKey);
				if (!mkFile.exists()) {
					mkFile.mkdirs();
				}
				String path = mkFile.getPath() + "/" + System.currentTimeMillis() + ".png";
				if (!isFullScreen && direction != 0) {
					sv.save(path, true);
				} else {
					sv.save(path, false);
				}
				setHistoryNum();
				sendSignToServer(path);
			}
			break;

		default:
			break;
		}
	}

	private void sendSignToServer(final String savePath) {
		if (requestParams == null) {
			UIHelper.showToast(mContext, "参数错误", Toast.LENGTH_SHORT).show();
			return;
		}
		Bitmap bm = BitmapFactory.decodeFile(savePath);
		if (bm == null) {
			bm = sv.getBitMap();
		}
		requestParams.put("file", Bitmap2InputStream(bm));

		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				close();
				if (onSignListener != null) {
					onSignListener.onSignSuccessed(savePath, sv.getBitMap(), json.optString("filename"));
				}
				UIHelper.showToast(mContext, mContext.getString(R.string.sync_success), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void handFinish() {
				// UIHelper.showToast(mContext, "Failure!",
				// Toast.LENGTH_SHORT).show();
				close();
			}

			@Override
			public void handFail() {
			}
		}.doPost(HttpUrlPath.AndroidSignatureFileUpAction, requestParams, mContext);
	}

	// 将Bitmap转换成InputStream
	public InputStream Bitmap2InputStream(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		return is;
	}

	private void showOKDialog(final String path) {
		RewriteBuilderDialog.Builder dialog = new RewriteBuilderDialog.Builder(mContext);
		final ImageView iv = new ImageView(mContext);
		iv.setImageBitmap(BitmapFactory.decodeFile(path));
		dialog.setContentView(iv);
		dialog.setTitle("Signature");
		dialog.setPositiveButton(mContext.getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				sendSignToServer(path);
			}
		});
		dialog.setNegativeButton(mContext.getString(R.string.sync_cancel), null);
		dialog.create().show();
	}

	public void setSignType(String type) {
		String name = "Unknown Role";
		if (type.equals("1"))
			name = "SHIPPER SIGNATURE:";
		if (type.equals("2"))
			name = "CARRIER SIGNATURE:";
		if (type.equals("3"))
			name = "CARRIER INITIAL SIGNATURE:";

		typeTv.setText(name);
	}

	public Dialog getDialog() {
		return dialog;
	}

	public OnSignListener getOnSignListener() {
		return onSignListener;
	}

	public void setOnSignListener(OnSignListener onSignListener) {
		this.onSignListener = onSignListener;
	}

	private OnSignListener onSignListener;

	// 提交签名-成功后
	public interface OnSignListener {
		// 入参:imgUrl(上传签名图片后生成的url)
		void onSignSuccessed(String savePath, Bitmap bm, String imgUrl);
	}

}