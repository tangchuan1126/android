package oso.widget;

import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.window.util.AdpRecOcuppied;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.datas.HoldDoubleValue;
import support.key.OccupyTypeKey;
import utility.Utility;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 选择资源
 * 
 * @author 朱成
 * @date 2014-12-31
 */
public class FragSelRes extends Fragment implements OnClickListener {

	private Activity mActivity;
	private View v;

	private VpWithTab vp;
	private SingleSelectBar singleSelectBar;

	private ListView lv;
	private DoorAdapter adapter;
	private ListView lvOccupied;
	private AdpRecOcuppied adpOccupied;

	private Button btnZoneFree;
	private Button btnZoneOccupied;

	private String[] freeZones;

	// 入参
	private List<ResourceInfo> doorlist;
	private ResourceInfo selectDoorInfo;

	// ==================接口===============================
	private boolean canSelOccupied = true; // 可选占用的
	
	private String dor_tag = "";		//zone名称
	private String stop_door;
	private int resources_id;
	
//	public void inits(List<ResourceInfo> doorlist) {
//		init(doorlist, true);
//	}
	
	public void init(List<ResourceInfo> doorlist,int resources_id) {
		init(doorlist, true,resources_id);
	}

	public void init(List<ResourceInfo> doorlist, boolean canSelOccupied,int resources_id) {
		this.doorlist = doorlist;
		this.canSelOccupied = canSelOccupied;
		this.resources_id = resources_id;

		// 初始化
		freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
		int zoneIndex=getArrayIndex(freeZones, dor_tag);
		changeZone(zoneIndex);
		
		
	}
	
	private int getArrayIndex(String[] list,String zoneName){
		if(Utility.isEmpty(list) || TextUtils.isEmpty(zoneName))
			return 0;
		
		for (int i = 0; i < list.length; i++) {
			if(zoneName.equals(list[i]))
				return i;
		}
		return 0;
	}

	public ResourceInfo getSelectRes() {
		return selectDoorInfo;
	}
	
	public void initData(String areaName, String stop_door) {
		this.dor_tag = areaName;
		this.stop_door = stop_door;
	}

	// =================================================

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mActivity = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// savedInstanceState.getSerializable("datas");
		// applyParams();

		v = inflater.inflate(R.layout.frag_seldoor, container, false);

		// 取view
		vp = (VpWithTab) v.findViewById(R.id.vp_frag);
		singleSelectBar = (SingleSelectBar) v.findViewById(R.id.bar_frag);
		btnZoneFree = (Button) v.findViewById(R.id.btnZoneFree);
		btnZoneOccupied = (Button) v.findViewById(R.id.btnZoneOccupied);
		lv = (ListView) v.findViewById(R.id.lv);
		lvOccupied = (ListView) v.findViewById(R.id.lv2);
		// 监听事件
		btnZoneFree.setOnClickListener(this);
		btnZoneOccupied.setOnClickListener(this);

		lv.setEmptyView((TextView) v.findViewById(R.id.tvEmpty_kk));
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectDoorInfo = (DockHelpUtil.ResourceInfo) lv
						.getItemAtPosition(position);
				if (adapter != null)
					adapter.setCurrentID(selectDoorInfo);
				if (adpOccupied != null)
					adpOccupied.setCurrentID(selectDoorInfo);
			}
		});
		lvOccupied.setEmptyView((TextView) v.findViewById(R.id.tvEmpty_kk2));
		lvOccupied.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 若不能选-占用的
				if (!canSelOccupied)
					return;
				selectDoorInfo = (DockHelpUtil.ResourceInfo) lvOccupied
						.getItemAtPosition(position);
				if (adapter != null)
					adapter.setCurrentID(selectDoorInfo);
				if (adpOccupied != null)
					adpOccupied.setCurrentID(selectDoorInfo);
			}
		});
		
		// 初始化vp
		initSingleSelectBar();
		vp.init(singleSelectBar);

		// // 初始化
		// freeZones = DockHelpUtil.getZones(doorlist, OccupyTypeKey.BOTH);
		// changeZone(0);

		return v;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnZoneFree:
			showDlg_changeZone();
			break;
		case R.id.btnZoneOccupied:
			showDlg_changeZone();
			break;

		default:
			break;
		}
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.shuttle_select_available), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.shuttle_select_occupied), 1));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.userDefineSelectIndexExcuteClick(0);
	}

	// spinner未改变时 不会触发onItemSelected,故不会循环调
	private void changeZone(int position) {
		// 清空当前项
		selectDoorInfo = null;
		// 未占用,注:直接new,其下当前项会消失
		adapter = new DoorAdapter(DockHelpUtil.filtersDoors(doorlist,freeZones[position], false));
		lv.setAdapter(adapter);
		
		//-----------------判断选中状态,并且跳到指定位置
		if(resources_id!=0&&adapter!=null&&!Utility.isNullForList(doorlist)){
			for(int i=0;i<doorlist.size();i++){
				if(resources_id==doorlist.get(i).resource_id){
					selectDoorInfo = doorlist.get(i);
					lv.setSelection(adapter.getPosition(selectDoorInfo));
					break;
				}
			}
		}
		
		btnZoneFree.setText(freeZones[position]);
		
		
		// 占用
		List<ResourceInfo> listOccupiedZones = DockHelpUtil.filtersDoors(doorlist, freeZones[position], true);
		adpOccupied = new AdpRecOcuppied(mActivity, listOccupiedZones,canSelOccupied);
		lvOccupied.setAdapter(adpOccupied);
		btnZoneOccupied.setText(freeZones[position]);
		
		
	}

	// 改区域
	private void showDlg_changeZone() {
		RewriteBuilderDialog.Builder b = new RewriteBuilderDialog.Builder(mActivity);
		b.setTitle(getString(R.string.shuttle_select_areaorzone));
		b.setHeightLimit(true);
		b.setHeightLimitCount(7);
		b.setItems(freeZones, new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				changeZone(position);
			}
		});
		b.show();
	}

	// ==================nested===============================

	public class DoorAdapter extends BaseAdapter {

		private List<DockHelpUtil.ResourceInfo> arrayList;

		private LayoutInflater inflater;

		// int currentID = -1;
		ResourceInfo selBean;

		public DoorAdapter(List<ResourceInfo> arrayList) {
			super();
			this.arrayList = arrayList;
			this.inflater = LayoutInflater.from(mActivity);
		}

		@Override
		public int getCount() {
			return arrayList != null ? arrayList.size() : 0;
		}

		@Override
		public DockHelpUtil.ResourceInfo getItem(int position) {
			return arrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			DoorHoder holder = null;
			if (convertView == null) {
				holder = new DoorHoder();
				convertView = inflater.inflate(R.layout.dci_s_door_item, null);
				holder.select_img = (ImageView) convertView.findViewById(R.id.select_img);
				holder.door_name = (TextView) convertView.findViewById(R.id.door_name);
				convertView.setTag(holder);
			} else {
				holder = (DoorHoder) convertView.getTag();
			}
			DockHelpUtil.ResourceInfo temp = arrayList.get(position);
			if(stop_door != null && stop_door.equals(temp.resource_name)){
				position = temp.resource_id;
				holder.door_name.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,temp.resource_type) + " : [ " + stop_door + " ]");
				setCurrentID(temp);
				holder.select_img.setBackgroundResource(temp.isEqual(selBean) ? R.drawable.radio_clk : R.drawable.radio);
				stop_door = null;
			}else{
				holder.door_name.setText(OccupyTypeKey.getOccupyTypeKeyName(mActivity,temp.resource_type) + " : [ " + temp.resource_name + " ]");
				// 选中效果
				holder.select_img.setBackgroundResource(temp.isEqual(selBean) ? R.drawable.radio_clk : R.drawable.radio);
			}
			return convertView;
		}

		public void setCurrentID(ResourceInfo currentID) {
			this.selBean = currentID;
			notifyDataSetChanged();
		}

		public ResourceInfo getCurrentId() {
			return this.selBean;
		}
		
		/**
		 * @Description:根据需要选中的资源 来获取该索引值 使listview 跳到指定位置
		 */
		public int getPosition(ResourceInfo currentID) {
			int position = 0;
			if(!Utility.isNullForList(arrayList)){
				for(int i=0;i<arrayList.size();i++){
					if(currentID.resource_id==arrayList.get(i).resource_id){
						position = i;
						break;
					}
				}
			}
			setCurrentID(currentID);
			return position;
		}
	}

	public class DoorHoder {
		public TextView door_name;
		public ImageView select_img;
	}

}
