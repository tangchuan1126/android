package oso.widget;

import oso.widget.dlgeditview.DlgEditView;
import oso.widget.edittext.SearchEditText;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import declare.com.vvme.R;

public abstract class DialogSearchView extends Dialog implements OnClickListener{
	private Context context;
	
	private View lo_dlg;
	private SearchEditText et_dlg;
//	private EditText et_dlg;
	private ListView lv_dlg;
	private TextView tv_search_prompt;
	private int threshold = 1; // 请求阈值
	

	/**
	 * 搜索时,此处显示列表
	 * @param keyword
	 */
	protected abstract void onSearch(String keyword) ;
	
	/**
	 * 选中项时
	 * @param bean
	 */
	protected abstract void onItemSelected(String result);
	
	public ListView getListView() {
		return lv_dlg;
	}
	
	//==================inner===============================
	
	public DialogSearchView(Context context) {
		super(context, R.style.dlgEditView);
		
		this.context=context;
		setContentView(R.layout.dlg_searchview);
		setCanceledOnTouchOutside(true);
		setCancelable(true);
		
		// dlg
		Window window = getWindow();
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		
		//取view
		lo_dlg = findViewById(R.id.lo_dlg);
		et_dlg = (SearchEditText) findViewById(R.id.vsearch);
//		et_dlg = vsearch.getInputEt();
		lv_dlg= (ListView) findViewById(R.id.lv_dlg);
		tv_search_prompt = (TextView) findViewById(R.id.tv_search_prompt);
		//监听事件
		lo_dlg.setOnClickListener(this);
		et_dlg.addTextChangedListener(onTextChanged);
		findViewById(R.id.btn_search_dialog_cancel).setOnClickListener(this);
		//初始化
		et_dlg.setIconMode(SearchEditText.IconMode_Del);
//		et_dlg.post(new Runnable() {
//			
//			@Override
//			public void run() {
//				Utility.showSoftkeyboard(getContext(), et_dlg);
//			}
//		});
		
	
		//lv
		lv_dlg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 防止重复触发
				et_dlg.removeTextChangedListener(onTextChanged);
				onItemSelected((String)lv_dlg.getItemAtPosition(position));
				dismiss();
			}
		});
	}
	
	/**
	 * 查询提示
	 * @param hint
	 */
	public DialogSearchView setHint(String hint) {
		if(TextUtils.isEmpty(hint)) return this;
		et_dlg.setHint(hint);
		return this;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.lo_dlg:
			dismiss();
			break;
		
		case R.id.btn_search_dialog_cancel:
			dismiss();
			break;
		default:
			break;
		}
	}
	
	private TextWatcher onTextChanged = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
//			updateTvDirect();

			String key = s.toString();
			// 超过阈值时,提示
			if (!TextUtils.isEmpty(key) && key.length() >= threshold)
				onSearch(key);
			else
				setItems(null);
		}
	};
	
	public void setItems(CharSequence[] items) {
		setItems(items, DlgEditView.ITEM_STYLE_1);
	}

	public void setItems(CharSequence[] items, int style) {
		if (items == null)
			items = new String[0];
		BaseAdapter adp = new ArrayAdapter<CharSequence>(context, style, items);
		lv_dlg.setAdapter(adp);
	}

}
