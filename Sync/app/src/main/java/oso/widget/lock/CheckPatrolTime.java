package oso.widget.lock;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.common.tts.TTS;
import support.dbhelper.StoredData;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * 
 * Patrol的时候用到的
 * 
 * 防止工人偷懒。5分钟后弹出提示
 * 
 * @author jiang
 *
 */
public class CheckPatrolTime {

	private static CheckPatrolTime check = null;

	public static CheckPatrolTime getThis() {
		if (check == null)
			check = new CheckPatrolTime();
		return check;
	}

	/**
	 * 间隔时间
	 */
	private static final int INTERVAL_TIME_5M = 5 * 60; // s
	public static final String INTERVAL_TEXT = "More than 5 minutes without patrol, please continue";

	private Timer waitTimer = null;

	private boolean isWaitTimerRunning = true;

	private RewriteBuilderDialog.Builder mBuilder;

	private Context mContext;

	int waitTime = INTERVAL_TIME_5M, alarmTime = ALARM_TIME_2H; // 读秒

	/**
	 * 重置时间
	 */
	private static final int ALARM_TIME_2H = 2 * 60 * 60; // s
	public static final String ALARM_TEXT = "More than 2 hours without patrol, must again patrol";

	private AlarmManager am;
	private PendingIntent sender;

	public CheckPatrolTime() {
		patrolFinish();
	}

	// public void checkPatrol(Context c) {
	// mContext = c;
	// patroling();
	// }

	public void checkPatrol(Context c, boolean isFinish) {
		mContext = c;
		initAlarmTimer();

		if (!isFinish) {
			// 正在进行Patrol
			patroling();
		} else {
			// Patrol已经完成
			patrolFinish();
		}
	}

	/**
	 * 开始Patorl
	 */
	private void patroling() {
		waitTime = INTERVAL_TIME_5M;
		startWaitTimer();

		alarmTime = ALARM_TIME_2H;
		initAlarmTimer();
	}

	/**
	 * Patrol结束后停止Timer与Alert
	 */
	public void patrolFinish() {
		waitTime = INTERVAL_TIME_5M;
		cancelWaitTimer();

		// if (am != null && sender != null)
		// am.cancel(sender);
	}

	public void initAlarmTimer() {
		Intent intent = new Intent(mContext, PatrolAlarmReceiver.class);
		sender = PendingIntent.getBroadcast(mContext, 0, intent, 0);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, alarmTime);
		// 开启闹钟
		am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
		System.out.println("开启闹钟");

	}

	public void startWaitTimer() {
		// 启动定时器
		if (waitTimer == null) {
			waitTimer = new Timer();
			waitTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					((Activity) mContext).runOnUiThread(waitRunnable);
				}
			}, 20, 1000);
		}
	}

	// 10秒倒计时线程
	final Runnable waitRunnable = new Runnable() {
		@Override
		public void run() {
			if (isWaitTimerRunning) {
				waitTime--;
				if (waitTime < 0) {
					// 等待时间结束,启动违规计时
					cancelWaitTimer();
					showLockDialog();
				} else {
					System.out.println("倒计时：" + waitTime);
				}
			}
		}
	};

	/**
	 * 弹出违规提示
	 * 
	 * @param time
	 */
	public void showLockDialog() {
		mBuilder = new RewriteBuilderDialog.Builder(mContext);
		mBuilder.hideCancelBtn();
		mBuilder.setMessage(INTERVAL_TEXT);
		mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mBuilder.getDialog().isShowing())
					mBuilder.dismiss();
			}
		});
		mBuilder.create().show();
		TTS.getInstance().speakAll_withToast("Too slowly!", true);
	}

	public void cancelWaitTimer() {
		if (waitTimer != null)
			waitTimer.cancel();
		waitTimer = null;
	}

	public static class PatrolAlarmReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			System.out.println("闹钟时间到了");
			UIHelper.showToast(ALARM_TEXT);

			// 保存
			StoredData.saveResetPatrolStatus(true);

			if (onAlarmListener != null)
				onAlarmListener.onDone();
		}
	}

	public static OnAlarmListener getOnAlarmListener() {
		return onAlarmListener;
	}

	public static void setOnAlarmListener(OnAlarmListener onAlarmListener) {
		CheckPatrolTime.onAlarmListener = onAlarmListener;
	}

	private static OnAlarmListener onAlarmListener;

	public interface OnAlarmListener {
		void onDone();
	}

}
