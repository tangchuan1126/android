package oso.widget.lock;

import java.util.Timer;
import java.util.TimerTask;

import oso.widget.dialog.RewriteBuilderDialog;
import support.common.tts.TTS;
import utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 
 * Load的时候用到的
 * 
 * 防止工人偷懒。被动弹出dialog提示
 * 
 * @author jiang
 *
 */
public class CheckScanTimes {

	private static final int WAIT_TIME = 10; // s
	private static final int LOCK_TIME_20 = 20; // s
	private static final int LOCK_TIME_30 = 30; // s
	private static final String TEXT = "Load too frequently, you can load again after %s seconds!";

	private Timer waitTimer, lockTimer = null;

	private boolean isWaitTimerRunning = true;

	private int totalCount, scanCount = 0;

	private RewriteBuilderDialog.Builder mBuilder;

	private Context mContext;

	int waitTime, lockTime = 0; // 读秒

	public boolean isLockRunning = false;

	public CheckScanTimes(Context context) {
		mContext = context;
		isLockRunning = false;
		totalCount = 0;
		scanCount = 0;
		cancelWaitTimer();
	}

	/**
	 * 检查扫描次数，在扫描时候调用
	 */
	public void checkScanTimes() {
		if (isLockRunning) {
			System.out.println("10秒后违规");
			if (lockTime <= 0)
				startLockTimer(LOCK_TIME_20);
			showLockDialog();
			return;
		}
		// 增加扫描次数
		scanCount++;
		System.out.println("scanCount= " + scanCount);

		if (totalCount != scanCount) {
			// 装载超过4次立即锁屏
			if (scanCount > 4) {
				System.out.println("超4次违规");
				startLockTimer(LOCK_TIME_30);
				showLockDialog();
				return;
			}
			// 10s内多次扫描时间重置
			waitTime = WAIT_TIME;
			totalCount = scanCount;
		}
	}

	/**
	 * 在选type之后使用
	 */
	public void startWaitTimer() {
		// 启动定时器
		if (waitTimer == null) {
			waitTimer = new Timer();
			waitTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					((Activity) mContext).runOnUiThread(waitRunnable);
				}
			}, 20, 1000);
		}
	}

	// 10秒倒计时线程
	final Runnable waitRunnable = new Runnable() {
		@Override
		public void run() {
			if (isWaitTimerRunning) {
				waitTime--;
				if (waitTime < 0) {
					// 等待时间结束,启动违规计时
					cancelWaitTimer();
					startLockTimer(LOCK_TIME_20);
				} else {
					System.out.println("倒计时：" + formatDate(waitTime));
				}
			}
		}
	};

	public void startLockTimer(int time) {
		lockTime = time;
		// 初始化属性
		isLockRunning = true;
		totalCount = 0;
		scanCount = 0;
		System.out.println("scanCount= " + scanCount);
		cancelWaitTimer();
		// 启动违规定时器
		if (lockTimer == null) {
			lockTimer = new Timer();
			lockTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					((Activity) mContext).runOnUiThread(lockRunnable);
				}
			}, 20, 1000);
		}
	}

	// 违规倒计时线程
	Runnable lockRunnable = new Runnable() {
		@Override
		public void run() {
			lockTime--;
			if (mBuilder != null) {
				setMessage();
			}

			if (lockTime <= 0) {
				cancelLockTimer();
			} else {
				System.out.println("违规倒计时：" + formatDate(lockTime));
			}
		}
	};

	protected void setMessage() {
		TextView tv = ((TextView) mBuilder.getDialog().findViewById(R.id.message));
		// tv.setText(String.format(TEXT, formatDate(lockTime)));
		int color = mContext.getResources().getColor(R.color.sync_orange);
		String[] strs = { "Load too frequently, you can load again after ", formatDate(lockTime), " seconds!" };
		ForegroundColorSpan[] styles = { new ForegroundColorSpan(Color.BLACK), new ForegroundColorSpan(color), new ForegroundColorSpan(Color.BLACK) };
		tv.setText(Utility.getCompoundText(strs, styles));
	}

	/**
	 * 弹出违规提示
	 * 
	 * @param time
	 */
	public void showLockDialog() {
		mBuilder = new RewriteBuilderDialog.Builder(mContext);
		mBuilder.hideCancelBtn();
		mBuilder.setMessage(String.format(TEXT, ""));
		mBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mBuilder.getDialog().isShowing())
					mBuilder.dismiss();
			}
		});
		mBuilder.create().show();
		TTS.getInstance().speakAll_withToast("Too Frequently!", true);
	}

	public void cancelWaitTimer() {
		if (waitTimer != null)
			waitTimer.cancel();
		waitTimer = null;
	}

	public void cancelLockTimer() {
		if (lockTimer != null) {
			lockTimer.cancel();
			lockTimer = null;
			isLockRunning = false;
			scanCount = 0;
			totalCount = 0;
		}
		if (mBuilder != null && mBuilder.getDialog().isShowing())
			mBuilder.dismiss();
	}

	public void onDestory() {
		resetData();
	}

	public void resetData() {
		isLockRunning = false;
		cancelWaitTimer();
		cancelLockTimer();
		scanCount = 0;
		totalCount = 0;
	}

	// 时间格式化
	private static String formatDate(int d) {
		return d >= 10 ? "" + d : "0" + d;
	}

}
