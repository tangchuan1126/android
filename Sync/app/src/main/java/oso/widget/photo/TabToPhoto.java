package oso.widget.photo;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.base.Checkable;
import oso.widget.photo.bean.TTPImgBean;
import support.common.bean.CheckInTaskBeanMain;
import support.key.CheckInMainDocumentsStatusTypeKey;
import utility.FileUtil;
import utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 拍照控件
 * 
 * 调用方法：
 * 
 * 1. 声明
 * 
 * private TabToPhoto ttp;
 * 
 * 2. 初始化
 * 
 * ttp = (TabToPhoto) findViewById(R.id.ttp);
 * 
 * ttp.init(mActivity, getTabParamList());
 * 
 * 3. 回调
 * 
 * ttp.onActivityResult(requestCode, resultCode, data); //
 * 在onActivityResult方法里调用
 * 
 * 4. 上传
 * 
 * ttp.uploadZip(params, zipFileName)
 * 
 * 5. 上传成功后清除本地数据
 * 
 * ttp.clearData();
 * 
 * @Override protected void onBackBtnOrKey() { ttp.clearCache();
 *           super.onBackBtnOrKey(); }
 * 
 *           ************************** 初始化tab参数的方法 ************************
 * 
 *           private List<TabParam> getTabParamList() { List<TabParam> params =
 *           new ArrayList<TabParam>(); String key0 =
 *           TTPKey.getSamsungLoadKey(loadBean.dlo_detail_id, 0); params.add(new
 *           TabParam("Load", key0, new PhotoCheckable(0, "Load", ttp))); return
 *           params; }
 * 
 * @author jiang
 * 
 */
public class TabToPhoto extends LinearLayout implements OnCheckedChangeListener {

	public static final int Def_ImgSize = 40; // 默认图片尺寸

	public OnClickListener onClick_btnAdd; // 外部给定,onClick中View为TabToPhoto

	public boolean isAddToTmp = false; // 添加图片时,加"_tmp"后缀
	public boolean isShowTmp = false; // 显示时显示"临时图片"(_tmp)

	public TabToPhoto(Context context) {
		super(context);
		initBackground(context);
	}

	public TabToPhoto(Context context, AttributeSet attrs) {
		super(context, attrs);
		initBackground(context);
	}

	public TabToPhoto(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initBackground(context);
	}

	private void initBackground(Context c) {
		mContext = c;
		int minHeight = (int) c.getResources().getDimension(R.dimen.widget_tabtophoto_height);
		setMinimumHeight(minHeight);
	}

	public void init(Context c, List<TabParam> tabParamList) {
		init(c, tabParamList, true);
	}

	public void initNoTitleBar(Context c, TabParam tabParam) {
		initNoTitleBar(c, tabParam, Def_ImgSize);
	}

	/**
	 * initNoTitleBar 不带选项卡的初始化方法
	 * 
	 * @param Context
	 * @param key保存路径
	 * @param size控件大小
	 */
	public void initNoTitleBar(Context c, TabParam tabParam, int size) {
		mTabParamList = new ArrayList<TabParam>();
		tabParam.setIvSize(size);
		mTabParamList.add(tabParam);
		init(c, mTabParamList, true, false);
	}

	/**
	 * init 初始化方法
	 * 
	 * @param Context
	 * @param TabParam图片参数
	 * @param isShowLocalDatas显示本地缓存
	 */
	public void init(Context c, List<TabParam> tabParamList, boolean isShowLocalDatas) {
		init(c, tabParamList, isShowLocalDatas, true);
	}

	private void init(Context c, List<TabParam> tabParamList, boolean isShowLocalDatas, boolean isShowTitleBar) {
		if (tabParamList == null || tabParamList.isEmpty()) {
			throw new IllegalArgumentException(" 初始化参数 TabParam 不能为空!");
		}
		// debug,重新初始化前,终止-之前的上传
		abortUploading();

		// debug
		removeAllViews();

		mTabParamList = tabParamList;
		RelativeLayout mainLayout = (RelativeLayout) View.inflate(c, R.layout.widget_tabtophoto, null);

		// 初始化HorizontalTab
		hb = (HorizontalTab) mainLayout.findViewById(R.id.hb);
		hb.initTab(c, mTabParamList);
		hb.getGroup().setOnCheckedChangeListener(this);
		if (!isShowTitleBar)
			hb.setVisibility(View.GONE);

		// 图片列表
		FrameLayout photoLayout = (FrameLayout) mainLayout.findViewById(R.id.photoLayout);
		// PhotoView
		int length = mTabParamList.size();
		pvs = new PhotoView[length];
		for (int i = 0; i < length; i++) {
			PhotoView iv = new PhotoView(c, mTabParamList.get(i), isShowLocalDatas);
			iv.setVisibility(View.GONE);
			photoLayout.addView(iv);
			pvs[i] = iv;
		}
		pvs[0].setVisibility(View.VISIBLE);

		addView(mainLayout);
	}

	/**
	 * 设置加号按钮的可见性
	 * 
	 * @param visibility
	 */
	public void setAddBtnVisibility(boolean visibility) {
		for (int i = 0; i < pvs.length; i++) {
			pvs[i].setAddBtnVisibility(visibility);
		}
	}

	@Deprecated
	public void onResume(List<TabParam> params) {
		// debug
		// pvs[0].setPath(params.get(0).getKey());
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		int index = hb.getCurrentIndex();
		for (int i = 0; i < pvs.length; i++) {
			if (i == index) {
				pvs[i].setVisibility(View.VISIBLE);
			} else {
				pvs[i].setVisibility(View.GONE);
			}
		}
		for (int i = 0; i < hb.getRBList().size(); i++) {
			RadioButton rb = hb.getRBList().get(i);
			if (rb.getId() == checkedId) {
				rb.setTextColor(getResources().getColor(R.color.sync_blue));
			} else {
				rb.setTextColor(getResources().getColor(R.color.sync_widget_tabtophoto_bg));
			}
		}
	}

	/**
	 * 设置控件的可用性
	 * 
	 * @param enabled
	 */
	public void setTTPEnabled(boolean enabled) {
		for (int i = 0; mTabParamList != null && i < mTabParamList.size(); i++) {
			if (pvs[i] != null) {
				pvs[i].setPVEnabled(enabled);
			}
		}
	}

	/**
	 * @Description:根据form bean 来判断控件的可用性
	 * @param @param enabled
	 */
	public void setTTPEnabled(CheckInTaskBeanMain mainbean) {
		if (mainbean == null) {
			return;
		}
		// 校验已经关闭的单子不能添加照片
		setTTPEnabled(!CheckInMainDocumentsStatusTypeKey.isEquipmentLeft(mainbean.equipment_status));
		// true代表可用
		setTTPEnabled(true);
	}

	/**
	 * onActivityResult里调用,获取图片时
	 * 
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_CANCELED)
			return;

		if (requestCode == PhotoView.OPEN_CAMERA_ACTIVITY || requestCode == PhotoView.OPEN_PHOTO_ACTIVITY) {
			int currentTabIndex = hb == null ? 0 : hb.getCurrentIndex();
			pvs[currentTabIndex].onActivityResult(requestCode, resultCode, data);
			// 修改-tab标题
			if (hb != null && !isPhotoEmpty(true)) {
				RadioButton rb = hb.getRBList().get(currentTabIndex);
				hb.setRadioButtonText(rb, currentTabIndex, getCurrentTabPhotoNames(true).size());
			}
		}
	}

	/**
	 * @param requestCode
	 * @return
	 */
	public static boolean isFromTTP(int requestCode) {
		return requestCode == PhotoView.OPEN_CAMERA_ACTIVITY || requestCode == PhotoView.OPEN_PHOTO_ACTIVITY;
	}

	/**
	 * 打包上传图片时调用
	 * 
	 * @param params
	 * @param zipFileName
	 */
	public void uploadZip(RequestParams params, String zipFileName) {
		try {
			if (!isPhotoEmpty(false)) {
				File zipFile = FileUtil.createZipFile(zipFileName, getLocalFiles());
				params.put("file", zipFile);
				params.put("config", getConfigStr());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public List<File> getFile() {
		return getLocalFiles();
	}

	/**
	 * 停止上传,防止和手动上传冲突
	 */
	public void abortUploading() {
		// for (int i = 0; i < listReq_uploading.size(); i++) {
		// listReq_uploading.get(i).cancel(true);
		// listReq_uploading.remove(i);
		// }
		if (pvs == null)
			return;

		for (int i = 0; i < pvs.length; i++) {
			pvs[i].abortUploading();
		}

	}

	public int getCurrentTabIndex() {
		return hb == null ? 0 : hb.getCurrentIndex();
	}

	public TabParam getCurrentTabParam() {
		int index = getCurrentTabIndex();
		return getCurrentTabParam(index);
	}

	public TabParam getCurrentTabParam(int index) {
		return mTabParamList.get(index);
	}

	// 含serverImg
	public boolean isPhotoEmpty(boolean incServer) {
		return getAllTabPhotoNames(incServer).isEmpty();
	}

	/**
	 * 获取当前tab下的文件名
	 */
	public List<String> getCurrentTabPhotoNames(boolean incServer) {
		int index = getCurrentTabIndex();
		return getPhotoNames(index, incServer);
	}

	public List<String> getPhotoNames(int index, boolean incServer) {
		return pvs[index].getPhotoNames(incServer);
	}

	/**
	 * 获取所有tab下的文件名
	 */
	public List<String> getAllTabPhotoNames(boolean incServer) {
		int tabCount = hb == null ? 1 : hb.getTabCount();
		List<String> all = new ArrayList<String>();
		for (int i = 0; i < tabCount; i++) {
			all.addAll(getPhotoNames(i, incServer));
		}
		return all;
	}

	public int getPhotoCnt(boolean incServer) {
		List<String> list = getAllTabPhotoNames(incServer);
		return list == null ? 0 : list.size();
	}

	public String getConfigStr() throws JSONException {
		int tabCount = hb == null ? 1 : hb.getTabCount();
		JSONArray array = new JSONArray();
		for (int i = 0; i < tabCount; i++) {
			List<String> names = getPhotoNames(i, false);
			for (int j = 0; j < names.size(); j++) {
				JSONObject obj = new JSONObject();
				obj.put("name", names.get(j));
				obj.put("file_with_class", mTabParamList.get(i).file_with_class);
				array.put(obj);
			}
		}
		System.out.println("上传图片Config= " + array.toString());
		return array.toString();
	}

	// /**
	// * 获取当前上传的图片文件数组
	// */
	// public List<File> getCurrentTabPhotoFiles() {
	// List<File> files = new ArrayList<File>();
	// List<String> fileNames = getCurrentTabPhotoNames();
	//
	// for (String fileName : fileNames) {
	// files.add(new File(pvs[hb.getCurrentIndex()].getPath_Upload(),
	// fileName));
	// }
	// return files;
	// }

	/**
	 * 获取上传的图片文件数组,用于上传(仅含"本地图片")
	 */
	public List<File> getLocalFiles() {
		List<File> files = new ArrayList<File>();
		// List<String> allFileNames = getAllTabPhotoNames(false);
		// for (String fileName : allFileNames) {
		// files.add(new File(pvs[hb.getCurrentIndex()].getPath_Upload(),
		// fileName));
		// }

		// debug
		for (int i = 0; i < pvs.length; i++) {
			PhotoView pv = pvs[i];
			List<File> tmp = pv.getLocalFiles();
			if (tmp != null)
				files.addAll(tmp);
		}
		return files;
	}

	/**
	 * 清除所有view、本地图片,不会调接口删除"服务端图片"
	 */
	public void clearData() {
		clearData(false);
	}

	/**
	 * 清除所有tab下的照片数据及文件,本地文件、视图,不会调接口删除"服务端图片"
	 * 
	 * @param keepServerView
	 *            不动serverImg的view
	 */
	public void clearData(boolean keepServerView) {
		int tabCount = hb == null ? 1 : hb.getTabCount();
		for (int i = 0; i < tabCount; i++) {
			PhotoView tmpPv = pvs[i];
			List<TTPImgBean> tmpList = tmpPv.getPhotoBeans();
			int tmpSize = tmpList.size();
			// 注:photoView.removeItem中也移除了数据
			for (int j = 0; j < tmpSize; j++) {
				TTPImgBean tmpBean = tmpList.get(0);
				if (keepServerView && tmpBean.isServerImg())
					continue;
				tmpPv.removeItem(tmpBean);
			}
		}
		if (hb == null)
			return;
		for (int i = 0; i < tabCount; i++) {
			hb.setRadioButtonText(hb.getRBList().get(i), i, 0);
		}
	}

	/**
	 * 清除"imageLoader的缓存",rom、ram
	 */
	public void clearCache() {
		for (int i = 0; i < mTabParamList.size(); i++) {
			pvs[i].clearCache();
		}
	}

	// 由photoView回调,用于TTP在ListView中时 通知外部当前"调用相机"的ttb
	public void onBtnAddClick() {
		if (onClick_btnAdd != null)
			onClick_btnAdd.onClick(this);
	}

	// 使"临时文件"有效,即去掉"_tmp"
	public void validTmpPhotos() {

		if (pvs == null)
			return;

		for (int i = 0; i < pvs.length; i++) {

			PhotoView tmpPV = pvs[i];
			File dir_cam = new File(tmpPV.getPath_Camera());
			final File dir_thumbnail = new File(tmpPV.getPath_Thumb());
			final File dir_upload = new File(tmpPV.getPath_Upload());
			dir_cam.listFiles(new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					// TODO Auto-generated method stub
					String filename = pathname.getName();
					if (filename.contains("_tmp")) {
						String newFileName = filename.replace("_tmp", "");

						// 移除"_tmp"
						Utility.renameFile(pathname, newFileName);
						Utility.renameFile(new File(dir_thumbnail, filename), newFileName);
						Utility.renameFile(new File(dir_upload, filename), newFileName);
					}
					return false;
				}
			});
		}

	}

	public HorizontalTab getTab() {
		return hb;
	}

	public List<Checkable> getCheckableList() {
		return mCheckableList;
	}

	public void setTabParamList(List<TabParam> params) {
		this.mTabParamList = params;
	}

	public OnTTPDeleteListener getOnTTPDeleteListener() {
		return onTTPDeleteListener;
	}

	public void setOnTTPDeleteListener(OnTTPDeleteListener onTTPDeleteListener) {
		this.onTTPDeleteListener = onTTPDeleteListener;
	}

	private Context mContext;

	private HorizontalTab hb;
	// debug
	public PhotoView[] pvs;

	private List<TabParam> mTabParamList;

	private List<Checkable> mCheckableList = new ArrayList<Checkable>();

	// ==================nested===============================

	private OnTTPDeleteListener onTTPDeleteListener;

	public interface OnTTPDeleteListener {
		/**
		 * 删除后,1.若删server图片 则网络请求成功后
		 */
		void onDeleteComplete(int tabIndex, TTPImgBean b);
	}
}
