/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package oso.widget.photo.pager;

import java.util.Arrays;

import oso.ui.MainActivity;
import oso.widget.photo.pagerindicator.CirclePageIndicator;
import oso.widget.photo.pagerindicator.HackyViewPager;
import oso.widget.photo.pagerindicator.PageIndicator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import declare.com.vvme.R;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
/**
 * images 参数图片的地址 String imageUri = "http://site.com/image.png"; // from Web
 * String imageUri = "file:///mnt/sdcard/image.png"; // from SD card String
 * imageUri = "content://media/external/audio/albumart/13"; // from content
 * provider String imageUri = "assets://image.png"; // from assets String
 * imageUri = "drawable://" + R.drawable.image; // from drawables (only images,
 * non-9patch)
 */
public class ImagePagerActivity extends Activity {

	protected ImageLoader imageLoader = ImageLoader.getInstance();

	DisplayImageOptions options;

	private static final String STATE_POSITION = "STATE_POSITION";

	private static final String IMAGES = "images";

	private static final String IMAGE_POSITION = "image_index";

	HackyViewPager pager;
	PageIndicator mIndicator;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_pager);

		// initImageLoader(this);

		Bundle bundle = getIntent().getExtras();
		String[] imageUrls = bundle.getStringArray(IMAGES);
		int pagerPosition = bundle.getInt(IMAGE_POSITION, 0);

		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
		}

		options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.drawable.ic_empty).showImageOnFail(R.drawable.ic_error)
				.resetViewBeforeLoading(true).cacheOnDisc(true)
				// debug
				.cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.ARGB_8888)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		pager = (HackyViewPager) findViewById(R.id.pager);
		// pager.setAdapter(new ImagePagerAdapter(imageUrls,this));
		pager.setAdapter(new ImagePagerAdapter(Arrays.asList(imageUrls), this));
		pager.setCurrentItem(pagerPosition);

		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(pager);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == 137) {
			finish();
			Intent intent = new Intent(ImagePagerActivity.this, MainActivity.class);
			startActivity(intent);
		}
		return super.onKeyDown(keyCode, event);
	}

	// public void initImageLoader(Context context) {
	// // This configuration tuning is custom. You can tune every option, you
	// may tune some of them,
	// // or you can create default configuration by
	// // ImageLoaderConfiguration.createDefault(this);
	// // method.
	// ImageLoaderConfiguration config = new
	// ImageLoaderConfiguration.Builder(context)
	// .threadPriority(Thread.NORM_PRIORITY - 2)
	// .denyCacheImageMultipleSizesInMemory()
	// .discCacheFileNameGenerator(new Md5FileNameGenerator())
	// .tasksProcessingOrder(QueueProcessingType.LIFO)
	// //.writeDebugLogs() // Remove for release app
	// .build();
	// // Initialize ImageLoader with configuration.
	// ImageLoader.getInstance().init(config);
	// }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
	}
	

	// private class ImagePagerAdapter extends PagerAdapter {
	//
	// private String[] images;
	// private LayoutInflater inflater;
	// private Context mContext;
	//
	// ImagePagerAdapter(String[] images,Context context) {
	// this.images = images;
	// this.mContext=context;
	// inflater = getLayoutInflater();
	// }
	// @Override
	// public void destroyItem(ViewGroup container, int position, Object object)
	// {
	// ((ViewPager) container).removeView((View) object);
	// }
	//
	// @Override
	// public void finishUpdate(View container) {
	// }
	//
	// @Override
	// public int getCount() {
	// return images.length;
	// }
	//
	// @Override
	// public Object instantiateItem(ViewGroup view, int position) {
	// View imageLayout = inflater.inflate(R.layout.item_pager_image, view,
	// false);
	//
	// final PhotoView imageView = (PhotoView)
	// imageLayout.findViewById(R.id.image);
	// final ProgressBar spinner = (ProgressBar)
	// imageLayout.findViewById(R.id.loading);
	// final Button left_n = (Button) imageLayout.findViewById(R.id.left_n);
	// final Button right_n = (Button) imageLayout.findViewById(R.id.right_n);
	// imageLoader.displayImage(images[position], imageView, options, new
	// SimpleImageLoadingListener() {
	// @Override
	// public void onLoadingStarted(String imageUri, View view) {
	// spinner.setVisibility(View.VISIBLE);
	// left_n.setVisibility(View.GONE);
	// right_n.setVisibility(View.GONE);
	// }
	//
	//
	// @Override
	// public void onLoadingFailed(String imageUri, View view, FailReason
	// failReason) {
	// String message = null;
	// switch (failReason.getType()) {
	// case IO_ERROR:
	// message = "Input/Output error";
	// break;
	// case DECODING_ERROR:
	// message = "Image can't be decoded";
	// break;
	// case NETWORK_DENIED:
	// message = "Downloads are denied";
	// break;
	// case OUT_OF_MEMORY:
	// message = "Out Of Memory error";
	// break;
	// case UNKNOWN:
	// message = "Unknown error";
	// break;
	// }
	// UIHelper.showToast(ImagePagerActivity.this, message,
	// Toast.LENGTH_SHORT).show();
	//
	// spinner.setVisibility(View.GONE);
	//
	// left_n.setVisibility(View.GONE);
	// right_n.setVisibility(View.GONE);
	// }
	//
	// @Override
	// public void onLoadingComplete(String imageUri, View view, final Bitmap
	// loadedImage) {
	// spinner.setVisibility(View.GONE);
	// left_n.setVisibility(View.VISIBLE);
	// right_n.setVisibility(View.VISIBLE);
	// final Map<String, Float> bbs = new HashMap<String, Float>();
	// bbs.put("degree", 0f);
	// left_n.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// if(bbs.get("degree")==-360||bbs.get("degree")==360){
	// bbs.put("degree", 0f);
	// }
	// bbs.put("degree", bbs.get("degree")-90);
	// bbq(loadedImage, imageView, bbs.get("degree"),bbs.get("degree")+90);
	// }
	// });
	// right_n.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// if(bbs.get("degree")==-360||bbs.get("degree")==360){
	// bbs.put("degree", 0f);
	// }
	// bbs.put("degree", bbs.get("degree")+90);
	// bbq(loadedImage, imageView, bbs.get("degree"),bbs.get("degree")-90);
	// }
	// });
	// }
	//
	//
	// });
	//
	// ((ViewPager) view).addView(imageLayout, 0);
	// return imageLayout;
	// }
	//
	// private void bbq(final Bitmap loadedImage,final PhotoView imageView,final
	// float progress,final float blackProgress){
	// final Matrix matrix = new Matrix();
	// matrix.setRotate(progress); //设置翻转的角度
	// //重新绘制翻转后的图片
	// DisplayMetrics dm=new DisplayMetrics();
	// getWindowManager().getDefaultDisplay().getMetrics(dm);
	// RotateAnimation rotateAnimation = null;
	// if(blackProgress<progress){
	// rotateAnimation = new RotateAnimation(-90, 0,0,dm.widthPixels/2,0,
	// dm.heightPixels/2);
	// rotateAnimation.setDuration(200);
	// imageView.startAnimation(rotateAnimation);
	// }
	// else{
	// rotateAnimation = new RotateAnimation(90, 0,0,dm.widthPixels/2,0,
	// dm.heightPixels/2);
	// rotateAnimation.setDuration(200);
	// imageView.startAnimation(rotateAnimation);
	// }
	//
	//
	// Bitmap sloadedImage = Bitmap.createBitmap(loadedImage, 0, 0,
	// loadedImage.getWidth(), loadedImage.getHeight(), matrix, true);
	// imageView.setImageBitmap(sloadedImage);
	//
	//
	// }
	// @Override
	// public boolean isViewFromObject(View view, Object object) {
	// return view.equals(object);
	// }
	//
	// @Override
	// public void restoreState(Parcelable state, ClassLoader loader) {
	// }
	//
	// @Override
	// public Parcelable saveState() {
	// return null;
	// }
	//
	// @Override
	// public void startUpdate(View container) {
	// }
	// }
}