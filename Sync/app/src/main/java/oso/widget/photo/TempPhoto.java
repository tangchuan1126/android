package oso.widget.photo;

import support.dbhelper.Goable;

public class TempPhoto {
	public static final String imageBasePath =  Goable.file_image_path; 
	private String imageFilePath  ; 
	private long currentTime ; 
 	
	public static final String imagePathOfThumbnail = Goable.file_image_path_thumbnail ;
	
 	public TempPhoto(){}
	
	public static TempPhoto  createTempPhoto(){
		TempPhoto tempPhoto = new TempPhoto();
		long currentTime = System.currentTimeMillis() ;
		tempPhoto.imageFilePath = imageBasePath+currentTime+".jpg";
		tempPhoto.currentTime = currentTime ;
 		return tempPhoto;
	}
	
	public String getImageFilePath() {
		return imageFilePath;
	}

	public long getCurrentTime() {
		return currentTime;
	}
}
