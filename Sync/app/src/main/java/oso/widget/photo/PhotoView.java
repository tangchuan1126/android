package oso.widget.photo;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.base.PhotoCheckable;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.photo.pager.ImagePagerActivity;
import support.common.AnimateFirstDisplayListener;
import support.common.UIHelper;
import support.common.datas.HoldDoubleValue;
import support.dbhelper.Goable;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.network.SimpleJSONUtil;
import utility.DisplayPictureUtil;
import utility.FileUtil;
import utility.FileUtils;
import utility.HttpUrlPath;
import utility.Utility;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

/**
 * TabToPhoto中tab下方"图片列表"
 * 
 * @author 朱成
 * @date 2014-11-3
 */
public class PhotoView extends LinearLayout {

	private TabParam param;
	public static final int  REQ_FROM_GAL = 466213+3;
	public static final int  REQ_CROP = 928112;
	private String path = "";
	// 若在TabToPhoto中 则可获取tap
	public TabToPhoto getTTP() {
		if (param.getAble() instanceof PhotoCheckable) {
			PhotoCheckable ab = (PhotoCheckable) param.getAble();
			return ab.photoCapture;
		}
		return null;
	}

	public int getTabIndex() {
		TabToPhoto ttp = getTTP();
		if (ttp == null)
			return 0;
		for (int i = 0; i < ttp.pvs.length; i++) {
			if (ttp.pvs[i] == this)
				return i;
		}
		return 0;
	}

	private boolean isAddToTmp() {
		TabToPhoto ttp = getTTP();
		if (ttp != null)
			return ttp.isAddToTmp;
		return false;
	}

	// private boolean isShowTmp() {
	// TabToPhoto ttp = getTTP();
	// if (ttp != null)
	// return ttp.isShowTmp;
	// return false;
	// }

	// ------------tab相关路径------------------

	private String tmpFile; // 拍照时的"临时文件-文件名"
	private boolean isEnavled = true;

	public String getPath_Camera() {
		return getFilePath("Camera");
	}

	public String getPath_Thumb() {
		return getFilePath("Thumbnail");
	}

	public String getPath_Upload() {
		return getFilePath("Upload");
	}

	// tab目录-子路径,不带"/"
	private String getFilePath(String type) {
		File dir = new File(Goable.getDir_takephoto(), param.getKey() + "/" + type);
		if (!dir.exists())
			dir.mkdirs();
		return dir.getPath();
	}

	// 刷新-当前文件名
	private String createFileName() {
		String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
		return fileName;
	}

	// "_tmp"为临时文件
	private String createFileName(boolean isTmp) {
		String append = isTmp ? "_tmp.jpg" : ".jpg";
		String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + append;
		return fileName;
	}

	// ==================================

	public PhotoView(Context context, TabParam param, boolean isShowLocalDatas) {
		super(context);
		mContext = context;
		this.param = param;
		imageViewSize = Utility.pxTodip(mContext, param.getIvSize());
		initView();
		initPhotoPath(param, isShowLocalDatas);

		if (param.hasWebImgs())
			reqServerUrls();
	}

	public PhotoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * 初始化方法
	 * 
	 * @param c
	 * @param prefix
	 */
	private void initView() {
		mInflater = LayoutInflater.from(mContext);
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View main = inflater.inflate(R.layout.widget_photoview, this, true);
		// 获得对子控件的引用
		photoLayout = (LinearLayout) main.findViewById(R.id.showImageLayout);
		addImageBtn = (ImageView) main.findViewById(R.id.addImageBtn);
		setImageViewSize(imageViewSize);
		addImageBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isEnavled) {
					// 刷新-文件名
					// pp.createFileName(isAddToTmp());
					tmpFile = createFileName(isAddToTmp());

					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(getPath_Camera(), tmpFile)));
					((Activity) mContext).startActivityForResult(intent, OPEN_CAMERA_ACTIVITY);

					// debug
					TabToPhoto ttp = getTTP();
					if (ttp != null)
						ttp.onBtnAddClick();
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
			}
		});
		addImageBtn.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (isEnavled) {
					tmpFile = createFileName(isAddToTmp());

					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					((Activity) mContext).startActivityForResult(intent, OPEN_PHOTO_ACTIVITY);
					// debug
					TabToPhoto ttp = getTTP();
					if (ttp != null)
						ttp.onBtnAddClick();
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
				return true;
			}
		});
	}
 	/**
	 * 设置加号按钮的可见性
	 * 
	 * @param visibility
	 */
	public void setAddBtnVisibility(boolean visibility) {
		addImageBtn.setVisibility(visibility ? View.VISIBLE : View.GONE);
	}

	public void setAddBtnEnabled(boolean enabled) {
		addImageBtn.setEnabled(enabled);
	}

	private void initPhotoPath(TabParam param, boolean isShowLocalDatas) {
		// debug
		// pp = new PhotoPath(param.getKey());
		if (isShowLocalDatas) {
			initLocalDatas();
		}
	}

	/**
	 * 赋值路径
	 * 
	 * @param key
	 */
	// public void setPath(String key) {
	// pp.setPath(key);
	// }

	/**
	 * 加载本地记录
	 */
	private void initLocalDatas() {
		File fileDir = new File(getPath_Thumb());
		File[] files = fileDir.listFiles();
		if (files != null) {
			for (File file : files) {
				addPhotoToIv(file.getName());
			}
		}
	}
	/**
	 * 压缩图片
	 * @param File
	 * @param bm
	 * @return File
	 */
	private File compressImg(File ret, Bitmap bm) {
		if(!TextUtils.isEmpty(path)) {   // 拍照 /mnt/sdcard/DCIM/Camera/1425979093242.jpg
			ret = new File(path);
			 try {
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(ret));
                bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                bos.flush();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
			ret=Utility.scaleFile(ret, new PointF(600f,800f));
			return ret;
		}
		return null;
	}
	public File handleImg(File ret, Bitmap bm) {
		ret = compressImg(ret, bm);
		if(ret==null) return null;
		if(!ret.exists()){
			UIHelper.showToast(mContext.getString(R.string.account_setting_invalid_image));
			return null;
		}
		bm.recycle();
		bm=null;
		return ret;
	}
	/**
	 * 在目标activity.onActivityResult中回调,以获取图片
	 * 
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_CANCELED)
			return;
		switch (requestCode) {
		case OPEN_CAMERA_ACTIVITY:
			handPhoto();
			break;
		case REQ_CROP:
			//裁剪
			File ret = null;
			Bitmap bm = data.getExtras().getParcelable("data");
			//ret = compressImg(ret, bm);
			ret = handleImg(ret, bm);
			if(ret!=null) {
				handPhoto(ret);
				ret = null;
			}
			path = "";
			break;
		case REQ_FROM_GAL:

			// 做非空判断，当我们觉得不满意想重新剪裁的时候便不会报异常，下同
            if (data != null) {
            	//Bitmap cameraBitmap = (Bitmap) data.getExtras().get("data"); 
    			Uri galleryUri = data.getData();
    			ContentResolver cr = mContext.getContentResolver();
    			Cursor cur = cr.query(galleryUri, null, null, null, null);
    			if(cur == null) {
    				UIHelper.showToast(mContext.getString(R.string.sync_select_gallery));
    				return;
    			}
    			cur.moveToFirst();
    			if (cur != null) {
    				int column = cur.getColumnIndex("_data");
    				if(column < 0) {
    					UIHelper.showToast(mContext.getString(R.string.sync_select_gallery));
    					return;
    				}
    				path = cur.getString(column);
    				cur.close();
    			}
                startPhotoZoom(data.getData());
            } else {
            }
			break;
		}
 
		if (requestCode == OPEN_PHOTO_ACTIVITY)
			doPickPhoto(data);
	}
	private void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("aspectX", 1);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 350);
        intent.putExtra("outputY", 350);
        intent.putExtra("return-data", true);
        ((Activity) mContext).startActivityForResult(intent, REQ_CROP);
	}
	private void handPhoto(File newAvatarFile) {

		if (!Utility.isSDExits()) {
			UIHelper.showToast(mContext, mContext.getResources().getString(R.string.photo_no_sdcard), Toast.LENGTH_SHORT).show();
			return;
		}
		// 获取bitmap
//		Bitmap bm = BitmapFactory.decodeFile(new File(getPath_Camera(), tmpFile).getPath());
//		// 保存上传图片
//		BitmapUtils.zoomImage(bm, new File(getPath_Upload(), tmpFile).getPath(), param.getZoomMaxSize());
//		// 保存缩略图
//		bm = BitmapFactory.decodeFile(new File(getPath_Upload(), tmpFile).getPath());
//		BitmapUtils.zoomImage(bm, new File(getPath_Thumb(), tmpFile).getPath(), 10);
		
		//debug
		File file_cam=new File(getPath_Camera(), path);
		File file_upload=new File(getPath_Upload(), path);
		File file_thumb=new File(getPath_Thumb(), path);
		Utility.scaleFile(file_cam, file_upload, param.getMaxSize_upload());
		Utility.scaleFile(file_cam, file_thumb, new PointF(150,150));
		
		// 控件上添加缩略图
		addPhotoToIv(path);
		System.gc();
	
	}
	// 拍照后
	private void handPhoto() {
		if (!Utility.isSDExits()) {
			UIHelper.showToast(mContext, mContext.getResources().getString(R.string.photo_no_sdcard), Toast.LENGTH_SHORT).show();
			return;
		}
		// 获取bitmap
		// Bitmap bm = BitmapFactory.decodeFile(new File(getPath_Camera(),
		// tmpFile).getPath());
		// // 保存上传图片
		// BitmapUtils.zoomImage(bm, new File(getPath_Upload(),
		// tmpFile).getPath(), param.getZoomMaxSize());
		// // 保存缩略图
		// bm = BitmapFactory.decodeFile(new File(getPath_Upload(),
		// tmpFile).getPath());
		// BitmapUtils.zoomImage(bm, new File(getPath_Thumb(),
		// tmpFile).getPath(), 10);

		// debug
		File file_cam = new File(getPath_Camera(), tmpFile);
		File file_upload = new File(getPath_Upload(), tmpFile);
		File file_thumb = new File(getPath_Thumb(), tmpFile);
		Utility.scaleFile(file_cam, file_upload, param.getMaxSize_upload());
		Utility.scaleFile(file_cam, file_thumb, new PointF(150, 150));

		// 控件上添加缩略图
		addPhotoToIv(tmpFile);
		System.gc();
	}

	private void doPickPhoto(Intent data) {
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picPath = cursor.getString(columnIndex);
		cursor.close();
		if (picPath != null) {
			File file_cam = new File(getPath_Camera(), tmpFile);
			FileUtils.copyFile(picPath, file_cam.getAbsolutePath());
			// debug
			File file_upload = new File(getPath_Upload(), tmpFile);
			File file_thumb = new File(getPath_Thumb(), tmpFile);
			Utility.scaleFile(file_cam, file_upload, param.getMaxSize_upload());
			Utility.scaleFile(file_cam, file_thumb, new PointF(150, 150));
			// 控件上添加缩略图
			addPhotoToIv(tmpFile);
			System.gc();
		}
	}

	// 设置-条目状态,local/server
	private void setItemState(View loItem, boolean isServer) {
		ImageView photoIv = (ImageView) loItem.findViewById(R.id.photoIv);
		TextView flagTv = (TextView) loItem.findViewById(R.id.flagTv);

		int bgImg = isServer ? R.drawable.photoview_server : R.drawable.photoview_local;
		String str = isServer ? "Server" : "Local";
		int bgTv = isServer ? 0xff007aff : 0xff900000;

		photoIv.setBackgroundResource(bgImg);
		flagTv.setText(str);
		flagTv.setBackgroundColor(bgTv);
	}

	public void addWebPhotoToIv(List<TTPImgBean> list) {
		if (list == null)
			return;
		for (int i = 0; i < list.size(); i++)
			addWebPhotoToIv(list.get(i));
	}

	/**
	 * ImageView添加网络图片
	 */
	public void addWebPhotoToIv(final TTPImgBean imgBean) {
		final String webUrl = HttpUrlPath.basePath + imgBean.uri;
		RelativeLayout ivContainer = (RelativeLayout) mInflater.inflate(R.layout.widget_photoview_server, null);
		ivContainer.setTag(imgBean);
		ImageView iv = (ImageView) ivContainer.findViewById(R.id.photoIv);
		TextView flagTv = (TextView) ivContainer.findViewById(R.id.flagTv);
		setImageViewSize(iv, imageViewSize, false);
		setImageViewSize(flagTv, imageViewSize, true);
		imageLoader.displayImage(webUrl, iv, DisplayPictureUtil.getDisplayImageOptions(), new AnimateFirstDisplayListener());
		// 加至第一个,photoLayout.getChildCount()
		photoLayout.addView(ivContainer, 1);
		photoNames.add(0, imgBean);
		// 点击查看的
		iv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isEnavled) {
					XnView(imgBean);
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
			}
		});
		iv.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (isEnavled) {
					affrDelete(imgBean);
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
				return true;
			}
		});
	}

	/**
	 * ImageView添加图片
	 * 
	 * @param fileName
	 */
	public void addPhotoToIv(final String fileName) {
		final TTPImgBean imgBean = new TTPImgBean(fileName);
		final RelativeLayout ivContainer = (RelativeLayout) mInflater.inflate(R.layout.widget_photoview_local, null);
		ImageView iv = (ImageView) ivContainer.findViewById(R.id.photoIv);
		TextView flagTv = (TextView) ivContainer.findViewById(R.id.flagTv);
		setImageViewSize(iv, imageViewSize, false);
		setImageViewSize(flagTv, imageViewSize, true);
		// imageLoader.displayImage("file://" + pp.thumbnailPath + fileName, iv,
		// DisplayPictureUtil.getDisplayImageOptions(),
		// new AnimateFirstDisplayListener());
		// debug
		imageLoader.displayImage("file://" + new File(getPath_Thumb(), fileName).getPath(), iv, DisplayPictureUtil.getDisplayImageOptions(),
				new AnimateFirstDisplayListener());
		ivContainer.setTag(imgBean);
		// 加至-最左侧
		photoLayout.addView(ivContainer, 1);
		photoNames.add(0, imgBean);
		// 点击查看的
		iv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isEnavled) {
					XnView(imgBean);
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
			}
		});
		iv.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (isEnavled) {
					affrDelete(imgBean);
				} else {
					CheckInMainDocumentsStatusTypeKey.ToashStr();
				}
				return true;
			}
		});

		// debug
		// if (param.hasWebImgs()) {
		if (param.canAutoUpload()) {
			postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					uploadImg(ivContainer, imgBean);
				}
			}, 300);
		}
	}

	// 显示大图
	private void XnView(TTPImgBean imgBean) {
		Intent intent = new Intent(mContext, ImagePagerActivity.class);
		// 这里给出图片的地址.
		// images 图片地址
		// image_index从第几张开始显示
		HoldDoubleValue<Integer, String[]> result = getPhotoParam(imgBean.uri);
		intent.putExtra("image_index", result.a);
		intent.putExtra("images", result.b);
		mContext.startActivity(intent);
	}

	/**
	 * 禁用控件
	 */
	public void setPVEnabled(boolean enabled) {
		isEnavled = enabled;
	}

	/**
	 * 删除fileName
	 * 
	 * @param fileName
	 * @descrition
	 */
	// ,final boolean isServerUrl
	private void affrDelete(final TTPImgBean imgBean) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mContext);
		builder.setTitle(mContext.getResources().getString(R.string.sys_holdon));
		// builder.setMessage(mContext.getResources().getString(R.string.delete_local_file));
		builder.setMessage(mContext.getString(R.string.sync_delete_notify));
		builder.setNegativeButton(R.string.sync_cancel, null);
		builder.setPositiveButton(mContext.getResources().getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (imgBean.isServerImg()) {
					delServerImg(imgBean);
					return;
				}
				removeItem(imgBean);
			}
		});
		builder.create().show();
	}

	/**
	 * 删除条目,数据/本地图片、视图,注:1.serverImg并不在服务端删除 2.删除所有时,for易出问题(该函数内也有"移除数据")
	 * 
	 * @param fileName
	 */
	public void removeItem(TTPImgBean imgBean) {
		// 移除view
		View view = photoLayout.findViewWithTag(imgBean);
		if (view != null) {
			photoLayout.removeView(view);
		}
		photoNames.remove(imgBean);

		// 若为本地图片
		if (!imgBean.isServerImg())
			delPhotoFiles(imgBean.uri);
	}

	public void delPhotoFiles(String fileName) {
		// 删除本地文件
		Utility.delFolder(new File(getPath_Camera(), fileName));
		Utility.delFolder(new File(getPath_Thumb(), fileName));
		Utility.delFolder(new File(getPath_Upload(), fileName));
	}

	// public String get

	/**
	 * 清除缓存
	 */
	public void clearCache() {
		imageLoader.clearDiscCache();
		imageLoader.clearMemoryCache();
	}

	/**
	 * 获取要显示的图片的地址，然后还有当前图片的Index
	 * 
	 * @param fileName
	 * @return image_index (int index) , String[] images ;
	 * @descrition
	 */
	private HoldDoubleValue<Integer, String[]> getPhotoParam(String url) {
		int totalSize = photoNames.size();
		String[] urls = new String[totalSize];
		int showIndex = 0;
		for (int index = 0, count = photoNames.size(); index < count; index++) {
			TTPImgBean tmp = photoNames.get(index);
			if (url.equals(tmp.uri))
				showIndex = index;

			if (!tmp.isServerImg())
				urls[index] = "file://" + getPath_Camera() + "/" + photoNames.get(index).uri;
			else
				urls[index] = tmp.getWebFullUrl();
		}
		HoldDoubleValue<Integer, String[]> returnValue = new HoldDoubleValue<Integer, String[]>(showIndex, urls);
		return returnValue;
	}

	/**
	 * 赋值加号的大小
	 * 
	 * @param size
	 */
	public void setImageViewSize(int size) {
		imageViewSize = size;
		LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) addImageBtn.getLayoutParams(); // 取控件textView当前的布局参数
		linearParams.height = size;// 控件的高
		linearParams.width = size;// 控件的宽
		addImageBtn.setLayoutParams(linearParams);

		// "加号"都隐藏时,保持高度
		ViewGroup.LayoutParams lp = photoLayout.getLayoutParams();
		if (lp != null) {
			lp.height = size;
			photoLayout.requestLayout();
		}
	}

	/**
	 * 赋值照片的大小
	 * 
	 * @param iv
	 * @param size
	 */
	public void setImageViewSize(View v, int size, boolean isFlag) {
		RelativeLayout.LayoutParams ivParams = (RelativeLayout.LayoutParams) v.getLayoutParams(); // 取控件当前的布局参数
		if (isFlag) {
			ivParams.width = size - Utility.pxTodip(mContext, 4);// 控件的宽
		} else {
			ivParams.width = size;// 控件的宽
			ivParams.height = size;// 控件的高
		}
		v.setLayoutParams(ivParams);
	}

	// 仅含localImg,用于上传
	public List<String> getPhotoNames(boolean incServer) {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < photoNames.size(); i++) {
			TTPImgBean tmpB = photoNames.get(i);
			if (incServer)
				list.add(tmpB.uri);
			else
			// 若仅添加本地
			{
				if (!tmpB.isServerImg())
					list.add(tmpB.uri);
			}
		}
		return list;
	}

	// 仅含localImg,用于上传
	public List<File> getLocalFiles() {
		List<File> list = new ArrayList<File>();
		for (int i = 0; i < photoNames.size(); i++) {
			TTPImgBean tmpB = photoNames.get(i);
			// 若仅添加本地
			if (!tmpB.isServerImg())
				list.add(new File(getPath_Upload(), tmpB.uri));
		}
		return list;
	}

	public List<TTPImgBean> getPhotoBeans() {
		return photoNames;
	}

	public void setPhotoNames(List<String> addPhotoNames) {
		for (String fileName : addPhotoNames) {
			addPhotoToIv(fileName);
		}
	}

	public int getImageViewSize() {
		return imageViewSize;
	}

	// =====网络访问========================================================

	private List<RequestHandle> listReq_uploading = new ArrayList<RequestHandle>(); // 上传中的-请求s

	/**
	 * 停止上传,防止和手动上传冲突
	 */
	public void abortUploading() {
		for (int i = 0; i < listReq_uploading.size(); i++) {
			// debug
			System.out.println("nimei>>cancel_req:" + listReq_uploading.get(i));

			listReq_uploading.get(i).cancel(true);
			listReq_uploading.remove(i);
		}
	}

	// 上传图片
	private void uploadImg(final View loItem, final TTPImgBean imgBean) {

		// 压缩图片
		List<File> imgs = new ArrayList<File>();
		imgs.add(new File(new File(getPath_Upload(), imgBean.uri).getPath()));
		final File zipFile = FileUtil.createZipFile("ttp_" + new Date().getTime(), imgs);

		RequestParams p = new RequestParams();
		p.add("Method", "UpFile");
		p.add("file_with_class", param.file_with_class);
		p.add("file_with_type", param.file_with_type);
		p.add("file_with_id", param.file_with_id);
		// p.put("config", getConfigStr(imgs.get(0).getName(),
		// param.file_with_class));
		if (!TextUtils.isEmpty(param.file_with_detail_id))
			p.add("file_with_detail_id", param.file_with_detail_id);
		if (!TextUtils.isEmpty(param.option_file_param))
			p.add("option_file_param", param.option_file_param);

		try {
			p.put("gate_out", zipFile);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		SimpleJSONUtil req = new SimpleJSONUtil() {

			public void handFinish() {

				// debug
				if (reqHandle != null)
					listReq_uploading.remove(reqHandle);
				// 移除zip
				if (zipFile != null)
					zipFile.delete();

				// debug
				logReqsCnt();
			};

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				String oldUri = imgBean.uri;
				JSONObject jo = json.optJSONArray("datas").optJSONObject(0);
				imgBean.uri = jo.optString("uri");
				imgBean.file_id = jo.optString("file_id");

				// 修改状态
				ImageView photoIv = (ImageView) loItem.findViewById(R.id.photoIv);
				imageLoader.displayImage(HttpUrlPath.basePath + imgBean.uri, photoIv, DisplayPictureUtil.getDisplayImageOptions(),
						new AnimateFirstDisplayListener());
				setItemState(loItem, true);
				// 删除本地图片,三种
				delPhotoFiles(oldUri);
				// zipFile.delete();
			}

			// public void handFail() {
			// System.out.println("nimei>>pv.fail");
			// if(zipFile!=null)
			// zipFile.delete();
			// };

		};
		req.doPost(HttpUrlPath.FileUpZipAction, p, getContext(), true);
		// debug
		listReq_uploading.add(req.reqHandle);
	}

	// debug
	private void logReqsCnt() {
		System.out.println("nimei>>ttp.reqCnt:" + listReq_uploading.size());
	}

	// private String getConfigStr(String name, String file_with_class) {
	// JSONArray array = new JSONArray();
	// JSONObject obj = new JSONObject();
	// try {
	// obj.put("name", name);
	// obj.put("file_with_class", file_with_class);
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	// array.put(obj);
	// System.out.println("上传图片Config= " + array.toString());
	// return array.toString();
	// }

	// 拉取-服务端图片
	public void reqServerUrls() {
		RequestParams p = new RequestParams();
		p.add("Method", "GetFileUri");
		p.add("file_with_class", param.file_with_class);
		p.add("file_with_type", param.file_with_type);
		p.add("file_with_id", param.file_with_id);
		if (!TextUtils.isEmpty(param.file_with_detail_id))
			p.add("file_with_detail_id", param.file_with_detail_id);
		if (!TextUtils.isEmpty(param.option_file_param))
			p.add("option_file_param", param.option_file_param);

		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub

				JSONArray jaDatas = json.optJSONArray("datas");
				if (jaDatas == null || jaDatas.length() == 0)
					return;
				List<TTPImgBean> tmpList = new Gson().fromJson(jaDatas.toString(), new TypeToken<List<TTPImgBean>>() {
				}.getType());

				System.out.println("");
				for (int i = 0; i < tmpList.size(); i++)
					addWebPhotoToIv(tmpList.get(i));
			}

			public void handFail() {
				System.err.println("nimei>>pv.reqServerUrls.fail");
			};
		}.doPost(HttpUrlPath.FileUpZipAction, p, getContext(), true);

	}

	// relUrl:相对url
	private void delServerImg(final TTPImgBean imgBean) {
		RequestParams p = new RequestParams();
		p.add("Method", "DeleteFile");
		p.add("file_id ", imgBean.file_id);
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
				// TODO Auto-generated method stub
				removeItem(imgBean);

				TabToPhoto ttp = getTTP();
				if (ttp != null && ttp.getOnTTPDeleteListener() != null)
					ttp.getOnTTPDeleteListener().onDeleteComplete(getTabIndex(), imgBean);
			}
		}.doPost(HttpUrlPath.FileUpZipAction, p, getContext());
	}

	private Context mContext;
	private LayoutInflater mInflater;
	public static final int OPEN_CAMERA_ACTIVITY = 0x10;
	public static final int OPEN_PHOTO_ACTIVITY = 0x11;

	private ImageView addImageBtn; // 添加按钮
	private LinearLayout photoLayout; // 存放图片的LinearLayout

	// ImageLoader
	private ImageLoader imageLoader = DisplayPictureUtil.getImageLoader();

	private List<TTPImgBean> photoNames = new ArrayList<TTPImgBean>(); // 文件名-列表
	// private List<String> photoWebNames = new ArrayList<String>();

	private int imageViewSize = 0; // 默认60dp

}
