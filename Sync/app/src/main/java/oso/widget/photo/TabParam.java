package oso.widget.photo;

import oso.base.Checkable;
import oso.base.PhotoCheckable;
import android.graphics.PointF;
import android.text.TextUtils;

/**TabToPhoto下一个tab,含:名称、路径等
 * @author 朱成
 * @date 2014-11-3
 */
public class TabParam { 
	
	private CharSequence tabName;
	private String key;
	private Checkable able;
	private int ivSize;	
//	private int zoomMaxSize = 50; //图片允许最大空间 单位：KB
	
	//网络图片-请求参数
	public String file_with_class;
	public String file_with_type;
	public String file_with_id;
	public String file_with_detail_id;		//如:dol
	public String option_file_param;	//如:customer(用于区分mbol)
	
	//debug
	public boolean disable_autoUpload=false;
	public PointF maxSize_upload=new PointF(1000f, 1000f);	//最大上传尺寸
	
	/**
	 * @return
	 */
	public PointF getMaxSize_upload(){
		if(maxSize_upload!=null && maxSize_upload.x>0f&& maxSize_upload.y>0f)
			return maxSize_upload;
		return new PointF(1000f, 1000f);
	}
	
	public boolean canAutoUpload(){
		return !TextUtils.isEmpty(file_with_class)&&!disable_autoUpload;
	}
	
	public static TabParam new_NoTitle(String key,TabToPhoto ttp){
		return new TabParam("NoTitleBar", key, new PhotoCheckable(0, "Load", ttp));
	}

	public TabParam(CharSequence tabName, String key, Checkable able) {
		setParam(tabName, key, able, 50);
	}

	public TabParam(CharSequence tabName, String key, Checkable able, int ivSize) {
		setParam(tabName, key, able, ivSize);
	}

	public void setParam(CharSequence tabName, String key, Checkable able, int ivSize) {
		this.tabName = tabName;
		this.key = key;
		this.able = able;
		this.ivSize = ivSize;
	}
	
	//网络图片-请求参数,设置后 每添加一张本地图片后均会"自动上传"
	public void setWebImgsParams(String file_with_class,String file_with_type,String file_with_id){
		setWebImgsParams(file_with_class, file_with_type, file_with_id, "","");
	}
	
	// 网络图片-请求参数,设置后 每添加一张本地图片后均会"自动上传"
	public void setWebImgsParams(String file_with_class, String file_with_type,
			String file_with_id,String file_with_detail_id) {
//		this.file_with_class = file_with_class;
//		this.file_with_type = file_with_type;
//		this.file_with_id = file_with_id;
//		this.file_with_detail_id=file_with_detail_id;
		setWebImgsParams(file_with_class, file_with_type, file_with_id,file_with_detail_id,"");
	}
	
	// 网络图片-请求参数,设置后 每添加一张本地图片后均会"自动上传"
	public void setWebImgsParams(String file_with_class, String file_with_type,
			String file_with_id, String file_with_detail_id,
			String option_file_param) {
		this.file_with_class = file_with_class;
		this.file_with_type = file_with_type;
		this.file_with_id = file_with_id;
		this.file_with_detail_id = file_with_detail_id;
		this.option_file_param = option_file_param;
	}
	
	//是否需加载
	public boolean hasWebImgs(){
		return !TextUtils.isEmpty(file_with_class);
	}
	
	//==========================================================

	public CharSequence getTabName() {
		return tabName;
	}

	public void setTabName(CharSequence tabName) {
		this.tabName = tabName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Checkable getAble() {
		return able;
	}

	public void setAble(Checkable able) {
		this.able = able;
	}

	public int getIvSize() {
		return ivSize;
	}

	public void setIvSize(int ivSize) {
		this.ivSize = ivSize;
	}

//	public int getZoomMaxSize() {
//		return zoomMaxSize;
//	}
//
//	public void setZoomMaxSize(int zoomMaxSize) {
//		this.zoomMaxSize = zoomMaxSize;
//	}
}
