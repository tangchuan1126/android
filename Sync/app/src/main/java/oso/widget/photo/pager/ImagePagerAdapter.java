package oso.widget.photo.pager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oso.widget.photo.photoview.PhotoView;
import utility.DisplayPictureUtil;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import declare.com.vvme.R;

public class ImagePagerAdapter extends PagerAdapter {

	private List<String> images;
	private LayoutInflater inflater;
	private Context mContext;
	// =DisplayPictureUtil.getDisplayImageOptions();
	private DisplayImageOptions options;
	private ImageLoader imageLoader = DisplayPictureUtil.getImageLoader();

	public ImagePagerAdapter(List<String> images, Context context) {
		this.images = images;
		this.mContext = context;
		inflater = LayoutInflater.from(context);

		// debug
		options = new DisplayImageOptions.Builder()
//              .showImageForEmptyUri(R.drawable.ic_stub) // image连接地址为空时
//				.showImageOnFail(R.drawable.ic_stub) // image加载失败
				//debug
				.showImageOnFail(R.drawable.img_broken) // image加载失败
                .showImageForEmptyUri(R.drawable.img_broken)
				.cacheInMemory(true) // 加载图片时会在内存中加载缓存
				.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
				.displayer(new RoundedBitmapDisplayer(8)) // 设置用户加载图片task(这里是圆角图片显示)
				.build();
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
//		((ViewPager) container).removeView((View) object);
		
		// debug
	    View imageLayout=(View)object;
		PhotoView pv = (PhotoView) imageLayout.getTag();
		pv.setOnPhotoTapListener(null);
		
		imageLoader.cancelDisplayTask(pv);
		container.removeView(imageLayout);
	}

	@Override
	public void finishUpdate(View container) {
	}

	@Override
	public int getCount() {
		return images == null ? 0 : images.size();
	}

	@Override
	public Object instantiateItem(ViewGroup view, int position) {
		View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);

		final PhotoView pv = (PhotoView) imageLayout.findViewById(R.id.image);
		final ProgressBar progressBar = (ProgressBar) imageLayout.findViewById(R.id.loading);
		final ImageView left_n = (ImageView) imageLayout.findViewById(R.id.left_n);
		final ImageView right_n = (ImageView) imageLayout.findViewById(R.id.right_n);
		imageLoader.displayImage(images.get(position), pv, options, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
				progressBar.setVisibility(View.VISIBLE);
				left_n.setVisibility(View.GONE);
				right_n.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//				String message = null;
//				switch (failReason.getType()) {
//				case IO_ERROR:
//					message = "Input/Output error";
//					break;
//				case DECODING_ERROR:
//					message = "Image can't be decoded";
//					break;
//				case NETWORK_DENIED:
//					message = "Downloads are denied";
//					break;
//				case OUT_OF_MEMORY:
//					message = "Out Of Memory error";
//					break;
//				case UNKNOWN:
//					message = "Unknown error";
//					break;
//				}
//				UIHelper.showToast(message);

				progressBar.setVisibility(View.GONE);

				left_n.setVisibility(View.GONE);
				right_n.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
				progressBar.setVisibility(View.GONE);
				left_n.setVisibility(View.VISIBLE);
				right_n.setVisibility(View.VISIBLE);
				final Map<String, Float> bbs = new HashMap<String, Float>();
				bbs.put("degree", 0f);
				left_n.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (bbs.get("degree") == -360 || bbs.get("degree") == 360) {
							bbs.put("degree", 0f);
						}
						bbs.put("degree", bbs.get("degree") - 90);
						bbq(loadedImage, pv, bbs.get("degree"), bbs.get("degree") + 90);
					}
				});
				right_n.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (bbs.get("degree") == -360 || bbs.get("degree") == 360) {
							bbs.put("degree", 0f);
						}
						bbs.put("degree", bbs.get("degree") + 90);
						bbq(loadedImage, pv, bbs.get("degree"), bbs.get("degree") - 90);

					}
				});
			}

		});

		((ViewPager) view).addView(imageLayout, 0);
		imageLayout.setTag(pv);
		return imageLayout;
	}

	private void bbq(final Bitmap loadedImage, final ImageView iv, final float progress, final float blackProgress) {
		// final Matrix matrix = new Matrix();
		// matrix.setRotate(progress); // 设置翻转的角度
		// 重新绘制翻转后的图片
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
		RotateAnimation rotateAnimation = null;

		if (blackProgress < progress) {
			rotateAnimation = new RotateAnimation(-90, 0, 0, dm.widthPixels / 2, 0, dm.heightPixels / 2);
			rotateAnimation.setDuration(200);
			iv.startAnimation(rotateAnimation);
		} else {
			rotateAnimation = new RotateAnimation(90, 0, 0, dm.widthPixels / 2, 0, dm.heightPixels / 2);
			rotateAnimation.setDuration(200);
			iv.startAnimation(rotateAnimation);
		}

		//debug
		 final Matrix matrix = new Matrix();
		 matrix.setRotate(progress); // 设置翻转的角度
		 Bitmap sloadedImage = Bitmap.createBitmap(loadedImage, 0, 0,
		 loadedImage.getWidth(), loadedImage.getHeight(), matrix, true);
		 iv.setImageBitmap(sloadedImage);
//		iv.setRotation(progress);
//		iv.invalidate();

	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View container) {
	}
}
