package oso.widget.photo;

import utility.DisplayPictureUtil;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * 可加载-网络图片
 * @author 朱成
 * @date 2014-8-16
 */
public class RecycleImageView extends ImageView {
	
	private String uri;

	public RecycleImageView(Context context) {
		super(context);
		init();
	}

	public RecycleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
		setScaleType(ScaleType.CENTER_CROP);
	}
	
	//---------------------------------------------

	public void setImageUri(String uri) {
		DisplayPictureUtil.getImageLoader().displayImage(uri, this,
				DisplayPictureUtil.getDisplayImageOptions());
	}
	public String getImageUri(){
		return uri;
	}
}
