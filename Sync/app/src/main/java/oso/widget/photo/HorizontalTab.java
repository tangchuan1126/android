package oso.widget.photo;

import java.util.ArrayList;
import java.util.List;

import oso.base.PhotoCheckable;
import utility.Utility;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import declare.com.vvme.R;

/**
 * TabToPhoto上方的"tab列表"
 */
public class HorizontalTab extends LinearLayout {

	public HorizontalTab(Context context) {
		super(context);
		init();
	}

	public HorizontalTab(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		// setPadding(5, 0, 0, 0);
	}

	public void initTab(Context c, List<TabParam> tabParamList) {
		//debug
		removeAllViews();
		
		mContext = c;
		mTabParamList = tabParamList;
		setOrientation(VERTICAL);

		HorizontalScrollView hsv = new HorizontalScrollView(mContext);
		hsv.setVerticalScrollBarEnabled(false); // 禁用垂直滚动
		hsv.setHorizontalScrollBarEnabled(false); // 禁用水平滚动

		// 单选按钮组
		group = new RadioGroup(c);
		group.setOrientation(RadioGroup.HORIZONTAL);
		group.setGravity(Gravity.BOTTOM);
		hsv.addView(group, -1, -1);
		addView(hsv);

		// 添加单选按钮
		rbList = new ArrayList<RadioButton>();
		for (int i = 0; i < tabParamList.size(); i++) {
			RadioButton rb = new RadioButton(c);
			rb.setId(i);
			rb.setButtonDrawable(android.R.color.transparent);
			rb.setBackgroundResource(R.drawable.sel_tab_addphoto);
			// 设置tab上文字颜色
			setRadioButtonText(rb, i, 0);
			rb.setTextColor(getResources().getColor(R.color.sync_widget_tabtophoto_bg));
			rb.setTextSize(10);
			RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(-2, -1);
			lp.rightMargin = Utility.pxTodip(mContext, 5);
			group.addView(rb, lp);
			int paddingLR = Utility.pxTodip(mContext, 20);
			int paddingTB = Utility.pxTodip(mContext, 5);
			rb.setPadding(paddingLR, paddingTB, paddingLR, paddingTB);
			rbList.add(rb);
		}
		rbList.get(0).setChecked(true);
		rbList.get(0).setTextColor(getResources().getColor(R.color.sync_blue));
	}

	public void setRadioButtonText(RadioButton rb, int currentIndex, int photoCount) {
		PhotoCheckable pck = (PhotoCheckable) mTabParamList.get(currentIndex).getAble();
		if (pck.getPhotoCount() > 0) {
			String str0 = mTabParamList.get(currentIndex).getTabName().toString();
			String str1 = "  (" + photoCount + "/" + pck.getPhotoCount() + ")";
			Spannable sp = Utility.getCompoundText(str0, str1, new ForegroundColorSpan(Color.RED));
			Utility.addSpan(sp, str0, str1, new RelativeSizeSpan(0.7f));
			rb.setText(sp);
		} else {
			rb.setText(mTabParamList.get(currentIndex).getTabName());
		}
	}

	public void setCurrentIndex(int index) {
		RadioButton rb = rbList.get(index);
		rb.setChecked(true);
	}
	
	/**
	 * 高亮-tab
	 * @param checkedId
	 */
	public void hightlightTab(int checkedId){
		//高亮-当前tab
		for (int i = 0; i < getRBList().size(); i++) {
			RadioButton rb = getRBList().get(i);
			if (rb.getId() == checkedId) {
				rb.setTextColor(getResources().getColor(R.color.sync_blue));
			} else {
				rb.setTextColor(getResources().getColor(
						R.color.sync_widget_tabtophoto_bg));
			}
		}
	}

	public int getCurrentIndex() {
		return group.getCheckedRadioButtonId();
	}

	public int getTabCount() {
		return group.getChildCount();
	}

	public RadioGroup getGroup() {
		return group;
	}

	public List<RadioButton> getRBList() {
		return rbList;
	}

	private Context mContext;

	private RadioGroup group;
	private List<RadioButton> rbList;
	private List<TabParam> mTabParamList;

}