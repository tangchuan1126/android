package oso.widget.photo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.HttpUrlPath;
import utility.Utility;
import android.text.TextUtils;

/**tabtophoto中"本地、或网络图片"
 * @author 朱成
 * @date 2014-12-11
 */
public class TTPImgBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1918658375327148661L;
	
	public String uri;					//网络图片"相对地址",或本地图片"文件名"
	public String file_id;				//网络图片id(用于删除)
	
	//=====手工添加=====================
	
	public TTPImgBean(String fnameOrUri){
		uri=fnameOrUri;
	}
	
	public boolean isEqual(TTPImgBean b){
		if(uri==null)
			return false;
		return uri.equals(b.uri);
	}
	
	public boolean isServerImg(){
		return !TextUtils.isEmpty(file_id);
	}
	
	public String getWebFullUrl(){
		return HttpUrlPath.basePath+uri;
	}
	
	/**
	 * @Description:用于盘点任务解析返回的大写的JSON格式
	 */
	public static List<TTPImgBean> getCCT_PhotoList(JSONArray ja){
		List<TTPImgBean> listPhotos = new ArrayList<TTPImgBean>();
		for (int i = 0; i < ja.length(); i++) {
			JSONObject jobj = ja.optJSONObject(i);
			TTPImgBean b = new TTPImgBean(jobj.optString("URI"));
			b.file_id = jobj.optString("FILE_ID");
			listPhotos.add(b);
		}
		return listPhotos;
	}
	
	/**
	 * 防止-重复添加
	 * @param list
	 * @param listToAdd
	 */
	public static void addList_exceptSame(List<TTPImgBean> list,List<TTPImgBean> listToAdd){
		if(list==null||Utility.isEmpty(listToAdd))
			return;
		
		int len=listToAdd.size();
		for (int i = 0; i < len; i++) {
			TTPImgBean tmp=listToAdd.get(i);
			if(!contain(list, tmp))
				list.add(tmp);
		}
	}
	
	public static boolean contain(List<TTPImgBean> list,TTPImgBean b){
		if(Utility.isEmpty(list))
			return false;
		
		int len=list.size();
		for (int i = 0; i < len; i++) {
			if(list.get(i).isEqual(b))
				return true;
		}
		return false;
	}
	
	
	
	
	
	
	
}
