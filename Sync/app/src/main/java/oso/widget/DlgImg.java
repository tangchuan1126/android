package oso.widget;

import oso.widget.photo.photoview.PhotoView;
import oso.widget.photo.photoview.PhotoViewAttacher.OnPhotoTapListener;
import utility.DisplayPictureUtil;
import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;

/**
 * 显示图片
 * 
 * @author 朱成
 * @date 2015-1-23
 */
public class DlgImg extends Dialog {

	/**
	 * 设置图片url
	 * 
	 * @param url
	 * @return
	 */
	public DlgImg setUrl(String url) {

		//显示图片
		ImageLoader.getInstance().displayImage(url, img,
				DisplayPictureUtil.getDisplayImageOptions());

		return this;
	}

	public static DlgImg show(Context ct, String url) {
		DlgImg dlg = new DlgImg(ct).setUrl(url);
		dlg.show();
		return dlg;
	}

	// ==================inner===============================

	private PhotoView img;

	public DlgImg(Context context) {
		super(context, R.style.dlgEditView);
		setCanceledOnTouchOutside(true);
		setCancelable(true);

		img = new PhotoView(context);
		setContentView(img);

		// 监听事件
		img.setOnPhotoTapListener(new OnPhotoTapListener() {
			
			@Override
			public void onPhotoTap(View view, float x, float y) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});

	}

}
