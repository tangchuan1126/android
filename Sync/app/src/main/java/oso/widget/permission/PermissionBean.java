package oso.widget.permission;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import support.dbhelper.Goable;
import utility.FileUtils;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import declare.com.vvme.R;

public class PermissionBean implements Serializable {

	private static final long serialVersionUID = -6934095948900818644L;
	
//	public static List<PermissionBean> permissions;

	public PermissionBean() {
	}

	public PermissionBean(int id, int parentid, String title) {
		super();
		this.id = id;
		this.parentid = parentid;
	}

	public PermissionBean(int id) {
		super();
		this.id = id;
		this.parentid = 0;
	}

	public static List<PermissionBean> readPermissions() {
		StringBuilder sb = FileUtils.readFile(path, "UTF-8");
		if (sb != null) {
			return new Gson().fromJson(sb.toString(), new TypeToken<List<PermissionBean>>() {
			}.getType());
		}
		return new ArrayList<PermissionBean>();
	}

	public static void savePermissions(List<PermissionBean> ps) {
		Gson gson = new Gson();
		String jsonStr = gson.toJson(ps);
		FileUtils.writeFile(path, jsonStr);
	}

	public int parentid = 0;
	public int id = 0;
	public String title = "";

	public static final String path = Goable.file_base_path + "uses_permission.key";

	public static List<PermissionBean> getDeBugPermission(Context c) {
		List<PermissionBean> permissions = new ArrayList<PermissionBean>();
		// TODO 主模块ID
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_yard_control)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_load_receive)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_routing)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_monitor)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_processing)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_palletizing)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_items)));
		permissions.add(new PermissionBean(getId(c, R.integer.key_main_inventory)));
		// TODO Yard Control的二级模块ID
		permissions.add(new PermissionBean(getId(c, R.integer.key_gate_check_in), getId(c, R.integer.key_main_yard_control), "Check In"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_gate_pre_check_in), getId(c, R.integer.key_main_yard_control), "Pre Check In"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_gate_check_out), getId(c, R.integer.key_main_yard_control), "Check Out"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_gate_photo), getId(c, R.integer.key_main_yard_control), "Photo"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_rt_spot), getId(c, R.integer.key_main_yard_control), "RT Spot"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_rt_door), getId(c, R.integer.key_main_yard_control), "RT Door"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_shuttle), getId(c, R.integer.key_main_yard_control), "Shuttle"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_ctnr), getId(c, R.integer.key_main_yard_control), "CTNR?"));
		// TODO Load/Receive的二级模块ID
		permissions.add(new PermissionBean(getId(c, R.integer.key_window_check_in), getId(c, R.integer.key_main_load_receive), "Check In"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_warehouse_assign_task), getId(c, R.integer.key_main_load_receive), "Assign Task"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_warehouse_load_receive), getId(c, R.integer.key_main_load_receive),
				"Load / Receive"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_print), getId(c, R.integer.key_main_load_receive), "Print"));
		permissions.add(new PermissionBean(getId(c, R.integer.key_L_R_Photo), getId(c, R.integer.key_main_load_receive), "L/R Photo"));
		return permissions;
	}

	private static int getId(Context c, int resId) {
		return c.getResources().getInteger(resId);
	}
}