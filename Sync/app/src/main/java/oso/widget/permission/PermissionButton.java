package oso.widget.permission;

import java.util.List;

import support.AppConfig;
import support.common.UIHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 添加xmlns:oso xmlns:oso="http://schemas.android.com/apk/declare.com.vvme"
 * 
 * 按钮权限开关 oso:permission_enabled="false"
 * 
 * 按钮id oso:permission_id="@integer/key_main_monitor"
 * 
 * 按钮parentId oso:permission_parentId="0"
 * 
 * @author jiang
 */
public class PermissionButton extends TextView implements OnClickListener {

	private PermissionBean currentPermission;

	private OnClickListener onClick;

	private List<PermissionBean> permissions;

	private boolean isPermissionEnabled = true;
	
	private Context mContext;

	public PermissionButton(Context context, PermissionBean permissionBean) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
		currentPermission = permissionBean;
		permissions = PermissionBean.readPermissions();
		super.setOnClickListener(this);
	}

	public PermissionButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context, attrs);
	}

	public PermissionButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		mContext = context;
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PermissionKey);// 得到属性数组
		int id = a.getInt(R.styleable.PermissionKey_permission_id, 0);
		int parentId = a.getInt(R.styleable.PermissionKey_permission_parentId, 0);
		isPermissionEnabled = a.getBoolean(R.styleable.PermissionKey_permission_enabled, true);
		a.recycle();
		permissions = PermissionBean.readPermissions();
		currentPermission = new PermissionBean(id, parentId, "");
		super.setOnClickListener(this);
	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		// TODO Auto-generated method stub
		onClick = l;
	}

	@Override
	public void onClick(View v) {
		if (isPermissionEnabled && AppConfig.isDebug_Permission) {
			if (currentPermission != null && currentPermission.id != 0) {
				if (!isHasPermission()) {
					UIHelper.showToast(mContext.getString(R.string.bcs_key_operationnotpermit));
					return;
				}
			}
		}
		if (onClick != null)
			onClick.onClick(v);
	}

	private boolean isHasPermission() {
		if (currentPermission != null) {
			for (PermissionBean bean : permissions) {
				if (bean.id == currentPermission.id) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isMain() {
		return currentPermission.parentid == 0;
	}
}
