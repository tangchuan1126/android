package oso.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * 用于监听-键盘弹出
 * 
 * @author 朱成
 * @date 2015-1-26
 */
public class ResizeLayout extends LinearLayout {

	private OnSoftKbListener onSoftKeyboardListener;

	public final void setOnSoftKbListener(
			final OnSoftKbListener listener) {
		this.onSoftKeyboardListener = listener;
	}

	// ==================inner===============================

	public ResizeLayout(Context context) {
		super(context);
	}

	public ResizeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	private int oldH;
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// TODO Auto-generated method stub
		super.onLayout(changed, l, t, r, b);
		
		int newH=b-t;
		//oldH有值时,才开始比较
		if(oldH==0)
		{
			oldH=newH;
			return;
		}
		
		//改变时
		if(changed&&onSoftKeyboardListener!=null){
			onSoftKeyboardListener.onShowSoftKb(newH<oldH);
		}
		oldH=newH;
	}

	// ==================nested===============================

	public interface OnSoftKbListener {
		public void onShowSoftKb(boolean isShow);
	}

}
