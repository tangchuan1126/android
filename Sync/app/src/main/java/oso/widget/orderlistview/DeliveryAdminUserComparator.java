package oso.widget.orderlistview;

import java.util.Comparator;

import support.common.bean.DeliveryAdminUser;

public class DeliveryAdminUserComparator implements Comparator<DeliveryAdminUser>{

	@Override
	public int compare(DeliveryAdminUser lhs, DeliveryAdminUser rhs) {
		String str1 = lhs.getEmploye_name().toUpperCase();
		String str2 = rhs.getEmploye_name().toUpperCase();
		return str1.compareTo(str2);
	}

}
