package oso.widget.orderlistview;

import java.util.List;

import support.common.bean.DeliveryAdminUser;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SearchAdapter extends BaseAdapter {

	private List<DeliveryAdminUser> datas;

	private Context mContext;

	public SearchAdapter(Context c, List<DeliveryAdminUser> data) {
		datas = data;
		mContext = c;
	}

	@Override
	public int getCount() {
		return datas == null ? 0 : datas.size();
	}

	@Override
	public Object getItem(int position) {
		return datas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView tv = (TextView) View.inflate(mContext, android.R.layout.simple_list_item_1, null);
		tv.setTextColor(Color.BLACK);
		tv.setTextSize(12);
		tv.setText(datas.get(position).getEmploye_name());
		return tv;
	}
}
