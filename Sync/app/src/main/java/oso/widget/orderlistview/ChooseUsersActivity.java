package oso.widget.orderlistview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;

import oso.base.BaseActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.orderlistview.OrderListView.OnItemOrderClickListener;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.bean.DeliveryAdminUser;
import support.common.datas.HoldDoubleValue;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.key.BaseDataSetUpStaffJobKey;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.FileUtils;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @author jiang
 */
public class ChooseUsersActivity extends BaseActivity {

	public String select_key = "default_user";
	public static final String KEY_DOCK_CHECK_IN = "DockCheckIn";
	public static final String KEY_SHUTTLE = "Shuttle";
	public static final String KEY_WINDOW_CHECK_IN = "WindowCheckIn";
	public static final String KEY_REC_SUPERVISOR = "RecSuperVisor";
	public static final String KEY_REC_SCANLABOR = "RecScanLabor";
	public static final String KEY_SPECIALTASK_ASSIGN = "SpecialTaskAssign";

	/*
	 * 修改日期:2015年 1月30日 修改原因：本部分内容已经替换到 指定的Key中 BaseDataDepartmentKey.class
	 * 修改人：GCY
	 */
	// public static final String Manager = "10000";
	// public static final String Checkin = "1000001";
	// public static final String Warehouse = "1000002";

	private SingleSelectBar singleSelectBar;
	private TextView usersTv;
	private LinearLayout nameLayout;
	private Button submitBtn;
	private ViewPager mViewPager;
	private View commomLayout, leaderLayout, onlineLayout;

	private EditText searchEt;
	private LinearLayout searchLayout;
	private ListView searchLv;
	private boolean isShowSearch;
	private SearchAdapter searchAdapter;

	private ArrayList<View> mViewList = new ArrayList<View>();

	private OrderListView selectLv;
	private ListView commonLv, onlineLv;
	// private ExpandableListView groupExLv;

	private CommonAdapter commonAdapter;
	private SelectAdapter onlineAdapter;
	// private GroupAdapter groupAdapter;

	private List<DeliveryAdminUser> dataAll = new ArrayList<DeliveryAdminUser>();
	private List<DeliveryAdminUser> commonData = new ArrayList<DeliveryAdminUser>();
	// private List<List<DeliveryAdminUser>> groupData = new
	// ArrayList<List<DeliveryAdminUser>>();
	private List<DeliveryAdminUser> onlineData = new ArrayList<DeliveryAdminUser>();

	private List<DeliveryAdminUser> searchDatas = new ArrayList<DeliveryAdminUser>();

	private int dataPosition = 0;

	private int resoures_text_id; // text控件id
	private int resoures_hide_id; // hide控件id
	private String selected_adids = ""; // 选中的adids, 格式="1000143,1000078"
	private int role_ids = 0;// 按照角色筛选
	private int proJsId = BaseDataSetUpStaffJobKey.All; // 具体的筛选条件
	private boolean isSingleSelect;// 是否单选

	public static final int requestCode = 0xfff; // jiang

	public boolean flag = true; // 控制搜索框的点击

	String titleStr = "Users";
	String selectBarStr = "All Users";

	// int selectOnline = 0;
	// int selectTasks = 0;

	/**
	 * Done按钮默认自动根据规则进行显示/隐藏，设置为false 则一直显示
	 */
	private static boolean isBtnAutoShow = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_choose_users, 0);
		initView();
		initData();
		setData();
	}

	private void initData() {
		resoures_text_id = getIntent().getIntExtra("resoures_text_id", 0);
		resoures_hide_id = getIntent().getIntExtra("resoures_hide_id", 0);
		selected_adids = getIntent().getStringExtra("selected_adids");
		role_ids = getIntent().getIntExtra("role_ids", 0);
		isSingleSelect = getIntent().getBooleanExtra("single_select", false);
		select_key = getIntent().getStringExtra("select_key");
		proJsId = getIntent().getIntExtra("proJsId", BaseDataSetUpStaffJobKey.All);

		if (proJsId == BaseDataSetUpStaffJobKey.LeadSuperVisor) {
			titleStr = getString(R.string.sync_supervisor_select);
			selectBarStr = getString(R.string.sync_supervisor);
		}
		if (proJsId == BaseDataSetUpStaffJobKey.Employee) {
			titleStr = getString(R.string.sync_labor_select);
			selectBarStr = getString(R.string.sync_labor);
		}

		setTitleString(titleStr);
		searchEt.setHint(selectBarStr);
	}

	private void initView() {
		singleSelectBar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		usersTv = (TextView) findViewById(R.id.usersTv);
		nameLayout = (LinearLayout) findViewById(R.id.nameLayout);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		searchEt = (EditText) findViewById(R.id.searchEt);
		//searchEt.setTransformationMethod(AllCapTransformationMethod.getThis());
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);

		String buttonName = getIntent().getStringExtra("buttonName");
		if (!TextUtils.isEmpty(buttonName))
			submitBtn.setText(buttonName);
	}

	private void setData() {
		LayoutInflater inflater = LayoutInflater.from(mActivity);
		commomLayout = inflater.inflate(R.layout.act_select_leader_page0, null);
		leaderLayout = inflater.inflate(R.layout.act_select_leader_page1, null);
		// groupLayout = inflater.inflate(R.layout.act_select_leader_page2,
		// null);
		onlineLayout = inflater.inflate(R.layout.act_select_leader_page3, null);
		mViewList.add(onlineLayout);
		mViewList.add(commomLayout);
		mViewList.add(leaderLayout);
		// mViewList.add(groupLayout);

		commonLv = (ListView) commomLayout.findViewById(R.id.commonLv);
		selectLv = (OrderListView) leaderLayout.findViewById(R.id.selectLv);
		selectLv.setSingleSelect(isSingleSelect);
		// groupExLv = (ExpandableListView)
		// groupLayout.findViewById(R.id.groupExLv);
		onlineLv = (ListView) onlineLayout.findViewById(R.id.onlineLv);

		usersTv.setText("");

		submitBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent data = new Intent();
				data.putExtra("resoures_text_id", resoures_text_id);
				data.putExtra("resoures_hide_id", resoures_hide_id);
				data.putExtra("select_user_names", selectLv.getSelectedNamesStr());
				data.putExtra("select_user_adids", selectLv.getSelectedAdidsStr());
				if (onSubmitClickListener != null) {
					onSubmitClickListener.onClick(v, data, ChooseUsersActivity.this);
				} else {
					setResult(requestCode, data);
					mActivity.finish();
				}
			}
		});

		showRightButton(R.drawable.btn_ref_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				getHttpData();
				// showSelectDialog();
			}
		});
		setViewPagerData();
		getHttpData();
	}

//	private boolean searchOnTouch(View v, MotionEvent event) {
//		switch (event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			if (v.getId() == R.id.searchEt) {
//				if (flag) {
//					listAnimation(true, searchLayout);
//					flag = false;
//				}
//			}
//			break;
//		}
//		return false;
//	}

	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();

			searchDatas.clear();
			for (DeliveryAdminUser user : dataAll) {
				if (user.getEmploye_name().toLowerCase().startsWith(searchStr.toLowerCase())) {
					searchDatas.add(user);
				}
			}

			searchAdapter.notifyDataSetChanged();
		} else {
			hideSearch();
		}
	}

	/**
	 * @Description: 从网上拉取数据
	 */
	public void getHttpData() {
		RequestParams params = new RequestParams();
		params.put("Method", HttpPostMethod.CountUncompletedTask);
		params.put("ps_id", StoredData.getPs_id());
		params.put("adgid", role_ids + "");
		params.put("proJsId", proJsId + "");
		StringUtil.setLoginInfoParams(params);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				System.out.println("SysUser= " + json.toString());
				// 获取数据
				List<DeliveryAdminUser> datas = DeliveryAdminUser.handJSON(json);
				if (!datas.isEmpty()) {
					Collections.sort(datas, new DeliveryAdminUserComparator());
					// for (DeliveryAdminUser user : datas) {
					// all.add(user.copyUser());
					// }
					dataAll.clear();
					dataAll.addAll(datas);
					// groupData = DeliveryAdminUser.getGroupData(datas);
					onlineData = getOnlineData(datas);
					// 刷新列表清空选中状态
					DeliveryAdminUser.initState(commonData);
					DeliveryAdminUser.initState(dataAll);
					// DeliveryAdminUser.initStates(groupData);
					DeliveryAdminUser.initState(onlineData);
					// 数据绑定adapter
					setCommonData();
					setLeaderData();
					// setGroupData();
					setOnlineData();
					// 同步，刷新数据
					syncCheckData();
					refreshLayout();
					// 初始化搜索的数据
					setSearchData();
				}
			}
		}.doGet(HttpUrlPath.SysUserAction, params, mActivity);
	}

	private void setSearchData() {
		searchAdapter = new SearchAdapter(mActivity, searchDatas);
		searchLv.setAdapter(searchAdapter);
//		searchEt.setOnTouchListener(new View.OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				return searchOnTouch(v, event);
//			}
//		});
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		searchLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (isSingleSelect) {
					DeliveryAdminUser.initState(commonData);
					DeliveryAdminUser.initState(dataAll);
					// DeliveryAdminUser.initStates(groupData);
					DeliveryAdminUser.initState(onlineData);
				}
				DeliveryAdminUser user = searchDatas.get(position);
				user.isSelect = !user.isSelect;// 选中的选项反选
				user.setFile_path(dataAll.get(position).getFile_path());
				addCommon(user);// 添加历史列表
				setCheckState(commonData, user);// 当前选中的选项状态赋值给数组1
				// setCheckStates(groupData, user);// 当前选中的选项状态赋值给数组3
				setCheckState(onlineData, user); // 当前选中的选项状态赋值给数组4
				refreshLayout();// 刷新视图
				
				hideSearch();
				searchEt.setText("");
			}
		});
	}

	private List<DeliveryAdminUser> getOnlineData(List<DeliveryAdminUser> dataAll) {
		List<DeliveryAdminUser> datas = new ArrayList<DeliveryAdminUser>();
		for (DeliveryAdminUser user : dataAll) {
			if (user.useronline == 1) {
				datas.add(user);
			}
		}
		return datas;
	}

	/**
	 * 之前选中的数据
	 */
	private void syncCheckData() {
		if (TextUtils.isEmpty(selected_adids)) {
			return;
		}
		String[] ids = selected_adids.split(",");
		// 初始化数组1的选中状态
		initCheckState(commonData, ids);
		// 初始化数组2的选中状态
		initCheckState(dataAll, ids);
		// 初始化数组3的选中状态
		// initCheckStates(groupData, ids);
		// 初始化数组4的选中状态
		initCheckState(onlineData, ids);
		refreshLayout();
		selectLv.setSelection(dataPosition);
		selectLv.setAdapter(dataAll);
	}

	private void refreshLayout() {
		// 刷新显示
		selectLv.setAdapter(dataAll);
		commonAdapter.notifyDataSetChanged();
		// groupAdapter.notifyDataSetChanged();
		onlineAdapter.notifyDataSetChanged();
		usersTv.setText(selectLv.getSelectedCount() == 0 ? "" : selectLv.getSelectedCount() + "");
		addNameLayout(selectLv.getSelectedUsers());
	}

	private void setCommonData() {
		commonData = readUser();
		DeliveryAdminUser.initState(commonData);
		commonLv.setEmptyView(commomLayout.findViewById(R.id.nodataTv));
		commonAdapter = new CommonAdapter(mActivity, commonData, dataAll, isSingleSelect);
		commonLv.setAdapter(commonAdapter);
		commonLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if (isSingleSelect) {
					DeliveryAdminUser.initState(commonData);
					DeliveryAdminUser.initState(dataAll);
					// DeliveryAdminUser.initStates(groupData);
					DeliveryAdminUser.initState(onlineData);
				}
				final DeliveryAdminUser user = commonData.get(position);
				user.isSelect = !user.isSelect; // 选中的选项反选
				setCheckState(dataAll, user); // 当前选中的选项状态赋值给数组2
				// setCheckStates(groupData, user); // 当前选中的选项状态赋值给数组3
				setCheckState(onlineData, user); // 当前选中的选项状态赋值给数组4
				refreshLayout();// 刷新视图
			}
		});

	}

	private void setLeaderData() {
		selectLv.setEmptyView(leaderLayout.findViewById(R.id.nodataTv));
		selectLv.setAdapter(dataAll);
		selectLv.setOnItemClickListener(new OnItemOrderClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id, DeliveryAdminUser user) {
				if (isSingleSelect) {
					DeliveryAdminUser.initState(commonData);
					DeliveryAdminUser.initState(dataAll);
					// DeliveryAdminUser.initStates(groupData);
					DeliveryAdminUser.initState(onlineData);
				}
				user.isSelect = !user.isSelect;// 选中的选项反选
				user.setFile_path(dataAll.get(position).getFile_path());
				addCommon(user);// 添加历史列表
				setCheckState(commonData, user);// 当前选中的选项状态赋值给数组1
				// setCheckStates(groupData, user);// 当前选中的选项状态赋值给数组3
				setCheckState(onlineData, user); // 当前选中的选项状态赋值给数组4
				refreshLayout();// 刷新视图
			}
		});

	}

	private void setOnlineData() {
		onlineLv.setEmptyView(onlineLayout.findViewById(R.id.nodataTv));
		onlineAdapter = new SelectAdapter(mActivity, onlineData, isSingleSelect);
		onlineLv.setAdapter(onlineAdapter);
		onlineLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (isSingleSelect) {
					DeliveryAdminUser.initState(commonData);
					DeliveryAdminUser.initState(dataAll);
					// DeliveryAdminUser.initStates(groupData);
					DeliveryAdminUser.initState(onlineData);
				}
				final DeliveryAdminUser user = onlineData.get(position);
				user.isSelect = !user.isSelect;// 选中的选项反选
				addCommon(user);// 添加历史列表
				setCheckState(commonData, user);// 当前选中的选项状态赋值给数组1
				setCheckState(dataAll, user); // 当前选中的选项状态赋值给数组2
				// setCheckStates(groupData, user);// 当前选中的选项状态赋值给数组3
				refreshLayout();// 刷新视图
			}
		});

	}

	// private void setGroupData() {
	// groupExLv.setEmptyView(groupLayout.findViewById(R.id.nodataTv));
	// groupAdapter = new GroupAdapter(mActivity, groupData);
	// groupExLv.setAdapter(groupAdapter);
	// groupExLv.setOnChildClickListener(new OnChildClickListener() {
	// @Override
	// public boolean onChildClick(ExpandableListView parent, View v, int
	// groupPosition, int childPosition, long id) {
	// if (isSingleSelect) {
	// DeliveryAdminUser.initState(commonData);
	// DeliveryAdminUser.initState(dataAll);
	// DeliveryAdminUser.initStates(groupData);
	// DeliveryAdminUser.initState(onlineData);
	// }
	// final DeliveryAdminUser user =
	// groupData.get(groupPosition).get(childPosition);
	// user.isSelect = !user.isSelect;// 选中的选项反选
	// addCommon(user);// 添加历史列表
	// setCheckState(commonData, user);// 当前选中的选项状态赋值给数组1
	// setCheckState(dataAll, user); // 当前选中的选项状态赋值给数组2
	// setCheckState(onlineData, user); // 当前选中的选项状态赋值给数组4
	// refreshLayout();// 刷新视图
	// return true;
	// }
	// });
	//
	// }

	private void setCheckState(List<DeliveryAdminUser> datas, DeliveryAdminUser user) {
		for (DeliveryAdminUser u : datas) {
			if (u.getAdid().equals(user.getAdid())) {
				u.isSelect = user.isSelect;
				break;
			}
		}
	}

	private void setCheckStates(List<List<DeliveryAdminUser>> list, DeliveryAdminUser user) {
		for (List<DeliveryAdminUser> datas : list) {
			setCheckState(datas, user);
		}
	}

	private void initCheckState(List<DeliveryAdminUser> datas, String[] ids) {
		for (DeliveryAdminUser user : datas) {
			for (int i = 0; i < ids.length; i++) {
				String id = ids[i];
				if (user.getAdid().equals(id)) {
					dataPosition = i;
					user.isSelect = true;
					continue;
				}
			}
		}
	}

	private void initCheckStates(List<List<DeliveryAdminUser>> list, String[] ids) {
		for (List<DeliveryAdminUser> datas : list) {
			initCheckState(datas, ids);
		}
	}

	private void addCommon(DeliveryAdminUser user) {
		if (!isCommon(user)) {
			Collections.reverse(commonData);
			commonData.add(user);
			Collections.reverse(commonData);
			commonAdapter.notifyDataSetChanged();
		}
		saveUser(commonData);
	}

	private boolean isCommon(DeliveryAdminUser user) {
		if (!commonData.isEmpty()) {
			for (int i = 0; i < commonData.size(); i++) {
				if (commonData.get(i).getEmploye_name().equals(user.getEmploye_name())) {
					return true;
				}
			}
		}
		return false;
	}

	private void addNameLayout(List<DeliveryAdminUser> users) {
		nameLayout.removeAllViews();
		for (final DeliveryAdminUser user : users) {
			View v = View.inflate(mActivity, R.layout.text_leader, null);
			((TextView) v.findViewById(R.id.tv)).setText(user.getEmploye_name());
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showDialog_Delete(user);
				}
			});

			nameLayout.addView(v);
		}


		// --- insert code by xialimin
		if (!isBtnAutoShow) {
			submitBtn.setVisibility(View.VISIBLE);
			return;
		}
		// --- end

//		submitBtn.setVisibility(names.isEmpty() || selectLv.getSelectedAdidsStr().equals(selected_adids) ? View.GONE : View.VISIBLE);
		submitBtn.setVisibility(Utility.isEmpty(users) ? View.GONE : View.VISIBLE);
	}

	private void setViewPagerData() {
		mViewPager.setAdapter(new VPAdapter());
		initSingleSelectBar();
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				singleSelectBar.userDefineSelectIndexExcuteClick(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		mViewPager.setCurrentItem(1);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				mViewPager.setCurrentItem(selectValue.b);
			}
		});
	}
	
	private void showDialog_Delete(final DeliveryAdminUser user){
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setMessage(getString(R.string.sync_delete) + "?");
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				user.isSelect = !user.isSelect;// 选中的选项反选
				addCommon(user);// 添加历史列表
				setCheckState(commonData, user);// 当前选中的选项状态赋值给数组1
				setCheckState(dataAll, user); // 当前选中的选项状态赋值给数组2
				refreshLayout();// 刷新视图
			}
		});
		builder.show();
	}

	// private void showSelectDialog() {
	// View v = getLayoutInflater().inflate(R.layout.dialog_select_online,
	// null);
	// SingleSelectBar onlineBar = (SingleSelectBar) v.findViewById(R.id.ssb1);
	// SingleSelectBar tasksBar = (SingleSelectBar) v.findViewById(R.id.ssb2);
	// initOnlineBar(onlineBar);
	// initTasksBar(tasksBar);
	//
	// RewriteBuilderDialog.Builder builder = new
	// RewriteBuilderDialog.Builder(mActivity);
	// builder.setView(v);
	// builder.hideCancelBtn();
	// builder.setPositiveButton("Cancel", null);
	// builder.show();
	// }
	//
	// private void initOnlineBar(SingleSelectBar onlineBar) {
	// List<HoldDoubleValue<String, Integer>> clickItems = new
	// ArrayList<HoldDoubleValue<String, Integer>>();
	// clickItems.add(new HoldDoubleValue<String, Integer>("ALL", 0));
	// clickItems.add(new HoldDoubleValue<String, Integer>("YES", 1));
	// clickItems.add(new HoldDoubleValue<String, Integer>("NO", 2));
	// onlineBar.setUserDefineClickItems(clickItems);
	// onlineBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
	// @Override
	// public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
	// selectOnline = selectValue.b;
	// dataAll.clear();
	// dataAll.addAll(selecting());
	// refreshLayout();
	// }
	// });
	// onlineBar.userDefineSelectIndexExcuteClick(selectOnline);
	// }
	//
	// private void initTasksBar(SingleSelectBar tasksBar) {
	// List<HoldDoubleValue<String, Integer>> clickItems = new
	// ArrayList<HoldDoubleValue<String, Integer>>();
	// clickItems.add(new HoldDoubleValue<String, Integer>("ALL", 0));
	// clickItems.add(new HoldDoubleValue<String, Integer>("YES", 1));
	// clickItems.add(new HoldDoubleValue<String, Integer>("NO", 2));
	// tasksBar.setUserDefineClickItems(clickItems);
	// tasksBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
	// @Override
	// public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
	// selectTasks = selectValue.b;
	// dataAll.clear();
	// dataAll.addAll(selecting());
	// refreshLayout();
	// }
	// });
	// tasksBar.userDefineSelectIndexExcuteClick(selectTasks);
	// }
	//
	// List<DeliveryAdminUser> all = new ArrayList<DeliveryAdminUser>();
	//
	// private List<DeliveryAdminUser> selecting() {
	// List<DeliveryAdminUser> yesDatas = new ArrayList<DeliveryAdminUser>();
	// List<DeliveryAdminUser> onDatas = new ArrayList<DeliveryAdminUser>();
	// List<DeliveryAdminUser> yoDatas = new ArrayList<DeliveryAdminUser>();
	// // 所有
	// if (selectOnline == 0 && selectTasks == 0) {
	// return all;
	// } else
	// // 只选择online
	// if (selectTasks == 0 && selectOnline != 0) {
	// for (DeliveryAdminUser user : all) {
	// if (isOnline(user)) {
	// yesDatas.add(user.copyUser());
	// } else {
	// onDatas.add(user.copyUser());
	// }
	// }
	// return selectOnline == 1 ? yesDatas : onDatas;
	// } else
	// // 只选择task
	// if (selectOnline == 0 && selectTasks != 0) {
	// for (DeliveryAdminUser user : all) {
	// if (isTasks(user)) {
	// yesDatas.add(user.copyUser());
	// } else {
	// onDatas.add(user.copyUser());
	// }
	// }
	// return selectTasks == 1 ? yesDatas : onDatas;
	// } else {
	// // 都不等于All
	// // 在线 && 有任务
	// if (selectOnline == 1 && selectTasks == 1) {
	// for (DeliveryAdminUser user : all) {
	// if (isOnline(user) && isTasks(user)) {
	// yoDatas.add(user.copyUser());
	// }
	// }
	// return yoDatas;
	// }
	// // 在线 && 没任务
	// if (selectOnline == 1 && selectTasks == 2) {
	// for (DeliveryAdminUser user : all) {
	// if (isOnline(user) && !isTasks(user)) {
	// yoDatas.add(user.copyUser());
	// }
	// }
	// return yoDatas;
	// }
	// // 不在线 && 有任务的
	// if (selectOnline == 2 && selectTasks == 1) {
	// for (DeliveryAdminUser user : all) {
	// if (!isOnline(user) && isTasks(user)) {
	// yoDatas.add(user.copyUser());
	// }
	// }
	// return yoDatas;
	// }
	// // 不在线 && 没任务
	// if (selectOnline == 2 && selectTasks == 2) {
	// for (DeliveryAdminUser user : all) {
	// if (!isOnline(user) && !isTasks(user)) {
	// yoDatas.add(user.copyUser());
	// }
	// }
	// return yoDatas;
	// }
	// }
	// return all;
	// }

	private boolean isOnline(DeliveryAdminUser user) {
		return user.useronline == 1;
	}

	private boolean isTasks(DeliveryAdminUser user) {
		return !user.tasks.equals("0");
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.sync_online), 0));
		clickItems.add(new HoldDoubleValue<String, Integer>(getString(R.string.sync_recent), 1));
		clickItems.add(new HoldDoubleValue<String, Integer>(selectBarStr, 2));
		// clickItems.add(new HoldDoubleValue<String, Integer>("Group", 2));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.userDefineSelectIndexExcuteClick(1);
	}

	@Override
	protected void onBackBtnOrKey() {
		super.onBackBtnOrKey();
	}

	private List<DeliveryAdminUser> readUser() {
		StringBuilder sb = FileUtils.readFile(getCachePath(), "UTF-8");
		if (sb != null) {
			return new Gson().fromJson(sb.toString(), new TypeToken<List<DeliveryAdminUser>>() {
			}.getType());
		}
		return new ArrayList<DeliveryAdminUser>();
	}

	private void saveUser(List<DeliveryAdminUser> ls) {
		Gson gson = new Gson();
		String jsonStr = gson.toJson(ls);
		FileUtils.writeFile(getCachePath(), jsonStr);
	}

	private String getCachePath() {
		String cacheName = "choose_users.cache";
		cacheName = "choose_" + select_key + ".cache";
		String path = Goable.file_cache_path;
		if (mActivity.getCacheDir().getAbsolutePath() != null)
			path = mActivity.getCacheDir().getAbsolutePath() + "/";
		System.out.println(path);
		return path + cacheName;
	}

	private class VPAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return mViewList.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {
			((ViewPager) container).addView(mViewList.get(position), 0);
			return mViewList.get(position);
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView(mViewList.get(position));
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	}

	// =========传参===============================

	/**
	 * @param in
	 * @param single_select
	 *            单选
	 * @param role_ids
	 *            部门,取BaseDataDepartmentKey.x
	 * @param proJsId
	 *            角儿,取BaseDataSetUpStaffJobKey.x
	 * @param select_key
	 *            模块,用于存"最近选过的人",取ChooseUsersActivity.KEY_x
	 */
	public static void initParams(Intent in, boolean single_select, int role_ids, int proJsId, String select_key) {
		in.putExtra("single_select", single_select);
		in.putExtra("role_ids", role_ids);
		in.putExtra("proJsId", proJsId);
		in.putExtra("select_key", select_key);
	}

	public static OnSubmitClickListener getOnSubmitClickListener() {
		return onSubmitClickListener;
	}

	public static void setOnSubmitClickListener(OnSubmitClickListener onSubmitClickListener) {
		ChooseUsersActivity.onSubmitClickListener = onSubmitClickListener;
	}

	private static OnSubmitClickListener onSubmitClickListener;

	public interface OnSubmitClickListener {
		void onClick(View v, Intent data, ChooseUsersActivity c);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		onSubmitClickListener = null;
	}

	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
		isShowSearch = true;
		searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		isShowSearch = false;
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}

	/**
	 * 设置Submit按钮是否自动显示
	 * @true  自动显示/隐藏
	 * @false 一直显示
	 * @param isVisible
	 */
	public static void setSubmitBtnAutoshow(boolean isVisible) {
		isBtnAutoShow = isVisible;
	}

//	public void listAnimation(boolean listFlag, View view) {
//		AnimationSet mAnimationSet = new AnimationSet(true);
//		if (listFlag) {
//			TranslateAnimation translate = new TranslateAnimation(0, 0, ActivityUtils.getScreenHeight(mActivity), 0);
//			translate.setFillAfter(true);
//			translate.setDuration(400);
//			mAnimationSet.addAnimation(translate);
//			mAnimationSet.setAnimationListener(new AnimationListener() {
//				@Override
//				public void onAnimationStart(Animation animation) {
//				}
//
//				@Override
//				public void onAnimationRepeat(Animation animation) {
//				}
//
//				@Override
//				public void onAnimationEnd(Animation animation) {
//					showSearch();
//				}
//			});
//
//		} else {
//			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, ActivityUtils.getScreenHeight(mActivity));
//			translate.setFillAfter(true);
//			translate.setDuration(150);
//			mAnimationSet.addAnimation(translate);
//			mAnimationSet.setAnimationListener(new AnimationListener() {
//				@Override
//				public void onAnimationStart(Animation animation) {
//				}
//
//				@Override
//				public void onAnimationRepeat(Animation animation) {
//				}
//
//				@Override
//				public void onAnimationEnd(Animation animation) {
//					hideSearch();
//				}
//			});
//		}
//		view.startAnimation(mAnimationSet);
//	}
}