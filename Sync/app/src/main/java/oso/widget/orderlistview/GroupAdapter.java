package oso.widget.orderlistview;

import java.util.List;

import support.common.bean.DeliveryAdminUser;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class GroupAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private List<List<DeliveryAdminUser>> groupData;

	public GroupAdapter(Context c, List<List<DeliveryAdminUser>> groupData) {
		inflater = LayoutInflater.from(c);
		this.groupData = groupData;
	}

	@Override
	public int getGroupCount() {
		return groupData == null ? 0 : groupData.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return groupData.get(groupPosition) == null ? 0 : groupData.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groupData.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return groupData.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		GroupHolder holder = null;
		if (convertView == null) {
			holder = new GroupHolder();
			convertView = inflater.inflate(R.layout.simple_list_item_group, null);
			holder.group = (TextView) convertView.findViewById(R.id.groupTv);
			convertView.setTag(holder);
		} else {
			holder = (GroupHolder) convertView.getTag();
		}

		final DeliveryAdminUser user = groupData.get(groupPosition).get(0);
		holder.group.setText(user.getAdgid_name());

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ChildHolder holder = null;
		if (convertView == null) {
			holder = new ChildHolder();
			convertView = inflater.inflate(R.layout.simple_list_item_o, null);
			holder.text = (TextView) convertView.findViewById(R.id.text);
			holder.box = (CheckBox) convertView.findViewById(R.id.box);
			holder.userIv = (ImageView) convertView.findViewById(R.id.userIv);
			holder.notifyTv = (TextView) convertView.findViewById(R.id.notifyTv);
			convertView.setTag(holder);
		} else {
			holder = (ChildHolder) convertView.getTag();
		}

		final DeliveryAdminUser user = groupData.get(groupPosition).get(childPosition);
		holder.text.setText(user.getEmploye_name());
		holder.box.setChecked(user.isSelect);
		// 任务数
		holder.notifyTv.setVisibility(user.tasks.equals("0") ? View.GONE : View.VISIBLE);
		holder.notifyTv.setText(user.tasks);
		// 是否在线
		if (user.useronline == 0) {
			holder.userIv.setImageResource(R.drawable.ic_user_offline);
		} else {
			holder.userIv.setImageResource(R.drawable.ic_user_online);
		}

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	class GroupHolder {
		public TextView group;
	}

	class ChildHolder {
		public ImageView userIv;
		public TextView notifyTv;
		public TextView text;
		public CheckBox box;
	}

}
