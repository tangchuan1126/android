package oso.widget.orderlistview;

import java.util.List;

import oso.ui.chat.activity.ChatActivity;
import oso.ui.chat.util.ChatUtil;
import support.common.UIHelper;
import support.common.bean.DeliveryAdminUser;
import utility.DisplayPictureUtil;
import utility.HttpUrlPath;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import declare.com.vvme.R;

public class CommonAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<DeliveryAdminUser> users, allUser;
	private Context mContext;
	private boolean isSingleSelect;

	public CommonAdapter(Context c, List<DeliveryAdminUser> users, List<DeliveryAdminUser> allUser, boolean isSingleSelect) {
		mContext = c;
		inflater = LayoutInflater.from(c);
		this.users = users;
		this.allUser = allUser;
		this.isSingleSelect = isSingleSelect;
	}

	@Override
	public int getCount() {
		return users == null ? 0 : users.size();
	}

	@Override
	public Object getItem(int position) {
		return users;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		OHolder holder = null;
		if (convertView == null) {
			holder = new OHolder();
			convertView = inflater.inflate(R.layout.simple_list_item_o, null);
			holder.text = (TextView) convertView.findViewById(R.id.text);
			holder.box = (CheckBox) convertView.findViewById(R.id.box);
			holder.userIv = (ImageView) convertView.findViewById(R.id.userIv);
			holder.notifyTv = (TextView) convertView.findViewById(R.id.notifyTv);
			convertView.setTag(holder);
		} else {
			holder = (OHolder) convertView.getTag();
		}

		if (!isSingleSelect) {
			holder.box.setButtonDrawable(R.drawable.checkbox_style);
		}

		final DeliveryAdminUser user = users.get(position);
		holder.text.setText(user.getEmploye_name());
		holder.box.setChecked(user.isSelect);
		holder.userIv.setOnClickListener(new OnClickListener() { // Chat
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mContext, ChatActivity.class);
						if (ChatUtil.isChatWithSelf(user.account))
							UIHelper.showToast(mContext, "Can't talk with yourself!");
						else {
							ChatActivity.initParams(intent, user.account, user.getEmploye_name());
							mContext.startActivity(intent);
						}
					}
				});
		// 是否在线,默认不在线
		offline(holder.userIv);

		for (DeliveryAdminUser u : allUser) {
			if (user.adid.equals(u.adid)) {
				// 任务数
				holder.notifyTv.setVisibility(TextUtils.isEmpty(u.tasks) || u.tasks.equals("0") ? View.GONE : View.VISIBLE);
				holder.notifyTv.setText(u.tasks);
				
				holder.text.setText(u.getEmploye_name());
				/*
				 * if (u.useronline == 0) { offline(holder.userIv); } else {
				 * holder.userIv.getDrawable().setColorFilter(null); }
				 */

				// 判断是否有自定义头像
				boolean hasAvatar = hasAvatar(position);
				displayAvatar(holder, hasAvatar, u.file_path, u.useronline);
			}
		}

		return convertView;
	}

	private void offline(ImageView v) {
		v.getDrawable().mutate();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter cf = new ColorMatrixColorFilter(cm);
		v.getDrawable().setColorFilter(cf);
	}

	private void displayAvatar(OHolder holder, boolean hasAvatar, String avatarUrl, int useronline) {
		// 离线
		avatarUrl = HttpUrlPath.basePath + avatarUrl;
		if (0 == useronline) {
			if (hasAvatar) {
				loadImage(holder, ChatUtil.STATUS_OFFLINE, avatarUrl, DisplayPictureUtil.getChatAvatarDisplayImageOptions_Offline());
			} else {
				holder.userIv.setImageResource(R.drawable.account_offline_circle);
			}
		} else { // 在线
			if (hasAvatar) {
				loadImage(holder, ChatUtil.STATUS_ONLINE, avatarUrl, DisplayPictureUtil.getChatAvatarDisplayImageOptions_Online());
			} else {
				holder.userIv.setImageResource(R.drawable.account_circle);
			}
		}
	}

	public void loadImage(final OHolder holder, final String status, String url, DisplayImageOptions options) {
		ImageLoader.getInstance().loadImage(url, options, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				holder.userIv.setImageResource(ChatUtil.STATUS_ONLINE == status ? R.drawable.account_circle : R.drawable.account_offline_circle);
			}

			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				holder.userIv.setImageResource(ChatUtil.STATUS_ONLINE == status ? R.drawable.account_circle : R.drawable.account_offline_circle);
			}

			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap bm) {
				holder.userIv.setImageDrawable(ChatUtil.changeIconStatus(status, bm, true));
			}

			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
			}
		});
	}

	private boolean hasAvatar(int position) {
		String avatarUrl = users.get(position).file_path;
		return !TextUtils.isEmpty(avatarUrl);
	}

	class OHolder {
		public ImageView userIv;
		public TextView notifyTv;
		public TextView text;
		public CheckBox box;
	}

}
