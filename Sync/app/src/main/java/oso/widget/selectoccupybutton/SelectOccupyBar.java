package oso.widget.selectoccupybutton;

import static support.key.OccupyTypeKey.DOOR;

import java.util.List;

import org.json.JSONObject;

import oso.widget.DockHelpUtil;
import oso.widget.DockHelpUtil.ResourceInfo;
import oso.widget.selectoccupybutton.OccupyTypeSwitchButton.OnChangeTypeListener;
import support.network.HttpPostMethod;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 使用方法
 * 
 * 1. 初始化 </br>
 * 
 * private SelectOccupyBar soBar =
 * (SelectOccupyBar)findViewById(R.id.soBar);</br>
 * soBar.init(OccupyTypeKey.DOOR, mEntryId); //资源类型Id,EntryId</br>
 * 
 * 2. 回调</br> soBar.onSelectBarListener(new OnSelectBarListener() {
 * 
 * @Override public void onClick(List<ResourceInfo> resourceList) { Intent
 *           intent = new Intent(mActivity, AddDoorActivity.class);
 *           intent.putExtra("doorlist", (Serializable) resourceList);
 *           startActivityForResult(intent, AddDoorActivity.Code); } });
 * 
 *           3. 常用方法
 * 
 *           initState() //初始化状态，默认为门
 * 
 *           setCurrentType(int type) //设置资源类型
 * 
 *           setCurrentValue(String text) //设置资源值
 * 
 *           getTypeBtn(); //获取控件
 * 
 *           getValueBtn(); //获取控件
 * 
 * @author jiang
 */
public class SelectOccupyBar extends LinearLayout {

	public SelectOccupyBar(Context context, int occupyType, String entry_id) {
		super(context);
		// TODO Auto-generated constructor stub
		init();
		init(context,occupyType, entry_id);
	}

	public SelectOccupyBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}

	public SelectOccupyBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init();
	}

	public void init(Context c,String entryId) {
		init(c,DOOR, entryId);
	}

	private void init() {
		// 设置布局
		mainLayout = inflate(getContext(), R.layout.widget_selectoccupybar, null);
		addView(mainLayout, -1, -1);
		// 获取控件
		switchBtn = (OccupyTypeSwitchButton) mainLayout.findViewById(R.id.switchBtn);
		textTv = (TextView) mainLayout.findViewById(R.id.textTv);
		valueBtn = (LinearLayout) mainLayout.findViewById(R.id.valueBtn);
	}

	/**
	 * @param type
	 *            资源类型Id
	 * @param entry_id
	 */
	public void init(Context c,final int type, final String entry_id) {
		mEntryId = entry_id;
		switchBtn.setCurrentType(c,type);

		// 设置监听
		valueBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addDoor(mEntryId);
			}
		});

		switchBtn.setOnChangeTypeListener(new OnChangeTypeListener() {
			@Override
			public void onChange(int currentType) {
				setCurrentValue("");
			}
		});
	}

	private void addDoor(String entry_id) {
		RequestParams params = new RequestParams();
		params.add("Method", HttpPostMethod.GetDoorAndSpot);
		params.add("request_type", "2"); // 1:可用res 2:所有
		params.add("occupy_type", getCurrentType() + "");
		params.add("entry_id", entry_id);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<ResourceInfo> resourceList = DockHelpUtil.handjson_big(json);
				if (onSelectBarListener != null)
					onSelectBarListener.onClick(resourceList);
			}
		}.doGet(HttpUrlPath.AndroidDockCheckInAction, params, getContext());
	}

	/**
	 * 初始化状态，默认为门
	 */
	public void initState(Context c) {
		setCurrentType(c,DOOR);
		setCurrentValue("");
	}

	public String getCurrentValue() {
		return textTv.getText().toString();
	}

	public void setCurrentValue(String text) {
		textTv.setText(text);
	}

	public int getCurrentType() {
		return switchBtn.getCurrentType();
	}

	public void setCurrentType(Context c,int type) {
		switchBtn.setCurrentType(c,type);
	}

	public OccupyTypeSwitchButton getTypeBtn() {
		return switchBtn;
	}

	public TextView getValueBtn() {
		return textTv;
	}

	public OnSelectBarListener getOnSelectBarListener() {
		return onSelectBarListener;
	}

	public void setOnSelectBarListener(OnSelectBarListener onSelectBarListener) {
		this.onSelectBarListener = onSelectBarListener;
	}

	private String mEntryId = "";

	private OccupyTypeSwitchButton switchBtn;

	private LinearLayout valueBtn;

	private TextView textTv;

	private View mainLayout;

	private OnSelectBarListener onSelectBarListener;

	public interface OnSelectBarListener {
		void onClick(List<ResourceInfo> resourceList);
	}
}
