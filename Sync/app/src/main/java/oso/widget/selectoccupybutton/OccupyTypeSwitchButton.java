package oso.widget.selectoccupybutton;

import static support.key.OccupyTypeKey.DOOR;
import static support.key.OccupyTypeKey.SPOT;
import oso.widget.selectoccupybutton.Rotate3D.InterpolatedTimeListener;
import support.key.OccupyTypeKey;
import utility.Utility;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

public class OccupyTypeSwitchButton extends Button {

	@Deprecated
	public static enum ResType {
		DOOR, SPOT
	};

	public OccupyTypeSwitchButton(Context context, int occupyType) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context,occupyType);
	}

	public OccupyTypeSwitchButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context,DOOR);
	}

	public OccupyTypeSwitchButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context,DOOR);
	}

	private void init(final Context c,int type) {
		currentType = type;
		// 设置样式
		// setBackgroundResource(R.drawable.menu_button_style);
		// setTextColor(getContext().getResources().getColor(R.drawable.font_bg1));
		// setTextSize(getContext().getResources().getDimension(R.dimen.chickin_listview_text_size_three));

		// 设置监听
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startChange(c,isDoor());
			}
		});
	}

	private void showDoor(Context c) {
		Spannable spDoor = Utility.getCompoundText(PRDFIX, OccupyTypeKey.getOccupyTypeKeyName(c,DOOR), new StyleSpan(Typeface.BOLD));
		Utility.addSpan(spDoor, PRDFIX, OccupyTypeKey.getOccupyTypeKeyName(c,DOOR), new ForegroundColorSpan(0xff0066ca));
		setText(spDoor);
		currentType = DOOR;
	}

	private void showSpot(Context c) {
		Spannable spSpot = Utility.getCompoundText(PRDFIX, OccupyTypeKey.getOccupyTypeKeyName(c,SPOT), new StyleSpan(Typeface.BOLD));
		Utility.addSpan(spSpot, PRDFIX, OccupyTypeKey.getOccupyTypeKeyName(c,SPOT), new ForegroundColorSpan(0xff0066ca));
		setText(spSpot);
		currentType = SPOT;
	}

	private void startChange(final Context c,final boolean isDoor) {
		anim = new Rotate3D(getWidth() / 2, getHeight() / 2, true);
		setAnimation(anim);
		anim.setInterpolatedTimeListener(new InterpolatedTimeListener() {
			@Override
			public void interpolatedTime(float interpolatedTime) {
				boolean overHalf = (interpolatedTime > 0.5f);
				if (overHalf) {
					if (isDoor) {
						showSpot(c);
					} else {
						showDoor(c);
					}
				}
			}
		});
		if (onChangeTypeListener != null)
			onChangeTypeListener.onChange(currentType);
	}

	private boolean isDoor() {
		return currentType == OccupyTypeKey.DOOR;
	}

	public int getCurrentType() {
		return currentType;
	}

	public void setCurrentType(Context c,int currentType) {
		this.currentType = currentType;
		if (isDoor()) {
			showDoor(c);
		} else {
			showSpot(c);
		}
	}

	public OnChangeTypeListener getOnChangeTypeListener() {
		return onChangeTypeListener;
	}

	public void setOnChangeTypeListener(OnChangeTypeListener onChangeTypeListener) {
		this.onChangeTypeListener = onChangeTypeListener;
	}

	private Rotate3D anim = null;

	// private final String PRDFIX = "Assign ";
	private final String PRDFIX = "";

	private int currentType = 0;

	private OnChangeTypeListener onChangeTypeListener;

	public interface OnChangeTypeListener {
		void onChange(int currentType);
	}
}
