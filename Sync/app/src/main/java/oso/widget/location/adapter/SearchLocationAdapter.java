package oso.widget.location.adapter;

import java.util.List;

import oso.widget.location.bean.LocationBean;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SearchLocationAdapter extends BaseAdapter
{
	private Context            mContext;
	private List<LocationBean> searches;

	public SearchLocationAdapter(Context mContext, List<LocationBean> searches) {
		super();
		this.mContext = mContext;
		this.searches = searches;
	}

	@Override
	public int getCount() {
		return searches == null ? 0 : searches.size();
	}

	@Override
	public Object getItem(int position) {
		return searches.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView tv = (TextView) View.inflate(mContext, android.R.layout.simple_list_item_1, null);
		tv.setTextColor(Color.BLACK);
		tv.setTextSize(12);
		tv.setText(searches.get(position).location_name);
		return tv;
	}
	
}
