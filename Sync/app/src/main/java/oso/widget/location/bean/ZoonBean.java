package oso.widget.location.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import android.location.Location;

public class ZoonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2921591641760495381L;

	public String area;
	public String titles;
	
	public boolean isExpand = false;
	
	public List<LocationBean> locations;
	
	
	//----------------------------------------------
	
	public static List<LocationBean> getAllLocation(List<ZoonBean> lists){
		
		if(lists == null || lists.size() ==0){
			return null;
		}
		List<LocationBean> locations = new ArrayList<LocationBean>();
		
		for(ZoonBean bean : lists){
			for(LocationBean location : bean.locations){
				locations.add(location);
			}
		}
		
		Collections.sort(locations,new Comparator<LocationBean>() {

			@Override
			public int compare(LocationBean lhs, LocationBean rhs) {
				return lhs.location_name.compareToIgnoreCase(rhs.location_name);
			}
		});
		
		return locations;
	}
}
