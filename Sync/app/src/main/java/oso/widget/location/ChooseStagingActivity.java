package oso.widget.location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import oso.base.BaseActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.location.LocationListView.OnItemOrderClickListener;
import oso.widget.location.adapter.SearchLocationAdapter;
import oso.widget.location.bean.LocationBean;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.HttpUrlPath;
import utility.Utility;

public class ChooseStagingActivity extends BaseActivity implements OnClickListener{
	
	private EditText searchEt;
	private TextView locationTv;
	
	private Button submitBtn;
	private ListView searchLv;
	
	private LinearLayout nameLayout,searchLayout;
	
	private LocationListView   lvStaging;
	
	private List<LocationBean> listStagings;
	private List<LocationBean> listSelects;
	private List<LocationBean> listSearchs;
	
	private SearchLocationAdapter searchAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_choose_staging, 0);
		applyParams();
		
		searchEt = (EditText) findViewById(R.id.searchEt);
		locationTv = (TextView) findViewById(R.id.locationTv);
		nameLayout = (LinearLayout) findViewById(R.id.nameLayout);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
		
		lvStaging = (LocationListView) findViewById(R.id.lvStaging);
		
		submitBtn.setOnClickListener(this);
		
		setData();
		getDefaultData();
	}
	
	private void setData(){
		setTitleString(getString(R.string.window_add_staging));
		
		listSearchs = new ArrayList<LocationBean>();
		searchAdapter = new SearchLocationAdapter(mActivity, listSearchs);
		searchLv.setAdapter(searchAdapter);
		
		listSelects = new ArrayList<LocationBean>();
		
		showRightButton(R.drawable.btn_ref_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				getDefaultData();
			}
		});
		
		lvStaging.setOnItemClickListener(new OnItemOrderClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id, LocationBean letter) {
				Select(letter);
			}
		});
		
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		searchEt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchEt.setCursorVisible(true);// 显示光标
			}
		});
		
		searchLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				hideSearch();
				searchEt.setText("");
				Select(listSearchs.get(position));
				setPosition();
			}
		});
	}
	
	private void Select(LocationBean bean){
		bean.isSelected = !bean.isSelected;
		//单选
		if(isSingleSelect){
			showSingleSelect(bean);
			return;
		}
		showMultipleSelect();
	}
	
	private void showSelect(){
		nameLayout.removeAllViews();
		int count = 0;
		for(final LocationBean bean : listSelects){
			View v = View.inflate(mActivity, R.layout.text_leader, null);
			((TextView) v.findViewById(R.id.tv)).setText(bean.location_name);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showDialog_Delete(bean);
				}
			});

			nameLayout.addView(v);
			
			count++;
		}
		
		locationTv.setText(""+count);
		
		lvStaging.notifyDataSetChanged();
		
		submitBtn.setVisibility(Utility.isEmpty(listSelects) ? View.GONE : View.VISIBLE);
	}
	private void showMultipleSelect(){
		listSelects.clear();
		
		for(LocationBean bean : listStagings){
			if(bean.isSelected){
				listSelects.add(bean);
			}
		}
		showSelect();
	}
	private void showSingleSelect(LocationBean data){
		
		for(LocationBean bean : listStagings){
			if(!bean.equals(data)){
				bean.isSelected = false;
			}
		}
		lvStaging.notifyDataSetChanged();
		
		listSelects.clear();
		if(data.isSelected)listSelects.add(data);
		showSelect();
	}
	
	private void showDialog_Delete(final LocationBean bean){
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setMessage(getString(R.string.sync_delete)+" !");
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				bean.isSelected = !bean.isSelected;
				listSelects.remove(bean);
				showSelect();
			}
		});
		builder.show();
	}
	
	private void getDefaultData(){
		RequestParams params = new RequestParams();
		params.add("ps_id",StoredData.getPs_id());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				
				listStagings = new Gson().fromJson(json.optJSONArray("data").toString(), 
						new TypeToken<List<LocationBean>>(){}.getType());
				
				lvStaging.setAdapter(listStagings,"Staging");
				
				listSelects.clear();
				nameLayout.removeAllViews();
				locationTv.setText("");
				submitBtn.setVisibility(Utility.isEmpty(listSelects) ? View.GONE : View.VISIBLE);
				
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/getStorageByPsId", params, mActivity);
		
	}
	
	private void setPosition(){
		if(isSingleSelect && listSelects.size() == 1){
			int count =-1 ;
			for(LocationBean bean : listStagings){
				count++;
				if(bean.id == listSelects.get(0).id){
					if(count > 5){
						lvStaging.setSelection(count-3);
					}
					return;
				}
			}
		}
	}
	
	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();

			listSearchs.clear();
			for (LocationBean bean : listStagings) {
				if (bean.location_name.toUpperCase().startsWith(searchStr.toUpperCase())) {
					listSearchs.add(bean);
				}
			}

			searchAdapter.notifyDataSetChanged();
		} else {
			hideSearch();
		}
	}
	
	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.submitBtn:
			Intent in = new Intent();
			in.putExtra("SelectStaging",(Serializable)listSelects);
			setResult(RESULT_OK, in);
			finish();
			break;
		}
	}
	
	//----------------------------------------------------
	
		private boolean isSingleSelect;// 是否单选
		private List<LocationBean> oldSelects;
		
		public static void initParams(Intent in, boolean single_select) {
			in.putExtra("single_select", single_select);
		}
		
		public static void initParams(Intent in, boolean single_select,List<LocationBean> lists) {
			in.putExtra("single_select", single_select);
			in.putExtra("oldSelects",(Serializable)lists);
		}
		
		private void applyParams(){
			Intent in = getIntent();
			isSingleSelect = in.getBooleanExtra("single_select", false);
			oldSelects = (List<LocationBean>) in.getSerializableExtra("oldSelects");
		}
	
}
