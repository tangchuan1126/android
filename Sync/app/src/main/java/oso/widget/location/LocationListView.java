package oso.widget.location;

import java.util.List;

import declare.com.vvme.R;

import oso.SyncApplication;
import oso.widget.location.bean.LocationBean;
import oso.widget.orderlistview.SideBar;
import oso.widget.orderlistview.SideBar.OnTouchingLetterChangedListener;
import utility.Utility;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

public class LocationListView extends FrameLayout implements OnScrollListener, OnItemClickListener, OnTouchingLetterChangedListener{
	
	private TextView letterTv; // 显示点击的字母
	private SideBar sideBar; // ListView右侧导航选项卡
	private ListView lv;
	

	private List<LocationBean> locationDatas = null;
	
	private LocationAdapter adapter;
	
	private String strFromWhere;
	private Context mContext;
	
	public LocationListView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
		init(context);
	}

	public LocationListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init(context);
	}

	public LocationListView(Context context) {
		super(context);
		mContext = context;
		init(context);
	}
	
	private void init(Context context) {
		lv = new ListView(context);
		lv.setBackgroundColor(Color.TRANSPARENT);
		lv.setCacheColorHint(Color.TRANSPARENT);
		
		// 右侧导航条初始化
		sideBar = new SideBar(context);
		FrameLayout.LayoutParams sbParams = new FrameLayout.LayoutParams(Utility.pxTodip(getContext(), 30), -1, Gravity.RIGHT);
		// 中间的字母控件初始化
		letterTv = new TextView(context);
		letterTv.setMinWidth(Utility.pxTodip(context, 90));
		letterTv.setMinHeight(Utility.pxTodip(context, 90));
		letterTv.setBackgroundColor(0x40000000);
		letterTv.setTextColor(Color.WHITE);
		letterTv.setTypeface(Typeface.MONOSPACE);
		letterTv.setTextSize(Utility.pxTosp(context, 24));
		letterTv.setVisibility(View.GONE);
		letterTv.setGravity(Gravity.CENTER);
		
		// 添加View
		FrameLayout.LayoutParams ltParams = new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER);
		addView(lv, -1, -1);
		addView(sideBar, sbParams);
		addView(letterTv, ltParams);
		// 设置监听事件
		lv.setOnScrollListener(this);
		lv.setOnItemClickListener(this);
		sideBar.setOnTouchingLetterChangedListener(this);// 右侧导航条监听
		// initDataHandler.sendEmptyMessage(0);
	}

	public void setAdapter(List<LocationBean> dataAll,String str){
		if(dataAll == null || dataAll.size() ==0 ){
			return;
		}
		
		strFromWhere = str;
		
		locationDatas = dataAll;
		initDataHandler.sendEmptyMessage(0);
	}
	
	public void notifyDataSetChanged(){
		if(adapter != null){
			adapter.notifyDataSetChanged();
		}
	}
	
	private Handler initDataHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (adapter == null) {
				adapter = new LocationAdapter();
				lv.setAdapter(adapter);
			}
			adapter.notifyDataSetChanged();
		}

	};
	
	private Handler _handler = new Handler();
	private Runnable letterThread = new Runnable() {
		public void run() {
			letterTv.setVisibility(View.GONE);
		}
	};
	
	@Override
	public void onTouchingLetterChanged(String s) {
		if(locationDatas == null)
			return;
		
		if(s.equalsIgnoreCase(locationDatas.get(0).location_name.substring(0, 1))){
			lv.setSelection(0);
			showLetter(s);
		}
		
		if(alphaIndexer(s) > 0){
			int position = alphaIndexer(s);
			lv.setSelection(position);
			showLetter(s);
		}
	}

	private int alphaIndexer(String s) {
		int position = 0;
		for (int i = 0; i < locationDatas.size(); i++) {
			if (locationDatas.get(i).location_name.substring(0, 1).equalsIgnoreCase(s)) {
				position = i;
				break;
			}
		}
		return position;
	}

	private void showLetter(String s) {
		letterTv.setText(s);
		letterTv.setVisibility(View.VISIBLE);
		_handler.removeCallbacks(letterThread);
		_handler.postDelayed(letterThread, 1000);
	}
	
	
	public int getSelectedItemPosition(){
		return lv.getSelectedItemPosition();
	}
	
	public void setSelection(final int position){
		SyncApplication.getThis().getHandler().post(new Runnable() {
			
			@Override
			public void run() {
				lv.setSelectionFromTop(position, 10);
			}
		});
	}
	
	public void setEmptyView(View emptyView){
		lv.setEmptyView(emptyView);
	}
	
	

	boolean isStateChanged = false;

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:// 空闲状态
			isStateChanged = false;
			break;
		case OnScrollListener.SCROLL_STATE_FLING:// 滚动状态
			isStateChanged = true;
			break;
		case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:// 触摸后滚动
			isStateChanged = true;
			break;
		}
	}
	
	@Override
	public void onScroll(AbsListView view, final int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				if (isStateChanged) {
					//debug
					showLetter(locationDatas.get(firstVisibleItem).location_name.substring(0, 1));
				}
			}
		});
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (getOnItemClickListener() != null)
			getOnItemClickListener().onItemClick(parent, view, position, id, locationDatas.get(position));
	}
	
	public OnItemOrderClickListener getOnItemClickListener() {
		return onItemClickListener;
	}

	public void setOnItemClickListener(OnItemOrderClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	private OnItemOrderClickListener onItemClickListener;

	public interface OnItemOrderClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position, long id, LocationBean letter);
	}

	
	
	
	private class LocationAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return locationDatas == null ? 0 : locationDatas.size();
		}

		@Override
		public Object getItem(int position) {
			return locationDatas.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder;
			if(convertView == null){
				convertView = View.inflate(getContext(), R.layout.item_cc_location, null);
				holder = new ViewHolder();
				
				holder.box = (CompoundButton) convertView.findViewById(R.id.box);
				holder.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
				
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			
			LocationBean data = locationDatas.get(position);
			
			holder.tvLocation.setText(strFromWhere+": "+data.location_name);
			holder.box.setChecked(data.isSelected);
			return convertView;
		}
		
		class ViewHolder{
			TextView tvLocation;
			CompoundButton box;
		}
		
	}
}
