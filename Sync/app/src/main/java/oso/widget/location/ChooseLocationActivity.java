package oso.widget.location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import oso.SyncApplication;
import oso.base.BaseActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.location.LocationListView.OnItemOrderClickListener;
import oso.widget.location.adapter.SearchLocationAdapter;
import oso.widget.location.bean.LocationBean;
import oso.widget.location.bean.ZoonBean;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.datas.HoldDoubleValue;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.HttpUrlPath;
import utility.Utility;

public class ChooseLocationActivity extends BaseActivity implements OnClickListener {

	
	private EditText searchEt;
	private TextView locationTv;
	private LinearLayout nameLayout,searchLayout;
	private SingleSelectBar single_select_bar;
	private ViewPager pager;
	private Button submitBtn;
	private ListView searchLv;
	
	private LocationListView   lvLocation;
	private ExpandableListView lvZoon;
	
	private ArrayList<View> mViewList = new ArrayList<View>();
	
	private List<ZoonBean> listZoons;
	private List<LocationBean> listLocations;
	private List<LocationBean> listSelects;
	private List<LocationBean> listSearchs;
	
	private ZoonAdapter zoonAdapter;
	private SearchLocationAdapter searchAdapter;
	
	private final static int Mode_Zone = 0;
	private final static int Mode_Location = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_choose_location,0);
		applyParams();
		
		searchEt = (EditText) findViewById(R.id.searchEt);
		locationTv = (TextView) findViewById(R.id.locationTv);
		nameLayout = (LinearLayout) findViewById(R.id.nameLayout);
		single_select_bar = (SingleSelectBar) findViewById(R.id.single_select_bar);
		pager = (ViewPager) findViewById(R.id.pager);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
		searchLv = (ListView) findViewById(R.id.searchLv);
		
		submitBtn.setOnClickListener(this);
		
		setData();
		getDefaultData();
	}
	
	private void setData(){
		
		setTitleString(getString(R.string.patrol_location_name));
		
		View zoneLayout = View.inflate(mActivity, R.layout.act_select_zone_page, null);
		View locationLayout = View.inflate(mActivity, R.layout.act_select_location_page, null);
		
		mViewList.add(zoneLayout);
		mViewList.add(locationLayout);
		
		lvZoon     = (ExpandableListView) zoneLayout.findViewById(R.id.lvZoon);
		lvZoon.setGroupIndicator(null);
		zoonAdapter = new ZoonAdapter();
		lvZoon.setAdapter(zoonAdapter);
		lvLocation = (LocationListView) locationLayout.findViewById(R.id.lvLocation);
		
		
		//-----------
		listSearchs = new ArrayList<LocationBean>();
		searchAdapter = new SearchLocationAdapter(mActivity,listSearchs);
		searchLv.setAdapter(searchAdapter);
		
		listSelects = new ArrayList<LocationBean>();
		
		initSingleSelectBar();
		pager.setAdapter(new LocationAdapter());
		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
			@Override
			public void onPageSelected(int position) {
				single_select_bar.userDefineSelectIndexExcuteClick(position);
				
				zoonAdapter.notifyDataSetChanged();
				lvLocation.notifyDataSetChanged();
				
				setPosition();
			}
		});
		
		single_select_bar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				pager.setCurrentItem(selectValue.b);
				
				setPosition();
			}
		});
		
		showRightButton(R.drawable.btn_ref_style, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				getDefaultData();
			}
		});
		
		//--
		
		lvLocation.setOnItemClickListener(new OnItemOrderClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id, LocationBean letter) {
				
				Select(letter,false);
			}
		});
		
		lvZoon.setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				LocationBean bean = listZoons.get(groupPosition).locations.get(childPosition);
				Select(bean,false);
				return false;
			}
		});
		
		lvZoon.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				ZoonBean data = listZoons.get(groupPosition);
				data.isExpand = !data.isExpand;
				zoonAdapter.notifyDataSetChanged();
				return false;
			}
		});
		
		searchLv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				hideSearch();
				searchEt.setText("");
				LocationBean bean = listSearchs.get(position);
				if(listSelects.contains(bean)){
					return;
				}else{
					Select(bean,true);
				}	
			}
		});
		
		searchEt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				searchChanged(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		searchEt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchEt.setCursorVisible(true);// 显示光标
			}
		});
	}
	
	private void setPosition(){
		if(isSingleSelect && listSelects.size() == 1){
			switch(single_select_bar.getCurrentSelectItem().b){
			case Mode_Location:
				int count =-1 ;
				for(LocationBean bean : listLocations){
					count++;
					if(bean.id == listSelects.get(0).id){
						if(count > 5){
							lvLocation.setSelection(count-3);
						}
						return;
					}
				}
				break;
			case Mode_Zone:
				int groupCount = -1;
				
				for(ZoonBean bean : listZoons){
					groupCount++;
					int childCount = -1;
					for(LocationBean location : bean.locations){
						childCount++;
						if(location.id == listSelects.get(0).id){
							bean.isExpand = true;
							postMessageListView(groupCount, childCount);
							return;
						}
					}
				}
				break;
			}
		}
	}
	
	private void postMessageListView(final int groupPosition,final int childPosition){
		SyncApplication.getThis().getHandler().post(new Runnable() {
			@Override
			public void run() {
				lvZoon.expandGroup(groupPosition);
				lvZoon.setSelectedChild(groupPosition, childPosition, true);
			}
		});
	}
	
	private void Select(LocationBean bean,boolean search){
		bean.isSelected = !bean.isSelected;
		//单选
		if(isSingleSelect){
			showSingleSelect(bean,search);
			return;
		}
		showMultipleSelect();
	}
	
	private void showSelect(){
		nameLayout.removeAllViews();
		int count = 0;
		for(final LocationBean bean : listSelects){
			View v = View.inflate(mActivity, R.layout.text_leader, null);
			((TextView) v.findViewById(R.id.tv)).setText(bean.location_name);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					showDialog_Delete(bean);
				}
			});

			nameLayout.addView(v);
			
			count++;
		}
		
		locationTv.setText(""+count);
		
		lvLocation.notifyDataSetChanged();
		zoonAdapter.notifyDataSetChanged();
		
		submitBtn.setVisibility(Utility.isEmpty(listSelects) ? View.GONE : View.VISIBLE);
		
	}
	private void showMultipleSelect(){
		listSelects.clear();
		
		for(LocationBean bean : listLocations){
			if(bean.isSelected){
				listSelects.add(bean);
			}
		}
		showSelect();
	}
	private void showSingleSelect(LocationBean data,boolean search){
		
		for(LocationBean bean : listLocations){
			if(!bean.equals(data)){
				bean.isSelected = false;
			}
		}
		listSelects.clear();
		if(data.isSelected)listSelects.add(data);
		
		showSelect();
		if(search)setPosition();
	}
	
	private void showDialog_Delete(final LocationBean bean){
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mActivity);
		builder.setMessage(getString(R.string.sync_delete)+"?");
		builder.setPositiveButton(getString(R.string.sync_yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				bean.isSelected = !bean.isSelected;
				listSelects.remove(bean);
				showSelect();
			}
		});
		builder.show();
	}
	
	private void getDefaultData(){
		RequestParams params = new RequestParams();
		params.add("ps_id",StoredData.getPs_id());
		new SimpleJSONUtil() {
			
			@Override
			public void handReponseJson(JSONObject json) {
				listZoons = new Gson().fromJson(json.optJSONArray("data").toString(),
						new TypeToken<List<ZoonBean>>(){}.getType());
				zoonAdapter.notifyDataSetChanged();
				listLocations = ZoonBean.getAllLocation(listZoons);
				lvLocation.setAdapter(listLocations,"Location");
				
				listSelects.clear();
				nameLayout.removeAllViews();
				locationTv.setText("");
				submitBtn.setVisibility(Utility.isEmpty(listSelects) ? View.GONE : View.VISIBLE);
				
				if(oldSelects != null){
					//
					if(oldSelects.size() > 15){
						return;
					}
					for(LocationBean bean : oldSelects){
						for(LocationBean location : listLocations){
							if(location.id == bean.id){
								Select(location,true);
							}
						}
					}
				}
			}
		}.setCancelable(false).doGet(HttpUrlPath.basePath + "_receive/android/getZoneAndLoction", params, mActivity);
		
	}
	
	private void initSingleSelectBar() {
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>("Zone", 0));
		clickItems.add(new HoldDoubleValue<String, Integer>("Location", 1));
		single_select_bar.setUserDefineClickItems(clickItems);
		single_select_bar.userDefineSelectIndexExcuteClick(0);
	}
	
	private void searchChanged(CharSequence s) {
		String searchStr = s.toString();
		// 提示
		if (!searchStr.isEmpty()) {
			showSearch();

			listSearchs.clear();
			for (LocationBean bean : listLocations) {
				if (bean.location_name.toUpperCase().startsWith(searchStr.toUpperCase())) {
					listSearchs.add(bean);
				}
			}

			searchAdapter.notifyDataSetChanged();
		} else {
			hideSearch();
		}
	}
	
	public void showSearch() {
		searchLayout.setVisibility(View.VISIBLE);
		searchLv.setVisibility(View.VISIBLE);
	}

	public void hideSearch() {
		searchLv.setVisibility(View.GONE);
		searchLayout.setVisibility(View.GONE);
		searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(mActivity);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.submitBtn:
			Intent in = new Intent();
			in.putExtra("SelectLocation", (Serializable)listSelects);
			setResult(RESULT_OK, in);
			finish();
			break;
		}
	}
	
	
	//----------------------------------------------------
	
	private boolean isSingleSelect;// 是否单选
	private List<LocationBean> oldSelects;
	
	public static void initParams(Intent in, boolean single_select) {
		in.putExtra("single_select", single_select);
	}
	
	public static void initParams(Intent in, boolean single_select,List<LocationBean> lists) {
		in.putExtra("single_select", single_select);
		in.putExtra("oldSelects",(Serializable)lists);
	}
	
	private void applyParams(){
		Intent in = getIntent();
		isSingleSelect = in.getBooleanExtra("single_select", false);
		oldSelects = (List<LocationBean>) in.getSerializableExtra("oldSelects");
	}
	
	///---------------------ViewPager
	private class LocationAdapter extends PagerAdapter{

		
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(mViewList.get(position));
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(mViewList.get(position));
			return mViewList.get(position);
		}

		@Override
		public int getCount() {
			return mViewList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
		
	}
	
	//------------------------Zone
	private class ZoonAdapter extends BaseExpandableListAdapter{

		@Override
		public int getGroupCount() {
			return listZoons == null ? 0 :listZoons.size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return listZoons.get(groupPosition).locations.size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return listZoons.get(groupPosition);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return listZoons.get(groupPosition).locations.get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			
			ViewGroupHolder holder;
			if(convertView == null){
				convertView = View.inflate(mActivity, R.layout.item_cc_location_title, null);
				holder = new ViewGroupHolder();
				
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
				holder.tvZone = (TextView) convertView.findViewById(R.id.tvZone);
				holder.ivArrow = (ImageView) convertView.findViewById(R.id.ivArrow);
				
				convertView.setTag(holder);
			}else{
				holder = (ViewGroupHolder) convertView.getTag();
			}
			
			ZoonBean data = listZoons.get(groupPosition);
			holder.tvZone.setText(data.area);
			holder.tvTitle.setText(data.titles);
			
			holder.ivArrow.setRotation(data.isExpand ? 0 : 180);
			
			return convertView;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			
			ViewChildHolder holder;
			if(convertView == null){
				convertView = View.inflate(mActivity, R.layout.item_cc_location, null);
				holder = new ViewChildHolder();
				
				holder.box = (CompoundButton) convertView.findViewById(R.id.box);
				holder.tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
				
				convertView.setTag(holder);
			}else{
				holder = (ViewChildHolder) convertView.getTag();
			}
			
			LocationBean data = listZoons.get(groupPosition).locations.get(childPosition);
			holder.tvLocation.setText("Location: "+data.location_name);
			holder.box.setChecked(data.isSelected);
			return convertView;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
		private class ViewChildHolder{
			TextView tvLocation;
			CompoundButton box;
		}
		private class ViewGroupHolder{
			TextView tvZone;
			TextView tvTitle;
			ImageView ivArrow;
		}
		
	}
	
	//----------------------------------Search
	
}
