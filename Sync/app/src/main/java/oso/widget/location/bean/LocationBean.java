package oso.widget.location.bean;

import java.io.Serializable;

public class LocationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5924094536175447894L;

	public int id;
	public String location_name;
	
	public boolean isSelected;
	
}
