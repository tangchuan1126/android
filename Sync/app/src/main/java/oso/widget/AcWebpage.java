package oso.widget;

import oso.base.BaseActivity;
import support.common.UIHelper;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import declare.com.vvme.R;

/**
 * 公共-网页
 * @author 朱成
 * @date 2014-9-17
 */
public class AcWebpage extends BaseActivity {
	
	private WebView webview;
	private View loProgress;
	private TextView tvProgress;
	
	private String url;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cm_webview, 0);
		setTitleString(getString(R.string.sync_preview));
		applyParams();

		//取view
		webview=(WebView)findViewById(R.id.webview);
		loProgress=findViewById(R.id.loProgress);
		tvProgress=(TextView)findViewById(R.id.tvProgress);
		
		
		//初始化webview
		WebSettings ws = webview.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setSupportZoom(true);
		ws.setBuiltInZoomControls(true);
		ws.setUseWideViewPort(true);
		ws.setLoadWithOverviewMode(true);
		
		webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				tvProgress.setText(progress + "%");
			}
		});
		webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loProgress.setVisibility(View.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				UIHelper.showToast(mActivity, "Oh no! " + description, Toast.LENGTH_SHORT);
			}
		});
		
		webview.loadUrl(url);
	}
	
	
	//=========传参===============================

	public static void initParams(Intent in,String url) {
		in.putExtra("url", url);
	}

	private void applyParams() {
		Bundle params = getIntent().getExtras();
		url=params.getString("url");
	}
	
	
	
}
