package oso.widget.screennotice;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.text.TextUtils;

import java.util.List;

import oso.SyncApplication;
import oso.base.ActivityStack;
import utility.Utility;

/**
 * @author xialimin
 * @description 唤醒管理器 用于锁屏弹窗
 *
 * 调用方式:
 *        SyncNotifyManager.showNotify(boolean isTask, String title, String time, String message);
 *        参数1: isTask   是否是任务通知, false则为聊天消息
 *        参数2: title    通知标题
 *        参数3: time     通知时间  任务通知无时间, 默认隐藏时间
 *        参数2: message  通知消息体
 */
public class SyncNotifyManager {

    public static boolean isInKeyguard; // 是否在锁屏界面 亮屏

    public static Activity mActivity;

    /**
     * 锁屏弹窗
     * @param isTask
     * @param title
     * @param time
     * @param message
     */
    public static void showNotify(final boolean isTask, final String title, final String time, final String message) {
        final Activity top= ActivityStack.getTopActInstance();

        top.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                schedule(top, isTask, title, time, message);
            }
        });

    }

    /**
     * 执行弹窗操作
     * @param act
     * @param isTask
     * @param title
     * @param time
     * @param message
     */
    private static void schedule(Activity act, boolean isTask, String title, String time, String message) {


        if (isForeground(act, LockScreenNotifyActivity.class.getName())) {

            if(mActivity == null) return;

            // 若程序 熄屏 & 在锁屏界面 重启界面
            if (!Utility.isScreenOn(act)) {
                mActivity.finish();
                proceedToActivity(act, isTask, title, time, message);
                return;
            }

            // 若程序 亮屏 & 在锁屏界面 直接更新
            ((LockScreenNotifyActivity) mActivity).update(isTask, title, time, message);

        } else {
            proceedToActivity(act, isTask, title, time, message);
        }
    }

    /**
     * 进入弹窗Activity
     * @param act
     * @param isTask
     * @param title
     * @param time
     * @param message
     */
    public static void proceedToActivity(Activity act, boolean isTask, String title, String time, String message) {
        Intent in = new Intent(act, LockScreenNotifyActivity.class);
        LockScreenNotifyActivity.initParams(in, isTask, title, time, message);
        SyncApplication.getThis().startActivity(in);
    }

    /**
     * 判断某个界面是否在前台
     *
     * @param context
     * @param className
     *
     */
    private static boolean isForeground(Context context, String className) {
        if (context == null || TextUtils.isEmpty(className)) {
            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if (className.equals(cpn.getClassName())) {
                return true;
            }
        }

        return false;
    }
}
