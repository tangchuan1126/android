package oso.widget.screennotice;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import declare.com.vvme.R;
import oso.ui.chat.activity.ChatTabActivity;
import oso.ui.msg.TaskTypesAc;
import utility.Utility;

/**
 * @author xialimin
 * @description 锁屏通知
 *
 * 调用方式:
 *        SyncNotifyManager.showNotify(boolean isTask, String title, String time, String message);
 *        参数1: isTask   是否是任务通知, false则为聊天消息
 *        参数2: title    通知标题
 *        参数3: time     通知时间  任务通知无时间, 默认隐藏时间
 *        参数2: message  通知消息体
 */
public class LockScreenNotifyActivity extends FragmentActivity implements View.OnClickListener {

	public static final String CONTENT_ISTASK = "CONTENT_IS_TASK";

	public static final String CONTENT_TITLE = "CONTENT_TITLE";

	public static final String CONTENT_TIME = "CONTENT_TIME";

	public static final String CONTENT_CONTENT = "CONTENT_CONTENT";

	public static final int DEFAULT_INTERVAL_SCREEN_OFF = 30 * 1000;

	private static Handler mHandler = new Handler();

    private TurnScreenOffRunnable mScreenOffRunnable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setWindow();

		setContentView(R.layout.dlg_lockscreen_notice);

		initView();

	}

	/**
	 * 初始化参数
	 * @param in
	 * @params isTask  true为任务通知, false为聊天消息
	 * @param alias
	 * @param time
	 * @param description
	 */
	public static void initParams(Intent in, boolean isTask, String alias, String time, String description) {
		in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		in.putExtra(CONTENT_ISTASK, isTask);
		in.putExtra(CONTENT_TITLE, alias);
		in.putExtra(CONTENT_TIME, time);
		in.putExtra(CONTENT_CONTENT, description);
	}

	/**
	 * 配置Window参数
	 */
	private void setWindow() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		if (!Utility.isScreenOn(this)) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		}

		if (Utility.isScreenOn(this) && !SyncNotifyManager.isInKeyguard) {
			finish();
		}
	}

	/**
	 * 初始化视图
	 */
	private void initView() {

		SyncNotifyManager.mActivity = this;

		boolean isTask = getIntent().getBooleanExtra(CONTENT_ISTASK, false);
		String alias = getIntent().getStringExtra(CONTENT_TITLE);
		String time = getIntent().getStringExtra(CONTENT_TIME);
		String content = getIntent().getStringExtra(CONTENT_CONTENT);

		update(isTask, alias, time, content);
		findViewById(R.id.dlg_notice_time).setVisibility(isTask ? View.GONE : View.VISIBLE);
		findViewById(R.id.layoutContent).setOnClickListener(this);
		findViewById(R.id.btnCancel).setOnClickListener(this);

	}

	/**
	 * 数据更新
	 * @param isTask
	 * @param alias
	 * @param time
	 * @param message
	 */
	public void update(final boolean isTask, final String alias, final String time, final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				((TextView) findViewById(R.id.notifyTitle)).setText(isTask ? getString(R.string.sync_notice_new_task) : getString(R.string.sync_notice_new_message));
				((TextView) findViewById(R.id.dlg_notice_alias)).setText(TextUtils.isEmpty(alias) ? "" : alias);
				((TextView) findViewById(R.id.dlg_notice_time)).setText(TextUtils.isEmpty(time) ? "" : time);
				((TextView) findViewById(R.id.dlg_notice_des)).setText(TextUtils.isEmpty(message) ? "" : message);

				scheduleStart();
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnCancel:
			case R.id.layoutContent:
				proceedToNoticeDetail();
				break;
		}
	}

	/**
	 * 进入通知详情   TaskTypeAc / ChatTabActivity
	 */
	private void proceedToNoticeDetail() {
		boolean isTask = getIntent().getBooleanExtra(CONTENT_ISTASK, false);
		Intent in = new Intent(LockScreenNotifyActivity.this, isTask ? TaskTypesAc.class : ChatTabActivity.class);
		startActivity(in);
		overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
		LockScreenNotifyActivity.this.finish();
	}

	/**
	 * 取消常亮Window Flag，否则无法熄屏
	 */
    private void clearScreenFlags() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

	/**
	 * 获得系统自动熄屏时间，并执行熄屏
	 */
    private void scheduleStart() {

		if (mScreenOffRunnable != null) {
			mHandler.removeCallbacks(mScreenOffRunnable);
			mScreenOffRunnable = null;
		}
		mScreenOffRunnable = new TurnScreenOffRunnable();
		mHandler.postDelayed(mScreenOffRunnable, getScreenOffTime());
	}

	private class TurnScreenOffRunnable implements Runnable {

		@Override
		public void run() {

			clearScreenFlags();

        }
	}

	/**
	 * 获得系统自动锁屏时间
	 * @return
	 */
	private int getScreenOffTime() {
		int screenOffTime = 0;
		try {
			screenOffTime = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
		} catch (Exception localException) {
			return DEFAULT_INTERVAL_SCREEN_OFF;
		}
		return screenOffTime == 0 ? DEFAULT_INTERVAL_SCREEN_OFF : screenOffTime;
	}
}
