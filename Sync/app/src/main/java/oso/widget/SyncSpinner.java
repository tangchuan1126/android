package oso.widget;


import oso.widget.dialog.BottomDialog;
import utility.Utility;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import declare.com.vvme.R;

/**
 * @author jiang
 * @date 2015-06-02 11:13:00
 */
public class SyncSpinner extends Button {

    private CharSequence[] resdatas;
    private String titleStr;
    private int index;

    public SyncSpinner(Context context) {
        this(context, null);
    }

    public SyncSpinner(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.dropDownSpinnerStyle);
    }

    public SyncSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setTextSize(13);
        setPadding(Utility.dip2px(getContext(), 10), 0, 0, 0);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showSingleDialog();
            }
        });
    }

    /**
     * setadapter 无选项
     *
     * @param resdatas 数据
     * @param onItemSelectedListener 选择监听
     */
    public void setAdapterData(CharSequence[] resdatas, OnItemSelectedListener onItemSelectedListener) {
        setAdapterData(getContext().getResources().getString(R.string.sync_options), resdatas, onItemSelectedListener, -1);
    }

    /**
     * setadapter
     *
     * @param resdatas 数据
     * @param onItemSelectedListener 选择监听
     * @param index 默认选中项索引
     */
    public void setAdapterData(CharSequence[] resdatas, OnItemSelectedListener onItemSelectedListener, int index) {
        setAdapterData(getContext().getResources().getString(R.string.sync_options), resdatas, onItemSelectedListener, index);
    }

    public void setAdapterData(String titleStr, CharSequence[] resdatas, OnItemSelectedListener onItemSelectedListener, int index) {
        this.resdatas = resdatas;
        this.titleStr = titleStr;
        this.index = index;
        this.onItemSelectedListener = onItemSelectedListener;
        setText(resdatas[index]);
        if (onItemSelectedListener != null)
            onItemSelectedListener.onItemSelected(index);
    }

    private void showSingleDialog() {
        final BottomDialog dialog = new BottomDialog(getContext());
        dialog.setTitle(titleStr);
        dialog.hideButton();
        dialog.setAdapterData(resdatas, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                setText(resdatas[index]);
                dialog.dismiss();
                if (onItemSelectedListener != null)
                    onItemSelectedListener.onItemSelected(position);
            }
        }, index);
        dialog.show();
    }

    public OnItemSelectedListener getOnItemSelectedListener() {
        return onItemSelectedListener;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    protected OnItemSelectedListener onItemSelectedListener;


    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }

}
