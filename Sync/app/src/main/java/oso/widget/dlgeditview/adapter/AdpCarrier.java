package oso.widget.dlgeditview.adapter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import oso.widget.autosearch.MCBean;
import oso.widget.dlgeditview.AdpDlgEt;
import oso.widget.dlgeditview.DlgEditView;
import utility.Utility;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * carrier/mcdot-代码提示
 */
public class AdpCarrier extends AdpDlgEt {
	public boolean isChange_carrier = true;
	private List<MCBean> listCarriers;

	private Context ct;
	private TextView etCarrier, etMcDot;

	private DlgEditView dlg;
	
	private String keyword="";		//同搜索的"列表"同步,可能和dlgEditView不同步

	public AdpCarrier(TextView etCarrier, TextView etMcDot) {
		this.etCarrier = etCarrier;
		this.etMcDot = etMcDot;

		this.ct = etCarrier.getContext();
	}

	@Override
	public boolean onGetData(DlgEditView dlg, JSONObject json,String keyword) {
		this.dlg = dlg;

		listCarriers = new ArrayList<MCBean>();
		// String[] ds = new String[0];
		listCarriers = MCBean.handJsonForList(json);
		// if (listCarriers != null && listCarriers.size() > 0) {
		// ds = new String[listCarriers.size()];
		// getStringlist(listCarriers, ds);
		// }
		// dlg.setItems(ds);

		this.keyword=keyword;
		dlg.getListView().setAdapter(adp);
		return true;
	};

	public String onItemSelected(int position) {
		MCBean b = listCarriers.get(position);
		// 联动改变
		if (isChange_carrier)
			etMcDot.setText(b.mc_dot);
		else
			etCarrier.setText(b.carrier);
		return isChange_carrier ? b.carrier : b.mc_dot;
	};

	// ==================private===============================

	public void getStringlist(List<MCBean> mclist, String[] sb) {
		if (mclist == null || mclist.size() == 0)
			return;
		for (int i = 0; i < mclist.size(); i++) {
			if (isChange_carrier)
				sb[i] = mclist.get(i).carrier + "";
			else
				sb[i] = mclist.get(i).mc_dot + "";
		}
	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = LayoutInflater.from(ct).inflate(
						R.layout.item_carrier, null);
				h = new Holder();
				h.tvMcDot_ = (TextView) convertView.findViewById(R.id.tvMcDot_);
				h.tvCarrier_ = (TextView) convertView
						.findViewById(R.id.tvCarrier_);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			MCBean b = listCarriers.get(position);
			h.tvCarrier_.setText(getSp(b.carrier, "No Carrier Name", true));
			h.tvMcDot_.setText(getSp(b.mc_dot, "No MC/DOT", false));

			return convertView;
		}

		private CharacterStyle spRed = new ForegroundColorSpan(0xffff0000);

		private Spannable getSp(String str, String strEmpty,
				boolean isCarrierOrMC) {
			boolean hasValue = !TextUtils.isEmpty(str);
			if (!hasValue) {
				Spannable spEmpty = Utility.getSpannable(strEmpty, new ForegroundColorSpan(
						0xff888888));
				Utility.addSpan(spEmpty, "", strEmpty, new StyleSpan(Typeface.ITALIC));
				return spEmpty;
			}

			Spannable spCr = new SpannableString(str);
			// 高光-当前搜索项
			if (isCarrierOrMC == isChange_carrier){
				Utility.addSpan(spCr, "", keyword, spRed);
			}
			return spCr;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listCarriers.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listCarriers == null ? 0 : listCarriers.size();
		}
	};

	class Holder {
		TextView tvMcDot_, tvCarrier_;
	}

}
