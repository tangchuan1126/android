package oso.widget.dlgeditview;

import org.json.JSONObject;

public abstract class AdpDlgEt {
	
	/**用于显示列表
	 * @param json
	 * @param keyword 正在搜索的关键字,不应直接从dlg获取 防止动态改变
	 * @return true:显示了列表
	 */
	public boolean onGetData(DlgEditView dlg,JSONObject json,String keyword){
		return false;
	}
	
	/**
	 * 选中项时
	 */
	public String onItemSelected(int position){
		return "";
	}
	
	
	
	
}
