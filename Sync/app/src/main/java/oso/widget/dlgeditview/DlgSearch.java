package oso.widget.dlgeditview;

import java.util.ArrayList;
import java.util.List;

import oso.ui.chat.bean.ChatUserInfo;
import oso.ui.chat.util.ChatUtil;
import oso.widget.RoundImageView;
import oso.widget.edittext.SearchEditText;
import utility.HttpUrlPath;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import declare.com.vvme.R;

/**
 * @author 朱成
 * @date 2015-1-23
 */
public abstract class DlgSearch extends Dialog implements OnClickListener{
	
	private Context context;
	
	private View lo_dlg;
	private SearchEditText et_dlg;
//	private EditText et_dlg;
	private ListView lv_dlg;
	private int threshold = 1; // 请求阈值
	
	private List<ChatUserInfo> list = new ArrayList<ChatUserInfo>();
	
	private SearchAdapter adapter = new SearchAdapter();
	/**
	 * 搜索时,此处显示列表
	 * @param keyword
	 */
	protected abstract void onSearch(String keyword) ;
	
	/**
	 * 选中项时
	 * @param bean
	 */
	protected abstract void onItemSelected(ChatUserInfo userBean);
	
	public ListView getListView() {
		return lv_dlg;
	}
	
	//==================inner===============================
	
	public DlgSearch(Context context) {
		super(context, R.style.dlgEditView);
		
		this.context=context;
		setContentView(R.layout.dlg_searchview);
		setCanceledOnTouchOutside(true);
		setCancelable(true);
		
		// dlg
		Window window = getWindow();
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		
		//取view
		lo_dlg = findViewById(R.id.lo_dlg);
		et_dlg = (SearchEditText) findViewById(R.id.vsearch);
//		et_dlg = vsearch.getInputEt();
		lv_dlg= (ListView) findViewById(R.id.lv_dlg);
		//监听事件
		lo_dlg.setOnClickListener(this);
		et_dlg.addTextChangedListener(onTextChanged);
		findViewById(R.id.btn_search_dialog_cancel).setOnClickListener(this);
		//初始化
		et_dlg.setIconMode(SearchEditText.IconMode_Del);
		et_dlg.setHint(context.getString(R.string.chat_search_user));
//		et_dlg.post(new Runnable() {
//			
//			@Override
//			public void run() {
//				Utility.showSoftkeyboard(getContext(), et_dlg);
//			}
//		});
		
	
		//lv
		lv_dlg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 防止重复触发
				et_dlg.removeTextChangedListener(onTextChanged);
				onItemSelected((ChatUserInfo)lv_dlg.getItemAtPosition(position));
				dismiss();
			}
		});
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.lo_dlg:
			dismiss();
			break;
		
		case R.id.btn_search_dialog_cancel:
			dismiss();
			break;
		default:
			break;
		}
	}
	
	private TextWatcher onTextChanged = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
//			updateTvDirect();

			String key = s.toString();
			// 超过阈值时,提示
			if (!TextUtils.isEmpty(key) && key.length() >= threshold)
				onSearch(key);
			else
				setItems(null);
		}
	};
	
	public void setItems(CharSequence[] items) {
		setItems(items, DlgEditView.ITEM_STYLE_1);
	}

	public void setItems(CharSequence[] items, int style) {
		if (items == null)
			items = new String[0];
		BaseAdapter adp = new ArrayAdapter<CharSequence>(context, style, items);
		lv_dlg.setAdapter(adp);
	}
	
	
	public void setItems(List<ChatUserInfo> list, boolean isFashion) {
		if(list == null) {
			return;
		}
		if(this.list != null) {
			this.list.clear();
		}
		
		this.list = list;
		
		// 去不同分组里重复的人
        /*Set<ChatUserInfo> set=new HashSet<ChatUserInfo>(this.list);         
        this.list.clear();       
        this.list.addAll(set); 
        System.out.println(list);*/
		
		for(int i=0; i<list.size(); i++) {
			for(int j=list.size()-1; j>i; j--) {
				if (list.get(i).getUsername().equals(list.get(j).getUsername())) {
                    list.remove(j);
                }
			}
		}
		
		lv_dlg.setAdapter(adapter);
	}
	
	private class SearchAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if(convertView == null) {
				convertView = View.inflate(context, R.layout.chat_contacts_search_layout, null);
				holder = new ViewHolder();
				holder.ivAvatar =  (RoundImageView) convertView.findViewById(R.id.chat_search_user_icon);
				holder.username = (TextView) convertView.findViewById(R.id.chat_search_contacts_nickname);
//				holder.status = (TextView) convertView.findViewById(R.id.chat_search_contacts_childStatus);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			setIvTag(holder.ivAvatar, list.get(position).getAvatarURL());
			setAvatar(holder, list.get(position).getAvatarURL());
			holder.username.setText(list.get(position).getAlias());
			return convertView;
		}
		
		/**
		 * 设置Tag
		 * @param hasAvatar
		 * @param ivAvatar
		 */
		private void setIvTag(ImageView ivAvatar, String avatarUrl) {
			if(!TextUtils.isEmpty(avatarUrl)) {
				ivAvatar.setTag(avatarUrl);
			}
		}
		
		private void setAvatar(ViewHolder holder, String avatarURL) {
			if(!TextUtils.isEmpty(avatarURL)) {
				loadImage(holder, avatarURL);
			}
			holder.ivAvatar.setImageResource(R.drawable.account_circle);
		}

		private void loadImage(final ViewHolder holder, final String avatarURL) {
			ImageLoader.getInstance().loadImage(HttpUrlPath.basePath+avatarURL, new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
					if(avatarURL.equals(holder.ivAvatar.getTag())) {
						holder.ivAvatar.setImageDrawable(ChatUtil.bitmapToDrawable(bitmap));
					}
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
				}
			});
			
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

	}
	
	class ViewHolder {
		RoundImageView ivAvatar;
		TextView username;
//		TextView status;
	}
}
