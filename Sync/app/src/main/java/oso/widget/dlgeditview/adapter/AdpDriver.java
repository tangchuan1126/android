package oso.widget.dlgeditview.adapter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.widget.autosearch.MCBean;
import oso.widget.dlgeditview.AdpDlgEt;
import oso.widget.dlgeditview.DlgEditView;
import oso.widget.dlgeditview.DriverBean;
import utility.Utility;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import declare.com.vvme.R;

/**
 * driverLiscense/driverName-代码提示
 */
public class AdpDriver extends AdpDlgEt {
	public boolean isChange_dl = true;
	private List<DriverBean> listDrivers;

	private Context ct;
	private TextView etDL, etDName;

	private DlgEditView dlg;
	private String keyword="";		//同搜索的"列表"同步,可能和dlgEditView不同步
	
	public AdpDriver(TextView etDL, TextView etDName) {
		this.etDL = etDL;
		this.etDName = etDName;

		this.ct = etDL.getContext();
	}

	@Override
	public boolean onGetData(DlgEditView dlg, JSONObject json,String keyword) {
		this.dlg = dlg;
		JSONArray ja = json.optJSONArray("datas");
		if (ja != null)
			listDrivers = new Gson().fromJson(ja.toString(),
					new TypeToken<List<DriverBean>>() {
					}.getType());

		this.keyword=keyword;
		dlg.getListView().setAdapter(adp);
		return true;
	};

	public String onItemSelected(int position) {
		DriverBean b = listDrivers.get(position);
		// 联动改变
		if (isChange_dl)
			etDName.setText(b.gate_driver_name);
		else
			etDL.setText(b.gate_driver_liscense);
		return isChange_dl ? b.gate_driver_liscense : b.gate_driver_name;
	};

	// ==================private===============================

	public void getStringlist(List<MCBean> mclist, String[] sb) {
		if (mclist == null || mclist.size() == 0)
			return;
		for (int i = 0; i < mclist.size(); i++) {
			if (isChange_dl)
				sb[i] = mclist.get(i).carrier + "";
			else
				sb[i] = mclist.get(i).mc_dot + "";
		}
	}

	// ==================nested===============================

	private BaseAdapter adp = new BaseAdapter() {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder h;
			if (convertView == null) {
				convertView = LayoutInflater.from(ct).inflate(
						R.layout.item_carrier, null);
				h = new Holder();
				h.tvMcDot_ = (TextView) convertView.findViewById(R.id.tvCarrier_);
				h.tvCarrier_ = (TextView) convertView
						.findViewById(R.id.tvMcDot_);
				convertView.setTag(h);
			} else
				h = (Holder) convertView.getTag();

			// 刷新数据
			DriverBean b = listDrivers.get(position);
			h.tvCarrier_.setText(getSp(b.gate_driver_liscense, "No Driver License", true));
			h.tvMcDot_.setText(getSp(b.gate_driver_name, "No Driver Name", false));

			return convertView;
		}

		private CharacterStyle spRed = new ForegroundColorSpan(0xffff0000);

		private Spannable getSp(String str, String strEmpty,
				boolean isDL) {
			boolean hasValue = !TextUtils.isEmpty(str);
			if (!hasValue) {
				Spannable spEmpty = Utility.getSpannable(strEmpty,
						new ForegroundColorSpan(0xff888888));
				Utility.addSpan(spEmpty, "", strEmpty, new StyleSpan(
						Typeface.ITALIC));
				return spEmpty;
			}

			Spannable spCr = new SpannableString(str);
			// 高光-当前搜索项
			if (isDL == isChange_dl)
				Utility.addSpan(spCr, "", keyword, spRed);
			return spCr;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return listDrivers.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listDrivers == null ? 0 : listDrivers.size();
		}
	};

	class Holder {
		TextView tvMcDot_, tvCarrier_;
	}

}
