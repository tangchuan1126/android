//package oso.widget.dlgeditview.adapter;
//
//import java.util.List;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//import declare.com.vvme.R;
//
//public class CommonTitleAdapter  extends BaseAdapter
//{
//
//	private List<Title> list ;
//	private LayoutInflater inflater;
//	private Context context;
//	
//	public CommonTitleAdapter(List<Title> list, Context context)
//	{
//		this.list = list;
//		this.context = context;
//		this.inflater = LayoutInflater.from(context);
//	}
//	
//
//	@Override
//	public int getCount()
//	{
//		return list.size();
//	}
//
//	@Override
//	public Object getItem(int position)
//	{
//		return list.get(position);
//	}
//
//	@Override
//	public long getItemId(int position)
//	{
//		return list.get(position).getTitleId();
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent)
//	{
//		CommonHolder holder = null;
//		if(null == convertView)
//		{
//			holder = new CommonHolder();
//			convertView = inflater.inflate(R.layout.title_list_items, null);
//			holder.title_id = (TextView)convertView.findViewById(R.id.title_id);
//			holder.title_name = (TextView)convertView.findViewById(R.id.title_name);
//			convertView.setTag(holder);
//		}
//		else
//		{
//			holder = (CommonHolder)convertView.getTag();
//		}
//		Title title = list.get(position);
//		holder.title_id.setText(title.getTitleId()+"");
//		holder.title_name.setText(title.getTitleName());
//		
//		return convertView;
//	}
//
//}
