package oso.widget.dlgeditview;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.widget.autosearch.MCBean;
import oso.widget.autosearch.SearchTextView;
import support.dbhelper.Goable;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 顶部edittext,可下拉提示列表
 * 
 * @author 朱成
 * @date 2015-1-6
 */
public class DlgEditView extends Dialog implements View.OnClickListener {

	public static final int ITEM_STYLE_1 = R.layout.simple_list_item_1; // 一般textView

	private ListView lv_dlg;
	private EditText et_dlg;

	private Context context;
	private TextView etTarget;

	private static String prompt = "Input";

	private static WeakReference<DlgEditView> instance;

	public static DlgEditView show(Context ct, TextView vTarget) {
		return show(ct, vTarget, "Input");
	}

	public static DlgEditView show(Context ct, TextView vTarget, String prompt) {
		DlgEditView.prompt = prompt;
		DlgEditView dlg = null;
		if (instance == null || instance.get() == null) {
			dlg = new DlgEditView(ct, vTarget);
			instance = new WeakReference<DlgEditView>(dlg);
		} else {
			dlg = instance.get();
			// 若target不同,则重建
			if (dlg.etTarget != vTarget) {
				dlg = new DlgEditView(ct, vTarget);
				instance = new WeakReference<DlgEditView>(dlg);
			}
		}
		dlg.show();
		return dlg;
	}

	private String method = "SearchCarrier";

	public DlgEditView setMethod(String method) {
		this.method = method;
		return this;
	}

	public DlgEditView setPrompt(String text) {
		prompt = text;
		return this;
	}

	public DlgEditView setPromptOnClick(final OnPromptListener onPromptListener) {
		tvDirectInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onPromptListener != null)
					onPromptListener.onClick(et_dlg.getText().toString());
				dismiss();
			}
		});
		new_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onPromptListener != null)
					onPromptListener.onClick(et_dlg.getText().toString());
				dismiss();
			}
		});
		et_dlg.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				// 回车退出
				if(keyCode == KeyEvent.KEYCODE_ENTER){
					if (onPromptListener != null)
						onPromptListener.onClick(et_dlg.getText().toString());
					dismiss();
				}
				if (Utility.isEnterUp(event))
					dismiss();
				return false;
			}
		});
		return this;
	}

	private boolean def_systemKeyboard = false; // 默认用-系统键盘

	public DlgEditView setDefKeyboard(boolean systemKeyboard) {
		// this.method = method;
		// vsearch.hideKbLayout();
		def_systemKeyboard = systemKeyboard;
		return this;
	}

	public DlgEditView setHint(String hint) {
		et_dlg.setHint(hint);
		return this;
	}

	private AdpDlgEt adp;

	public DlgEditView setAdp(AdpDlgEt adp) {
		this.adp = adp;
		return this;
	}

	/**
	 * 提示阈值
	 * 
	 * @param threshold
	 */
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public void setItems(CharSequence[] items) {
		setItems(items, ITEM_STYLE_1);
	}

	public void setItems(CharSequence[] items, int style) {
		if (items == null)
			items = new String[0];
		BaseAdapter adp = new ArrayAdapter<CharSequence>(context, style, items);
		lv_dlg.setAdapter(adp);
	}

	public ListView getListView() {
		return lv_dlg;
	}

	/**
	 * 输入字符串
	 */
	public String getSearchStr() {
		return et_dlg.getEditableText().toString();
	}

	// ==================static===============================

	/**
	 * 监听"焦点、点击"
	 * 
	 * @param etTarget
	 * @param onFocus
	 */
	public static void attachOnFocus(TextView etTarget, final View.OnClickListener onFocus) {
		attachOnFocus(etTarget, onFocus, true);
	}

	public static void attachOnFocus(TextView etTarget, final View.OnClickListener onFocus, boolean showFocus) {
		// 不弹键盘,且无光标
		etTarget.setInputType(InputType.TYPE_NULL);
		etTarget.setTransformationMethod(new AllCapTransformationMethod());
		etTarget.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == MotionEvent.ACTION_UP)
					onFocus.onClick(v);
				return false;
			}
		});
		if (showFocus)
			etTarget.setOnFocusChangeListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					if (hasFocus)
						onFocus.onClick(v);
				}
			});
	}

	// ==========inner=======================================

	private SearchTextView vsearch;
	private View lo_dlg;
	private TextView tvDirectInput;
	private Button new_button;
	private View loDirectInput;

	public DlgEditView(Context context, TextView et) {
		super(context, R.style.dlgEditView);

		// 初始化dlg
		Window window = getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		// window.setWindowAnimations(R.style.signAnim); // 添加动画

		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		setContentView(R.layout.dlg_editview);
		setCanceledOnTouchOutside(true);
		setCancelable(true);

		// 取view
		tvDirectInput = (TextView) findViewById(R.id.tvDirectInput);
		new_button = (Button) findViewById(R.id.new_button);
		loDirectInput = findViewById(R.id.loDirectInput);
		lo_dlg = findViewById(R.id.lo_dlg);
		lv_dlg = (ListView) findViewById(R.id.lv_dlg);
		// et_dlg = (SearchEditText) findViewById(R.id.et_dlg);
		// debug
		vsearch = (SearchTextView) findViewById(R.id.vsearch);
		et_dlg = vsearch.getInputEt();
		vsearch.setMainLayout(lo_dlg);
		vsearch.hideKbLayout();
		// vsearch.hideSoftInput();
		vsearch.showSearchBtn(false);
		vsearch.showDelBtn(true);
		vsearch.disableSearch();
		vsearch.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (def_systemKeyboard)
					vsearch.showSoftInput(et_dlg);
				else
					vsearch.showKbLayout();
			}
		});

		// debug
		// Utility.colseInputMethod(context,et_dlg);

		// 监听事件
		lo_dlg.setOnClickListener(this);
		tvDirectInput.setOnClickListener(this);
		new_button.setOnClickListener(this);
		// 初始化
		this.context = context;
		this.etTarget = et;

		// et
		// copy属性,text、转换方法
		et_dlg.setTransformationMethod(etTarget.getTransformationMethod());
		et_dlg.setText(et.getText());
		Utility.endEtCursor(et_dlg);
		setPrompt(prompt);
		updateTvDirect();

		// debug
		// et_dlg.setIconMode(SearchEditText.IconMode_Del);
		et_dlg.addTextChangedListener(onTextChanged);

		// lv
		lv_dlg.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				// 防止重复触发
				et_dlg.removeTextChangedListener(onTextChanged);
				// 写顶部et
				// et_dlg.setText(mclist.get(position).carrier);
				// 回调
				if (adp != null) {
					String tar = adp.onItemSelected(position);
					// 写顶部et
					et_dlg.setText(tar);
				}

				dismiss();
			}
		});
		lv_dlg.setSelected(false);
		
		//初始时,也查一下
		et_dlg.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				onTextChanged.afterTextChanged(et_dlg.getEditableText());
			}
		});
	}

	/**
	 * 写会至目标et
	 */
	private void writeBack() {
		// 释放资源
		if (req != null)
			req.cancel(true);

		if (etTarget == null)
			return;

		// 移除关联
		etTarget.setText(et_dlg.getEditableText().toString());
		etTarget = null;
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		writeBack();
		if (req != null)
			req.cancel(true);
	}

	private TextWatcher onTextChanged = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			updateTvDirect();

			String key = s.toString();
			// 超过阈值时,提示
			if (!TextUtils.isEmpty(key) && key.length() >= threshold)
				reqServer(key);
			else{
				cancelLastReq();
				setItems(null);
			}
		}
	};

	private void updateTvDirect() {
		// 写至-写回
		String key = Utility.getTransformedText(et_dlg);
		Spannable sp = Utility.getCompoundText(prompt + ": ", key, new ForegroundColorSpan(0xff0000ff));
		tvDirectInput.setText(sp);

		loDirectInput.setVisibility(TextUtils.isEmpty(key) ? View.GONE : View.VISIBLE);
	}

	private int threshold = 3; // 请求阈值

	private List<MCBean> mclist = new ArrayList<MCBean>();
	private AsyncHttpClient httpClient = new AsyncHttpClient();

	private RequestHandle req;

	private void reqServer(final String keyword) {
		RequestParams params = new RequestParams();
		params.add("Method", method);
		params.add("SearchValue", keyword);
		Goable.initGoable(context);
		StringUtil.setLoginInfoParams(params);
		//取消-前1请求
		cancelLastReq();
		req = httpClient.get(context, HttpUrlPath.AndroidAutoAction, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				super.onSuccess(statusCode, headers, json);
				
				if(adp!=null)
					adp.onGetData(DlgEditView.this, json,keyword);
			}
		});
		
	}
	
	private void cancelLastReq() {
		// 若之前有请求,先取消
		// 注:httpClient.cancelRequests可能产生ConcurrentModificationException(有遍历)
		if (req != null && (!req.isCancelled() || !req.isFinished()))
			// httpClient.cancelRequests(context, true);
			req.cancel(true);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvDirectInput: // 直接写回
			dismiss();
			break;
		case R.id.new_button:
			dismiss();
			break;
		case R.id.lo_dlg:
			dismiss();
			break;
		default:
			break;
		}
	}

	// ==================nested===============================

	public static class Builder {

		Context ct;
		TextView etTarget;
		String method = "SearchCarrier";

		public Builder(Context ct, TextView etTarget) {
			// TODO Auto-generated constructor stub
			this.ct = ct;
			this.etTarget = etTarget;
		}

		public Builder setMethod(String method) {
			this.method = method;
			return this;
		}

		public Builder show() {
			return null;
		}

	}

	public interface OnPromptListener {
		void onClick(String value);
	}

}
