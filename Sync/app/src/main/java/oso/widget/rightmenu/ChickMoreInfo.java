package oso.widget.rightmenu;

import android.view.View.OnClickListener;

public class ChickMoreInfo {
	
	private String button_name;
	private int button_icon = -1;
	private android.view.View.OnClickListener btn_click;
	
	
	public ChickMoreInfo() {
		super();
	}
	
	public ChickMoreInfo(String button_name, int button_icon,
			OnClickListener btn_click) {
		super();
		this.button_name = button_name;
		this.button_icon = button_icon;
		this.btn_click = btn_click;
	}
	
	public String getButton_name() {
		return button_name;
	}
	public void setButton_name(String button_name) {
		this.button_name = button_name;
	}
	public int getButton_icon() {
		return button_icon;
	}
	public void setButton_icon(int button_icon) {
		this.button_icon = button_icon;
	}
	public android.view.View.OnClickListener getBtn_click() {
		return btn_click;
	}
	public void setBtn_click(android.view.View.OnClickListener btn_click) {
		this.btn_click = btn_click;
	}

	
}
