package oso.widget.floatinglabelview;

import android.view.View;


public interface OnFloatingLableFocusChangeListener {

	public void onFocusChange(View v, boolean hasFocus);
}
