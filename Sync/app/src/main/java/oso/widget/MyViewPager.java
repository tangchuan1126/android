package oso.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 
 * 可以控制手势的ViewPager
 * 
 * @author jiang
 *
 */
public class MyViewPager extends ViewPager {

	private boolean scrollble = false; // 不可以滑动

	public MyViewPager(Context context) {
		super(context);
	}

	public MyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		return scrollble ? super.onTouchEvent(ev) : scrollble;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return scrollble ? super.onTouchEvent(ev) : scrollble;
	}

	public boolean isScrollble() {
		return scrollble;
	}

	public void setScrollble(boolean scrollble) {
		this.scrollble = scrollble;
	}
}
