package oso.widget;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * 简化textWatcher,不用每个都重写
 * @author 朱成
 * @date 2015-1-23
 */
public class SimpleTextWatcher implements TextWatcher {

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

}
