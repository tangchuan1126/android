package oso.widget;

import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import support.key.OccupyTypeKey;
import utility.Utility;

public class DockHelpUtil {

	// public static List<ResourceInfo> DOORLIST;//门的集合 全局静态变量

	// public static void getData(Context context){
	// RequestParams params = new RequestParams();
	// params.add("Method", HttpPostMethod.GetDoorAndSpot);
	// params.add("request_type", "0");
	// params.add("entry_id", "0");
	// new SimpleJSONUtil() {
	// @Override
	// public void handReponseJson(JSONObject json) {
	// stopjson(json);
	// }
	// }.doGet(HttpUrlPath.AndroidDockCheckInAction, params, context);
	// }

	// 入参:1>resType取OccupyTypeKey.Spot/Door
	//	    2>includeAll:手动添加ALL
	private static List<ResourceInfo> CommonlistDoor = new ArrayList<DockHelpUtil.ResourceInfo>();
	
	public static String[] getZones(List<ResourceInfo> list, int resType,
			boolean isOccupy) {
		Set<String> setZones=new HashSet<String>();
//		if(includeAll)
//			setZones.add("All Zones");
		for (ResourceInfo door : list) {
			boolean ok=resType==OccupyTypeKey.BOTH ||resType== door.resource_type;
			if (ok&& isOccupy == door.isOccupied)
				setZones.add(door.area_name);
		}
//		String[] ret=StringUtil.removeDuplicateWithOrder(data);
		String[] ret=setZones.toArray(new String[0]);
		//排序
		Arrays.sort(ret,Collator.getInstance());
		return ret;
	}
	
	// 取所有zone
	// 入参:1>resType取OccupyTypeKey.Spot/Door
	public static String[] getZones(List<ResourceInfo> list, int resType) {
		Set<String> setZones = new HashSet<String>();
		//后续会设置成ALL,防止被排序
		setZones.add("0000000");
		for (ResourceInfo door : list) {
			boolean ok = resType == OccupyTypeKey.BOTH
					|| resType == door.resource_type;
			if (ok)
				setZones.add(door.area_name);
		}
		// String[] ret=StringUtil.removeDuplicateWithOrder(data);
		String[] ret = setZones.toArray(new String[0]);
		// 排序
		Arrays.sort(ret, Collator.getInstance());
		
		//还原ALL
		ret[0]="All Zones";
		return ret;
	}
	
	public static List<ResourceInfo> filtersDoors(List<ResourceInfo> list,String area_name,boolean isOccupied) {
		List<ResourceInfo> datas = new ArrayList<DockHelpUtil.ResourceInfo>();
		for (ResourceInfo door : list) {
			//zone相同
			boolean ok="All Zones".equals(area_name) || door.area_name.equals(area_name);
			//占用情况
			if(ok&& isOccupied==door.isOccupied)
				datas.add(door);
		}
		return datas;
	}

	// 解析"spot/door"(free/occupy均含)
	public static List<ResourceInfo> handjson_big(JSONObject json) {
		// 解析doors
		List<ResourceInfo> listDoor = new ArrayList<DockHelpUtil.ResourceInfo>();
		parseDoors(json.optJSONArray("door"), listDoor, false);
		parseDoors(json.optJSONArray("notfreedoor"), listDoor, true);
		// 基于name排序
		ComparatorResourceInfo comparator = new ComparatorResourceInfo();
		Collections.sort(listDoor, comparator);

		// 解析spots
		List<ResourceInfo> listSpots = new ArrayList<DockHelpUtil.ResourceInfo>();
		parseSpots(json.optJSONArray("spot"), listSpots, false);
		parseSpots(json.optJSONArray("notfreespot"), listSpots, true);
		Collections.sort(listSpots, comparator);

		listDoor.addAll(listSpots);
		CommonlistDoor = listDoor;
		return listDoor;
	}
	public static List<ResourceInfo> getStopordoor() {
		return CommonlistDoor;
	}
	public static boolean isLoadData(){
		return CommonlistDoor != null && CommonlistDoor.size() > 0 ;
	}
	public static void clearData(){
		CommonlistDoor = null ;
	}
	private static void parseDoors(JSONArray jaZones,
			List<ResourceInfo> listDoors, boolean isOccupied) {
		if (jaZones == null)
			return;

		// 解析zone
		for (int i = 0; i < jaZones.length(); i++) {
			JSONObject joZone = jaZones.optJSONObject(i);
			String area_id = joZone.optString("doorzoneid");
			String area_name = joZone.optString("doorzonename");
			JSONArray jaDoors = joZone.optJSONArray("doors");
			// 若无door,略过
			if (jaDoors == null)
				continue;
			// 解析door
			for (int j = 0; j < jaDoors.length(); j++) {
				JSONObject joDoor = jaDoors.optJSONObject(j);
				ResourceInfo door = new ResourceInfo();
				door.resource_id = joDoor.optInt("doorid");
				door.resource_name = joDoor.optString("doorname");
				door.task = joDoor.optInt("task");
				door.equipment = joDoor.optInt("equipment");
				door.resource_type = OccupyTypeKey.DOOR;
				door.area_id = area_id;
				door.area_name = area_name;
				door.isOccupied = isOccupied;
				listDoors.add(door);
			}
		}
	}

	private static void parseSpots(JSONArray jaZones,
			List<ResourceInfo> listDoors, boolean isOccupied) {
		if (jaZones == null)
			return;

		// 解析zone
		for (int i = 0; i < jaZones.length(); i++) {
			JSONObject joZone = jaZones.optJSONObject(i);
			String area_id = joZone.optString("spotzoneid");
			String area_name = joZone.optString("spotzonename");
			JSONArray jaDoors = joZone.optJSONArray("spots");
			// 若无door,略过
			if (jaDoors == null)
				continue;
			// 解析door
			for (int j = 0; j < jaDoors.length(); j++) {
				JSONObject joDoor = jaDoors.optJSONObject(j);
				ResourceInfo door = new ResourceInfo();
				door.resource_id = joDoor.optInt("spotid");
				door.resource_name = joDoor.optString("spotname");
				door.task = joDoor.optInt("task");
				door.equipment = joDoor.optInt("equipment");
				door.resource_type = OccupyTypeKey.SPOT;
				door.area_id = area_id;
				door.area_name = area_name;
				door.isOccupied = isOccupied;
				listDoors.add(door);
			}
		}
	}

	// ==================nested===============================

	public static class ResourceInfo implements Serializable {
		/**
		 * @Fields serialVersionUID :
		 */
		private static final long serialVersionUID = 1854169144763010162L;
		public int resource_type;
		public int resource_id;
		public String resource_name;
		public String area_id;
		public String area_name;
		
		//===不一定有=========
		public int task;				//task数
		public int equipment;			//equip数
		public boolean isOccupied = false; // 已占用
		
		public boolean isEqual(ResourceInfo res){
			if(res==null)
				return false;
			return resource_id==res.resource_id&& area_id==res.area_id;
		}
	}

	/**
	 * @ClassName: ComparatorResourceInfo
	 * @Description:
	 * @author gcy
	 * @date 2014-12-3 下午7:22:41
	 */
	static class ComparatorResourceInfo implements Comparator<ResourceInfo> {
		public int compare(ResourceInfo arg0, ResourceInfo arg1) {
			ResourceInfo tp_one = (ResourceInfo) arg0;
			ResourceInfo tp_two = (ResourceInfo) arg1;
			int a = Utility.parseInt(tp_one.resource_name);
			int b = Utility.parseInt(tp_two.resource_name);
			
//			return a.compareTo(b);
			
			int flag = a == b ? 0 : (a > b ? 1 : -1);
			return flag;
		}
	}
}
