package oso.widget.loadingview;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import declare.com.vvme.R;

public class LoadingView extends FrameLayout {

	private ShapeLoadingView shapeLoadingView;

	private ImageView indicationIm;

	private TextView loadTextView;

	private static final int ANIMATION_DURATION = 500;

	private float mDistance = 100;
	
	private View border;

	public LoadingView(Context context) {
		super(context);
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
		init(context, attrs);

	}

	public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
	}

//	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
//	public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//		super(context, attrs, defStyleAttr, defStyleRes);
//		init(context, attrs);
//	}

	private void init(Context context, AttributeSet attrs) {

	}

	public int dip2px(float dipValue) {
		final float scale = getContext().getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		final View view = LayoutInflater.from(getContext()).inflate(R.layout.load_view, null);
		
		border = view.findViewById(R.id.border);

		mDistance = dip2px(38f);

		LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		layoutParams.gravity = Gravity.CENTER;

		shapeLoadingView = (ShapeLoadingView) view.findViewById(R.id.shapeLoadingView);

		indicationIm = (ImageView) view.findViewById(R.id.indication);
		loadTextView = (TextView) view.findViewById(R.id.promptTV);

		addView(view, layoutParams);

		this.postDelayed(new Runnable() {
			@Override
			public void run() {
				freeFall();
			}
		}, 200);

	}

	public void setLoadingText(CharSequence loadingText) {
		loadTextView.setText(loadingText);
	}
	
	public void setLoadingTextColor(int color) {
		loadTextView.setTextColor(color);
	}
	
	public void setShowBorder(boolean isShowing) {
		border.setBackgroundResource(isShowing ? R.drawable.ajm : R.drawable.transparent);
	}

	/**
	 * 上抛
	 */
	public void upThrow() {

		ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(shapeLoadingView, "translationY", mDistance, 0);
		ObjectAnimator scaleIndication = ObjectAnimator.ofFloat(indicationIm, "scaleX", 0.5f, 1);

		ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(shapeLoadingView, "rotation", 0, -360);

		objectAnimator.setDuration(ANIMATION_DURATION);
		objectAnimator1.setDuration(ANIMATION_DURATION);
		objectAnimator.setInterpolator(new DecelerateInterpolator());
		objectAnimator1.setInterpolator(new DecelerateInterpolator());
		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.setDuration(ANIMATION_DURATION);
		animatorSet.playTogether(objectAnimator, objectAnimator1, scaleIndication);

		animatorSet.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {

			}

			@Override
			public void onAnimationEnd(Animator animation) {
				freeFall();

			}

			@Override
			public void onAnimationCancel(Animator animation) {

			}

			@Override
			public void onAnimationRepeat(Animator animation) {

			}
		});
		animatorSet.start();

	}

	/**
	 * 下落
	 */
	public void freeFall() {

		ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(shapeLoadingView, "translationY", 0, mDistance);
		ObjectAnimator scaleIndication = ObjectAnimator.ofFloat(indicationIm, "scaleX", 1, 0.5f);

		objectAnimator.setDuration(ANIMATION_DURATION);
		objectAnimator.setInterpolator(new AccelerateInterpolator());
		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.setDuration(ANIMATION_DURATION);
		animatorSet.playTogether(objectAnimator, scaleIndication);
		animatorSet.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {

			}

			@Override
			public void onAnimationEnd(Animator animation) {

				shapeLoadingView.changeShape();
				upThrow();
			}

			@Override
			public void onAnimationCancel(Animator animation) {

			}

			@Override
			public void onAnimationRepeat(Animator animation) {

			}
		});
		animatorSet.start();

	}

}
