package oso.widget.loadingview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import declare.com.vvme.R;

public class ShapeLoadingView extends View {

	public ShapeLoadingView(Context context) {
		super(context);
		init();
	}

	public ShapeLoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ShapeLoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

//	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
//	public ShapeLoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//		super(context, attrs, defStyleAttr, defStyleRes);
//		init();
//	}

	private void init() {
		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setAntiAlias(true);
		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		setBackgroundColor(Color.TRANSPARENT);
	}

	public boolean mIsLoading = false;

	private Paint mPaint;

	private float mAnimPercent;

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mIsLoading) {
			mAnimPercent += 0.1611113;
			if (mAnimPercent >= 1) {
				mIsLoading = false;
				mAnimPercent = 1;
			}
			drawOSO(canvas);
			invalidate();

		} else {
			mAnimPercent = 0;
			drawOSO(canvas);
		}

	}

	private void drawOSO(Canvas canvas) {
		Bitmap bm = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_oso);
		Matrix matrix = new Matrix();
		matrix.postScale(0.5f, 0.5f);
		canvas.drawBitmap(bm, matrix, mPaint);
	}

	public void changeShape() {
		mIsLoading = true;
		invalidate();
	}

}
