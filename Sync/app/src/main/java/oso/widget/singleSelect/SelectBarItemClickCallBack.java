package oso.widget.singleSelect;

import support.common.datas.HoldDoubleValue;


public interface SelectBarItemClickCallBack {
	public void clickCallBack(HoldDoubleValue<String, Integer> selectValue);

}
