package oso.widget.singleSelect;

import java.util.ArrayList;
import java.util.List;

import support.common.datas.HoldDoubleValue;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 用于选择的时候使用类似 radioButton (只要有两项进行选择的时候调用)<br />
 * setUserDefineCallBack:注册点击的事件，如果有那么在点击每一个Item的时候会回调你注册的事件(回调参数包含有两个值String,Int)<br />
 * setUserDefineClickItems:显示的Items<br />
 * userDefineSelectIndexExcuteClick(int): 如果页面有默认的选择 & 需要一加载页面就执行callBack 方法<br />
 * userDefineSelectValueExcuteClick(selectValue); 通过selectValue 去执行callBack回调方法 <br />
 * userDefineRefresh() 	;	// 当前如果有选中的bar 自动执行一次。
 * userDefineSelectIndex(int)如果页面有默认的选择 ,只是颜色的切换。不执行callBack方法<br />
 * getCurrentSelectItem() 得到当前选中的Item，返回<String,Int> 如果当前没有选中那么返回值null<br />
 * clearSelectItem() 清除所有选中的状态<br />
 * @author zhangrui
 * @date 2014年5月28日 下午2:52:14
 * @description
 */
public class TransparentSingleSelectBar extends LinearLayout{
	 private static final int unSelectedIndex = -100 ;	//unselected
	 private static final int LEFT = -1 ;
	 private static final int CENTER = 0 ;
	 private static final int RIGHT = 1 ;		


	 
 	 private LinearLayout linearLayout ;
	 private Context context ;
	 private Resources resources ;
	 private List<TextView> containerTextView =  new ArrayList<TextView>();
	 private List<HoldDoubleValue<String, Integer>> clickItems ;
	 
	 private int count = 0 ;
	 private int currentIndexSelect = unSelectedIndex ;
	 private SelectBarItemClickCallBack callBack ;
	 
	 public TransparentSingleSelectBar(Context context) {  
         super(context);  
		 initSelectBar(context);
	 }  
     public TransparentSingleSelectBar(Context context, AttributeSet attrs) {  
    	 super(context, attrs);  
		 initSelectBar(context);
     }
 
     
	 
	 
	 
	 public void setUserDefineClickItems(List<HoldDoubleValue<String, Integer>> clickItems){
		 this.clickItems = clickItems ;
		 count = clickItems.size() ;
		 initShowValue();
	 }
	 public void setUserDefineCallBack(SelectBarItemClickCallBack callBack){
		 this.callBack = callBack ;
	 }
	 /**
	  * 选中一个TextView 只是改变样式，不执行click 方法
	  * @descrition
	  */
	 public void userDefineSelectIndex(int selectedIndex){
		 if(selectedIndex >= count){
			 return ;
		 }
		 changeColors(selectedIndex);
		 currentIndexSelect = selectedIndex ;
	 }
	 /**
	  * userDefineSelectIndexExcuteClick (选中一个 & 执行它的Click方法)
	  * @param selectedIndex
	  * @descrition
	  */
	 public void userDefineSelectIndexExcuteClick(int selectedIndex){
		 if(selectedIndex >= count){
			 return ;
		 }
		 userDefineSelectIndex(selectedIndex);
		 if(callBack != null){
				callBack.clickCallBack(clickItems.get(currentIndexSelect));
		  }
	 }
	 
	 public void userDefineRefresh(){
		 if(getCurrentSelectItem() != null){
			int value = getCurrentSelectItem().b ;
			int selectedIndex = getSelectIndexfromSelectValue(value);
			userDefineSelectIndexExcuteClick(selectedIndex);
		 }
 	 }
	 
	 
	 /**
	  * 通过select Value 通过当前选中的index
	  * @param selectValue
	  * @descrition
	  */
	 public void userDefineSelectValueExcuteClick(int selectValue){
		 	userDefineSelectIndexExcuteClick(getSelectIndexfromSelectValue(selectValue));
	 }
	 
	 /**
	  * 获取当前选中的Item，如果是unSelectedIndex 那么表示还没有选择
	  * @return
	  * @descrition
	  */
	 public HoldDoubleValue<String, Integer> getCurrentSelectItem(){
		 if(currentIndexSelect == unSelectedIndex){
			 return null ;
		 }
		 return clickItems.get(currentIndexSelect);
	 }
	 /**
	  * 清除选中的Item(还原成一个都没有选中的模样)
	  * @descrition
	  */
	 public void clearSelectItem(){
		 if(currentIndexSelect != unSelectedIndex){
			 changeToOriginalStyle(currentIndexSelect);
			 currentIndexSelect = unSelectedIndex ;
		 }
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>下面方法不用管<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	 
	 
	 private int getSelectIndexfromSelectValue(int value){
		 int selectIndex = unSelectedIndex ;
		 for(int index = 0  , count = clickItems.size() ; index < count ; index++ ){
		 	if(clickItems.get(index).b == value){
		 		selectIndex = index ;
		 		break ;
		    }
		 }
		 return selectIndex ;
	 }
	 private void initSelectBar(Context context){
		  this.context = context ;
		  this.resources = context.getResources() ;
		  LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
		  inflater.inflate(R.layout.single_select_bar, this);  
		  linearLayout = (LinearLayout) findViewById(R.id.select_bar);  
		  linearLayout.setBackgroundResource(R.drawable.tab_style);
		  linearLayout.setGravity(Gravity.CENTER);
	 }

	 
	 private void initShowValue(){
		if(clickItems != null && clickItems.size() > 0 ){
 			for(int index = 0   ; index < count ; index++ ){
				final int finalIndex = index ;
 				TextView textView = new TextView(context);
 				wrapText(textView, clickItems.get(index), index);
 				textView.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						changeColors(finalIndex);
						if(callBack != null){
							callBack.clickCallBack(clickItems.get(currentIndexSelect));
						}
					}
				});

 				textView.setPadding(0, 5, 0, 5);
 				containerTextView.add(textView);
 				linearLayout.addView(textView);
 				if(isCanAddSpan(index)){
 					linearLayout.addView(getSplitSpan());
 				}
			}
		} 
	 }
	 private  View getSplitSpan(){
		 View view = new View(context);
		 view.setLayoutParams(new LinearLayout.LayoutParams(1,ViewGroup.LayoutParams.FILL_PARENT));
		 view.setBackgroundResource(R.drawable.tab_style);
 		 return view ;
	 }
	 private boolean isCanAddSpan(int index){
		 if(index < count -1){
			 return true ;
		 }
		 return false ;
	 }
	 private void wrapText(TextView textView , HoldDoubleValue<String, Integer> hold , int index){
		 textView.setText(hold.a);
		 LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				 ViewGroup.LayoutParams.MATCH_PARENT,
				 ViewGroup.LayoutParams.WRAP_CONTENT ,1);
 		textView.setLayoutParams(params);
 		textView.setBackgroundResource(getViewBackground(index));
 		textView.setGravity(Gravity.CENTER);
 		textView.setTextColor(resources.getColor(R.color.black));
  		textView.setTextSize((float)(resources.getDimension(R.dimen.inventory_listview_text_size_three)/1.5));
 	 }
	 private void changeToOriginalStyle(int index){
		 TextView beforeView =	containerTextView.get(index) ;
		 beforeView.setBackgroundDrawable(resources.getDrawable(getViewBackground(index)));
		 beforeView.setTextColor(resources.getColor(R.color.black));
	 }
	 /**
	  * 点击颜色切换
	  * @param textView
	  * @param index
	  * @param count
	  * @descrition
	  */
	 private void changeColors(int index){
		 if(currentIndexSelect != unSelectedIndex){
			 changeToOriginalStyle(currentIndexSelect);
		 }
		 currentIndexSelect = index ;
		 changeToPressStyle(currentIndexSelect);
	 }
	 private void changeToPressStyle(int index){
		TextView textView = containerTextView.get(index);
		textView.setTextColor(resources.getColor(R.color.white));
		textView.setBackgroundDrawable(resources.getDrawable(getViewPressBackground(index)));
	 }
	 /**
	  * @param index
	  * @return
	  * @descrition
	  */
	 private  int getViewPressBackground(int index){
		 int where = getLeftCenterRight(index) ;
		 if(where == LEFT){
			 return R.drawable.tab_left_style_press ;
		 }
		 if(where == CENTER){
			 return R.drawable.tab_center_style_press ;
		 }
		 return R.drawable.tab_right_style_press ;
	 }
	 /**
	  * @param index
	  * @param count
	  * @return
	  * @descrition
	  */
	 private  int getViewBackground(int index){
		 int where = getLeftCenterRight(index) ;
		 if(where == LEFT){
			 return R.drawable.tab_left_style ;
		 }
		 if(where == CENTER){
			 return R.drawable.tab_center_style ;
		 }
		 return R.drawable.tab_right_style ;
	 }
	 /**
	  * 至少有两个
	  * @return -1left,0center,1right
	  * @descrition
	  */
	 private  int getLeftCenterRight(int index){
		 if(index == 0){
			 return LEFT ;
		 }
		 if(index < count -1){
			 return CENTER ;
		 }
		 return RIGHT ;
	 }
}
