package oso.widget;

import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.datas.HoldDoubleValue;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * 支持带tab的vp
 * 
 * @usage 1.xml中定义VpWithTab 2.VpWithTab下直接添加"子布局" 3.init可绑定tab
 * 
 * @author 朱成
 * @date 2014-12-25
 */
public class VpWithTab extends ViewPager {

	private SingleSelectBar tab;

	public boolean isScrollable = true;

	// private View[] listViews;

	public VpWithTab(Context context) {
		super(context);
		init_inner();
	}

	public VpWithTab(Context context, AttributeSet attrs) {
		super(context, attrs);
		init_inner();
	}

	private void init_inner() {
		// 不销毁视图
		setOffscreenPageLimit(1000);

		// 监听事件
		setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				// 滑动时 切换tab
				tab.userDefineSelectIndexExcuteClick(arg0);
				if (onVpPageChangeListener != null)
					onVpPageChangeListener.onPageChange(arg0);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void init(SingleSelectBar tab) {
		this.tab = tab;
		setAdapter(adp);

		tab.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				// 切换tab时,下方更随
				if (selectValue.b < getChildCount()) {
					setCurrentItem(selectValue.b);
				}
			}
		});
		tab.userDefineSelectIndexExcuteClick(0);
	}

	// @Override
	// public void scrollTo(int x, int y) {
	// // TODO Auto-generated method stub
	// if(isScrollable)
	// super.scrollTo(x, y);
	// }

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		/* return false;//super.onTouchEvent(arg0); */
		if (isScrollable)
			return super.onTouchEvent(arg0);
		else
			return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		if (isScrollable)
			return super.onInterceptTouchEvent(arg0);
		else
			return false;
	}

	// ==================nested===============================

	public OnVpPageChangeListener getOnVpPageChangeListener() {
		return onVpPageChangeListener;
	}

	public void setOnVpPageChangeListener(OnVpPageChangeListener onVpPageChangeListener) {
		this.onVpPageChangeListener = onVpPageChangeListener;
	}

	private PagerAdapter adp = new PagerAdapter() {

		public Object instantiateItem(ViewGroup container, int position) {
			// View v=listViews[position];
			// container.addView(v);
			return getChildAt(position);
		};

		public void destroyItem(ViewGroup container, int position, Object object) {

		};

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			// return listViews==null?0:listViews.length;
			return getChildCount();
		}
	};

	// ==================nested===============================

	private OnVpPageChangeListener onVpPageChangeListener;

	public interface OnVpPageChangeListener {
		/**
		 * 页面改变时
		 * 
		 * @param index
		 */
		public void onPageChange(int index);
	}

}
