package oso.widget.autosearch;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import declare.com.vvme.R;

public class CheckInSimpleItemTwoAdapter extends BaseAdapter {
		List<MCBean> listPerson;
		private LayoutInflater inflater;
		private Context context;
		private int type;
		public CheckInSimpleItemTwoAdapter(Context context, List<MCBean> list,int type) {
			this.context = context;
			listPerson = new ArrayList<MCBean>();
			listPerson = list;
			this.type = type;
			this.inflater  = LayoutInflater.from(this.context);
		}
	
		@Override
		public int getCount() {
			return listPerson==null?0:listPerson.size();
		}
	
		@Override
		public Object getItem(int position) {
			return listPerson.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return position;
		}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;

		if (convertView==null) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.list_item2,null);
			holder.carrier = (TextView) convertView.findViewById(R.id.item_carrier);
			holder.mc_dot = (TextView) convertView.findViewById(R.id.item_mc_dot);
			holder.carrier.setText(listPerson.get(position).carrier+"");
			holder.mc_dot.setText(listPerson.get(position).mc_dot+"");
			if(type == 0){
				holder.carrier.setVisibility(View.VISIBLE);
			}
			if(type == 1){
				holder.mc_dot.setVisibility(View.VISIBLE);
			}
		}
		return convertView;
	}

}

class Holder {
	TextView carrier;
	TextView mc_dot;
}
