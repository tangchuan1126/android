package oso.widget.autosearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

public class MCBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 313052287207382913L;
	public String carrier;
	public String mc_dot;
	
	
	public static List<MCBean> handJsonForList(JSONObject json) {
		List<MCBean> list = null;
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "datas");
		if(jsonArray != null && jsonArray.length() > 0){
			list = new ArrayList<MCBean>();
			// 解析-doors
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				if(item!=null){
					MCBean b = new MCBean();
					b.carrier = item.optString("carrier");
					b.mc_dot = item.optString("mc_dot");
					list.add(b);
				}
			}
		}
		return list;
	}
}
