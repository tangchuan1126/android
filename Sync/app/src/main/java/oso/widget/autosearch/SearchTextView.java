package oso.widget.autosearch;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import oso.widget.autosearch.Kb.OnKBChangeSoftClickListener;
import oso.widget.autosearch.Kb.OnKBClickListener;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.network.SimpleJSONUtil;
import utility.ActivityUtils;
import utility.AllCapTransformationMethod;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @author jiangzhen
 * 
 *         初始化
 * 
 *         sv = (SearchTextView) findViewById(R.id.searchEt);
 *         sv.setMainLayout(vMiddle); sv.setMethod("SearchContainerNo");
 *         sv.hideKbLayout(); sv.getInputEt().setTransformationMethod(new
 *         AllCapTransformationMethod());
 * 
 *         sv.setOnSearchListener(new OnSearchListener() {
 * @Override public void onSearch(String value) { search(value); } });
 * 
 *           sv.getInputEt().setOnKeyListener(new View.OnKeyListener() {
 * @Override public boolean onKey(View v, int keyCode, KeyEvent event) { if
 *           (event.getAction() == KeyEvent.ACTION_UP) { if (keyCode ==
 *           KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_ENTER) {
 *           String value = sv.getInputEt().getText().toString().trim();
 *           search(value); return true; } } return false; } });
 * 
 *           重写
 * 
 * @Override public boolean onKeyDown(int keyCode, KeyEvent event) { return
 *           sv.onKeyDown(keyCode, event) ? true : super.onKeyDown(keyCode,
 *           event); }
 * 
 */
public class SearchTextView extends FrameLayout implements TextWatcher,OnClickListener {

	private Context mContext;
	private AutoCompleteTextView inputEt;
	private ImageButton kbBtn, searchBtn;

	private LayoutInflater mInflater;
	private ArrayAdapter<String> adapter;
	private CheckInSimpleItemTwoAdapter mAdapter;
	private boolean isShowInput = false;
	private Kb kb;

	private String method = "SearchTrailer";

	private boolean isSelected = false;

	private int etMargin;

	private int maxValue = 3;

	private List<MCBean> mclist = new ArrayList<MCBean>();

	private String getothertxt;

	SearchTextView txt;
	int type = 0;

	public SearchTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public SearchTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public SearchTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context);
	}
	
	private View btnDel;

	private void init(Context c) {
		mContext = c;
		mInflater = LayoutInflater.from(c);

		View layout = mInflater.inflate(R.layout.widget_et_search, null);
		addView(layout);
		inputEt = (AutoCompleteTextView) layout.findViewById(R.id.inputEt);
		inputEt.setTransformationMethod(new AllCapTransformationMethod());
		searchBtn = (ImageButton) layout.findViewById(R.id.searchBtn);
		kbBtn = (ImageButton) layout.findViewById(R.id.kbBtn);
//		searchBtn.setOnClickListener(this);
//		kbBtn.setOnClickListener(this);
		
		btnDel=(ImageButton)layout.findViewById(R.id.btnDel_search);
		btnDel.setOnClickListener(this);

		kb = new Kb(c, inputEt);
		kb.setOnKBClickListener(new OnKBClickListener() {
			@Override
			public void onKeyDown() {
				isSelected = false;
			}
		});
		kb.setOnKBChangeSoftClickListener(new OnKBChangeSoftClickListener() {
			@Override
			public void onChangeSoftInput() {
				hideKbLayout();
				showSoftInput(inputEt);
			}
		});

		inputEt.setDropDownBackgroundResource(R.drawable.kuang_autoinput);
		inputEt.setDropDownWidth((int) (ActivityUtils.getScreenWidth(c) * 0.93));
		int offset = Utility.pxTodip(mContext, 5);
		inputEt.setDropDownHorizontalOffset(offset);
		inputEt.setDropDownVerticalOffset(offset);
		inputEt.setDropDownHeight((int) mContext.getResources().getDimension(R.dimen.auto_height));
		// 创建适配器
		adapter = new ArrayAdapter<String>(c, android.R.layout.simple_dropdown_item_1line, new String[] {});
		inputEt.setAdapter(adapter);
		// 设置输入多少字符后提示，默认值为2
		inputEt.setThreshold(maxValue);
		inputEt.addTextChangedListener(this);
		// inputEt.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
		// long arg3) {
		// // TODO Auto-generated method stub
		// UIHelper.showToast(mContext, inputEt.getText().toString(),
		// Toast.LENGTH_SHORT).show();
		// }
		// });
		inputEt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (kb.getVisibility() == View.VISIBLE)
					hideKbLayout();
			}
		});
		inputEt.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				isSelected = true;

				hideKbLayout();
				hideSoftInput();

				String value = inputEt.getText().toString().trim();
				if (!StringUtil.isNullOfStr(value)) {
					if (canSearch(value)) {
						if (onSearchListener != null) {
							onSearchListener.onSearch(value);
						}
					}
				} else {
					UIHelper.showToast(mContext, "Input Search Value", Toast.LENGTH_SHORT).show();
				}
			}
		});

		searchBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = inputEt.getText().toString().trim();
				if (!StringUtil.isNullOfStr(value)) {
					if (canSearch(value)) {
						if (onSearchListener != null) {
							onSearchListener.onSearch(value);
						}
					}
				} else {
					UIHelper.showToast(mContext, "Input Search Value", Toast.LENGTH_SHORT).show();
				}
			}
		});

		kbBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				inputEt.requestFocus();
				if (isShowInput) {
					hideKbLayout();
					//debug
					showSoftInput(inputEt);
				} else {
					showKbLayout();
				}
			}
		});
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnDel_search:
			inputEt.setText("");
			break;

		default:
			break;
		}
	}

	/**
	 * 设置主布局
	 */
	public void setMainLayout(View main) {
		((ViewGroup) main).addView(kb);
		isShowInput = true;
	}

	/**
	 * 重写onKeyDown
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		setSelected(false);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (kb.getVisibility() == View.VISIBLE) {
				hideKbLayout();
				return true;
			}
			return false;
		}
		return false;
	}

	private String searchValue;
	private long clickTime;

	private boolean canSearch(String value) {
		if (searchValue == null || !searchValue.equals(value)) {
			clickTime = System.currentTimeMillis();
			searchValue = value;
			return true;
		}
		if (System.currentTimeMillis() - clickTime > 2000) {
			return true;
		}
		return false;
	}

	public void hideKbLayout() {
		if (kb.getVisibility() == View.VISIBLE) {
			kb.setVisibility(View.GONE);
			kb.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.kb_exit));
			isShowInput = false;
		}
	}

	public void hideKbLayout(SearchTextView txts, int types) {
		if (kb.getVisibility() == View.VISIBLE) {
			kb.setVisibility(View.GONE);
			kb.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.kb_exit));
			isShowInput = false;
		}
		txt = txts;
		type = types;
	}

	public void showKbLayout() {
		hideSoftInput();
		postDelayed(new Runnable() {
			@Override
			public void run() {
				kb.setVisibility(View.VISIBLE);
				kb.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.kb_enter));
				isShowInput = true;
			}
		}, 100);
	}

	public void hideSoftInput() {
//		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//		imm.hideSoftInputFromWindow(((Activity) mContext).getWindow().getDecorView().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		Utility.colseInputMethod(mContext,inputEt);
	}

	public void showSoftInput(View view) {
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}

//	private void changeEdit(int num) {
//		inputEt.setText(inputEt.getText().toString().trim() + num);
//		inputEt.setSelection(inputEt.getText().toString().length());
//
//	}

//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.num0Btn:
//			changeEdit(0);
//			break;
//		case R.id.num1Btn:
//			changeEdit(1);
//			break;
//		case R.id.num2Btn:
//			changeEdit(2);
//			break;
//		case R.id.num3Btn:
//			changeEdit(3);
//			break;
//		case R.id.num4Btn:
//			changeEdit(4);
//			break;
//		case R.id.num5Btn:
//			changeEdit(5);
//			break;
//		case R.id.num6Btn:
//			changeEdit(6);
//			break;
//		case R.id.num7Btn:
//			changeEdit(7);
//			break;
//		case R.id.num8Btn:
//			changeEdit(8);
//			break;
//		case R.id.num9Btn:
//			changeEdit(9);
//			break;
//
//		case R.id.cleanBtn:
//			// if (inputEt.getText().toString().length() > 0) {
//			// inputEt.setText(inputEt.getText().toString().substring(0,
//			// inputEt.getText().toString().length() - 1));
//			// inputEt.setSelection(inputEt.getText().toString().length());
//			// } else {
//			// UIHelper.showToast(mContext, "d隐藏", Toast.LENGTH_SHORT).show();
//			// adapter = new ArrayAdapter<String>(mContext,
//			// android.R.layout.simple_dropdown_item_1line, new String[] {});
//			// inputEt.setAdapter(adapter);
//			// inputEt.dismissDropDown();
//			// }
//			int code = KeyEvent.KEYCODE_DEL; // 这里是退格键
//			KeyEvent keyEventDown = new KeyEvent(KeyEvent.ACTION_DOWN, code);
//			KeyEvent keyEventUp = new KeyEvent(KeyEvent.ACTION_UP, code);
//			inputEt.onKeyDown(code, keyEventDown); // editNumber
//			inputEt.onKeyUp(code, keyEventUp);
//			break;
//		case R.id.changeBtn:
//			showSoftInput(inputEt);
//			hideKbLayout();
//			break;
//
//		default:
//			break;
//		}
//	}

	// private void sendDataToServer() {
	// RequestParams params = new RequestParams();
	// params.add("Method", "SearchContainerNo");
	// params.add("ContainerNo", data);
	// new SimpleJSONUtil() {
	// @Override
	// public void handReponseJson(JSONObject json) {
	// System.out.println(json);
	// }
	// @Override
	// public void handFinish() {
	//
	// }
	// }.doGet(autoUrl, params , mContext);
	// }

	private void sendDataToServer() {
		RequestParams params = new RequestParams();
		params.add("Method", getMethod());
		params.add("SearchValue", data);

		Goable.initGoable(mContext);
		StringUtil.setLoginInfoParams(params);

		SimpleJSONUtil.client.get(HttpUrlPath.AndroidAutoAction, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				super.onSuccess(statusCode, headers, json);
				System.out.println("返回数据= " + json.toString());
				String type = StringUtil.getJsonString(json, "type"); // 判断是字符串还是json
				if (type.equals("string")) {
					praseString(json);
				} else {
					praseJson(json);
				}
			}
		});
	}

	private void praseString(JSONObject json) {
		String datas = StringUtil.getJsonString(json, "datas");
		if (!StringUtil.isNullOfStr(inputEt.getText().toString().trim()) && !StringUtil.isNullOfStr(datas)) {
			String[] ds = datas.split(",");
			if (!isSelected) {
				adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, ds);
				inputEt.setAdapter(adapter);
				inputEt.showDropDown();
				if (onDropDownListener != null) {
					onDropDownListener.onShowDropDown();
				}
			} else {
				inputEt.dismissDropDown();
			}
		} else {
			adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, new String[] {});
			inputEt.setAdapter(adapter);
			inputEt.dismissDropDown();
		}
	}

	private void praseJson(JSONObject json) {
		mclist = MCBean.handJsonForList(json);
		if (!StringUtil.isNullOfStr(inputEt.getText().toString().trim()) && mclist != null && mclist.size() > 0) {
			String[] ds = new String[mclist.size()];
			ds = getStringlist(ds);
			if (!isSelected && ds != null) {
				adapter = new ArrayAdapter<String>(mContext, R.layout.simple_list_item_1, ds);
				// mAdapter = new CheckInSimpleItemTwoAdapter(mContext,
				// mclist, 0);
				inputEt.setAdapter(adapter);
				inputEt.showDropDown();
				if (onDropDownListener != null) {
					onDropDownListener.onShowDropDown();
				}
			} else {
				inputEt.dismissDropDown();
			}
		} else {
			adapter = new ArrayAdapter<String>(mContext, R.layout.simple_list_item_1, new String[] {});
			inputEt.setAdapter(adapter);
			inputEt.dismissDropDown();
		}

	}

	public String[] getStringlist(String[] sb) {
		if (mclist != null && mclist.size() > 0) {
			for (int i = 0; i < mclist.size(); i++) {
				sb[i] = mclist.get(i).carrier + "," + mclist.get(i).mc_dot;
			}
		}
		return sb;
	}

	String data = "";

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		updateDelBtn();
		
		data = s.toString();
		if (!StringUtil.isNullOfStr(data) && data.length() >= maxValue) {
			if (isFastDoubleChanged()) {
				return;
			}
			
			if(TextUtils.isEmpty(method))
				return;
			postDelayed(new Runnable() {
				@Override
				public void run() {
					sendDataToServer();
				}
			}, 200);
			
		} else {
			// UIHelper.showToast(mContext, "隐藏", Toast.LENGTH_SHORT).show();
			adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, new String[] {});
			inputEt.setAdapter(adapter);
			inputEt.dismissDropDown();
		}

	}
	
	private boolean toShowDelBtn=false;
	
	private void updateDelBtn(){
		String str=inputEt.getText().toString();
		boolean ok=toShowDelBtn&& !TextUtils.isEmpty(str);
		btnDel.setVisibility(ok?View.VISIBLE:View.GONE);
	}

	private long lastClickTime;
	boolean isf = true;

	public boolean isFastDoubleChanged() {

		long time = System.currentTimeMillis();
		long timeD = time - lastClickTime;
		if (timeD > 800) {
			lastClickTime = time;
			return false;
		}
		if (isf) {
			isf = false;
			return true;
		}
		return true;
	}

	// public void hideKbBtn() {
	// kbBtn.setVisibility(View.GONE);
	// tv.setVisibility(View.GONE);
	// }
	//
	// public void showKbBtn() {
	// kbBtn.setVisibility(View.VISIBLE);
	// tv.setVisibility(View.VISIBLE);
	// }

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub
		// UIHelper.showToast(mContext, inputEt.getText().toString(), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub
		if (mclist != null && mclist.size() > 0) {
			if (type == 0) {
				for (MCBean item : mclist) {
					s.equals(item.mc_dot);
					txt.setText(item.carrier + "");
				}
			}
			if (type == 1) {
				for (MCBean item : mclist) {
					s.equals(item.carrier);
					txt.setText(item.mc_dot + "");
				}
			}

		}
	}

	// public void setParams(RequestParams params) {
	// this.params = params;
	// }

	public OnSearchListener getOnSearchListener() {
		return onSearchListener;
	}

	public void setOnSearchListener(OnSearchListener onSearchListener) {
		this.onSearchListener = onSearchListener;
	}

	public String getMethod() {
		return method;
	}

	/**
	 * 设置查询的Method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	public View getKeyboard() {
		return kb;
	}

	public AutoCompleteTextView getInputEt() {
		return inputEt;
	}

	public int getEtMargin() {
		return etMargin;
	}

	public void setEtMargin(int etMargin) {
		this.etMargin = etMargin;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public OnDropDownListener getOnDropDownListener() {
		return onDropDownListener;
	}

	public void setOnDropDownListener(OnDropDownListener onDropDownListener) {
		this.onDropDownListener = onDropDownListener;
	}

	private OnSearchListener onSearchListener;

	public interface OnSearchListener {
		void onSearch(String value);
	}

	private OnDropDownListener onDropDownListener;

	public interface OnDropDownListener {
		void onShowDropDown();
	}

	public void setText(String text) {
		// TODO Auto-generated method stub
		inputEt.setText(text);
	}

	public void setsearchBtn(int searchBtnnum) {
		if (searchBtnnum == 1) {
			searchBtn.setVisibility(View.GONE);
		}
	}
	
	public void showSearchBtn(boolean show){
		searchBtn.setVisibility(show?View.VISIBLE:View.GONE);
	}
	
	public void disableSearch(){
		setMethod("");
	}
	
	public void showDelBtn(boolean show){
		toShowDelBtn=show;
		updateDelBtn();
	}
	
	
}
