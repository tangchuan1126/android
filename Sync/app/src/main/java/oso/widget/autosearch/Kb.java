package oso.widget.autosearch;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import declare.com.vvme.R;

/**
 * 自定义键盘
 * @author 朱成
 * @date 2015-1-9
 */
public class Kb extends FrameLayout implements OnClickListener {
	
	private Context mContext;
	private LayoutInflater mInflater;
	private AutoCompleteTextView inputEt;
	private RelativeLayout num0Btn, num1Btn, num2Btn, num3Btn, num4Btn, num5Btn, num6Btn, num7Btn, num8Btn, num9Btn, cleanBtn, changeBtn;
	
	public Kb(Context context, AutoCompleteTextView et) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context, et);
	}

	private void init(Context c, AutoCompleteTextView et) {
		mContext = c;
		inputEt = et;
		mInflater = LayoutInflater.from(c);
		View layout = mInflater.inflate(R.layout.widget_kb, null);
		addView(layout);
		
		num0Btn = (RelativeLayout) layout.findViewById(R.id.num0Btn);
		num1Btn = (RelativeLayout) layout.findViewById(R.id.num1Btn);
		num2Btn = (RelativeLayout) layout.findViewById(R.id.num2Btn);
		num3Btn = (RelativeLayout) layout.findViewById(R.id.num3Btn);
		num4Btn = (RelativeLayout) layout.findViewById(R.id.num4Btn);
		num5Btn = (RelativeLayout) layout.findViewById(R.id.num5Btn);
		num6Btn = (RelativeLayout) layout.findViewById(R.id.num6Btn);
		num7Btn = (RelativeLayout) layout.findViewById(R.id.num7Btn);
		num8Btn = (RelativeLayout) layout.findViewById(R.id.num8Btn);
		num9Btn = (RelativeLayout) layout.findViewById(R.id.num9Btn);
		cleanBtn = (RelativeLayout) layout.findViewById(R.id.cleanBtn);
		changeBtn = (RelativeLayout) layout.findViewById(R.id.changeBtn);

		num0Btn.setOnClickListener(this);
		num1Btn.setOnClickListener(this);
		num2Btn.setOnClickListener(this);
		num3Btn.setOnClickListener(this);
		num4Btn.setOnClickListener(this);
		num5Btn.setOnClickListener(this);
		num6Btn.setOnClickListener(this);
		num7Btn.setOnClickListener(this);
		num8Btn.setOnClickListener(this);
		num9Btn.setOnClickListener(this);
		cleanBtn.setOnClickListener(this);
		changeBtn.setOnClickListener(this);
	}
	
	private void changeEdit(int num) {
		inputEt.setText(inputEt.getText().toString().trim() + num);
		inputEt.setSelection(inputEt.getText().toString().length());
		if (onKbClick != null) {
			onKbClick.onKeyDown();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.num0Btn:
			changeEdit(0);
			break;
		case R.id.num1Btn:
			changeEdit(1);
			break;
		case R.id.num2Btn:
			changeEdit(2);
			break;
		case R.id.num3Btn:
			changeEdit(3);
			break;
		case R.id.num4Btn:
			changeEdit(4);
			break;
		case R.id.num5Btn:
			changeEdit(5);
			break;
		case R.id.num6Btn:
			changeEdit(6);
			break;
		case R.id.num7Btn:
			changeEdit(7);
			break;
		case R.id.num8Btn:
			changeEdit(8);
			break;
		case R.id.num9Btn:
			changeEdit(9);
			break;

		case R.id.cleanBtn:
			// if (inputEt.getText().toString().length() > 0) {
			// inputEt.setText(inputEt.getText().toString().substring(0,
			// inputEt.getText().toString().length() - 1));
			// inputEt.setSelection(inputEt.getText().toString().length());
			// }
			int code = KeyEvent.KEYCODE_DEL; // 这里是退格键
			KeyEvent keyEventDown = new KeyEvent(KeyEvent.ACTION_DOWN, code);
			KeyEvent keyEventUp = new KeyEvent(KeyEvent.ACTION_UP, code);
			inputEt.onKeyDown(code, keyEventDown); // editNumber
			inputEt.onKeyUp(code, keyEventUp);
			if (onKbClick != null) {
				onKbClick.onKeyDown();
			}
			break;
		case R.id.changeBtn:
//			showSoftInput(inputEt);
//			setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.kb_exit));
//			setVisibility(View.GONE);
			if (onKBChangeSoftClickListener != null) {
				onKBChangeSoftClickListener.onChangeSoftInput();
			}
			break;

		default:
			break;
		}
	}
	
	public void hideSoftInput() {
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(((Activity) mContext).getWindow().getDecorView().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void showSoftInput(View view) {
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}
	
	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm;
	}
	
	public OnKBClickListener getOnKBClickListener() {
		return onKbClick;
	}

	public void setOnKBClickListener(OnKBClickListener onKbClick) {
		this.onKbClick = onKbClick;
	}

	public OnKBChangeSoftClickListener getOnKBChangeSoftClickListener() {
		return onKBChangeSoftClickListener;
	}

	public void setOnKBChangeSoftClickListener(OnKBChangeSoftClickListener onKBChangeSoftClickListener) {
		this.onKBChangeSoftClickListener = onKBChangeSoftClickListener;
	}

	private OnKBClickListener onKbClick;
	
	public interface OnKBClickListener {
		void onKeyDown();
	}
	
	private OnKBChangeSoftClickListener onKBChangeSoftClickListener;
	
	public interface OnKBChangeSoftClickListener {
		void onChangeSoftInput();
	}

}
