package oso.widget.loadbar;

/**
 * 用于loadbarDialog中显示freightTerm
 * @author 朱成
 * @date 2015-2-10
 */
public class FreightTermBean {
	public String taskType;
	public String taskNo;
	public String freightTerm;
}
