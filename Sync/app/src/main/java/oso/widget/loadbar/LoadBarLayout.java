package oso.widget.loadbar;

import java.util.ArrayList;
import java.util.List;

import oso.widget.MyViewPager;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import oso.widget.singleSelect.SelectBarItemClickCallBack;
import oso.widget.singleSelect.SingleSelectBar;
import support.common.bean.CheckInTaskBeanMain;
import support.common.datas.HoldDoubleValue;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import declare.com.vvme.R;

/**
 * @ClassName: LoadBarLayout
 * @Description: 用于添加修改loadbar 以及 拍照
 * @author gcy
 * @date 2014-12-15 下午3:40:17
 */
public class LoadBarLayout extends LinearLayout {

	private Context context;

	private SingleSelectBar singleSelectBar;// 选项卡
	private MyViewPager mViewPager;
	private ArrayList<View> mViewList = new ArrayList<View>();
	private static final int PHOTO = 0;
	private static final int LOADBAR = 1;
	private static final int SEAL = 2;

	private View photoLayout, loadBarLayout, sealLayout;
	private TabToPhoto ttp;
	private VSealBox sealBox;
	private VLoadBarBox vLoadBarBox;

	public LoadBarLayout(Context context) {
		super(context);
		this.context = context;
	}

	public LoadBarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View linearLayout = li.inflate(R.layout.loadbar_layout, this, true);
		// 获得对子控件的引用

		initViewPager(linearLayout);
	}

	/**
	 * @Description:初始化singleBarSelect
	 */
	private void initSingleSelectBar(View linearLayout) {
		singleSelectBar = (SingleSelectBar) linearLayout.findViewById(R.id.single_select_bar);
		List<HoldDoubleValue<String, Integer>> clickItems = new ArrayList<HoldDoubleValue<String, Integer>>();
		clickItems.add(new HoldDoubleValue<String, Integer>(getResources().getString(R.string.task_item_photo), PHOTO));
		clickItems.add(new HoldDoubleValue<String, Integer>(getResources().getString(R.string.task_item_loadbar), LOADBAR));
		clickItems.add(new HoldDoubleValue<String, Integer>(getResources().getString(R.string.task_item_seal), SEAL));
		singleSelectBar.setUserDefineClickItems(clickItems);
		singleSelectBar.userDefineSelectIndexExcuteClick(PHOTO);
	}

	public TabToPhoto getTabToPhoto() {
		return ttp;
	}

	private void initViewPager(View linearLayout) {

		LayoutInflater inflater = LayoutInflater.from(context);
		photoLayout = inflater.inflate(R.layout.dock_checkin_task_dooritem_page0, null);
		loadBarLayout = inflater.inflate(R.layout.dock_checkin_task_dooritem_page1, null);
		sealLayout = inflater.inflate(R.layout.dock_checkin_task_dooritem_page2, null);
		mViewList.add(photoLayout);
		mViewList.add(loadBarLayout);
		mViewList.add(sealLayout);

		mViewPager = (MyViewPager) linearLayout.findViewById(R.id.pager);
		mViewPager.setAdapter(new VPAdapter());
		initSingleSelectBar(linearLayout);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				singleSelectBar.userDefineSelectIndexExcuteClick(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		mViewPager.setCurrentItem(PHOTO);
		singleSelectBar.setUserDefineCallBack(new SelectBarItemClickCallBack() {
			@Override
			public void clickCallBack(HoldDoubleValue<String, Integer> selectValue) {
				mViewPager.setCurrentItem(selectValue.b);
			}
		});
		ttp = (TabToPhoto) photoLayout.findViewById(R.id.ttp);
		vLoadBarBox = (VLoadBarBox) loadBarLayout.findViewById(R.id.vLoadBarBox);
		sealBox = (VSealBox) sealLayout.findViewById(R.id.sealBox);
	}

	private class VPAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return mViewList.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {
			((ViewPager) container).addView(mViewList.get(position), 0);
			return mViewList.get(position);
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView(mViewList.get(position));
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
	}

	private String curTaskNo;

	/**
	 * @Description:初始化控件
	 * @param @param activity 调用该控件的当前 activity
	 * @param @param loadUseList 获得的loadbar的数据用于显示
	 * @param @param vmiddle 基类中的一个控件
	 * @param @param mainbean CheckInTaskBeanMain 类型的数据
	 */
	public void initData(final CheckInTaskBeanMain mainbean, TabParam tab, String curTaskNo) {
		init(mainbean, curTaskNo);
		setPhotoData(tab, mainbean);
	}

	/**
	 * 不初始化ttp
	 */
	public void init(final CheckInTaskBeanMain mainbean, String curTaskNo) {
		// --设置照片
		this.curTaskNo = curTaskNo;
		vLoadBarBox.init(mainbean, curTaskNo);
		sealBox.init(mainbean);
	}

	/**
	 * @Description:初始化控件
	 * @param @param activity 调用该控件的当前 activity
	 * @param @param loadUseList 获得的loadbar的数据用于显示
	 * @param @param vmiddle 基类中的一个控件
	 * @param @param mainbean CheckInTaskBeanMain 类型的数据
	 */
	public void initData(final CheckInTaskBeanMain mainbean) {
		vLoadBarBox.init(mainbean, curTaskNo);
		sealBox.init(mainbean);
	}

	/**
	 * @Description:初始化拍照控件
	 * @param @param mainbean
	 */
	private void setPhotoData(TabParam tab, CheckInTaskBeanMain mainbean) {
		ttp.setBackgroundResource(R.color.white);
		// TabParam
		// p=TabParam.new_NoTitle("Task_processing_"+mainbean.entry_id+mainbean.resources_id,
		// ttp);
		// p.setWebImgsParams(String.valueOf(FileWithCheckInClassKey.PhotoTaskProcessing),
		// String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN), mainbean.entry_id);
		ttp.initNoTitleBar(context, tab, 40);
		ttp.setTTPEnabled(mainbean);
	}
}