package oso.widget.loadbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.List;

import declare.com.vvme.R;
import oso.ui.load_receive.do_task.main.bean.Load_useBean;
import oso.ui.load_receive.do_task.main.iface.CheckInTaskInterface;
import oso.widget.orderlistview.FlowLayout;
import support.common.bean.CheckInTaskBeanMain;
import support.common.bean.CheckInTaskItemBeanMain;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: VLoadBarBox
 * @Description: 用于LoadBar的添加与修改
 * @date 2015-1-19 下午3:47:58
 */
public class VLoadBarBox extends LinearLayout implements OnClickListener {


    private FlowLayout load_bar;
    private ImageView add_loadbar;

    private boolean isChange_loadbar = false;        //true:修改,false:新增

    private Context context;

    private CheckInTaskBeanMain mainbean;

    public VLoadBarBox(Context context) {
        super(context);
    }

    public VLoadBarBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_inner();
    }

    // ==================public========================

    private String curTaskNo;

    /**
     * 修改seal后,会直接写至doorBean
     *
     * @param doorBean
     */
    public void init(CheckInTaskBeanMain doorBean, String curTaskNo) {
        this.mainbean = doorBean;
        this.curTaskNo = curTaskNo;
        addNameLayout(context, mainbean);
    }

    // =================================================

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.submit_seal__:

                break;

            default:
                break;
        }


    }

    /**
     * @param @param activity
     * @param @param names
     * @param @param vmiddle
     * @param @param mainbean
     * @Description:刷新loadbar、seal
     */
    public void addNameLayout(final Context activity, final CheckInTaskBeanMain mainbean) {

        //新建接口用于弹出loadbar添加修改的 控件
        final CheckInTaskInterface iface = new CheckInTaskInterface() {
            @Override
            public void selectItem(CheckInTaskItemBeanMain item) {
            }

            @Override
            public void selectItem(CheckInTaskItemBeanMain item, int position) {
            }

            @Override
            public void addLoadBatData(int load_bar_id, String total,boolean isChange_loadbar) {
                if (load_bar_id > 0) {
                    RequestParams params = new RequestParams();
                    params.add("Method", "AddLoadBarUse");
                    params.add("entry_id", mainbean.entry_id + "");
                    params.add("equipment_id", mainbean.equipment_id + "");
                    params.add("load_bar_id", load_bar_id + "");
                    params.add("count", total);
                    params.put("isUpdate ", isChange_loadbar ? "1" : "0");
                    // 改成LoadJson 数据的方式
                    new SimpleJSONUtil() {
                        @Override
                        public void handReponseJson(JSONObject json) {
                            List<Load_useBean> list = CheckInTaskBeanMain.parseLoad_use(json);
                            mainbean.load_UseList = list;
                            //刷新loadbar
                            addNameLayout(activity, mainbean);
                        }
                    }.doGet(HttpUrlPath.AndroidTaskProcessingAction, params, activity);
                }
            }

            @Override
            public void all_btn(CheckInTaskItemBeanMain item, int btn_type) {
            }

            @Override
            public void DetailByEntry(CheckInTaskBeanMain bean) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setnewnumberstates(int i) {
                // TODO Auto-generated method stub
            }
        };


        //------------弹出添加控件
        add_loadbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //校验已经关闭的单子不能添加loadbar
//					if(CheckInMainDocumentsStatusTypeKey.isEquipmentLeft(mainbean.equipment_status)){
//						CheckInMainDocumentsStatusTypeKey.ToashStr();
//					}else{
//                isChange_loadbar = false;
                LoadbarDialogWindow d = new LoadbarDialogWindow(activity, iface, mainbean.load_barsList, null
                        , mainbean.getFreightTerms(),mainbean.load_UseList, curTaskNo);
                d.show();
//					}
            }
        });

        load_bar.removeAllViews();
        //----------弹出修改控件
        if (!Utility.isNullForList(mainbean.load_UseList)) {
            for (int i = 0; i < mainbean.load_UseList.size(); i++) {
                final Load_useBean bean = mainbean.load_UseList.get(i);
                LinearLayout ly = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.load_user_layout, null);
                TextView tv = (TextView) ly.findViewById(R.id.load_user);
                tv.setText(bean.load_bar_name + ": " + bean.count);


                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //校验已经关闭的单子不能添加loadbar
//						if(CheckInMainDocumentsStatusTypeKey.isEquipmentLeft(mainbean.equipment_status)){
//							CheckInMainDocumentsStatusTypeKey.ToashStr();
//						}else{
//                        isChange_loadbar = true;
                        LoadbarDialogWindow d = new LoadbarDialogWindow(activity, iface, mainbean.load_barsList, bean,
                                mainbean.getFreightTerms(),mainbean.load_UseList, curTaskNo);
                        d.show();
//						}
                    }
                });

                load_bar.addView(ly);
            }
        }
    }

    private void init_inner() {
        context = getContext();
        LayoutInflater.from(context).inflate(R.layout.vloadbar_box_layout, this);

        load_bar = (FlowLayout) findViewById(R.id.load_bar);
        add_loadbar = (ImageView) findViewById(R.id.add_loadbar);
    }
}
