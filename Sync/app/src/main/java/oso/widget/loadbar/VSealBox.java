package oso.widget.loadbar;

import support.common.bean.CheckInTaskBeanMain;
import utility.StringUtil;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import declare.com.vvme.R;

/**
 * 用于输InSeal/OutSeal
 * 
 * @author 朱成
 * @date 2015-1-6
 */
public class VSealBox extends LinearLayout implements OnClickListener{

	private TextView in_seal;
	private TextView out_seal;
	
	private View in_ly;
	private View out_ly;
	
	private ImageView submit_seal;
	private View line;
	
	private Context context;
	
	private CheckInTaskBeanMain mainbean;

	public VSealBox(Context context) {
		super(context);
	}

	public VSealBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		init_inner();
	}

	// ==================public========================

	/**修改seal后,会直接写至doorBean
	 * @param doorBean
	 */
	public void init(CheckInTaskBeanMain doorBean) {
		this.mainbean=doorBean;

		showLine(mainbean);
		// 初始化
		in_seal.setText(StringUtil.isNullOfStr(doorBean.in_seal)?"NA":doorBean.in_seal);
		out_seal.setText(StringUtil.isNullOfStr(doorBean.out_seal)?"NA":doorBean.out_seal);
		in_ly.setVisibility(mainbean.hasInSeal()?View.VISIBLE:View.GONE);
		out_ly.setVisibility(mainbean.hasOutSeal()?View.VISIBLE:View.GONE);
	}

	// =================================================

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.submit_seal__:
			
			//debug
			if(mainbean==null)
				return;
//			/校验已经关闭的单子不能添加loadbar
//			if(CheckInMainDocumentsStatusTypeKey.isEquipmentLeft(mainbean.equipment_status)){
//				CheckInMainDocumentsStatusTypeKey.ToashStr();
//			}else{
				SealDialogWindow d = new SealDialogWindow(context,mainbean,in_seal,out_seal);
				d.show();
//			}
			break;

		default:
			break;
		}
		
		
	}
	
	/**
	 * @Description:显示分割线
	 * @param @param mainbean
	 */
	private void showLine(CheckInTaskBeanMain mainbean){
		if(!mainbean.hasInSeal()||!mainbean.hasOutSeal()){
			line.setVisibility(View.GONE);
		}
	}
	
	
	
	
	private void init_inner() {
		context=getContext();
		LayoutInflater.from(context).inflate(R.layout.vseal_box_layout, this);

		// 取view
		in_seal = (TextView) findViewById(R.id.in_seal);
		out_seal = (TextView) findViewById(R.id.out_seal);
		
		in_ly = (View) findViewById(R.id.in_ly);
		out_ly= (View) findViewById(R.id.out_ly);
		
		submit_seal=(ImageView) findViewById(R.id.submit_seal__);
		line = (View) findViewById(R.id.line);
		//监听事件
		submit_seal.setOnClickListener(this);
	}
}
