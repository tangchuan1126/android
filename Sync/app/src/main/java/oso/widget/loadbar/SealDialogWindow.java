package oso.widget.loadbar;

import org.json.JSONObject;

import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SealEditText;
import support.common.bean.CheckInTaskBeanMain;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 添加/修改-loadbar
 * 
 * @author 朱成
 * @date 2014-12-17
 */
public class SealDialogWindow implements OnClickListener {

	private Context context;
	private Dialog dialog;

	int scWidth = 480;
	int scHeight = 800;

	private CheckInTaskBeanMain mainbean;				//当前"修改"的类别(修改时才有)
	
	private SealEditText in_seal_ex;
	private SealEditText out_seal_ex;
	
	private TextView in_seal;
	private TextView out_seal;
	
	public SealDialogWindow(Context context,CheckInTaskBeanMain mainbean,TextView in_seal_ex,TextView out_seal_ex) {
		this.context = context;
		this.in_seal = in_seal_ex;
		this.out_seal = out_seal_ex;
		initDialog(context, mainbean);
	}
	
	

	private void initDialog(Context context, CheckInTaskBeanMain mainbean) {
		this.mainbean = mainbean;

		scWidth = getScreenWidth(context);
		scHeight = getScreenHeight(context);
		dialog = new Dialog(context, R.style.signDialog);
		
		Window window = dialog.getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		window.setWindowAnimations(R.style.signAnim); // 添加动画
		dialog.setContentView(R.layout.dock_checkin_task_dooritem_page2_dialog);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);

		in_seal_ex = (SealEditText) dialog.findViewById(R.id.in_seal_ex);
		out_seal_ex = (SealEditText) dialog.findViewById(R.id.out_seal_ex);
		
		in_seal_ex.initView(context.getString(R.string.sync_inseal), mainbean.in_seal, "NA");
		out_seal_ex.initView(context.getString(R.string.sync_outseal), mainbean.out_seal, "NA");
		
		in_seal_ex.setVisibility(mainbean.hasInSeal() ? View.VISIBLE : View.GONE);
		out_seal_ex.setVisibility(mainbean.hasOutSeal() ? View.VISIBLE : View.GONE);
		
		if(mainbean.hasInSeal())
			in_seal_ex.endEtCursor();
		else
			out_seal_ex.endEtCursor();
		
		//初始化
		in_seal_ex.setEnable(true);
		out_seal_ex.setEnable(true);
		
		(dialog.findViewById(R.id.submit)).setOnClickListener(this);
	}

	//加减loadbar时
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			dialogForSeal();
			break;

		default:
			break;
		}
	}

	/**
	 * @Description:提交seal信息
	 * @param @param activity
	 * @param @param mainbean
	 */
	public void dialogForSeal(){
		//--------获取seal数据
		final String inSeal=in_seal_ex.getText();
		final String outSeal=out_seal_ex.getText();
		
		String message = "";
		//--------判断seal是否为空
		if(mainbean.hasInSeal()){
			message += context.getString(R.string.sync_in_seal)+((StringUtil.isNullOfStr(inSeal))?"NA":inSeal);
		}
		if(mainbean.hasInSeal()&&mainbean.hasOutSeal()){
			message += "\n";
		}
		if(mainbean.hasOutSeal()){
			message += context.getString(R.string.sync_out_seal)+((StringUtil.isNullOfStr(outSeal))?"NA":outSeal);
		}
		
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(context.getString(R.string.sync_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//--------初始化提交数据
				submitData();
			}
		});
		builder.setNegativeButton(context.getString(R.string.sync_cancel), null);
		builder.create().show();
	}
	
	public void submitData(){
		//--------获取seal数据
		final String inSeal=StringUtil.isNullOfStr(in_seal_ex.getText())?"NA":in_seal_ex.getText();
		final String outSeal=StringUtil.isNullOfStr(out_seal_ex.getText())?"NA":out_seal_ex.getText();
		
		
		RequestParams params = new RequestParams();
		params.add("Method", "UpDateSeal");
		params.add("equipment_id", mainbean.equipment_id+"");
 		params.add("in_seal", inSeal);
 		params.add("out_seal", outSeal);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				mainbean.in_seal=inSeal;
				mainbean.out_seal=outSeal;
				in_seal.setText(inSeal);
				out_seal.setText(outSeal);
				close();
			}
		}.doPost(HttpUrlPath.AndroidTaskProcessingAction, params, context);
	}

	public void show() {
		// isShowing = true;
		dialog.show();
	}

	public void close() {
		if (dialog.isShowing()) {
			// isShowing = false;
			dialog.dismiss();
		}
	}

	private static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	private static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm;
	}

}
