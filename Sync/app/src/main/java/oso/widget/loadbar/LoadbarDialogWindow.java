package oso.widget.loadbar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.load_receive.do_task.main.bean.Load_barsBean;
import oso.ui.load_receive.do_task.main.bean.Load_useBean;
import oso.ui.load_receive.do_task.main.iface.CheckInTaskInterface;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import utility.Utility;

/**
 * @author gcy
 * @ClassName: LoadbarDialogWindow
 * @Description:
 * @date 2015-1-25 下午5:04:05
 */
public class LoadbarDialogWindow implements OnClickListener {

    private Context mContext;
    private Dialog dialog;

    private int flagNum = -1;// 判断弹出的pop窗体的性质 是添加数据还是修改数据 如果大于0则是修改数据
    // private EditText total;
    private Spinner loadbar_type;

    //	private Button btnMinus, btnPlus;
    private TextView tvBarCnt;

    int scWidth = 480;
    int scHeight = 800;

    private Load_useBean loadBean;                //当前"修改"的类别(修改时才有)
    private CheckInTaskInterface iface;

    private List<Load_barsBean> listBars;
    private List<Load_useBean> load_UseList;

    private ListView lvFreightTerm_dlg;
    private List<FreightTermBean> listFt;
    private String curTaskNo;

    private View loFt_dlg;

    public LoadbarDialogWindow(Context activity,
                               final CheckInTaskInterface iface, List<Load_barsBean> listBars,
                               final Load_useBean loadBean, List<FreightTermBean> listFreightTerms, List<Load_useBean> load_UseList, String curTaskNo) {
        mContext = activity;
//		this.isChange = isChange;
        this.listFt = listFreightTerms;
        this.curTaskNo = curTaskNo;
        initDialog(activity, iface, listBars, loadBean, load_UseList);
    }

    //true:修改loadbar,false:新增loadbar
    private boolean isChange() {
        return loadBean != null;
    }

    private void initDialog(Context activity,
                            final CheckInTaskInterface iface,
                            final List<Load_barsBean> listBars, final Load_useBean loadBean, final List<Load_useBean> load_UseList) {
        this.listBars = listBars;
        this.load_UseList = load_UseList;
        this.loadBean = loadBean;
        this.iface = iface;

        scWidth = getScreenWidth(mContext);
        scHeight = getScreenHeight(mContext);
        dialog = new Dialog(mContext, R.style.signDialog);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
        window.setWindowAnimations(R.style.signAnim); // 添加动画
        dialog.setContentView(R.layout.dock_checkin_task_dooritem_page1_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        //view
        lvFreightTerm_dlg = (ListView) dialog.findViewById(R.id.lvFreightTerm_dlg);
        loFt_dlg = dialog.findViewById(R.id.loFt_dlg);

        loadbar_type = (Spinner) dialog.findViewById(R.id.loadbar_type);
        View imgArrow = dialog.findViewById(R.id.imgArrow);
        // 若为修改
        if (isChange()) {
            loadbar_type.setEnabled(false);
            imgArrow.setVisibility(View.GONE);
        }

        Button submit = (Button) dialog.findViewById(R.id.submit);
        tvBarCnt = (TextView) dialog.findViewById(R.id.tvBarCnt);
        (dialog.findViewById(R.id.btnMinus)).setOnClickListener(this);
        (dialog.findViewById(R.id.btnPlus)).setOnClickListener(this);

        // --------------------------遍历loadbar_type 控件的显示数据
        final List<String> strList = new ArrayList<String>();
        if (!Utility.isNullForList(listBars)) {
            for (int i = 0; i < listBars.size(); i++) {
                strList.add(listBars.get(i).load_bar_name);
                if (loadBean != null
                        && loadBean.load_bar_id == listBars.get(i).load_bar_id) {
                    flagNum = i;
                }
            }
        } else {
            strList.add("No Data");
        }
        // ---------------------------New Adp
        ArrayAdapter loadbar_type_adapter = new ArrayAdapter(mContext,
                R.layout.simple_spinner_item, strList);
        // 设置下拉列表的风格
        loadbar_type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loadbar_type.setAdapter(loadbar_type_adapter);
        loadbar_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!Utility.isNullForList(load_UseList)&&!Utility.isNullForList(strList)){
                    for (int i = 0; i < load_UseList.size(); i++) {
                        if(load_UseList.get(i).load_bar_name.equals(strList.get(position))){
                            tvBarCnt.setText(load_UseList.get(i).count + "");
                            return;
                        }
                    }
                    tvBarCnt.setText("1");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // ------------------判断是修改数据还是添加数据
        if (flagNum > -1) {
            loadbar_type.setSelection(flagNum);
            tvBarCnt.setText(loadBean.count + "");
        }else {
            if(!Utility.isNullForList(load_UseList)&&!Utility.isNullForList(strList)){
                for (int i = 0; i < load_UseList.size(); i++) {
                    if(load_UseList.get(i).load_bar_name.equals(strList.get(0))){
                        tvBarCnt.setText(load_UseList.get(i).count + "");
                        break;
                    }
                }
            }else {
                loadbar_type.setSelection(0);
                tvBarCnt.setText(1 + "");
            }
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int exnum = getLoadBarCnt();
                // 修改时,提示删除
                if (isChange() && exnum == 0) {
                    showDlg_tipRemoveLoadbar(exnum);
                    return;
                }

                // --------------判断操作性质
                int load_bar_id = listBars.get(loadbar_type
                        .getSelectedItemPosition()).load_bar_id;

                boolean isAdd = true;
                if(!Utility.isNullForList(load_UseList)&&!Utility.isNullForList(strList)){
                    for (int i = 0; i < load_UseList.size(); i++) {
                        if(load_UseList.get(i).load_bar_name.equals(strList.get(loadbar_type.getSelectedItemPosition()))){
                            isAdd = false;
                            break;
                        }
                    }
                }

                // 新增
                if (isAdd) {
                    iface.addLoadBatData(load_bar_id, exnum + "", false);
                }
                // 修改
                else {
                    // int totalnum = exnum-loadBean.count;
                    iface.addLoadBatData(load_bar_id, exnum + "", true);
                }
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        //lv
        loFt_dlg.setVisibility(hasFreightTerm() ? View.VISIBLE : View.GONE);
        if (hasFreightTerm()) {
            lvFreightTerm_dlg.setAdapter(adpFreight);

            LayoutParams lp = lvFreightTerm_dlg.getLayoutParams();
            //最多显示5项
            if (listFt.size() <= 5)
                lp.height = LayoutParams.WRAP_CONTENT;
            else
                lp.height = Utility.dip2px(mContext, 170);
            lvFreightTerm_dlg.setLayoutParams(lp);

        }
    }

    private boolean hasFreightTerm() {
        return listFt != null && listFt.size() > 0;
    }

    //加减loadbar时
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int newCnt = 0;
        switch (v.getId()) {
            case R.id.btnPlus:
                newCnt = getLoadBarCnt() + 1;
                break;
            case R.id.btnMinus:
                newCnt = getLoadBarCnt() - 1;
                break;

            default:
                break;
        }

        //最大99
        if (newCnt > 99) {
            UIHelper.showToast(mContext,
                    "Must less than 99!",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        //修改时,最小为0
        if (isChange() && newCnt < 0) {
            UIHelper.showToast(mContext,
                    "Must greater than 0!",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        //新增时,最小为1
        if (!isChange() && newCnt < 1) {
            UIHelper.showToast(mContext,
                    "Must greater than 1!",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        tvBarCnt.setText(newCnt + "");
    }

    private int getLoadBarCnt() {
        return Integer.parseInt(tvBarCnt.getText().toString());
    }

    // 提示-移除loadbar
    private void showDlg_tipRemoveLoadbar(final int exnum) {
        RewriteBuilderDialog.showSimpleDialog(mContext, "Remove loadbar?",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        // TODO Auto-generated method stub
                        // --------------判断操作性质
                        int load_bar_id = listBars.get(loadbar_type
                                .getSelectedItemPosition()).load_bar_id;
                        // 新增
                        if (loadBean == null) {
                            iface.addLoadBatData(load_bar_id, exnum + "",false);
                        }
                        // 修改
                        else {
                            // int totalnum = exnum-loadBean.count;
                            iface.addLoadBatData(load_bar_id, exnum + "",true);
                        }
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });
    }

    public void show() {
        // isShowing = true;
        dialog.show();
    }

    public void close() {
        if (dialog.isShowing()) {
            // isShowing = false;
            dialog.dismiss();
        }
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics dm = getDisplayMetrics(context);
        return dm.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics dm = getDisplayMetrics(context);
        return dm.heightPixels;
    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(dm);
        return dm;
    }

    // ==================nested===============================

    private BaseAdapter adpFreight = new BaseAdapter() {

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder h;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.it_addpallet_freight, null);
                h = new Holder();
                h.tvTask_it = (TextView) convertView
                        .findViewById(R.id.tvTask_it);
                h.tvFreightTerm_it = (TextView) convertView
                        .findViewById(R.id.tvFreightTerm_it);
                h.imgArrow_dlg = convertView.findViewById(R.id.imgArrow_dlg);
                convertView.setTag(h);
            } else
                h = (Holder) convertView.getTag();

            //刷数据
            FreightTermBean b = listFt.get(position);
            h.tvTask_it.setText(b.taskType + ": " + b.taskNo);
            h.tvFreightTerm_it.setText("Freight Term: " + b.freightTerm);
            h.imgArrow_dlg.setVisibility(isCurTask(b.taskNo) ? View.VISIBLE : View.INVISIBLE);

            return convertView;
        }

        private boolean isCurTask(String taskNo) {
            return !TextUtils.isEmpty(taskNo) && taskNo.equals(curTaskNo);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listFt.size();
        }
    };

    private class Holder {
        TextView tvTask_it;
        TextView tvFreightTerm_it;
        View imgArrow_dlg;
    }
}
