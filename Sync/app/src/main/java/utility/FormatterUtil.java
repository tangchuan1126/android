package utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class FormatterUtil {
	/**
	 * 将返回的 username@domain 去掉"@"及后面的
	 * 
	 * @param username
	 * @return halfname
	 */
	public static String getHalfname(String username) {
		int index = username.lastIndexOf("@");
		if (index > 0) {
			return username.substring(0, index);
		} else {
			return username;
		}
	}

	/**
	 * 获得去除 最后面 /Smack xxx 得到完整的username For example: result =
	 * admin@192.168.1.15
	 * 
	 * @param unamePeer
	 * @return unamePeer
	 */
	public static String getWholeName(String unamePeer) {
		int atIndex = unamePeer.lastIndexOf("@");
		int index = unamePeer.lastIndexOf("/");
		if (atIndex > 0 && index > 0 && index > atIndex) {
			unamePeer = unamePeer.substring(0, index);
			return unamePeer;
		}
		return unamePeer;
	}
	
	/**
	 * 添加域
	 * @param uname
	 * @return
	 */
	public static String addDomain(String uname) {
		return uname + "@" + HttpUrlPath.chatServerUrl();
	}
	
	/**
	 * 获得格式化的时间
	 * @param createtime
	 * @return
	 */
	public static String getFormatTime(String createtime) {
		String month = createtime.substring(5, 7);
		String day = createtime.substring(8, 10);
		String hour = createtime.substring(11, 13);
		String minute = createtime.substring(14, 16);
		
		return month + "/" + day + " " + hour + ":" + minute;
	}
	
	/**
	 * 根据BaseUrl自动sub出聊天服务器ip
	 * @param url
	 * @return
	 */
	public static String getChatBaseUrl(String url) {
		int index = url.indexOf("http://");
		if(index == 0) {
			url = url.substring("http://".length());
		}
		int endIndex = 0;
		if(url.indexOf(":") > 0) {
			endIndex = url.indexOf(":");
		} else {
			endIndex = url.indexOf("/");
		}
		url = url.substring(0, endIndex);

		if(url.indexOf("192.168.88") != -1 || url.indexOf("192.168.89") != -1 ) {
			return "192.168.88.15";
		}

		return url;
	}
	
	
	/**
	 * 获得聊天记录的url
	 * @param chatServerUrl
	 * @param chatServerPort
	 * @return 
	 */
	public static String getChatRecordServerUrl(String chatServerUrl, int chatServerPort) {
		return "http://" + chatServerUrl + ":" + chatServerPort;
	}
	
	/**
	 * 获得UTC时间  0时区时间
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String getUTCTimeStr() {
        DateFormat dateFormatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        TimeZone pst = TimeZone.getTimeZone("Etc/GMT+0");  
  
        Date curDate = new Date();  
        dateFormatter.setTimeZone(pst);  
        String str=dateFormatter.format(curDate);
        return str;
	}
	
	/**
	 * 获得本地时间
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String getLocalTimeStr() {
		String FORMAT_TIME = "yyyy-MM-dd HH:mm:ss";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(FORMAT_TIME);
		return df.format(calendar.getTime());
	}
}
