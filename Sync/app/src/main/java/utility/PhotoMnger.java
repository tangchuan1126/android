package utility;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import oso.SyncApplication;
import support.common.UIHelper;
import support.dbhelper.Goable;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PointF;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import declare.com.vvme.R;

/**用于获取"相机/相册-图片"
 * @author 朱成
 * @date 2015-1-22
 */
public class PhotoMnger {
	
	public static final int REQ_FROM_GALLERY =466213;
	public static final int REQ_FROM_CAM = 466213+1;
	private static final int PHOTO_REQUEST_CAMERA = 1;// 拍照
	
	private Context ct=SyncApplication.getThis();
//	private Uri photoUri;
	
	private static final String PHOTO_FILE_NAME = new Date().getTime() + ".jpg";
	private static final String PHOTO_FILE_PATH = getPath(Goable.getDir_TmpImg().getAbsolutePath());
	private File tempFile=null;
	
	/**打开相机
	 * @param ac
	 */
	public void openCamera(Activity ac) {
		/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		ContentValues values = new ContentValues();
		photoUri = ac.getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
		ac.startActivityForResult(intent, REQ_FROM_CAM);*/
		
		
		
		//-------debug----------
		// 判断存储卡是否可以用，可用进行存储
		if (Utility.isSDExits()) {
			tempFile = getFile(PHOTO_FILE_PATH + "/" + PHOTO_FILE_NAME);
			
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);// 调用照相机
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
			ac.startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
		}
	}
	
	/**
	 * 打开相册
	 * @param ac
	 */
	public void openAlbum(Activity ac){
		Intent intent = new Intent(Intent.ACTION_PICK, Media.EXTERNAL_CONTENT_URI);
		ac.startActivityForResult(intent, REQ_FROM_GALLERY);
	}
	
//	public boolean hasPhoto(int requestCode, int resultCode){
//		if(resultCode==Activity.RESULT_CANCELED)
//			return false;
//		return requestCode==REQ_FROM_CAM||requestCode==REQ_FROM_GALLERY;
//	}
	
	public File onActivityResult(int requestCode, int resultCode, Intent data) {
		File ret=null;
		
		if(resultCode==Activity.RESULT_CANCELED)
			return null;
		
		switch (requestCode) {
		case REQ_FROM_GALLERY: // 相册
			Uri uri = data.getData();
			String imgToSend;

			// cursor用于获取uccam中图片(为"content://"故不能直接decode)
			// uri.getPath用于获取"存储设备"中图像(其用cursor会在cursor.moveToFirst时报错)
			ContentResolver cr = ct.getContentResolver();
			Cursor c = cr.query(uri, null, null, null, null);
			try {
				c.moveToFirst();
				imgToSend = c.getString(c.getColumnIndex("_data"));

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				imgToSend = uri.getPath();
			}

			// 如:伍哥手机"默认相册"直接返回路径时 c为null
			if (c != null)
				c.close();

			ret=new File(imgToSend);
			break;
		/*case REQ_FROM_CAM: // 相机
			// String img=data.getData().getPath();
			// data.getExtras().get("data"); //取bitmap
			ContentResolver cr1 = ct.getContentResolver();
			Cursor cursor = cr1.query(photoUri, null, null, null, null);
			cursor.moveToFirst();
			if (cursor != null) {
				String path = cursor.getString(1);
				cursor.close();
				ret=new File(path);
			}
			break;*/
			
			
			//-----debug
			case PHOTO_REQUEST_CAMERA: // 照相机选取   
			if (resultCode == Activity.RESULT_OK) {
				ret = tempFile;
			}
			break;
		}
		//压缩
		ret=Utility.scaleFile(ret, new PointF(600f,800f));
		
		if(!ret.exists()){
			UIHelper.showToast(ct.getString(R.string.sync_invalid_photo));
			return null;
		}
		return ret;
	}
	
	//==================inner===============================

	private static PhotoMnger instance;

	public static PhotoMnger getThis() {
		if (instance == null)
			instance = new PhotoMnger();

		return instance;
	}
	
	
	//==================nested===============================
	
	
	/**
	 * 创建文件夹
	 * 
	 * @param path
	 * @return
	 */
	private static String getPath(String path) {
		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 * @return
	 */
	private File getFile(String path) {
		File f = new File(path);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}
	
	
}
