package utility;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

/**
 * 多选-工具,用于联动"全选、单选-checkbox"
 * 
 * @author 朱成
 * @date 2015-3-18
 */
public abstract class MultiSelTool implements OnClickListener {

	private CheckBox cbAll;
	private BaseAdapter adp;
	
	/**
	 * 选中/去选-所有时
	 * 
	 * @param toSelect
	 */
	public abstract void onSelAll(boolean toSelect);
	
	/**
	 * 是否所有均已选中
	 * @return
	 */
	public abstract boolean isAllSel();
	
	//==================inner===============================

	public MultiSelTool() {

	}

	public void init(CheckBox cbAll, BaseAdapter adp) {
		this.cbAll = cbAll;
		this.adp=adp;
		
		this.cbAll.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == cbAll) {
			boolean toSelAll = is_btnSelAll_toSelectAll();
			onSelAll(toSelAll);
			adp.notifyDataSetChanged();
//			set_btnSelAll_toSelectAll(!toSelAll);
		}
	}

	// 将选择所有(选中时)
	private boolean is_btnSelAll_toSelectAll() {
		return cbAll.isChecked();
	}

	// // 打印-全选/全不选
	// private void selectAll_print(boolean toSelect) {
	// for (int i = 0; i < adp.getCount(); i++)
	// listPlts.get(i).isSelected = toSelect;
	// }

	private void set_btnSelAll_toSelectAll(boolean toSelectAll) {
//		cbAll.setChecked(!toSelectAll);
		// cbSelAll.setText(!toSelectAll ? "Deselect All" : "Select All");
		
		cbAll.setChecked(toSelectAll);
	}

	// 根据-选中状态,调整"全选/全不选"
	public void adjustPrintAll() {

//		int len = adp.getCount();
//		if (len == 0)
//			return;
//		int cntSelected = 0;
//		for (int i = 0; i < len; i++) {
//			if (listPlts.get(i).isSelected)
//				cntSelected++;
//		}

		// 若已全选 则显示"全不选"
//		set_btnSelAll_toSelectAll(cntSelected != len);
		set_btnSelAll_toSelectAll(isAllSel());
	}
	
	
	

}
