package utility;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import oso.SyncApplication;
import support.common.UIHelper;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.LocationManager;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.nostra13.universalimageloader.utils.L;

public class ActivityUtils {

	private static Boolean isExit = false;

	public static void exitSystem(Activity activity) {
		if (isExit == false) {
			isExit = true;
			UIHelper.showToast(activity, "再按一次退出程序", Toast.LENGTH_SHORT);
			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			}, 2000);
		} else {
			activity.finish();
		}
	}

	public static int mScreenWidth; // 屏幕宽度
	public static int mScreenHeight; // 屏幕高度

	public static void initScreen(Context context) {
		// WindowManager wm = (WindowManager)
		// _activity.getSystemService(Context.WINDOW_SERVICE);
		// DConstants.screenWidth = wm.getDefaultDisplay().getWidth(); //屏幕宽度
		// DConstants.screenHeight = wm.getDefaultDisplay().getHeight(); //屏幕高度
		// 获取屏幕宽度高度
		DisplayMetrics dm = getDisplayMetrics(context);
		mScreenWidth = dm.widthPixels;
		mScreenHeight = dm.heightPixels;
		L.i("屏幕宽高= " + mScreenWidth + " , " + mScreenHeight);
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		// ((Activity)
		// context).getWindowManager().getDefaultDisplay().getMetrics(dm);
		WindowManager wm = (WindowManager) SyncApplication.getThis().getSystemService(Activity.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(dm);
		return dm;
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	public static void hideSoftInput(Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(((Activity) context).getWindow().getDecorView().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
	 * 
	 * @param context
	 * @return true 表示开启
	 */
	public static void isOPenGps(Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		// 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
		boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
		boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		if (gps || network) {
			UIHelper.showToast(context, "GPS可用", Toast.LENGTH_SHORT).show();
		} else {
			UIHelper.showToast(context, "GPS已打开", Toast.LENGTH_SHORT).show();
			openGPS(context);
		}
	}

	/**
	 * 强制帮用户打开GPS
	 * 
	 * @param context
	 */
	public static final void openGPS(Context context) {
		Intent GPSIntent = new Intent();
		GPSIntent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
		GPSIntent.addCategory("android.intent.category.ALTERNATIVE");
		GPSIntent.setData(Uri.parse("custom:3"));
		try {
			PendingIntent.getBroadcast(context, 0, GPSIntent, 0).send();
		} catch (CanceledException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 检查指定apk是否已经安装
	 * 
	 * @param context
	 *            上下文
	 * @param packageName
	 *            apk包名
	 * @return
	 */
	public static boolean isAppInstalled(Context context, String packageName) {
		PackageManager pm = context.getPackageManager();
		boolean installed = false;
		try {
			pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			// 捕捉到异常,说明未安装
			installed = false;
		}
		return installed;
	}

	/**
	 * 安装APK
	 */
	public static void installApk(Context c, File file) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		c.startActivity(intent);
	}

	/**
	 * 打开已经安装好的apk
	 */
	public static void openApk(Context context, String url) {
		PackageManager manager = context.getPackageManager();
		// 这里的是你下载好的文件路径
		PackageInfo info = manager.getPackageArchiveInfo(url, PackageManager.GET_ACTIVITIES);
		if (info != null) {
			Intent intent = manager.getLaunchIntentForPackage(info.applicationInfo.packageName);
			context.startActivity(intent);
		}
	}

	/**
	 * 获取未安装apk的版本号
	 * 
	 * @param c
	 * @param archiveFilePath
	 * @return
	 */
	public static int getUninstallAPKVersionCode(Context c, String archiveFilePath) {
		PackageManager pm = c.getPackageManager();
		PackageInfo pakinfo = pm.getPackageArchiveInfo(archiveFilePath, PackageManager.GET_ACTIVITIES);
		if (pakinfo != null)
			return pakinfo.versionCode;
		return 0;
	}

	/**
	 * 获取已安装apk的版本号
	 * 
	 * @param c
	 * @param pak
	 * @return
	 */
	public static int getInstallAPKVersionCode(Context c, String pak) {
		int versionCode = 0;
		PackageManager pm = c.getPackageManager();
		PackageInfo pakinfo;
		try {
			pakinfo = pm.getPackageInfo(pak, PackageManager.GET_ACTIVITIES);
			if (pakinfo != null) {
				versionCode = pakinfo.versionCode;
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionCode;
	}

}
