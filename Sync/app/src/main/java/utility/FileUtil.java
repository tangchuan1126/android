package utility;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import support.dbhelper.Goable;
import support.exception.SystemException;
import android.content.Context;
import android.os.Environment;

public class FileUtil {

	private static String BasePath = Environment.getExternalStorageDirectory().getPath() + File.separator + "vvme";
	private static int buffer_size = 1024 * 50;
	public static String basedata = "basedata";
	static {
		File baseDir = new File(BasePath);
		if (!baseDir.exists()) {
			baseDir.mkdirs();
		}
	}

	public void saveFile(String path, InputStream inputStream, String fileName) throws IOException {
		File dir = new File(BasePath + File.separator + path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File outPutFile = new File(dir, fileName);
		outPutFile.createNewFile();
		byte[] buffer = new byte[buffer_size];
		FileOutputStream fileOutputStream = new FileOutputStream(outPutFile);
		int length = -1;
		while ((length = inputStream.read(buffer)) != -1) {
			fileOutputStream.write(buffer, 0, length);
		}
		inputStream.close();
		fileOutputStream.close();
	}

	public static void saveFileNoSdCard(String fileName, InputStream inputStream, Context context) throws Exception {
		FileOutputStream outPutStream = null;
		if (inputStream != null) {
			try {
				byte[] buffer = new byte[buffer_size];
				outPutStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
				int length = -1;
				while ((length = inputStream.read(buffer)) != -1) {
					outPutStream.write(buffer, 0, length);
				}
				outPutStream.close();
				inputStream.close();
			} catch (Exception e) {
				throw new SystemException("FileUtil.saveFileNoSdCard");
			} finally {
				inputStream.close();
				if (outPutStream != null) {
					outPutStream.close();
				}
			}
		}
	}

	public static InputStream readFromSD(String dir, String fileName) {
		File file = new File(BasePath + dir + File.separator + fileName);
		String path = file.getAbsolutePath();
		InputStream is = null;
		StringBuffer sb = new StringBuffer();
		try {
			File myile = new File(path);
			if (!myile.exists()) {
				return null;
			}
			FileInputStream fileIS = new FileInputStream(path);
			BufferedReader buf = new BufferedReader(new InputStreamReader(fileIS));

			String readString = new String();
			while ((readString = buf.readLine()) != null) {
				sb.append(readString);
			}
			is = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is;
	}

	public static void saveFileInSdCard(String fileName, InputStream inputStream, Context context, String path) throws Exception {
		FileOutputStream outPutStream = null;
		if (inputStream != null) {
			try {
				byte[] buffer = new byte[buffer_size];
				File dir = new File(path);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				File file = new File(dir, fileName);
				outPutStream = new FileOutputStream(file);
				int length = -1;
				while ((length = inputStream.read(buffer)) != -1) {
					outPutStream.write(buffer, 0, length);
				}
				outPutStream.close();
				inputStream.close();
			} catch (Exception e) {
				throw new SystemException("FileUtil.saveFileNoSdCard");
			} finally {
				inputStream.close();
				if (outPutStream != null) {
					outPutStream.close();
				}
			}
		}
	}

	// 添加文件在 zip中
	private static void putFileInZip(ZipOutputStream zipOutputStream, File file, String fileName) throws Exception {
		FileInputStream fileInputStream = null;
		try {
			ZipEntry entry = new ZipEntry(fileName);
			zipOutputStream.putNextEntry(entry);

			fileInputStream = new FileInputStream(file);
			byte[] buffer = new byte[1024 * 20];
			int length = -1;
			while ((length = fileInputStream.read(buffer)) != -1) {
				zipOutputStream.write(buffer, 0, length);
			}
		} catch (Exception e) {
			throw new SystemException(e);
		} finally {
			try {
				if (fileInputStream != null)
					fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static File createNewFile(String basePath, String fileName) throws IOException {
		File dirs = new File(basePath);
		if (!dirs.exists()) {
			dirs.mkdir();
		}
		File file = new File(dirs, fileName);
		file.createNewFile();
		return file;
	}

	/**
	 * 给定一个List文件然后生成一个Zip文件
	 * 
	 * @param fileName
	 * @param files
	 * @return
	 * @descrition
	 */
	public static File createZipFile(String fileName, List<File> files) {
		String parentDirPath = Goable.file_path;
		File parentDirs = new File(parentDirPath);
		if (!parentDirs.exists()) {
			parentDirs.mkdirs();
		}
		ZipOutputStream zipOutputStream = null;
		File zip = null;
		try {
			zip = new File(parentDirs, fileName + "_" + Goable.LoginAccount + ".zip");
			zip.createNewFile();
			zipOutputStream = new ZipOutputStream(new FileOutputStream(zip));
			// 文件 比如图片文件
			if (files != null && files.size() > 0) {
				for (File file : files) {
					if (file.exists()) {
						putFileInZip(zipOutputStream, file, file.getName());
					}
				}
			}
			return zip;
		} catch (Exception e) {
			throw new SystemException(e);
		} finally {
			try {
				if (zipOutputStream != null) {
					zipOutputStream.close();
				}
			} catch (Exception e) {
			}
		}

	}
}
