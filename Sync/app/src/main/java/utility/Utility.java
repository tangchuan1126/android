package utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.os.Vibrator;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.text.style.CharacterStyle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;

import declare.com.vvme.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oso.SyncApplication;
import oso.ui.MainActivity;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.edittext.SearchEditText;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.key.LanguageKey;
import support.receiver.AlarmReceiver;

public class Utility {

	// ==================多媒体===========================

	private static long[] vibrate_param = { 50, 100, 50, 200 };

	public static void vibrate() {
		Vibrator vibrator = (Vibrator) SyncApplication.getThis().getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(vibrate_param, -1);
	}

	// ==================图形处理==========================

	/**
	 * 缩放图片
	 * 
	 * @param file
	 * @param maxSize
	 * @return
	 */
	public static File scaleFile(File file, PointF maxSize) {
		Bitmap bm = loadBm_limitSize(file, maxSize);
		File tmp = saveTmpImage(bm);
		return tmp;
	}

	public static void scaleFile(File fileFrom, File fileTo, PointF maxSize) {
		Bitmap bm = loadBm_limitSize(fileFrom, maxSize);
		saveTmpImage(bm, fileTo);
	}

	// 保存-临时图片
	// 注:1.存为".jpg"
	// 2.dir为"目标目录",null时为"tmp"目录
	public static File saveTmpImage(Bitmap bm) {
		File dir = Goable.getDir_TmpImg();
		File fileToSave = null;
		try {
			fileToSave = new File(dir, new Date().getTime() + ".jpg");
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileToSave));
			bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileToSave;
	}

	public static void saveTmpImage(Bitmap bm, File file) {
		// File dir=Goable.getDir_TmpImg();
		File fileToSave = file;
		try {
			// fileToSave = new File(dir, new Date().getTime() + ".jpg");
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileToSave));
			bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// return fileToSave;
	}
	
	
	/**
	 * 拷贝文件
	 * @param srcFileName  源文件路径
	 * @param destFileName 目标文件路径
	 * @param overlay      如果目标文件存在  是否覆盖
	 * @return
	 */
	public static boolean copyFile(String srcFileName, String destFileName, boolean overlay) {  
        File srcFile = new File(srcFileName);  
  
        // 判断源文件是否存在  
        if (!srcFile.exists()) {  
            return false;  
        } else if (!srcFile.isFile()) {  
            return false;  
        }  
  
        // 判断目标文件是否存在  
        File destFile = new File(destFileName);  
        if (destFile.exists()) {  
            // 如果目标文件存在并允许覆盖  
            if (overlay) {  
                new File(destFileName).delete();  
            }  
        } else {  
            // 如果目标文件所在目录不存在，则创建目录  
            if (!destFile.getParentFile().exists()) {  
                if (!destFile.getParentFile().mkdirs()) {  
                    return false;  
                }  
            }  
        }  
  
        // 复制文件  
        int byteread = 0;
        InputStream in = null;  
        OutputStream out = null;  
  
        try {  
            in = new FileInputStream(srcFile);  
            out = new FileOutputStream(destFile);  
            byte[] buffer = new byte[1024];  
  
            while ((byteread = in.read(buffer)) != -1) {  
                out.write(buffer, 0, byteread);  
            }  
            return true;  
        } catch (FileNotFoundException e) {  
            return false;  
        } catch (IOException e) {  
            return false;  
        } finally {  
            try {  
                if (out != null)  
                    out.close();  
                if (in != null)  
                    in.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
    }  

	/**
	 * 加载-图片,限定-最大维度>>>注:1.缩放时"四舍五入" 故存在误差(或多或少)
	 * 
	 * @param path
	 * @param maxSize
	 * @return
	 */
	public static Bitmap loadBm_limitSize(File file, PointF maxSize) {
		String path = file.getPath();
		// 获取-旋转角
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		int rot = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
		int rotAngle = 0;
		switch (rot) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotAngle = 90;
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotAngle = 180;
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotAngle = 270;
			break;

		default:
			break;
		}

		// 限定-尺寸
		float wMax = maxSize.x;
		float hMax = maxSize.y;

		Options opt = new Options();
		opt.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opt);
		if (opt.mCancel || opt.outWidth == -1 || opt.outHeight == -1) {
			System.out.println("====w:" + opt.outWidth + ",h:" + opt.outHeight + ",cancel:" + opt.mCancel);
			return null;
		}

		float wIn = opt.outWidth;
		float hIn = opt.outHeight;

		// 若"横向-拍摄" 则调整"源尺寸",只为获取"缩放比例" 故调wIn、wMax均无所谓
		if (rotAngle == 90 || rotAngle == 270) {
			wIn = opt.outHeight;
			hIn = opt.outWidth;
		}

		float scaleIn = wIn / hIn;
		float scaleMax = wMax / hMax;

		// 若较宽 则限制宽
		opt.inJustDecodeBounds = false;
		if (scaleIn > scaleMax)
			opt.inSampleSize = Math.round(wIn / wMax); // "1/inSampleSize"表缩放比率
		else
			opt.inSampleSize = Math.round(hIn / hMax);

		// return BitmapFactory.decodeFile(path, opt);

		Bitmap bmScaled = BitmapFactory.decodeFile(path, opt);

		if (rotAngle == 0)
			return bmScaled;
		else
			return Utility.rotateBitmap(bmScaled, rotAngle);
	}

	/**
	 * 选转,正时针
	 * 
	 * @param bmpIn
	 * @param angle
	 * @return
	 */
	public static Bitmap rotateBitmap(Bitmap bmpIn, int angle) {
		Matrix mat = new Matrix();
		mat.postRotate(angle);
		return Bitmap.createBitmap(bmpIn, 0, 0, bmpIn.getWidth(), bmpIn.getHeight(), mat, true);
	}
	
	// ==================事件========================

	/**
	 * enter键弹起
	 * 
	 * @param event
	 * @return
	 */
	public static boolean isEnterUp(KeyEvent event) {
		return event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP;
	}

	// =================通知===========================

	public static int NotifyID_Task = 123; // notifyID
	public static int NotifyID_Chat = 124;

	/**
	 * 取消通知
	 * 
	 * @param id
	 *            取NotifyID_x
	 */
	public static void cancelNotify(int id) {
		SyncApplication app = SyncApplication.getThis();
		NotificationManager nm = (NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(id);
	}

	public static void cancelAllNotify() {
		SyncApplication app = SyncApplication.getThis();
		NotificationManager nm = (NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancelAll();
	}

	// ----------------系统属性----------------------

	/**
	 * 当前版本
	 * 
	 * @return
	 */
	public static String getAppVersion() {

		String versionName = "";
		Application app = SyncApplication.getThis();
		try {
			PackageManager pkgMng = app.getPackageManager();
			PackageInfo pkgInfo = pkgMng.getPackageInfo(app.getPackageName(), 0);
			versionName = pkgInfo.versionName;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return versionName;
	}

	/**
	 * app可见,考虑熄屏、锁屏
	 * 
	 * @return
	 */
	public static boolean isAppInView(Context ct) {
		PowerManager pw = (PowerManager) ct.getSystemService(Context.POWER_SERVICE);
		// 熄屏时
		if (!pw.isScreenOn())
			return false;

		KeyguardManager mKeyguardManager = (KeyguardManager) ct.getSystemService(Context.KEYGUARD_SERVICE);
		// 锁屏时
		if (mKeyguardManager.inKeyguardRestrictedInputMode())
			return false;

		// 当前显示app-非该app
		if (!isRunningInForeground(ct))
			return false;

		return true;
	}

	/**
	 * 该app是否在视野中,未考虑熄屏、锁屏
	 * 
	 * @param manager
	 * @return
	 */
	public static boolean isRunningInForeground(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		// 获得Sync的包名
		String currAppPackageName = context.getPackageName();
		List<RunningTaskInfo> tasks = manager.getRunningTasks(1);

		ComponentName topActivity = tasks.get(0).topActivity;
		return topActivity.getPackageName().equals(currAppPackageName);
	}

	private static String mac;

	public static String getMac() {
		if (TextUtils.isEmpty(mac)) {
			WifiManager wifi = (WifiManager) SyncApplication.getThis().getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = wifi.getConnectionInfo();
			mac = info.getMacAddress();
		}
		return mac;
	}

	// 是否在-主进程
	// 注:1>一个apk可能有多个进程,如:com.ishowtu.mfthd(主进程)、com.ishowtu.mfthd:remote
	public static boolean isInMainProcess(Context ct) {
		return ct.getPackageName().equals(getCurProcessName(ct));
	}

	// 获取-当前"进程名"
	public static String getCurProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {

				return appProcess.processName;
			}
		}
		return "";
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm;
	}

	// ---------------类型转换----------------------

	public static long parseLong(String str) {
		long ret = 0L;
		try {
			ret = Long.parseLong(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	public static boolean isInt(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static int parseInt(String str) {
		int ret = 0;
		try {
			ret = Integer.parseInt(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	// str转float,屏蔽了异常
	public static float parseFloat_noEx(String str) {
		float ret = 0f;
		try {
			ret = Float.parseFloat(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	// 序列化
	public static String serialize(Serializable obj) {
		String ret = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(obj);
			// 用utf8会转义部分byte,导致无法还原
			ret = new String(baos.toByteArray(), "ISO-8859-1");
		} catch (Exception e) {
			// TODO Auto-generated
			e.printStackTrace();
		}
		return ret;
	}

	// 反序列化
	public static Object unserialize(String str) {
		Object ret = null;
		if (TextUtils.isEmpty(str))
			return null;

		ByteArrayInputStream bais = null;
		try {
			bais = new ByteArrayInputStream(str.getBytes("ISO-8859-1"));
			ObjectInputStream bis = new ObjectInputStream(bais);
			ret = bis.readObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	// --------------文件处理----------------------

	// 重命名,若"新文件"存在 则不修改
	public static void renameFile(File file, String newName) {
		if (!file.exists())
			return;

		File newFile = new File(file.getParentFile(), newName);
		if (newFile.exists())
			return;
		file.renameTo(newFile);
	}

	// 重命名,若"新文件"存在 则不修改
	public static void renameFile(File file, File newFile) {
		if (!file.exists())
			return;

		if (newFile.exists())
			return;
		file.renameTo(newFile);
	}

	// 同getFileSize,单位为M 最多2位小数
	public static String getFileSize_M(File file) {
		float cacheSizeM = (float) Utility.getFileSize(file) / (float) (1024 * 1024);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		return df.format(cacheSizeM);
	}

	// 获取"文件/文件夹"的大小(字节),包含子文件夹
	public static long getFileSize(File file) {
		if (file == null || !file.exists())
			return 0;

		// 若为文件
		if (file.isFile())
			return file.length();

		long size = 0;
		File flist[] = file.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileSize(flist[i]);
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}

	// 判断-目录为空
	public static boolean isDirEmpty(File dir) {
		String[] files = dir.list();
		if (files != null && files.length > 0) {
			return false;
		}
		return true;
	}

	/**
	 * 删除文件、文件夹(含子文件)
	 * 
	 * @param path
	 */
	public static void delFile(String path) {
		delFolder(new File(path));
	}

	// 删除-文件夹(含子文件)、文件
	public static void delFolder(File file) {
		if (file.isFile()) {
			file.delete();
			return;
		}

		if (file.isDirectory()) {
			File[] childFiles = file.listFiles();
			// 若无子文件
			if (childFiles == null || childFiles.length == 0) {
				file.delete();
				return;
			}
			// 含子文件
			for (int i = 0; i < childFiles.length; i++) {
				delFolder(childFiles[i]);
			}
			file.delete();
		}
	}

	// -------------ac操作------------------------

	/**
	 * app已被干掉,如:1.crash后 2.退至后台被kill掉
	 * 
	 * @param ct
	 * @return
	 */
	public static boolean isAppKilled(Context ct) {
		ActivityManager am = (ActivityManager) ct.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> infos = am.getRunningTasks(100);

		for (int i = 0; i < infos.size(); i++) {
			RunningTaskInfo info = infos.get(i);
			// 若为当前包
			if (info.baseActivity.getPackageName().equals(ct.getPackageName())) {
				System.out.println("nimei>>baseAc_" + i + ":" + info.baseActivity);
				System.out.println("nimei>>cnt:" + info.numActivities + "_run:" + info.numRunning);
				// 若有ac被干掉,即使找到 继续下行(可能有多个task)
				if (info.numActivities != info.numRunning)
					return true;
			}
		}

		return false;
	}

	// 检查apk是否已安装
	public static boolean isApkInstalled(Context context, String packageName) {
		final PackageManager packageManager = context.getPackageManager();
		// 获取所有已安装程序的包信息
		List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
		for (int i = 0; i < pinfo.size(); i++) {
			if (pinfo.get(i).packageName.equalsIgnoreCase(packageName))
				return true;
		}
		return false;
	}

	// 跳至商店,以下载指定apk
	public static void downApk(Activity cw, String packageName) {
		// com.google.android.gsf
		Uri uri = Uri.parse("market://details?id=" + packageName);
		Intent it = new Intent(Intent.ACTION_VIEW, uri);
		cw.startActivity(it);
	}

	// 弹至某activity,如:主页
	public static void popTo(Context context, Class<?> main) {
		Intent intent = new Intent(context, main);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	public static void addLs_backHome_onLongclick(final Context ct, View btn) {
		OnLongClickListener onLongClick_btnBack = new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (!StoredData.isLogin())
					return false;

				String tip = ct.getString(R.string.back_to_home);
				RewriteBuilderDialog.showSimpleDialog(ct, tip,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Utility.popTo(ct, MainActivity.class);
							}
						});

				return false;
			}
		};

		btn.setOnLongClickListener(onLongClick_btnBack);
	}
	

	// ----------文本处理-------------------------------

	/**
	 * 连接list
	 * 
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static <E> List<E> combineList(List<E> list1, List<E> list2) {
		List<E> listAll = new ArrayList<E>();
		if (list1 != null)
			listAll.addAll(list1);
		if (list2 != null)
			listAll.addAll(list2);
		return listAll;
	}

	public static boolean isEmpty(List list) {
		return list == null || list.size() == 0;
	}
	
	public static boolean isEmpty(Object[] list) {
		return list == null || list.length == 0;
	}

	public static boolean isEmpty(JSONArray ja) {
		return ja == null || ja.length() == 0;
	}

	public static String read(InputStream instream) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = instream.read(buffer)) != -1) {
			bos.write(buffer, 0, len);
		}
		String ret = new String(bos.toByteArray());
		// System.out.println("---stat>>" + ret);
		return ret;
	}

	/**
	 * 获取-变换后的文本
	 * 
	 * @param et
	 * @return
	 */
	public static String getTransformedText(EditText et) {
		TransformationMethod tran = et.getTransformationMethod();
		CharSequence str = et.getEditableText().toString();
		if (tran != null)
			str = tran.getTransformation(str, et);
		return str.toString();
	}

	/**
	 * 添加span,style1添加到str1
	 * 
	 * @param spOld
	 *            待修改的spannable
	 * @param str0
	 * @param str1
	 * @param style1
	 * 
	 * @如 spOld为[str0][str1][str],style1添加至str1
	 */
	public static void addSpan(Spannable spOld, String str0, String str1, CharacterStyle style1) {
		spOld.setSpan(style1, str0.length(), str0.length() + str1.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
	}

	public static Spannable getSpannable(String str, CharacterStyle style) {
		SpannableString sp = new SpannableString(str);
		sp.setSpan(style, 0, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		return sp;
	}

	// 获取-复合文本,用于只有"两个子串"
	// 参数:style1(str1的样式)
	public static Spannable getCompoundText(String str0, String str1, CharacterStyle style1) {
		return getCompoundText(new String[] { str0, str1 }, new CharacterStyle[] { null, style1 });
	}

	/**
	 * 获取-复合文本,style1应用于str1
	 * 
	 * @param str0
	 * @param str1
	 * @param str2
	 * @param style1
	 * @return str0+str1+str2
	 */
	public static Spannable getCompoundText(String str0, String str1, String str2, CharacterStyle style1) {
		return getCompoundText(new String[] { str0, str1, str2 }, new CharacterStyle[] { null, style1, null });
	}

	// 获取-复合文本,两个数组维度需相同
	public static Spannable getCompoundText(String[] strs, CharacterStyle[] styles) {
		SpannableStringBuilder strTarget = new SpannableStringBuilder();
		int len = strs.length;
		for (int i = 0; i < len; i++) {
			SpannableString str = new SpannableString(strs[i]);
			// if(styles[i]!=null)
			str.setSpan(styles[i], 0, str.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			strTarget.append(str);
		}
		return strTarget;
	}

	public static int getMaxLen_ofString(float textSize, String[] strs) {
		return getMaxLen_ofString(textSize, strs, null);
	}

	/**
	 * 获取-最长str的长度
	 * 
	 * @param textSize
	 *            ,文字尺寸(px)
	 * @param strs
	 * @param addition
	 *            ,附加字符
	 * @return
	 */
	public static int getMaxLen_ofString(float textSize, String[] strs, String addition) {
		if (strs == null)
			return 0;

		if (addition == null)
			addition = "";

		Paint pt = new Paint();
		pt.setTextSize(textSize);
		float maxLen = 0;
		for (int i = 0; i < strs.length; i++) {
			float tmpLen = (float) pt.measureText(strs[i] + addition);
			if (tmpLen > maxLen)
				maxLen = tmpLen;
		}
		return (int) (maxLen + 5f);
	}

	// ==================lv===============================

	/**
	 * 展开/收缩-二级列表
	 * 
	 * @param lv
	 * @param toExpand
	 */
	public static void expandAll(ExpandableListView lv, boolean toExpand) {
		ListAdapter adp = lv.getAdapter();
		if (adp == null)
			return;

		int len = adp.getCount();
		for (int i = 0; i < len; i++) {
			// 展开
			if (toExpand)
				lv.expandGroup(i);
			// 收齐
			else
				lv.collapseGroup(i);
		}
	}

	/**
	 * 展开锁定到固定索引
	 * 
	 * @param lv
	 * @param toExpand
	 */
	public static void expandIndex(ExpandableListView lv,int index) {
		ListAdapter adp = lv.getAdapter();
		if (adp == null)
			return;

		int len = adp.getCount();
		for (int i = 0; i < len; i++) {
			if(index==i){
				lv.expandGroup(i);
			}else{
				lv.collapseGroup(i);
			}
		}
		lv.setSelection(index);
	}
	
	/**
	 * 切换exLv数据后 childView可能会多出几条,此方法可临时解决(收齐、选中第一项)
	 * 
	 * @param lv
	 */
	public static void resetExLv(ExpandableListView lv, BaseExpandableListAdapter adp) {
		if (adp == null)
			return;

		expandAll(lv, false);
		adp.notifyDataSetChanged();

		lv.setSelection(0);
	}

	// ==============ui操作============================

	private static Rect tmpRect = new Rect();

	/**
	 * 判断"点"是否在视图内
	 * 
	 * @param v
	 * @param x
	 *            ,y 全局点_____________如:MotionEvent.getRawX
	 * @return
	 */
	public static boolean isPointInView(View v, int x, int y) {
		v.getGlobalVisibleRect(tmpRect);

		// System.out.println("nimei>>["+x+","+y+"]"+",v>>["+tmpRect.left+","+tmpRect.top+"]");

		return tmpRect.contains(x, y);
	}

	public static boolean isPointInView(View v, MotionEvent e) {
		return isPointInView(v, (int) e.getRawX(), (int) e.getRawY());
	}

	// 移动edittext光标至末尾
	public static void endEtCursor(EditText et) {
		String str = et.getEditableText().toString();
		if (str == null)
			str = "";
		et.setSelection(str.length());
	}

	public static TextView getEmptyView_lv(Context ct, String text) {
		TextView tv = new TextView(ct);
		tv.setText(text);
		return tv;
	}

	/**
	 * 取et.value,若无 则取hint
	 * 
	 * @param et
	 * @return
	 */
	public static String getEt_valueOrHint(EditText et) {
		String v = et.getText().toString();
		if (TextUtils.isEmpty(v))
			v = et.getHint().toString();
		return v;
	}

	// -------------------------------------------

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()������������Ŀ
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // ��������View �Ŀ��
			totalHeight += listItem.getMeasuredHeight(); // ͳ������������ܸ߶�
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static boolean isConnectNet(Context context) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected())
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isSDExits() {
		boolean sdExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
		return sdExist;
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, View edit) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, EditText edit) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		// edit.setCursorVisible(false);//失去光标
		edit.requestFocus();
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, SearchEditText edit) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		// edit.setCursorVisible(false);//失去光标
		edit.requestFocus();
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * 显示软键盘
	 * 
	 * @param ct
	 * @param v
	 */
	public static void showSoftkeyboard(Context ct, View v) {
		InputMethodManager imm = (InputMethodManager) ct.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
	}

	public static String getVersionName(Context context) {
		String version = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			android.content.pm.PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			version = packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	} // 获取手机状态栏高度

	public static int getStatusBarHeight(Context context) {
		Class<?> c = null;
		Object obj = null;
		Field field = null;
		int x = 0, statusBarHeight = 0;
		try {
			c = Class.forName("com.android.internal.R$dimen");
			obj = c.newInstance();
			field = c.getField("status_bar_height");
			x = Integer.parseInt(field.get(obj).toString());
			statusBarHeight = context.getResources().getDimensionPixelSize(x);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return statusBarHeight;
	}

	/**
	 * @Description:判断List是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 * @param <E>
	 */
	public static <E> boolean isNullForList(List<E> list) {
		boolean flag = true;
		if (list != null && list.size() > 0) {
			flag = false;
		}
		return flag;
	}

	/**
	 * @Description:判断List是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 * @param <E>
	 */
	public static <E> boolean isNullForMap(Map<String, E> map) {
		boolean flag = true;
		if (map != null && map.size() > 0) {
			flag = false;
		}
		return flag;
	}

	// 获取登陆后的adid
	public static String getSharedPreferencesUrlValue(Activity context, String key) {
		String adid = "";
		if (!StringUtil.isNullOfStr(key)) {
			SharedPreferences sharedPreferences = context.getSharedPreferences("loginSave", Context.MODE_PRIVATE);
			adid = ("&" + key + "=" + sharedPreferences.getString(key, ""));
		}
		return adid;
	}

	// jiang
	public static int pxTodip(Context c, int px) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, c.getResources().getDisplayMetrics());
	}

	public static int pxTosp(Context c, int px) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, px, c.getResources().getDisplayMetrics());
	}

	private static long lastOneClickTime = 0;// click事件的基准时间

	/**
	 * @Description:根据所传时间判断click事件点击是否过快 false 为正常速度 true 为点击过快
	 * @param @param timeMillis 以毫秒为单位的时间
	 * @param @return
	 */
	public static boolean isFastClick(int timeMillis) {
		long time = System.currentTimeMillis();
		long timeD = time - lastOneClickTime;
		lastOneClickTime = time;
		if (timeD > timeMillis) {
			return false;
		}
		return true;
	}

	public static boolean isFastClick() {
		return isFastClick(1000);
	}

	/**
	 * 将px值转换为sp值，保证文字大小不变
	 * 
	 * @param pxValue
	 * @param fontScale
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int px2sp(Context context, float pxValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (pxValue / fontScale + 0.5f);
	}

	/**
	 * 将sp值转换为px值，保证文字大小不变
	 * 
	 * @param spValue
	 * @param fontScale
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int sp2px(Context context, float spValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (spValue * fontScale + 0.5f);
	}

	@SuppressWarnings("deprecation")
	public static void AcquireWakeLock(Context ctx) {
		PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);

		WakeLock m_wakeObj = (WakeLock) pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.ON_AFTER_RELEASE, "");

		// 点亮屏幕15秒钟
		m_wakeObj.acquire(1000 * 30);
		m_wakeObj.release();// 释放资源

		// 解锁屏幕
		// 屏幕解锁
		KeyguardManager keyguardManager = (KeyguardManager) ctx.getSystemService(Context.KEYGUARD_SERVICE);
		KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("");
		keyguardLock.disableKeyguard();
	}

	/**
	 * 开启全局定时器
	 * 
	 * @param ctx
	 * @param minutes
	 */
	public static void startAlarm(Context ctx, int minutes) {
		Intent intent = new Intent(ctx, AlarmReceiver.class);
		intent.setAction("repeating");
		PendingIntent sender = PendingIntent.getBroadcast(ctx, 0, intent, 0);
		// 开始时间
		long firstime = SystemClock.elapsedRealtime();
		AlarmManager am = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstime, minutes * 1000 * 60, sender);
	}

	// 取消全局定时
	public static void cancelAlarm(Context ctx) {
		Intent intent = new Intent(ctx, AlarmReceiver.class);
		intent.setAction("repeating");
		PendingIntent sender = PendingIntent.getBroadcast(ctx, 0, intent, 0);
		AlarmManager alarm = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(sender);
	}

	/**
	 * 检测录音权限
	 * 
	 * @param c
	 * @return
	 */
	public static boolean isAudioPermissions(Context c, File audio) {
		// PackageManager pm = c.getPackageManager();
		// boolean permission = (PackageManager.PERMISSION_GRANTED ==
		// pm.checkPermission("android.permission.RECORD_AUDIO",
		// "declare.com.vvme"));
		// if (permission) {
		// return true;
		// } else {
		// RewriteBuilderDialog.showSimpleDialog_Tip(c,
		// c.getString(R.string.str_audio_permissions_error));
		// return false;
		// }
		MediaPlayer mp = MediaPlayer.create(SyncApplication.getThis(), Uri.fromFile(audio));
		return mp != null;
	}
	
	/**
	 * 查看账号信息保存设置
	 * @param l
	 */
	public static void updateSaveInfo(boolean saveinfo) {
		 StoredData.saveinfo = saveinfo;
	}
	/**
	 * 修改语言配置
	 * @param c
	 */
	public static void updateConfigurationLanguage(Context c) {
        Configuration config = c.getResources().getConfiguration();
        if (StoredData.readIsFirstSelectLanguage()) {
            StoredData.saveLanguageName(LanguageKey.getLanguageID(config.locale));
            c.getResources().updateConfiguration(config, null);
        } else {
            config.locale = StoredData.readLanguageID() == StoredData.LA_CN ? Locale.SIMPLIFIED_CHINESE : Locale.ENGLISH;
            c.getResources().updateConfiguration(config, null);
        }
	}
	
	/**
	 * 同步一下cookie
	 */
	public static void synCookies(Context context, String url, String cookies) {
//		CookieSyncManager.createInstance(context);
//		CookieManager cookieManager = CookieManager.getInstance();
//		cookieManager.setAcceptCookie(true);
//		cookieManager.removeSessionCookie();//移除
//		cookieManager.setCookie(url, cookies);//cookies是在HttpClient中获得的cookie
//		CookieSyncManager.getInstance().sync();
	}

	/**
	 * EditText获取焦点，焦点移动到最后
	 * @param et
	 * @param moveToLast
	 */
	public static void etRequestFocus(EditText et, boolean moveToLast) {
		if(et == null) return;
		et.requestFocus();

		if(!moveToLast) return;
		int cursorIndex = et.getText().toString().length();
		et.setSelection(cursorIndex);
	}

	/**
	 * byte(字节)根据长度转成kb(千字节)和mb(兆字节)
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytes2kb(long bytes) {
		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal megabyte = new BigDecimal(1024 * 1024);
		float returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP)
				.floatValue();
		if (returnValue > 1)
			return (returnValue + "MB");
		BigDecimal kilobyte = new BigDecimal(1024);
		returnValue = filesize.divide(kilobyte, 2, BigDecimal.ROUND_UP)
				.floatValue();
		return (returnValue + "KB");
	}

	/**
	 * 灰度Bitmap   彩色转黑白
	 * @param image
	 * @return
	 */
	public static Bitmap getGrayBitmap(final Bitmap image) {
		// 创建新的Bitmap用来保存最终结果
		Bitmap grayscalBitmap = Bitmap.createBitmap(image.getWidth(),
				image.getHeight(), Bitmap.Config.RGB_565);

		Canvas canvas = new Canvas(grayscalBitmap);
		Paint paint = new Paint();
		ColorMatrix matrix = new ColorMatrix();
		matrix.setSaturation(0); // 饱和度
		ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
		paint.setColorFilter(filter);
		canvas.drawBitmap(image, 0, 0, paint);
		return grayscalBitmap;
	}

	/**
	 * 给Layout设置壁纸背景
	 * @param context
	 * @param layout
	 */
//	public static void setBgFromWallpaper(Context context, ViewGroup layout) {
//		WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
//		Drawable wallpaperDrawable = wallpaperManager.getDrawable();
//		if(wallpaperDrawable == null) {
//			return;
//		}
//		layout.setBackground(wallpaperDrawable);
//	}


	/**
	 * 判断屏幕是否点亮[需要注意 在锁屏界面 返回true]
	 * @param context
	 * @return true : a、屏幕是黑的 | b、目前正处于解锁状态
	 * 		   false: 表示目前未锁屏
	 */
	public static boolean isScreenOn(Context context) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		return pm.isScreenOn();
	}


	/**
	 * 设置EditText的整数与小数点的过滤
	 * @param number 整数长度
	 * @param decimal 小数长度
	 * @return
	 */
	public static InputFilter[] setNumberDecimal(int number,int decimal){
		number = (number<=0)?0:number;
		decimal = (decimal<=0)?0:decimal;

		final int number_ = number;
		final int decimal_ = decimal;

		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
									   Spanned dest, int dstart, int dend) {
				String so = source.toString();
				String de = dest.toString();
				if("".equals(so)|| end==0){
					de = de.substring(0, dstart)+de.substring(dend, de.length());
				}else{
					de = de.substring(0, dstart)+so+de.substring(dend, de.length());
				}
				System.out.println(de);

				Pattern pattern = Pattern.compile("^\\d{0," + number_ + "}\\.?(?:\\.\\d{0," + decimal_ +"})?$");
				Matcher matcher = pattern.matcher(de);
				if(matcher.find()){
					return null;
				}else{
					return "";
				}
			}
		};
		return filters;
	}
}
