package utility;

import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import support.dbhelper.StoredData;

public class HttpUrlPath {


	/**
	 * ================================================================
	 * <p/>
	 * 初始化URL
	 * <p/>
	 * ================================================================
	 */
	public static void initBaseUrls() {
		String baseServerName = StoredData.readBaseServerName();
		basePath = serverUrls.get(baseServerName);

		//--如果为空 则为用户自定义的服务器地址
		if(TextUtils.isEmpty(basePath)) {
			Map<String, HashSet<String>> allExtServers = StoredData.getExtServers();
			HashSet<String> urlSet = allExtServers.get(baseServerName);
			basePath = urlSet.iterator().next();
		}

		Log.i("BaseUrl", HttpUrlPath.basePath);
		initUrls();
	}

	public static void initUrls() {
		WebServerFileBasePath = basePath + "upload/";
		WEBServerProductUrl = basePath + "upload/product/";
		WEBServerCheckInUrl = basePath + "upload/check_in/";
		WEBServerProductSmallUrl = basePath + "upload/thumbnail/";

		getTransportAction = basePath + "action/android/GetTransportAction.action";
		getTransportOutboundJsons = basePath + "action/android/GetTransportOutboundAction.action";
		getStreamSerialProduct = basePath + "action/android/GetStreamSerialProductAction.action";
		getStreamLpInfoAction = basePath + "action/android/GetStreamLpInfoAction.action";
		postReceiveInfoAction = basePath + "action/android/PostReceiveInfoAction.action";
		getZipLpBaseInfoAction = basePath + "action/bcs/BCSDownContainerProduct.action";
		getXMLTransportCheckInOver = basePath + "action/android/TransportCheckInOverAction.action";
		submitLpProductRelationAction = basePath + "action/bcs/BCSLicensePlate.action";

		receiveInfoSubmit = basePath + "action/bcs/BCSReceiveProduct.action";
		getPositionStreamSerialProduct = basePath + "action/android/GetPositionStreamSerialProductAction.action";
		positionAction = basePath + "action/bcs/BCSPutProduct.action";

		seachLoginUser = basePath + "action/android/AndroidLoginUserAction.action";
		seachProductLine = basePath + "action/android/AndroidSeachProductLineAction.action";
		seachProductByType = basePath + "action/android/AndroidSeachProductAction.action";
		BCSOutbound = basePath + "action/bcs/BCSOutbound.action";
		PickUp = basePath + "action/bcs/BCSOutBill.action";
		PickUpBCSOutBill = basePath + "action/LoadPickUpBaseData.action";

		InventoryLoadBaseInfo = basePath + "action/bcsDownLoadProductLocation.action";
		InventoryAction = basePath + "action/bcs/BCSProductTakeStock.action";
		InventoryActionNew = basePath + "action/android/InventoryAction.action";
		InventoryLoadBaseInfo_new = basePath + "action/android/InventoryBaseDataAction.action";

		productBaseDataXMLAction = basePath + "action/productBaseData/productBaseDataXMLAction.action";
		getProductDetailAction = basePath + "action/productBaseData/GetProductDetailXMLAction.action";
		getProductUnionAction = basePath + "action/productBaseData/GetProductUnionXMLAction.action";
		getProductCodeAction = basePath + "action/productBaseData/GetProductCodeXMLAction.action";
		getProductByIdAction = basePath + "action/android/AndroidSeachProductByIdAction.action";

		getProductClassify = basePath + "action/productBaseData/GetProductCatalogXMLAction.action";
		updateProductCatalog = basePath + "action/productBaseData/productBaseDataXMLAction.action";
		getProductFile = basePath + "action/productBaseData/GetProductPictureXMLAction.action";
		postFiles = basePath + "action/android/UpPictureFileAction.action";
		postProductFiles = basePath + "action/productBaseData/UploadPictrueProductFileXMLAction.action";

		getStorageCatalogAction = basePath + "action/storage_catalog/GetStroageByTypeXMLAction.action";
		getCountryAction = basePath + "action/storage_catalog/GetAllCountryCodeXMLAction.action";
		getProvinceAction = basePath + "action/storage_catalog/GetProvinceByCcidXMLAction.action";

		getTransportResourceAction = basePath + "action/transport/GetFreightResourcesByCompanyXMLAction.action";
		getAllFreightItemAction = basePath + "action/transport/GetAllFreightCostAction.action";
		// 保存交货单
		submitDataByTabIdAction = basePath + "action/transport/AddPurchaseTransportByXMLAction.action";
		// 通过单据类型获取流程
		getProduresByOrderTypeXMLAction = basePath + "action/transport/GetProduresByOrderTypeXMLAction.action";
		// 通过流程ID获取阶段
		getProduresDetailsByProduresIdDValsXMLAction = basePath + "action/transport/GetProduresDetailsByProduresIdDValsXMLAction.action";
		// 通过流程ID获取通知
		getProduresNoticesByProduresIdXMLAction = basePath + "action/transport/GetProduresNoticesByProduresIdXMLAction.action";
		// 获取users
		getAllUserAndDeptXMLAction = basePath + "action/transport/GetAllUserAndDeptXMLAction.action";
		// 通过ID获取转运单
		getDetailTransportByIdXMLAction = basePath + "action/transport/GetDetailTransportByIdXMLAction.action";

		GetProductInfoByStorageIdXMLAction = basePath + "action/storage_catalog/GetProductInfoByStorageIdXMLAction.action";
		SaveOrUpdateTransportItemsXMLAction = basePath + "action/transport/SaveOrUpdateTransportItemsXMLAction.action";
		LoadApkVersion = basePath + "action/android/UpdateAndroidVersionAction.action";
		LoadAndroidScreenApkAction = basePath + "action/android/LoadAndroidScreenApkAction.action";
		LoadApk = basePath + "action/warehouse.action";
		GetFileByTypeAction = basePath + "action/file/GetFileByTypeAction.action";
		GetTransportRelatedInfoXmlAction = basePath + "action/transport/GetTransportRelatedInfoXmlAction.action";
		LoadProductAndPicture = basePath + "action/android/GetThumbnailAction.action";
		LoadBarCodeInfoByBarCode = basePath + "action/storage_catalog/GetBarCodesByAndroidScanBarCodeAction.action";
		LoadOneProductThumbnailByPcid = basePath + "action/android/LoadOneProductAllThumbnailByPcidAction.action";
		loadFile = basePath + "action/android/LoadFileThumbnailAction.action";
		clearDoor = basePath + "action/android/ClearDoorAndLocationAction.action";
		GetTransportsByRequireAction = basePath + "action/transport/GetTransportsByRequireAction.action";
		GetDoorCanChooseOrAllByStorageToXMLAction = basePath + "action/product_catalog/GetDoorCanChooseOrAllByStorageToXMLAction.action";
		GetLocationCanChooseOrAllByStorageToXMLAction = basePath + "action/product_catalog/GetLocationCanChooseOrAllByStorageToXMLAction.action";
		AddDoorOrLocationUseInfoXmlAction = basePath + "action/storage_catalog/AddDoorOrLocationUseInfoXmlAction.action";
		UpdateTransportToTransitAndReleaseOccXmlActon = basePath + "action/transport/UpdateTransportToTransitAndReleaseOccXmlActon.action";
		AddNewLpInfoAction = basePath + "action/android/AddNewLpInfo.action";
		AndroidLoadFile = basePath + "action/android/AndroidLoadFileAction.action"; // android请求文件

		productBaseDataZipAction = basePath + "action/android/AddBaseDataProductAction.action";
		Container = basePath + "action/android/ContainerAction.action";
		SubmitContainer = basePath + "action/android/SubmitContainerAction.action";
		AppendContainer = basePath + "action/android/ContainerAppendProductOrContainerAction.action";

		BaseProductAction = basePath + "action/android/BaseProductAction.action";
		TransprortAction = basePath + "action/android/TransportAction.action";
		loadProductMainPicture = basePath + "action/android/LoadProductMainPictrueAction.action";
		AndroidDockCloseAction = basePath + "action/checkin/AndroidDockCloseAction.action";
		CheckInAction = basePath + "action/checkin/CheckInAction.action";
		CheckInActionMutiRequest = basePath + "action/checkin/CheckInMutiRequestAction.action";
		CheckInPatrol = basePath + "action/checkin/CheckInPatrolAction.action";
		againPatrolAll = basePath + "action/checkin/againPatrolAll.action";

		ProprietaryFindByAdidXmlAction = basePath + "action/proprietary/ProprietaryFindByAdidXmlAction.action";
		AndroidSubmitContainerAndChildAction = basePath + "action/android/AndroidSubmitContainerAndChildAction.action";
		AndroidGetTransportProductAction = basePath + "action/android/AndroidGetTransportProductAction.action";
		SysUserAction = basePath + "action/AndroidSysBaseDataAction.action";// 获取人员信息

		FileUpZipAction = basePath + "action/android/AndroidUpZipFileAction.action"; // 上传zip文件里面包含图片
		AndroidPrintByWebAction = basePath + "action/checkin/AndroidPrintByWebAction.action";
		AndroidAutoAction = basePath + "action/android/AndroidNumberAutoCompleteAction.action";
		LoadPaperWorkAction = basePath + "action/android/LoadPaperWorkAction.action";
		androidSmallParcel=basePath+ "action/android/androidSmallParcel.action";
		ConsolidateAction = basePath + "action/android/LoadPaperWorkAction.action"; // "action/android/ConsolidateAction.action";
		ShippingAndPlateAction = basePath + "action/android/ShippingAndPlateAction.action";
		// 预览-打印
		Load_PrintLabel = basePath + "check_in/a4print_packing_list.html";
		ReceiveWorkAction = basePath + "action/android/ReceiveWorkAction.action";
		AndroidSignatureFileUpAction = basePath + "action/android/AndroidSignatureFileUpAction.action";
		// 大屏幕action
		CheckInWareHouseBigScreenAction = basePath + "action/checkin/CheckInWareHouseBigScreenAction.action";
		// dockcheckin
		AndroidDockCheckInAction = basePath + "action/checkin/AndroidDockCheckInAction.action";
		AndroidAssignTaskAction = basePath + "action/checkin/AndroidAssignTaskAction.action";
		// gcm==========================
		GCMAction = basePath + "action/android/PushMessageByGCMAction.action";
		PushMessageByGCMAction = basePath + "action/android/PushMessageByGCMAction.action";
		AndroidTaskProcessingAction = basePath + "action/checkin/AndroidTaskProcessingAction.action";

		AndroidWindowCheckInAction = basePath + "action/checkin/AndroidWindowCheckInAction.action";

		AndroidShuttleAction = basePath + "action/checkin/AndroidShuttleAction.action";

		// ------盘点
		TaskInstanceListByUser = basePath + "_inventoryControl/cycleCount/search/taskInstanceListByUser";// List
																											// of
																											// Task
																											// for
																											// a
																											// User
		LocationData = basePath + "_inventoryControl/cycleCount/search/locationData";// Get
																						// Data
																						// for

																			// Location
		StartTask = basePath + "_inventoryControl/cycleCount/execute/startTask";// Start
																		// task
		TaskInstanceAreas = basePath + "_inventoryControl/cycleCount/search/taskInstanceAreas";// Fetch
																								// Areas
																								// in
																								// Task
		TaskInstanceAreaLocations = basePath + "_inventoryControl/cycleCount/search/taskInstanceAreaLocations";// Fetch
																												// Locations
																												// of
																												// area
																												// in
																												// Task;

		AssignToMe = basePath + "_inventoryControl/execute/assignToMe";// Assign
																		// to me
		SubmitLocationScan = basePath + "_inventoryControl/cycleCount/execute/submitLocationScan";// Submit
																									// location
																									// Data

		AssignToMe = basePath + "_inventoryControl/execute/assignToMe";// Assign
																		// to me

		CCT_uploadPhoto = basePath + "_inventoryControl/cycleCount/upload/android/uploadPhoto";

		CCT_delUploadPhotos = basePath + "_inventoryControl/cycleCount/upload/android/deletePhoto";
		// -------------盲盘
		CCT_blindRequestProductBySn = basePath + "_inventoryControl/cycleCount/search/productBySn";
		CCT_blindRequestProductByProduct = basePath + "_inventoryControl/cycleCount/search/productByProductCode";
		CCT_blindRequestCLP = basePath + "_inventoryControl/cycleCount/search/containerByBarcode";
		CCT_RequestSKU = basePath + "_inventoryControl/cycleCount/search/productBySku";
		CCT_CheckSnList = basePath + "_inventoryControl/cycleCount/search/checkSnList";

		CCT_CreatePackaging = basePath + "basicdata/packaging/add";
		CCT_GetCreateLpDataInfo = basePath + "basicdata/getCreateLpDataInfo/";
		CCT_AddLP_Type = basePath + "basicdata/lp/add";
		CCT_TLP_Context = basePath + "basicdata/tlpcontext";
		CCT_CLP_Context = basePath + "basicdata/clpcontext";
		CCT_CLP_TypeAdd = basePath + "basicdata/clptypeadd";

		/**
		 * GIS 地图接口
		 */
		getStorageBaseData = basePath + "_gis/storagesCotroller/getStorageBaseData";
		queryAreaByPsId = basePath + "_gis/areaInfoCotroller/queryAreaByPsIdAndroid";
		queryWebcamAndroid = basePath + "_gis/storageCotroller/queryWebcamAndroid";
		queryDoorYardAndroid = basePath + "_gis/dockInfoCotroller/queryDoorYardAndroid";
		getDocksParkingOccupancyDetail = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancyDetail";
		getDocksParkingOccupancyDetailAndroid = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancyDetailAndroid";
		getDocksParkingOccupancy = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancy";
		storageDatas = basePath + "_gis/androidCotroller/storageDatas";

		// 微服务
		UploadFile = basePath + "_fileserv_chat/upload";
		DownFile = basePath + "_fileserv_chat/download/"; // 后面直接接"文件ID"

		/**
		 * 聊天历史记录接口
		 */
		// ChatServerUrl = FormatterUtil.getChatBaseUrl(basePath);
		// ChatRecordUrl = FormatterUtil.getChatRecordServerUrl(ChatServerUrl,
		// ChatServerPort) +
		// "/plugins/chatRecord/chatrecordservlet?action=query!record";
		// debug
		// GetAccountInfoAction = basePath
		// + "action/checkin/GetAccountInfoAction.action";
		GetAccountInfoAction = basePath + "action/account/GetAccountInfoAction.action";
		// 个人信息
		accountUpdateInfoUrl = basePath + "action/administrator/proprietary/AndroidAccountMgrAction.action";
		accountUpdateAvatarMainUrl = basePath + "action/administrator/proprietary/AndroidAccountPhotoMgrAction.action";
		accountDeleteAvatarUrl = basePath + "action/administrator/proprietary/AndroidAccountPhotoMgrAction.action";
		accountAddAvatarUrl = basePath + "action/administrator/proprietary/AndroidAccountPhotoMgrAction.action";

		// Receive
		acquireRNLineInfo = basePath + "_receive/android/acquireRNLineInfo";
		acquireRNLineInfoByReceiptID = basePath + "_receive/android/acquireRNLineInfoByReceiptID";
		bindQTYToPallet = basePath + "_receive/android/bindQTYToPallet";
		acquirePalletsInfoByLine = basePath + "_receive/android/acquirePalletsInfoByLine";

		//cc
		createContainerForCC = basePath + "_receive/android/createContainerForCC";

		// PutWay
		logMovement = basePath + "_receive/android/logMovement";
		acquireMovementByPalletId = basePath + "_receive/android/acquireMovementByPalletId";

		// ---TMS
		GetRouteList=basePath + "_tms/anload/getAllRouteByCurrent";
		GetOrderByTN=basePath + "_tms/anload/scanNo";
		TMSLoadtn=basePath + "_tms/anload/load";
		TMSDeleLoadTn=basePath + "_tms/anload/deleLoad";
		GetOrderAndShippto=basePath + "_tms/anload/getAllOrderWeight";
		getAnnengLoadingOrders = basePath + "_tms/anneng/getAnnengLoadingOrders";
		addAmmemgReceoveorders = basePath + "_tms/anneng/addAmmemgReceoveorders";
		rollbackReceive = basePath + "_tms/anneng/rollbackReceive";
		addAmmengOrderAndRefresh = basePath + "_tms/anneng/addAmmengOrderAndRefresh";
		getWaybillByWaybillItemId = basePath + "_tms/anneng/getWaybillByWaybillItemId";
		updateWaybillAndWaybillItems = basePath + "_tms/anneng/updateWaybillAndWaybillItems";

		getAnnengInventoryInfo = basePath + "_tms/route/getRouteLineByPsId";
		getAnnengBranchInventory = basePath + "_tms/route/findBranchInventoryByPsId";
		getBranchInventoryById = basePath + "_tms/route/findBranchInventoryById";
		getRouteLineByPsIdRouteId = basePath + "_tms/route/getRouteLineByPsIdRouteId";

		// Receive
		GetTMSEntry = basePath + "_tms/unloading/getEntry";
		GetTMSEntryTask = basePath + "_tms/unloading/getEntryTask";
		GetPalletsOrPackages = basePath + "_tms/unloadingInfo/getPalletsOrPackages";
		GetPalletToTrackNo = basePath + "_tms/unloading/getPalletToTrackNo";
		GetPalletIDByTrackNo = basePath + "_tms/unloading/getPalletIDByTrackNo";
		UpdateLoadingDetailsStatus = basePath + "_tms/unloading/updateLoadingDetailsStatus";
		GetUnPlanPalletOrPackage = basePath + "_tms/unloading/getUnPlanPalletOrPackage";
		// 删除单个的sn
		deleteSN = basePath + "_receive/android/deleteSN";
		deletePalletSN = basePath + "_receive/android/deletePalletSN";
		// 删除pallets
		deletePallet = basePath + "_receive/android/deletePallet";
		bindSNInfoToPallet = basePath + "_receive/android/bindSNInfoToPallet";
		createContainer = basePath + "_receive/android/createContainer";
		deleteQTY = basePath + "_receive/android/deletePallet";
		scanCreateContainer = basePath + "_receive/android/scanCreateContainer";

		// Load
		GetLoadingTicket = basePath + "_tms/loading/getLoadingTicket";
		GetLoadingTicket1 = basePath + "_tms/loadingInfo/getLoadingTicket";
		GetPalletIDByTrackNoLoad = basePath + "_tms/loading/getPalletIDByTrackNo";
		HandleSplitPallet = basePath + "_tms/loading/handleSplitPallet";
		UpdateLoadingDetailsStatusload = basePath + "_tms/loading/updateLoadingDetailsStatus";
		AddBOLTempInfo = basePath + "_tms/bolTemp/addBOLTempInfo";
		AddLoadingTempInfo = basePath + "_tms/unloadingStag/addUnLoadingTempInfo";
		GetUnPlanPalletOrPackageLoad = basePath + "_tms/loading/getUnPlanPalletOrPackage";

		GetLoadingPhotos = basePath + "_tms/loading/getLoadingPhotos";


		//print
		GetPrintBolOrderData = basePath + "_tms/print/getPrintBolOrderData";//bol数据
		PrintBolInfo = basePath + "_tms/print/printBolInfo";//bol 打印
		PrintLable = basePath +"_tms/sorting/pallet/printLable";//pallet
		// Sorting
		PickUpCargo = basePath + "_tms/sorting/track/pickUpCargo";
		SendAlong = basePath + "_tms/sorting/track/updateTrack";
		GetExceptionPalletData = basePath + "_tms/sorting/track/getTrack";
		PalletUnpacking = basePath + "_tms/sorting/pallet/upPack";
		createRelationT_P = basePath + "_tms/sorting/track/createRelationT_P";
		deleteRelationT_P = basePath + "_tms/sorting/track/deleteRelationT_P";
		getTrack = basePath + "_tms/sorting/track/getTrack";

		findAllPallet = basePath + "_receive/android/findAllPallet";

		// CreatePallet
		damagedException = basePath + "_tms/sorting/pallet/damagedException";
		TMSCreatePallet = basePath + "_tms/sorting/pallet/linkCreatePallet";
		TMSCreatePalletDoSubmit = basePath + "_tms/sorting/pallet/createPallet";
		getOpenPalletOrderById = basePath + "_tms/sorting/pallet/getOpenPalletOrderById";
		closePallet = basePath + "_tms/sorting/pallet/closePallet";
		// TMSCreatePalletDoSubmit =
		// "http://localhost:9087/Sync10/_tms/sorting/pallet/aaPallet";
		TMS_cleanPalletById = basePath + "_tms/sorting/pallet/cleanPalletById";
		TMS_deletePalletById = basePath + "_tms/sorting/pallet/deletePalletById";
		TMS_getPalletListOrderByItemId = basePath + "_tms/sorting/pallet/getPalletListOrderByItemId";

		product_add = basePath + "basicdata/product/add";
		product_context = basePath + "basicdata/product/context";
		productIndexAction = basePath + "action/administrator/proprietary/productIndexAction.action";


		// WMS Special Project
		taskAndInvoiceAction = basePath + "action/administrator/android/taskAndInvoiceAction.action";
	}

	/**
	 * ================================================================
	 * <p/>
	 * URL声明
	 * <p/>
	 * ================================================================
	 */
	public static String againPatrolAll;
	public static String WebServerFileBasePath;
	public static String WEBServerProductUrl;
	public static String WEBServerCheckInUrl;
	public static String WEBServerProductSmallUrl;
	public static String getTransportAction;
	public static String getTransportOutboundJsons;
	public static String getStreamSerialProduct;
	public static String getStreamLpInfoAction;
	public static String postReceiveInfoAction;
	public static String getZipLpBaseInfoAction;
	public static String getXMLTransportCheckInOver;
	public static String submitLpProductRelationAction;
	public static String receiveInfoSubmit;
	public static String getPositionStreamSerialProduct;
	public static String positionAction;
	public static String seachLoginUser;
	public static String seachProductLine;
	public static String seachProductByType;
	public static String BCSOutbound;
	public static String PickUp;
	public static String PickUpBCSOutBill;
	public static String InventoryLoadBaseInfo;
	public static String InventoryAction;
	public static String InventoryActionNew;
	public static String InventoryLoadBaseInfo_new;
	public static String productBaseDataXMLAction;
	public static String getProductDetailAction;
	public static String getProductUnionAction;
	public static String getProductCodeAction;
	public static String getProductByIdAction;
	public static String getProductClassify;
	public static String updateProductCatalog;
	public static String getProductFile;
	public static String postFiles;
	public static String postProductFiles;
	public static String getStorageCatalogAction;
	public static String getCountryAction;
	public static String getProvinceAction;
	public static String getTransportResourceAction;
	public static String getAllFreightItemAction;
	public static String submitDataByTabIdAction;
	public static String getProduresByOrderTypeXMLAction;
	public static String getProduresDetailsByProduresIdDValsXMLAction;
	public static String getProduresNoticesByProduresIdXMLAction;
	public static String getAllUserAndDeptXMLAction;
	public static String getDetailTransportByIdXMLAction;
	public static String GetProductInfoByStorageIdXMLAction;
	public static String SaveOrUpdateTransportItemsXMLAction;
	public static String LoadApkVersion;
	public static String LoadAndroidScreenApkAction;// 大屏幕的
	public static String LoadApk;
	public static String GetFileByTypeAction;
	public static String GetTransportRelatedInfoXmlAction;
	public static String LoadProductAndPicture;
	public static String LoadBarCodeInfoByBarCode;
	public static String LoadOneProductThumbnailByPcid;
	public static String loadFile;
	public static String clearDoor;
	public static String GetTransportsByRequireAction;
	public static String GetDoorCanChooseOrAllByStorageToXMLAction;
	public static String GetLocationCanChooseOrAllByStorageToXMLAction;
	public static String AddDoorOrLocationUseInfoXmlAction;
	public static String UpdateTransportToTransitAndReleaseOccXmlActon;
	public static String AddNewLpInfoAction;
	public static String AndroidLoadFile;
	public static String productBaseDataZipAction;
	public static String Container;
	public static String SubmitContainer;
	public static String AppendContainer;
	public static String BaseProductAction;
	public static String TransprortAction;
	public static String loadProductMainPicture;
	public static String AndroidDockCloseAction;
	public static String CheckInAction;
	public static String CheckInActionMutiRequest;
	public static String CheckInPatrol;
	public static String ProprietaryFindByAdidXmlAction;
	public static String AndroidSubmitContainerAndChildAction;
	public static String AndroidGetTransportProductAction;
	public static String SysUserAction;
	public static String FileUpZipAction;
	public static String AndroidPrintByWebAction;
	public static String AndroidAutoAction;
	public static String LoadPaperWorkAction;
	public static String androidSmallParcel;
	public static String ConsolidateAction;
	public static String ShippingAndPlateAction;
	public static String Load_PrintLabel;
	public static String ReceiveWorkAction;
	public static String AndroidSignatureFileUpAction;
	public static String CheckInWareHouseBigScreenAction;
	public static String AndroidDockCheckInAction;
	public static String AndroidAssignTaskAction;
	public static String GCMAction;
	public static String PushMessageByGCMAction;
	public static String AndroidTaskProcessingAction;
	public static String GetAccountInfoAction;
	public static String AndroidWindowCheckInAction;
	public static String AndroidShuttleAction;

	public static String getStorageBaseData;// 获取仓库四个点的坐标
	public static String queryAreaByPsId; // zone
	public static String queryDoorYardAndroid; // 门与停车位
	public static String queryWebcamAndroid; // 网络摄像头
	public static String getDocksParkingOccupancy; // 占用情况
	public static String getDocksParkingOccupancyDetail; // 获取占用信息
	public static String getDocksParkingOccupancyDetailAndroid; // 获取占用信息
	public static String storageDatas; // 请求地图数据zip

	public static String UploadFile; // 上传文件
	public static String DownFile; // 下载文件

	public static String accountUpdateInfoUrl; // 修改账号密码
	public static String accountUpdateAvatarMainUrl; // 修改是否为默认头像, 默认 / 非默认
	public static String accountDeleteAvatarUrl; // 删除头像
	public static String accountAddAvatarUrl; // 添加头像
	public static String logMovement;
	public static String acquireMovementByPalletId;

	// Receive
	public static String acquireRNLineInfo;
	public static String acquireRNLineInfoByReceiptID;
	public static String bindQTYToPallet;
	public static String acquirePalletsInfoByLine;
	public static String createContainer;
	public static String scanCreateContainer;

	//cc
	public static String createContainerForCC;

	public static String deleteSN;
	public static String deletePalletSN;
	public static String deletePallet;
	public static String bindSNInfoToPallet;
	public static String deleteQTY;
	public static String findAllPallet; // 获取所有的Pallets

	//商品
	public static String product_context;
	public static String product_add;
	public static String productIndexAction;

	// ---for Yasir
	public static String TaskInstanceListByUser;// List of Task for a User
	public static String LocationData;// Get Data for Location
	public static String StartTask;// Start task
	public static String TaskInstanceAreas;// Fetch Areas in Task
	public static String TaskInstanceAreaLocations;// Fetch Locations of area in
													// Task;
	public static String AssignToMe;// Assign to me
	public static String SubmitLocationScan;// Submit location Data

	public static String CCT_uploadPhoto;
	public static String CCT_delUploadPhotos;
	public static String CCT_blindRequestProductBySn;
	public static String CCT_blindRequestProductByProduct;
	public static String CCT_blindRequestCLP;
	public static String CCT_RequestSKU;
	public static String CCT_CreatePackaging;
	public static String CCT_GetCreateLpDataInfo;
	public static String CCT_AddLP_Type;
	public static String CCT_TLP_Context;
	public static String CCT_CLP_Context;
	public static String CCT_CLP_TypeAdd;
	public static String CCT_CheckSnList;

	// ---TMS
		//--------TMS AnNeng----
	public static String getAnnengLoadingOrders;
	public static String addAmmemgReceoveorders;
	public static String rollbackReceive;
	public static String addAmmengOrderAndRefresh;
	public static String updateWaybillAndWaybillItems;
	public static String getWaybillByWaybillItemId;

	public static String GetRouteList;
	public static String GetOrderByTN;
	public static String TMSLoadtn;
	public static String TMSDeleLoadTn;
	public static String GetOrderAndShippto;

	public static String getAnnengInventoryInfo;
	public static String getAnnengBranchInventory;
	public static String getBranchInventoryById;
	public static String getRouteLineByPsIdRouteId;

		//----------------------

	// Receive
	public static String GetTMSEntry;
	public static String GetTMSEntryTask;
	public static String GetPalletsOrPackages;// 根据接收清单ID获取接收清单
	public static String GetPalletToTrackNo;// 获取Pallet下所有的TrackNo
	public static String GetPalletIDByTrackNo;// 根据TRACK_NO反向获取PALLET_ID
	public static String UpdateLoadingDetailsStatus;// 提交收货单据
	public static String GetUnPlanPalletOrPackage;// 如果包裹或托盘为非装货计划内的货物，则判断该货物是否是待发货状态
	// Load
	public static String GetLoadingTicket,GetLoadingTicket1;
	public static String GetPalletIDByTrackNoLoad;
	public static String HandleSplitPallet;// 根据PALLET
											// ID获取PALLET并将PALLET下的PACKAGE设置为散货。设置PACKAGE与BOL,SO之间的关联关系
	public static String UpdateLoadingDetailsStatusload;// 提交装载清单
	public static String AddBOLTempInfo ;//暂存数据 load
	public static String AddLoadingTempInfo ;//暂存数据 receive
	public static String GetUnPlanPalletOrPackageLoad;// 如果包裹或托盘为非装货计划内的货物，则判断该货物是否是待发货状态
	// tms
	public static String GetLoadingPhotos; // 获取图片 暂不可用

	//bol print
	public static String GetPrintBolOrderData;//取bol打印数据
	public static String PrintBolInfo;//bol打印
	//pallet print
	public static String PrintLable;
	// Sorting
	public static String PickUpCargo;// 扫描TN 或者 Pallet 返回数据
	public static String SendAlong;// 扫描TN 或者 Pallet 返回数据
	public static String GetExceptionPalletData;// 获取pallet数据
	public static String PalletUnpacking;// 获取pallet数据
	public static String createRelationT_P;// TN关联托盘
	public static String deleteRelationT_P;
	public static String getTrack;
	public static String damagedException;
	public static String getOpenPalletOrderById;

	// tms createPallet
	public static String TMSCreatePallet;
	public static String TMSCreatePalletDoSubmit;
	public static String closePallet;
	public static String TMS_cleanPalletById;
	public static String TMS_deletePalletById;
	public static String TMS_getPalletListOrderByItemId;

	// WMS Special Project
	public static String taskAndInvoiceAction;

	// public static String ChatRecordUrl; // 服务器历史聊天记录(Chat)

	public static Map<String, String> serverUrls = new LinkedHashMap<String, String>();

	static {
		serverUrls.put("USA AWS", formatUrl("sync.logisticsteam.com"));
		serverUrls.put("USA AWS Test", formatUrl("sync-test.logisticsteam.com"));
		serverUrls.put("USA AWS Test Walnut", formatUrl("sync-test-walnut.logisticsteam.com"));
	}

	public static String formatUrl(String ipAndPort) {
		return String.format("http://%s/Sync10/", ipAndPort);
	}

	/**
	 * 获取服务器名称列表
	 */
	public static String[] getServerNames() {
		final List<String> list = new ArrayList<String>();
		final Iterator<Entry<String, String>> iterator = serverUrls.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			list.add(entry.getKey());
		}

		final Iterator<Entry<String, HashSet<String>>> itExt = StoredData.getExtServers().entrySet().iterator();
		while(itExt.hasNext()) {
			Entry<String, HashSet<String>> en = itExt.next();
			list.add(en.getKey());
		}
		return list.toArray(new String[] {});
	}

	/**
	 * ================================================================
	 * <p/>
	 * basePath初始值
	 * <p/>
	 * ================================================================
	 */
	public static final String DEFAULT_SERVER_NAME = "USA AWS";
	public static String basePath = serverUrls.get(DEFAULT_SERVER_NAME);

	/**
	 * 聊天服务器
	 */
	// public static String ChatServerUrl ;//= "192.168.1.15";
	// //"124.205.21.209";
	// public static String ChatServerDomain = "@192.168.1.15"; // 暂时先处理
	// 此处应与openfire上的设置域相同。可以获取
	public static String chatServerUrl() {
		return FormatterUtil.getChatBaseUrl(basePath);
	}

	public static String getSipServerUrl() {
		return FormatterUtil.getChatBaseUrl(basePath);
	}

	public static String chatRecordServerUrl() {
		return FormatterUtil.getChatRecordServerUrl(chatServerUrl(), ChatServerPort) + "/plugins/chatRecord/chatrecordservlet?action=query!record";
	}

	public static int Xmpp_Port = 5222;

	public static int ChatServerPort = 9090;

	// public static String ChatRecordServerUrl;

}
