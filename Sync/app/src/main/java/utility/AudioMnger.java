package utility;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import org.apache.http.Header;

import oso.SyncApplication;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.network.SimpleFileUtil;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import declare.com.vvme.R;


/**
 * 音频管理器,录音、播放
 * @author 朱成
 * @date 2015-2-3
 */
public class AudioMnger {
	private File mSampleFile;
	private MediaRecorder mRecorder;
	
	private Handler handler=SyncApplication.getThis().getHandler();
	
	private WeakReference<OnRecListener> onRec;
	
	private void setOnRecListener(OnRecListener onRec){
		this.onRec=new WeakReference<AudioMnger.OnRecListener>(onRec);
	}

	
	public void startRec(OnRecListener onRec) {
		// when plan comes to interrupt activity
		stopRec();
		Log.i("Start Rec", "MessageManager");

		mRecorder = new MediaRecorder();
		initRecorder();
		mRecorder.start();
		
		setOnRecListener(onRec);
		handler.post(run_onRec);
	}

	/**
	 * 结束录音
	 * @return
	 */
	public File stopRec() {
		onRec=null;
		
		if (mRecorder != null) {
			try {
				mRecorder.stop();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			mRecorder.release();
			mRecorder = null;
		}
		return mSampleFile;
	}
	
	/**
	 * 取消录音
	 */
	public void cancelRec() {
//		if (mRecorder != null) {
//			mRecorder.stop();
//			mRecorder.release();
//			mRecorder = null;
//		}
		stopRec();
		
		if(mSampleFile!=null&& mSampleFile.exists())
			mSampleFile.delete();
	}

	private MediaPlayer mp = new MediaPlayer();

//	public void playRec(File file) {
//		playRec(file.getPath());
//	}
	
	/**
	 * 时长,s
	 * @param audio
	 * @return
	 */
	public int getAudioDuration(File audio){
		MediaPlayer mp=MediaPlayer.create(SyncApplication.getThis(), Uri.fromFile(audio));
		int dur=mp.getDuration();
		mp.release();
		return dur;
	}
	
	private void playRec_inner(String path,OnCompletionListener onComplete,Context c) {
		mp.reset();
		try {
			
			mp.setDataSource(path);
			mp.prepare();
			mp.start();
			
			mp.setOnCompletionListener(onComplete);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			UIHelper.showToast(c.getString(R.string.sync_invalid_audio));
			//debug
			if(onComplete!=null)
				onComplete.onCompletion(mp);
		}
		
		
		
	}
	
	public void onDestroy(){
//		if(mp!=null){
//			mp.release();
//			mp=null;
//		}
		cancelRec();
		mp.reset();
	}
	
	public void playRec(final String uri,final OnCompletionListener onComplete,final Context c){
		ImageLoader imgLoader=DisplayPictureUtil.getImageLoader();
		final File f=imgLoader.getDiscCache().get(uri);
		if(f.exists())
		{
			playRec_inner(f.getPath(),onComplete,c);
			return;
		}
		
		new SimpleFileUtil() {
			
			@Override
			public void handResponse(int statusCode, Header[] headers,
					byte[] responseBody) {
				// TODO Auto-generated method stub
//				FileUtils.writeFile(f, new ByteArrayInputStream(responseBody));
				playRec_inner(f.getPath(),onComplete,c);
			}
			public void handFail() {
				UIHelper.showToast(c.getString(R.string.sync_downaudio_fail));
				//debug
				if(onComplete!=null)
					onComplete.onCompletion(mp);
			};
		}
		.doGetFile_audio(uri, f, new RequestParams(), SyncApplication.getThis());
//		.doGetFile(uri,new RequestParams(),WareHouseApplication.getThis());
		
	}

	// ==================inner===============================

	private static AudioMnger instance;

	public static AudioMnger getThis() {
		if (instance == null)
			instance = new AudioMnger();

		return instance;
	}

	private AudioMnger() {

	}

	private void initRecorder() {
		
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mSampleFile = null;
		try {
			mSampleFile = File.createTempFile("record", ".wav",
					Goable.getDir_Audio());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// AudioManager am;

		mRecorder.setOutputFile(mSampleFile.getAbsolutePath());
		try {
			mRecorder.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private Runnable run_onRec = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			OnRecListener onRec_x = onRec != null ? onRec.get() : null;
			if (onRec_x != null && mRecorder != null) {
				onRec_x.onRec(mRecorder.getMaxAmplitude());
				handler.postDelayed(run_onRec, 200);
			}
		}
	};
	
	//==================nested===============================

	public interface OnRecListener{
		public void onRec(int volume);
	}

}
