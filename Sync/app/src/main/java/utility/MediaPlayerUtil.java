package utility;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class MediaPlayerUtil {
	
	public static void playMedia(MediaPlayer mediaPlayer) {
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.seekTo(0);
				mediaPlayer.start();
				mediaPlayer.setVolume(0.1f, 0.2f);
 			} else {
				mediaPlayer.start();
			}
		}
	}
	/**
	 * 初始化Player
	 * @param mediaPlayer
	 * @param id
	 * @param resources
	 * @return
	 */
	private static MediaPlayer initMediaPlayer(MediaPlayer mediaPlayer, int id , Resources resources) {
		AssetFileDescriptor file = resources.openRawResourceFd(id);
		try {
			mediaPlayer.setDataSource(file.getFileDescriptor(),file.getStartOffset(), file.getLength());
			file.close();
			mediaPlayer.setVolume(0.1f, 0.2f);
			mediaPlayer.prepare();
		} catch (IOException ioe) {
			mediaPlayer = null;
		}

		return mediaPlayer;
	}
	public static MediaPlayer getInitMediaPlayer(int id , Resources resources){
		
	    MediaPlayer alertPlayer = new MediaPlayer();
		alertPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		alertPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer player) {
						player.seekTo(0);
					}
				});
	 
		alertPlayer =  initMediaPlayer(alertPlayer, id ,resources);
		return alertPlayer;
	}
}
