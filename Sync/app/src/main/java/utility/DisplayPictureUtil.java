package utility;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import declare.com.vvme.R;

public class DisplayPictureUtil {
	
	public static DisplayImageOptions getDisplayImageOptions() { 
			DisplayImageOptions	options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub) // 在ImageView加载过程中显示图片
				.showImageForEmptyUri(R.drawable.ic_stub) // image连接地址为空时
				.showImageOnFail(R.drawable.ic_stub) // image加载失败
				.cacheInMemory(true) // 加载图片时会在内存中加载缓存
				.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
				.displayer(new RoundedBitmapDisplayer(8)) // 设置用户加载图片task(这里是圆角图片显示)
				.build();
			return options ;
	}
	public static ImageLoader getImageLoader() {
		return  ImageLoader.getInstance();
	}
	
	// 电话界面头像显示
	public static DisplayImageOptions getCallAvatarDisplayImageOptions() {
		DisplayImageOptions	options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.chat_call_avatar_default) // 在ImageView加载过程中显示图片
			.showImageForEmptyUri(R.drawable.chat_call_avatar_default) // image连接地址为空时
			.showImageOnFail(R.drawable.chat_call_avatar_default) // image加载失败
			.cacheInMemory(true) // 加载图片时会在内存中加载缓存
			.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
			.displayer(new FadeInBitmapDisplayer(300))
			.build();
		return options ;
	}
	
	// account头像显示options
	public static DisplayImageOptions getAvatarDisplayImageOptions() {
		DisplayImageOptions	options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.ic_account_avatar_loding_display) // 在ImageView加载过程中显示图片
			.showImageForEmptyUri(R.drawable.ic_account_avatar_loding_display) // image连接地址为空时
			.showImageOnFail(R.drawable.ic_account_avatar_loding_display) // image加载失败
			.cacheInMemory(true) // 加载图片时会在内存中加载缓存
			.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
			.build();
		return options ;
	}
	
	// Contacts account头像显示options
	public static DisplayImageOptions getChatAvatarDisplayImageOptions_Online() {
		DisplayImageOptions	options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.account_circle) // 在ImageView加载过程中显示图片
			.showImageForEmptyUri(R.drawable.account_circle) // image连接地址为空时
			.showImageOnFail(R.drawable.account_circle) // image加载失败
			.cacheInMemory(true) // 加载图片时会在内存中加载缓存
			.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
			.build();
		return options ;
	}
	
	// Contacts account头像显示options
		public static DisplayImageOptions getChatAvatarDisplayImageOptions_Offline() {
			DisplayImageOptions	options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.account_offline_circle) // 在ImageView加载过程中显示图片
				.showImageForEmptyUri(R.drawable.account_offline_circle) // image连接地址为空时
				.showImageOnFail(R.drawable.account_offline_circle) // image加载失败
				.cacheInMemory(true) // 加载图片时会在内存中加载缓存
				.cacheOnDisc(true) // 加载图片时会在磁盘中加载缓存
				.build();
			return options ;
		}
}
