package support.exception;

public class ContainerNumLimitException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContainerNumLimitException() {
		super();
 	}
	
	
	
	
	public ContainerNumLimitException(String detailMessage) {
		super(detailMessage);
 	}




	public ContainerNumLimitException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
