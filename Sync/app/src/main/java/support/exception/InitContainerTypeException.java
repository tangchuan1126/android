package support.exception;

public class InitContainerTypeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitContainerTypeException() {
		super();
 	}
	
	
	
	
	public InitContainerTypeException(String detailMessage) {
		super(detailMessage);
 	}




	public InitContainerTypeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
