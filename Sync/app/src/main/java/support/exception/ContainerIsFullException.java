package support.exception;

public class ContainerIsFullException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContainerIsFullException() {
		super();
 	}
	
	
	
	
	public ContainerIsFullException(String detailMessage) {
		super(detailMessage);
 	}

 
	public ContainerIsFullException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
