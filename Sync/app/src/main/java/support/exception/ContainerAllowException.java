package support.exception;

public class ContainerAllowException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContainerAllowException() {
		super();
 	}
	
	
	
	
	public ContainerAllowException(String detailMessage) {
		super(detailMessage);
 	}

 
	public ContainerAllowException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
