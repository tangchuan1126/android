package support.exception;

public class ProducNumLimitException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProducNumLimitException() {
		super();
 	}
	
	
	
	
	public ProducNumLimitException(String detailMessage) {
		super(detailMessage);
 	}




	public ProducNumLimitException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
