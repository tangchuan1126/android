package support.exception;

public class SystemException extends RuntimeException{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -4409204982274519641L;

	public SystemException(String detailMessage) {
		super(detailMessage);
 	}

	public SystemException() {
		super();
 	}

	public SystemException(Throwable throwable) {
		super(throwable);
 	}
 
}
