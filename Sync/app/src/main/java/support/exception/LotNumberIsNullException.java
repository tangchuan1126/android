package support.exception;

public class LotNumberIsNullException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -168973156005026165L;

	public LotNumberIsNullException() {
		super();
 	}

	public LotNumberIsNullException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
 	}

	
}
