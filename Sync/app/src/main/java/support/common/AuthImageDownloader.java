package support.common;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import support.network.SimpleJSONUtil;
import android.content.Context;
import android.net.Uri;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

/**
 * 使imageLoader支持cookie
 * @author 朱成
 * @date 2015-1-22
 */
public class AuthImageDownloader extends BaseImageDownloader {

	private static final int MAX_REDIRECT_COUNT = 5;

	public AuthImageDownloader(Context context) {
		super(context);
	}

	public AuthImageDownloader(Context context, int connectTimeout,
			int readTimeout) {
		super(context, connectTimeout, readTimeout);
	}

	protected InputStream getStreamFromNetwork(String imageUri, Object extra)
			throws IOException {
		HttpURLConnection conn = connectTo(imageUri);

		int redirectCount = 0;
		while (conn.getResponseCode() / 100 == 3
				&& redirectCount < MAX_REDIRECT_COUNT) {
			conn = connectTo(conn.getHeaderField("Location"));
			redirectCount++;
		}

		return new BufferedInputStream(conn.getInputStream(), BUFFER_SIZE);
	}

	/**
	 * 获取带有用户验证信息的HttpURLConnection
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	private HttpURLConnection connectTo(String url) throws IOException {
		String encodedUrl = Uri.encode(url, ALLOWED_URI_CHARS);
		HttpURLConnection conn = (HttpURLConnection) new URL(encodedUrl)
				.openConnection();
                //这句话为urlconnection加入身份验证信息
		conn.setConnectTimeout(connectTimeout);
		//添加cookie
		conn.addRequestProperty("Cookie", SimpleJSONUtil.getCookie());
		conn.setReadTimeout(readTimeout);
		conn.connect();
		return conn;
	}
}