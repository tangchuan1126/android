package support.common.print;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import declare.com.vvme.R;
import oso.ui.load_receive.print.bean.PrintServer;
import oso.widget.dialog.RewriteBuilderDialog;
import support.common.UIHelper;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;

/**
 * 打印工具,请求printers、显示printersDialog
 * 
 * @author 朱成
 * @date 2015-3-17
 */
public class PrintTool {

	private Context mContext;

	private UseMySelfWay iface;

	public PrintTool(Context c) {
		this.mContext = c;
	}

	/**
	 * 使用自定义接口
	 * @param iface
	 */
	public void setIface(UseMySelfWay iface){
		this.iface = iface;
	}

//	/**
//	 * 点击-打印时
//	 *
//	 * @param printServerID
//	 */
//	public abstract void doPrint(long printServerID);

	// public void showReprint() {
	// final View v = View.inflate(mActivity, R.layout.dialog_reprint, null);
	// final EditText et = (EditText) v.findViewById(R.id.printEt);
	// RewriteBuilderDialog.Builder builder = new
	// RewriteBuilderDialog.Builder(mActivity);
	// builder.setTitle("Reprint Pallet Labels");
	// builder.setView(v);
	// builder.hideCancelBtn();
	// builder.setPositiveButton("Submit", new DialogInterface.OnClickListener()
	// {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// showDlg_printers();
	// }
	// });
	// builder.show();
	// }
	
	public void showDlg_printers(final OnPrintLs ls){
		showDlg_printers(ls, "");
	}

	/**
	 * 显示-打印机列表
	 */
	public void showDlg_printers(final OnPrintLs ls,String title) {
		if (listPrinters1.size() == 0) {
			reqPrinters(ls,title);
			return;
		}

		List<PrintServer> printServerList = listPrinters1;
		if (printServerList == null || printServerList.size() == 0) {
			UIHelper.showToast(mContext, mContext.getString(R.string.sync_no_printserver));
			return;
		}

		/*************************** 调试代码 **********************************/
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_load_select_printserver, null);
		ListView listView = (ListView) layout.findViewById(R.id.select_printer);
		TextView tvEmpty = (TextView) layout.findViewById(R.id.tvEmpty);
		adpPrinters = new PrinterAdapter(mContext, printServerList, onInnerClick_Printers);
		listView.setEmptyView(tvEmpty);
		listView.setAdapter(adpPrinters);
		LayoutParams lp = listView.getLayoutParams();
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = (printServerList.size() < 3) ? dm.heightPixels / 7 * printServerList.size() : (dm.heightPixels / 3);
		listView.setLayoutParams(lp);
		if (printServerList.size() <= 1) {
			listView.setDivider(null);
		}
		adpPrinters.setCurItem(getCurPrinter_index());
		// 初始-滑至可见
		listView.setSelection(getCurPrinter_index());
		/*****************************************************************/

		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(mContext);
		builder.setTitle(TextUtils.isEmpty(title)?mContext.getString(R.string.print_select_printer):title);
		builder.setContentView(layout);
		builder.setPositiveButtonOnClickDismiss(false);
		builder.setPositiveButton(mContext.getString(R.string.tms_print_text), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (getCurPrinter_index() != -1) {
//					PrintTool.this.doPrint(curPrinterId);
					if(ls!=null)
						ls.onPrint(curPrinterId);
					
					dialog.dismiss();
				} else
					UIHelper.showToast(mContext, mContext.getString(R.string.sync_select_printserver));
			}
		});
		builder.setNegativeButton(mContext.getString(R.string.sync_cancel), null);
		builder.create().show();
	}

	// ==================inner===============================

	// 请求printers
	private void reqPrinters(final OnPrintLs ls,final String title) {
		// 若已加载,则直接显示
		if (listPrinters1.size() > 0) {
			showDlg_printers(ls);
			return;
		}

		if(iface!=null){
			iface.getPrintServer();
			return;
		}


		RequestParams params = new RequestParams();
//		params.add("Method", "GetLetterPrintServer");
		params.add("ps_id", StoredData.getPs_id());
//		params.add("area_id", "GetLetterPrintServer");
		new SimpleJSONUtil() {

			@Override
			public void handReponseJson(JSONObject json) {
//				// TODO Auto-generated method stub
//				listPrinters1.clear();
//
//				// 解析
//				JSONObject joData = json.optJSONObject("printserver");
//				curPrinterId = joData.optLong("default");
//				PrintServer.parseBeans(joData.optJSONArray("allprintserver"), listPrinters1);
//				if (listPrinters1.size() == 0) {
//					UIHelper.showToast(mContext, mContext.getString(R.string.sync_no_printserver));
//					return;
//				}
//
//				// 显示-列表
//				showDlg_printers(ls,title);
				jsonToShow(json,ls,title);
			}
		}.doGet(HttpUrlPath.basePath + "_receive/android/aquirePrintServers", params, mContext);
	}

	/**
	 * 用于显示
	 * @param json
	 * @param ls
	 * @param title
	 */
	public void jsonToShow(JSONObject json,final OnPrintLs ls,final String title){

		if(utility.Utility.isNullForList(listPrinters1)){
			listPrinters1 = new ArrayList<PrintServer>();
		}else {
			listPrinters1.clear();
		}

		// 解析
		JSONObject joData = json.optJSONObject("printserver");
		curPrinterId = joData.optLong("default");
		PrintServer.parseBeans(joData.optJSONArray("allprintserver"), listPrinters1);
		if (listPrinters1.size() == 0) {
			UIHelper.showToast(mContext, mContext.getString(R.string.sync_no_printserver));
			return;
		}

		// 显示-列表
		showDlg_printers(ls,title);
	}


	private List<PrintServer> listPrinters1 = new ArrayList<PrintServer>();
	private long curPrinterId; // 当前printer

	private PrinterAdapter adpPrinters = null;

	// 返回值:默认打印机-索引(若无默认,则为-1)
	private int getCurPrinter_index() {

		List<PrintServer> listPrinters = listPrinters1;
		if (listPrinters == null)
			return -1;

		for (int i = 0; i < listPrinters.size(); i++) {
			String tmpId = listPrinters.get(i).getPrinter_server_id();
			if ((curPrinterId + "").equals(tmpId))
				return i;
		}
		return -1;
	}

	private OnInnerClickListener onInnerClick_Printers = new OnInnerClickListener() {
		public void onInnerClick(View v, int position) {
			String id = listPrinters1.get(position).getPrinter_server_id();
			curPrinterId = Long.parseLong(id);
			adpPrinters.setCurItem(position);
		};
	};

	public class PrinterAdapter extends BaseAdapter {

		private List<PrintServer> arrayList;

		private LayoutInflater inflater;

		private Context context;
		private PrinterAdapter oThis = PrinterAdapter.this;

		private OnInnerClickListener onInnerClick;
		private int curItem = 0; // 当前-索引

		public void setCurItem(int curItem) {
			this.curItem = curItem;
			notifyDataSetChanged();
		}

		// -------------------
		public PrinterAdapter(Context context, List<PrintServer> arrayList, OnInnerClickListener onInnerClick) {
			super();
			this.context = context;
			this.arrayList = arrayList;
			this.inflater = LayoutInflater.from(context);
			this.onInnerClick = onInnerClick;
		}

		@Override
		public int getCount() {
			return arrayList != null ? arrayList.size() : 0;
		}

		@Override
		public PrintServer getItem(int position) {
			return arrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			CheckinScanLoadSelectPrinterAdapterHoder holder = null;
			if (convertView == null) {
				holder = new CheckinScanLoadSelectPrinterAdapterHoder();
				convertView = inflater.inflate(R.layout.item_select_printer, null);
				holder.item = (View) convertView.findViewById(R.id.item);
				holder.printer_name = (TextView) convertView.findViewById(R.id.printer_name);
				holder.zone_layout = (View) convertView.findViewById(R.id.zone_layout);
				holder.printer_zone = (TextView) convertView.findViewById(R.id.printer_zone);
				holder.cbPrint = (RadioButton) convertView.findViewById(R.id.cbPrint);
				holder.printIv = (ImageView) convertView.findViewById(R.id.printIv);
				convertView.setTag(holder);
			} else {
				holder = (CheckinScanLoadSelectPrinterAdapterHoder) convertView.getTag();
			}
			final PrintServer temp = arrayList.get(position);
			holder.printer_name.setText(temp.getPrinter_server_name() + "");
			if (StringUtil.isNullOfStr(temp.getZone())) {
				holder.zone_layout.setVisibility(View.GONE);
			} else {
				holder.zone_layout.setVisibility(View.VISIBLE);
				holder.printer_zone.setText(temp.getZone() + "");
			}
			holder.printIv.setImageResource(temp.isonline ? R.drawable.print_icon_style : R.drawable.print_icon_offline);
			holder.cbPrint.setChecked(curItem == position);
			holder.item.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (onInnerClick != null)
						onInnerClick.onInnerClick(v, position);
				}
			});
			return convertView;
		}

		public void cancelChecked(PrintServer temp) {
			for (int i = 0; i < arrayList.size(); i++) {
				if (!(arrayList.get(i).getPrinter_server_id()).equals(temp.getPrinter_server_id())) {
					arrayList.get(i).setCheckedFlag(false);
				}
			}
		}

		public PrintServer getCheckedPrintServer() {
			for (int i = 0; i < arrayList.size(); i++) {
				if (arrayList.get(i).isCheckedFlag()) {
					return arrayList.get(i);
				}
			}
			return null;
		}
	}

	class CheckinScanLoadSelectPrinterAdapterHoder {
		public View item;
		public TextView printer_name;
		public View zone_layout;
		public TextView printer_zone;
		public RadioButton cbPrint;
		public ImageView printIv;

	}
	
	//==================nested===============================
	
//	public OnPrintLs ls;

	public interface OnPrintLs{
		/**
		 * 选中服务器后 点打印时
		 * @param printServerID
		 */
		public void onPrint(long printServerID);
	}

	public interface UseMySelfWay{
		/**
		 *  自定义打印的接口请求
		 */
		public void getPrintServer();
	}

}
