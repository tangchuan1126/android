package support.common.print;

import android.view.View;

public interface OnInnerClickListener {
	public void onInnerClick(View v,int position);
}
