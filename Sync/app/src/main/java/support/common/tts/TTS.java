package support.common.tts;

import java.util.HashMap;
import java.util.Locale;

import oso.SyncApplication;
import support.common.UIHelper;
import utility.Utility;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.Engine;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.TextUtils;

/**
 * @author jiang 2014-8-13 上午11:33:08
 */
public class TTS implements OnInitListener {

	private static final String TAG = "TTS Demo";

	public static final int REQ_TTS_STATUS_CHECK = 0x123;
	private boolean speakAvailable = false;
	private TextToSpeech mTts;
	// private Activity mActivity;
//	private ConfigDao cf;

	private static HashMap<String, String> params = new HashMap<String, String>();

	private static TTS _instance = null;

	public static TTS getInstance() {
		if (_instance == null) {
			synchronized (Object.class) {
				if (_instance == null) {
					_instance = new TTS();
				}
			}
			return _instance;
		}
		return _instance;
	}

	private TTS() {
		// 初始化
		params.put(Engine.KEY_PARAM_VOLUME, "1");
	}

	public void init(Activity act) {
		// 若可用 则不再初始化
		if (speakAvailable)
			return;

		// mActivity = act;
//		cf = new ConfigDao(SyncApplication.getThis());
		Intent intent = new Intent(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);

		PackageManager pm = act.getPackageManager();
		ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
		if (resolveInfo == null) {
			// Not able to find the activity which should be started for this
			// intent
		} else {
			act.startActivityForResult(intent, REQ_TTS_STATUS_CHECK);
		}
	}

	public void speakAll(String value) {
		speakAll(value, false);
	}

	public void speakAll(String value, boolean vibrate) {
		if (vibrate)
			Utility.vibrate();

		// app被杀死时,mActivity可能不存在
		// if(mActivity==null)
		// return;

		if (!checkTTSAvailable())
			return;
		speechRate();
		mTts.speak(value, TextToSpeech.QUEUE_ADD, params);
	}

	private final String tipUnavailable = "TTS is not available!";

	private boolean checkTTSAvailable() {
		if (!speakAvailable) {
			UIHelper.showToast(tipUnavailable);
		}
		return speakAvailable;
	}

	public void speakAll_withToast(String value, boolean vibrate) {
		if (TextUtils.isEmpty(value))
			return;
		speakAll(value, vibrate);
		UIHelper.showToast(value);
	}

	/**
	 * 默认-带振动
	 * 
	 * @param value
	 */
	public void speakAll_withToast(String value) {
		speakAll_withToast(value, false);
	}

	public void speak(String value) {
		if (!checkTTSAvailable())
			return;
		speechRate();
		String str = addSpace(value);
		mTts.speak(str, TextToSpeech.QUEUE_ADD, params);
	}

	public void speechRate() {
		// String speed = cf.getConfigValue("sp_speed");
		// if (speed != null && speed.length() > 0) {
		// mTts.setSpeechRate(Float.parseFloat(cf.getConfigValue("sp_speed")));
		// } else {
		// mTts.setSpeechRate(1.0f);
		// }
		// debug
		mTts.setSpeechRate(1.3f);
	}

	public static String addSpace(String value) {
		if (value != null && value.length() > 0) {
			StringBuffer sb = new StringBuffer();
			for (int index = 0, count = value.length(); index < count; index++) {
				sb.append(value.charAt(index) + "").append(" ");
			}
			return sb.toString();
		}
		return "";
	}

	public void onTTSActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode != REQ_TTS_STATUS_CHECK)
			return;

		switch (resultCode) {
		case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS: // tts可用
			speakAvailable = true;
			mTts = new TextToSpeech(SyncApplication.getThis(), this);
			break;
		// case TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA:
		// // 需要的语音数据已损坏
		// case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA:
		// // 缺少需要语言的语音数据
		// case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME:
		// // 缺少需要语言的发音数据
		// // 这三种情况都表明数据有错,重新下载安装需要的数据
		// Log.v(TAG, "Need language stuff:" + resultCode);
		// // Intent dataIntent = new Intent();
		// // dataIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
		// // mActivity.startActivity(dataIntent);
		// speakAvailable = false;
		// break;
		case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL: // tts不可用
			speakAvailable = false;
			break;
		default:
			speakAvailable = false;
			break;
		}

	}

	private void onTTSInit(int status) {
		// 初始化-未完成
		if (status != TextToSpeech.SUCCESS) {
			speakAvailable = false;
			return;
		}

		// 设置-语言
		int result = mTts.setLanguage(Locale.US);
		// 不可用
		if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
			speakAvailable = false;
		}
		// 可用
		else {
			speakAvailable = true;
		}
	}

	// public void onTTSPause() {
	// // TODO Auto-generated method stub
	// if (mTts != null) {
	// mTts.stop();
	// }
	// }

	public void onTTSDestroy() {
		// TODO Auto-generated method stub
		if (mTts != null) {
			mTts.shutdown();
		}
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		onTTSInit(status);
	}

	// ----------------api-------------------------

	// 断句
	// 注:1>","、" "均无效,1次最多可有3个点
	public static String comma() {
		return ". ";
	}

}
