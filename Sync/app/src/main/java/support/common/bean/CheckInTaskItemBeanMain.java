package support.common.bean;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.do_task.receive.samsung.bean.Ctnr;
import support.AppConfig;
import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import utility.StringUtil;

/**任务bean
 * @author 朱成
 * @date 2015-1-14
 */
public class CheckInTaskItemBeanMain implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3050372441703697113L;
	public String company_id;
	public String supplier_id;
	public String account_id;
	public String dlo_detail_id;
	public int number_type;						//任务类型,取EntryDetailNumberTypeKey.CHECK_IN_x
	public String ic_id = "";
	public long lr_id;
	public String number;
	public String customer_id;
	public int number_status;						//任务-状态
	public String freight_term;
	public String executer;
	public String staging_area_id;
	public int is_forget_task;
	public int takeoverflag;
	public String osoNote;							//我们自己的note
	
	public String receipt_no;			//wms收货-才有

	// 自定义字段  选中状态
	public boolean checked;
	
	/**
	 * 有单据的收货,bol、ctnr
	 * @return
	 */
	public boolean isWmsReceive(){
		return number_type==EntryDetailNumberTypeKey.CHECK_IN_BOL||
				number_type==EntryDetailNumberTypeKey.CHECK_IN_CTN;
	}
	
	/**
	 * TMS有单据的收货,bol
	 * @return
	 */
	public boolean isTmsReceive(){
		return AppConfig.TMS_Task&&number_type==EntryDetailNumberTypeKey.TMS_LOADDING;
	}
	
	/**
	 * TMS有单据的装货,bol、ctnr
	 * @return
	 */
	public boolean isTmsLoad(){
		return AppConfig.TMS_Task&&number_type==EntryDetailNumberTypeKey.TMS_BOL;
	}

	/**
	 * TMS有单据的装货,bol、ctnr
	 * @return
	 */
	public boolean isTmsAnNengReceive(){
		return number_type==EntryDetailNumberTypeKey.TMS_AnNeng_Receive;
	}
	
	/**
	 * TMS有单据的装货,bol、ctnr
	 * @return
	 */
	public boolean isTmsAnNengLoad(){
		return number_type==EntryDetailNumberTypeKey.TMS_AnNeng_LOAD;
	}
	
	public boolean isSmallParcel(){
		return number_type==EntryDetailNumberTypeKey.SMALL_PARCEL;
	}
	
	/**
	 * 任务类型名,如:LOAD
	 * @return
	 */
	public String getTaskTypeName(){
		return EntryDetailNumberTypeKey
				.getModuleName(number_type);
	}
	
	public boolean hasFreightTerm(){
		return !TextUtils.isEmpty(freight_term);
	}
	
	public boolean hasOsoNote(){
		return !TextUtils.isEmpty(osoNote);
	}
	
	//================static=================================
	
	public static void parseBean2(JSONObject joEntry,
			List<CheckInTaskItemBeanMain> complexDoorList) {

		JSONArray ja = StringUtil.getJsonArrayFromJson(joEntry, "datas");

		if(ja != null && ja.length() > 0){
		// 解析-doors
		for (int i = 0; i < ja.length(); i++) {
			JSONObject item = StringUtil.getJsonObjectFromArray(ja, i);
			// 解析-loads
			CheckInTaskItemBeanMain complexDoor = new CheckInTaskItemBeanMain();
			parseBean(item, complexDoor);
			complexDoorList.add(complexDoor);
		}
		}
//		JSONArray files = StringUtil.getJsonArrayFromJson(joEntry, "files");
//		String retStr ;
//		StringBuilder sb = new StringBuilder();
//		if(files != null && files.length() > 0){
//		for (int i = 0; i < files.length(); i++) {
//			String item = null;
//			try {
//				item = files.get(i).toString();
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			// 解析-loads
//			sb.append(item+",");
//			photoList.add(item);
//		}
//		retStr = sb.toString();
//		retStr = retStr.substring(0, retStr.length() - 1);
//		}
	}

	public static void parseBean(JSONObject jo, CheckInTaskItemBeanMain b) {
		b.company_id = jo.optString("company_id");
		b.supplier_id = jo.optString("supplier_id");
		b.account_id = jo.optString("account_id");
		b.dlo_detail_id = jo.optString("dlo_detail_id");
		b.number_type = jo.optInt("number_type");
		
		b.ic_id = jo.optString("ic_id");
		b.lr_id=jo.optLong("lr_id");
		b.number = jo.optString("number");
		b.customer_id = jo.optString("customer_id");
		b.number_status = jo.optInt("number_status");
		
		b.freight_term = jo.optString("freight_term");
		b.executer = jo.optString("executer");
		b.staging_area_id = jo.optString("staging_area_id");
		b.is_forget_task = jo.optInt("is_forget_task");
		b.takeoverflag = jo.optInt("takeoverflag");
		b.osoNote=jo.optString("note");
		b.receipt_no=jo.optString("receipt_no");
	}
	
	public static List<String> handJson(JSONObject json){
		List<String> list = new ArrayList<String>();
		JSONArray jsonArray = json.optJSONArray("load_use");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.optJSONObject(i);
				if(jsonObject!=null){
					list.add(jsonObject.optString("load_bar_name")+"    "+jsonObject.optString("count"));
				}
			}
		}
		return list;
	}
	
	//============================================
	
	public CheckInLoadLoadBean toLoadBean(){
		CheckInLoadLoadBean load=new CheckInLoadLoadBean();
//		public String status;
//
//		public int number_type; // 取EntryDetailNumberTypeKey.xxx
//		public String number;
//		public int append_type;
//		public String append_number;
//
//		public String company_id;
//		public String customer_id;
//
//		public String dlo_detail_id;
//
//		public String ic_id; // 若">0",则为三星
//		public String staging_area_id;
//		public String freight_term;
//
//		// ----搜entry时没有---------
//		public String entry_id;
//		public String door;
		
		//可暂时不传:status,number/append_number:
		//暂缺:staging_area_id、freight_term
		load.number_type=number_type;
		load.number=number;
		
		load.company_id=company_id;
		load.customer_id=customer_id;
		load.dlo_detail_id=dlo_detail_id;
		load.freight_term=freight_term;
		load.ic_id=ic_id;
		
		return load;
	}
	//李君浩注释
	public Ctnr toCtnrBean(Context c) {
		Ctnr ctnr = new Ctnr();
		ctnr.setAccount_id(account_id);
		ctnr.setCompany_id(company_id);
		ctnr.setCustomer_id(customer_id);
		ctnr.setDlo_detail_id(dlo_detail_id);
		ctnr.setIc_id(ic_id);
		ctnr.setNumber(number);
		ctnr.setNumber_type(number_type+"");
		ctnr.setStatus(EntryDetailNumberStateKey.getType(c,number_status));
		ctnr.setStatus_int(number_status+"");
		return ctnr;
	}
	
}
