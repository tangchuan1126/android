package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

/**
 * @ClassName: RtAreaSpotBaseBean 
 * @Description: Pratol的area spot的bean类
 * @author gcy
 * @date 2014-11-25 下午2:58:48
 */

public class DockEquipmentBaseBean implements Serializable{
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 3801203298199894747L;
	public String equipment_type_value;// "Tractor",
	public int equipment_type;// 1,
	public String equipment_number;// "20141125",
	public String check_in_entry_id;// 120847,
	public int equipment_id;// 1
	public int total_task;
	
	public static List<DockEquipmentBaseBean> helpJson(JSONObject json){
		List<DockEquipmentBaseBean> list = null;
		JSONArray jsonArray = json.optJSONArray("data");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			list = new ArrayList<DockEquipmentBaseBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				DockEquipmentBaseBean bean = new DockEquipmentBaseBean();
				bean.equipment_type_value = item.optString("equipment_type_value");
				bean.equipment_type = item.optInt("equipment_type");// 1,
				bean.equipment_number = item.optString("equipment_number");
				bean.check_in_entry_id = item.optString("check_in_entry_id");
				bean.equipment_id = item.optInt("equipment_id");
				list.add(bean);
			}
		}
		return list;
	}
}
