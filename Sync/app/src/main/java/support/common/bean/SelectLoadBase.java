package support.common.bean;

import java.io.Serializable;
/**
 * 
 * @ClassName: SelectLoadBase 
 * @Description:
 * @author gcy 
 * @date 2014年7月17日 上午11:42:17 
 *
 */
public class SelectLoadBase implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String ompany_id; // "W12",
	private String load_no; // "125745687",
	private String customer_id; // "VIZIO"
	
	/**
	 * @Description:
	 * @return the ompany_id
	 */
	public String getOmpany_id() {
		return ompany_id;
	}
	/**
	 * @Description:
	 * @param ompany_id the ompany_id to set
	 */
	public void setOmpany_id(String ompany_id) {
		this.ompany_id = ompany_id;
	}
	/**
	 * @Description:
	 * @return the load_no
	 */
	public String getLoad_no() {
		return load_no;
	}
	/**
	 * @Description:
	 * @param load_no the load_no to set
	 */
	public void setLoad_no(String load_no) {
		this.load_no = load_no;
	}
	/**
	 * @Description:
	 * @return the customer_id
	 */
	public String getCustomer_id() {
		return customer_id;
	}
	/**
	 * @Description:
	 * @param customer_id the customer_id to set
	 */
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	
}
