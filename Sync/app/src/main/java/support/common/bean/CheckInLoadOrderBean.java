package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Color;
import android.text.TextUtils;

public class CheckInLoadOrderBean implements Serializable {

	private static final long serialVersionUID = -4321952882740246330L;
	public static final String[] LOAD_SEQ_VALUE = {"20", "3", "33", "53", "7"};
	public static final String[] LOAD_SEQ_TEXT = {"1st", "2nd", "3rd", "4th", "5th"};
	public static final int[] LOAD_SEQ_COLORS = {Color.RED, -755167, Color.YELLOW, Color.GREEN, Color.BLUE};
	public static final String[] COSTCO_LOAD_SEQ_VALUE = {"costco.com", "costco"};

	public String od_status;
	public String orderno;
	public int pallets;
	public String ml_status;
	public List<PalletBean> code_nos;

	public String staging_area_id;
	public String proNo;
	public String note;

	public String case_total;

	public String externalID = null;
	public String loadingSequence = null;
	public int loadingSeqColor = 0;
	public String accountID = null;

	// ---非接口数据--------
	// public List<Boolean> code_isScaned; //对应code_nos,表已扫描

	public boolean isPrintSelected = false; // 待打印

	public String firstPalletType; // 保留每个order首个选择的palletType,再扫该order时
									// 调至首位

	// ------------------------

	public boolean isEqual(CheckInLoadOrderBean order) {
		// TODO Auto-generated method stub
		if (order == null)
			return false;
		return orderno.equals(order.orderno);
	}

	// 注:1>firstPalletType未给定时才给定
	public void setFirstPalletType_whenNone(String type) {
		if (!TextUtils.isEmpty(firstPalletType) || TextUtils.isEmpty(type))
			return;
		firstPalletType = type;
	}

	// 调整"类型列表",保持第一个为首次选择
	public List<CheckInLoadPalletTypeBean> adjustPalletTypes_order(
			List<CheckInLoadPalletTypeBean> listTypes) {
		if (listTypes == null)
			return null;
		if (TextUtils.isEmpty(firstPalletType))
			return listTypes;

		List<CheckInLoadPalletTypeBean> tmpList = new ArrayList<CheckInLoadPalletTypeBean>();
		tmpList.addAll(listTypes);

		int index = CheckInLoadPalletTypeBean.getIndex(listTypes,
				firstPalletType);
		// 若找到,则移至最前
		if (index != -1)
			tmpList.add(0, tmpList.remove(index));
		// 未找到
		else
			firstPalletType = null;
		return tmpList;
	}

	// private Map<String, Integer> mapPallettypeCnt; //从"子单据列表"过来,可能算过后会不再算

	// 取"palletType-cnt"对
	// 注:1.调用一次后,若修改"pallet类型" 则该函数返回值"失效"
	public Map<String, Integer> getMapPalletTypeCnt() {

		// if (mapPallettypeCnt != null)
		// return mapPallettypeCnt;

		if (code_nos == null || code_nos.size() == 0)
			return null;

		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < code_nos.size(); i++) {
			PalletBean pallet = code_nos.get(i);
			// null也能map.put,故需跳出
			if (!pallet.hasType())
				continue;
			// String type = pallet.type.pallettypename;
			// debug
			String type = pallet.get_TypeId();
			if (map.containsKey(type)) {
				int tmpCnt = map.get(type);
				map.put(type, tmpCnt + 1);
			} else
				map.put(type, 1);
		}
		// mapPallettypeCnt = map;
		// return mapPallettypeCnt;
		return map;
	}

	// 重置pallets为"未load"
	public void resetPallets() {
		if (code_nos == null)
			return;

		for (int i = 0; i < code_nos.size(); i++)
			code_nos.get(i).isScaned = false;
	}

	public int getPalletsScaned() {

		int cnt = 0;

		if (code_nos == null)
			return 0;

		for (int i = 0; i < code_nos.size(); i++) {
			if (code_nos.get(i).isScaned)
				cnt++;
		}
		return cnt;
	}

	public boolean isAllPalletsScanned() {
		if (code_nos == null)
			return true;
		return getPalletsScaned() == code_nos.size();
	}

	// 部分扫描,(0,total)
	public boolean isPartiallyScaned() {
		if (code_nos == null)
			return false;

		int scaned_cnt=getPalletsScaned();
		return  scaned_cnt> 0 && scaned_cnt < code_nos.size();
	}

	public PalletBean getPallet(String palletNo) {
		if (TextUtils.isEmpty(palletNo))
			return null;
		if (code_nos == null)
			return null;
		for (int i = 0; i < code_nos.size(); i++) {
			if (palletNo.equals(code_nos.get(i).palletNo))
				return code_nos.get(i);
		}
		return null;
	}

	// 移除pallet
	public void removePallet(String palletNo) {
		if (TextUtils.isEmpty(palletNo))
			return;
		if (code_nos == null)
			return;

		for (int i = 0; i < code_nos.size(); i++) {
			if (palletNo.equals(code_nos.get(i).palletNo))
				code_nos.remove(i);
		}
	}

	// pallet数
	public int getPalletCnt() {
		return code_nos == null ? 0 : code_nos.size();
	}

	// -----static----------------------
	
	
	/**
	 * 获取第一个-部分扫描的order
	 * @param listOrders
	 * @return
	 */
	public static CheckInLoadOrderBean getOrder_PartiallyScaned(List<CheckInLoadOrderBean> listOrders){
		if(listOrders==null)
			return null;
		for (int i = 0; i < listOrders.size(); i++) {
			CheckInLoadOrderBean tmpOrder=listOrders.get(i);
			if(tmpOrder.isPartiallyScaned())
				return tmpOrder;
		}
		return null;
	}

	// 获取order,据palletNo
	public static CheckInLoadOrderBean getOrder_byPallet(
			List<CheckInLoadOrderBean> listOrders, String palletNo) {
		for (int i = 0; i < listOrders.size(); i++) {
			CheckInLoadOrderBean tmpOrder = listOrders.get(i);
			if (tmpOrder.getPallet(palletNo) != null)
				return tmpOrder;
		}
		return null;
	}

	// 恢复-已扫pallet-状态
	public static void restorePalletState(List<CheckInLoadOrderBean> listNew,
			List<CheckInLoadOrderBean> listOld) {
		for (int i = 0; i < listNew.size(); i++) {
			List<PalletBean> tmpPallets = listNew.get(i).code_nos;
			if (tmpPallets == null)
				continue;

			for (int j = 0; j < tmpPallets.size(); j++) {
				PalletBean tmpPallet = tmpPallets.get(j);
				tmpPallet.isScaned = hasScaned_pallet(listOld,
						tmpPallet.palletNo);
			}
		}
	}

	// 检测
	public static boolean hasScaned_pallet(
			List<CheckInLoadOrderBean> listOrders, String palletNo) {
		for (int i = 0; i < listOrders.size(); i++) {
			List<PalletBean> tmpPallets = listOrders.get(i).code_nos;
			if (tmpPallets == null)
				continue;
			for (int j = 0; j < tmpPallets.size(); j++) {
				PalletBean tmpPallet = tmpPallets.get(j);
				if (tmpPallet.palletNo == null)
					continue;
				if (tmpPallet.palletNo.equals(palletNo))
					return tmpPallet.isScaned;
			}
		}
		return false;
	}

	// 重置-打印状态
	public static void resetPrintState(List<CheckInLoadOrderBean> listOrders) {
		if (listOrders == null)
			return;
		for (int i = 0; i < listOrders.size(); i++)
			listOrders.get(i).isPrintSelected = false;
	}

	// ---

	public static void parseBeans(JSONArray ja, List<CheckInLoadOrderBean> list) {

		for (int i = 0; i < ja.length(); i++) {
			CheckInLoadOrderBean b = new CheckInLoadOrderBean();
			JSONObject jo = ja.optJSONObject(i);
			parseBean(jo, b);
			list.add(b);
		}

	}

	public static void parseBean(JSONObject jo, CheckInLoadOrderBean b) {
		b.od_status = jo.optString("od_status");
		b.orderno = jo.optString("orderno");
		b.pallets = jo.optInt("pallets");
		b.ml_status = jo.optString("ml_status");
		b.staging_area_id = jo.optString("staging_area_id");
		b.proNo = jo.optString("pro_no");
		b.note = jo.optString("note");
		b.case_total = jo.optString("case_total");
		b.externalID = jo.optString("externalid");
		b.accountID = jo.optString("accountid");

		if (TextUtils.isEmpty(b.staging_area_id))
			b.staging_area_id = jo.optString("stagingareaid");

		JSONArray jaCodes = jo.optJSONArray("code_nos");
		if (jaCodes == null || jaCodes.length() == 0)
			return;

		b.code_nos = new ArrayList<PalletBean>();
		PalletBean.parseBeans(jaCodes, b.code_nos, true);
	}

	// ==================nested===================================

	public static class PalletBean implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3349186250574634425L;

		// pallet-状态
		public static final int State_NotScaned = 0;
		public static final int State_Scaned = 1;
		public static final int State_Consolidated = 2;

		public String palletNo;
		public boolean isScaned = false;

		private int scan_flag; // 扫描状态(暂时不用该字段,用isScaned),取PalletBean.State_x

		public String scan_detail_id;

		// --------
		// public CheckInLoadPalletTypeBean type;
		private String typeId; // 强制用get_TypeId获取,可能有变更

		public String get_TypeId() {
			// if (type == null)
			// return "";
			// return type.pallettypeid;

			return typeId == null ? "" : typeId;
		}

		public void setTypeId(String typeId) {
			this.typeId = typeId;
		}

		// 已设置-类型
		public boolean hasType() {
			// return type!=null;
			return !TextUtils.isEmpty(typeId);
		}

		/**
		 * 由其他人创建,则不可"修改、移除"
		 * 
		 * @return
		 */
		public boolean isByOthers(String now_dlo) {
			return !TextUtils.isEmpty(scan_detail_id) && !scan_detail_id.equals(now_dlo);
		}

		// ==================================

		public static void parseBean(JSONObject jo, PalletBean b) {
			b.palletNo = jo.optString("code_no");
			b.typeId = jo.optString("pallet_type");
			b.scan_flag = jo.optInt("scan_flag");
			b.scan_detail_id = jo.optString("scan_detail_id");

			b.isScaned = b.scan_flag == State_Scaned;
		}

		// 入参:excludeConsolidated(true:排除consolidated掉的pallet)
		public static void parseBeans(JSONArray ja, List<PalletBean> list,
				boolean excludeConsolidated) {
			for (int i = 0; i < ja.length(); i++) {
				PalletBean b = new PalletBean();
				parseBean(ja.optJSONObject(i), b);
				// 排除-合并掉的
				if (excludeConsolidated && b.scan_flag == State_Consolidated)
					continue;
				list.add(b);
			}
		}

	}

}
