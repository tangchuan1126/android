package support.common.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;
import android.text.TextUtils;

public class DeliveryAdminUser implements Serializable {

	private static final long serialVersionUID = -6237267709569573002L;

	public String adid;
	public String account;
	public int llock;
	public String adgid;
	public String employe_name;
	public long ps_id;
	public String email;
	public String skype;
	public String msn;
	public int AreaId;
	public String mobilePhone;
	public String proQQ;
	public String proJsId;
	public String file_path;   // 头像Url
	public String adgid_name;

	public DeliveryAdminUser parent;// 部门
	public boolean childrenOrParent;// false：人员 true：部门

	public boolean isSelect = false;
	public String upperCase;

	public String tasks = "-1"; //任务数
	public int useronline = 0;
	

	public String getEmploye_name() {
		return employe_name;
	}

	public void setEmploye_name(String employeName) {
		employe_name = employeName;
		upperCase = TextUtils.isEmpty(employeName) ? employeName : employeName.toUpperCase().substring(0, 1);
	}
	
	public Object copyObject() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
		return ois.readObject();
	}

	public DeliveryAdminUser copyUser() {
		try {
			return (DeliveryAdminUser) this.copyObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 按照组分类
	 */
	public static List<List<DeliveryAdminUser>> getGroupData(List<DeliveryAdminUser> dataAll) {
		List<List<DeliveryAdminUser>> groupData = new ArrayList<List<DeliveryAdminUser>>();
		if (dataAll != null && !dataAll.isEmpty()) {
			String[] groupNames = getPalletNames(dataAll);
			for (int i = 0; i < groupNames.length; i++) {
				String groupName = groupNames[i];
				List<DeliveryAdminUser> list = getGroupData(dataAll, groupName);
				groupData.add(list);
			}
		}
		return groupData;
	}

	public static List<DeliveryAdminUser> getGroupData(List<DeliveryAdminUser> dataAll, String groupName) {
		List<DeliveryAdminUser> list = new ArrayList<DeliveryAdminUser>();
		for (DeliveryAdminUser user : dataAll) {
			if (groupName.equals(user.getAdgid_name())) {
				list.add(user);
			}
		}
		return list;
	}

	public static List<List<DeliveryAdminUser>> getGroupData_where(List<DeliveryAdminUser> dataAll, String groupName) {
		List<List<DeliveryAdminUser>> groupData = new ArrayList<List<DeliveryAdminUser>>();
		List<DeliveryAdminUser> list = new ArrayList<DeliveryAdminUser>();
		for (DeliveryAdminUser user : dataAll) {
			if (groupName.equals(user.getAdgid_name())) {
				list.add(user);
			}
		}
		groupData.add(list);
		return groupData;
	}

	/**
	 * String数组去重复值
	 */
	private static String[] removeDuplicateWithOrder(List<String> list) {
		Set<String> set = new HashSet<String>(list);
		return (String[]) set.toArray(new String[0]);
	}

	/**
	 * 取出不重复的组名
	 */
	public static String[] getPalletNames(List<DeliveryAdminUser> dataAll) {
		List<String> groupNames = new ArrayList<String>();
		for (DeliveryAdminUser user : dataAll) {
			if (TextUtils.isEmpty(user.getAdgid_name())) {
				user.setAdgid_name("Not Group");
			}
			groupNames.add(user.getAdgid_name());
		}
		// 去掉重复值
		return removeDuplicateWithOrder(groupNames);
	}

	public static void initState(List<DeliveryAdminUser> ls) {
		if (ls != null && !ls.isEmpty()) {
			for (DeliveryAdminUser l : ls) {
				l.isSelect = false;
			}
		}
	}

	public static void initStates(List<List<DeliveryAdminUser>> lss) {
		if (lss != null && !lss.isEmpty()) {
			for (List<DeliveryAdminUser> ls : lss) {
				initState(ls);
			}
		}
	}

	/**
	 * @Description:解析数据结构
	 * @param json
	 * @return
	 */
//	public static void handJSONSaveDB(JSONObject json, DeliveryAdminUserDaoImpl adminUserDaoImpl) {
//		List<DeliveryAdminUser> adminUsers = new ArrayList<DeliveryAdminUser>();
//		JSONArray jsonArrays = StringUtil.getJsonArrayFromJson(json, "data");
//		if (jsonArrays != null && jsonArrays.length() > 0) {
//			for (int i = 0; i < jsonArrays.length(); i++) {
//				JSONObject jsonItem = StringUtil.getJsonObjectFromArray(jsonArrays, i);
//				DeliveryAdminUser dItem = new DeliveryAdminUser();
//				dItem.setAdid(StringUtil.getJsonString(jsonItem, "adid"));
//				dItem.setAccount(StringUtil.getJsonString(jsonItem, "account"));
//				dItem.setAdgid(StringUtil.getJsonString(jsonItem, "adgid"));
//				dItem.setEmploye_name(StringUtil.getJsonString(jsonItem, "employe_name"));
//				dItem.setPs_id(StringUtil.getJsonLong(jsonItem, "ps_id"));
//				dItem.setEmail(StringUtil.getJsonString(jsonItem, "email"));
//				dItem.setSkype(StringUtil.getJsonString(jsonItem, "skype"));
//				dItem.setMsn(StringUtil.getJsonString(jsonItem, "msn"));
//				dItem.setAreaId(StringUtil.getJsonInt(jsonItem, "areaid"));
//				dItem.setMobilePhone(StringUtil.getJsonString(jsonItem, "mobilephone"));
//				dItem.setProQQ(StringUtil.getJsonString(jsonItem, "proqq"));
//				dItem.setProJsId(StringUtil.getJsonString(jsonItem, "projsid"));
//				dItem.setAdgid_name(StringUtil.getJsonString(jsonItem, "name"));
//				dItem.tasks = StringUtil.getJsonString(jsonItem, "tasks");
//				dItem.useronline = StringUtil.getJsonInt(jsonItem, "useronline");
//				adminUsers.add(dItem);
//			}
//			if (adminUsers != null && adminUsers.size() > 0) {
//				adminUserDaoImpl.deleteAdminUsers();
//			}
//			adminUserDaoImpl.saveOrUpdateAdminUsers(adminUsers);
//		}
//	}

	public static List<DeliveryAdminUser> handJSON(JSONObject json) {
		List<DeliveryAdminUser> adminUsers = new ArrayList<DeliveryAdminUser>();
		JSONArray jsonArrays = StringUtil.getJsonArrayFromJson(json, "data");
		if (jsonArrays != null && jsonArrays.length() > 0) {
			for (int i = 0; i < jsonArrays.length(); i++) {
				JSONObject jsonItem = StringUtil.getJsonObjectFromArray(jsonArrays, i);
				DeliveryAdminUser dItem = new DeliveryAdminUser();
				dItem.setAdid(StringUtil.getJsonString(jsonItem, "adid"));
				dItem.setAccount(StringUtil.getJsonString(jsonItem, "account"));
				dItem.setAdgid(StringUtil.getJsonString(jsonItem, "adgid"));
				dItem.setEmploye_name(StringUtil.getJsonString(jsonItem, "employe_name"));
				dItem.setPs_id(StringUtil.getJsonLong(jsonItem, "ps_id"));
				dItem.setEmail(StringUtil.getJsonString(jsonItem, "email"));
				dItem.setSkype(StringUtil.getJsonString(jsonItem, "skype"));
				dItem.setMsn(StringUtil.getJsonString(jsonItem, "msn"));
				dItem.setAreaId(StringUtil.getJsonInt(jsonItem, "areaid"));
				dItem.setMobilePhone(StringUtil.getJsonString(jsonItem, "mobilephone"));
				dItem.setProQQ(StringUtil.getJsonString(jsonItem, "proqq"));
				dItem.setProJsId(StringUtil.getJsonString(jsonItem, "projsid"));
				dItem.setAdgid_name(StringUtil.getJsonString(jsonItem, "name"));
				dItem.tasks = StringUtil.getJsonString(jsonItem, "tasks");
				dItem.useronline = StringUtil.getJsonInt(jsonItem, "useronline");
				
				// 头像url  字段: file_path
				dItem.setFile_path(StringUtil.getJsonString(jsonItem, "file_path"));
				adminUsers.add(dItem);
			}
		}
		return adminUsers;
	}

	public String getAdid() {
		return adid;
	}

	public void setAdid(String adid) {
		this.adid = adid;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getLlock() {
		return llock;
	}

	public void setLlock(int llock) {
		this.llock = llock;
	}

	public String getAdgid() {
		return adgid;
	}

	public void setAdgid(String adgid) {
		this.adgid = adgid;
	}

	public long getPs_id() {
		return ps_id;
	}

	public void setPs_id(long psId) {
		ps_id = psId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public int getAreaId() {
		return AreaId;
	}

	public void setAreaId(int areaId) {
		AreaId = areaId;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getProQQ() {
		return proQQ;
	}

	public void setProQQ(String proQQ) {
		this.proQQ = proQQ;
	}

	public String getProJsId() {
		return proJsId;
	}

	public void setProJsId(String proJsId) {
		this.proJsId = proJsId;
	}

	/**
	 * 获得头像url
	 * @return 头像url
	 */
	public String getFile_path() {
		return file_path;
	}

	/**
	 * 设置头像url
	 * @param 头像url
	 */
	public void setFile_path(String filePath) {
		if(!TextUtils.isEmpty(filePath)) {
			if(filePath.startsWith("/Sync10/")) {
				file_path = filePath.substring("/Sync10/".length());
				return;
			}
		} else if(filePath.startsWith("upload/account/")) {
			file_path = filePath;
		} else {
			file_path = "";
		}
	}

	public String getAdgid_name() {
		return adgid_name;
	}

	public void setAdgid_name(String adgidName) {
		adgid_name = adgidName;
	}

	/**
	 * @return the childrenOrParent
	 */
	public boolean isChildrenOrParent() {
		return childrenOrParent;
	}

	/**
	 * @param childrenOrParent
	 *            the childrenOrParent to set
	 */
	public void setChildrenOrParent(boolean childrenOrParent) {
		this.childrenOrParent = childrenOrParent;
	}

	/**
	 * @return the parent
	 */
	public DeliveryAdminUser getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(DeliveryAdminUser parent) {
		this.parent = parent;
	}

}
