package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;
import android.text.TextUtils;

public class CheckInBeanMain implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 313052287207382913L;
	
	public String resources_type;
	public String resources_name;
	
	public String entry_id;
	
	public String prompt;
	
	public List<CheckInTaskBeanMain> complexDoorList;
	
	
	//==================static===============================
	
	/**
	 * @Description:解析数据结构返回list数组
	 * @param @param json
	 * @param @return
	 */
	public static CheckInBeanMain handJsonForList(JSONObject json) {
		CheckInBeanMain bean = new CheckInBeanMain();
		List<CheckInTaskBeanMain> list = null;
		bean.entry_id = json.optString("entry_id");
		bean.resources_name = json.optString("resource_name");
		bean.resources_type = json.optString("resource_type");
		bean.prompt = json.optString("prompt");
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "equipments");
		if(jsonArray != null && jsonArray.length() > 0){
			list = new ArrayList<CheckInTaskBeanMain>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				if(item!=null){
					CheckInTaskBeanMain b = new CheckInTaskBeanMain();
					b.resources_type = item.optInt("resources_type");
					b.equipment_type = item.optInt("equipment_type");
					b.equipment_number = item.optString("equipment_number");
					b.resources_name = item.optString("resources_name");
					b.resources_id = item.optInt("resources_id");
					b.occupy_status = item.optInt("occupy_status");
					
					//debug,若设备无效 则不加入
					if(TextUtils.isEmpty(b.equipment_number))
						continue;
					
					list.add(b);
				}
			}
			bean.complexDoorList = list;
		}
		return bean;
	}
	
}
