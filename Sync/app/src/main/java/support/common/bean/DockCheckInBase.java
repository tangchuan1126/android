package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.ui.load_receive.assign_task.adapter.DockCheckInAssignAdapter;
import support.key.CheckInMainDocumentsRelTypeKey;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: NewDockCheckInBase 
 * @Description: 
 * @author gcy
 * @date 2014-9-1 下午12:30:00
 */
public class DockCheckInBase implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 5397544719629054994L;
	
	private String entryId;
	private String in_seal;
	private String out_seal;
	private boolean showInSeal;//true显示inseal false不显示
	private boolean showOutSeal;//true显示outseal false不显示
	private int rel_type ;//判断seal的显示
	
	private List<DockCheckInDoorBase> treeList;
	private List<DockCheckInScheduleBase> scheduleList;
	
	
	
	public int equipment_status;//": 2,
	public String equipment_purpose;//": 1,
	public int resources_id;//": 47,
	public String resources_name;//: "34",
	public int resources_type;//": 1,
	public int occupy_status;
	
	/******************************************************/
	/**
	 * @Description:判断当前entryId是否是关闭状态
	 * @param @return true为可用状态 false为不可用状态
	 */
	public static boolean determineEntryId(JSONObject json){
		JSONObject jsonObject = json.optJSONObject("data");
		if(jsonObject!=null){
			int status = jsonObject.optInt("status");
			if(status==5 || status==10 ){
				return false;
			}
		}
		return true;
	}

	public static DockCheckInBase parsingJSON(JSONObject json,String entryId){
		DockCheckInBase base = new DockCheckInBase();
		base.setEntryId(entryId);
		JSONObject jsonObject = StringUtil.getJsonObjectFromJSONObject(json, "data");
		if(jsonObject!=null){
			base.setIn_seal(jsonObject.optString("in_seal"));
			base.setOut_seal(jsonObject.optString("out_seal"));
			base.setRel_type(jsonObject.optInt("rel_type"));
			if(base.getRel_type()==CheckInMainDocumentsRelTypeKey.NONE){
				base.setShowInSeal(true);
				base.setShowOutSeal(true);
			}else{
				boolean inSealFlag = base.getRel_type()==CheckInMainDocumentsRelTypeKey.BOTH||base.getRel_type()==CheckInMainDocumentsRelTypeKey.DELIVERY;
				base.setShowInSeal(inSealFlag);
				boolean outSealFlag = base.getRel_type()==CheckInMainDocumentsRelTypeKey.BOTH||base.getRel_type()==CheckInMainDocumentsRelTypeKey.PICK_UP;
				base.setShowOutSeal(outSealFlag);
			}
			//------------------解析 equipment
			parsingJSONForEquipment(jsonObject,base);
			
			base.setTreeList(parsingJSONForDoor(jsonObject,base));
			base.setScheduleList(parsingJSONScheduleBase(jsonObject,base));
			
			
		}
		return base;
	}

	public static void parsingJSONForEquipment(JSONObject jsonObject,DockCheckInBase base){
		JSONObject eqJson = jsonObject.optJSONObject("equipment");
		if(eqJson!=null){
			base.equipment_status = eqJson.optInt("equipment_status");
			base.equipment_purpose = eqJson.optString("equipment_purpose");
			base.resources_id = eqJson.optInt("resources_id");
			base.resources_name = eqJson.optString("resources_name");
			base.resources_type = eqJson.optInt("resources_type");
			base.occupy_status = eqJson.optInt("occupy_status");
		}
	}
	
	public static List<DockCheckInDoorBase> parsingJSONForDoor(JSONObject jsonObject,DockCheckInBase newDockCheckInBase){
		List<DockCheckInDoorBase> list = new ArrayList<DockCheckInDoorBase>();
		JSONArray jsonArrays = jsonObject.optJSONArray("tree");
		if(!StringUtil.isNullForJSONArray(jsonArrays)){
			for(int i=0; i<jsonArrays.length();i++){
				JSONObject jsonItem = jsonArrays.optJSONObject(i);
				if(jsonItem!=null){
					DockCheckInDoorBase n = new DockCheckInDoorBase();
					n.setResources_id(jsonItem.optInt("resources_id"));
					n.setResources_type(jsonItem.optInt("resources_type"));
					n.setResources_type_value(jsonItem.optString("resources_type_value"));
					n.setLoadList(parsingJSONForLoadList(jsonItem,n));
					list.add(n);
				}
			}
		}
		return list;
	}
	
	public static List<DockCheckInScheduleBase> parsingJSONScheduleBase(JSONObject jsonObject,DockCheckInBase newDockCheckInBase){
		List<DockCheckInScheduleBase> list = new ArrayList<DockCheckInScheduleBase>();
		JSONArray jsonArrays = jsonObject.optJSONArray("schedule");
		if(!StringUtil.isNullForJSONArray(jsonArrays)){
			for(int i=0; i<jsonArrays.length();i++){
				JSONObject jsonItem = jsonArrays.optJSONObject(i);
				if(jsonItem!=null){
					DockCheckInScheduleBase n = new DockCheckInScheduleBase();
					n.setSchedule_detail(jsonItem.optString("schedule_detail"));
		    		n.setSchedule_id(jsonItem.optString("schedule_id"));
		    		n.setAdid(jsonItem.optString("adid"));
		    		n.setEmploye_name(jsonItem.optString("employe_name"));
		    		n.setSchedule_is_note(jsonItem.optString("schedule_is_note"));
		    		n.setSms_short_notify(jsonItem.optString("sms_short_notify"));
		    		n.setSms_email_notify(jsonItem.optString("sms_email_notify"));
		    		n.setAssociate_id(jsonItem.optString("associate_id"));
					list.add(n);
				}
			}
		}
		return list;
	}
	
	public static List<DockCheckInDoorForLoadListBase> parsingJSONForLoadList(JSONObject jsonObject,DockCheckInDoorBase d){
		List<DockCheckInDoorForLoadListBase> list = new ArrayList<DockCheckInDoorForLoadListBase>();
		JSONArray jsonArrays = jsonObject.optJSONArray("load_list");
		
		List<DockCheckInDoorForLoadListBase> hasCloseList = new ArrayList<DockCheckInDoorForLoadListBase>();//用于排序 添加已经关闭过的数据
		
		if(!StringUtil.isNullForJSONArray(jsonArrays)){
			int flagNum = 0;//定义已经指派人员执行task的数量 用于判断改load下 是否有执行的task
			int notHaveCB = 0;//定义不能显示checkBox的数量 用于判断
			for(int i=0; i<jsonArrays.length();i++){
				JSONObject jsonItem = jsonArrays.optJSONObject(i);
				if(jsonItem!=null){
					DockCheckInDoorForLoadListBase n = new DockCheckInDoorForLoadListBase();
					n.setId(jsonItem.optString("id"));
					n.setPo_no(jsonItem.optString("po_no"));
					n.setOrder_no(jsonItem.optString("order_no"));
					n.setDlo_detail_id(jsonItem.optString("dlo_detail_id"));
					n.setNumber_type(jsonItem.optInt("number_type"));
					n.setSchedule_id(jsonItem.optString("schedule_id"));
					n.setCompany_id(jsonItem.optString("company_id"));
					n.setCustomer_id(jsonItem.optString("customer_id"));
					n.setType(jsonItem.optInt("type"));
					n.setSrr_id(jsonItem.optString("srr_id"));
					n.setEquipment_id(jsonItem.optString("equipment_id"));
					n.setExecute_user(jsonItem.optString("execute_user"));
					n.setNumber_status(jsonItem.optInt("number_status"));
					if(!StringUtil.isNullOfStr(n.getExecute_user())){//||!DockCheckInAssignAdapter.judgeShowCheckBox(n.getNumber_status())){
						flagNum++;
					}
					
					//-----已经关闭的添加在 hasCloseList里
					if(!DockCheckInAssignAdapter.judgeTaskNoClose(n.getNumber_status())){
						hasCloseList.add(n);
						notHaveCB++;
					}else{
						if(!StringUtil.isNullOfStr(n.getExecute_user())){//||!DockCheckInAssignAdapter.judgeShowCheckBox(n.getNumber_status())){
							list.add(n);//已经指派的人添加在后面
							flagNum++;
						}else{
							list.add(0,n);//未指派的人添加在前面
						}
					}
					
				}
			}
			
			//解析后配置完整数据 未指派的人添加在前面 已经指派的人添加在后面 已经关闭的在最后面
			list.addAll(hasCloseList);
			
			if(!Utility.isNullForList(list) && list.size() != notHaveCB && flagNum>0 /*flagNum==(list.size())*/){
				d.setUpdate(true);
			}
			if(!Utility.isNullForList(list)&&(list.size() - notHaveCB)==1){
				d.setSelected(true);
				d.setHaveSelected(true);
				for(int i=0;i<list.size();i++){
					if(DockCheckInAssignAdapter.judgeTaskNoClose(list.get(i).getNumber_status())){
						list.get(i).setSelect(true);
					}
				}
			}
		}
		return list;
	}
	
	/******************************************************/
	
	
	public String getIn_seal() {
		return in_seal;
	}
	public void setIn_seal(String in_seal) {
		this.in_seal = in_seal;
	}
	public String getOut_seal() {
		return out_seal;
	}
	public void setOut_seal(String out_seal) {
		this.out_seal = out_seal;
	}
	public boolean isShowInSeal() {
		return showInSeal;
	}
	public void setShowInSeal(boolean showInSeal) {
		this.showInSeal = showInSeal;
	}
	public boolean isShowOutSeal() {
		return showOutSeal;
	}
	public void setShowOutSeal(boolean showOutSeal) {
		this.showOutSeal = showOutSeal;
	}

	public List<DockCheckInDoorBase> getTreeList() {
		return treeList;
	}

	public void setTreeList(
			List<DockCheckInDoorBase> newDockCheckInDoorBaseList) {
		this.treeList = newDockCheckInDoorBaseList;
	}

	public List<DockCheckInScheduleBase> getScheduleList() {
		return scheduleList;
	}

	public void setScheduleList(
			List<DockCheckInScheduleBase> dockCheckInScheduleBaseList) {
		this.scheduleList = dockCheckInScheduleBaseList;
	}

	public String getEntryId() {
		return entryId;
	}

	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}

	public int getRel_type() {
		return rel_type;
	}

	public void setRel_type(int rel_type) {
		this.rel_type = rel_type;
	}
	
	
	
}
