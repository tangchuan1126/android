package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.ui.yardcontrol.patrol_spot.bean.RtAreaResouseBean;
import utility.StringUtil;
import utility.Utility;

/**
 * @ClassName: RtEquipment 
 * @Description: patrol的设备信息bean
 * @author gcy
 * @date 2014-11-25 下午2:58:48
 */

public class RtEquipment implements Serializable{
	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = -6265396850038554093L;
	public String equipment_number;
	public String equipment_id;
	public int equipment_type;
	
    public String check_in_entry_id;	//------|
    public String resources_id;			//		|
    public int resources_type;			//		|->是对resouse进行修改时查询出来的设备的基础bean
    public String location;				//		|
    public String equipment_flag;		//		|
    public boolean checkflag = true;			//------|判断是否点击默认为true已点击
    
    
    public static Map<String,List<RtEquipment>> handJsonArray(JSONObject json){
		Map<String,List<RtEquipment>> map = new HashMap<String, List<RtEquipment>>();
		JSONArray jsonArray = json.optJSONArray("data");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.optJSONObject(i);
				if(jsonObject!=null){
					String entry_id = jsonObject.optString("check_in_entry_id");
					if(Utility.isNullForList(map.get(entry_id))){
						map.put(entry_id, new ArrayList<RtEquipment>());
					}
					
					RtEquipment r = new RtEquipment();
					r.check_in_entry_id = entry_id;
					r.equipment_id = jsonObject.optString("equipment_id");
					r.equipment_number = jsonObject.optString("equipment_number");
					r.equipment_type = jsonObject.optInt("equipment_type");
					r.location = jsonObject.optString("location");
					r.resources_id = jsonObject.optString("resources_id");
					r.resources_type = jsonObject.optInt("resources_type");	
					map.get(entry_id).add(r);
				}
			}
		}		
		return map;
	}
    
    /**
     * @Description:获取选中的数据如果为true 表示有选中的数据 否则相反
     * @param @param map
     * @param @param doorObject
     * @param @param doorBean
     * @param @return
     */
    public static boolean getSelectJson(Map<String,List<RtEquipment>> map,JSONObject doorObject,RtAreaResouseBean doorBean){
    	int selectNum = 0;
    	try {
			Set<String> key = map.keySet();
			Iterator<String> it = key.iterator();
	        while(it.hasNext()) {
	        	final String entry_id = (String) it.next();
	        	//引用外部UI填充主控件
		        
		        //根据当前key值获取map的value值
		        List<RtEquipment> list = map.get(entry_id);

		        if(!Utility.isNullForList(list)){
					JSONArray equipmentArray = new JSONArray();
					for(int i=0;i<list.size();i++){
						RtEquipment bean = list.get(i);
						if(bean.checkflag){
							JSONObject tractorObject = new JSONObject();
							tractorObject.put("equipment_id", bean.equipment_id);
							tractorObject.put("check_in_entry_id", bean.check_in_entry_id);
							tractorObject.put("search_resources_type", bean.resources_type+"");
							selectNum++;
							equipmentArray.put(tractorObject);
						}
					}
					if(!StringUtil.isNullForJSONArray(equipmentArray)){
						doorObject.put("equipments", equipmentArray);
						doorObject.put("resource_id", doorBean.resource_id);
						doorObject.put("resource_type", doorBean.resource_type);
					}
				}
		    }
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	if(selectNum==0){
    		return false;
    	}
    	return true;
    }
}
