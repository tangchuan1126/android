package support.common.bean;

import java.io.Serializable;

import utility.StringUtil;
/**
 * 
 * @ClassName: CheckInDockBaseType 
 * @Description:
 * @author gcy 
 * @date 2014年5月20日 下午8:32:08 
 *
 */
public class DockCheckInScheduleBase implements Serializable {
	/**
	 * @Fields serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/*****************************************/
	private String schedule_detail;
	private String schedule_id;
	private String adid;
	private String employe_name;
	private String schedule_is_note;
	private String sms_short_notify;
	private String sms_email_notify;
	private String associate_id;
	private String Delivery;
	private String DOOR;
	private String Note;
	/*****************************************/
	/**
	 * @return the schedule_detail
	 */
	public String getSchedule_detail() {
		return schedule_detail;
	}
	/**
	 * @param schedule_detail the schedule_detail to set
	 */
	public void setSchedule_detail(String schedule_detail) {
		schedule_detail = schedule_detail.toUpperCase();
		this.schedule_detail = schedule_detail;
		if(!StringUtil.isNullOfStr(schedule_detail)){
			if(schedule_detail.indexOf("TO")>=0){
				setDelivery(schedule_detail.substring(0, schedule_detail.indexOf("TO")));
			}
			if(schedule_detail.indexOf("DOOR")>=0&&schedule_detail.indexOf("NOTE")>=0){
				setDOOR(schedule_detail.substring(schedule_detail.indexOf("DOOR"), schedule_detail.indexOf("NOTE")));
			}
			if(schedule_detail.indexOf("NOTE")>=0&&schedule_detail.indexOf("NOTE")<schedule_detail.length()){
				setNote(schedule_detail.substring(schedule_detail.indexOf("NOTE"), schedule_detail.length()));
			}
		}
	}
	/**
	 * @return the schedule_id
	 */
	public String getSchedule_id() {
		return schedule_id;
	}
	/**
	 * @param schedule_id the schedule_id to set
	 */
	public void setSchedule_id(String schedule_id) {
		this.schedule_id = schedule_id;
	}
	/**
	 * @return the adid
	 */
	public String getAdid() {
		return adid;
	}
	/**
	 * @param adid the adid to set
	 */
	public void setAdid(String adid) {
		this.adid = adid;
	}
	/**
	 * @return the employe_name
	 */
	public String getEmploye_name() {
		return employe_name;
	}
	/**
	 * @param employe_name the employe_name to set
	 */
	public void setEmploye_name(String employe_name) {
		this.employe_name = employe_name;
	}
	/**
	 * @return the schedule_is_note
	 */
	public String getSchedule_is_note() {
		return schedule_is_note;
	}
	/**
	 * @param schedule_is_note the schedule_is_note to set
	 */
	public void setSchedule_is_note(String schedule_is_note) {
		this.schedule_is_note = schedule_is_note;
	}
	/**
	 * @return the sms_short_notify
	 */
	public String getSms_short_notify() {
		return sms_short_notify;
	}
	/**
	 * @param sms_short_notify the sms_short_notify to set
	 */
	public void setSms_short_notify(String sms_short_notify) {
		this.sms_short_notify = sms_short_notify;
	}
	/**
	 * @return the sms_email_notify
	 */
	public String getSms_email_notify() {
		return sms_email_notify;
	}
	/**
	 * @param sms_email_notify the sms_email_notify to set
	 */
	public void setSms_email_notify(String sms_email_notify) {
		this.sms_email_notify = sms_email_notify;
	}
	/**
	 * @return the associate_id
	 */
	public String getAssociate_id() {
		return associate_id;
	}
	/**
	 * @param associate_id the associate_id to set
	 */
	public void setAssociate_id(String associate_id) {
		this.associate_id = associate_id;
	}
	/**
	 * @return the delivery
	 */
	public String getDelivery() {
		return Delivery;
	}
	/**
	 * @param delivery the delivery to set
	 */
	public void setDelivery(String delivery) {
		Delivery = delivery;
	}
	/**
	 * @return the dOOR
	 */
	public String getDOOR() {
		return DOOR;
	}
	/**
	 * @param dOOR the dOOR to set
	 */
	public void setDOOR(String dOOR) {
		DOOR = dOOR;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return Note;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		Note = note;
	}
	
	
	
}
