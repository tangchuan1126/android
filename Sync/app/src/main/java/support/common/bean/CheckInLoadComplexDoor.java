package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utility.StringUtil;

/**
 * 复合-load列表
 * 
 * @author 朱成
 * @date 2014-8-13
 */
public class CheckInLoadComplexDoor implements Serializable {

	private static final long serialVersionUID = 2414475136023096088L;

	public String entryID;
	public String resources_name;
	public int resources_type;
	public int resources_id;
	public String out_seal;

	public List<CheckInLoadLoadBean> listLoads;
	public List<CheckInLoadLoadBean> nolistLoads;

	// ---
	public boolean isLoad = false; // "单条load"转成的bean,用于"高亮load"

	// --------------------------------
	
	//所有loads均已关闭
	public boolean isAllLoadsClosed(){
		
		if(listLoads==null)
			return true;
		
		for (int i = 0; i < listLoads.size(); i++) {
			if(!"Closed".equalsIgnoreCase(listLoads.get(i).status))
				return false;
		}
		return true;
	}

	// jiang
	public static void parseBean2(JSONObject joEntry,
			List<CheckInLoadComplexDoor> complexDoorList) {

		JSONObject jo = joEntry.optJSONObject("entry");
		if (jo == null)
			return;

		String entryId = StringUtil.getJsonString(jo, "entry_id");
		String out_seal = StringUtil.getJsonString(jo, "out_seal");

		JSONArray ja = jo.optJSONArray("details");

		// 解析-doors
		for (int i = 0; i < ja.length(); i++) {
			List<CheckInLoadLoadBean> details = new ArrayList<CheckInLoadLoadBean>();
			List<CheckInLoadLoadBean> nodetails = new ArrayList<CheckInLoadLoadBean>();
			JSONObject item = StringUtil.getJsonObjectFromArray(ja, i);
			String resources_name = StringUtil.getJsonString(item, "resources_name");
			int resources_type = StringUtil.getJsonInt(item, "resources_type");
			int resources_id = StringUtil.getJsonInt(item, "resources_id");
			JSONArray loadJa = StringUtil.getJsonArrayFromJson(item, "loads");
			
			CheckInLoadComplexDoor complexDoor = new CheckInLoadComplexDoor();
			complexDoor.resources_name = resources_name;
			complexDoor.resources_type = resources_type;
			complexDoor.resources_id = resources_id;
			complexDoor.entryID = entryId;

			//解析-loads
			CheckInLoadLoadBean.parseBeans(loadJa, details,nodetails);

			complexDoor.listLoads = details;
			complexDoor.nolistLoads = nodetails;
			
			if(complexDoor.nolistLoads.size() < complexDoor.listLoads.size()){
				complexDoorList.add(0,complexDoor);
			}else{
				complexDoorList.add(complexDoor);
			}
		}
	}

}
