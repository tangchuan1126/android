package support.common.bean;

import java.io.Serializable;

public class CheckInTaskMainItem implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 313052287207382913L;
	
	public int equipment_status;				 
	public int equipment_type;	
	public int equipment_purpose;
	public String equipment_number;
	
}