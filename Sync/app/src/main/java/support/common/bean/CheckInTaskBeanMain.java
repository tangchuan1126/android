package support.common.bean;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import oso.ui.load_receive.do_task.main.bean.Load_barsBean;
import oso.ui.load_receive.do_task.main.bean.Load_useBean;
import oso.widget.loadbar.FreightTermBean;
import support.key.CheckInMainDocumentsRelTypeKey;
import support.key.CheckInMainDocumentsStatusTypeKey;
import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import support.key.OccupyTypeKey;
import utility.StringUtil;

/**
 * 门级对象,含任务列表
 * @author 朱成
 * @date 2015-1-14
 */
public class CheckInTaskBeanMain implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 313052287207382913L;
	
	public int equipment_type;				//设备类型/类型名称、设备名(用于显示)、id
	public String equipment_type_value;	
	public String equipment_number;
	public int equipment_id;
	public int equipment_status;			//
	
	public int resources_type;
	
	@Deprecated
	public String resources_type_value;		//如:Door,有时可能无   '改用getResTypeStr
	
	public String resources_name;
	public int resources_id;
	
	public int occupy_status;
	
	public int showfinish;
	
	public int is_forget_close;
	
	public int total_task;					//总任务数、已完成数
	public int finish_task;
	
	public int rel_type;//设备状态
	//------------------------new add---gcy-----------------------------
	public List<Load_useBean> load_UseList;
//	public List<String> photoList;
	public List<CheckInTaskItemBeanMain> complexDoorList;
	public List<Load_barsBean> load_barsList;
	public String in_seal;
	public String out_seal;
	public String entry_id;
	
	
	//可改门
	public boolean canChangeDoor(){
		return equipment_status!=CheckInMainDocumentsStatusTypeKey.LEFT;
	}
	
	public boolean hasInSeal(){
		return CheckInMainDocumentsRelTypeKey.getSealType(rel_type)!=CheckInMainDocumentsRelTypeKey.SealType_Out;
	}
	
	public boolean hasOutSeal(){
		return CheckInMainDocumentsRelTypeKey.getSealType(rel_type)!=CheckInMainDocumentsRelTypeKey.SealType_In;
	}
	
	/**
	 * 用于传参
	 */
	public void setResTypeValue_byTypeID(Context c){
		resources_type_value=OccupyTypeKey.getOccupyTypeKeyName(c,resources_type);
	}
	
	/**
	 * 资源类型,如:Door
	 * @return
	 */
	public String getResTypeStr(Context c){
		return OccupyTypeKey.getOccupyTypeKeyName(c,resources_type);
	}
	
	/**
	 * 提取freightTerms
	 * @return
	 */
	public List<FreightTermBean> getFreightTerms(){
		if(complexDoorList==null||complexDoorList.size()==0)
			return null;
		
		List<FreightTermBean> listFt=new ArrayList<FreightTermBean>();
		for (int i = 0; i < complexDoorList.size(); i++) {
			CheckInTaskItemBeanMain task=complexDoorList.get(i);
			if(!task.hasFreightTerm())
				continue;
			FreightTermBean ft=new FreightTermBean();
			ft.taskType=task.getTaskTypeName();
			ft.taskNo=task.number;
			ft.freightTerm=task.freight_term;
			listFt.add(ft);
		}
		return listFt;
	}
	
	//==================static===============================
	
	/**
	 * @Description:解析数据结构返回list数组
	 * @param @param json
	 * @param @return
	 */
	public static List<CheckInTaskBeanMain> handJsonForList(JSONObject json) {
		List<CheckInTaskBeanMain> list = null;
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "data");
		if(jsonArray != null && jsonArray.length() > 0){
			list = new ArrayList<CheckInTaskBeanMain>();
			// 解析-doors
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				if(item!=null){
					CheckInTaskBeanMain b = new CheckInTaskBeanMain();
					b.equipment_type_value = item.optString("equipment_type_value");
					b.resources_id = item.optInt("resources_id");
					b.resources_type_value = item.optString("resources_type_value");
					b.resources_type = item.optInt("resources_type");
					b.equipment_type = item.optInt("equipment_type");
					b.total_task = item.optInt("total_task");
					b.finish_task = item.optInt("finish_task");
					b.equipment_number = item.optString("equipment_number");
					b.equipment_status = item.optInt("equipment_status");
					b.resources_name = item.optString("resources_name");
					b.equipment_id = item.optInt("equipment_id");
					b.is_forget_close = item.optInt("is_forget_close");
					b.rel_type = item.optInt("rel_type");
					list.add(b);
				}
			}
		}
		return list;
	}
	public static List<CheckInTaskBeanMain> handJsonEquipmentsList(JSONObject json) {
		List<CheckInTaskBeanMain> list = null;
		JSONArray jsonArray = StringUtil.getJsonArrayFromJson(json, "equipments");
		if(jsonArray != null && jsonArray.length() > 0){
			list = new ArrayList<CheckInTaskBeanMain>();
			// 解析-doors
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.optJSONObject(i);
				if(item!=null){
					CheckInTaskBeanMain b = new CheckInTaskBeanMain();
					b.equipment_type_value = item.optString("equipment_type_value");
					b.resources_id = item.optInt("resources_id");
					b.resources_type_value = item.optString("resources_type_value");
					b.resources_type = item.optInt("resources_type");
					b.equipment_type = item.optInt("equipment_type");
					b.equipment_number = item.optString("equipment_number");
					b.resources_name = item.optString("resources_name");
					b.equipment_id = item.optInt("equipment_id");
					b.equipment_status = item.optInt("equipment_status");
					b.occupy_status = item.optInt("occupy_status");
					b.total_task = item.optInt("total_task");
					b.rel_type = item.optInt("rel_type");
					list.add(b);
				}
			}
		}
		return list;
	}
	
	/**
	 * @Description:解析数据结构 填充Bean对象
	 * @param @param json
	 * @param @return
	 */
	public static void handJson(JSONObject json,CheckInTaskBeanMain bean) {
		
		bean.complexDoorList = CheckInTaskBeanMain.parseCheckInTaskItemBeanMainList(json);
		bean.load_UseList = CheckInTaskBeanMain.parseLoad_use(json);
		bean.load_barsList = CheckInTaskBeanMain.parseLoad_barsBean(json);
		bean.showfinish = json.optInt("showfinish");
		JSONObject equimentObj = json.optJSONObject("equiment");
		if(equimentObj!=null){
			bean.in_seal = equimentObj.optString("in_seal");
			bean.out_seal = equimentObj.optString("out_seal");
			bean.equipment_status=equimentObj.optInt("equipment_status");
			bean.rel_type=equimentObj.optInt("rel_type");
		}
	}
	
	private static List<CheckInTaskItemBeanMain> parseCheckInTaskItemBeanMainList(JSONObject json){
		JSONArray datasArray = json.optJSONArray("datas");
		List<CheckInTaskItemBeanMain> list = null;
		if (!StringUtil.isNullForJSONArray(datasArray)) {
			list = new ArrayList<CheckInTaskItemBeanMain>();
			// 解析-doors
			for (int i = 0; i < datasArray.length(); i++) {
				JSONObject item = datasArray.optJSONObject(i);
				// 解析-loads
				if(item!=null){
					CheckInTaskItemBeanMain complexDoor = new CheckInTaskItemBeanMain();
					CheckInTaskItemBeanMain.parseBean(item, complexDoor);
					list.add(complexDoor);
				}
			}
		}
		return list;
	}
	
	public static List<Load_useBean> parseLoad_use(JSONObject json){
		List<Load_useBean> list = null;
		JSONArray jsonArray = json.optJSONArray("load_use");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			list = new ArrayList<Load_useBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.optJSONObject(i);
				if(jsonObject!=null){
					Load_useBean bean = new Load_useBean();
					bean.count = jsonObject.optInt("count");
					bean.load_bar_id = jsonObject.optInt("load_bar_id");
					bean.load_bar_name = jsonObject.optString("load_bar_name");
					list.add(bean);
				}
			}
		}
		return list;
	}
	
	public static List<Load_barsBean> parseLoad_barsBean(JSONObject json){
		List<Load_barsBean> list = null;
		JSONArray jsonArray = json.optJSONArray("load_bars");
		if(!StringUtil.isNullForJSONArray(jsonArray)){
			list = new ArrayList<Load_barsBean>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.optJSONObject(i);
				if(jsonObject!=null){
					Load_barsBean bean = new Load_barsBean();
					bean.load_bar_id = jsonObject.optInt("load_bar_id");
					bean.load_bar_name = jsonObject.optString("load_bar_name");
					list.add(bean);
				}
			}
		}
		return list;
	}

	/**
	 * 其他任务有BOL/CTNR 不允许的Close的情况
	 * @param mCheckedList
	 * @param itemBean
	 * @return
	 */
	public boolean getOtherNeedClose(List<CheckInTaskItemBeanMain> mCheckedList, CheckInTaskItemBeanMain itemBean) {
		boolean shouldClose = false;

		for (int i = 0; i < complexDoorList.size(); i ++) {
			CheckInTaskItemBeanMain currBean = complexDoorList.get(i);

			// 若是未选中的 则略过
			if (!mCheckedList.get(i).checked) {
				continue;
			}

			// 判断非自己的其他任务
			if (currBean != itemBean) {

				// 如果任务是Delivery或者PickUp的 则应该显示Close按钮
				/*if (!(currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_BOL || currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_CTN
						|| currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER || currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PONO
						|| currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD || currBean.isSmallParcel())) {
					shouldClose = true;
					return shouldClose;
				}*/

				if (currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS || currBean.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS) {

					if (currBean.number_status == EntryDetailNumberStateKey.Processing) {
						shouldClose = true;
						return shouldClose;
					}
				}

			}
		}
		return shouldClose;
	}


}
