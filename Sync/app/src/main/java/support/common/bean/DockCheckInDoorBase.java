package support.common.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: DockCheckInDoorBase 
 * @Description: 
 * @author gcy
 * @date 2014-9-1 下午12:38:06
 */
public class DockCheckInDoorBase implements Serializable {

	/** 
	 * @Fields serialVersionUID : 
	 */ 
	private static final long serialVersionUID = 9162222621782379371L;
	
	private int resources_id;//门ID
	private int resources_type;
	private String resources_type_value;//门名称
	private boolean isSelected;
	private boolean haveSelected;//判断子列表是否有选中的
	private boolean update;//判断该资源下 是否都已经被指派  false 为未指派   true为以指派
	private List<DockCheckInDoorForLoadListBase> loadList;
	
	public String getResources_type_value() {
		return resources_type_value;
	}
	public void setResources_type_value(String door_name) {
		this.resources_type_value = door_name;
	}
	public int getResources_id() {
		return resources_id;
	}
	public void setResources_id(int door_id) {
		this.resources_id = door_id;
	}
	public List<DockCheckInDoorForLoadListBase> getLoadList() {
		return loadList;
	}
	public void setLoadList(
			List<DockCheckInDoorForLoadListBase> newDockCheckInDoorForLoadListBaseList) {
		this.loadList = newDockCheckInDoorForLoadListBaseList;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public boolean isHaveSelected() {
		return haveSelected;
	}
	public void setHaveSelected(boolean haveSelected) {
		this.haveSelected = haveSelected;
	}
	public int getResources_type() {
		return resources_type;
	}
	public void setResources_type(int resources_type) {
		this.resources_type = resources_type;
	}
	public boolean isUpdate() {
		return update;
	}
	public void setUpdate(boolean update) {
		this.update = update;
	}	
}
