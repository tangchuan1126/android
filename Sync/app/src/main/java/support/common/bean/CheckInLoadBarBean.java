package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
 
public class CheckInLoadBarBean implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 629594356769983048L;
	private int load_bar_id;				//load_bar id
	private String load_bar_name = "";	//load_bar name
	private String loadber_numner;
	
	public int getLoad_bar_id() {
		return load_bar_id;
	}
	public void setLoad_bar_id(int load_bar_id) {
		this.load_bar_id = load_bar_id;
	}
	public String getLoad_bar_name() {
		return load_bar_name;
	}
	public void setLoad_bar_name(String load_bar_name) {
		this.load_bar_name = load_bar_name;
	}
	
	//=======static==============================
	
	//提取"name列表",第一条加上NA
	public static List<String> getLoadBar_Strs(List<CheckInLoadBarBean> list) {
		List<String> listStrs = new ArrayList<String>();
		listStrs.add("NA");
		if (list == null)
			return listStrs;

		for (int i = 0; i < list.size(); i++)
			listStrs.add(list.get(i).getLoad_bar_name());
		return listStrs;
	}
	
	
	//========parse=============================

	public String getLoadber_numner() {
		return loadber_numner;
	}
	public void setLoadber_numner(String loadber_numner) {
		this.loadber_numner = loadber_numner;
	}
	public static void parseBean(JSONObject jo, CheckInLoadBarBean b) {
		b.setLoad_bar_id(jo.optInt("load_bar_id"));
		b.setLoad_bar_name(jo.optString("load_bar_name"));
	}

	public static void parseBeans(JSONArray ja, List<CheckInLoadBarBean> list) {
		for (int i = 0; i < ja.length(); i++) {
			CheckInLoadBarBean tmpB = new CheckInLoadBarBean();
			parseBean(ja.optJSONObject(i), tmpB);
			list.add(tmpB);
		}
	}





	
}
