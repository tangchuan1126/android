package support.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.ui.load_receive.do_task.load.scan.ScanLoadActivity;
import support.common.bean.CheckInLoadOrderBean.PalletBean;
import utility.Utility;
import android.text.TextUtils;

/**
 * 子单据
 * 
 * @author 朱成
 * @date 2014-8-21
 */
public class CheckInLoad_SubLoadBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1547934970785321903L;
	
	public static final int MBolStatus_Undefined = 0;
	public static final int MBolStatus_Open = 1;
	public static final int MBolStatus_Closed = 2;// 已关闭(客户服务器上的条目,防止重复关闭),我们的服务器不一定关闭

	public String load_no;
	public long master_bol_no;
	public String staging_area_id;
	public int total_pallets;
	public List<CheckInLoadOrderBean> listOrders;
	
	public String customerid;		//mbol级customer,和load级不同(load级含所有customer)
	
	//debug
	public List<String> noscanorders;	//未扫sn的orders,注:对多mbol 不要用该值(应在选mbol的页面请求接口)
	public boolean hasNOScanedSNs=false;
	
	public String pro_no;

	// --临时数据--------
	public int closeType = ScanLoadActivity.CloseType_Normal; // 关闭类型(用作传参,不一定已关闭),取ScanLoadActivity.CloseType_x 李君浩注释
	public int syncMasterBolStatus = MBolStatus_Undefined; // 关闭状态

	public String loadingSequence = null;
	public int loadingSeqColor = 0;
	// public boolean wmsCloseFlag = false; //

	// public boolean isFinished=false; //已完成
	// ------------------
	
//	public boolean hasNOScanOrders(){
//		return !Utility.isEmpty(noscanorders);
//	}
	
	/**
	 * 将做normalClose
	 * @return
	 */
	public boolean isToNormalClose(){
		return closeType==ScanLoadActivity.CloseType_Normal;
	}

	// 已close(不一定finish)
	public boolean isClosed() {
		return syncMasterBolStatus == MBolStatus_Closed;
	}

	public void close() {
		syncMasterBolStatus = MBolStatus_Closed;
	}

	// 重置-pallets扫描状态
	public void resetPallets() {
		if (listOrders == null)
			return;

		for (int i = 0; i < listOrders.size(); i++)
			listOrders.get(i).resetPallets();
	}
	
	/**
	 * 已扫描过至少1个pallet,如:用于判断是否需countsheet
	 */
	public boolean hasScaned1Pallet(){
		if (listOrders == null || listOrders.isEmpty())
			return false;
		
		for (int i = 0; i < listOrders.size(); i++) {
			List<PalletBean> tmpPallets = listOrders.get(i).code_nos;
			if (tmpPallets == null)
				continue;
			for (int j = 0; j < tmpPallets.size(); j++) {
				PalletBean tmpPallet = tmpPallets.get(j);
//				if (tmpPallet.palletNo == null)
//					continue;
//				if (tmpPallet.palletNo.equals(palletNo))
//					return tmpPallet.isScaned;
				
				//若有扫描 则跳出
				if(tmpPallet.isScaned)
					return true;
			}
		}
		return false;
	}
 
	/**
	 * 若其下所有order.proNo相同 则返回其,否则为空
	 * @return
	 */
	public String getCommonProNo() {
		String ret;
		if (listOrders == null || listOrders.isEmpty())
			return "";

		// 取第一个为"基准"
		ret = listOrders.get(0).proNo;
		if (TextUtils.isEmpty(ret))
			return "";

		// 全同基准,才取proNo
		for (int i = 0; i < listOrders.size(); i++) {
			String tmpProNo = listOrders.get(i).proNo;
			if (!ret.equals(tmpProNo))
				return "";
		}
		return ret;
	}
	
	public String getBolOrOrder(boolean isLoadOrOrder,String orderNo,boolean withKey){
		boolean isLoad = isLoadOrOrder;
		String bolOrOrder = isLoad ? "MBOL: " : "Order: ";
		String bol_value = isLoad ? master_bol_no + "" : orderNo;
		if (withKey)
			return bolOrOrder + bol_value;
		else
			return bol_value;
	}

	// -----------------------------
	
	public static List<String> parseNOScanedSNs(JSONArray jaNOScanOrders){
		List<String> list=new ArrayList<String>();
		if(!Utility.isEmpty(jaNOScanOrders))
		{
			//加了一前缀 方便显示
			for (int i = 0; i < jaNOScanOrders.length(); i++) 
				list.add("Order: "+jaNOScanOrders.optJSONObject(i).optString("ORDERNO"));
		}
		return list;
	}

	public static void parseBean(JSONObject jo, CheckInLoad_SubLoadBean b) {
		b.load_no = jo.optString("load_no");
		b.master_bol_no = jo.optLong("master_bol_no");
		b.staging_area_id = jo.optString("staging_area_id");
		b.total_pallets = jo.optInt("total_pallets");
		b.syncMasterBolStatus = jo.optInt("wmscloseflag") == 1 ? MBolStatus_Closed
				: MBolStatus_Undefined;
		b.customerid=jo.optString("customerid");
		b.pro_no=jo.optString("pro_no");
		
		
		//debug,未扫sn的orders
		JSONArray jaNOScanOrders=jo.optJSONArray("noscanorders");
		b.noscanorders=parseNOScanedSNs(jaNOScanOrders);
		
//		JSONArray jaNOScanOrders=jo.optJSONArray("noscanorders");
//		if(!Utility.isEmpty(jaNOScanOrders))
//		{
//			b.noscanorders=new ArrayList<String>();
//			for (int i = 0; i < jaNOScanOrders.length(); i++) 
//				b.noscanorders.add(jaNOScanOrders.optJSONObject(i).optString("ORDERNO"));
//		}
		b.hasNOScanedSNs=jo.optInt("is_all_picked")==2;

		JSONArray jaOrders = jo.optJSONArray("order_info");
		b.listOrders = new ArrayList<CheckInLoadOrderBean>();
		CheckInLoadOrderBean.parseBeans(jaOrders, b.listOrders);
	}

	public static void parseBeans(JSONArray ja,
			List<CheckInLoad_SubLoadBean> list) {
		if (ja == null)
			return;

		for (int i = 0; i < ja.length(); i++) {
			CheckInLoad_SubLoadBean tmpB = new CheckInLoad_SubLoadBean();
			parseBean(ja.optJSONObject(i), tmpB);
			list.add(tmpB);
		}

	}

}
