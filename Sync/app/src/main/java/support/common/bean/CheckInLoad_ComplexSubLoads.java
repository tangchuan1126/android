package support.common.bean;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import oso.ui.load_receive.print.bean.PrintServer;
import utility.StringUtil;

/**
 * 复合-子单据列表(含全局信息)
 * 
 * @author 朱成
 * @date 2014-8-21
 */
public class CheckInLoad_ComplexSubLoads implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1943789555563788526L;
	// public String loadNo; //基于get
	public String company_id;
	public String customer_id;	
	public String lr_id;		//业务单据号
	public int system_type;		
	public int order_type;		//如:dn/rn
	public boolean islastdetail;		//为最后一个load
	public List<PrintServer> listPrinters;
	
	public int defaultPrinter;
	public List<CheckInLoadPalletTypeBean> listPalletTypes;
	public List<CheckInLoadBarBean> listLoadBars;
	public List<CheckInLoad_SubLoadBean> listSubLoads; // 理论上-一定存在

	// --手工添加------
	public String entry_id;
//	public String door_name;
	public String out_seal;
	
	public String equipment_id;	

	public CheckInLoadLoadBean load; // 子单据对应的-大load
	public CheckInTaskBeanMain doorBean;	//用于选loadbar
	public CheckInTaskItemBeanMain taskBean;	

	private boolean isSequenceLoading = false;
	private boolean isMbolSeq = false;
	// ----临时参数------
	public int curSubload = 0; // 当前正在处理的-子单据
	public int number_type; // gcy number_type用于打印及预览
	
	public boolean isFromLoad=false;	//来自"load模块"
	
	public CheckInLoad_ComplexSubLoads() {

	}

	public CheckInLoad_SubLoadBean getCurSubLoad() {
		return listSubLoads.get(curSubload);
	}

	public String getLoadNo() {
		return getCurSubLoad().load_no;
	}

	public String getDlo_detail_id() {
		return load == null ? "" : load.dlo_detail_id;
	}
	
	//---------public-------------------
	
	//所有"子单据"均已关闭
	public boolean isAllClosed(){
		if(listSubLoads==null)
			return true;
		
		for (int i = 0; i < listSubLoads.size(); i++) {
			//有1个未关闭,则未关闭
			if(!listSubLoads.get(i).isClosed())
				return false;
		}
		return true;
	}
	
	//尚未关闭的-数目
	private int getUnclosed_BolCnt(){
		if(listSubLoads==null)
			return 0;
		
		int unclosed_cnt=0;
		for (int i = 0; i < listSubLoads.size(); i++) {
			if(!listSubLoads.get(i).isClosed())
				unclosed_cnt++;
		}
		return unclosed_cnt;
	}
	
	//为door中最后一个load,且为最后1个bol
	public boolean isLastLoad_andLastSubLoad(){
		
		if(!islastdetail)
			return false;
		//为最后一个未关闭的bol
		boolean isLastUnclosed_Bol=getUnclosed_BolCnt()==1&& !getCurSubLoad().isClosed();
		
		//即islastdetail&&isLastUnclosed_Bol
		return isLastUnclosed_Bol;
	}
	
	// --------------------------------

//	// 搜mbol时用,其实为mbol下"orders列表" 未兼容程序 做成ComplexSubLoads
//	public static void parseBean_searchMbol(JSONObject joSubload,
//			CheckInLoad_ComplexSubLoads ret) {
//		// 子单据
//		CheckInLoad_SubLoadBean subload = new CheckInLoad_SubLoadBean();
//		CheckInLoad_SubLoadBean.parseBean(joSubload, subload);
//		ret.listSubLoads = new ArrayList<CheckInLoad_SubLoadBean>();
//		ret.listSubLoads.add(subload);
//		// palletType
//		JSONArray jaPalletTypes = joSubload.optJSONArray("pallet_types");
//		ret.listPalletTypes = new ArrayList<CheckInLoadPalletTypeBean>();
//		CheckInLoadPalletTypeBean
//				.parseBeans(jaPalletTypes, ret.listPalletTypes);
//	}

	// 搜entry/load时
	public static void parseBean(JSONObject json, CheckInLoad_ComplexSubLoads b) {
//		JSONObject jo = json.optJSONObject("datas");
		JSONObject jo = StringUtil.getJsonObjectFromJSONObject(json,"datas");
		// ok
		b.company_id = jo.optString("company_id");
		b.customer_id = jo.optString("customer_id");
		b.lr_id = jo.optString("lr_id");
		b.system_type = jo.optInt("system_type");
		b.order_type = jo.optInt("order_type");
		
		
		b.islastdetail=json.optInt("islastdetail")==1?true:false;
		b.listLoadBars=new ArrayList<CheckInLoadBarBean>();
		CheckInLoadBarBean.parseBeans(json.optJSONArray("loadbars"), b.listLoadBars);

		JSONArray jaSubLoads = jo.optJSONArray("load_info");
		if (jaSubLoads != null && jaSubLoads.length() > 0) {
			b.listSubLoads = new ArrayList<CheckInLoad_SubLoadBean>();
			CheckInLoad_SubLoadBean.parseBeans(jaSubLoads, b.listSubLoads);
			checkLoadingSequence(b);

//			//palletTypes
//			JSONArray jaPalletTypes = jaSubLoads.optJSONObject(0).optJSONArray(
//					"pallet_types");
//			if (jaPalletTypes != null && jaPalletTypes.length() > 0) {
//				b.listPalletTypes = new ArrayList<CheckInLoadPalletTypeBean>();
//				CheckInLoadPalletTypeBean.parseBeans(jaPalletTypes,
//						b.listPalletTypes);
//			}
		}

		JSONObject joPrinters = json.optJSONObject("printserver");
		if (joPrinters != null) {
			b.listPrinters = new ArrayList<PrintServer>();
			PrintServer.parseBeans(joPrinters.optJSONArray("allprintserver"),
					b.listPrinters);
			b.defaultPrinter = joPrinters.optInt("default");
		}

	}

	public boolean isSequenceLoading() {
		return isSequenceLoading;
	}
	public boolean isMbolSeq() {
		return isMbolSeq;
	}


	private static void checkLoadingSequence(CheckInLoad_ComplexSubLoads complexSubLoads) {
		checkExternidLoadSeq(complexSubLoads);
		if (complexSubLoads.isSequenceLoading) {
			return;
		}
		checkCostcoLoadSeq(complexSubLoads);
	}

	private static void checkExternidLoadSeq(CheckInLoad_ComplexSubLoads complexSubLoads) {
		int loadConter = 0;
		int orderCounter = 0;
		for (CheckInLoad_SubLoadBean loadbean : complexSubLoads.listSubLoads) {
			orderCounter = 0;
			for (CheckInLoadOrderBean orderbean : loadbean.listOrders) {
				if (!TextUtils.isEmpty(orderbean.externalID)) {
					int index = Arrays.binarySearch(orderbean.LOAD_SEQ_VALUE, orderbean.externalID);
					if (index > -1 && index < orderbean.LOAD_SEQ_VALUE.length)
						++orderCounter;
				}
			}

			if (orderCounter == loadbean.listOrders.size()) {
				++loadConter;
			}
		}

		if (loadConter == complexSubLoads.listSubLoads.size()) {
			complexSubLoads.isSequenceLoading = true;

			for (CheckInLoad_SubLoadBean loadbean : complexSubLoads.listSubLoads) {
				Collections.sort(loadbean.listOrders, new Comparator<CheckInLoadOrderBean>() {
					@Override
					public int compare(CheckInLoadOrderBean rv, CheckInLoadOrderBean lv) {
						int rindex = Arrays.binarySearch(rv.LOAD_SEQ_VALUE, rv.externalID);
						int lindex = Arrays.binarySearch(lv.LOAD_SEQ_VALUE, lv.externalID);
						return rindex - lindex;
					}
				});

				String externid = "";
				int index = -1;
				for (CheckInLoadOrderBean orderBean: loadbean.listOrders) {
					if (!externid.equals(orderBean.externalID)) {
						externid = orderBean.externalID;
						if (index < orderBean.LOAD_SEQ_COLORS.length)
							++index;
					}
					orderBean.loadingSequence = orderBean.LOAD_SEQ_TEXT[index];
					orderBean.loadingSeqColor = orderBean.LOAD_SEQ_COLORS[index];
				}
			}
		}
	}

	private static void checkCostcoLoadSeq(CheckInLoad_ComplexSubLoads complexSubLoads) {
		int loadConter = 0;
		int orderCounter = 0;
		for (CheckInLoad_SubLoadBean loadbean : complexSubLoads.listSubLoads) {
			orderCounter = 0;
			for (CheckInLoadOrderBean orderbean : loadbean.listOrders) {
				if (!TextUtils.isEmpty(orderbean.accountID)) {
					List values = Arrays.asList(orderbean.COSTCO_LOAD_SEQ_VALUE);
					if (values.contains(orderbean.accountID.toLowerCase().trim())) {
						++orderCounter;
					}
				}
			}

			if (orderCounter == loadbean.listOrders.size()) {
				++loadConter;
			}
		}

		if (loadConter == complexSubLoads.listSubLoads.size() && complexSubLoads.listSubLoads.size() > 1) {
			String firstAccountId = complexSubLoads.listSubLoads.get(0).listOrders.get(0).accountID;
			for (CheckInLoad_SubLoadBean loadbean : complexSubLoads.listSubLoads) {
				if (!firstAccountId.equals(loadbean.listOrders.get(0).accountID)) {
					complexSubLoads.isMbolSeq = true;
					break;
				}
			}
			if (!complexSubLoads.isMbolSeq) {
				return;
			}

			Collections.sort(complexSubLoads.listSubLoads, new Comparator<CheckInLoad_SubLoadBean>() {
				@Override
				public int compare(CheckInLoad_SubLoadBean rbean, CheckInLoad_SubLoadBean lbean) {
					int rindex = Arrays.asList(CheckInLoadOrderBean.COSTCO_LOAD_SEQ_VALUE)
							.indexOf(rbean.listOrders.get(0).accountID.toLowerCase().trim());
					int lindex = Arrays.asList(CheckInLoadOrderBean.COSTCO_LOAD_SEQ_VALUE)
							.indexOf(lbean.listOrders.get(0).accountID.toLowerCase().trim());
					return rindex - lindex;
				}
			});

			String accountid = "";
			int index = -1;

			for (CheckInLoad_SubLoadBean loadbean : complexSubLoads.listSubLoads) {
				if (!accountid.equals(loadbean.listOrders.get(0).accountID)) {
					accountid = loadbean.listOrders.get(0).accountID;
					if (index < CheckInLoadOrderBean.LOAD_SEQ_COLORS.length)
						++index;
				}
				loadbean.loadingSequence = CheckInLoadOrderBean.LOAD_SEQ_TEXT[index];
				loadbean.loadingSeqColor = CheckInLoadOrderBean.LOAD_SEQ_COLORS[index];
			}
		}
	}

//	public JSONObject getJsonObject() {
//		JSONObject data = new JSONObject();
//		try {
//			data.put("master_bol_no", getCurSubLoad().master_bol_no + "");
//			data.put("company_id", company_id);
//			data.put("customer_id", customer_id);
//			JSONArray orderInfo = new JSONArray();
//			List<CheckInLoadOrderBean> listOrders = getCurSubLoad().listOrders;
//			for (int i = 0; i < listOrders.size(); i++) {
//				CheckInLoadOrderBean c = listOrders.get(i);
//				JSONObject jsonObject = new JSONObject();
//				jsonObject.put("orderno", c.orderno);
//
//				JSONArray palletTypeInfo = new JSONArray();
//				Map<String, Integer> map_PalletType_Cnt = c
//						.getMapPalletTypeCnt();
//				if (map_PalletType_Cnt != null) {
//					Set<String> types = map_PalletType_Cnt.keySet();
//					for (String type : types) {
//						JSONObject palletTypeInfoObj = new JSONObject();
//						// 刷新-数据
//						palletTypeInfoObj.put("pallet_type", type);
//						palletTypeInfoObj.put("count",
//								map_PalletType_Cnt.get(type) + "");
//						palletTypeInfo.put(palletTypeInfoObj);
//					}
//					jsonObject.put("pallet_info", palletTypeInfo);
//				}
//
//				orderInfo.put(jsonObject);
//			}
//			data.put("order_infos", orderInfo);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return data;
//	}

}
