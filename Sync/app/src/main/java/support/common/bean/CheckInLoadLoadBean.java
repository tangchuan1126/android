package support.common.bean;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import support.key.EntryDetailNumberStateKey;
import support.key.EntryDetailNumberTypeKey;
import utility.StringUtil;
import android.content.Context;

/**
 * load
 * 
 * @author 朱成
 * @date 2014-8-18
 */
public class CheckInLoadLoadBean implements Serializable {

	private static final long serialVersionUID = 6402964814846020171L;

	// debug,loadno待去
	// public String loadnumber;
	public String status;

	public int number_type; // 取EntryDetailNumberTypeKey.xxx
	public String number;
	public int append_type;
	public String append_number;

	public String company_id;
	public String customer_id;

	public String dlo_detail_id;

	public String ic_id; // 若">0",则为三星
	public String staging_area_id;
	public String freight_term;

	// ----搜entry时没有---------
	public String entry_id;
	public String door;
	
	public String equipment_id;
	public String resources_id;
	public String resources_type;

	public String equipment_number;
	public String appointment_date;

    //----------打印时-----------------
	public String receipt_no;

	public CheckInLoadLoadBean() {
	}

	// public CheckInLoadLoadBean(String loadnumber,String status){
	// this.loadnumber=loadnumber;
	// this.status=status;
	// }

	public boolean isSamsung() {
		return StringUtil.convert2Inter(ic_id) > 0;
	}

	// 已处理,close、exception、partially
	public boolean isFinished(Context c) {
		return (EntryDetailNumberStateKey
				.getType(c,EntryDetailNumberStateKey.Close))
				.equalsIgnoreCase(status)
				|| (EntryDetailNumberStateKey
						.getType(c,EntryDetailNumberStateKey.Exception))
						.equalsIgnoreCase(status)
				|| (EntryDetailNumberStateKey
						.getType(c,EntryDetailNumberStateKey.Partially))
						.equalsIgnoreCase(status);
	}

	// 条目类型,取EntryDetailNumberTypeKey.CHECK_IN_x
	public int getMainType() {
		// load取load、po/order取order
		if (number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD)
			return EntryDetailNumberTypeKey.CHECK_IN_LOAD;
		else
			return EntryDetailNumberTypeKey.CHECK_IN_ORDER;
	}

	// 主类型-为load
	public boolean isMainType_load() {
		return number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD;
	}

	public String getMainType_Str() {
		// load取load、po/order取order
		if (number_type == EntryDetailNumberTypeKey.CHECK_IN_LOAD)
			return "load";
		else
			return "order";
	}

	// 条目-类型号,1.类别为load时取number、为order时找"order号"
	public String getMainNo() {
		// 为load
		if (getMainType() == EntryDetailNumberTypeKey.CHECK_IN_LOAD)
			return number;

		// 为order
		if (number_type == EntryDetailNumberTypeKey.CHECK_IN_ORDER)
			return number;
		else
			return append_number;
	}

	// ======================================

	public static void parseBean(JSONObject jo, CheckInLoadLoadBean b) {
		b.status = jo.optString("status");
		b.number_type = jo.optInt("number_type");
		b.number = jo.optString("number");
		b.append_type = jo.optInt("append_type");
		b.append_number = jo.optString("append_number");

		b.company_id = jo.optString("company_id");
		b.customer_id = jo.optString("customer_id");

		b.dlo_detail_id = jo.optString("dlo_detail_id");
		b.ic_id = jo.optString("ic_id");
		b.staging_area_id = jo.optString("staging_area_id");
		b.freight_term = jo.optString("freight_term");
		
		b.equipment_number = jo.optString("equipment_number");
		b.appointment_date = jo.optString("appointment_date");

        //debug
        b.receipt_no=jo.optString("receipt_no");
	}

	public static void parseBeans(JSONArray ja, List<CheckInLoadLoadBean> list,List<CheckInLoadLoadBean> nolist) {
		if (ja == null)
			return;

		for (int i = 0; i < ja.length(); i++) {
			CheckInLoadLoadBean b = new CheckInLoadLoadBean();
			parseBean(ja.optJSONObject(i), b);
			list.add(b);
			if(b.number_type == EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS || b.number_type == EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS){
				nolist.add(b);
			}
		}
	}

}
