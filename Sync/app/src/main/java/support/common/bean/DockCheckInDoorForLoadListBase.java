package support.common.bean;

import java.io.Serializable;

/**
 * @ClassName: DockCheckInDoorBase 
 * @Description: 
 * @author gcy
 * @date 2014-9-1 下午12:38:06
 */
public class DockCheckInDoorForLoadListBase implements Serializable {

	/**
	 * @Description:
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;//子门名称
	private String po_no;
	private String order_no;
	private String dlo_detail_id;//子门Id
	private int type;
	private int number_type;//load类型
	private String company_id;
	private String customer_id;
	private String schedule_id;
	private boolean isSelect;
	private String srr_id;
	private String equipment_id;
	private String execute_user;
	private int number_status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	public String getOrder_no() {
		return order_no;
	}
	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}
	public String getDlo_detail_id() {
		return dlo_detail_id;
	}
	public void setDlo_detail_id(String dlo_detail_id) {
		this.dlo_detail_id = dlo_detail_id;
	}
	public int getNumber_type() {
		return number_type;
	}
	public void setNumber_type(int number_type) {
		this.number_type = number_type;
	}
	public boolean isSelect() {
		return isSelect;
	}
	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getCompany_id() {
		return company_id;
	}
	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getSrr_id() {
		return srr_id;
	}
	public void setSrr_id(String srr_id) {
		this.srr_id = srr_id;
	}
	public String getEquipment_id() {
		return equipment_id;
	}
	public void setEquipment_id(String equipment_id) {
		this.equipment_id = equipment_id;
	}
	public String getSchedule_id() {
		return schedule_id;
	}
	public void setSchedule_id(String schedule_id) {
		this.schedule_id = schedule_id;
	}
	public String getExecute_user() {
		return execute_user;
	}
	public void setExecute_user(String execute_user) {
		this.execute_user = execute_user;
	}
	public int getNumber_status() {
		return number_status;
	}
	public void setNumber_status(int number_status) {
		this.number_status = number_status;
	}
		
}
