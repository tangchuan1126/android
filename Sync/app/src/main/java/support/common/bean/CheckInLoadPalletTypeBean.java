package support.common.bean;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.TextUtils;

/**
 * pallet类型
 * 
 * @author 朱成
 * @date 2014-8-20
 */
public class CheckInLoadPalletTypeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7562314021244821571L;
	
	public String pallettypeid;
	public String pallettypename;
	public String companyid;
	public String datecreated;
	public String usercreated;

	// ----------------------------------

	public static void parseBean(JSONObject jo, CheckInLoadPalletTypeBean b) {
		b.pallettypeid = jo.optString("pallettypeid");
		b.pallettypename = jo.optString("pallettypename");
		b.companyid = jo.optString("companyid");
		b.datecreated = jo.optString("datecreated");
		b.usercreated = jo.optString("usercreated");
	}

	public static void parseBeans(JSONArray ja,
			List<CheckInLoadPalletTypeBean> list) {
		if(ja==null)
			return;
		
		for (int i = 0; i < ja.length(); i++) {
			CheckInLoadPalletTypeBean b = new CheckInLoadPalletTypeBean();
			parseBean(ja.optJSONObject(i), b);
			list.add(b);
		}
	}

	// ========static=====================================
	// 找索引
	// 注:1>返回:-1(未找到)
	public static int getIndex(List<CheckInLoadPalletTypeBean> listTypes,
			String palletNo) {
		if (listTypes == null || TextUtils.isEmpty(palletNo))
			return -1;

		for (int i = 0; i < listTypes.size(); i++) {
			CheckInLoadPalletTypeBean tmpType = listTypes.get(i);
			if (palletNo.equals(tmpType.pallettypeid))
				return i;
		}
		return -1;
	}
	
	// 搜palletType
	// 注:1.id为"扫描值" 一把以"86"开头(此处也支持无"86"开头)
	public static CheckInLoadPalletTypeBean getPalletType_byID(
			List<CheckInLoadPalletTypeBean> listPalletTypes, String id) {
		if (TextUtils.isEmpty(id))
			return null;

		// 以86开头
		boolean startWith86 = id.indexOf("86") == 0;

		// List<CheckInLoadPalletTypeBean> listPalletTypes =
		// complexSubLoads.listPalletTypes;
		for (int i = 0; i < listPalletTypes.size(); i++) {
			CheckInLoadPalletTypeBean b = listPalletTypes.get(i);
			String tmpTypeId = startWith86 ? "86" + b.pallettypeid
					: b.pallettypeid;
			//debug
			if (id.equalsIgnoreCase(tmpTypeId))
				return b;
		}
		return null;
	}

}
