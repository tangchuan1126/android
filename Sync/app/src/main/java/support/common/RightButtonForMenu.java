package support.common;

import java.util.List;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import declare.com.vvme.R;

public class RightButtonForMenu extends PopupWindow{
	
	private View more_lay;
	
	public RightButtonForMenu(Activity context,View more_lay,List<ChickMoreInfo> chickMoreInfoList){
		super(context);  
		this.more_lay = more_lay;
		LinearLayout layout = null;
		layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.dialog_full_more, null);
		LinearLayout more_btn_show = (LinearLayout) layout.findViewById(R.id.more_btn_show);
		for(int i = 0; i<chickMoreInfoList.size();i++){
			View   item = LayoutInflater.from(context).inflate(R.layout.set_more_btn_item, null);
			((View)item.findViewById(R.id.contact_line_second)).setOnClickListener(chickMoreInfoList.get(i).getBtn_click());
			((View)item.findViewById(R.id.line_gray)).setVisibility((i==0)?View.GONE:View.VISIBLE);
			((TextView)item.findViewById(R.id.set_info_name)).setText(chickMoreInfoList.get(i).getButton_name());
			((TextView)item.findViewById(R.id.set_info_name)).setGravity(Gravity.CENTER_HORIZONTAL);
			more_btn_show.addView(item);
		}
	
		
		this.setBackgroundDrawable(new BitmapDrawable());
		this.setWidth(context.getWindowManager().getDefaultDisplay().getWidth()/9*4);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		this.setOutsideTouchable(true);
		this.setFocusable(true);
		this.setContentView(layout);
	}
	
	public void show(){
		int[] location = new int[2];  
		more_lay.getLocationOnScreen(location); 
		this.showAtLocation(more_lay,Gravity.NO_GRAVITY, location[0]/5*4, location[1]+more_lay.getHeight()-11);// 需要指定Gravity，默认情况是center.
	}
	
	
	public static void dismissPopupWindow(PopupWindow pop){
		if(pop!=null&&pop.isShowing()){
			pop.dismiss();
		}
	}
}
