package support.common.push;

import java.util.HashMap;
import java.util.Map;

/**子业务-类型,用于推送
 * @author 朱成
 * @date 2014-11-10
 */
public class ProcessKey {
	
	public static int TASK=0;
	public static int CHECK_IN_WINDOW = 52;							//check in window 
	public static int CHECK_IN_WAREHOUSE = 53;						//check in warehouse 
	public static int CHECK_IN_ANDROID_WAREHOUSE = 54;				//check in warehouse  android
	public static int CHECK_IN_ANDROID_PARROL = 55;					//check in warehouse  android
	
	private static Map<Integer, String> mapKeyNames=new HashMap<Integer, String>();
	
	static{
		mapKeyNames.put(CHECK_IN_WINDOW, "Check In Window");
		mapKeyNames.put(CHECK_IN_WAREHOUSE, "Check In Warehouse");
		mapKeyNames.put(CHECK_IN_ANDROID_WAREHOUSE, "Check In Android Warehouse");
		mapKeyNames.put(CHECK_IN_ANDROID_PARROL, "Check In Android Patrol");
	}
	
	public static String getName(int key){
		return mapKeyNames.get(key);
	}

}
