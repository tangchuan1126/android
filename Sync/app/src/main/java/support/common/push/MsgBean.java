package support.common.push;



/**
 * 消息对象,用于推送
 * 
 * @author 朱成
 * @date 2014-11-11
 */
public class MsgBean {

//	  "content": "sdssdsfsdf",
//    "module_key": "0",
//    "title": "sds",
//    "associate_main_id": "0",
//    "process_key": "0",
//    "associate_id": "0",
//    "message_type": "2",
//    "header": " Schedule [1201731]"

	public String content;
	public String module_key;
	public String title;			//通知-描述
	public String associate_main_id;
	public String process_key;
	
	public String associate_id;
	public String message_type;
	public String header;			//通知-标题
	
	//-----
	public int moduleKey;
	public String date;

}
