package support.common.push;

import oso.SyncApplication;
import oso.base.ActivityStack;
import oso.base.BaseActivity;
import oso.base.BaseFramentActivity;
import oso.ui.main.WelcomeActivity;
import utility.Utility;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.google.gson.Gson;

import declare.com.vvme.R;

/**
 * MyPushService的消息监听、开机监听
 * 
 * @注:1>"提醒"处理:1.外面notify移除(点击/手动移除)时,移除里面"提醒" 2.里面移除时,外面移除
 * 
 * @author 朱成
 * @date 2014-12-8
 */
public class MyReceiver extends BroadcastReceiver {

	public static final String Action_DismissMsg = "declare.com.vvme.Action_DismissMsg";
	public static final String Action_OnNotifyClick_Chat = "declare.com.vvme.Action_OnNotifyClick_Chat";

	public final static int From_OnNotifyClick_Task = 1;
	public final static int From_OnNotifyClick_Chat = 2;

	private Context context;
	private Intent intent;

	@Override
	public void onReceive(Context context, Intent intent) {
		System.out.println("nimei>>MyReceiver.onReceive");
		this.context = context;
		this.intent = intent;

		String action = intent.getAction();
		if (MyPushService.Action_OnPush.equals(action))
			onPush();
		// 暂时不需
		// else if(Intent.ACTION_BOOT_COMPLETED.equals(action))
		// onBoot();
		else if (MyReceiver.Action_DismissMsg.equals(action))
			onDismissMsg();

	}

	// 移除消息时
	private void onDismissMsg() {
		// 移除消息
		Activity ac = ActivityStack.getTopActInstance();
		if (ac != null && ac instanceof BaseActivity)
			((BaseActivity) ac).removeMsg(false);

		// debug start-----
		if (ac != null && ac instanceof BaseFramentActivity)
			((BaseFramentActivity) ac).removeMsg(false);
		// debug end-------
	}

	// 收到消息时
	private void onPush() {

		boolean isMsgDec = intent.getBooleanExtra("isCntDec", false);
		// 若仅消息数变更,则仅刷新首页
		if (isMsgDec) {
			Activity ac = ActivityStack.getTopActInstance();
			// 刷下-状态
			if (ac != null && ac instanceof BaseActivity)
				((BaseActivity) ac).onPush();

			// debug start-----
			if (ac != null && ac instanceof BaseFramentActivity)
				((BaseFramentActivity) ac).onPush();
			// debug end-------

			return;
		}
		String datas = intent.getStringExtra("datas");

		// debug
		System.out.println("nimei>>MyReceiver:" + datas);

		MsgBean b = null;
		try {
			b = new Gson().fromJson(datas, MsgBean.class);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			// 解析有问题 直接跳出,debug 应上报错误
			return;
		}

		// x,开头有空格
		if (!TextUtils.isEmpty(b.header))
			b.header = b.header.trim();
		String message = b.title;

		int icon = R.drawable.icon;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		notification.defaults = Notification.DEFAULT_ALL;

		Context app = SyncApplication.getThis();
		RemoteViews remoteView = new RemoteViews(app.getPackageName(), R.layout.lo_notify);
		remoteView.setImageViewResource(R.id.img, R.drawable.icon);
		// b.header
		remoteView.setTextViewText(R.id.tvTitle, "Sync");
		remoteView.setTextViewText(R.id.tvMsg, message);
		notification.contentView = remoteView;

		Intent notificationIntent = new Intent(context, WelcomeActivity.class);
		WelcomeActivity.initParams(notificationIntent, WelcomeActivity.From_Task_Notification);
		// set intent so it does not start a new activity,debug
		// notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notification.contentIntent = intent;
		// 监听"移除"
		notification.deleteIntent = PendingIntent.getBroadcast(context, 0, new Intent(Action_DismissMsg), 0);

		// debug
		// notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(Utility.NotifyID_Task, notification);

		// 抛给-当前页面
		Activity ac = ActivityStack.getTopActInstance();
		BaseActivity.last_msg = message;
		if (ac != null && ac instanceof BaseActivity)
			((BaseActivity) ac).onPush();

		// start debug-----
		BaseFramentActivity.last_msg = message;
		if (ac != null && ac instanceof BaseFramentActivity)
			((BaseFramentActivity) ac).onPush();
		// end debug-----
	}

	// //开机时,启动服务
	// private void onBoot(){
	// context.startService(new Intent(MyPushService.Action_MyPushService));
	// }

}
