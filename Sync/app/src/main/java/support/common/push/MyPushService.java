package support.common.push;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.Header;

import oso.SyncApplication;
import oso.ui.chat.util.XmppTool;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import support.network.SimpleJSONUtil;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

/**
 * 基于轮询的-推送
 * 
 * @author 朱成
 * @date 2014-12-8
 */
public class MyPushService extends Service {

	private final static String Tag = "MyPushService";

	// 用于启动该service
	public static final String Action_MyPushService = "declare.com.vvme.Action_MyPushService";
	// 用于"监听消息"
	public static final String Action_OnPush = "declare.com.vvme.Action_OnPush";

	// 延迟60s,60000
//	private final long Loop_Interval =60000;
	
	//刷cookie,周期
	private long Period_UpdateCookie=1000*60*5;
//	private long Period_UpdateCookie=1000*10;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	UrlReceiver urlReceiver;

	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		System.out.println("nimei>>MyPushService.onCreate");
		
		urlReceiver = new UrlReceiver();  
		//设置接收的action 
		IntentFilter filter=new IntentFilter();
		filter.addAction("android.intent.action.INIT_URL");
		registerReceiver(urlReceiver,filter);
		
		//定时-更新cookie
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				
				//已登录,才更新cookie
				if(StoredData.isLogin())
					reqUpdateCookie();
			}
		},1000, Period_UpdateCookie);
		
		//监听网络变化
		//网络可用时 一定会调用(1.启动app时若有网 2.启动app时没网,后续来往)
		IntentFilter filterNet = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		rec_onNetChange=new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				boolean isNetOk=Utility.isConnectNet(MyPushService.this);
				//联网时-立即连接openfire
				if(isNetOk)
					XmppTool.getThis().login_of();
				
				//debug,连上网时 且已登陆，刷cookie
				if(isNetOk && StoredData.isLogin())
					reqUpdateCookie();
			}
		};
		registerReceiver(rec_onNetChange, filterNet);
		
	}
	
	private BroadcastReceiver rec_onNetChange;
	
	private int k=0;
	
	/**
	 * 刷cookie
	 */
	private void reqUpdateCookie(){
		AsyncHttpClient client=SimpleJSONUtil.client;
		RequestParams params = new RequestParams();
		params.add("Method", "RefreshCookie");
		// 添加-登陆信息
		Goable.initGoable(this);
		StringUtil.setLoginInfoParams(params);
		
		//cookie
		PersistentCookieStore myCookieStore = new PersistentCookieStore(this);
		client.setCookieStore(myCookieStore);
		client.post(HttpUrlPath.seachLoginUser,params, new TextHttpResponseHandler(){
			
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					String responseBody) {
				// TODO Auto-generated method stub
//				super.onSuccess(statusCode, headers, responseBody);
				System.out.println("nimei>>refresh_cookie:ok");
			}
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseBody, Throwable error) {
				// TODO Auto-generated method stub
//				super.onFailure(statusCode, headers, responseBody, error);
				System.out.println("nimei>>refresh_cookie:fail");
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//子线程中不能跑"同步模式" 会报错
				return false;
			}
		});
	}
	
	@Override
	public void onDestroy() {
		unregisterReceiver(urlReceiver);
		if(rec_onNetChange!=null)
			unregisterReceiver(rec_onNetChange);
		super.onDestroy();
	}

//	private int cnt = 0;
//
//	private String last_adid = "";
//
//	// 基于-独立线程
//	private void doPull() {
//
//		// 未登录,直接退出
//		if (!StoredData.isLogin()) {
//			Log.d(Tag, "Not Login!");
//			return;
//		}
//
//		final int kk = ++cnt;
//
//		final int lastMsgCnt = StoredData.getLastMsgCnt();
//		final String adid = StoredData.getAdid();
//
//		// 若账户变更,则重置"ram信息"
//		if (adid != null && !adid.equals(last_adid)) {
//			// 重置goable相关
//			Goable.LoginAccount = "";
//			last_adid = adid;
//		}
//
//		// RequestParams p = new RequestParams();
//		Map<String, String> p = new HashMap<String, String>();
//		p.put("Method", "NewSchedule");
//		p.put("schedule_id", StoredData.getLastMsgID() + "");
//		p.put("count_message", lastMsgCnt + "");
//		// 添加-登陆信息
//		Goable.initGoable(this);
//		StringUtil.setLoginInfoParams(p);
//		Log.d(Tag, "basePath= "+HttpUrlPath.basePath);
//		Log.d(Tag, kk + "," + p.toString());
//		try {
//			// String ret = doRequest(HttpUrlPath.GCMAction, p.toString());
//			JSONObject json = HttpTool.getJsonFromUrl(HttpUrlPath.GCMAction, HttpTool.POST, p, null);
//			Log.d(Tag, kk + ",pull.ok," + json.toString());
//
//			// 若中途adid变更,则跳出t
//			if (last_adid != null && !last_adid.equals(StoredData.getAdid()))
//				return;
//
//			// 若出错,直接跳过
//			if (json.optInt("ret") != 1)
//				return;
//
//			int newMsgCnt = json.optInt("count_message");
//			// 没新消息时,无该字段
//			JSONObject joDatas = json.optJSONObject("datas");
//			if (json == null || json.optJSONObject("datas") == null) {
//				// 若消息数改变
//				if (newMsgCnt != lastMsgCnt) {
//					broadcastPush(true, "");
//					StoredData.setLastMsgCnt(newMsgCnt);
//				}
//				return;
//			}
//
//			// 保存-最新消息id
//			StoredData.setLastMsgID(joDatas.optLong("schedule_id"));
//			StoredData.setLastMsgCnt(newMsgCnt);
//
//			broadcastPush(false, joDatas.toString());
//		} catch (Exception e) {
//			// TODO: handle exception
//			Log.d(Tag, kk + ",pull.fail");
//		}
//
//	}

	public static void broadcastPush(boolean isCntDec, String datas) {
		// 若有消息,则抛出
		Intent in = new Intent(Action_OnPush);
		in.putExtra("datas", datas);
		in.putExtra("isCntDec", isCntDec);
		in.setPackage(SyncApplication.getThis().getPackageName());
		SyncApplication.getThis().sendBroadcast(in);
	}

//	private void doDelay() {
//
//		SystemClock.sleep(Loop_Interval);
//		doPull();
//
//		// handler.sendEmptyMessageDelayed(0, Loop_Interval);
//	}

//	private Handler handler = new Handler() {
//
//		public void handleMessage(Message msg) {
//			doPull();
//		};
//	};

	public class UrlReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			HttpUrlPath.basePath = intent.getStringExtra("basePath");
			HttpUrlPath.initUrls();
			Log.i("BaseUrl", "UrlReceiver" + HttpUrlPath.basePath);
		}

	}

}
