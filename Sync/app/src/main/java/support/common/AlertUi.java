package support.common;

import oso.widget.dialog.RewriteBuilderDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import declare.com.vvme.R;

public class AlertUi {

	public static void showAlertBuilder(Context context, String message) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		Resources resources = context.getResources();
//		R.string.sys_holdon
		builder.setTitle(resources.getString(R.string.sync_notice));
		builder.setMessage(message);
		builder.setNegativeButton(resources.getString(R.string.sync_yes), null);
		builder.create().show();
	}

	public static void showVersionError(final Context context) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setMessage(context.getString(R.string.login_update_version));
		builder.setPositiveButton(context.getString(R.string.login_update_version_btn), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				DownloadApk.loadApk(context);
			}
		});
		builder.isHideCancelBtn(true);
		builder.create().show();
	}

}
