package support.common.datas;

public class HoldFourValue<A, B, C, D> extends HoldThreeValue<A, B, C> {

	public final D d;

	public HoldFourValue(A a, B b, C c, D d) {
		super(a, b, c);
		this.d = d;
	}
}