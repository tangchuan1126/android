package support.common.datas;

public class HoldThreeValue<A, B, C> extends HoldDoubleValue<A, B> {

	public final C c;

	public HoldThreeValue(A a, B b, C c) {
		super(a, b);
		this.c = c;
	}
}
