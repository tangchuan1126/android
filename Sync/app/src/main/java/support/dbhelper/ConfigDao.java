package support.dbhelper;

import java.util.List;

import oso.ui.processing.picking.bean.PickUpLoactionProduct;
import oso.ui.processing.picking.bean.PickUpLocation;
import oso.ui.processing.picking.bean.PickUpSerialProduct;
import oso.ui.processing.picking.bean.Product;
import oso.ui.processing.picking.bean.ProductCode;
import oso.ui.processing.picking.bean.ReturnBean;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ConfigDao {

	private    DatabaseHelper helper;
	
	private     SQLiteDatabase  db;
	 
	public ConfigDao(Context context) {
		helper = new DatabaseHelper(context);
		 
 	}
	public int updateConfigDao(String column , Object value) {
		int count = 0 ;
		db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("columnValue", value.toString());
		db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='"+ value.toString() + "' where columnName='"+column+"'");
		 
		db.close();
		return count ;
	}
	//01-24 08:07:30.960: I/info(884): <?xml version="1.0" encoding="UTF-8"?><data><fc>TransportUpload</fc><Machine>bj0001</Machine><LoginAccount>admin</LoginAccount><Password>admin</Password><Transport>101763</Transport><Detail><Details>154627,100000,123456789,1.0|154627,100000,65421345,1.0|154627,100000,NULL,5.0|154627,100000,NULL,5.0</Details></Detail><Length>100</Length><Efficacy>4lFljqs49cGYqAwLTHbnOJZyeI4=</Efficacy></data>

	public String getConfigValue(String column){
		db = helper.getReadableDatabase();
		String value = "" ;
		Cursor cursor = db.rawQuery("select * from " + DatabaseHelper.tbl_config + " where columnName=?", new String[]{column});
		if(cursor != null && cursor.getCount() > 0 ){
			cursor.moveToFirst();
			value = cursor.getString(cursor.getColumnIndex("columnValue"));
		}
		cursor.close();
		db.close();
		return  value;
	}
//	public void deleteOutboundBaseData(long transport_id) {
//		db = helper.getWritableDatabase();
//		try{
//			db.beginTransaction();
//			//db.delete(DatabaseHelper.tbl_transport, " transport_id=? ", new String[]{transport_id+""});
//			//db.delete(DatabaseHelper.tbl_receive_info, "transport_id = ? ", new String[]{transport_id+""});
//			db.delete(DatabaseHelper.tbl_outbound_serial_product, " 1=1 ", null );
//			db.delete(DatabaseHelper.tbl_outbound_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_product_code, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_transport_product, "1=1", null);
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
// 		}finally{
// 			db.endTransaction();
// 			db.close();
//		}
//	}
//	/**
//	 * 点击刷新按钮的时候执行的操作。
//	 */
//	public void deleteInboundRefresh(){
//		
//	}
//	public void deleteInboundData(long transport_id){
//		db = helper.getWritableDatabase();
//		 
//		try{
//
//			db.beginTransaction();
//			db.delete(DatabaseHelper.tbl_receive_info, "transport_id = ? ", new String[]{transport_id+""});
//			db.delete(DatabaseHelper.tbl_transport, "transport_id= ? ", new String[]{transport_id+""});
// 			db.delete(DatabaseHelper.tbl_serial_product, " 1=1 ", null );
//			db.delete(DatabaseHelper.tbl_inbound_transport_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_inbound_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_inbound_product_code, "1=1", null);
//			db.delete(DatabaseHelper.tbl_inbound_lp_info, " bill_id = ? ", new String[]{transport_id+""});
//			db.delete(DatabaseHelper.tbl_inbound_compare_product, " transport_id = ? ", null);
//			db.delete(DatabaseHelper.tbl_inbound_positioned_product, " transport_id="+transport_id, new String[]{transport_id+""});
//
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='' where columnName='lpvalue'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='lpstate'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='isReceiveing'");
//			
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
//			e.printStackTrace();
// 		}finally{
// 			db.endTransaction();
// 			db.close();
//		}
//	}
//	public void deleteOutboundData(long transport_id){
//		db = helper.getWritableDatabase();
//		 
//		try{
//
//			db.beginTransaction();
//			db.delete(DatabaseHelper.tbl_outbound_transport, "transport_id = ? ", new String[]{transport_id+""});
//			db.delete(DatabaseHelper.tbl_outbound_receive_info, "transport_id = ? ", new String[]{transport_id+""});
// 			db.delete(DatabaseHelper.tbl_outbound_serial_product, " 1=1 ", null );
//			db.delete(DatabaseHelper.tbl_outbound_transport_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_product_code, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_lp_info, "1=1", null);
//			db.delete(DatabaseHelper.tbl_inbound_compare_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_outbound_scaned_submit_product, " transport_id="+transport_id, null);
//
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='' where columnName='outboundlpvalue'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='outboundlpstate'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='isOutbounding'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='' where columnName='outbound_id'");
//
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
// 		}finally{
// 			db.endTransaction();
// 			db.close();
//		}
//	}
//	public void deletePositionData(long transport_id){
//		db = helper.getWritableDatabase();
//		 
//		try{
//
//			db.beginTransaction();
// 			db.delete(DatabaseHelper.tbl_position_scan_info, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_product_code, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_serial_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_wareHouse_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_lp_info, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_positioned_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_different_product, "1=1", null);
//			db.delete(DatabaseHelper.tbl_position_transport, " transport_id = ? ", new String[]{transport_id+""});
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='positionlpstate'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='position_id'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='0' where columnName='isPositioning'");
//			
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
// 		}finally{
// 			db.endTransaction();
// 			db.close();
//		}
//	}
	public void deletePickupData(long pickup_id){
		db = helper.getWritableDatabase();
		try{
			db.beginTransaction();
			db.delete(DatabaseHelper.tbl_pickup_location, " pickup_id = ?  ", new String[]{pickup_id+""});
			db.delete(DatabaseHelper.tbl_pickup_location_product, " pickup_id = ?  ", new String[]{pickup_id+""});
			db.delete(DatabaseHelper.tbl_pickup_product_code, " 1=1  ", null);
			db.delete(DatabaseHelper.tbl_pickup_product_info, " 1=1 ", null);
			 db.delete(DatabaseHelper.tbl_pickup_scan_info, " 1=1 ", null);
			db.delete(DatabaseHelper.tbl_pickup_serial_product, " 1=1 ", null);
			 db.delete(DatabaseHelper.tbl_pickup_container_info, " pickup_id = ? ", new String[]{pickup_id+""});

			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_scanInfo'");
			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_location_sort'");

			db.setTransactionSuccessful();
		}catch (Exception e) {
			e.printStackTrace();
 		}finally{
 			db.endTransaction();
 			db.close();
 		}
	}
//public void deleteAllInventoryData(){
//		
//		db = helper.getWritableDatabase();
//		try{
//			db.beginTransaction();
//			db.delete(DatabaseHelper.tbl_inventory_location, " 1=1  ", null);
//			db.delete(DatabaseHelper.tbl_inventory_location_product, " 1=1  ", null);
//			db.delete(DatabaseHelper.tbl_inventory_product_code, " 1=1  ", null);
//			db.delete(DatabaseHelper.tbl_inventory_product_info, " 1=1 ", null);
//			db.delete(DatabaseHelper.tbl_inventory_serial_product, " 1=1 ", null);
//			db.delete(DatabaseHelper.tbl_inventory_scan_info, " 1=1  ", null);
//			db.delete(DatabaseHelper.tbl_inventory_difference, " 1=1 ", null);
//
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_scanInfo'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_location_sort'");
//
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
//			e.printStackTrace();
// 		}finally{
// 			db.endTransaction();
// 			db.close();
// 		}
//	}
//	public void deleteInventoryData(long area_id){
//		
//		db = helper.getWritableDatabase();
//		try{
//			db.beginTransaction();
//			db.delete(DatabaseHelper.tbl_inventory_location, " area_id = ?  ", new String[]{area_id+""});
//			db.delete(DatabaseHelper.tbl_inventory_location_product, " area_id = ?  ", new String[]{area_id+""});
//			db.delete(DatabaseHelper.tbl_inventory_product_code, " 1=1  ", null);
//			db.delete(DatabaseHelper.tbl_inventory_product_info, " 1=1 ", null);
//			db.delete(DatabaseHelper.tbl_inventory_serial_product, " 1=1 ", null);
//			db.delete(DatabaseHelper.tbl_inventory_scan_info, " area_id = ?  ", new String[]{area_id+""});
//			db.delete(DatabaseHelper.tbl_inventory_difference, " 1=1 ", null);
//
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_scanInfo'");
//			db.execSQL("update " + DatabaseHelper.tbl_config + " set columnValue='1' where columnName='pickup_location_sort'");
//
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
//			e.printStackTrace();
// 		}finally{
// 			db.endTransaction();
// 			db.close();
// 		}
//	}
//	 /**
//	  * 插入数据
//	  * @param transportProducts
//	  * @param productCodes
//	  * @param products
//	  */
//	public void addInboundDataBaseInfo( List<TransportProduct>	 transportProducts , List<ProductCode> productCodes  , List<Product> products  ) {
//		try{
//			db = helper.getWritableDatabase();
//			db.beginTransaction();
//			insertInboundTransportProduct(transportProducts,db);
//			insertInboundProductCode(productCodes,db);
//			insertInboundProduct(products,db);
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
//			throw new SystemException();
//  		}finally{
// 			db.endTransaction();
// 			db.close();
// 		}
//	}
//	public void deleteInboundBaseDataInfo(long transport_id){
//	
//
//		try{
//			db = helper.getReadableDatabase();
//			db.beginTransaction();
// 			db.delete(DatabaseHelper.tbl_inbound_transport_product, " transport_id = ? ", new String[]{transport_id+""});
//			db.delete(DatabaseHelper.tbl_inbound_product_code, " 1=1", null);
//			db.delete(DatabaseHelper.tbl_inbound_product, "1=1", null);
//			db.setTransactionSuccessful();
//		}catch(Exception e){
//			throw new SystemException();
//		}finally{
//			db.endTransaction();
//			db.close();{}
//		}
//		
//	}
//	
//	
//	
//	
//	private void insertInboundProduct( List<Product> products  , SQLiteDatabase  insertDb ){
//		db.execSQL("delete from " + DatabaseHelper.tbl_inbound_product);
//		for(Product product : products ){
//			ContentValues contentValue = new ContentValues();
//			contentValue.put("name", product.getName());
//			contentValue.put("heigth", product.getHeigth());
//			contentValue.put("length", product.getLength());
//			contentValue.put("pid", product.getPid());
//			contentValue.put("weight", product.getWeight());
//			contentValue.put("width", product.getWidth());
//			contentValue.put("sn_length", product.getSn_length());
//			db.insert(DatabaseHelper.tbl_inbound_product, null, contentValue);
//		}
//	}
//	private void insertInboundProductCode( List<ProductCode> productCodes  , SQLiteDatabase  insertDb){
//		db.execSQL("delete from " + DatabaseHelper.tbl_inbound_product_code);
//		for(ProductCode productCode : productCodes ){
//			ContentValues contentValue = new ContentValues();
//			contentValue.put("pcid", productCode.getPcid());
//			contentValue.put("code_type", productCode.getCodeType());
//			contentValue.put("barcode", productCode.getBarcode());
//			contentValue.put("p_name", productCode.getP_name());
//			db.insert(DatabaseHelper.tbl_inbound_product_code, null, contentValue);
//		}
//	}
//	private void insertInboundTransportProduct( List<TransportProduct>	 transportProducts ,SQLiteDatabase  insertDb ){
//		insertDb.execSQL("delete from " + DatabaseHelper.tbl_inbound_transport_product); // 先删除
//		for (TransportProduct transportOutbound : transportProducts) {
//			ContentValues contentValue = new ContentValues();
// 			contentValue.put("sku", transportOutbound.getSku());
//			contentValue.put("qty", transportOutbound.getQty());
//			contentValue.put("sn", transportOutbound.getSn());
//			contentValue.put("p_name", transportOutbound.getP_name());
//			contentValue.put("p_code", transportOutbound.getP_code());
//			contentValue.put("transport_id",transportOutbound.getTransport_id());
//			contentValue.put("lotnumber",transportOutbound.getLotNumber());
//
//			insertDb.insert(DatabaseHelper.tbl_inbound_transport_product, null, contentValue);
//		}
//	}
//	
//	public void addPositionDataBaseInfo(List<TransportProduct>	 transportProducts , List<ProductCode> productCodes  , List<Product> products ){
//		db = helper.getWritableDatabase();
//		try{
//			db.beginTransaction();
//			insertPositionTransportProduct(transportProducts,db);
//			insertPositionProductCode(productCodes,db);
//			insertPositionProduct(products,db);
//			db.setTransactionSuccessful();
//		}catch (Exception e) {
//			throw new SystemException();
//  		}finally{
// 			db.endTransaction();
// 			db.close();
// 		}
//	}
//	
//	private void insertPositionTransportProduct( List<TransportProduct>	 transportProducts ,SQLiteDatabase  insertDb ){
//		insertDb.execSQL("delete from " + DatabaseHelper.tbl_position_wareHouse_product); // 先删除
//		for (TransportProduct transportOutbound : transportProducts) {
//			ContentValues contentValue = new ContentValues();
// 			contentValue.put("sku", transportOutbound.getSku());
//			contentValue.put("qty", transportOutbound.getQty());
//			contentValue.put("sn", transportOutbound.getSn());
//			contentValue.put("p_name", transportOutbound.getP_name());
//			contentValue.put("p_code", transportOutbound.getP_code());
//			contentValue.put("transport_id",transportOutbound.getTransport_id());
//			contentValue.put("lotnumber",transportOutbound.getLotNumber());
//
//			insertDb.insert(DatabaseHelper.tbl_position_wareHouse_product, null, contentValue);
//		}
//	}
//	
//	private void insertPositionProductCode( List<ProductCode> productCodes  , SQLiteDatabase  insertDb){
//		db.execSQL("delete from " + DatabaseHelper.tbl_position_product_code);
//		for(ProductCode productCode : productCodes ){
//			ContentValues contentValue = new ContentValues();
//			contentValue.put("pcid", productCode.getPcid());
//			contentValue.put("code_type", productCode.getCodeType());
//			contentValue.put("barcode", productCode.getBarcode());
//			contentValue.put("p_name", productCode.getP_name());
//			db.insert(DatabaseHelper.tbl_position_product_code, null, contentValue);
//		}
//	}
//	
//	private void insertPositionProduct( List<Product> products  , SQLiteDatabase  insertDb ){
//		db.execSQL("delete from " + DatabaseHelper.tbl_position_product);
//		for(Product product : products ){
//			ContentValues contentValue = new ContentValues();
//			contentValue.put("name", product.getName());
//			contentValue.put("heigth", product.getHeigth());
//			contentValue.put("length", product.getLength());
//			contentValue.put("pid", product.getPid());
//			contentValue.put("weight", product.getWeight());
//			contentValue.put("width", product.getWidth());
//			contentValue.put("sn_length", product.getSn_length());
//			db.insert(DatabaseHelper.tbl_position_product, null, contentValue);
//		}
//	}
	//添加PickupBaseData
	public void addPickUpBaseData( ReturnBean<PickUpLocation>  pickUplocation ,  
			ReturnBean<Product>  returnBeanProduct , 
			ReturnBean<ProductCode>  returnBeanProductCode ,
			ReturnBean<PickUpSerialProduct> returnSnBean ,
			ReturnBean<PickUpLoactionProduct> returnNeedProduct  , 
			long pickup_id){
		//首先是删除基础数据,然后再添加
	
		try{
			deletePickupData(pickup_id);    //每次下载数据了吧这个先删除 （演示）

			
			db = helper.getWritableDatabase();
			db.beginTransaction();
			db.delete(DatabaseHelper.tbl_pickup_location, " pickup_id = ?   ", new String[]{pickup_id+""});
			db.delete(DatabaseHelper.tbl_pickup_product_code, " 1=1  ", null);
			db.delete(DatabaseHelper.tbl_pickup_product_info, " 1=1  ", null);
 			db.delete(DatabaseHelper.tbl_pickup_serial_product," 1=1  ", null);
			db.delete(DatabaseHelper.tbl_pickup_location_product," 1=1  ", null);
			
			//添加数据
			if(pickUplocation != null){insertPickUpLocation(pickUplocation,db);}
			if(returnBeanProduct != null){insertPickUpProduct(returnBeanProduct,db);}
			if(returnBeanProductCode != null){insertPickUpProductCode(returnBeanProductCode,db);}
			if(returnSnBean != null){insertPickupSn(returnSnBean,db);}
			if(returnNeedProduct != null){insertPickUpNeedProduct(returnNeedProduct,db);}
			
  			db.setTransactionSuccessful();
		}catch (Exception e) {
			e.printStackTrace();
 		}finally{
 			db.endTransaction();
 			db.close();
 		}
	}
	
	private void insertPickUpLocation( ReturnBean<PickUpLocation>  pickUplocation , SQLiteDatabase db){
		List<PickUpLocation> locations = pickUplocation.getArrayList();
		if(locations != null && locations.size() > 0 ){
			for(PickUpLocation location : locations){
				ContentValues values = new ContentValues();
				values.put("pickup_id", location.getPickup_id());
				values.put("location", location.getLocation());
				values.put("location_id", location.getLocation_id());
				values.put("sort", location.getSort());
				values.put("location_type", location.getLocation_type());

				db.insert(DatabaseHelper.tbl_pickup_location, null, values );
			}
			
		}
	}
	private void insertPickUpProduct(ReturnBean<Product>  returnBeanProduct  , SQLiteDatabase db){
		List<Product> arrayList =	returnBeanProduct.getArrayList();
		if(arrayList != null && arrayList.size() > 0 ){
			for(Product product : arrayList){
				ContentValues values = new ContentValues();
				values.put("pid", product.getPid());
				values.put("name", product.getName());
				values.put("length", product.getLength());
				values.put("width", product.getWidth());
				values.put("heigth", product.getHeigth());
				values.put("weight", product.getWeight());
				values.put("sn_length", product.getSn_length());
				db.insert(DatabaseHelper.tbl_pickup_product_info, null, values  );

			}
		}
	}
	private void insertPickUpProductCode(ReturnBean<ProductCode> productCodeBean , SQLiteDatabase db){
		List<ProductCode> arrayList = productCodeBean.getArrayList();
		if(arrayList != null && arrayList.size() > 0){
			for(ProductCode productCode : arrayList){
				ContentValues values = new ContentValues();
				values.put("pcid", productCode.getPcid());
				values.put("barcode", productCode.getBarcode());
				values.put("code_type", productCode.getCodeType());
				values.put("p_name", productCode.getP_name());
 				db.insert(DatabaseHelper.tbl_pickup_product_code, null, values  );

			}
		}
	}
	private void insertPickupSn(ReturnBean<PickUpSerialProduct> returnBean , SQLiteDatabase db){
		List<PickUpSerialProduct> arrayList = returnBean.getArrayList() ;
		if(arrayList != null && arrayList.size() > 0){
			for(PickUpSerialProduct sn : arrayList){
				ContentValues values = new ContentValues();
				values.put("pcid", sn.getPcid());
				values.put("sn", sn.getSn());
			    db.insert(DatabaseHelper.tbl_pickup_serial_product, null, values  );
			}
		}
	}
	private void insertPickUpNeedProduct(ReturnBean<PickUpLoactionProduct> returnNeedProduct , SQLiteDatabase db){
		List<PickUpLoactionProduct>  arrayList =  returnNeedProduct.getArrayList() ;
		if(arrayList != null && arrayList.size() > 0){
			for(PickUpLoactionProduct needProduct : arrayList){
				
			    ContentValues values = new ContentValues();
			    values.put("pcid", needProduct.getPcid());
			    values.put("qty", needProduct.getQty());
			    values.put("location", needProduct.getLocation());
			    values.put("location_id", needProduct.getLocation_id());
			    values.put("pickup_id", needProduct.getPickup_id());
			    values.put("slcId", needProduct.getSlcId());
			    values.put("sn", needProduct.getSn());
			    values.put("from_container_type", needProduct.getFrom_container_type());
			    values.put("from_container_type_id", needProduct.getFrom_container_type_id());
			    values.put("from_con_id", needProduct.getFrom_con_id());
			    
			    
			    
			    values.put("pick_container_Type", needProduct.getPick_container_Type());
			    values.put("Pick_container_Type_id", needProduct.getPick_container_Type_id());
			    values.put("pick_con_id", needProduct.getPick_con_id());
			    values.put("pick_container_qty", needProduct.getPickContainerQty());

				db.insert(DatabaseHelper.tbl_pickup_location_product, null, values   );

			}
			Log.i("data", "insert in table   size : " + arrayList.size());
		}
	}
	//处理在提交拣货的数据成功过后
	//删除这个Location的拣货的信息
	public  void afterSubmitPickupLocationScanInfo(long location_id , long pickup_id , ReturnBean<PickUpLoactionProduct> returnNeedProduct){ 
		try{
			db = helper.getWritableDatabase() ;
		    while (db.isDbLockedByOtherThreads() || db.isDbLockedByCurrentThread()){  
		            Thread.sleep(10);  
		    }
		
			
 			db.beginTransaction();
			db.delete(DatabaseHelper.tbl_pickup_scan_info, " location_id = ? and pickup_id =? ", new String[]{location_id+"",pickup_id+""});
			db.delete(DatabaseHelper.tbl_pickup_container_info, " location_id = ? and pickup_id = ? ", new String[]{location_id+"",pickup_id+""});
			db.delete(DatabaseHelper.tbl_pickup_location_product," location_id = ? and pickup_id = ? ", new String[]{location_id+"",pickup_id+""});
			insertPickUpNeedProduct(returnNeedProduct, db);
			db.setTransactionSuccessful();
		}catch(Exception e){
 			 e.printStackTrace();
		}finally{
 			db.endTransaction();
 			if(db != null && db.isOpen()){db.close();}
		}
	}
     	 
//	public void addInventoryBaseProduct(ReturnBean<InventoryLocationProduct>  returnBeanLocationProduct ,
//			ReturnBean<InventoryLocation> returnBeanLocation ,
//			ReturnBean<Product> returnBeanProduct,
//			ReturnBean<ProductCode> returnBeanCode ,
//			ReturnBean<InventoryProductStoreContainer>  productStoreContainer ){
//			try{
//				db = helper.getWritableDatabase() ;
//			    while (db.isDbLockedByOtherThreads() || db.isDbLockedByCurrentThread()){  
//			            Thread.sleep(10);  
//			    }
//			    //插入到数据库中
//			    db.beginTransaction(); 
//			    //首先删除数据,然后再添加数据
//			    db.delete(DatabaseHelper.tbl_inventory_lp_info, " 1=1 ", null);
//			    db.delete(DatabaseHelper.tbl_inventory_scan_info, " 1=1 ", null);
//			    db.delete(DatabaseHelper.tbl_inventory_location, " 1=1  ", null);
//				db.delete(DatabaseHelper.tbl_inventory_location_product, " 1=1  ", null);
//				db.delete(DatabaseHelper.tbl_inventory_product_code, " 1=1  ", null);
//				db.delete(DatabaseHelper.tbl_inventory_product_info, " 1=1 ", null);
//				db.delete(DatabaseHelper.tbl_inventory_product_store_container, " 1=1 ", null);
//
//				
//			    insertInventoryLoactionProduct(returnBeanLocationProduct,db);
//			    insertInventoryLocation(returnBeanLocation,db);
//			    insertInventoryProduct(returnBeanProduct,db);
//			    insertInventoryProductCode(returnBeanCode,db);
//			    insertInventoryProductStoreContainer(productStoreContainer,db);
//			    db.setTransactionSuccessful();
//			}catch(Exception e){
//				e.printStackTrace();
//			}finally{
//				db.endTransaction();
//	 			if(db != null && db.isOpen()){db.close();}
//			}
//	}
//	
//	//inventory location 
//	private void insertInventoryLocation(ReturnBean<InventoryLocation>  returnBeanLocation,SQLiteDatabase db){
//
//		if(returnBeanLocation != null ){
//			List<InventoryLocation> arrayList =  returnBeanLocation.getArrayList() ;
//			if(arrayList != null && arrayList.size() > 0 ){
//				for(InventoryLocation temp : arrayList){
//					ContentValues values = new ContentValues();
// 					values .put("area_id", temp.getArea_id());
//					values.put("location", temp.getLocation());
//					values.put("sort", temp.getSort());
//					values.put("location_id", temp.getLocation_id());
//   					db.insert(DatabaseHelper.tbl_inventory_location, null, values );
//					
//				}
//			}
//		}
//	}
//	
//	//invenvtory  location product 
//	private void insertInventoryLoactionProduct(ReturnBean<InventoryLocationProduct>  returnBeanLocationProduct ,SQLiteDatabase db){
//		if(returnBeanLocationProduct != null ){
//			List<InventoryLocationProduct>  inventoryLocations =  returnBeanLocationProduct.getArrayList() ;
//			if(inventoryLocations != null && inventoryLocations.size() > 0 ){
//				for(InventoryLocationProduct temp : inventoryLocations){
//					ContentValues values = new ContentValues();
//					values.put("pcid", temp.getPcid());
//					values.put("qty", temp.getQty());
//					values.put("location", temp.getLocation());
//					values.put("slcId", temp.getSlcId());
//					values.put("area_id", temp.getArea_id());
//					values.put("sn", temp.getSn());
//					values.put("location_id", temp.getLocation_id());
// 					db.insert(DatabaseHelper.tbl_inventory_location_product, null, values );
//				}
//			}
//		}
//	}
//	//商品的基础的信息
//	private void insertInventoryProduct(ReturnBean<Product> returnBeanProduct, SQLiteDatabase db){
//		if(returnBeanProduct != null){
//			List<Product> productCodes = returnBeanProduct.getArrayList();
//			if(productCodes != null && productCodes.size() > 0 ){
//				for(Product product : productCodes ){
//					ContentValues values = new ContentValues();
//					values.put("pid", product.getPid());
//					values.put("name", product.getName());
//					values.put("length", product.getLength());
//					values.put("width", product.getWidth());
//					values.put("heigth", product.getHeigth());
//					values.put("weight", product.getWeight());
//					values.put("sn_length", product.getSn_length());
//					db.insert(DatabaseHelper.tbl_inventory_product_info, null, values  );
//				}
//			}
//		}
//		
//		 
//		
//	}
//	//插入ProductCode
//	private void insertInventoryProductCode(ReturnBean<ProductCode> returnBeanCode , SQLiteDatabase db){
//
//		List<ProductCode> arrayList = returnBeanCode.getArrayList();
//		if(arrayList != null && arrayList.size() > 0){
//			for(ProductCode productCode : arrayList){
//				ContentValues values = new ContentValues();
//				values.put("pcid", productCode.getPcid());
//				values.put("barcode", productCode.getBarcode());
//				values.put("code_type", productCode.getCodeType());
//				values.put("p_name", productCode.getP_name());
// 				db.insert(DatabaseHelper.tbl_inventory_product_code, null, values  );
//			}
//		}
//	}
//	//接地容器的 上面的数量()
//	private void insertInventoryProductStoreContainer(ReturnBean<InventoryProductStoreContainer>  productStoreContainer ,SQLiteDatabase db){
//		List<InventoryProductStoreContainer> arrayList = productStoreContainer.getArrayList();
//		if(arrayList != null && arrayList.size() > 0){
//			for(InventoryProductStoreContainer temp : arrayList){
//				ContentValues values = new ContentValues();
//				values.put("pcid", temp.getPcid());
//				values.put("qty", temp.getQty());
//				values.put("container_id", temp.getContainer_id());
//				values.put("container_type", temp.getContainer_type());
//				values.put("container_type_id", temp.getContainer_type_id());
//				values.put("title", temp.getTitle());
//				values.put("title_name", temp.getTitleName());
//				values.put("slcid", temp.getSlcid());
//				values.put("location", temp.getLocation());
//				values.put("area_id", temp.getAreaId());
//				
// 				db.insert(DatabaseHelper.tbl_inventory_product_store_container, null, values  );
//			}
//		}
//	}
 }
