package support.dbhelper;

import java.io.File;

import utility.Utility;
import android.content.Context;
import android.os.Environment;

public class Goable {

	public static final String from_product_list_to_create = "from_product_list_to_create";
	public static final String from_product_create = "from_product_create";
	public static String Machine = "";

	public static String LoginAccount = "";
	public static String Password = "";
	public static boolean hasSd = false;
	public static String Version = "";

	public static long adid = 0L;
	public static long adgid = 0L;
	public static int CLIENT_ADGID = 100026;// 客户部门ID

	public static int adminUsersPerRow = 3;

	// ====全局路径=======================================================

	public static final String file_base_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/";

	public static final String file_image_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/"; // 拍照原始的地址

	// public static final String file_image_path_takephoto =
	// Environment.getExternalStorageDirectory().getPath() +
	// "/vvme/image/takephoto/"; // PhotoView的本地图片

	public static final String file_image_path_up = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/up/"; // 拍照上传图片的地址

	public static final String file_image_path_thumbnail = Environment.getExternalStorageDirectory().getPath() + "/vvme/image/thumbnail/"; // 拍照图片生成缩略图的地址

	// android has sd .file dirs
	public static final String file_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/files/";

	public static final String file_cache_path = Environment.getExternalStorageDirectory().getPath() + "/vvme/cache/";

	/**
	 * 音频目录
	 * 
	 * @return
	 */
	public static File getDir_Audio() {
		File file = new File(getHomeDir(), "audio_tmp");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	/**
	 * 临时图片目录
	 * 
	 * @return
	 */
	public static File getDir_TmpImg() {
		File file = new File(getHomeDir(), "image/tmp");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	public static File getDir_takephoto() {
		File file = new File(getHomeDir(), "image/takephoto");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	/**
	 * 用于tabphoto
	 * 
	 * @return
	 */
	public static File getDir_photoCache() {
		File file = new File(getHomeDir(), "image/cache");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	public static File getDir_photoCapture() {
		File file = new File(getHomeDir(), "image/takephoto/PhotoCapture");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	/**
	 * load/receive-photo
	 * 
	 * @return
	 */
	public static File getDir_PhotoLR() {
		File file = new File(getHomeDir(), "image/takephoto/PhotoLR");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	public static File getHomeDir() {
		File file = new File(Environment.getExternalStorageDirectory(), "vvme");
		if (!file.exists())
			file.mkdirs();
		return file;
	}

	public static void initGoable(Context context) {
		if (Goable.LoginAccount != null && Goable.LoginAccount.trim().length() > 0) {
			return;
		}
		Goable.LoginAccount = StoredData.readUserName();
		Goable.Password = StoredData.readPassword();
		Goable.Version = Utility.getVersionName(context);
		Goable.Machine = "";
		Goable.hasSd = Utility.isSDExits();
	}

	// 重置-登录信息,如:切换账号、注销时
	public static void resetLoginInfo_inRam() {
		Goable.LoginAccount = "";
		Goable.Password = "";
		Goable.Machine = "";
	}

}
