package support.dbhelper;

 import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String name = "paypal20.db"; //数据库名称   
	private static final int version = 39; 			  //数据库版本   
	
	public static final String tbl_config = "config";	//用来记录当前的一些状态。
	public static final String tbl_upfile  = "upflie" ; 	//file表
//	public static final String tbl_config_title = "config_title" ;	//inventory 使用临时的title
//	public static final String tbl_recent_product_container = "recent_product_container" ;	//最近常用的recent_container
//	public static final String tbl_receive_info = "receive_info" ; 
//	public static final String tbl_transport = "transport";
//	public static final String tbl_serial_product = "serial_product";
//	
//	
//	
//	public static final String tbl_inbound_transport_product= "transport_product";
//	public static final String tbl_inbound_product = "product";
	public static final String tbl_inbound_product_code = "inbound_product_code";
//	public static final String tbl_inbound_lp_info =  "lp_info";
//	public static final String tbl_inbound_compare_product = "inbound_compare_product" ;
//	public static final String tbl_inbound_positioned_product = "inbound_positioned_product";	// 收货放货的时候显示所有已经放货的列表
//	public static final String tbl_inbound_need_positioned_product  = "inbound_need_positioned_product"; //待放货表
//	public static final String tbl_inbound_ware_house  = "inbound_recevied_product"; //已经收到的货物
//
//	 
//	//outbound
//	public static final String tbl_outbound_transport = "outbound_transport" ;			//transport 列表
//	public static final String tbl_outbound_receive_info = "outbound_receive_info";		//扫描的表
//	public static final String tbl_outbound_serial_product = "outbound_serial_product"; //序列号表
//	public static final String tbl_outbound_product = "outbound_product";				//商品的长宽高
//	public static final String tbl_outbound_product_code = "outbound_product_code";		//sku 和 barcode 对应的表
//	public static final String tbl_outbound_transport_product = "outbound_transport_product"; //那些商品需要出库的表
//	public static final String tbl_outbound_lp_info = "outbound_lp_info";				//收托的表
//	public static final String tbl_outbound_different_product = "outbound_different_product" ; // compare的时候差异
//	public static final String tbl_outbound_scaned_submit_product = "outbound_scaned_submit_product" ; // compare的时候差异
//
//	//position 
//	public static final String tbl_position_transport = "position_transport";
// 	public static final String tbl_position_scan_info = "position_scan_info" ;
//	public static final String tbl_position_product = "position_product" ;
//	public static final String tbl_position_product_code = "position_product_code";
//	public static final String tbl_position_serial_product = "position_serial_product";
//	public static final String tbl_position_wareHouse_product = "position_warehouse_product" ;
//	public static final String tbl_position_lp_info = "position_lp_info";
//	public static final String tbl_position_positioned_product= "positioned_product";
//	public static final String tbl_position_different_product = "position_different_product" ;
	
	
	
	//pick up
	public static final String tbl_pickup = "pickup" ;
	public static final String tbl_pickup_location = "pickup_location" ;
	public static final String tbl_pickup_product_info = "pickup_product_info";
	public static final String tbl_pickup_product_code = "pickup_product_code";
	public static final String tbl_pickup_serial_product= "pickup_serial_product";
	public static final String tbl_pickup_location_product = "pickup_location_product";
	public static final String tbl_pickup_scan_info = "pickup_scan_info" ;
	public static final String tbl_pickup_container_info = "pickup_container_info" ;			   //拣货的时候捡的Container
	
//	//inventory 
//	public static final String tbl_inventory_area = "inventory_area";				   //盘点的区域
//	public static final String tbl_inventory_product_info = "inventory_product_info" ; //商品的基本信息 长宽高
//	public static final String tbl_inventory_product_code = "inventory_product_code" ; //盘点的所有的商品Code 
//	public static final String tbl_inventory_location = "inventory_location" ;			//盘点的区域位置
//	public static final String tbl_inventory_location_product = "inventory_location_product" ;	//盘点的区域的位置上面的商品
//	public static final String tbl_inventory_serial_product = "inventory_serial_product";	 	//序列号
//	public static final String tbl_inventory_scan_info = "inventory_scan_info" ;	//inventory 扫描
//	public static final String tbl_inventory_difference = "inventory_difference" ;  //inventory提交后的差异
//	public static final String tbl_inventory_product_store_container = "inventory_product_store_container" ;  //inventory提交后的差异
// 	public static final String tbl_inventory_lp_info = "inventory_lp_info" ;
//	//
//	public static final String TAB_PRODUCT_STORAGE_CATALOG = "product_storage_catalog"; // 仓库
//	public static final String TAB_COUNTRY_CODE = "country_code";// 国家表
//	public static final String TAB_COUNTRY_PROVINCE = "country_province";//省份表
//	public static final String TAB_TRANSPORT_RESOURCE = "transport_resource";//运输资源表
//	public static final String TAB_FREIGHT_ITEM = "transport_item";//运费项目表
//	public static final String TAB_TRANSPORT = "transport_temp";
//	public static final String TAB_ADMIN_USERS = "admin_users";
//	public static final String TAB_PRODURES = "produres";//produres
//	public static final String TAB_PRODURES_DETAILS = "produres_details";
//	public static final String TAB_PRODURES_NOTICE = "produres_notice";
//	
//	public static final String TBL_DELIVERY_CONFIG = "delivery_config";	//用来记录当前的一些状态。
//	public static final String TBL_DELIVERY_SCAN_PRODUCT = "delivery_scan_product" ; //receive_info
//     	 
//	//delivery outbound
//	public static final String TBL_DELIVERY_OUTBOUND_RECEIVE_INFO = "delivery_outbound_receive_info";		//扫描的表
//  	public static final String TBL_DELIVERY_OUTBOUND_TRANSPORT_PRODUCT = "delivery_outbound_transport_product"; //有用 //那些商品需要出库的表
//	public static final String TBL_DELIVERY_PRODUCT_CODE_SEARCH_SAME = "delivery_product_code_search_same";//通过sku查询商品，查出多个
//	public static final String TBL_STORAGE_PRODUCT = "storage_product";	//有用
//	public static final String TBL_STORAGE_PRODUCT_CODE = "storage_product_code"; //有用
//	public static final String TBL_STORAGE_PRODUCT_SERIAL = "storage_product_serial";
// 	
//	public static final String TBL_DELIVERY_TRANSPORT_OUTBOUND_PRODUCT = "delivery_transport_outbound_product";
//	public static final String TBL_DELIVERY_OUTBOUND_TRANSPORT = "delivery_outbound_transport"; //有用
//
//	//create_product 
//	
//	public static final String TBL_BASEDATA_PRODUCT = "basedata_product" ;
//	public static final String TBL_BASEDATA_FILE= "basedata_file" ;
//	
//	public static final String TBL_DELIVERY_PRODURE_SUBMIT = "delivery_produre_submit";
//	public static final String TBL_DELIVERY_FREIGHT_SUBMIT = "delivery_freight_submit";
//	public static final String TBL_PRODUCT_CODE_TEMP = "product_code_temp";
//	
//	public static final String TBL_PRODUCT_TITLE = "product_title";//商品title信息 在创建商品的时候用的
//	public static final String TBL_PRODUCT_TITLES = "product_titles";//商品title各种所属信息在创建商品的时候用的
//	public static final String TBL_TITLE = "title";	//在创建商品的时候用的
//	
//	public static final String TBL_LP_INFO_TRANSPORT =  "lp_info_transport";
//	public static final String TBL_CONTAINER_LOAD_TRANSPORT = "container_load_transport";//容器与容器的关系表
//	public static final String TBL_CONTAINER_LOAD_CHILD_LIST = "container_load_child_list";//容器与容器关系的平铺表
//	public static final String TBL_PRINTER_TCP_CONNECT =	 "printer_tcp_connect" ;	 //本地添加的连接打印机的设置 
//	
//	
//	//yard_control 的时候使用的
//	public static final String TBL_SPOT_AREA = "spot_area" ;
//	public static final String TBL_DOCK_ZONE = "dock_zone" ;
//	
//	
//	//single_create_container 纯打托
// 	public static final String tbl_single_lp_info = "single_lp_info" ;
// 	public static final String tbl_single_scan_info = "single_scan_info" ;
// 	public static final String tbl_single_product_code = "single_product_code";
	
	
	
	public DatabaseHelper(Context context) {
		super(context, name, null, version);
	}
	
/*	private static DatabaseHelper mInstance;   
    public synchronized static DatabaseHelper getInstance(Context context) {  
    	
    	if (mInstance == null){  
            synchronized (DatabaseHelper.class) {  
                if (mInstance == null){  
                	DatabaseHelper temp = new DatabaseHelper(context);  
                	mInstance = temp;  
                }  
            }  
        }  
         return mInstance;  
    }; */
     
	@Override
	public void onCreate(SQLiteDatabase db) {
		
//		StringBuffer tbl_receive_infoBuffer = new StringBuffer("create table if not exists ");
//		tbl_receive_infoBuffer.append(tbl_receive_info+"(id integer primary key autoincrement,"); 
//		tbl_receive_infoBuffer.append("sn varchar2(50),") ;
//		tbl_receive_infoBuffer.append("sku varchar2(30), ");			//为扫描的东西barCode
//		tbl_receive_infoBuffer.append("quantity double,");			//serial_number
//		tbl_receive_infoBuffer.append("createTime timestamp,");	
//		tbl_receive_infoBuffer.append("palletNum varchar2(30),");
//		tbl_receive_infoBuffer.append("transport_id  integer , ");
//		tbl_receive_infoBuffer.append("isSubmit integer default 0, ");
//		tbl_receive_infoBuffer.append("isSnRepeat integer default 0 , ");
//		tbl_receive_infoBuffer.append("place varchar2(40) DEFAULT '', ");
//		tbl_receive_infoBuffer.append("p_name DEFAULT '' ,");
//		tbl_receive_infoBuffer.append("p_id  integer  default 0 ,");			//商品的ID
//		tbl_receive_infoBuffer.append("barcode_type  varchar2(10)  default '',  ");			//商品的ID
//		tbl_receive_infoBuffer.append("pallet_id  integer ");			// 
//
//		
//		tbl_receive_infoBuffer.append(")");
//		db.execSQL(tbl_receive_infoBuffer.toString());
//		
//		 
//	
//		StringBuffer transport = new StringBuffer("create table if not exists ");
//		transport.append(tbl_transport+" (id integer primary key autoincrement , ");
//		transport.append("transport_id integer, ");
//		transport.append("fromWarehouse varchar2(30), ");
//		transport.append("toWarehouse varchar2(30), ");
//		transport.append("area varchar2(50), ");
//		transport.append("state varchar2(50),");
//		transport.append("title_id integer,");
//		transport.append("title_name varchar2(50)");
//		
//		transport.append(")");
//		db.execSQL(transport.toString());
		
		
		StringBuffer serialProduct = new StringBuffer("create table if not exists ");
		serialProduct.append("serial_product(id integer primary key autoincrement,"); 
		serialProduct.append("product_serial_id integer,") ;
		serialProduct.append("sku integer, ");			//pc_id
		serialProduct.append("sn varchar(80),");			//serial_number
		serialProduct.append("supplier_id integer,");	
		serialProduct.append("in_time varchar(30),");
		serialProduct.append("out_time varchar(30)");
		serialProduct.append(")");
		db.execSQL(serialProduct.toString());
		
//		StringBuffer transportProduct = new StringBuffer("create table if not exists ");
//		transportProduct.append(tbl_inbound_transport_product+" (to_id integer primary key ,");
//		transportProduct.append("sku integer ,"); 				//pc_id;
//		transportProduct.append("qty double,"); 				//数量
//		transportProduct.append("transport_id integer,"); 		//transport_id
//		transportProduct.append("sn varchar(100),");			//sn
//		transportProduct.append("p_name varchar(50),");		//name
//		transportProduct.append("p_code varchar(50) ,");			//code
//		transportProduct.append("lotnumber varchar(50)");		//lotNumber
//		transportProduct.append(")");
//		db.execSQL(transportProduct.toString());
//		
//		StringBuffer product = new StringBuffer("create table if not exists ");
//		product.append(tbl_inbound_product+"(id integer primary key autoincrement,");
//		product.append("pid integer,");
//		product.append("name varchar(80),");
//		product.append("length double,");
//		product.append("width double,");
//		product.append("heigth double,");
//		product.append("weight double,");
//		product.append("sn_length integer");
//		product.append(")");
//		
//		db.execSQL(product.toString());
		
		
		StringBuffer productCode = new StringBuffer("create table if not exists ");
		productCode.append(tbl_inbound_product_code+" (id integer primary key autoincrement,");
		productCode.append("pcid integer , "); 
		productCode.append("barcode varchar(80),") ;
		productCode.append("code_type varchar(10),");
		productCode.append("p_name varchar(80)");
		productCode.append(")");
		db.execSQL(productCode.toString());
//		
//		StringBuffer lpInfo = new StringBuffer("create table if not exists ");
//		lpInfo.append(tbl_inbound_lp_info+"(id integer primary key autoincrement,");
//		lpInfo.append("lp integer,");
//		lpInfo.append("typeName varchar(80) , ");
//		lpInfo.append("isReceive  integer, ");
//		lpInfo.append("lpName  varchar(20) ,");
//		lpInfo.append("bill_id  integer ,");
//		lpInfo.append("container_type_id  integer ,");
//		
//		lpInfo.append("container_has_sn  integer ,");
//		lpInfo.append("is_full  integer ,");
//		lpInfo.append("container_type  varchar(10) ,");
//		lpInfo.append("sub_container_type  varchar(10) ,");
//		lpInfo.append("sub_container_type_id  integer,");
//		lpInfo.append("sub_count  integer ,");
//		lpInfo.append("container_pcid  integer ,");
//		lpInfo.append("container_pc_name  varchar(10) ,");
//		lpInfo.append("container_pc_count  integer , ");
//		lpInfo.append("parent_container_id  integer,");
//		lpInfo.append("location_id  integer default 0,");
//		 
//		lpInfo.append("location_name varchar(50), ");
//		lpInfo.append("title_id integer, ");
//		lpInfo.append("title varchar(50), ") ;
//		lpInfo.append("lotNumber varchar(20), ");
//		lpInfo.append("is_append_success integer default 0 ,");
//		lpInfo.append("is_server_copy integer default 0 ");
//		lpInfo.append(")");
//		db.execSQL(lpInfo.toString());
		
		
		
		StringBuffer inboundPositionedProduct = new StringBuffer("create table if not exists ");
		inboundPositionedProduct.append("inbound_positioned_product(id integer primary key autoincrement,");
		inboundPositionedProduct.append("pcid integer ,");
		inboundPositionedProduct.append("machine varchar(20),");
		inboundPositionedProduct.append("qty double,");
		inboundPositionedProduct.append("sn varchar(80),");
		inboundPositionedProduct.append("transport_id integer ,  ");
		inboundPositionedProduct.append("location varchar(80)  ");
		inboundPositionedProduct.append(")");
		db.execSQL(inboundPositionedProduct.toString());
		
		StringBuffer innboundNeedPositionDifferentProduct = new StringBuffer("create table if not exists ");
		innboundNeedPositionDifferentProduct.append("inbound_need_positioned_product(id integer primary key autoincrement,");
		innboundNeedPositionDifferentProduct.append("pcid integer ,");
		innboundNeedPositionDifferentProduct.append("machine varchar(20) ,");
		innboundNeedPositionDifferentProduct.append("different_type varchar(20) ,");
		innboundNeedPositionDifferentProduct.append("sn varchar(80) ,");
		innboundNeedPositionDifferentProduct.append("transport_id integer ");
		innboundNeedPositionDifferentProduct.append(")");
		db.execSQL(innboundNeedPositionDifferentProduct.toString());
		
		
		StringBuffer inboundCompareProduct = new StringBuffer("create table if not exists ");
		inboundCompareProduct.append("inbound_compare_product(id integer primary key autoincrement , ") ;
		inboundCompareProduct.append("pcid integer,") ;
		inboundCompareProduct.append("sn varchar(100) , ");
		inboundCompareProduct.append("machine varchar(20) ,");
		inboundCompareProduct.append("qty double , ");
		inboundCompareProduct.append("lp varchar(20),");
		inboundCompareProduct.append("different_type varchar(20),");
		inboundCompareProduct.append("type integer ,");  //1表示表示的是sn级别.2表示的是 sku级别的
		inboundCompareProduct.append("transport_id integer");
		inboundCompareProduct.append(")");
		db.execSQL(inboundCompareProduct.toString());
		
		StringBuffer transportWareHouse = new StringBuffer("create table if not exists ");
		transportWareHouse.append("transport_ware_house (to_id integer primary key ,");
		transportWareHouse.append("sku integer ,"); 				//pc_id;
		transportWareHouse.append("qty double,"); 				//数量
		transportWareHouse.append("transport_id integer,"); 		//transport_id
		transportWareHouse.append("sn varchar(100),");			//sn
		transportWareHouse.append("p_name varchar2(50),");		//name
		transportWareHouse.append("p_code varchar2(50)");			//code
		transportWareHouse.append(")");
		db.execSQL(transportWareHouse.toString());
		
		//config 表
		StringBuffer config = new  StringBuffer("create table if not exists ");
		config.append("config ( id integer primary key autoincrement , ") ; 
		config.append("columnName varchar(30) , ") ;  
		config.append("columnValue varchar(100)  ") ; 
		config.append(")");
		db.execSQL(config.toString());
		db.execSQL("insert into config('columnName','columnValue') values('position_id','0')") ;	//表示position当前处理的tansport_id,主要是用来做清除数据用的
		db.execSQL("insert into config('columnName','columnValue') values('inbound_id','0')") ;	//表示inbound当前处理的tansport_id,主要是用来做清除数据用的
		db.execSQL("insert into config('columnName','columnValue') values('lpstate','0')") ; 	//0 表示的最开始进入的时候 。1:表示出去打托状态 .2:表示的收托的状态
		db.execSQL("insert into config('columnName','columnValue') values('lpvalue','')") ; 	//不管是打托 还是收托的时候。Lptext里面的数据
		
		db.execSQL("insert into config('columnName','columnValue') values('outboundlpstate','0')") ; 	//0 表示的最开始进入的时候 。1:表示出去打托状态 .2:表示的收托的状态
		db.execSQL("insert into config('columnName','columnValue') values('outboundlpvalue','')") ; 	//不管是打托 还是收托的时候。Lptext里面的数据

		db.execSQL("insert into config('columnName','columnValue') values('outbound_transport_id','0')") ; 	//表示当前正在操作出库的transport_id; 每次进入outboundActivity的时候去判断是否去加载基础的数据
		db.execSQL("insert into config('columnName','columnValue') values('outbound_scan_view','1')") ; 	//outbound scan view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('outbound_list_view','2')") ; 	//outbound sku view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('outbound_compare_view','2')") ; 	//outbound compare view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('isOutbounding','0')") ; 			//  .isOutbounding表示的是当前是否有新的装货货。有那么在compare 界面就要提交数据
		db.execSQL("insert into config('columnName','columnValue') values('outbound_id','0')") ; 			//  .当前outbound 处理的Id 用来删除基础数据用的。

		
		
		db.execSQL("insert into config('columnName','columnValue') values('inbound_scan_view','1')") ; 	//inbound scan view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('inbound_sku_view','2')") ; 	//inbound sku view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('inbound_compare_view','2')") ; 	//inbound compare view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('inbound_receive_product_positioned','2')") ; 	//inbound_receive_product_positioned view 1表示的是待放货的. 2:表示是group by的视图.

		
		db.execSQL("insert into config('columnName','columnValue') values('isReceiveing','0')") ; 	// .isReceiveing表示的是当前是否有新的收货。有那么在compare 界面就要提交数据
		db.execSQL("insert into config('columnName','columnValue') values('position_scan_view','1')") ; 	// position scan view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('position_sku_view','2')") ; 	// position sku view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('position_compare_view','2')") ; 	// position compare view 1表示的是 详细的视图. 2:表示是group by的视图.
		db.execSQL("insert into config('columnName','columnValue') values('isPositioning','0')") ; 	// .isPositioning表示的是当前正在放货。有那么在compare 界面就要提交数据
		db.execSQL("insert into config('columnName','columnValue') values('positionlpstate','0')") ; 	//.positionlpstate表示的当前放货的lpstate的状态0:表示的是没有扫描lp.1表示在扫描lp
		db.execSQL("insert into config('columnName','columnValue') values('position_positoned_view','1')") ; 	//positioned view 1表示的是普通的视图 。2:表示的是sku视图
		db.execSQL("insert into config('columnName','columnValue') values('is_need_load_positoned_data','1')") ; 	// 1.表示的是进入positioned的页面需要下载数据.0表示的不需要下载
		db.execSQL("insert into config('columnName','columnValue') values('is_need_load_received_data','1')") ; 	// 1.表示的是进入received的页面需要下载数据.0表示的不需要下载
		db.execSQL("insert into config('columnName','columnValue') values('is_need_load_compare_data','1')") ; 	// 1.表示的是进入compare的页面需要下载数据.0表示的不需要下载
		db.execSQL("insert into config('columnName','columnValue') values('pickup_scanInfo','1')") ; 	//  scaninfo 的列表 sn 还是group 视图
		db.execSQL("insert into config('columnName','columnValue') values('picking_id','0')") ; 	//  正在拣货的Id 。 
		db.execSQL("insert into config('columnName','columnValue') values('pickup_list','1')") ; 	//  list 的列表 sn 还是group 视图
		//pickup
		db.execSQL("insert into config('columnName','columnValue') values('pickup_location','')") ; 	// 表示正在拣货的位置名字
		db.execSQL("insert into config('columnName','columnValue') values('pickup_location_sort','1')") ; 	// 表示正在拣货的位置pickup_location_sort  
		db.execSQL("insert into config('columnName','columnValue') values('net_work','0')") ; 	//  当前网络.0表示不通.1表示通

		//inventory
		db.execSQL("insert into config('columnName','columnValue') values('inventory_list','1')") ; 	//  inventory_list 的列表 sn 还是group 视图
		db.execSQL("insert into config('columnName','columnValue') values('inventory_scanInfo','1')") ; 	//  inventory_scaninfo 的列表 sn 还是group 视图
		db.execSQL("insert into config('columnName','columnValue') values('inventory_area_id','0')") ; 	//   正在盘点的区域

//		db.execSQL("insert into config('columnName','columnValue') values('login_adid','')") ; 	//    登录的用户Id
//		db.execSQL("insert into config('columnName','columnValue') values('login_psd','')") ; 	//    登录的password 
//		db.execSQL("insert into config('columnName','columnValue') values('machine','')") ; 	//    登录的机器名
		
		db.execSQL("insert into config('columnName','columnValue') values('sp_la','US')") ; 	//  语言 US
		db.execSQL("insert into config('columnName','columnValue') values('sp_model','1')") ; 	//  前一位 , 1顺序，2.倒序,0 ：表示全部读取.
		db.execSQL("insert into config('columnName','columnValue') values('sp_model_length','4')") ; 	//  默认表示是读取4位，

		db.execSQL("insert into config('columnName','columnValue') values('sp_speed','1')") ; 	//  语速慢  

		db.execSQL("insert into config('columnName','columnValue') values('printer_connect_type','0')") ; 	//0:表示没有设置连接,1:TCP,2:USB,3:蓝牙
		db.execSQL("insert into config('columnName','columnValue') values('printer_connect_ip','')") ; 	// ip address
		db.execSQL("insert into config('columnName','columnValue') values('printer_connect_port','')") ; 	// port : 9100
		db.execSQL("insert into config('columnName','columnValue') values('printer_connect_mac','')") ; 	// 蓝牙连接的时候，使用的mac地址

		
		
		
		StringBuffer outboundTransport = new StringBuffer("create table if not exists ");
		outboundTransport.append("outbound_transport(id integer primary key autoincrement , ");
		outboundTransport.append("transport_id integer, ");
		outboundTransport.append("fromWarehouse varchar2(30), ");
		outboundTransport.append("toWarehouse varchar2(30), ");
		outboundTransport.append("area varchar2(50), ");
		outboundTransport.append("state varchar2(50) , ");
		outboundTransport.append("reserve varchar2(50)");
		
		outboundTransport.append(")");
		db.execSQL(outboundTransport.toString());
		
		//outbound ----	outbound_serial_product
		StringBuffer outBoundSerialProduct = new StringBuffer("create table if not exists ");
		outBoundSerialProduct.append("outbound_serial_product(id integer primary key autoincrement,"); 
		outBoundSerialProduct.append("product_serial_id integer,") ;
		outBoundSerialProduct.append("sku integer, ");			//pc_id
		outBoundSerialProduct.append("sn varchar(80),");			//serial_number
		outBoundSerialProduct.append("supplier_id integer,");	
		outBoundSerialProduct.append("in_time varchar(30),");
		outBoundSerialProduct.append("out_time varchar(30)");
		outBoundSerialProduct.append(")");
		db.execSQL(outBoundSerialProduct.toString());
		
		StringBuffer outboundProduct = new StringBuffer("create table if not exists ");
		outboundProduct.append("outbound_product(id integer primary key autoincrement,");
		outboundProduct.append("pid integer,");
		outboundProduct.append("name varchar(80),");
		outboundProduct.append("length double,");
		outboundProduct.append("width double,");
		outboundProduct.append("heigth double,");
		outboundProduct.append("weight double");
		outboundProduct.append(")");
		db.execSQL(outboundProduct.toString());
		
		
//		StringBuffer outboundProductCode = new StringBuffer("create table if not exists ");
//		outboundProductCode.append(tbl_outbound_product_code+" (id integer primary key autoincrement,");
//		outboundProductCode.append("pcid integer , ");
//		outboundProductCode.append("barcode varchar(80),") ;
//		outboundProductCode.append("code_type varchar(10),");
//		outboundProductCode.append("p_name varchar2(50)");
//		outboundProductCode.append(")");
//		db.execSQL(outboundProductCode.toString());
//		
//		StringBuffer transportOutboundProduct = new StringBuffer("create table if not exists ");
//		transportOutboundProduct.append("transport_outbound_product (to_id integer primary key ,");
//		transportOutboundProduct.append("sku integer ,"); 				//pc_id;
//		transportOutboundProduct.append("qty double,"); 				//数量
//		transportOutboundProduct.append("transport_id integer"); 		//transport_id
//		transportOutboundProduct.append(")");
//		db.execSQL(transportOutboundProduct.toString());
//		 
//		StringBuffer outboundTransportProduct = new StringBuffer("create table if not exists ");
//		outboundTransportProduct.append("outbound_transport_product(to_id integer primary key ,");
//		outboundTransportProduct.append("sku integer ,"); 				//pc_id;
//		outboundTransportProduct.append("qty double,"); 				//数量
//		outboundTransportProduct.append("transport_id integer,"); 		//transport_id
//		outboundTransportProduct.append("sn varchar(100),");			//sn
//		outboundTransportProduct.append("p_name varchar(50),");		//name
//		outboundTransportProduct.append("p_code varchar(50)");			//code
//		outboundTransportProduct.append(")");
//		db.execSQL(outboundTransportProduct.toString());
//		
//		StringBuffer outboundDifferentProduct = new StringBuffer("create table if not exists ");
//		outboundDifferentProduct.append("outbound_different_product(id integer primary key autoincrement,");
//		outboundDifferentProduct.append("pcid integer ,");
//		outboundDifferentProduct.append("machine varchar(20) ,");
//		outboundDifferentProduct.append("different_type varchar(20) ,");
//		outboundDifferentProduct.append("sn varchar(80) ,");
//		outboundDifferentProduct.append("transport_id integer ");
//		outboundDifferentProduct.append(")");
//		db.execSQL(outboundDifferentProduct.toString());
//		
//	
//		
//		
//		
//		StringBuffer tbl_outbound_scaninfo_Buffer = new StringBuffer("create table if not exists ");
//		tbl_outbound_scaninfo_Buffer.append(tbl_outbound_receive_info+"(id integer primary key autoincrement,"); 
//		tbl_outbound_scaninfo_Buffer.append("sn varchar2(50),") ;
//		tbl_outbound_scaninfo_Buffer.append("sku varchar2(30), ");			//为扫描的东西barCode
//		tbl_outbound_scaninfo_Buffer.append("quantity double,");			//serial_number
//		tbl_outbound_scaninfo_Buffer.append("createTime timestamp,");	
//		tbl_outbound_scaninfo_Buffer.append("palletNum varchar2(30),");
//		tbl_outbound_scaninfo_Buffer.append("transport_id  integer , ");
//		tbl_outbound_scaninfo_Buffer.append("isSubmit integer default 0, ");
//		tbl_outbound_scaninfo_Buffer.append("isSnRepeat integer default 0 , ");
//		tbl_outbound_scaninfo_Buffer.append("place varchar2(40) DEFAULT '', ");
//		tbl_outbound_scaninfo_Buffer.append("p_name DEFAULT '' ,");
//		tbl_outbound_scaninfo_Buffer.append("p_id  integer  default 0 ,");			//商品的ID
//		tbl_outbound_scaninfo_Buffer.append("barcode_type  varchar2(10)  default '' ");			//商品的ID
//
//		tbl_outbound_scaninfo_Buffer.append(")");
//		db.execSQL(tbl_outbound_scaninfo_Buffer.toString());
		
	 
		/* db.execSQL("create table if not exists outbound_receive_info(id integer primary key autoincrement,sn varchar2(30)," +
				"sku varchar2(30),quantity double," +
				"createTime timestamp,palletNum varchar2(30),transport_id  integer , isSubmit integer, isSnRepeat integer,place varchar2(40) DEFAULT '')"); */
//		
//	 
//		
//		StringBuffer outboundLpInfo = new StringBuffer("create table if not exists ");
//		outboundLpInfo.append("outbound_lp_info (id integer primary key autoincrement,");
//		outboundLpInfo.append("lp integer,");
//		outboundLpInfo.append("typeName varchar(80) , ");
//		outboundLpInfo.append("leftWeight  double, ");
//		outboundLpInfo.append("leftVolume  double, ");
//		outboundLpInfo.append("totalCount  double, ");
//		outboundLpInfo.append("isReceive  integer, ");
//		outboundLpInfo.append("lpName  varchar(20)");
//		outboundLpInfo.append(")");
//		db.execSQL(outboundLpInfo.toString());
//		
//		StringBuffer scanedAndSubmitProduct = new StringBuffer("create table if not exists ");
//		scanedAndSubmitProduct.append("outbound_scaned_submit_product(id integer primary key autoincrement,");
//		scanedAndSubmitProduct.append("pcid integer ,");
//		scanedAndSubmitProduct.append("machine varchar(20),");
//		scanedAndSubmitProduct.append("qty double,");
//		scanedAndSubmitProduct.append("sn varchar(80),");
//		scanedAndSubmitProduct.append("transport_id integer ");
//		scanedAndSubmitProduct.append(")");
//		db.execSQL(scanedAndSubmitProduct.toString());
//		
//		
//		// position 
//		StringBuffer positionTransport = new StringBuffer("create table if not exists ");
//		positionTransport.append(tbl_position_transport+" (id integer primary key autoincrement , ");
//		positionTransport.append("transport_id integer, ");
//		positionTransport.append("fromWarehouse varchar2(30), ");
//		positionTransport.append("toWarehouse varchar2(30), ");
//		positionTransport.append("area varchar2(50), ");
//		positionTransport.append("state varchar2(50),");
//		positionTransport.append("title_id integer,");
//		positionTransport.append("title_name varchar2(50)");
//		positionTransport.append(")");
//		db.execSQL(positionTransport.toString());
//		//scan_info
//		StringBuffer positionReceiveInfo = new StringBuffer("create table if not exists ");
//		positionReceiveInfo.append(tbl_position_scan_info+"(id integer primary key autoincrement,"); 
//		positionReceiveInfo.append("sn varchar2(50),") ;
//		positionReceiveInfo.append("sku varchar2(30), ");			//为扫描的东西
//		positionReceiveInfo.append("quantity double,");			//serial_number
//		positionReceiveInfo.append("createTime timestamp,");	
//		positionReceiveInfo.append("palletNum varchar2(30),");
//		positionReceiveInfo.append("transport_id  integer , ");
//		positionReceiveInfo.append("isSubmit integer default 0, ");
//		positionReceiveInfo.append("isSnRepeat integer default 0 , ");
//		positionReceiveInfo.append("place varchar2(40) DEFAULT '', ");
//		positionReceiveInfo.append("p_name DEFAULT '' ,");
//		positionReceiveInfo.append("p_id  integer  default 0 ,");			//商品的ID
//		positionReceiveInfo.append("barcode_type  varchar2(10)  default '', ");
//		positionReceiveInfo.append("pallet_id  integer ");	
//		positionReceiveInfo.append(")");
//	 
//		db.execSQL(positionReceiveInfo.toString());
//		
//		
//		StringBuffer inboundReceviedProduct = new StringBuffer("create table if not exists ");
//		inboundReceviedProduct.append(tbl_inbound_ware_house+"(to_id integer primary key ,");
//		inboundReceviedProduct.append("sku integer ,"); 				//pc_id;
//		inboundReceviedProduct.append("qty double,"); 				//数量
//		inboundReceviedProduct.append("transport_id integer ,"); 		//transport_id
//		inboundReceviedProduct.append("sn varchar(100) ,");			//sn
//		inboundReceviedProduct.append("p_name varchar(50) ,");		//name
//		inboundReceviedProduct.append("p_code varchar(50) ");			//code
//		 
//		inboundReceviedProduct.append(")");
//		db.execSQL(inboundReceviedProduct.toString());
//		
//		
//		StringBuffer positionProductCode = new StringBuffer("create table if not exists ");
//		positionProductCode.append(tbl_position_product_code +" (id integer primary key autoincrement,");
//		positionProductCode.append("pcid integer , ");
//		positionProductCode.append("barcode varchar(80),") ;
//		positionProductCode.append("code_type varchar(10),");
//		positionProductCode.append("p_name varchar(30)");
//		positionProductCode.append(")");
//		db.execSQL(positionProductCode.toString());
//		
		
	
		
		
		
		StringBuffer positionSerialProduct = new StringBuffer("create table if not exists ");
		positionSerialProduct.append("position_serial_product(id integer primary key autoincrement,"); 
		positionSerialProduct.append("product_serial_id integer,") ;
		positionSerialProduct.append("sku integer, ");			//pc_id
		positionSerialProduct.append("sn varchar(80),");			//serial_number
		positionSerialProduct.append("supplier_id integer,");	
		positionSerialProduct.append("in_time varchar(30),");
		positionSerialProduct.append("out_time varchar(30)");
		positionSerialProduct.append(")");
		db.execSQL(positionSerialProduct.toString());
	
//		StringBuffer positionProduct = new StringBuffer("create table if not exists ");
//		positionProduct.append(tbl_position_product+"(id integer primary key autoincrement,");
//		positionProduct.append("pid integer,");
//		positionProduct.append("name varchar(80),");
//		positionProduct.append("length double,");
//		positionProduct.append("width double,");
//		positionProduct.append("heigth double,");
//		positionProduct.append("weight double,");
//		positionProduct.append("sn_length integer");
//		positionProduct.append(")");
//		db.execSQL(positionProduct.toString());
//		
//		StringBuffer positionWareHouseProduct = new StringBuffer("create table if not exists ");
//		positionWareHouseProduct.append(tbl_position_wareHouse_product+" (to_id integer primary key ,");
//		positionWareHouseProduct.append("sku integer ,"); 				//pc_id;
//		positionWareHouseProduct.append("qty double,"); 				//数量
//		positionWareHouseProduct.append("transport_id integer,"); 		//transport_id
//		positionWareHouseProduct.append("sn varchar(100),");			//sn
//		positionWareHouseProduct.append("p_name varchar(50),");		//name
//		positionWareHouseProduct.append("p_code varchar(50),");			//code
//		positionWareHouseProduct.append("lotnumber varchar(50)");	
//		
//		 
//		positionWareHouseProduct.append(")");
//		db.execSQL(positionWareHouseProduct.toString());
//		//tbl_position_lp_info
//		StringBuffer positionLpInfo = new StringBuffer("create table if not exists ");
//		positionLpInfo.append(tbl_position_lp_info+"(id integer primary key autoincrement,");
//		positionLpInfo.append("lp integer,");
//		positionLpInfo.append("typeName varchar(80) , ");
//		positionLpInfo.append("isReceive  integer, ");
//		positionLpInfo.append("lpName  varchar(20) ,");
//		positionLpInfo.append("bill_id  integer ,");
//		positionLpInfo.append("container_type_id  integer ,");
//		
//		positionLpInfo.append("container_has_sn  integer ,");
//		positionLpInfo.append("is_full  integer ,");
//		positionLpInfo.append("container_type  varchar(10) ,");
//		positionLpInfo.append("sub_container_type  varchar(10) ,");
//		positionLpInfo.append("sub_container_type_id  integer,");
//		positionLpInfo.append("sub_count  integer ,");
//		positionLpInfo.append("container_pcid  integer ,");
//		positionLpInfo.append("container_pc_name  varchar(10) ,");
//		positionLpInfo.append("container_pc_count  integer , ");
//		positionLpInfo.append("parent_container_id  integer,");
//		positionLpInfo.append("location_id  integer default 0,");
//		 
//		positionLpInfo.append("location_name varchar(50), ");
//		positionLpInfo.append("title_id integer, ");
//		positionLpInfo.append("title varchar(50), ") ;
//		positionLpInfo.append("lotNumber varchar(20), ");
//		positionLpInfo.append("is_append_success integer default 0 ,");
//		positionLpInfo.append("is_server_copy integer default 0 ");
//		positionLpInfo.append(")");
//		db.execSQL(positionLpInfo.toString());
		
		StringBuffer positionedProduct = new StringBuffer("create table if not exists ");
		positionedProduct.append("positioned_product(id integer primary key autoincrement,");
		positionedProduct.append("pcid integer ,");
		positionedProduct.append("machine varchar(20),");
		positionedProduct.append("qty double,");
		positionedProduct.append("sn varchar(80),");
		positionedProduct.append("transport_id integer, ");
		positionedProduct.append("location varchar(80)");
		
		positionedProduct.append(")");
		db.execSQL(positionedProduct.toString());
		
		StringBuffer positionDifferentProduct = new StringBuffer("create table if not exists ");
		positionDifferentProduct.append("position_different_product(id integer primary key autoincrement,");
		positionDifferentProduct.append("pcid integer ,");
		positionDifferentProduct.append("machine varchar(20) ,");
		positionDifferentProduct.append("different_type varchar(20) ,");
		positionDifferentProduct.append("sn varchar(80) ,");
		positionDifferentProduct.append("transport_id integer ");
		positionDifferentProduct.append(")");
		db.execSQL(positionDifferentProduct.toString());
		
		
		StringBuffer upfile = new StringBuffer("create table if not exists ");
		upfile.append("upflie(id integer primary key autoincrement,");
		upfile.append("fileName varchar(50) ,");
		upfile.append("fileLocalPath varchar(250) ,");
		upfile.append("file_with_id varchar(20) ,");
		upfile.append("file_with_class varchar(10) ,");
		upfile.append("file_with_type varchar(10) ,");
		upfile.append("file_path varchar(30) ,");
		upfile.append("isSubmit integer default 0 , ");
		upfile.append("fileNameThumbnail varchar(50)  ") ;
	 
		upfile.append(")");
		db.execSQL(upfile.toString());
		
		//pick up
		StringBuffer pickup = new StringBuffer("create table if not exists ");
		pickup.append("pickup(id integer primary key autoincrement,");
		pickup.append("pickup_id integer ,");
		pickup.append("isOver integer  default 0 , ") ;
		pickup.append("type integer ,"); 		// 1 表示的是必须拣Sn .2 表示的是关心的数量
		pickup.append("doorName varchar(200) ,");
		pickup.append("locationName varchar(200)");
		pickup.append(")");
		db.execSQL(pickup.toString());
		
		//pickupLocation
		StringBuffer pickupLocation = new StringBuffer("create table if not exists ");
		pickupLocation.append(tbl_pickup_location + "(id integer primary key autoincrement,");
		pickupLocation.append("pickup_id integer, ");		//out
		pickupLocation.append("location varchar(100), ");	//位置
		pickupLocation.append("sort varchar(100) ,");	 	//排序
		pickupLocation.append("location_id integer ,") ;
		pickupLocation.append("totalQty double, ");
		pickupLocation.append("scanQty double, ");
		pickupLocation.append("islodeProduct integer default 0,");	//0:表示没有下载数据,1表示的是已经下载了上面的商品的数据.
		pickupLocation.append("location_type");	// 2.表示的是2D位置.3.表示的是3D位置
		pickupLocation.append(")");
		db.execSQL(pickupLocation.toString());
		
		StringBuffer pickupProductInfo = new StringBuffer("create table if not exists ");
		pickupProductInfo.append(tbl_pickup_product_info+"(id integer primary key autoincrement,");
		pickupProductInfo.append("pid integer,");
		pickupProductInfo.append("name varchar(80),");
		pickupProductInfo.append("length double,");
		pickupProductInfo.append("width double,");
		pickupProductInfo.append("heigth double,");
		pickupProductInfo.append("weight double,");
		pickupProductInfo.append("sn_length integer");
		pickupProductInfo.append(")");
		db.execSQL(pickupProductInfo.toString());
		
		StringBuffer pickupProductCode = new StringBuffer("create table if not exists ");
		pickupProductCode.append(tbl_pickup_product_code+"(id integer primary key autoincrement,");
		pickupProductCode.append("pcid integer , ");
		pickupProductCode.append("barcode varchar(80),") ;
		pickupProductCode.append("code_type varchar(10),");
		pickupProductCode.append("p_name varchar(10)");
		pickupProductCode.append(")");
		db.execSQL(pickupProductCode.toString());
		
		StringBuffer pickupSerialProduct = new StringBuffer("create table if not exists ");
		pickupSerialProduct.append(tbl_pickup_serial_product+"(id integer primary key autoincrement,");
		pickupSerialProduct.append("pcid integer ,");
		pickupSerialProduct.append("sn varchar(100) ");
		pickupSerialProduct.append(")");
		db.execSQL(pickupSerialProduct.toString());
		
		StringBuffer pickupLocationProduct = new StringBuffer("create table if not exists ");
		pickupLocationProduct.append(tbl_pickup_location_product+"(id integer primary key autoincrement,");
		pickupLocationProduct.append("pcid integer ,");
		pickupLocationProduct.append("qty double , ");
		pickupLocationProduct.append("location varchar2(40), ");
		pickupLocationProduct.append("location_id integer, ");
		pickupLocationProduct.append("pickup_id integer, ");
		pickupLocationProduct.append("slcId  integer ,");
		pickupLocationProduct.append("sn  varchar(50) ,");
		
		pickupLocationProduct.append("from_container_type integer ,  ") ;
		pickupLocationProduct.append("from_container_type_id integer ,  ") ;
		pickupLocationProduct.append("from_con_id integer ,  ") ;
		
		pickupLocationProduct.append("pick_container_Type integer ,  ") ;
		pickupLocationProduct.append("pick_container_Type_id integer ,  ") ;
		pickupLocationProduct.append("pick_con_id integer , ") ;
		pickupLocationProduct.append("pick_container_qty integer ,  ") ;
		
		pickupLocationProduct.append("from_location_id  varchar(50) ,");	//位置缺货的时候来从其他位置来的location_id
		pickupLocationProduct.append("from_submit_qty  double default 0");	//位置缺货的时候 提交成功的数量,在提交成功的时候要更新这个数量
		pickupLocationProduct.append(")");
		db.execSQL(pickupLocationProduct.toString());
		
		//pickup_scan_info
		StringBuffer pickupScanInfo = new StringBuffer("create table if not exists ");
		pickupScanInfo.append(tbl_pickup_scan_info+"(id integer primary key autoincrement,");
		pickupScanInfo.append("sn varchar2(80) , ");
		pickupScanInfo.append("sku integer , ");	//当成pcid 处理
		pickupScanInfo.append("qty double,") ;
		pickupScanInfo.append("pickup_id integer , ");
		pickupScanInfo.append("location_id integer ,") ;
		pickupScanInfo.append("isSubmit integer default 0,") ; //0 表示没有。1表示已经提交了
		pickupScanInfo.append("from_location_id integer,") ; 	//表示从其他位置差异过来的
		pickupScanInfo.append("pname varchar2(80), ");
		pickupScanInfo.append("barCode varchar2(40) ,");
		pickupScanInfo.append("barCodeType varchar2(10), ");
		
		pickupScanInfo.append("from_container_id integer ,");			//从from_container_id
		pickupScanInfo.append("from_container_type integer, ");			//from_container_id 的类型
		pickupScanInfo.append("from_container_type_id integer, ");			//from_container_type_id 的类型
		
		pickupScanInfo.append("pickup_container_id integer ,");			//pikcup container_id ;(已经捡到的)
		pickupScanInfo.append("pickup_container_type integer, ");		//pickup container_type ;(已经捡到的类型1,2,3,4BLP,CLP,ILP,TLP)
		pickupScanInfo.append("pickup_container_type_id integer ");		//pickup container_type_id ;(已经捡到的 container_type_id)
		
		
		pickupScanInfo.append(")");
		db.execSQL(pickupScanInfo.toString());
		
		//pickup_container_info
		StringBuffer pickUpContainer = new StringBuffer("create table if not exists ");
		pickUpContainer.append(tbl_pickup_container_info + "(id integer primary key autoincrement,");
		pickUpContainer.append("location_id integer , ");
		pickUpContainer.append("pickup_id varchar2(80), ");					 
 
		pickUpContainer.append("container_id varchar2(80) , ");				//pickup_container
		pickUpContainer.append("container_type integer , ");				
		pickUpContainer.append("container_type_id integer , ");
		pickUpContainer.append("container varchar2(80), ");					//container 编码
		
		
		pickUpContainer.append("from_container_id varchar2(80) , ");		//parent_container_id
		pickUpContainer.append("from_container_type integer , ");			//parent_container_type
		pickUpContainer.append("from_container_type_id integer  ");		//parent_container_type_id	
		
		pickUpContainer.append(")");
		
		db.execSQL(pickUpContainer.toString()); 
		
//		//inventory
//		StringBuffer inventoryArea = new StringBuffer("create table if not exists ");
//		inventoryArea.append(tbl_inventory_area +"(id integer primary key autoincrement,");
//		inventoryArea.append("area_name varchar2(80) , ");
//		inventoryArea.append("area_psid integer , ");
//		inventoryArea.append("area_id integer, ");
//		inventoryArea.append("type integer, ");
//		inventoryArea.append("state integer ");
//		inventoryArea.append(")");
//		
//		
//		db.execSQL(inventoryArea.toString());
//		
//		//inventoryAreaProductInfo
//		StringBuffer inventoryAreaProductInfo = new StringBuffer("create table if not exists ");
//		inventoryAreaProductInfo.append(tbl_inventory_product_info + "(id integer primary key autoincrement,");
//		inventoryAreaProductInfo.append("pid integer,");
//		inventoryAreaProductInfo.append("name varchar(80),");
//		inventoryAreaProductInfo.append("length double,");
//		inventoryAreaProductInfo.append("width double,");
//		inventoryAreaProductInfo.append("heigth double,");
//		inventoryAreaProductInfo.append("sn_length int,");
//
//		inventoryAreaProductInfo.append("weight double");
//		inventoryAreaProductInfo.append(")");
//		db.execSQL(inventoryAreaProductInfo.toString());
//		
//		//inventoryAreaProductCode
//		StringBuffer inventoryAreaProductCode = new StringBuffer("create table if not exists ");
//		inventoryAreaProductCode.append(tbl_inventory_product_code+" (id integer primary key autoincrement,");
//		inventoryAreaProductCode.append("pcid integer , ");
//		inventoryAreaProductCode.append("barcode varchar(80),") ;
//		inventoryAreaProductCode.append("code_type varchar(10),");
//		inventoryAreaProductCode.append("p_name varchar(10)");
//		inventoryAreaProductCode.append(")");
//		db.execSQL(inventoryAreaProductCode.toString());
//		
//		//inventoryLocation
//		 
//		StringBuffer inventoryLocation = new StringBuffer("create table if not exists ");
//		inventoryLocation.append(tbl_inventory_location + "(id integer primary key autoincrement,");
//		inventoryLocation.append("area_id integer, ");		//out
//		inventoryLocation.append("location varchar(100), ");	//位置
//		inventoryLocation.append("sort varchar(100) ,");	 	//排序
//		inventoryLocation.append("location_id integer ,") ;
//  		inventoryLocation.append("isSubmit integer default 0 , ") ;  //0表示没有提交 。1表示已经提交过了。
//		inventoryLocation.append("isNoProduct integer default 0") ;  //1.表示没有商品. 2.表示有商品(通过扫描的结果来看)
//
// 		inventoryLocation.append(")");
//		db.execSQL(inventoryLocation.toString());
//		
//		
//		//inventoryLocationProduct 区域位置上面的商品
//		StringBuffer inventoryLocationProduct = new StringBuffer("create table if not exists ");
//		inventoryLocationProduct.append(tbl_inventory_location_product + "(id integer primary key autoincrement,");
//		inventoryLocationProduct.append("pcid integer ,");
//		inventoryLocationProduct.append("qty double , ");
//		inventoryLocationProduct.append("location varchar2(40), ");
//		inventoryLocationProduct.append("location_id integer, ");
//		inventoryLocationProduct.append("area_id integer, ");
//		inventoryLocationProduct.append("slcId  integer ,");
//		inventoryLocationProduct.append("sn  varchar(50) ");
// 
//		inventoryLocationProduct.append(")");
//		db.execSQL(inventoryLocationProduct.toString());
//		
//		
//		StringBuffer inventorySerialProduct = new StringBuffer("create table if not exists  ") ;
//		inventorySerialProduct.append("inventory_serial_product(id integer primary key autoincrement, ");
//		inventorySerialProduct.append("sn varchar2(80) , ");
//		inventorySerialProduct.append("pcid integer ");
//		inventorySerialProduct.append(")");
//		db.execSQL(inventorySerialProduct.toString());
//		
//		
//		StringBuffer inventoryScanInfo = new StringBuffer("create table if not exists ");
//		inventoryScanInfo.append(tbl_inventory_scan_info+"(id integer primary key autoincrement,");
//		inventoryScanInfo.append("sn varchar2(80) , ");
//		inventoryScanInfo.append("pcid integer , ");		//当成pcid 处理 
//		inventoryScanInfo.append("qty double,") ;
//		inventoryScanInfo.append("area_id integer , ");
//		inventoryScanInfo.append("location_id integer ,") ;
//		inventoryScanInfo.append("isSubmit integer default 0 , ") ; //0 表示没有。1表示已经提交了
//		inventoryScanInfo.append("p_name DEFAULT '' ,");
// 		inventoryScanInfo.append("barCode  varchar2(50)  default '' ,");			 
// 		inventoryScanInfo.append("barCodeType  varchar2(50)  default '', ");	
// 		inventoryScanInfo.append("lp  integer  , ");	
// 		inventoryScanInfo.append("lpName  varchar2(50)  default '', ");	
// 		inventoryScanInfo.append("parentLp  integer   ");		//父级容器的Container
//		inventoryScanInfo.append(")");
//		db.execSQL(inventoryScanInfo.toString());
//		
//		
//		StringBuffer inventoryDifference = new StringBuffer("create table if not exists ");
//		inventoryDifference.append("inventory_difference(id integer primary key autoincrement,");
//		inventoryDifference.append("location_id  integer , ");
//		inventoryDifference.append("location varchar2(80) , ");
//		inventoryDifference.append("pcid integer,") ;
//		inventoryDifference.append("pname varchar2(100) , ");
//		inventoryDifference.append("pqty double ,") ;
//		inventoryDifference.append("tqty double ,") ;
//		inventoryDifference.append("area_id integer ") ;
//
//		inventoryDifference.append(")");
//		db.execSQL(inventoryDifference.toString());
//		
//		//接地容器
//		StringBuffer inventoryProductStoreContainer =new StringBuffer("create table if not exists ");
//		inventoryProductStoreContainer.append(tbl_inventory_product_store_container+"(id integer primary key autoincrement,");
//		inventoryProductStoreContainer.append("pcid  integer , ");
//		inventoryProductStoreContainer.append("qty  double , ");
//		inventoryProductStoreContainer.append("container_id integer,") ;
//		inventoryProductStoreContainer.append("container_type integer, ");
//		inventoryProductStoreContainer.append("container_type_id integer ,") ;
//		inventoryProductStoreContainer.append("title integer ,") ;
//		inventoryProductStoreContainer.append("slcid integer, ") ;
//		inventoryProductStoreContainer.append("title_name varchar2(100), ") ;
//		inventoryProductStoreContainer.append("is_in_received  integer default 0 , ") ;
//
//		inventoryProductStoreContainer.append("location varchar2(100), ") ;
//		inventoryProductStoreContainer.append("area_id integer ") ;
//		inventoryProductStoreContainer.append(")");
//		db.execSQL(inventoryProductStoreContainer.toString());
// 
//		StringBuffer inventoryLpInfo = new StringBuffer("create table if not exists ");
//		inventoryLpInfo.append(tbl_inventory_lp_info+"(id integer primary key autoincrement,");
//		inventoryLpInfo.append("lp integer,");
//		inventoryLpInfo.append("typeName varchar(80) , ");
//		inventoryLpInfo.append("isReceive  integer, ");
//		inventoryLpInfo.append("lpName  varchar(20) ,");
//		inventoryLpInfo.append("bill_id  integer ,");
//		inventoryLpInfo.append("container_type_id  integer ,");
//		
//		inventoryLpInfo.append("container_has_sn  integer ,");
//		inventoryLpInfo.append("is_full  integer ,");
//		inventoryLpInfo.append("container_type  varchar(10) ,");
//		inventoryLpInfo.append("sub_container_type  varchar(10) ,");
//		inventoryLpInfo.append("sub_container_type_id  integer,");
//		inventoryLpInfo.append("sub_count  integer ,");
//		inventoryLpInfo.append("container_pcid  integer ,");
//		inventoryLpInfo.append("container_pc_name  varchar(10) ,");
//		inventoryLpInfo.append("container_pc_count  integer , ");
//		inventoryLpInfo.append("parent_container_id  integer,");
//		inventoryLpInfo.append("location_id  integer default 0,");
//		 
//		inventoryLpInfo.append("location_name varchar(50), ");
//		inventoryLpInfo.append("title_id integer, ");
//		inventoryLpInfo.append("title varchar(50), ") ;
//		inventoryLpInfo.append("lotNumber varchar(20), ");
//		inventoryLpInfo.append("is_append_success integer default 0 ,");
//		inventoryLpInfo.append("is_server_copy integer default 0 ");
//
//		inventoryLpInfo.append(")");
//		db.execSQL(inventoryLpInfo.toString());
//		
//		StringBuffer inventoryTitle = new StringBuffer("create table if not exists ");
//		inventoryTitle.append(tbl_config_title+"(id integer primary key autoincrement,");
//		inventoryTitle.append("title_id integer,");
//		inventoryTitle.append("title varchar(50)");
//		inventoryTitle.append(")");
//		db.execSQL(inventoryTitle.toString());
//
//		/**
//		 * delivery
//		 */
//		//users
//		StringBuilder user = new StringBuilder();
//		user.append("create table if not exists ")
//		.append(TAB_ADMIN_USERS)
//		.append(" (`adid` int(10) NOT NULL DEFAULT '0',")
//		.append(" `account` varchar(45) DEFAULT NULL,")
//		.append(" `llock` tinyint(3)  DEFAULT '0',")
//		.append(" `adgid` int(10)  DEFAULT NULL,")
//		.append(" `employe_name` varchar(45) DEFAULT NULL,")
//		.append(" `ps_id` int(10)  DEFAULT '0',")
//		.append(" `email` varchar(200) DEFAULT NULL,")
//		.append(" `skype` varchar(200) DEFAULT NULL,")
//		.append(" `msn` varchar(200) DEFAULT NULL,")
//		.append(" `AreaId` int(13) DEFAULT NULL,")
//		.append(" `mobilePhone` varchar(200) DEFAULT NULL,")
//		.append(" `proQQ` varchar(200) DEFAULT NULL,")
//		.append(" `proJsId` tinyint(4) DEFAULT NULL,")
//		.append(" `file_path` varchar(100) DEFAULT NULL,")
//		.append(" `adgid_name` varchar(100) DEFAULT NULL)");
//		db.execSQL(user.toString());
//
//		//流程
//		StringBuilder produres = new StringBuilder();
//		produres.append("create table if not exists ")
//		.append(TAB_PRODURES)
//		.append(" (`produres_id` int(10) NOT NULL,")
//		.append(" `process_key` int(10) DEFAULT NULL,")
//		.append(" `process_coloum` varchar(30)  DEFAULT NULL,")
//		.append(" `process_coloum_page` varchar(30)  DEFAULT NULL,")
//		.append(" `process_name` varchar(100)  DEFAULT NULL,")
//		.append(" `process_note_is_display` tinyint(3) DEFAULT NULL,")
//		.append(" `process_note` varchar(200)  DEFAULT NULL,")
//		.append(" `process_person_is_display` tinyint(3) DEFAULT NULL,")
//		.append(" `process_person` varchar(20)  DEFAULT NULL,")
//		.append(" `activity_is_need_display` tinyint(3) DEFAULT NULL,")
//		.append(" `associate_order_type` tinyint(4) DEFAULT NULL,")
//		.append(" `process_start_time_is_create` tinyint(3) DEFAULT NULL,")
//		.append(" `process_period` int(10) DEFAULT NULL,")
//		.append(" `activity_default_selected` tinyint(3) DEFAULT NULL)");
//		db.execSQL(produres.toString());
//
//		//流程阶段
//		StringBuilder produreDetails = new StringBuilder();
//		produreDetails.append("create table if not exists ")
//		.append(TAB_PRODURES_DETAILS)
//		.append(" (`id` int(10) NOT NULL,")
//		.append(" `produres_id` int(10) DEFAULT NULL,")
//		.append(" `activity_name` varchar(20)  DEFAULT NULL,")
//		.append(" `activity_val` tinyint(3) DEFAULT NULL,")
//		.append(" `val_status` tinyint(3) DEFAULT NULL)");
//		db.execSQL(produreDetails.toString());
//		
//		//流程通知
//		StringBuilder produreNotices = new StringBuilder();
//		produreNotices.append("create table if not exists ")
//		.append(TAB_PRODURES_NOTICE)
//		.append(" (`id` int(10) NOT NULL,")
//		.append(" `produres_id` int(10) DEFAULT NULL,")
//		.append(" `notice_is_display` tinyint(2) DEFAULT NULL,")
//		.append(" `notice_coloum_name` varchar(20)  DEFAULT NULL,")
//		.append(" `notice_name` varchar(20)  DEFAULT NULL,")
//		.append(" `notice_default_selected` int(10) DEFAULT NULL,")
//		.append(" `notice_type` tinyint(3) DEFAULT NULL)");
//		db.execSQL(produreNotices.toString());
//		
//			//创建仓库信息表
//		db.execSQL("create table if not exists "
//						+ TAB_PRODUCT_STORAGE_CATALOG
//						+ "(id integer ,title varchar2(100),"
//						+ "address varchar2(1000),send_zip_code varchar2(30),contact varchar2(45),phone varchar2(30),native int,deliver_address varchar2(1000),"
//						+ "deliver_zip_code varchar2(30),pro_id int,city varchar2(50),freight_coefficient tinyint,send_nation int,send_pro_id int,send_pro_input varchar2(150),send_city varchar2(50),"
//						+ "send_house_number varchar2(100),send_street varchar2(100),send_contact varchar2(50),send_phone varchar2(50),deliver_nation int,deliver_pro_id int,deliver_pro_input varchar2(150),"
//						+ "deliver_city varchar2(50),deliver_house_number varchar2(100),deliver_street varchar2(100),deliver_contact varchar2(50),deliver_phone varchar2(50),"
//						+ "storage_type tinyint)");
//
//		//创建国家表
//		db.execSQL("create table if not exists "
//						+ TAB_COUNTRY_CODE
//						+ "(ccid integer,c_country varchar2(100),c_code varchar2(100),fusion_id varchar2(20),map_flash varchar2(100))");
//
//		//创建省份表
//		db.execSQL("create table if not exists "
//						+ TAB_COUNTRY_PROVINCE
//						+ "(pro_id integer,pro_name varchar2(300),nation_id integer,p_code varchar2(100),fusion_id varchar2(20))");
//
//		//创建运输资源表
//		StringBuffer transportResource = new StringBuffer(
//				"create table if not exists ");
//		transportResource.append(TAB_TRANSPORT_RESOURCE);
//		transportResource.append("( fr_id integer,fr_company varchar2(100),fr_way int,");
//		transportResource.append("fr_undertake_company varchar2(100),fr_from_country int,fr_to_country int,");
//		transportResource.append("fr_from_port varchar2(100),fr_to_port varchar2(100),fr_contact varchar2(50),");
//		transportResource.append("fr_contact_tel varchar2(50),fr_delivery_address varchar2(100),");
//		transportResource.append("fr_remark varchar2(200))");
//		db.execSQL(transportResource.toString());
//
//		//创建运费项目表
//		StringBuffer freightItem = new StringBuffer("create table if not exists ");
//		freightItem.append(TAB_FREIGHT_ITEM);
//		freightItem.append(" (fc_id integer,fr_id integer,fc_project_name varchar2(100),");
//		freightItem.append("fc_way varchar2(20),fc_unit varchar(20),fc_unit_price double,");
//		freightItem.append("freight_index int)");
//		db.execSQL(freightItem.toString());
//
//		// ����ת�˵�
//		StringBuffer transport_temp = new StringBuffer();
//		transport_temp.append("create table if not exists ");
//		transport_temp.append(TAB_TRANSPORT);
//		transport_temp.append(" (`transport_id_temp` int(10) NOT NULL,");
//		transport_temp.append(" `from_ps_type` int(3) DEFAULT NULL ,");
//		transport_temp.append(" `send_psid` int(10) DEFAULT NULL ,");
//		transport_temp.append(" `send_ccid` int(10) DEFAULT NULL ,");
//		transport_temp.append(" `send_pro_id` int(10) DEFAULT NULL ,");
//		transport_temp.append("  `sendStorageName` varchar(60) DEFAULT NULL ,");
//		transport_temp.append(" `send_nation_name` varchar(60) DEFAULT NULL,");
//		transport_temp.append(" `send_pro_name` varchar(60) DEFAULT NULL,");
//		transport_temp.append("  `send_city` varchar(50) DEFAULT NULL ,");
//		transport_temp.append(" `send_house_number` varchar(300) DEFAULT NULL,");
//		transport_temp.append(" `send_street` varchar(300) DEFAULT NULL,");
//		transport_temp.append(" `send_zip_code` varchar(20) DEFAULT NULL ,");
//		transport_temp.append(" `send_name` varchar(50) DEFAULT NULL ,");
//		transport_temp.append("  `send_linkman_phone` varchar(50) DEFAULT NULL ,");
//		transport_temp.append(" `address_state_send` varchar(30) DEFAULT NULL ,");
//		transport_temp.append(" `target_ps_type` int(3) DEFAULT NULL ,");
//		transport_temp.append(" `receive_psid` int(10) DEFAULT NULL ,");
//		transport_temp.append(" `deliver_ccid` int(10) DEFAULT NULL ,");
//		transport_temp.append(" `deliver_pro_id` int(10) DEFAULT NULL ,");
//		transport_temp.append("  `receiveStroageName` varchar(60) DEFAULT NULL ,");
//		transport_temp.append(" `receive_nation_name` varchar(60) DEFAULT NULL,");
//		transport_temp.append(" `receive_pro_name` varchar(60) DEFAULT NULL,");
//		transport_temp.append(" `deliver_city` varchar(50) DEFAULT NULL ,");
//		transport_temp.append(" `deliver_house_number` varchar(300) DEFAULT NULL,");
//		transport_temp.append(" `deliver_street` varchar(300) DEFAULT NULL,");
//		transport_temp.append(" `deliver_zip_code` varchar(20) DEFAULT NULL ,");
//		transport_temp.append(" `transport_linkman` varchar(100) DEFAULT NULL ,");
//		transport_temp.append(" `transport_linkman_phone` varchar(100) DEFAULT NULL ,");
//		transport_temp.append(" `address_state_deliver` varchar(255) DEFAULT NULL ,");
//		transport_temp.append(" `remark` varchar(500) DEFAULT NULL ,");
//		transport_temp.append(" `title_id` int(10) DEFAULT 0,");
//		
//		//运输资源
//		transport_temp.append(" `fr_id` int(11) DEFAULT NULL,");
//		transport_temp.append(" `transport_waybill_number` varchar(100) DEFAULT NULL ,");
//		transport_temp.append(" `transport_waybill_name` varchar(100) DEFAULT NULL ,");
//		transport_temp.append(" `transportby` tinyint(4) DEFAULT NULL ,");
//		transport_temp.append(" `carriers` varchar(200) DEFAULT NULL ,");
//		transport_temp.append(" `transport_send_place` varchar(200) DEFAULT NULL ,");
//		transport_temp.append(" `transport_receive_place` varchar(200) DEFAULT NULL ,");
//		
//		transport_temp.append(" `transport_status` tinyint(4) DEFAULT '1' ,");
//		transport_temp.append(" `transport_receive_date` varchar(30))");
////		transport.append(" INSERT INTO \"transport_temp\" (\"transport_id_temp\") VALUES (1)");
//		db.execSQL(transport_temp.toString());
//
//		
//		//config ��
//		StringBuffer delivery_config = new  StringBuffer("create table if not exists ");
//		delivery_config.append(TBL_DELIVERY_CONFIG+" ( id integer primary key autoincrement , ") ; 
//		delivery_config.append("columnName varchar(30) , ") ;  
//		delivery_config.append("columnValue varchar(100)  ") ; 
//		delivery_config.append(")");
//		db.execSQL(delivery_config.toString());
//		db.execSQL("insert into "+TBL_DELIVERY_CONFIG+"('columnName','columnValue') values('delivery_transport_id','0')") ;	//表示inbound当前处理的tansport_id,主要是用来做清除数据用的
//		db.execSQL("insert into "+TBL_DELIVERY_CONFIG+"('columnName','columnValue') values('delivery_scan_view','1')") ; 	//delivery scan view 1表示的是 详细的视图. 2:表示是group by的视图.
//		db.execSQL("insert into "+TBL_DELIVERY_CONFIG+"('columnName','columnValue') values('delivery_sku_view','2')") ; 	//delivery sku view 1表示的是 详细的视图. 2:表示是group by的视图.
//		db.execSQL("insert into "+TBL_DELIVERY_CONFIG+"('columnName','columnValue') values('delivery_compare_view','2')") ; 	//delivery compare view 1表示的是 详细的视图. 2:表示是group by的视图.	
//		db.execSQL("insert into "+TBL_DELIVERY_CONFIG+"('columnName','columnValue') values('delivery_isReceiveing','0')") ; 	// .isReceiveing表示的是当前是否有新的收货。有那么在compare 界面就要提交数据	
//		
//		//delivery_scan_product
//		db.execSQL("create table if not exists "+TBL_DELIVERY_SCAN_PRODUCT+"(id integer primary key autoincrement,sn varchar2(30)," +
//				"pc_id integer, p_code varchar2(120), p_name varchar2(120), p_code_type varchar2(10) ,quantity double, isPalletSubmit integer" +
//				"createTime timestamp,palletNum varchar2(30),transport_id  integer , isSubmit integer, isSnRepeat integer,place varchar2(40) DEFAULT '')");
//		
//		//delivery_serial_product
//		StringBuffer deliverySerialProduct = new StringBuffer("create table if not exists ");
//		deliverySerialProduct.append("delivery_serial_product(id integer primary key autoincrement,"); 
//		deliverySerialProduct.append("product_serial_id integer,") ;
//		deliverySerialProduct.append("sku integer, ");			//pc_id
//		deliverySerialProduct.append("sn varchar(80),");			//serial_number
//		deliverySerialProduct.append("supplier_id integer,");	
//		deliverySerialProduct.append("in_time varchar(30),");
//		deliverySerialProduct.append("out_time varchar(30)");
//		deliverySerialProduct.append(")");
//		db.execSQL(deliverySerialProduct.toString());
//		 
//		
//	 
//		
//	 
//		
//		//delivery_outbound_receive_info
//		db.execSQL("create table if not exists "+TBL_DELIVERY_OUTBOUND_RECEIVE_INFO+"(id integer primary key autoincrement,sn varchar2(30)," +
//				"sku varchar2(30),pallet_id integer,pc_id integer,p_code_type varchar2(10), p_code varchar2(120), p_name varchar2(120),quantity double,isPalletSubmit integer," +
//				"createTime timestamp,palletNum varchar2(30),transport_id  integer , isSubmit integer, isSnRepeat integer,place varchar2(40) DEFAULT '')");
//		
// 
//		
//		 
//		
//	 
//		
//		//outbound_transport_product
//		StringBuffer deliveryOutboundTransportProduct1 = new StringBuffer("create table if not exists ");
//		deliveryOutboundTransportProduct1.append(TBL_DELIVERY_OUTBOUND_TRANSPORT_PRODUCT+"(to_id integer primary key ,");
//		deliveryOutboundTransportProduct1.append("sku integer ,"); 				//pc_id;
//		deliveryOutboundTransportProduct1.append("qty double,"); 				//����
//		deliveryOutboundTransportProduct1.append("transport_id integer,"); 		//transport_id
//		deliveryOutboundTransportProduct1.append("sn varchar(100),");			//sn
//		deliveryOutboundTransportProduct1.append("p_name varchar(50),");		//name
//		deliveryOutboundTransportProduct1.append("p_code varchar(50),");			//code
//		deliveryOutboundTransportProduct1.append("pc_id integer");	
//		deliveryOutboundTransportProduct1.append(")");
//		db.execSQL(deliveryOutboundTransportProduct1.toString());
//		
//		
//		//transport_outbound_product
//		StringBuffer deliveryTransportOutboundProduct1 = new StringBuffer("create table if not exists ");
//		deliveryTransportOutboundProduct1.append(TBL_DELIVERY_TRANSPORT_OUTBOUND_PRODUCT+" (to_id integer primary key ,");
//		deliveryTransportOutboundProduct1.append("sku integer ,"); 				//pc_id;
//		deliveryTransportOutboundProduct1.append("qty double,"); 				//����
//		deliveryTransportOutboundProduct1.append("transport_id integer"); 		//transport_id
//		deliveryTransportOutboundProduct1.append(")");
//		db.execSQL(deliveryTransportOutboundProduct1.toString());
//		
//		//storage_product
//		StringBuilder storageProduct = new StringBuilder();
//		storageProduct.append("create table if not exists ");
//		storageProduct.append(TBL_STORAGE_PRODUCT);
//		storageProduct.append("(id integer primary key autoincrement,");
////		storageProduct.append("storage_id INTEGER,");
//		storageProduct.append("pid INTEGER,");
//		storageProduct.append("name  varchar(80),");
//		storageProduct.append("length double,");
//		storageProduct.append("width double,");
//		storageProduct.append("heigth double,");
//		storageProduct.append("weight double)");
//		db.execSQL(storageProduct.toString());
//		
//		//storage_product_code
//		StringBuilder storageProductCode = new StringBuilder();
//		storageProductCode.append("create table if not exists ");
//		storageProductCode.append(TBL_STORAGE_PRODUCT_CODE);
//		storageProductCode.append("(id integer primary key autoincrement,");
//		storageProductCode.append(" pcid INTEGER,");
//		storageProductCode.append(" barcode varchar,");
//		storageProductCode.append("code_type varchar ,");
//		storageProductCode.append("pc_name varchar2(80))");
//		db.execSQL(storageProductCode.toString());
//		
//	 
//		
//		
//	    //basedata_product
//		StringBuffer baseDataProduct = new StringBuffer();
//		baseDataProduct.append("create table if not exists ");
//		baseDataProduct.append(TBL_BASEDATA_PRODUCT);
//		baseDataProduct.append("(id integer primary key autoincrement,");
// 		baseDataProduct.append("pid INTEGER,");
//		baseDataProduct.append("name  varchar(80),");
//		baseDataProduct.append("length double,");
//		baseDataProduct.append("width double,");
//		baseDataProduct.append("heigth double,");
//		baseDataProduct.append("weight double,");
//		baseDataProduct.append("unitPrice double,");
//		baseDataProduct.append("unitName varchar(80),");
//		baseDataProduct.append("sn_length  INTEGER,");
//		
//		
//		baseDataProduct.append("thumbnail varchar(100),");
//		
//		baseDataProduct.append("unionFlag INTEGER,");
//		baseDataProduct.append("catalogId INTEGER,");
//		baseDataProduct.append("pcode varchar(100),");
//		baseDataProduct.append("volume double,");
//		baseDataProduct.append("catalogTitle varchar(150),");
//		baseDataProduct.append("isLocal integer default 0,") ; //是否是本地占存的没有提交的
//		baseDataProduct.append("isSubmit integer default 0") ;	//本地是否提交了0
//		
//		baseDataProduct.append(")");
//		db.execSQL(baseDataProduct.toString());
//		
//		//暂存要提交的流程信息
//		StringBuffer deliveryProdureSubmit = new StringBuffer();
//		deliveryProdureSubmit.append("create table if not exists ");
//		deliveryProdureSubmit.append(TBL_DELIVERY_PRODURE_SUBMIT);
//		deliveryProdureSubmit.append("(produre_id integer,");
//		deliveryProdureSubmit.append("activity_val integer,");
//		deliveryProdureSubmit.append("adminUserIds varchar2(100),");
//		deliveryProdureSubmit.append("isMail integer,");
//		deliveryProdureSubmit.append("isMessage integer,");
//		deliveryProdureSubmit.append("isPage integer,");
//		deliveryProdureSubmit.append("produre_name varchar2(60),");
//		deliveryProdureSubmit.append("adminUserNames varchar2(200)");
//		deliveryProdureSubmit.append(")");
//		db.execSQL(deliveryProdureSubmit.toString());
//		
//		
//		//暂存要提交的运费项目信息
//		StringBuffer deliveryFreightSubmit = new StringBuffer();
//		deliveryFreightSubmit.append("create table if not exists ");
//		deliveryFreightSubmit.append(TBL_DELIVERY_FREIGHT_SUBMIT);
//		deliveryFreightSubmit.append("(fc_id integer,");
//		deliveryFreightSubmit.append("fc_project_name varchar2(100),");
//		deliveryFreightSubmit.append("fc_way varchar2(100),");
//		deliveryFreightSubmit.append("fc_unit varchar2(100),");
//		deliveryFreightSubmit.append("fc_unit_price varchar2(100),");
//		deliveryFreightSubmit.append("currency varchar2(100),");
//		deliveryFreightSubmit.append("exchange_rate varchar2(100),");
//		deliveryFreightSubmit.append("count varchar2(100)");
//		deliveryFreightSubmit.append(")");
//		db.execSQL(deliveryFreightSubmit.toString());
//		StringBuffer transportCheckIn = new StringBuffer("create table if not exists ");
//		transportCheckIn.append(TBL_DELIVERY_OUTBOUND_TRANSPORT+" (id integer primary key autoincrement , ");
//		transportCheckIn.append("transport_id integer, ");
//		transportCheckIn.append("fromWarehouse varchar2(30), ");
//		transportCheckIn.append("toWarehouse varchar2(30), ");
//		transportCheckIn.append("title_id integer, ");
//		transportCheckIn.append("title_name varchar2(30) ");
//		transportCheckIn.append(")");
//		db.execSQL(transportCheckIn.toString());
//		StringBuffer product_code_temp = new StringBuffer();
//		product_code_temp.append("create table if not exists ")
//		.append(TBL_PRODUCT_CODE_TEMP)
//		.append("(id integer primary key autoincrement , pc_id integer, pc_code varchar2(50), pc_code_type integer)");
//		db.execSQL(product_code_temp.toString());
//		StringBuffer productCodeSearchSame = new StringBuffer();
//		productCodeSearchSame.append("create table if not exists ")
//		.append(TBL_DELIVERY_PRODUCT_CODE_SEARCH_SAME)
//		.append("(id integer primary key autoincrement,")
//		.append(" pcid INTEGER,")
//		.append(" barcode varchar2(80),")
//		.append("code_type varchar ,")
//		.append("pc_name varchar2(80))");
//		;
//		db.execSQL(productCodeSearchSame.toString());
//		
//		StringBuffer title = new StringBuffer();
//		title.append("create table if not exists ").append(TBL_TITLE);
//		title.append("(id integer primary key autoincrement, title_id integer, title_name varchar2(100),");
//		title.append("title_admin_sort integer, title_admin_adid integer)");
//		db.execSQL(title.toString());
//		
//		
//		StringBuffer storageSerialProduct = new StringBuffer("create table if not exists ");
//		storageSerialProduct.append(TBL_STORAGE_PRODUCT_SERIAL).append("(id integer primary key autoincrement,"); 
//		storageSerialProduct.append("product_serial_id integer,") ;
//		storageSerialProduct.append("pc_id integer, ");			//pc_id
//		storageSerialProduct.append("serial_number varchar(80),");			//serial_number
//		storageSerialProduct.append("supplier_id integer,");	
//		storageSerialProduct.append("in_time varchar(30),");
//		storageSerialProduct.append("out_time varchar(30)");
//		storageSerialProduct.append(")");
//		db.execSQL(storageSerialProduct.toString());
//		
//		
//		StringBuffer lpInfoTransport = new StringBuffer("create table if not exists ");
//		lpInfoTransport.append(TBL_LP_INFO_TRANSPORT+"(id integer primary key autoincrement,");
//		lpInfoTransport.append("lp integer,");
//		lpInfoTransport.append("typeName varchar(80) , ");
//		lpInfoTransport.append("lpName  varchar(20) ,");
//		lpInfoTransport.append("bill_id  integer ,");
//		lpInfoTransport.append("container_type_id  integer ,");
//		lpInfoTransport.append("container_has_sn  integer ,");
//		lpInfoTransport.append("is_full  integer ,");
//		lpInfoTransport.append("container_type  varchar(10) ,");
//		lpInfoTransport.append("sub_container_type  varchar(10) ,");
//		lpInfoTransport.append("sub_container_type_id  integer,");
//		lpInfoTransport.append("sub_count  integer ,");
//		lpInfoTransport.append("container_pcid  integer ,");
//		lpInfoTransport.append("container_pc_name  varchar(10) ,");
//		lpInfoTransport.append("container_pc_count  integer , ");
//		lpInfoTransport.append("parent_container_id  integer,");
//		lpInfoTransport.append("lotNumber varchar(20), ");
//		lpInfoTransport.append("is_append_success integer,");
//		lpInfoTransport.append("is_server_copy integer default 0 ");
//		 
//		lpInfoTransport.append(")");
//		db.execSQL(lpInfoTransport.toString());
//		
//		
//		StringBuffer containerLoadTransport = new StringBuffer();
//		containerLoadTransport.append("create table if not exists ")
//		.append(TBL_CONTAINER_LOAD_TRANSPORT+"(id integer primary key autoincrement,")
//		.append("con_id integer,")
//		.append("container_type integer,")
//		.append("container_type_id integer,")
//		.append("parent_con_id integer,")
//		.append("parent_container_type integer,")
//		.append("parent_container_type_id integer")
//		.append(")");
//		db.execSQL(containerLoadTransport.toString());
//		
//		StringBuffer containerLoadChildListTransport = new StringBuffer();
//		containerLoadChildListTransport.append("create table if not exists ")
//		.append(TBL_CONTAINER_LOAD_CHILD_LIST+"(id integer primary key autoincrement,")
//		.append("search_root_con_id integer,")
//		.append("cont_id integer,")
//		.append("parent_con_id integer,")
//		.append("levv integer")
//		.append(")");
//		db.execSQL(containerLoadChildListTransport.toString());
//		
//		StringBuffer product_title = new StringBuffer();
//		product_title.append("create table if not exists ")
//		.append(TBL_PRODUCT_TITLE)
//		.append(" (title_id integer,")
//		.append("title_name varchar(100),")
//		.append("type_id integer,")
//		.append("title_admin_sort varchar(30))");
//		db.execSQL(product_title.toString());
//		
//		StringBuffer product_titles = new StringBuffer();
//		product_titles.append("create table if not exists ")
//		.append(TBL_PRODUCT_TITLES)
//		.append(" (id integer primary key autoincrement,")
//		.append("title_id integer,")
//		.append("title_name varchar(100),")
//		.append("title_admin_sort varchar(30),")
//		.append("state varchar,")//状态
//		.append("type integer)");//类别
//		db.execSQL(product_titles.toString());
//		
//		//TBL_PRINTER_TCP_CONNECT
//		StringBuffer PRINTER_TCP_CONNECT = new StringBuffer();
//		PRINTER_TCP_CONNECT.append("create table if not exists ")
//		.append(TBL_PRINTER_TCP_CONNECT)
//		.append(" (id integer primary key autoincrement,")
//		.append("connect_ip varchar(100),")
//		.append("connect_port varchar(10),")
//  		.append("connect_name varchar(50))");//类别
//		db.execSQL(PRINTER_TCP_CONNECT.toString());
//		 
//		StringBuffer recent_product_container = new StringBuffer();
//		recent_product_container.append("create table if not exists ")
//		.append(tbl_recent_product_container)
//		.append(" (id integer primary key autoincrement,")
//		.append("type_id integer,")
//		.append("pc_id integer,")
//		.append("pName varchar(50),")
//		.append("total integer,")
//		.append("total_length integer,")
//		.append("total_width integer,")
//		.append("total_height integer,")
//		.append("container_type integer,")
//		.append("inner_type integer,")
//		.append("subtotal integer,")
//		.append("own varchar(50),")
//		.append("supplier varchar(50),")
//		.append("supplier_id integer,")
//		.append("shopTo varchar(50),")
//		.append("lotnumber varchar(50),")
//  		.append("customer varchar(50))");//类别
//		db.execSQL(recent_product_container.toString());
//		
//		//yard_control
//		StringBuffer spot_area = new StringBuffer();
//		spot_area.append("create table if not exists ")
//		.append(TBL_SPOT_AREA)
//		.append(" (id integer primary key autoincrement,")
//		.append("spot_area_id integer,")
//  		.append("spot_area_name varchar(50))");
//		db.execSQL(spot_area.toString());
//		
//		
//		StringBuffer dock_zone = new StringBuffer();
//		dock_zone.append("create table if not exists ")
//		.append(TBL_DOCK_ZONE)
//		.append(" (id integer primary key autoincrement,")
//		.append("dock_zone_id integer,")
//  		.append("dock_zone_name varchar(50))");
//		db.execSQL(dock_zone.toString());
//		
//		//single_create_container
//		StringBuilder singleLpInfo = new StringBuilder("create table if not exists ");
//		singleLpInfo.append(tbl_single_lp_info+"(id integer primary key autoincrement,");
//		singleLpInfo.append("lp integer,");
//		singleLpInfo.append("typeName varchar(80) , ");
//		singleLpInfo.append("isReceive  integer, ");
//		singleLpInfo.append("lpName  varchar(20) ,");
//		singleLpInfo.append("bill_id  integer ,");
//		singleLpInfo.append("container_type_id  integer ,");
//		
//		singleLpInfo.append("container_has_sn  integer ,");
//		singleLpInfo.append("is_full  integer ,");
//		singleLpInfo.append("container_type  varchar(10) ,");
//		singleLpInfo.append("sub_container_type  varchar(10) ,");
//		singleLpInfo.append("sub_container_type_id  integer,");
//		singleLpInfo.append("sub_count  integer ,");
//		singleLpInfo.append("container_pcid  integer ,");
//		singleLpInfo.append("container_pc_name  varchar(10) ,");
//		singleLpInfo.append("container_pc_count  integer , ");
//		singleLpInfo.append("parent_container_id  integer,");
//		singleLpInfo.append("location_id  integer default 0,");
//		 
//		singleLpInfo.append("location_name varchar(50), ");
//		singleLpInfo.append("title_id integer, ");
//		singleLpInfo.append("title varchar(50), ") ;
//		singleLpInfo.append("lotNumber varchar(20), ");
//		singleLpInfo.append("is_append_success integer default 0 ,");
//		singleLpInfo.append("is_server_copy integer default 0 ");
//		singleLpInfo.append(")");
//		
//		db.execSQL(singleLpInfo.toString());
//		
//		//single_scan_info
//		StringBuilder singleScanInfo = new StringBuilder("create table if not exists ");
//		singleScanInfo.append(tbl_single_scan_info+"(id integer primary key autoincrement,");
//		singleScanInfo.append("sn varchar2(80) , ");
//		singleScanInfo.append("pcid integer , ");		//当成pcid 处理 
//		singleScanInfo.append("qty double,") ;
//		singleScanInfo.append("area_id integer , ");
//		singleScanInfo.append("location_id integer ,") ;
//		singleScanInfo.append("isSubmit integer default 0 , ") ; //0 表示没有。1表示已经提交了
//		singleScanInfo.append("p_name DEFAULT '' ,");
//		singleScanInfo.append("barCode  varchar2(50)  default '' ,");			 
//		singleScanInfo.append("barCodeType  varchar2(50)  default '', ");	
// 		singleScanInfo.append("lp  integer  , ");	
// 		singleScanInfo.append("lpName  varchar2(50)  default '', ");	
// 		singleScanInfo.append("parentLp  integer   ");		//父级容器的Container
// 		singleScanInfo.append(")");
//		db.execSQL(singleScanInfo.toString());
//		
//		//single_product_code
//		StringBuffer singleProductCode = new StringBuffer("create table if not exists ");
//		singleProductCode.append(tbl_single_product_code+" (id integer primary key autoincrement,");
//		singleProductCode.append("pcid integer , ");
//		singleProductCode.append("barcode varchar(80),") ;
//		singleProductCode.append("code_type varchar(10),");
//		singleProductCode.append("p_name varchar(10)");
//		singleProductCode.append(")");
//		db.execSQL(singleProductCode.toString());
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
 		onCreate(db);
	}
	

}
