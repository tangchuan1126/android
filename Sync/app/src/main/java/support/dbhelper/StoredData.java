package support.dbhelper;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import oso.SyncApplication;
import oso.ui.chat.util.XmppTool;
import support.common.bean.LoginPostname;
import support.key.BaseDataSetUpStaffJobKey;
import utility.FileUtils;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;
import utility.WakeLockManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.text.TextUtils;

/**
 * sharePreference封装
 * 
 * @author 朱成
 * @date 2014-9-17
 */
public class StoredData {

	public static final int Access_Normal = 0; // 普通人
	public static final String BaseAvatarUrl = "Avatar"; //应该在HttpUrlPath中定义
	
	private static SharedPreferences sp;

	private static List<LoginPostname> allpost = new ArrayList<LoginPostname>();
	
	private static SharedPreferences getSp() {
		if (sp == null)
			sp = SyncApplication.getThis().getSharedPreferences("loginSave", Context.MODE_PRIVATE);
			
		return sp;
	}

	// 用于跨进程
	private static SharedPreferences getSp_multiProc() {
		return SyncApplication.getThis().getSharedPreferences("loginSave", Context.MODE_MULTI_PROCESS);
	}
	
	// =========登陆相关============================================

	public static String getAdid() {
		// debug
		return getSp_multiProc().getString("adid", "");
	}

	public static void setAdid(String value) {
		getSp().edit().putString("adid", value).commit();
	}
	
	//avatarId,avatarUrl
	public static void setAvatarUrl(String imgId, String avatarUrl) {
		Set<String> avatarUrlSet = getAvatarUrlSet();
		getSp().edit().remove("avatarurl");
		avatarUrlSet.add(imgId+","+avatarUrl);
		getSp().edit().putStringSet("avatarurl", avatarUrlSet).commit();	
	}
	public static void delAvatarUrl(String imgId) {
		if(TextUtils.isEmpty(imgId)) return;
		Set<String> avatarUrlSet = getAvatarUrlSet();
		getSp().edit().remove("avatarurl");
		Iterator<String> it = avatarUrlSet.iterator();
		while(it.hasNext()) {
			if(imgId.equals(it.next().split(",")[0])) {
				it.remove();
				break;
			}
		}
		getSp().edit().putStringSet("avatarurl", avatarUrlSet).commit();	
	}
	//avatarId,avatarUrl
	public static void setDefaultAvatarUrl(String imgId, String avatarUrl) {
		getSp().edit().putString("defaultavatarurl", imgId+","+avatarUrl).commit();
	}
	//返回  imgId,avatarUrl
	public static String getDefaultAvatarUrl() {
		return getSp().getString("defaultavatarurl", "");
	}
	
	public static String getCurrUserAvatarUrl() {
		String url = StoredData.getDefaultAvatarUrl();
		if(TextUtils.isEmpty(url)) return "";
		if(url.indexOf(",") > 0) {
			return url.split(",")[1];
		}
		return "";
	}
	
	public static String getCurrUserAvatarImgId() {
		String url = StoredData.getDefaultAvatarUrl();
		if(TextUtils.isEmpty(url)) return "";
		if(url.indexOf(",") > 0) {
			return url.split(",")[0];
		}
		return "";
	}
	
	public static void clearDefaultAvatarUrl() {
		getSp().edit().remove("defaultavatarurl").commit();
	}
	public static Set<String> getAvatarUrlSet() {
		return getSp().getStringSet("avatarurl", new HashSet<String>());
	}
	// debug
	public static boolean isLogin_inRam = false; // 已在"登陆页"登陆
	
	public static boolean saveinfo = false; // 是否保存登陆信息

	/**
	 * 用户变更时,注:退出后重登-不算变更
	 */
	public static void onUserChange() {
		Goable.resetLoginInfo_inRam();
		DBManager.getThis().deleteAll();
	}

	/**
	 * 登出时调用
	 */
	public static void logout() {
		isLogin_inRam = false;
		// 断掉xmpp
		XmppTool.getThis().loginOut_of();
		
		//取消-notify
		Utility.cancelAllNotify();
		
		//断开 Sip debug
		//Receiver.engine(WareHouseApplication.getThis()).halt();
		
		//取消Alarm周期调用
		Utility.cancelAlarm(SyncApplication.getThis());
		
		// 释放WakeLock锁
		WakeLockManager.getInstants().releaseWakeLock();
	}

	/**
	 * oso账号-已登录(非推送)
	 * 
	 * @return
	 */
	public static boolean isLogin() {
		// debug
		return !TextUtils.isEmpty(getAdid()) && isLogin_inRam;
	}

	// =================================================

	public static boolean readShowMsgState() {
		return getSp().getBoolean("isShowMsg", true);
	}

	public static void saveShowMsgState(boolean isShowMsg) {
		SharedPreferences.Editor editor = getSp().edit();
		editor.putBoolean("isShowMsg", isShowMsg);
		editor.commit();
	}

	public static String getPs_id() {
		return getSp().getString("ps_id", "");
	}

	public static String getPs_name() {
		return getSp().getString("ps_name", "");
	}
	
	/**
	 * UserName
	 * @return
	 */
	public static String getUsername() {
		return getSp().getString("username", "");
	}
	
	public static void setUsername(String username) {
		getSp().edit().putString("username", username).commit();
	}
	
	/**
	 * 用户名
	 * 
	 * @return
	 */
	public static String getName() {
		return getSp().getString("name", "");
	}

	/**
	 * 密码
	 * 
	 * @return
	 */
	public static String getPwd() {
		return getSp().getString("pwd", "");
	}
	
	public static void setPwd(String newPwd) {
		getSp().edit().putString("pwd", newPwd).commit();
	}	
	/**
	 * URL
	 * 
	 * @return
	 */
	public static String readBaseServerName() {
		return getSp().getString("server_name", HttpUrlPath.DEFAULT_SERVER_NAME);
	}

	public static void saveBaseServerName(String serverName) {
		getSp().edit().putString("server_name", serverName).commit();
	}
	
	public static final int LA_EN = 0; //English
	public static final int LA_CN = 1; //简体中文
	
	/**
	 * 获取id语言
	 * 0,English  1,简体中文
	 */
	public static int readLanguageID() {
		return getSp().getInt("language", LA_EN);
	}

	/**
	 * 获取当前语言字符
	 * @return
	 */
	public static String readLanguageStr(){
		String zh_cn = "zh_cn";
		String en_us = "en_us";

		String currentStr = "";

		int readLanguageID = readLanguageID();

		switch (readLanguageID) {
			case LA_EN:
				currentStr = en_us;
				break;
			case LA_CN:
				currentStr = zh_cn;
				break;
			default:
				currentStr = en_us;
				break;
		}

		return currentStr;
	}

	public static void saveLanguageName(int languageID) {
		getSp().edit().putInt("language", languageID).commit();
	}
	
	public static boolean readIsFirstSelectLanguage() {
		return getSp().getBoolean("first_select_language", true);
	}
	
	public static void saveIsFirstSelectLanguage(boolean isFirst) {
		getSp().edit().putBoolean("first_select_language", isFirst).commit();
	}
	
	
	/**
	 * Patorl 是否重置
	 * @return
	 */
	public static boolean readResetPatrolStatus() {
		return getSp().getBoolean("reset_patrol_status", false);
	}

	public static void saveResetPatrolStatus(boolean isReset) {
		SharedPreferences.Editor editor = getSp().edit();
		editor.putBoolean("reset_patrol_status", isReset);
		editor.commit();
	}
	
	
	/**
	 * 账号   密码
	 * @return
	 */
	public static String readUserName() {
		return getSp().getString("username", "");
	}
	
	public static String readPassword() {
		return getSp().getString("password", "");
	}

	public static void saveUser(String username, String password) {
		SharedPreferences.Editor editor = getSp().edit();
		editor.putString("username", username);
		editor.putString("password", password);
		editor.commit();
	}
	
	

	/**
	 * Add Task Notify
	 * 
	 * @return
	 */
	public static String readNotifyUser() {
		return getSp().getString("notify", "");
	}

	public static void saveNotifyUser(String users) { // 用逗号隔开的字符串
		getSp().edit().putString("notify", users).commit();
	}

	public static String readNotifyUserId() {
		return getSp().getString("notifyUserId", "");
	}

	public static void saveNotifyUserId(String userIds) { // 用逗号隔开的字符串
		getSp().edit().putString("notifyUserId", userIds).commit();
	}

	/**
	 * 获取地图zip包版本号
	 * 
	 * @param value
	 */
	public static void setMapDataVersion(String path) {
		if (!FileUtils.isFileExist(path)) {
			getSp().edit().putInt("version", -11).commit();
			return;
		}
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		JSONObject json = null;
		try {
			json = new JSONObject(jsonStr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		int version = StringUtil.getJsonInt(json, "verson");
		getSp().edit().putInt("version", version).commit();
	}

	/**
	 * @Description:获取人员权限级别
	 */
	public static String getStaffPrivileges() { // 用逗号隔开的字符串
		return getSp().getString("projsid", "");
	}

	/**
	 * @Description:获取Adgid
	 */
	public static String getAdgid() { // 用逗号隔开的字符串
		return getSp().getString("adgid", "");
	}

	/**
	 * @Description:判断该人员是否是SuperVesor
	 * @param @return
	 */
	public static boolean isSupervisor() {
		String staffPrivileges = StoredData.getStaffPrivileges();

		if (StringUtil.isNullOfStr(staffPrivileges)) {
			return false;
		}

		if (staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.Supervisor + ",") >= 0) {
			return true;
		}
		if (staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.Manager + ",") >= 0) {
			return true;
		}
		if (staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.DeputyDirector + ",") >= 0) {
			return true;
		}

		return false;
	}

	/**
	 * 可访问assignTask
	 * 
	 * @return
	 */
	/*
	 * public static boolean canAccess_assignTask() {
	 * 
	 * String staffPrivileges = StoredData.getStaffPrivileges();
	 * 
	 * if (StringUtil.isNullOfStr(getAdgid())) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Manager + ",") >= 0) {
	 * return true; }
	 * 
	 * if (StringUtil.isNullOfStr(staffPrivileges)) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Warehouse + ",") >= 0 &&
	 * staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.DeputyDirector + ",") >=
	 * 0 || staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.Supervisor + ",")
	 * >= 0) { return true; }
	 * 
	 * return false; }
	 *//**
	 * 可访问Process
	 * 
	 * @return
	 */
	/*
	 * public static boolean canAccess_Process() { String staffPrivileges =
	 * StoredData.getStaffPrivileges();
	 * 
	 * if (StringUtil.isNullOfStr(getAdgid())) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Manager + ",") >= 0) {
	 * return true; }
	 * 
	 * // -------Window 人目前不确定可不可以进来 if
	 * (getAdgid().indexOf(BaseDataDepartmentKey.Window + ",") >= 0) { return
	 * true; }
	 * 
	 * if (StringUtil.isNullOfStr(staffPrivileges)) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Warehouse + ",") >= 0 &&
	 * staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.Employee + ",") >= 0 ||
	 * staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.DeputyDirector + ",") >=
	 * 0 || staffPrivileges.indexOf(BaseDataSetUpStaffJobKey.Supervisor + ",")
	 * >= 0) { return true; }
	 * 
	 * return false; }
	 *//**
	 * 可访问window_checkin
	 * 
	 * @return
	 */
	/*
	 * public static boolean canAccess_WindowCheckin() { if
	 * (StringUtil.isNullOfStr(getAdgid())) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Window + ",") >= 0) { return
	 * true; } if (getAdgid().indexOf(BaseDataDepartmentKey.Manager + ",") >= 0)
	 * { return true; } return false; }
	 *//**
	 * 可访问Gate
	 * 
	 * @return
	 */
	/*
	 * public static boolean canAccess_Gate() { if
	 * (StringUtil.isNullOfStr(getAdgid())) { return false; }
	 * 
	 * if (getAdgid().indexOf(BaseDataDepartmentKey.Gate + ",") >= 0) { return
	 * true; } if (getAdgid().indexOf(BaseDataDepartmentKey.Manager + ",") >= 0)
	 * { return true; }
	 * 
	 * return false; }
	 *//**
	 * 可访问Gate
	 * 
	 * @return
	 */
	/*
	 * public static boolean is_Gate() { if
	 * (getAdgid().indexOf(BaseDataDepartmentKey.Gate + ",") >= 0) { return
	 * true; } return false; }
	 */

	// public static boolean canAccess_LoadReceive(){
	// return true ;
	// }

	public static int getMapDataVersion() {
		return getSp().getInt("version", -1);
	}
	
	/**
	 * 获取本地登陆用户的头像url
	 * @param array
	 */
	public static String[] getLocalAvatarUrl() {
		boolean hasNext=true;
		int index = 0;
		String[] urlArray = {};
		while(hasNext) {
			if(getSp().contains(BaseAvatarUrl + index)) {
				urlArray[index] = BaseAvatarUrl + index;
			} else {
				hasNext = false;
				break;
			}
		}
		return urlArray;
	}
	
	/**
	 * 登陆account所在仓库的role
	 * --supervisor
	 * --loader     etc.
	 * @param post_name
	 */
	public static List<LoginPostname> getAllpost() {
		return allpost;
	}

	public static void setAllpost(List<LoginPostname> allpost) {
		StoredData.allpost = allpost;
	}
	public static void setPostname(String post_name) {
		getSp().edit().putString("post_name", post_name).commit();
	}
	public static String getPostname() {
		return getSp().getString("post_name", "");
	}
	public static void setPostId(String post_id) {
		getSp().edit().putString("post_id", post_id).commit();
	}
	public static String getPostId() {
		return getSp().getString("post_id", "");
	}
	//=============== Chat account : warehousename 键值对 
	private static SharedPreferences spAccWarehouse;
	public static SharedPreferences getExtAccSp() {
		if (spAccWarehouse == null) {
			spAccWarehouse = SyncApplication.getThis()
					.getSharedPreferences("ExtAccount", Context.MODE_PRIVATE);
		}
		return spAccWarehouse;
	}
	/**
	 * 获得Account:WarehouseName Map集合
	 * @return Map<String, ?> 
	 */
	public static Map<String, ?> getExtAccMap() {
		return getExtAccSp().getAll();
	}
	/**
	 * 缓存额外的账号信息  
	 * @param String adid
	 * @param Set<String> warename, file_path[头像下载路径]
	 */
	public static void clearExtAcc() {
		getExtAccSp().edit().clear();
	}
	public static void saveExtAcc(String adid, Set<String> strSet) {
		Editor editor = getExtAccSp().edit();
		editor.putStringSet(adid, strSet).commit();
	}

	public static void clearAvatar() {
		getSp().edit().remove("defaultavatarurl").remove("avatarurl").commit();
	}
	
	//==================receive===============================
	
//	/**
//	 * 全大写
//	 * @param etLoc
//	 */
//	public static void setRec_def_loc(EditText etLoc,String rnNO){
//		//非空-才保存
//		String loc=etLoc.getText().toString().toUpperCase();
//		if (!TextUtils.isEmpty(loc)) {
//			getSp().edit().putString("rec_def_loc", loc)
//					.putString("rec_def_loc_rnNO", rnNO).commit();
//		}
//	}
//	
//	public static String getRec_def_loc(String rnNO){
//		String saveRN=getSp().getString("rec_def_loc_rnNO", "");
//		String saveLoc=getSp().getString("rec_def_loc", "");
//		//取缓存
//		if(TextUtils.equals(saveRN, rnNO))
//			return saveLoc;
//		return "";
//	}

	/**
	 * 存储对象到内存
	 * 
	 * @param context
	 * @param key
	 * @param obj
	 */
	public static <T> void saveObjectToRAM(Context context, String key, T obj) {
		saveObject(context, key, obj, false);
	}

	/**
	 * 存储对象到SD卡
	 * 
	 * @param context
	 * @param key
	 * @param obj
	 */
	public static <T> void saveObjectToSD(Context context, String key, T obj) {
		saveObject(context, key, obj, true);
	}

	private static <T> void saveObject(Context context, String key, T obj, boolean isSaveToSD) {
		String rootpath = context.getCacheDir().getAbsolutePath();
		if (SDUtils.isSDExists() && isSaveToSD)
			rootpath = Environment.getExternalStorageDirectory().toString() + File.separator + "cache";
		String filePath = rootpath + File.separator + key + ".cache";
		SDUtils.serialization(filePath, obj);
	}

	public static <T> void saveListToRAM(Context context, String key, List<T> list) {
		if (list == null || list.isEmpty())
			return;
		for (T obj : list) {
			if (obj != null)
				saveObjectToRAM(context, key, obj);
		}
	}

	/**
	 * 从内存中读取对象
	 * 
	 * @param context
	 * @param key
	 * @param obj
	 */
	public static <T> T readObjectToRAM(Context context, String key) {
		return readObject(context, key, false);
	}

	/**
	 * 从SD卡中读取对象
	 * 
	 * @param context
	 * @param key
	 * @param obj
	 */
	public static <T> T readObjectToSD(Context context, String key) {
		return readObject(context, key, false);
	}

	private static <T> T readObject(Context context, String key, boolean isReadToSD) {
		String rootpath = context.getCacheDir().getAbsolutePath();
		if (SDUtils.isSDExists() && isReadToSD)
			rootpath = Environment.getExternalStorageDirectory().toString() + File.separator + "cache";
		String filePath = rootpath + File.separator + key + ".cache";
		return SDUtils.deserialization(filePath);
	}

	private static SharedPreferences spServers;
	public static SharedPreferences getServerSp() {
		if (spServers == null) {
			spServers = SyncApplication.getThis()
					.getSharedPreferences("Server", Context.MODE_PRIVATE);
		}
		return spServers;
	}
	/**
	 * 设置添加新服务器url
	 * @param name
	 * @param url
	 */
	public static void saveNewServer(String name, String url) {
		Map<String, String> allSerMap = (Map<String, String>) getServerSp().getAll();
		Set<String> urlSet = new HashSet<String>();
		urlSet.add(url);

		if(allSerMap == null) {
			allSerMap = new HashMap<String, String>();
		}
		allSerMap.put(name, url);
		getServerSp().edit().clear();
		getServerSp().edit().putStringSet(name, urlSet).commit();
	}

	/**
	 * 获得所有自定义的服务器Map
	 * @return
	 */
	public static Map<String, HashSet<String>> getExtServers() {
		if(getServerSp() != null && getServerSp().getAll() != null) {
			return (Map<String, HashSet<String>>) getServerSp().getAll();
		}
		return new HashMap<String, HashSet<String>>();
	}
}
