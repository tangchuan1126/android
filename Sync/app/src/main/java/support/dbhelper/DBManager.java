package support.dbhelper;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import oso.SyncApplication;
import oso.ui.chat.bean.ChatHistoryRecord;
import oso.ui.chat.bean.ChatHistoryRecord.Records;
import oso.ui.chat.bean.ChatMessage;
import oso.ui.chat.bean.RecentMsg;
import oso.ui.chat.util.ChatUtil;
import oso.widget.photo.TabParam;
import oso.widget.photo.bean.TTPImgBean;
import oso.widget.sign.SignatureBoard;
import utility.Utility;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

public class DBManager extends SQLiteOpenHelper {

	private final static String DBNAME = "mydata.db";

	/*
	 * db更新→
	 * v1:添加消息表chat_msg
	 * v2:添加file
	 */
	private final static int DB_VERSION = 1;
//	private final static int DB_VERSION = AppConfig.isDebug?2:1;

	private static DBManager instance;

	private DBManager(Context context) {
		// TODO Auto-generated constructor stub
		super(context, DBNAME, null, DB_VERSION);
	}

	public static DBManager getThis() {
		if (instance == null)
			instance = new DBManager(SyncApplication.getThis());

		return instance;
	}

	// 初始化,用于触发"onCreate/onUpdate"
	public void init() {
		SQLiteDatabase db = getReadableDatabase();
		db.close();
	}

	// 请求-语句
	private void doQuery(String stat) {
		SQLiteDatabase db = getWritableDatabase();
		try {
			System.out.println("sql>>" + stat);
			db.execSQL(stat);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		db.close();
	}

	//==================file===================================
	
	/**
	 * 获取-本地缓存
	 * @注:1.不取"正在上传"的  2.删掉无图片的(已上传)
	 * @param list
	 * @param p
	 */
	public synchronized void getLocalImgs(List<TTPImgBean> list,TabParam p){
		StringBuffer con=new StringBuffer("1=1 ");
		if(!TextUtils.isEmpty(p.file_with_class))
			con.append(" and file_with_class="+p.file_with_class);
		if(!TextUtils.isEmpty(p.file_with_type))
			con.append(" and file_with_type="+p.file_with_type);
		if(!TextUtils.isEmpty(p.file_with_id))
			con.append(" and file_with_id="+p.file_with_id);
		if(!TextUtils.isEmpty(p.file_with_detail_id))
			con.append(" and file_with_detail_id="+p.file_with_detail_id);
		if(!TextUtils.isEmpty(p.option_file_param))
			con.append(" and option_file_param="+p.option_file_param);
		
		SQLiteDatabase db=getReadableDatabase();
		String sql="select * from file where "+con.toString();
		Cursor c=db.rawQuery(sql, null);
		
		while (c.moveToNext()) {
			String uri=c.getString(c.getColumnIndex(T_File.uri));
			File tmpF=new File(uri);
			//debug,若不存在 则删掉
			if(!tmpF.exists())
			{
				sql="delete from file where uri='"+uri+"'";
				db.execSQL(sql);
				continue;
			}
			
			//若在上传中 则不提,防止重复
//			if(UpImgMnger.getThis().mapFileReq.containsKey(uri))
//				continue;
			
			TTPImgBean tmpB=new TTPImgBean(uri);
			list.add(tmpB);
		}
		c.close();
		db.close();
	}
	
	public synchronized void delLocalImg(String file){
		SQLiteDatabase db=getWritableDatabase();
		String sql="delete from file where uri='"+file+"'";
		db.execSQL(sql);
		db.close();
		
		//debug,删除文件
		Utility.delFile(file);
	}

	/**
	 * 添加-缓存图片
	 * @param p
	 * @param file
	 */
	public synchronized void addLocalImg(TabParam p,String file){ 
		SQLiteDatabase db = getWritableDatabase();
		ContentValues v=new ContentValues();
		v.put(T_File.file_with_class, p.file_with_class);
		v.put(T_File.file_with_type, p.file_with_type);
		v.put(T_File.file_with_id, p.file_with_id);
		v.put(T_File.file_with_detail_id, p.file_with_detail_id);
		v.put(T_File.option_file_param, p.option_file_param);
		
		v.put(T_File.uri, file);
		long rowID=db.insert(T_File.tableName, null, v);
		if(rowID==-1)
			System.err.println("DBManager.addLocalImg,insertFail");
		db.close();
	}
	
	
	// -------------chat---------------------------------------

	/**
	 * 插入一条聊天消息到数据库。Message中包括id, unameMe, unamePeer, messageBody, createTime,
	 * isMine(是本地发出，还是本地被动接收)
	 * 
	 * @param message
	 * @return long返回值
	 */
	public synchronized long insert(ChatMessage message) {
		String id = message.getId();
		String aliasPeer = ChatUtil.findAliasByUsername(message.getUnamePeer());
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(T_ChatMsg.ID, id);
		values.put(T_ChatMsg.uname_me, message.getUnameMe());
		values.put(T_ChatMsg.uname_peer, message.getUnamePeer());
		values.put(T_ChatMsg.alias_peer, aliasPeer);
		values.put(T_ChatMsg.msg, message.getMessageBody());
		values.put(T_ChatMsg.createtime, message.getMessageTime());
		values.put(T_ChatMsg.out, String.valueOf(message.isMine())); // true:
																		// 本地发出
																		// false:
																		// 本地被动接收
		values.put(T_ChatMsg.isread, String.valueOf(message.isRead())); // true:
																		// 本地发出
																		// false:
																		// 本地被动接收
		long result = db.insert(T_ChatMsg.tableName, null, values);
		db.close();
		return result;
	}
	
	/**
	 * 将历史记录全部插入数据库  历史记录为全部已读
	 * @param chatHistoryRecord
	 */
	public synchronized void insert(ChatHistoryRecord chatHistoryRecord) {
		List<Records> records = chatHistoryRecord.records;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		String uname_me = StoredData.getName();
		for(int i=0; i<records.size(); i++) {
			Records record = records.get(i);
			values.put(T_ChatMsg.ID, record.id);
			
			boolean isMine = true;
			String uname_peer = null;
			// 判断是否是自己发送的
			if(StoredData.getName().equals(record.sender_ID)) {
				isMine = true;
			} else {
				isMine = false;  
			}
			uname_peer = StoredData.getName().equals(record.sender_ID) ? record.receiver_ID : record.sender_ID ;
			values.put(T_ChatMsg.uname_me, uname_me);
			values.put(T_ChatMsg.uname_peer, uname_peer);
			
			String createtime = record.create_DATE.substring(0, record.create_DATE.length()-2); // 处理时间
			values.put(T_ChatMsg.alias_peer, ChatUtil.findAliasByUsername(uname_peer));
			values.put(T_ChatMsg.msg, record.content);
			values.put(T_ChatMsg.createtime, createtime);
			values.put(T_ChatMsg.out, String.valueOf(isMine));
			values.put(T_ChatMsg.isread, String.valueOf(true));
			
			db.insert(T_ChatMsg.tableName, null, values);
		}
		
	}
	/**
	 * 根据指定unamePeer 和 MessageId删除消息
	 * 
	 * @param unamePeer
	 * @param message
	 * @return int返回值
	 */
	public synchronized int delete(String messageId) {
		SQLiteDatabase db = getWritableDatabase();
		int result = db.delete(T_ChatMsg.tableName, "ID=?",
				new String[] { String.valueOf(messageId) });
		db.close();
		return result;
	}

	public synchronized int delete(ChatMessage message) {
		int result = delete(message.getId());
		return result;
	}

	/**
	 * 删除和指定unamePeer 聊天用户的所有聊天记录
	 * 
	 * @param unamePeer
	 * @return int 返回值
	 */
	public synchronized int deleteAll(String unamePeer) {
		SQLiteDatabase db = getWritableDatabase();
		int result = db.delete(T_ChatMsg.tableName, "uname_peer=?",
				new String[] { unamePeer });
		db.close();
		return result;
	}
	
	
	/**
	 * 删除数据库中所有记录
	 * @return
	 */
	public synchronized int deleteAll() {
		// 删除签名历史记录
		final File imgFolder = new File(SignatureBoard.img_sign);
		Utility.delFolder(imgFolder);
		//
		SQLiteDatabase db = getWritableDatabase();
		return db.delete(T_ChatMsg.tableName, null, null);
	}
	
	/**
	 * 更改Message的状态(未读/已读)
	 * 
	 * @param messageId
	 * @param isRead
	 * @return int状态码
	 */
	public synchronized int update(int messageId, boolean isRead) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(T_ChatMsg.isread, String.valueOf(isRead));
		int result = db.update(T_ChatMsg.tableName, values, "ID=?",
				new String[] { String.valueOf(messageId) });
		db.close();
		return result;
	}

	private synchronized int callUpdate(String messageId, boolean isRead) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(T_ChatMsg.isread, String.valueOf(isRead));
		int result = db.update(T_ChatMsg.tableName, values, "ID=?",
				new String[] { String.valueOf(messageId) });
		db.close();
		return result;
	}
	
	/**
	 * 查询指定会话的最后一条消息ID
	 * @param unameMe
	 * @param unamePeer
	 * @return
	 */
	public String queryLastId(String unameMe, String unamePeer) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select ID from chat_msg where uname_me=? and uname_peer=? order by createtime", new String[]{unameMe, unamePeer});
		if(cursor.moveToFirst()) {
			return cursor.getString(0);
		}
		return "";
	}
	
	/**
	 * 根据指定unamePeer(chat with somebody), 查询所有和此人的聊天记录
	 * 
	 * @param unamePeer
	 * @return List<ChatMessage>
	 */
	public synchronized List<ChatMessage> query(String unameMe, String unamePeer) {

		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(T_ChatMsg.tableName, new String[] {
				T_ChatMsg.ID, T_ChatMsg.createtime, T_ChatMsg.msg,
				T_ChatMsg.uname_me, T_ChatMsg.out, T_ChatMsg.isread },
				"uname_me=? and uname_peer=?", new String[] { unameMe,
						unamePeer }, null, null, null);
		ArrayList<ChatMessage> msgRecordList = new ArrayList<ChatMessage>();

		while (cursor.moveToNext()) {
			String id = cursor.getString(0);
			String messageTime = cursor.getString(1);
			String messageBody = cursor.getString(2);

			String uname_me = cursor.getString(3);
			boolean out = Boolean.parseBoolean(cursor.getString(4));
			boolean isRead = Boolean.parseBoolean(cursor.getString(5));
			String uname_peer = unamePeer;
			ChatMessage message = new ChatMessage(id,
					messageTime, messageBody, uname_me, uname_peer, out, isRead);
			msgRecordList.add(message);
		}
		cursor.close();
		db.close();
		return msgRecordList;
	}

	/**
	 * 根据unameMe 和 unameYou 查询指定条数的数据，从哪个位置开始查询
	 * 
	 * @param unameMe
	 * @param unamePeer
	 * @param count
	 * @return 消息记录集合
	 */
	public synchronized List<ChatMessage> query(String unameMe,
			String unamePeer, int count, int startIndex) {
		SQLiteDatabase db = getReadableDatabase();
		ArrayList<ChatMessage> msgRecordList = new ArrayList<ChatMessage>();

		// limit 查询的数量 offset 从哪个位置开始 取 limit的数量
		/*Cursor cursor = db
				.rawQuery(
						"select * from chat_msg where uname_me=? and uname_peer=? order by createtime DESC limit ? offset ?",
						new String[] { unameMe, unamePeer, count + "",
								startIndex + "" });*/
		
		/*Cursor cursor = db.rawQuery("select * from (select * " +
				"from chat_msg where uname_me=? and uname_peer=? order by createtime DESC limit ? offset ?)" +
				" as t order by t.ID" , new String[]{unameMe, unamePeer, count+"", startIndex+""});*/
		
		Cursor cursor = db.rawQuery("select * from chat_msg where uname_me=? and uname_peer=? order by createtime DESC,ID desc limit ? offset ?", 
				new String[] {unameMe, unamePeer, count+"", startIndex+""});
		
		while (cursor.moveToNext()) {
			String id = cursor.getString(cursor.getColumnIndex(T_ChatMsg.ID));
			String messageTime = cursor.getString(cursor
					.getColumnIndex(T_ChatMsg.createtime));
			String messageBody = cursor.getString(cursor
					.getColumnIndex(T_ChatMsg.msg));
			String alias = cursor.getString(cursor
					.getColumnIndex(T_ChatMsg.alias_peer));
			boolean out = Boolean.parseBoolean(cursor.getString(cursor
					.getColumnIndex(T_ChatMsg.out)));
			boolean isRead = Boolean.parseBoolean(cursor.getString(cursor
					.getColumnIndex(T_ChatMsg.isread)));
			ChatMessage message = new ChatMessage(id,
					messageTime, messageBody, unameMe, unamePeer, out, isRead);
			message.setAliasPeer(alias);

			msgRecordList.add(message);
		}

		return msgRecordList;
	}

	/**
	 * 查询最近消息
	 * 
	 * @return List<RecentMsg>
	 */
	public synchronized List<RecentMsg> queryRecentMsg(String unameMe) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db
				.rawQuery(
						"select count(*), uname_peer, alias_peer, msg, createtime from chat_msg where uname_me=? and isread='false' group by uname_peer  order by createtime DESC ",
						new String[] { unameMe });
		List<RecentMsg> dataList = null;
		List<String> messageIdList = null;
		while (cursor.moveToNext()) {
			if (dataList == null) {
				dataList = new ArrayList<RecentMsg>();
				messageIdList = new ArrayList<String>();
			}
			int unreadMsgCount = cursor.getInt(0);
			String uname_peer = cursor.getString(1);
			String alias_peer = cursor.getString(2);
			String messageBody = cursor.getString(3);
			String createtime = cursor.getString(4);

			RecentMsg recentMsg = new RecentMsg(null, uname_peer, alias_peer,
					messageBody, createtime, unreadMsgCount);

			// 查询指定uname_peer 所有未读状态的消息ID
			messageIdList = queryUnreadMessageIDList(unameMe, uname_peer);
			recentMsg.setMessageIdList(messageIdList);
			dataList.add(recentMsg);
		}
		cursor.close();
		db.close();
		return dataList;
	}

	public synchronized List<RecentMsg> queryRecentMsgItem(String unameMe) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.rawQuery("select count(*) , *  " +
				"from (select createtime, uname_peer, alias_peer, msg, createtime " +
				"from chat_msg where uname_me=? order by createtime) " +
				"as t group by  uname_peer order by t.createtime desc ", 
				new String[]{unameMe});
		List<RecentMsg> dataList = null;
		List<String> messageIdList = null;
		while (cursor.moveToNext()) {
			if (dataList == null) {
				dataList = new ArrayList<RecentMsg>();
				messageIdList = new ArrayList<String>();
			}
			String createtime = cursor.getString(1);
			String uname_peer = cursor.getString(2);
			String alias_peer = cursor.getString(3);
			String messageBody = cursor.getString(4);
			int unreadMsgCount = queryRecentMsgCount(unameMe, uname_peer);

			RecentMsg recentMsg = new RecentMsg(null, uname_peer, alias_peer,
					messageBody, createtime, unreadMsgCount);

			// 查询指定uname_peer 所有未读状态的消息ID
			messageIdList = queryUnreadMessageIDList(unameMe, uname_peer);
			recentMsg.setMessageIdList(messageIdList);
			dataList.add(recentMsg);
		}
		cursor.close();
		db.close();
		return dataList;
	}

	/**
	 * 查询指定好友未读消息的消息数量
	 * 
	 * @return List<RecentMsg> RecentMsg中包含未读字段
	 */
	public synchronized int queryRecentMsgCount(String unameMe, String unamePeer) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db
				.rawQuery(
						"select count(*) from chat_msg where uname_me=? and isread='false' and uname_peer=?",
						new String[] { unameMe, unamePeer });
		int count = 0;
		if (cursor.moveToNext()) {
			count = cursor.getInt(0);
		}
		cursor.close();
		return count;
	}

	/**
	 * 查询所有含有未读消息的消息数量集合
	 * 
	 * @return List<RecentMsg> RecentMsg中包含未读字段
	 */
	public synchronized int queryRecentMsgCount(String unameMe) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db
				.rawQuery(
						"select count(*) from chat_msg where uname_me=? and isread='false'",
						new String[] { unameMe });
		int count = 0;
		if (cursor.moveToNext()) {
			count = cursor.getInt(0);
		}
		cursor.close();
		db.close();
		return count;
	}

	public synchronized void updateReadStatusWithPeer(String unameMe,
			String unamePeer) {
		
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("isread", "true"); // 会话修改为全部已读
		db.update(T_ChatMsg.tableName, values, "uname_me=? and uname_peer=?", new String[]{unameMe, unamePeer});
		db.close();
	}

	/**
	 * 查询指定uname_peer 所有未读状态的消息ID
	 * 
	 * @param unamePeer
	 * @return List<Integer> 指定用户的所有未读状态的消息ID集合
	 */
	private synchronized List<String> queryUnreadMessageIDList(String unameMe,
			String unamePeer) {
		List<String> messageIdList = null;
		SQLiteDatabase db = getWritableDatabase();
		Cursor msgCursor = db
				.rawQuery(
						"select ID from chat_msg where uname_me=? and isread='false' and uname_peer=?",
						new String[] { unameMe, unamePeer });
		while (msgCursor.moveToNext()) {
			if (messageIdList == null) {
				messageIdList = new ArrayList<String>();
			}
			String messageId = msgCursor.getString(0);
			messageIdList.add(messageId);
		}
		msgCursor.close();
		return messageIdList;
	}

	// --------------------------------------------------

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		onUpgrade(db, 0, DB_VERSION);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		for (int i = oldVersion + 1; i < newVersion + 1; i++)
			execDBScript(db, "db/update_" + i + ".sql");
	}

	// 执行脚本
	private void execDBScript(SQLiteDatabase db, String assetName) {
		try {
			InputStream is = SyncApplication.getThis().getAssets()
					.open(assetName);
			String stats = Utility.read(is);

			String[] createScript = stats.split(";");
			for (int i = 0; i < createScript.length; i++) {
				String sqlStatement = createScript[i].trim();
				// TODO You may want to parse out comments here
				if (sqlStatement.length() > 0) {

					// 捕获错误,防止执行drop时 直接越过
					try {
						db.execSQL(sqlStatement + ";");
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// ==================nested===============================

	/**
	 * 聊天消息
	 * 
	 * @author 朱成
	 * @date 2015-1-20
	 */
	public static class T_ChatMsg {
		public final static String tableName = "chat_msg";
		public final static String ID = "ID";
		public final static String uname_me = "uname_me";
		public final static String uname_peer = "uname_peer";
		public final static String alias_peer = "alias_peer";
		public final static String msg = "msg";
		public final static String out = "out";
		public final static String isread = "isread";
		public final static String createtime = "createtime";
	}
	
	/**
	 * 本地图片-缓存
	 * @author 朱成
	 * @date 2015-4-23
	 */
	public static class T_File{
		public final static String tableName = "file";
		public final static String file_id = "file_id";				//服务器端id,本地缓存"file_id<=0"
		public final static String uri = "uri";
		public final static String file_with_class = "file_with_class";
		public final static String file_with_type = "file_with_type";
		public final static String file_with_id = "file_with_id";
		
		public final static String file_with_detail_id = "file_with_detail_id";
		public final static String option_file_param = "option_file_param";
	}


}
