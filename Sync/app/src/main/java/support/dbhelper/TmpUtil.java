package support.dbhelper;

import java.util.List;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletConfigBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;


/*
 * QtyDialog_CC   保存临时信息，下次默认显示
 */
public class TmpUtil {
	private static LocBean rec_def_loc;		//显示组合    Location:RT    Staging:RT
	private static String rec_def_loc_rnNO="";
	
	/**
	 * 全大写
	 * @param etLoc
	 */
	public static void setRec_def_loc(LocBean loc,String rnNO){
		//非空-才保存
		if(LocBean.isValid(loc)){
			rec_def_loc=loc;
			rec_def_loc_rnNO=rnNO;
		}
	}
	
	public static LocBean getRec_def_loc(String rnNO,List<Rec_PalletConfigBean> listPlts){
		LocBean defLoc=null;
		//取缓存
		if(TextUtils.equals(rec_def_loc_rnNO, rnNO)&&LocBean.isValid(rec_def_loc))
			defLoc=rec_def_loc;
		
		//若未缓存
		if(!LocBean.isValid(defLoc)){
			defLoc=Rec_PalletConfigBean.getMostLoc(listPlts);
			//缓存
			if(LocBean.isValid(defLoc))
			{
				rec_def_loc=defLoc;
				rec_def_loc_rnNO=rnNO;
			}
		}
		return defLoc;

	}
}
