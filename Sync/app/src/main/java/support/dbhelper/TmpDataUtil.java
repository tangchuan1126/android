package support.dbhelper;

import java.util.List;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import android.text.TextUtils;

/**
 * 临时全局变量,如:receive中记住上次输的StagingLocation
 * @author 朱成
 * @date 2015-4-15
 */
public class TmpDataUtil {

	private static LocBean rec_def_loc;		//收货中默认stagingLocation,每次提交后记下(同一个rn 才会自动给上)
	private static String rec_def_loc_rnNO="";
	
	/**
	 * 全大写
	 */
	public static void setRec_def_loc(LocBean loc,String rnNO){
		//非空-才保存
//		String loc=etLoc.getText().toString().toUpperCase();
		if(LocBean.isValid(loc)){
			rec_def_loc=loc;
			rec_def_loc_rnNO=rnNO;
		}
	}
	
	public static LocBean getRec_def_loc(String rnNO,List<Rec_PalletBean> listPlts){
		LocBean defLoc=null;
		//取缓存
		if(TextUtils.equals(rec_def_loc_rnNO, rnNO)&&LocBean.isValid(rec_def_loc))
			defLoc=rec_def_loc;
		
		//若未缓存
		if(!LocBean.isValid(defLoc)){
			defLoc=Rec_PalletBean.getMostLoc(listPlts);
			//缓存
			if(LocBean.isValid(defLoc))
			{
				rec_def_loc=defLoc;
				rec_def_loc_rnNO=rnNO;
			}
		}
		return defLoc;
		
//		//rn不同,则不提示
//		if(!TextUtils.equals(rec_def_loc_rnNO, rnNO))
//			return "";
//		return rec_def_loc==null?"":rec_def_loc;
	}
	

//	public String getRec_location() {
//		return rec_location==null?"":rec_location;
//	}
//
//	public void setRec_location(String rec_location) {
//		this.rec_location = rec_location;
//	}
	
	
	
}
