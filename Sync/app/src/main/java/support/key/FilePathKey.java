package support.key;

/**
 * 系统中config 表中配置的文件路径
 * @author Administrator
 *
 */
public class FilePathKey {
	
	public static String file_path_admin  = "admin";					//用户图片目录
	public static String file_path_financial  = "financial_management";	//资金文件目录
	public static String file_path_knowledge  = "knowledge";			//问答文件目录
	public static String file_path_product  = "product";				//商品文件目录
	public static String file_path_product_label  = "product_label";	//标签文件目录	
	public static String file_path_receive  = "receive";			
	public static String file_path_repair  = "repair";	
	public static String file_path_return_product  = "return_product";	
	public static String file_path_return_product_detail  = "return_product_detail";	
	public static String file_path_transport  = "transport";			//转运单文件目录
	public static String file_check_in  = "check_in";			//转运单文件目录

	
}
