package support.key;

import java.util.HashMap;
import java.util.Map;

import utility.StringUtil;
/**
 * @ClassName: BaseDataSetUpStaffJobKey 
 * @Description: 人物权限Key
 * @author gcy
 * @date 2015-1-30 下午9:34:00
 */
public class BaseDataSetUpStaffJobKey {

	
 
	
	public static int All = -1;
	public static int LeadSuperVisor = 15 ;			//副主管 +  主管
	public static int Supervisor = 10;				//主管
	public static int DeputyDirector = 5 ;			//副主管
	public static int Manager = 1 ;					//管理员
	public static int Employee = 0 ;				//普通人

	 
 
	private static Map<Integer, String> values;
	static {
		values = new HashMap<Integer, String>();
		values.put(All, "ALL");
		values.put(LeadSuperVisor, "LeadSuperVisor");
		values.put(Supervisor, "Supervisor");
		values.put(DeputyDirector, "Leader");
 		values.put(Manager, "Manager");
		values.put(Employee, "Worker");
	}

	public static String getType(int key) {
		return values.get(key);
	}

	public static String getType(String key) {
		return getType(StringUtil.convert2Inter(key));
	}

}
