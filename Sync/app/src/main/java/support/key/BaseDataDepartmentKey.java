package support.key;

import support.dbhelper.StoredData;

/**
 * @ClassName: BaseDataDepartmentKey 
 * @Description: 部门分类key   针对登录人员所返回的字段adgid 进行判断  相关工具类 StoredData
 * @author gcy
 * @date 2015-1-30 下午9:22:37
 */
public class BaseDataDepartmentKey {

 
	
	public static final int Window = 1000003;
	public static final int Gate = 1000001 ;			 
	public static final int Warehouse = 1000002;				 
	public static final int Manager = 10000 ;
    //debug
    public static final int Scanner = 1000013 ;
	 
	
	/**
	 * @Description:判断操作人是否可以进行window操作
	 * @param @return
	 */
	public static boolean judgeIsCanGoingToWindow(){
		if((BaseDataDepartmentKey.Window+"").equals(StoredData.getAdgid())){
			return true;
		}
		if((BaseDataDepartmentKey.Manager+"").equals(StoredData.getAdgid())){
			return true;
		}
		return false;
	}
}
