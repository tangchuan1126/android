package support.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class PurchaseTransportPageTypesKey {
	
	Map<String, String> row;
	public static String SEND_ADDRESS = "1";
	public static String DELIVER_ADDRESS = "2";
	public static String OTHERS = "3";
	public static String TRANSPORT_DETAILS = "4";
	public static String PROCESS_INFO = "5";
	public static String FREGHT_COMPANY = "6";
	public static String FREGHT_COST = "7";
	
	public PurchaseTransportPageTypesKey()
	{
		row = new HashMap<String, String>();
		row.put(PurchaseTransportPageTypesKey.SEND_ADDRESS, "提货地址");
		row.put(PurchaseTransportPageTypesKey.DELIVER_ADDRESS, "收货地址");
		row.put(PurchaseTransportPageTypesKey.OTHERS, "其他");
		row.put(PurchaseTransportPageTypesKey.TRANSPORT_DETAILS, "货物列表");
		row.put(PurchaseTransportPageTypesKey.PROCESS_INFO, "流程指派");
		row.put(PurchaseTransportPageTypesKey.FREGHT_COMPANY, "运输设置");
		row.put(PurchaseTransportPageTypesKey.FREGHT_COST, "运费设置");
	}
	
	
	public List getPurchaseTransportPageTypesKeys()
	{
		Set<String> set = row.keySet();
		TreeSet<String> treeSet = new TreeSet<String>();
		List<String> list = new ArrayList<String>();
		for (Iterator iterator = set.iterator(); iterator.hasNext();)
		{
			String string = (String) iterator.next();
			treeSet.add(string);
		}
		for (Iterator iterator = treeSet.iterator(); iterator.hasNext();)
		{
			String string = (String) iterator.next();
			list.add(string);
		}
		return list;
	}
	
	public String getPurchaseTransportPageTypesKeyValueById(String id)
	{
		return row.get(id);
	}
	

}
