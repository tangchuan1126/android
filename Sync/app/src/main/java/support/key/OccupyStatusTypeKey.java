package support.key;

import java.util.HashMap;
import java.util.Map;

import utility.StringUtil;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import declare.com.vvme.R;

public class OccupyStatusTypeKey {

	public final static int NULL = 0;
	public final static int RESERVERED = 1;
	public final static int OCUPIED = 2;
	public final static int RELEASED = 3;
	final static Map<String, Integer> row = new HashMap<String, Integer>();
	
	static{
		row.put(String.valueOf(NULL), R.string.sync_null);
		row.put(String.valueOf(RESERVERED), R.string.tms_reserved);		//保留
		row.put(String.valueOf(OCUPIED), R.string.tms_occupied);			//	占用
 		row.put(String.valueOf(RELEASED), R.string.tms_release);			//释放 也就是Free	
 		row.put(String.valueOf(NULL), R.string.sync_null);//null
	}
		
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getContainerTypeKeyValue(int id,Context c)
	{
		return getContainerTypeKeyValue(id+"",c);
	}
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getContainerTypeKeyValue(String id,Context c)
	{
		
			return c.getString(row.get(id));
		
	
	}
	

}
