package support.key;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LanguageKey {
	public final static int ENGLISH = 0;
	public final static int SIMPLIFIED_CHINESE = 1;

	private static Map<String, String> row;

	static {
		row = new HashMap<String, String>();
		row.put(String.valueOf(ENGLISH), "En");
		row.put(String.valueOf(SIMPLIFIED_CHINESE), "中文");
	}

	public static String getLanguageKey(int id) {
		return getLanguageKey(String.valueOf(id));
	}

	public static String getLanguageKey(String id) {
		return row.get(id);
	}

	public static int getLanguageID(Locale locale) {
		if (locale.getLanguage().equals(Locale.SIMPLIFIED_CHINESE.getLanguage())) {
			return 1;
		}
		return 0;
	}

	public static final String[] languages = { "English", "简体中文" };

}
