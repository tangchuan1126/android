package support.key;

public class FileWithCheckInClassKey {
	
	public final static int PhotoGateCheckIn = 1 ;    //Gate Check In   web页面
	public final static int PhotoDockCheckIn = 2 ;    //Dock Check In  手持设备
	public final static int PhotoWindowCheckIn = 3 ;  //Window Check In web页面
	public final static int PhotoGateCheckOut = 4;    //check out  both
	public final static int PhotoDockClose = 5 ;      //dock close 
	public final static int PhotoWindowCheckOut = 6 ; //window Check Out 不知道是什么 问徐佳
	public final static int PhotoLoad = 7 ;			//Load 的时候使用
	public final static int PhotoReceive = 8 ; 		//Receive 的时候使用
	public final static int PhotoWarehouseCheckIn  = 9 ; //Photo WarehouseCheckIn
	public final static int PhotoTaskProcessing = 10 ; 	//任务处理中
	public final static int PhotoCountingSheet = 11 ; 	//CountSheet
	public final static int PhotoTms = 12;//tms 图片

}
