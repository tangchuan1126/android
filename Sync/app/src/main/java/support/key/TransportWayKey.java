package support.key;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class TransportWayKey {
//	Map<String, String> fr_way;
	public static String OCEAN_SHIPPING="1";
	public static String LAND_SHIPPING="2";
	public static String AIR_SHIPPING="3";
	public static String EXPRESS_DELIVERY="4";
	
	public static Map<String, String> getFrWay(){
		 Map<String, String> fr_way_string;
		 fr_way_string=new HashMap<String, String>();
		 fr_way_string.put(TransportWayKey.OCEAN_SHIPPING, "海运");
		 fr_way_string.put(TransportWayKey.LAND_SHIPPING, "陆运");
		 fr_way_string.put(TransportWayKey.AIR_SHIPPING, "空运");
		 fr_way_string.put(TransportWayKey.EXPRESS_DELIVERY, "快递");
		 return fr_way_string;
	}
	public static String getFrWayId(String transportWay){
		Set ks=getFrWay().keySet(); 
		Iterator it=ks.iterator(); 
		while(it.hasNext()){ 
		String key=(String) it.next(); 
		Object getValue=getFrWay().get(key); 
		if(getValue.equals(transportWay)){
			return key;
			}
		}
		return "0";
		
	}
	
	public static String getFrWayName(String id){
		String reStr = "";
		if(null != id)
		{
			Set ks=getFrWay().keySet();
			Iterator it=ks.iterator(); 
			while(it.hasNext()){ 
				String key=(String) it.next(); 
				if(id.equals(key))
				{
					reStr = getFrWay().get(key);
					break;
				}
			}
			return reStr;
		}
		else
		{
			return "";
		}
	}
	
	
	
}
