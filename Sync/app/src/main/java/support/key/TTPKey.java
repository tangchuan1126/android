package support.key;

import utility.StringUtil;
import android.text.TextUtils;

public class TTPKey {

	public static final String KEY_DEFAULT = "Default";

	public static final String PREFIX_RECEIVE = "Receive_";
	public static final String PREFIX_RECEIVE_OSO = "ReceiveOSO_";
	public static final String PREFIX_RECEIVE_EXCEPTION = "RE_";
	public static final String PREFIX_RECEIVE_FINISH = "RF_";
	public static final String PREFIX_SAMSUNG_LOAD = "SL_";
	public static final String PREFIX_SAMSUNG_LOAD_EXCEPTION = "SLE_";
	public static final String PREFIX_SAMSUNG_LOAD_FINISH = "SLF_";
	public static final String PREFIX_LOAD = "Load_";
	public static final String PREFIX_LOAD_PRINT = "LP_";
	public static final String PREFIX_GATE_CHECKOUT_PHOTO = "GCP/GCP_";
	public static final String PREFIX_CREATE_PRODUCT = "Product_";

	private static boolean isEmpty(String str) {
		return str == null || TextUtils.isEmpty(str) || str.equals("0");
	}

	/**
	 * ------------------------ 收货 -----------------------------------------
	 */
	public static String getReceiveKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_RECEIVE + key + "_" + tabIndex;
	}

	@Deprecated
	public static String getReceiveKey_oSo(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_RECEIVE_OSO + key + "_" + tabIndex;
	}

	public static String getReceiveExceptionKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_RECEIVE_EXCEPTION + key + "_" + tabIndex;
	}

	public static String getReceiveFinishKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_RECEIVE_FINISH + key + "_" + tabIndex;
	}

	/**
	 * ------------------------ 三星Load -----------------------------------------
	 */
	public static String getSamsungLoadKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_SAMSUNG_LOAD + key + "_" + tabIndex;
	}

	public static String getSamsungLoadExceptionKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_SAMSUNG_LOAD_EXCEPTION + key + "_" + tabIndex;
	}

	public static String getSamsungLoadFinishKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_SAMSUNG_LOAD_FINISH + key + "_" + tabIndex;
	}

	/**
	 * ------------------------ Load -----------------------------------------
	 * 
	 */
	@Deprecated
	public static String getLoadKey(String detail_id, String master_bol_no, int tabIndex) {
		String did = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		String bol = isEmpty(master_bol_no) ? KEY_DEFAULT : master_bol_no;
		String key = did + "_" + bol;
		return PREFIX_LOAD + key + "_" + tabIndex;
	}

	@Deprecated
	public static String getLoadToPrintKey(String detail_id, String master_bol_no, int tabIndex) {
		String did = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		String bol = isEmpty(master_bol_no) ? KEY_DEFAULT : master_bol_no;
		String key = did + "_" + bol;
		return PREFIX_LOAD_PRINT + key + "_" + tabIndex;
	}

	@Deprecated
	public static String getLoadExpKey(String detail_id, String master_bol_no, int tabIndex) {
		String did = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		String bol = isEmpty(master_bol_no) ? KEY_DEFAULT : master_bol_no;
		String key = did + "_" + bol;
		return "LOAD_EXP_" + key + "_" + tabIndex;
	}

	/**
	 * 任务处理key
	 * 
	 * @param entry
	 * @return
	 */
	public static String getTaskProcessKey(String entry) {
		return "Process_" + entry;
	}

	/**
	 * Tms
	 * 
	 * @param bol
	 *            or loading
	 * @return
	 */
	public static String getTmsKey(String key) {
		return "Tms_" + key;
	}

	/**
	 * wms收货
	 * 
	 * @param dlo
	 * @param pltID
	 * @return
	 */
	public static String getRecPltKey(String receiptNO, String pltID) {
		return "Rec_" + receiptNO + "/" + pltID;
	}

	/**
	 * TmsDestory
	 * 
	 * @return
	 */
	public static String getTmsDestoryKey(String tnId) {
		return "TD_" + tnId;
	}

	/**
	 * cct收货
	 * 
	 * @param dlo
	 * @param pltID
	 * @return
	 */
	public static String getCCT_Key(String receiptNO, String pltID) {
		return "CCT_" + (StringUtil.isNullOfStr(receiptNO)?"":receiptNO) + "/" + pltID;
	}

	/**
	 * Tms
	 * 
	 * @param pallet
	 *            or tn
	 * @return
	 */
	public static String getTms_Key(String receiptNO) {
		return "Tms" + "/" + receiptNO;
	}

	/**
	 * 盲盘
	 * 
	 * @param sku
	 * @param containerId
	 * @return
	 */
	public static String getCCT_Blind_Key(String sku, String containerId) {
		if (TextUtils.isEmpty(sku))
			sku = "";
		if (TextUtils.isEmpty(containerId))
			containerId = "";
		return "CCT_Blind" + sku + "/" + containerId;
	}

	/**
	 * @param entry
	 * @param dlo
	 * @param customerID
	 *            mbol级的customerID
	 * @return
	 */
	public static String getCountSheetKey(String entry, String dlo, String customerID) {
		return "CSheet_" + entry + "/" + dlo + "_" + customerID;
	}

	// public static String getLoadExpKey(String detail_id, String
	// master_bol_no, int tabIndex) {
	// String did = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
	// String bol = isEmpty(master_bol_no) ? KEY_DEFAULT : master_bol_no;
	// String key = did + "_" + bol;
	// return "LoadExp" + key + "_" + tabIndex;
	// }

	/**
	 * ------------------------ Gate Checkout Photo
	 * -----------------------------------------
	 */
	public static String getGateCheckoutPhotoKey(String entry_id, int tabIndex) {
		String key = isEmpty(entry_id) ? KEY_DEFAULT : entry_id;
		return PREFIX_GATE_CHECKOUT_PHOTO + key + "_" + tabIndex;
	}

	// ============photo capture=====================================

	public static String Dir_PhotoCapture = "PhotoCapture";

	public static final String DirN_Cam = "Camera"; // 目录名
	public static final String DirN_Thumb = "Thumbnail";
	public static final String DirN_Upload = "Upload";

	/**
	 * photoCapture目录
	 * 
	 * @param entry_id
	 * @param tabIndex
	 * @return
	 */
	public static String getPhotoCaptureDir(String entry_id, int tabIndex) {
		return "PhotoCapture/" + entry_id + "_" + tabIndex;
	}

	/**
	 * photoCapture缓存目录,entry暂不确定,后续会改为getPhotoCaptureDir
	 * 
	 * @param tabIndex
	 * @return
	 */
	public static String getPhotoCapture_new(int tabIndex) {
		return "PhotoCapture/new_" + tabIndex;
	}

	// ==================load/receive-photo===============================

	public static String getPhotoLRDir(String entry_id, int tabIndex) {
		return "PhotoLR/" + entry_id + "_" + tabIndex;
	}

	public static String getPhotoLRDir_new(int tabIndex) {
		return "PhotoLR/new_" + tabIndex;
	}
	
	// ==================Create Product===============================
	public static String getCreateProductKey(String detail_id, int tabIndex) {
		String key = isEmpty(detail_id) ? KEY_DEFAULT : detail_id;
		return PREFIX_CREATE_PRODUCT + key + "_" + tabIndex;
	}

}
