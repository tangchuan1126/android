package support.key;

import java.util.HashMap;
import java.util.Map;

import utility.StringUtil;
import android.content.Context;
import android.util.Log;
import declare.com.vvme.R;

public class EntryDetailNumberStateKey {
	public static int NULL = 0;
	public static int Unprocess = 1;
	public static int Processing = 2;
	public static int Close = 3;
	public static int Exception = 4;
	public static int Partially = 5;

	// ========朱成==========================
	public static int TakeOver = -1;

	private static Map<Integer, Integer> values;
	static {
		values = new HashMap<Integer, Integer>();
		values.put(NULL, R.string.sync_null);
		values.put(Unprocess, R.string.qeuipment_type_unprocess);
		values.put(Processing, R.string.qeuipment_type_processing);
		values.put(Close, R.string.qeuipment_type_close);
		values.put(Exception, R.string.qeuipment_type_exception);
		values.put(Partially, R.string.qeuipment_type_partially);
	}

	public static String getType(Context c,int key) {
		if(values.containsKey(key)){
			return c.getString(values.get(key));
		}else{
			return null;
		}
		
	}

	public static String getType(Context c,String key) {
		return getType(c,StringUtil.convert2Inter(key));
	}

	public static boolean isClose(int key) {
		return key == Close || key == Exception || key == Partially;
	}

}
