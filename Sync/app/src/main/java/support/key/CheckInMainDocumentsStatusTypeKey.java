package support.key;

import java.util.HashMap;
import java.util.Map;

import support.common.UIHelper;
import android.content.Context;
import declare.com.vvme.R;


/**
 * 设备-状态,处理中/离开等
 * @author 朱成
 * @date 2014-12-24
 */
public class CheckInMainDocumentsStatusTypeKey
{
	private static Map<String, Integer> row;
	
	/**	public static int PICK_UP_UNPROCESS = 1;
	public static int PICK_UP_PROCESSING = 2;
	public static int PICK_UP_INYARD = 3;
	public static int PICK_UP_LEAVING = 4;
	public static int PICK_UP_LEFT = 5;
	public static int DELIVERY_UNPROCESS = 6;
	public static int DELIVERY_PROCESSING = 7;
	public static int DELIVERY_INYARD = 8;
	public static int DELIVERY_LEAVING = 9;
	public static int DELIVERY_LEFT = 10;
	public static int NONE = 11;
	public static int PICK_UP_CONTAINER_INYARD = 12;
	public static int DELIVERY_CONTAINER_INYARD = 13;
//	public static int TR_PICK_UP_LEFT = 14;
//	public static int TR_DELIVERY_LEFT = 15;*/
	public static int UNPROCESS = 1;
	public static int PROCESSING = 2;
	public static int INYARD = 3;
	public static int LEAVING = 4;
	public static int LEFT = 5;
	
	static
	{
		row = new HashMap<String, Integer>();
		
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_UNPROCESS), "取货未处理");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_PROCESSING), "取货处理中");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_INYARD), "取货完成  InYard");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEAVING), "取货完成  Leaving");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEFT), "取货完成  Left");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_UNPROCESS), "送货未处理");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_PROCESSING), "送货处理中");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_INYARD), "送货完成  InYard");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING), "送货完成  Leaving");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEFT), "送货完成   Left");
/**		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_UNPROCESS), "Pick up:Open ");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_PROCESSING), "Pick up:Processing");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_INYARD), "Pick up:Closed  InYard");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEAVING), "Pick up:Closed  Leaving");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEFT), "Pick up:Closed  Left");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.TR_PICK_UP_LEFT), "Pick up:Left");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_UNPROCESS), "Delivery:Open");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_PROCESSING), "Delivery:Processing");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_INYARD), "Delivery:Closed  InYard");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING), "Delivery:Closed  Leaving");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEFT), "Delivery:Closed  Left");
//		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.TR_DELIVERY_LEFT), "Delivery:Left");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.NONE), "");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_CONTAINER_INYARD), "Pick up:Trailer InYard");
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_CONTAINER_INYARD), "Delivery:Trailer InYard");*/
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.UNPROCESS), R.string.qeuipment_type_unprocess);
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.PROCESSING), R.string.qeuipment_type_processing);
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.INYARD), R.string.qeuipment_type_processed);
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.LEAVING), R.string.qeuipment_type_leaving);
		row.put(String.valueOf(CheckInMainDocumentsStatusTypeKey.LEFT), R.string.qeuipment_type_left);
		
	}

	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(Context  c,int id)
	{
		return getReturnStatusById(c,id+"");
	}
	
	
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(Context c,String id)
	{
		return c.getString(row.get(id));
	}
	
	/**
	 * @Description:判断设备是否离开
	 * @param @param status 设备状态key值
	 * @param @return true 离开   false 未离开
	 */
	public static boolean isEquipmentLeft(int status){
		return status == LEFT;
	}
	
	public static void ToashStr(){
		UIHelper.showToast("Equipment has left!");
	}
}

