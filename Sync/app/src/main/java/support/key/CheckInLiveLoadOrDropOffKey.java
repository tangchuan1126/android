package support.key;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import declare.com.vvme.R;

/**
 * 设备-任务类型,liveLoad/dropOff等
 * @author 朱成
 * @date 2014-12-24
 */
public class CheckInLiveLoadOrDropOffKey {
 	 
	

	public static int LIVE = 1;
	public static int DROP = 2;
	public static int SWAP = 3;
	public static int PICK_UP = 4;
	private static Map<String, Integer> row;
	
	static{
		row = new HashMap<String, Integer>();
		row.put(String.valueOf(CheckInLiveLoadOrDropOffKey.LIVE), R.string.qeuipment_tasktype_live);
		row.put(String.valueOf(CheckInLiveLoadOrDropOffKey.DROP), R.string.qeuipment_tasktype_drop);
		row.put(String.valueOf(CheckInLiveLoadOrDropOffKey.SWAP), R.string.qeuipment_tasktype_swap);
		row.put(String.valueOf(CheckInLiveLoadOrDropOffKey.PICK_UP), R.string.qeuipment_tasktype_pickup);
	}
	
	public static String getCheckInLiveLoadOrDropOffKey(Context c,int id)
	{
		return getCheckInLiveLoadOrDropOffKey(c,String.valueOf(id));
	}
	
	public static String getCheckInLiveLoadOrDropOffKey(Context c,String id)
	{
		return c.getString(row.get(id));
	}
}
