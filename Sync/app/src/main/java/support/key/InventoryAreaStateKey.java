package support.key;

import java.util.HashMap;
import java.util.Map;

public class InventoryAreaStateKey {
	//1.可以盘点 。2.正在审核中，不能进入盘点。 3. 盘点中  4. 盘点提交完成	(也就是审核中了)
	
	public static int INVENTORY = 1 ; //

	public static int WAITINGAPPROVE = 2 ; //
	
	public static int INVENTORYING = 3 ; // 
	
	public static Map<String, String> row;

	static{
		row = new HashMap<String, String>();
		row.put(String.valueOf(INVENTORY), "Inventory");
		row.put(String.valueOf(WAITINGAPPROVE), "Wait Approve");
 		row.put(String.valueOf(INVENTORYING), "InventoryIng");
	}
	private InventoryAreaStateKey() {}

	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(int id)
	{
		return getReturnStatusById(id+"");
	}
	
	
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(String id)
	{
		return String.valueOf(row.get(id));
	}
}
