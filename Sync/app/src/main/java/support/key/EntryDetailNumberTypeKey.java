package support.key;

import java.util.HashMap;
import java.util.Map;

public class EntryDetailNumberTypeKey {

	private static Map<String, String> row;
	 
	public static int CHECK_IN_LOAD = 10;    //load   ----
	public static int CHECK_IN_ORDER = 11;    //order ----
	public static int CHECK_IN_PONO = 17; //pono      ----
	public static int CHECK_IN_PICKUP_ORTHERS = 18;//pickup other

	public static int CHECK_IN_CTN = 12;    //ctnr    ----
	public static int CHECK_IN_BOL = 13; //bol        ---- 311502
	public static int CHECK_IN_DELIVERY_ORTHERS = 14;//delivery other          这三个为Load
	
	public static int TMS_BOL = 56; //装货
	public static int TMS_LOADDING = 57;//收货
	
	public static int TMS_AnNeng_LOAD = 66; //装货 主要用于安能
	public static int TMS_AnNeng_Receive = 67; //装货 主要用于安能

	
	public static int SMALL_PARCEL = 25;
	
//	public EntryDetailNumberTypeKey() {
	static{
		row = new HashMap<String, String>();
	 
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_LOAD), "LOAD");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_ORDER), "ORDER");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_CTN), "CTNR");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_BOL), "BOL");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_DELIVERY_ORTHERS), "Delivery");
		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_PICKUP_ORTHERS), "Pick Up");
  		row.put(String.valueOf(EntryDetailNumberTypeKey.CHECK_IN_PONO), "PO");
  		
  		
  		//-----------tms
		row.put(String.valueOf(EntryDetailNumberTypeKey.TMS_BOL), "Receiving");
  		row.put(String.valueOf(EntryDetailNumberTypeKey.TMS_LOADDING), "Loading");
  		
		row.put(String.valueOf(EntryDetailNumberTypeKey.TMS_AnNeng_Receive), "收货");
  		row.put(String.valueOf(EntryDetailNumberTypeKey.TMS_AnNeng_LOAD), "装货");
  		
  		row.put(String.valueOf(EntryDetailNumberTypeKey.SMALL_PARCEL), "Small Parcel");
	}
//	}
	

	public static String getModuleName(String id)
	{
		return(String.valueOf(row.get(id)));
	}
	
	public static String getModuleName(int id)
	{
		return getModuleName(String.valueOf(id));
	}
	
	
}
