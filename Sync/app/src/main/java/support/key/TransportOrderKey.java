package support.key;

import java.util.HashMap;
import java.util.Map;


public class TransportOrderKey
{
	Map<String, String> row;
	//转运单状态
	public static int READY = 1;	//备货中
	public static int INTRANSIT = 2;//正常运送
	public static int APPROVEING = 3;//审核中
	public static int FINISH = 4;//完成
	public static int PACKING = 5;//装箱中
	public static int NOFINISH = 7;//未完成
	public static int AlREADYARRIAL = 8;	//已到货
	public static int RECEIVEING = 9;	//收货中
	public static int AlREADYRECEIVE = 10;	//已收货
	public static int CANINSTORE = 11;		//条码机可下载
	
	//提货/收货
	public static int SEND = 1;
	public static int RECEIVE = 2;
	
	public TransportOrderKey() 
	{
		row = new HashMap<String, String>();
		row.put(String.valueOf(TransportOrderKey.READY), "备货中");
		row.put(String.valueOf(TransportOrderKey.PACKING), "装货中");
		row.put(String.valueOf(TransportOrderKey.INTRANSIT), "运输中");
		row.put(String.valueOf(TransportOrderKey.APPROVEING), "审核中");
		row.put(String.valueOf(TransportOrderKey.FINISH), "已入库");
		row.put(String.valueOf(TransportOrderKey.AlREADYARRIAL), "已到货");
		row.put(String.valueOf(TransportOrderKey.RECEIVEING), "收货中");
		row.put(String.valueOf(TransportOrderKey.AlREADYRECEIVE), "未完成");
		row.put(String.valueOf(TransportOrderKey.AlREADYRECEIVE),"已收货");
	}

}
