package support.key;

import java.util.HashMap;
import java.util.Map;

public class ProductStoreBillKey {
	Map<String, String> row;
	   // 拆分中
	public static int DELIVERY_ORDER = 1;	//交货单
	public static int TRANSPORT_ORDER = 2;	//转运单
	public static int WAYBILL_ORDER = 3;	//运单
	public static int RETURN_ORDER = 4;		//退货单
	public static int DAMAGED_REPAIR = 5;	//残损返修单
	public static int ORDER = 6;			//订单
	public static int STORAGE_APPROVE = 7;	//库存审核
	public static int PICK_UP_ORDER = 8;	//拣货单

	public ProductStoreBillKey() 
	{
		row = new HashMap<String, String>();
		row.put(String.valueOf(ProductStoreBillKey.DELIVERY_ORDER), "交货单");
		row.put(String.valueOf(ProductStoreBillKey.TRANSPORT_ORDER), "转运单");
		row.put(String.valueOf(ProductStoreBillKey.WAYBILL_ORDER), "运单");
		row.put(String.valueOf(ProductStoreBillKey.RETURN_ORDER), "退货单");
		row.put(String.valueOf(ProductStoreBillKey.DAMAGED_REPAIR), "残损返修单");
		row.put(String.valueOf(ProductStoreBillKey.ORDER),"订单");
		row.put(String.valueOf(ProductStoreBillKey.STORAGE_APPROVE),"库存审核单");
	}

}
