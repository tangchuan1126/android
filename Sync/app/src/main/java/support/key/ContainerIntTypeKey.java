package support.key;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import utility.StringUtil;

public class ContainerIntTypeKey {
	
	public static int Original = 0;
	public static int CLP = 1 ; //CLP   商品托盘
	public static int BLP = 2 ; //BLP	主箱子
	public static int TLP = 3 ; //TLP	临时托盘
	public static int ILP = 4;	//ILP	内箱
	private static Map<Integer, String> values ; 
	static{
		values = new HashMap<Integer, String>();
		values.put(CLP, "CLP");
		values.put(BLP, "BLP");
		values.put(TLP, "TLP");
		values.put(ILP, "ILP");
		values.put(Original, "Original");
	}
	
	public static String getType(int key){
		return values.get(key);
	}
	public static String getType(String key){
		return getType(StringUtil.convert2Inter(key));
	}
	public static int conventToInterValue(String value){
		Iterator<Entry<Integer, String>> it = values.entrySet().iterator() ;
		while(it.hasNext()){
			Entry<Integer, String> entry = it.next();
			if(entry.getValue().toUpperCase().equals(value.toUpperCase())){
				return entry.getKey() ;
			}
		}
		return 0 ;
	}
 }
