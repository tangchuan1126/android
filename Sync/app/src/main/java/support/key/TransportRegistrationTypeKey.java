package support.key;

import java.util.HashMap;
import java.util.Map;

public class TransportRegistrationTypeKey {

	Map<String, String> row;
	public static int SEND = 1;//装货司机签到
	public static int DELEIVER = 2;//缷货司机签到
	
	public TransportRegistrationTypeKey()
	{
		row = new HashMap<String, String>();
		row.put(String.valueOf(TransportRegistrationTypeKey.SEND), "装货司机签到");
		row.put(String.valueOf(TransportRegistrationTypeKey.DELEIVER), "缷货司机签到");
	}
	
}
