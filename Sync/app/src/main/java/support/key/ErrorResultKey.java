package support.key;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ErrorResultKey {
	
	public static int DATASIZEINCORRECTLY = 0;//数据大小有误
	public static int DATATYPEINCRRECTLY = 1;//数据格式错误
	public static int SYSTEMERROR = 2;//系统内部错误
	public static int LOGINERROR = 3;//登录失败,账号密码不匹配
	public static int OPERATIONNOTPERMIT = 7;//无权限操作
	public static int NOTEXITSPRODUCT = 10;//系统内无此商品 
	public static int NETWORKEXCEPTION = 11;//网络传输错误 
	
	//商品基础数据
	public static int ProductNameIsExist = 25; //商品名已经存在
	public static int ProductCodeIsExist = 26; //商品条码已经存在
	public static int ProductUnionCannotDoChild = 27; //已经设置了的组合套装不能做子商品
	public static int WithProductUnionNotRepeat = 28 ;//同一个组合套装下，商品不能重复
	public static int ProductUnionNoCreateProduct = 29;  //已经作为组合的商品，不能变成组合，在其下创建商品
	public static int ProductInUnionNotDel = 30; //商品有组合关系，不能删除
	public static int ProductHasRelationStorage = 31; //商品已建跟库存关系，不能删除
	
	
	/*
	 * 返回错误信息的key值和value值
	 */
	public static Map<Integer, String> getErrorResultKey(){
		Map<Integer, String> map=new HashMap<Integer, String>();
		map.put(ErrorResultKey.DATASIZEINCORRECTLY, "数据大小有误");
		map.put(ErrorResultKey.DATATYPEINCRRECTLY, "数据格式错误");
		map.put(ErrorResultKey.SYSTEMERROR, "系统内部错误");
		map.put(ErrorResultKey.LOGINERROR, "登录失败,账号密码不匹配");
		map.put(ErrorResultKey.OPERATIONNOTPERMIT, "无权限操作");
		map.put(ErrorResultKey.NETWORKEXCEPTION, "网络传输错误 ");
		map.put(ErrorResultKey.NOTEXITSPRODUCT, "此商品不存在");
		map.put(ErrorResultKey.ProductNameIsExist, "商品名已经存在");
		map.put(ErrorResultKey.ProductCodeIsExist, "该商品条码已存在");
		map.put(ErrorResultKey.ProductUnionCannotDoChild, "已经设置了的组合套装不能做子商品");
		map.put(ErrorResultKey.WithProductUnionNotRepeat, "同一个组合套装下，商品不能重复");
		map.put(ErrorResultKey.ProductUnionNoCreateProduct, "已经作为组合的商品，不能变成作为其他组合商品下的商品");
		map.put(ErrorResultKey.ProductInUnionNotDel, "商品有组合关系，不能删除");
		map.put(ErrorResultKey.ProductHasRelationStorage, "商品已建跟库存关系，不能删除");
		return map;
	}
	/*
	 * 通过value的值查找key的值
	 */
	public static int getKeyByValue(Map map,String value){
		Set ks=map.keySet(); 
		Iterator it=ks.iterator(); 
		while(it.hasNext()){ 
		int key=(Integer) it.next(); 
		Object getValue=map.get(key); 
		if(getValue.equals(value)){
			return key;
			}
		}
		return 0;
	}
	
	/*
	 * 通过key值得到名字
	 */
	public static String getValueByKey(Map map,int key){
		String value = (String)map.get(key);
		return value;
	}
}
