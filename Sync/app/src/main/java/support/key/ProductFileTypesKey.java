package support.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 商品上传图片文件类型
 * @author Administrator
 *
 */
public class ProductFileTypesKey {
	
	public static int packet	= 1 ; //商品包装
	public static int self		= 2 ; //商品本身
	public static int bottom	= 3 ; //商品底贴
	public static int weight	= 4 ; //商品称重
	public static int tags		= 5 ; //商品标签
	
	public static Map<Integer, String> getProductFileTypesKey(){
		Map<Integer, String> map=new HashMap<Integer, String>();
		map.put(ProductFileTypesKey.packet, "商品包装");
		map.put(ProductFileTypesKey.self, "商品本身");
		map.put(ProductFileTypesKey.bottom, "商品底贴");
		map.put(ProductFileTypesKey.weight, "商品称重");
		map.put(ProductFileTypesKey.tags, "商品标签");
		return map;
	}
	
	/*
	 * 获取文件类型的名字
	 */
	public static List<String> getFileTypeName(){
		List<String> typeName=new ArrayList<String>();
		Map<Integer, String> typeKey=getProductFileTypesKey();
		typeName.add(typeKey.get(ProductFileTypesKey.packet));
		typeName.add(typeKey.get(ProductFileTypesKey.self));
		typeName.add(typeKey.get(ProductFileTypesKey.bottom));
		typeName.add(typeKey.get(ProductFileTypesKey.weight));
		typeName.add(typeKey.get(ProductFileTypesKey.tags));
		return typeName;
	}
	/*
	 * 通过value的值查找key的值
	 */
	public static int getKeyByValue(Map map,String value){
		Set ks=map.keySet(); 
		Iterator it=ks.iterator(); 
		while(it.hasNext()){ 
		int key=(Integer) it.next(); 
		Object getValue=map.get(key); 
		if(getValue.equals(value)){
			return key;
			}
		}
		return 0;
	}
}
