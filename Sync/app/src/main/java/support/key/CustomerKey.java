package support.key;

import java.util.HashMap;
import java.util.Map;
/**
 * @ClassName: CustomerKey 
 * @Description: 用于判断来自于哪个顾客  主用于打印页面 PrintBillOfLoadingFragment
 * @author gcy
 * @date 2015-3-20 上午11:15:47
 */
public class CustomerKey {

	public static String VIZIO = "VIZIO";
	public static String VZB = "VZB";		
	public static String VIZIO3 = "VIZIO3";		
	public static String VZO = "VZO";	
	public static String VIZIO2 = "VIZIO2";	
	private static Map<String, String> values;
	static {
		values = new HashMap<String, String>();
		values.put(VIZIO, VIZIO);
		values.put(VZB, VZB);
		values.put(VIZIO3, VIZIO3);
		values.put(VZO, VZO);
 		values.put(VIZIO2, VIZIO2);
	}

	public static String getType(String key) {
		return values.get(key);
	}

	public static String getType(int key) {
		return getType(key+"");
	}

}
