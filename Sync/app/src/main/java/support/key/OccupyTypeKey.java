package support.key;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import declare.com.vvme.R;

/**
 * 位置-bean
 * @author 朱成
 * @date 2014-12-16
 */
public class OccupyTypeKey {

	public static int NULL = 0;
	public static int DOOR = 1;
	public static int SPOT = 2;
	
	//debug
	public static int BOTH=-1;
	
	final static Map<String, Integer> row = new HashMap<String, Integer>();
	
	static{
		
		row.put(String.valueOf(OccupyTypeKey.DOOR), R.string.shuttle_door_text);
		row.put(String.valueOf(OccupyTypeKey.SPOT), R.string.shuttle_spot_text);
		row.put(String.valueOf(OccupyTypeKey.NULL), R.string.sync_null);
	
	}
	

	public static String getOccupyTypeKeyName(Context c,String id)
	{
		return c.getString(row.get(id));
	}
	
	public static String getOccupyTypeKeyName(Context c,int id)
	{
		return getOccupyTypeKeyName(c,id+"");
	}
	
	public static String getPrompt(Context c,int resource_type)
	{
		return c.getString(R.string.sync_select_please)+" "+((resource_type==OccupyTypeKey.DOOR) ? c.getString(R.string.shuttle_door_text)+"!" : c.getString(R.string.shuttle_spot_text)+"!");
	}
	
	
}
