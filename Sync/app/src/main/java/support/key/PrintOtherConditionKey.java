package support.key;

import java.util.HashMap;
import java.util.Map;
/**
 * @ClassName: CustomerKey 
 * @Description: 用于判断来自于哪个顾客  主用于打印页面 PrintBillOfLoadingFragment
 * @author gcy
 * @date 2015-3-20 上午11:15:47
 */
public class PrintOtherConditionKey {

	//-----------------静态key 相对于打印页面弹出的单选条件
	public static final int T_By_Shipper=1;
	public static final int T_By_Driver=2;
	private static Map<String, String> trailer_loader;
	
	
	public static final int F_By_Shipper = 1;
	public static final int F_By_Driver_pallets_said_to_contain = 2;  
	public static final int F_By_Driver_Pieces = 3;
	private static Map<String, String> freight_counted;
	static {
		trailer_loader = new HashMap<String, String>();
		trailer_loader.put(T_By_Shipper+"", "By Shipper");
		trailer_loader.put(T_By_Driver+"", "By Driver");

		freight_counted = new HashMap<String, String>();
		freight_counted.put(F_By_Shipper+"", "By Shipper");
		freight_counted.put(F_By_Driver_pallets_said_to_contain+"", "By Driver/pallets said to contain");
		freight_counted.put(F_By_Driver_Pieces+"", "By Driver/Pieces");
	}

	public static String getTailerLoader(String key) {
		return trailer_loader.get(key);
	}

	public static String getTailerLoader(int key) {
		return getTailerLoader(key+"");
	}

	
	public static String getEightCounted(String key) {
		return freight_counted.get(key);
	}

	public static String getEightCounted(int key) {
		return getEightCounted(key+"");
	}
}
