package support.key;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import declare.com.vvme.R;


public class BCSKey
{

	
	
	
	/**
	 * ret == 1 表示成功
	 * ret == 0 表示服务器错误 然后err 的数字对应下面的错误
	 */
 	
	 
	public final static int FAIL = 0;	//操作失败
	public final static int SUCCESS = 1;	//操作成功
	
	public final static int Err_WithMsg = 90 ;	//出错,同时data为出错消息
	
  	public final static int SYSTEMERROR = 2;//系统内部错误
	public final static int LOGINERROR = 3;//登录失败,账号密码不匹配
	public final static int OPERATIONNOTPERMIT = 7;//无权限操作
	public final static int NOTEXITSPRODUCT = 10;//系统内无此商品 
	public final static int NETWORKEXCEPTION = 11;//网络传输错误 
	public final static int NOEXITSDELIVERYORDER = 12;//交货单不存在 
	public final static int SerialNumberRepeat = 20;	//序列号重复
	public final static int JSONERROR = 21 ;	//序列号解析出错
	public final static int ProductCodeIsExist = 26; //商品条码已经存在
	public final static int LPNotFound = 33 ;	//container 不存在
	public final static int SerchLoadingNotFoundException = 36 ;	//SerchLoadingNot	checkin Loading search没有找到

	public final static int ProductPictureDeleteFailed = 35 ;	//图片删除失败
	public final static int TitleNotFoundException = 37 ;	//TitleNotFoundException
	public final static int CheckInNotFound = 40 ;				//CHECKIN主单据没
	public final static int AppVersionException = 41 ;			//android 版本的更新问题
	public final static int AndroidPrintServerUnEable = 43 ; 	// android 打印服务器不可用
	public final static int NoPermiessionEntryIdException = 44 ; 	// 当前CheckInEntry的 没有权限
	public final static int PrintServerNoPrintException = 45 ; 	// 当前的PrintServer没有设置print
	public final static int NoRecordsException=46;
	public final static int LoadIsCloseException = 47;				//load is close 
	public final static int LoadNumberNotExistException = 48;				//load is close 
	public final static int DataFormateException = 49 ;				//数据格式错误
	public final static int DockCheckInFirstException = 50 ;		//Dock Check In First
	public final static int SumsangPalletNotFound = 51 ; 			//
	public final static int ContainerNotFoundException = 52 ;  		//Container not found
	public final static int CheckInEntryIsLeftException = 54 ;  		//Container not found
	public final static int DoorHasUsedException = 55 ;					//Door has 	RESERVERED,OCUPIED
	public final static int SpotHasUsedException = 56 ;					//Spot has 	RESERVERED,OCUPIED

	public final static int CheckTaskProcessingChangeTaskToDockException = 57 ;	 //
	public final static int EquipmentNotFindException = 58 ;			//EquipmentNotFindException			 
	public final static int DeleteFileException  = 59 ;					//删除文件失败
	public final static int AddLoadBarUseFoundException =60 ;				//添加Load的失败
	public final static int EquipmentHadOutException = 61 ;					//equipment is Left;
	public final static int DeletePalletNoFailedException = 62 ;					//DeletePalletNoFailedException
	public final static int EntryTaskHasFinishException = 63 ;				//EntryTaskHasFinishException
	public final static int EquipmentHasDecidedPickedException = 64;
	public final static int EquipmentOccupyResourcesException = 65;
	public final static int ResourceHasUsedException = 66;
	public final static int DoorNotFindException = 67;
	public final static int SpotNotFindException = 68;
	public final static int EquipmentInYardException = 69;
	public final static int CheckInTaskCanntDeleteException = 70;
	public final static int CheckInTaskRepeatToEntryException = 71;
	public final static int CheckInTaskRepeatToOthersException = 72;
	public final static int CheckInMustHasTasksException = 73;
	public final static int CheckInAllTasksClosedException = 74;
	public final static int CheckInMustNotHaveTasksException = 75;
	public final static int CheckInTaskHaveDeletedException = 76;
	public final static int EquipmentHasLoadedOrdersException = 77;
	public final static int CheckinTaskNotFoundException = 78;
	public final static int EquipmentSameToEntryException = 79;					//  Repeat Add Equipment
	public final static int UpdatePalletTypeFailedException = 80;					// Update Pallet Type Failed 
	public final static int TaskNoAssignWarehouseSupervisorException = 81;	    //TaskWareHouse 
	public final static int UserIsOnLineCantLoginException = 82;				// 当前登录的人已经在线
	public final static int AccountNotPermitLoginException = 83 ;				//登录没有权限
	public final static int OperationNotPermitException = 84 ;					//没有操作权限
	public final static int AccountNullpointException = 85 ; 	//传递的account 为空	  
	public final static int NoUserByaccountException = 86 ; 	//通过account没查到user信息
	public final static int GetAdidException = 87 ; 		 //未获取到adid
	public final static int PackagingTypeExist = 88;       //Packaging Type Exist


	final static Map<String, Integer> row = new HashMap<String, Integer>();
	static{

		row.put(String.valueOf(FAIL), R.string.bcs_key_fail);	//操作失败
		row.put(String.valueOf(SUCCESS), R.string.bcs_key_success);//
 		row.put(String.valueOf(SYSTEMERROR), R.string.bcs_key_system_error);	//系统内部错误
		row.put(String.valueOf(LOGINERROR), R.string.bcs_key_loginfail);//登录失败,账号密码不匹配
		row.put(String.valueOf(OPERATIONNOTPERMIT), R.string.bcs_key_operationnotpermit);
		row.put(String.valueOf(NOTEXITSPRODUCT), R.string.bcs_key_notexitsproduct);
		row.put(String.valueOf(NETWORKEXCEPTION), R.string.bcs_key_neterror);	//网络传输错误
		row.put(String.valueOf(NOEXITSDELIVERYORDER), R.string.bcs_key_noexitsdeliveryorder);
		row.put(String.valueOf(SerialNumberRepeat), R.string.bcs_key_serialnumberrepeat);
		row.put(String.valueOf(JSONERROR), R.string.bcs_key_jsonerrror);
		row.put(String.valueOf(ProductCodeIsExist), R.string.bcs_key_productcodeisexist);
		row.put(String.valueOf(LPNotFound), R.string.bcs_key_lpnotfound); //容器不存在
		row.put(String.valueOf(ProductPictureDeleteFailed), R.string.bcs_key_productpicturedeletefailed); //图片删除失败
		row.put(String.valueOf(SerchLoadingNotFoundException), R.string.bcs_key_serchloadingnotfoundexception);
		row.put(String.valueOf(TitleNotFoundException), R.string.bcs_key_titlenotfoundexception);
		row.put(String.valueOf(CheckInNotFound), R.string.bcs_key_checkinnotfound);
		row.put(String.valueOf(AppVersionException), R.string.bcs_key_appversionexception);
		row.put(String.valueOf(AndroidPrintServerUnEable), R.string.bcs_key_androidprintserveruneable);
		row.put(String.valueOf(NoPermiessionEntryIdException), R.string.bcs_key_nopermiessionentryidexception);
		row.put(String.valueOf(PrintServerNoPrintException), R.string.bcs_key_printservernoprintexception);
		row.put(String.valueOf(NoRecordsException), R.string.bcs_key_norecordsexception);
		row.put(String.valueOf(LoadIsCloseException), R.string.bcs_key_loadiscloseexception);
		row.put(String.valueOf(DataFormateException), R.string.bcs_key_dataformateexception);
		row.put(String.valueOf(DockCheckInFirstException), R.string.bcs_key_dockcheckinfirstexception);
		row.put(String.valueOf(SumsangPalletNotFound), R.string.bcs_key_sumsangpalletnotfound);
		row.put(String.valueOf(ContainerNotFoundException), R.string.bcs_key_containernotfoundexception);
		row.put(String.valueOf(CheckInEntryIsLeftException), R.string.bcs_key_checkinentryisleftexception);
		row.put(String.valueOf(DoorHasUsedException), R.string.bcs_key_doorhasusedexception);
		row.put(String.valueOf(SpotHasUsedException), R.string.bcs_key_spothasusedexception);
		
		row.put(String.valueOf(CheckTaskProcessingChangeTaskToDockException), R.string.bcs_key_checktaskprocessingchangetasktodockexception);
		row.put(String.valueOf(EquipmentNotFindException), R.string.bcs_key_equipmentnotfindexception);
		row.put(String.valueOf(DeleteFileException), R.string.bcs_key_deletefileexception);
		row.put(String.valueOf(AddLoadBarUseFoundException), R.string.bcs_key_addloadbarusefoundexception);
		row.put(String.valueOf(EquipmentHadOutException), R.string.bcs_key_equipmenthadoutexception);
		row.put(String.valueOf(DeletePalletNoFailedException), R.string.bcs_key_deletepalletnofailedexception);
		row.put(String.valueOf(EntryTaskHasFinishException), R.string.bcs_key_entrytaskhasfinishexception);
		row.put(String.valueOf(UpdatePalletTypeFailedException), R.string.bcs_key_updatepallettypefailedexception);


		row.put(String.valueOf(EquipmentInYardException), R.string.bcs_key_equipmentinyardexception);
		row.put(String.valueOf(CheckInTaskCanntDeleteException), R.string.bcs_key_checkintaskcanntdeleteexception);
		row.put(String.valueOf(CheckInTaskRepeatToEntryException), R.string.bcs_key_checkintaskrepeattoentryexception);
		row.put(String.valueOf(CheckInTaskRepeatToOthersException), R.string.bcs_key_checkintaskrepeattoothersexception);
		row.put(String.valueOf(CheckInMustHasTasksException), R.string.bcs_key_checkinmusthastasksexception);
		row.put(String.valueOf(CheckInAllTasksClosedException), R.string.bcs_key_checkinalltasksclosedexception);
		row.put(String.valueOf(CheckInMustNotHaveTasksException), R.string.bcs_key_checkinmustnothavetasksexception);
		row.put(String.valueOf(CheckInTaskHaveDeletedException), R.string.bcs_key_checkintaskhavedeletedexception);
		row.put(String.valueOf(EquipmentHasLoadedOrdersException), R.string.bcs_key_equipmenthasloadedordersexception);
		row.put(String.valueOf(CheckinTaskNotFoundException), R.string.bcs_key_checkintasknotfoundexception);
		row.put(String.valueOf(EquipmentSameToEntryException), R.string.bcs_key_equipmentsametoentryexception);
		row.put(String.valueOf(AccountNotPermitLoginException), R.string.bcs_key_accountnotpermitloginexception);
		row.put(String.valueOf(TaskNoAssignWarehouseSupervisorException), R.string.bcs_key_tasknoassignwarehousesupervisorexception);
		row.put(String.valueOf(OperationNotPermitException), R.string.bcs_key_operationnotpermitexception);
		
		row.put(String.valueOf(AccountNullpointException), R.string.bcs_key_accountnullpointexception);
		row.put(String.valueOf(NoUserByaccountException), R.string.bcs_key_nouserbyaccountexception);
		row.put(String.valueOf(GetAdidException), R.string.bcs_key_getadidexception);
		row.put(String.valueOf(PackagingTypeExist), R.string.bcs_key_packagingtypeexist);

		
	}

	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(Context c,int id)
	{
		return getReturnStatusById(c,id+"");
	}
	
	
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(Context c,String id)
	{
		return c.getString(row.get(id));
	}
	
}
