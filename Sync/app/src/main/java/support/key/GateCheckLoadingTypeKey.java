package support.key;

import java.util.HashMap;
import java.util.Map;

public class GateCheckLoadingTypeKey {

	Map<String, String> row;
	
	public static int LOAD = 1;
	public static int CTNR = 2;
	public static int BOL = 3;
	public static int OTHERS = 4;
	
	public GateCheckLoadingTypeKey() 
	{
		row = new HashMap<String, String>();
		row.put(String.valueOf(LOAD), "LOAD");
		row.put(String.valueOf(CTNR), "CTNR");
		row.put(String.valueOf(BOL), "BOL");
		row.put(String.valueOf(OTHERS), "OTHERS");
 
	}

	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public String getReturnStatusById(int id)
	{
		return getReturnStatusById(id+"");
	}
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public String getReturnStatusById(String id)
	{
		return String.valueOf(row.get(id));
	}

}
