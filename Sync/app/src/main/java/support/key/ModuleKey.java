package support.key;

import java.util.HashMap;
import java.util.Map;

/**
 * 系统中的moduleKey
 * 
 * @author Administrator
 * 
 */
public class ModuleKey {
	public final static int PURCHASE_ORDER = 1;// 采货单
	public final static int TRANSPORT_ORDER = 2;// 转货单
	public final static int SCHEDULE = 3; // 任务
	public final static int APPLY_MONEY = 4; // 资金
	public final static int APPLY_TRANSFER_MONEY = 5;// 转账
	public final static int REPAIR_ORDER = 6;// 返修单
	public final static int WORK_FLOW_REFUND = 7; // 申请退款
	public final static int WORK_FLOW_RETAIL_PRICE = 8; // 调整零售报价
	public final static int B2B_ORDER = 9;
	// public final static int CHECK_IN = 22 ; //Check in模块

	public static int CHECK_IN_LOAD = 10; // load(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_ORDER = 11; // order(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_CTN = 12; // ctnr(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_BOL = 13; // bol(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_DELIVERY_ORTHERS = 14; // delivery
														// other(这些是徐佳子单据的类型,和这个ModuleKey没有关系)

	public static int CHECK_IN_PONO = 17; // pono(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_PICKUP_ORTHERS = 18; // pickup
													// other(这些是徐佳子单据的类型,和这个ModuleKey没有关系)

	private static Map<String, String> row;

	static {
		row = new HashMap<String, String>();
		row.put(String.valueOf(ModuleKey.CHECK_IN_LOAD), "LOAD");
		row.put(String.valueOf(ModuleKey.CHECK_IN_ORDER), "ORDER");
		row.put(String.valueOf(ModuleKey.CHECK_IN_CTN), "CTNR");
		row.put(String.valueOf(ModuleKey.CHECK_IN_BOL), "BOL");
		row.put(String.valueOf(ModuleKey.CHECK_IN_DELIVERY_ORTHERS), "Delivery");
		row.put(String.valueOf(ModuleKey.CHECK_IN_PICKUP_ORTHERS), "Pick Up");
		row.put(String.valueOf(ModuleKey.CHECK_IN_PONO), "PO");
	}

	public static String getCheckInModuleKey(int id) {
		return getCheckInModuleKey(String.valueOf(id));
	}

	public static String getCheckInModuleKey(String id) {
		return row.get(id);
	}
}
