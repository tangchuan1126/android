package support.key;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.text.TextUtils;

import declare.com.vvme.R;

public class CheckInMainDocumentsRelTypeKey {
	public static final int SealType_In = 1;
	public static final int SealType_Out = 2;
	public static final int SealType_Both = 3;

	private static Map<String, Integer> row;
	public static int DELIVERY = 1;
	public static int PICK_UP = 2;
	public static int BOTH = 3;
	public static int NONE = 4;
	// public static int PICKUP_CTNR = 5;
	public static int CTNR = 5;
	public static int PATROL = 6;
	public static int VISITOR = 7;
	public static int SMALL_PARCEL = 8;

	// public CheckInMainDocumentsRelTypeKey()
	// {
	static {
		row = new HashMap<String, Integer>();
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.DELIVERY), R.string.entry_type_delivery);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.PICK_UP), R.string.entry_type_pick_up);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.BOTH), R.string.entry_type_both);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.NONE), R.string.entry_type_none);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.CTNR), R.string.entry_type_ctnr);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.PATROL), R.string.entry_type_patrol);
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.VISITOR), R.string.entry_type_visitor);

		// debug
		row.put(String.valueOf(CheckInMainDocumentsRelTypeKey.SMALL_PARCEL), R.string.entry_type_small_parcel);
	}

	// }

	public static String getContainerTypeKeyValue(Context c,int id) {
		if (row.get(String.valueOf(id)) == null) {
			return "NULL";
		}
		return c.getString(row.get(String.valueOf(id)));
	}

	public static String getContainerTypeKeyValue(Context c,String id) {
		return c.getString(row.get(id));
	}

	public static String getShowContainerTypeKeyValue(Context c,int id) {
		return ((id == 3) ? c.getString(row.get(String.valueOf(CheckInMainDocumentsRelTypeKey.DELIVERY))) : c.getString(row.get(String.valueOf(id))));
	}

	public static String getShowContainerTypeKeyValue(Context c,String id) {
		return (("3".equals(id)) ? c.getString(row.get(String.valueOf(CheckInMainDocumentsRelTypeKey.DELIVERY))) : c.getString(row.get(id)));
	}

	/**
	 * 操作类型-对应sealType
	 * 
	 * @param id
	 * @return 取SealType_x
	 */
	public static int getSealType(int id) {
		if (DELIVERY == id)
			return SealType_In;
		else if (PICK_UP == id)
			return SealType_Out;
		else
			return SealType_Both;
	}

	// public static boolean hasInSeal(int id){
	// return getSealType(id)!=SealType_Out;
	// }
	//
	// public static boolean hasOutSeal(int id){
	// return getSealType(id)!=SealType_In;
	// }

}
