package support.key;

import java.util.HashMap;
import java.util.Map;

public class LoadUnloadOccupancyTypeKey {
	
	Map<String, String> dbRow;
	public static int DOOR = 1;//门
	public static int LOCATION = 2;//位置
	
	public LoadUnloadOccupancyTypeKey()
	{
		dbRow = new HashMap<String, String>();
		dbRow.put(String.valueOf(LoadUnloadOccupancyTypeKey.DOOR), "门");
		dbRow.put(String.valueOf(LoadUnloadOccupancyTypeKey.LOCATION), "位置");
	}
	
}
