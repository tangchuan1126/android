package support.key;

/**
 * 系统中的moduleKey (用于文件保存)
 * @author Administrator
 *
 */
public class FileWithTypeKey {

	public static int PURCHASE_ORDER = 1;//采货单
	public static int TRANSPORT_ORDER = 2;//转货单
	public static int SCHEDULE = 3 ; //任务
	public static int APPLY_MONEY =4 ; //资金
	public static int APPLY_TRANSFER_MONEY = 5;//转账
	public static int REPAIR_ORDER = 6;//返修单
	public static int WORK_FLOW_REFUND = 7 ; // 申请退款
	public static int WORK_FLOW_RETAIL_PRICE = 8 ; //调整零售报价
	
	public static int PRODUCT_SELF_FILE = 31;//商品信息的图片
	
	public static int TRANSPORT_RECEIVE = 39 ; //transport 收货
	public static int TRANSPORT_OUTBOUND = 40 ; // transport 装货图片.
 
	public static int OCCUPANCY_MAIN = 52 ; // 司机签到上传照片.
	
	public static int TMSPHOTO_ITEM_RECEIVE = 57;//tms receive 图片
	public static int TMSPHOTO_ITEM_LOAD = 56;//tms load 图片
	
 }
