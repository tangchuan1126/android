package support.key;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.widget.ImageView;
import declare.com.vvme.R;

public class CheckInTractorOrTrailerTypeKey {

	public final static int TRACTOR = 1;
	public final static int TRAILER = 2;
	public static int TRACTOR_TRAILER = 3;
//	public static int ANOTHER_TRACTOR_TRAILER = 3;
	
	final static Map<String, Integer> row = new HashMap<String, Integer>();
	static
	{
		row.put(String.valueOf(CheckInTractorOrTrailerTypeKey.TRACTOR),   R.string.patrol_tractor_title);		
		row.put(String.valueOf(CheckInTractorOrTrailerTypeKey.TRAILER),  R.string.patrol_trailer_title);
		row.put(String.valueOf(CheckInTractorOrTrailerTypeKey.TRACTOR_TRAILER), R.string.patrol_t_title);
//		row.put(String.valueOf(CheckInTractorOrTrailerTypeKey.ANOTHER_TRACTOR_TRAILER), "Tractor+Another Trailer");
 
	}


	public static String getContainerTypeKeyValue(Context c,int id) {
		return getContainerTypeKeyValue(c,id+"");
	}

	public static String getContainerTypeKeyValue(Context c,String id) {
		return c.getString(row.get(id));
	}
	
	/**
	 * @Description:设置车头or车尾的图标
	 * @param @param type equipment type (int)
	 * @param @param view (需要显示图片的控件)
	 */
	public static void setImageViewBgd(int type,ImageView view){
		if (type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			view.setImageResource(R.drawable.ic_s_tractor_write);
		}
		if (type == CheckInTractorOrTrailerTypeKey.TRAILER) {
			view.setImageResource(R.drawable.ic_s_trailer_write);
		}
	}
	
	/**
	 * @Description:设置车头or车尾的图标
	 * @param @param type equipment type (int)
	 * @param @param view (需要显示图片的控件)
	 */
	public static void setImageViewBgdBig(int type,ImageView view){
		if (type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
			view.setImageResource(R.drawable.ic_tractor);
		}
		if (type == CheckInTractorOrTrailerTypeKey.TRAILER) {
			view.setImageResource(R.drawable.ic_trailer);
		}
	}
	
}
