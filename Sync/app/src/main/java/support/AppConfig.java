package support;

/**
 * app全局配置
 * 
 * @author 朱成
 * @date 2014-9-10
 */
public class AppConfig {

	// ==================调试配置===============================
//	 public static final boolean isDebug =true;					//	 * true:调试模式(发布时应为false)
//	 public static final boolean Debug_NotLogin_Openfire = true; // 可防止被踢(也不会踢别人),发布时应取false,debug
//	 public static boolean isDebug_Permission = false;   //权限,true(使用权限),发布时取true
//	 public static final boolean Debug_Receive=true; 	//收货调试,true:调试模式,发布时为false
//	 public static final boolean Debug_SipCall = true;  // 语音通话, true：调试模式, 发布时请置为false
//	 public static final boolean TMS_Task = true;		//TMS任务调试 不参与项目发布  o(︶︿︶)o GCY

	// =================发布模式===============================
 
	 public static final boolean isDebug = false;	//	 * true:调试模式(发布时应为false)
 	 public static final boolean Debug_NotLogin_Openfire = false;	//	 * 可防止被踢(也不会踢别人),发布时应取false,debug
 	 public static boolean isDebug_Permission = true;	//	 * 权限,true(使用权限),发布时取true
 	 public static final boolean TMS_Task = false;		//盘点任务调试 不参与项目发布  o(︶︿︶)o GCY
 	 public static final boolean Debug_Receive = false; // 收货调试,true:调试模式,发布时为false
 	 public static final boolean Debug_SipCall = false; // 语音通话调试模式, 开发中，发布时请置为false
}
