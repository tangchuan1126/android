package support.network;

import support.dbhelper.StoredData;
import utility.HttpUrlPath;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

public class NetConnection_YMS extends NetConnectionInterface {

	private static NetConnection_YMS _instantce;

	/**
	 * 获取网络请求单例
	 */
	public static NetConnection_YMS getThis() {
		if (_instantce == null)
			_instantce = new NetConnection_YMS();
		return _instantce;
	}

	public RequestHandle reqAcquireRNLineInfo(SyncJsonHandler handler, String companyID, String recNo, String detailID, String osoNote) {
		final RequestParams params = new RequestParams();
		params.add("company_id", companyID); // ABL
		params.add("receipt_no", recNo); // 4
		params.add("detail_id", detailID);
		initNetParams(HttpUrlPath.acquireRNLineInfo, params);
		return client.get(HttpUrlPath.acquireRNLineInfo, params, handler);
	}

	/**
	 * 收货时创建商品
	 *
	 * @param itemId
	 */
	public RequestHandle reqProduct_data(SyncJsonHandler handler, String companyId, String customerId, String itemId) {
		final String url = HttpUrlPath.basePath + "_receive/product/data";
		RequestParams params = new RequestParams();
		params.add("companyId", companyId);
		params.add("customerId", customerId);
		params.add("itemId", itemId);
		initNetParams(url, params);
		return client.get(url, params, handler);
	}

	/**
	 * 请求商品基础数据
	 */
	public RequestHandle reqProduct_context(SyncJsonHandler handler) {
		RequestParams params = new RequestParams();
		initNetParams(HttpUrlPath.product_context, params);
		return client.get(HttpUrlPath.product_context, params, handler);
	}

	/**
	 * 添加商品
	 */
	public RequestHandle reqProduct_add(SyncJsonHandler handler, String jsonStr) {
		RequestParams params = new RequestParams();
		params.add("json", jsonStr);
		params.add("ps_id", StoredData.getPs_id());
		initNetParams(HttpUrlPath.product_add, params);
		return client.post(HttpUrlPath.product_add, params, handler);
	}

	/**
	 * 商品回写数据
	 */
	public void reqProduct_write(String id) {
		RequestParams params = new RequestParams();
		params.put("id", id + "");
		initNetParams(HttpUrlPath.productIndexAction, params);
		client.get(HttpUrlPath.productIndexAction, params, NULL_HANDLER);
	}

	/**
	 * Patrol 重置
	 */
	public void reqResetPatrol(SyncJsonHandler handler) {
		RequestParams params = new RequestParams();
		params.add("Method", "againPatrolAll");
		initNetParams(HttpUrlPath.CheckInPatrol, params);
		client.get(HttpUrlPath.CheckInPatrol, params, handler);
	}

}
