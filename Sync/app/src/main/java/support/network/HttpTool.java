package support.network;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import support.dbhelper.Goable;
import utility.StringUtil;

public class HttpTool {

	public static final int GET = 1;
	public static final int POST = 2;
	public static final int POST_STREAM = 3;
	private static final String encode = "utf-8";
	private static String xmlPre = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data>";
	private static String xmlAft = "</data>";
	String loginName;
	String loginPwd;

	/**
	 * 提交简单的JSON格式的数据 返回JSON格式的数据
	 * 
	 * @param path
	 * @param method
	 * @param params
	 * @param headers
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject getJsonFromUrl(String path, int method, Map<String, String> params, Map<String, String> headers) throws JSONException {
		JSONObject json = new JSONObject();
		try {
			ArrayList<BasicNameValuePair> listParam = convertToBasicNameValuePair(params);
			ArrayList<BasicNameValuePair> listHears = convertToBasicNameValuePair(headers);
			HttpEntity entity = getEntity(path, listHears, listParam, method);
			if (entity != null) {
				String value = EntityUtils.toString(entity);
				return new JSONObject(value);
			}

		} catch (Exception e) {
			json.put("flag", "net_error");
		}
		return json;

	}

	public static JSONObject getJsonAndPostXml(String path, int method, String detail) throws Exception {
		JSONObject json = new JSONObject();
		HttpEntity entity = getEntity(path, detail, method);
		if (entity != null) {
			String value = EntityUtils.toString(entity);
			return new JSONObject(value);
		}
		return json;

	}

	public static String getTextFromUrl(String path, int method, Map<String, String> params, Map<String, String> headers) throws IOException {
		ArrayList<BasicNameValuePair> listParam = convertToBasicNameValuePair(params);
		ArrayList<BasicNameValuePair> listHears = convertToBasicNameValuePair(headers);
		HttpEntity entity = getEntity(path, listHears, listParam, method);
		if (entity != null) {
			return EntityUtils.toString(entity);
		}
		return "";
	}

	public static InputStream getStream(String path, int method, Map<String, String> params, Map<String, String> headers) throws IOException {
		InputStream in = null;
		ArrayList<BasicNameValuePair> listParam = convertToBasicNameValuePair(params);
		ArrayList<BasicNameValuePair> listHears = convertToBasicNameValuePair(headers);
		HttpEntity entity = getEntity(path, listHears, listParam, method);
		if (entity != null) {
			in = entity.getContent();
		}
		return in;
	}

	/**
	 * post XML ����
	 * 
	 * @param path
	 * @param method
	 * @param postString
	 * @return
	 * @throws Exception
	 */
	public static InputStream getStreamAndPostXML(String path, int method, String postString) throws Exception {
		InputStream in = null;

		HttpEntity entity = getEntity(path, postString, method);
		if (entity != null) {
			in = entity.getContent();
		}
		return in;
	}

	public static InputStream getStreamAndPostXML(String requestUrl, String postString) throws Exception {
		URL url = new URL(requestUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setConnectTimeout(10 * 1000);
		conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8"); // ????????
		conn.setRequestProperty("Charset", HTTP.UTF_8);
		conn.setRequestProperty("Content-Length", String.valueOf(postString.getBytes().length)); // ???????????
		// conn.getOutputStream().write(postString.getBytes());
		// //?????????????��????????
		conn.getOutputStream().write(postString.getBytes("UTF-8"));
		InputStream in = null;
		if (conn.getResponseCode() == 200) {
			in = conn.getInputStream();
		}
		return in;

	}

	public static String getPostXmlString() {
		StringBuffer postXml = new StringBuffer();
		postXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		postXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		postXml.append("<Password>").append(Goable.Password).append("</Password>");
		postXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		// postXml.append("<stroageTypeId>").append(typeId).append("</stroageTypeId>");
		return postXml.toString();
	}

	public static String getPostXmlString(String changeValue) {
		StringBuffer postXml = new StringBuffer();
		postXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		postXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		postXml.append("<Password>").append(Goable.Password).append("</Password>");
		postXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		postXml.append(changeValue);
		// postXml.append("<stroageTypeId>").append(typeId).append("</stroageTypeId>");
		return postXml.toString();
	}

	// ����Details ��detail��
	public static String getPostXmlString(String detail, String fc, Map<String, String> param) {
		String efficacy = StringUtil.HashBase64(detail.toString());
		StringBuffer postXml = new StringBuffer(xmlPre);
		postXml.append("<fc>").append(fc).append("</fc>");
		postXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		postXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		postXml.append("<Password>").append(Goable.Password).append("</Password>");
		// new readLogin().getLogin();
		// ReadLogin readLogin=new HttpTool().new ReadLogin();
		// readLogin.getLogin();

		if (param != null && param.size() > 0) {
			Set<Entry<String, String>> set = param.entrySet();
			Iterator<Entry<String, String>> it = set.iterator();
			while (it.hasNext()) {
				Entry<String, String> temp = it.next();
				String pre = "<" + temp.getKey() + ">";
				String aft = "</" + temp.getKey() + ">";
				postXml.append(pre).append(temp.getValue()).append(aft);
			}
		}
		postXml.append("<Detail><Details>").append(detail).append("</Details></Detail>");
		postXml.append("<Length>").append(detail.length()).append("</Length>");
		postXml.append("<Efficacy>").append(efficacy).append("</Efficacy>");
		postXml.append(xmlAft);
		return postXml.toString();
	}

	public static String noDetailPostXml(String detail, String fc, Map<String, String> param) {
		String efficacy = StringUtil.HashBase64(detail.toString());
		StringBuffer postXml = new StringBuffer(xmlPre);
		postXml.append("<fc>").append(fc).append("</fc>");
		postXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		postXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		postXml.append("<Password>").append(Goable.Password).append("</Password>");
		if (param != null && param.size() > 0) {
			Set<Entry<String, String>> set = param.entrySet();
			Iterator<Entry<String, String>> it = set.iterator();
			while (it.hasNext()) {
				Entry<String, String> temp = it.next();
				String pre = "<" + temp.getKey() + ">";
				String aft = "</" + temp.getKey() + ">";
				postXml.append(pre).append(temp.getValue()).append(aft);
			}
		}
		postXml.append("<Details>").append(detail).append("</Details>");
		postXml.append("<Length>").append(detail.length()).append("</Length>");
		postXml.append("<Efficacy>").append(efficacy).append("</Efficacy>");
		postXml.append(xmlAft);
		return postXml.toString();
	}

	public static String getPositionPostXmlString(String detail, String fc, Map<String, String> param) {
		detail = "<PutProduct>" + detail + "</PutProduct>";
		String efficacy = StringUtil.HashBase64(detail.toString());
		StringBuffer postXml = new StringBuffer(xmlPre);
		postXml.append("<fc>").append(fc).append("</fc>");
		postXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		postXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		postXml.append("<Password>").append(Goable.Password).append("</Password>");
		if (param != null && param.size() > 0) {
			Set<Entry<String, String>> set = param.entrySet();
			Iterator<Entry<String, String>> it = set.iterator();
			while (it.hasNext()) {
				Entry<String, String> temp = it.next();
				String pre = "<" + temp.getKey() + ">";
				String aft = "</" + temp.getKey() + ">";
				postXml.append(pre).append(temp.getValue()).append(aft);
			}
		}
		postXml.append("<Details>").append(detail).append("</Details>");
		postXml.append("<Length>").append(detail.length()).append("</Length>");
		postXml.append("<Efficacy>").append(efficacy).append("</Efficacy>");
		postXml.append(xmlAft);
		return postXml.toString();
	}

	public static byte[] getBytes(String path, ArrayList<BasicNameValuePair> headers, ArrayList<BasicNameValuePair> params, int method)
			throws IOException {
		byte[] bytes = null;
		HttpEntity entity = getEntity(path, headers, params, method);
		if (entity != null) {
			bytes = EntityUtils.toByteArray(entity);
		}
		return bytes;
	}

	private static ArrayList<BasicNameValuePair> convertToBasicNameValuePair(Map<String, String> params) {
		ArrayList<BasicNameValuePair> arrayList = null;
		if (params != null && params.size() > 0) {
			arrayList = new ArrayList<BasicNameValuePair>();
			for (Map.Entry<String, String> entry : params.entrySet()) {
				arrayList.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
		}
		return arrayList;

	}

	private static HttpEntity getEntity(String path, ArrayList<BasicNameValuePair> headers, ArrayList<BasicNameValuePair> params, int method)
			throws IOException {
		HttpEntity entity = null;
		// ����client
		DefaultHttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 15000);
		client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 15000);
		// ����request����
		HttpUriRequest request = null;
		switch (method) {
		case GET:
			StringBuilder sb = new StringBuilder(path);
			// ����в����򽫲�����Ͳ���ֵ���ӵ�path֮��
			if (params != null && !params.isEmpty()) {
				sb.append('?');
				for (BasicNameValuePair pair : params) {
					sb.append(pair.getName()).append('=').append(URLEncoder.encode(pair.getValue(), encode)).append('&');
				}
				sb.deleteCharAt(sb.length() - 1);
			}
			request = new HttpGet(sb.toString());
			break;
		case POST:
			request = new HttpPost(path);
			if (params != null && !params.isEmpty()) {

				UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params);
				((HttpPost) request).setEntity(ent);
			}
			break;

		}

		if (headers != null && !headers.isEmpty()) {
			for (BasicNameValuePair pair : headers) {
				request.setHeader(pair.getName(), pair.getValue());
			}
		}

		// ִ�����󣬻��response
		HttpResponse response = client.execute(request);
		// �ж�response״̬��
		int code = response.getStatusLine().getStatusCode();
		if (code == HttpStatus.SC_OK) {
			// �������ɹ��������Ӧʵ��
			entity = response.getEntity();
		}

		return entity;
	}

	private static HttpEntity getEntity(String path, String postString, int method) throws IOException {
		HttpEntity entity = null;
		DefaultHttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 15000);
		client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 15000);
		HttpUriRequest request = null;
		switch (method) {

		case POST:
			request = new HttpPost(path);
			if (postString != null && postString.length() > 0) {
				StringEntity stringEntity = new StringEntity(postString);

				stringEntity.setContentType("text/xml charset=utf-8");
				((HttpPost) request).setEntity(stringEntity);
			}
			break;

		}

		HttpResponse response = client.execute(request);
		int code = response.getStatusLine().getStatusCode();
		if (code == HttpStatus.SC_OK) {
			entity = response.getEntity();
		}

		return entity;
	}

	/**
	 * 
	 * @param actionUrl
	 * @param params
	 * @param files
	 * @return
	 * @throws IOException
	 */
	public static String postFiles(String actionUrl, Map<String, String> params, Map<String, File> files) throws IOException {
		StringBuilder sb2 = new StringBuilder();
		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = "UTF-8";

		URL uri = new URL(actionUrl);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setReadTimeout(15 * 1000);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", "UTF-8");

		// debug
		conn.setRequestProperty("Cookie", SimpleJSONUtil.getCookie());

		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA + ";boundary=" + BOUNDARY);

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			sb.append(PREFIX);
			sb.append(BOUNDARY);
			sb.append(LINEND);
			sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEND);
			sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
			sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
			sb.append(LINEND);
			sb.append(entry.getValue());
			sb.append(LINEND);
		}

		DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
		outStream.write(sb.toString().getBytes());
		InputStream in = null;
		// �����ļ����
		if (files != null) {
			for (Map.Entry<String, File> file : files.entrySet()) {
				StringBuilder sb1 = new StringBuilder();
				sb1.append(PREFIX);
				sb1.append(BOUNDARY);
				sb1.append(LINEND);
				sb1.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getKey() + "\"" + LINEND);
				sb1.append("Content-Type: application/octet-stream; charset=" + CHARSET + LINEND);
				sb1.append(LINEND);
				outStream.write(sb1.toString().getBytes());

				InputStream is = new FileInputStream(file.getValue());
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}

				is.close();
				outStream.write(LINEND.getBytes());
			}

			// ��������־
			byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
			outStream.write(end_data);
			outStream.flush();
			// �õ���Ӧ��
			int res = conn.getResponseCode();
			if (res == 200) {
				in = conn.getInputStream();
				int ch;
				while ((ch = in.read()) != -1) {
					sb2.append((char) ch);
				}
			}
			outStream.close();
			conn.disconnect();
			if (in != null)
				in.close();
		}
		return sb2.toString();

	}

	public static String getLoginXml() {
		StringBuffer loginXml = new StringBuffer();

		loginXml.append(xmlPre);

		loginXml.append("<LoginAccount>").append(Goable.LoginAccount).append("</LoginAccount>");
		loginXml.append("<Password>").append(Goable.Password).append("</Password>");
		loginXml.append("<Machine>").append(Goable.Machine).append("</Machine>");
		loginXml.append(xmlAft);

		return loginXml.toString();
	}

}
