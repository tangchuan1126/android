package support.network;

import android.text.TextUtils;

import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import oso.ui.inventory.cyclecount.key.Cycle_Count_Task_Key;
import oso.ui.inventory.cyclecount.model.CCT_AreasBean;
import oso.ui.inventory.cyclecount.model.CCT_LocationBean;
import oso.ui.inventory.cyclecount.model.Cycle_Count_Tasks_Bean;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.dbhelper.StoredData;
import utility.HttpUrlPath;
import utility.StringUtil;
import utility.Utility;

/**
 * @author jiang
 * @date 2015-05-20 18:37:46
 */
public class NetConnection_CCT extends NetConnectionInterface {

    private static NetConnection_CCT _instantce;

    /**
     * 获取网络请求单例
     */
    public static NetConnection_CCT getThis() {
        if (_instantce == null)
            _instantce = new NetConnection_CCT();
        return _instantce;
    }

//    public String formatCCT_Url(String method) {
//        return String.format(HttpUrlPath.basePath + "_inventoryControl/%s", method);
//    }
//
    /**
     * 拼接获取创建LP的基础信息的Url
     * @param product_id  商品的ID
     * @return
     */
    public String formatCCT_GetCreateLP_DataUrl(String product_id){
        return String.format(HttpUrlPath.CCT_GetCreateLpDataInfo + "%s", product_id);
    }


    /**
     *  根据当前登录人获取当前登录人的任务列表
     *  应用类 Cycle_Count_Tasks
     *  请求方式get
     */
    public RequestHandle CCT_TaskInstanceListByUser(SyncJsonHandler handler) {
        RequestParams p = new RequestParams();
        p.add("user_id", StoredData.getAdid());
        initNetParams("",p);
        return client.get(HttpUrlPath.TaskInstanceListByUser, p, handler);
    }

    /**
     *  根据任务获取任务下的区域列表
     *  应用类 CCT_SlcArea
     *  请求方式get
     */
    public RequestHandle CCT_TaskInstanceAreas(SyncJsonHandler handler,Cycle_Count_Tasks_Bean tBean) {
        RequestParams p = new RequestParams();
        p.add("task_id", tBean.ID + "");
        p.add("task_type", tBean.TYPE + "");
        p.add("user_id", StoredData.getAdid());
        initNetParams("",p);
        return client.get(HttpUrlPath.TaskInstanceAreas, p, handler);
    }


    /**
     * 进入盘点任务后 的开始按钮 的开始任务事件
     * 应用类 CCT_SlcArea/CCT_SlcLocation
     * 请求方式post
     */
    public RequestHandle CCT_StartTask(SyncJsonHandler handler,Cycle_Count_Tasks_Bean tBean) {
        RequestParams p = new RequestParams();
        p.put("task_id", tBean.ID);
        p.put("task_type", tBean.TYPE);
        p.put("user_id", StoredData.getAdid());
        initNetParams("",p);
        return client.post(HttpUrlPath.StartTask, p, handler);
    }

    /**
     * 根据区域获取区域下的位置列表
     * 应用类 CCT_SlcLocation
     * 请求方式get
     */
    public RequestHandle CCT_TaskInstanceAreaLocations(SyncJsonHandler handler,Cycle_Count_Tasks_Bean tBean,CCT_AreasBean area) {
        RequestParams p = new RequestParams();
        p.add("task_id", tBean.ID+"");
        p.add("task_type", tBean.TYPE+"");
        p.add("area_id", area.AREA_ID + "");
        p.add("area_type", area.AREA_TYPE + "");
        p.add("user_id", StoredData.getAdid());
        initNetParams("",p);
        return client.get(HttpUrlPath.TaskInstanceAreaLocations, p, handler);
    }

    /**
     * 获取位置上的数据
     * 应用类 CCT_SlcLocation
     * 请求方式get
     */
    public RequestHandle CCT_LocationData(SyncJsonHandler handler,Cycle_Count_Tasks_Bean tBean,CCT_LocationBean t) {
        RequestParams p = new RequestParams();
        p.add("task_id", tBean.ID+"");
        p.add("task_type", tBean.TYPE+"");
        p.put("slc_id", t.LOCATION_ID);
        p.put("slc_type", t.LOCATION_TYPE);
        p.add("user_id", StoredData.getAdid());
        initNetParams("", p);
        return client.get(HttpUrlPath.LocationData, p, handler);
    }

    /**
     * 通过code来获取tlp或clp
     * 应用类 CCT_SlcLocation
     * 请求方式get
     */
    public RequestHandle CCT_GetTLP_Or_CLP_ByCode(SyncJsonHandler handler,String code) {
       return CCT_GetTLP_Or_CLP_ByCode(handler, code, "");
    }

    /**
     * 通过code来获取tlp或clp
     * 应用类 CCT_SlcLocation
     * 请求方式get
     */
    public RequestHandle CCT_GetTLP_Or_CLP_ByCode(SyncJsonHandler handler,String code,String title) {
        RequestParams p = new RequestParams();
        p.put("barcode", code.toUpperCase(Locale.ENGLISH));
        if(!TextUtils.isEmpty(title)){
            p.put("title_id", title);
        }

        // xia title id 不存在 则默认为0
        else {
            p.put("title_id", 0);
        }
        p.put("ps_id", Long.valueOf(StoredData.getPs_id()));
        // xia end

        initNetParams("", p);
        return client.get(HttpUrlPath.CCT_blindRequestCLP, p, handler);
    }

    /**
     * 创建包装类型
     */
    public RequestHandle CCT_GetPackaging_CreateInfo(SyncJsonHandler handler) {
        RequestParams p = new RequestParams();
        initNetParams("",p);
        return client.get(HttpUrlPath.CCT_CreatePackaging, p, handler);
    }

    /**
     * 获取创建CLP的基础信息
     * @param handler
     * @param product_id 商品ID
     * @return
     */
    public RequestHandle CCT_GetCreateCLPInfo(SyncJsonHandler handler, String product_id) {
        RequestParams p = new RequestParams();
        initNetParams("",p);
        return client.get(formatCCT_GetCreateLP_DataUrl(product_id), p, handler);
    }

    /**
     * 创建LP type
     */
    public RequestHandle CCT_Addtlpcontext(SyncJsonHandler handler,String json) {
        RequestParams p = new RequestParams();
        p.add("json",json);
        initNetParams("", p);
        return client.post(HttpUrlPath.CCT_AddLP_Type, p, handler);
    }

    /**
     * 获取创建LP的基础信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetCreateLPInfo(SyncJsonHandler handler, String product_id) {
        RequestParams p = new RequestParams();
        p.add("pc_id",product_id);
        p.add("at_ps_id",StoredData.getPs_id());
        initNetParams("",p);
        return client.get(HttpUrlPath.CCT_CLP_Context, p, handler);
    }

    /**
     * 获取创建LP的基础信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_CLP_TypeAdd(SyncJsonHandler handler, String json) {
        RequestParams p = new RequestParams();
        p.add("json", json);
        initNetParams("",p);
        return client.post(HttpUrlPath.CCT_CLP_TypeAdd, p, handler);
    }

    /**
     * 获取创建LP的基础信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetCreateTLPInfo(SyncJsonHandler handler) {
        RequestParams p = new RequestParams();
        initNetParams("",p);
        return client.get(HttpUrlPath.CCT_TLP_Context, p, handler);
    }


    //------------------------------------------------
    /** 由于搜索产品比较特殊,有很多种情况
     *  1.由于产品在两个库表中存储 有的时候另外一张库表 并没有数据
     *  所以在请求的时候 如果把lp_id传过去的话后台自己就会在没有创建商品的库表中创建数据并进行关联
     *  2.若果创建CLP的时候 该商品没有title_id这样会很麻烦 所以再创建CLP查到商品数据的时候如果没有返回title
     *  那么就需要人为去创建 所以这时候就会传相应的title_id
     *  3.如果在tlp中创建CLP 因为本身tlp就有相应的title所以这时候就会进行相关联╮(╯_╰)╭。
     * /
    /**
     * 通过product code,tlp_id获取商品信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetProductByCode(SyncJsonHandler handler,String sku,String title_id) {
        return CCT_GetProductByCode(handler, sku, 0, title_id);
    }

    /**
     * 通过product code,tlp_id获取商品信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetProductByCode(SyncJsonHandler handler,String sku) {
        return CCT_GetProductByCode(handler, sku, 0, null);
    }

    /**
     * 通过product code,tlp_id获取商品信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetProductByCode(SyncJsonHandler handler,String sku,long lp_id) {
        return CCT_GetProductByCode(handler, sku, lp_id, null);
    }

    /**
     * 通过product code,tlp_id获取商品信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetProductByCode(SyncJsonHandler handler,String sku,long lp_id,String title_id) {
        RequestParams p = new RequestParams();
        p.add("sku", sku.toUpperCase(Locale.ENGLISH));

        p.put("lp_id", lp_id != 0 ? lp_id : 0);
        p.put("title_id", !TextUtils.isEmpty(title_id)?title_id:0);

        p.put("ps_id", StoredData.getPs_id());
        initNetParams("", p);
        return client.get(HttpUrlPath.CCT_RequestSKU, p, handler);
    }

    /**
     * 校验SN的
     * @param handler
     * @return
     */
    public RequestHandle CCT_CheckSnList(SyncJsonHandler handler,long pc_id,String sn_list) {
        RequestParams p = new RequestParams();
        p.put("pc_id", pc_id);
        p.put("serial_number_str", sn_list);
        initNetParams("", p);
        return client.get(HttpUrlPath.CCT_CheckSnList, p, handler);
    }

    //------------------------------------------------


    /**
     * 通过SN获取商品信息
     * @param handler
     * @return
     */
    public RequestHandle CCT_GetProductBySN(SyncJsonHandler handler,String sn) {
        RequestParams p = new RequestParams();
        p.add("sn",sn.toUpperCase(Locale.ENGLISH));
        initNetParams("", p);
        return client.get(HttpUrlPath.CCT_blindRequestProductBySn, p, handler);
    }


    /**
     * 用于处理请求失败的一般性错误
     * @param json
     */
    public void handFail(JSONObject json){
        if(json!=null){
            JSONArray jArray = json.optJSONArray("errors");
            if(!StringUtil.isNullForJSONArray(jArray)){
                JSONObject jObj = jArray.optJSONObject(0);
                //errorCode
                UIHelper.showToast(jObj.optString("errorMessage"));
            }
        }
    }


    /**
     * 打印LP
     * @param printServerID
     * @param ids
     * @return
     */
    public RequestHandle recPrintLP_p(SyncJsonHandler handler,long printServerID, int lp_type, int type_id, String ids) {
        RequestParams p = new RequestParams();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("printer", printServerID);
            jsonObject.put("con_id","3016606");
            jsonObject.put("lp_type",Cycle_Count_Task_Key.CCT_CLP);
            if(lp_type== Cycle_Count_Task_Key.CCT_CLP){
                jsonObject.put("type_id", "181");
                jsonObject.put("pc_id", "1001747");
            }

            jsonObject.put("loginaccount", Goable.LoginAccount);
            jsonObject.put("password", Goable.Password);
            jsonObject.put("machine", Goable.Machine);
            jsonObject.put("version", Goable.Version);
            //debug
            jsonObject.put("adid", StoredData.getAdid());
            //debug
            jsonObject.put("mac", Utility.getMac());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        p.add("json", jsonObject.toString());
        initNetParams( "",p);
        return client.post(recPrintLP_url(), p, handler);
    }

    public String recPrintLP_url() {
        return HttpUrlPath.basePath + "basicdata/basicLabelPrint";
    }

    /**
     * 获取创建LP的基础信息
     * @param handler
     * @return
     */
    public RequestHandle getPrintServerList(SyncJsonHandler handler) {
        RequestParams p = new RequestParams();
        p.add("Method", "GetPrintServer");
        initNetParams("",p);
        return client.get(HttpUrlPath.AndroidPrintByWebAction, p, handler);
    }

}
