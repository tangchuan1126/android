package support.network;

import support.common.print.PrintTool;
import support.common.print.PrintTool.OnPrintLs;
import utility.HttpUrlPath;
import android.content.Context;

import com.loopj.android.http.RequestParams;

/**
 * @author jiang
 * 
 * @date 2015-05-20 18:37:46
 */
public class NetConnection_Printer extends NetConnectionInterface {

	private static NetConnection_Printer _instantce;

	/**
	 * 获取网络请求单例
	 */
	public static NetConnection_Printer getThis() {
		if (_instantce == null)
			_instantce = new NetConnection_Printer();
		return _instantce;
	}

	/**
	 * TMS 打印小签, 已弃用
	 * 
	 * @return
	 */
	@Deprecated
	public void reqTmsPrintNUM(final SyncJsonHandler handler, final String palletIds, Context c) {
		new PrintTool(c).showDlg_printers(new OnPrintLs() {
			@Override
			public void onPrint(long printServerID) {
				final String url = HttpUrlPath.basePath + "_tms/sorting/pallet/printNUM";
				RequestParams params = new RequestParams();
				params.add("printServerId", printServerID + "");
				params.add("palletIds", palletIds);
				initNetParams(url, params);
				client.get(url, params, handler);
			}
		});
	}

	/**
	 * TMS 打印大签
	 * 
	 * @return
	 */
	public void reqTmsPrintLabel(final SyncJsonHandler handler, final String palletIds, Context c) {
		new PrintTool(c).showDlg_printers(new OnPrintLs() {
			@Override
			public void onPrint(long printServerID) {
				final String url = HttpUrlPath.basePath + "_tms/sorting/printLable";
				RequestParams params = new RequestParams();
				params.add("printServerId", printServerID + "");
				params.add("palletIds", palletIds);
				initNetParams(url, params);
				client.get(url, params, handler);
			}
		});
	}

}
