package support.network;

import support.dbhelper.StoredData;
import utility.HttpUrlPath;

import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

/**
 * @author jiang
 * @date 2015-05-20 18:37:46
 */
public class NetConnection_TMS extends NetConnectionInterface {

    private static NetConnection_TMS _instantce;

    /**
     * 获取网络请求单例
     */
    public static NetConnection_TMS getThis() {
        if (_instantce == null)
            _instantce = new NetConnection_TMS();
        return _instantce;
    }

    public String formatTmsSortingUrl(String method) {
        return String.format(HttpUrlPath.basePath + "_tms/sorting/%s", method);
    }

    /**
     * 获取TN推荐托盘, 已弃用
     */
    @Deprecated
    public RequestHandle reqTMS_getPalletListOrderByItemId(
            SyncJsonHandler handler, String itemId, int pageNo, int pageSize) {
        RequestParams params = new RequestParams();
        params.add("itemId", itemId);
        params.add("pageNo", pageNo + "");
        params.add("pageSize", pageSize + "");
        initNetParams(HttpUrlPath.TMS_getPalletListOrderByItemId, params);
        return client.get(HttpUrlPath.TMS_getPalletListOrderByItemId, params,
                handler);
    }

    /**
     * 扫描tn, 返回wayBill
     *
     * @param no i
     */
    public RequestHandle reqScanNo(SyncJsonHandler handler, String no) {
        final String url = formatTmsSortingUrl("scanNo");
        RequestParams params = new RequestParams();
        params.add("no", no);
        params.add("ps_id", StoredData.getPs_id());
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 批量添加tn到托盘上
     *
     * @param palletId
     * @param itemIds  里面tn之间用“，”隔开
     */
    public RequestHandle reqCreateRelation(SyncJsonHandler handler,
                                           String palletId, String itemIds) {
        final String url = formatTmsSortingUrl("createRelation");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        params.add("itemIds", itemIds);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 将tn从托盘上拿下
     *
     * @param palletId
     * @param itemId
     */
    public RequestHandle reqDeleteRelation(SyncJsonHandler handler,
                                           String palletId, String itemId) {
        final String url = formatTmsSortingUrl("deleteRelation");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        params.add("itemId", itemId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 将一个托盘上的货物放到另一个托盘上
     *
     * @param fromPalletId
     * @param goalPalletId
     */
    public RequestHandle reqRemovePackges2Other(SyncJsonHandler handler,
                                                String fromPalletId, String goalPalletId) {
        final String url = formatTmsSortingUrl("removePackges2Other");
        RequestParams params = new RequestParams();
        params.add("fromPalletId", fromPalletId);
        params.add("goalPalletId", goalPalletId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 异常接口
     *
     * @param palletId
     */
    public RequestHandle reqPalletException(SyncJsonHandler handler,
                                            String palletId) {
        final String url = formatTmsSortingUrl("palletException");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 更新托盘状态
     *
     * @param palletId
     * @param status
     */
    public RequestHandle reqUpdatePalletStatus(SyncJsonHandler handler,
                                               String palletId, int status) {
        final String url = formatTmsSortingUrl("updatePalletStatus");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        params.add("status", status + "");
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 关闭托盘
     *
     * @param palletId
     */
    public RequestHandle reqClosePallet(SyncJsonHandler handler, String palletId) {
        final String url = formatTmsSortingUrl("closePallet");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 打开托盘
     *
     * @param palletId
     */
    public RequestHandle reqOpenPallet(SyncJsonHandler handler, String palletId) {
        final String url = formatTmsSortingUrl("openPallet");
        RequestParams params = new RequestParams();
        params.add("palletId", palletId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 创建托盘
     */
    public RequestHandle reqxx(SyncJsonHandler handler) {
        RequestParams params = new RequestParams();
        params.add("", "");
        initNetParams("", params);
        return client.get(formatTmsSortingUrl(""), params, handler);
    }

    /**
     * 更新托盘
     */
    public RequestHandle reqUpdateItemStatus(SyncJsonHandler handler,
                                             String itemId, int status) {
        final String url = formatTmsSortingUrl("updateItemStatus");
        RequestParams params = new RequestParams();
        params.add("itemId", itemId);
        params.add("status", status + "");
        initNetParams(url, params);
        return client.get(url, params, handler);
    }

    /**
     * 根据tnno获取order信息
     */
    public RequestHandle reqOrderByTn(SyncJsonHandler handler,
                                      String itemId, String shipto_id, String entryId) {
        RequestParams p = new RequestParams();
        p.add("ps_id", StoredData.getPs_id());
        p.add("itemId", itemId);
        p.add("shipto_id", shipto_id);
        p.add("entryId", entryId);
        p.add("language_type", StoredData.readLanguageID() + "");
        initNetParams(HttpUrlPath.GetOrderByTN, p);
        return client.get(HttpUrlPath.GetOrderByTN, p, handler);
    }

    /**
     * 根据order批量装载tn
     */
    public RequestHandle TMSLoadtn(SyncJsonHandler handler, String no,
                                   String waybill_id, String shipto_id, String entryId, String isCloseTask, String isReflash, String loading_id) {
        RequestParams p = new RequestParams();
        p.add("itemIds", no);
        p.add("ps_id", StoredData.getPs_id());
        p.add("waybill_ids", waybill_id);
        p.add("loading_id", loading_id);
//		p.add("shipto_id", shipto_id);
        p.add("language_type", StoredData.readLanguageID() + "");
        p.add("entryId", entryId);
        p.add("isReflash", isReflash);
        p.add("isCloseTask", isCloseTask);
        initNetParams(HttpUrlPath.TMSLoadtn, p);
        return client.get(HttpUrlPath.TMSLoadtn, p, handler);
    }

    /**
     * 卸载tn
     */
    public RequestHandle TMSDeleLoadTn(SyncJsonHandler handler, String no,
                                       String shipto_id, String entryId) {
        RequestParams p = new RequestParams();
        p.add("itemIds", no);
        p.add("ps_id", StoredData.getPs_id());
        // p.add("waybill_id", listOrders.get(listOrders.size()-1).WAYBILL_ID);
        p.add("shipto_id", shipto_id);
        p.add("language_type", StoredData.readLanguageID() + "");
        p.add("entryId", entryId);
        initNetParams(HttpUrlPath.TMSDeleLoadTn, p);
        return client.get(HttpUrlPath.TMSDeleLoadTn, p, handler);
    }


}
