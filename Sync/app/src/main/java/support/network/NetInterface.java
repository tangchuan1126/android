package support.network;

import android.text.TextUtils;

import com.loopj.android.http.RequestParams;

import java.io.File;

import oso.ui.load_receive.do_task.receive.wms.bean.Rec_PalletBean;
import oso.ui.load_receive.do_task.receive.wms.util.LocBean;
import oso.widget.location.bean.LocationBean;
import oso.widget.photo.TabParam;
import oso.widget.photo.TabToPhoto;
import support.dbhelper.StoredData;
import utility.HttpUrlPath;

/**
 * 网络请求-相关
 * 
 * @author 朱成
 * @date 2015-3-27
 */
public class NetInterface {

	// public static RequestParams printTlp_p(long printServerID,String
	// pltID_conID,String line_id){
	// RequestParams p = new RequestParams();
	// p.add("printer", printServerID + "");
	// p.add("param_type", "0"); // 0:pltIds 1:pallet_ids传configID 2:receiptId
	// p.add("params", pltID_conID);
	// p.add("ps_id", StoredData.getPs_id());
	// p.add("line_id", line_id);
	// return p;
	// }
	//
	// public static String printTlp_url(){
	// return HttpUrlPath.basePath+"_receive/android/printContainers";
	// }

	/**
	 * 上传图片
	 * 
	 * @param param
	 *            只需给定file_with_class等5个参数
	 * @param zipImgs
	 *            图片压缩到该zip中,可用FileUtil.createZipFile压缩 记得上传完后删除(成功或失败时)
	 * @return
	 */
	// public static RequestParams uploadImgs_p(TabParam param,List<File>
	// listImgs){
	public static RequestParams uploadImgs_p(TabParam param, File zipImgs) {
		// 压缩图片
		// List<File> imgs = new ArrayList<File>();
		// imgs.add(new File(new File(getPath_Upload(),
		// imgBean.uri).getPath()));

		// if(Utility.isEmpty(listImgs))
		// return null;

		// final File zipFile = FileUtil.createZipFile(
		// "ttp_" + new Date().getTime(), listImgs);

		if (!zipImgs.exists())
			return null;

		RequestParams p = new RequestParams();
		p.add("Method", "UpFile");
		p.add("file_with_class", param.file_with_class);
		p.add("file_with_type", param.file_with_type);
		p.add("file_with_id", param.file_with_id);
		// p.put("config", getConfigStr(imgs.get(0).getName(),
		// param.file_with_class));
		if (!TextUtils.isEmpty(param.file_with_detail_id))
			p.add("file_with_detail_id", param.file_with_detail_id);
		if (!TextUtils.isEmpty(param.option_file_param))
			p.add("option_file_param", param.option_file_param);

		try {
			p.put("gate_out", zipImgs);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return p;
	}

	public static String uploadImgs_url() {
		return HttpUrlPath.FileUpZipAction;
	}

	/**
	 * 收货-删除托盘
	 * 
	 * @param pltID
	 *            sync里的pltID
	 * @param is_scan_delete
	 *            1:扫sn时删除(此时不更新line上GoodQty、DmgQty)
	 * @return
	 */
	public static RequestParams recDelPlt_p(String pltID, String line_id, boolean is_scan_delete) {
		RequestParams p = new RequestParams();
		p.add("con_ids", pltID);
		p.add("ps_id", StoredData.getPs_id());
		p.add("receipt_line_id", line_id);
		p.add("is_scan_delete", is_scan_delete ? "1" : "0");
		return p;
	}

	public static String recDelPlt_url() {
		return HttpUrlPath.basePath + "_receive/android/deletePallet";
	}

	/**
	 * 添加-坏托盘
	 * 
	 * @param pltQty
	 * @param line_id
	 * @param item_id
	 * @param lot_no
	 * @param detail_id
	 * @param location
     * @param isAtLocation true(location)、false(staging)
	 * @param is_scan_create
	 *            1:会直接添加"receive_plt" 0:cnt后才加
	 * @return
	 */
	public static RequestParams recAddDmgPlts_p(int pltQty, String line_id, String item_id, String lot_no, String detail_id, LocationBean bean
            ,boolean isAtLocation,
			boolean is_scan_create
			,String type_id
			,int check_location) {
		RequestParams p = new RequestParams();
		p.add("receipt_line_id", line_id);
		p.add("item_id", item_id);
		p.add("lot_no", lot_no);
		p.add("pallet_qty", pltQty + "");
		p.add("ps_id", StoredData.getPs_id());
		p.add("goods_type", Rec_PalletBean.PltType_Damage + "");
		p.add("detail_id", detail_id);
		p.add("location", bean.location_name);
		p.add("location_id", bean.id+"");
        p.add("location_type",isAtLocation? LocBean.LocType_Location+"":LocBean.LocType_Staging+"");
		p.add("is_scan_create", is_scan_create ? "1" : "0");
		p.add("pallet_type", type_id);
		p.add("is_check_location", check_location+"");
		return p;
	}
	public static RequestParams recAddDmgPlts_p(int pltQty, String line_id, String item_id, String lot_no, String detail_id, LocBean bean
            ,boolean isAtLocation,
			boolean is_scan_create
			,String type_id
			,int check_location) {
		RequestParams p = new RequestParams();
		p.add("receipt_line_id", line_id);
		p.add("item_id", item_id);
		p.add("lot_no", lot_no);
		p.add("pallet_qty", pltQty + "");
		p.add("ps_id", StoredData.getPs_id());
		p.add("goods_type", Rec_PalletBean.PltType_Damage + "");
		p.add("detail_id", detail_id);
		p.add("location", bean.name);
		p.add("location_id", bean.id+"");
        p.add("location_type",isAtLocation? LocBean.LocType_Location+"":LocBean.LocType_Staging+"");
		p.add("is_scan_create", is_scan_create ? "1" : "0");
		p.add("pallet_type", type_id);
		p.add("is_check_location", check_location+"");
		return p;
	}

	public static String recAddDmgPlts_url() {
		return HttpUrlPath.basePath + "_receive/android/createDamageContainer";
	}

	/**
	 * 关任务
	 * 
	 * @param entry_id
	 * @param detail_id
	 * @param number_status
	 *            取ScanLoadActivity.CloseType_x
	 * @param equipment_id
	 * @param resources_type
	 * @param resources_id
	 * @param ttp
	 * @param noTaskInfos
	 *            true:仅返回"returnflag",不需tasks信息
	 * @return
	 */
	public static RequestParams closeTask_p(String entry_id, String detail_id, int number_status, int equipment_id, int resources_type,
			int resources_id, TabToPhoto ttp, boolean noTaskInfos) {
		RequestParams params = new RequestParams();
		params.add("Method", "TaskProcessingCloseDetail");
		params.add("entry_id", entry_id + "");
		params.add("detail_id", detail_id + "");
		params.add("number_status", number_status + "");
		params.add("equipment_id", equipment_id + "");
		params.add("resources_type", resources_type + "");
		params.add("resources_id", resources_id + "");
		if (noTaskInfos)
			params.add("request_type", "notaskinfos");
		if (ttp != null)
			ttp.uploadZip(params, "TaskClose");
		return params;
	}

	public static final int PrintLP_PltIDs = 0; // 打plts、打configs下plts、打rn下plts、打line下plts
	public static final int PrintLP_ConfigIDs = 1;
	// public static final int PrintLP_ReceiptID=2;
	public static final int PrintLP_LineID = 3;
	
	public static final int PrintLP_Inbound = 4;
	public static final int PrintLP_NewConfig = 5;

	/**
	 * 打印LP
	 * 
	 * @param printServerID
	 * @param printType
	 *            取PrintLP_x
	 * @param ids
	 * @return
	 */
	public static RequestParams recPrintLP_p(long printServerID, int printType, String ids) {
		RequestParams p = new RequestParams();
		p.add("printer", printServerID + "");
		p.add("param_type", printType + ""); // 0:pltIds 1:pallet_ids传configID
												// 2:recId
		p.add("params", ids);
		p.add("ps_id", StoredData.getPs_id());
		// p.add("line_id", lineID);
		return p;
	}

	public static String recPrintLP_url() {
		return HttpUrlPath.basePath + "_receive/android/printContainers";
	}

	public static RequestParams getImgs_p(String file_with_class, String file_with_type, String file_with_id, String file_with_detail_id,
			String option_file_param) {
		RequestParams p = new RequestParams();
		p.add("Method", "GetFileUri");
		p.add("file_with_class", file_with_class);
		p.add("file_with_type", file_with_type);
		p.add("file_with_id", file_with_id);
		if (!TextUtils.isEmpty(file_with_detail_id))
			p.add("file_with_detail_id", file_with_detail_id);
		if (!TextUtils.isEmpty(option_file_param))
			p.add("option_file_param", option_file_param);
		return p;
	}

	public static String getImgs_url() {
		return HttpUrlPath.FileUpZipAction;
	}

	// ==================tms===============================

	/**
	 * 打印LP
	 * 
	 * @param printServerID
	 * @return
	 */
	public static RequestParams tmsPrintLp_p(long printServerID, String pltID) {
		RequestParams p = new RequestParams();
		p.add("palletIds", pltID);
		p.add("printServerId", printServerID + "");
		return p;
	}

	public static String tms_printNUM() {// 打印小签
		return HttpUrlPath.basePath + "_tms/sorting/pallet/printNUM";
	}

	public static String tms_printLabel() {// 打印大签
		return HttpUrlPath.basePath + "_tms/sorting/pallet/printLable";
	}

}
