package support.network;

import java.net.SocketTimeoutException;

import org.apache.http.Header;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.SyncApplication;
import oso.widget.dialog.RewriteDialog;
import support.common.AlertUi;
import support.common.UIHelper;
import support.dbhelper.Goable;
import utility.StringUtil;
import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * @author jiang
 * 
 * @date 2015-05-20 18:37:46
 */
public class NetConnectionInterface {

	private static final AsyncHttpClient simpleClient = new AsyncHttpClient();

	public static final String NULL_ARRAY = "[]";
	public static final int REQ_SUCCESS = 1;
	public static final int REQ_ERROR = 90;
	public static final boolean isPrintLog = true;

	public static final String CONTENT_TYPE_TEXT = "text/html;charset=UTF-8";
	public static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
	
	public static final JsonHttpResponseHandler NULL_HANDLER =  new JsonHttpResponseHandler();

	static {
		simpleClient.setTimeout(30 * 1000);
	}

	public static AsyncHttpClient client = null;

	public static abstract class SyncJsonHandler extends JsonHttpResponseHandler {

		public Context mContext = null;

		public RewriteDialog progressDialog = null;

		public boolean isShowDialog = true;

		public SyncJsonHandler(Context c) {
			mContext = c;
		}

		public SyncJsonHandler(Context c, boolean isShowDialog) {
			mContext = c;
			this.isShowDialog = isShowDialog;
		}

		public abstract void handReponseJson(JSONObject json);

		public void handFail(int statusCode, Header[] headers, JSONObject json) {
			if (json != null)
				System.err.println("errorResponse= " + json.toString());
		};

		@Override
		public void onStart() {
			super.onStart();
			if (isShowDialog) {
				progressDialog = RewriteDialog.createDialog(mContext, 1);
				progressDialog.setTitle("");
				progressDialog.setMessage("");
				progressDialog.setCancelable(true);
				progressDialog.show();
			}
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
			super.onSuccess(statusCode, headers, json);
			finishDialog();
			if (statusCode != 200) {
//				AlertUi.showAlertBuilder(mContext, mContext.getString(R.string.sync_system_error));
				handFail(statusCode, headers, json);
			} else {
				if (isChecked(statusCode, headers, json))
					handReponseJson(json);
				else
					handFail(statusCode, headers, json);
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
			super.onFailure(statusCode, headers, responseString, throwable);
			finishDialog();
			AlertUi.showAlertBuilder(mContext, mContext.getString(R.string.sync_fail));
			System.out.println("onFailure --> " + responseString);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
			super.onFailure(statusCode, headers, throwable, errorResponse);
			finishDialog();
			AlertUi.showAlertBuilder(mContext, mContext.getString(R.string.sync_fail));
			System.out.println("onFailure --> 返回了一个JSONArray，应该为JSONObject");
			System.out.println("onFailure --> " + errorResponse.toString());
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
			super.onFailure(statusCode, headers, throwable, errorResponse);
			finishDialog();
			handFail(statusCode, headers, errorResponse);
			// 超时
			if (throwable instanceof SocketTimeoutException || throwable instanceof ConnectTimeoutException)
				UIHelper.showToast(mContext, mContext.getString(R.string.sync_timeout), Toast.LENGTH_LONG).show();
			else
				UIHelper.showToast(mContext, mContext.getString(R.string.sync_fail), Toast.LENGTH_LONG).show();
		}

		public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
			if (json.optInt("err") != 0) {
				if (json.optInt("err") == REQ_ERROR) {
					return false;
				} else {
					AlertUi.showAlertBuilder(mContext, mContext.getString(R.string.sync_system_error));
					return true;
				}
			}

			if (json.optInt("ret") == REQ_SUCCESS) {
				return true;
			} else {
				AlertUi.showAlertBuilder(mContext, mContext.getString(R.string.sync_system_error));
				return false;
			}
		}

		private void finishDialog() {
			if (progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
		}
	}

	public void initNetParams(RequestParams params) {
		initNetParams("", params);
	}

	public void initNetParams(String url, RequestParams params) {
		initNetParams(simpleClient, url, params);
	}

	/**
	 * 有特殊的需求如addHeader，contentType，JSESSIONID，StringEntity等
	 * 注：在调用此方法initNetParams_NewClient之后，get/post之前添加
	 * @param params
	 */
	public void initNetParams_NewClient(RequestParams params) {
		AsyncHttpClient customClient = new AsyncHttpClient();
		customClient.setTimeout(30 * 1000);
		initNetParams(customClient, "", params);
	}

	public void initNetParams(AsyncHttpClient c, String url, RequestParams params) {
		Context context = SyncApplication.getThis();
		Goable.initGoable(context);
		StringUtil.setLoginInfoParams(params);
		client = c;

		// 设置cookie
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);

		// 打印url
		if (isPrintLog)
			System.out.println(url + "?" + params.toString());
	}

}
