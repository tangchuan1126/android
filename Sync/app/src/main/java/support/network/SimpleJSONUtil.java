package support.network;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.SyncApplication;
import oso.widget.dialog.RewriteBuilderDialog;
import oso.widget.dialog.RewriteDialog;
import support.common.AlertUi;
import support.common.UIHelper;
import support.dbhelper.Goable;
import support.key.BCSKey;
import utility.FileUtil;
import utility.StringUtil;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import declare.com.vvme.R;

/**
 * 0.这个类的目的是除去，很多错误信息的重复判断<br />
 * 1.只用于 请求简单的JSON数据格式的时候<br />
 * 2.handReponseJson 处理返回的JSON 格式的数据<br />
 * 3.handFinish 处理不管是错误，或者是正确执行的方法<br />
 * 4.现在很多的处理都是获取数据,然后存储在数据库中。所以在handReponseJson，写入数据库中 <br />
 * 5.handFinish 请求完成过后，然后读取数据库里面的数据(你也可以没有)
 * 
 * @author zhangrui
 * 
 */
public abstract class SimpleJSONUtil {

	public static AsyncHttpClient client = AsyncHttpClientGenerate.getInstance();
	static {
		client.setTimeout(30 * 1000);
	}
	
	private boolean enableCookie=true;
	private boolean showLoadingDlg=true;
	
	public abstract void handReponseJson(JSONObject json);
	
	public void handReponseJsonArray(JSONArray array) {
	}

	/**
	 * 成功、失败、cancel
	 */
	public void handFinish() {
	};

	public void handFail() {
	};
	
	
	public RequestHandle reqHandle;		//请求句柄

	// 入参:1>inSilence:静默调用,即不作任何ui提示
	public void doPost(final String url, final RequestParams params, final Context context, boolean inSilence) {
		//非静默-访问
		if (!inSilence) {
			doPost(url, params, context);
		}

		// 添加-登陆信息
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
		//若禁用cookie
		if (!enableCookie)
			client.setCookieStore(null);

		reqHandle=client.post(url, params, new JsonHttpResponseHandler("utf-8") {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				// 网络异常
				if (statusCode != 200) {
					handFail();
					return;
				}

				// 外界处理
				if (useTheCustom(json))
					return;

				// 请求成功
				if (StringUtil.getJsonInt(json, "ret") == 1) {
					handReponseJson(json);
				} else {
					// 版本问题
					// if (StringUtil.getJsonInt(json, "err") ==
					// BCSKey.AppVersionException)
					// return;
					// 回调
					handFail();
				}
			}

			@Override
			public void onFinish() {
				handFinish();
			}
			
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				super.onCancel();
				handFinish();
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				handFail();
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				// TODO Auto-generated method stub
				handFail();
				if((responseString.contains("Sorry, the page you are looking for is currently unavailable.") && responseString.contains("Faithfully yours, nginx."))
						|| responseString.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
		//debug
//		System.out.println("nimei>>post_req:"+reqHandle);
	}
	
	/**
	 * 带cookie,默认true
	 * @param enable
	 * @return
	 */
	public SimpleJSONUtil enableCookie(boolean enable){
		this.enableCookie=enable;
		return this;
	}
	
	/**
	 * 显示加载dlg(暂时仅支持post),默认true
	 * @param show
	 * @return
	 */
	public SimpleJSONUtil showLoadingDlg(boolean show){
		this.showLoadingDlg=show;
		return this;
	}
	
	public void doPost(final String url, final RequestParams params, final Context context) {
//		AsyncHttpClient client = new AsyncHttpClient();
		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(isCancelable);
		dialog.setCanceledOnTouchOutside(isCancelable);
		
		// final String appendNofity =
		// resources.getString(R.string.sys_load_data_failed) ;
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
	    client.setCookieStore(myCookieStore);
		//若禁用cookie
		if(!enableCookie)
			client.setCookieStore(null);
		
		// 改成HttpClient 的方式，放回的为JSON的数据
		reqHandle=client.post(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				if(showLoadingDlg)
					dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				final String appendNofity = resources.getString(R.string.sys_load_data_failed);
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
//						if (StringUtil.getJsonInt(json, "ret") == 1) {
//							handReponseJson(json);
//						} else {
//							// 提示系统的错误(更加返回的err值)
//							if (StringUtil.getJsonInt(json, "err") == BCSKey.AppVersionException) {
//								AlertUi.showVersionError(context);
//								return;
//							}
//							AlertUi.showAlertBuilder(context, BCSKey.getReturnStatusById(StringUtil.getJsonInt(json, "err")));
//							UIHelper.showToast(context, BCSKey.getReturnStatusById(StringUtil.getJsonInt(json, "err")), Toast.LENGTH_SHORT).show();
//							handFail();
//						}
						processReturn(context, json, appendNofity);
					}
				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_submit_data_failed), Toast.LENGTH_SHORT).show();
					handFail();
				}
			}

			// debug,上传文件微服务返回的是jsonArray,shit
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing())
					dialog.dismiss();
				super.onSuccess(statusCode, headers, response);
				useTheCustom(response);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				super.onCancel();
				handFinish();
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				onFail_inner(context, dialog,throwable);
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				// TODO Auto-generated method stub
				onFail_inner(context, dialog,throwable);
				if((responseString.contains("Sorry, the page you are looking for is currently unavailable.") && responseString.contains("Faithfully yours, nginx."))
						|| responseString.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}
		});
	}

	public void doGisGet(final String url, final RequestParams params, final Context context, final View progress) {
		params.add("from", "android");
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
		// 改成HttpClient 的方式，放回的为JSON的数据
		System.out.println("GIS Url= " + url + "?" + params.toString());
		reqHandle=client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				progress.setVisibility(View.GONE);
				System.out.println("responseBody : " + json.toString());
				handReponseJson(json);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				progress.setVisibility(View.GONE);
				handFail();
				UIHelper.showToast(context, context.getString(R.string.sync_timeout), Toast.LENGTH_LONG).show();
				if((responseBody.contains("Sorry, the page you are looking for is currently unavailable.") && responseBody.contains("Faithfully yours, nginx."))
						|| responseBody.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}
	
	public void doGisGetArray(final String url, final RequestParams params, final Context context, final View progress) {
		params.add("from", "android");
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
		// 改成HttpClient 的方式，放回的为JSON的数据
		System.out.println("GIS Url= " + url + "?" + params.toString());
		reqHandle=client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				progress.setVisibility(View.GONE);
				System.out.println("responseBody : " + response.toString());
				handReponseJsonArray(response);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				progress.setVisibility(View.GONE);
				handFail();
				UIHelper.showToast(context, context.getString(R.string.sync_timeout), Toast.LENGTH_LONG).show();
				if((responseBody.contains("Sorry, the page you are looking for is currently unavailable.") && responseBody.contains("Faithfully yours, nginx."))
						|| responseBody.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}
	
	private boolean isCancelable=true;
	
	/**
	 * 返回键、点击dialog外部-是否可取消
	 * @param isCancelable
	 * @return
	 */
	public SimpleJSONUtil setCancelable(boolean isCancelable){
		this.isCancelable=isCancelable;
		return this;
	}

	public void doGet(final String url, final RequestParams params, final Context context) {

		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(isCancelable);
		dialog.setCanceledOnTouchOutside(isCancelable);

		final String appendNofity =  resources.getString(R.string.sys_load_data_failed);
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
		// 改成HttpClient 的方式，放回的为JSON的数据
		System.out.println(url + "?" + params.toString());
		reqHandle=client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						processReturn(context, json,appendNofity);
					}

				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_getData_failed) + ", " + appendNofity, Toast.LENGTH_SHORT)
							.show();
					handFail();
				}
			}
			
			@Override
			public void onFinish() {
				handFinish();
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				onFail_inner(context, dialog,throwable);
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				// TODO Auto-generated method stub
				onFail_inner(context, dialog,throwable);
				if((responseString.contains("Sorry, the page you are looking for is currently unavailable.") && responseString.contains("Faithfully yours, nginx."))
						|| responseString.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}

            //debug,fiddler调试时若无法连接主机返回"[fiddler] xxx",会解析成JsonArray,oh shit
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                super.onFailure(statusCode, headers, throwable, errorResponse);
                onFail_inner(context, dialog,throwable);
            }

            @Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}
	
	/**
	 * 回调至onFailure时
	 * @param context
	 * @param dialog
	 */
	private void onFail_inner(Context context, Dialog dialog,Throwable throwable){
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		handFail();
		//超时
		if(throwable instanceof SocketTimeoutException || throwable instanceof ConnectTimeoutException)
			UIHelper.showToast(context, context.getString(R.string.sync_timeout), Toast.LENGTH_LONG).show();
		//无网络,server不可用也会报这个
//		else if(throwable instanceof HttpHostConnectException)
//			UIHelper.showToast(context, "Network Unavailable!", Toast.LENGTH_LONG).show();
		else
			UIHelper.showToast(context, context.getString(R.string.sync_fail), Toast.LENGTH_LONG).show();
	}
	
	/**
	 * 处理返回值,statusCode=200才调
	 * @return true:有效
	 */
	private void processReturn(Context context,JSONObject json,String appendNofity){
		/* 更改日期：20150422
		 * 更改原因：盘点返回的JSON格式全部为大写 保持兼容性更改此方法
		 * 操作人：郭春阳
		 */
		
		int ret = json.optInt("ret");
		if(ret==0) ret = json.optInt("RET");
		
		if (ret == 1) {
			handReponseJson(json);
		} else {
			
			int err = json.optInt("err");
			if(err==0) err = json.optInt("ERR");
			
			if (err == BCSKey.AppVersionException) {
				AlertUi.showVersionError(context);
				return;
			}
			//debug
			if (err == BCSKey.Err_WithMsg) {
				AlertUi.showAlertBuilder(context,json.optString("data"));
				return;
			}
			
			// 提示系统的错误(更加返回的err值)
			AlertUi.showAlertBuilder(context, BCSKey.getReturnStatusById(context,err));
			UIHelper.showToast(context, BCSKey.getReturnStatusById(context,err)  ,
					Toast.LENGTH_SHORT).show();
			handFail();
		}
	}

	public void doGetForYasir(final String url, final RequestParams params, final Context context) {
		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(true);

		final String appendNofity = resources.getString(R.string.sys_load_data_failed);
		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		client.setCookieStore(myCookieStore);
		// 改成HttpClient 的方式，放回的为JSON的数据
		client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						if (StringUtil.getJsonInt(json, "status") == 1) {
							handReponseJson(json);
						} else {
							JSONArray jArray = json.optJSONArray("errors");
							if(!StringUtil.isNullForJSONArray(jArray)){
								JSONObject jObj = jArray.optJSONObject(0);
								UIHelper.showToast(context, jObj.optString("errorMessage"));
							}
							handFail();
						}
					}

				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_getData_failed)  , Toast.LENGTH_SHORT)
							.show();
					handFail();
				}
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				onFail_inner(context, dialog,throwable);
			}
			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				if((responseString.contains("Sorry, the page you are looking for is currently unavailable.") && responseString.contains("Faithfully yours, nginx."))
						|| responseString.contains("502 Bad Gateway")){
					RewriteBuilderDialog.showSimpleDialog_Tip(context,"The server is unavailable. Please contact the system administrator.");
				}
			}

		});
	}
	
	public void doPostForYasir(final String url,JSONObject json, final Context context) {

		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(true);
		
		AsyncHttpClient clientPost = new AsyncHttpClient();
		
		PersistentCookieStore myCookieStore = new PersistentCookieStore(context);
		clientPost.setCookieStore(myCookieStore);
	    
		StringEntity entity = null;
        try {
			entity = new StringEntity(json.toString(),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        
        clientPost.addHeader("JSESSIONID", getCookie_jsession());
        clientPost.addHeader("Content-Type", "application/json");
        
        
        clientPost.post(context, url, entity, "application/json",new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				if(showLoadingDlg)
					dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						if (StringUtil.getJsonInt(json, "status") == 1) {
							handReponseJson(json);
						} else {
							JSONArray jArray = json.optJSONArray("errors");
							if(!StringUtil.isNullForJSONArray(jArray)){
								JSONObject jObj = jArray.optJSONObject(0);
								UIHelper.showToast(context, jObj.optString("errorMessage"));
							}
							handFail();
						}
					}
	
				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_submit_data_failed), Toast.LENGTH_SHORT).show();
					handFail();
				}
			}

			// debug,上传文件微服务返回的是jsonArray,shit
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing())
					dialog.dismiss();
				super.onSuccess(statusCode, headers, response);
				useTheCustom(response);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				onFail_inner(context, dialog,throwable);
			}
		});
	}


	/**
	 * @Description:大屏幕专用
	 * @param url
	 * @param params
	 * @param context
	 */
	private static boolean runFlag = true;

	public void doGetNotHaveDialog(final String url, final RequestParams params, final Context context) {

		Goable.initGoable(context);
		if (setLoginInfoParams()) {
			StringUtil.setLoginInfoParams(params);
		}

		// 改成HttpClient 的方式，放回的为JSON的数据
		reqHandle= client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				// 处理结果
				if (statusCode == 200) {
					if (json.optInt("ret") == 1) {
						handReponseJson(json);
					} else {
						if (StringUtil.getJsonInt(json, "err") == BCSKey.AppVersionException) {
							return;
						}
						if (runFlag) {
							// 提示系统的错误(更加返回的err值)
							AlertUi.showAlertBuilder(context, BCSKey.getReturnStatusById(context,StringUtil.getJsonInt(json, "err")));
							runFlag = false;
						}
						handFail();
					}
				} else {
					handFail();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				handFail();
			}
			
			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				//若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}

	/**
	 * @Description:用自定义的方法去处理返回结果时的处理
	 * @param @param json
	 * @param @return 返回false则用默认的方法 返回 ture则用自定义的方法
	 */
	public boolean useTheCustom(JSONObject json) {
		return false;
	};

	/**
	 * @Description:用自定义的方法去处理返回结果时的处理
	 * @param @param json
	 * @param @return 返回false则用默认的方法 返回 ture则用自定义的方法
	 */
	public boolean useTheCustom(JSONArray json) {
		return false;
	};

	/**
	 * @Description:用自定义的方法去处理是否反填登陆信息
	 * @param @param json
	 * @param @return 返回false则用不填 返回 ture则填
	 */
	public boolean setLoginInfoParams() {
		return true;
	}

	// =================static=============================

	/**
	 * 获取cookie
	 * 
	 * @param headers
	 */
	public static void getCookie(Map<String, String> headers) {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(SyncApplication.getThis());
		List<Cookie> list = myCookieStore.getCookies();
		if (list == null)
			return;

		for (int i = 0; i < list.size(); i++) {
			Cookie co = list.get(i);
			headers.put(co.getName(), co.getValue());
		}
	}

	/**
	 * 获取cookie,如:JSESSIONID=xxx;c2=xxx;
	 * 
	 * @return
	 */
	public static String getCookie() {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(SyncApplication.getThis());
		List<Cookie> list = myCookieStore.getCookies();
		if (list == null)
			return "";

		String ret = "";
		for (int i = 0; i < list.size(); i++) {
			Cookie co = list.get(i);
			ret = co.getName() + "=" + co.getValue() + ";";
		}
		return ret;
	}
	
	public static String getCookie_jsession() {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(SyncApplication.getThis());
		List<Cookie> list = myCookieStore.getCookies();
		if (list == null)
			return "";

		String ret = "";
		for (int i = 0; i < list.size(); i++) {
			Cookie co = list.get(i);
			if("JSESSIONID".equals(co.getName()))
				return co.getValue();
			
//			ret = co.getName() + "=" + co.getValue() + ";";
		}
		return ret;
	}

	// 添加"图片s"至RequestParams
	// 入参:1>listPhotoNames:图片名s,一般来自LinearLayoutAddPhotoSmall等
	public static void addPhotosToParams(RequestParams params, List<String> listPhotoNames) {
		if (listPhotoNames == null || listPhotoNames.size() == 0)
			return;

		String upFileBasePath = Goable.file_image_path;
		List<File> files = new ArrayList<File>();
		for (int index = 0; index < listPhotoNames.size(); index++) {
			String fileName = StringUtil.getUpFilePictureFileName(listPhotoNames.get(index));
			String filePath = upFileBasePath + fileName;
			files.add(new File(filePath));
		}
		if (files.size() > 0) {
			try {
				File zipFile = FileUtil.createZipFile("checkin_gate_", files);
				if (zipFile != null)
					params.put("gate_checkin_photot", zipFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
