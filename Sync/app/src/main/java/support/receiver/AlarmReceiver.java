package support.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 防止熄屏后CPU降频/不工作 导致openfire异常的问题。开启定时器5分钟起一次
 * @author xialimin
 *
 */
public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context ctx, Intent in) {
		Log.d(getClass().getSimpleName(), "AlarmManager");
	}

}
