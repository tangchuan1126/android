package support.receiver;

import oso.ui.chat.activity.CallActivity;
import utility.Utility;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class IncomingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        
        //打开进入是否接听界面
    	Utility.AcquireWakeLock(context);  // 点亮屏幕
        Intent intentCall = new Intent(context, CallActivity.class);
        intentCall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentCall.putExtra("intent", intent);
        CallActivity.initParams(intentCall, null, null, CallActivity.STATUS_INCOMING);
        context.startActivity(intentCall);
       
    }
    
}
