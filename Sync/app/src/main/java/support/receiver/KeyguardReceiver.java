package support.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import oso.ui.chat.util.ChatUtil;
import oso.widget.screennotice.SyncNotifyManager;
import support.common.UIHelper;

/**
 * @author xialimin
 * 解锁广播
 */
public class KeyguardReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // 解锁
        if (intent != null && Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
            SyncNotifyManager.isInKeyguard = false;
        }
    }
}
