package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Instrumentation;

/**
 * @author 朱成
 * @date 2014-8-8
 */
public class SocketServer {

	ServerSocket sever;

	public SocketServer(int port) {
		try {
			sever = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void beginListen() {
		if(sever==null)
			return;
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						final Socket socket = sever.accept();
						BufferedReader in;
						try {
							in = new BufferedReader(new InputStreamReader(
									socket.getInputStream(), "UTF-8"));
							PrintWriter out = new PrintWriter(
									socket.getOutputStream());
							
							// 读取
							String str="";
//							String line;
//							while ((line=in.readLine())!=null) {
//								str+=line;
//							}
							str=in.readLine();
							
//							String value=str;
							if(str==null || str.indexOf("GET /?v=")==-1){
//								socket.close();
								continue;
							}
								
							str=str.substring("GET /?v=".length(),str.indexOf(" HTTP/"));
							
							System.out.println("nimei>>req:"+str);
							
							//模拟-输入
							Instrumentation ins=new Instrumentation();
							ins.sendStringSync(str+"\n");
							
							//输出
							out.println("nimei:" + str);
							out.flush();
							socket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
//				System.out.println("nimei>>exit thread");
			}
		}).start();
	}
}
