package oso.gis;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.gis.adapter.GoogleMapExpandableListAdapter;
import oso.gis.adapter.MapSearchContentAdapter;
import oso.gis.adapter.MapSearchPositionContentAdapter;
import oso.gis.adapter.MapValleyPopupAdapter;
import oso.gis.bean.AccessServerBean;
import oso.gis.bean.MenuBean;
import oso.gis.bean.OccupyBean;
import oso.gis.bean.RectBean;
import oso.gis.bean.WarehouseBean;
import oso.gis.bean.WebCamBean;
import oso.gis.util.GMapUtils;
import oso.gis.util.ZipUtil;
import utility.ActivityUtils;
import utility.BottomDialog;
import utility.FileUtils;
import utility.HttpUrlPath;
import utility.RewriteBuilderDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.common.SimpleFileUtil;
import com.android.common.SimpleJSONUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.loopj.android.http.RequestParams;

public class GMapActivity extends BaseMapActivity implements OnClickListener, OnMarkerClickListener, OnInfoWindowClickListener, OnItemClickListener,
		TextWatcher, OnCameraChangeListener, OnMapClickListener, OnTouchListener {

	public static String ps_id = "1000005";
	public static String cookie = "";

	private DrawerLayout drawerLayout;
	private ImageButton left_button;
	private ExpandableListView left_drawer;
	private ProgressBar progress;
	private Button topbar_back;
	private TextView titleTv;
	private EditText map_searchEt;
	private LinearLayout map_header_show;

	private ListView searchDataLv;
	private PopupWindow mPopupWindow;
	private ListView popupLv;
	private MapValleyPopupAdapter valleyAdapter;
	private GoogleMapExpandableListAdapter expandableListAdapter;
	private MapSearchContentAdapter searchAdapter;
	private ImageView map_search_bg;
	private LinearLayout searchListLayout;

	private TextView menuTitleTv; // 菜单标题
	private List<RectBean> listContent;

	// 数据
	private List<WarehouseBean> warehouseDatas; // 所有仓库的数据
	private WarehouseBean currentWarehouseData; // 当前仓库
	private Marker mMarker;
	private List<RectBean> zones, doors, spots;
	private List<RectBean> oDoors = new ArrayList<RectBean>();
	private List<RectBean> oSpots = new ArrayList<RectBean>();
	private List<WebCamBean> webCams;

	// boolean markersShow; // 控制地图的点击
	public boolean flag = true; // 控制搜索框的点击

	private boolean isShowSearch = false;

	public static final String ACTION_GIS = "android.intent.action.SYNC_GIS";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_google_map_header_title);
		recriveIntent();
	}

	@Override
	protected void onStart() {
		super.onStart();
		loadMapDatas();
	}

	private void recriveIntent() {
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			ps_id = bundle.getString("ps_id", "1000005");
			cookie = bundle.getString("cookie", "");
			String basePath = bundle.getString("base_url", HttpUrlPath.formatUrl("192.198.208.43"));
			HttpUrlPath.initBaseUrls(basePath);
			Log.e("mofind", "ps_id= " + ps_id);
			Log.e("mofind", "cookie= " + cookie);
			Log.e("mofind", "baseUrl= " + basePath);
		} else {
			showErrorDialog("bundle == null");
			return;
		}
	}

	public void loadMapDatas() {
		RequestParams params = new RequestParams();
		params.add("version", GISApplication.getMapDataVersion() + "");
		new SimpleFileUtil() {
			@Override
			public void handResponse(int statusCode, Header[] headers, byte[] responseBody) {
				if (responseBody != null && responseBody.length != 0) {
					parseMapData(responseBody);
				} else {
					showErrorDialog("Server No Datas! (v=" + GISApplication.getMapDataVersion() + ")");
					return;
				}
			}

			@Override
			public void handFail() {
				if (!FileUtils.isFileExist(GMapUtils.unZipPath + File.separator + GMapUtils.FILE_VERSION)) {
					if (!FileUtils.isFileExist(GMapUtils.zipFilePath)) {
						showErrorDialog("Server No Datas! (v=" + GISApplication.getMapDataVersion() + ")");
						return;
					} else {
						try {
							ZipUtil.UnZipFolder(GMapUtils.zipFilePath, GMapUtils.unZipPath);// 解压数据
						} catch (Exception e) {
							showErrorDialog("Parse Zip Error!");
							return;
						}
					}
				}
				// TODO 正常加载数据
				initData();
				initView();
				setData();
				setUpMapIfNeeded();
			};

		}.doGetFile(HttpUrlPath.storageDatas, params, context);
	}

	private void parseMapData(final byte[] responseBody) {
		GMapActivity.mainHandler.post(new Runnable() {
			@Override
			public void run() {
				try {
					InputStream is = new ByteArrayInputStream(responseBody);
					FileUtils.deleteFile(GMapUtils.zipFilePath); // 删除之前的zip文件
					FileUtils.deleteFile(GMapUtils.unZipPath); // 删除之前的文件
					FileUtils.writeFile(GMapUtils.zipFilePath, is); // 写入数据
					ZipUtil.UnZipFolder(GMapUtils.zipFilePath, GMapUtils.unZipPath); // 解压数据
					GISApplication.setMapDataVersion(GMapUtils.unZipPath + File.separator + GMapUtils.FILE_VERSION);
					// TODO 正常加载数据
					initData();
					initView();
					setData();
					setUpMapIfNeeded();

				} catch (Exception e) {
					showErrorDialog("Parse Zip Error!");
					return;
				}
			}
		});
	}

	private void initData() {
		warehouseDatas = WarehouseBean.getAllWarehouseDatas();
		currentWarehouseData = WarehouseBean.getWarehouseData(ps_id);
		if (currentWarehouseData == null) {
			currentWarehouseData = warehouseDatas.get(0);
		}
	}

	private void initView() {
		titleTv = (TextView) findViewById(R.id.titleTv);
		progress = (ProgressBar) findViewById(R.id.progress);
		topbar_back = (Button) findViewById(R.id.topbar_back);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		left_button = (ImageButton) findViewById(R.id.ib_left_button);
		map_searchEt = (EditText) findViewById(R.id.et_map_search);
		map_search_bg = (ImageView) findViewById(R.id.et_map_search_bg);
		searchDataLv = (ListView) findViewById(R.id.lv_map_search_content);
		searchListLayout = (LinearLayout) findViewById(R.id.lv_map_search_content_bg);
		map_header_show = (LinearLayout) findViewById(R.id.map_header_title_show);
		menuTitleTv = (TextView) findViewById(R.id.tv_left_ps_id);
		left_drawer = (ExpandableListView) findViewById(R.id.left_drawer);

		topbar_back.setOnClickListener(this);
		left_button.setOnClickListener(this);
		map_header_show.setOnClickListener(this);
		map_search_bg.setOnClickListener(this);
		searchListLayout.setOnClickListener(this);
		map_searchEt.setOnTouchListener(this);
		map_searchEt.addTextChangedListener(this);
	}

	private void setData() {
		menuTitleTv.setText(currentWarehouseData.psName);
	}

	@Override
	protected void setUpMap() {
		showLoadingView();
		mMap.clear();
		// 绘制仓库
		drawWarehouse(currentWarehouseData);
		titleTv.setText(currentWarehouseData.psName);

		mainHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// 绘制zone
				zones = drawZones(currentWarehouseData.psId);
				// 绘制door
				doors = drawDoorsOrSpots(Type.DOOR, currentWarehouseData.psId);
				// 绘制spot
				spots = drawDoorsOrSpots(Type.SPOT, currentWarehouseData.psId);
				// 绘制webCams
				webCams = drawWebCams(currentWarehouseData.psId);
				// 绘制Marker
				mMarker = drawMark(currentWarehouseData.center, "Marker");
				// 设置菜单
				setMenuDrawer();
				// 设置地图监听
				mMap.setPadding(10, 120, 0, 0);
				mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
				mMap.setOnCameraChangeListener(GMapActivity.this);
				mMap.setOnMarkerClickListener(GMapActivity.this);
				mMap.setOnInfoWindowClickListener(GMapActivity.this);
				mMap.setOnMapClickListener(GMapActivity.this);
				// 启动线程
				mainHandler.post(trackHandler);
				// 显示地图页面
				showContentView();
			}
		}, 500);
	}

	/**
	 * 显示加载页面
	 */
	private void showLoadingView() {
		findViewById(R.id.loadingLayout).setVisibility(View.VISIBLE);
		// hideSearch();
	}

	/**
	 * 显示地图页面
	 */
	private void showContentView() {
		findViewById(R.id.loadingLayout).setVisibility(View.GONE);
		Animation anim = new AlphaAnimation(1, 0);
		anim.setDuration(2000);
		findViewById(R.id.loadingLayout).setAnimation(anim);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentWarehouseData.center, MAP_DEFAULT_LEVEL)); // 定位
	}

	/**
	 * 监视器用于循环守卫线程是否正常运行
	 */
	private Runnable trackHandler = new Runnable() {
		public void run() {
			reqOccupancyData();
			mainHandler.postDelayed(trackHandler, 30 * 1000);
		}
	};

	private void setMenuDrawer() {
		expandableListAdapter = new GoogleMapExpandableListAdapter(context);
		left_drawer.setGroupIndicator(null);
		left_drawer.setAdapter(expandableListAdapter);
		// 默认展开
		left_drawer.expandGroup(0);
		// 重写组点击监听，不可点击
		left_drawer.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
		left_drawer.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				final MenuBean bean = (MenuBean) expandableListAdapter.getChild(groupPosition, childPosition);
				bean.status = !bean.status;
				drawerLayout.closeDrawers();
				// 刷新地图
				refreshMaplay();
				return true;
			}
		});
	}

	private void initPopupWindow() {
		View popupView = View.inflate(context, R.layout.act_google_map_left_header_popup, null);
		popupLv = (ListView) popupView.findViewById(R.id.lv_popup_content);
		valleyAdapter = new MapValleyPopupAdapter(context, warehouseDatas);
		popupLv.setAdapter(valleyAdapter);
		popupLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				currentWarehouseData = (WarehouseBean) valleyAdapter.getItem(position);
				menuTitleTv.setText(currentWarehouseData.psName);
				hidePopupWindow();
				drawerLayout.closeDrawers();
				MenuBean.initMenus();
				expandableListAdapter.notifyDataSetChanged();
				initDataStatus();
				setUpMap();
			}
		});

		if (mPopupWindow == null) {
			mPopupWindow = new PopupWindow(popupView, -2, -2);
			mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			mPopupWindow.setWidth(map_header_show.getWidth());
			mPopupWindow.setOutsideTouchable(true);
			mPopupWindow.setFocusable(true);
			mPopupWindow.setTouchable(true);
		}
	}

	private void initDataStatus() {
		oDoors.clear();
		oSpots.clear();
		mMarker = null;
		zones = null;
		doors = null;
		spots = null;
		webCams = null;
	}

	private void refreshMaplay() {
		final MenuBean spotMenu = MenuBean.list.get(0);
		final MenuBean doorMenu = MenuBean.list.get(1);
		final MenuBean webCamMenu = MenuBean.list.get(2);
		final MenuBean zoneMenu = MenuBean.list.get(3);
		// 设置选中状态
		setPolygonVisible(spots, spotMenu.status, MAP_DEFAULT_LEVEL);
		setPolygonVisible(doors, doorMenu.status, MAP_DEFAULT_LEVEL);
		setWebCamVisible(webCams, webCamMenu.status);
		setPolygonVisible(zones, zoneMenu.status, MAP_DEFAULT_LEVEL);
		// 设置占用门刷新状态
		boolean isOpenTrack = spotMenu.status || doorMenu.status;
		// 设置占用门的显示状态
		if (spotMenu.status && doorMenu.status) {
			reqOccupancyData();
		}
		if (!spotMenu.status && doorMenu.status) {
			reqOccupancyData();
		}
		if (spotMenu.status && !doorMenu.status) {
			reqOccupancyData();
		}
		if (!spotMenu.status && !doorMenu.status) {
			hideMarker();
			setOccupyStatus(oDoors, false);
			setOccupyStatus(oSpots, false);
		}
		expandableListAdapter.notifyDataSetChanged();
	}

	private void reqOccupancyData() {
		RequestParams params = new RequestParams();
		params.add("ps_id", currentWarehouseData.psId);
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
				List<OccupyBean> objs = GMapUtils.getOccupyData(json);
				if (objs.isEmpty()) {
					return;
				}
				// 选出已经被占用的门
				for (RectBean bean : doors) {
					for (OccupyBean ob : objs) {
						if (bean.obj_id.equals(ob.obj_id) && ob.obj_type.equals(OccupyBean.DOCKS)) {
							bean.type = Type.DOOR;
							oDoors.add(bean);
						}
					}
				}
				// 选出已经被占用的spot
				for (RectBean bean : spots) {
					for (OccupyBean ob : objs) {
						if (bean.obj_id.equals(ob.obj_id) && ob.obj_type.equals(OccupyBean.PARKING)) {
							bean.type = Type.SPOT;
							oSpots.add(bean);
						}
					}

				}

				final MenuBean spotMenu = MenuBean.list.get(0);
				final MenuBean doorMenu = MenuBean.list.get(1);

				if (!spotMenu.status && doorMenu.status) {
					setOccupyStatus(oDoors, true);
					setOccupyStatus(oSpots, false);
				}
				if (spotMenu.status && !doorMenu.status) {
					setOccupyStatus(oDoors, false);
					setOccupyStatus(oSpots, true);
				}
				if (spotMenu.status && doorMenu.status) {
					setOccupyStatus(oDoors, true);
					setOccupyStatus(oSpots, true);
				}
				if (!spotMenu.status && !doorMenu.status) {
					setOccupyStatus(oDoors, false);
					setOccupyStatus(oSpots, false);
				}
			};

			@Override
			public void handFail() {
				Log.e("mofind", "获取reqOccupancyData数据失败！");

			};
		}.doGisGet(HttpUrlPath.getDocksParkingOccupancy, params, context, progress);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.topbar_back:
			onBackBtnOrKey();
			break;
		case R.id.ib_left_button:
			if (drawerLayout != null)
				drawerLayout.openDrawer(Gravity.START);
			break;
		case R.id.map_header_title_show:
			showPopupWindow();
			break;
		case R.id.et_map_search_bg:
		case R.id.lv_map_search_content_bg:
			// hidderKeyboard(v);
			break;
		}
	}

	private RectBean rectBean;

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (v.getId() == R.id.et_map_search) {
				if (flag) {
					listAnimation(true, searchListLayout);
					flag = false;
				}
			}
			break;
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long l) {
		hideSearch();
		rectBean = listContent.get(position);
		final MenuBean spotMenu = MenuBean.list.get(0);
		final MenuBean doorMenu = MenuBean.list.get(1);
		if (rectBean.type == Type.DOOR && doorMenu.status == false)
			doorMenu.status = true;
		if (rectBean.type == Type.SPOT && spotMenu.status == false)
			spotMenu.status = true;
		refreshMaplay();
		showMarker(rectBean);
		map_searchEt.setText("");
		flag = true;
	}

	// 监听EditView的变化
	@Override
	public void onTextChanged(CharSequence arg0, int position, int arg2, int arg3) {
		if (doors == null || spots == null) {
			return;
		}
		String searchStr = map_searchEt.getText().toString();
		// 提示
		listContent = new LinkedList<RectBean>();
		searchAdapter = new MapSearchContentAdapter(context, listContent);
		searchDataLv.setAdapter(searchAdapter);
		searchDataLv.setOnItemClickListener(this);
		searchListLayout.setOnClickListener(this);
		if (!searchStr.isEmpty()) {
			showSearch();
			for (RectBean door : doors) {
				if (door.obj_name.startsWith(searchStr)) {
					door.type = Type.DOOR;
					listContent.add(door);
				}
			}
			for (RectBean spot : spots) {
				if (spot.obj_name.startsWith(searchStr)) {
					spot.type = Type.SPOT;
					listContent.add(spot);
				}
			}

			searchAdapter.notifyDataSetChanged();
		} else {
			hideSearch();
		}
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
	}

	@Override
	public void afterTextChanged(Editable arg0) {
	}

	public void showSearch() {
		left_button.setVisibility(View.INVISIBLE);
		map_search_bg.setVisibility(View.VISIBLE);
		searchListLayout.setVisibility(View.VISIBLE);
		isShowSearch = true;
		map_searchEt.setCursorVisible(true);// 显示光标
	}

	public void hideSearch() {
		left_button.setVisibility(View.VISIBLE);
		map_search_bg.setVisibility(View.GONE);
		searchListLayout.setVisibility(View.GONE);
		isShowSearch = false;
		map_searchEt.setCursorVisible(false);// 隐藏光标
		ActivityUtils.hideSoftInput(context);
	}

	public void listAnimation(boolean listFlag, View view) {

		AnimationSet mAnimationSet = new AnimationSet(true);
		if (listFlag) {
			TranslateAnimation translate = new TranslateAnimation(0, 0, ActivityUtils.getScreenHeight(context), 0);
			translate.setFillAfter(true);
			translate.setDuration(400);
			mAnimationSet.addAnimation(translate);
			mAnimationSet.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					showSearch();
				}
			});

		} else {
			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, ActivityUtils.getScreenHeight(context));
			translate.setFillAfter(true);
			translate.setDuration(150);
			mAnimationSet.addAnimation(translate);
			mAnimationSet.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					hideSearch();
				}
			});
		}
		view.startAnimation(mAnimationSet);
	}

	public void showInfoDialog(List<AccessServerBean> datas, RectBean bean) {
		if (datas == null || datas.isEmpty()) {
			return;
		}
		final View view = View.inflate(context, R.layout.dlg_info, null);
		final ExpandableListView exlv = (ExpandableListView) view.findViewById(R.id.exLv);
		// 去掉剪头
		exlv.setGroupIndicator(null);
		exlv.setAdapter(new MapSearchPositionContentAdapter(context, datas, bean));
		// 不可点击group
		exlv.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
		// 展开所有选项
		for (int i = 0; i < datas.size(); i++) {
			exlv.expandGroup(i);
		}

		final BottomDialog dialog = new BottomDialog(context);
		dialog.setTitle(bean.type + "  " + bean.obj_name);
		dialog.setView(view);
		dialog.setCanceledOnTouchOutside(true);
		dialog.hideButton();
		dialog.show();
		// 限制高度, 必须在show方法之后
		LayoutParams params = exlv.getLayoutParams();
		int maxHeight = ActivityUtils.getScreenHeight(context) / 2;
		if (datas.size() > 3) {
			params.height = maxHeight;
		}
		exlv.setLayoutParams(params);
	}

	public void requestServer(final RectBean bean) {
		RequestParams params = new RequestParams();
		params.add("id", bean.obj_id);
		params.add("ps_id", currentWarehouseData.psId);
		params.add("type", bean.type == Type.SPOT ? "4" : "3");
		new SimpleJSONUtil() {
			@Override
			public void handReponseJson(JSONObject json) {
			};

			@Override
			public void handReponseJsonArray(JSONArray array) {
				List<AccessServerBean> datas = GMapUtils.getAccessServer(array);
				showInfoDialog(datas, bean);
			};

		}.doGisGetArray(HttpUrlPath.getDocksParkingOccupancyDetailAndroid, params, context, progress);
	}

	private void showPopupWindow() {
		initPopupWindow();
		mPopupWindow.showAsDropDown(map_header_show);
	}

	private void hidePopupWindow() {
		if (mPopupWindow != null) {
			mPopupWindow.dismiss();
			mPopupWindow = null;
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		if (marker.getSnippet().equals("Door") || marker.getSnippet().equals("Spot")) {
			if (marker.getSnippet().equals("Door")) {
				for (RectBean door : oDoors) {
					if (door.obj_name.equals(marker.getTitle())) {
						requestServer(door);
						return;
					}
				}
			}
			if (marker.getSnippet().equals("Spot")) {
				for (RectBean spot : oSpots) {
					if (spot.obj_name.equals(marker.getTitle())) {
						requestServer(spot);
						return;
					}
				}
			}
			RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
			builder.setMessage("No executable task");
			builder.create().show();
			return;
		}
		for (final WebCamBean bean : webCams) {
			if (bean.port.equals(marker.getTitle())) {
				if (!ActivityUtils.isAppInstalled(this, "org.rtspplr.app")) {
					RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
					builder.setMessage("Download RTSP Player");
					builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							toDownloadPalyer();
						}
					});
					builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							toPalyer(getWebcamRtsp(bean));
						}
					});
					builder.create().show();
				} else {
					toPalyer(getWebcamRtsp(bean));
				}
				return;
			}
		}
	}

	private void toDownloadPalyer() {
		Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=org.rtspplr.app");
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}

	private void toPalyer(String url) {
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}

	// 生成摄像头rtsp地址
	private String getWebcamRtsp(WebCamBean webCam) {
		String rtsp = "rtsp://";
		if (!TextUtils.isEmpty(webCam.username) && !TextUtils.isEmpty(webCam.password)) {
			rtsp += webCam.username + ":" + webCam.password + "@";
		}
		rtsp += webCam.ip + ":" + webCam.port + "/h264/ch1/main/av_stream";
		return rtsp;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mainHandler.removeCallbacks(trackHandler);
	}

	@Override
	public void onMapClick(LatLng p) {
		doorOnClick(p);
		spotOnClick(p);
	}

	private void spotOnClick(LatLng p) {
		final MenuBean spotMenu = MenuBean.list.get(0);
		RectBean spot = null;
		if (!spotMenu.status || (spot = RectBean.findRect(p, spots)) == null) {
			zoneOnClick(p);
			return;
		}
		showMarker(spot);
	}

	private void doorOnClick(LatLng p) {
		final MenuBean doorMenu = MenuBean.list.get(1);
		RectBean door = null;
		if (!doorMenu.status || (door = RectBean.findRect(p, doors)) == null) {
			zoneOnClick(p);
			return;
		}
		showMarker(door);
	}

	private void zoneOnClick(LatLng p) {
		final MenuBean zoneMenu = MenuBean.list.get(3);
		RectBean zone = null;
		if (!zoneMenu.status || (zone = RectBean.findRect(p, zones)) == null) {
			return;
		}
		// UIHelper.showToast(zone.obj_name+".."+zone.obj_id);c
	}

	private void showMarker(RectBean bean) {
		if (currentZoomLevel < 20)
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bean.center, 20)); // 定位
		else
			mMap.moveCamera(CameraUpdateFactory.newLatLng(bean.center));
		mMarker.setVisible(true);
		mMarker.setPosition(bean.center);
		mMarker.setTitle(bean.obj_name);
		mMarker.setSnippet(bean.type == Type.DOOR ? "Door" : "Spot");
		mMarker.showInfoWindow();
		requestServerMarker(bean);
	}

	public void requestServerMarker(RectBean bean) {
		if (bean.type == Type.DOOR) {
			for (RectBean door : oDoors) {
				if (door.obj_name.equals(bean.obj_name)) {
					requestServer(door);
					return;
				}
			}
		}
		if (bean.type == Type.SPOT) {
			for (RectBean spot : oSpots) {
				if (spot.obj_name.equals(bean.obj_name)) {
					requestServer(spot);
					return;
				}
			}
		}
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setMessage("No executable task");
		builder.create().show();
		return;
	}

	private void hideMarker() {
		if (mMarker != null) {
			mMarker.setVisible(false);
		}
	}

	@Override
	public boolean onMarkerClick(final Marker marker) {
		// final Handler handler = new Handler();
		// final long start = SystemClock.uptimeMillis();
		// final long duration = 500;
		//
		// final Interpolator interpolator = new BounceInterpolator();
		//
		// handler.post(new Runnable() {
		// @Ovide
		// public void run() {
		// long elapsed = SystemClock.uptimeMillis() - start;
		// float t = Math.max(1 - interpolator.getInterpolation((float) elapsed
		// / duration), 0);
		// marker.setAnchor(0.5f, 1.0f + t);
		//
		// if (t > 0.0) {
		// // Post again 16ms later.
		// handler.postDelayed(this, 16);
		// }
		// }
		// });
		return false;
	}

	@Override
	public void onCameraChange(CameraPosition cp) {
		if (currentZoomLevel == cp.zoom) {
			return;
		}
		currentZoomLevel = cp.zoom;
		final MenuBean spotMenu = MenuBean.list.get(0);
		final MenuBean doorMenu = MenuBean.list.get(1);
		final MenuBean zoneMenu = MenuBean.list.get(3);
		setTextVisible(zones, zoneMenu.status, MAP_DEFAULT_LEVEL);
		setTextVisible(spots, spotMenu.status, MAP_DEFAULT_LEVEL);
		setTextVisible(doors, doorMenu.status, MAP_DEFAULT_LEVEL);
		updateWebCamRes(webCams);
		if (currentZoomLevel < MAP_DEFAULT_LEVEL)
			hideMarker();
	}

	// 点返回按钮、返回键时
	protected void onBackBtnOrKey() {
		if (isShowSearch) {
			listAnimation(false, searchListLayout);
			hideSearch();
			flag = true;
		} else {
			RewriteBuilderDialog.showSimpleDialog(this, "Exit?", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					GMapActivity.this.finish();
					overridePendingTransition(R.anim.push_from_right_out, R.anim.push_from_right_in);
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		onBackBtnOrKey();
	}

	@Override
	protected void noGoogleService() {
		// TODO Auto-generated method stub
		showErrorDialog(getString(R.string.no_google_play));
	}

	private void showErrorDialog(CharSequence msg) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		builder.setCancelable(false);
		builder.hideCancelBtn();
		builder.setMessage(msg);
		builder.setPositiveButton("EXIT", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				GMapActivity.this.finish();
			}
		});
		builder.create().show();
	}
}