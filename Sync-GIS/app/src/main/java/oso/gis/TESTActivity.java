package oso.gis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TESTActivity extends Activity {

	final String base_url = "http://192.168.1.15/Sync10/"; // 192.198.208.43/Sync10/
	final String cookie = "JSESSIONID=E82F8619480E716DF85C25CA94D96476;";
	final String ps_id = "1000005";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_main);
	}

	public void toMapOnClick(View v) {
		Intent intent = new Intent(this, GMapActivity.class);
		Bundle b = new Bundle();
		b.putString("ps_id", ps_id);
		b.putString("cookie", cookie);
		b.putString("base_url", base_url);
		intent.putExtras(b);
		startActivity(intent);
	}

}
