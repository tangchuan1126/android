package oso.gis;


import java.util.List;

import oso.gis.R;
import oso.gis.bean.RectBean;
import oso.gis.bean.WarehouseBean;
import oso.gis.bean.WebCamBean;
import oso.gis.util.GMapUtils;
import utility.BitmapUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

public abstract class BaseMapActivity extends FragmentActivity {

	/**
	 * Note that this may be null if the Google Play services APK is not
	 * available.
	 */
	protected GoogleMap mMap;
	protected Context context;

	public static Handler mainHandler = new Handler();

	public enum Type {
		DOOR, SPOT
	}

	protected final float MAP_DEFAULT_LEVEL = 17;
	protected float currentZoomLevel = MAP_DEFAULT_LEVEL;

	public static final double RADIUS = 200;

	public static final int COLOR_OCCUPY = 0xffbb0000;
	public static final int COLOR_UNOCCUPY = 0xff92fbbf;

	protected final int delayMillis = 20;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		context = this;
	}

	/**
	 * Sets up the map if it is possible to do so (i.e., the Google Play
	 * services APK is correctly installed) and the map has not already been
	 * instantiated.. This will ensure that we only ever call
	 * {@link #setUpMap()} once when {@link #mMap} is not null.
	 * <p>
	 * If it isn't installed {@link SupportMapFragment} (and
	 * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt
	 * for the user to install/update the Google Play services APK on their
	 * device.
	 * <p>
	 * A user can return to this FragmentActivity after following the prompt and
	 * correctly installing/updating/enabling the Google Play services. Since
	 * the FragmentActivity may not have been completely destroyed during this
	 * process (it is likely that it would only be stopped or paused),
	 * {@link #onCreate(Bundle)} may not be called again so we should call this
	 * method in {@link #onResume()} to guarantee that it will be called.
	 */
	protected void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			} else {
				noGoogleService();
			}
		}
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera. In this case, we just add a marker near Africa.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	protected abstract void setUpMap();

	/**
	 * 无谷歌服务
	 */
	protected abstract void noGoogleService();

	/**
	 * 是否显示多边形
	 */
	protected void setPolygonVisible(List<RectBean> rects, boolean visible, float minShowText) {
		if (rects != null) {
			for (RectBean rect : rects) {
				if (rect.polygon != null)
					rect.polygon.setVisible(visible);
				if (rect.text != null)
					rect.text.setVisible(visible && currentZoomLevel > minShowText);
			}
		}
	}

	/**
	 * 是否显示文字
	 */
	protected void setTextVisible(List<RectBean> rects, boolean visible, float minShow) {
		if (rects != null) {
			for (RectBean rect : rects) {
				if (rect.text != null)
					rect.text.setVisible(visible && currentZoomLevel > minShow);
			}
		}
	}

	/**
	 * 是否显示摄像头
	 */
	protected void setWebCamVisible(List<WebCamBean> webCams, boolean visible) {
		if (webCams != null) {
			for (WebCamBean bean : webCams) {
				if (bean.marker != null)
					bean.marker.setVisible(visible);
			}
		}
	}

	/**
	 * 占用状态
	 */
	protected void setOccupyStatus(List<RectBean> rects, boolean isOccupy) {
		if (rects == null)
			return;

		if (isOccupy) {
			for (RectBean bean : rects) {
				bean.polygon.setFillColor(COLOR_OCCUPY);
			}
		} else {
			for (RectBean bean : rects) {
				bean.polygon.setFillColor(COLOR_UNOCCUPY);
			}
		}
	}

	/**
	 * 更新Webcam图标
	 */
	protected void updateWebCamRes(List<WebCamBean> webCams) {
		if (webCams == null || webCams.size() == 0) {
			return;
		}
		for (WebCamBean bean : webCams) {
			if (bean.marker != null)
				if (currentZoomLevel <= 17) {
					bean.marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.webcam_16));
				} else if (currentZoomLevel >= 19) {
					bean.marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.webcam_64));
				} else {
					bean.marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.webcam_32));
				}
		}
	}

	/**
	 * 绘制仓库的方法
	 */
	protected void drawWarehouse(WarehouseBean bean) {
		if (bean.latLng.size() == 0)
			return;
		PolygonOptions options = new PolygonOptions().addAll(bean.latLng).fillColor(Color.WHITE).strokeColor(Color.GRAY).strokeWidth(5).zIndex(0);
		bean.polygon = mMap.addPolygon(options);
	}

	/**
	 * 绘制Zones的方法
	 * 
	 * @return
	 */
	protected List<RectBean> drawZones(String ps_id) {
		List<RectBean> datas = GMapUtils.getZoneData(ps_id);
		for (final RectBean bean : datas) {
			if (bean.latLng.size() != 0)
				mainHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						bean.polygon = drawZone(bean.latLng);
						bean.text = drawText(bean);
					}
				}, delayMillis);
		}
		return datas;
	}

	private Polygon drawZone(List<LatLng> rect) {
		if (rect.size() == 0)
			return null;
		PolygonOptions options = new PolygonOptions().addAll(rect).fillColor(Color.WHITE).strokeColor(Color.GRAY).strokeWidth(3).zIndex(1);
		return mMap.addPolygon(options);
	}

	/**
	 * 绘制门与停车位的方法
	 */
	protected List<RectBean> drawDoorsOrSpots(final Type type, String ps_id) {
		List<RectBean> datas = type == Type.DOOR ? GMapUtils.getDocksData(ps_id) : GMapUtils.getParkingData(ps_id);
		for (final RectBean bean : datas) {
			mainHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					bean.type = type;
					bean.polygon = drawDoorOrSpot(bean.latLng);
					bean.text = drawText(bean, 0xffffffe1, 2);
				}
			}, delayMillis);
		}
		return datas;
	}

	private Polygon drawDoorOrSpot(final List<LatLng> rect) {
		PolygonOptions options = new PolygonOptions().addAll(rect).visible(false).strokeWidth(2).zIndex(2).fillColor(COLOR_UNOCCUPY)
				.strokeColor(0xff6bd571);
		return mMap.addPolygon(options);
	}

	/**
	 * 绘制摄像头的方法
	 */
	protected List<WebCamBean> drawWebCams(String ps_id) {
		List<WebCamBean> datas = GMapUtils.getWebCamData(ps_id);
		for (final WebCamBean bean : datas) {
			mainHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (bean.latLng != null)
						bean.marker = drawWebCam(bean.latLng, bean.port, "WebCam");
				}
			}, delayMillis);
		}
		return datas;
	}

	private Marker drawWebCam(final LatLng latLng, final String title, final String snippet) {
		return mMap.addMarker(new MarkerOptions().position(latLng).visible(false).title(title).snippet(snippet)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.webcam_32)));
	}

	/**
	 * 绘制Mark的方法
	 */
	protected Marker drawMark(final LatLng latLng, final String title) {
		return mMap.addMarker(new MarkerOptions().position(latLng).title(title).visible(false));
	}

	private void drawMarks(List<RectBean> list, String title) {
		for (RectBean bean : list) {
			drawMark(bean.center, title);
		}
	}

	/**
	 * 绘制文字的方法
	 */
	protected GroundOverlay drawText(RectBean bean) {
		return drawText(bean, 0, 1);
	}

	protected GroundOverlay drawText(RectBean bean, int color, float scale) {
		MapText mt = new MapText(bean, color);
		return drawBitmap(mt.descriptor, bean.center, mt.width / scale, mt.height / scale, 3, false);
	}

	protected GroundOverlay drawBitmap(BitmapDescriptor descriptor, LatLng NEWARK, float width, float height, int layer, boolean visible) {
		return mMap.addGroundOverlay(new GroundOverlayOptions().image(descriptor).position(NEWARK, width, height).zIndex(layer).visible(visible));
	}

	/**
	 * 绘制圆形
	 * 
	 * @return
	 */
	protected Marker drawCircle(LatLng center, double radius) {
		return mMap.addMarker(new MarkerOptions().position(toRadiusLatLng(center, radius)).draggable(true)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_camera)));
	}

	private static LatLng toRadiusLatLng(LatLng center, double radius) {
		double radiusAngle = Math.toDegrees(radius / RADIUS) / Math.cos(Math.toRadians(center.latitude));
		return new LatLng(center.latitude, center.longitude + radiusAngle);
	}

	/**
	 * 绘制摄像头监控范围的方法（扇形）
	 * 
	 * @return
	 */
	protected Polygon drawArc() {
		return null;
	};

	protected class CustomInfoWindowAdapter implements InfoWindowAdapter {

		// private final View mWindow;
		private final View mContents;

		CustomInfoWindowAdapter() {
			// mWindow =
			// getLayoutInflater().inflate(R.layout.custom_info_window, null);
			mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
		}

		// @Override
		// public View getInfoWindow(Marker marker) {
		// render(marker, mWindow);
		// return mWindow;
		// }

		@Override
		public View getInfoWindow(Marker arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public View getInfoContents(Marker marker) {
			final String titleStr = marker.getTitle();
			final String snippetStr = marker.getSnippet();
			TextView titleTv = ((TextView) mContents.findViewById(R.id.title));
			TextView snippetTv = ((TextView) mContents.findViewById(R.id.snippet));
			ImageView iv = (ImageView) mContents.findViewById(R.id.badge);

			titleTv.setText(titleStr);
			snippetTv.setText(snippetStr);
			if (snippetStr.equals("WebCam")) {
				iv.setImageResource(R.drawable.ic_play_over_video);
			} else {
				iv.setImageResource(R.drawable.ic_info);
			}

			return mContents;
		}

	}

	private class MapText {

		public MapText(RectBean rect, int color) {
			imgCache = new TextView(context);
			imgCache.setText(rect.obj_name);
			imgCache.setTextColor(color != 0 ? color : 0xff999999);
			Bitmap bm = BitmapUtils.getBitmapFromView(imgCache);
			descriptor = BitmapDescriptorFactory.fromBitmap(bm);
			setWH();
		}

		private void setWH() {
			width = imgCache.getText().toString().length() * 2;
		}

		public String getText() {
			return imgCache.getText().toString();
		}

		public BitmapDescriptor descriptor;
		public int width = 10;
		public int height = 5;
		private TextView imgCache;
	}

}
