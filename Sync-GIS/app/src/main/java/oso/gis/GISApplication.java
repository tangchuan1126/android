package oso.gis;

import org.json.JSONException;
import org.json.JSONObject;

import utility.FileUtils;
import utility.HttpUrlPath;
import utility.StringUtil;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

/**
 * 初始化Application
 * 
 * @author jiang
 * 
 */
public class GISApplication extends Application {

	private static GISApplication instance;
	private static Handler handler = new Handler(); // 公共handler

	public static GISApplication getThis() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		initspf(getApplicationContext(), "ps_gis");
		HttpUrlPath.initBaseUrls();
	}
	
	public static SharedPreferences initspf(Context context, String spfname) {
		if (sp == null)
			sp = context.getSharedPreferences(spfname, Context.MODE_PRIVATE);
		return sp;
	}

	public Handler getHandler() {
		return handler;
	}

	private static SharedPreferences sp;

	/**
	 * 获取地图zip包版本号
	 * 
	 * @param value
	 */
	public static void setMapDataVersion(String path) {
		if (!FileUtils.isFileExist(path)) {
			sp.edit().putInt("version", -11).commit();
			return;
		}
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		JSONObject json = null;
		try {
			json = new JSONObject(jsonStr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		int version = StringUtil.getJsonInt(json, "verson");
		sp.edit().putInt("version", version).commit();
	}

	public static int getMapDataVersion() {
		return sp.getInt("version", 0);
	}

}
