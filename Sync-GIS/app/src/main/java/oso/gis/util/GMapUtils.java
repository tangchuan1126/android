package oso.gis.util;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import oso.gis.bean.AccessServerBean;
import oso.gis.bean.OccupyBean;
import oso.gis.bean.RectBean;
import oso.gis.bean.WebCamBean;

import com.google.gson.Gson;

import utility.FileUtils;
import utility.StringUtil;
import android.os.Environment;

public class GMapUtils {
	// data路径
	public static final String baseMapPath = Environment.getExternalStorageDirectory().getPath() + "/vvme/files";
	public static final String zipFilePath = GMapUtils.baseMapPath + File.separator + "storageDatas.zip";
	public static final String unZipPath = baseMapPath + File.separator + "mapDatas"; // 解压路径
	// 文件名
	public static final String FILE_VERSION = "version.txt";
	public static final String FILE_POSTFIX_AREAS = "_areas.txt";
	public static final String FILE_POSTFIX_BASE = "_base.txt";
	public static final String FILE_POSTFIX_DOCKS = "_docks.txt";
	public static final String FILE_POSTFIX_LOCATIONS = "_locations.txt";
	public static final String FILE_POSTFIX_PARKING = "_parking.txt";
	public static final String FILE_POSTFIX_STAGINS = "_stagins.txt";
	public static final String FILE_POSTFIX_WEBCAMS = "_webcams.txt";

	/**
	 * 获取本地Stagins的数据
	 * 
	 * @return
	 */
	public static List<RectBean> getZoneData(String ps_id) {
		String path = unZipPath + File.separator + ps_id + FILE_POSTFIX_AREAS;
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		return RectBean.parseDatas(jsonStr.toString());
	}

	/**
	 * 获取本地Docks的数据
	 * 
	 * @return
	 */
	public static List<RectBean> getDocksData(String ps_id) {
		String path = unZipPath + File.separator + ps_id + FILE_POSTFIX_DOCKS;
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		return RectBean.parseDatas(jsonStr.toString());
	}

	/**
	 * 获取本地Parking的数据
	 * 
	 * @return
	 */
	public static List<RectBean> getParkingData(String ps_id) {
		String path = unZipPath + File.separator + ps_id + FILE_POSTFIX_PARKING;
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		return RectBean.parseDatas(jsonStr.toString());
	}

	/**
	 * 获取摄像头数据
	 * 
	 * @param json
	 * @return
	 */
	public static List<WebCamBean> getWebCamData(String ps_id) {
		String path = unZipPath + File.separator + ps_id + FILE_POSTFIX_WEBCAMS;
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		return WebCamBean.parseDatas(jsonStr.toString());
	}

	/**
	 * 获取门与停车位的占用情况数据
	 * 
	 * @param json
	 * @param type
	 * @return
	 */
	public static List<OccupyBean> getOccupyData(final JSONObject json) {
		List<OccupyBean> datas = new ArrayList<OccupyBean>();
		JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "objs");
		if (dataArray != null) {
			for (int i = 0; i < dataArray.length(); i++) {
				JSONObject joZone = dataArray.optJSONObject(i);
				OccupyBean bean = new OccupyBean();
				bean.obj_id = joZone.optString("obj_id");
				bean.obj_type = joZone.optString("obj_type");
				datas.add(bean);
			}
		}
		return datas;
	}

	/**
	 * 获取门与停车位的任务
	 * 
	 * @param json
	 * @param type
	 * @return
	 */
	public static List<AccessServerBean> getAccessServer(final JSONArray dataArray) {
		List<AccessServerBean> datas = new ArrayList<AccessServerBean>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject json = dataArray.optJSONObject(i);
			AccessServerBean bean = getAccessServer(json);
			datas.add(bean);
		}
		return datas;
	}

	public static AccessServerBean getAccessServer(final JSONObject json) {
		Gson gson = new Gson();
		return gson.fromJson(json.toString(), AccessServerBean.class);
	}
}
