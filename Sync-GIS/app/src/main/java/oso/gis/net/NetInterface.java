package oso.gis.net;

import java.net.SocketTimeoutException;

import org.apache.http.Header;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONObject;

import oso.gis.GISApplication;

import utility.AlertUi;
import utility.RewriteDialog;
import utility.StringUtil;
import utility.UIHelper;
import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

/**
 * @author jiang
 * 
 * @date 2015-05-20 18:37:46
 */
public class NetInterface {

	public static final AsyncHttpClient client = new AsyncHttpClient();

	public static final String NULL_ARRAY = "[]";
	public static final int REQ_SUCCESS = 1;
	public static final int REQ_ERROR = 90;
	public static final boolean isPrintLog = true;

	static {
		client.setTimeout(30 * 1000);
	}

	public static abstract class SyncJsonHandler extends JsonHttpResponseHandler {

		public Context mContext = null;

		public RewriteDialog progressDialog = null;

		public boolean isShowDialog = true;

		public SyncJsonHandler(Context c) {
			mContext = c;
		}

		public SyncJsonHandler(Context c, boolean isShowDialog) {
			mContext = c;
			this.isShowDialog = isShowDialog;
		}

		public abstract void handReponseJson(JSONObject json);

		public void handFail(int statusCode, Header[] headers, JSONObject json) {
			if (json != null)
				System.err.println("errorResponse= " + json.toString());
		};

		@Override
		public void onStart() {
			super.onStart();
			if (isShowDialog) {
				progressDialog = RewriteDialog.createDialog(mContext, 1);
				progressDialog.setTitle("");
				progressDialog.setMessage("");
				progressDialog.setCancelable(true);
				progressDialog.show();
			}
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
			super.onSuccess(statusCode, headers, json);
			if (progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			if (isChecked(statusCode, headers, json))
				handReponseJson(json);
			else
				handFail(statusCode, headers, json);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
			super.onFailure(statusCode, headers, throwable, errorResponse);
			if (progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			handFail(statusCode, headers, errorResponse);
			// 超时
			if (throwable instanceof SocketTimeoutException || throwable instanceof ConnectTimeoutException)
				UIHelper.showToast(mContext, "Connection Timeout!", Toast.LENGTH_LONG).show();
			else
				UIHelper.showToast(mContext, "Request Fail!", Toast.LENGTH_LONG).show();
		}

		public boolean isChecked(int statusCode, Header[] headers, JSONObject json) {
			if (statusCode != 200) {
				return false;
			}
			if (json.optInt("err") == REQ_ERROR) {
				return false;
			}
			if (json.optInt("ret") == REQ_SUCCESS) {
				return true;
			} else {
				AlertUi.showAlertBuilder(mContext, "System Error!");
				return false;
			}
		}
	}

	public void initNetParams(String url, RequestParams params) {
		Context c = GISApplication.getThis();
//		Goable.initGoable(c);
//		StringUtil.setLoginInfoParams(params);
		PersistentCookieStore myCookieStore = new PersistentCookieStore(c);
		client.setCookieStore(myCookieStore);
		if (isPrintLog)
			System.out.println(url + "?" + params.toString());
	}

}
