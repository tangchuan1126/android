package oso.gis.net;

import utility.HttpUrlPath;

import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

public class NetConnection_GIS extends NetInterface {

    private static NetConnection_GIS _instantce;

    /**
     * 获取网络请求单例
     */
    public static NetConnection_GIS getThis() {
        if (_instantce == null)
            _instantce = new NetConnection_GIS();
        return _instantce;
    }

    /**
     * 收货时创建商品
     *
     * @param itemId
     */
    public RequestHandle reqProduct_data(SyncJsonHandler handler, String companyId, String customerId, String itemId) {
        final String url = HttpUrlPath.getStorageBaseData;
        RequestParams params = new RequestParams();
        params.add("companyId", companyId);
        params.add("customerId", customerId);
        params.add("itemId", itemId);
        initNetParams(url, params);
        return client.get(url, params, handler);
    }
}
