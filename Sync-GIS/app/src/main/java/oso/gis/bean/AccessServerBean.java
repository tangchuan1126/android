package oso.gis.bean;

import java.util.List;

public class AccessServerBean {

	@Override
	public String toString() {
		return "AccessServerBean [relation_id=" + relation_id + ", resources_id=" + resources_id + ", resources_type=" + resources_type
				+ ", equipment_type=" + equipment_type + ", module_id=" + module_id + ", rel_type=" + rel_type + ", equipment_status="
				+ equipment_status + ", equipment_number=" + equipment_number + ", tasks=" + tasks + ", occupy_status=" + occupy_status
				+ ", relation_type=" + relation_type + ", check_in_time=" + check_in_time + "]";
	}
	public String relation_id;
	public String resources_id;
	public String resources_type;
	public String equipment_type;
	public String module_id;
	public String rel_type;
	public String equipment_status;
	public String equipment_number;
	public List<TakcsBean> tasks;
	public String occupy_status;
	public String relation_type;
	public String check_in_time;
 
	public class TakcsBean {
		public String number_type;
		public String number;
		public String number_status;
	}
	
}
