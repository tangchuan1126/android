package oso.gis.bean;

import java.util.ArrayList;
import java.util.List;

import oso.gis.R;

public class MenuBean {

	public static List<MenuBean> list;
	public static final String SPOT = "Spot";
	public static final String DOOR = "Door";
	public static final String WEBCAM = "WebCam";
	public static final String AREA = "Area";

	public MenuBean(String title, int icon, boolean status) {
		this.title = title;
		this.icon = icon;
		this.status = status;
	}

	public static List<MenuBean> initMenus() {
		if (list == null) {
			list = new ArrayList<MenuBean>();
			list.add(new MenuBean(SPOT, R.drawable.ic_stop, false));
			list.add(new MenuBean(DOOR, R.drawable.ic_door, false));
			list.add(new MenuBean(WEBCAM, R.drawable.ic_photo, false));
			list.add(new MenuBean(AREA, R.drawable.icon_maps, true));
		} else {
			initStatus();
		}
		return list;
	}

	private static void initStatus() {
		for (MenuBean bean : list) {
			if (bean.title.equals(AREA)) {
				bean.status = true;
			} else {
				bean.status = false;
			}
		}
	}

	public String title;
	public int icon;
	public boolean status;
}
