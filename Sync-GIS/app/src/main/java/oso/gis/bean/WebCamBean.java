package oso.gis.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utility.StringUtil;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class WebCamBean {

	public String username;
	public String password;
	public String ip;
	public String port;

	public LatLng latLng;
	public Marker marker;

	/**
	 * 解析数据
	 * 
	 * @param jsonStr
	 * @return
	 */
	public static List<WebCamBean> parseDatas(final String jsonStr) {
		JSONObject json = null;
		try {
			json = new JSONObject(jsonStr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return parseDatas(json);
	}

	public static List<WebCamBean> parseDatas(JSONObject json) {
		List<WebCamBean> list = new ArrayList<WebCamBean>();
		JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "datas");
		if (dataArray != null && dataArray.length() != 0) {
			for (int i = 0; i < dataArray.length(); i++) {
				WebCamBean bean = new WebCamBean();
				JSONObject joZone = dataArray.optJSONObject(i);
				bean.username = joZone.optString("username");
				bean.password = joZone.optString("password");
				bean.ip = joZone.optString("ip");
				bean.port = joZone.optString("port");
				bean.latLng = RectBean.formatPoint(joZone.optString("latlng"));
				list.add(bean);
			}
		}
		return list;
	}
}
