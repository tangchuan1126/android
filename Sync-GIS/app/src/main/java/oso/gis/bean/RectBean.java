package oso.gis.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oso.gis.BaseMapActivity.Type;

import utility.StringUtil;

import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;

public class RectBean {

	public String obj_id; // id
	public String obj_name; // name
	public List<LatLng> latLng; // 矩形坐标
	public LatLng center; // 矩形中点

	public Polygon polygon;
	public GroundOverlay text;

	public Type type;

	// public boolean isOccupy;

	/**
	 * 返回矩形中点
	 */
	public static LatLng getCenterLatLng(List<LatLng> rect) {
		if (rect == null || rect.size() == 0)
			return null;
		double centerX = (rect.get(0).longitude + rect.get(2).longitude) / 2;
		double centerY = (rect.get(0).latitude + rect.get(2).latitude) / 2;
		return new LatLng(centerY, centerX);
	}

	/**
	 * 判断一个点是否在一个矩形内
	 */
	public static boolean containsLocation(LatLng p, List<LatLng> rect) {
		return pointOnLine(p, rect.get(0), rect.get(1)) < 0 && pointOnLine(p, rect.get(1), rect.get(2)) < 0
				&& pointOnLine(p, rect.get(2), rect.get(3)) < 0 && pointOnLine(p, rect.get(3), rect.get(0)) < 0;
	}

	private static double pointOnLine(LatLng p, LatLng a, LatLng b) {
		return (p.latitude - a.latitude) * (p.longitude - b.longitude) - (p.latitude - b.latitude) * (p.longitude - a.longitude);
	}

	/**
	 * 查找矩形
	 */
	public static RectBean findRect(LatLng p, List<RectBean> datas) {
		if (datas != null && p != null) {
			for (RectBean bean : datas) {
				if (bean.polygon != null && containsLocation(p, bean.polygon.getPoints())) {
					return bean;
				}
			}
		}
		return null;
	}

	/**
	 * 解析数据
	 * 
	 * @param jsonStr
	 * @return
	 */
	public static List<RectBean> parseDatas(final String jsonStr) {
		JSONObject json = null;
		try {
			json = new JSONObject(jsonStr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return parseDatas(json);
	}

	/**
	 * 解析数据
	 * 
	 * @param json
	 * @return
	 */
	public static List<RectBean> parseDatas(final JSONObject json) {
		List<RectBean> list = new ArrayList<RectBean>();
		JSONArray dataArray = StringUtil.getJsonArrayFromJson(json, "datas");
		if (dataArray != null && dataArray.length() != 0) {
			for (int i = 0; i < dataArray.length(); i++) {
				RectBean bean = new RectBean();
				JSONObject joZone = dataArray.optJSONObject(i);
				bean.obj_id = joZone.optString("obj_id");
				bean.obj_name = joZone.optString("obj_name");
				bean.latLng = formatRect(joZone.optString("latlng"));
				bean.center = getCenterLatLng(bean.latLng);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * 解析一个矩形
	 * 
	 * @param rectStr
	 *            -117.957055304204,34.0143686892657,0.0
	 *            -117.954422045504,34.0134632553511,0.0
	 *            -117.953502047802,34.015311352989,0.0
	 *            -117.956135306512,34.0162167671938,0.0
	 * @return
	 */
	public static List<LatLng> formatRect(String rectStr) {
		List<LatLng> list = new ArrayList<LatLng>();
		String[] data = rectStr.split(SEPARATOR1);
		if (data.length != 0) {
			for (String d : data) {
				if (formatPoint(d) != null)
					list.add(formatPoint(d));
			}
		}
		return list;
	}

	/**
	 * 解析点坐标
	 * 
	 * @param -117.957055304204,34.0143686892657
	 * @return
	 */
	public static LatLng formatPoint(String latlngStr) {
		String[] ds = latlngStr.split(SEPARATOR2);
		if (ds.length != 2) {
			return null;
		}
		return new LatLng(Double.parseDouble(ds[1]), Double.parseDouble(ds[0]));
	}

	public static final String SEPARATOR1 = ",0.0 ";
	public static final String SEPARATOR2 = ",";
}
