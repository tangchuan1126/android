package oso.gis.bean;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import oso.gis.util.GMapUtils;

import utility.FileUtils;
import utility.StringUtil;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;

public class WarehouseBean {

	public String psId;
	public String psName;
	public List<LatLng> latLng;

	public LatLng center;
	public Polygon polygon;

	/**
	 * 获取仓库名称
	 */
	public static List<WarehouseBean> getAllWarehouseDatas() {
		List<WarehouseBean> datas = new ArrayList<WarehouseBean>();
		String[] ids = getWarehouseIds();
		for (String ps_id : ids) {
			WarehouseBean bean = getWarehouseData(ps_id);
			if (bean != null)
				datas.add(bean);
		}
		return datas;
	}

	/**
	 * 获取仓库名字数组
	 * 
	 * @return
	 */
	public static String[] getWarehouseIds() {
		List<String> psIds = new ArrayList<String>();
		File[] files = new File(GMapUtils.unZipPath).listFiles();
		for (File file : files) {
			if (file.toString().contains("/") && file.toString().contains("_")) {
				psIds.add(file.toString().substring(file.toString().lastIndexOf("/") + 1, file.toString().lastIndexOf("_")));
			}
		}
		return StringUtil.removeDuplicateWithOrder(psIds);
	}

	/**
	 * 获取本地仓库的数据
	 * 
	 * @return
	 */
	public static WarehouseBean getWarehouseData(String ps_id) {
		WarehouseBean data = new WarehouseBean();
		String path = GMapUtils.unZipPath + File.separator + ps_id + GMapUtils.FILE_POSTFIX_BASE;
		if (!FileUtils.isFileExist(path)) {
			return null;
		}
		StringBuilder jsonStr = FileUtils.readFile(path, "UTF-8");
		JSONObject json = null;
		try {
			json = new JSONObject(jsonStr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		JSONObject datasJo = StringUtil.getJsonObjectFromJSONObject(json, "datas");
		data.latLng = formatWarehouse(StringUtil.getJsonString(datasJo, "latlng"));
		data.psId = ps_id;
		data.psName = StringUtil.getJsonString(datasJo, "name");
		data.center = getCenterLatLng(data.latLng);
		return data;
	}

	public static List<LatLng> formatWarehouse(String rectStr) {
		List<LatLng> list = new ArrayList<LatLng>();
		String[] data = rectStr.split(RectBean.SEPARATOR1);
		if (data.length != 0) {
			for (String d : data) {
				if (RectBean.formatPoint(d) != null)
					list.add(RectBean.formatPoint(d));
			}
			if (RectBean.formatPoint(data[0]) != null)
				list.add(RectBean.formatPoint(data[0]));

		}
		return list;
	}

	/**
	 * 返回仓库中点
	 */
	public static LatLng getCenterLatLng(List<LatLng> rect) {
		double centerX = (rect.get(0).longitude + rect.get(2).longitude) / 2;
		double centerY = (rect.get(0).latitude + rect.get(2).latitude) / 2;
		return new LatLng(centerY, centerX);
	}

	public static double getCenterX(List<LatLng> rect) {
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (LatLng loc : rect) {
			max = max > loc.longitude ? max : loc.longitude;
			min = min < loc.longitude ? min : loc.longitude;
		}
		return (max + min) / 2;
	}

	public static double getCenterY(List<LatLng> rect) {
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (LatLng loc : rect) {
			max = max > loc.latitude ? max : loc.latitude;
			min = min < loc.latitude ? min : loc.latitude;
		}
		return (max + min) / 2;
	}
}
