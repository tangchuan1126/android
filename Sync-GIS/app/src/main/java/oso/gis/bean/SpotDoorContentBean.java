package oso.gis.bean;

import java.util.ArrayList;
import java.util.List;

public class SpotDoorContentBean {
 
	public String title;
	public String name;
	
	public SpotDoorContentBean(String title, String name) {
		this.title = title;
		this.name = name;
	}

	public static List<SpotDoorContentBean> getSpotDoorContent() {
		List<SpotDoorContentBean> list = new ArrayList<SpotDoorContentBean>();
		list.add(new SpotDoorContentBean("Spot", "12"));
		list.add(new SpotDoorContentBean("Door", "12"));
		return list;
	}
	
}
