package oso.gis.bean;

import java.util.ArrayList;
import java.util.List;

public class MapSearchPositionContentBean {
	
	public String name;
	public String load;
	
	public MapSearchPositionContentBean(String name, String load) {
		this.name = name;
		this.load = load;
	}
	
	public static List<MapSearchPositionContentBean> getSearchPositionList(){
		List<MapSearchPositionContentBean> list = new ArrayList<MapSearchPositionContentBean>();
		list.add(new MapSearchPositionContentBean("Tom", "12121212"));
		list.add(new MapSearchPositionContentBean("Janet", "12121212"));
		return list;
	}
	
}
