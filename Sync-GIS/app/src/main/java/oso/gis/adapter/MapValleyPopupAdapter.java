package oso.gis.adapter;


import java.util.List;

import oso.gis.R;
import oso.gis.bean.WarehouseBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MapValleyPopupAdapter extends BaseAdapter {

	private Context context;
	public List<WarehouseBean> valleyList;

	public MapValleyPopupAdapter(Context context, List<WarehouseBean> valleyList) {
		this.context = context;
		this.valleyList = valleyList;
	}

	@Override
	public int getCount() {
		return valleyList == null ? 0 : valleyList.size();
	}

	@Override
	public Object getItem(int position) {
		return valleyList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View converView, ViewGroup viewGroup) {
		ValleyHolder h;
		if (converView == null) {
			h = new ValleyHolder();
			converView = View.inflate(context, R.layout.act_google_map_valley_popup, null);
			h.tv_title = (TextView) converView.findViewById(R.id.tv_title);
			converView.setTag(h);
		} else {
			h = (ValleyHolder) converView.getTag();
		}
		WarehouseBean bean = valleyList.get(position);
		h.tv_title.setText(bean.psName);
		return converView;
	}

	class ValleyHolder {
		TextView tv_title;
	}
}
