package oso.gis.adapter;


import java.util.ArrayList;
import java.util.List;

import oso.gis.R;
import oso.gis.bean.MenuBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class GoogleMapExpandableListAdapter extends BaseExpandableListAdapter {

	private Context context;
	List<List<MenuBean>> datas;

	public GoogleMapExpandableListAdapter(Context context) {
		this.context = context;
		datas = new ArrayList<List<MenuBean>>();
		datas.add(MenuBean.initMenus());
	}

	@Override
	public int getGroupCount() {
		return datas.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return datas.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return datas.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return datas.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View converView, ViewGroup parent) {
		TitleHolder titleHolder = null;
		if (converView != null) {
			titleHolder = (TitleHolder) converView.getTag();
		} else {
			titleHolder = new TitleHolder();
			converView = View.inflate(context, R.layout.act_google_map_title, null);
			titleHolder.title_content = (TextView) converView.findViewById(R.id.tv_title);
			converView.setTag(titleHolder);
		}
		titleHolder.title_content.setText("Layer");
		return converView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ContentHolder contentHolder = null;
		if (convertView != null) {
			contentHolder = (ContentHolder) convertView.getTag();
		} else {
			contentHolder = new ContentHolder();
			convertView = View.inflate(context, R.layout.act_google_map_content, null);
			contentHolder.content_icon = (ImageView) convertView.findViewById(R.id.iv_content_icon);
			contentHolder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
			contentHolder.check_box = (CheckBox) convertView.findViewById(R.id.content_check_box);
			convertView.setTag(contentHolder);
		}

		final MenuBean bean = datas.get(groupPosition).get(childPosition);

		contentHolder.tv_content.setText(bean.title);
		contentHolder.content_icon.setImageResource(bean.icon);
		contentHolder.check_box.setChecked(bean.status);

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}

class TitleHolder {
	ImageView title_icon;
	TextView title_content;
	ImageView title_image;
}

class ContentHolder {
	ImageView content_icon;
	TextView tv_content;
	CheckBox check_box;
}
