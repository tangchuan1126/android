package oso.gis.adapter;

import java.util.List;

import key.EntryDetailNumberStateKey;
import key.ModuleKey;
import oso.gis.R;
import oso.gis.R.drawable;
import oso.gis.bean.AccessServerBean;
import oso.gis.bean.RectBean;
import oso.gis.bean.AccessServerBean.TakcsBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MapSearchPositionContentAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<AccessServerBean> datas;

	public MapSearchPositionContentAdapter(Context context, List<AccessServerBean> list, RectBean bean) {
		this.context = context;
		this.datas = list;
	}

	@Override
	public int getGroupCount() {
		return datas == null ? 0 : datas.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return datas.get(groupPosition).tasks.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return datas.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return datas.get(groupPosition).tasks.get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	Holder h;

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View converView, ViewGroup parent) {
		AccessServerBean bean = datas.get(groupPosition);
		if (converView != null) {
			h = (Holder) converView.getTag();
		} else {
			h = new Holder();
			converView = View.inflate(context, R.layout.act_google_map_search_dialog_header, null);
			h.title_icon = (ImageView) converView.findViewById(R.id.map_search_position_title_icon);
			h.map_search_search_position_title = (TextView) converView.findViewById(R.id.map_search_search_position_title);
			h.content_title_bg = (LinearLayout) converView.findViewById(R.id.map_content_title_bg);
			converView.setTag(h);
		}
		if (bean.equipment_type.equals("tractor")) {
			h.title_icon.setImageResource(R.drawable.ic_tractor);
		} else if (bean.equipment_type.equals("container")) {
			h.title_icon.setImageResource(R.drawable.ic_trailer);
		}
		h.map_search_search_position_title.setText(bean.equipment_number);
		return converView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View converView, ViewGroup parent) {
		SearchPositionContentHolder holder;
		TakcsBean bean = datas.get(groupPosition).tasks.get(childPosition);
		if (converView != null) {
			holder = (SearchPositionContentHolder) converView.getTag();
		} else {
			holder = new SearchPositionContentHolder();
			converView = View.inflate(context, R.layout.act_google_map_search_dialog_content, null);
			holder.map_search_position_content_load = (TextView) converView.findViewById(R.id.map_search_position_content_load);
			holder.map_search_position_content_date = (TextView) converView.findViewById(R.id.map_search_position_content_date);
			holder.map_search_position_content_right = (TextView) converView.findViewById(R.id.map_search_position_content_right);
			holder.map_content_line = (ImageView) converView.findViewById(R.id.map_content_line);
			holder.map_content_layout = (LinearLayout) converView.findViewById(R.id.map_content_layout);
			converView.setTag(holder);
		}
		if (h != null) {
			if (bean.number_type.equals("0") || bean.number_type == null) {
				holder.map_content_layout.setVisibility(View.GONE);
				h.content_title_bg.setBackgroundResource(drawable.map_top_bg);
				holder.map_search_position_content_load.setText("");
				holder.map_search_position_content_date.setText("");
				holder.map_search_position_content_right.setText("");
				// holder.map_content_line.setVisibility(View.GONE);
			} else {
				h.content_title_bg.setBackgroundResource(drawable.map_top);
				holder.map_content_layout.setVisibility(View.VISIBLE);
				holder.map_search_position_content_load.setText(ModuleKey.getCheckInModuleKey(bean.number_type));
				holder.map_search_position_content_date.setText(bean.number);
				holder.map_search_position_content_right.setText(EntryDetailNumberStateKey.getType(bean.number_status) + "");
				// holder.map_content_line.setVisibility(View.VISIBLE);
			}
		}
		if (childPosition == getChildrenCount(groupPosition) - 1) {
			holder.map_content_line.setVisibility(View.GONE);
		} else {
			holder.map_content_line.setVisibility(View.VISIBLE);
		}
		return converView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}

class Holder {
	LinearLayout content_title_bg;
	ImageView title_icon;
	TextView map_search_search_position_title;
}

class SearchPositionContentHolder {
	LinearLayout map_content_layout;
	ImageView map_content_line;
	TextView map_search_position_content_right;
	TextView map_search_position_content_load;
	TextView map_search_position_content_date;
}
