package oso.gis.adapter;



import java.util.List;

import oso.gis.R;
import oso.gis.BaseMapActivity.Type;
import oso.gis.bean.RectBean;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MapSearchContentAdapter extends BaseAdapter {
	
	private Context context;
	private SearchHolder holder;
	private List<RectBean> searchBean;
	
	public MapSearchContentAdapter(Context context, List<RectBean> searchBean){
		this.context = context;
		this.searchBean = searchBean;
	}

	@Override
	public int getCount() {
		return searchBean.size();
	}
	
	@Override
	public View getView(int position, View converView, ViewGroup viewGroup) {
		RectBean bean = searchBean.get(position);
		if(converView != null){
			holder = (SearchHolder) converView.getTag();
		}else{
			holder = new SearchHolder();
			converView = View.inflate(context, R.layout.act_google_map_search_content, null);
			holder.spot_title = (TextView) converView.findViewById(R.id.tv_map_search_spot_title);
			holder.spot_content = (TextView) converView.findViewById(R.id.tv_map_search_spot_content);
			converView.setTag(holder);
		}
		if(bean.type == Type.SPOT){
			holder.spot_title.setText("Spot");
		}
		if(bean.type == Type.DOOR){
			holder.spot_title.setText("Door");
		}
		holder.spot_content.setText(bean.obj_name);
		
		return converView;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

}

class SearchHolder {
	LinearLayout search_spot_layout;
	TextView spot_title,spot_content;
}
