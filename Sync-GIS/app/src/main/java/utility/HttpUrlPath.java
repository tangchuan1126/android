package utility;


public class HttpUrlPath {

	/**
	 * 初始化URL
	 */
	public static void initBaseUrls() {
		initBaseUrls(basePath);
	}

	public static void initBaseUrls(String basePath) {
		/**
		 * GIS 地图接口
		 */
		getStorageBaseData = basePath + "_gis/storagesCotroller/getStorageBaseData";
		queryAreaByPsId = basePath + "_gis/areaInfoCotroller/queryAreaByPsIdAndroid";
		queryWebcamAndroid = basePath + "_gis/storageCotroller/queryWebcamAndroid";
		queryDoorYardAndroid = basePath + "_gis/dockInfoCotroller/queryDoorYardAndroid";
		getDocksParkingOccupancyDetail = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancyDetail";
		getDocksParkingOccupancyDetailAndroid = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancyDetailAndroid";
		getDocksParkingOccupancy = basePath + "_gis/dockInfoCotroller/getDocksParkingOccupancy";
		storageDatas = basePath + "_gis/androidCotroller/storageDatas";

	}

	/**
	 * ================================================================
	 * 
	 * URL声明
	 * 
	 * ================================================================
	 */
	public static String getStorageBaseData;// 获取仓库四个点的坐标
	public static String queryAreaByPsId; // zone
	public static String queryDoorYardAndroid; // 门与停车位
	public static String queryWebcamAndroid; // 网络摄像头
	public static String getDocksParkingOccupancy; // 占用情况
	public static String getDocksParkingOccupancyDetail; // 获取占用信息
	public static String getDocksParkingOccupancyDetailAndroid; // 获取占用信息
	public static String storageDatas; // 请求地图数据zip

	public static String formatUrl(String ipAndPort) {
		return String.format("http://%s/Sync10/", ipAndPort);
	}
	/**
	 * basePath初始值
	 */
	private static String basePath = formatUrl("192.198.208.43");
}
