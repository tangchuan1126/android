package utility;

import oso.gis.R;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BottomDialog implements OnClickListener {

	private Context mContext;
	private Dialog dialog;
	private ViewGroup parentLayout, contentLayout;

	private View shadowView, dialogView, defLayout, btnLayout;
	private TextView titleTv;
	private Button submitBtn, cancelBtn;

	private TextView defTv;
	private EditText defEt;
	
	/**
	 * 是否显示取消按钮
	 */
	private boolean isShowCancelBtn = true;

	/**
	 * 是否可以点击屏幕取消
	 */
	private boolean isCanceledOnTouchOutside = true;

	/**
	 * 是否可以动态改变Dialog布局大小
	 */
	private boolean isAdjustResize = false;

	public BottomDialog(Context context) {
		init(context);
	}

	public BottomDialog(Context context, boolean isAdjustResize) {
		this.isAdjustResize = isAdjustResize;
		init(context);
	}

	public Dialog getDlg() {
		return dialog;
	}

	private void init(Context context) {
		mContext = context;
		parentLayout = (ViewGroup) View.inflate(mContext, R.layout.widget_bottom_dialog, null);
		initDialog(parentLayout);
		initView();
	}

	private void initDialog(View parentView) {
		dialog = new Dialog(mContext, R.style.signDialog);
		Window window = dialog.getWindow();
		window.setGravity(Gravity.BOTTOM); // 此处可以设置dialog显示的位置
		window.setWindowAnimations(R.style.signAnim); // 添加动画
		if (isAdjustResize)
			window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		dialog.setContentView(parentView);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
	}

	public void initView() {
		shadowView = dialog.findViewById(R.id.shadowView);
		dialogView = dialog.findViewById(R.id.dialogView);
		contentLayout = (ViewGroup) dialog.findViewById(R.id.contentLayout);
		submitBtn = (Button) dialog.findViewById(R.id.submitBtn);
		cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
		titleTv = (TextView) dialog.findViewById(R.id.titleTv);
		defLayout = (LinearLayout) dialog.findViewById(R.id.defLayout);
		btnLayout = (LinearLayout) dialog.findViewById(R.id.btnLayout);
		defTv = (TextView) defLayout.findViewById(R.id.defTv);
		defEt = (EditText) defLayout.findViewById(R.id.defEt);
		dialogView.setOnClickListener(this);
		shadowView.setOnClickListener(this);
		submitBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);
		// 是否显示取消按钮
		cancelBtn.setVisibility(isShowCancelBtn ? View.VISIBLE : View.GONE);
		// contentView默认等于defLayout
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.shadowView:
			if (isCanceledOnTouchOutside)
				dismiss();
			break;
		case R.id.cancelBtn:
			dismiss();
			break;
		case R.id.dialogView:
			// nothing
			break;
		case R.id.submitBtn:
			doSubmit();
			break;
		default:
			break;
		}
	}

	private void doSubmit() {
		String value = defEt.getText().toString().trim();
		if (onSubmitClickListener != null)
			onSubmitClickListener.onSubmitClick(this, value);
		Utility.colseInputMethod(mContext, defEt);
	}

	public TextView getTitleView() {
		return titleTv;
	}

	public BottomDialog setView(View view) {
		return setContentView(view);
	}

	public BottomDialog setContentView(View view) {
		if (view != null) {
			defLayout.setVisibility(View.GONE);
			contentLayout.addView(view);
		} else {
			defLayout.setVisibility(View.VISIBLE);
		}
		return this;
	}

	public BottomDialog setTitle(CharSequence text) {
		titleTv.setText(text);
		return this;
	}

	public BottomDialog setDefTv(CharSequence text) {
		defTv.setText(text);
		return this;
	}

	public BottomDialog setDefHint(CharSequence text) {
		defEt.setHint(text);
		return this;
	}

	public BottomDialog hideButton() {
		btnLayout.setVisibility(View.GONE);
		return this;
	}

	public BottomDialog show() {
		dialog.show();
		return this;
	}

	public void dismiss() {
		if (defEt != null)
			Utility.colseInputMethod(mContext, defEt);
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	public boolean isShowCancelBtn() {
		return isShowCancelBtn;
	}

	public BottomDialog setShowCancelBtn(boolean isShowCancelBtn) {
		this.isShowCancelBtn = isShowCancelBtn;
		cancelBtn.setVisibility(isShowCancelBtn ? View.VISIBLE : View.GONE);
		return this;
	}

	public TextView getDefTv() {
		return defTv;
	}

	public EditText getDefEt() {
		return defEt;
	}

	public boolean isCanceledOnTouchOutside() {
		return isCanceledOnTouchOutside;
	}

	public BottomDialog setCanceledOnTouchOutside(boolean isCanceledOnTouchOutside) {
		this.isCanceledOnTouchOutside = isCanceledOnTouchOutside;
		return this;
	}

	public OnSubmitClickListener getOnSubmitClickListener() {
		return onSubmitClickListener;
	}

	public BottomDialog setOnSubmitClickListener(OnSubmitClickListener onSubmitClickListener) {
		this.onSubmitClickListener = onSubmitClickListener;
		return this;
	}

	private OnSubmitClickListener onSubmitClickListener;

	public interface OnSubmitClickListener {
		void onSubmitClick(BottomDialog dlg, String value);
	}

	public boolean isAdjustResize() {
		return isAdjustResize;
	}

	public BottomDialog setAdjustResize(boolean isAdjustResize) {
		this.isAdjustResize = isAdjustResize;
		return this;
	}

	public static void showSimpleDialog(Context context, String title, String tvStr, String hint, boolean isShowCancelBtn,
			OnSubmitClickListener onSubmitClickListener) {
		new BottomDialog(context).setTitle(title).setDefTv(tvStr).setDefHint(hint).setCanceledOnTouchOutside(true).setShowCancelBtn(isShowCancelBtn)
				.setOnSubmitClickListener(onSubmitClickListener).show();
	}

	public static void showCustomViewDialog(Context context, String title, View contentView, boolean isShowCancelBtn,
			OnSubmitClickListener onSubmitClickListener) {
		new BottomDialog(context).setTitle(title).setView(contentView).setCanceledOnTouchOutside(true).setShowCancelBtn(isShowCancelBtn)
				.setOnSubmitClickListener(onSubmitClickListener).show();
	}

}
