package utility;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import oso.gis.GISApplication;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.PowerManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.text.style.CharacterStyle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class Utility {
	
	//==================图形处理==========================
	
	public static void scaleFile(File fileFrom,File fileTo,PointF maxSize){
		Bitmap bm=loadBm_limitSize(fileFrom, maxSize);
		saveTmpImage(bm,fileTo);
	}
	
	public static void saveTmpImage(Bitmap bm,File file) {
//		File dir=Goable.getDir_TmpImg();
		File fileToSave = file;
		try {
//			fileToSave = new File(dir, new Date().getTime() + ".jpg");
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(fileToSave));
			bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

//		return fileToSave;
	}
	 
	/**
	 * 加载-图片,限定-最大维度>>>注:1.缩放时"四舍五入" 故存在误差(或多或少)
	 * 
	 * @param path
	 * @param maxSize
	 * @return
	 */
	public static Bitmap loadBm_limitSize(File file, PointF maxSize) {
		String path=file.getPath();
		// 获取-旋转角
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		int rot = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
				ExifInterface.ORIENTATION_NORMAL);
		int rotAngle = 0;
		switch (rot) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotAngle = 90;
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotAngle = 180;
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotAngle = 270;
			break;

		default:
			break;
		}

		// 限定-尺寸
		float wMax = maxSize.x;
		float hMax = maxSize.y;

		Options opt = new Options();
		opt.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opt);
		if (opt.mCancel || opt.outWidth == -1 || opt.outHeight == -1) {
			return null;
		}

		float wIn = opt.outWidth;
		float hIn = opt.outHeight;

		// 若"横向-拍摄" 则调整"源尺寸",只为获取"缩放比例" 故调wIn、wMax均无所谓
		if (rotAngle == 90 || rotAngle == 270) {
			wIn = opt.outHeight;
			hIn = opt.outWidth;
		}

		float scaleIn = wIn / hIn;
		float scaleMax = wMax / hMax;

		// 若较宽 则限制宽
		opt.inJustDecodeBounds = false;
		if (scaleIn > scaleMax)
			opt.inSampleSize = Math.round(wIn / wMax); // "1/inSampleSize"表缩放比率
		else
			opt.inSampleSize = Math.round(hIn / hMax);

		// return BitmapFactory.decodeFile(path, opt);

		Bitmap bmScaled = BitmapFactory.decodeFile(path, opt);

		if (rotAngle == 0)
			return bmScaled;
		else
			return Utility.rotateBitmap(bmScaled, rotAngle);
	}

	/**
	 * 选转,正时针
	 * @param bmpIn
	 * @param angle
	 * @return
	 */
	public static Bitmap rotateBitmap(Bitmap bmpIn, int angle) {
		Matrix mat = new Matrix();
		mat.postRotate(angle);
		return Bitmap.createBitmap(bmpIn, 0, 0, bmpIn.getWidth(),
				bmpIn.getHeight(), mat, true);
	}
	
	
	//==================事件========================

	
	/**enter键弹起
	 * @param event
	 * @return
	 */
	public static boolean isEnterUp(KeyEvent event){
		return event.getKeyCode() == KeyEvent.KEYCODE_ENTER
				&& event.getAction() == KeyEvent.ACTION_UP ;
	}
	
	//=================通知===========================
	
	public static int NotifyID_Task=123;		//notifyID
	public static int NotifyID_Chat=124;

	/**
	 * 取消通知
	 * @param id 取NotifyID_x
	 */
	public static void cancelNotify(int id){
		GISApplication app=GISApplication.getThis();
		NotificationManager nm = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(id);
	}
	
	public static void cancelAllNotify(){
		GISApplication app=GISApplication.getThis();
		NotificationManager nm = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancelAll();
	}

	// ----------------系统属性----------------------

	/**
	 * 当前版本
	 * @return
	 */
	public static String getAppVersion() {

		String versionName = "";
		Application app = GISApplication.getThis();
		try {
			PackageManager pkgMng = app.getPackageManager();
			PackageInfo pkgInfo = pkgMng
					.getPackageInfo(app.getPackageName(), 0);
			versionName = pkgInfo.versionName;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return versionName;
	}
	
	/**
	 * app可见,考虑熄屏、锁屏
	 * @return
	 */
	public static boolean isAppInView(Context ct){
		PowerManager pw=(PowerManager)ct.getSystemService(Context.POWER_SERVICE);
		//熄屏时
		if(!pw.isScreenOn())
			return false;
		
		KeyguardManager mKeyguardManager = (KeyguardManager) ct.getSystemService(Context.KEYGUARD_SERVICE);    
		//锁屏时
	    if (mKeyguardManager.inKeyguardRestrictedInputMode())
	    	return false;
	    
	    //当前显示app-非该app
	    if(!isRunningInForeground(ct))
	    	return false;
	    
	    return true;
	}
	
	
	/**
	 * 该app是否在视野中,未考虑熄屏、锁屏
	 * @param manager
	 * @return
	 */
	public static boolean isRunningInForeground(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		//获得Sync的包名
		String currAppPackageName = context.getPackageName();
        List<RunningTaskInfo> tasks = manager.getRunningTasks(1);
        
        ComponentName topActivity = tasks.get(0).topActivity;
        return topActivity.getPackageName().equals(currAppPackageName);
	}
	
	private static String mac;
	
	public static String getMac() {  
		if(TextUtils.isEmpty(mac)){
			WifiManager wifi = (WifiManager) GISApplication.getThis().getSystemService(Context.WIFI_SERVICE);  
			WifiInfo info = wifi.getConnectionInfo();  
			mac=info.getMacAddress();
		}
        return mac;  
    }  

	// 是否在-主进程
	// 注:1>一个apk可能有多个进程,如:com.ishowtu.mfthd(主进程)、com.ishowtu.mfthd:remote
	public static boolean isInMainProcess(Context ct) {
		return ct.getPackageName().equals(getCurProcessName(ct));
	}

	// 获取-当前"进程名"
	public static String getCurProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
				.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {

				return appProcess.processName;
			}
		}
		return "";
	}

	public static int getScreenWidth(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.widthPixels;
	}

	public static int getScreenHeight(Context context) {
		DisplayMetrics dm = getDisplayMetrics(context);
		return dm.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm;
	}

	// ---------------类型转换----------------------
	
	public static long parseLong(String str){
		long ret = 0L;
		try {
			ret = Long.parseLong(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}
	
	public static int parseInt(String str){
		int ret = 0;
		try {
			ret = Integer.parseInt(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	// str转float,屏蔽了异常
	public static float parseFloat_noEx(String str) {
		float ret = 0f;
		try {
			ret = Float.parseFloat(str);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	// 序列化
	public static String serialize(Serializable obj) {
		String ret = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(obj);
			// 用utf8会转义部分byte,导致无法还原
			ret = new String(baos.toByteArray(), "ISO-8859-1");
		} catch (Exception e) {
			// TODO Auto-generated
			e.printStackTrace();
		}
		return ret;
	}

	// 反序列化
	public static Object unserialize(String str) {
		Object ret = null;
		if (TextUtils.isEmpty(str))
			return null;

		ByteArrayInputStream bais = null;
		try {
			bais = new ByteArrayInputStream(str.getBytes("ISO-8859-1"));
			ObjectInputStream bis = new ObjectInputStream(bais);
			ret = bis.readObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	// --------------文件处理----------------------

	// 重命名,若"新文件"存在 则不修改
	public static void renameFile(File file, String newName) {
		if (!file.exists())
			return;

		File newFile = new File(file.getParentFile(), newName);
		if (newFile.exists())
			return;
		file.renameTo(newFile);
	}

	// 重命名,若"新文件"存在 则不修改
	public static void renameFile(File file, File newFile) {
		if (!file.exists())
			return;

		if (newFile.exists())
			return;
		file.renameTo(newFile);
	}

	// 同getFileSize,单位为M 最多2位小数
	public static String getFileSize_M(File file) {
		float cacheSizeM = (float) Utility.getFileSize(file)
				/ (float) (1024 * 1024);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		return df.format(cacheSizeM);
	}

	// 获取"文件/文件夹"的大小(字节),包含子文件夹
	public static long getFileSize(File file) {
		if (file == null || !file.exists())
			return 0;

		// 若为文件
		if (file.isFile())
			return file.length();

		long size = 0;
		File flist[] = file.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileSize(flist[i]);
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}

	// 判断-目录为空
	public static boolean isDirEmpty(File dir) {
		String[] files = dir.list();
		if (files != null && files.length > 0) {
			return false;
		}
		return true;
	}

	// 删除-文件夹(含子文件)、文件
	public static void delFolder(File file) {
		if (file.isFile()) {
			file.delete();
			return;
		}

		if (file.isDirectory()) {
			File[] childFiles = file.listFiles();
			// 若无子文件
			if (childFiles == null || childFiles.length == 0) {
				file.delete();
				return;
			}
			// 含子文件
			for (int i = 0; i < childFiles.length; i++) {
				delFolder(childFiles[i]);
			}
			file.delete();
		}
	}

	// -------------ac操作------------------------
	
	/**
	 * app已被干掉,如:1.crash后 2.退至后台被kill掉
	 * @param ct
	 * @return
	 */
	public static boolean isAppKilled(Context ct){
		ActivityManager am = (ActivityManager) ct
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> infos = am.getRunningTasks(100);

		for (int i = 0; i < infos.size(); i++) {
			RunningTaskInfo info = infos.get(i);
			//若为当前包
			if (info.baseActivity.getPackageName().equals(ct.getPackageName())) {
				// 若有ac被干掉,即使找到 继续下行(可能有多个task)
				if (info.numActivities != info.numRunning)
					return true;
			}
		}
		
		return false;
	}

	// 检查apk是否已安装
	public static boolean isApkInstalled(Context context, String packageName) {
		final PackageManager packageManager = context.getPackageManager();
		// 获取所有已安装程序的包信息
		List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
		for (int i = 0; i < pinfo.size(); i++) {
			if (pinfo.get(i).packageName.equalsIgnoreCase(packageName))
				return true;
		}
		return false;
	}

	// 跳至商店,以下载指定apk
	public static void downApk(Activity cw, String packageName) {
		// com.google.android.gsf
		Uri uri = Uri.parse("market://details?id=" + packageName);
		Intent it = new Intent(Intent.ACTION_VIEW, uri);
		cw.startActivity(it);
	}

	// 弹至某activity,如:主页
	public static void popTo(Activity context, Class<?> main) {
		Intent intent = new Intent(context, main);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}

	// ----------文本处理-------------------------------
	
	public static boolean isEmpty(List list){
		return list==null||list.size()==0;
	}
	
	public static boolean isEmpty(JSONArray ja){
		return ja==null||ja.length()==0;
	}
	
	public static String read(InputStream instream) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = instream.read(buffer)) != -1) {
			bos.write(buffer, 0, len);
		}
		String ret = new String(bos.toByteArray());
		return ret;
	}
	
	/**获取-变换后的文本
	 * @param et
	 * @return
	 */
	public static String getTransformedText(EditText et){
		TransformationMethod tran=et.getTransformationMethod();
		CharSequence str=et.getEditableText().toString();
		if(tran!=null)
			str=tran.getTransformation(str, et);
		return str.toString();
	}
	
	/** 添加span,style1添加到str1
	 * @param spOld 待修改的spannable
	 * @param str0 
	 * @param str1 
	 * @param style1
	 * 
	 * @如 spOld为[str0][str1][str],style1添加至str1
	 */
	public static void addSpan(Spannable spOld, String str0, String str1,
			CharacterStyle style1) {
		spOld.setSpan(style1, str0.length(), str0.length() + str1.length(),
				Spannable.SPAN_INCLUSIVE_INCLUSIVE);
	}
	
	public static Spannable getSpannable(String str,CharacterStyle style) {
		SpannableString sp=new SpannableString(str);
		sp.setSpan(style, 0, str.length(),
				Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		return sp;
	}

	// 获取-复合文本,用于只有"两个子串"
	// 参数:style1(str1的样式)
	public static Spannable getCompoundText(String str0, String str1,
			CharacterStyle style1) {
		return getCompoundText(new String[] { str0, str1 },
				new CharacterStyle[] { null, style1 });
	}
	
	/** 获取-复合文本,style1应用于str1
	 * @param str0
	 * @param str1
	 * @param str2
	 * @param style1
	 * @return str0+str1+str2
	 */
	public static Spannable getCompoundText(String str0, String str1,
			String str2, CharacterStyle style1) {
		return getCompoundText(new String[] { str0, str1,str2},
				new CharacterStyle[] { null, style1,null});
	}

	// 获取-复合文本,两个数组维度需相同
	public static Spannable getCompoundText(String[] strs,
			CharacterStyle[] styles) {
		SpannableStringBuilder strTarget = new SpannableStringBuilder();
		int len = strs.length;
		for (int i = 0; i < len; i++) {
			SpannableString str = new SpannableString(strs[i]);
//			if(styles[i]!=null)
			str.setSpan(styles[i], 0, str.length(),
					Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			strTarget.append(str);
		}
		return strTarget;
	}

	public static int getMaxLen_ofString(float textSize, String[] strs) {
		return getMaxLen_ofString(textSize, strs, null);
	}

	/**
	 * 获取-最长str的长度
	 * 
	 * @param textSize
	 *            ,文字尺寸(px)
	 * @param strs
	 * @param addition
	 *            ,附加字符
	 * @return
	 */
	public static int getMaxLen_ofString(float textSize, String[] strs,
			String addition) {
		if (strs == null)
			return 0;

		if (addition == null)
			addition = "";

		Paint pt = new Paint();
		pt.setTextSize(textSize);
		float maxLen = 0;
		for (int i = 0; i < strs.length; i++) {
			float tmpLen = (float) pt.measureText(strs[i] + addition);
			if (tmpLen > maxLen)
				maxLen = tmpLen;
		}
		return (int) (maxLen + 5f);
	}

	// ==============ui操作============================
	
	private static Rect tmpRect = new Rect();

	/**
	 * 判断"点"是否在视图内
	 * 
	 * @param v
	 * @param x
	 *            ,y 全局点_____________如:MotionEvent.getRawX
	 * @return
	 */
	public static boolean isPointInView(View v, int x, int y) {
		v.getGlobalVisibleRect(tmpRect);
		
		return tmpRect.contains(x, y);
	}
	
	public static boolean isPointInView(View v, MotionEvent e) {
		return isPointInView(v, (int)e.getRawX(), (int)e.getRawY());
	}

	// 移动edittext光标至末尾
	public static void endEtCursor(EditText et) {
		String str = et.getEditableText().toString();
		if (str == null)
			str = "";
		et.setSelection(str.length());
	}
	
	public static TextView getEmptyView_lv(Context ct,String text){
		TextView tv=new TextView(ct);
		tv.setText(text);
		return tv;
	}

	// -------------------------------------------

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()������������Ŀ
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // ��������View �Ŀ��
			totalHeight += listItem.getMeasuredHeight(); // ͳ������������ܸ߶�
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static boolean isConnectNet(Context context) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected())
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isSDExits() {
		boolean sdExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
		return sdExist;
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, View edit) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * @Description:关闭软键盘
	 * @param edit
	 * @param context
	 */
	public static void colseInputMethod(Context context, EditText edit) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		// edit.setCursorVisible(false);//失去光标
		edit.requestFocus();
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * 显示软键盘
	 * @param ct
	 * @param v
	 */
	public static void showSoftkeyboard(Context ct,View v){
		InputMethodManager imm = (InputMethodManager) ct
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
	}

	public static String getVersionName(Context context) {
		String version = "";
		try {
			PackageManager packageManager = context.getPackageManager();
			android.content.pm.PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			version = packInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	} // 获取手机状态栏高度

	public static int getStatusBarHeight(Context context) {
		Class<?> c = null;
		Object obj = null;
		Field field = null;
		int x = 0, statusBarHeight = 0;
		try {
			c = Class.forName("com.android.internal.R$dimen");
			obj = c.newInstance();
			field = c.getField("status_bar_height");
			x = Integer.parseInt(field.get(obj).toString());
			statusBarHeight = context.getResources().getDimensionPixelSize(x);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return statusBarHeight;
	}

	/**
	 * @Description:判断List是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 * @param <E>
	 */
	public static <E> boolean isNullForList(List<E> list) {
		boolean flag = true;
		if (list != null && list.size() > 0) {
			flag = false;
		}
		return flag;
	}
	
	/**
	 * @Description:判断List是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 * @param <E>
	 */
	public static <E> boolean isNullForMap(Map<String,E> map) {
		boolean flag = true;
		if (map != null && map.size() > 0) {
			flag = false;
		}
		return flag;
	}
	

	// 获取登陆后的adid
	public static String getSharedPreferencesUrlValue(Activity context,
			String key) {
		String adid = "";
		if (!StringUtil.isNullOfStr(key)) {
			SharedPreferences sharedPreferences = context.getSharedPreferences(
					"loginSave", Context.MODE_PRIVATE);
			adid = ("&" + key + "=" + sharedPreferences.getString(key, ""));
		}
		return adid;
	}

	// jiang
	public static int pxTodip(Context c, int px) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px,
				c.getResources().getDisplayMetrics());
	}
	
	public static int pxTosp(Context c, int px) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, px,
				c.getResources().getDisplayMetrics());
	}

	private static long lastOneClickTime = 0;// click事件的基准时间

	/**
	 * @Description:根据所传时间判断click事件点击是否过快 false 为正常速度 true 为点击过快
	 * @param @param timeMillis 以毫秒为单位的时间
	 * @param @return
	 */
	public static boolean isFastClick(int timeMillis) {
		long time = System.currentTimeMillis();
		long timeD = time - lastOneClickTime;
		lastOneClickTime = time;
		if (timeD > timeMillis) {
			return false;
		}
		return true;
	}

	
	 /** 
     * 将px值转换为sp值，保证文字大小不变 
     *  
     * @param pxValue 
     * @param fontScale 
     *            （DisplayMetrics类中属性scaledDensity） 
     * @return 
     */  
    public static int px2sp(Context context, float pxValue) {  
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
        return (int) (pxValue / fontScale + 0.5f);  
    }  
  
    /** 
     * 将sp值转换为px值，保证文字大小不变 
     *  
     * @param spValue 
     * @param fontScale 
     *            （DisplayMetrics类中属性scaledDensity） 
     * @return 
     */  
    public static int sp2px(Context context, float spValue) {  
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;  
        return (int) (spValue * fontScale + 0.5f);  
    }  
}
