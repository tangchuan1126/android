package utility;

import oso.gis.R;
import android.content.Context;
import android.content.res.Resources;

public class AlertUi {

	public static void showAlertBuilder(Context context, String message) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
		Resources resources = context.getResources();
		builder.setTitle(resources.getString(R.string.sys_holdon));
		builder.setMessage(message);
		builder.setNegativeButton(resources.getString(R.string.sync_yes), null);
		builder.create().show();
	}

	public static void showVersionError(final Context context) {
//		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(context);
//		builder.setMessage("New version is ready for download, please remove old APP first");
//		builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				DownloadApk.loadApk(context);
//			}
//		});
//		builder.isHideCancelBtn(true);
//		builder.create().show();
	}

}
