package utility;

import java.io.Serializable;

import android.graphics.Bitmap;
/**
 * file 表
 * @author zhangrui
 *
 */
public class UpFile implements Serializable{
	private long id ;
	private Bitmap bitMap ;
	private String fileName ;
	private String fileLocalPath ;			 
	
	private String file_with_id ;		 
	private String file_with_type ; 	 
	private String file_with_class ;	
	private String file_path ; 	
	
	private int isSubmit =0 ; 			 
	
	private String fileNameThumbnail ;
	
	
	
	private String smallPath ;	//小图的路径		在transport_picture 要用
	private String bigPath ;	//大图的路径		在transport_picture 要用
	public UpFile() {
		super();
		this.isSubmit = 0;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Bitmap getBitMap() {
		return bitMap;
	}
	public void setBitMap(Bitmap bitMap) {
		this.bitMap = bitMap;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileLocalPath() {
		return fileLocalPath;
	}
	public void setFileLocalPath(String fileLocalPath) {
		this.fileLocalPath = fileLocalPath;
	}
	public String getFile_with_id() {
		return file_with_id;
	}
	public void setFile_with_id(String file_with_id) {
		this.file_with_id = file_with_id;
	}
	public String getFile_with_class() {
		return file_with_class;
	}
	public void setFile_with_class(String file_with_class) {
		this.file_with_class = file_with_class;
	}
	public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	
	public int getIsSubmit() {
		return isSubmit;
	}
	public void setIsSubmit(int isSubmit) {
		this.isSubmit = isSubmit;
	}
	public String getFile_with_type() {
		return file_with_type;
	}
	public void setFile_with_type(String file_with_type) {
		this.file_with_type = file_with_type;
	}
	public String getFileNameThumbnail() {
		return fileNameThumbnail;
	}
	public void setFileNameThumbnail(String fileNameThumbnail) {
		this.fileNameThumbnail = fileNameThumbnail;
	}
	public String getSmallPath() {
		return smallPath;
	}
	public void setSmallPath(String smallPath) {
		this.smallPath = smallPath;
	}
	public String getBigPath() {
		return bigPath;
	}
	public void setBigPath(String bigPath) {
		this.bigPath = bigPath;
	}
	
	
	
	
}
