/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 * 
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * 
 * Copyright ZIH Corp. 2012
 * 
 * ALL RIGHTS RESERVED
 ***********************************************/

package utility;


import oso.gis.GISApplication;
import oso.gis.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.TextView;
import android.widget.Toast;


public class UIHelper {
    ProgressDialog loadingDialog;
    Activity activity;

    public UIHelper(Activity activity) {
        this.activity = activity;
    }

    public void showLoadingDialog(final String message) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    loadingDialog = ProgressDialog.show(activity, "", message, true, false);
                }
            });
        }
    }

    public void updateLoadingDialog(final String message) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    loadingDialog.setMessage(message);
                }
            });
        }
    }

    public boolean isDialogActive() {
        if (loadingDialog != null) {
            return loadingDialog.isShowing();
        } else {
            return false;
        }
    }

    public void dismissLoadingDialog() {
        if (activity != null && loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    public void showErrorDialog(String errorMessage) {
        new AlertDialog.Builder(activity).setMessage(errorMessage).setTitle("Error").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public void showErrorDialogOnGuiThread(final String errorMessage) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    new AlertDialog.Builder(activity).setMessage(errorMessage).setTitle("Error").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            dismissLoadingDialog();
                        }
                    }).create().show();
                }
            });
        }
    }
    
    //==============static=========================================
    
//    private static int k=1;
	
	private static Toast t;
	
	//黑色背景
//	private static void showToast_inner(Context ctx, String text) {
//
//		if (t != null)
//			t.cancel();
//
//		TextView tv = new TextView(ctx);
//		tv.setBackgroundResource(R.drawable.toast_bgi);
//		tv.setText(text);
//		tv.setTextSize(16f);
//		int paddingL = Utility.dip2px(ctx, 25);
//		int paddingT = Utility.dip2px(ctx, 15);
//		tv.setPadding(paddingL, paddingT, paddingL, paddingT);
//		tv.setTextColor(0xffffffff);
//
//		t = new Toast(ctx);
//		t.setView(tv);
//		t.setDuration(Toast.LENGTH_SHORT);
//		t.show();
//	}
	
	//蓝色边框
	private static Toast showToast_inner(Context ctx,String text,int duration){
		
		if(t!=null)
			t.cancel();
		
		//debug
//		if(true)
//		{
//			Toast tt=Toast.makeText(ctx, text, duration);
//			return tt;
//		}
			
		TextView tv=new TextView(ctx);
		tv.setBackgroundResource(R.drawable.sh_toast);
		tv.setText(text);
//		tv.setTextSize(16f);
		tv.setTextSize(14f);
		tv.setTextColor(0xff000000);
		t=new Toast(ctx);
		t.setView(tv); 
		t.setDuration(duration);
		
		t.show();
		return t;
	}
    
    public static Toast showToast(Context ctx,String text){
//    	UIHelper.showToast(ctx,text, Toast.LENGTH_SHORT).show();
    	return showToast_inner(GISApplication.getThis(), text,Toast.LENGTH_SHORT);
    }
    
    public static Toast showToast(Context ctx,String text,int duration){
//    	UIHelper.showToast(ctx,text, Toast.LENGTH_SHORT).show();
    	return showToast_inner(GISApplication.getThis(), text,duration);
    }
    
    public static Toast showToast(String text){
    	return showToast_inner(GISApplication.getThis(), text,Toast.LENGTH_SHORT);
    }
    

}
