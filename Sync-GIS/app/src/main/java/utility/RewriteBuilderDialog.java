package utility;

import oso.gis.R;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @ClassName: RewriteBuilderDialog
 * @Description:
 * @author gcy
 * @date 2014年6月10日 下午5:20:47
 * 
 */
public class RewriteBuilderDialog extends Dialog {

	// private boolean mCancelable;

	public RewriteBuilderDialog(Context context) {
		super(context);
		init(context);
	}

	public RewriteBuilderDialog(Context context, int theme) {
		super(context, theme);
		init(context);
	}

	private void init(Context context) {
		// WindowManager.LayoutParams lp = getWindow().getAttributes();
		// DisplayMetrics dm = context.getResources().getDisplayMetrics();
		// lp.width = (int) (ActivityUtils.getScreenWidth(context) * 0.5);
		// //设置宽度
		// Window window = getWindow();
		// window.setGravity(Gravity.CENTER); // 此处可以设置dialog显示的位置
	}

	// public boolean onKeyUp(int keyCode, KeyEvent event) {
	// if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking() &&
	// !event.isCanceled()) {
	// onBackPressed();
	// return true;
	// }
	// return false;
	// }
	//
	// @Override
	// public void onBackPressed() {
	// if (mCancelable) {
	// super.onBackPressed();
	// }
	// }

	// ==================nested===============================

	public static class Builder {
		private boolean mCancelable = true;

		private Context context;
		// private String title = "Prompt";
		// debug,Notice
		private String title = "";
		private CharSequence message;
		private String positiveButtonText;
		private String negativeButtonText;
		private View contentView;
		private DialogInterface.OnClickListener positiveButtonClickListener;
		private DialogInterface.OnClickListener negativeButtonClickListener;
		private boolean isCancelBtnHidden;
		private boolean positiveButtonOnClickDismiss = true;// 判断 点击是否消失
															// 如果为true默认消失
		private boolean canceledOnTouchOutside = false;// 触摸屏幕边缘关闭dialog
		private ListAdapter mAdapter;
		private OnItemClickListener mOnItemClickListener;

		private boolean isHeightLimit = true; // dialog高度限制
		private int heightLimitCount = 5; // 初始限制条件为5条数据的高度

		public static final int ITEM_STYLE_1 = R.layout.simple_list_item_1; // 一般textView
		public static final int ITEM_STYLE_2 = R.layout.simple_list_item_1_arraw;// 左边带箭头
		public static final int ITEM_STYLE_SINGLE = R.layout.simple_list_single_sel;
		private int selectStyle = ITEM_STYLE_1;

		public Builder(Context context) {
			this.context = context;
		}

		/**
		 * @Description:显示Dialog
		 * @param
		 */
		public RewriteBuilderDialog show() {
			RewriteBuilderDialog dlg = create();
			dlg.show();
			return dlg;
		}

		/**
		 * @Description:设置Dialog 显示的消息内容
		 * @param @param message String 类型字符串
		 * @param @return
		 */
		public Builder setMessage(CharSequence message) {
			this.message = message;
			return this;
		}

		/**
		 * @Description:设置Dialog 显示的消息内容
		 * @param @param message 资源ID
		 * @param @return
		 */
		public Builder setMessage(int message) {
			this.message = (String) context.getText(message);
			return this;
		}

		/**
		 * @Description:设置Dialog 标题
		 * @param @param message String 类型字符串
		 * @param @return
		 */
		public Builder setTitle(int title) {
			this.title = (String) context.getText(title);
			return this;
		}

		/**
		 * @Description:设置Dialog 标题
		 * @param @param message 资源ID
		 * @param @return
		 */
		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		/**
		 * @Description:插入控件内容
		 * @param @param v
		 * @param @return
		 */
		public Builder setContentView(View v) {
			this.contentView = v;
			return this;
		}

		public Builder setView(View v) {
			return setContentView(v);
		}

		public boolean isPositiveButtonOnClickDismiss() {
			return positiveButtonOnClickDismiss;
		}

		// positiveBtn点击后dismiss
		public void setPositiveButtonOnClickDismiss(boolean isDismiss) {
			this.positiveButtonOnClickDismiss = isDismiss;
		}

		public Builder setPositiveButton(int positiveButtonText, DialogInterface.OnClickListener listener) {
			this.positiveButtonText = (String) context.getText(positiveButtonText);
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setPositiveButton(String positiveButtonText, DialogInterface.OnClickListener listener) {
			this.positiveButtonText = positiveButtonText;
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(int negativeButtonText, DialogInterface.OnClickListener listener) {
			this.negativeButtonText = (String) context.getText(negativeButtonText);
			this.negativeButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(String negativeButtonText, DialogInterface.OnClickListener listener) {
			this.negativeButtonText = negativeButtonText;
			this.negativeButtonClickListener = listener;
			return this;
		}

		private String mid_btn;
		private DialogInterface.OnClickListener midBtn_listener;

		/**
		 * 中间按钮
		 * 
		 * @param text
		 * @param listener
		 * @return
		 */
		public Builder setMidButton(String text, DialogInterface.OnClickListener listener) {
			this.mid_btn = text;
			this.midBtn_listener = listener;
			return this;
		}
		
		public Builder setMidButton(int resText, DialogInterface.OnClickListener listener) {
			this.mid_btn = (String) context.getText(resText);
			this.midBtn_listener = listener;
			return this;
		}

		// 隐藏-cancel
		public Builder isHideCancelBtn(boolean flag) {
			this.isCancelBtnHidden = flag;
			return this;
		}

		// jiang
		public Builder hideCancelBtn() {
			return isHideCancelBtn(true);
		}

		public Builder setItems(CharSequence[] items, final OnItemClickListener listener) {
			return setItems(items, selectStyle, listener);
		}

		private int defIndex = -1;

		public Builder setItems_singleChoice(CharSequence[] items, final OnItemClickListener listener, int defIndex) {
			selectStyle = ITEM_STYLE_SINGLE;
			// return setItems(items, selectStyle, listener);
			// this.mAdapter = new ArrayAdapter<CharSequence>(context,
			// selectStyle,android.R.id.text1, items);
			this.mAdapter = new AdpDlgSingleSel(context, items, defIndex);
			this.mOnItemClickListener = listener;
			this.defIndex = defIndex;
			return this;
		}

		public Builder setArrowItems(CharSequence[] items, final OnItemClickListener listener) {
			selectStyle = ITEM_STYLE_2;
			isHeightLimit = false;
			return setItems(items, selectStyle, listener);
		}

		public Builder setItems(CharSequence[] items, int style, final OnItemClickListener listener) {
			this.mAdapter = new ArrayAdapter<CharSequence>(context, style, items);
			this.mOnItemClickListener = listener;
			return this;
		}

		public Builder setAdapter(final ListAdapter adapter, final OnItemClickListener listener) {
			this.mAdapter = adapter;
			this.mOnItemClickListener = listener;
			return this;
		}

		private RewriteBuilderDialog mDialog = null;

		private Button btnMid;

		public RewriteBuilderDialog create() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mDialog = new RewriteBuilderDialog(context, R.style.Dialog);
			// 初始化
			// mDialog.mCancelable=mCancelable;
			// debug
			mDialog.setCancelable(mCancelable);
			mDialog.setCanceledOnTouchOutside(canceledOnTouchOutside);// 防止触摸屏外返回

			View layout = inflater.inflate(R.layout.rewrite_dialog_layout, null);
			layout.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// System.out.println("");
					if (canceledOnTouchOutside)
						mDialog.dismiss();
				}
			});
			// debug
			// mDialog.addContentView(layout, new
			// LayoutParams(LayoutParams.FILL_PARENT,
			// LayoutParams.WRAP_CONTENT));
			
			//debug,若有值
			if(!TextUtils.isEmpty(title))
				((TextView) layout.findViewById(R.id.title)).setText(title);
			if (positiveButtonText != null) {
				layout.findViewById(R.id.positiveButton).setVisibility(View.VISIBLE);
				((Button) layout.findViewById(R.id.positiveButton)).setText(positiveButtonText);

			}
			if (positiveButtonClickListener != null) {
				((Button) layout.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						positiveButtonClickListener.onClick(mDialog, DialogInterface.BUTTON_POSITIVE);
						if (mDialog.isShowing() && positiveButtonOnClickDismiss) {
							mDialog.dismiss();
						}
					}
				});
			} else {
				((Button) layout.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						if (mDialog.isShowing()) {
							mDialog.dismiss();
						}
					}
				});
			}

			if (negativeButtonText != null) {
				((Button) layout.findViewById(R.id.negativeButton)).setText(negativeButtonText);

			}
			if (negativeButtonClickListener != null) {
				((Button) layout.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						negativeButtonClickListener.onClick(mDialog, DialogInterface.BUTTON_NEGATIVE);
						if (mDialog.isShowing()) {
							mDialog.dismiss();
						}
					}
				});
			} else {
				((Button) layout.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Utility.colseInputMethod(context, v);
						mDialog.dismiss();
					}
				});
			}

			// 中间的按钮
			btnMid = (Button) layout.findViewById(R.id.btnMid);
			if (!TextUtils.isEmpty(mid_btn) && midBtn_listener != null) {
				btnMid.setVisibility(View.VISIBLE);
				btnMid.setText(mid_btn);
				btnMid.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						midBtn_listener.onClick(mDialog, DialogInterface.BUTTON_NEUTRAL);
					}
				});
			}

			if (isCancelBtnHidden) {
				layout.findViewById(R.id.line).setVisibility(View.GONE);
				layout.findViewById(R.id.negativeButton).setVisibility(View.GONE);
			}

			if (message != null) {
				((TextView) layout.findViewById(R.id.message)).setVisibility(View.VISIBLE);
				((TextView) layout.findViewById(R.id.message)).setText(message);
			}
			if (contentView != null) {
				ViewGroup customPanel = (ViewGroup) layout.findViewById(R.id.customPanel);
				customPanel.setVisibility(View.VISIBLE);
				customPanel.removeAllViews();
				customPanel.addView(contentView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			}
			if (mAdapter != null) {
				final ListView lv = (ListView) layout.findViewById(R.id.lv);
				LayoutParams params = lv.getLayoutParams();
				int maxHeight = Utility.pxTodip(context, 160);
				if (mAdapter.getCount() > heightLimitCount && isHeightLimit) {
					params.height = maxHeight;
				}

				// 限制-最多显示条目
				if ((selectStyle == ITEM_STYLE_1 || selectStyle == ITEM_STYLE_SINGLE) && mAdapter.getCount() > heightLimitCount && isHeightLimit) {
					int height = Utility.pxTodip(context, 40);
					// dividerHeight
					params.height = (heightLimitCount * (height));
				}

				if (selectStyle == ITEM_STYLE_2) {
					lv.setDivider(null);
					int dividerHeight = Utility.pxTodip(context, 5);
					lv.setDividerHeight(dividerHeight);
					int height = Utility.pxTodip(context, 50);
					params.height = (mAdapter.getCount() * (height + dividerHeight));
				}
				lv.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						if (mOnItemClickListener == null)
							return;

						mOnItemClickListener.onItemClick(parent, view, position, id);
						if (mDialog.isShowing()) {
							mDialog.dismiss();
						}
					}
				});
				lv.setLayoutParams(params);
				lv.setVisibility(View.VISIBLE);
				lv.setAdapter(mAdapter);

				// debug,选中默认项
				if (defIndex >= 0)
					lv.setSelection(defIndex);
			}
			mDialog.setContentView(layout);
			return mDialog;
		}

		/**
		 * @Description:设置触摸屏幕边缘是否关闭 默认为否
		 * @param @param canceledOnTouchOutside false禁止 true允许("返回键"也可dismiss)
		 */
		public Builder setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
			this.canceledOnTouchOutside = canceledOnTouchOutside;
			return this;
		}

		// 允许"返回",setCanceledOnTouchOutside(true)时 "返回键"也有效
		public Builder setCancelable(boolean b) {
			mCancelable = b;
			return this;
		}

		public void dismiss() {
			if (mDialog != null && mDialog.isShowing()) {
				mDialog.dismiss();
			}
		}

		public RewriteBuilderDialog getDialog() {
			return mDialog;
		}

		public boolean isHeightLimit() {
			return isHeightLimit;
		}

		public void setHeightLimit(boolean isHeightLimit) {
			this.isHeightLimit = isHeightLimit;
		}

		public int getHeightLimitCount() {
			return heightLimitCount;
		}

		public void setHeightLimitCount(int heightLimitCount) {
			this.heightLimitCount = heightLimitCount;
		}
	}

	// ===========static=====================================

	// 显示-提示dlg,仅含yes
	public static void showSimpleDialog_Tip(Context c, String msg) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(c);
		builder.setTitle(c.getString(R.string.sync_notice));
		builder.setMessage(msg);
		builder.setPositiveButton(c.getString(R.string.sync_yes), null);
		builder.isHideCancelBtn(true);
		builder.create().show();
	}

	/*
	 * // 显示-没有Button 仅有列表的Dialog public static void
	 * showDialogWithoutBtn(Activity ac, CharSequence charSequence) {
	 * RewriteBuilderDialog.Builder builder = new
	 * RewriteBuilderDialog.Builder(ac); builder.setTitle("Prompt");
	 * builder.setMessage(msg); builder.setPositiveButton("Yes", null);
	 * builder.isHideCancelBtn(true); builder.create().show(); }
	 */
	// 显示-简单dlg
	public static RewriteBuilderDialog.Builder newSimpleDialog(Context c, CharSequence msg, DialogInterface.OnClickListener onClick_Positive) {
		RewriteBuilderDialog.Builder builder = new RewriteBuilderDialog.Builder(c);
		builder.setTitle(c.getString(R.string.sync_notice));
		builder.setMessage(msg);
		builder.setPositiveButton(c.getString(R.string.sync_yes), onClick_Positive);
		return builder;
	}

	// 显示-简单dlg,含:msg、yes监听
	// 如:退出提示
	public static RewriteBuilderDialog showSimpleDialog(Context c, CharSequence msg, DialogInterface.OnClickListener onClick_Positive) {
		// RewriteBuilderDialog.Builder builder = new
		// RewriteBuilderDialog.Builder(
		// ac);
		// builder.setTitle("Prompt");
		// builder.setMessage(msg);
		// builder.setPositiveButton("Yes", onClick_Positive);
		// builder.setNegativeButton("No", null);
		// builder.create().show();
		return newSimpleDialog(c, msg, onClick_Positive).show();
	}

	public static void dialogDismiss(Dialog dialog) {
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

}
