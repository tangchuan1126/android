/**************************************************************************************
 * [Project]
 *       MyProgressDialog
 * [Package]
 *       com.lxd.widgets
 * [FileName]
 *       CustomProgressDialog.java
 * [Copyright]
 *       Copyright 2012 LXD All Rights Reserved.
 * [History]
 *       Version          Date              Author                        Record
 *--------------------------------------------------------------------------------------
 *       1.0.0           2012-4-27         lxd (rohsuton@gmail.com)        Create
 **************************************************************************************/

package utility;

import oso.gis.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;

/********************************************************************
 * [Summary] TODO 请在此处简要描述此类所实现的功能。因为这项注释主要是为了在IDE环境中生成tip帮助，务必简明扼要 [Remarks]
 * TODO 请在此处详细描述类的功能、调用方法、注意事项、以及与其它类的关系.
 *******************************************************************/

public class RewriteDialog extends Dialog {

	private Context context = null;
	private static RewriteDialog customProgressDialog = null;

	public RewriteDialog(Context context) {
		super(context);
		this.context = context;
	}

	public RewriteDialog(Context context, int theme) {
		super(context, theme);
		this.context = context;
	}

	/**
	 * 重构dialog的 基本样式
	 * 
	 * @param context
	 * @param flag
	 *            通过flag 来选择 dialog的模板
	 * @return
	 */
	public static RewriteDialog createDialog(Context context, int flag) {
//		switch (flag) {
//		case 0:
//			customProgressDialog = new RewriteDialog(context, R.style.RewriteDialog);
//			customProgressDialog.setContentView(R.layout.rewrite_dialog);
//			break;
//		case 1:
//			customProgressDialog = new RewriteDialog(context, R.style.RewriteDialog);
//			customProgressDialog.setContentView(R.layout.rewrite_dialog_two);
//			break;
//		default:
//			customProgressDialog.setContentView(R.layout.rewrite_dialog);
//			break;
//		}

		customProgressDialog = new RewriteDialog(context, R.style.Dialog);
		customProgressDialog.setContentView(R.layout.rewrite_dialog_three);
		customProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;

		return customProgressDialog;
	}

	public void onWindowFocusChanged(boolean hasFocus) {
		if (customProgressDialog == null) {
			return;
		}
//		ImageView imageView = (ImageView) customProgressDialog.findViewById(R.id.loadingImageView);
//		AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
//		animationDrawable.start();
	}

	/**
	 * 
	 * [Summary] setTitile 标题
	 * 
	 * @param strTitle
	 * @return
	 * 
	 */
	public RewriteDialog setTitile(String strTitle) {
		return customProgressDialog;
	}

	/**
	 * 
	 * [Summary] setMessage 提示内容
	 * 
	 * @param strMessage
	 * @return
	 * 
	 */
	public RewriteDialog setMessage(String strMessage) {
		TextView tvMsg = (TextView) customProgressDialog.findViewById(R.id.id_tv_loadingmsg);

		if (tvMsg != null) {
			tvMsg.setText(strMessage);
		}
		return customProgressDialog;
	}

	public void showMessageOnUiThread(Activity activity) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {

			}
		});
	}
}
