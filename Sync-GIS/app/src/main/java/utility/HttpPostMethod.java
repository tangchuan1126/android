package utility;

public class HttpPostMethod {
	
	public static String SysUser = "SysUser";//获取人员信息
	
	public static String CheckInTransports = "CheckInTransports";
	public static String PrintLicensePlate = "PrintLicensePlate";
	public static String TRANSPORTLP = "TransportLp";
	public static String LoadLpBaseInfo = "LoadLpBaseInfo";
	public static String TransportCheckInOver = "TransportCheckInOver" ;
	
	public static String TransportOutbound =  "TransportOutbound" ;	  
	//Inbound
	public static String InboundBaseInfo = "InboundBaseInfo";
	public static String InboundTransportBaseData = "InboundTransportBaseData";
	public static String TransportInboundImage = "TransportInboundImage";
	public static String DeleteTransportFile = "DeleteTransportFile";
	public static String TransportPositionBaseInfo = "TransportPositionBaseInfo";			//position load baseInfo
	
	public static String TransportReceiveCompare = "TransportReceiveCompare" ;  
	public static String TransportReceiveComplete = "TransportReceiveComplete" ; 
	public static String TransportReceivePut = "TransportReceivePut" ;  
	public static String TransportReceiveOver = "TransportReceiveOver";
	
	/**
	 * outbound
	 */
	public static String OutBoundList = "OutBoundList";				//��ȡ��Щװ�˵���Ҫ����
	public static String OutBoundBaseInfo = "OutBoundBaseInfo" ;	//��ȡ����Ϣ
	public static String TransportDetail = "TransportDetail";		//��ȡĳ��װ�˵�����Ҫ�������Ʒ
	public static String TransportSend = "TransportSend";			//�ύ��������
	public static String TransportOutboundCompare = "TransportOutboundCompare" ;	//TransportOutboundCompare�ύ���رȽ���ϸ
	public static String TransportOutboundComplete = "TransportOutboundComplete" ;	//TransportOutboundComplete�ύ���رȽϽ�������
	public static String TransportOutboundOver = "TransportOutboundOver" ;	//outboudn  

	
	//position 
	public static String PositionReceiveBaseData = "PositionReceiveBaseData"; //position baseData ;	
	public static String ReceivePutProductLocation = "ReceivePutProductLocation" ; // 
	public static String AlreadyPut = "AlreadyPut"; // 
	public static String WaitPutReceive = "WaitPutReceive";	// 
	public static String TransportReceive = "TransportReceive";	// 
	public static String PutOver = "PutOver";
	
	// position
	public static String PositionReceiveTransportList = "PositionReceiveTransportList"  ; //positionReceiveTransportList
	
	//pickup
	public static String DownLoadOutBill = "DownLoadOutBill"; //����pickUp Id 
	public static String DownLoadOutBillLocationPoroduct = "DownLoadOutBillLocationPoroduct";
	public static String PickUpPoroduct = "PickUpPoroduct";
	public static String PickUpException = "PickUpException";
	
	//inventory
	public static String TakeStock = "TakeStock"; // inventory 在位置上提交所有的接地容器
	public static String OperatorArea = "OperatorArea"; //
	public static String GetProductStoreLocation = "GetProductStoreLocation"; //
	public static String TakeStockOver = "TakeStockOver";		//区域上提交AreaId 该区域提交完成，然后会返回差异的编号
	public static String TakeStockApprove = "TakeStockApprove";	 //
	
	//AndroidLoadFile
	public static String LoadFilePinterTCP = "LoadFilePinterTCP" ;	//LoadFilePinterTCP

	
	public static String addProduct = "addProduct";
	public static String addProductUnion = "addProductUnion";
	public static String modProduct = "modProduct";
	public static String delProduct = "delProduct";
	public static String addProductCode = "addProductCode";
	public static String ADDPRODUCTCODEFORCHECK = "addProductCodeForCheck";
	public static String delProductUnion = "delProductUnion";
	public static String modProductUnion = "modProductUnion";
	public static String modProductCatalog="modProductCatalog";
	public static String delProductFile = "delProductFile";
	public static String FindTitle = "FindTitle" ;
	//delivery
	public static String GetTransportByMachine = "GetTransportByMachine" ;
	//version update
	public static String LoadVersion = "LoadVersion" ;
	public static String LoadApk = "LoadApk" ;
	//baseData
	public static String LoadProductAndPicture = "LoadProductAndPicture" ;
	public static String RefreshProduct = "RefreshProduct" ;
	public static String ClearInboundDoor = "ClearInboundDoor" ;
	public static String ClearOutBoundDoor = "ClearOutBoundDoor";
	
	public static String SubmitContainer = "SubmitContainer";
	public static String SubmitTempContainer = "SubmitTempContainer";
	public static String CopyContainer = "CopyContainer";
	public static String InnerContainerLoad = "InnerContainerLoad";
	public static String CreateTLPOnTemp = "CreateTLPOnTemp";
	
	/**
	 * baseProductInfo
	 */
	public static String GetBaseProductInfo = "GetBaseProductInfo";
	public static String AddProductCode = "AddProductCode";	
	public static String DeleteProductFile = "DeleteProductFile";
	public static String GetLoginTitle = "GetLoginTitle" ;
	public static String GetProductInfo = "GetAllTitle" ;//获取title数据信息
	/**
	 * 
	 * checkin
	 */
	public static String SearchLoading="SearchLoading";
	public static String AddGateCheckIn="AddGateCheckIn";
	public static String CheckInMainList="CheckInMainList";
	public static String CheckInHasRecode="CheckInHasRecode";
	public static String SearchDockCloseByEntry="SearchDockCloseByEntry";
	public static String GetEntryFreightTerm="GetEntryFreightTerm";
	public static String CheckInGetDetail="CheckInGetDetail";
	public static String AndroidWareHouseData="AndroidWareHouseData";
	public static String GetPackDetails="GetPackDetails";
	public static String CheckInGetUsableDoor="CheckInGetUsableDoor";
	public static String CheckInGetOccupyDoor="CheckInGetOccupyDoor";
//	public static String AddCheckWindowDetails="AddCheckWindowDetails";
	public static String AddCheckWareHouseSchedule="AddCheckWareHouseSchedule";
	public static String CheckOut="CheckOut";
	public static String AndroidCheckInWindowStop="AndroidCheckInWindowStop";
	public static String WahouseCheckIn="WahouseCheckIn";
	public static String CloseBill="CloseBill";
	public static String DockCloseBill="DockCloseBill";
	public static String DockCloseUpPhoto="DockCloseUpPhoto";
	public static String BatchDockCloseBill="BatchDockCloseBill";
	public static String DockCloseReleaseDoor="DockCloseReleaseDoor";
	public static String CloseLoadStayOrLeave="CloseLoadStayOrLeave";
	public static String CloseLoadRelaseDoor ="CloseLoadRelaseDoor ";
	public static String findPickUpSealByEntryId="findPickUpSealByEntryId";
	public static String selectLoadBar="selectLoadBar";
	public static String modPickUpSeal="modPickUpSeal";
	public static String CloseLoadInputSeal="CloseLoadInputSeal";
	public static String GetCheckOutData = "GetCheckOutData";/** getcheckout 获取数据 **/
	public static String GetCheckOutDataSave = "GetCheckOutDataSave";
	public static String GetCheckOutDataSearch="GetCheckOutDataSearch";
	public static String IsNotifyPushAndroidMessage = "IsNotifyPushAndroidMessage";
	public static String GetUseDoorOrSpot = "GetUseDoorOrSpot";/** getcheckout 获取数据 **/
	public static String DetAndroidNotices="DetAndroidNotices";/** 通知删除**/
	public static String CheckOutSearchEntryId="CheckOutSearchEntryId";
	public static String GetSpotArea="GetSpotArea";
	public static String GetDoorAndSpot="GetDoorAndSpot";
	public static String VerificationGps="VerificationGps";
	public static String PickUpSearchNumberGetDoorOrLocation="PickUpSearchNumberGetDoorOrLocation";
	public static String GetLoading="GetLoading";
	public static String GetZoneByTitle="GetZoneByTitle";
	
	public static String patrolApproveList="patrolApproveList";
	public static String submitPatrolApprove="submitPatrolApprove";
	public static String createEntryId="createEntryId";
	public static String CloseEntryDetail="CloseEntryDetail";
	public static String CloseSyncBillStatus="CloseSyncBillStatus";
	
	
	//title
	public static String LoginTitle = "LoginTitle";
	public static String SearhProductByCondition = "SearhProductByCondition";
	public static String SearchProductByDim = "SearchProductByDim" ;
	
	//print    CreateContatinerPrintActivity
	public static String BaseTLPContainerTypes = "BaseTLPContainerTypes";
	public static String AndroidLinkWebPrint = "AndroidLinkWebPrint";
	public static String AndroidCtnrWebPrint = "AndroidCtnrWebPrint";
	public static String AndroidLinkWebPrintGetPath = "AndroidLinkWebPrintGetPath";
	public static String LoadGetBillOfLadingInfo = "LoadGetBillOfLadingInfo";				
	public static String BillOfLoadingPrintTask = "BillOfLoadingPrintTask";
	public static String LoadingTickPrintTask = "LoadingTickPrintTask";
	public static String ReceiptsTicketPrintTask = "ReceiptsTicketPrintTask";
	public static String CountingSheetPrintTask = "CountingSheetPrintTask";
	
	//jiang
	public static String BillOfLoadingSignature = "BillOfLoadingSignature";
	public static String FindLoadOrderItems = "FindLoadOrderItems";
	public static String GetPalletTypeByCompanyId = "GetPalletTypeByCompanyId";
	public static String ConlidateChangePalletsNumber = "ConlidateChangePalletsNumber";
	public static String ShippingLabelPrintTask = "ShippingLabelPrintTask";
	public static String ChangeOrderItemPallets = "ChangeOrderItemPallets";
	public static String ConfirmPlateByOrder = "ConfirmPlateByOrder";
	public static String PlateBindSequence = "PlateBindSequence";

	//checkin_load
	public static String SubmitLoadPalletInfo="SubmitLoadPalletInfo";
	
	public static String EntryTaskEquipment = "EntryTaskEquipment"; //Entry 查询的 "; //Entry 查询的 
	public static String DockCheckInSearchEntryDetail = "DockCheckInSearchEntryDetail"; //Entry 查询的 
	public static String DockCheckInDeleteNotice = "DockCheckInDeleteNotice"; //  删除通知的
	public static String DockCheckInSubmit = "DockCheckInSubmit"; //提交的界面的按钮 Submit
	public static String DockCheckInAssignTask = "DockCheckInAssignTask"; // 选择任务Assign
	public static String DockCheckInTaskMoveToDoor = "DockCheckInTaskMoveToDoor"; //换门
	
	// window check in
	public static String WindowCheckInList = "WindowCheckInList";
	public static String AssginTaskListByEntry = "AssginTaskListByEntry";
	public static String WindowCheckInEntryEquipmentList = "WindowCheckInEntryEquipmentList";
	public static String AddOrUpdateEquipment = "AddOrUpdateEquipment";
	public static String DeleteEquipment = "DeleteEquipment";
	public static String TaskInfosByTaskId = "TaskInfosByTaskId";
	public static String DeleteTask = "DeleteTask";
	public static String TaskChangeResourceOrEquipment = "TaskChangeResourceOrEquipment";
	
	public static String WindowCheckInGetEquipmentInfos = "WindowCheckInGetEquipmentInfos";
	public static String CountUncompletedTask = "CountUncompletedTask";

}
