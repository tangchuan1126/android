package utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class StringUtil {
	
	/**
	 * String数组去重复值
	 */
	public static String[] removeDuplicateWithOrder(List<String> list) {
		Set<String> set = new HashSet<String>(list);
		return (String[]) set.toArray(new String[0]);
	}

	// 匹配字符串最后的几个数字,比如DK13,DK 13, 99(DK)13,都返回13,不支持99(DK)13.23 这种只会返回23
	private static final Pattern LastStringIntPattern = Pattern.compile("^.*((?<!\\d)\\d+).*$");

	private static final String NULL = "NULL";

	private static ForegroundColorSpan span = new ForegroundColorSpan(Color.RED);

	public static String getXmlElementStringValue(NodeList nodeList) {
		if (nodeList != null && nodeList.getLength() > 0) {
			Node node = nodeList.item(0);
			if (node != null) {
				Node tempNode = node.getFirstChild();
				if (tempNode != null) {
					return tempNode.getNodeValue();
				}
			}
		}
		return "";
	}

	public static Double getDoubleXmlElementDoubleValue(NodeList nodeList) {
		String value = getXmlElementStringValue(nodeList);
		if (value == null || value.length() < 1) {
			value = "0.0";
		}
		return Double.parseDouble(value);
	}

	public static long getLongXmlElementLongValue(NodeList nodeList) {
		String value = getXmlElementStringValue(nodeList);
		if (value == null || value.length() < 1) {
			value = "0";
		}
		return Long.parseLong(value);
	}

	public static String HashBase64(String str) {
		String ret = "";
		try {
			// Hash算法
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(str.getBytes());
			ret = Base64.encodeToString(sha.digest(), Base64.NO_WRAP);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return ret;
	}

	/**
	 * 获得节点数据（只适合唯一名称节点）
	 * 
	 * @return
	 */
	public static String getSampleNode(String xml, String name) throws Exception {
		if (xml.indexOf(name) != -1) {
			String xmlSplit1[] = xml.split("<" + name + ">");
			if (xmlSplit1 != null && xmlSplit1.length > 0) {
				String xmlSplit2[] = xmlSplit1[1].split("</" + name + ">");
				if (xmlSplit2 != null && xmlSplit2.length > 0) {
					return (xmlSplit2[0]);
				}
			}
		}
		return "";

	}

	public static String convertInputStreamToString(InputStream inputStream) {
		StringBuilder stringBuilder = new StringBuilder("");
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			boolean firstLine = true;
			String line = null;
			;
			while ((line = bufferedReader.readLine()) != null) {
				if (!firstLine) {
					stringBuilder.append(System.getProperty("line.separator"));
				} else {
					firstLine = false;
				}
				stringBuilder.append(line);
			}
			bufferedReader.close();
		} catch (Exception e) {
		}
		return stringBuilder.toString();
	}

	public static String addSpace(String value) {

		if (value != null && value.length() > 0) {
			StringBuffer sb = new StringBuffer();
			for (int index = 0, count = value.length(); index < count; index++) {
				sb.append(value.charAt(index) + "").append(" ");
			}
			return sb.toString();
		}
		return "";
	}

	public static boolean isInteger(String value) {
		boolean flag = true;
		try {
			Integer.parseInt(value.trim());
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 检查浮点数
	 * 
	 * @param num
	 * @param type
	 *            "0+":非负浮点数 "+":正浮点数 "-0":非正浮点数 "-":负浮点数 "":浮点数
	 * @return
	 */
	public static boolean checkFloat(String num, String type) {
		String eL = "";
		if (type.equals("0+"))
			eL = "^\\d+(\\.\\d+)?$";// 非负浮点数
		else if (type.equals("+"))
			eL = "^((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*))$";// 正浮点数
		else if (type.equals("-0"))
			eL = "^((-\\d+(\\.\\d+)?)|(0+(\\.0+)?))$";// 非正浮点数
		else if (type.equals("-"))
			eL = "^(-((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*)))$";// 负浮点数
		else
			eL = "^(-?\\d+)(\\.\\d+)?$";// 浮点数
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(num);
		boolean b = m.matches();
		return b;
	}

	public static SpannableStringBuilder getSpannable(String value) {
		SpannableStringBuilder spannable = null;
		if (null != value) {
			spannable = new SpannableStringBuilder(value);
			spannable.setSpan(span, 0, value.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			spannable = new SpannableStringBuilder();
		}
		return spannable;

	}

	public static String returnStringValue(String value) {
		if (value != null && !value.trim().toUpperCase().equals("NULL")) {
			return value;
		}
		return "";
	}


	public static double convert2Double(String value) {
		double result = 0.0d;
		if (value != null && value.trim().length() > 0) {
			try {
				result = Double.parseDouble(value);
			} catch (Exception e) {
			}
		}
		return result;
	}

	public static long convert2Long(String value) {
		long returnValue = 0l;
		if (value != null && value.trim().length() > 0) {
			try {
				returnValue = Long.parseLong(value.trim());
			} catch (Exception e) {
			}
		}
		return returnValue;
	}

	public static boolean isNullOfLong(String value) {
		long l = 0l;
		if (value != null && value.trim().length() > 0) {
			try {
				l = Long.parseLong(value);
			} catch (Exception e) {
				l = 0l;
			}
		}
		return l == 0l;
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		// 获取ListView对应的Adapter
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);
	}

	public static double convertToV(double hight, double width, double length) {
		return hight * width * length / 1000;
	}

	public static int convert2Inter(String value) {
		return (int) convert2Long(value);
	}

	public static String getUpFilePictureFileName(String fileName) {
		if (fileName != null && fileName.indexOf(".") != -1) {
			return fileName.substring(0, fileName.indexOf(".")) + "_up.jpg";

		}
		return fileName;
	}

	public static boolean isNullOfStr(String value) {
		return !(value != null && value.trim().length() > 0 && !value.toUpperCase().equals(NULL) && !value.toUpperCase().equals("NA")
				&& !value.toUpperCase().equals("N/A") && !value.toUpperCase().equals("NA_") && !value.toUpperCase().equals("NA/") && !value.toUpperCase().equals("0"));
	}

	public static String convertArrayToStr(String[] arrays) {
		StringBuffer sb = new StringBuffer();
		if (arrays != null && arrays.length > 0) {
			for (String temp : arrays) {
				sb.append(",");
				sb.append(temp);
			}
		}
		return sb.length() < 1 ? "" : sb.substring(1);
	}

	/**
	 * 返回是否扫描的Container开头的数据
	 * 
	 * @param value
	 *            testValue
	 * @param types
	 * @return
	 */
	public static boolean isBeginWithContainer(String value, String[] types) {
		boolean flag = false;
		if (value != null && value.length() > 0) {
			value = value.toUpperCase();

			for (String type : types) {
				if (value.indexOf(type) != -1) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	public static JSONObject converntString(String jsonStr) {
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jsonStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static String getJsonString(JSONObject json, String fileName) {
		String value = "";
		try {
			value = json.getString(fileName);
		} catch (Exception e) {

		}
		return value;
	}

	public static double getJsonDouble(JSONObject json, String fileName) {
		double result = 0.0d;
		try {
			result = Double.parseDouble(json.getString(fileName));
		} catch (Exception e) {
		}
		return result;
	}

	public static int getJsonInt(JSONObject json, String fileName) {
		int result = 0;
		try {
			result = Integer.parseInt(json.getString(fileName));
		} catch (Exception e) {
		}
		return result;
	}

	public static JSONObject getJsonObjectFromArray(JSONArray array, int index) {
		JSONObject returnJson = null;
		try {
			return fixJSONObject(array.getJSONObject(index));
		} catch (Exception e) {
		}
		return returnJson;
	}

	/**
	 * @param json
	 * @param key
	 * @return
	 */
	public static JSONArray getJsonArrayFromJson(JSONObject json, String key) {
		JSONArray returnArray = null;
		try {
			return json.getJSONArray(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnArray;
	}

	/**
	 * @param json
	 * @param key
	 * @return
	 */
	public static String getStringFromJsonArray(JSONArray array, int index) {
		String returnArray = null;
		try {
			return array.getString(index);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnArray;
	}

	/**
	 * {} 返回为null
	 * 
	 * @param json
	 * @param key
	 * @return
	 */
	public static JSONObject getJsonObjectFromJSONObject(JSONObject json, String key) {
		JSONObject jsonObject = null;
		try {
			jsonObject = json.getJSONObject(key);
			return fixJSONObject(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * 处理如果是{} 那么直接返回null
	 * 
	 * @return
	 */
	private static JSONObject fixJSONObject(JSONObject jsonObject) {
		if (jsonObject != null && jsonObject.keys().hasNext()) {
			return jsonObject;
		}
		return null;
	}

	public static JSONObject getJsonObjectFromString(String str) {
		try {
			return fixJSONObject(new JSONObject(str));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static JSONArray getJSONArray(String source) {
		try {
			return new JSONArray(source);
		} catch (JSONException e) {
			return null;
		} catch (Exception e) {
			return null;
		}

	}

	public static Long getJsonLong(JSONObject json, String fileName) {
		Long result = 0l;
		try {
			result = Long.parseLong(json.getString(fileName));
		} catch (Exception e) {
		}
		return result;
	}

	public static double convertStrToDouble(String value) {
		double returnValue = 0.0d;
		try {
			returnValue = Double.parseDouble(value);
		} catch (Exception e) {
		}
		return returnValue;
	}

	/**
	 * @Description:判断JSONArray是否为空 true为空的状态 false是不为空的状态
	 * @author gcy
	 */
	public static boolean isNullForJSONArray(JSONArray array){
		boolean flag = true;
		if(array!=null&&array.length()>0&&!"[]".equals(array)){
			flag = false;
		}
		return flag;
	}
	
	/**
	 * 在TLP3434343 中找出他的ID也就是返回3434343
	 * 
	 * @param container
	 * @return
	 * @descrition
	 */
	public static long findLongInStringValue(String container) {
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(container);
		if (matcher.find()) {
			return StringUtil.convert2Long(matcher.group());
		}
		return 0l;
	}

	public static int convertDoubleStringToInt(String value) {
		int returnValue = 0;
		try {
			returnValue = (int) Double.parseDouble(value.trim());
		} catch (Exception e) {
		}
		return returnValue;
	}

	/**
	 * 验证ip是否合法
	 * 
	 * @param textip地址
	 * @return 验证信息
	 */
	public static boolean ipCheck(String text) {
		if (text != null && !TextUtils.isEmpty(text)) {
			// 定义正则表达式
			String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
					+ "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\." + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
			// 判断ip地址是否与正则表达式匹配
			if (text.matches(regex)) {
				// 返回判断信息
				return true;
			} else {
				// 返回判断信息
				return false;
			}
		}
		// 返回判断信息
		return false;
	}

	public static String findLastStringNumber(String value) {
		if (value.length() > 4) {
			if (value.substring(0, 3).equals("99(")) {
				value = value.substring(5);
			} else if (value.substring(0, 3).equals("99D")) {
				value = value.substring(4);
			} else if (value.substring(0, 3).equals("99 ")) {
				value = value.substring(5);
			}
		}

		Matcher matcher = LastStringIntPattern.matcher(value);
		if (matcher.matches())
			return matcher.group(1);
		return "";
	}

	public static String findLastStringNumbers(String value) {

		Matcher matcher = LastStringIntPattern.matcher(value);
		if (matcher.matches())
			return matcher.group(1);
		return "";
	}

	/**
	 * 
	 * 扫描出来的门有可能是DK50 扫面出来的门有可能是99DK50 扫面出来的有可能是50.
	 * 
	 * @param value
	 * @return
	 * @descrition
	 */
	public static String fixScanDoor(String value, int num) {
		if (num == 0) {
			return findLastStringNumber(value);
		} else {
			return findLastStringNumbers(value);
		}
	}

	public static String toUpperCase(String value) {
		if (!StringUtil.isNullOfStr(value)) {
			return value.toUpperCase();
		}
		return "";
	}

	/**
	 * 验证端口是否合法
	 * 
	 * @param 端口
	 * @return 验证信息
	 */
	public static boolean portCheck(String text) {
		if (text != null && !TextUtils.isEmpty(text)) {
			// 定义正则表达式
			String regex = "^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-5][0-5][0-3][0-5]$)";
			// 判断ip地址是否与正则表达式匹配
			return Pattern.matches(regex, text);
		}
		// 返回判断信息
		return false;
	}
}
