package key;

import java.util.HashMap;
import java.util.Map;

import utility.StringUtil;

public class EntryDetailNumberStateKey {

	public static int Unprocess = 1;
	public static int Processing = 2;
	public static int Close = 3;
	public static int Exception = 4;
	public static int Partially = 5;

	// ========朱成==========================
	public static int TakeOver = -1;

	private static Map<Integer, String> values;
	static {
		values = new HashMap<Integer, String>();
		values.put(Unprocess, "Unprocess");
		values.put(Processing, "Processing");
		values.put(Close, "Closed");
		values.put(Exception, "Exception");
		values.put(Partially, "Partially");
	}

	public static String getType(int key) {
		return values.get(key);
	}

	public static String getType(String key) {
		return getType(StringUtil.convert2Inter(key));
	}

	public static boolean isClose(int key) {
		return key == Close || key == Exception || key == Partially;
	}

}
