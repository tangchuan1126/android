package key;

import java.util.HashMap;
import java.util.Map;

public class CheckInEntryPriorityKey {

	private static Map<Integer, String> row;
	public static int NA = 0;
	public static int HIGH = 1;
	public static int MIDDLE = 2;
	public static int LOW = 3;

	static {
		row = new HashMap<Integer, String>();
		row.put(CheckInEntryPriorityKey.NA, "NA");
		row.put(CheckInEntryPriorityKey.HIGH, "High");
		row.put(CheckInEntryPriorityKey.MIDDLE, "Middle");
		row.put(CheckInEntryPriorityKey.LOW, "Low");
	}

	public static String getCheckInEntryPriorityKey(int id) {
		return String.valueOf(row.get(id));
	}

	public static int getPositionToKey(int position) {
		switch (position) {
		case 0:
			return CheckInEntryPriorityKey.HIGH;
		case 1:
			return CheckInEntryPriorityKey.MIDDLE;
		case 2:
			return CheckInEntryPriorityKey.LOW;
		}
		return CheckInEntryPriorityKey.NA;
	}
}
