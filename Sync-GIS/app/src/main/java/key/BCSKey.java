package key;

import java.util.HashMap;
import java.util.Map;


public class BCSKey
{

	
	
	
	/**
	 * ret == 1 表示成功
	 * ret == 0 表示服务器错误 然后err 的数字对应下面的错误
	 */
 	
	 
	public final static int FAIL = 0;	//操作失败
	public final static int SUCCESS = 1;	//操作成功
	
	public final static int Err_WithMsg = 90 ;	//出错,同时data为出错消息
	
  	public final static int SYSTEMERROR = 2;//系统内部错误
	public final static int LOGINERROR = 3;//登录失败,账号密码不匹配
	public final static int OPERATIONNOTPERMIT = 7;//无权限操作
	public final static int NOTEXITSPRODUCT = 10;//系统内无此商品 
	public final static int NETWORKEXCEPTION = 11;//网络传输错误 
	public final static int NOEXITSDELIVERYORDER = 12;//交货单不存在 
	public final static int SerialNumberRepeat = 20;	//序列号重复
	public final static int JSONERROR = 21 ;	//序列号解析出错
	public final static int ProductCodeIsExist = 26; //商品条码已经存在
	public final static int LPNotFound = 33 ;	//container 不存在
	public final static int SerchLoadingNotFoundException = 36 ;	//SerchLoadingNot	checkin Loading search没有找到

	public final static int ProductPictureDeleteFailed = 35 ;	//图片删除失败
	public final static int TitleNotFoundException = 37 ;	//TitleNotFoundException
	public final static int CheckInNotFound = 40 ;				//CHECKIN主单据没
	public final static int AppVersionException = 41 ;			//android 版本的更新问题
	public final static int AndroidPrintServerUnEable = 43 ; 	// android 打印服务器不可用
	public final static int NoPermiessionEntryIdException = 44 ; 	// 当前CheckInEntry的 没有权限
	public final static int PrintServerNoPrintException = 45 ; 	// 当前的PrintServer没有设置print
	public final static int NoRecordsException=46;
	public final static int LoadIsCloseException = 47;				//load is close 
	public final static int LoadNumberNotExistException = 48;				//load is close 
	public final static int DataFormateException = 49 ;				//数据格式错误
	public final static int DockCheckInFirstException = 50 ;		//Dock Check In First
	public final static int SumsangPalletNotFound = 51 ; 			//
	public final static int ContainerNotFoundException = 52 ;  		//Container not found
	public final static int CheckInEntryIsLeftException = 54 ;  		//Container not found
	public final static int DoorHasUsedException = 55 ;					//Door has 	RESERVERED,OCUPIED
	public final static int SpotHasUsedException = 56 ;					//Spot has 	RESERVERED,OCUPIED

	public final static int CheckTaskProcessingChangeTaskToDockException = 57 ;	 //
	public final static int EquipmentNotFindException = 58 ;			//EquipmentNotFindException			 
	public final static int DeleteFileException  = 59 ;					//删除文件失败
	public final static int AddLoadBarUseFoundException =60 ;				//添加Load的失败
	public final static int EquipmentHadOutException = 61 ;					//equipment is Left;
	public final static int DeletePalletNoFailedException = 62 ;					//DeletePalletNoFailedException
	public final static int EntryTaskHasFinishException = 63 ;				//EntryTaskHasFinishException
	public final static int EquipmentHasDecidedPickedException = 64;
	public final static int EquipmentOccupyResourcesException = 65;
	public final static int ResourceHasUsedException = 66;
	public final static int DoorNotFindException = 67;
	public final static int SpotNotFindException = 68;
	public final static int EquipmentInYardException = 69;
	public final static int CheckInTaskCanntDeleteException = 70;
	public final static int CheckInTaskRepeatToEntryException = 71;
	public final static int CheckInTaskRepeatToOthersException = 72;
	public final static int CheckInMustHasTasksException = 73;
	public final static int CheckInAllTasksClosedException = 74;
	public final static int CheckInMustNotHaveTasksException = 75;
	public final static int CheckInTaskHaveDeletedException = 76;
	public final static int EquipmentHasLoadedOrdersException = 77;
	public final static int CheckinTaskNotFoundException = 78;
	public final static int EquipmentSameToEntryException = 79;					//  Repeat Add Equipment
	public final static int UpdatePalletTypeFailedException = 80;					// Update Pallet Type Failed 
	public final static int TaskNoAssignWarehouseSupervisorException = 81;	    //TaskWareHouse 
	public final static int UserIsOnLineCantLoginException = 82;				// 当前登录的人已经在线
	public final static int AccountNotPermitLoginException = 83 ;				//登录没有权限
	public final static int OperationNotPermitException = 84 ;					//没有操作权限
	public final static int AccountNullpointException = 85 ; 	//传递的account 为空	  
	public final static int NoUserByaccountException = 86 ; 	//通过account没查到user信息
	public final static int GetAdidException = 87 ; 		 //未获取到adid
	
	final static Map<String, String> row = new HashMap<String, String>();
	static{

		row.put(String.valueOf(FAIL), "Fail");	//操作失败
		row.put(String.valueOf(SUCCESS), "Success");//
 		row.put(String.valueOf(SYSTEMERROR), "System Error");	//系统内部错误
		row.put(String.valueOf(LOGINERROR), "Login Failed");//登录失败,账号密码不匹配
		row.put(String.valueOf(OPERATIONNOTPERMIT), "无权限操作");
		row.put(String.valueOf(NOTEXITSPRODUCT), "系统内无此商品 ");
		row.put(String.valueOf(NETWORKEXCEPTION), "Net Error");	//网络传输错误
		row.put(String.valueOf(NOEXITSDELIVERYORDER), "交货单不存在 ");
		row.put(String.valueOf(SerialNumberRepeat), "序列号重复");
		row.put(String.valueOf(JSONERROR), "JSON parse Error");
		row.put(String.valueOf(ProductCodeIsExist), "Product Code repeat.");
		row.put(String.valueOf(LPNotFound), "Container Not Found"); //容器不存在
		row.put(String.valueOf(ProductPictureDeleteFailed), "Delete Failed"); //图片删除失败
		row.put(String.valueOf(SerchLoadingNotFoundException), "SerchLoading NotFound.");
		row.put(String.valueOf(TitleNotFoundException), "Title NotFound.");
		row.put(String.valueOf(CheckInNotFound), "Entry ID Not Found.");
		row.put(String.valueOf(AppVersionException), "New version is ready for download, please remove old APP first.");
		row.put(String.valueOf(AndroidPrintServerUnEable), "Print Server Unable");
		row.put(String.valueOf(NoPermiessionEntryIdException), "No Access");
		row.put(String.valueOf(PrintServerNoPrintException), "The Print Server No Printer");
		row.put(String.valueOf(NoRecordsException), "No Records");
		row.put(String.valueOf(LoadIsCloseException), "LoadNumber Is Closed");
		row.put(String.valueOf(DataFormateException), "Data Formate Error ");
		row.put(String.valueOf(DockCheckInFirstException), "Dock CheckIn First");
		row.put(String.valueOf(SumsangPalletNotFound), "Pallet  Not Found");
		row.put(String.valueOf(ContainerNotFoundException), "Container Not Found");
		row.put(String.valueOf(CheckInEntryIsLeftException), "Entry Already Gate Check Out");
		row.put(String.valueOf(DoorHasUsedException), "Door Is Reservred or Occupied");
		row.put(String.valueOf(SpotHasUsedException), "Spot Is Reservred or Occupied");
		
		row.put(String.valueOf(CheckTaskProcessingChangeTaskToDockException), "Change Task To Door ,First!");
		row.put(String.valueOf(EquipmentNotFindException), "Equipment Not Found");
		row.put(String.valueOf(DeleteFileException), "Delete File Failed");
		row.put(String.valueOf(AddLoadBarUseFoundException), "Add Load Bar Failed");
		row.put(String.valueOf(EquipmentHadOutException), "Equipment Had Left");
		row.put(String.valueOf(DeletePalletNoFailedException), "Delete Pallet Failed");
		row.put(String.valueOf(EntryTaskHasFinishException), "Task Has Finish");
		row.put(String.valueOf(EquipmentSameToEntryException), "Repeat Equipment");
		row.put(String.valueOf(UpdatePalletTypeFailedException), "Update Pallet Type Failed");


		row.put(String.valueOf(EquipmentInYardException), "Equipment In Yard");
		row.put(String.valueOf(CheckInTaskCanntDeleteException), "Cann't delete");
		row.put(String.valueOf(CheckInTaskRepeatToEntryException), "Task repeat");
		row.put(String.valueOf(CheckInTaskRepeatToOthersException), "Task repeat to other entry");
		row.put(String.valueOf(CheckInMustHasTasksException), "Task first");
		row.put(String.valueOf(CheckInAllTasksClosedException), "Task closed");
		row.put(String.valueOf(CheckInMustNotHaveTasksException), "Delete task first");
		row.put(String.valueOf(CheckInTaskHaveDeletedException), "Task deleted");
		row.put(String.valueOf(EquipmentHasLoadedOrdersException), "Task has loaded");
		row.put(String.valueOf(CheckinTaskNotFoundException), "Task not found");
		row.put(String.valueOf(EquipmentSameToEntryException), "Equipment repeat");
		row.put(String.valueOf(AccountNotPermitLoginException), "No Access");
		row.put(String.valueOf(TaskNoAssignWarehouseSupervisorException), "All task need assign supervisor");
		row.put(String.valueOf(TaskNoAssignWarehouseSupervisorException), "All task need assign supervisor");
		row.put(String.valueOf(OperationNotPermitException), "No Permission");
		
		row.put(String.valueOf(AccountNullpointException), "No other information!");
		row.put(String.valueOf(NoUserByaccountException), "No other information!");
		row.put(String.valueOf(GetAdidException), "No other ADID!");

		
		
	}

	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(int id)
	{
		return getReturnStatusById(id+"");
	}
	
	
	
	/**
	 * 通过ID获取返回信息
	 * @param id
	 * @return
	 */
	public static String getReturnStatusById(String id)
	{
		return String.valueOf(row.get(id));
	}
	
}
