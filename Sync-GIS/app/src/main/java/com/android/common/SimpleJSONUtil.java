package com.android.common;

import java.util.List;
import java.util.Map;

import key.BCSKey;

import org.apache.http.Header;
import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONObject;

import oso.gis.GISApplication;
import oso.gis.GMapActivity;
import oso.gis.R;
import utility.AlertUi;
import utility.RewriteDialog;
import utility.StringUtil;
import utility.UIHelper;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

/**
 * 0.这个类的目的是除去，很多错误信息的重复判断<br />
 * 1.只用于 请求简单的JSON数据格式的时候<br />
 * 2.handReponseJson 处理返回的JSON 格式的数据<br />
 * 3.handFinish 处理不管是错误，或者是正确执行的方法<br />
 * 4.现在很多的处理都是获取数据,然后存储在数据库中。所以在handReponseJson，写入数据库中 <br />
 * 5.handFinish 请求完成过后，然后读取数据库里面的数据(你也可以没有)
 * 
 * @author zhangrui
 * 
 */
public abstract class SimpleJSONUtil {

	public static AsyncHttpClient client = AsyncHttpClientGenerate.getInstance();
	static {
		client.setTimeout(30 * 1000);
	}

	private boolean showLoadingDlg = true;

	public abstract void handReponseJson(JSONObject json);

	public void handReponseJsonArray(JSONArray array) {
	}

	/**
	 * 成功、失败、cancel
	 */
	public void handFinish() {
	};

	public void handFail() {
	};

	public RequestHandle reqHandle; // 请求句柄

	// 入参:1>inSilence:静默调用,即不作任何ui提示
	public void doPost(final String url, final RequestParams params, final Context context, boolean inSilence) {
		// 非静默-访问
		if (!inSilence) {
			doPost(url, params, context);
		}
		client.addHeader("Cookie", GMapActivity.cookie);

		reqHandle = client.post(url, params, new JsonHttpResponseHandler("utf-8") {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				// 网络异常
				if (statusCode != 200) {
					handFail();
					return;
				}

				// 外界处理
				if (useTheCustom(json))
					return;

				// 请求成功
				if (StringUtil.getJsonInt(json, "ret") == 1) {
					handReponseJson(json);
				} else {
					// 版本问题
					// if (StringUtil.getJsonInt(json, "err") ==
					// BCSKey.AppVersionException)
					// return;
					// 回调
					handFail();
				}
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				super.onCancel();
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				handFail();
			}

			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				// 若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}

	public void doPost(final String url, final RequestParams params, final Context context) {

		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(true);
		client.addHeader("Cookie", GMapActivity.cookie);

		// 改成HttpClient 的方式，放回的为JSON的数据
		reqHandle = client.post(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				if (showLoadingDlg)
					dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				final String appendNofity = resources.getString(R.string.sys_load_data_failed);
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						processReturn(context, json, appendNofity);
					}
				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_submit_data_failed), Toast.LENGTH_SHORT).show();
					handFail();
				}
			}

			// debug,上传文件微服务返回的是jsonArray,shit
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing())
					dialog.dismiss();
				super.onSuccess(statusCode, headers, response);
				useTheCustom(response);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				super.onCancel();
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				handFail();
				UIHelper.showToast(context, "Connection Timeout!", Toast.LENGTH_LONG).show();
			}

			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				// 若返回true 则请求不能在子线程做(会报错)
				return false;
			}
		});
	}

	public void doGisGet(final String url, final RequestParams params, final Context context, final View progress) {
		params.add("from", "android");
		client.addHeader("Cookie", GMapActivity.cookie);
		// 改成HttpClient 的方式，放回的为JSON的数据
		Log.d("mofind", url + "?" + params.toString());
		reqHandle = client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				progress.setVisibility(View.GONE);
				Log.d("mofind", "responseBody : " + json.toString());
				handReponseJson(json);
			}

			@Override
			public void onFinish() {
				progress.setVisibility(View.GONE);
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				progress.setVisibility(View.GONE);
				handFail();
				UIHelper.showToast(context, "Connection Timeout!", Toast.LENGTH_LONG).show();
			}

			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				// 若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}

	public void doGisGetArray(final String url, final RequestParams params, final Context context, final View progress) {
		params.add("from", "android");
		client.addHeader("Cookie", GMapActivity.cookie);
		// 改成HttpClient 的方式，放回的为JSON的数据
		Log.d("mofind", url + "?" + params.toString());
		reqHandle = client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				progress.setVisibility(View.GONE);
				Log.d("mofind", "responseBody : " + response.toString());
				handReponseJsonArray(response);
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				progress.setVisibility(View.GONE);
				handFail();
				UIHelper.showToast(context, "Connection Timeout!", Toast.LENGTH_LONG).show();
			}

			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				// 若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}

	public void doGet(final String url, final RequestParams params, final Context context) {

		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(true);

		final String appendNofity = resources.getString(R.string.sys_load_data_failed);
		client.addHeader("Cookie", GMapActivity.cookie);
		// 改成HttpClient 的方式，放回的为JSON的数据
		reqHandle = client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						processReturn(context, json, appendNofity);
					}

				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_getData_failed) + ", " + appendNofity, Toast.LENGTH_SHORT)
							.show();
					handFail();
				}
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				handFail();
				UIHelper.showToast(context, "Connection Timeout!", Toast.LENGTH_LONG).show();
			}

			@Override
			public boolean getUseSynchronousMode() {
				// TODO Auto-generated method stub
				// 若返回true 则请求不能在子线程做(会报错)
				return false;
			}

		});
	}

	/**
	 * 处理返回值,statusCode=200才调
	 * 
	 * @return true:有效
	 */
	private void processReturn(Context context, JSONObject json, String appendNofity) {
		if (StringUtil.getJsonInt(json, "ret") == 1) {
			handReponseJson(json);
		} else {
			if (StringUtil.getJsonInt(json, "err") == BCSKey.AppVersionException) {
				AlertUi.showVersionError(context);
				return;
			}
			// debug
			if (StringUtil.getJsonInt(json, "err") == BCSKey.Err_WithMsg) {
				AlertUi.showAlertBuilder(context, json.optString("data"));
				return;
			}

			// 提示系统的错误(更加返回的err值)
			AlertUi.showAlertBuilder(context, BCSKey.getReturnStatusById(StringUtil.getJsonInt(json, "err")));
			UIHelper.showToast(context, BCSKey.getReturnStatusById(StringUtil.getJsonInt(json, "err")) + "," + appendNofity, Toast.LENGTH_SHORT)
					.show();
			handFail();
		}
	}

	public void doGetForYasir(final String url, final RequestParams params, final Context context) {
		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		final Resources resources = context.getResources();
		dialog.setTitle("");
		dialog.setMessage("");
		dialog.setCancelable(true);

		final String appendNofity = resources.getString(R.string.sys_load_data_failed);
		client.addHeader("Cookie", GMapActivity.cookie);
		// 改成HttpClient 的方式，放回的为JSON的数据
		client.get(url, params, new JsonHttpResponseHandler("utf-8") {
			@Override
			public void onStart() {
				super.onStart();
				dialog.show();
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				// 处理结果
				if (statusCode == 200) {
					if (!useTheCustom(json)) {
						if (StringUtil.getJsonInt(json, "status") == 1) {
							handReponseJson(json);
						} else {
							JSONArray jArray = json.optJSONArray("errors");
							if (!StringUtil.isNullForJSONArray(jArray)) {
								JSONObject jObj = jArray.optJSONObject(0);
								UIHelper.showToast(context, jObj.optString("errorMessage"));
							}
							handFail();
						}
					}

				} else {
					// 系统的错误 不是返回的200
					UIHelper.showToast(context, resources.getString(R.string.sys_syserror_getData_failed) + ", " + appendNofity, Toast.LENGTH_SHORT)
							.show();
					handFail();
				}
			}

			@Override
			public void onFinish() {
				handFinish();
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				handFail();
				UIHelper.showToast(context, "Connection Timeout!", Toast.LENGTH_LONG).show();
			}

		});
	}

	/**
	 * @Description:用自定义的方法去处理返回结果时的处理
	 * @param @param json
	 * @param @return 返回false则用默认的方法 返回 ture则用自定义的方法
	 */
	public boolean useTheCustom(JSONObject json) {
		return false;
	};

	/**
	 * @Description:用自定义的方法去处理返回结果时的处理
	 * @param @param json
	 * @param @return 返回false则用默认的方法 返回 ture则用自定义的方法
	 */
	public boolean useTheCustom(JSONArray json) {
		return false;
	};

	/**
	 * @Description:用自定义的方法去处理是否反填登陆信息
	 * @param @param json
	 * @param @return 返回false则用不填 返回 ture则填
	 */
	public boolean setLoginInfoParams() {
		return true;
	}

	// =================static=============================

	/**
	 * 获取cookie
	 * 
	 * @param headers
	 */
	public static void getCookie(Map<String, String> headers) {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(GISApplication.getThis());
		List<Cookie> list = myCookieStore.getCookies();
		if (list == null)
			return;

		for (int i = 0; i < list.size(); i++) {
			Cookie co = list.get(i);
			headers.put(co.getName(), co.getValue());
		}
	}

	/**
	 * 获取cookie,如:JSESSIONID=xxx;c2=xxx;
	 * 
	 * @return
	 */
	public static String getCookie() {
		PersistentCookieStore myCookieStore = new PersistentCookieStore(GISApplication.getThis());
		List<Cookie> list = myCookieStore.getCookies();
		if (list == null)
			return "";

		String ret = "";
		for (int i = 0; i < list.size(); i++) {
			Cookie co = list.get(i);
			ret = co.getName() + "=" + co.getValue() + ";";
		}
		return ret;
	}

}
