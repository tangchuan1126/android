package com.android.common;

import com.loopj.android.http.AsyncHttpClient;

public class AsyncHttpClientGenerate {

	private static AsyncHttpClient instance;

	private AsyncHttpClientGenerate() {
	}

	public static synchronized AsyncHttpClient getInstance() {
		if (instance == null) {
			instance = new AsyncHttpClient();
		}
		return instance;
	}
}
