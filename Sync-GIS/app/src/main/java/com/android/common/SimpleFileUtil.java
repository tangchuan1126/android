package com.android.common;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.http.Header;

import oso.gis.GMapActivity;
import oso.gis.TESTActivity;
import utility.RewriteDialog;
import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public abstract class SimpleFileUtil {

	public static AsyncHttpClient client = AsyncHttpClientGenerate.getInstance();
	static {
		client.setTimeout(30 * 1000);
	}

	public abstract void handResponse(int statusCode, Header[] headers, byte[] responseBody);

	public void handFinish() {
	};

	public void handFail() {
	};

	public void doGetFile(final String url, final RequestParams params, final Context context) {
		String[] allowedContentTypes = new String[] { "application/zip;charset=UTF-8" };
		client.addHeader("Cookie", GMapActivity.cookie);
		Log.d("mofind", url + "?" + params.toString());
		client.get(url, params, new BinaryHttpResponseHandler(allowedContentTypes) {

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] binaryData) {
				handResponse(statusCode, headers, binaryData);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] binaryData, Throwable error) {
				handFail();
			}
		});
	}

	public void doGetFile_audio(final String url, File file, final RequestParams params, final Context context) {
		client.addHeader("Cookie", GMapActivity.cookie);
		Log.d("mofind", url + "?" + params.toString());
		client.get(url, params, new FileAsyncHttpResponseHandler(file) {
			@Override
			public void onSuccess(int statusCode, Header[] headers, File file) {
				// TODO Auto-generated method stub
				handResponse(statusCode, headers, null);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
				// TODO Auto-generated method stub
				handFail();
			}
		});
	}

	public void doPostFile(final String url, final Context context, final File file) throws FileNotFoundException {
		final RewriteDialog dialog = RewriteDialog.createDialog(context, 1);
		dialog.setCancelable(true);
		client.addHeader("Cookie", GMapActivity.cookie);
		RequestParams params = new RequestParams();
		params.put("file", file);
		Log.d("mofind", url + "?" + params.toString());
		// 改成HttpClient 的方式，放回的为JSON的数据
		client.post(url, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				if (statusCode == 200) {
					handResponse(statusCode, headers, responseBody);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable throwable) {

			}
		});

	}
}
